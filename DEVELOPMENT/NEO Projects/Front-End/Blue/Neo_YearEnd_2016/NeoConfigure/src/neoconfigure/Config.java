/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Config.java
 *
 * Created on 2010/01/19, 10:54:59
 */

package neoconfigure;

import java.util.prefs.Preferences;

/**
 *
 * @author johanl
 */
public class Config extends javax.swing.JFrame {
    public static final String REALKEY = "za.co.neo";
    public static final String REALKEYTEST = "za.co.neo.textkey";

    Preferences p = Preferences.userRoot();
    Preferences p2 = Preferences.systemRoot();

    /** Creates new form Config */
    public Config() {
        initComponents();

        if (p.get(REALKEYTEST,"ERROR").equalsIgnoreCase("ERROR")) {

            //First time we run this on a machine, so put defaults
           
            p.put(REALKEYTEST, "SET");
            p2.put(REALKEYTEST, "SET");

            //CONTRIBUTIONS
            p.put("za.co.neo.contribution.WS_URL", "http://localhost:8080/KPRepositoryNeo_CO_Test_Rules/WSKPRepositoryNeo_CO_Test_RulesService");
            p.put("za.co.neo.contribution.WS_NAMESPACE", "http://webservice/");
            p.put("za.co.neo.contribution.WS_SERVICENAME", "WSKPRepositoryNeo_CO_Test_RulesService");
            p.put("za.co.neo.contribution.WS_PORTNAME", "WSKPRepositoryNeo_CO_Test_RulesPort");

            p2.put("za.co.neo.contribution.WS_URL", "http://localhost:8080/KPRepositoryNeo_CO_Test_Rules/WSKPRepositoryNeo_CO_Test_RulesService");
            p2.put("za.co.neo.contribution.WS_NAMESPACE", "http://webservice/");
            p2.put("za.co.neo.contribution.WS_SERVICENAME", "WSKPRepositoryNeo_CO_Test_RulesService");
            p2.put("za.co.neo.contribution.WS_PORTNAME", "WSKPRepositoryNeo_CO_Test_RulesPort");

            //DOCUMENTUM
            p.put("za.co.neo.documentum.DATABASE_DRIVER", "oracle.jdbc.driver.OracleDriver");
            p.put("za.co.neo.documentum.DATABASE_STRING", "jdbc:jtds:sqlserver://localhost:1433/SybrinDB");
            p.put("za.co.neo.documentum.DATABASE_USERNAME", "SybrinAdmin");
            p.put("za.co.neo.documentum.DATABASE_PASSWORD", "12345");

            p2.put("za.co.neo.documentum.DATABASE_DRIVER", "oracle.jdbc.driver.OracleDriver");
            p2.put("za.co.neo.documentum.DATABASE_STRING", "jdbc:jtds:sqlserver://localhost:1433/SybrinDB");
            p2.put("za.co.neo.documentum.DATABASE_USERNAME", "SybrinAdmin");
            p2.put("za.co.neo.documentum.DATABASE_PASSWORD", "12345");

            //CLAIMS
            p.put("za.co.neo.claims.WS_URL", "http://localhost:8080/KPRepositoryNeo_CA_Test_Rules/WSKPRepositoryNeo_CA_Test_RulesService?wsdl");
            p.put("za.co.neo.claims.WS_NAMESPACE", "http://webservice/");
            p.put("za.co.neo.claims.WS_SERVICENAME", "WSKPRepositoryNeo_CA_Test_RulesService");
            p.put("za.co.neo.claims.WS_PORTNAME", "WSKPRepositoryNeo_CA_Test_RulesPort");

            p2.put("za.co.neo.claims.WS_URL", "http://localhost:8080/KPRepositoryNeo_CA_Test_Rules/WSKPRepositoryNeo_CA_Test_RulesService?wsdl");
            p2.put("za.co.neo.claims.WS_NAMESPACE", "http://webservice/");
            p2.put("za.co.neo.claims.WS_SERVICENAME", "WSKPRepositoryNeo_CA_Test_RulesService");
            p2.put("za.co.neo.claims.WS_PORTNAME", "WSKPRepositoryNeo_CA_Test_RulesPort");

            //COMMUNICATION
            p.put("za.co.neo.communication.SMS_URL", "http://services.ambientmobile.co.za/sms");
            p.put("za.co.neo.communication.SMS_API_KEY", "98E597CF-3BDD-4876-8D09-A7016D3225DC");
            p.put("za.co.neo.communication.MMS_API_KEY", "D9E47A42-9936-4301-8E61-75EEBA1FE257");
            p.put("za.co.neo.communication.PASSWORD", "4g1L1tyt3ch");

            //Added-----
            p.put("za.co.neo.communication.SMS_PORT", "");
            p.put("za.co.neo.communication.SMS_DEFAULT_NUMBER", "");
            p.put("za.co.neo.communication.DEBUG_MAIL", "");
            p.put("za.co.neo.communication.EMAIL_SEND", "");
            p.put("za.co.neo.communication.EMAIL_DEFAULT_ADDRESS", "");
            p.put("za.co.neo.communication.TRIGGER_PROCESS", "");
            p.put("za.co.neo.communication.TRIGGER_COMMUNICATION", "");
            //p.put("za.co.neo.communication.SMS_LIVE", "");
            //p.put("za.co.neo.communication.EMAIL_LIVE", "");
            //Added------
            p2.put("za.co.neo.communication.SMS_PORT", "");
            p2.put("za.co.neo.communication.SMS_DEFAULT_NUMBER", "");
            p2.put("za.co.neo.communication.DEBUG_MAIL", "");
            p2.put("za.co.neo.communication.EMAIL_SEND", "");
            p2.put("za.co.neo.communication.EMAIL_DEFAULT_ADDRESS", "");
            p2.put("za.co.neo.communication.TRIGGER_PROCESS", "");
            p2.put("za.co.neo.communication.TRIGGER_COMMUNICATION", "");
            //p2.put("za.co.neo.communication.SMS_LIVE", "");
            //p2.put("za.co.neo.communication.EMAIL_LIVE", "");

            p2.put("za.co.neo.communication.SMS_URL", "http://services.ambientmobile.co.za/sms");
            p2.put("za.co.neo.communication.SMS_API_KEY", "98E597CF-3BDD-4876-8D09-A7016D3225DC");
            p2.put("za.co.neo.communication.MMS_API_KEY", "D9E47A42-9936-4301-8E61-75EEBA1FE257");
            p2.put("za.co.neo.communication.PASSWORD", "4g1L1tyt3ch");

            p.put("za.co.neo.communication.SMTP_SERVER","ctmail.caretech.co.za");
            p.put("za.co.neo.communication.EMAIL_USER","RHAdministration@agh.co.za");
            p.put("za.co.neo.communication.EMAIL_PASSWORD","rmadmin");

            p2.put("za.co.neo.communication.SMTP_SERVER","ctmail.caretech.co.za");
            p2.put("za.co.neo.communication.EMAIL_USER","RHAdministration@agh.co.za");
            p2.put("za.co.neo.communication.EMAIL_PASSWORD","rmadmin");

            p.put("za.co.neo.communication.INITIAL_CONTEXT_FACTORY", "org.jnp.interfaces.NamingContextFactory");
            p.put("za.co.neo.communication.URL_PKG_PREFIX", "org.jboss.naming:org.jnp.interfaces");
            p.put("za.co.neo.communication.PROVIDER_URL", "jnp://localhost:1099");
            p.put("za.co.neo.communication.JNDI_CONNECTION_FACTORY", "java:/XAConnectionFactory");

            p2.put("za.co.neo.communication.INITIAL_CONTEXT_FACTORY", "org.jnp.interfaces.NamingContextFactory");
            p2.put("za.co.neo.communication.URL_PKG_PREFIX", "org.jboss.naming:org.jnp.interfaces");
            p2.put("za.co.neo.communication.PROVIDER_URL", "jnp://localhost:1099");
            p2.put("za.co.neo.communication.JNDI_CONNECTION_FACTORY", "java:/XAConnectionFactory");

            //REPORTING
            p.put("za.co.neo.reporting.URL", "http://172.16.1.118:8889/reports/rwservlet?server=neo_reports+report=");
            p2.put("za.co.neo.reporting.URL", "http://172.16.1.118:8889/reports/rwservlet?server=neo_reports+report=");

            //PDC AUTH
            p.put("za.co.neo.pdcauth.WS_URL", "http://localhost:8080/KPRepositoryNeoPDC_CPOP_Test_Rules/WSKPRepositoryNeoPDC_CPOP_Test_RulesService");
            p.put("za.co.neo.pdcauth.WS_NAMESPACE", "http://webservice/");
            p.put("za.co.neo.pdcauth.WS_SERVICENAME", "WSKPRepositoryNeoPDC_CPOP_Test_RulesService");
            p.put("za.co.neo.pdcauth.WS_PORTNAME", "WSKPRepositoryNeoPDC_CPOP_Test_RulesPort");

            p2.put("za.co.neo.pdcauth.WS_URL", "http://localhost:8080/KPRepositoryNeoPDC_CPOP_Test_Rules/WSKPRepositoryNeoPDC_CPOP_Test_RulesService");
            p2.put("za.co.neo.pdcauth.WS_NAMESPACE", "http://webservice/");
            p2.put("za.co.neo.pdcauth.WS_SERVICENAME", "WSKPRepositoryNeoPDC_CPOP_Test_RulesService");
            p2.put("za.co.neo.pdcauth.WS_PORTNAME", "WSKPRepositoryNeoPDC_CPOP_Test_RulesPort");

            //PDC CLAIM
            p.put("za.co.neo.pdcclaims.WS_URL", "http://localhost:8080/KPRepositoryNeoPDC_CPO_Test_Rules/WSKPRepositoryNeoPDC_CPO_Test_RulesService");
            p.put("za.co.neo.pdcclaims.WS_NAMESPACE", "http://webservice/");
            p.put("za.co.neo.pdcclaims.WS_SERVICENAME", "WSKPRepositoryNeoPDC_CPO_Test_RulesService");
            p.put("za.co.neo.pdcclaims.WS_PORTNAME", "WSKPRepositoryNeoPDC_CPO_Test_RulesPort");

            p2.put("za.co.neo.pdcclaims.WS_URL", "http://localhost:8080/KPRepositoryNeoPDC_CPO_Test_Rules/WSKPRepositoryNeoPDC_CPO_Test_RulesService");
            p2.put("za.co.neo.pdcclaims.WS_NAMESPACE", "http://webservice/");
            p2.put("za.co.neo.pdcclaims.WS_SERVICENAME", "WSKPRepositoryNeoPDC_CPO_Test_RulesService");
            p2.put("za.co.neo.pdcclaims.WS_PORTNAME", "WSKPRepositoryNeoPDC_CPO_Test_RulesPort");

            //PDC BIOMETRIC
            p.put("za.co.neo.pdcbio.WS_URL", "http://localhost:8080/KPRepositoryNeoPDC_CPOB_Test_Rules/WSKPRepositoryNeoPDC_CPOB_Test_RulesService");
            p.put("za.co.neo.pdcbio.WS_NAMESPACE", "http://webservice/");
            p.put("za.co.neo.pdcbio.WS_SERVICENAME", "WSKPRepositoryNeoPDC_CPOB_Test_RulesService");
            p.put("za.co.neo.pdcbio.WS_PORTNAME", "WSKPRepositoryNeoPDC_CPOB_Test_RulesPort");

            p2.put("za.co.neo.pdcbio.WS_URL", "http://localhost:8080/KPRepositoryNeoPDC_CPOB_Test_Rules/WSKPRepositoryNeoPDC_CPOB_Test_RulesService");
            p2.put("za.co.neo.pdcbio.WS_NAMESPACE", "http://webservice/");
            p2.put("za.co.neo.pdcbio.WS_SERVICENAME", "WSKPRepositoryNeoPDC_CPOB_Test_RulesService");
            p2.put("za.co.neo.pdcbio.WS_PORTNAME", "WSKPRepositoryNeoPDC_CPOB_Test_RulesPort");

            //PREAUTH
            p.put("za.co.neo.preauth.WS_URL", "http://localhost:8080/KPRepositoryNeo_CO_Test_Rules/WSKPRepositoryNeo_CO_Test_RulesService");
            p.put("za.co.neo.preauth.WS_NAMESPACE", "http://webservice/");
            p.put("za.co.neo.preauth.WS_SERVICENAME", "WSKPRepositoryNeo_CA_Test_RulesService");
            p.put("za.co.neo.preauth.WS_PORTNAME", "WSKPRepositoryNeo_CA_Test_RulesPort");

            p2.put("za.co.neo.preauth.WS_URL", "http://localhost:8080/KPRepositoryNeo_CO_Test_Rules/WSKPRepositoryNeo_CO_Test_RulesService");
            p2.put("za.co.neo.preauth.WS_NAMESPACE", "http://webservice/");
            p2.put("za.co.neo.preauth.WS_SERVICENAME", "WSKPRepositoryNeo_CA_Test_RulesService");
            p2.put("za.co.neo.preauth.WS_PORTNAME", "WSKPRepositoryNeo_CA_Test_RulesPort");

            if (p.get("za.co.neo.contribution","ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField1.setText(p2.get("za.co.neo.contribution.WS_URL","ERROR - KEY NOT FOUND"));
                textField2.setText(p2.get("za.co.neo.contribution.WS_NAMESPACE","ERROR - KEY NOT FOUND"));
                textField3.setText(p2.get("za.co.neo.contribution.WS_SERVICENAME","ERROR - KEY NOT FOUND"));
                textField4.setText(p2.get("za.co.neo.contribution.WS_PORTNAME","ERROR - KEY NOT FOUND"));
            } else {
                textField1.setText(p.get("za.co.neo.contribution.WS_URL","ERROR - KEY NOT FOUND"));
                textField2.setText(p.get("za.co.neo.contribution.WS_NAMESPACE","ERROR - KEY NOT FOUND"));
                textField3.setText(p.get("za.co.neo.contribution.WS_SERVICENAME","ERROR - KEY NOT FOUND"));
                textField4.setText(p.get("za.co.neo.contribution.WS_PORTNAME","ERROR - KEY NOT FOUND"));
            }

            if (p.get("za.co.neo.document","ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField5.setText(p2.get("za.co.neo.document.DESIGN_FILE_DRIVE","ERROR - KEY NOT FOUND"));
                textField6.setText(p2.get("za.co.neo.document.SCAN_DRIVE","ERROR - KEY NOT FOUND"));
                textField7.setText(p2.get("za.co.neo.document.SCAN_FOLDER","ERROR - KEY NOT FOUND"));
                textField8.setText(p2.get("za.co.neo.document.WORK_DRIVE","ERROR - KEY NOT FOUND"));
                textField_9.setText(p2.get("za.co.neo.document.WORK_FOLDER","ERROR - KEY NOT FOUND"));
                textField_10.setText(p2.get("za.co.neo.document.INDEX_DRIVE","ERROR - KEY NOT FOUND"));
                textField_11.setText(p2.get("za.co.neo.document.PRINT_POOL_DRIVE","ERROR - KEY NOT FOUND"));
                textField_12.setText(p2.get("za.co.neo.document.PRINT_POOL_FOLDER","ERROR - KEY NOT FOUND"));
                textField_13.setText(p2.get("za.co.neo.document.BULK_PRINTER","ERROR - KEY NOT FOUND"));
                textField_14.setText(p2.get("za.co.neo.document.CARD_PRINTER","ERROR - KEY NOT FOUND"));
                textField_15.setText(p2.get("za.co.neo.document.PRINTER_WSDL","ERROR - KEY NOT FOUND"));
                  
            } else {
                textField5.setText(p.get("za.co.neo.document.DESIGN_FILE_DRIVE","ERROR - KEY NOT FOUND"));
                textField6.setText(p.get("za.co.neo.document.SCAN_DRIVE","ERROR - KEY NOT FOUND"));
                textField7.setText(p.get("za.co.neo.document.SCAN_FOLDER","ERROR - KEY NOT FOUND"));
                textField8.setText(p.get("za.co.neo.document.WORK_DRIVE","ERROR - KEY NOT FOUND"));
                textField_9.setText(p.get("za.co.neo.document.WORK_FOLDER","ERROR - KEY NOT FOUND"));
                textField_10.setText(p.get("za.co.neo.document.INDEX_DRIVE","ERROR - KEY NOT FOUND"));
                textField_11.setText(p.get("za.co.neo.document.PRINT_POOL_DRIVE","ERROR - KEY NOT FOUND"));
                textField_12.setText(p.get("za.co.neo.document.PRINT_POOL_FOLDER","ERROR - KEY NOT FOUND"));
                textField_13.setText(p.get("za.co.neo.document.BULK_PRINTER","ERROR - KEY NOT FOUND"));
                textField_14.setText(p.get("za.co.neo.document.CARD_PRINTER","ERROR - KEY NOT FOUND"));
                textField_15.setText(p.get("za.co.neo.document.PRINTER_WSDL","ERROR - KEY NOT FOUND"));
            }

            if (p.get("za.co.neo.claims","ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField9.setText(p2.get("za.co.neo.claims.WS_URL","ERROR - KEY NOT FOUND"));
                textField10.setText(p2.get("za.co.neo.claims.WS_NAMESPACE","ERROR - KEY NOT FOUND"));
                textField11.setText(p2.get("za.co.neo.claims.WS_SERVICENAME","ERROR - KEY NOT FOUND"));
                textField12.setText(p2.get("za.co.neo.claims.WS_PORTNAME","ERROR - KEY NOT FOUND"));
            } else {
                textField9.setText(p.get("za.co.neo.claims.WS_URL","ERROR - KEY NOT FOUND"));
                textField10.setText(p.get("za.co.neo.claims.WS_NAMESPACE","ERROR - KEY NOT FOUND"));
                textField11.setText(p.get("za.co.neo.claims.WS_SERVICENAME","ERROR - KEY NOT FOUND"));
                textField12.setText(p.get("za.co.neo.claims.WS_PORTNAME","ERROR - KEY NOT FOUND"));
            }
          
            if (p.get("za.co.neo.communication","ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField13.setText(p2.get("za.co.neo.communication.SMS_URL","ERROR - KEY NOT FOUND"));
                textField14.setText(p2.get("za.co.neo.communication.SMS_API_KEY","ERROR - KEY NOT FOUND"));
                textField15.setText(p2.get("za.co.neo.communication.MMS_API_KEY","ERROR - KEY NOT FOUND"));
                textField16.setText(p2.get("za.co.neo.communication.PASSWORD","ERROR - KEY NOT FOUND"));

                textField17.setText(p2.get("za.co.neo.communication.SMTP_SERVER","ERROR - KEY NOT FOUND"));
                //textField18.setText(p2.get("za.co.neo.communication.EMAIL_USER","ERROR - KEY NOT FOUND"));
                //textField19.setText(p2.get("za.co.neo.communication.EMAIL_PASSWORD", "ERROR - KEY NOT FOUND"));

                textField21.setText(p2.get("za.co.neo.communication.INITIAL_CONTEXT_FACTORY", "ERROR - KEY NOT FOUND"));
                textField22.setText(p2.get("za.co.neo.communication.URL_PKG_PREFIX", "ERROR - KEY NOT FOUND"));
                textField23.setText(p2.get("za.co.neo.communication.PROVIDER_URL", "ERROR - KEY NOT FOUND"));
                textField24.setText(p2.get("za.co.neo.communication.JNDI_CONNECTION_FACTORY", "ERROR - KEY NOT FOUND"));

                //Added-----
                jTextField1.setText(p2.get("za.co.neo.communication.SMS_PORT", "ERROR - KEY NOT FOUND"));
                //jTextField2.setText(p2.get("za.co.neo.communication.EMAIL_URL", "ERROR - KEY NOT FOUND"));
                jTextField3.setText(p2.get("za.co.neo.communication.DEBUG_MAIL", "ERROR - KEY NOT FOUND"));
                jTextField4.setText(p2.get("za.co.neo.communication.TRIGGER_PROCESS", "ERROR - KEY NOT FOUND"));
                jTextField5.setText(p2.get("za.co.neo.communication.TRIGGER_COMMUNICATION", "ERROR - KEY NOT FOUND"));
                jTextField6.setText(p2.get("za.co.neo.communication.SMS_DEFAULT_NUMBER", "ERROR - KEY NOT FOUND"));
                jTextField7.setText(p2.get("za.co.neo.communication.EMAIL_DEFAULT_ADDRESS", "ERROR - KEY NOT FOUND"));
                jTextField10.setText(p2.get("za.co.neo.communication.EMAIL_SEND", "ERROR - KEY NOT FOUND"));

            } else {
                textField13.setText(p.get("za.co.neo.communication.SMS_URL","ERROR - KEY NOT FOUND"));
                textField14.setText(p.get("za.co.neo.communication.SMS_API_KEY","ERROR - KEY NOT FOUND"));
                textField15.setText(p.get("za.co.neo.communication.MMS_API_KEY","ERROR - KEY NOT FOUND"));
                textField16.setText(p.get("za.co.neo.communication.PASSWORD","ERROR - KEY NOT FOUND"));

                textField17.setText(p.get("za.co.neo.communication.SMTP_SERVER","ERROR - KEY NOT FOUND"));
                //textField18.setText(p.get("za.co.neo.communication.EMAIL_USER","ERROR - KEY NOT FOUND"));
                //textField19.setText(p.get("za.co.neo.communication.EMAIL_PASSWORD", "ERROR - KEY NOT FOUND"));

                textField21.setText(p.get("za.co.neo.communication.INITIAL_CONTEXT_FACTORY", "ERROR - KEY NOT FOUND"));
                textField22.setText(p.get("za.co.neo.communication.URL_PKG_PREFIX", "ERROR - KEY NOT FOUND"));
                textField23.setText(p.get("za.co.neo.communication.PROVIDER_URL", "ERROR - KEY NOT FOUND"));
                textField24.setText(p.get("za.co.neo.communication.JNDI_CONNECTION_FACTORY", "ERROR - KEY NOT FOUND"));

                //Added-----
                jTextField1.setText(p.get("za.co.neo.communication.SMS_PORT", "ERROR - KEY NOT FOUND"));
                //jTextField2.setText(p.get("za.co.neo.communication.EMIL_URL", "ERROR - KEY NOT FOUND"));
                jTextField3.setText(p.get("za.co.neo.communication.DEBUG_MAIL", "ERROR - KEY NOT FOUND"));
                jTextField4.setText(p.get("za.co.neo.communication.TRIGGER_PROCESS", "ERROR - KEY NOT FOUND"));
                jTextField5.setText(p.get("za.co.neo.communication.TRIGGER_COMMUNICATION", "ERROR - KEY NOT FOUND"));
                jTextField6.setText(p.get("za.co.neo.communication.SMS_DEFAULT_NUMBER", "ERROR - KEY NOT FOUND"));
                jTextField7.setText(p.get("za.co.neo.communication.EMAIL_DEFAULT_ADDRESS", "ERROR - KEY NOT FOUND"));
                jTextField10.setText(p.get("za.co.neo.communication.EMAIL_SEND", "ERROR - KEY NOT FOUND"));
            }

            if (p.get("za.co.neo.reporting","ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField20.setText(p2.get("za.co.neo.reporting.URL","ERROR - KEY NOT FOUND"));
            } else {
                textField20.setText(p.get("za.co.neo.reporting.URL","ERROR - KEY NOT FOUND"));
            }

            if (p.get("za.co.neo.pdcauth", "ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField25.setText(p2.get("za.co.neo.pdcauth.WS_URL", "ERROR - KEY NOT FOUND"));
                textField26.setText(p2.get("za.co.neo.pdcauth.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField27.setText(p2.get("za.co.neo.pdcauth.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField28.setText(p2.get("za.co.neo.pdcauth.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
            } else {
                textField25.setText(p.get("za.co.neo.pdcauth.WS_URL", "ERROR - KEY NOT FOUND"));
                textField26.setText(p.get("za.co.neo.pdcauth.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField27.setText(p.get("za.co.neo.pdcauth.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField28.setText(p.get("za.co.neo.pdcauth.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
            }
          
            if (p.get("za.co.neo.pdcclaims", "ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField25.setText(p2.get("za.co.neo.pdcclaims.WS_URL", "ERROR - KEY NOT FOUND"));
                textField26.setText(p2.get("za.co.neo.pdcclaims.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField27.setText(p2.get("za.co.neo.pdcclaims.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField28.setText(p2.get("za.co.neo.pdcclaims.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
            } else {
                textField25.setText(p.get("za.co.neo.pdcclaims.WS_URL", "ERROR - KEY NOT FOUND"));
                textField26.setText(p.get("za.co.neo.pdcclaims.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField27.setText(p.get("za.co.neo.pdcclaims.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField28.setText(p.get("za.co.neo.pdcclaims.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
            }

            if (p.get("za.co.neo.pdcbio", "ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField25.setText(p2.get("za.co.neo.pdcbio.WS_URL", "ERROR - KEY NOT FOUND"));
                textField26.setText(p2.get("za.co.neo.pdcbio.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField27.setText(p2.get("za.co.neo.pdcbio.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField28.setText(p2.get("za.co.neo.pdcbio.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
            } else {
                textField25.setText(p.get("za.co.neo.pdcbio.WS_URL", "ERROR - KEY NOT FOUND"));
                textField26.setText(p.get("za.co.neo.pdcbio.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField27.setText(p.get("za.co.neo.pdcbio.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField28.setText(p.get("za.co.neo.pdcbio.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
            }

            if (p.get("za.co.neo.preauth", "ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField40.setText(p2.get("za.co.neo.preauth.WS_URL", "ERROR - KEY NOT FOUND"));
                textField39.setText(p2.get("za.co.neo.preauth.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField38.setText(p2.get("za.co.neo.preauth.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField37.setText(p2.get("za.co.neo.preauth.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
            } else {
                textField40.setText(p.get("za.co.neo.preauth.WS_URL", "ERROR - KEY NOT FOUND"));
                textField39.setText(p.get("za.co.neo.preauth.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField38.setText(p.get("za.co.neo.preauth.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField37.setText(p.get("za.co.neo.preauth.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
            }
        } else {
            //Registry key exists, so display stored values
                       
            //CONTRIBUTIONS
            if (p.get("za.co.neo.contribution","ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField1.setText(p2.get("za.co.neo.contribution.WS_URL","ERROR - KEY NOT FOUND"));
                textField2.setText(p2.get("za.co.neo.contribution.WS_NAMESPACE","ERROR - KEY NOT FOUND"));
                textField3.setText(p2.get("za.co.neo.contribution.WS_SERVICENAME","ERROR - KEY NOT FOUND"));
                textField4.setText(p2.get("za.co.neo.contribution.WS_PORTNAME","ERROR - KEY NOT FOUND"));
            } else {
                textField1.setText(p.get("za.co.neo.contribution.WS_URL","ERROR - KEY NOT FOUND"));
                textField2.setText(p.get("za.co.neo.contribution.WS_NAMESPACE","ERROR - KEY NOT FOUND"));
                textField3.setText(p.get("za.co.neo.contribution.WS_SERVICENAME","ERROR - KEY NOT FOUND"));
                textField4.setText(p.get("za.co.neo.contribution.WS_PORTNAME","ERROR - KEY NOT FOUND"));
            }

            if (p.get("za.co.neo.documentum","ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField5.setText(p2.get("za.co.neo.document.DESIGN_FILE_DRIVE","ERROR - KEY NOT FOUND"));
                textField6.setText(p2.get("za.co.neo.document.SCAN_DRIVE","ERROR - KEY NOT FOUND"));
                textField7.setText(p2.get("za.co.neo.document.SCAN_FOLDER","ERROR - KEY NOT FOUND"));
                textField8.setText(p2.get("za.co.neo.document.WORK_DRIVE","ERROR - KEY NOT FOUND"));
                textField_9.setText(p2.get("za.co.neo.document.WORK_FOLDER","ERROR - KEY NOT FOUND"));
                textField_10.setText(p2.get("za.co.neo.document.INDEX_DRIVE","ERROR - KEY NOT FOUND"));
                textField_11.setText(p2.get("za.co.neo.document.PRINT_POOL_DRIVE","ERROR - KEY NOT FOUND"));
                textField_12.setText(p2.get("za.co.neo.document.PRINT_POOL_FOLDER","ERROR - KEY NOT FOUND"));
                textField_13.setText(p2.get("za.co.neo.document.BULK_PRINTER","ERROR - KEY NOT FOUND"));
                textField_14.setText(p2.get("za.co.neo.document.CARD_PRINTER","ERROR - KEY NOT FOUND"));
                textField_15.setText(p2.get("za.co.neo.document.PRINTER_WSDL","ERROR - KEY NOT FOUND"));
            } else {
                textField5.setText(p2.get("za.co.neo.document.DESIGN_FILE_DRIVE","ERROR - KEY NOT FOUND"));
                textField6.setText(p2.get("za.co.neo.document.SCAN_DRIVE","ERROR - KEY NOT FOUND"));
                textField7.setText(p2.get("za.co.neo.document.SCAN_FOLDER","ERROR - KEY NOT FOUND"));
                textField8.setText(p2.get("za.co.neo.document.WORK_DRIVE","ERROR - KEY NOT FOUND"));
                textField_9.setText(p2.get("za.co.neo.document.WORK_FOLDER","ERROR - KEY NOT FOUND"));
                textField_10.setText(p2.get("za.co.neo.document.INDEX_DRIVE","ERROR - KEY NOT FOUND"));
                textField_11.setText(p2.get("za.co.neo.document.PRINT_POOL_DRIVE","ERROR - KEY NOT FOUND"));
                textField_12.setText(p2.get("za.co.neo.document.PRINT_POOL_FOLDER","ERROR - KEY NOT FOUND"));
                textField_13.setText(p2.get("za.co.neo.document.BULK_PRINTER","ERROR - KEY NOT FOUND"));
                textField_14.setText(p2.get("za.co.neo.document.CARD_PRINTER","ERROR - KEY NOT FOUND"));
                textField_15.setText(p2.get("za.co.neo.document.PRINTER_WSDL","ERROR - KEY NOT FOUND"));
            }

            if (p.get("za.co.neo.claims","ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField9.setText(p2.get("za.co.neo.claims.WS_URL","ERROR - KEY NOT FOUND"));
                textField10.setText(p2.get("za.co.neo.claims.WS_NAMESPACE","ERROR - KEY NOT FOUND"));
                textField11.setText(p2.get("za.co.neo.claims.WS_SERVICENAME","ERROR - KEY NOT FOUND"));
                textField12.setText(p2.get("za.co.neo.claims.WS_PORTNAME","ERROR - KEY NOT FOUND"));
            } else {
                textField9.setText(p.get("za.co.neo.claims.WS_URL","ERROR - KEY NOT FOUND"));
                textField10.setText(p.get("za.co.neo.claims.WS_NAMESPACE","ERROR - KEY NOT FOUND"));
                textField11.setText(p.get("za.co.neo.claims.WS_SERVICENAME","ERROR - KEY NOT FOUND"));
                textField12.setText(p.get("za.co.neo.claims.WS_PORTNAME","ERROR - KEY NOT FOUND"));
            }

            if (p.get("za.co.neo.communication","ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField13.setText(p2.get("za.co.neo.communication.SMS_URL","ERROR - KEY NOT FOUND"));
                textField14.setText(p2.get("za.co.neo.communication.SMS_API_KEY","ERROR - KEY NOT FOUND"));
                textField15.setText(p2.get("za.co.neo.communication.MMS_API_KEY","ERROR - KEY NOT FOUND"));
                textField16.setText(p2.get("za.co.neo.communication.PASSWORD","ERROR - KEY NOT FOUND"));

                textField17.setText(p2.get("za.co.neo.communication.SMTP_SERVER","ERROR - KEY NOT FOUND"));
                //textField18.setText(p2.get("za.co.neo.communication.EMAIL_USER","ERROR - KEY NOT FOUND"));
                //textField19.setText(p2.get("za.co.neo.communication.EMAIL_PASSWORD", "ERROR - KEY NOT FOUND"));

                textField21.setText(p2.get("za.co.neo.communication.INITIAL_CONTEXT_FACTORY", "ERROR - KEY NOT FOUND"));
                textField22.setText(p2.get("za.co.neo.communication.URL_PKG_PREFIX", "ERROR - KEY NOT FOUND"));
                textField23.setText(p2.get("za.co.neo.communication.PROVIDER_URL", "ERROR - KEY NOT FOUND"));
                textField24.setText(p2.get("za.co.neo.communication.JNDI_CONNECTION_FACTORY", "ERROR - KEY NOT FOUND"));

                //Added-----
                jTextField1.setText(p2.get("za.co.neo.communication.SMS_PORT", "ERROR - KEY NOT FOUND"));
                //jTextField2.setText(p2.get("za.co.neo.communication.EMIL_URL", "ERROR - KEY NOT FOUND"));
                jTextField3.setText(p2.get("za.co.neo.communication.DEBUG_MAIL", "ERROR - KEY NOT FOUND"));
                jTextField4.setText(p2.get("za.co.neo.communication.TRIGGER_PROCESS", "ERROR - KEY NOT FOUND"));
                jTextField5.setText(p2.get("za.co.neo.communication.TRIGGER_COMMUNICATION", "ERROR - KEY NOT FOUND"));
                jTextField6.setText(p2.get("za.co.neo.communication.SMS_DEFAULT_NUMBER", "ERROR - KEY NOT FOUND"));
                jTextField7.setText(p2.get("za.co.neo.communication.EMAIL_DEFAULT_ADDRESS", "ERROR - KEY NOT FOUND"));
                jTextField10.setText(p2.get("za.co.neo.communication.EMAIL_SEND", "ERROR - KEY NOT FOUND"));
            } else {
                textField13.setText(p.get("za.co.neo.communication.SMS_URL","ERROR - KEY NOT FOUND"));
                textField14.setText(p.get("za.co.neo.communication.SMS_API_KEY","ERROR - KEY NOT FOUND"));
                textField15.setText(p.get("za.co.neo.communication.MMS_API_KEY","ERROR - KEY NOT FOUND"));
                textField16.setText(p.get("za.co.neo.communication.PASSWORD","ERROR - KEY NOT FOUND"));

                textField17.setText(p.get("za.co.neo.communication.SMTP_SERVER","ERROR - KEY NOT FOUND"));
                //textField18.setText(p.get("za.co.neo.communication.EMAIL_USER","ERROR - KEY NOT FOUND"));
                //textField19.setText(p.get("za.co.neo.communication.EMAIL_PASSWORD", "ERROR - KEY NOT FOUND"));

                textField21.setText(p.get("za.co.neo.communication.INITIAL_CONTEXT_FACTORY", "ERROR - KEY NOT FOUND"));
                textField22.setText(p.get("za.co.neo.communication.URL_PKG_PREFIX", "ERROR - KEY NOT FOUND"));
                textField23.setText(p.get("za.co.neo.communication.PROVIDER_URL", "ERROR - KEY NOT FOUND"));
                textField24.setText(p.get("za.co.neo.communication.JNDI_CONNECTION_FACTORY", "ERROR - KEY NOT FOUND"));

                //Added-----
                jTextField1.setText(p.get("za.co.neo.communication.SMS_PORT", "ERROR - KEY NOT FOUND"));
                //jTextField2.setText(p.get("za.co.neo.communication.EMIL_URL", "ERROR - KEY NOT FOUND"));
                jTextField3.setText(p.get("za.co.neo.communication.DEBUG_MAIL", "ERROR - KEY NOT FOUND"));
                jTextField4.setText(p.get("za.co.neo.communication.TRIGGER_PROCESS", "ERROR - KEY NOT FOUND"));
                jTextField5.setText(p.get("za.co.neo.communication.TRIGGER_COMMUNICATION", "ERROR - KEY NOT FOUND"));
                jTextField6.setText(p2.get("za.co.neo.communication.SMS_DEFAULT_NUMBER", "ERROR - KEY NOT FOUND"));
                jTextField7.setText(p2.get("za.co.neo.communication.EMAIL_DEFAULT_ADDRESS", "ERROR - KEY NOT FOUND"));
                jTextField10.setText(p2.get("za.co.neo.communication.EMAIL_SEND", "ERROR - KEY NOT FOUND"));
            }
          
            if (p.get("za.co.neo.reporting","ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField20.setText(p2.get("za.co.neo.reporting.URL","ERROR - KEY NOT FOUND"));
            } else {
                textField20.setText(p.get("za.co.neo.reporting.URL","ERROR - KEY NOT FOUND"));
            }

            if (p.get("za.co.neo.pdcauth", "ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField25.setText(p2.get("za.co.neo.pdcauth.WS_URL", "ERROR - KEY NOT FOUND"));
                textField26.setText(p2.get("za.co.neo.pdcauth.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField27.setText(p2.get("za.co.neo.pdcauth.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField28.setText(p2.get("za.co.neo.pdcauth.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
            } else {
                textField25.setText(p.get("za.co.neo.pdcauth.WS_URL", "ERROR - KEY NOT FOUND"));
                textField26.setText(p.get("za.co.neo.pdcauth.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField27.setText(p.get("za.co.neo.pdcauth.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField28.setText(p.get("za.co.neo.pdcauth.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
            }

            if (p.get("za.co.neo.pdcclaims", "ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField29.setText(p2.get("za.co.neo.pdcclaims.WS_URL", "ERROR - KEY NOT FOUND"));
                textField30.setText(p2.get("za.co.neo.pdcclaims.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField31.setText(p2.get("za.co.neo.pdcclaims.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField32.setText(p2.get("za.co.neo.pdcclaims.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
            } else {
                textField29.setText(p.get("za.co.neo.pdcclaims.WS_URL", "ERROR - KEY NOT FOUND"));
                textField30.setText(p.get("za.co.neo.pdcclaims.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField31.setText(p.get("za.co.neo.pdcclaims.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField32.setText(p.get("za.co.neo.pdcclaims.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
            }

            if (p.get("za.co.neo.pdcbio", "ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField33.setText(p2.get("za.co.neo.pdcbio.WS_URL", "ERROR - KEY NOT FOUND"));
                textField34.setText(p2.get("za.co.neo.pdcbio.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField35.setText(p2.get("za.co.neo.pdcbio.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField36.setText(p2.get("za.co.neo.pdcbio.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
            } else {
                textField33.setText(p.get("za.co.neo.pdcbio.WS_URL", "ERROR - KEY NOT FOUND"));
                textField34.setText(p.get("za.co.neo.pdcbio.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField35.setText(p.get("za.co.neo.pdcbio.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField36.setText(p.get("za.co.neo.pdcbio.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
            }

            if (p.get("za.co.neo.preauth", "ERROR - KEY NOT FOUND").equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                textField40.setText(p2.get("za.co.neo.preauth.WS_URL", "ERROR - KEY NOT FOUND"));
                textField39.setText(p2.get("za.co.neo.preauth.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField38.setText(p2.get("za.co.neo.preauth.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField37.setText(p2.get("za.co.neo.preauth.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
            } else {
                textField40.setText(p.get("za.co.neo.preauth.WS_URL", "ERROR - KEY NOT FOUND"));
                textField39.setText(p.get("za.co.neo.preauth.WS_NAMESPACE", "ERROR - KEY NOT FOUND"));
                textField38.setText(p.get("za.co.neo.preauth.WS_SERVICENAME", "ERROR - KEY NOT FOUND"));
                textField37.setText(p.get("za.co.neo.preauth.WS_PORTNAME", "ERROR - KEY NOT FOUND"));
           }

        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        textField1 = new javax.swing.JTextField();
        textField2 = new javax.swing.JTextField();
        textField3 = new javax.swing.JTextField();
        textField4 = new javax.swing.JTextField();
        contributionButton = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        textField12 = new javax.swing.JTextField();
        textField11 = new javax.swing.JTextField();
        textField10 = new javax.swing.JTextField();
        textField9 = new javax.swing.JTextField();
        claimsButton = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        textField20 = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        reportsButton = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        textField25 = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        textField26 = new javax.swing.JTextField();
        textField27 = new javax.swing.JTextField();
        textField28 = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        textField29 = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        textField30 = new javax.swing.JTextField();
        textField31 = new javax.swing.JTextField();
        textField32 = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        textField33 = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        textField34 = new javax.swing.JTextField();
        textField35 = new javax.swing.JTextField();
        textField36 = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        textField37 = new javax.swing.JTextField();
        textField38 = new javax.swing.JTextField();
        textField39 = new javax.swing.JTextField();
        textField40 = new javax.swing.JTextField();
        claimsButton1 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        textField16 = new javax.swing.JTextField();
        textField15 = new javax.swing.JTextField();
        textField14 = new javax.swing.JTextField();
        textField13 = new javax.swing.JTextField();
        communicationButton = new javax.swing.JButton();
        textField17 = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        textField21 = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        textField22 = new javax.swing.JTextField();
        textField23 = new javax.swing.JTextField();
        textField24 = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jLabel50 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jLabel54 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        jLabel45 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        textField5 = new javax.swing.JTextField();
        textField6 = new javax.swing.JTextField();
        textField7 = new javax.swing.JTextField();
        textField8 = new javax.swing.JTextField();
        documentumButton = new javax.swing.JButton();
        jLabel_9 = new javax.swing.JLabel();
        jLabel_10 = new javax.swing.JLabel();
        jLabel_11 = new javax.swing.JLabel();
        jLabel_12 = new javax.swing.JLabel();
        jLabel_13 = new javax.swing.JLabel();
        jLabel_14 = new javax.swing.JLabel();
        textField_9 = new javax.swing.JTextField();
        textField_10 = new javax.swing.JTextField();
        textField_11 = new javax.swing.JTextField();
        textField_12 = new javax.swing.JTextField();
        textField_13 = new javax.swing.JTextField();
        textField_14 = new javax.swing.JTextField();
        jLabel_15 = new javax.swing.JLabel();
        textField_15 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("WS_URL ");

        jLabel2.setText("WS_NAMESPACE");

        jLabel3.setText("WS_SERVICENAME");

        jLabel4.setText("WS_PORTNAME");

        contributionButton.setText("SAVE");
        contributionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                contributionButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addGap(109, 109, 109)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(textField4)
                    .addComponent(textField3)
                    .addComponent(textField2)
                    .addComponent(textField1, javax.swing.GroupLayout.PREFERRED_SIZE, 595, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(contributionButton)
                .addGap(328, 328, 328))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(textField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(textField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(textField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(textField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addComponent(contributionButton)
                .addContainerGap(472, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("CONTRIBUTIONS", jPanel1);

        jLabel9.setText("WS_URL ");

        jLabel10.setText("WS_NAMESPACE");

        jLabel11.setText("WS_SERVICENAME");

        jLabel12.setText("WS_PORTNAME");

        claimsButton.setText("SAVE");
        claimsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                claimsButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12))
                .addGap(109, 109, 109)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(textField12)
                    .addComponent(textField11)
                    .addComponent(textField10)
                    .addComponent(textField9, javax.swing.GroupLayout.PREFERRED_SIZE, 595, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(claimsButton)
                .addGap(312, 312, 312))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(textField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(textField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(textField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(textField12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addComponent(claimsButton)
                .addContainerGap(472, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("CLAIMS", jPanel3);

        jLabel20.setText("URL");

        reportsButton.setText("SAVE");
        reportsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportsButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addComponent(jLabel20)
                        .addGap(46, 46, 46)
                        .addComponent(textField20, javax.swing.GroupLayout.PREFERRED_SIZE, 597, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(286, 286, 286)
                        .addComponent(reportsButton)))
                .addContainerGap(116, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(textField20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(64, 64, 64)
                .addComponent(reportsButton)
                .addContainerGap(566, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("REPORTS", jPanel5);

        jLabel25.setText("WS_URL");

        jLabel26.setText("WS_NAMESPACE");

        jLabel27.setText("WS_SERVICENAME");

        jLabel28.setText("JNDI CONNECTION FACTORY");

        jButton1.setText("SAVE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdcAuthButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton1)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel25)
                            .addComponent(jLabel28)
                            .addComponent(jLabel27)
                            .addComponent(jLabel26))
                        .addGap(51, 51, 51)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textField25, javax.swing.GroupLayout.DEFAULT_SIZE, 521, Short.MAX_VALUE)
                            .addComponent(textField26, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 521, Short.MAX_VALUE)
                            .addComponent(textField27, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 521, Short.MAX_VALUE)
                            .addComponent(textField28, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 521, Short.MAX_VALUE))))
                .addGap(99, 99, 99))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(textField25, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textField26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textField27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textField28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap(531, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("PDC Auth", jPanel6);

        jLabel29.setText("WS_URL");

        jLabel30.setText("WS_NAMESPACE");

        jLabel31.setText("WS_SERVICENAME");

        jLabel32.setText("JNDI CONNECTION FACTORY");

        jButton2.setText("SAVE");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdcClaimButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton2)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel29)
                            .addComponent(jLabel32)
                            .addComponent(jLabel31)
                            .addComponent(jLabel30))
                        .addGap(51, 51, 51)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textField29, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                            .addComponent(textField30, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                            .addComponent(textField31, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                            .addComponent(textField32, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE))))
                .addGap(113, 113, 113))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(textField29, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textField30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel30))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textField31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textField32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel32))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addContainerGap(531, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("PDC Claim", jPanel7);

        jLabel33.setText("WS_URL");

        jLabel34.setText("WS_NAMESPACE");

        jLabel35.setText("WS_SERVICENAME");

        jLabel36.setText("JNDI CONNECTION FACTORY");

        jButton3.setText("SAVE");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdcBiometricButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton3)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel33)
                            .addComponent(jLabel36)
                            .addComponent(jLabel35)
                            .addComponent(jLabel34))
                        .addGap(51, 51, 51)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textField33, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                            .addComponent(textField34, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                            .addComponent(textField35, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                            .addComponent(textField36, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE))))
                .addGap(113, 113, 113))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel33)
                    .addComponent(textField33, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textField34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel34))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textField35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textField36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3)
                .addContainerGap(531, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("PDC Biometric", jPanel8);

        jLabel37.setText("WS_URL ");

        jLabel38.setText("WS_NAMESPACE");

        jLabel39.setText("WS_SERVICENAME");

        jLabel40.setText("WS_PORTNAME");

        claimsButton1.setText("SAVE");
        claimsButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                preAuthButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(claimsButton1)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel37)
                            .addComponent(jLabel38)
                            .addComponent(jLabel39)
                            .addComponent(jLabel40))
                        .addGap(109, 109, 109)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(textField37)
                            .addComponent(textField38)
                            .addComponent(textField39)
                            .addComponent(textField40, javax.swing.GroupLayout.PREFERRED_SIZE, 595, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel37)
                    .addComponent(textField40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38)
                    .addComponent(textField39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel39)
                    .addComponent(textField38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel40)
                    .addComponent(textField37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(claimsButton1)
                .addContainerGap(516, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("PREAUTH", jPanel9);

        jLabel13.setText("SMS URL");

        jLabel14.setText("SMS API KEY");

        jLabel15.setText("MMS API KEY");

        jLabel16.setText("PASSWORD");

        communicationButton.setText("SAVE");
        communicationButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                communicationButtonActionPerformed(evt);
            }
        });

        jLabel17.setText("SMTP SERVER");

        jLabel21.setText("INITIAL CONTEXT FACTORY");

        jLabel22.setText("URL PKG PREFIX");

        jLabel23.setText("PROVIDER URL");

        jLabel24.setText("JNDI CONNECTION FACTORY");

        jLabel41.setText("SMS PORT");

        jLabel42.setText("SMS");

        jLabel44.setText("EMAIL");

        jLabel46.setText("DEBUG FOR MAIL");

        jLabel47.setText("TRIGGERS");

        jLabel48.setText("PROCESS TRIGGER");

        jLabel49.setText("COMMUNICATION TRIGGER");

        jLabel50.setText("DEFAULT NUMBER");

        jLabel51.setText("DEFAULT EMAIL ADDRESS");

        jLabel54.setText("DEFAULT SENDER EMAIL");

        jLabel45.setText("MESSAGE QUEUE");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel42)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addGap(129, 129, 129)
                                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel50)
                                            .addComponent(jLabel41)
                                            .addComponent(jLabel13)
                                            .addComponent(jLabel17)
                                            .addComponent(jLabel54)
                                            .addComponent(jLabel51)
                                            .addComponent(jLabel46)
                                            .addComponent(jLabel21)
                                            .addComponent(jLabel22)
                                            .addComponent(jLabel23)
                                            .addComponent(jLabel24)
                                            .addComponent(jLabel48)
                                            .addComponent(jLabel49))
                                        .addGap(76, 76, 76)
                                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jTextField5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextField4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(textField24, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(textField23, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(textField22, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(textField21, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextField10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextField7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(textField13, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jTextField6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(textField14, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(textField15, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(textField16, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                                                .addGap(1, 1, 1)
                                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(textField17, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(139, 139, 139)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel15)
                                    .addComponent(jLabel16)
                                    .addComponent(jLabel14))))
                        .addGap(4, 4, 4))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel45)
                                    .addComponent(jLabel47)
                                    .addComponent(jLabel44))
                                .addGap(701, 701, 701))
                            .addComponent(communicationButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addComponent(textField13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(11, 11, 11))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel41)
                                .addGap(11, 11, 11)
                                .addComponent(jLabel50)
                                .addGap(5, 5, 5)))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addComponent(textField14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addComponent(textField15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(textField16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(59, 59, 59)
                        .addComponent(jLabel44)
                        .addGap(36, 36, 36)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(textField17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel46))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel51))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel54)
                            .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel42))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel45)
                .addGap(17, 17, 17)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(textField21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(textField22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(textField23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(textField24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(47, 47, 47)
                .addComponent(jLabel47)
                .addGap(34, 34, 34)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel48)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel49)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(communicationButton)
                .addContainerGap())
        );

        jTabbedPane1.addTab("COMMUNICATION", jPanel4);

        jLabel5.setText("Design File Drive");

        jLabel6.setText("Scan Drive");

        jLabel7.setText("Scan Folder");

        jLabel8.setText("Work Drive");

        documentumButton.setText("SAVE");
        documentumButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                documentumButtonActionPerformed(evt);
            }
        });

        jLabel_9.setText("Work Folder");

        jLabel_10.setText("Index Drive");

        jLabel_11.setText("Print Pool Drive");

        jLabel_12.setText("PrintPoolFolder");

        jLabel_13.setText("Bulk Printer");

        jLabel_14.setText("Card Printer");

        jLabel_15.setText("Printer WSDL");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel_9)
                    .addComponent(jLabel_10)
                    .addComponent(jLabel_14)
                    .addComponent(jLabel_13)
                    .addComponent(jLabel_12)
                    .addComponent(jLabel_11)
                    .addComponent(jLabel_15))
                .addGap(110, 110, 110)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(textField_13)
                    .addComponent(textField_12, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textField_11, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textField8, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textField7, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textField6, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textField5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 596, Short.MAX_VALUE)
                    .addComponent(textField_9, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textField_10, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textField_14)
                    .addComponent(textField_15))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(444, Short.MAX_VALUE)
                .addComponent(documentumButton)
                .addGap(324, 324, 324))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(textField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(textField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(textField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(textField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_9)
                    .addComponent(textField_9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_10)
                    .addComponent(textField_10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textField_11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel_11))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textField_12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel_12))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_13)
                    .addComponent(textField_13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_14)
                    .addComponent(textField_14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel_15)
                    .addComponent(textField_15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(documentumButton)
                .addContainerGap(237, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("DOCUMENTUM", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 829, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void contributionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contributionButtonActionPerformed
        p.put("za.co.neo.contribution.WS_URL",textField1.getText());
        p.put("za.co.neo.contribution.WS_NAMESPACE",textField2.getText());
        p.put("za.co.neo.contribution.WS_SERVICENAME",textField3.getText());
        p.put("za.co.neo.contribution.WS_PORTNAME",textField4.getText());

        p2.put("za.co.neo.contribution.WS_URL",textField1.getText());
        p2.put("za.co.neo.contribution.WS_NAMESPACE",textField2.getText());
        p2.put("za.co.neo.contribution.WS_SERVICENAME",textField3.getText());
        p2.put("za.co.neo.contribution.WS_PORTNAME",textField4.getText());

    }//GEN-LAST:event_contributionButtonActionPerformed

    private void documentumButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_documentumButtonActionPerformed
        p.put("za.co.neo.document.DESIGN_FILE_DRIVE",textField5.getText());
        p.put("za.co.neo.document.SCAN_DRIVE",textField6.getText());
        p.put("za.co.neo.document.SCAN_FOLDER",textField7.getText());
        p.put("za.co.neo.document.WORK_DRIVE",textField8.getText());
        p.put("za.co.neo.document.WORK_FOLDER",textField_9.getText());
        p.put("za.co.neo.document.INDEX_DRIVE",textField_10.getText());
        p.put("za.co.neo.document.PRINT_POOL_DRIVE",textField_11.getText());
        p.put("za.co.neo.document.PRINT_POOL_FOLDER",textField_12.getText());
        p.put("za.co.neo.document.BULK_PRINTER",textField_13.getText());
        p.put("za.co.neo.document.CARD_PRINTER",textField_14.getText());
        p.put("za.co.neo.document.PRINTER_WSDL",textField_15.getText());


        p2.put("za.co.neo.document.DESIGN_FILE_DRIVE",textField5.getText());
        p2.put("za.co.neo.document.SCAN_DRIVE",textField6.getText());
        p2.put("za.co.neo.document.SCAN_FOLDER",textField7.getText());
        p2.put("za.co.neo.document.WORK_DRIVE",textField8.getText());
        p2.put("za.co.neo.document.WORK_FOLDER",textField_9.getText());
        p2.put("za.co.neo.document.INDEX_DRIVE",textField_10.getText());
        p2.put("za.co.neo.document.PRINT_POOL_DRIVE",textField_11.getText());
        p2.put("za.co.neo.document.PRINT_POOL_FOLDER",textField_12.getText());
        p2.put("za.co.neo.document.BULK_PRINTER",textField_13.getText());
        p2.put("za.co.neo.document.CARD_PRINTER",textField_14.getText());
        p2.put("za.co.neo.document.PRINTER_WSDL",textField_15.getText());
    }//GEN-LAST:event_documentumButtonActionPerformed

    private void claimsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_claimsButtonActionPerformed
        p.put("za.co.neo.claims.WS_URL",textField9.getText());
        p.put("za.co.neo.claims.WS_NAMESPACE",textField10.getText());
        p.put("za.co.neo.claims.WS_SERVICENAME",textField11.getText());
        p.put("za.co.neo.claims.WS_PORTNAME",textField12.getText());

        p2.put("za.co.neo.claims.WS_URL",textField9.getText());
        p2.put("za.co.neo.claims.WS_NAMESPACE",textField10.getText());
        p2.put("za.co.neo.claims.WS_SERVICENAME",textField11.getText());
        p2.put("za.co.neo.claims.WS_PORTNAME",textField12.getText());
    }//GEN-LAST:event_claimsButtonActionPerformed

    private void communicationButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_communicationButtonActionPerformed
        p.put("za.co.neo.communication.SMS_URL",textField13.getText());
        p.put("za.co.neo.communication.SMS_API_KEY",textField14.getText());
        p.put("za.co.neo.communication.MMS_API_KEY",textField15.getText());
        p.put("za.co.neo.communication.PASSWORD",textField16.getText());

        p2.put("za.co.neo.communication.SMS_URL",textField13.getText());
        p2.put("za.co.neo.communication.SMS_API_KEY",textField14.getText());
        p2.put("za.co.neo.communication.MMS_API_KEY",textField15.getText());
        p2.put("za.co.neo.communication.PASSWORD",textField16.getText());
       
        p.put("za.co.neo.communication.SMTP_SERVER",textField17.getText());
        //p.put("za.co.neo.communication.EMAIL_USER",textField18.getText());
        //p.put("za.co.neo.communication.EMAIL_PASSWORD",textField19.getText());

        p2.put("za.co.neo.communication.SMTP_SERVER",textField17.getText());
        //p2.put("za.co.neo.communication.EMAIL_USER",textField18.getText());
        //p2.put("za.co.neo.communication.EMAIL_PASSWORD",textField19.getText());

        p.put("za.co.neo.communication.INITIAL_CONTEXT_FACTORY", textField21.getText());
        p.put("za.co.neo.communication.URL_PKG_PREFIX", textField22.getText());
        p.put("za.co.neo.communication.PROVIDER_URL", textField23.getText());
        p.put("za.co.neo.communication.JNDI_CONNECTION_FACTORY", textField24.getText());

        p2.put("za.co.neo.communication.INITIAL_CONTEXT_FACTORY", textField21.getText());
        p2.put("za.co.neo.communication.URL_PKG_PREFIX", textField22.getText());
        p2.put("za.co.neo.communication.PROVIDER_URL", textField23.getText());
        p2.put("za.co.neo.communication.JNDI_CONNECTION_FACTORY", textField24.getText());

        //Added-----
        p.put("za.co.neo.communication.SMS_PORT", jTextField1.getText());
        //p.put("za.co.neo.communication.EMAIL_URL", jTextField2.getText());
        p.put("za.co.neo.communication.DEBUG_MAIL", jTextField3.getText());
        p.put("za.co.neo.communication.TRIGGER_PROCESS", jTextField4.getText());
        p.put("za.co.neo.communication.TRIGGER_COMMUNICATION", jTextField5.getText());
        p.put("za.co.neo.communication.SMS_DEFAULT_NUMBER", jTextField6.getText());
        p.put("za.co.neo.communication.EMAIL_DEFAULT_ADDRESS", jTextField7.getText());
        p.put("za.co.neo.communication.EMAIL_SEND", jTextField10.getText());

        //Added-----
        p2.put("za.co.neo.communication.SMS_PORT", jTextField1.getText());
        //p2.put("za.co.neo.communication.EMAIL_URL", jTextField2.getText());
        p2.put("za.co.neo.communication.DEBUG_MAIL", jTextField3.getText());
        p2.put("za.co.neo.communication.TRIGGER_PROCESS", jTextField4.getText());
        p2.put("za.co.neo.communication.TRIGGER_COMMUNICATION", jTextField5.getText());
        p2.put("za.co.neo.communication.SMS_DEFAULT_NUMBER", jTextField6.getText());
        p2.put("za.co.neo.communication.EMAIL_DEFAULT_ADDRESS", jTextField7.getText());
        p2.put("za.co.neo.communication.EMAIL_SEND", jTextField10.getText());

}//GEN-LAST:event_communicationButtonActionPerformed

    private void reportsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportsButtonActionPerformed
        p.put("za.co.neo.reporting.URL",textField20.getText());
    }//GEN-LAST:event_reportsButtonActionPerformed

    private void pdcAuthButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdcAuthButtonActionPerformed
        p.put("za.co.neo.pdcauth.WS_URL", textField25.getText());
        p.put("za.co.neo.pdcauth.WS_NAMESPACE", textField26.getText());
        p.put("za.co.neo.pdcauth.WS_SERVICENAME", textField27.getText());
        p.put("za.co.neo.pdcauth.WS_PORTNAME", textField28.getText());

        p2.put("za.co.neo.pdcauth.WS_URL", textField25.getText());
        p2.put("za.co.neo.pdcauth.WS_NAMESPACE", textField26.getText());
        p2.put("za.co.neo.pdcauth.WS_SERVICENAME", textField27.getText());
        p2.put("za.co.neo.pdcauth.WS_PORTNAME", textField28.getText());
}//GEN-LAST:event_pdcAuthButtonActionPerformed

    private void pdcClaimButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdcClaimButtonActionPerformed
        p.put("za.co.neo.pdcclaims.WS_URL", textField29.getText());
        p.put("za.co.neo.pdcclaims.WS_NAMESPACE", textField30.getText());
        p.put("za.co.neo.pdcclaims.WS_SERVICENAME", textField31.getText());
        p.put("za.co.neo.pdcclaims.WS_PORTNAME", textField32.getText());

        p2.put("za.co.neo.pdcclaims.WS_URL", textField29.getText());
        p2.put("za.co.neo.pdcclaims.WS_NAMESPACE", textField30.getText());
        p2.put("za.co.neo.pdcclaims.WS_SERVICENAME", textField31.getText());
        p2.put("za.co.neo.pdcclaims.WS_PORTNAME", textField32.getText());
}//GEN-LAST:event_pdcClaimButtonActionPerformed

    private void pdcBiometricButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdcBiometricButtonActionPerformed
        p.put("za.co.neo.pdcbio.WS_URL", textField33.getText());
        p.put("za.co.neo.pdcbio.WS_NAMESPACE", textField34.getText());
        p.put("za.co.neo.pdcbio.WS_SERVICENAME", textField35.getText());
        p.put("za.co.neo.pdcbio.WS_PORTNAME", textField36.getText());

        p2.put("za.co.neo.pdcbio.WS_URL", textField33.getText());
        p2.put("za.co.neo.pdcbio.WS_NAMESPACE", textField34.getText());
        p2.put("za.co.neo.pdcbio.WS_SERVICENAME", textField35.getText());
        p2.put("za.co.neo.pdcbio.WS_PORTNAME", textField36.getText());
}//GEN-LAST:event_pdcBiometricButtonActionPerformed

    private void preAuthButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_preAuthButtonActionPerformed
        p.put("za.co.neo.preauth.WS_URL", textField40.getText());
        p.put("za.co.neo.preauth.WS_NAMESPACE", textField39.getText());
        p.put("za.co.neo.preauth.WS_SERVICENAME", textField38.getText());
        p.put("za.co.neo.preauth.WS_PORTNAME", textField37.getText());

        p2.put("za.co.neo.preauth.WS_URL", textField40.getText());
        p2.put("za.co.neo.preauth.WS_NAMESPACE", textField39.getText());
        p2.put("za.co.neo.preauth.WS_SERVICENAME", textField38.getText());
        p2.put("za.co.neo.preauth.WS_PORTNAME", textField37.getText());
}//GEN-LAST:event_preAuthButtonActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Config().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton claimsButton;
    private javax.swing.JButton claimsButton1;
    private javax.swing.JButton communicationButton;
    private javax.swing.JButton contributionButton;
    private javax.swing.JButton documentumButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel_10;
    private javax.swing.JLabel jLabel_11;
    private javax.swing.JLabel jLabel_12;
    private javax.swing.JLabel jLabel_13;
    private javax.swing.JLabel jLabel_14;
    private javax.swing.JLabel jLabel_15;
    private javax.swing.JLabel jLabel_9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JButton reportsButton;
    public static javax.swing.JTextField textField1;
    public static javax.swing.JTextField textField10;
    public static javax.swing.JTextField textField11;
    public static javax.swing.JTextField textField12;
    public static javax.swing.JTextField textField13;
    public static javax.swing.JTextField textField14;
    public static javax.swing.JTextField textField15;
    public static javax.swing.JTextField textField16;
    public static javax.swing.JTextField textField17;
    public static javax.swing.JTextField textField2;
    public static javax.swing.JTextField textField20;
    public static javax.swing.JTextField textField21;
    public static javax.swing.JTextField textField22;
    public static javax.swing.JTextField textField23;
    public static javax.swing.JTextField textField24;
    public static javax.swing.JTextField textField25;
    public static javax.swing.JTextField textField26;
    public static javax.swing.JTextField textField27;
    public static javax.swing.JTextField textField28;
    public static javax.swing.JTextField textField29;
    public static javax.swing.JTextField textField3;
    public static javax.swing.JTextField textField30;
    public static javax.swing.JTextField textField31;
    public static javax.swing.JTextField textField32;
    public static javax.swing.JTextField textField33;
    public static javax.swing.JTextField textField34;
    public static javax.swing.JTextField textField35;
    public static javax.swing.JTextField textField36;
    public static javax.swing.JTextField textField37;
    public static javax.swing.JTextField textField38;
    public static javax.swing.JTextField textField39;
    public static javax.swing.JTextField textField4;
    public static javax.swing.JTextField textField40;
    public static javax.swing.JTextField textField5;
    public static javax.swing.JTextField textField6;
    public static javax.swing.JTextField textField7;
    public static javax.swing.JTextField textField8;
    public static javax.swing.JTextField textField9;
    private javax.swing.JTextField textField_10;
    private javax.swing.JTextField textField_11;
    private javax.swing.JTextField textField_12;
    private javax.swing.JTextField textField_13;
    private javax.swing.JTextField textField_14;
    private javax.swing.JTextField textField_15;
    private javax.swing.JTextField textField_9;
    // End of variables declaration//GEN-END:variables

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neoconfigure;

import java.util.prefs.Preferences;

/**
 *
 * @author lesliej
 */
public class SetConfig {


    public SetConfig() {

           Preferences.userRoot().remove("za.co.neo.contribution.WS_URL");
           Preferences.userRoot().remove("za.co.neo.contribution.WS_NAMESPACE");
           Preferences.userRoot().remove("za.co.neo.contribution.WS_SERVICENAME");
           Preferences.userRoot().remove("za.co.neo.contribution.WS_PORTNAME");

           Preferences.userRoot().put("za.co.neo.contribution.WS_URL", "http://kodev01:8080/KPRepositoryNeo_CO_Test_Rules/WSKPRepositoryNeo_CO_Test_RulesService");
           Preferences.userRoot().put("za.co.neo.contribution.WS_NAMESPACE", "http://webservice/");
           Preferences.userRoot().put("za.co.neo.contribution.WS_SERVICENAME", "WSKPRepositoryNeo_CO_Test_RulesService");
           Preferences.userRoot().put("za.co.neo.contribution.WS_PORTNAME", "WSKPRepositoryNeo_CO_Test_RulesPort");

           //DOCUMENTUM
           Preferences.userRoot().remove("za.co.neo.documentum.DATABASE_DRIVER");
           Preferences.userRoot().remove("za.co.neo.documentum.DATABASE_STRING");
           Preferences.userRoot().remove("za.co.neo.documentum.DATABASE_USERNAME");
           Preferences.userRoot().remove("za.co.neo.documentum.DATABASE_PASSWORD");
           
           Preferences.userRoot().put("za.co.neo.documentum.DATABASE_DRIVER", "oracle.jdbc.driver.OracleDriver");
           Preferences.userRoot().put("za.co.neo.documentum.DATABASE_STRING", "jdbc:jtds:sqlserver://localhost:1433/SybrinDB");
           Preferences.userRoot().put("za.co.neo.documentum.DATABASE_USERNAME", "SybrinAdmin");
           Preferences.userRoot().put("za.co.neo.documentum.DATABASE_PASSWORD", "12345");

           //CLAIMS
           Preferences.userRoot().remove("za.co.neo.claims.WS_URL");
           Preferences.userRoot().remove("za.co.neo.claims.WS_NAMESPACE");
           Preferences.userRoot().remove("za.co.neo.claims.WS_SERVICENAME");
           Preferences.userRoot().remove("za.co.neo.claims.WS_PORTNAME");

           Preferences.userRoot().put("za.co.neo.claims.WS_URL", "http://kodev01:8080/KPRepositoryNeo_CA_Test_Rules/WSKPRepositoryNeo_CA_Test_RulesService?wsdl");
           Preferences.userRoot().put("za.co.neo.claims.WS_NAMESPACE", "http://webservice/");
           Preferences.userRoot().put("za.co.neo.claims.WS_SERVICENAME", "WSKPRepositoryNeo_CA_Test_RulesService");
           Preferences.userRoot().put("za.co.neo.claims.WS_PORTNAME", "WSKPRepositoryNeo_CA_Test_RulesPort");

           //COMMUNICATION
            Preferences.userRoot().remove("za.co.neo.communication.SMS_URL");
            Preferences.userRoot().remove("za.co.neo.communication.SMS_API_KEY");
            Preferences.userRoot().remove("za.co.neo.communication.MMS_API_KEY");
            Preferences.userRoot().remove("za.co.neo.communication.PASSWORD");
            Preferences.userRoot().remove("za.co.neo.communication.INITIAL_CONTEXT_FACTORY");
            Preferences.userRoot().remove("za.co.neo.communication.URL_PKG_PREFIX");
            Preferences.userRoot().remove("za.co.neo.communication.PROVIDER_URL");
            Preferences.userRoot().remove("za.co.neo.communication.JNDI_CONNECTION_FACTORY");

            //Added-----
            Preferences.userRoot().remove("za.co.neo.communication.SMS_PORT");
            Preferences.userRoot().remove("za.co.neo.communication.MAILSERVER_URL");
            Preferences.userRoot().remove("za.co.neo.communication.DEBUG_MAILSERVER");
            Preferences.userRoot().remove("za.co.neo.communication.TRIGGER_PROCESS");
            Preferences.userRoot().remove("za.co.neo.communication.TRIGGER_COMMUNICATION");
            Preferences.userRoot().remove("za.co.neo.communication.SMS_DEFAULT_NUMBER");
            Preferences.userRoot().remove("za.co.neo.communication.EMAIL_DEFAULT_ADDRESS");
            Preferences.userRoot().remove("za.co.neo.communication.EMAIL_SEND");

           Preferences.userRoot().put("za.co.neo.communication.SMS_URL", "http://services.ambientmobile.co.za/sms");
           Preferences.userRoot().put("za.co.neo.communication.SMS_API_KEY", "98E597CF-3BDD-4876-8D09-A7016D3225DC");
           Preferences.userRoot().put("za.co.neo.communication.MMS_API_KEY", "D9E47A42-9936-4301-8E61-75EEBA1FE257");
           Preferences.userRoot().put("za.co.neo.communication.PASSWORD", "4g1L1tyt3ch");
           Preferences.userRoot().put("za.co.neo.communication.INITIAL_CONTEXT_FACTORY","org.jnp.interfaces.NamingContextFactory");
           Preferences.userRoot().put("za.co.neo.communication.URL_PKG_PREFIX","org.jboss.naming:org.jnp.interfaces");
           Preferences.userRoot().put("za.co.neo.communication.PROVIDER_URL","jnp://localhost:1099");
           Preferences.userRoot().put("za.co.neo.communication.JNDI_CONNECTION_FACTORY","java:/XAConnectionFactory");

           //Added-----
           Preferences.userRoot().put("za.co.neo.communication.SMS_PORT","");
           Preferences.userRoot().put("za.co.neo.communication.MAILSERVER_URL","");
           Preferences.userRoot().put("za.co.neo.communication.DEBUG_MAILSERVER","");
           Preferences.userRoot().put("za.co.neo.communication.TRIGGER_PROCESS","");
           Preferences.userRoot().put("za.co.neo.communication.TRIGGER_COMMUNICATION","");

           //REPORTING
           Preferences.userRoot().remove("za.co.neo.reporting.URL");
           Preferences.userRoot().put("za.co.neo.reporting.URL", "http://172.16.1.118:8889/reports/rwservlet?server=neo_reports+report=");

           //PDC AUTH
           Preferences.userRoot().remove("za.co.neo.pdcAuth.WS_URL");
           Preferences.userRoot().remove("za.co.neo.pdcAuth.WS_NAMESPACE");
           Preferences.userRoot().remove("za.co.neo.pdcAuth.WS_SERVICENAME");
           Preferences.userRoot().remove("za.co.neo.pdcAuth.WS_PORTNAME");

           Preferences.userRoot().put("za.co.neo.pdcAuth.WS_URL","http://kodev01:8080/KPRepositoryNeo_CA_Test_Rules/WSKPRepositoryNeo_CA_Test_RulesService?wsdl");
           Preferences.userRoot().put("za.co.neo.pdcAuth.WS_NAMESPACE","http://webservice/");
           Preferences.userRoot().put("za.co.neo.pdcAuth.WS_SERVICENAME","WSKPRepositoryNeo_CA_Test_RulesService");
           Preferences.userRoot().put("za.co.neo.pdcAuth.WS_PORTNAME","WSKPRepositoryNeo_CA_Test_RulesPort");

           //PDC CLAIM
           Preferences.userRoot().remove("za.co.neo.pdcClaim.WS_URL");
           Preferences.userRoot().remove("za.co.neo.pdcClaim.WS_NAMESPACE");
           Preferences.userRoot().remove("za.co.neo.pdcClaim.WS_SERVICENAME");
           Preferences.userRoot().remove("za.co.neo.pdcClaim.WS_PORTNAME");

           Preferences.userRoot().put("za.co.neo.pdcClaim.WS_URL","http://kodev01:8080/KPRepositoryNeo_CA_Test_Rules/WSKPRepositoryNeo_CA_Test_RulesService?wsdl");
           Preferences.userRoot().put("za.co.neo.pdcClaim.WS_NAMESPACE","http://webservice/");
           Preferences.userRoot().put("za.co.neo.pdcClaim.WS_SERVICENAME","WSKPRepositoryNeo_CA_Test_RulesService");
           Preferences.userRoot().put("za.co.neo.pdcClaim.WS_PORTNAME","WSKPRepositoryNeo_CA_Test_RulesPort");

           //PDC BIOMETRIC
           Preferences.userRoot().remove("za.co.neo.pdcBiometric.WS_URL");
           Preferences.userRoot().remove("za.co.neo.pdcBiometric.WS_NAMESPACE");
           Preferences.userRoot().remove("za.co.neo.pdcBiometric.WS_SERVICENAME");
           Preferences.userRoot().remove("za.co.neo.pdcBiometric.WS_PORTNAME");

           Preferences.userRoot().put("za.co.neo.pdcBiometric.WS_URL","http://kodev01:8080/KPRepositoryNeo_CA_Test_Rules/WSKPRepositoryNeo_CA_Test_RulesService?wsdl");
           Preferences.userRoot().put("za.co.neo.pdcBiometric.WS_NAMESPACE","http://webservice/");
           Preferences.userRoot().put("za.co.neo.pdcBiometric.WS_SERVICENAME","WSKPRepositoryNeo_CA_Test_RulesService");
           Preferences.userRoot().put("za.co.neo.pdcBiometric.WS_PORTNAME","WSKPRepositoryNeo_CA_Test_RulesPort");

    }

    public static void main(String[] args) {
        new SetConfig();

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SearchProviderDetails.java
 *
 * Created on 2009/11/25, 10:09:04
 */
package newEntity;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.Collection;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;
import neo.manager.ProviderDetails;
import neo.manager.ProviderSearchCriteria;
import neo.product.utils.NeoWebService;
import za.co.AgilityTechnologies.DockingFramework;
import za.co.AgilityTechnologies.LeftMenu.EntityActionsFrame;

/**
 *
 * @author johanl
 */
public class SearchPracticeDetails extends javax.swing.JPanel {

    public static String practiceNumber;
    public Collection<ProviderDetails> practiceDetailsList;
    public DockingFramework df = new DockingFramework();
    /** Creates new form SearchProviderDetails */
    public SearchPracticeDetails() {
        initComponents();
        viewDetailsBtn.setVisible(false);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        titlePanel = new javax.swing.JPanel();
        titleName = new com.jidesoft.swing.StyledLabel();
        jPanel2 = new javax.swing.JPanel();
        styledLabel1 = new com.jidesoft.swing.StyledLabel();
        txt_PracticeName = new javax.swing.JTextField();
        styledLabel2 = new com.jidesoft.swing.StyledLabel();
        txt_PracticeNumber = new javax.swing.JTextField();
        findBtn = new com.jidesoft.swing.JideButton();
        tablePanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        resultTable = new com.jidesoft.grid.JideTable();
        viewDetailsBtn = new com.jidesoft.swing.JideButton();

        jPanel1.setBackground(new java.awt.Color(235, 235, 235));

        Border raisedEdge = BorderFactory.createEtchedBorder(EtchedBorder.RAISED, Color.decode("#EBEBEB"), Color.decode("#99C3FD"));
        titlePanel.setBackground(new java.awt.Color(138, 177, 230));
        titlePanel.setBorder(raisedEdge);
        titlePanel.setBackground(new Color(153,195,253));
        titlePanel.add(titleName,BorderLayout.LINE_START);
        titlePanel.add(titleName);

        titleName.setBackground(new java.awt.Color(255, 255, 255));
        titleName.setForeground(new java.awt.Color(255, 255, 255));
        titleName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleName.setText("ENQUIRIES");
        titleName.setFont(new java.awt.Font("Arial", 1, 13));
        titleName.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titlePanel.add(titleName);

        jPanel2.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel1.setText("Practice Number");

        txt_PracticeName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_PracticeNameKeyPressed(evt);
            }
        });

        styledLabel2.setText("Practice Name");

        findBtn.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        findBtn.setText("Find");
        findBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                findBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(styledLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                                .addComponent(txt_PracticeNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(59, 59, 59)
                                .addComponent(txt_PracticeName, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(172, 172, 172)
                        .addComponent(findBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(48, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_PracticeNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_PracticeName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(52, 52, 52)
                .addComponent(findBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        tablePanel.setBackground(new java.awt.Color(235, 235, 235));

        resultTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Practice Number", "Practice Name", "Network", "Number of Partners"
            }
        ));
        resultTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                resultTableMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(resultTable);
        resultTable.getColumnModel().getColumn(0).setMinWidth(150);
        resultTable.getColumnModel().getColumn(0).setPreferredWidth(150);
        resultTable.getColumnModel().getColumn(0).setMaxWidth(150);
        resultTable.getColumnModel().getColumn(2).setMinWidth(150);
        resultTable.getColumnModel().getColumn(2).setPreferredWidth(150);
        resultTable.getColumnModel().getColumn(2).setMaxWidth(150);

        viewDetailsBtn.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        viewDetailsBtn.setText("View Practice Details");
        viewDetailsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewDetailsBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout tablePanelLayout = new javax.swing.GroupLayout(tablePanel);
        tablePanel.setLayout(tablePanelLayout);
        tablePanelLayout.setHorizontalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tablePanelLayout.createSequentialGroup()
                .addContainerGap(51, Short.MAX_VALUE)
                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tablePanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 565, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tablePanelLayout.createSequentialGroup()
                        .addComponent(viewDetailsBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(244, 244, 244))))
        );
        tablePanelLayout.setVerticalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tablePanelLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(viewDetailsBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titlePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 761, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(tablePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(71, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(150, 150, 150)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(173, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(titlePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tablePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 761, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 495, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txt_PracticeNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_PracticeNameKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.searchPracticeDetails();
        }
}//GEN-LAST:event_txt_PracticeNameKeyPressed

    private void findBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_findBtnActionPerformed
        searchPracticeDetails();
}//GEN-LAST:event_findBtnActionPerformed
    public void searchPracticeDetails() {
        String practice = txt_PracticeName.getText();
        String number = txt_PracticeNumber.getText();

        ProviderSearchCriteria psc = new ProviderSearchCriteria();
        psc.setPracticeName(practice);
        psc.setRegistrationNumber(number);

        List<ProviderDetails> practiceList = NeoWebService.getNeoManagerBeanService().findAllPracticesByCriteria(psc);
        practiceDetailsList = practiceList;
        int reSize = practiceList.size();
        addTableResultRows(reSize);
        int x = 0;
        for (ProviderDetails pd : practiceList) {
            resultTable.setValueAt(pd.getPracticeNumber(), x, 0);
            resultTable.setValueAt(pd.getPracticeName(),x,1);
            resultTable.setValueAt(pd.getNetworkName(),x,2);
            
            x++;
        }
        tablePanel.setVisible(true);
        viewDetailsBtn.setVisible(false);
    }
    private void resultTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_resultTableMouseReleased
        int selectedRow = resultTable.getSelectedRow();
        System.out.println("selectedRow = " + selectedRow);
        practiceNumber = resultTable.getValueAt(selectedRow, 0).toString();
        viewDetailsBtn.setVisible(true);
}//GEN-LAST:event_resultTableMouseReleased
    public static void addTableResultRows(int reSize) {
        int reinTableRows = 0;
        reinTableRows = resultTable.getRowCount();
        if (reinTableRows != 0) {
            //removing all rows
            resultTable.setModel(new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                        "Practice Number", "Practice Name", "Network","Number of Partners"
                    }));
            //adding rows according to request
            DefaultTableModel displayTableModel = (DefaultTableModel) resultTable.getModel();
            for (int k = 0; k < reSize; k++) {
                displayTableModel.addRow(new Object[]{});
            }
        } else {
            //adding rows according to request
            DefaultTableModel displayTableModel = (DefaultTableModel) resultTable.getModel();
            for (int k = 0; k < reSize; k++) {
                displayTableModel.addRow(new Object[]{});
            }
        }
    }
    private void viewDetailsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewDetailsBtnActionPerformed

        df.ViewPracticeDetailsActions(practiceNumber);
        EntityActionsFrame.entityActionList.setSelectedIndex(1);

}//GEN-LAST:event_viewDetailsBtnActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.jidesoft.swing.JideButton findBtn;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    public static com.jidesoft.grid.JideTable resultTable;
    private com.jidesoft.swing.StyledLabel styledLabel1;
    private com.jidesoft.swing.StyledLabel styledLabel2;
    private javax.swing.JPanel tablePanel;
    private com.jidesoft.swing.StyledLabel titleName;
    private javax.swing.JPanel titlePanel;
    public static javax.swing.JTextField txt_PracticeName;
    public static javax.swing.JTextField txt_PracticeNumber;
    private com.jidesoft.swing.JideButton viewDetailsBtn;
    // End of variables declaration//GEN-END:variables
}

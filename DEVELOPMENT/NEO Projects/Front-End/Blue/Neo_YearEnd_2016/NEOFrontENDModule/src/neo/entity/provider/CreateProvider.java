/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * CreatePerson.java
 *
 * Created on 2009/05/05, 03:28:32
 */
package neo.entity.provider;

import java.awt.event.FocusEvent;
import neo.entity.person.*;
import com.jidesoft.popup.JidePopup;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.FocusListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import neo.entity.practice.CreateRolesToPractice;
import neo.entity.practice.PracticeDispensingStatusTable;
import neo.entity.search.SearchDocsFrame;
import neo.entity.search.SearchPerson;
import neo.entity.search.SearchPersonTest;

import neo.manager.ContractType;
import neo.manager.DiscountBand;

//import neo.manager.DiscountType;
import neo.manager.DispensingStatus;
import neo.manager.EntitySearchCriteria;
import neo.manager.EntityType;
import neo.manager.Gender;
import neo.manager.IdentificationType;
import neo.manager.LogicalOperandType;
import neo.manager.MaritalStatus;

//import neo.webservices.delegate.DelegateWebServices;
import neo.manager.Option;
import neo.manager.PaymentType;
import neo.manager.PersonContract;
import neo.manager.PersonDetails;
import neo.manager.Product;
import neo.manager.ReasonType;
import neo.manager.RoleType;
import neo.manager.StatusType;
import neo.manager.TargetObjectType;
import neo.product.system.AndorOrProductTable;
import neo.product.utils.NeoWebService;
import za.co.AgilityTechnologies.DockingFramework;
import za.co.AgilityTechnologies.LeftMenu.EntityActionsFrame;

import za.co.AgilityTechnologies.TableEditors.ApplyToType;
import za.co.AgilityTechnologies.TableEditors.DiscountType;
import za.co.AgilityTechnologies.TableEditors.OptionType;
import za.co.AgilityTechnologies.TableEditors.SelectProductTable;

/**
 *
 * @author Abigail 
 */
public class CreateProvider extends javax.swing.JPanel implements FocusListener {

    public static JComboBox productBox = new SelectProductTable().getCbComponent();
    private TableCellEditor productComboCell = new DefaultCellEditor(productBox);
    
    public static JComboBox optionBox = new SelectProductTable().getCbComponent();
    private TableCellEditor optionComboCell = new DefaultCellEditor(optionBox);

    public static JComboBox discTypeBox = new DisciplineTypeTable().getCbComponent();
    private TableCellEditor disciplineComboCell = new DefaultCellEditor(discTypeBox);

    public static OptionBean ob = new OptionBean();
    public static Map discTypeMap = new DisciplineTypeTable().getDiscTypeMap();

     //NEW VARIABLES TO HANDLE LOGIC OF SEARCH AND ADDING OF ROLES
    public static boolean addRoleMode = false;
    public static boolean searched = false;

    /** Creates new form CreatePerson */
    public CreateProvider() {
        productBox.addFocusListener(this);
        optionBox.addFocusListener(this);
        discTypeBox.addFocusListener(this);
        initComponents();
        //  BHFRegNoLabel.setVisible(false);
        //   BHFRegNumber.setVisible(false);
        EntityContractNumber.setVisible(false);
        flagRole.setVisible(false);
        relationshipType.setVisible(false);
        addRole.setVisible(false);
        ProviderNumber.setVisible(false);
        entityID.setVisible(false);
        ContractEntityType.setVisible(false);
        entityID.setEnabled(false);
        entityID.setOpaque(false);
        discountBandsTable.getColumnModel().getColumn(7).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });

        discountBandsTable.getColumnModel().getColumn(8).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });
        LicensingTable.getColumnModel().getColumn(3).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });

        LicensingTable.getColumnModel().getColumn(4).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });
        LicensingTable.getColumnModel().getColumn(5).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });

        DisciplineDispensingTable.getColumnModel().getColumn(5).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });
        DisciplineDispensingTable.getColumnModel().getColumn(6).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });
    }
    protected static int entityid = 0;
    protected static String initial = null;
    protected static String surname1 = null;

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titlePanel = new javax.swing.JPanel();
        titleName = new com.jidesoft.swing.StyledLabel();
        bodyPanel = new javax.swing.JPanel();
        BHFRegNoLabel = new com.jidesoft.swing.StyledLabel();
        BHFRegNumber = new javax.swing.JTextField();
        styledLabel2 = new com.jidesoft.swing.StyledLabel();
        title = new com.jidesoft.combobox.ListComboBox();
        styledLabel3 = new com.jidesoft.swing.StyledLabel();
        initials = new javax.swing.JTextField();
        styledLabel4 = new com.jidesoft.swing.StyledLabel();
        firstname = new javax.swing.JTextField();
        styledLabel5 = new com.jidesoft.swing.StyledLabel();
        secondname = new javax.swing.JTextField();
        styledLabel6 = new com.jidesoft.swing.StyledLabel();
        surname = new javax.swing.JTextField();
        styledLabel7 = new com.jidesoft.swing.StyledLabel();
        gender = new com.jidesoft.combobox.ListComboBox();
        styledLabel8 = new com.jidesoft.swing.StyledLabel();
        maritalStatus = new com.jidesoft.combobox.ListComboBox();
        styledLabel9 = new com.jidesoft.swing.StyledLabel();
        identificationType = new com.jidesoft.combobox.ListComboBox();
        styledLabel10 = new com.jidesoft.swing.StyledLabel();
        dateofBirth = new com.jidesoft.combobox.DateComboBox();
        styledLabel11 = new com.jidesoft.swing.StyledLabel();
        identificationNumber = new javax.swing.JTextField();
        styledLabel13 = new com.jidesoft.swing.StyledLabel();
        homeLanguage = new com.jidesoft.combobox.ListComboBox();
        styledLabel12 = new com.jidesoft.swing.StyledLabel();
        status = new com.jidesoft.combobox.ListComboBox();
        styledLabel17 = new com.jidesoft.swing.StyledLabel();
        styledLabel18 = new com.jidesoft.swing.StyledLabel();
        statusFromDate = new com.jidesoft.combobox.DateComboBox();
        reason = new com.jidesoft.combobox.ListComboBox();
        styledLabel21 = new com.jidesoft.swing.StyledLabel();
        titlePanel2 = new javax.swing.JPanel();
        titleName2 = new com.jidesoft.swing.StyledLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        LicensingTable = new com.jidesoft.grid.JideTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        DisciplineDispensingTable = new com.jidesoft.grid.JideTable();
        PaymentMethod = new com.jidesoft.combobox.ListComboBox();
        titlePanel1 = new javax.swing.JPanel();
        titleName1 = new com.jidesoft.swing.StyledLabel();
        titlePanel3 = new javax.swing.JPanel();
        titleName3 = new com.jidesoft.swing.StyledLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        discountBandsTable = new com.jidesoft.grid.JideTable();
        searchPerson = new com.jidesoft.swing.JideButton();
        saveProvider = new com.jidesoft.swing.JideButton();
        cancelPerson = new com.jidesoft.swing.JideButton();
        NextButton = new com.jidesoft.swing.JideButton();
        EntityContractNumber = new javax.swing.JTextField();
        flagRole = new javax.swing.JTextField();
        relationshipType = new javax.swing.JTextField();
        addRole = new com.jidesoft.swing.JideButton();
        ProviderNumber = new com.jidesoft.swing.StyledLabel();
        entityID = new javax.swing.JTextField();
        ContractEntityType = new javax.swing.JTextField();
        searchPerson1 = new com.jidesoft.swing.JideButton();

        setBackground(new java.awt.Color(235, 235, 235));

        Border raisedEdge = BorderFactory.createEtchedBorder(EtchedBorder.RAISED, Color.decode("#EBEBEB"), Color.decode("#99C3FD"));
        titlePanel.setBackground(new java.awt.Color(138, 177, 230));
        titlePanel.setBackground(new Color(153,195,253));
        titlePanel.add(titleName,BorderLayout.LINE_START);
        titlePanel.add(titleName);

        titleName.setBackground(new java.awt.Color(255, 255, 255));
        titleName.setForeground(new java.awt.Color(255, 255, 255));
        titleName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleName.setText("CREATE PROVIDER CONTRACT");
        titleName.setFont(new java.awt.Font("Arial", 1, 13));
        titleName.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titlePanel.add(titleName);

        bodyPanel.setBackground(new java.awt.Color(235, 235, 235));

        BHFRegNoLabel.setText("Registration No:");
        BHFRegNoLabel.setFont(new java.awt.Font("Arial", 0, 11));

        styledLabel2.setText("Title ");
        styledLabel2.setFont(new java.awt.Font("Arial", 0, 11));

        title.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Mr", "Miss", "Mrs", "Dr", "Prof", " " }));

        styledLabel3.setForeground(new java.awt.Color(255, 51, 51));
        styledLabel3.setText("Initials ");
        styledLabel3.setFont(new java.awt.Font("Arial", 0, 11));

        styledLabel4.setForeground(new java.awt.Color(255, 51, 51));
        styledLabel4.setText("First Name");

        styledLabel5.setText("Second Name ");

        styledLabel6.setForeground(new java.awt.Color(255, 51, 51));
        styledLabel6.setText("Surname ");

        styledLabel7.setText("Gender");

        neo.manager.Gender[] genderTypes=neo.manager.Gender.values();
        gender.setModel(new javax.swing.DefaultComboBoxModel(genderTypes)
        );

        styledLabel8.setText("Marital Status");

        MaritalStatus[] maritalTypes=MaritalStatus.values();
        maritalStatus.setModel(new javax.swing.DefaultComboBoxModel(maritalTypes));

        styledLabel9.setText("Identification Type");

        neo.manager.IdentificationType[] idTypes=neo.manager.IdentificationType.values();
        identificationType.setModel(new javax.swing.DefaultComboBoxModel(idTypes));

        styledLabel10.setForeground(new java.awt.Color(255, 51, 51));
        styledLabel10.setText("Date of Birth");

        styledLabel11.setText("Identification Number");

        styledLabel13.setText("Home Language");

        homeLanguage.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "English", "French" }));

        styledLabel12.setText("Status");

        neo.manager.StatusType[] sTypes=neo.manager.StatusType.values();
        status.setModel(new javax.swing.DefaultComboBoxModel(sTypes));

        styledLabel17.setText("Reason");

        styledLabel18.setForeground(new java.awt.Color(255, 51, 51));
        styledLabel18.setText("From Date");

        //Calendar cal = Calendar.getInstance();

        //java.util.Date adate = cal.
        //statusFromDate.setCalendar();
        statusFromDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                statusFromDateActionPerformed(evt);
            }
        });

        neo.manager.ReasonType[] rType=neo.manager.ReasonType.values();
        reason.setModel(new javax.swing.DefaultComboBoxModel(rType));

        styledLabel21.setText("Payment Method");

        titlePanel2.setBackground(new java.awt.Color(138, 177, 230));
        titlePanel.setBackground(new Color(153,195,253));
        titlePanel.add(titleName,BorderLayout.LINE_START);
        titlePanel.add(titleName);

        titleName2.setBackground(new java.awt.Color(255, 255, 255));
        titleName2.setForeground(new java.awt.Color(255, 255, 255));
        titleName2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleName2.setText("DISCIPLINE AND DISPENSING STATUSES");
        titleName2.setFont(new java.awt.Font("Arial", 1, 13));
        titleName2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titlePanel2.add(titleName2);

        LicensingTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Accreditation Group", "Licensing Type", "Licensing Number", "Registration Date", "Date Licensed From", "Date Licensed To"
            }
        ));
        jScrollPane1.setViewportView(LicensingTable);
        LicensingTable.getColumnModel().getColumn(0).setCellEditor(new ProviderAccreditationGroupTable());
        LicensingTable.getColumnModel().getColumn(1).setCellEditor(new ProviderLicenseTypeTable());
        LicensingTable.getColumnModel().getColumn(3).setCellEditor(new neo.entity.DatePickerCellTable());
        LicensingTable.getColumnModel().getColumn(4).setCellEditor(new neo.entity.DatePickerCellTable());
        LicensingTable.getColumnModel().getColumn(5).setCellEditor(new neo.entity.DatePickerCellTable());

        DisciplineDispensingTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Discipline Type Description", "Discipline Type", "Discipline Status", "Dispensing Status", "Dispensing Number", "Date Active From", "Date Active To"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, true, true, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        DisciplineDispensingTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(DisciplineDispensingTable);
        DisciplineDispensingTable.getColumnModel().getColumn(0).setCellEditor(disciplineComboCell);
        DisciplineDispensingTable.getColumnModel().getColumn(2).setCellEditor(new ProviderDisciplineStatusTable());
        DisciplineDispensingTable.getColumnModel().getColumn(3).setCellEditor(new PracticeDispensingStatusTable());
        DisciplineDispensingTable.getColumnModel().getColumn(5).setCellEditor(new neo.entity.DatePickerCellTable());
        DisciplineDispensingTable.getColumnModel().getColumn(6).setCellEditor(new neo.entity.DatePickerCellTable());

        neo.manager.PaymentType[] payTypes = neo.manager.PaymentType.values();
        PaymentMethod.setModel(new javax.swing.DefaultComboBoxModel(payTypes));
        PaymentMethod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PaymentMethodActionPerformed(evt);
            }
        });

        titlePanel1.setBackground(new java.awt.Color(138, 177, 230));
        titlePanel.setBackground(new Color(153,195,253));
        titlePanel.add(titleName,BorderLayout.LINE_START);
        titlePanel.add(titleName);

        titleName1.setBackground(new java.awt.Color(255, 255, 255));
        titleName1.setForeground(new java.awt.Color(255, 255, 255));
        titleName1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleName1.setText("LICENSING");
        titleName1.setFont(new java.awt.Font("Arial", 1, 13));
        titleName1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titlePanel1.add(titleName1);

        titlePanel3.setBackground(new java.awt.Color(138, 177, 230));
        titlePanel.setBackground(new Color(153,195,253));
        titlePanel.add(titleName,BorderLayout.LINE_START);
        titlePanel.add(titleName);

        titleName3.setBackground(new java.awt.Color(255, 255, 255));
        titleName3.setForeground(new java.awt.Color(255, 255, 255));
        titleName3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleName3.setText("DISCOUNT BANDS");
        titleName3.setFont(new java.awt.Font("Arial", 1, 13));
        titleName3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titlePanel3.add(titleName3);

        discountBandsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Apply To", "Discount Type", "%", "And/Or", "Amount", "Days From", "Days To", "Date Effective From", "Date Effective To", "Product Name", "Option Name"
            }
        ));
        jScrollPane3.setViewportView(discountBandsTable);
        discountBandsTable.getColumnModel().getColumn(0).setMinWidth(90);
        discountBandsTable.getColumnModel().getColumn(0).setPreferredWidth(90);
        discountBandsTable.getColumnModel().getColumn(0).setMaxWidth(90);
        discountBandsTable.getColumnModel().getColumn(0).setCellEditor(new ApplyToType());
        discountBandsTable.getColumnModel().getColumn(1).setMinWidth(90);
        discountBandsTable.getColumnModel().getColumn(1).setPreferredWidth(90);
        discountBandsTable.getColumnModel().getColumn(1).setMaxWidth(90);
        discountBandsTable.getColumnModel().getColumn(1).setCellEditor(new DiscountType());
        discountBandsTable.getColumnModel().getColumn(2).setMinWidth(40);
        discountBandsTable.getColumnModel().getColumn(2).setPreferredWidth(40);
        discountBandsTable.getColumnModel().getColumn(2).setMaxWidth(40);
        discountBandsTable.getColumnModel().getColumn(3).setMinWidth(70);
        discountBandsTable.getColumnModel().getColumn(3).setPreferredWidth(70);
        discountBandsTable.getColumnModel().getColumn(3).setMaxWidth(70);
        discountBandsTable.getColumnModel().getColumn(3).setCellEditor(new AndorOrProductTable());
        discountBandsTable.getColumnModel().getColumn(4).setMinWidth(70);
        discountBandsTable.getColumnModel().getColumn(4).setPreferredWidth(70);
        discountBandsTable.getColumnModel().getColumn(4).setMaxWidth(70);
        discountBandsTable.getColumnModel().getColumn(5).setMinWidth(70);
        discountBandsTable.getColumnModel().getColumn(5).setPreferredWidth(70);
        discountBandsTable.getColumnModel().getColumn(5).setMaxWidth(70);
        discountBandsTable.getColumnModel().getColumn(6).setMinWidth(70);
        discountBandsTable.getColumnModel().getColumn(6).setPreferredWidth(70);
        discountBandsTable.getColumnModel().getColumn(6).setMaxWidth(70);
        discountBandsTable.getColumnModel().getColumn(7).setCellEditor(new neo.entity.DatePickerCellTable());
        discountBandsTable.getColumnModel().getColumn(8).setCellEditor(new neo.entity.DatePickerCellTable());
        discountBandsTable.getColumnModel().getColumn(9).setCellEditor(productComboCell);
        discountBandsTable.getColumnModel().getColumn(10).setCellEditor(optionComboCell);

        searchPerson.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        searchPerson.setText("SEARCH");
        searchPerson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchPersonActionPerformed(evt);
            }
        });

        saveProvider.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        saveProvider.setText("SAVE");
        saveProvider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveProviderActionPerformed(evt);
            }
        });

        cancelPerson.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        cancelPerson.setText("CANCEL");
        cancelPerson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelPersonActionPerformed(evt);
            }
        });

        NextButton.setBorder(null);
        NextButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/arrow2_right_green.png"))); // NOI18N
        NextButton.setToolTipText("Save and Next");
        NextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NextButtonActionPerformed(evt);
            }
        });

        addRole.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        addRole.setText("ADD ROLE");
        addRole.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addRoleActionPerformed(evt);
            }
        });

        ProviderNumber.setText("Provider Number:");
        ProviderNumber.setFont(new java.awt.Font("Arial", 0, 11));

        entityID.setEnabled(false);
        entityID.setOpaque(false);

        searchPerson1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        searchPerson1.setText("FIND DOCS");
        searchPerson1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchDocsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout bodyPanelLayout = new javax.swing.GroupLayout(bodyPanel);
        bodyPanel.setLayout(bodyPanelLayout);
        bodyPanelLayout.setHorizontalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addGap(83, 83, 83)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                    .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(styledLabel18, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(styledLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bodyPanelLayout.createSequentialGroup()
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(styledLabel21, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                            .addComponent(BHFRegNoLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                            .addComponent(styledLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                            .addComponent(styledLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE))
                        .addGap(15, 15, 15)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BHFRegNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                    .addComponent(surname, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                    .addComponent(maritalStatus, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                    .addComponent(dateofBirth, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                    .addComponent(homeLanguage, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                    .addComponent(title, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                    .addComponent(firstname, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                    .addComponent(status, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                    .addComponent(statusFromDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                    .addComponent(PaymentMethod, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE))
                .addGap(135, 135, 135)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(styledLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(bodyPanelLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(styledLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(ProviderNumber, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(styledLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(entityID)
                    .addComponent(reason, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(identificationType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(identificationNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
                    .addComponent(gender, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(secondname, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
                    .addComponent(initials, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE))
                .addGap(229, 229, 229))
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(searchPerson, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(saveProvider, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(addRole, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cancelPerson, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(searchPerson1, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 303, Short.MAX_VALUE)
                .addComponent(NextButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(90, 90, 90))
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titlePanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 935, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titlePanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 935, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bodyPanelLayout.createSequentialGroup()
                .addContainerGap(550, Short.MAX_VALUE)
                .addComponent(ContractEntityType, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(relationshipType, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(flagRole, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(EntityContractNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(109, 109, 109))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 935, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titlePanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 935, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 935, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 935, Short.MAX_VALUE)
                .addContainerGap())
        );
        bodyPanelLayout.setVerticalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BHFRegNoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BHFRegNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ProviderNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(entityID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(title, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(initials, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(firstname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(secondname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(surname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(gender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(maritalStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(identificationType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bodyPanelLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(styledLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(identificationNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(bodyPanelLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dateofBirth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(homeLanguage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(reason, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(status, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusFromDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PaymentMethod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(EntityContractNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(flagRole, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(relationshipType, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ContractEntityType, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(titlePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(titlePanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(titlePanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(searchPerson, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(saveProvider, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(addRole, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cancelPerson, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(searchPerson1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(NextButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(57, 57, 57))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titlePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 965, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(42, Short.MAX_VALUE)
                .addComponent(titlePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void searchPersonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchPersonActionPerformed


        EntitySearchCriteria personSearch = new EntitySearchCriteria();

        // String firstname, Surname, Initial, = "";
        personSearch.setEntityType(EntityType.PERSON);

        personSearch.setSurname(surname.getText());
        personSearch.setName(firstname.getText());
        personSearch.setBhfRegistrationNumber(BHFRegNumber.getText());

        personSearch.setContractType(neo.manager.ContractType.PROVIDER);


        List<PersonDetails> result = NeoWebService.getNeoManagerBeanService().findPersonsWithSearchCriteria(personSearch);


        System.out.println("Count of Entity Details" + result.size());
        int rowCount = result.size();

        if (rowCount == 0) {
            JOptionPane.showMessageDialog(null, "No Record found", "Information", JOptionPane.INFORMATION_MESSAGE);
        } else {
            final JidePopup popup = new JidePopup();
            //JFrame frame = new JFrame("Search Results");
            SearchPersonTest frame = new SearchPersonTest();
            frame.add(new SearchPerson());
            // frame.setSize(500, 500);
            popup.setMovable(true);

            String colName[] = {"Entity Type", "Entity ID", "First Name", "Surname", "Date of Birth"};
            DefaultTableModel model = new DefaultTableModel(rowCount, colName.length);
            model.setColumnIdentifiers(colName);
            frame.resultTable.setModel(model);
            SearchPersonTest.ContractEntityNumber.setVisible(false);
            SearchPersonTest.FlagSearchRole.setVisible(false);
            SearchPersonTest.contractRelationship.setVisible(false);

            if (flagRole.getText().equals("Y")) {
                frame.FlagSearchRole.setText("Y");
                frame.ContractEntityNumber.setText(EntityContractNumber.getText());
                frame.contractRelationship.setText(relationshipType.getText());
                frame.contractEntityType.setText(ContractEntityType.getText());

            }
            int i = 0;
            for (Iterator it = result.iterator(); it.hasNext();) {
                PersonDetails nP = (PersonDetails) it.next();
                frame.resultTable.setValueAt("PROVIDER", i, 0);
                frame.resultTable.setValueAt(nP.getEntityId(), i, 1);
                frame.resultTable.setValueAt(nP.getName(), i, 2);
                frame.resultTable.setValueAt(nP.getSurname(), i, 3);
                frame.resultTable.setValueAt(nP.getDateOfBirth(), i, 4);
                i++;




            }

            frame.setVisible(true);
            popup.showPopup();


}//GEN-LAST:event_searchPersonActionPerformed
    }
    private void NextButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NextButtonActionPerformed

        //checks if BHFNumber has a value
        if (!(BHFRegNumber.getText().isEmpty() || BHFRegNumber == null || BHFRegNumber.equals(""))) {

            if (EntityActionsFrame.GLOBALPersonContract.getEntityDetails() == null) {

                entityid = NeoWebService.getNeoManagerBeanService().saveEntity(EntityType.PERSON);

                //searches the db for a instance of the BHFNumber
                EntitySearchCriteria personSearch = new EntitySearchCriteria();

                // String firstname, Surname, Initial, = "";
                personSearch.setEntityType(EntityType.PERSON);

                personSearch.setSurname(surname.getText());
                personSearch.setName(firstname.getText());
                personSearch.setBhfRegistrationNumber(BHFRegNumber.getText());

                java.util.List<PersonDetails> result = NeoWebService.getNeoManagerBeanService().findPersonsWithSearchCriteria(personSearch);

                int rowCount = result.size();
                //if a instance of the BHFNumber was not found,save new else update execisting
                if (rowCount == 0) {

                    if (saveEntity()!=0) {

                        JOptionPane.showMessageDialog(null, "Provider successfully created", "Information", JOptionPane.INFORMATION_MESSAGE);
                        EntityActionsFrame.entityActionList.setSelectedIndex(1);
                        String entid = new Integer(entityid).toString();
                        CreatePersonAddressDetails.personAddressDisplayFields();
                        System.out.println(entid);

                    }

                } else {

                    String entityNo = entityID.getText();
                    int entitycontactNo = Integer.parseInt(entityNo);
                    //int entitycontactNo =entityid ;
                    PersonContract ec = NeoWebService.getNeoManagerBeanService().getPersonContract(entitycontactNo);
                    if (ec != null) {
                        if (updateEntity(entitycontactNo)!=0) {
                            JOptionPane.showMessageDialog(null, "Provider successfully updated", "Information", JOptionPane.INFORMATION_MESSAGE);
                            EntityActionsFrame.entityActionList.setSelectedIndex(1);
                            //String entid = new Integer(entityNo).toString();
                            String entid = entityNo.toString();
                            CreatePersonAddressDetails.personAddressDisplayFields();
                            System.out.println("Entity ID on update = " + entid);
                        } else {
                            JOptionPane.showMessageDialog(null, "Error in updating Entity", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    // EntityActionsFrame.CreatePersonsDetails.setSelectedIndex(1);
                    }
                }
            } else {
                EntityActionsFrame.entityActionList.setSelectedIndex(1);
                CreatePersonAddressDetails.personAddressDisplayFields();

            }

        }
}//GEN-LAST:event_NextButtonActionPerformed
    public static void persondisplayFields(int entityContactNo) {

        /*if (EntityActionsFrame.GLOBALPersonContract.getEntityDetails() == null) {
        PersonContract econtract = NeoWebService.getNeoManagerBeanService().getPersonContract(entityContactNo);
        EntityActionsFrame.GLOBALPersonContract = econtract;
        }

        PersonDetails edeatils = EntityActionsFrame.GLOBALPersonContract.getEntityDetails();
        Integer contractId = edeatils.getEntityId();
        System.out.println(contractId);
        //BHFRegNumber.setText(new Integer(entityContactNo).toString());

        BHFRegNumber.setText(edeatils.getBhfRegistrationNumber());
        initials.setText(edeatils.getInitials());
        System.out.println(edeatils.getInitials());
        title.setSelectedItem(edeatils.getTitle());
        firstname.setText(edeatils.getName());
        secondname.setText(edeatils.getSecondName());
        surname.setText(edeatils.getSurname());
        gender.setSelectedItem(edeatils.getGender());
        maritalStatus.setSelectedItem(edeatils.getMaritalStatus());
        identificationType.setSelectedItem(edeatils.getIdentificationType());
        java.util.Date adate = (java.util.Date) edeatils.getDateOfBirth().toGregorianCalendar().getTime();
        dateofBirth.setDate(adate);
        homeLanguage.setSelectedItem(edeatils.getHomeLanguage());
        status.setSelectedItem(edeatils.getStatus());
        reason.setSelectedItem(edeatils.getReason());
        identificationNumber.setText(edeatils.getIDNumber());
        statusFromDate.setDate((java.util.Date) edeatils.getFromDate().toGregorianCalendar().getTime());
        PaymentMethod.setSelectedItem(edeatils.getPaymentType());
        BHFRegNumber.setText(edeatils.getBhfRegistrationNumber());

        int discountRow = 0;
        int licenseRow = 0;
        int dispensingRow = 0;
        List<DiscountBand> discList = edeatils.getDiscoundBands();
        List<ProviderLicense> providerLicenses = edeatils.getProviderLicenses();
        List<ProviderDispensing> providerDispens = edeatils.getProviderDispensing();
        System.out.println("Provider License Detail Size = " + providerLicenses.size());
        System.out.println("DiscountBand Detail Size = " + discList.size());
        try {
        for (DiscountBand discBandDisplay : discList) {

        discountBandsTable.setValueAt((neo.manager.ApplyToType) discBandDisplay.getApplyTo(), discountRow, 0);
        discountBandsTable.setValueAt((neo.manager.DiscountType) discBandDisplay.getDiscountType(), discountRow, 1);
        discountBandsTable.setValueAt(discBandDisplay.getPercentage(), discountRow, 2);
        discountBandsTable.setValueAt((LogicalOperandType) discBandDisplay.getLogicalOperand(), discountRow, 3);
        discountBandsTable.setValueAt(discBandDisplay.getAmount(), discountRow, 4);
        discountBandsTable.setValueAt(discBandDisplay.getDaysCountFrom(), discountRow, 5);
        discountBandsTable.setValueAt(discBandDisplay.getDaysCountTo(), discountRow, 6);
        discountBandsTable.setValueAt(discBandDisplay.getFromDate().toGregorianCalendar().getTime(), discountRow, 7);
        discountBandsTable.setValueAt(discBandDisplay.getToDate().toGregorianCalendar().getTime(), discountRow, 8);

        //String prodName = discBandDisplay.getProduct().toString();
        String prodName = discBandDisplay.getProduct().getProductName();
        //Product prodName = NeoWebService.getNeoManagerBeanService().findProductWithID(prodId);
        //Option optDesc = (Option) NeoWebService.getNeoManagerBeanService().findOptionsForProduct(prodId);

        productBox.removeAllItems();
        optionBox.removeAllItems();
        String optName = ob.getOptionName();
        System.out.println("Option Name on Display =" + optName);
        discountBandsTable.setValueAt(prodName, discountRow, 9);
        discountBandsTable.setValueAt(optName, discountRow, 10);

        discountRow++;
        }

        for (ProviderLicense providerLicens : providerLicenses) {
        LicensingTable.setValueAt(providerLicens.getAccredationGroup(), licenseRow, 0);

        ProviderLicenseType ltype = providerLicens.getLicenseType();
        LicensingTable.setValueAt(ltype.value(), licenseRow, 1);
        LicensingTable.setValueAt(providerLicens.getLicenseNumber(), licenseRow, 2);

        Date registrationDate = providerLicens.getRegistrationDate().toGregorianCalendar().getTime();

        Date dateFrom1 = providerLicens.getDateFrom().toGregorianCalendar().getTime();

        Date dateto1 = providerLicens.getDateTo().toGregorianCalendar().getTime();

        System.out.println("dateto1  ---------" + dateto1);
        System.out.println("datefrom1  ---------" + dateFrom1);

        LicensingTable.setValueAt(registrationDate, licenseRow, 3);
        LicensingTable.setValueAt(dateFrom1, licenseRow, 4);
        LicensingTable.setValueAt(dateto1, licenseRow, 5);

        licenseRow++;

        }
        for (ProviderDispensing provDispens : providerDispens) {

        DisciplineDispensingTable.setValueAt(provDispens.getDisciplineDescription(), dispensingRow, 0);
        DisciplineDispensingTable.setValueAt(provDispens.getDisciplineType(), dispensingRow, 1);
        DisciplineDispensingTable.setValueAt(provDispens.getDisciplineStatus(), dispensingRow, 2);
        DisciplineDispensingTable.setValueAt(provDispens.getDispensingStatus(), dispensingRow, 3);
        DisciplineDispensingTable.setValueAt(provDispens.getDispensingNumber(), dispensingRow, 4);
        DisciplineDispensingTable.setValueAt(provDispens.getDispenseFromDate().toGregorianCalendar().getTime(), dispensingRow, 5);
        DisciplineDispensingTable.setValueAt(provDispens.getDispenseToDate().toGregorianCalendar().getTime(), dispensingRow, 6);

        dispensingRow++;
        }

        } catch (Exception e) {
        e.printStackTrace();
        System.out.println("Error message: " + e.getMessage());
        }*/
    }
    private void cancelPersonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelPersonActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_cancelPersonActionPerformed

    private void saveProviderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveProviderActionPerformed

        //checks if BHFNumber has a value
        if (!(BHFRegNumber.getText().isEmpty() || BHFRegNumber == null || BHFRegNumber.equals(""))) {

            entityid = NeoWebService.getNeoManagerBeanService().saveEntity(EntityType.PERSON);

            //searches the db for a instance of the BHFNumber
            EntitySearchCriteria personSearch = new EntitySearchCriteria();
            // String firstname, Surname, Initial, = "";
            personSearch.setEntityType(EntityType.PERSON);

            personSearch.setSurname(surname.getText());
            personSearch.setName(firstname.getText());
            personSearch.setBhfRegistrationNumber(BHFRegNumber.getText());

            personSearch.setContractType(neo.manager.ContractType.PROVIDER);
            personSearch.setEntityType(EntityType.PERSON);
            java.util.List<PersonDetails> result = NeoWebService.getNeoManagerBeanService().findPersonsWithSearchCriteria(personSearch);

            int rowCount = result.size();
            //if a instance of the BHFNumber was not found,save new else update execisting
            if (rowCount == 0) {

                if (saveEntity()!=0) {

                    JOptionPane.showMessageDialog(null, "Provider successfully created", "Information", JOptionPane.INFORMATION_MESSAGE);
                    EntityActionsFrame.entityActionList.setSelectedIndex(1);
                    String entid = new Integer(entityid).toString();
                    CreatePersonAddressDetails.personAddressDisplayFields();
                    System.out.println("on save entityID =" + entid);
                }

            } else {

                String entityNo = entityID.getText();
                int entitycontactNo = Integer.parseInt(entityNo);
                PersonContract ec = NeoWebService.getNeoManagerBeanService().getPersonContract(entitycontactNo);
                if (ec != null) {
                    if (updateEntity(entitycontactNo)!=0) {
                        JOptionPane.showMessageDialog(null, "Provider successfully updated", "Information", JOptionPane.INFORMATION_MESSAGE);
                        EntityActionsFrame.entityActionList.setSelectedIndex(1);
                        String entid = new Integer(entityid).toString();
                        CreatePersonAddressDetails.personAddressDisplayFields();
                        System.out.println(entid);
                    } else {
                        JOptionPane.showMessageDialog(null, "Error in updating Entity", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                //EntityActionsFrame.CreatePersonsDetails.setSelectedIndex(1);
                }
            }
        }
}//GEN-LAST:event_saveProviderActionPerformed
    public int saveEntity() {

        /*entityid = NeoWebService.getNeoManagerBeanService().saveEntity(EntityType.PERSON);
        System.out.println(entityid + " id type");
        String title1 = (String) title.getSelectedItem();
        initial = initials.getText();
        String fname = firstname.getText();
        String sname = secondname.getText();
        surname1 = surname.getText();
        Gender gender1 = (Gender) gender.getSelectedItem();
        MaritalStatus mstatus = (MaritalStatus) maritalStatus.getSelectedItem();
        IdentificationType idtype = (IdentificationType) identificationType.getSelectedItem();
        String hlanguage = (String) homeLanguage.getSelectedItem();
        Date date = dateofBirth.getDate();


        //  PaymentType payMethod = (PaymentType) PaymentMethod.getSelectedItem();
        System.out.println("Save date: " + date);

        PersonDetails ed = new PersonDetails();

        ed.setTitle(title1);
        ed.setInitials(initial);
        ed.setName(fname);
        ed.setSecondName(sname);
        ed.setSurname(surname1);
        ed.setGender(gender1);
        ed.setMaritalStatus(mstatus);
        ed.setIdentificationType(idtype);
        ed.setHomeLanguage(hlanguage);

        ed.setIDNumber(identificationNumber.getText());
        ed.setStatus((StatusType) status.getSelectedItem());
        ed.setReason((ReasonType) reason.getSelectedItem());
        System.out.println("Date value is:  " + statusFromDate.getDate());
        ed.setFromDate(convert(statusFromDate.getDate()));

        ed.setDateOfBirth(convert(date));
        ed.setPaymentType((PaymentType) PaymentMethod.getSelectedItem());
        ed.setBhfRegistrationNumber(BHFRegNumber.getText());
        entityID.setText(new Integer(entityid).toString());

        //List<License> dispensingDetails = ed.getLicenses();


        int selectedDBRows = discountBandsTable.getModel().getRowCount();
        //discountBandsTable.getCellEditor().getCellEditorValue().toString();
        System.out.println("discountBandsTable row count = " + selectedDBRows);
        int selectedDescDespRows = DisciplineDispensingTable.getModel().getRowCount();
        int selectedLicenceRows = LicensingTable.getModel().getRowCount();

        List<ProviderLicense> providerLicenses = ed.getProviderLicenses();

        for (int i = 0; i < selectedLicenceRows; i++) {
        try {
        if (LicensingTable.getValueAt(i, 0) != null) {
        System.out.println(i + "-----------------i");

        ProviderLicense providerLicense = new ProviderLicense();


        providerLicense.setAccredationGroup((String) LicensingTable.getValueAt(i, 0));
        //String licenseType=(String)LicensingTable.getValueAt(i, 1);s
        providerLicense.setLicenseType((ProviderLicenseType) LicensingTable.getValueAt(i, 1));
        providerLicense.setLicenseNumber((String) LicensingTable.getValueAt(i, 2));

        String registrationDateStr = (String) LicensingTable.getValueAt(i, 3);
        DateFormat dformat = new SimpleDateFormat("yyyy/MM/dd");
        Date registrationDate = dformat.parse(registrationDateStr);
        System.out.println(registrationDate + "------------fromdate");
        providerLicense.setRegistrationDate(convert(registrationDate));

        String fromdatestr = (String) LicensingTable.getValueAt(i, 4);
        Date datefrom = dformat.parse(fromdatestr);
        System.out.println(datefrom + "------------fromdate");
        providerLicense.setDateFrom(convert(datefrom));

        String todatestr = (String) LicensingTable.getValueAt(i, 5);
        Date todate = dformat.parse(todatestr);
        System.out.println(todate + "------------todate");
        providerLicense.setDateTo(convert(todate));

        providerLicenses.add(providerLicense);
        } else {
        break;
        }
        System.out.println("providerLicenses count: " + providerLicenses.size());
        } catch (Exception e) {
        e.printStackTrace();
        System.out.println("Error message: " + e.getMessage());
        }
        }

        System.out.println("before Discount list");
        List<DiscountBand> discList = ed.getDiscoundBands();
        DiscountBand discBand = new DiscountBand();
        System.out.println("DiscountBand List Returns " + discList.toString());
        for (int i = 0; i < selectedDBRows; i++) {
        try {
        System.out.println("looping through discBand List");
        if (discountBandsTable.getValueAt(i, 0) != null && !(discountBandsTable.getValueAt(i, 0).equals(""))) {
        System.out.println("inside if for discBand List");
        //String licenseType = (String)LicensingTable.getValueAt(i, 0);

        neo.manager.ApplyToType applyTo = (neo.manager.ApplyToType) discountBandsTable.getValueAt(i, 0);
        neo.manager.DiscountType discType = (neo.manager.DiscountType) discountBandsTable.getValueAt(i, 1);
        double perc = Double.parseDouble(discountBandsTable.getValueAt(i, 2).toString());
        LogicalOperandType logic = (LogicalOperandType) discountBandsTable.getValueAt(i, 3);
        double amount = Double.parseDouble(discountBandsTable.getValueAt(i, 4).toString());
        int daysCountFrom = Integer.parseInt(discountBandsTable.getValueAt(i, 5).toString());
        int daysCountTo = Integer.parseInt(discountBandsTable.getValueAt(i, 6).toString());
        String effectiveFromDate = discountBandsTable.getValueAt(i, 7).toString();
        String effectiveFromTo = discountBandsTable.getValueAt(i, 8).toString();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date effectFrom = dateFormat.parse(effectiveFromDate);
        Date effectTo = dateFormat.parse(effectiveFromTo);

        String prodName = discountBandsTable.getValueAt(i, 9).toString();
        String optName = discountBandsTable.getValueAt(i, 10).toString();

        ob.setOptionName(optName);
        System.out.println("Option Name on Save =" + optName);
        Product prod = NeoWebService.getNeoManagerBeanService().findProductWithDesc(prodName);

        discBand.setApplyTo(applyTo);
        discBand.setDiscountType(discType);
        discBand.setPercentage(perc);
        discBand.setLogicalOperand(logic);
        discBand.setAmount(amount);
        discBand.setDaysCountFrom(daysCountFrom);
        discBand.setDaysCountTo(daysCountTo);
        discBand.setFromDate(convert(effectFrom));
        discBand.setToDate(convert(effectTo));
        discBand.setProduct(prod);

        discList.add(discBand);

        } else {
        break;
        }
        } catch (Exception e) {
        e.printStackTrace();
        System.out.println("Error message: " + e.getMessage());

        }
        }

        System.out.println("before Discount list");
        List<ProviderDispensing> dispensingList = ed.getProviderDispensing();
        ProviderDispensing provDesp = new ProviderDispensing();
        System.out.println("DiscountBand List Returns " + discList.toString());
        for (int i = 0; i < selectedDescDespRows; i++) {
        try {
        System.out.println("looping through discBand List");
        if (DisciplineDispensingTable.getValueAt(i, 0) != null && !(DisciplineDispensingTable.getValueAt(i, 0).equals(""))) {
        System.out.println("inside if for discBand List");
        //String licenseType = (String)LicensingTable.getValueAt(i, 0);

        provDesp.setDisciplineDescription(DisciplineDispensingTable.getValueAt(i, 0).toString());
        provDesp.setDisciplineType(DisciplineDispensingTable.getValueAt(i, 1).toString());
        provDesp.setDisciplineStatus(DisciplineDispensingTable.getValueAt(i, 2).toString());

        DispensingStatus provDispStatus = (DispensingStatus)DisciplineDispensingTable.getValueAt(i, 3);
        provDesp.setDispensingStatus(provDispStatus);

        provDesp.setDispensingNumber(DisciplineDispensingTable.getValueAt(i, 4).toString());

        String dispenseFromDate = DisciplineDispensingTable.getValueAt(i, 5).toString();
        String dispenseToDate = DisciplineDispensingTable.getValueAt(i, 6).toString();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date effectFrom = dateFormat.parse(dispenseFromDate);
        Date effectTo = dateFormat.parse(dispenseToDate);

        provDesp.setDispenseFromDate(convert(effectFrom));
        provDesp.setDispenseToDate(convert(effectTo));

        dispensingList.add(provDesp);

        } else {
        break;
        }
        } catch (Exception e) {
        e.printStackTrace();
        System.out.println("Error message: " + e.getMessage());

        }
        }

        //  System.out.println("dispensingDetails count: " + dispensingDetails.size());

        ed.setContractType(ContractType.PROVIDER);

        int result = NeoWebService.getNeoManagerBeanService().savePersonDetails(entityid, ed);

        if (result!=0) {
        EntityActionsFrame.GLOBALPersonContract.setEntityId(entityid);
        EntityActionsFrame.GLOBALPersonContract.setEntityDetails(ed);
        EntityActionsFrame.GLOBALPersonContract.getEntityDetails().setEntityId(entityid);
        EntityActionsFrame.GLOBALPersonContract.getEntityDetails().setEntityDetailsID(result);
        }*/
        return 0;
    }

    public int updateEntity(int entitycontactNo) {


        /*entityid = entitycontactNo;
        System.out.println(entityid + " id type");
        String title1 = (String) title.getSelectedItem();
        initial = initials.getText();
        String fname = firstname.getText();
        String sname = secondname.getText();
        surname1 = surname.getText();
        Gender gender1 = (Gender) gender.getSelectedItem();
        MaritalStatus mstatus = (MaritalStatus) maritalStatus.getSelectedItem();
        IdentificationType idtype = (IdentificationType) identificationType.getSelectedItem();
        String hlanguage = (String) homeLanguage.getSelectedItem();

        Date date = dateofBirth.getDate();
        System.out.println("Save date: " + date);

        PersonDetails ed = new PersonDetails();

        ed.setTitle(title1);
        ed.setInitials(initial);
        ed.setName(fname);
        ed.setSecondName(sname);
        ed.setSurname(surname1);
        ed.setGender(gender1);
        ed.setMaritalStatus(mstatus);
        ed.setIdentificationType(idtype);
        ed.setHomeLanguage(hlanguage);

        ed.setIDNumber(identificationNumber.getText());
        ed.setStatus((StatusType) status.getSelectedItem());
        ed.setReason((ReasonType) reason.getSelectedItem());

        ed.setFromDate(convert(statusFromDate.getDate()));
        ed.setBhfRegistrationNumber(BHFRegNumber.getText());
        ed.setPaymentType((PaymentType) PaymentMethod.getSelectedItem());
        ed.setDateOfBirth(convert(date));

        /*List<License> listLicence = ed.getLicenses();

        int selectedRows = LicensingTable.getRowCount();

        //int rows=tmodel.getRowCount();

        for (int i = 0; i < selectedRows; i++) {
        try {
        System.out.println(i + "-----------------i");
        License conprePreference = new License();
        conprePreference.setLicenseName((String) LicensingTable.getValueAt(i, 0));
        conprePreference.setLicenseType((LicenseType) LicensingTable.getValueAt(i, 1));
        conprePreference.setLicenseNumber((String) LicensingTable.getValueAt(i, 2));


        String fromdatestr = (String) LicensingTable.getValueAt(i, 3);
        DateFormat dformat = new SimpleDateFormat("yyyy/MM/dd");
        Date datefrom = dformat.parse(fromdatestr);
        System.out.println(datefrom + "------------fromdate");
        conprePreference.setDateFrom(convert(datefrom));

        String todatestr = (String) LicensingTable.getValueAt(i, 4);
        DateFormat dformat1 = new SimpleDateFormat("yyyy/MM/dd");
        Date todate = dformat1.parse(todatestr);
        System.out.println(todate + "------------todate");
        conprePreference.setDateTo(convert(todate));

        listLicence.add(conprePreference);
        } catch (Exception e) {
        e.printStackTrace();
        System.out.println("Error message: " + e.getMessage());

        }
        }
        ed.setContractType(ContractType.PROVIDER);
        int result = NeoWebService.getNeoManagerBeanService().savePersonDetails(entityid, ed);

        if (result != 0) {
            EntityActionsFrame.GLOBALPersonContract.setEntityDetails(ed);
            EntityActionsFrame.GLOBALPersonContract.getEntityDetails().setEntityId(entityid);
            EntityActionsFrame.GLOBALPersonContract.getEntityDetails().setEntityDetailsID(result);
        }

        return result;
        */
        return 0;
    }

    private void PaymentMethodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PaymentMethodActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_PaymentMethodActionPerformed

    private void statusFromDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_statusFromDateActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_statusFromDateActionPerformed

    private void addRoleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addRoleActionPerformed



        if (saveEntity()!=0) {

            System.out.println("relationshipType: " + relationshipType.getText().toString());
            System.out.println("EntityContractNumber.getText().toString()" + EntityContractNumber.getText().toString());
            System.out.println("RoleType.fromValue(relationshipType.getText()" + RoleType.fromValue(relationshipType.getText().toString()));
            System.out.println("TargetObjectType.ENTITY" + TargetObjectType.ENTITY);

            int targetId = Integer.parseInt(entityID.getText().toString());

            System.out.println("targetId" + targetId);
            boolean addRoleresult =
                    NeoWebService.getNeoManagerBeanService().addRoles(Integer.parseInt(EntityContractNumber.getText().toString()), targetId, RoleType.fromValue(relationshipType.getText().toString()), TargetObjectType.ENTITY);
            if (addRoleresult) {
                JOptionPane.showMessageDialog(null, "Relationship  successfully created", "Information", JOptionPane.INFORMATION_MESSAGE);
            } else {

                JOptionPane.showMessageDialog(null, "Unable to create relationship", "Information", JOptionPane.INFORMATION_MESSAGE);
            }
            if (relationshipType.getText().equals("PROVIDER")) {
                DockingFramework.EntityCreateRolesToProviderActions();
                CreateRolesToProvider.personRolesDisplayFields();

            }else if(relationshipType.getText().equals("PRACTICE")) {
                DockingFramework.EntityCreateRolesToPracticeActions();
                CreateRolesToPractice.personRolesDisplayFields();
            }

        }
}//GEN-LAST:event_addRoleActionPerformed

    private void searchDocsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchDocsActionPerformed
        SearchDocsFrame frame = new SearchDocsFrame(EntityActionsFrame.GLOBALPersonContract.getEntityDetails().getEntityId());
}//GEN-LAST:event_searchDocsActionPerformed

    protected XMLGregorianCalendar convert(Date date) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getDatatypeFactory().newXMLGregorianCalendar(calendar);
    }

    private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                Logger.getLogger(CreateProvider.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


        return _datatypeFactory;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static com.jidesoft.swing.StyledLabel BHFRegNoLabel;
    public static javax.swing.JTextField BHFRegNumber;
    public static javax.swing.JTextField ContractEntityType;
    public static com.jidesoft.grid.JideTable DisciplineDispensingTable;
    public static javax.swing.JTextField EntityContractNumber;
    public static com.jidesoft.grid.JideTable LicensingTable;
    public static com.jidesoft.swing.JideButton NextButton;
    public static com.jidesoft.combobox.ListComboBox PaymentMethod;
    public static com.jidesoft.swing.StyledLabel ProviderNumber;
    public static com.jidesoft.swing.JideButton addRole;
    private javax.swing.JPanel bodyPanel;
    private com.jidesoft.swing.JideButton cancelPerson;
    public static com.jidesoft.combobox.DateComboBox dateofBirth;
    public static com.jidesoft.grid.JideTable discountBandsTable;
    public static javax.swing.JTextField entityID;
    public static javax.swing.JTextField firstname;
    public static javax.swing.JTextField flagRole;
    public static com.jidesoft.combobox.ListComboBox gender;
    public static com.jidesoft.combobox.ListComboBox homeLanguage;
    public static javax.swing.JTextField identificationNumber;
    public static com.jidesoft.combobox.ListComboBox identificationType;
    public static javax.swing.JTextField initials;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    public static com.jidesoft.combobox.ListComboBox maritalStatus;
    public static com.jidesoft.combobox.ListComboBox reason;
    public static javax.swing.JTextField relationshipType;
    public static com.jidesoft.swing.JideButton saveProvider;
    private com.jidesoft.swing.JideButton searchPerson;
    private com.jidesoft.swing.JideButton searchPerson1;
    public static javax.swing.JTextField secondname;
    public static com.jidesoft.combobox.ListComboBox status;
    public static com.jidesoft.combobox.DateComboBox statusFromDate;
    private com.jidesoft.swing.StyledLabel styledLabel10;
    private com.jidesoft.swing.StyledLabel styledLabel11;
    private com.jidesoft.swing.StyledLabel styledLabel12;
    private com.jidesoft.swing.StyledLabel styledLabel13;
    private com.jidesoft.swing.StyledLabel styledLabel17;
    private com.jidesoft.swing.StyledLabel styledLabel18;
    private com.jidesoft.swing.StyledLabel styledLabel2;
    private com.jidesoft.swing.StyledLabel styledLabel21;
    private com.jidesoft.swing.StyledLabel styledLabel3;
    private com.jidesoft.swing.StyledLabel styledLabel4;
    private com.jidesoft.swing.StyledLabel styledLabel5;
    private com.jidesoft.swing.StyledLabel styledLabel6;
    private com.jidesoft.swing.StyledLabel styledLabel7;
    private com.jidesoft.swing.StyledLabel styledLabel8;
    private com.jidesoft.swing.StyledLabel styledLabel9;
    public static javax.swing.JTextField surname;
    public static com.jidesoft.combobox.ListComboBox title;
    private com.jidesoft.swing.StyledLabel titleName;
    private com.jidesoft.swing.StyledLabel titleName1;
    private com.jidesoft.swing.StyledLabel titleName2;
    private com.jidesoft.swing.StyledLabel titleName3;
    private javax.swing.JPanel titlePanel;
    private javax.swing.JPanel titlePanel1;
    private javax.swing.JPanel titlePanel2;
    private javax.swing.JPanel titlePanel3;
    // End of variables declaration//GEN-END:variables
    private DatatypeFactory _datatypeFactory;

    public void focusGained(FocusEvent e) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public void focusLost(FocusEvent e) {
        String item = "";
        System.out.println("focusLost");
        Object source = e.getSource();
        if (source == productBox) {
            item = productBox.getSelectedItem().toString();
            if (item.equals("")) {
                System.out.println("combo is blanc no selected index");
            } else {
                item = productBox.getSelectedItem().toString();
                int prodId = 0;

                Product prodDesc = NeoWebService.getNeoManagerBeanService().findProductWithDesc(item);
                prodId = prodDesc.getProductId();
                System.out.println("Column value = " + item);
                System.out.println("Product ID = " + prodId);
                discountBandsTable.getColumnModel().getColumn(10).setCellEditor(new OptionType(prodId));
            }
        }
        if (source == optionBox) {
            item = optionBox.getSelectedItem().toString();
            if (item.equals("")) {
                System.out.println("combo is blanc no selected index");
            } else {
                item = optionBox.getSelectedItem().toString();
                int optionId = 0;

                Option optDesc = NeoWebService.getNeoManagerBeanService().findOptionWithName(item);
                optionId = optDesc.getOptionId();
                System.out.println("Column value = " + item);
                System.out.println("Option ID = " + optionId);

            }
        }
        if (source == discTypeBox) {
            int row = DisciplineDispensingTable.getSelectedRow();
            if (row == -1){
                row = 0;
            }
            
            System.out.println("Selected row = "+row);
            item = discTypeBox.getSelectedItem().toString();
            if (item.equals("")) {
                System.out.println("combo is blanc no selected index");
            } else {

                //List<DisciplineTypes> disciplineTypes = NeoWebService.getNeoManagerBeanService().fetchAlldisciplineTypes();
                DisciplineTypeTable dscType = new DisciplineTypeTable();
                String iMap = dscType.getDiscTypeMap().get(item).toString();
                    DisciplineDispensingTable.setValueAt(iMap, row, 1);
                System.out.println("Disciplne Discription Returned as : " + iMap);

            }
        }
    }
}

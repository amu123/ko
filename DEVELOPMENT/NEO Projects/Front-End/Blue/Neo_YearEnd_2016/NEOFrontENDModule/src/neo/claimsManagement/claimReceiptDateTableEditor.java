/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.claimsManagement;

import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import neo.util.dateConverter;

/**
 *
 * @author deonp
 */
public class claimReceiptDateTableEditor extends AbstractCellEditor implements TableCellEditor {

    private com.jidesoft.combobox.DateComboBox dateCombo;

    public claimReceiptDateTableEditor() {
        dateCombo = new com.jidesoft.combobox.DateComboBox();
    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {
        return dateCombo;
    }

    public Object getCellEditorValue() {
        System.out.println(new dateConverter().convertToYYYMMD(dateCombo.getDate()));
        return new dateConverter().convertToYYYMMD(dateCombo.getDate());
    }
}
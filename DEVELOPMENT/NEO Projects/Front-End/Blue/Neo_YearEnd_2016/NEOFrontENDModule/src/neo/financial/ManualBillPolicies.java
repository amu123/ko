/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.financial;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import neo.manager.BillingFrequency;
import neo.manager.Invoice;
import neo.manager.PolicyDetails;
import neo.product.utils.NeoWebService;
import net.gethos.util.time.DateUtils;


/**
 *
 * @author AbigailM
 */
public class ManualBillPolicies {

    private static List<PolicyDetails> policyList;

    public ManualBillPolicies() {
    }

    public static boolean generateInvoice() {

        boolean billresult = false;
        int countSuccess = 0;
        int policyID = 0;
        String DATE_FORMAT = "yyyy/MM/dd";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

        Calendar fcalendar = Calendar.getInstance();
        Calendar lcalendar = Calendar.getInstance();
        neo.manager.Period period = new neo.manager.Period();


        List<neo.manager.PolicyDetails> policyList = NeoWebService.getNeoManagerBeanService().getAllPolicies();

        System.out.println("Get all policy  count  : " + policyList.size());

        if (policyList.size() != 0) {

           int j =0;
            for (PolicyDetails pdetails : policyList) {

                policyID = pdetails.getPolicyID();

                if (pdetails.getPolicyStatus().getStatusType().value().equals("ACTIVE")) {

                    ArrayList<PolicyDetails> apolicyList = new ArrayList<PolicyDetails>();
                    apolicyList.add(pdetails);

                    // Calculate month dates
                    int lastDate = lcalendar.getActualMaximum(Calendar.DATE);
                    int firstDate = fcalendar.getActualMinimum(Calendar.DATE);
                    fcalendar.set(Calendar.DATE, firstDate);
                    lcalendar.set(Calendar.DATE, lastDate);
                    int lastDay = lcalendar.get(Calendar.DAY_OF_WEEK);

                    java.util.Date startdate = fcalendar.getTime();
                    java.util.Date enddate = lcalendar.getTime();

                    period.setEffectiveDate(DateUtils.toYYYYMMDDInt(startdate));
                    period.setTerminationDate(DateUtils.toYYYYMMDDInt(enddate));


                    int prodIdNo = pdetails.getProductId();

                    neo.manager.Product prod = NeoWebService.getNeoManagerBeanService().findProductWithID(prodIdNo);
                    String prodName = prod.getProductName();

                    pdetails.setBillingFrequency(BillingFrequency.MONTHLY);

                    System.out.println("Start billing process.........");

                    try {
                        List<Invoice> billinv = NeoWebService.getNeoManagerBeanService().billPolicies(prod, apolicyList, period);

                        if (billinv.size() != 0) {

                            countSuccess++;

                        }

                    } catch (Exception e) {
                         System.out.println("error message  : " + e.getMessage());
                        continue;
                    }


                }

          j++;
            }
        }
            System.out.println("countSuccess : " + countSuccess);
            if (countSuccess != 0) {
                billresult = true;

            }

            return billresult;
        }



}
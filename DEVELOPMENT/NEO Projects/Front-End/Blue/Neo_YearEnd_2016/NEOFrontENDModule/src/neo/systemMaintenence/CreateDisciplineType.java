/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * CreateDisciplineType.java
 *
 * Created on 2009/06/26, 09:18:48
 */
package neo.systemMaintenence;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import neo.entity.provider.DisciplineTypeTable;
import neo.product.utils.NeoWebService;


/**
 *
 * @author JohanL
 */
public class CreateDisciplineType extends javax.swing.JPanel {

    public DefaultTableModel tableModel = new DefaultTableModel();
    DisciplineTypeTable dscType = new DisciplineTypeTable();

    /** Creates new form CreateDisciplineType */
    public CreateDisciplineType() {
        initComponents();
        displayDisciplineTypes();
        tableModel = (DefaultTableModel) createDisciplineTable.getModel();

    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        createDisciplineTable = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        styledLabel3 = new com.jidesoft.swing.StyledLabel();
        save = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        styledLabel4 = new com.jidesoft.swing.StyledLabel();
        discCode = new javax.swing.JTextField();
        discDescription = new javax.swing.JTextField();

        setBackground(new java.awt.Color(235, 235, 235));

        createDisciplineTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Discipline Code", "Discipline Description"
            }
        ));
        createDisciplineTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(createDisciplineTable);
        createDisciplineTable.getColumnModel().getColumn(0).setMinWidth(150);
        createDisciplineTable.getColumnModel().getColumn(0).setPreferredWidth(150);
        createDisciplineTable.getColumnModel().getColumn(0).setMaxWidth(150);

        jPanel1.setBackground(new java.awt.Color(138, 177, 230));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel3.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel3.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel3.setText("CREATE DISCIPLINE TYPE");
        styledLabel3.setFont(new java.awt.Font("Arial", 1, 14));
        jPanel1.add(styledLabel3);

        save.setText("SAVE");
        save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveActionPerformed(evt);
            }
        });

        jLabel1.setText("Discipline Code");

        jLabel2.setText("Discipline Description");

        jPanel2.setBackground(new java.awt.Color(138, 177, 230));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel4.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel4.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel4.setText("DISCIPLINE TYPES");
        styledLabel4.setFont(new java.awt.Font("Arial", 1, 14));
        jPanel2.add(styledLabel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(50, 50, 50)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(discDescription)
                            .addComponent(discCode, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                            .addComponent(save, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 344, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 469, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(63, Short.MAX_VALUE))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 598, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(discCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(discDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addComponent(save)
                .addGap(37, 37, 37)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveActionPerformed

        int discType;

        System.out.println("Object Check");
        String discCodeS = discCode.getText().toString();
        String discDesc = discDescription.getText().toString();

        if ((discCodeS.equals("")) || (discDesc.equals(""))) {

            System.out.println("Blank");
            JOptionPane.showMessageDialog(null, "Please Enter All the Details", "Error", JOptionPane.ERROR_MESSAGE);

        } else {

            //discType = Integer.parseInt(discCodeS);
            System.out.println("dscType.getDiscTypeMap().get(discDesc) = " + dscType.getDiscTypeMap().get(discDesc));
            Map dttMap = dscType.getDiscTypeMap();
            System.out.println("Map = " + dttMap);

            if (dttMap.get(discDesc) == null) {
                
                tableModel.addRow(new Object[]{});
                neo.manager.DisciplineTypes disc = NeoWebService.getNeoManagerBeanService().addDisciplineType(discCodeS, discDesc);
                JOptionPane.showMessageDialog(null, "Discipline Type Successfully Created", "Information", JOptionPane.INFORMATION_MESSAGE);
                displayDisciplineTypes();
                discCode.setText("");
                discDescription.setText("");

            } else {

                System.out.println("discType" + discCodeS);
                System.out.println("If Object is not blank");
                JOptionPane.showMessageDialog(null, "Discipline Type Already Exists", "Error", JOptionPane.ERROR_MESSAGE);

            }
        }
    }//GEN-LAST:event_saveActionPerformed
    public void displayDisciplineTypes() {

        List<neo.manager.DisciplineTypes> disciplineTypes = NeoWebService.getNeoManagerBeanService().fetchAllDisciplineTypes();
        //DisciplineTypes discType = new DisciplineTypes();
        //DefaultTableModel tableModel = (DefaultTableModel) createDisciplineTable.getModel();
        createDisciplineTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {},
            new String [] {
                "Discipline Code", "Discipline Description"
            }
        ));
        int i = 0;
        for (Iterator it = disciplineTypes.iterator(); it.hasNext();) {
            neo.manager.DisciplineTypes discType = (neo.manager.DisciplineTypes) it.next();
            tableModel = (DefaultTableModel) createDisciplineTable.getModel();
            tableModel.addRow(new Object[]{});
            createDisciplineTable.setValueAt(discType.getDisciplineType(), i, 0);
            createDisciplineTable.setValueAt(discType.getDisciplineDesc(), i, 1);

            i++;
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTable createDisciplineTable;
    private javax.swing.JTextField discCode;
    private javax.swing.JTextField discDescription;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton save;
    private com.jidesoft.swing.StyledLabel styledLabel3;
    private com.jidesoft.swing.StyledLabel styledLabel4;
    // End of variables declaration//GEN-END:variables
}

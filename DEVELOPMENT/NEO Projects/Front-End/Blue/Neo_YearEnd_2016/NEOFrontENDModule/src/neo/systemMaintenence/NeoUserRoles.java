/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * NeoUserRoles.java
 *
 * Created on 2009/09/30, 02:01:40
 */
package neo.systemMaintenence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.swing.table.DefaultTableModel;
import neo.manager.NeoUser;
import neo.manager.SecurityResponsibility;
import neo.manager.SecurityRole;
import neo.product.utils.NeoWebService;
import za.co.AgilityTechnologies.LoginPage;

/**
 *
 * @author johanl
 */
public class NeoUserRoles extends javax.swing.JPanel {

    /** Creates new form NeoUserRoles */
    public static NeoUser userDeatails = LoginPage.neoUser;
    public static Map<String, Integer> roleIdMap = new HashMap<String, Integer>();
    public static Collection<SecurityRole> secRoles = NeoWebService.getNeoManagerBeanService().fetchAllSecurityRoles();
    public static Collection<SecurityRole> userSecRoles = NeoWebService.getNeoManagerBeanService().getUserRoles(userDeatails.getUserId());
    public static ArrayList cbPop;
    public static int reSize;

    public NeoUserRoles() {
        initComponents();
        loadUserRoleDetail();
    }

    public static void loadUserRoleDetail() {
        //user object variables
        int secRoleId = 0;
        String roleDesc = "";
        reSize = userSecRoles.size();
        if (reSize == 0) {
            System.out.println("No previous roles allocated to user");
            //populate user information
            if (userDeatails.getErrorMsg().equals("none")) {
                txt_UserName.setText(userDeatails.getName());
                txt_UserId.setText(new Integer(userDeatails.getUserId()).toString());
                txt_UserSecGroupId.setText(new Integer(userDeatails.getSecurityGroupId()).toString());
                dc_PassExp.setDate(userDeatails.getPasswordExpiryDate().toGregorianCalendar().getTime());

            }

            //add all roles to array
            cbPop = new ArrayList();
            
            for (SecurityRole securityRole : secRoles) {
                roleDesc = securityRole.getDescription();
                int roleId = securityRole.getSecurityRoleId();
                roleIdMap.put(roleDesc, roleId);
                cbPop.add(roleDesc);
            }
            //cb_box population
            cbUserSecPop();
        } else {

            //add all roles to array
            cbPop = new ArrayList();
            
            for (SecurityRole securityRole : secRoles) {
                roleDesc = securityRole.getDescription();
                int roleId = securityRole.getSecurityRoleId();
                roleIdMap.put(roleDesc, roleId);
                cbPop.add(roleDesc);
            }

            //populate user information
            if (userDeatails.getErrorMsg().equals("none")) {
                txt_UserName.setText(userDeatails.getName());
                txt_UserId.setText(new Integer(userDeatails.getUserId()).toString());
                txt_UserSecGroupId.setText(new Integer(userDeatails.getSecurityGroupId()).toString());
                dc_PassExp.setDate(userDeatails.getPasswordExpiryDate().toGregorianCalendar().getTime());

            }

            //populate security role grid
            addRoleResultsRows(reSize);
            int x = 0;
            cbPop = new ArrayList();
            
            for (SecurityRole securityRole : userSecRoles) {
                roleDesc = securityRole.getDescription();
                secRoleId = securityRole.getSecurityRoleId();
                cbPop.remove(roleDesc);

                tbl_UserSecRole.setValueAt(secRoleId, x, 0);
                tbl_UserSecRole.setValueAt(roleDesc, x, 1);

                x++;
            }

            //cb_box population
            cbUserSecPop();
        }
    }

    public static void addRoleResultsRows(int reSize) {

        int reinTableRows = tbl_UserSecRole.getRowCount();

        if (reinTableRows != 0) {
            //removing all rows
            tbl_UserSecRole.setModel(new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                        "Security Role Id", "Security Role Description"
                    }));
            //adding rows according to request
            DefaultTableModel displayTableModel = (DefaultTableModel) tbl_UserSecRole.getModel();
            for (int k = 0; k < reSize; k++) {
                displayTableModel.addRow(new Object[]{});
            }
        } else {
            //adding rows according to request
            DefaultTableModel displayTableModel = (DefaultTableModel) tbl_UserSecRole.getModel();
            for (int k = 0; k < reSize; k++) {
                displayTableModel.addRow(new Object[]{});
            }
        }
    }

    public static void cbUserSecPop() {
        if (cbPop.size() == 0) {
            cb_UserSecRoles.setEditable(false);
            cb_UserSecRoles.setEnabled(false);
        } else {
            cb_UserSecRoles.setModel(new javax.swing.DefaultComboBoxModel(cbPop.toArray()));
        }

    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlView = new javax.swing.JPanel();
        titlePanel1 = new javax.swing.JPanel();
        styledLabel2 = new com.jidesoft.swing.StyledLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_UserName = new javax.swing.JTextField();
        txt_UserId = new javax.swing.JTextField();
        txt_UserSecGroupId = new javax.swing.JTextField();
        dc_PassExp = new com.jidesoft.combobox.DateComboBox();
        cb_UserSecRoles = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        btn_AddSecRole = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_UserSecRole = new com.jidesoft.grid.JideTable();
        btn_DelSecRole = new javax.swing.JButton();
        titlePanel3 = new javax.swing.JPanel();
        styledLabel5 = new com.jidesoft.swing.StyledLabel();
        jPanel1 = new javax.swing.JPanel();
        styledLabel3 = new com.jidesoft.swing.StyledLabel();

        setBackground(new java.awt.Color(235, 235, 235));

        pnlView.setBackground(new java.awt.Color(235, 235, 235));
        pnlView.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        titlePanel1.setBackground(new java.awt.Color(138, 177, 230));

        styledLabel2.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel2.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel2.setText("USER DETAILS");
        styledLabel2.setFont(new java.awt.Font("Arial", 1, 14));
        styledLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titlePanel1.add(styledLabel2);

        jLabel1.setText("User Name :");

        jLabel2.setText("User ID :");

        jLabel3.setText("Security Group ID :");

        jLabel4.setText("Password Expiry Date :");

        txt_UserName.setEditable(false);
        txt_UserName.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txt_UserName.setEnabled(false);

        txt_UserId.setEditable(false);
        txt_UserId.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txt_UserId.setEnabled(false);

        txt_UserSecGroupId.setEditable(false);
        txt_UserSecGroupId.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txt_UserSecGroupId.setEnabled(false);

        dc_PassExp.setButtonVisible(false);
        dc_PassExp.setDisabledBackground(new java.awt.Color(255, 255, 255));
        dc_PassExp.setDisabledForeground(new java.awt.Color(0, 0, 0));
        dc_PassExp.setEditable(false);
        dc_PassExp.setEnabled(false);
        dc_PassExp.setPreferredSize(new java.awt.Dimension(6, 20));

        cb_UserSecRoles.setPreferredSize(new java.awt.Dimension(6, 20));

        jLabel6.setText("User Security Roles :");

        btn_AddSecRole.setText("ADD");
        btn_AddSecRole.setPreferredSize(new java.awt.Dimension(6, 20));

        tbl_UserSecRole.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Security Role Id", "Security Role Description"
            }
        ));
        jScrollPane2.setViewportView(tbl_UserSecRole);
        tbl_UserSecRole.getColumnModel().getColumn(0).setMinWidth(100);
        tbl_UserSecRole.getColumnModel().getColumn(0).setPreferredWidth(100);
        tbl_UserSecRole.getColumnModel().getColumn(0).setMaxWidth(100);

        btn_DelSecRole.setText("DELETE ");

        titlePanel3.setBackground(new java.awt.Color(138, 177, 230));

        styledLabel5.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel5.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel5.setText("USER SECURITY ROLES");
        styledLabel5.setFont(new java.awt.Font("Arial", 1, 14));
        styledLabel5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titlePanel3.add(styledLabel5);

        javax.swing.GroupLayout pnlViewLayout = new javax.swing.GroupLayout(pnlView);
        pnlView.setLayout(pnlViewLayout);
        pnlViewLayout.setHorizontalGroup(
            pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titlePanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 880, Short.MAX_VALUE)
            .addGroup(pnlViewLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6))
                .addGap(95, 95, 95)
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlViewLayout.createSequentialGroup()
                        .addComponent(cb_UserSecRoles, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_AddSecRole, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(354, 354, 354))
                    .addGroup(pnlViewLayout.createSequentialGroup()
                        .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dc_PassExp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_UserSecGroupId)
                            .addComponent(txt_UserId)
                            .addComponent(txt_UserName, javax.swing.GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE))
                        .addGap(450, 450, 450))))
            .addGroup(pnlViewLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(titlePanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 850, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
            .addGroup(pnlViewLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(btn_DelSecRole)
                .addGap(776, 776, 776))
        );
        pnlViewLayout.setVerticalGroup(
            pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlViewLayout.createSequentialGroup()
                .addComponent(titlePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_UserName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txt_UserId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txt_UserSecGroupId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dc_PassExp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cb_UserSecRoles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(btn_AddSecRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addComponent(titlePanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btn_DelSecRole)
                .addGap(22, 22, 22))
        );

        jPanel1.setBackground(new java.awt.Color(138, 177, 230));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel3.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel3.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel3.setText("USER ROLE MANAGEMENT");
        styledLabel3.setFont(new java.awt.Font("Arial", 1, 14));
        jPanel1.add(styledLabel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(pnlView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 990, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(pnlView, javax.swing.GroupLayout.PREFERRED_SIZE, 467, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(59, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_AddSecRole;
    private javax.swing.JButton btn_DelSecRole;
    public static javax.swing.JComboBox cb_UserSecRoles;
    public static com.jidesoft.combobox.DateComboBox dc_PassExp;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel pnlView;
    private com.jidesoft.swing.StyledLabel styledLabel2;
    private com.jidesoft.swing.StyledLabel styledLabel3;
    private com.jidesoft.swing.StyledLabel styledLabel5;
    public static com.jidesoft.grid.JideTable tbl_UserSecRole;
    private javax.swing.JPanel titlePanel1;
    private javax.swing.JPanel titlePanel3;
    public static javax.swing.JTextField txt_UserId;
    public static javax.swing.JTextField txt_UserName;
    public static javax.swing.JTextField txt_UserSecGroupId;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neo.product.utils;

import java.util.List;

/**
 *
 * @author BharathP
 */
public class RegistrationUtil {

    private List entityList;
    private List optionVersionList;
    private List benefitList;

    public RegistrationUtil(){
    }

    
    /**
     * @return the entityList
     */
    public List getEntityList() {
        return entityList;
    }

    /**
     * @param entityList the entityList to set
     */
    public void setEntityList(List entityList) {
        this.entityList = entityList;
    }

    /**
     * @return the optionVersionList
     */
    public List getOptionVersionList() {
        return optionVersionList;
    }

    /**
     * @param optionVersionList the optionVersionList to set
     */
    public void setOptionVersionList(List optionVersionList) {
        this.optionVersionList = optionVersionList;
    }

    /**
     * @return the benefitList
     */
    public List getBenefitList() {
        return benefitList;
    }

    /**
     * @param benefitList the benefitList to set
     */
    public void setBenefitList(List benefitList) {
        this.benefitList = benefitList;
    }

}

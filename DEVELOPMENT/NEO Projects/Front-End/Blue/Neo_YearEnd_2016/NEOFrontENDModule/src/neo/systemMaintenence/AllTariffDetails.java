/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.systemMaintenence;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import neo.manager.LookupType;
import neo.product.utils.NeoWebService;


/**
 *
 * @author johanl
 */
public class AllTariffDetails extends AbstractCellEditor implements TableCellEditor {

    private JComboBox cbComponent;

    public AllTariffDetails(LookupType type) {
        ArrayList strArr = new ArrayList();

            List<String> sysTypeList = NeoWebService.getNeoManagerBeanService().fetchAllFieldsForType(type);
            System.out.println("sysTypeList = " + sysTypeList.toString());

            for (int i = 0; i < sysTypeList.size(); i++) {

                  String sysTypeData = sysTypeList.get(i).toString();
                  strArr.add(sysTypeData);

            }

        cbComponent = new JComboBox(strArr.toArray());
    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {

        return cbComponent;

    }

    public Object getCellEditorValue() {

        return cbComponent.getSelectedItem();

    }

    public JComboBox getCbComponent() {
        return cbComponent;
    }
}

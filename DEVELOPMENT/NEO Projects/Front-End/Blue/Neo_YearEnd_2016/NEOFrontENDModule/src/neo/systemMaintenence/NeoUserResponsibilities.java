/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * NeoUserResponsebilities.java
 *
 * Created on 2009/09/30, 11:08:28
 */
package neo.systemMaintenence;

import java.awt.Component;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import neo.manager.NeoUserResponsibility;
import javax.swing.table.DefaultTableModel;
import neo.manager.NeoUser;
import neo.manager.SecurityResponsibility;
import neo.product.utils.NeoWebService;
import za.co.AgilityTechnologies.LoginPage;

/**
 *
 * @author johanl
 */
public class NeoUserResponsibilities extends javax.swing.JPanel {

    public static NeoUser userDetails = LoginPage.neoUser;
    public static SecurityResponsibility secResponse = null;
    public static Collection<SecurityResponsibility> returnList = new ArrayList<SecurityResponsibility>();
    public static Collection<SecurityResponsibility> secResp = NeoWebService.getNeoManagerBeanService().fetchAllSecurityResponsibilities();
    public static Collection<SecurityResponsibility> userSecResp = null;
    public static int reSize;
    public static int userID = userDetails.getUserId();
    public static ArrayList cbPop;

    /** Creates new form NeoUserResponsebilities */
    public NeoUserResponsibilities() {
        initComponents();
        loadUserRoleDetail();

    }

    public static void loadUserRoleDetail() {
        //user object variables
        String respDesc = "";
        int respId = 0;
        int menuId = 0;
        int reqGroupId = 0;
        Date respStart = null;
        Date respEnd = null;
        userSecResp = NeoWebService.getNeoManagerBeanService().getUserResponsibilities(userID);
        reSize = userSecResp.size();
        System.out.println("result Size = " + reSize);
        System.out.println("cbPop size = "+cbPop);
        if (reSize == 0) {
            System.out.println("No previous responsibilities allocated to user");
            //populate user information
            if (userDetails.getErrorMsg().equals("none")) {
                txt_UserName.setText(userDetails.getName());
                txt_UserId.setText(new Integer(userDetails.getUserId()).toString());
                txt_UserSecGroupId.setText(new Integer(userDetails.getSecurityGroupId()).toString());
                dc_PassExp.setDate(userDetails.getPasswordExpiryDate().toGregorianCalendar().getTime());

            }
            //add all roles to array
            cbPop = new ArrayList();

            for (SecurityResponsibility securityResponsibility : secResp) {
                returnList.add(securityResponsibility);
                respDesc = securityResponsibility.getDescription();
                cbPop.add(respDesc);
            }
        } else {
            System.out.println("cbPop size = "+cbPop);
            //add all roles to array
            cbPop = new ArrayList();

            for (SecurityResponsibility securityResponsibility : secResp) {
                returnList.add(securityResponsibility);
                respDesc = securityResponsibility.getDescription();
                cbPop.add(respDesc);
            }

            //populate user information
            if (userDetails.getErrorMsg().equals("none")) {
                txt_UserName.setText(userDetails.getName());
                txt_UserId.setText(new Integer(userDetails.getUserId()).toString());
                txt_UserSecGroupId.setText(new Integer(userDetails.getSecurityGroupId()).toString());
                dc_PassExp.setDate(userDetails.getPasswordExpiryDate().toGregorianCalendar().getTime());

            }
            //populate security responsibility grid
            addRespResultsRows(reSize);
            //format date colunms
            tableDateFormat();
            int x = 0;
            for (SecurityResponsibility uSecRespDetails : userSecResp) {
                respDesc = uSecRespDetails.getDescription();
                respId = uSecRespDetails.getResponsibilityId();
                menuId = uSecRespDetails.getMenuId();
                reqGroupId = uSecRespDetails.getRequestGroupId();
                respStart = uSecRespDetails.getRespStartDate().toGregorianCalendar().getTime();
                respEnd = uSecRespDetails.getRespEndDate().toGregorianCalendar().getTime();

                cbPop.remove(respDesc);

                tbl_UserResp.setValueAt(respId, x, 0);
                tbl_UserResp.setValueAt(respDesc, x, 1);
                tbl_UserResp.setValueAt(menuId, x, 2);
                tbl_UserResp.setValueAt(reqGroupId, x, 3);
                tbl_UserResp.setValueAt(respStart, x, 4);
                tbl_UserResp.setValueAt(respEnd, x, 5);

                x++;
            }
            System.out.println("cbPop size2 = "+cbPop);
            //cb_box population
            cbUserSecPop();
        }
    }

    public static void addRespResultsRows(int reSize) {

        int reinTableRows = tbl_UserResp.getRowCount();

        if (reinTableRows != 0) {
            //removing all rows
            tbl_UserResp.setModel(new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                        "Responsibility Id", "Responsibility_Description", "Menu_Id", "Request_Group_Id", "Responsibility Start Date", "Responsibility End Date"
                    }));
            //adding rows according to request
            DefaultTableModel displayTableModel = (DefaultTableModel) tbl_UserResp.getModel();
            for (int k = 0; k < reSize; k++) {
                displayTableModel.addRow(new Object[]{});
            }
        } else {
            //adding rows according to request
            DefaultTableModel displayTableModel = (DefaultTableModel) tbl_UserResp.getModel();
            for (int k = 0; k < reSize; k++) {
                displayTableModel.addRow(new Object[]{});
            }
        }
    }

    public static void cbUserSecPop() {

        if (cbPop.size() == 0) {
            cb_UserResp.setEditable(false);
            cb_UserResp.setEnabled(false);
            cb_UserResp.setSelectedItem("");
            dc_RespStartDate.setEditable(false);
            dc_RespStartDate.setEnabled(false);
            

        } else {
            cb_UserResp.setEditable(true);
            cb_UserResp.setEnabled(true);
            cb_UserResp.setSelectedItem("");
            dc_RespStartDate.setEditable(true);
            dc_RespStartDate.setEnabled(true);
            
            cb_UserResp.setModel(new javax.swing.DefaultComboBoxModel(cbPop.toArray()));
        }
        System.out.println("cbPop size in cbUserSecPop = "+cbPop);
    }

    public static void tableDateFormat() {
        tbl_UserResp.getColumnModel().getColumn(4).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });
        tbl_UserResp.getColumnModel().getColumn(5).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlView = new javax.swing.JPanel();
        titlePanel1 = new javax.swing.JPanel();
        styledLabel2 = new com.jidesoft.swing.StyledLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_UserName = new javax.swing.JTextField();
        txt_UserId = new javax.swing.JTextField();
        txt_UserSecGroupId = new javax.swing.JTextField();
        dc_PassExp = new com.jidesoft.combobox.DateComboBox();
        cb_UserResp = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        btn_AddResp = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_UserResp = new com.jidesoft.grid.JideTable();
        btn_DelResp = new javax.swing.JButton();
        titlePanel3 = new javax.swing.JPanel();
        styledLabel5 = new com.jidesoft.swing.StyledLabel();
        jLabel5 = new javax.swing.JLabel();
        dc_RespStartDate = new com.jidesoft.combobox.DateComboBox();
        jPanel1 = new javax.swing.JPanel();
        styledLabel3 = new com.jidesoft.swing.StyledLabel();

        setBackground(new java.awt.Color(235, 235, 235));

        pnlView.setBackground(new java.awt.Color(235, 235, 235));
        pnlView.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        titlePanel1.setBackground(new java.awt.Color(138, 177, 230));

        styledLabel2.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel2.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel2.setText("USER DETAILS");
        styledLabel2.setFont(new java.awt.Font("Arial", 1, 14));
        styledLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titlePanel1.add(styledLabel2);

        jLabel1.setText("User Name :");

        jLabel2.setText("User ID :");

        jLabel3.setText("Security Group ID :");

        jLabel4.setText("Password Expiry Date :");

        txt_UserName.setEditable(false);
        txt_UserName.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txt_UserName.setEnabled(false);

        txt_UserId.setEditable(false);
        txt_UserId.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txt_UserId.setEnabled(false);

        txt_UserSecGroupId.setEditable(false);
        txt_UserSecGroupId.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txt_UserSecGroupId.setEnabled(false);

        dc_PassExp.setButtonVisible(false);
        dc_PassExp.setDisabledBackground(new java.awt.Color(255, 255, 255));
        dc_PassExp.setDisabledForeground(new java.awt.Color(0, 0, 0));
        dc_PassExp.setEditable(false);
        dc_PassExp.setEnabled(false);
        dc_PassExp.setPreferredSize(new java.awt.Dimension(6, 20));

        cb_UserResp.setPreferredSize(new java.awt.Dimension(6, 20));

        jLabel6.setText("User Responsibilities :");

        btn_AddResp.setText("ADD");
        btn_AddResp.setPreferredSize(new java.awt.Dimension(6, 20));
        btn_AddResp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AddRespActionPerformed(evt);
            }
        });

        tbl_UserResp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Responsibility Id", "Responsibility_Description", "Menu_Id", "Request_Group_Id", "Responsibility Start Date", "Responsibility End Date"
            }
        ));
        jScrollPane2.setViewportView(tbl_UserResp);
        tbl_UserResp.getColumnModel().getColumn(0).setMinWidth(100);
        tbl_UserResp.getColumnModel().getColumn(0).setPreferredWidth(100);
        tbl_UserResp.getColumnModel().getColumn(0).setMaxWidth(100);
        tbl_UserResp.getColumnModel().getColumn(1).setMinWidth(150);
        tbl_UserResp.getColumnModel().getColumn(1).setPreferredWidth(150);
        tbl_UserResp.getColumnModel().getColumn(1).setMaxWidth(150);
        tbl_UserResp.getColumnModel().getColumn(2).setMinWidth(100);
        tbl_UserResp.getColumnModel().getColumn(2).setPreferredWidth(100);
        tbl_UserResp.getColumnModel().getColumn(2).setMaxWidth(100);
        tbl_UserResp.getColumnModel().getColumn(3).setMinWidth(100);
        tbl_UserResp.getColumnModel().getColumn(3).setPreferredWidth(100);
        tbl_UserResp.getColumnModel().getColumn(3).setMaxWidth(100);

        btn_DelResp.setText("DELETE ");
        btn_DelResp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_DelRespActionPerformed(evt);
            }
        });

        titlePanel3.setBackground(new java.awt.Color(138, 177, 230));

        styledLabel5.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel5.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel5.setText("USER RESPONSIBILITIES");
        styledLabel5.setFont(new java.awt.Font("Arial", 1, 14));
        styledLabel5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titlePanel3.add(styledLabel5);

        jLabel5.setText("Responsibility Start Date :");

        dc_RespStartDate.setPreferredSize(new java.awt.Dimension(6, 20));

        javax.swing.GroupLayout pnlViewLayout = new javax.swing.GroupLayout(pnlView);
        pnlView.setLayout(pnlViewLayout);
        pnlViewLayout.setHorizontalGroup(
            pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titlePanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1010, Short.MAX_VALUE)
            .addGroup(pnlViewLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6)
                    .addComponent(jLabel5))
                .addGap(82, 82, 82)
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlViewLayout.createSequentialGroup()
                        .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dc_PassExp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_UserSecGroupId)
                            .addComponent(txt_UserId)
                            .addComponent(txt_UserName, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(368, 368, 368))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlViewLayout.createSequentialGroup()
                        .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(dc_RespStartDate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)
                            .addComponent(cb_UserResp, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(368, 368, 368)))
                .addGap(192, 192, 192))
            .addGroup(pnlViewLayout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addComponent(btn_AddResp, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(898, Short.MAX_VALUE))
            .addGroup(pnlViewLayout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addComponent(btn_DelResp)
                .addContainerGap(896, Short.MAX_VALUE))
            .addGroup(pnlViewLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane2)
                    .addComponent(titlePanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 850, Short.MAX_VALUE))
                .addContainerGap(150, Short.MAX_VALUE))
        );
        pnlViewLayout.setVerticalGroup(
            pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlViewLayout.createSequentialGroup()
                .addComponent(titlePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_UserName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txt_UserId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txt_UserSecGroupId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dc_PassExp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cb_UserResp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(dc_RespStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btn_AddResp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(titlePanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btn_DelResp)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(138, 177, 230));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel3.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel3.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel3.setText("USER RESPONSIBILITY MANAGEMENT");
        styledLabel3.setFont(new java.awt.Font("Arial", 1, 14));
        jPanel1.add(styledLabel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1011, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(pnlView, javax.swing.GroupLayout.PREFERRED_SIZE, 895, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(64, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnlView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btn_AddRespActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AddRespActionPerformed
        int rowsAdded = 0;
        int respId = 0;
        String respName = "";
        String startDateStr = "";
        String endDateStr = "";

        Date startDate = dc_RespStartDate.getDate();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
        startDateStr = sdf.format(startDate);

        respName = cb_UserResp.getSelectedItem().toString();
        if (respName.equals("Superuser")) {
            respId = 1;
        } else {
            respId = 2;
        }

        NeoUserResponsibility nUserResp = new NeoUserResponsibility();
        nUserResp.setResponsibilityStartDate(startDateStr);
        //nUserResp.setResponsibilityEndDate(endDateStr);
        nUserResp.setResponsibilityId(respId);
        nUserResp.setUserId(userDetails.getUserId());

        System.out.println("startDateStr = " + startDateStr);
        //System.out.println("endDateStr = " + endDateStr);
        System.out.println("respId = " + respId);
        System.out.println("userId = " + userDetails.getUserId());

        System.out.println("rowsAdded = " + rowsAdded);
        rowsAdded = NeoWebService.getNeoManagerBeanService().addUserResponsibility(nUserResp);
        System.out.println("rowsAdded = " + rowsAdded);
        loadUserRoleDetail();

    }//GEN-LAST:event_btn_AddRespActionPerformed

    private void btn_DelRespActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_DelRespActionPerformed
        int selectedRow = tbl_UserResp.getSelectedRow();
        int selectedResp = new Integer(tbl_UserResp.getValueAt(selectedRow, 0).toString());
        String todayDate = "";
        Date today = GregorianCalendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
        todayDate = sdf.format(today);
        System.out.println("toDay = " + todayDate);
        boolean test = NeoWebService.getNeoManagerBeanService().removeUserResponsibility(todayDate, userID, selectedResp);
        if (test == true) {
            cbPop.clear();
            loadUserRoleDetail();
        } else {
            System.out.println("Update failed");
        }
        System.out.println("test = " + test);
    }//GEN-LAST:event_btn_DelRespActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_AddResp;
    private javax.swing.JButton btn_DelResp;
    public static javax.swing.JComboBox cb_UserResp;
    public static com.jidesoft.combobox.DateComboBox dc_PassExp;
    public static com.jidesoft.combobox.DateComboBox dc_RespStartDate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel pnlView;
    private com.jidesoft.swing.StyledLabel styledLabel2;
    private com.jidesoft.swing.StyledLabel styledLabel3;
    private com.jidesoft.swing.StyledLabel styledLabel5;
    public static com.jidesoft.grid.JideTable tbl_UserResp;
    private javax.swing.JPanel titlePanel1;
    private javax.swing.JPanel titlePanel3;
    public static javax.swing.JTextField txt_UserId;
    public static javax.swing.JTextField txt_UserName;
    public static javax.swing.JTextField txt_UserSecGroupId;
    // End of variables declaration//GEN-END:variables
}

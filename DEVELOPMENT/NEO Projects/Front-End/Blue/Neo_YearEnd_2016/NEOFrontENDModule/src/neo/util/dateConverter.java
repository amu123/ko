/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neo.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.*;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author DeonP
 */
public class dateConverter {
    private Object _datatypeFactory;

    public dateConverter() {

    }

     public XMLGregorianCalendar convertDateXML(Date date) {
       
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            return getDatatypeFactory().newXMLGregorianCalendar(calendar);
        
    }

      public String convertToYYYMMD(java.util.Date vdate) {

        String DATE_FORMAT = "yyyy/MM/dd";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        String adate = sdf.format(vdate);

        return adate;
    }

  
    private DatatypeFactory getDatatypeFactory()  {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                Logger.getLogger(dateConverter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

return  (DatatypeFactory) _datatypeFactory;
    }
}

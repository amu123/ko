/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * CreateCompany.java
 *
 * Created on 2009/05/05, 03:28:32
 */
package neo.quotation.createQuotation;


import com.jidesoft.combobox.ListComboBox;
import com.jidesoft.popup.JidePopup;
import com.jidesoft.swing.AutoCompletionComboBox;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.entity.broker.CreateBroker;
import neo.manager.BrokerHouseProduct;
import neo.manager.CompanyDetails;

import neo.manager.EntitySearchCriteria;
import neo.manager.EntityType;

import neo.manager.PaymentType;
import neo.manager.PersonDetails;
import neo.manager.PolicyDetails;
import neo.manager.Product;
import neo.manager.Quotation;
import neo.manager.ReasonType;
import neo.manager.StatusType;

import neo.quotation.entitydepends.SearchBrokerHouse;
import neo.quotation.entitydepends.SearchPolicyHolderCompany;
import neo.quotation.entitydepends.SearchPolicyHolderTest;

import za.co.AgilityTechnologies.LeftMenu.QuotationsActionFrame;

import neo.product.utils.NeoWebService;

/**
 *
 * @author BharathP
 */
public class CreateNewQuotation extends javax.swing.JPanel {

    String policyNumber = null;
    static Vector vectImportData = null;
    public String CAPTURE_FORMAT_INDIVIDUAL_DETAILS = "Capture individuals Details";
    public String CAPTURE_FORMAT_IMPORT_DETAILS = "Import Individual Details From a File";
    public static Map<String, Integer> brokerMap = new HashMap<String, Integer>();
    public static Map<String, Integer> brokerhouseMap = new HashMap<String, Integer>();
    public static Map<String, Integer> policyHolderMap = new HashMap<String, Integer>();
    Map<String, Integer> insurerMap = new HashMap<String, Integer>();
    Map<String, Integer> previousInsurerMap = new HashMap<String, Integer>();
    Map<String, Integer> productMap = new HashMap<String, Integer>();

    public CreateNewQuotation() {


        initComponents();
        uploadFileBtn.setVisible(false);
        vectImportData = null;
        policyNo.setVisible(false);
        PolicyID.setVisible(false);
        
        
        btnAddProductName.setVisible(false);
        
        nextButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
            }
        });

        List lstOfItems = new ArrayList();






        lstOfItems.clear();
        lstOfItems.add(" ");
        lstOfItems.add("Calendar");
        lstOfItems.add("Annual Anniversary");
        lstOfItems.add("Defined Period");
        lstOfItems.add("Quarterly");
        lstOfItems.add("Bi-Annual");
        this.addPolicyPeriods(lstOfItems);
        lstOfItems.clear();
        lstOfItems.add(CAPTURE_FORMAT_INDIVIDUAL_DETAILS);
        lstOfItems.add(CAPTURE_FORMAT_IMPORT_DETAILS);
        this.addCaptureFormats(lstOfItems);



    }

    protected void addProductNames(List lstOfProductNames) {
        productName.removeAllItems();
        Iterator lstIter = lstOfProductNames.iterator();
        while (lstIter.hasNext()) {
            productName.addItem(lstIter.next());
        }
    }

    protected void addPolicyTypes(List lstOfPolicyTypes) {
        policyType.removeAllItems();
        Iterator lstIter = lstOfPolicyTypes.iterator();
        while (lstIter.hasNext()) {
            policyType.addItem(lstIter.next());
        }
    }

    protected void addPolicyHolders(List lstOfPolicyHolders) {
        policyHolder.removeAllItems();
        Iterator lstIter = lstOfPolicyHolders.iterator();
        while (lstIter.hasNext()) {
            policyHolder.addItem(lstIter.next());
        }
    }

    protected void addPolicyPeriods(List lstOfPolicyPeriods) {
        policyPeriod.removeAllItems();
        Iterator lstIter = lstOfPolicyPeriods.iterator();
        while (lstIter.hasNext()) {
            policyPeriod.addItem(lstIter.next());
        }
    }

    protected void addAgentNames(List lstOfAgentNames) {
        brokerHouse.removeAllItems();
        Iterator lstIter = lstOfAgentNames.iterator();
        while (lstIter.hasNext()) {
            brokerHouse.addItem(lstIter.next());
        }
    }

    protected void addBrokerHouses(List lstOfBrokerHouses) {
        brokerName.removeAllItems();
        Iterator lstIter = lstOfBrokerHouses.iterator();
        while (lstIter.hasNext()) {
            brokerName.addItem(lstIter.next());
        }
    }

    protected void addCaptureFormats(List lstOfCaptureFormats) {
        insuredPersonCaptureFormat.removeAllItems();
        Iterator lstIter = lstOfCaptureFormats.iterator();
        while (lstIter.hasNext()) {
            insuredPersonCaptureFormat.addItem(lstIter.next());
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titlePanel = new javax.swing.JPanel();
        titleName = new com.jidesoft.swing.StyledLabel();
        bodyPanel = new javax.swing.JPanel();
        styledLabel1 = new com.jidesoft.swing.StyledLabel();
        styledLabel2 = new com.jidesoft.swing.StyledLabel();
        styledLabel4 = new com.jidesoft.swing.StyledLabel();
        styledLabel5 = new com.jidesoft.swing.StyledLabel();
        styledLabel9 = new com.jidesoft.swing.StyledLabel();
        styledLabel11 = new com.jidesoft.swing.StyledLabel();
        brokerCommision = new javax.swing.JTextField();
        policyType = new com.jidesoft.combobox.ListComboBox();
        policyPeriod = new com.jidesoft.combobox.ListComboBox();
        policyStartDate = new com.jidesoft.combobox.DateComboBox();
        styledLabel6 = new com.jidesoft.swing.StyledLabel();
        styledLabel7 = new com.jidesoft.swing.StyledLabel();
        styledLabel10 = new com.jidesoft.swing.StyledLabel();
        insuredPersonCaptureFormat = new com.jidesoft.combobox.ListComboBox();
        jPanel1 = new javax.swing.JPanel();
        styledLabel24 = new com.jidesoft.swing.StyledLabel();
        policyEndDate = new com.jidesoft.combobox.DateComboBox();
        styledLabel3 = new com.jidesoft.swing.StyledLabel();
        brokerName = new com.jidesoft.combobox.ListComboBox();
        styledLabel8 = new com.jidesoft.swing.StyledLabel();
        priorPolicyClaims = new javax.swing.JTextField();
        styledLabel12 = new com.jidesoft.swing.StyledLabel();
        paymentTypeCombo = new com.jidesoft.swing.AutoCompletionComboBox();
        btnAddProductName = new com.jidesoft.swing.JideButton();
        btnAddPolicyHolder = new com.jidesoft.swing.JideButton();
        btnAddBrokerHouseName = new com.jidesoft.swing.JideButton();
        productName = new com.jidesoft.swing.AutoCompletionComboBox();
        policyHolder = new com.jidesoft.swing.AutoCompletionComboBox();
        brokerHouse = new com.jidesoft.swing.AutoCompletionComboBox();
        nextButton = new com.jidesoft.swing.JideButton();
        savePolicyDetails = new com.jidesoft.swing.JideButton();
        Previousinsurer = new com.jidesoft.combobox.ListComboBox();
        policyNo = new com.jidesoft.swing.StyledLabel();
        PolicyID = new javax.swing.JTextField();
        uploadFileBtn = new com.jidesoft.swing.JideButton();

        setBackground(new java.awt.Color(235, 235, 235));
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(143, 196, 200)));

        Border raisedEdge = BorderFactory.createEtchedBorder(EtchedBorder.RAISED, Color.decode("#EBEBEB"), Color.decode("#99C3FD"));
        titlePanel.setBorder(raisedEdge);
        titlePanel.setBackground(new Color(153,195,253));
        titlePanel.add(titleName,BorderLayout.LINE_START);
        titlePanel.add(titleName);
        titlePanel.setLayout(new java.awt.BorderLayout());

        titleName.setForeground(new java.awt.Color(255, 255, 255));
        titleName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleName.setText("CREATE A NEW APPLICATION");
        titleName.setFont(new java.awt.Font("Arial", 1, 14));
        titleName.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titlePanel.add(titleName, java.awt.BorderLayout.CENTER);

        bodyPanel.setBackground(new java.awt.Color(235, 235, 235));
        bodyPanel.setOpaque(false);

        styledLabel1.setForeground(new java.awt.Color(217, 82, 82));
        styledLabel1.setText("Product Name");
        styledLabel1.setFont(new java.awt.Font("Arial", 0, 11));

        styledLabel2.setBackground(new java.awt.Color(236, 81, 25));
        styledLabel2.setForeground(new java.awt.Color(205, 30, 30));
        styledLabel2.setText("Policy Type");

        styledLabel4.setBackground(new java.awt.Color(236, 81, 25));
        styledLabel4.setForeground(new java.awt.Color(205, 30, 30));
        styledLabel4.setText("Policy Holder ");

        styledLabel5.setBackground(new java.awt.Color(236, 81, 25));
        styledLabel5.setForeground(new java.awt.Color(205, 30, 30));
        styledLabel5.setText("Policy Billing Period");

        styledLabel9.setBackground(new java.awt.Color(236, 81, 25));
        styledLabel9.setForeground(new java.awt.Color(205, 30, 30));
        styledLabel9.setText("Policy Start Date(YYYY/MM/DD)");

        styledLabel11.setBackground(new java.awt.Color(236, 81, 25));
        styledLabel11.setForeground(new java.awt.Color(205, 30, 30));
        styledLabel11.setText("Broker House");

        policyType.setMinimumSize(new java.awt.Dimension(6, 20));
        policyType.setModel(new javax.swing.DefaultComboBoxModel(new String[] {"","Group Policy", "Individual Policy" }));
        policyType.setPreferredSize(new java.awt.Dimension(6, 20));
        policyType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                policyTypeItemStateChanged(evt);
            }
        });

        policyPeriod.setMinimumSize(new java.awt.Dimension(6, 20));
        policyPeriod.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Calendar", "Annual Anniversary", "Defined Period", "Quarterly", "Bi-Annual" }));
        policyPeriod.setPreferredSize(new java.awt.Dimension(6, 20));
        policyPeriod.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                policyPeriodItemStateChanged(evt);
            }
        });

        policyStartDate.setFormat(new SimpleDateFormat("yyyy/MM/dd"));
        policyStartDate.setMinimumSize(new java.awt.Dimension(6, 20));
        policyStartDate.setPreferredSize(new java.awt.Dimension(6, 20));
        policyStartDate.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                policyStartDateItemStateChanged(evt);
            }
        });

        styledLabel6.setForeground(new java.awt.Color(205, 30, 30));
        styledLabel6.setText("Broker Commission Percentage");

        styledLabel7.setText("Previous Insurer");

        styledLabel10.setBackground(new java.awt.Color(236, 81, 25));
        styledLabel10.setForeground(new java.awt.Color(205, 30, 30));
        styledLabel10.setText("Insured Persons Capture Format");

        insuredPersonCaptureFormat.setEditable(false);
        insuredPersonCaptureFormat.setMinimumSize(new java.awt.Dimension(6, 20));
        insuredPersonCaptureFormat.setPreferredSize(new java.awt.Dimension(6, 20));
        insuredPersonCaptureFormat.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                insuredPersonCaptureFormatItemStateChanged(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel24.setBackground(new java.awt.Color(236, 81, 25));
        styledLabel24.setForeground(new java.awt.Color(200, 57, 57));
        styledLabel24.setText("Policy End Date((YYYY/MM/DD)");

        policyEndDate.setFormat(new SimpleDateFormat("yyyy/MM/dd"));
        policyEndDate.setMinimumSize(new java.awt.Dimension(6, 20));
        policyEndDate.setPreferredSize(new java.awt.Dimension(6, 20));

        styledLabel3.setBackground(new java.awt.Color(236, 81, 25));
        styledLabel3.setForeground(new java.awt.Color(200, 57, 57));
        styledLabel3.setText("Broker/Agent Name");

        /*neo.manager.EntitySearchCriteria brokerSearch = new EntitySearchCriteria();
        brokerSearch.setEntityType(neo.manager.EntityType.PERSON);
        brokerSearch.setContractType(neo.manager.ContractType.BROKER);
        java.util.List<neo.manager.PersonDetails> brokerresult = NeoManagerBeanFactory.neoMangerBean.findPersonsWithSearchCriteria(brokerSearch);
        int totalbrokersize=brokerresult.size();

        String brokers[]=new String[totalbrokersize];
        int brokerno=0;

        for(PersonDetails brokerDetails: brokerresult)
        {
            int brokerID=brokerDetails.getEntityId();

            brokers[brokerno]=brokerDetails.getName();
            brokerMap.put(brokers[brokerno], brokerID);

            brokerno++;

        }
        brokerName.setModel(new javax.swing.DefaultComboBoxModel(brokers));*/
        brokerName.setMinimumSize(new java.awt.Dimension(6, 20));
        brokerName.setPreferredSize(new java.awt.Dimension(6, 20));
        brokerName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                brokerNameActionPerformed(evt);
            }
        });

        styledLabel8.setText("Prior Policy Claims Paid");

        styledLabel12.setForeground(new java.awt.Color(216, 82, 82));
        styledLabel12.setText("Payment Type");

        neo.manager.PaymentType paymenttypes[]=PaymentType.values();
        paymentTypeCombo.setModel(new javax.swing.DefaultComboBoxModel(paymenttypes));
        paymentTypeCombo.setMinimumSize(new java.awt.Dimension(6, 20));
        paymentTypeCombo.setPreferredSize(new java.awt.Dimension(6, 20));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(styledLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(styledLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel24, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(styledLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(42, 42, 42)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(priorPolicyClaims, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                    .addComponent(policyEndDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                    .addComponent(brokerName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                    .addComponent(paymentTypeCombo, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE))
                .addGap(20, 20, 20))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(71, 71, 71)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(paymentTypeCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(policyEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(brokerName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(priorPolicyClaims, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(82, Short.MAX_VALUE))
        );

        btnAddProductName.setBorder(null);
        btnAddProductName.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/view_add.png"))); // NOI18N
        btnAddProductName.setButtonStyle(1);
        btnAddProductName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddProductNameActionPerformed(evt);
            }
        });

        btnAddPolicyHolder.setBorder(null);
        btnAddPolicyHolder.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/view_add.png"))); // NOI18N
        btnAddPolicyHolder.setButtonStyle(1);
        btnAddPolicyHolder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddPolicyHolderActionPerformed(evt);
            }
        });

        btnAddBrokerHouseName.setBorder(null);
        btnAddBrokerHouseName.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/view_add.png"))); // NOI18N
        btnAddBrokerHouseName.setButtonStyle(1);
        btnAddBrokerHouseName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddBrokerHouseNameActionPerformed(evt);
            }
        });

        List<Product> listProduct=NeoWebService.getNeoManagerBeanService().fetchAllProducts();
        int totalproduct=listProduct.size();

        String products[]=new String[totalproduct];
        int productno=0;
        for(Product productDetails: listProduct)
        {
            products[productno]=productDetails.getProductName();

            int productsID=productDetails.getProductId();
            productMap.put(products[productno], productsID);
            productno++;

        }

        productName.setModel(new javax.swing.DefaultComboBoxModel(products));
        productName.setMinimumSize(new java.awt.Dimension(6, 20));
        productName.setPreferredSize(new java.awt.Dimension(6, 20));
        productName.setStrict(false);

        policyHolder.setMinimumSize(new java.awt.Dimension(6, 20));
        policyHolder.setPreferredSize(new java.awt.Dimension(6, 20));
        policyHolder.setStrict(false);

        neo.manager.EntitySearchCriteria brokerhouseSearch = new EntitySearchCriteria();
        brokerhouseSearch.setEntityType(neo.manager.EntityType.COMPANY);
        brokerhouseSearch.setContractType(neo.manager.ContractType.BROKER_HOUSE);
        java.util.List<neo.manager.CompanyDetails> brokerhouseresult = NeoWebService.getNeoManagerBeanService().findCompaniesWithSearchCriteria(brokerhouseSearch);
        int totalbhsize=brokerhouseresult.size();

        String brokerhouses[]=new String[totalbhsize];
        int bh=0;
        for(CompanyDetails brokerhouseDetails: brokerhouseresult)
        {

            brokerhouses[bh]=brokerhouseDetails.getName();
            int brokerhouseID=brokerhouseDetails.getEntityId();
            brokerhouseMap.put(brokerhouses[bh], brokerhouseID);

            bh++;

        }

        brokerHouse.setModel(new javax.swing.DefaultComboBoxModel(brokerhouses));
        int BrokerHouseID = brokerhouseMap.get(brokerHouse.getSelectedItem());
        List<Integer> listInteger = NeoWebService.getNeoManagerBeanService().getAllAgentsForBrokerHouse(BrokerHouseID);

        brokerName.removeAllItems();
        if (listInteger.size() == 0) {
            neo.manager.EntitySearchCriteria brokerSearch = new EntitySearchCriteria();
            brokerSearch.setEntityType(neo.manager.EntityType.PERSON);
            brokerSearch.setContractType(neo.manager.ContractType.BROKER);
            java.util.List<neo.manager.PersonDetails> brokerresult = NeoWebService.getNeoManagerBeanService().findPersonsWithSearchCriteria(brokerSearch);
            int totalbrokersize=brokerresult.size();

            String brokers[]=new String[totalbrokersize];
            int brokerno=0;

            for(PersonDetails brokerDetails: brokerresult)
            {
                int brokerID=brokerDetails.getEntityId();

                brokers[brokerno]=brokerDetails.getName();
                brokerMap.put(brokers[brokerno], brokerID);

                brokerno++;
            }
            brokerName.setModel(new javax.swing.DefaultComboBoxModel(brokers));
        } else {
            String[] brokers=new String[listInteger.size()];

            int bro=0;
            for (Integer brokerid : listInteger) {
                brokerName.removeAllItems();
                PersonDetails edbrokers = NeoWebService.getNeoManagerBeanService().getPersonDetails(brokerid);
                brokers[bro] = edbrokers.getName();
                brokerMap.put(brokers[bro], brokerid);
                //addNewItemsToBrokerBox(brokerName, brokerNames);
                bro++;
            }
            brokerName.setModel(new javax.swing.DefaultComboBoxModel(brokers));
        }
        brokerHouse.setMinimumSize(new java.awt.Dimension(6, 20));
        brokerHouse.setPreferredSize(new java.awt.Dimension(6, 20));
        brokerHouse.setStrict(false);
        brokerHouse.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                brokerHouseItemStateChanged(evt);
            }
        });

        nextButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/arrow2_right_green.png"))); // NOI18N
        nextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextButtonActionPerformed(evt);
            }
        });

        savePolicyDetails.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        savePolicyDetails.setText("SAVE");
        savePolicyDetails.setButtonStyle(1);
        savePolicyDetails.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savePolicyDetailsActionPerformed(evt);
            }
        });

        neo.manager.EntitySearchCriteria preinsurerSearch = new EntitySearchCriteria();
        preinsurerSearch.setEntityType(neo.manager.EntityType.COMPANY);
        preinsurerSearch.setContractType(neo.manager.ContractType.INSURER);

        java.util.List<neo.manager.CompanyDetails> preresult = NeoWebService.getNeoManagerBeanService().findCompaniesWithSearchCriteria(preinsurerSearch);
        int pretotalsize=preresult.size();

        String preinsurers[]=new String[pretotalsize];
        int j=0;
        for(CompanyDetails preinsurerDetails: preresult)
        {
            preinsurers[j]=preinsurerDetails.getName();
            int preinsurerID=preinsurerDetails.getEntityId();
            previousInsurerMap.put(preinsurers[j], preinsurerID);

            j++;

        }

        Previousinsurer.setModel(new javax.swing.DefaultComboBoxModel(preinsurers));
        Previousinsurer.setMinimumSize(new java.awt.Dimension(6, 20));
        Previousinsurer.setPreferredSize(new java.awt.Dimension(6, 20));

        policyNo.setForeground(new java.awt.Color(217, 82, 82));
        policyNo.setText("New Application Number");
        policyNo.setFont(new java.awt.Font("Arial", 0, 11));

        uploadFileBtn.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        uploadFileBtn.setText("UpLoad File");
        uploadFileBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uploadFileBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout bodyPanelLayout = new javax.swing.GroupLayout(bodyPanel);
        bodyPanel.setLayout(bodyPanelLayout);
        bodyPanelLayout.setHorizontalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bodyPanelLayout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(styledLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bodyPanelLayout.createSequentialGroup()
                                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(bodyPanelLayout.createSequentialGroup()
                                                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(styledLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(styledLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(styledLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(32, 32, 32))
                                            .addGroup(bodyPanelLayout.createSequentialGroup()
                                                .addComponent(styledLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                        .addGroup(bodyPanelLayout.createSequentialGroup()
                                            .addComponent(styledLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                    .addGroup(bodyPanelLayout.createSequentialGroup()
                                        .addComponent(policyNo, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(17, 17, 17)))
                                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(bodyPanelLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(brokerCommision, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                                            .addComponent(policyStartDate, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                                            .addComponent(brokerHouse, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                                            .addComponent(Previousinsurer, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                                            .addComponent(insuredPersonCaptureFormat, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                                            .addComponent(uploadFileBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(policyType, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                                    .addComponent(productName, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                                    .addGroup(bodyPanelLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(policyPeriod, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                                            .addComponent(policyHolder, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)))
                                    .addComponent(PolicyID, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)))
                            .addComponent(styledLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnAddBrokerHouseName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnAddPolicyHolder, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnAddProductName, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE))
                        .addGap(39, 39, 39))
                    .addGroup(bodyPanelLayout.createSequentialGroup()
                        .addGap(133, 133, 133)
                        .addComponent(savePolicyDetails, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bodyPanelLayout.createSequentialGroup()
                        .addGap(398, 398, 398)
                        .addComponent(nextButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(bodyPanelLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18))
        );
        bodyPanelLayout.setVerticalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(bodyPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, bodyPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(bodyPanelLayout.createSequentialGroup()
                                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(policyNo, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(PolicyID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(styledLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(productName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(btnAddProductName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(policyType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(policyHolder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAddPolicyHolder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(bodyPanelLayout.createSequentialGroup()
                                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(styledLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(policyPeriod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(policyStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(styledLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(brokerHouse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(btnAddBrokerHouseName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(brokerCommision, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(styledLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Previousinsurer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(styledLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(insuredPersonCaptureFormat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(uploadFileBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nextButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(bodyPanelLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(savePolicyDetails, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(101, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titlePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 1007, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(bodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(67, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titlePanel, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void brokerNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_brokerNameActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_brokerNameActionPerformed

private void insuredPersonCaptureFormatItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_insuredPersonCaptureFormatItemStateChanged
// TODO add your handling code here:
    vectImportData = null;
    if (insuredPersonCaptureFormat.getSelectedItem().equals(CAPTURE_FORMAT_IMPORT_DETAILS)) {
    uploadFileBtn.setVisible(true);
        /*JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Select the Excel file to import individuals details");
        fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setFileFilter(new FileNameExtensionFilter("MS Excel File", "xls"));
        //fileChooser.showDialog(null, "Open");
        fileChooser.showOpenDialog(null);
        File selectedFile = fileChooser.getSelectedFile();*/
//johan code
     /*JFileChooser fileChooser = new JFileChooser();
        fileChooser.approveSelection();
        fileChooser.setDialogTitle("Select Excel File To be Imported");
        fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setFileFilter(new FileNameExtensionFilter("MS Excel File", "xls"));
        //fileChooser.showDialog(jPanel2, "Open");
        fileChooser.showOpenDialog(null);

        //get selected file from file chooser

        java.io.File selectedFile = fileChooser.getSelectedFile();






        ReadIndividualDetails readIndividualDetails = new ReadIndividualDetails();
        readIndividualDetails.init(selectedFile.getAbsolutePath());
        vectImportData = readIndividualDetails.getValues();*/

    } else if (evt.getStateChange() == ItemEvent.SELECTED && insuredPersonCaptureFormat.getSelectedItem().equals(CAPTURE_FORMAT_INDIVIDUAL_DETAILS)) {
        uploadFileBtn.setVisible(false);
        vectImportData = null;
    }
}//GEN-LAST:event_insuredPersonCaptureFormatItemStateChanged

private void nextButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextButtonActionPerformed

    if (PolicyID.getText().isEmpty() || PolicyID == null || PolicyID.getText().equals("")) {
        if (savePolicy()) {

            Quotation quote = new Quotation();

            int policyID = NeoWebService.getNeoManagerBeanService().getPolicy(policyNumber).getPolicyID();


            NeoWebService.getNeoManagerBeanService().addQuotation(quote, policyID);
            JOptionPane.showMessageDialog(null, "New Application Details Successfully created", "Information", JOptionPane.INFORMATION_MESSAGE);

            QuotationsActionFrame.quotation.setSelectedIndex(1);
            CaptureIndividuals.policyNumber.setText(policyNumber);
            CaptureIndividuals.policyHoldername.setText((String) policyHolder.getSelectedItem());
            CaptureIndividuals.broker.setText((String) brokerName.getSelectedItem());

        } else {
            JOptionPane.showMessageDialog(null, "Please Enter All the values", "Error", JOptionPane.ERROR_MESSAGE);
        }
    } else {
        QuotationsActionFrame.quotation.setSelectedIndex(1);
        CaptureIndividuals.displayInsuredPersons(PolicyID.getText());


    }
}//GEN-LAST:event_nextButtonActionPerformed

private void btnAddProductNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddProductNameActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_btnAddProductNameActionPerformed

private void btnAddPolicyHolderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddPolicyHolderActionPerformed
    SearchPolicyHolderTest.personFlag = "NewQuotation";
    SearchPolicyHolderCompany.Flag = "NewQuotation";
    if (policyType.getSelectedIndex() == 1) {
        neo.manager.EntitySearchCriteria companySearch = new EntitySearchCriteria();

        companySearch.setEntityType(neo.manager.EntityType.COMPANY);

        companySearch.setContractType(neo.manager.ContractType.POLICY_HOLDER);
        java.util.List<neo.manager.CompanyDetails> result = NeoWebService.getNeoManagerBeanService().findCompaniesWithSearchCriteria(companySearch);


        int rowCount = result.size();
        final JidePopup popup = new JidePopup();
        SearchPolicyHolderCompany frame = new SearchPolicyHolderCompany();


        frame.setSize(1000, 1000);
        popup.setMovable(true);



        String colName[] = {"Entity Type", "Entity ID", "Company Name", "Reg Number", "VAT Number"};
        DefaultTableModel model = new DefaultTableModel(rowCount, colName.length);
        model.setColumnIdentifiers(colName);
        SearchPolicyHolderCompany.resultTable.setModel(model);

        int i = 0;
        for (Iterator it = result.iterator(); it.hasNext();) {
            neo.manager.CompanyDetails nP = (neo.manager.CompanyDetails) it.next();
            SearchPolicyHolderCompany.resultTable.setValueAt("POLICY_HOLDER", i, 0);
            SearchPolicyHolderCompany.resultTable.setValueAt(nP.getEntityId(), i, 1);
            SearchPolicyHolderCompany.resultTable.setValueAt(nP.getName(), i, 2);
            SearchPolicyHolderCompany.resultTable.setValueAt(nP.getRegistrationNumber(), i, 3);
            SearchPolicyHolderCompany.resultTable.setValueAt(nP.getVatNumber(), i, 4);
            i++;

        }

        frame.setVisible(true);
        popup.showPopup();

    }
    if (policyType.getSelectedIndex() == 2) {


        neo.manager.EntitySearchCriteria personSearch = new EntitySearchCriteria();

        // String firstname, Surname, Initial, = "";
        personSearch.setEntityType(neo.manager.EntityType.PERSON);
        personSearch.setContractType(neo.manager.ContractType.POLICY_HOLDER);
        java.util.List<neo.manager.PersonDetails> result = NeoWebService.getNeoManagerBeanService().findPersonsWithSearchCriteria(personSearch);
        int rowCount = result.size();
        final JidePopup popup = new JidePopup();
        //JFrame frame = new JFrame("Search Results");
        SearchPolicyHolderTest frame = new SearchPolicyHolderTest();
        //    frame.add(new SearchPerson());
        // frame.setSize(500, 500);
        popup.setMovable(true);

        String colName[] = {"Entity Type", "Entity ID", "First Name", "Surname", "Date of Birth"};
        DefaultTableModel model = new DefaultTableModel(rowCount, colName.length);
        model.setColumnIdentifiers(colName);
        SearchPolicyHolderTest.resultTable.setModel(model);
        int i = 0;
        for (Iterator it = result.iterator(); it.hasNext();) {
            neo.manager.PersonDetails nP = (neo.manager.PersonDetails) it.next();
            SearchPolicyHolderTest.resultTable.setValueAt("Policy Holder", i, 0);
            SearchPolicyHolderTest.resultTable.setValueAt(nP.getEntityId(), i, 1);
            SearchPolicyHolderTest.resultTable.setValueAt(nP.getName(), i, 2);
            SearchPolicyHolderTest.resultTable.setValueAt(nP.getSurname(), i, 3);
            SearchPolicyHolderTest.resultTable.setValueAt(nP.getDateOfBirth(), i, 4);
            i++;
        }

        frame.setVisible(true);
        popup.showPopup();

    }
}//GEN-LAST:event_btnAddPolicyHolderActionPerformed

private void btnAddBrokerHouseNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddBrokerHouseNameActionPerformed

    neo.manager.EntitySearchCriteria companySearch = new EntitySearchCriteria();

    companySearch.setEntityType(EntityType.COMPANY);
    companySearch.setContractType(neo.manager.ContractType.BROKER_HOUSE);


    java.util.List<neo.manager.CompanyDetails> result = NeoWebService.getNeoManagerBeanService().findCompaniesWithSearchCriteria(companySearch);


    int rowCount = result.size();

    if (rowCount == 0) {
        JOptionPane.showMessageDialog(null, "No Record found", "Information", JOptionPane.INFORMATION_MESSAGE);
    } else {

        final JidePopup popup = new JidePopup();
        SearchBrokerHouse frame = new SearchBrokerHouse();


        frame.setSize(500, 500);
        popup.setMovable(true);

        String colName[] = {"Entity Type", "Entity ID", "Company Name", "Reg Number", "VAT Number"};
        DefaultTableModel model = new DefaultTableModel(rowCount, colName.length);
        model.setColumnIdentifiers(colName);
        SearchBrokerHouse.resultTable.setModel(model);

        int i = 0;
        for (Iterator it = result.iterator(); it.hasNext();) {
            neo.manager.CompanyDetails nP = (neo.manager.CompanyDetails) it.next();
            SearchBrokerHouse.resultTable.setValueAt("BROKER_HOUSE", i, 0);
            SearchBrokerHouse.resultTable.setValueAt(nP.getEntityId(), i, 1);
            SearchBrokerHouse.resultTable.setValueAt(nP.getName(), i, 2);
            SearchBrokerHouse.resultTable.setValueAt(nP.getRegistrationNumber(), i, 3);
            SearchBrokerHouse.resultTable.setValueAt(nP.getVatNumber(), i, 4);
            i++;

        }

        frame.setVisible(true);
        popup.showPopup();


}//GEN-LAST:event_btnAddBrokerHouseNameActionPerformed
    }
private void savePolicyDetailsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savePolicyDetailsActionPerformed
    // TODO add your handling code here:
    if (PolicyID.getText().isEmpty() || PolicyID == null || PolicyID.getText().equals("")) {
        if (savePolicy()) {

            Quotation quote = new Quotation();

            int policyID = NeoWebService.getNeoManagerBeanService().getPolicy(policyNumber).getPolicyID();


            NeoWebService.getNeoManagerBeanService().addQuotation(quote, policyID);
            JOptionPane.showMessageDialog(null, "New Application Details Successfully created", "Information", JOptionPane.INFORMATION_MESSAGE);
            QuotationsActionFrame.quotation.setSelectedIndex(1);
            CaptureIndividuals.policyNumber.setText(policyNumber);
            CaptureIndividuals.policyHoldername.setText((String) policyHolder.getSelectedItem());
            CaptureIndividuals.broker.setText((String) brokerName.getSelectedItem());


        } else {
            JOptionPane.showMessageDialog(null, "Please Enter All the values", "Error", JOptionPane.ERROR_MESSAGE);
        }
    } else {
        QuotationsActionFrame.quotation.setSelectedIndex(1);
        CaptureIndividuals.policyNumber.setText(PolicyID.getText());
        CaptureIndividuals.policyHoldername.setText((String) policyHolder.getSelectedItem());
        CaptureIndividuals.broker.setText((String) brokerName.getSelectedItem());
    }
}//GEN-LAST:event_savePolicyDetailsActionPerformed

private void policyPeriodItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_policyPeriodItemStateChanged
    // TODO add your handling code here:
    if (evt.getStateChange() == ItemEvent.SELECTED && policyPeriod.getSelectedItem().equals("Calendar")) {

        /*Calendar cal = Calendar.getInstance();

        cal.set(2009, 00, 01);
        Date startDates = cal.getTime();

        policyStartDate.setDate(startDates);
        cal.set(2009, 11, 31);
        policyEndDate.setDate(cal.getTime());
        } else if (evt.getStateChange() == ItemEvent.SELECTED && policyPeriod.getSelectedItem().equals("Annual Anniversary")) {
        //"Annual Anniversary", "Defined Period", "Quarterly", "Bi-Annual"


        if (policyStartDate.getDate() != null) {
        Date startDate = policyStartDate.getDate();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(startDate);

        policyEndDate.setDate(cal1.getTime());
        }*/
    }
}//GEN-LAST:event_policyPeriodItemStateChanged

private void policyStartDateItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_policyStartDateItemStateChanged
    // TODO add your handling code here:


    if (policyStartDate.getDate() != null) {
        Date startDate = policyStartDate.getDate();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(startDate);
        cal1.add(Calendar.DATE, 364);
        policyEndDate.setDate(cal1.getTime());
    }


}//GEN-LAST:event_policyStartDateItemStateChanged

private void policyTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_policyTypeItemStateChanged
    // TODO add your handling code here:

    //Policy Holder is Company
    if (evt.getStateChange() == ItemEvent.SELECTED && policyType.getSelectedIndex() == 1) {
        neo.manager.EntitySearchCriteria policyHolderSearch = new EntitySearchCriteria();
        policyHolderSearch.setEntityType(neo.manager.EntityType.COMPANY);
        policyHolderSearch.setContractType(neo.manager.ContractType.POLICY_HOLDER);
        java.util.List<neo.manager.CompanyDetails> policyHolderresult = NeoWebService.getNeoManagerBeanService().findCompaniesWithSearchCriteria(policyHolderSearch);
        int totalpolicyHoldersize = policyHolderresult.size();

        String policyHolders[] = new String[totalpolicyHoldersize];
        int phno = 0;
        for (CompanyDetails policyHolderDetails : policyHolderresult) {
            policyHolders[phno] = policyHolderDetails.getName();
            int policyHolderID = policyHolderDetails.getEntityId();
            policyHolderMap.put(policyHolders[phno], policyHolderID);
            phno++;


        }

        policyHolder.setModel(new javax.swing.DefaultComboBoxModel(policyHolders));




    } //Policy holder is Person
    else if (evt.getStateChange() == ItemEvent.SELECTED && policyType.getSelectedIndex() == 2) {

        neo.manager.EntitySearchCriteria policyHolderSearch = new EntitySearchCriteria();
        policyHolderSearch.setEntityType(EntityType.PERSON);
        policyHolderSearch.setContractType(neo.manager.ContractType.POLICY_HOLDER);
        java.util.List<neo.manager.PersonDetails> policyHolderresult = NeoWebService.getNeoManagerBeanService().findPersonsWithSearchCriteria(policyHolderSearch);
        int totalpolicyHoldersize = policyHolderresult.size();

        String policyHolders[] = new String[totalpolicyHoldersize];
        int phno = 0;
        for (PersonDetails policyHolderDetails : policyHolderresult) {
            policyHolders[phno] = policyHolderDetails.getName();
            int policyHolderID = policyHolderDetails.getEntityId();
            policyHolderMap.put(policyHolders[phno], policyHolderID);
            phno++;


        }

        policyHolder.setModel(new javax.swing.DefaultComboBoxModel(policyHolders));



    }

}//GEN-LAST:event_policyTypeItemStateChanged

private void brokerHouseItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_brokerHouseItemStateChanged
    // TODO add your handling code here:
    int BrokerHouseID = brokerhouseMap.get(brokerHouse.getSelectedItem());
    List<Integer> listIntegers = NeoWebService.getNeoManagerBeanService().getAllAgentsForBrokerHouse(BrokerHouseID);

    brokerName.removeAllItems();
    if (listIntegers.size() == 0) {
        neo.manager.EntitySearchCriteria brokerSearch = new EntitySearchCriteria();
        brokerSearch.setEntityType(neo.manager.EntityType.PERSON);
        brokerSearch.setContractType(neo.manager.ContractType.BROKER);
        java.util.List<neo.manager.PersonDetails> brokerresult = NeoWebService.getNeoManagerBeanService().findPersonsWithSearchCriteria(brokerSearch);
        int totalbrokersize = brokerresult.size();

        String brokers[] = new String[totalbrokersize];
        int brokerno = 0;

        for (PersonDetails brokerDetails : brokerresult) {
            int brokerID = brokerDetails.getEntityId();

            brokers[brokerno] = brokerDetails.getName();
            brokerMap.put(brokers[brokerno], brokerID);


            brokerno++;
        }
        brokerName.setModel(new javax.swing.DefaultComboBoxModel(brokers));
    } else {
        String[] brokers = new String[listIntegers.size()];

        int bro = 0;
        for (Integer brokerid : listIntegers) {
            brokerName.removeAllItems();
            PersonDetails edbrokers = NeoWebService.getNeoManagerBeanService().getPersonDetails(brokerid);
            brokers[bro] = edbrokers.getName();
            brokerMap.put(brokers[bro], brokerid);
            //addNewItemsToBrokerBox(brokerName, brokerNames);
            bro++;
        }
        brokerName.setModel(new javax.swing.DefaultComboBoxModel(brokers));

    }

    CompanyDetails companyDetails = NeoWebService.getNeoManagerBeanService().getCompanyDetails(BrokerHouseID);
    List<BrokerHouseProduct> brokerHouseProducts = companyDetails.getBrokerProducts();
    for (BrokerHouseProduct houseProduct : brokerHouseProducts) {
        if (houseProduct.getProductId() == productMap.get(productName.getSelectedItem())) {
            brokerCommision.setText(String.valueOf(houseProduct.getCommisionPercentage()));
        }

    }
}//GEN-LAST:event_brokerHouseItemStateChanged

private void uploadFileBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uploadFileBtnActionPerformed

    FileDialog fd = new FileDialog(new Frame(), "File Chooser`", FileDialog.LOAD);
    fd.setFile("*.xls");
    fd.setDirectory("\\");
    fd.setLocation(50, 50);
    fd.show();

    //File selectedFile = new File(fd.getFile());

    ReadIndividualDetails readIndividualDetails = new ReadIndividualDetails();
    readIndividualDetails.init(fd.getDirectory() + fd.getFile());
    vectImportData = readIndividualDetails.getValues();
    
}//GEN-LAST:event_uploadFileBtnActionPerformed

    public static void addNewItemToComboBox(AutoCompletionComboBox comboBox, String newItem) {

        comboBox.setSelectedItem(newItem);
    }

    public static void addNewItemsToComboBox(AutoCompletionComboBox comboBox, String newItem) {
        comboBox.addItem(newItem);
        comboBox.setSelectedItem(newItem);
    }

    public void addNewItemsToBrokerBox(ListComboBox comboBox, String newItem) {
        comboBox.addItem(newItem);
        comboBox.setSelectedItem(newItem);
    }

    public static void addNewItemToBrokerBox(ListComboBox comboBox, String newItem) {
        comboBox.addItem(newItem);
        comboBox.setSelectedItem(newItem);
    }

    public boolean savePolicy() {
        QuotationsActionFrame.policyNo = "";
        String previousInsurers = (String) Previousinsurer.getSelectedItem();
        String policyHolders1 = (String) policyHolder.getSelectedItem();
        Date startDate = policyStartDate.getDate();
        Date endDate = policyEndDate.getDate();
        String brokernames = (String) brokerName.getSelectedItem();
        String beokerhouse = (String) brokerHouse.getSelectedItem();
        String brokercommision = brokerCommision.getText();

        PolicyDetails polDetails = new PolicyDetails();

        polDetails.setBroker(brokerMap.get(brokernames));
        if (previousInsurers == null || previousInsurers.equals("")) {
            int entityid = NeoWebService.getNeoManagerBeanService().saveEntity(EntityType.COMPANY);
            CompanyDetails ed = new CompanyDetails();
            ed.setName(previousInsurers);
            ed.setStatus(StatusType.ACCEPT);
            ed.setReason(ReasonType.NEW_MEMBER);
            ed.setFromDate(convert(new Date()));

            NeoWebService.getNeoManagerBeanService().saveCompanyDetails(entityid, ed);

            polDetails.setPreviousInsurer(entityid);
            polDetails.setInsurer(entityid);
        } else {
            if (previousInsurerMap.get(previousInsurers) == null) {
                int entityid = NeoWebService.getNeoManagerBeanService().saveEntity(EntityType.COMPANY);
                CompanyDetails ed = new CompanyDetails();
                ed.setName(previousInsurers);
                ed.setStatus(StatusType.ACCEPT);
                ed.setReason(ReasonType.NEW_MEMBER);
                ed.setFromDate(convert(new Date()));

                NeoWebService.getNeoManagerBeanService().saveCompanyDetails(entityid, ed);

                polDetails.setPreviousInsurer(entityid);
                polDetails.setInsurer(entityid);
            } else {
                int preInsurerID = previousInsurerMap.get(previousInsurers);

                polDetails.setPreviousInsurer(preInsurerID);
                polDetails.setInsurer(previousInsurerMap.get(previousInsurers));
            }
        }
        polDetails.setEndDate(convert(endDate));
        polDetails.setStartDate(convert(startDate));
        polDetails.setBrokerHouse(brokerhouseMap.get(beokerhouse));

        polDetails.setBCommission((int) Double.parseDouble(brokercommision));
        polDetails.setPolicyPeriod((String) policyPeriod.getSelectedItem());
        int capture = insuredPersonCaptureFormat.getSelectedIndex();
        if (capture == 0) {
            polDetails.setCaptureFormat("CaptureDatails");
        }
        if (capture == 1) {
            polDetails.setCaptureFormat("ImportFile");
        }
        int jk = policyHolderMap.get(policyHolders1);
        polDetails.setPolicyHolder(policyHolderMap.get(policyHolders1));
        polDetails.setPolicyType((String) policyType.getSelectedItem());
        if (priorPolicyClaims.getText().equalsIgnoreCase("") || priorPolicyClaims.getText() == null) {
            polDetails.setPriorPolicyClaims("12019");
        } else {
            polDetails.setPriorPolicyClaims(priorPolicyClaims.getText());
        }
        String pname = (String) productName.getSelectedItem();
        polDetails.setProduct(productMap.get(pname));
        polDetails.setProductId(productMap.get(pname));

        polDetails.setPaymentType((PaymentType) paymentTypeCombo.getSelectedItem());
        policyNumber = NeoWebService.getNeoManagerBeanService().createNewPolicy(polDetails);
        QuotationsActionFrame.policyNo = policyNumber;
        if (policyNumber == null) {
            return false;
        } else {
            return true;
        }
    }

    public static void displayPolicyDetails(String policyNumber) {


        PolicyDetails pdetails = NeoWebService.getNeoManagerBeanService().getPolicy(policyNumber);
        String brokehousename = NeoWebService.getNeoManagerBeanService().getCompanyContract(pdetails.getBrokerHouse()).getEntityDetails().getName();
        brokerHouse.setSelectedItem(brokehousename);
        String brokename = NeoWebService.getNeoManagerBeanService().getPersonContract(pdetails.getBroker()).getEntityDetails().getName();
        brokerName.removeAllItems();
        brokerName.setSelectedItem(brokename);
        PolicyID.setText(policyNumber);
        PolicyID.setVisible(true);
        policyNo.setVisible(true);
        policyStartDate.setDate(pdetails.getStartDate().toGregorianCalendar().getTime());
        policyEndDate.setDate(pdetails.getEndDate().toGregorianCalendar().getTime());

        policyType.setSelectedItem(pdetails.getPolicyType());
        policyPeriod.setSelectedItem(pdetails.getPolicyPeriod());
        Product Productname = NeoWebService.getNeoManagerBeanService().findProductWithID(pdetails.getProduct());
        productName.setSelectedItem(Productname.getProductName());

        paymentTypeCombo.setSelectedItem(pdetails.getPaymentType());

        String policyholdname = null;
        EntityType entityType = NeoWebService.getNeoManagerBeanService().getEntityType(pdetails.getPolicyHolder());
        if (entityType.value().equalsIgnoreCase("COMPANY")) {
            policyholdname = NeoWebService.getNeoManagerBeanService().getCompanyDetails(pdetails.getPolicyHolder()).getName();
        }
        if (entityType.value().equalsIgnoreCase("PERSON")) {
            policyholdname = NeoWebService.getNeoManagerBeanService().getPersonDetails(pdetails.getPolicyHolder()).getName();
        }

        policyHolder.setSelectedItem(policyholdname);



        brokerCommision.setText(new Integer(pdetails.getBCommission()).toString());
        if (pdetails.getPriorPolicyClaims().equalsIgnoreCase("12019")) {
            priorPolicyClaims.setText("0");
        } else {
            priorPolicyClaims.setText(pdetails.getPriorPolicyClaims());
        }
        if (pdetails.getCaptureFormat().equalsIgnoreCase("CaptureDatails")) {
            insuredPersonCaptureFormat.setSelectedItem("Capture Individuals Details");
        }
        if (pdetails.getCaptureFormat().equalsIgnoreCase("ImportFile")) {
            insuredPersonCaptureFormat.setSelectedItem("Import Individuals Details From File");
        }
        String preinsurername = NeoWebService.getNeoManagerBeanService().getCompanyContract(pdetails.getPreviousInsurer()).getEntityDetails().getName();
        Previousinsurer.setSelectedItem(preinsurername);

    }

    public static Vector getImportData() {
        return vectImportData;
    }

    protected XMLGregorianCalendar convert(Date date) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getDatatypeFactory().newXMLGregorianCalendar(calendar);
    }

    private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                Logger.getLogger(CreateBroker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


        return _datatypeFactory;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTextField PolicyID;
    public static com.jidesoft.combobox.ListComboBox Previousinsurer;
    private javax.swing.JPanel bodyPanel;
    public static javax.swing.JTextField brokerCommision;
    public static com.jidesoft.swing.AutoCompletionComboBox brokerHouse;
    public static com.jidesoft.combobox.ListComboBox brokerName;
    private com.jidesoft.swing.JideButton btnAddBrokerHouseName;
    private com.jidesoft.swing.JideButton btnAddPolicyHolder;
    private com.jidesoft.swing.JideButton btnAddProductName;
    public static com.jidesoft.combobox.ListComboBox insuredPersonCaptureFormat;
    private javax.swing.JPanel jPanel1;
    private com.jidesoft.swing.JideButton nextButton;
    public static com.jidesoft.swing.AutoCompletionComboBox paymentTypeCombo;
    public static com.jidesoft.combobox.DateComboBox policyEndDate;
    public static com.jidesoft.swing.AutoCompletionComboBox policyHolder;
    public static com.jidesoft.swing.StyledLabel policyNo;
    public static com.jidesoft.combobox.ListComboBox policyPeriod;
    public static com.jidesoft.combobox.DateComboBox policyStartDate;
    public static com.jidesoft.combobox.ListComboBox policyType;
    public static javax.swing.JTextField priorPolicyClaims;
    public static com.jidesoft.swing.AutoCompletionComboBox productName;
    private com.jidesoft.swing.JideButton savePolicyDetails;
    private com.jidesoft.swing.StyledLabel styledLabel1;
    private com.jidesoft.swing.StyledLabel styledLabel10;
    private com.jidesoft.swing.StyledLabel styledLabel11;
    private com.jidesoft.swing.StyledLabel styledLabel12;
    private com.jidesoft.swing.StyledLabel styledLabel2;
    private com.jidesoft.swing.StyledLabel styledLabel24;
    private com.jidesoft.swing.StyledLabel styledLabel3;
    private com.jidesoft.swing.StyledLabel styledLabel4;
    private com.jidesoft.swing.StyledLabel styledLabel5;
    private com.jidesoft.swing.StyledLabel styledLabel6;
    private com.jidesoft.swing.StyledLabel styledLabel7;
    private com.jidesoft.swing.StyledLabel styledLabel8;
    private com.jidesoft.swing.StyledLabel styledLabel9;
    private com.jidesoft.swing.StyledLabel titleName;
    private javax.swing.JPanel titlePanel;
    private com.jidesoft.swing.JideButton uploadFileBtn;
    // End of variables declaration//GEN-END:variables
    private DatatypeFactory _datatypeFactory;
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * CreateCompany.java
 *
 * Created on 2009/05/05, 03:28:32
 */
package neo.entity.company;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.entity.person.CreatePerson;
import neo.manager.CommMethod;
import neo.manager.ContactDetails;
import neo.manager.ContactPreference;
import neo.manager.CompanyContract;
import neo.product.utils.NeoWebService;
import za.co.AgilityTechnologies.LeftMenu.EntityActionsFrame;
import za.co.AgilityTechnologies.TableEditors.CommunicationMethod;
import za.co.AgilityTechnologies.TableEditors.CommunicationType;

/**
 *
 * @author BharathP
 */
public class CreateCompanyContact extends javax.swing.JPanel {

    int entityid = 0;
    private DatatypeFactory _datatypeFactory;

    /** Creates new form CreateCompany */
    public CreateCompanyContact() {
        initComponents();
        companyContactdisplayFields();

    }

    protected XMLGregorianCalendar convert(Date date) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getDatatypeFactory().newXMLGregorianCalendar(calendar);
    }

    private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                Logger.getLogger(CreatePerson.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


        return _datatypeFactory;
    }

    static void companyContactdisplayFields() {

        CompanyContract companyContract = EntityActionsFrame.GLOBALCompanyContract;

        /*if (companyContract.getContactDetails() != null) {

        //if (companyContract.getContactDetails().getContactDetailsID()!=0) {



        ContactDetails conDetails = companyContract.getContactDetails();

        TelPhoneNumber.setText(conDetails.getWorkTelephoneNumber());
        faxNumber.setText(conDetails.getFaxTelephoneNumber());
        contactPersonName.setText(conDetails.getContactPersonName());
        contactPersonCellNumber.setText(conDetails.getCellphoneNumber());
        email.setText(conDetails.getEmail());

        dateFrom.setDate((java.util.Date) conDetails.getDateFrom().toGregorianCalendar().getTime());
        List<ContactPreference> listPreferences = conDetails.getContactPreferences();

        System.out.println(listPreferences.size() + " size of-----------------");

        entityID.setText(new Integer(EntityActionsFrame.GLOBALCompanyContract.getEntityDetails().getEntityId()).toString());

        companyName.setText(EntityActionsFrame.GLOBALCompanyContract.getEntityDetails().getName());


        int row = 0;

        for (ContactPreference contactprefer : listPreferences) {
        preferedCommunication.setValueAt(contactprefer.getCommType().value(), row, 1);

        System.out.println(contactprefer.getCommType().value() + "Type   ---------");

        preferedCommunication.setValueAt(contactprefer.getCommMethod().value(), row, 0);
        System.out.println(contactprefer.getCommMethod().value() + "method   ---------");


        preferedCommunication.setValueAt(contactprefer.getDescription(), row, 2);
        System.out.println(contactprefer.getDescription() + "desc   ---------");

        row++;


        }
        // }
        }
        entityID.setText(new Integer(EntityActionsFrame.GLOBALCompanyContract.getEntityDetails().getEntityId()).toString());

        companyName.setText(EntityActionsFrame.GLOBALCompanyContract.getEntityDetails().getName());*/

        }


    public boolean saveContactDetails() {

        neo.manager.ContactDetails contactdetatils = new neo.manager.ContactDetails();

        /*contactdetatils.setCellphoneNumber(contactPersonCellNumber.getText());
        contactdetatils.setFaxTelephoneNumber(faxNumber.getText());
        contactdetatils.setHomeTelephoneNumber(TelPhoneNumber.getText());
        contactdetatils.setWorkTelephoneNumber(TelPhoneNumber.getText());
        contactdetatils.setEmail(email.getText());
        contactdetatils.setContactPersonName(contactPersonName.getText());

        Date date1 = dateFrom.getDate();
        contactdetatils.setDateFrom(convert(date1));

        List<ContactPreference> listconprefer = contactdetatils.getContactPreferences();



        int rows = preferedCommunication.getRowCount();

        //preferedCommunication.selectAll();
        boolean firstRowDone = false;

        for (int i = 0; i < rows; i++) {

        if (preferedCommunication.getValueAt(i, 0) != null) {
        ContactPreference conprePreference = new ContactPreference();

        conprePreference.setCommType((CommType) preferedCommunication.getValueAt(i, 0));

        conprePreference.setCommMethod((CommMethod) preferedCommunication.getValueAt(i, 1));

        String tempString3 = (String) preferedCommunication.getValueAt(i, 2);
        conprePreference.setDescription(tempString3);

        listconprefer.add(conprePreference);

        firstRowDone = true;
        } else {
        break;
        }
        }
        int entitynumber = EntityActionsFrame.GLOBALCompanyContract.getEntityId();


        boolean results = NeoWebService.getNeoManagerBeanService().saveContactDetails(entitynumber, contactdetatils);

        if (results) {
        EntityActionsFrame.GLOBALCompanyContract.setContactDetails(contactdetatils);
        EntityActionsFrame.GLOBALCompanyContract.setEntityId(entitynumber);
        }
        return results;*/
        return false;
    }

    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bodyPanel = new javax.swing.JPanel();
        styledLabel1 = new com.jidesoft.swing.StyledLabel();
        entityID = new javax.swing.JTextField();
        styledLabel2 = new com.jidesoft.swing.StyledLabel();
        companyName = new javax.swing.JTextField();
        styledLabel3 = new com.jidesoft.swing.StyledLabel();
        TelPhoneNumber = new javax.swing.JTextField();
        styledLabel4 = new com.jidesoft.swing.StyledLabel();
        faxNumber = new javax.swing.JTextField();
        styledLabel5 = new com.jidesoft.swing.StyledLabel();
        contactPersonName = new javax.swing.JTextField();
        styledLabel7 = new com.jidesoft.swing.StyledLabel();
        contactPersonCellNumber = new javax.swing.JTextField();
        styledLabel9 = new com.jidesoft.swing.StyledLabel();
        dateFrom = new com.jidesoft.combobox.DateComboBox();
        styledLabel10 = new com.jidesoft.swing.StyledLabel();
        suppressMail = new com.jidesoft.combobox.ListComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        preferedCommunication = new com.jidesoft.grid.JideTable();
        nextButton = new com.jidesoft.swing.JideButton();
        jPanel1 = new javax.swing.JPanel();
        styledLabel8 = new com.jidesoft.swing.StyledLabel();
        email = new javax.swing.JTextField();
        styledLabel11 = new com.jidesoft.swing.StyledLabel();
        dateComboBox1 = new com.jidesoft.combobox.DateComboBox();
        jPanel2 = new javax.swing.JPanel();
        styledLabel12 = new com.jidesoft.swing.StyledLabel();
        jPanel3 = new javax.swing.JPanel();
        styledLabel13 = new com.jidesoft.swing.StyledLabel();
        saveContactDetails = new com.jidesoft.swing.JideButton();
        backButton = new com.jidesoft.swing.JideButton();

        setBackground(new java.awt.Color(235, 235, 235));
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(143, 196, 200)));

        bodyPanel.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel1.setText("Contract Number");
        styledLabel1.setFont(new java.awt.Font("Arial", 0, 11));

        entityID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                entityIDActionPerformed(evt);
            }
        });

        styledLabel2.setText("Company Name");

        styledLabel3.setText("Company Telephone Number");

        TelPhoneNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TelPhoneNumberActionPerformed(evt);
            }
        });

        styledLabel4.setText("Fax Number");

        styledLabel5.setText("Contact Person Name");

        styledLabel7.setText("Contact Person Cell Number");

        styledLabel9.setForeground(new java.awt.Color(255, 51, 51));
        styledLabel9.setText("Date From");

        styledLabel10.setText("Suppress Mail");

        suppressMail.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "NO", "YES" }));

        javax.swing.GroupLayout bodyPanelLayout = new javax.swing.GroupLayout(bodyPanel);
        bodyPanel.setLayout(bodyPanelLayout);
        bodyPanelLayout.setHorizontalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addGap(126, 126, 126)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(bodyPanelLayout.createSequentialGroup()
                                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(styledLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(styledLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(styledLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)))
                                        .addGap(25, 25, 25))
                                    .addGroup(bodyPanelLayout.createSequentialGroup()
                                        .addComponent(styledLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(105, 105, 105)))
                                .addGroup(bodyPanelLayout.createSequentialGroup()
                                    .addComponent(styledLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(59, 59, 59)))
                            .addGroup(bodyPanelLayout.createSequentialGroup()
                                .addComponent(styledLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29)))
                        .addGroup(bodyPanelLayout.createSequentialGroup()
                            .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(113, 113, 113)))
                    .addGroup(bodyPanelLayout.createSequentialGroup()
                        .addComponent(styledLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(98, 98, 98)))
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(suppressMail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(dateFrom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(contactPersonCellNumber)
                    .addComponent(contactPersonName)
                    .addComponent(faxNumber)
                    .addComponent(TelPhoneNumber)
                    .addComponent(companyName)
                    .addComponent(entityID, javax.swing.GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE))
                .addContainerGap())
        );
        bodyPanelLayout.setVerticalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(entityID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(companyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TelPhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(faxNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(contactPersonName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(contactPersonCellNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dateFrom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(suppressMail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        preferedCommunication.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Communication Type", "Method", "Method Detail"
            }
        ));
        preferedCommunication.setRowResizable(true);
        preferedCommunication.setSelectionBackground(new java.awt.Color(138, 177, 230));
        jScrollPane1.setViewportView(preferedCommunication);
        preferedCommunication.getColumnModel().getColumn(0).setCellEditor(new CommunicationType());
        preferedCommunication.getColumnModel().getColumn(1).setCellEditor(new CommunicationMethod());

        nextButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/arrow2_right_green.png"))); // NOI18N
        nextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextButtonActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel8.setText("Email Address");

        styledLabel11.setText("Date From Suppress Mail");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(49, 49, 49)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(dateComboBox1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(email, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE))
                .addContainerGap(60, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(231, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(76, 76, 76)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dateComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38))
        );

        jPanel2.setBackground(new java.awt.Color(138, 177, 230));

        styledLabel12.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel12.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel12.setText("CONTACT DETAILS");
        styledLabel12.setFont(new java.awt.Font("Arial", 1, 13));
        jPanel2.add(styledLabel12);

        jPanel3.setBackground(new java.awt.Color(138, 177, 230));

        styledLabel13.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel13.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel13.setText("PREFFERED COMMUNICATION");
        styledLabel13.setFont(new java.awt.Font("Arial", 1, 12));
        jPanel3.add(styledLabel13);

        saveContactDetails.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        saveContactDetails.setText("Save");
        saveContactDetails.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveContactDetailsActionPerformed(evt);
            }
        });

        backButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/arrow2_left_blue.png"))); // NOI18N
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 969, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 955, Short.MAX_VALUE))
                .addContainerGap(24, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(801, 801, 801)
                .addComponent(backButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(nextButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(118, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(saveContactDetails, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(858, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(saveContactDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nextButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(backButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(45, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void TelPhoneNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TelPhoneNumberActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_TelPhoneNumberActionPerformed

    private void nextButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextButtonActionPerformed
        CompanyContract entityContract = EntityActionsFrame.GLOBALCompanyContract;

        if (entityContract.getContactDetails() == null) {

            if (saveContactDetails()) {
                JOptionPane.showMessageDialog(null, "Contact Details successfully created", "Information", JOptionPane.INFORMATION_MESSAGE);
                EntityActionsFrame.entityActionList.setSelectedIndex(3);
                // CreateCompanyBankingDetails.entityID.setText(entityID.getText());
                CreateCompanyBankingDetails.companyBankingDisplayFields();


            } else {
                JOptionPane.showMessageDialog(null, "Error in Create Contact Details", "Error", JOptionPane.ERROR_MESSAGE);

            }
        } else {

            EntityActionsFrame.entityActionList.setSelectedIndex(3);
            CreateCompanyBankingDetails.entityID.setText(entityID.getText());
            CreateCompanyBankingDetails.companyBankingDisplayFields();

        }
    }//GEN-LAST:event_nextButtonActionPerformed

    private void entityIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_entityIDActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_entityIDActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        // TODO add your handling code here:
        EntityActionsFrame.entityActionList.setSelectedIndex(1);
        if (entityID.getText() != null) {
            String eID = entityID.getText();
            CreateCompanyAddressDetails.companyAddressDisplayFields();

        }
}//GEN-LAST:event_backButtonActionPerformed

    private void saveContactDetailsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveContactDetailsActionPerformed
       

        CompanyContract entityContract = EntityActionsFrame.GLOBALCompanyContract;

        /*if (entityContract.getContactDetails() == null || entityContract.getContactDetails().getContactDetailsID()==0) {
        if (saveContactDetails()) {
        JOptionPane.showMessageDialog(null, "Contact Details successfully created", "Information", JOptionPane.INFORMATION_MESSAGE);

        EntityActionsFrame.CreatePersonsDetails.setSelectedIndex(3);
        CreateCompanyBankingDetails.companyBankingDisplayFields();


        } else {
        JOptionPane.showMessageDialog(null, "Error in Creating Contact Details", "Error", JOptionPane.ERROR_MESSAGE);
        }
        } else {
        if (saveContactDetails()) {
        JOptionPane.showMessageDialog(null, "Contact Details successfully updated", "Information", JOptionPane.INFORMATION_MESSAGE);

        //  EntityActionsFrame.CreatePersonsDetails.setSelectedIndex(3);
        //  CreatePersonBankingDetails.personBankingDisplayFields(entityNo);
        } else {
        JOptionPane.showMessageDialog(null, "Error in Updating Contact Details", "Error", JOptionPane.ERROR_MESSAGE);
        }
        }*/
}//GEN-LAST:event_saveContactDetailsActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTextField TelPhoneNumber;
    private com.jidesoft.swing.JideButton backButton;
    private javax.swing.JPanel bodyPanel;
    public static javax.swing.JTextField companyName;
    public static javax.swing.JTextField contactPersonCellNumber;
    public static javax.swing.JTextField contactPersonName;
    public static com.jidesoft.combobox.DateComboBox dateComboBox1;
    public static com.jidesoft.combobox.DateComboBox dateFrom;
    public static javax.swing.JTextField email;
    public static javax.swing.JTextField entityID;
    public static javax.swing.JTextField faxNumber;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private com.jidesoft.swing.JideButton nextButton;
    public static com.jidesoft.grid.JideTable preferedCommunication;
    private com.jidesoft.swing.JideButton saveContactDetails;
    private com.jidesoft.swing.StyledLabel styledLabel1;
    private com.jidesoft.swing.StyledLabel styledLabel10;
    private com.jidesoft.swing.StyledLabel styledLabel11;
    private com.jidesoft.swing.StyledLabel styledLabel12;
    private com.jidesoft.swing.StyledLabel styledLabel13;
    private com.jidesoft.swing.StyledLabel styledLabel2;
    private com.jidesoft.swing.StyledLabel styledLabel3;
    private com.jidesoft.swing.StyledLabel styledLabel4;
    private com.jidesoft.swing.StyledLabel styledLabel5;
    private com.jidesoft.swing.StyledLabel styledLabel7;
    private com.jidesoft.swing.StyledLabel styledLabel8;
    private com.jidesoft.swing.StyledLabel styledLabel9;
    public static com.jidesoft.combobox.ListComboBox suppressMail;
    // End of variables declaration//GEN-END:variables
}

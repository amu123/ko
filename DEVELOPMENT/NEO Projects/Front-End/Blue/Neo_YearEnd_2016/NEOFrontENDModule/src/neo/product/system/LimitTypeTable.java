/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neo.product.system;

/**
 *
 * @author Abigail
 */

  import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;


public class LimitTypeTable
extends AbstractCellEditor
implements TableCellEditor {

private JComboBox cbComponent;

public LimitTypeTable() {
   //neo.manager.LimitType[] status = neo.manager.LimitType.values();

    //cbComponent = new JComboBox(status );
}

public Component getTableCellEditorComponent(JTable table,
Object value, boolean isSelected, int row, int column) {
return cbComponent;
}

public Object getCellEditorValue() {
return cbComponent.getSelectedItem();
}


}


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.product.system;

/**
 *
 * @author AbigailM
 */
import java.awt.Component;



import javax.swing.AbstractCellEditor;

import javax.swing.JComboBox;

import javax.swing.JTable;

import javax.swing.table.TableCellEditor;
//import neo.manager.SystemParameterType;

public class SystemParameterTypeTable extends AbstractCellEditor implements TableCellEditor {

    private JComboBox cbComponent;

    public SystemParameterTypeTable() {

      
      // cbComponent = new JComboBox(SystemParameterType.values());

    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {

        return cbComponent;

    }

    public Object getCellEditorValue() {

        return cbComponent.getSelectedItem();

    }
}



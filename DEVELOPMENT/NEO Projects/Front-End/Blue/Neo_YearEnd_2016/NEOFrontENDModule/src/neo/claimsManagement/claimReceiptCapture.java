/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * claimReceiptCapture.java
 *
 * Created on 2009/11/26, 01:00:41
 */
package neo.claimsManagement;

import com.jidesoft.converter.DateConverter;
import java.awt.Color;
import java.awt.Component;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import neo.manager.ClaimReceipt;
import neo.util.dateConverter;

/**
 *
 * @author deonp
 */
public class claimReceiptCapture extends javax.swing.JFrame {

    /** Creates new form claimReceiptCapture */
    public claimReceiptCapture() {
        initComponents();
        tbl_receipt.setDefaultRenderer(Object.class, new MyTableCellRender());
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        styledLabel7 = new com.jidesoft.swing.StyledLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_receipt = new javax.swing.JTable();
        btn_save = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel3.setBackground(new java.awt.Color(138, 177, 230));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel7.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel7.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel7.setText("Receipt Capturing");
        styledLabel7.setFont(new java.awt.Font("Arial", 1, 14));
        jPanel3.add(styledLabel7);

        tbl_receipt.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "Service Date", "Receipt Amount"
            }
        ));
        jScrollPane1.setViewportView(tbl_receipt);
        tbl_receipt.getColumnModel().getColumn(0).setCellEditor(new claimReceiptDateTableEditor());

        btn_save.setText("Save");
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });

        jButton1.setText("Close");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 408, Short.MAX_VALUE)
                .addGap(10, 10, 10))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(296, Short.MAX_VALUE)
                .addComponent(btn_save)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_save)
                    .addComponent(jButton1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-444)/2, (screenSize.height-236)/2, 444, 236);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
 try {
        Collection<ClaimReceipt> claimreceiptCollection = new Vector();
        int rowCount = tbl_receipt.getRowCount()-1;
        for (int i = 0; i < rowCount; i++) {
            ClaimReceipt cr = new ClaimReceipt();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            String date = tbl_receipt.getValueAt(i, 0).toString();
            Date d = null;
            try {
                d = sdf.parse(date);
            } catch (ParseException ex) {
                Logger.getLogger(claimReceiptCapture.class.getName()).log(Level.SEVERE, null, ex);
            }
            cr.setReceiptDate(new dateConverter().convertDateXML(d));
            cr.setReceiptAmount(new Double(tbl_receipt.getValueAt(i, 1) + ""));
            claimreceiptCollection.add(cr);
        }
        ClaimHeaderCapturing.claimReceipt = claimreceiptCollection;
        ClaimHeaderCapturing.cb_capturedReceipt.setSelected(true);
        System.out.println("collection size: " + claimreceiptCollection.size());
 } catch (Exception e) {
     JOptionPane.showConfirmDialog(this, "Please tab out!", "Null Error", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
 }
        
        this.dispose();
    }//GEN-LAST:event_btn_saveActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new claimReceiptCapture().setVisible(true);
            }
        });
    }

    class MyTableCellRender extends DefaultTableCellRenderer {

        public MyTableCellRender() {
            setOpaque(true);
        }

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

            int lastrow = table.getRowCount() - 1;
            int secondLast = table.getRowCount() - 2;
            String ndcValueLast = (String) table.getValueAt(lastrow, 1);

            DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
            if (ndcValueLast != null) {
                if (!ndcValueLast.equals("")) {
                    tableModel.addRow(new Object[]{});
                }
            }

            if (lastrow >= 1) {
                String ndcValueSecLast = (String) table.getValueAt(secondLast, 1) + "";
                if (ndcValueSecLast.equals("")) {
                    tableModel.removeRow(lastrow);
                }
            }

            Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if (row == table.getSelectedRow() && column == table.getSelectedColumn()) {
                c.setBackground(new java.awt.Color(150, 190, 255));
                return c;
            }

            if (isSelected == true) {
                setBackground(new java.awt.Color(138, 177, 230));
            } else {
                setBackground(Color.white);
            }
            setText(value != null ? value.toString() : "");

            return this;
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_save;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private com.jidesoft.swing.StyledLabel styledLabel7;
    private javax.swing.JTable tbl_receipt;
    // End of variables declaration//GEN-END:variables
}

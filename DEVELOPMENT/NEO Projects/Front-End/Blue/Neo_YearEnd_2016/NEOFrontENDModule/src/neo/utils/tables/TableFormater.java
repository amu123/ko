/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.utils.tables;

import com.jidesoft.grid.JideTable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author BharathP
 */
public class TableFormater {

    private static DatatypeFactory _datatypeFactory;

    public static XMLGregorianCalendar formatDateFromTable(String cellDateValue) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date effectFrom = null;
        XMLGregorianCalendar returnDate = null;

        try {
            effectFrom = dateFormat.parse(cellDateValue);

        } catch (ParseException ex) {
            Logger.getLogger(TableFormater.class.getName()).log(Level.SEVERE, null, ex);
        }

        returnDate = convert(effectFrom);

        return returnDate;
    }

    public static XMLGregorianCalendar convert(Date date) {
        try {
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            return getDatatypeFactory().newXMLGregorianCalendar(calendar);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(TableFormater.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static DatatypeFactory getDatatypeFactory() throws DatatypeConfigurationException {
        if (_datatypeFactory == null) {
            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
            }
        }


        return _datatypeFactory;
    }

     public static void addTableResultsRows(JideTable table,String[] columns,int reSize) {

        int reinTableRows = table.getRowCount();

        if (reinTableRows != 0) {
            //removing all rows
            table.setModel(new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    columns));
            //adding rows according to request
            DefaultTableModel displayTableModel = (DefaultTableModel) table.getModel();
            for (int k = 0; k < reSize; k++) {
                displayTableModel.addRow(new Object[]{});
            }
        } else {
            //adding rows according to request
            DefaultTableModel displayTableModel = (DefaultTableModel) table.getModel();
            for (int k = 0; k < reSize; k++) {
                displayTableModel.addRow(new Object[]{});
            }
        }
    }
}

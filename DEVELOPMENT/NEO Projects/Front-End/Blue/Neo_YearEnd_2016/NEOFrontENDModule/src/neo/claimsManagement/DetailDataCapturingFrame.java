/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * DetailDataCapturingFrame.java
 *
 * Created on 2009/06/25, 01:17:37
 */
package neo.claimsManagement;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import neo.manager.ClaimCptCodes;
import neo.manager.ClaimIcd10;
import neo.manager.ClaimLine;
import neo.manager.CoverDetails;
import neo.manager.CoverProductDetails;
import neo.manager.Diagnosis;
import neo.manager.Eye;
import neo.manager.Modifier;
import neo.manager.Ndc;
import neo.manager.PersonDetails;
import neo.manager.PreAuthSearchAuthNumber;
import neo.manager.Procedure;
import neo.manager.Security;
import neo.manager.SecurityResponsibility;

import neo.manager.TariffCode;
import neo.manager.Treatment;
import neo.product.utils.NeoWebService;
import neo.util.dateConverter;
import za.co.AgilityTechnologies.LoginPage;

/**
 *
 * @author DeonP
 */
public class DetailDataCapturingFrame extends javax.swing.JFrame {

    Date today = null;
    int claimID;
    ClaimHeaderCapturing ch;
    int row = 0;
    String selectedCatagory = "GP";
    public static Collection<Modifier> CapModifiers = new Vector();
    public static Collection<Ndc> CapNDCMixtures = new Vector();
    String practiceType = "0";
    public static String dependantCode = "0";
    String coverNumber;
    String providerNumber;
    Date serviceDate = null;
    private NDCcodes ndc;
    public static HashMap<String, DefaultTableModel> models = new HashMap<String, DefaultTableModel>();

    public NDCcodes getNDCcodes() {
        if (ndc == null) {
            ndc = new NDCcodes();
        }
        return ndc;
    }

    /** Creates new form DetailDataCapturingFrame */
    public DetailDataCapturingFrame() {
        initComponents();
        ndc = new NDCcodes();
    }

    public DetailDataCapturingFrame(String claimType, int claimId, ClaimHeaderCapturing ch, int row, int principalMember, String coverNumber, String providerNumber, String practiceType) {
        initComponents();
       
        models.put("GP", new NDCcodes.NDCTableModel());
        models.put("Optical", new NDCcodes.NDCTableModel());
        models.put("Dental", new NDCcodes.NDCTableModel());
        models.put("Specialists", new NDCcodes.NDCTableModel());
        models.put("Hospitals", new NDCcodes.NDCHospitalModel());
        models.put("Pharmacy", new NDCcodes.NDCTableModel());
        models.put("Auxiliaries", new NDCcodes.NDCTableModel());
        models.put("Dental Laboratory", new NDCcodes.NDCTableModel());
        models.put("Specialists Laboratory", new NDCcodes.NDCTableModel());
        models.put("Multiple Discipline", new NDCcodes.NDCTableModel());
        models.put("Overseas", new NDCcodes.NDCTableModel());

        selectedCatagory = claimType;
        styledLabel46.setVisible(false);
        remarkCode.setVisible(false);
        styledLabel47.setVisible(false);
        remarkcodeDescription.setVisible(false);
        styledLabel48.setVisible(false);
        reversalCode.setVisible(false);
        styledLabel49.setVisible(false);
        reversalDescription.setVisible(false);
        this.practiceType = practiceType;
        this.coverNumber = coverNumber;
        this.providerNumber = providerNumber;
        getToday();
        //todo replace with real tarrifs

        System.out.println("claimLine: " + principalMember);

        Collection<CoverDetails> cov = NeoWebService.getNeoManagerBeanService().getResignedMemberByCoverNumber(coverNumber);
        HashMap<Integer, PersonDetails> covMap = new HashMap<Integer, PersonDetails>();
        //getReCoverDetailsByCoverNumber(coverNumber);
        insuredRelation.removeAllItems();
        for (Iterator<CoverDetails> it = cov.iterator(); it.hasNext();) {
            CoverDetails cover = it.next();
            //System.out.println("End date: " + cover.getEffectiveEndDate().toGregorianCalendar().getTime());
            // System.out.println("today:" + today);
            int entityID = cover.getEntityId();
            PersonDetails p = NeoWebService.getNeoManagerBeanService().getPersonDetailsByEntityId(entityID);
            boolean insert = false;

            if (covMap.isEmpty()) {
                covMap.put(entityID, p);
                insert = true;
            } else if (!covMap.containsKey(entityID)) {
                covMap.put(entityID, p);
                insert = true;
            }
            //System.out.println(cover.getDependentNumber());
            //System.out.println(cover.getEntityCommonId());


            //System.out.println(p.getName());
            //PersonDetails p = NeoWebService.getNeoManagerBeanService().getPersonDetails(cover.getInsured());
            if (insert) {
                ComboPopulatorDependant cc = new ComboPopulatorDependant(cover.getDependentNumber(), p.getName(), cover.getDependentType(), entityID, new dateConverter().convertToYYYMMD(p.getDateOfBirth().toGregorianCalendar().getTime()));
                insuredRelation.addItem(cc);
                insuredRelation.setSelectedItem(cc);
                insuredPerson.setText(entityID + "");
                System.out.println("getDependentNumber" + cover.getDependentNumber());
                dependantCode = cover.getDependentNumber() + "";
            }

        }
        ComboPopulatorDependant cc = new ComboPopulatorDependant(99, "UNKNOWN", "UNKNOWN RELATION", 0, "UNKNOWN");
        insuredRelation.addItem(cc);

        insuredRelation.setSelectedIndex(0);
        ComboPopulatorDependant c = (ComboPopulatorDependant) insuredRelation.getSelectedItem();
        insuredPerson.setText("" + c.entityId);
        dependantCode = c.dependentCode;
        //insuredPerson.setText("" + principalMember);
        insuredPerson.setEditable(false);


        this.claimID = claimId;
        this.ch = ch;
        this.row = row;

        Multiplier.setText("1");
        Units.setText("1");

        CapModifiers = new Vector();
        cb_modifiersCaptured.setSelected(false);
        CapNDCMixtures = new Vector();
        cb_mixtureCaptured.setSelected(false);

        setScreen(selectedCatagory);
        setRelationCombo(0);

        /*  lensCylindricalPower.setVisible(false);
        lensDiameter.setVisible(false);
        lensRecurring.setVisible(false);
        lensSphericalPower.setVisible(false);
        visualAuite.setVisible(false);
        visualFeilds.setVisible(false);
        IntraOcularPressure.setVisible(false);

        lensCylindricalPower1.setVisible(false);
        lensDiameter1.setVisible(false);
        lensRecurring1.setVisible(false);
        lensSphericalPower1.setVisible(false);
        visualAuite1.setVisible(false);
        visualFeilds1.setVisible(false);
        IntraOcularPressure1.setVisible(false);

        styledLabel36.setVisible(false);
        styledLabel37.setVisible(false);
        styledLabel38.setVisible(false);
        styledLabel39.setVisible(false);
        styledLabel40.setVisible(false);
        styledLabel41.setVisible(false);
        styledLabel42.setVisible(false);*/

        PanelEyeToRemove.setVisible(false);

    }

    public DetailDataCapturingFrame(String claimType, int claimId, ClaimHeaderCapturing ch, int row, int principalMember, String coverNumber, String providerNumber, String practiceType, int dependantRelation) {
        initComponents();

        models.put("GP", new NDCcodes.NDCTableModel());
        models.put("Optical", new NDCcodes.NDCTableModel());
        models.put("Dental", new NDCcodes.NDCTableModel());
        models.put("Specialists", new NDCcodes.NDCTableModel());
        models.put("Hospitals", new NDCcodes.NDCHospitalModel());
        models.put("Pharmacy", new NDCcodes.NDCTableModel());
        models.put("Auxiliaries", new NDCcodes.NDCTableModel());
        models.put("Dental Laboratory", new NDCcodes.NDCTableModel());
        models.put("Specialists Laboratory", new NDCcodes.NDCTableModel());
        models.put("Multiple Discipline", new NDCcodes.NDCTableModel());
        models.put("Overseas", new NDCcodes.NDCTableModel());

        selectedCatagory = claimType;
        styledLabel46.setVisible(false);
        remarkCode.setVisible(false);
        styledLabel47.setVisible(false);
        remarkcodeDescription.setVisible(false);
        styledLabel48.setVisible(false);
        reversalCode.setVisible(false);
        styledLabel49.setVisible(false);
        reversalDescription.setVisible(false);
        this.practiceType = practiceType;
        this.coverNumber = coverNumber;
        this.providerNumber = providerNumber;
        getToday();
        //todo replace with real tarrifs

        System.out.println("claimLine: " + principalMember);

        Collection<CoverDetails> cov = NeoWebService.getNeoManagerBeanService().getResignedMemberByCoverNumber(coverNumber);

        //getReCoverDetailsByCoverNumber(coverNumber);
        insuredRelation.removeAllItems();
        for (Iterator<CoverDetails> it = cov.iterator(); it.hasNext();) {
            CoverDetails cover = it.next();
            //System.out.println("End date: " + cover.getEffectiveEndDate().toGregorianCalendar().getTime());
            // System.out.println("today:" + today);
            if (cover.getEffectiveEndDate().toGregorianCalendar().getTime().before(today)) {
                System.out.println("Cover expired");
            } else {
                //System.out.println(cover.getDependentNumber());
                //System.out.println(cover.getEntityCommonId());
                int entityID = cover.getEntityId();
                PersonDetails p = NeoWebService.getNeoManagerBeanService().getPersonDetailsByEntityId(entityID);
                //System.out.println(p.getName());
                //PersonDetails p = NeoWebService.getNeoManagerBeanService().getPersonDetails(cover.getInsured());
                ComboPopulatorDependant cc = new ComboPopulatorDependant(cover.getDependentNumber(), p.getName(), cover.getDependentType(), entityID, new dateConverter().convertToYYYMMD(p.getDateOfBirth().toGregorianCalendar().getTime()));
                insuredRelation.addItem(cc);
                insuredRelation.setSelectedItem(cc);
                insuredPerson.setText(entityID + "");
                System.out.println("getDependentNumber" + cover.getDependentNumber());
                dependantCode = cover.getDependentNumber() + "";
            }
        }
        ComboPopulatorDependant cc = new ComboPopulatorDependant(99, "UNKNOWN", "UNKNOWN RELATION", 0, "UNKNOWN");
        insuredRelation.addItem(cc);

        insuredRelation.setSelectedIndex(0);
        ComboPopulatorDependant c = (ComboPopulatorDependant) insuredRelation.getSelectedItem();
        insuredPerson.setText("" + c.entityId);
        dependantCode = c.dependentCode;
        //insuredPerson.setText("" + principalMember);
        insuredPerson.setEditable(false);


        this.claimID = claimId;
        this.ch = ch;
        this.row = row;

        Multiplier.setText("1");
        Units.setText("1");

        CapModifiers = new Vector();
        cb_modifiersCaptured.setSelected(false);
        CapNDCMixtures = new Vector();
        cb_mixtureCaptured.setSelected(false);

        setScreen(selectedCatagory);
        setRelationCombo(dependantRelation);

        PanelEyeToRemove.setVisible(false);

    }

    /*
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        Panel1 = new javax.swing.JPanel();
        styledLabel1 = new com.jidesoft.swing.StyledLabel();
        insuredPerson = new javax.swing.JTextField();
        styledLabel2 = new com.jidesoft.swing.StyledLabel();
        insuredRelation = new com.jidesoft.swing.AutoCompletionComboBox();
        Panel2 = new javax.swing.JPanel();
        LabInvoiceNumber = new javax.swing.JTextField();
        styledLabel5 = new com.jidesoft.swing.StyledLabel();
        styledLabel6 = new com.jidesoft.swing.StyledLabel();
        LabOrderNumber = new javax.swing.JTextField();
        Panel3 = new javax.swing.JPanel();
        styledLabel8 = new com.jidesoft.swing.StyledLabel();
        styledLabel9 = new com.jidesoft.swing.StyledLabel();
        ServiceDateFrom = new com.jidesoft.combobox.DateComboBox();
        ServiceDateTo = new com.jidesoft.combobox.DateComboBox();
        Panel4 = new javax.swing.JPanel();
        styledLabel10 = new com.jidesoft.swing.StyledLabel();
        styledLabel11 = new com.jidesoft.swing.StyledLabel();
        TimeIn = new javax.swing.JTextField();
        TimeOut = new javax.swing.JTextField();
        Panel5 = new javax.swing.JPanel();
        Diagnoses = new javax.swing.JTextField();
        styledLabel12 = new com.jidesoft.swing.StyledLabel();
        DiagnosesDescription = new javax.swing.JTextField();
        refDiagnoses = new javax.swing.JTextField();
        styledLabel13 = new com.jidesoft.swing.StyledLabel();
        styledLabel14 = new com.jidesoft.swing.StyledLabel();
        DiagnosesCombo = new javax.swing.JComboBox();
        refDiagnosesCombo = new javax.swing.JComboBox();
        refDiagnosesDescription = new javax.swing.JTextField();
        styledLabel15 = new com.jidesoft.swing.StyledLabel();
        Panel7 = new javax.swing.JPanel();
        styledLabel16 = new com.jidesoft.swing.StyledLabel();
        CTPCodes = new javax.swing.JTextField();
        styledLabel17 = new com.jidesoft.swing.StyledLabel();
        CTPCodesDescription = new javax.swing.JTextField();
        CPTCombo = new javax.swing.JComboBox();
        Panel8 = new javax.swing.JPanel();
        styledLabel18 = new com.jidesoft.swing.StyledLabel();
        Tariff = new javax.swing.JTextField();
        styledLabel19 = new com.jidesoft.swing.StyledLabel();
        TariffDescription = new javax.swing.JTextField();
        Multiplier = new javax.swing.JTextField();
        Units = new javax.swing.JTextField();
        styledLabel20 = new com.jidesoft.swing.StyledLabel();
        styledLabel21 = new com.jidesoft.swing.StyledLabel();
        styledLabel22 = new com.jidesoft.swing.StyledLabel();
        btn_captureModifiers = new javax.swing.JButton();
        cb_modifiersCaptured = new javax.swing.JCheckBox();
        tf_quantity = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        tf_authNumber = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        Panel9 = new javax.swing.JPanel();
        styledLabel27 = new com.jidesoft.swing.StyledLabel();
        jButton4 = new javax.swing.JButton();
        cb_mixtureCaptured = new javax.swing.JCheckBox();
        Panel10 = new javax.swing.JPanel();
        toothNumber = new javax.swing.JTextField();
        styledLabel33 = new com.jidesoft.swing.StyledLabel();
        toothCombo = new javax.swing.JComboBox();
        Panel11 = new javax.swing.JPanel();
        lensesRx = new javax.swing.JTextField();
        styledLabel35 = new com.jidesoft.swing.StyledLabel();
        lensesRx1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        Panel12 = new javax.swing.JPanel();
        claimedAmount = new javax.swing.JTextField();
        styledLabel45 = new com.jidesoft.swing.StyledLabel();
        btn_save = new javax.swing.JButton();
        btn_close = new javax.swing.JButton();
        Panel13 = new javax.swing.JPanel();
        styledLabel46 = new com.jidesoft.swing.StyledLabel();
        styledLabel47 = new com.jidesoft.swing.StyledLabel();
        styledLabel48 = new com.jidesoft.swing.StyledLabel();
        styledLabel49 = new com.jidesoft.swing.StyledLabel();
        remarkCode = new javax.swing.JTextField();
        remarkcodeDescription = new javax.swing.JTextField();
        reversalCode = new javax.swing.JTextField();
        reversalDescription = new javax.swing.JTextField();
        Panel9_cb = new javax.swing.JPanel();
        cb_ndcCodes = new javax.swing.JCheckBox();
        jPanel4 = new javax.swing.JPanel();
        cb_patientDetails = new javax.swing.JCheckBox();
        weight = new javax.swing.JTextField();
        styledLabel44 = new com.jidesoft.swing.StyledLabel();
        styledLabel43 = new com.jidesoft.swing.StyledLabel();
        height = new javax.swing.JTextField();
        PanelEyeToRemove = new javax.swing.JPanel();
        styledLabel37 = new com.jidesoft.swing.StyledLabel();
        styledLabel36 = new com.jidesoft.swing.StyledLabel();
        styledLabel42 = new com.jidesoft.swing.StyledLabel();
        styledLabel41 = new com.jidesoft.swing.StyledLabel();
        styledLabel40 = new com.jidesoft.swing.StyledLabel();
        styledLabel39 = new com.jidesoft.swing.StyledLabel();
        styledLabel38 = new com.jidesoft.swing.StyledLabel();
        lensSphericalPower = new javax.swing.JTextField();
        lensCylindricalPower = new javax.swing.JTextField();
        visualAuite = new javax.swing.JTextField();
        visualFeilds = new javax.swing.JTextField();
        lensDiameter = new javax.swing.JTextField();
        lensRecurring = new javax.swing.JTextField();
        IntraOcularPressure = new javax.swing.JTextField();
        lensDiameter1 = new javax.swing.JTextField();
        lensRecurring1 = new javax.swing.JTextField();
        IntraOcularPressure1 = new javax.swing.JTextField();
        visualFeilds1 = new javax.swing.JTextField();
        visualAuite1 = new javax.swing.JTextField();
        lensCylindricalPower1 = new javax.swing.JTextField();
        lensSphericalPower1 = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        styledLabel3 = new com.jidesoft.swing.StyledLabel();
        jComboBox1 = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Detail Data Capturing");
        setBackground(new java.awt.Color(235, 235, 235));

        jScrollPane1.setMaximumSize(new java.awt.Dimension(736, 700));
        jScrollPane1.setRequestFocusEnabled(false);

        jPanel2.setBackground(new java.awt.Color(200, 200, 200));
        jPanel2.setMaximumSize(new java.awt.Dimension(734, 1286));

        Panel1.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel1.setText("Insured Person Number");

        styledLabel2.setText("Insured Relation");

        insuredRelation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                insuredRelationActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout Panel1Layout = new javax.swing.GroupLayout(Panel1);
        Panel1.setLayout(Panel1Layout);
        Panel1Layout.setHorizontalGroup(
            Panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(223, 223, 223)
                .addGroup(Panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(insuredPerson)
                    .addComponent(insuredRelation, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE))
                .addContainerGap(201, Short.MAX_VALUE))
        );
        Panel1Layout.setVerticalGroup(
            Panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Panel1Layout.createSequentialGroup()
                .addGroup(Panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(insuredPerson, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(insuredRelation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Panel2.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel5.setText("Laboratory Invoice Number");

        styledLabel6.setText("Laboratory order number");

        javax.swing.GroupLayout Panel2Layout = new javax.swing.GroupLayout(Panel2);
        Panel2.setLayout(Panel2Layout);
        Panel2Layout.setHorizontalGroup(
            Panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(205, 205, 205)
                .addGroup(Panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LabInvoiceNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LabOrderNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(201, Short.MAX_VALUE))
        );
        Panel2Layout.setVerticalGroup(
            Panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(Panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabInvoiceNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabOrderNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Panel3.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel8.setText("Service date from");

        styledLabel9.setText("Service date to");

        ServiceDateFrom.setFormat(new SimpleDateFormat("yyyy/MM/dd"));
        ServiceDateFrom.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ServiceDateFromItemStateChanged(evt);
            }
        });
        ServiceDateFrom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ServiceDateFromActionPerformed(evt);
            }
        });

        ServiceDateTo.setFormat(new SimpleDateFormat("yyyy/MM/dd"));
        ServiceDateTo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ServiceDateToItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout Panel3Layout = new javax.swing.GroupLayout(Panel3);
        Panel3.setLayout(Panel3Layout);
        Panel3Layout.setHorizontalGroup(
            Panel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Panel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(253, 253, 253)
                .addGroup(Panel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ServiceDateFrom, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ServiceDateTo, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(201, Short.MAX_VALUE))
        );
        Panel3Layout.setVerticalGroup(
            Panel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(Panel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ServiceDateFrom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ServiceDateTo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Panel4.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel10.setText("Time In");

        styledLabel11.setText("Time Out");

        TimeIn.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                TimeInFocusLost(evt);
            }
        });

        TimeOut.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                TimeOutFocusLost(evt);
            }
        });

        javax.swing.GroupLayout Panel4Layout = new javax.swing.GroupLayout(Panel4);
        Panel4.setLayout(Panel4Layout);
        Panel4Layout.setHorizontalGroup(
            Panel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Panel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(293, 293, 293)
                .addGroup(Panel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(TimeIn, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TimeOut, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(201, Short.MAX_VALUE))
        );
        Panel4Layout.setVerticalGroup(
            Panel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(Panel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TimeIn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TimeOut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Panel5.setBackground(new java.awt.Color(235, 235, 235));

        Diagnoses.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DiagnosesKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                DiagnosesKeyReleased(evt);
            }
        });

        styledLabel12.setText("Diagnoses /ICD 10/9");

        DiagnosesDescription.setEditable(false);
        DiagnosesDescription.setFocusable(false);

        refDiagnoses.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                refDiagnosesKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                refDiagnosesKeyReleased(evt);
            }
        });

        styledLabel13.setText("Diagnoses /ICD 10/9 Description");

        styledLabel14.setText("Referring Provider Diagnoses/ICD10");

        DiagnosesCombo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                DiagnosesComboMouseClicked(evt);
            }
        });
        DiagnosesCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                DiagnosesComboItemStateChanged(evt);
            }
        });
        DiagnosesCombo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DiagnosesComboKeyPressed(evt);
            }
        });

        refDiagnosesCombo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                refDiagnosesComboMouseClicked(evt);
            }
        });
        refDiagnosesCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                refDiagnosesComboItemStateChanged(evt);
            }
        });
        refDiagnosesCombo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                refDiagnosesComboKeyPressed(evt);
            }
        });

        refDiagnosesDescription.setEditable(false);
        refDiagnosesDescription.setFocusable(false);

        styledLabel15.setText("Diagnoses /ICD 10/9 Description");

        javax.swing.GroupLayout Panel5Layout = new javax.swing.GroupLayout(Panel5);
        Panel5.setLayout(Panel5Layout);
        Panel5Layout.setHorizontalGroup(
            Panel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Panel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(162, 162, 162)
                .addGroup(Panel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(refDiagnosesDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(Panel5Layout.createSequentialGroup()
                        .addComponent(Diagnoses, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(DiagnosesCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(Panel5Layout.createSequentialGroup()
                        .addComponent(refDiagnoses, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(refDiagnosesCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(DiagnosesDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 72, Short.MAX_VALUE))
        );
        Panel5Layout.setVerticalGroup(
            Panel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(Panel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Diagnoses, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DiagnosesCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(DiagnosesDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(refDiagnoses, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(refDiagnosesCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(refDiagnosesDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Panel7.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel16.setText("CPT codes");

        CTPCodes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CTPCodesKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                CTPCodesKeyReleased(evt);
            }
        });

        styledLabel17.setText("CPT codes description");

        CPTCombo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CPTComboMouseClicked(evt);
            }
        });
        CPTCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CPTComboItemStateChanged(evt);
            }
        });
        CPTCombo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CPTComboKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout Panel7Layout = new javax.swing.GroupLayout(Panel7);
        Panel7.setLayout(Panel7Layout);
        Panel7Layout.setHorizontalGroup(
            Panel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Panel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(231, 231, 231)
                .addGroup(Panel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(Panel7Layout.createSequentialGroup()
                        .addComponent(CTPCodes, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(CPTCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(CTPCodesDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(72, Short.MAX_VALUE))
        );
        Panel7Layout.setVerticalGroup(
            Panel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Panel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CTPCodes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CPTCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CTPCodesDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Panel8.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel18.setText("Tariff");

        Tariff.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                TariffFocusLost(evt);
            }
        });
        Tariff.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TariffKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TariffKeyTyped(evt);
            }
        });

        styledLabel19.setText("Tariff Description");

        TariffDescription.setEditable(false);
        TariffDescription.setFocusable(false);

        styledLabel20.setText("Modifier");

        styledLabel21.setText("Multiplier");

        styledLabel22.setText("Units");

        btn_captureModifiers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/box_add.png"))); // NOI18N
        btn_captureModifiers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_captureModifiersActionPerformed(evt);
            }
        });

        cb_modifiersCaptured.setBackground(new java.awt.Color(235, 235, 235));
        cb_modifiersCaptured.setText("Modifiers Captured");
        cb_modifiersCaptured.setEnabled(false);
        cb_modifiersCaptured.setFocusable(false);

        tf_quantity.setText("1");

        jLabel3.setText("Quantity");

        jLabel4.setText("Authorization");

        javax.swing.GroupLayout Panel8Layout = new javax.swing.GroupLayout(Panel8);
        Panel8.setLayout(Panel8Layout);
        Panel8Layout.setHorizontalGroup(
            Panel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Panel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addGap(254, 254, 254)
                .addGroup(Panel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tf_authNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Tariff, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Multiplier, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Units, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(Panel8Layout.createSequentialGroup()
                        .addComponent(cb_modifiersCaptured)
                        .addGap(56, 56, 56)
                        .addComponent(btn_captureModifiers, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(TariffDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(89, Short.MAX_VALUE))
        );
        Panel8Layout.setVerticalGroup(
            Panel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Panel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(Panel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Tariff, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TariffDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_modifiersCaptured)
                    .addComponent(btn_captureModifiers))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Multiplier, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Units, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tf_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tf_authNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)))
        );

        Panel9.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel27.setText("NDC Captured");

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/box_add.png"))); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        cb_mixtureCaptured.setText("NDC Captured");
        cb_mixtureCaptured.setEnabled(false);

        javax.swing.GroupLayout Panel9Layout = new javax.swing.GroupLayout(Panel9);
        Panel9.setLayout(Panel9Layout);
        Panel9Layout.setHorizontalGroup(
            Panel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Panel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(styledLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 285, Short.MAX_VALUE)
                .addComponent(cb_mixtureCaptured)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(220, 220, 220))
        );
        Panel9Layout.setVerticalGroup(
            Panel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Panel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton4)
                    .addGroup(Panel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cb_mixtureCaptured)
                        .addComponent(styledLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        Panel10.setBackground(new java.awt.Color(235, 235, 235));

        toothNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                toothNumberKeyPressed(evt);
            }
        });

        styledLabel33.setText("Tooth Number");

        toothCombo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                toothComboMouseClicked(evt);
            }
        });
        toothCombo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                toothComboKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout Panel10Layout = new javax.swing.GroupLayout(Panel10);
        Panel10.setLayout(Panel10Layout);
        Panel10Layout.setHorizontalGroup(
            Panel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(styledLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(267, 267, 267)
                .addComponent(toothNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(toothCombo, 0, 122, Short.MAX_VALUE)
                .addGap(71, 71, 71))
        );
        Panel10Layout.setVerticalGroup(
            Panel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel10Layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addGroup(Panel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(toothNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(toothCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Panel11.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel35.setText("Lenses RX");

        jLabel1.setText("Left");

        jLabel2.setText("Right");

        javax.swing.GroupLayout Panel11Layout = new javax.swing.GroupLayout(Panel11);
        Panel11.setLayout(Panel11Layout);
        Panel11Layout.setHorizontalGroup(
            Panel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(styledLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(292, 292, 292)
                .addGroup(Panel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(lensesRx, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(Panel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(lensesRx1, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(130, 130, 130))
        );
        Panel11Layout.setVerticalGroup(
            Panel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Panel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(Panel11Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lensesRx1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(Panel11Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(Panel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lensesRx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        Panel12.setBackground(new java.awt.Color(235, 235, 235));

        claimedAmount.setText("0");
        claimedAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                claimedAmountActionPerformed(evt);
            }
        });

        styledLabel45.setText("Claimed amount");

        javax.swing.GroupLayout Panel12Layout = new javax.swing.GroupLayout(Panel12);
        Panel12.setLayout(Panel12Layout);
        Panel12Layout.setHorizontalGroup(
            Panel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(styledLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(262, 262, 262)
                .addComponent(claimedAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(199, Short.MAX_VALUE))
        );
        Panel12Layout.setVerticalGroup(
            Panel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Panel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(claimedAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        btn_save.setText("Save");
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });

        btn_close.setText("Close");
        btn_close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_closeActionPerformed(evt);
            }
        });

        Panel13.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel46.setText("Remark code");

        styledLabel47.setText("Remark code description");

        styledLabel48.setText("Reversal code");

        styledLabel49.setText("Reversal description");

        javax.swing.GroupLayout Panel13Layout = new javax.swing.GroupLayout(Panel13);
        Panel13.setLayout(Panel13Layout);
        Panel13Layout.setHorizontalGroup(
            Panel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Panel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel48, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(219, 219, 219)
                .addGroup(Panel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(remarkCode, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(remarkcodeDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(reversalCode, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(reversalDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(201, Short.MAX_VALUE))
        );
        Panel13Layout.setVerticalGroup(
            Panel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(Panel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(remarkCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(remarkcodeDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(reversalCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel48, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Panel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(reversalDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        Panel9_cb.setBackground(new java.awt.Color(235, 235, 235));

        cb_ndcCodes.setBackground(new java.awt.Color(235, 235, 235));
        cb_ndcCodes.setText("NDC Codes");
        cb_ndcCodes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_ndcCodesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout Panel9_cbLayout = new javax.swing.GroupLayout(Panel9_cb);
        Panel9_cb.setLayout(Panel9_cbLayout);
        Panel9_cbLayout.setHorizontalGroup(
            Panel9_cbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel9_cbLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(cb_ndcCodes)
                .addContainerGap(619, Short.MAX_VALUE))
        );
        Panel9_cbLayout.setVerticalGroup(
            Panel9_cbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Panel9_cbLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cb_ndcCodes))
        );

        jPanel4.setBackground(new java.awt.Color(235, 235, 235));

        cb_patientDetails.setBackground(new java.awt.Color(235, 235, 235));
        cb_patientDetails.setText("Patient Details");
        cb_patientDetails.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_patientDetailsActionPerformed(evt);
            }
        });

        styledLabel44.setText("Patient Weight");

        styledLabel43.setText("Patient Height");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cb_patientDetails)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(styledLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 265, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(height, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(weight, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(7, 7, 7)))
                .addContainerGap(194, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cb_patientDetails)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(weight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(height, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        PanelEyeToRemove.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel37.setText("Lens Prescription: Cylindrical power");

        styledLabel36.setText("Lens Prescription: Spherical power");

        styledLabel42.setText("Intra-Ocular Pressure");

        styledLabel41.setText("Lens Prescription recurring");

        styledLabel40.setText("Lens Prescription diameter (only for contact lenses)");

        styledLabel39.setText("Visual Fields");

        styledLabel38.setText("Visual Acute");

        javax.swing.GroupLayout PanelEyeToRemoveLayout = new javax.swing.GroupLayout(PanelEyeToRemove);
        PanelEyeToRemove.setLayout(PanelEyeToRemoveLayout);
        PanelEyeToRemoveLayout.setHorizontalGroup(
            PanelEyeToRemoveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelEyeToRemoveLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelEyeToRemoveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(90, 90, 90)
                .addGroup(PanelEyeToRemoveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lensSphericalPower, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                    .addComponent(lensCylindricalPower, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                    .addComponent(visualAuite, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                    .addComponent(visualFeilds, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                    .addComponent(lensDiameter, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                    .addComponent(lensRecurring, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                    .addComponent(IntraOcularPressure, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PanelEyeToRemoveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelEyeToRemoveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lensDiameter1, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                        .addComponent(lensRecurring1, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                        .addComponent(IntraOcularPressure1, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE))
                    .addGroup(PanelEyeToRemoveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(visualFeilds1, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(visualAuite1, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lensCylindricalPower1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                        .addComponent(lensSphericalPower1, javax.swing.GroupLayout.Alignment.LEADING)))
                .addGap(189, 189, 189))
        );
        PanelEyeToRemoveLayout.setVerticalGroup(
            PanelEyeToRemoveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelEyeToRemoveLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelEyeToRemoveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lensSphericalPower, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lensSphericalPower1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PanelEyeToRemoveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lensCylindricalPower, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lensCylindricalPower1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PanelEyeToRemoveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(visualAuite, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(visualAuite1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PanelEyeToRemoveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(visualFeilds, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(visualFeilds1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PanelEyeToRemoveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lensDiameter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lensDiameter1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PanelEyeToRemoveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lensRecurring, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lensRecurring1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PanelEyeToRemoveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(IntraOcularPressure, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(IntraOcularPressure1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(PanelEyeToRemove, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Panel13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Panel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Panel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Panel9_cb, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Panel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Panel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Panel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Panel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Panel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Panel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Panel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Panel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Panel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(268, 268, 268)
                .addComponent(btn_save)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_close)
                .addContainerGap(344, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(Panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Panel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Panel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Panel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Panel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Panel7, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Panel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Panel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Panel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Panel9_cb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Panel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Panel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Panel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PanelEyeToRemove, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_save)
                    .addComponent(btn_close)))
        );

        jScrollPane1.setViewportView(jPanel2);

        jPanel1.setBackground(new java.awt.Color(138, 177, 230));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel3.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel3.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel3.setText("Detail Data Capturing");
        styledLabel3.setFont(new java.awt.Font("Arial", 1, 14));
        jPanel1.add(styledLabel3);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "GP", "Optical", "Dental", "Specialists", "Hospitals", "Pharmacy", "Auxiliaries", "Dental Laboratory", "Specialists Laboratory", "Multiple Discipline", "Overseas" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });
        jPanel1.add(jComboBox1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 773, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 753, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE))
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-789)/2, (screenSize.height-723)/2, 789, 723);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_closeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_closeActionPerformed
        CapModifiers = new Vector();
        cb_modifiersCaptured.setSelected(false);
        CapNDCMixtures = new Vector();
        cb_mixtureCaptured.setSelected(false);
        cb_ndcCodes.setSelected(false);
        models.put("GP", new NDCcodes.NDCTableModel());
        models.put("Optical", new NDCcodes.NDCTableModel());
        models.put("Dental", new NDCcodes.NDCTableModel());
        models.put("Specialists", new NDCcodes.NDCTableModel());
        models.put("Hospitals", new NDCcodes.NDCHospitalModel());
        models.put("Pharmacy", new NDCcodes.NDCTableModel());
        models.put("Auxiliaries", new NDCcodes.NDCTableModel());
        models.put("Dental Laboratory", new NDCcodes.NDCTableModel());
        models.put("Specialists Laboratory", new NDCcodes.NDCTableModel());
        models.put("Multiple Discipline", new NDCcodes.NDCTableModel());
        models.put("Overseas", new NDCcodes.NDCTableModel());
        this.dispose();
}//GEN-LAST:event_btn_closeActionPerformed

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
        if (Tariff.getText().equals("")) {
            int option = JOptionPane.showConfirmDialog(this, "No tariff entered!\nAre you sure you want to save?", "Save", JOptionPane.YES_NO_CANCEL_OPTION);
            if (option == JOptionPane.YES_OPTION) {
                System.out.println("Save");
                saveClaimLine();
            } else {
                System.out.println("Dont Save");
            }

        } else {
            if (Tariff.getText().equalsIgnoreCase("GMED") || Tariff.getText().equalsIgnoreCase("MEDS")) {
                if (claimedAmount.getText().trim().equals("") || claimedAmount.getText().trim().equals("0")) {
                    int option = JOptionPane.showConfirmDialog(this, "No Claimed Amount entered!\nAre you sure you want to save?", "Save", JOptionPane.YES_NO_CANCEL_OPTION);
                    if (option == JOptionPane.YES_OPTION) {
                        System.out.println("Save");
                        saveClaimLine();
                    } else {
                        System.out.println("Dont Save");
                    }
                } else {
                    System.out.println("Save");
                    saveClaimLine();
                }
            } else {
                try {
                    Date yearEndDate = (new GregorianCalendar(2010, Calendar.DECEMBER, 31)).getTime();
                    //System.out.println("t " + t.getCode() + " " + t.getDescription());
                    if (new dateConverter().convertDateXML(ServiceDateFrom.getDate()).toGregorianCalendar().getTime().after(yearEndDate)) {
                    } else {
                        int tar = new Integer(Tariff.getText());
                    }
                    System.out.println("Save");
                    saveClaimLine();
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(this, "Tariff not a number, Invalid For 2010 Claims", "Invalid tariff code", JOptionPane.ERROR_MESSAGE);
                    Tariff.setText("");
                    System.out.println("Dont Save");
                }
            }
        }
}//GEN-LAST:event_btn_saveActionPerformed

    private void ServiceDateFromItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ServiceDateFromItemStateChanged
        if (ServiceDateFrom.getDate() != null) {
            /*   if (ServiceDateFrom.getDate().after(today)) {
            //ServiceDateFrom.setSelectedItem(today);
            // ServiceDateFrom.hidePopup();
            //JOptionPane.showConfirmDialog(this, "Entered Invalid Date!", "Invalid Date", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
            } else {*/
            ServiceDateTo.setSelectedItem(ServiceDateFrom.getDate());

            // }
        }
    }//GEN-LAST:event_ServiceDateFromItemStateChanged

    private void ServiceDateToItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ServiceDateToItemStateChanged
        if (ServiceDateTo.getDate() != null) {
            if (ServiceDateTo.getDate().before(ServiceDateFrom.getDate())) {
                //dateCombo.setSelectedItem("");
                ServiceDateTo.hidePopup();
                ServiceDateTo.setDate(ServiceDateFrom.getDate());
                JOptionPane.showConfirmDialog(this, "Entered Invalid Date!", "Invalid Date", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
            }
        }
    }//GEN-LAST:event_ServiceDateToItemStateChanged

    private void insuredRelationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_insuredRelationActionPerformed
        if (insuredRelation.getSelectedItem() instanceof ComboPopulatorDependant) {
            ComboPopulatorDependant c = (ComboPopulatorDependant) insuredRelation.getSelectedItem();
            insuredPerson.setText("" + c.entityId);
            dependantCode = c.dependentCode;
        }

    }//GEN-LAST:event_insuredRelationActionPerformed

    private void TariffFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_TariffFocusLost
        if (!Tariff.getText().equals("")) {
            if ((practiceType == null) || (practiceType.equals("")) || (coverNumber.equals(""))) {
                JOptionPane.showConfirmDialog(this, "Entered invalid tariff or tariff not allowed for this option", "Invalid", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                TariffDescription.setText("Invalid Tariff Code");
            } else {
                Date yearEndDate = (new GregorianCalendar(2010, Calendar.DECEMBER, 31)).getTime();
                CoverProductDetails cpd = NeoWebService.getNeoManagerBeanService().getCoverProductDetailsByCoverNumberByDate(coverNumber, new dateConverter().convertDateXML(ServiceDateFrom.getDate()));
                //Treatment t = NeoWebService.getNeoManagerBeanService().getTreatmentForCode(new Integer(practiceType), Tariff.getText());
                //System.out.println("practiceType " + practiceType);
                //System.out.println("Tariff " + Tariff.getText());
                // System.out.println("getOptionName " + cpd.getOptionName());
                if (cpd != null) {
                    if (Tariff.getText().equalsIgnoreCase("GMED") || Tariff.getText().equalsIgnoreCase("MEDS")) {
                        TariffDescription.setText("MEDICINE");
                    } else {
                        TariffCode t = null;
                        //System.out.println("t " + t.getCode() + " " + t.getDescription());

                        if (new dateConverter().convertDateXML(ServiceDateFrom.getDate()).toGregorianCalendar().getTime().after(yearEndDate)) {
                            List<String> icd10 = new ArrayList<String>();
                            for (int i = 0; i < DiagnosesCombo.getItemCount(); i++) {
                                String icd10combo = DiagnosesCombo.getItemAt(i) + "";
                                icd10.add(icd10combo);
                            }
                            String authNumber = null;
                            if (tf_authNumber.getText() != null && !tf_authNumber.getText().equals("")) {
                                authNumber = tf_authNumber.getText();;
                            }
                            t = NeoWebService.getNeoManagerBeanService().getTariffCode(cpd.getProductId(), cpd.getOptionId(), new dateConverter().convertDateXML(ServiceDateFrom.getDate()), practiceType, providerNumber, providerNumber, icd10, authNumber, Tariff.getText());
                        } else {
                            Treatment nhrpl = NeoWebService.getNeoManagerBeanService().getTreatmentForResoCode(practiceType, new Integer(Tariff.getText().trim()), cpd.getOptionName());
                            if (nhrpl != null) {
                                TariffCode tc = new TariffCode();
                                tc.setDisciplineType(practiceType);
                                tc.setCode(nhrpl.getCode());
                                tc.setDescription(nhrpl.getDescription());
                                tc.setUnits(1);
                                tc.setPrice(nhrpl.getPrice());
                                tc.setAltprice(nhrpl.getPrice());
                                t = tc;
                            }

                        }
                        if (t != null) {
                            TariffDescription.setText(t.getDescription());
                        } else {
                            JOptionPane.showConfirmDialog(this, "Entered invalid tariff or tariff not allowed for this option", "Invalid", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                            TariffDescription.setText("Invalid tariff or tariff not allowed for this option");
                        }
                    }
                    
                    //Put the access validation here for System administrator, Clinical Team Leader and Super user
                    
                    if (!(LoginPage.isClinicalTeamLeader || LoginPage.isSuperUser || LoginPage.isAdministrator) && Tariff.getText().equalsIgnoreCase("PMB99")) {

                        JOptionPane.showConfirmDialog(this, "You don't have permission to use this Tarrif", "Invalid", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                        Tariff.setText("");
                        TariffDescription.setText("You don't have permission to use this Tarrif");
                    }
                                        
                    if (!(LoginPage.isSuperUser || LoginPage.isAdministrator) && (Tariff.getText().equalsIgnoreCase("EXG99") || Tariff.getText().equalsIgnoreCase("TRV99"))) {

                        JOptionPane.showConfirmDialog(this, "You don't have permission to use this Tarrif", "Invalid", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                        Tariff.setText("");
                        TariffDescription.setText("You don't have permission to use this Tarrif");

                    }

                }
                System.out.println("Practice Type: " + practiceType);
            }
        }

        /*
        boolean superLeader = false;
        Collection<SecurityResponsibility> sr = LoginPage.neoUser.getSecurityResponsibility();
        for (SecurityResponsibility securityResponsibility : sr) {
            if (securityResponsibility.getResponsibilityId() == 2) {
                superLeader = true;
                break;
            }
        }


        if (!superLeader) {
            if (Tariff.getText().equalsIgnoreCase("EXG99") || Tariff.getText().equalsIgnoreCase("PMB99")){
                JOptionPane.showConfirmDialog(this, "Tariff code not allowed for this user\nPlease contact super user", "Invalid User", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                Tariff.setText("");
                TariffDescription.setText("Tariff code not allowed for this user");
            }

        }
        */
    }//GEN-LAST:event_TariffFocusLost

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        String selected = jComboBox1.getSelectedItem() + "";
        getNDCcodes().getNDCTable().setModel(models.get(selected));
        getNDCcodes().setPracticeType(practiceType);
        getNDCcodes().setBoundField(claimedAmount);
        getNDCcodes().setVisible(true);
        getNDCcodes().setSelectedScreenName(selected);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void DiagnosesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DiagnosesKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String enteredText = Diagnoses.getText().trim();
            if (!enteredText.equals("")) {
                Diagnosis diagnosis = NeoWebService.getNeoManagerBeanService().getDiagnosisForCode(enteredText);
                if (diagnosis != null) {
                    //DiagnosesDescription.setText(diagnosis.getDescription());
                } else {
                    JOptionPane.showConfirmDialog(this, "ICD10/9 Invalid", "Invalid ICD", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                }
                if (DiagnosesCombo.getItemCount() == 0) {
                    DiagnosesCombo.addItem(enteredText);
                    DiagnosesCombo.setSelectedItem(enteredText);
                    Diagnoses.setText("");
                } else {
                    boolean test = true;
                    for (int i = 0; i < DiagnosesCombo.getItemCount(); i++) {
                        if ((DiagnosesCombo.getItemAt(i)).equals(enteredText)) {
                            test = false;
                        }
                    }
                    if (test) {
                        DiagnosesCombo.addItem(enteredText);
                        DiagnosesCombo.setSelectedItem(enteredText);
                        String diag = Diagnoses.getText();
                        Diagnoses.setText("");
                        //Diagnosis diagnosis = NeoWebService.getNeoManagerBeanService().getDiagnosisForCode(diag);

                    } else {
                        Diagnoses.setText("");
                        JOptionPane.showConfirmDialog(this, "ICD10/9 already entered", "Error", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        } else if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            System.out.println("tab");
        }
    }//GEN-LAST:event_DiagnosesKeyPressed

    private void DiagnosesComboMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DiagnosesComboMouseClicked
        if (evt.getButton() == 3) {
            DiagnosesCombo.removeItem(DiagnosesCombo.getSelectedItem());
        }
    }//GEN-LAST:event_DiagnosesComboMouseClicked

    private void DiagnosesComboKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DiagnosesComboKeyPressed
        if ((evt.getKeyCode() == KeyEvent.VK_DELETE) || (evt.getKeyCode() == 110)) {
            DiagnosesCombo.removeItem(DiagnosesCombo.getSelectedItem());
        }
    }//GEN-LAST:event_DiagnosesComboKeyPressed

    private void CPTComboMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CPTComboMouseClicked
        if (evt.getButton() == 3) {
            CPTCombo.removeItem(CPTCombo.getSelectedItem());
        }
}//GEN-LAST:event_CPTComboMouseClicked

    private void CPTComboKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CPTComboKeyPressed
        if ((evt.getKeyCode() == KeyEvent.VK_DELETE) || (evt.getKeyCode() == 110)) {
            CPTCombo.removeItem(CPTCombo.getSelectedItem());
        }
}//GEN-LAST:event_CPTComboKeyPressed

    private void CTPCodesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CTPCodesKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String enteredText = CTPCodes.getText().trim();
            PreAuthSearchAuthNumber search  = new PreAuthSearchAuthNumber();
            search.setCoverNumber(coverNumber);
            search.setProviderNumber(new Integer(providerNumber));
            search.setDependantCode(new Integer(dependantCode));

            try{
                search.setServiceDate(new dateConverter().convertDateXML(ServiceDateFrom.getDate()));
            }
            catch(Exception e){
                 
            }
            if (!enteredText.equals("")) {
                Procedure cpt = NeoWebService.getNeoManagerBeanService().getProcedureForCode(enteredText);
                if (cpt != null) {

                } else {
                    JOptionPane.showConfirmDialog(this, "CPT Invalid", "Invalid ICD", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                }
                if (CPTCombo.getItemCount() == 0){
                    CPTCombo.addItem(enteredText);
                    CPTCombo.setSelectedItem(enteredText);
                    CTPCodes.setText("");
                } else {
                    boolean test = true;
                    for (int i = 0; i < CPTCombo.getItemCount(); i++) {
                        if ((CPTCombo.getItemAt(i)).equals(enteredText)) {
                            test = false;
                        }
                    }
                    if (test) {
                        CPTCombo.addItem(enteredText);
                        CPTCombo.setSelectedItem(enteredText);
                        CTPCodes.setText("");
                        //CTPCodesDescription.setText(cpt.getDescription());
                    } else {
                        CTPCodes.setText("");
                        JOptionPane.showConfirmDialog(this, "CPT Already Entered", "Error", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
            search.setCpt(CPTCombo.getItemAt(CPTCombo.getItemCount()-1).toString());
            tf_authNumber.setText(NeoWebService.getNeoManagerBeanService().getAuthNumberForMember(search));
        }
    }//GEN-LAST:event_CTPCodesKeyPressed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        String selected = jComboBox1.getSelectedItem() + "";
        System.out.println(selected);
        setScreen(selected);
        claimedAmount.setText("0");


    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void cb_ndcCodesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_ndcCodesActionPerformed
        if (cb_ndcCodes.isSelected()) {
            Panel9.setVisible(true);
        } else if (!cb_ndcCodes.isSelected()) {
            Panel9.setVisible(false);
        }
}//GEN-LAST:event_cb_ndcCodesActionPerformed

    private void cb_patientDetailsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_patientDetailsActionPerformed
        if (cb_patientDetails.isSelected()) {
            height.setVisible(true);
            weight.setVisible(true);
            styledLabel43.setVisible(true);
            styledLabel44.setVisible(true);
        } else if (!cb_patientDetails.isSelected()) {
            height.setVisible(false);
            weight.setVisible(false);
            styledLabel43.setVisible(false);
            styledLabel44.setVisible(false);
        }
}//GEN-LAST:event_cb_patientDetailsActionPerformed

    private void TimeInFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_TimeInFocusLost
        String timeInEntered = TimeIn.getText();
        String rightTimeIn = "";
        boolean flagIn = false;
        boolean timeMatch = (Pattern.matches("[0-1]{1}[0-9]{1}.?[0-5]{1}[0-9]{1}", timeInEntered)) || (Pattern.matches("[2]{1}[0-3]{1}.?[0-5]{1}[0-9]{1}", timeInEntered));
        if ((timeInEntered.length() == 5) && (timeMatch == true)) {
            if ((timeInEntered.substring(2, 3).equals("h")) || (timeInEntered.substring(2, 3).equals("-")) || (timeInEntered.substring(2, 3).equals(":"))) {
                flagIn = true;
                rightTimeIn = timeInEntered.substring(0, 2) + ":" + timeInEntered.substring(3, 5);
            }
        } else if (timeMatch == true) {
            flagIn = true;
            rightTimeIn = timeInEntered.substring(0, 2) + ":" + timeInEntered.substring(2, 4);
        } else {
            JOptionPane.showMessageDialog(null, "Invalid Time entered", "Input Error", JOptionPane.WARNING_MESSAGE);
        }
        if (flagIn == true) {
            TimeIn.setText(rightTimeIn);
        }
    }//GEN-LAST:event_TimeInFocusLost

    private void TimeOutFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_TimeOutFocusLost
        String timeInEntered = TimeOut.getText();
        String rightTimeOut = "";
        boolean flagIn = false;
        boolean timeMatch = (Pattern.matches("[0-1]{1}[0-9]{1}.?[0-5]{1}[0-9]{1}", timeInEntered)) || (Pattern.matches("[2]{1}[0-3]{1}.?[0-5]{1}[0-9]{1}", timeInEntered));
        if ((timeInEntered.length() == 5) && (timeMatch == true)) {
            if ((timeInEntered.substring(2, 3).equals("h")) || (timeInEntered.substring(2, 3).equals("-")) || (timeInEntered.substring(2, 3).equals(":"))) {
                flagIn = true;
                rightTimeOut = timeInEntered.substring(0, 2) + ":" + timeInEntered.substring(3, 5);
            }
        } else if (timeMatch == true) {
            flagIn = true;
            rightTimeOut = timeInEntered.substring(0, 2) + ":" + timeInEntered.substring(2, 4);
        } else {
            JOptionPane.showMessageDialog(null, "Invalid Time entered", "Input Error", JOptionPane.WARNING_MESSAGE);
        }
        if (flagIn == true) {
            TimeOut.setText(rightTimeOut);
        }
    }//GEN-LAST:event_TimeOutFocusLost

    private void DiagnosesComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_DiagnosesComboItemStateChanged
        System.out.println("combo: " + DiagnosesCombo.getItemCount());
        System.out.println("Combo Selected: " + DiagnosesCombo.getSelectedItem());
        if (DiagnosesCombo.getItemCount() > 0) {
            String diag = DiagnosesCombo.getSelectedItem() + "";
            Diagnosis diagnosis = NeoWebService.getNeoManagerBeanService().getDiagnosisForCode(diag);
            if (diagnosis != null) {
                DiagnosesDescription.setForeground(Color.BLACK);
                DiagnosesDescription.setText(diagnosis.getDescription());
            } else {
                DiagnosesDescription.setForeground(Color.RED);
                DiagnosesDescription.setText("Invalid ICD10/9");
                //JOptionPane.showConfirmDialog(this, "ICD10/9 Invalid", "Invalid ICD", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);sdfg
            }
        }
    }//GEN-LAST:event_DiagnosesComboItemStateChanged

    private void CPTComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CPTComboItemStateChanged
        System.out.println("combo: " + CPTCombo.getItemCount());
        System.out.println("Combo Selected: " + CPTCombo.getSelectedItem());
        if (CPTCombo.getItemCount() > 0) {
            String diag = CPTCombo.getSelectedItem() + "";
            Procedure cpt = NeoWebService.getNeoManagerBeanService().getProcedureForCode(diag);
            if (cpt != null) {
                CTPCodesDescription.setForeground(Color.BLACK);
                CTPCodesDescription.setText(cpt.getDescription());
            } else {
                CTPCodesDescription.setForeground(Color.RED);
                CTPCodesDescription.setText("Invalid CPT");
            //JOptionPane.showConfirmDialog(this, "ICD10/9 Invalid", "Invalid ICD", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);sdfg
            }
        }
    }//GEN-LAST:event_CPTComboItemStateChanged

    private void btn_captureModifiersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_captureModifiersActionPerformed
        System.out.println("collection: " + CapModifiers);
        new CaptureModifier().setVisible(true);
}//GEN-LAST:event_btn_captureModifiersActionPerformed

    private void refDiagnosesComboMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_refDiagnosesComboMouseClicked
        if (evt.getButton() == 3) {
            refDiagnosesCombo.removeItem(refDiagnosesCombo.getSelectedItem());
        }
}//GEN-LAST:event_refDiagnosesComboMouseClicked

    private void refDiagnosesComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_refDiagnosesComboItemStateChanged
        System.out.println("combo: " + refDiagnosesCombo.getItemCount());
        System.out.println("Combo Selected: " + refDiagnosesCombo.getSelectedItem());
        if (refDiagnosesCombo.getItemCount() > 0) {
            String diag = refDiagnosesCombo.getSelectedItem() + "";
            Diagnosis diagnosis = NeoWebService.getNeoManagerBeanService().getDiagnosisForCode(diag);
            refDiagnosesDescription.setText(diagnosis.getDescription());
        }
}//GEN-LAST:event_refDiagnosesComboItemStateChanged

    private void refDiagnosesComboKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_refDiagnosesComboKeyPressed
        if ((evt.getKeyCode() == KeyEvent.VK_DELETE) || (evt.getKeyCode() == 110)) {
            refDiagnosesCombo.removeItem(refDiagnosesCombo.getSelectedItem());
        }
}//GEN-LAST:event_refDiagnosesComboKeyPressed

    private void refDiagnosesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_refDiagnosesKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String enteredText = refDiagnoses.getText();
            if (!enteredText.equals("")) {
                if (refDiagnosesCombo.getItemCount() == 0) {
                    refDiagnosesCombo.addItem(enteredText);
                    refDiagnosesCombo.setSelectedItem(enteredText);
                    refDiagnoses.setText("");
                } else {
                    boolean test = true;
                    for (int i = 0; i < refDiagnosesCombo.getItemCount(); i++) {
                        if ((refDiagnosesCombo.getItemAt(i)).equals(enteredText)) {
                            test = false;
                        }
                    }
                    if (test) {
                        refDiagnosesCombo.addItem(enteredText);
                        refDiagnosesCombo.setSelectedItem(enteredText);
                        refDiagnoses.setText("");
                        String diag = refDiagnoses.getText();
                        Diagnosis diagnosis = NeoWebService.getNeoManagerBeanService().getDiagnosisForCode(diag);
                        refDiagnosesDescription.setText(diagnosis.getDescription());
                    } else {
                        refDiagnoses.setText("");
                        JOptionPane.showConfirmDialog(this, "ICD10/9 already entered", "Error", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        }
}//GEN-LAST:event_refDiagnosesKeyPressed

    private void toothNumberKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_toothNumberKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String enteredText = toothNumber.getText();
            int numberTest = 0;
            if (!enteredText.equals("")) {
                try {
                    numberTest = new Integer(enteredText);

                    if (toothCombo.getItemCount() == 0) {
                        toothCombo.addItem(enteredText);
                        toothCombo.setSelectedItem(enteredText);
                        toothNumber.setText("");
                    } else {
                        boolean test = true;
                        for (int i = 0; i < toothCombo.getItemCount(); i++) {
                            if ((toothCombo.getItemAt(i)).equals(enteredText)) {
                                test = false;
                            }
                        }
                        if (test) {
                            toothCombo.addItem(enteredText);
                            toothCombo.setSelectedItem(enteredText);
                            toothNumber.setText("");
                        } else {
                            toothNumber.setText("");
                            JOptionPane.showConfirmDialog(this, "Tooth Number Already Entered", "Error", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                        }
                    }

                } catch (Exception e) {
                    JOptionPane.showConfirmDialog(this, numberTest + "Not a valid Number", "Error", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                }

            }

        }
    }//GEN-LAST:event_toothNumberKeyPressed

    private void toothComboKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_toothComboKeyPressed
        if ((evt.getKeyCode() == KeyEvent.VK_DELETE) || (evt.getKeyCode() == 110)) {
            toothCombo.removeItem(toothCombo.getSelectedItem());
        }
    }//GEN-LAST:event_toothComboKeyPressed

    private void toothComboMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_toothComboMouseClicked
        if (evt.getButton() == 3) {
            toothCombo.removeItem(toothCombo.getSelectedItem());
        }
    }//GEN-LAST:event_toothComboMouseClicked

    private void claimedAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_claimedAmountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_claimedAmountActionPerformed

    private void ServiceDateFromActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ServiceDateFromActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ServiceDateFromActionPerformed

    private void TariffKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TariffKeyTyped
    }//GEN-LAST:event_TariffKeyTyped

    private void TariffKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TariffKeyReleased
        Tariff.setText(Tariff.getText().toUpperCase());
    }//GEN-LAST:event_TariffKeyReleased

    private void DiagnosesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DiagnosesKeyReleased
        Diagnoses.setText(Diagnoses.getText().toUpperCase());
    }//GEN-LAST:event_DiagnosesKeyReleased

    private void refDiagnosesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_refDiagnosesKeyReleased
        refDiagnoses.setText(refDiagnoses.getText().toUpperCase());
    }//GEN-LAST:event_refDiagnosesKeyReleased

    private void CTPCodesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CTPCodesKeyReleased
        CTPCodes.setText(CTPCodes.getText().toUpperCase());
    }//GEN-LAST:event_CTPCodesKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new DetailDataCapturingFrame().setVisible(true);
            }
        });
    }

    public void saveClaimLine() {
        ClaimLine cl = new ClaimLine();
        cl.setClaimId(claimID);
        if (ServiceDateFrom.getDate() != null) {
            cl.setServiceDateFrom(new dateConverter().convertDateXML(ServiceDateFrom.getDate()));
            cl.setServiceDateTo(new dateConverter().convertDateXML(ServiceDateTo.getDate()));
        }
        cl.setInsuredPersonEntityId(new Integer(insuredPerson.getText()));
        cl.setInsuredPersonDependentCode(new Integer(dependantCode));    // TODO - dependant Code
        cl.setTimeIn(TimeIn.getText());
        cl.setTimeOut(TimeOut.getText());
        if (!Tariff.getText().equals("")) {
            cl.setTarrifCodeNo(Tariff.getText());
        }
        //cl.setTarrifCodeId(0);    
        if (!Multiplier.getText().equals("")) {
            cl.setMultiplier(new Integer(Multiplier.getText()));
        }
        if (!Units.getText().equals("")) {
            cl.setUnits(new Double(Units.getText()));
        }
        if (!tf_quantity.getText().equals("")) {
            cl.setQuantity(new Integer(tf_quantity.getText()));
        }

        if (!height.getText().equals("")) {
            cl.setPatientHeight(new Double(height.getText()));
        }
        if (!weight.getText().equals("")) {
            cl.setPatientWeight(new Double(weight.getText()));
        }
        cl.setLabOrderNumber(LabOrderNumber.getText());
        cl.setLabInvoiceNumber(LabInvoiceNumber.getText());
        if (!claimedAmount.getText().equals("")) {
            cl.setClaimedAmount(new Double(claimedAmount.getText()));
        }
        cl.setClaimLineStatusId(1);

        /* if (rb_leftEye.isSelected()) {
        cl.setEyeLR("L");
        } else if (rb_rightEye.isSelected()) {
        cl.setEyeLR("R");
        }*/
        Collection<Eye> eyes = cl.getEyes();

        Eye eyeLeft = new Eye();
        eyeLeft.setEyeLR("L");
        eyeLeft.setVisualFields(visualFeilds.getText());
        eyeLeft.setVisualAcute(visualAuite.getText());
        eyeLeft.setIntraOccularPressure(IntraOcularPressure.getText());
        eyeLeft.setLensesRX(lensesRx.getText());
        eyeLeft.setLensPrescriptionCylindricalPower(lensCylindricalPower.getText());
        if (!lensDiameter.getText().equals("")) {
            eyeLeft.setLensDiameter(new Double(lensDiameter.getText()));
        }
        eyeLeft.setLensPrescriptionRecurring(lensRecurring.getText());
        eyeLeft.setLensPrescriptionSphericalPower(lensSphericalPower.getText());

        Eye eyeRight = new Eye();
        eyeRight.setEyeLR("R");
        eyeRight.setVisualFields(visualFeilds1.getText());
        eyeRight.setVisualAcute(visualAuite1.getText());
        eyeRight.setIntraOccularPressure(IntraOcularPressure1.getText());
        eyeRight.setLensesRX(lensesRx1.getText());
        eyeRight.setLensPrescriptionCylindricalPower(lensCylindricalPower1.getText());
        if (!lensDiameter1.getText().equals("")) {
            eyeRight.setLensDiameter(new Double(lensDiameter1.getText()));
        }
        eyeRight.setLensPrescriptionRecurring(lensRecurring1.getText());
        eyeRight.setLensPrescriptionSphericalPower(lensSphericalPower1.getText());

        if (!lensesRx.getText().equals("")) {
            eyes.add(eyeLeft);
        }
        if (!lensesRx1.getText().equals("")) {
            eyes.add(eyeRight);
        }

        cl.setChequeRunID(0);           // TODO - id
        cl.setChequeRunDate(null);
        cl.setProsessDate(null);
        // cl.setEffectiveStartDate(new dateConverter().convertDateXML(today));
        //cl.setEffectiveEndDate(new dateConverter().convertDateXML(today));
        Collection<String> authNumbers = cl.getAuthNumber();
        // TODO capture auth Number
        String authNum = tf_authNumber.getText();
        if (authNum != null && !authNum.equals("")) {
            authNumbers.add(authNum);
        }

        Collection<Integer> toothNumbers = cl.getToothNumber();         // TODO capture tooth Numbers
        for (int i = 0; i < toothCombo.getItemCount(); i++) {
            int toothNumberinCombo = new Integer(toothCombo.getItemAt(i) + "");
            toothNumbers.add(toothNumberinCombo);
        }


        Collection<ClaimCptCodes> cptCodes = cl.getCptCodes();
        for (int i = 0; i < CPTCombo.getItemCount(); i++) {
            ClaimCptCodes ccptc = new ClaimCptCodes();
            ccptc.setCptCode(CPTCombo.getItemAt(i) + "");
            cptCodes.add(ccptc);
        }
        if (!CapNDCMixtures.isEmpty()) {
            Collection<Ndc> ndcMixtures = cl.getNdc();
            ndcMixtures.addAll(CapNDCMixtures);
            System.out.println("ndc size: " + ndcMixtures.size());
        }
        if (!CapModifiers.isEmpty()) {
            System.out.println("capModifier" + CapModifiers);
            Collection<Modifier> modifiers = cl.getModifiers();
            modifiers.addAll(CapModifiers);
        }

        Collection<ClaimIcd10> icd10 = cl.getIcd10();                   // TODO capture icd10 id
        for (int i = 0; i < DiagnosesCombo.getItemCount(); i++) {
            String icd10combo = DiagnosesCombo.getItemAt(i) + "";
            ClaimIcd10 cicd = new ClaimIcd10();
            cicd.setIcd10Code(icd10combo);
            cicd.setReferingIcd10(0); //not refering
            icd10.add(cicd);
        }
        for (int i = 0; i < refDiagnosesCombo.getItemCount(); i++) {
            String icd10combo = refDiagnosesCombo.getItemAt(i) + "";
            ClaimIcd10 cicd = new ClaimIcd10();
            cicd.setIcd10Code(icd10combo);
            cicd.setReferingIcd10(1); //refering
            icd10.add(cicd);
        }



        Security _secure = new Security();
        _secure.setCreatedBy(LoginPage.neoUser.getUserId());
        _secure.setCreationDate(new dateConverter().convertDateXML(today));
        _secure.setLastUpdatedBy(LoginPage.neoUser.getUserId());
        _secure.setLastUpdateDate(new dateConverter().convertDateXML(today));
        _secure.setSecurityGroupId(2);

        System.out.println("ndc: " + cl.getNdc());
        System.out.println("modifiers: " + cl.getModifiers());
        System.out.println("claimId: " + claimID);
        cl.setClaimLineId(NeoWebService.getNeoManagerBeanService().saveClaimLine(cl, claimID, _secure, true));
        System.out.println("captured claim line id: " + cl.getClaimLineId());

        if (cl.getClaimLineId() != 0) {
            //display date correctly
            JOptionPane.showConfirmDialog(this, "ClaimLine " + cl.getClaimLineId() + " saved", "Saved", JOptionPane.CLOSED_OPTION, JOptionPane.INFORMATION_MESSAGE);

            //This only works if we are capturing new lines, not when correcting
            try {
                ch.headerTable.getModel().setValueAt(true, row, 9);
            } catch (Exception ex) {
               ex.printStackTrace();
            }
            CTPCodes.setText("");
            CTPCodesDescription.setText("");
            Diagnoses.setText("");
            DiagnosesDescription.setText("");
            IntraOcularPressure.setText("");
            LabInvoiceNumber.setText("");
            LabOrderNumber.setText("");
            Multiplier.setText("1");
            //ServiceDateFrom.setSelectedItem(null);
            //ServiceDateTo.setSelectedItem(null);
            Tariff.setText("");
            TariffDescription.setText("");
            TimeIn.setText("");
            TimeOut.setText("");
            Units.setText("1");
            tf_quantity.setText("1");
            claimedAmount.setText("");
            height.setText("");
            //insuredPerson.setText("");

            lensCylindricalPower.setText("");
            lensDiameter.setText("");
            lensRecurring.setText("");
            lensSphericalPower.setText("");
            lensesRx.setText("");
            visualAuite.setText("");
            visualFeilds.setText("");
            IntraOcularPressure.setText("");

            lensCylindricalPower1.setText("");
            lensDiameter1.setText("");
            lensRecurring1.setText("");
            lensSphericalPower1.setText("");
            lensesRx1.setText("");
            visualAuite1.setText("");
            visualFeilds1.setText("");
            IntraOcularPressure1.setText("");

            remarkCode.setText("");
            remarkcodeDescription.setText("");
            refDiagnoses.setText("");
            reversalCode.setText("");
            reversalDescription.setText("");
            toothNumber.setText("");

            weight.setText("");
            //DiagnosesDescription.setText("");
            //DiagnosesCombo.removeAllItems();
            refDiagnosesDescription.setText("");
            refDiagnosesCombo.removeAllItems();
            TariffDescription.setText("");

            CapModifiers = new Vector();
            cb_modifiersCaptured.setSelected(false);
            toothCombo.removeAllItems();
            CapNDCMixtures = new Vector();
            cb_mixtureCaptured.setSelected(false);
            
            Tariff.requestFocus();
            //CapNDCMixtures = null;
            //ndc = null;
            //cb_ndcCodes.setSelected(false);
            //CapNDCMixtures = new Vector();
            models.put("GP", new NDCcodes.NDCTableModel());
            models.put("Optical", new NDCcodes.NDCTableModel());
            models.put("Dental", new NDCcodes.NDCTableModel());
            models.put("Specialists", new NDCcodes.NDCTableModel());
            models.put("Hospitals", new NDCcodes.NDCHospitalModel());
            models.put("Pharmacy", new NDCcodes.NDCTableModel());
            models.put("Auxiliaries", new NDCcodes.NDCTableModel());
            models.put("Dental Laboratory", new NDCcodes.NDCTableModel());
            models.put("Specialists Laboratory", new NDCcodes.NDCTableModel());
            models.put("Multiple Discipline", new NDCcodes.NDCTableModel());
            models.put("Overseas", new NDCcodes.NDCTableModel());

            System.out.println("All reset.");

            /*    //DOCUMENT SET CLAIM TO CAPTURE
            if (reconcilliationOfClaimBacth.batchClaims != null && reconcilliationOfClaimBacth.batchClaims.getClaimsList() != null) {
            if (reconcilliationOfClaimBacth.batchClaims.getClaimsList().size() > ClaimHeaderCapturing.docClaimsController) {
            int claimIndNo = Integer.parseInt(reconcilliationOfClaimBacth.batchClaims.getClaimsList().get(ClaimHeaderCapturing.docClaimsController) + "");
            boolean claimCaptured = NeoWebService.getNeoManagerBeanService().setClaimBatchCaptured(claimIndNo);
            if (claimCaptured) {
            System.out.println("Saving of claim in DMS DONE");
            if (reconcilliationOfClaimBacth.batchClaims.getClaimsList().size() == (ClaimHeaderCapturing.docClaimsController + 1)) {
            // ClaimHeaderCapturing.butGetClaim.setEnabled(false);
            boolean batchCaptured = NeoWebService.getNeoManagerBeanService().setClaimBatchCaptured(reconcilliationOfClaimBacth.batchClaims.getIndNo());
            if (batchCaptured) {
            reconcilliationOfClaimBacth.batchClaims = null;
            System.out.println("Saving of batch in DMS DONE");
            } else {
            System.out.println("PROBLEM FINALIZING CAPTURE OF BATCH IN DMS COMPLETE");
            }
            }
            ClaimHeaderCapturing.docClaimsController++;
            } else {
            System.out.println("PROBLEM FINALIZING CAPTURE OF CLAIM IN DMS");
            }
            }
            }*/

        } else {
            JOptionPane.showConfirmDialog(this, "Saving of claimline failed", "Failed", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
        }
    }

    public void getToday() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        today = calendar.getTime();
    }

    public void setRelationCombo(int sentDependantCode) {
        Collection<CoverDetails> cov = NeoWebService.getNeoManagerBeanService().getCoverDetailsByCoverNumber(coverNumber);
        if (cov != null) {
            for (CoverDetails coverDetails : cov) {
                if (coverDetails.getDependentNumber() == sentDependantCode) {
                    PersonDetails p = NeoWebService.getNeoManagerBeanService().getPersonDetailsByEntityId(coverDetails.getEntityId());

                    ComboPopulatorDependant cc = new ComboPopulatorDependant(coverDetails.getDependentNumber(), p.getName(), coverDetails.getDependentType(), coverDetails.getEntityId(), new dateConverter().convertToYYYMMD(p.getDateOfBirth().toGregorianCalendar().getTime()));
                    insuredRelation.setSelectedItem(cc);
                    insuredPerson.setText(coverDetails.getEntityId() + "");
                    dependantCode = sentDependantCode + "";
                }
            }
        }
    }

    private class ComboPopulatorDependant {

        public String dependentCode;
        public String name;
        public String relation;
        public int entityId;

        public ComboPopulatorDependant(int dependentCode, String name, String relation, int entityId, String dob) {
            this.dependentCode = dependentCode + "";
            this.name = dependentCode + " - " + name;
            this.relation = relation + " - " + dob;
            this.entityId = entityId;
        }

        public String toString() {
            return name + " - " + relation;
        }
    }

    public void setScreen(String catagory) {
        String selected = catagory;
        System.out.println("selected catagory = "+selected);
        jComboBox1.setSelectedItem(selected);
        Panel2.setVisible(false);
        Panel4.setVisible(false);
        Panel7.setVisible(false);
        Panel9.setVisible(false);
        Panel10.setVisible(false);
        Panel11.setVisible(false);
        Panel13.setVisible(false);
        height.setVisible(false);
        weight.setVisible(false);
        styledLabel43.setVisible(false);
        styledLabel44.setVisible(false);
        cb_ndcCodes.setSelected(false);

        if (selected.equals("Multiple Discipline")) {
            Panel2.setVisible(true);
            Panel4.setVisible(true);
            Panel7.setVisible(true);
            Panel9_cb.setVisible(true);
            Panel10.setVisible(true);
            Panel11.setVisible(true);

        } else if (selected.equals("Pharmacy")) {
            Panel9.setVisible(true);
            Panel9_cb.setVisible(true);
            cb_ndcCodes.setSelected(true);

        } else if (selected.equals("Optical")) {
            //Panel2.setVisible(true);
            Panel9_cb.setVisible(true);
            Panel11.setVisible(true);

        } else if (selected.equals("Dental")) {
            Panel9_cb.setVisible(true);
            Panel10.setVisible(true);

        } else if (selected.equals("Dental Laboratory")) {
            Panel2.setVisible(true);
            Panel10.setVisible(true);

        } else if (selected.equals("Auxiliaries")) {
            Panel4.setVisible(true);
            Panel9_cb.setVisible(true);

        } else if (selected.equals("Specialists")) {
            Panel9_cb.setVisible(true);

        } else if (selected.equals("Specialists Laboratory")) {
            Panel2.setVisible(true);

        } else if (selected.equals("Hospitals")) {
            Panel4.setVisible(true);
            Panel7.setVisible(true);
            Panel9_cb.setVisible(true);

        } else if (selected.equals("GP")) {
            Panel9_cb.setVisible(true);

        } else if (selected.equals("Overseas")) {
            Panel9_cb.setVisible(false);
            
        } else {
            Panel9_cb.setVisible(true);
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JComboBox CPTCombo;
    private javax.swing.JTextField CTPCodes;
    private javax.swing.JTextField CTPCodesDescription;
    private javax.swing.JTextField Diagnoses;
    public static javax.swing.JComboBox DiagnosesCombo;
    private javax.swing.JTextField DiagnosesDescription;
    public static javax.swing.JTextField IntraOcularPressure;
    public static javax.swing.JTextField IntraOcularPressure1;
    public static javax.swing.JTextField LabInvoiceNumber;
    public static javax.swing.JTextField LabOrderNumber;
    public static javax.swing.JTextField Multiplier;
    private javax.swing.JPanel Panel1;
    private javax.swing.JPanel Panel10;
    private javax.swing.JPanel Panel11;
    private javax.swing.JPanel Panel12;
    private javax.swing.JPanel Panel13;
    private javax.swing.JPanel Panel2;
    private javax.swing.JPanel Panel3;
    private javax.swing.JPanel Panel4;
    private javax.swing.JPanel Panel5;
    private javax.swing.JPanel Panel7;
    private javax.swing.JPanel Panel8;
    private javax.swing.JPanel Panel9;
    private javax.swing.JPanel Panel9_cb;
    private javax.swing.JPanel PanelEyeToRemove;
    public static com.jidesoft.combobox.DateComboBox ServiceDateFrom;
    public static com.jidesoft.combobox.DateComboBox ServiceDateTo;
    public static javax.swing.JTextField Tariff;
    private javax.swing.JTextField TariffDescription;
    public static javax.swing.JTextField TimeIn;
    public static javax.swing.JTextField TimeOut;
    public static javax.swing.JTextField Units;
    private javax.swing.JButton btn_captureModifiers;
    private javax.swing.JButton btn_close;
    private javax.swing.JButton btn_save;
    public static javax.swing.JCheckBox cb_mixtureCaptured;
    public static javax.swing.JCheckBox cb_modifiersCaptured;
    private javax.swing.JCheckBox cb_ndcCodes;
    private javax.swing.JCheckBox cb_patientDetails;
    public static javax.swing.JTextField claimedAmount;
    private javax.swing.JTextField height;
    public static javax.swing.JTextField insuredPerson;
    public static com.jidesoft.swing.AutoCompletionComboBox insuredRelation;
    private javax.swing.JButton jButton4;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTextField lensCylindricalPower;
    public static javax.swing.JTextField lensCylindricalPower1;
    public static javax.swing.JTextField lensDiameter;
    public static javax.swing.JTextField lensDiameter1;
    public static javax.swing.JTextField lensRecurring;
    public static javax.swing.JTextField lensRecurring1;
    public static javax.swing.JTextField lensSphericalPower;
    public static javax.swing.JTextField lensSphericalPower1;
    public static javax.swing.JTextField lensesRx;
    public static javax.swing.JTextField lensesRx1;
    private javax.swing.JTextField refDiagnoses;
    public static javax.swing.JComboBox refDiagnosesCombo;
    private javax.swing.JTextField refDiagnosesDescription;
    private javax.swing.JTextField remarkCode;
    private javax.swing.JTextField remarkcodeDescription;
    private javax.swing.JTextField reversalCode;
    private javax.swing.JTextField reversalDescription;
    private com.jidesoft.swing.StyledLabel styledLabel1;
    private com.jidesoft.swing.StyledLabel styledLabel10;
    private com.jidesoft.swing.StyledLabel styledLabel11;
    private com.jidesoft.swing.StyledLabel styledLabel12;
    private com.jidesoft.swing.StyledLabel styledLabel13;
    private com.jidesoft.swing.StyledLabel styledLabel14;
    private com.jidesoft.swing.StyledLabel styledLabel15;
    private com.jidesoft.swing.StyledLabel styledLabel16;
    private com.jidesoft.swing.StyledLabel styledLabel17;
    private com.jidesoft.swing.StyledLabel styledLabel18;
    private com.jidesoft.swing.StyledLabel styledLabel19;
    private com.jidesoft.swing.StyledLabel styledLabel2;
    private com.jidesoft.swing.StyledLabel styledLabel20;
    private com.jidesoft.swing.StyledLabel styledLabel21;
    private com.jidesoft.swing.StyledLabel styledLabel22;
    private com.jidesoft.swing.StyledLabel styledLabel27;
    private com.jidesoft.swing.StyledLabel styledLabel3;
    private com.jidesoft.swing.StyledLabel styledLabel33;
    private com.jidesoft.swing.StyledLabel styledLabel35;
    private com.jidesoft.swing.StyledLabel styledLabel36;
    private com.jidesoft.swing.StyledLabel styledLabel37;
    private com.jidesoft.swing.StyledLabel styledLabel38;
    private com.jidesoft.swing.StyledLabel styledLabel39;
    private com.jidesoft.swing.StyledLabel styledLabel40;
    private com.jidesoft.swing.StyledLabel styledLabel41;
    private com.jidesoft.swing.StyledLabel styledLabel42;
    private com.jidesoft.swing.StyledLabel styledLabel43;
    private com.jidesoft.swing.StyledLabel styledLabel44;
    private com.jidesoft.swing.StyledLabel styledLabel45;
    private com.jidesoft.swing.StyledLabel styledLabel46;
    private com.jidesoft.swing.StyledLabel styledLabel47;
    private com.jidesoft.swing.StyledLabel styledLabel48;
    private com.jidesoft.swing.StyledLabel styledLabel49;
    private com.jidesoft.swing.StyledLabel styledLabel5;
    private com.jidesoft.swing.StyledLabel styledLabel6;
    private com.jidesoft.swing.StyledLabel styledLabel8;
    private com.jidesoft.swing.StyledLabel styledLabel9;
    public static javax.swing.JTextField tf_authNumber;
    public static javax.swing.JTextField tf_quantity;
    public static javax.swing.JComboBox toothCombo;
    public static javax.swing.JTextField toothNumber;
    public static javax.swing.JTextField visualAuite;
    public static javax.swing.JTextField visualAuite1;
    public static javax.swing.JTextField visualFeilds;
    public static javax.swing.JTextField visualFeilds1;
    private javax.swing.JTextField weight;
    // End of variables declaration//GEN-END:variables
}

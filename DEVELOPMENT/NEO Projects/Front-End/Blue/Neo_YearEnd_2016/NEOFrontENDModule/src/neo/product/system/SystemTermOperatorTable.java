/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.product.system;

/**
 *
 * @author AbigailM
 */
import java.awt.Component;
import java.util.Map;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import neo.util.UtilMethods;

public class SystemTermOperatorTable extends AbstractCellEditor implements TableCellEditor {

    private JComboBox cbComponent;

    public SystemTermOperatorTable() {

        //neo.manager.OperatorType[] operator =neo.manager.OperatorType.values();
  
        cbComponent = new JComboBox(UtilMethods.getLookupValues("Operator Type"));

    }

    public SystemTermOperatorTable(Map lookupValues) {

        //neo.manager.OperatorType[] operator =neo.manager.OperatorType.values();

        cbComponent = new JComboBox(UtilMethods.getLookupValues("Operator Type", lookupValues));

    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {

        return cbComponent;

    }

    public Object getCellEditorValue() {

        return cbComponent.getSelectedItem();

    }
}



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neo.quotation.manageQuotation;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Vector;
import jxl.Cell;
import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;

/**
 *
 * @author Administrator
 */
public class ReadCensusIndividualDetails
{
    protected Vector vectValues = new Vector();
 
    public void init(String filePath) 
    {
	FileInputStream fs = null;
	try 
        {
            fs = new FileInputStream(new File(filePath));
            this.contentReading(fs);
	} 
        catch (IOException e) 
        {
		e.printStackTrace();
	} 
        catch (Exception e) 
        {
		e.printStackTrace();
	}
        finally 
        {
            try 
            {
		fs.close();
            } 
            catch (IOException e) 
            {
                e.printStackTrace();
            }
        }
    }

    private void contentReading(InputStream fileInputStream) 
    {
        WorkbookSettings ws = null;
        Workbook workbook = null;
        Sheet s = null;
        Cell rowData[] = null;
        int rowCount = '0';
        int columnCount = '0';
        DateCell dc = null;
        
        try {
                ws = new WorkbookSettings();
                ws.setLocale(new Locale("en", "EN"));
                workbook = Workbook.getWorkbook(fileInputStream, ws);

                //Getting Default Sheet i.e. 0
                s = workbook.getSheet(0);

               //Total Total No Of Rows in Sheet, will return you no of rows that are occupied with some data
                System.out.println("Total Rows inside Sheet:" + s.getRows());
                rowCount = s.getRows();

                //Total Total No Of Columns in Sheet
                System.out.println("Total Column inside Sheet:" + s.getColumns());
                columnCount = s.getColumns();

                //Reading Individual Row Content
                for (int i = 1; i < rowCount; i++) 
                {
                        //Get Individual Row
                        rowData = s.getRow(i);
                        if (rowData[0].getContents().length() != 0) 
                        { 
                            Vector vectRows = new Vector();
                            // the first date column must not null
                                for (int j = 0; j < columnCount; j++) 
                                {
                                    vectRows.add(rowData[j].getContents());
                                }
                            vectValues.add(vectRows);
                        }
                }
                workbook.close();			
        } catch (IOException e) {
                e.printStackTrace();
        } catch (BiffException e) {
                e.printStackTrace();
        }
    }
    
    
    public Vector getValues()
    {
        return vectValues;
    }
    
    

}

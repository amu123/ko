/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.entity.provider;

/**
 *
 * @author JohanL
 */
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import neo.manager.DisciplineTypes;
import neo.product.utils.NeoWebService;


public class DisciplineTypeTable extends AbstractCellEditor implements TableCellEditor {

    Map<String, String> discTypeMap = new HashMap<String, String>();
    private JComboBox cbComponent;

    public DisciplineTypeTable() {

        List<DisciplineTypes> disciplineTypes = NeoWebService.getNeoManagerBeanService().fetchAllDisciplineTypes();
        //DisciplineTypes discType = new DisciplineTypes();
        ArrayList stArr = new ArrayList();
        int i = 0;

        for (Iterator it = disciplineTypes.iterator(); it.hasNext();) {

            DisciplineTypes discType = (DisciplineTypes) it.next();

            stArr.add(discType.getDisciplineDesc());

            discTypeMap.put(discType.getDisciplineDesc(), discType.getDisciplineType());


            i++;

        }
        cbComponent = new JComboBox(stArr.toArray());
    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {
        return cbComponent;
    }

    public Object getCellEditorValue() {
        return cbComponent.getSelectedItem();
    }
    public Map getDiscTypeMap(){
        return discTypeMap;
    }
    JComboBox getCbComponent() {
        return cbComponent;
    }
}
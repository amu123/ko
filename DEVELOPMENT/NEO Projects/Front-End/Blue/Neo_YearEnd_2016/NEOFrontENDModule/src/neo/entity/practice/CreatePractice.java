/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * CreatePractice.java
 *
 * Created on 2009/05/05, 03:28:32
 */
package neo.entity.practice;

import com.jidesoft.popup.JidePopup;
import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import javax.swing.table.TableCellEditor;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.entity.company.CreateCompanyAddressDetails;
import neo.entity.person.CreatePerson;
import neo.entity.provider.CreateRolesToProvider;

import neo.entity.search.SearchCompany1;
import neo.entity.search.SearchDocsFrame;

import neo.manager.CompanyDetails;
import neo.manager.ContractType;
import neo.manager.DiscountBand;

import neo.manager.DispensingStatus;
import neo.manager.CompanyContract;
//import neo.manager.EntityDetails;
import neo.manager.EntitySearchCriteria;
import neo.manager.EntityType;
import neo.manager.LogicalOperandType;
import neo.manager.PracticeDispensing;
import neo.manager.PracticeStatus;
import neo.manager.StatusType;
import neo.product.utils.NeoWebService;
import neo.utils.tables.TableFormater;
import za.co.AgilityTechnologies.DockingFramework;
import za.co.AgilityTechnologies.LeftMenu.EntityActionsFrame;

import za.co.AgilityTechnologies.TableEditors.AndorOrProductTable;
import za.co.AgilityTechnologies.TableEditors.ApplyToType;
import za.co.AgilityTechnologies.TableEditors.DiscountType;
import za.co.AgilityTechnologies.TableEditors.OptionType;
import za.co.AgilityTechnologies.TableEditors.SelectProductTable;

/**
 *
 * @author BharathP
 */
public class CreatePractice extends javax.swing.JPanel implements FocusListener {

    protected static int entityid = 0;
    protected static String compName = null;
    private DatatypeFactory _datatypeFactory;
    private JComboBox productBox = new SelectProductTable().getCbComponent();
    private TableCellEditor productComboCell = new DefaultCellEditor(productBox);
    private JComboBox optionBox = new SelectProductTable().getCbComponent();
    private TableCellEditor optionComboCell = new DefaultCellEditor(optionBox);

     //NEW VARIABLES TO HANDLE LOGIC OF SEARCH AND ADDING OF ROLES
    public static boolean addRoleMode = false;
    public static boolean searched = false;

    /** Creates new form CreatePractice */
    public CreatePractice() {
        initComponents();

        //jScrollPane1.getVerticalScrollBar().setBlockIncrement(62);

        // jScrollPane1.getVerticalScrollBar().setUnitIncrement(62);

        // jScrollPane1.getViewport().setScrollMode(JViewport.BACKINGSTORE_SCROLL_MODE);

        // jScrollPane3.getVerticalScrollBar().setBlockIncrement(62);

        //jScrollPane3.getVerticalScrollBar().setUnitIncrement(62);

        //jScrollPane3.getViewport().setScrollMode(JViewport.BACKINGSTORE_SCROLL_MODE);

        PracticeNumberlabel.setVisible(false);
        entityID.setVisible(false);
        EntityContractNumber.setVisible(false);
        flagRole.setVisible(false);
        relationshipType.setVisible(false);
        addRole.setVisible(false);
        ContractEntityType.setVisible(false);

        dispensingStatusTable.getColumnModel().getColumn(1).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });

        dispensingStatusTable.getColumnModel().getColumn(2).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });
        dispensingStatusTable.getColumnModel().getColumn(5).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });

        dispensingStatusTable.getColumnModel().getColumn(6).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });
        discountBandsTable.getColumnModel().getColumn(7).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });

        discountBandsTable.getColumnModel().getColumn(8).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bodyPanel = new javax.swing.JPanel();
        PracticeNumberlabel = new com.jidesoft.swing.StyledLabel();
        entityID = new javax.swing.JTextField();
        styledLabel2 = new com.jidesoft.swing.StyledLabel();
        status = new com.jidesoft.combobox.ListComboBox();
        styledLabel4 = new com.jidesoft.swing.StyledLabel();
        companyName = new javax.swing.JTextField();
        styledLabel8 = new com.jidesoft.swing.StyledLabel();
        styledLabel9 = new com.jidesoft.swing.StyledLabel();
        contractedPractice = new com.jidesoft.combobox.ListComboBox();
        fromdateStatus = new com.jidesoft.combobox.DateComboBox();
        styledLabel12 = new com.jidesoft.swing.StyledLabel();
        practiceCategroy = new com.jidesoft.combobox.ListComboBox();
        styledLabel13 = new com.jidesoft.swing.StyledLabel();
        cashPractice = new com.jidesoft.combobox.ListComboBox();
        styledLabel6 = new com.jidesoft.swing.StyledLabel();
        regNumber = new javax.swing.JTextField();
        styledLabel7 = new com.jidesoft.swing.StyledLabel();
        vatNumber = new javax.swing.JTextField();
        searchPractice = new com.jidesoft.swing.JideButton();
        NextButton = new com.jidesoft.swing.JideButton();
        cancelCompany = new com.jidesoft.swing.JideButton();
        savePractice = new com.jidesoft.swing.JideButton();
        jPanel1 = new javax.swing.JPanel();
        styledLabel3 = new com.jidesoft.swing.StyledLabel();
        jPanel2 = new javax.swing.JPanel();
        styledLabel20 = new com.jidesoft.swing.StyledLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        dispensingStatusTable = new com.jidesoft.grid.JideTable();
        jPanel4 = new javax.swing.JPanel();
        styledLabel22 = new com.jidesoft.swing.StyledLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        discountBandsTable = new com.jidesoft.grid.JideTable();
        addRole = new com.jidesoft.swing.JideButton();
        relationshipType = new javax.swing.JTextField();
        flagRole = new javax.swing.JTextField();
        EntityContractNumber = new javax.swing.JTextField();
        ContractEntityType = new javax.swing.JTextField();
        searchPerson1 = new com.jidesoft.swing.JideButton();

        setBackground(new java.awt.Color(235, 235, 235));
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(166, 213, 224)));

        bodyPanel.setBackground(new java.awt.Color(235, 235, 235));

        PracticeNumberlabel.setText("Practice Number");
        PracticeNumberlabel.setFont(new java.awt.Font("Arial", 0, 11));

        styledLabel2.setText("Status");
        styledLabel2.setFont(new java.awt.Font("Arial", 0, 11));

        neo.manager.StatusType[] sTypes=neo.manager.StatusType.values();
        status.setModel(new javax.swing.DefaultComboBoxModel(sTypes));

        styledLabel4.setText("Practice Name");

        styledLabel8.setText("Practice Active From");

        styledLabel9.setText("Contracted Practice");

        contractedPractice.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Y", "N" }));

        styledLabel12.setText("Network Practice Category");

        practiceCategroy.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Owner", "Part of Network", "Both" }));

        styledLabel13.setText("Cash Practice?");

        cashPractice.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Y", "N" }));

        styledLabel6.setText("Registration Number");

        styledLabel7.setText("VAT Number");

        javax.swing.GroupLayout bodyPanelLayout = new javax.swing.GroupLayout(bodyPanel);
        bodyPanel.setLayout(bodyPanelLayout);
        bodyPanelLayout.setHorizontalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bodyPanelLayout.createSequentialGroup()
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(bodyPanelLayout.createSequentialGroup()
                        .addComponent(PracticeNumberlabel, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                        .addGap(70, 70, 70)
                        .addComponent(entityID, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(companyName, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, bodyPanelLayout.createSequentialGroup()
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(bodyPanelLayout.createSequentialGroup()
                                    .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(styledLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(styledLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
                                        .addComponent(styledLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(styledLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(styledLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                .addGroup(bodyPanelLayout.createSequentialGroup()
                                    .addComponent(styledLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(58, 58, 58)))
                            .addGroup(bodyPanelLayout.createSequentialGroup()
                                .addComponent(styledLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(55, 55, 55)))
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(fromdateStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(status, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cashPractice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(practiceCategroy, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(contractedPractice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(vatNumber)
                            .addComponent(regNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE))))
                .addGap(18, 18, 18))
        );
        bodyPanelLayout.setVerticalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(entityID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PracticeNumberlabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(companyName)
                    .addComponent(styledLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(regNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(vatNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(contractedPractice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(practiceCategroy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                    .addComponent(cashPractice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(status, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fromdateStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        searchPractice.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        searchPractice.setText("SEARCH");
        searchPractice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchPracticeActionPerformed(evt);
            }
        });

        NextButton.setBorder(null);
        NextButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/arrow2_right_green.png"))); // NOI18N
        NextButton.setToolTipText("Save and Next");
        NextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NextButtonActionPerformed(evt);
            }
        });

        cancelCompany.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        cancelCompany.setText("CANCEL");
        cancelCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelCompanyActionPerformed(evt);
            }
        });

        savePractice.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        savePractice.setText("SAVE");
        savePractice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savePracticeActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(138, 177, 230));

        styledLabel3.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel3.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel3.setText("CREATE PRACTICE CONTRACT");
        styledLabel3.setFont(new java.awt.Font("Arial", 1, 12));
        jPanel1.add(styledLabel3);

        jPanel2.setBackground(new java.awt.Color(138, 177, 230));

        styledLabel20.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel20.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel20.setText("DISPENSING STATUS");
        styledLabel20.setFont(new java.awt.Font("Arial", 1, 12));
        jPanel2.add(styledLabel20);

        dispensingStatusTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Practice Status", "Practice Active From", "Practice Active To", "Dispensing Status", "Dispensing Number", "Date Dispensing Active From", "Date Dispensing Active To"
            }
        ));
        jScrollPane1.setViewportView(dispensingStatusTable);
        dispensingStatusTable.getColumnModel().getColumn(0).setCellEditor(new PracticeStatusTable());
        dispensingStatusTable.getColumnModel().getColumn(1).setCellEditor(new neo.entity.DatePickerCellTable());
        dispensingStatusTable.getColumnModel().getColumn(2).setCellEditor(new neo.entity.DatePickerCellTable());
        dispensingStatusTable.getColumnModel().getColumn(3).setCellEditor(new PracticeDispensingStatusTable());
        dispensingStatusTable.getColumnModel().getColumn(5).setCellEditor(new neo.entity.DatePickerCellTable());
        dispensingStatusTable.getColumnModel().getColumn(6).setCellEditor(new neo.entity.DatePickerCellTable());

        jPanel4.setBackground(new java.awt.Color(138, 177, 230));

        styledLabel22.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel22.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel22.setText("CREATE DISCOUNT BANDS");
        styledLabel22.setFont(new java.awt.Font("Arial", 1, 12));
        jPanel4.add(styledLabel22);

        discountBandsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Apply To", "Discount Type", "Percentage", "And/Or", "Amount", "Days From", "Days To", "Date Effective From", "Date Effective To", "Product Name", "Option Name"
            }
        ));
        jScrollPane3.setViewportView(discountBandsTable);
        discountBandsTable.getColumnModel().getColumn(0).setCellEditor(new ApplyToType());
        discountBandsTable.getColumnModel().getColumn(1).setCellEditor(new DiscountType());
        discountBandsTable.getColumnModel().getColumn(3).setCellEditor(new AndorOrProductTable());
        discountBandsTable.getColumnModel().getColumn(7).setCellEditor(new neo.entity.DatePickerCellTable());
        discountBandsTable.getColumnModel().getColumn(8).setCellEditor(new neo.entity.DatePickerCellTable());
        discountBandsTable.getColumnModel().getColumn(9).setCellEditor(productComboCell);
        discountBandsTable.getColumnModel().getColumn(10).setMinWidth(0);
        discountBandsTable.getColumnModel().getColumn(10).setPreferredWidth(0);
        discountBandsTable.getColumnModel().getColumn(10).setMaxWidth(0);
        discountBandsTable.getColumnModel().getColumn(10).setCellEditor(optionComboCell);

        addRole.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        addRole.setText("ADD ROLE");
        addRole.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addRoleActionPerformed(evt);
            }
        });

        searchPerson1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        searchPerson1.setText("FIND DOCS");
        searchPerson1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchDocsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(flagRole, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(EntityContractNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(relationshipType, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ContractEntityType, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(105, 105, 105)
                        .addComponent(bodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 755, Short.MAX_VALUE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 765, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 765, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 765, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 755, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(searchPractice, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(80, 80, 80)
                .addComponent(savePractice, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(80, 80, 80)
                .addComponent(addRole, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(80, 80, 80)
                .addComponent(cancelCompany, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(searchPerson1, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
                .addComponent(NextButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(48, 48, 48))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(EntityContractNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(flagRole, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(relationshipType, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ContractEntityType, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(bodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(searchPerson1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(searchPractice, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                        .addComponent(savePractice, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                        .addComponent(addRole, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                        .addComponent(cancelCompany, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE))
                    .addComponent(NextButton, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(104, 104, 104))
        );
    }// </editor-fold>//GEN-END:initComponents

    protected XMLGregorianCalendar convert(Date date) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getDatatypeFactory().newXMLGregorianCalendar(calendar);
    }

    protected XMLGregorianCalendar StringToXMLDate(String sdate) throws ParseException {

        DateFormat dformat = new SimpleDateFormat("yyyy/MM/dd");
        Date Xdate = dformat.parse(sdate);

        return convert(Xdate);
    }

    private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                Logger.getLogger(CreatePerson.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


        return _datatypeFactory;
    }

    @SuppressWarnings("static-access")
    private void searchPracticeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchPracticeActionPerformed

        if (addRoleMode) {
            searched = true;
        }

        neo.manager.EntitySearchCriteria companySearch = new EntitySearchCriteria();
        companySearch.setEntityType(neo.manager.EntityType.COMPANY);
        companySearch.setContractType(neo.manager.ContractType.PRACTICE);
        companySearch.setName(companyName.getText());
        companySearch.setRegistrationNumber(regNumber.getText());
        companySearch.setVatNumber(vatNumber.getText());

        java.util.List<CompanyDetails> result = NeoWebService.getNeoManagerBeanService().findCompaniesWithSearchCriteria(companySearch);

        System.out.println("Count of Entity Details" + result.size());
        int rowCount = result.size();

        if (rowCount == 0) {
            JOptionPane.showMessageDialog(null, "No Record found", "Information", JOptionPane.INFORMATION_MESSAGE);
        } else {

            final JidePopup popup = new JidePopup();
            // JFrame frame = new JFrame("Search Results");

            SearchCompany1 frame = new SearchCompany1();


            frame.setSize(500, 500);
            popup.setMovable(true);

            String colName[] = {"Entity Type", "Entity ID", "Company Name", "Reg Number", "VAT Number"};
            DefaultTableModel model = new DefaultTableModel(rowCount, colName.length);
            model.setColumnIdentifiers(colName);
            SearchCompany1.ContractEntityNumber.setVisible(false);
            SearchCompany1.FlagSearchRole.setVisible(false);
            SearchCompany1.contractRelationship.setVisible(false);

            if (flagRole.getText().equals("Y")) {
                frame.FlagSearchRole.setText("Y");
                frame.ContractEntityNumber.setText(EntityContractNumber.getText());
                frame.contractRelationship.setText(relationshipType.getText());
                frame.contractEntityType.setText(ContractEntityType.getText());
            }
            frame.resultTable.setModel(model);

            int i = 0;
            for (Iterator it = result.iterator(); it.hasNext();) {
                neo.manager.CompanyDetails nP = (neo.manager.CompanyDetails) it.next();
                frame.resultTable.setValueAt("PRACTICE", i, 0);
                frame.resultTable.setValueAt(nP.getEntityId(), i, 1);
                frame.resultTable.setValueAt(nP.getName(), i, 2);
                frame.resultTable.setValueAt(nP.getRegistrationNumber(), i, 3);
                frame.resultTable.setValueAt(nP.getVatNumber(), i, 4);
                i++;

            }


            frame.setVisible(true);
            popup.showPopup();

        }

}//GEN-LAST:event_searchPracticeActionPerformed

    public static void practicedisplayFields(int entityContactNo) {


        CompanyContract econtract = NeoWebService.getNeoManagerBeanService().getCompanyContract(entityContactNo);

        EntityActionsFrame.GLOBALCompanyContract = econtract;

        CompanyDetails edeatils = EntityActionsFrame.GLOBALCompanyContract.getEntityDetails();
        //econtract.getEntityDetails();
        Integer contractId = edeatils.getEntityId();

        System.out.println(contractId);
        System.out.println("entityContactNo: " + entityContactNo);

        entityID.setText(new Integer(entityContactNo).toString());

        System.out.println("entityID: " + entityID);
        companyName.setText(edeatils.getName());
        vatNumber.setText(edeatils.getVatNumber());
        regNumber.setText(edeatils.getRegistrationNumber());
        java.util.Date adate = (java.util.Date) edeatils.getFromDate().toGregorianCalendar().getTime();
        fromdateStatus.setDate(adate);
        status.setSelectedItem(edeatils.getStatus());
        contractedPractice.setSelectedItem(edeatils.getContractedPractice());
        practiceCategroy.setSelectedItem(edeatils.getNetworkPracticeCat());
        cashPractice.setSelectedItem(edeatils.getCashPractice());


        List<neo.manager.PracticeDispensing> dispensingDetails = edeatils.getPracticeDispensing();

        System.out.println("Total Practice Dispencing returned = " + dispensingDetails.size());

        int row = 0;

        for (PracticeDispensing dipensingValue : dispensingDetails) {

            dispensingStatusTable.setValueAt((PracticeStatus) dipensingValue.getPracticeStatus(), row, 0);

            dispensingStatusTable.setValueAt(dipensingValue.getPracticeActiveDateFrom().toGregorianCalendar().getTime(), row, 1);
            dispensingStatusTable.setValueAt(dipensingValue.getPracticeActiveToDate().toGregorianCalendar().getTime(), row, 2);

            dispensingStatusTable.setValueAt((DispensingStatus) dipensingValue.getDispensingStatus(), row, 3);
            dispensingStatusTable.setValueAt((String) dipensingValue.getDispensingNumber(), row, 4);

            dispensingStatusTable.setValueAt(dipensingValue.getDispenseFromDate().toGregorianCalendar().getTime(), row, 5);
            dispensingStatusTable.setValueAt(dipensingValue.getDispenseToDate().toGregorianCalendar().getTime(), row, 6);

            row++;
        }

        int row2 = 0;
        List<DiscountBand> discList = edeatils.getDiscoundBands();
        try {
            for (DiscountBand discBandDisplay : discList) {

                discountBandsTable.setValueAt((neo.manager.ApplyToType) discBandDisplay.getApplyTo(), row2, 0);
                discountBandsTable.setValueAt(discBandDisplay.getDiscountType(), row2, 1);
                discountBandsTable.setValueAt(discBandDisplay.getPercentage(), row2, 2);
                discountBandsTable.setValueAt((neo.manager.LogicalOperandType) discBandDisplay.getLogicalOperand(), row2, 3);
                discountBandsTable.setValueAt(discBandDisplay.getAmount(), row2, 4);
                discountBandsTable.setValueAt(discBandDisplay.getDaysCountFrom(), row2, 5);
                discountBandsTable.setValueAt(discBandDisplay.getDaysCountTo(), row2, 6);
                discountBandsTable.setValueAt(discBandDisplay.getFromDate().toGregorianCalendar().getTime(), row2, 7);
                discountBandsTable.setValueAt(discBandDisplay.getToDate().toGregorianCalendar().getTime(), row2, 8);

                String prodName = discBandDisplay.getProduct().getProductName();
                int prodId = discBandDisplay.getProduct().getProductId();
//                neo.manager.Option optDesc = (Option) NeoWebService.getNeoManagerBeanService().findOptionsForProduct(prodId);
                discountBandsTable.setValueAt(prodName, row2, 9);
                // discountBandsTable.setValueAt(optDesc, row2, 10);

                row2++;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error message: " + e.getMessage());
        }

    }

    public int saveEntity() {

        compName = companyName.getText();

        entityid = NeoWebService.getNeoManagerBeanService().saveEntity(EntityType.COMPANY);
        System.out.println(entityid + " id type");
        CompanyDetails ed = new CompanyDetails();
        ed.setName(compName);

        ed.setRegistrationNumber(regNumber.getText());
        ed.setVatNumber(vatNumber.getText());
        ed.setStatus((StatusType) status.getSelectedItem());
        ed.setFromDate(convert(fromdateStatus.getDate()));
        ed.setContractedPractice((String) contractedPractice.getSelectedItem());
        ed.setNetworkPracticeCat((String) practiceCategroy.getSelectedItem());
        ed.setCashPractice((String) cashPractice.getSelectedItem());
        entityID.setText(new Integer(entityid).toString());

        List<neo.manager.PracticeDispensing> dispensingDetails = ed.getPracticeDispensing();

        int selectedRows = dispensingStatusTable.getModel().getRowCount();
        int selectedDBRows = discountBandsTable.getModel().getRowCount();
        try {
            for (int i = 0; i < selectedRows; i++) {
                if (dispensingStatusTable.getValueAt(i, 0) != null) {
                    PracticeDispensing dispensList = new PracticeDispensing();
                    dispensList.setPracticeStatus((PracticeStatus) dispensingStatusTable.getValueAt(i, 0));
                    dispensList.setPracticeActiveDateFrom(TableFormater.formatDateFromTable(dispensingStatusTable.getValueAt(i, 1).toString()));
                    dispensList.setPracticeActiveToDate(TableFormater.formatDateFromTable(dispensingStatusTable.getValueAt(i, 2).toString()));
                    dispensList.setDispensingStatus((DispensingStatus) dispensingStatusTable.getValueAt(i, 3));
                    dispensList.setDispensingNumber(dispensingStatusTable.getValueAt(i, 4).toString());
                    dispensList.setDispenseFromDate(TableFormater.formatDateFromTable(dispensingStatusTable.getValueAt(i, 5).toString()));
                    dispensList.setDispenseToDate(TableFormater.formatDateFromTable(dispensingStatusTable.getValueAt(i, 6).toString()));


                    dispensingDetails.add(dispensList);
                } else {
                    break;
                }
            }
            System.out.println("dispensingDetails count: " + dispensingDetails.size());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error message: " + e.getMessage());

        }


        List<DiscountBand> discList = ed.getDiscoundBands();
        try {
            for (int i = 0; i < selectedDBRows; i++) {
                if (discountBandsTable.getValueAt(i, 0) != null) {

                    //String licenseType = (String)LicensingTable.getValueAt(i, 0);

                    neo.manager.ApplyToType applyTo = (neo.manager.ApplyToType) discountBandsTable.getValueAt(i, 0);
                    neo.manager.DiscountType discType = (neo.manager.DiscountType) discountBandsTable.getValueAt(i, 1);
                    double perc = Double.parseDouble(discountBandsTable.getValueAt(i, 2).toString());
                    String logicStr = (String) discountBandsTable.getValueAt(i, 3);
                    double amount = Double.parseDouble(discountBandsTable.getValueAt(i, 4).toString());
                    int daysCountFrom = Integer.parseInt(discountBandsTable.getValueAt(i, 5).toString());
                    int daysCountTo = Integer.parseInt(discountBandsTable.getValueAt(i, 6).toString());
                    String effectiveFromDate = discountBandsTable.getValueAt(i, 7).toString();
                    String effectiveFromTo = discountBandsTable.getValueAt(i, 8).toString();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                    Date effectFrom = dateFormat.parse(effectiveFromDate);
                    Date effectTo = dateFormat.parse(effectiveFromTo);
                    String prodName = discountBandsTable.getValueAt(i, 9).toString();
                    //String optName = discountBandsTable.getValueAt(i, 10).toString();
                    neo.manager.Product prod = NeoWebService.getNeoManagerBeanService().findProductWithDesc(prodName);

                    DiscountBand discBand = new DiscountBand();

                    discBand.setApplyTo(applyTo);
                    discBand.setDiscountType(discType);
                    discBand.setPercentage(perc);
                    discBand.setLogicalOperand(LogicalOperandType.valueOf(logicStr));
                    discBand.setAmount(amount);
                    discBand.setDaysCountFrom(daysCountFrom);
                    discBand.setDaysCountTo(daysCountTo);
                    discBand.setFromDate(convert(effectFrom));
                    discBand.setToDate(convert(effectTo));
                    discBand.setProduct(prod);

                    discList.add(discBand);

                } else {
                    break;
                }
            }
            System.out.println("dispensingDetails count: " + dispensingDetails.size());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error message: " + e.getMessage());

        }

        ed.setContractType(ContractType.PRACTICE);

        //NEW CODE FROM LESLIE
        int result = NeoWebService.getNeoManagerBeanService().saveCompanyDetails(entityid, ed);

        if (result != 0) {
            if (searched==false) {
                EntityActionsFrame.GLOBALCompanyContract.setEntityId(entityid);
                EntityActionsFrame.GLOBALCompanyContract.setEntityDetails(ed);
                EntityActionsFrame.GLOBALCompanyContract.getEntityDetails().setEntityId(entityid);
                EntityActionsFrame.GLOBALCompanyContract.getEntityDetails().setEntityDetailsID(result);
            }
        }
        return result;
    }

    public int updateEntity(int entityidno) {

        compName = companyName.getText();

        entityid = entityidno;
        System.out.println(entityid + " id type");
        CompanyDetails ed = new CompanyDetails();
        ed.setName(compName);
        //ed.setSurname(compName);
        ed.setRegistrationNumber(regNumber.getText());
        ed.setVatNumber(vatNumber.getText());
        ed.setStatus((StatusType) status.getSelectedItem());
        ed.setFromDate(convert(fromdateStatus.getDate()));
        ed.setContractedPractice((String) contractedPractice.getSelectedItem());
        ed.setNetworkPracticeCat((String) practiceCategroy.getSelectedItem());
        ed.setCashPractice((String) cashPractice.getSelectedItem());

        List<neo.manager.PracticeDispensing> dispensingDetails = ed.getPracticeDispensing();

        int selectedRows = dispensingStatusTable.getModel().getRowCount();
        int selectedDBRows = discountBandsTable.getModel().getRowCount();
        try {
            for (int i = 0; i < selectedRows; i++) {
                if (dispensingStatusTable.getValueAt(i, 0) != null) {
                    PracticeDispensing dispensList = new PracticeDispensing();
                    dispensList.setPracticeStatus((PracticeStatus) dispensingStatusTable.getValueAt(i, 0));

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

                    Date effectFrom = dateFormat.parse(dispensingStatusTable.getValueAt(i, 5).toString());
                    Date effectTo = dateFormat.parse(dispensingStatusTable.getValueAt(i, 6).toString());
                    dispensList.setDispenseFromDate(convert(effectFrom));
                    dispensList.setDispenseToDate(convert(effectTo));

                    Date practiceEffectFrom = dateFormat.parse(dispensingStatusTable.getValueAt(i, 1).toString());
                    Date practiceEffectTo = dateFormat.parse(dispensingStatusTable.getValueAt(i, 2).toString());
                    dispensList.setPracticeActiveDateFrom(convert(practiceEffectFrom));
                    dispensList.setPracticeActiveToDate(convert(practiceEffectTo));

                    dispensList.setDispensingStatus((DispensingStatus) dispensingStatusTable.getValueAt(i, 3));
                    dispensList.setDispensingNumber(dispensingStatusTable.getValueAt(i, 4).toString());
                    dispensingDetails.add(dispensList);
                } else {
                    break;
                }
            }
            System.out.println("dispensingDetails count: " + dispensingDetails.size());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error message: " + e.getMessage());

        }


        List<DiscountBand> discList = ed.getDiscoundBands();
        try {
            for (int i = 0; i < selectedDBRows; i++) {
                if (discountBandsTable.getValueAt(i, 0) != null) {

                    //String licenseType = (String)LicensingTable.getValueAt(i, 0);

                    neo.manager.ApplyToType applyTo = (neo.manager.ApplyToType) discountBandsTable.getValueAt(i, 0);
                    neo.manager.DiscountType discType = (neo.manager.DiscountType) discountBandsTable.getValueAt(i, 1);
                    double perc = Double.parseDouble(discountBandsTable.getValueAt(i, 2).toString());
                    String logicStr = (String) discountBandsTable.getValueAt(i, 3);
                    double amount = Double.parseDouble(discountBandsTable.getValueAt(i, 4).toString());
                    int daysCountFrom = Integer.parseInt(discountBandsTable.getValueAt(i, 5).toString());
                    int daysCountTo = Integer.parseInt(discountBandsTable.getValueAt(i, 6).toString());
                    String effectiveFromDate = discountBandsTable.getValueAt(i, 7).toString();
                    String effectiveFromTo = discountBandsTable.getValueAt(i, 8).toString();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                    Date effectFrom = dateFormat.parse(effectiveFromDate);
                    Date effectTo = dateFormat.parse(effectiveFromTo);
                    String prodName = discountBandsTable.getValueAt(i, 9).toString();
                    //String optName = discountBandsTable.getValueAt(i, 10).toString();
                    neo.manager.Product prod = NeoWebService.getNeoManagerBeanService().findProductWithDesc(prodName);

                    DiscountBand discBand = new DiscountBand();

                    discBand.setApplyTo(applyTo);
                    discBand.setDiscountType(discType);
                    discBand.setPercentage(perc);
                    discBand.setLogicalOperand(LogicalOperandType.valueOf(logicStr));
                    discBand.setAmount(amount);
                    discBand.setDaysCountFrom(daysCountFrom);
                    discBand.setDaysCountTo(daysCountTo);
                    discBand.setFromDate(convert(effectFrom));
                    discBand.setToDate(convert(effectTo));
                    discBand.setProduct(prod);

                    discList.add(discBand);

                } else {
                    break;
                }
            }
            System.out.println("dispensingDetails count: " + dispensingDetails.size());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error message: " + e.getMessage());

        }

        ed.setContractType(ContractType.PRACTICE);
        int result = NeoWebService.getNeoManagerBeanService().saveCompanyDetails(entityid, ed);

        if (result != 0) {
            EntityActionsFrame.GLOBALCompanyContract.setEntityDetails(ed);
            EntityActionsFrame.GLOBALCompanyContract.getEntityDetails().setEntityId(entityid);
            EntityActionsFrame.GLOBALCompanyContract.getEntityDetails().setEntityDetailsID(result);
        }
        return result;
    }

    private void NextButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NextButtonActionPerformed


        if (entityID.getText().isEmpty() || entityID.getText() == null || entityID.getText().equals("")) {
            if (saveEntity() != 0) {
                JOptionPane.showMessageDialog(null, "Practice successfully created", "Information", JOptionPane.INFORMATION_MESSAGE);
                EntityActionsFrame.entityActionList.setSelectedIndex(1);
                String entid = new Integer(entityid).toString();
                CreateCompanyAddressDetails.companyAddressDisplayFields();
                System.out.println(entid);
            } else {
                JOptionPane.showMessageDialog(null, "Please Enter All the Details", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            String entityNo = entityID.getText();
            int entitycontactNo = Integer.parseInt(entityNo);
            CompanyContract ec = NeoWebService.getNeoManagerBeanService().getCompanyContract(entitycontactNo);
            if (ec != null) {
                EntityActionsFrame.entityActionList.setSelectedIndex(1);
                CreateCompanyAddressDetails.companyAddressDisplayFields();
            }
        }
}//GEN-LAST:event_NextButtonActionPerformed

    private void cancelCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelCompanyActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_cancelCompanyActionPerformed

    private void savePracticeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savePracticeActionPerformed
        // TODO add your handling code here:

        if (entityID.getText().isEmpty() || entityID.getText() == null || entityID.getText().equals("")) {


            if (saveEntity() != 0) {
                JOptionPane.showMessageDialog(null, "Practice successfully created", "Information", JOptionPane.INFORMATION_MESSAGE);

                EntityActionsFrame.entityActionList.setSelectedIndex(1);
                String entid = new Integer(entityid).toString();
                CreateCompanyAddressDetails.entityID.setText(entid);
                CreateCompanyAddressDetails.companyName.setText(compName);

                System.out.println(entid);

            } else {
                JOptionPane.showMessageDialog(null, "Please Enter All the Details", "Error", JOptionPane.ERROR_MESSAGE);
            }

        } else {
            String entityNo = entityID.getText();
            int entitycontactNo = Integer.parseInt(entityNo);
            CompanyContract ec = NeoWebService.getNeoManagerBeanService().getCompanyContract(entitycontactNo);
            if (ec != null) {
                if (updateEntity(entitycontactNo) != 0) {
                    JOptionPane.showMessageDialog(null, "Practice successfully updated", "Information", JOptionPane.INFORMATION_MESSAGE);

                }
            } else {
                JOptionPane.showMessageDialog(null, "updating failed", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
}//GEN-LAST:event_savePracticeActionPerformed

    private void addRoleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addRoleActionPerformed
        if (addRoleMode) {
            if (searched) {
                int targetId = Integer.parseInt(entityID.getText().toString());

                System.out.println("targetId" + targetId);
                boolean addRoleresult =  NeoWebService.getNeoManagerBeanService().addRoles(Integer.parseInt(EntityContractNumber.getText().toString()), targetId, neo.manager.RoleType.fromValue(relationshipType.getText().toString()), neo.manager.TargetObjectType.ENTITY);

                if (addRoleresult) {
                    JOptionPane.showMessageDialog(null, "Relationship  successfully created", "Information", JOptionPane.INFORMATION_MESSAGE);
                } else {

                    JOptionPane.showMessageDialog(null, "Unable to create relationship", "Information", JOptionPane.INFORMATION_MESSAGE);
                }
            } else {
             if (saveEntity() != 0) {

                System.out.println("EntityContractNumber.getText().toString()" + EntityContractNumber.getText().toString());
                System.out.println("neo.manager.RoleType.fromValue(relationshipType.getText()" + neo.manager.RoleType.fromValue(relationshipType.getText().toString()));
                System.out.println("neo.manager.TargetObjectType.ENTITY" + neo.manager.TargetObjectType.ENTITY);

                int targetId = Integer.parseInt(entityID.getText().toString());

                System.out.println("targetId" + targetId);
                boolean addRoleresult =
                        NeoWebService.getNeoManagerBeanService().addRoles(Integer.parseInt(EntityContractNumber.getText().toString()), targetId, neo.manager.RoleType.fromValue(relationshipType.getText().toString()), neo.manager.TargetObjectType.ENTITY);
                if (addRoleresult) {
                    JOptionPane.showMessageDialog(null, "Relationship  successfully created", "Information", JOptionPane.INFORMATION_MESSAGE);
                } else {

                    JOptionPane.showMessageDialog(null, "Unable to create relationship", "Information", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }
    }
    if (relationshipType.getText().equals("PROVIDER")) {
        DockingFramework.EntityCreateRolesToProviderActions();
        CreateRolesToProvider.personRolesDisplayFields( );

    } else if(relationshipType.getText().equals("PRACTICE")) {
        DockingFramework.EntityCreateRolesToPracticeActions();
        CreateRolesToPractice.personRolesDisplayFields();
    }

    searched = false;
    addRoleMode = false;

}//GEN-LAST:event_addRoleActionPerformed

    private void searchDocsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchDocsActionPerformed
        SearchDocsFrame frame = new SearchDocsFrame(EntityActionsFrame.GLOBALCompanyContract.getEntityDetails().getEntityId());
}//GEN-LAST:event_searchDocsActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTextField ContractEntityType;
    public static javax.swing.JTextField EntityContractNumber;
    public static com.jidesoft.swing.JideButton NextButton;
    public static com.jidesoft.swing.StyledLabel PracticeNumberlabel;
    public static com.jidesoft.swing.JideButton addRole;
    private javax.swing.JPanel bodyPanel;
    private com.jidesoft.swing.JideButton cancelCompany;
    public static com.jidesoft.combobox.ListComboBox cashPractice;
    public static javax.swing.JTextField companyName;
    public static com.jidesoft.combobox.ListComboBox contractedPractice;
    public static com.jidesoft.grid.JideTable discountBandsTable;
    public static com.jidesoft.grid.JideTable dispensingStatusTable;
    public static javax.swing.JTextField entityID;
    public static javax.swing.JTextField flagRole;
    public static com.jidesoft.combobox.DateComboBox fromdateStatus;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    public static com.jidesoft.combobox.ListComboBox practiceCategroy;
    public static javax.swing.JTextField regNumber;
    public static javax.swing.JTextField relationshipType;
    public static com.jidesoft.swing.JideButton savePractice;
    private com.jidesoft.swing.JideButton searchPerson1;
    private com.jidesoft.swing.JideButton searchPractice;
    public static com.jidesoft.combobox.ListComboBox status;
    private com.jidesoft.swing.StyledLabel styledLabel12;
    private com.jidesoft.swing.StyledLabel styledLabel13;
    private com.jidesoft.swing.StyledLabel styledLabel2;
    private com.jidesoft.swing.StyledLabel styledLabel20;
    private com.jidesoft.swing.StyledLabel styledLabel22;
    private com.jidesoft.swing.StyledLabel styledLabel3;
    private com.jidesoft.swing.StyledLabel styledLabel4;
    private com.jidesoft.swing.StyledLabel styledLabel6;
    private com.jidesoft.swing.StyledLabel styledLabel7;
    private com.jidesoft.swing.StyledLabel styledLabel8;
    private com.jidesoft.swing.StyledLabel styledLabel9;
    public static javax.swing.JTextField vatNumber;
    // End of variables declaration//GEN-END:variables

    public void focusGained(FocusEvent e) {
    }

    public void focusLost(FocusEvent e) {
        String item = "";
        System.out.println("focusLost");
        Object source = e.getSource();
        if (source == productBox) {
            item = productBox.getSelectedItem().toString();
            if (item.equals("")) {
                System.out.println("combo is blanc no selected index");
            } else {
                item = productBox.getSelectedItem().toString();
                int prodId = 0;

                neo.manager.Product prodDesc = NeoWebService.getNeoManagerBeanService().findProductWithDesc(item);
                prodId = prodDesc.getProductId();
                System.out.println("Column value = " + item);
                System.out.println("Product ID = " + prodId);
                discountBandsTable.getColumnModel().getColumn(10).setCellEditor(new OptionType(prodId));
            }
        }
        if (source == optionBox) {
            item = optionBox.getSelectedItem().toString();
            if (item.equals("")) {
                System.out.println("combo is blanc no selected index");
            } else {
                item = optionBox.getSelectedItem().toString();
                int optionId = 0;

                neo.manager.Option optDesc = NeoWebService.getNeoManagerBeanService().findOptionWithName(item);
                optionId = optDesc.getOptionId();
                System.out.println("Column value = " + item);
                System.out.println("Option ID = " + optionId);

            }
        }
    }
}

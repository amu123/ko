/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.AgilityTechnologies.TableEditors;

/**
 *
 * @author johanl
 */
import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

public class DiscountType
        extends AbstractCellEditor
        implements TableCellEditor {

    private JComboBox cbComponent;

    public DiscountType() {
        neo.manager.DiscountType[] discountType = neo.manager.DiscountType.values();

        cbComponent = new JComboBox(discountType);
    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {
        return cbComponent;
    }

    public Object getCellEditorValue() {
        return cbComponent.getSelectedItem();
    }
}


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.AgilityTechnologies.TableEditors;

import com.jidesoft.swing.AutoCompletionComboBox;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
//import neo.manager.EntityDetails;
import neo.manager.EntitySearchCriteria;
import neo.manager.NeoManagerBean;
import neo.manager.PersonDetails;
import neo.product.utils.NeoWebService;


/**
 *
 * @author DeonP
 */
public class ProviderComboTable
        extends AbstractCellEditor
        implements TableCellEditor {

    private JComboBox cbComponent;
    private com.jidesoft.swing.AutoCompletionComboBox autoCompletionComboBoxProvider;

    public ProviderComboTable() {

       
        neo.manager.EntitySearchCriteria practiceSearch = new EntitySearchCriteria();

        practiceSearch.setEntityType(neo.manager.EntityType.PERSON);
        practiceSearch.setContractType(neo.manager.ContractType.PROVIDER);

        List<PersonDetails> result = NeoWebService.getNeoManagerBeanService().findPersonsWithSearchCriteria(practiceSearch);

        autoCompletionComboBoxProvider = new AutoCompletionComboBox(new ComboPopulatorProvider(result).comboPopList.toArray());
        autoCompletionComboBoxProvider.addItem("");

    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {
        return autoCompletionComboBoxProvider;
    }

    public Object getCellEditorValue() {
        return autoCompletionComboBoxProvider.getSelectedItem();
    }

    public class ComboPopulatorProvider {
        public int id;
        public String name;
        public int type;


        Collection<ComboPopulatorProvider> comboPopList = new ArrayList<ComboPopulatorProvider>();

        public ComboPopulatorProvider(int id,String name,int type) {
            this.id = id;
            this.name = id + " - " + name;
            this.type = type;
        }

        public ComboPopulatorProvider(Collection<PersonDetails> list) {
            for (PersonDetails entity : list) {
              ComboPopulatorProvider tmp = null;//new ComboPopulatorProvider(entity.getEntityId(),entity.getName());//entity.getProviderType() );
              comboPopList.add(tmp);
            }
        }

        public String toString() {
            return name;
        }
     }

}



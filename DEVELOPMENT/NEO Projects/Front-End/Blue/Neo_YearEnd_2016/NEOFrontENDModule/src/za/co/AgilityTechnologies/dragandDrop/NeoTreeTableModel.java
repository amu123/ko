package za.co.AgilityTechnologies.dragandDrop;


import java.util.List;

import com.jidesoft.grid.Row;
import com.jidesoft.grid.TreeTableModel;

/**
 * This class acts as Tree table model. 
 * 
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class NeoTreeTableModel extends TreeTableModel<Row>
{
	
	public NeoTreeTableModel(List rows)
	{
		super(rows);
	}
	
	public int getColumnCount() 
	{
		return 1;
	}

	@Override
	public Class<?> getColumnClass(int index) 
	{
		return String.class;
	}

	@Override
	public String getColumnName(int arg0) 
	{
		return "Title";
	}
	
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.AgilityTechnologies.TableEditors;

import com.jidesoft.swing.AutoCompletionComboBox;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import neo.manager.NeoManagerBean;
import neo.manager.Product;
import neo.product.utils.NeoWebService;


/**
 *
 * @author DeonP
 */
public class ProductComboTable
            extends AbstractCellEditor
            implements TableCellEditor {
    
    private JComboBox cbComponent;
    private com.jidesoft.swing.AutoCompletionComboBox autoCompletionComboBox;

    public ProductComboTable() {

        
      

        List<Product> result = NeoWebService.getNeoManagerBeanService().fetchAllProducts();


        

        autoCompletionComboBox = new AutoCompletionComboBox(new ProductComboPopulator(result).comboPopList.toArray());
        autoCompletionComboBox.addItem("");

    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {
        return autoCompletionComboBox;
    }

    public Object getCellEditorValue() {
        return autoCompletionComboBox.getSelectedItem();

    }

    public class ProductComboPopulator {
        public int id;
        public String name;

        Collection<ProductComboPopulator> comboPopList = new ArrayList<ProductComboPopulator>();

        public ProductComboPopulator(int id,String name) {
            this.id = id;
            this.name = name;
        }

        public ProductComboPopulator(Collection<Product> list) {
            for (Product product : list) {
              ProductComboPopulator tmp = new ProductComboPopulator(product.getProductId() ,product.getProductName());
              comboPopList.add(tmp);
            }
        }

        public String toString() {
            return name;
        }
     }

}

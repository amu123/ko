/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.AgilityTechnologies;

import javax.swing.JPanel;
import neo.entity.company.CreateCompany;
import neo.entity.company.CreateCompanyAddressDetails;
import neo.entity.company.CreateCompanyBankingDetails;
import neo.entity.company.CreateCompanyContact;
import neo.entity.person.CreatePerson;

import neo.quotation.createQuotation.AssignOptionLimits;
import neo.quotation.createQuotation.AssignOptionstoQuote;
import neo.quotation.createQuotation.CaptureIndividuals;
import neo.quotation.createQuotation.CreateNewQuotation;

/**
 *
 * @author BharathP
 */
public class BodyPanelRenderer {
    
    //Create Entity
    public CreateCompany company = new CreateCompany();
    public CreateCompanyAddressDetails companyAddressDetails = new CreateCompanyAddressDetails();
    public CreateCompanyBankingDetails companybankingDetails = new CreateCompanyBankingDetails();
    public CreateCompanyContact companyContact = new CreateCompanyContact();
    public CreatePerson person = new CreatePerson();

 
  


    //Create a New Quotations
    public CreateNewQuotation createnewQuote = new CreateNewQuotation();
    public CaptureIndividuals captureIndividuals = new CaptureIndividuals();
   
   public AssignOptionLimits assignOptionLimits=new AssignOptionLimits();
    
   

    public JPanel bodyRender(int i) {
        JPanel obj = null;
        switch (i) {
            case 1:
                obj = company;
                break;
            case 2:
                obj = companyAddressDetails;
                break;
            case 3:
                obj = companybankingDetails;
                break;
            case 4:
                obj = companyContact;
                break;

            case 5:
                obj = person;
                break;

//Product
          



//Quotations
            case 300:
                obj = createnewQuote;
                break;

            case 301:
                             
               
                
                obj = captureIndividuals;
                break;
            case 302:
                
               
                break;
            case 303:
                obj = assignOptionLimits;
                break;



        }


        return obj;
    }
}

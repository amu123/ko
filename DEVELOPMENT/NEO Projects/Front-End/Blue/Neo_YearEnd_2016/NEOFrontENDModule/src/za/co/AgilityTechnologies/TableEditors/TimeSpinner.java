/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.AgilityTechnologies.TableEditors;

import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author DeonP
 */
public class TimeSpinner
        extends AbstractCellEditor
        implements TableCellEditor {
    
    private com.jidesoft.spinner.DateSpinner timeSpin;

    public TimeSpinner() {
        timeSpin = new com.jidesoft.spinner.DateSpinner();
    }

    public Component getTableCellEditorComponent(JTable table,
Object value, boolean isSelected, int row, int column) {
return timeSpin;
}

public Object getCellEditorValue() {
return timeSpin.getValue();
}

}

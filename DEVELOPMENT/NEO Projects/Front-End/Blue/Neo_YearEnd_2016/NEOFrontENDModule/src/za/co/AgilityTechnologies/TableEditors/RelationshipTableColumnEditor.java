/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.AgilityTechnologies.TableEditors;

/**
 *
 * @author BharathP
 */
import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;


public class RelationshipTableColumnEditor
        extends AbstractCellEditor
        implements TableCellEditor {

    neo.manager.RelationshipType[] relationshipTypes = neo.manager.RelationshipType.values();
    private JComboBox cbComponent;

    public RelationshipTableColumnEditor() {


        cbComponent = new JComboBox(relationshipTypes);
    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {

        return cbComponent;
    }

    public Object getCellEditorValue() {
        String relationshipStr = cbComponent.getSelectedItem().toString();

        return relationshipStr;
    }
}

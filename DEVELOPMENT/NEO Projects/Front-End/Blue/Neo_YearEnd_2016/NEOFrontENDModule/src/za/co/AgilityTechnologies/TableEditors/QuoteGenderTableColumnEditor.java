/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.AgilityTechnologies.TableEditors;

import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import neo.manager.Gender;

public class QuoteGenderTableColumnEditor
        extends AbstractCellEditor
        implements TableCellEditor {

    Gender[] genders = neo.manager.Gender.values();
    private JComboBox cbComponent;

    public QuoteGenderTableColumnEditor() {

        cbComponent = new JComboBox(genders);
    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {
        return cbComponent;
    }

    public Object getCellEditorValue() {
        String genderStr = cbComponent.getSelectedItem().toString();

        return genderStr;
    }
}

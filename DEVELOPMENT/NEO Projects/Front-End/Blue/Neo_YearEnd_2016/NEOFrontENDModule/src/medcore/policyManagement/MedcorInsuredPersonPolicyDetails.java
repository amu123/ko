/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * MedcorInsuredPersonPolicyDetails.java
 *
 * Created on 2009/07/20, 02:56:31
 */
package medcore.policyManagement;

import com.jidesoft.popup.JidePopup;
import java.awt.Component;
import java.awt.Font;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import medcor.entity.view.UnderwritingViewFrame;
import neo.claimsManagement.ClaimEnquiryResult;
import neo.claimsManagement.SearchResult;
import neo.entity.broker.CreateBroker;
import neo.manager.AttributeTypes;
//import neo.manager.AuthBenefitBooking;
import neo.manager.ClaimSearchCriteria;
import neo.manager.ClaimSearchResult;
import neo.manager.ClaimsBatch;
import neo.manager.CoverClaimBenefitLimitStructure;
import neo.manager.CoverClaimLimits;
import neo.manager.CoverDetails;
import neo.manager.CoverProductDetails;
import neo.manager.EntityPaymentRunDetails;
import neo.manager.LookupValue;
import neo.manager.PaymentRunSearchCriteria;
import neo.manager.PersonDetails;
import neo.manager.PolicyProduct;
import neo.manager.PreAuthView;
import neo.manager.Security;
import neo.product.utils.NeoWebService;
import neo.util.UtilMethods;
import neo.utils.tables.TableFormater;
import newEntity.ViewPracticeDetails;
import za.co.AgilityTechnologies.DockingFramework;
import za.co.AgilityTechnologies.LeftMenu.PolicyActionFrame;
import za.co.AgilityTechnologies.LoginPage;
import za.co.AgilityTechnologies.NeoMessageDialog;

/**
 *
 * @author BharathP
 */
public class MedcorInsuredPersonPolicyDetails extends javax.swing.JPanel {

    public static Collection<CoverClaimLimits> claimProdLimits;
    public static Map<String, Integer> insurerMap = new HashMap<String, Integer>();
    public static Map<String, Integer> OptionMap = new HashMap<String, Integer>();
    public static Map<String, Integer> dependentMap = new HashMap<String, Integer>();
    public static Map<String, LookupValue> claimStatusMap = new HashMap<String, LookupValue>();
    public static String CoverNumber = "";
    public static String CoverName = "";
    static int coverEntityId;
    static int selectedBenRow = 0;
    static Collection<CoverDetails> coverMemList;
    DockingFramework df = new DockingFramework();

    /** Creates new form MedcorInsuredPersonPolicyDetails */
    public MedcorInsuredPersonPolicyDetails() {
        initComponents();
        hideMedcorObjects();

        //  medicalAuthhorizationGridDisplay();
        //  authhorizationGridDisplay();
        if (CoverNumber.equals("")) {
            System.out.println("not dependent");
        } else {
            System.out.println(CoverNumber);
            //displayAllInsuredPersonDetails(CoverNumber);
            displayInsuredPersonTab(CoverNumber);
        }

        ViewPracticeDetails.cashPractice = null;
        //table date formats
        authorizationTable.getColumnModel().getColumn(3).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });

        authorizationTable.getColumnModel().getColumn(4).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });
    }

    public MedcorInsuredPersonPolicyDetails(String coverNumber) {
        CoverNumber = coverNumber;
        System.out.println("recieved coverNumber = " + coverNumber);
        System.out.println("set coverNumber = " + CoverNumber);
        initComponents();
        hideMedcorObjects();

        //  medicalAuthhorizationGridDisplay();
        //  authhorizationGridDisplay();
        if (CoverNumber.equals("")) {
            System.out.println("not dependent");
        } else {
            System.out.println(CoverNumber);
            //displayAllInsuredPersonDetails(CoverNumber);
            displayInsuredPersonTab(CoverNumber);
        }
        //table date formats
        authorizationTable.getColumnModel().getColumn(3).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });

        authorizationTable.getColumnModel().getColumn(4).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });
    }

    public static void paneSelection(String tabName) {

        if (tabName.equals("INSURED PERSON")) {
            System.out.println("loading....." + tabName);
            btn_AddDep.setVisible(false);
            displayInsuredPersonTab(CoverNumber);

        } else if (tabName.equals("BENEFIT LIMITS 2010")) {
            System.out.println("loading....." + tabName);
            displayBenefitLimits(CoverNumber);

        } else if (tabName.equals("BENEFIT LIMITS")) {
            System.out.println("loading....." + tabName);
            displayBenefitLimitsAndBookings(CoverNumber);

        } else if (tabName.equals("CLAIMS HISTORY")) {
            System.out.println("loading....." + tabName);
            cb_ClaimStatus.setSelectedItem("");
            displayClaimsHistory(CoverNumber);

        } else if (tabName.equals("PAYMENT RUN DETAILS")) {
            System.out.println("loading....." + tabName);
            cb_EntityType.setSelectedItem("");
            loadPaymentRunDetails();
            paymentTable.getColumnModel().getColumn(8).setCellRenderer(new DefaultTableCellRenderer() {

                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    if (value instanceof Date) {
                        Date date = (Date) value;
                        DateFormat format = new SimpleDateFormat("yyyy/MM/dd 'at' HH:mm:ss");
                        value = format.format(date);
                    }
                    return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                }
            });
            pnlResult.setVisible(false);
            pnl_ProvNum.setVisible(false);
            txt_ProvNum.setText("");
        }

    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tpane_MedcorInsured = new javax.swing.JTabbedPane();
        INSUREDPERSONSCORRLPANE = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        styledLabel8 = new com.jidesoft.swing.StyledLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        policyNumberInsTabTxt = new com.jidesoft.swing.AutoResizingTextArea();
        styledLabel9 = new com.jidesoft.swing.StyledLabel();
        jScrollPane10 = new javax.swing.JScrollPane();
        coverNameInsTabTxt = new com.jidesoft.swing.AutoResizingTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        jPanel18 = new javax.swing.JPanel();
        styledLabel26 = new com.jidesoft.swing.StyledLabel();
        jScrollPane11 = new javax.swing.JScrollPane();
        listingofPersons = new com.jidesoft.grid.JideTable();
        jScrollPane16 = new javax.swing.JScrollPane();
        coverDetailsTable = new com.jidesoft.grid.JideTable();
        jScrollPane13 = new javax.swing.JScrollPane();
        jPanel20 = new javax.swing.JPanel();
        styledLabel27 = new com.jidesoft.swing.StyledLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane14 = new javax.swing.JScrollPane();
        jPanel22 = new javax.swing.JPanel();
        styledLabel28 = new com.jidesoft.swing.StyledLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_PersonDetails = new javax.swing.JTable();
        backInsPersonTabBtn = new com.jidesoft.swing.JideButton();
        btn_View = new javax.swing.JButton();
        btn_AddDep = new javax.swing.JButton();
        jScrollPane8 = new javax.swing.JScrollPane();
        jPanel8 = new javax.swing.JPanel();
        styledLabel11 = new com.jidesoft.swing.StyledLabel();
        btn_Underwriting = new javax.swing.JButton();
        BenefitLimitsScrollPane = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        styledLabel1 = new com.jidesoft.swing.StyledLabel();
        styledLabel2 = new com.jidesoft.swing.StyledLabel();
        styledLabel3 = new com.jidesoft.swing.StyledLabel();
        dcb_BenefitDateFrom = new com.jidesoft.combobox.DateComboBox();
        styledLabel4 = new com.jidesoft.swing.StyledLabel();
        dcb_BenefitDateTo = new com.jidesoft.combobox.DateComboBox();
        styledLabel5 = new com.jidesoft.swing.StyledLabel();
        cb_InsuredDep = new com.jidesoft.swing.AutoCompletionComboBox();
        styledLabel6 = new com.jidesoft.swing.StyledLabel();
        searchBenefitActionBtn = new com.jidesoft.swing.JideButton();
        styledLabel7 = new com.jidesoft.swing.StyledLabel();
        txt_PolicyNumber = new javax.swing.JTextField();
        txt_Option = new javax.swing.JTextField();
        txt_Product = new javax.swing.JTextField();
        txt_Status = new javax.swing.JTextField();
        jScrollPane6 = new javax.swing.JScrollPane();
        benefitTable = new com.jidesoft.grid.JideTable();
        jScrollPane7 = new javax.swing.JScrollPane();
        jPanel7 = new javax.swing.JPanel();
        styledLabel10 = new com.jidesoft.swing.StyledLabel();
        backBenefitTabBtn = new com.jidesoft.swing.JideButton();
        btn_ViewClaim = new javax.swing.JButton();
        BenefitLimitBookingsScrollPane = new javax.swing.JScrollPane();
        jPanel31 = new javax.swing.JPanel();
        jScrollPane28 = new javax.swing.JScrollPane();
        jPanel32 = new javax.swing.JPanel();
        styledLabel21 = new com.jidesoft.swing.StyledLabel();
        jPanel33 = new javax.swing.JPanel();
        styledLabel22 = new com.jidesoft.swing.StyledLabel();
        cb_InsuredDep2 = new com.jidesoft.swing.AutoCompletionComboBox();
        btn_viewAllBen = new javax.swing.JButton();
        styledLabel19 = new com.jidesoft.swing.StyledLabel();
        txt_PolicyNumber1 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txt_PolicyProductBL = new javax.swing.JTextField();
        txt_ProductBLOption = new javax.swing.JTextField();
        styledLabel20 = new com.jidesoft.swing.StyledLabel();
        dcb_2011BenefitDateFrom = new com.jidesoft.combobox.DateComboBox();
        jScrollPane29 = new javax.swing.JScrollPane();
        tbl_benLimBook = new javax.swing.JTable();
        ClaimsHistoryPane = new javax.swing.JScrollPane();
        jPanel16 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        pnl_RefinedSearch = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        searchButton = new javax.swing.JButton();
        dcb_ServiceDateFrom = new com.jidesoft.combobox.DateComboBox();
        dcb_ProcessDate = new com.jidesoft.combobox.DateComboBox();
        dcb_ServiceDateTo = new com.jidesoft.combobox.DateComboBox();
        dcb_PaymentDate = new com.jidesoft.combobox.DateComboBox();
        txt_AccountNumber = new javax.swing.JTextField();
        txt_DependentNo = new javax.swing.JTextField();
        txt_AuthNumber = new javax.swing.JTextField();
        txt_ClaimNumber = new javax.swing.JTextField();
        txt_BatchNumber = new javax.swing.JTextField();
        txt_ScanIndexNumber = new javax.swing.JTextField();
        txt_EdiId = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        txt_PracticeNumber = new javax.swing.JTextField();
        txt_ProviderNumber = new javax.swing.JTextField();
        txt_CoverNumber = new javax.swing.JTextField();
        jScrollPane23 = new javax.swing.JScrollPane();
        jPanel21 = new javax.swing.JPanel();
        styledLabel17 = new com.jidesoft.swing.StyledLabel();
        jLabel14 = new javax.swing.JLabel();
        txt_provDiscipline = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txt_TariffCode = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        cb_ClaimStatus = new javax.swing.JComboBox();
        jScrollPane15 = new javax.swing.JScrollPane();
        jPanel9 = new javax.swing.JPanel();
        styledLabel12 = new com.jidesoft.swing.StyledLabel();
        PreAuthHistoryPane = new javax.swing.JScrollPane();
        jPanel17 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        authorizationTable = new javax.swing.JTable();
        backPreAuthTabBtn = new com.jidesoft.swing.JideButton();
        jScrollPane17 = new javax.swing.JScrollPane();
        jPanel10 = new javax.swing.JPanel();
        styledLabel13 = new com.jidesoft.swing.StyledLabel();
        CardsHistorPane = new javax.swing.JScrollPane();
        pnl_CardHistory = new javax.swing.JPanel();
        backChronicPreAuthTabBtn1 = new com.jidesoft.swing.JideButton();
        jPanel11 = new javax.swing.JPanel();
        jLabel45 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        beneficiaryNameTxt = new javax.swing.JTextField();
        cardsHistoryInsTxt = new javax.swing.JTextField();
        jScrollPane18 = new javax.swing.JScrollPane();
        cardsHistoryTable = new com.jidesoft.grid.JideTable();
        jScrollPane19 = new javax.swing.JScrollPane();
        jPanel12 = new javax.swing.JPanel();
        styledLabel14 = new com.jidesoft.swing.StyledLabel();
        AuditTrailPane = new javax.swing.JScrollPane();
        jPanel26 = new javax.swing.JPanel();
        backChronicPreAuthTabBtn2 = new com.jidesoft.swing.JideButton();
        jScrollPane21 = new javax.swing.JScrollPane();
        jPanel13 = new javax.swing.JPanel();
        styledLabel15 = new com.jidesoft.swing.StyledLabel();
        jPanel14 = new javax.swing.JPanel();
        jScrollPane20 = new javax.swing.JScrollPane();
        auditTrailTable = new com.jidesoft.grid.JideTable();
        jLabel47 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        auditTrailNameTxt = new javax.swing.JTextField();
        auditTrailInsTxt = new javax.swing.JTextField();
        NotePane = new javax.swing.JScrollPane();
        jPanel27 = new javax.swing.JPanel();
        backChronicPreAuthTabBtn3 = new com.jidesoft.swing.JideButton();
        newnoteBtn = new com.jidesoft.swing.JideButton();
        amendBtn = new com.jidesoft.swing.JideButton();
        jScrollPane22 = new javax.swing.JScrollPane();
        jPanel15 = new javax.swing.JPanel();
        styledLabel16 = new com.jidesoft.swing.StyledLabel();
        jPanel19 = new javax.swing.JPanel();
        jScrollPane24 = new javax.swing.JScrollPane();
        NotesTable = new com.jidesoft.grid.JideTable();
        jLabel49 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        notesNameTxt = new javax.swing.JTextField();
        NotesInsTxt = new javax.swing.JTextField();
        jPanel23 = new javax.swing.JPanel();
        cb_PayDates = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        search = new javax.swing.JButton();
        pnlResult = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        paymentTable = new javax.swing.JTable();
        jScrollPane25 = new javax.swing.JScrollPane();
        jPanel24 = new javax.swing.JPanel();
        styledLabel18 = new com.jidesoft.swing.StyledLabel();
        jLabel2 = new javax.swing.JLabel();
        cb_EntityType = new javax.swing.JComboBox();
        pnl_ProvNum = new javax.swing.JPanel();
        jLabel35 = new javax.swing.JLabel();
        txt_ProvNum = new javax.swing.JTextField();

        setBackground(new java.awt.Color(235, 235, 235));
        setPreferredSize(new java.awt.Dimension(940, 966));

        tpane_MedcorInsured.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tpane_MedcorInsuredStateChanged(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(235, 235, 235));
        jPanel1.setPreferredSize(new java.awt.Dimension(910, 700));

        jPanel6.setBackground(new java.awt.Color(235, 235, 235));
        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel8.setText("Cover Number");

        policyNumberInsTabTxt.setColumns(20);
        policyNumberInsTabTxt.setEditable(false);
        jScrollPane9.setViewportView(policyNumberInsTabTxt);

        styledLabel9.setText("Cover Name");

        coverNameInsTabTxt.setColumns(20);
        coverNameInsTabTxt.setEditable(false);
        jScrollPane10.setViewportView(coverNameInsTabTxt);

        jScrollPane3.setBackground(new java.awt.Color(138, 177, 230));

        jPanel18.setBackground(new java.awt.Color(138, 177, 230));
        jPanel18.setForeground(new java.awt.Color(138, 177, 230));

        styledLabel26.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel26.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel26.setText("MEMBER INFORMATION");
        styledLabel26.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jPanel18.add(styledLabel26);

        jScrollPane3.setViewportView(jPanel18);

        listingofPersons.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Insured Number", "Name", "Relationship Type", "Status", "Date of Birth", "Date _ From", "Date _ To"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        listingofPersons.setColumnAutoResizable(true);
        listingofPersons.setColumnResizable(true);
        listingofPersons.setRowResizable(true);
        listingofPersons.setScrollRowWhenRowHeightChanges(true);
        listingofPersons.setSelectionBackground(new java.awt.Color(138, 177, 230));
        listingofPersons.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                listingofPersonsMouseReleased(evt);
            }
        });
        jScrollPane11.setViewportView(listingofPersons);

        coverDetailsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null}
            },
            new String [] {
                "Employer Group", "Payroll Number", "Pay Point"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane16.setViewportView(coverDetailsTable);

        jScrollPane13.setBackground(new java.awt.Color(138, 177, 230));

        jPanel20.setBackground(new java.awt.Color(138, 177, 230));
        jPanel20.setForeground(new java.awt.Color(138, 177, 230));

        styledLabel27.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel27.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel27.setText("ADDITIONAL INFORMATION");
        styledLabel27.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jPanel20.add(styledLabel27);

        jScrollPane13.setViewportView(jPanel20);

        jScrollPane14.setBackground(new java.awt.Color(138, 177, 230));

        jPanel22.setBackground(new java.awt.Color(138, 177, 230));
        jPanel22.setForeground(new java.awt.Color(138, 177, 230));

        styledLabel28.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel28.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel28.setText("VIEW MEMBER PERSONAL DETAILS");
        styledLabel28.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jPanel22.add(styledLabel28);

        jScrollPane14.setViewportView(jPanel22);

        tbl_PersonDetails.setModel(setViewGridDetail());
        jScrollPane2.setViewportView(tbl_PersonDetails);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, 0, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane14, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE))
                .addGap(451, 451, 451))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane16, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 784, Short.MAX_VALUE)
                    .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 784, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 784, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(74, 74, 74)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane10)
                            .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)))
                    .addComponent(jScrollPane13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 784, Short.MAX_VALUE))
                .addGap(28, 28, 28))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(styledLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane9))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                    .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane16, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        backInsPersonTabBtn.setBorder(null);
        backInsPersonTabBtn.setButtonStyle(com.jidesoft.swing.JideButton.FLAT_STYLE);
        backInsPersonTabBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/arrow2_left_blue.png"))); // NOI18N
        backInsPersonTabBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backInsPersonTabBtnActionPerformed(evt);
            }
        });

        btn_View.setText("View");
        btn_View.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ViewActionPerformed(evt);
            }
        });

        btn_AddDep.setText("Add Dependent");
        btn_AddDep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AddDepActionPerformed(evt);
            }
        });

        jScrollPane8.setBackground(new java.awt.Color(138, 177, 230));

        jPanel8.setBackground(new java.awt.Color(138, 177, 230));
        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel11.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel11.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel11.setText("INSURED PERSON DETAILS");
        styledLabel11.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jPanel8.add(styledLabel11);

        jScrollPane8.setViewportView(jPanel8);

        btn_Underwriting.setText("Underwriting");
        btn_Underwriting.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_UnderwritingActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 849, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(72, 72, 72)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(704, 704, 704)
                                .addComponent(backInsPersonTabBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btn_View, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btn_Underwriting, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btn_AddDep, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false))))
                .addContainerGap(94, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_View)
                    .addComponent(btn_Underwriting)
                    .addComponent(btn_AddDep))
                .addGap(18, 18, 18)
                .addComponent(backInsPersonTabBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(92, 92, 92))
        );

        INSUREDPERSONSCORRLPANE.setViewportView(jPanel1);

        tpane_MedcorInsured.addTab("INSURED PERSON", INSUREDPERSONSCORRLPANE);

        jPanel3.setBackground(new java.awt.Color(235, 235, 235));
        jPanel3.setPreferredSize(new java.awt.Dimension(910, 650));

        jPanel4.setBackground(new java.awt.Color(235, 235, 235));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setPreferredSize(new java.awt.Dimension(300, 267));

        styledLabel1.setText("Product Name");

        styledLabel2.setText("Status");

        styledLabel3.setText("Benefit Date From");

        dcb_BenefitDateFrom.setFormat(new SimpleDateFormat("yyyy/MM/dd"));
        dcb_BenefitDateFrom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dcb_BenefitDateFromActionPerformed(evt);
            }
        });

        styledLabel4.setText("Benefit Date To");

        dcb_BenefitDateTo.setFormat(new SimpleDateFormat("yyyy/MM/dd"));

        styledLabel5.setText("Insured Persons");

        cb_InsuredDep.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cb_InsuredDepItemStateChanged(evt);
            }
        });

        styledLabel6.setText("Option");

        searchBenefitActionBtn.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        searchBenefitActionBtn.setButtonStyle(com.jidesoft.swing.JideButton.TOOLBOX_STYLE);
        searchBenefitActionBtn.setText("Search");
        searchBenefitActionBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchBenefitActionBtnActionPerformed(evt);
            }
        });

        styledLabel7.setText("Policy Number");

        txt_PolicyNumber.setEditable(false);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(styledLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(styledLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(styledLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(styledLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(styledLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(38, 38, 38)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txt_Status)
                            .addComponent(txt_Option)
                            .addComponent(cb_InsuredDep, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dcb_BenefitDateFrom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_Product, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(styledLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(53, 53, 53)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(dcb_BenefitDateTo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_PolicyNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(364, 364, 364)
                        .addComponent(searchBenefitActionBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(208, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(styledLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_Product, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_PolicyNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(styledLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(26, 26, 26)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_Status, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dcb_BenefitDateFrom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dcb_BenefitDateTo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_InsuredDep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_Option, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addComponent(searchBenefitActionBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        benefitTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Benefit Description", "System Term", "Claims Count", "Limit (Count)", "Limit Allowed", "Limit Used", "Limit Available"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        benefitTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                benefitTableMouseReleased(evt);
            }
        });
        jScrollPane6.setViewportView(benefitTable);

        jScrollPane7.setBackground(new java.awt.Color(138, 177, 230));

        jPanel7.setBackground(new java.awt.Color(138, 177, 230));
        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel10.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel10.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel10.setText("BENEFIT LIMITS");
        styledLabel10.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jPanel7.add(styledLabel10);

        jScrollPane7.setViewportView(jPanel7);

        backBenefitTabBtn.setBorder(null);
        backBenefitTabBtn.setButtonStyle(com.jidesoft.swing.JideButton.FLAT_STYLE);
        backBenefitTabBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/arrow2_left_blue.png"))); // NOI18N
        backBenefitTabBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBenefitTabBtnActionPerformed(evt);
            }
        });

        btn_ViewClaim.setText("View Claim History");
        btn_ViewClaim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ViewClaimActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addComponent(btn_ViewClaim, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(backBenefitTabBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1096, 1096, 1096))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 888, Short.MAX_VALUE)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 888, Short.MAX_VALUE))
                .addGap(55, 55, 55))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(450, 450, 450)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(33, 33, 33)
                                .addComponent(backBenefitTabBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btn_ViewClaim)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(352, 352, 352)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 444, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(77, Short.MAX_VALUE))
        );

        BenefitLimitsScrollPane.setViewportView(jPanel3);

        tpane_MedcorInsured.addTab("BENEFIT LIMITS 2010", BenefitLimitsScrollPane);

        jScrollPane28.setBackground(new java.awt.Color(138, 177, 230));

        jPanel32.setBackground(new java.awt.Color(138, 177, 230));
        jPanel32.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel21.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel21.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel21.setText("BENEFIT LIMITS");
        styledLabel21.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jPanel32.add(styledLabel21);

        jScrollPane28.setViewportView(jPanel32);

        jPanel33.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel22.setText("Benefit Date");

        cb_InsuredDep2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cb_InsuredDep2ItemStateChanged(evt);
            }
        });

        btn_viewAllBen.setText("VIEW BENEFITS");
        btn_viewAllBen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewAllBenActionPerformed(evt);
            }
        });

        styledLabel19.setText("Policy Number:");

        txt_PolicyNumber1.setEditable(false);

        jLabel3.setText("Product:");

        jLabel21.setText("Option:");

        txt_PolicyProductBL.setEditable(false);

        txt_ProductBLOption.setEditable(false);

        styledLabel20.setText("Insured Persons:");

        dcb_2011BenefitDateFrom.setFormat(new SimpleDateFormat("yyyy/MM/dd"));
        dcb_2011BenefitDateFrom.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                dcb_2011BenefitDateFromItemStateChanged(evt);
            }
        });
        dcb_2011BenefitDateFrom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dcb_2011BenefitDateFromActionPerformed(evt);
            }
        });
        dcb_2011BenefitDateFrom.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                dcb_2011BenefitDateFromFocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel33Layout = new javax.swing.GroupLayout(jPanel33);
        jPanel33.setLayout(jPanel33Layout);
        jPanel33Layout.setHorizontalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_viewAllBen)
                    .addGroup(jPanel33Layout.createSequentialGroup()
                        .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(styledLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(32, 32, 32)
                        .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dcb_2011BenefitDateFrom, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                            .addComponent(cb_InsuredDep2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                            .addComponent(txt_PolicyNumber1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE))))
                .addGap(53, 53, 53)
                .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel21))
                .addGap(56, 56, 56)
                .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txt_ProductBLOption)
                    .addComponent(txt_PolicyProductBL, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE))
                .addGap(307, 307, 307))
        );
        jPanel33Layout.setVerticalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel33Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                    .addComponent(styledLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_PolicyNumber1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txt_PolicyProductBL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel33Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(styledLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21)
                            .addComponent(txt_ProductBLOption, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel33Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(dcb_2011BenefitDateFrom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(styledLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_InsuredDep2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(btn_viewAllBen)
                .addContainerGap())
        );

        tbl_benLimBook.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Limit", "Sub Limit", "Sub- Sub Limit", "Limit Amount", "Amount Used", "Amount Avail.", "Limit Count ", "Count Used", "Count Avail."
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane29.setViewportView(tbl_benLimBook);
        tbl_benLimBook.getColumnModel().getColumn(3).setMinWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(3).setPreferredWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(3).setMaxWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(4).setMinWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(4).setPreferredWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(4).setMaxWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(5).setMinWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(5).setPreferredWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(5).setMaxWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(6).setMinWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(6).setPreferredWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(6).setMaxWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(7).setMinWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(7).setPreferredWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(7).setMaxWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(8).setMinWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(8).setPreferredWidth(80);
        tbl_benLimBook.getColumnModel().getColumn(8).setMaxWidth(80);

        javax.swing.GroupLayout jPanel31Layout = new javax.swing.GroupLayout(jPanel31);
        jPanel31.setLayout(jPanel31Layout);
        jPanel31Layout.setHorizontalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel31Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane28, javax.swing.GroupLayout.DEFAULT_SIZE, 1008, Short.MAX_VALUE)
                    .addComponent(jPanel33, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane29, javax.swing.GroupLayout.DEFAULT_SIZE, 1008, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel31Layout.setVerticalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel31Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane28, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel33, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jScrollPane29, javax.swing.GroupLayout.PREFERRED_SIZE, 711, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        BenefitLimitBookingsScrollPane.setViewportView(jPanel31);

        tpane_MedcorInsured.addTab("BENEFIT LIMITS", BenefitLimitBookingsScrollPane);

        jPanel16.setBackground(new java.awt.Color(235, 235, 235));
        jPanel16.setPreferredSize(new java.awt.Dimension(910, 700));

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        pnl_RefinedSearch.setBackground(new java.awt.Color(235, 235, 235));
        pnl_RefinedSearch.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnl_RefinedSearch.setPreferredSize(new java.awt.Dimension(591, 411));

        jLabel4.setText("Process Date To:");

        jLabel5.setText("Service Date From:");

        jLabel6.setText("Service Date To:");

        jLabel7.setText("Account Number:");

        jLabel8.setText("Scan Number:");

        jLabel9.setText("Batch Number:");

        jLabel10.setText("Claim Number:");

        jLabel11.setText("Dependant Number:");

        jLabel12.setText("Authorization Number:");

        jLabel13.setText("Payment Date:");

        jLabel15.setText("EDI File Number:");

        searchButton.setText("Search");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });

        dcb_ServiceDateFrom.setPreferredSize(new java.awt.Dimension(9, 20));

        dcb_ServiceDateTo.setPreferredSize(new java.awt.Dimension(9, 20));

        dcb_PaymentDate.setPopupVolatile(false);
        dcb_PaymentDate.setPreferredSize(new java.awt.Dimension(9, 20));

        txt_AccountNumber.setColumns(20);
        txt_AccountNumber.setPreferredSize(new java.awt.Dimension(9, 20));

        txt_DependentNo.setColumns(20);

        txt_AuthNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_AuthNumberActionPerformed(evt);
            }
        });

        txt_ScanIndexNumber.setColumns(20);

        txt_EdiId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_EdiIdActionPerformed(evt);
            }
        });

        jLabel17.setText("Cover Number:");

        jLabel18.setText("Provider Number:");

        jLabel19.setText("Practice Number:");

        txt_PracticeNumber.setColumns(20);

        txt_ProviderNumber.setColumns(20);

        txt_CoverNumber.setColumns(20);

        jScrollPane23.setBackground(new java.awt.Color(138, 177, 230));

        jPanel21.setBackground(new java.awt.Color(138, 177, 230));
        jPanel21.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel17.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel17.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel17.setText("Detailed Claim Enquiry");
        styledLabel17.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jPanel21.add(styledLabel17);

        jScrollPane23.setViewportView(jPanel21);

        jLabel14.setText("Provider Discipline Id:");

        jLabel16.setText("Tariff Code:");

        jLabel20.setText("Claim Status:");

        javax.swing.GroupLayout pnl_RefinedSearchLayout = new javax.swing.GroupLayout(pnl_RefinedSearch);
        pnl_RefinedSearch.setLayout(pnl_RefinedSearchLayout);
        pnl_RefinedSearchLayout.setHorizontalGroup(
            pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_RefinedSearchLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_RefinedSearchLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel13)
                            .addComponent(jLabel11)
                            .addComponent(jLabel5)
                            .addComponent(jLabel7)
                            .addComponent(jLabel9)
                            .addGroup(pnl_RefinedSearchLayout.createSequentialGroup()
                                .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel18)
                                    .addComponent(jLabel17)
                                    .addComponent(jLabel19)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel16))
                                .addGap(35, 35, 35)
                                .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_TariffCode)
                                    .addComponent(txt_ProviderNumber, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                                    .addComponent(txt_CoverNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                                    .addComponent(dcb_ProcessDate, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                                    .addComponent(dcb_PaymentDate, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                                    .addComponent(txt_DependentNo, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                                    .addComponent(txt_BatchNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                                    .addComponent(txt_AccountNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                                    .addComponent(dcb_ServiceDateFrom, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                                    .addComponent(txt_PracticeNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE))))
                        .addGap(50, 50, 50)
                        .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel6)
                            .addComponent(jLabel10)
                            .addComponent(jLabel12)
                            .addComponent(jLabel15)
                            .addComponent(jLabel14)
                            .addComponent(jLabel20))
                        .addGap(31, 31, 31)
                        .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cb_ClaimStatus, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_provDiscipline)
                            .addComponent(txt_EdiId)
                            .addComponent(txt_AuthNumber)
                            .addComponent(txt_ClaimNumber)
                            .addComponent(txt_ScanIndexNumber)
                            .addComponent(dcb_ServiceDateTo, javax.swing.GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE)))
                    .addGroup(pnl_RefinedSearchLayout.createSequentialGroup()
                        .addGap(161, 161, 161)
                        .addComponent(searchButton)))
                .addContainerGap(39, Short.MAX_VALUE))
            .addComponent(jScrollPane23, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 781, Short.MAX_VALUE)
        );
        pnl_RefinedSearchLayout.setVerticalGroup(
            pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_RefinedSearchLayout.createSequentialGroup()
                .addComponent(jScrollPane23, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                    .addComponent(jLabel17)
                    .addComponent(txt_CoverNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(txt_ProviderNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(txt_provDiscipline, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(txt_PracticeNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(dcb_ProcessDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(dcb_ServiceDateFrom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(dcb_ServiceDateTo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txt_AccountNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(txt_ScanIndexNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txt_BatchNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(txt_ClaimNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txt_DependentNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(txt_AuthNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(dcb_PaymentDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(txt_EdiId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnl_RefinedSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txt_TariffCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20)
                    .addComponent(cb_ClaimStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(searchButton)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(pnl_RefinedSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 785, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(pnl_RefinedSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 496, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(57, Short.MAX_VALUE))
        );

        jScrollPane15.setBackground(new java.awt.Color(138, 177, 230));

        jPanel9.setBackground(new java.awt.Color(138, 177, 230));
        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel12.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel12.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel12.setText("CLAIMS HISTORY");
        styledLabel12.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jPanel9.add(styledLabel12);

        jScrollPane15.setViewportView(jPanel9);

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 852, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1010, 1010, 1010))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(223, 223, 223))
        );

        ClaimsHistoryPane.setViewportView(jPanel16);

        tpane_MedcorInsured.addTab("CLAIMS HISTORY", ClaimsHistoryPane);

        jPanel17.setBackground(new java.awt.Color(235, 235, 235));
        jPanel17.setPreferredSize(new java.awt.Dimension(910, 700));

        authorizationTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Authorization", "Authorization Status", "Facility Provider", "Admission Date", "Discharge Date", "ICD10 Code", "CPT Code", "Insured Person Number", "Lengh of stay"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(authorizationTable);

        backPreAuthTabBtn.setBorder(null);
        backPreAuthTabBtn.setButtonStyle(com.jidesoft.swing.JideButton.FLAT_STYLE);
        backPreAuthTabBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/arrow2_left_blue.png"))); // NOI18N
        backPreAuthTabBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backPreAuthTabBtnActionPerformed(evt);
            }
        });

        jScrollPane17.setBackground(new java.awt.Color(138, 177, 230));

        jPanel10.setBackground(new java.awt.Color(138, 177, 230));
        jPanel10.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel13.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel13.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel13.setText("NON- MEDICAL PRE- AUTH");
        styledLabel13.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jPanel10.add(styledLabel13);

        jScrollPane17.setViewportView(jPanel10);

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 886, Short.MAX_VALUE)
                    .addComponent(jScrollPane17, javax.swing.GroupLayout.DEFAULT_SIZE, 886, Short.MAX_VALUE)
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addGap(826, 826, 826)
                        .addComponent(backPreAuthTabBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(36, 36, 36)))
                .addGap(991, 991, 991))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane17, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 442, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46)
                .addComponent(backPreAuthTabBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(392, 392, 392))
        );

        PreAuthHistoryPane.setViewportView(jPanel17);

        tpane_MedcorInsured.addTab("NON-MEDICINE PRE - AUTH", PreAuthHistoryPane);

        pnl_CardHistory.setBackground(new java.awt.Color(235, 235, 235));
        pnl_CardHistory.setPreferredSize(new java.awt.Dimension(910, 700));

        backChronicPreAuthTabBtn1.setBorder(null);
        backChronicPreAuthTabBtn1.setButtonStyle(com.jidesoft.swing.JideButton.FLAT_STYLE);
        backChronicPreAuthTabBtn1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/arrow2_left_blue.png"))); // NOI18N
        backChronicPreAuthTabBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backChronicPreAuthTabBtn1ActionPerformed(evt);
            }
        });

        jPanel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel45.setText("Beneficiary Name");

        jLabel34.setText("Insured Person Number");

        cardsHistoryTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Card Number", "Insured Perrson Detail", "Card Type", "Card Ordered Date", "Card Printed Date", "Card Post Date"
            }
        ));
        jScrollPane18.setViewportView(cardsHistoryTable);
        cardsHistoryTable.getColumnModel().getColumn(0).setMinWidth(100);
        cardsHistoryTable.getColumnModel().getColumn(0).setPreferredWidth(100);
        cardsHistoryTable.getColumnModel().getColumn(0).setMaxWidth(100);
        cardsHistoryTable.getColumnModel().getColumn(1).setMinWidth(100);
        cardsHistoryTable.getColumnModel().getColumn(1).setPreferredWidth(100);
        cardsHistoryTable.getColumnModel().getColumn(1).setMaxWidth(100);
        cardsHistoryTable.getColumnModel().getColumn(3).setMinWidth(100);
        cardsHistoryTable.getColumnModel().getColumn(3).setPreferredWidth(100);
        cardsHistoryTable.getColumnModel().getColumn(3).setMaxWidth(100);
        cardsHistoryTable.getColumnModel().getColumn(4).setHeaderValue("Date To");
        cardsHistoryTable.getColumnModel().getColumn(5).setHeaderValue("User");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane18, javax.swing.GroupLayout.PREFERRED_SIZE, 779, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel45, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel34))
                        .addGap(56, 56, 56)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(beneficiaryNameTxt)
                            .addComponent(cardsHistoryInsTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 182, Short.MAX_VALUE))))
                .addContainerGap(69, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel34)
                    .addComponent(cardsHistoryInsTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel45)
                    .addComponent(beneficiaryNameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addComponent(jScrollPane18, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(73, Short.MAX_VALUE))
        );

        jScrollPane19.setBackground(new java.awt.Color(138, 177, 230));

        jPanel12.setBackground(new java.awt.Color(138, 177, 230));
        jPanel12.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel14.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel14.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel14.setText("CARD HISTORY");
        styledLabel14.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jPanel12.add(styledLabel14);

        jScrollPane19.setViewportView(jPanel12);

        javax.swing.GroupLayout pnl_CardHistoryLayout = new javax.swing.GroupLayout(pnl_CardHistory);
        pnl_CardHistory.setLayout(pnl_CardHistoryLayout);
        pnl_CardHistoryLayout.setHorizontalGroup(
            pnl_CardHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_CardHistoryLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_CardHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_CardHistoryLayout.createSequentialGroup()
                        .addGroup(pnl_CardHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane19, javax.swing.GroupLayout.DEFAULT_SIZE, 886, Short.MAX_VALUE))
                        .addGap(991, 991, 991))
                    .addGroup(pnl_CardHistoryLayout.createSequentialGroup()
                        .addGap(814, 814, 814)
                        .addComponent(backChronicPreAuthTabBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1039, 1039, 1039))))
        );
        pnl_CardHistoryLayout.setVerticalGroup(
            pnl_CardHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_CardHistoryLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane19, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54)
                .addComponent(backChronicPreAuthTabBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(464, 464, 464))
        );

        CardsHistorPane.setViewportView(pnl_CardHistory);

        tpane_MedcorInsured.addTab("CARDS HISTORY", CardsHistorPane);

        jPanel26.setBackground(new java.awt.Color(235, 235, 235));
        jPanel26.setPreferredSize(new java.awt.Dimension(910, 700));

        backChronicPreAuthTabBtn2.setBorder(null);
        backChronicPreAuthTabBtn2.setButtonStyle(com.jidesoft.swing.JideButton.FLAT_STYLE);
        backChronicPreAuthTabBtn2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/arrow2_left_blue.png"))); // NOI18N
        backChronicPreAuthTabBtn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backChronicPreAuthTabBtn2ActionPerformed(evt);
            }
        });

        jScrollPane21.setBackground(new java.awt.Color(138, 177, 230));

        jPanel13.setBackground(new java.awt.Color(138, 177, 230));
        jPanel13.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel15.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel15.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel15.setText("AUDIT TRAIL");
        styledLabel15.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jPanel13.add(styledLabel15);

        jScrollPane21.setViewportView(jPanel13);

        jPanel14.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        auditTrailTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Change Date", "Old Value", "New Value", "Date From", "Date To", "User"
            }
        ));
        jScrollPane20.setViewportView(auditTrailTable);
        auditTrailTable.getColumnModel().getColumn(0).setMinWidth(100);
        auditTrailTable.getColumnModel().getColumn(0).setPreferredWidth(100);
        auditTrailTable.getColumnModel().getColumn(0).setMaxWidth(100);
        auditTrailTable.getColumnModel().getColumn(1).setMinWidth(100);
        auditTrailTable.getColumnModel().getColumn(1).setPreferredWidth(100);
        auditTrailTable.getColumnModel().getColumn(1).setMaxWidth(100);
        auditTrailTable.getColumnModel().getColumn(3).setMinWidth(100);
        auditTrailTable.getColumnModel().getColumn(3).setPreferredWidth(100);
        auditTrailTable.getColumnModel().getColumn(3).setMaxWidth(100);
        auditTrailTable.getColumnModel().getColumn(4).setHeaderValue("Date To");
        auditTrailTable.getColumnModel().getColumn(5).setHeaderValue("User");

        jLabel47.setText("Name");

        jLabel46.setText("Insured Person Number");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane20, javax.swing.GroupLayout.PREFERRED_SIZE, 776, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel47, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel46))
                        .addGap(56, 56, 56)
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(auditTrailNameTxt)
                            .addComponent(auditTrailInsTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(77, Short.MAX_VALUE))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel46)
                    .addComponent(auditTrailInsTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel47)
                    .addComponent(auditTrailNameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addComponent(jScrollPane20, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(55, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel26Layout = new javax.swing.GroupLayout(jPanel26);
        jPanel26.setLayout(jPanel26Layout);
        jPanel26Layout.setHorizontalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel26Layout.createSequentialGroup()
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel26Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel14, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane21, javax.swing.GroupLayout.DEFAULT_SIZE, 886, Short.MAX_VALUE)))
                    .addGroup(jPanel26Layout.createSequentialGroup()
                        .addGap(821, 821, 821)
                        .addComponent(backChronicPreAuthTabBtn2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(52, Short.MAX_VALUE))
        );
        jPanel26Layout.setVerticalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel26Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane21, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(63, 63, 63)
                .addComponent(backChronicPreAuthTabBtn2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(270, 270, 270))
        );

        AuditTrailPane.setViewportView(jPanel26);

        tpane_MedcorInsured.addTab("AUDIT TRAIL", AuditTrailPane);

        jPanel27.setBackground(new java.awt.Color(235, 235, 235));
        jPanel27.setPreferredSize(new java.awt.Dimension(910, 700));

        backChronicPreAuthTabBtn3.setBorder(null);
        backChronicPreAuthTabBtn3.setButtonStyle(com.jidesoft.swing.JideButton.FLAT_STYLE);
        backChronicPreAuthTabBtn3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/arrow2_left_blue.png"))); // NOI18N
        backChronicPreAuthTabBtn3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backChronicPreAuthTabBtn3ActionPerformed(evt);
            }
        });

        newnoteBtn.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        newnoteBtn.setText("Add New Note");

        amendBtn.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        amendBtn.setText("Amend Note");

        jScrollPane22.setBackground(new java.awt.Color(138, 177, 230));

        jPanel15.setBackground(new java.awt.Color(138, 177, 230));
        jPanel15.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel16.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel16.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel16.setText("NOTES");
        styledLabel16.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jPanel15.add(styledLabel16);

        jScrollPane22.setViewportView(jPanel15);

        jPanel19.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        NotesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Date", "User", "Description", "Completed Date"
            }
        ));
        jScrollPane24.setViewportView(NotesTable);
        NotesTable.getColumnModel().getColumn(0).setMinWidth(100);
        NotesTable.getColumnModel().getColumn(0).setPreferredWidth(100);
        NotesTable.getColumnModel().getColumn(0).setMaxWidth(100);
        NotesTable.getColumnModel().getColumn(1).setMinWidth(100);
        NotesTable.getColumnModel().getColumn(1).setPreferredWidth(100);
        NotesTable.getColumnModel().getColumn(1).setMaxWidth(100);
        NotesTable.getColumnModel().getColumn(3).setMinWidth(100);
        NotesTable.getColumnModel().getColumn(3).setPreferredWidth(100);
        NotesTable.getColumnModel().getColumn(3).setMaxWidth(100);

        jLabel49.setText("Name");

        jLabel48.setText("Insured Person Number");

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane24, javax.swing.GroupLayout.PREFERRED_SIZE, 724, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel49, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel48))
                        .addGap(56, 56, 56)
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(notesNameTxt)
                            .addComponent(NotesInsTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(128, Short.MAX_VALUE))
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel48)
                    .addComponent(NotesInsTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel49)
                    .addComponent(notesNameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addComponent(jScrollPane24, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43))
        );

        javax.swing.GroupLayout jPanel27Layout = new javax.swing.GroupLayout(jPanel27);
        jPanel27.setLayout(jPanel27Layout);
        jPanel27Layout.setHorizontalGroup(
            jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel27Layout.createSequentialGroup()
                .addGap(107, 107, 107)
                .addComponent(newnoteBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(65, 65, 65)
                .addComponent(amendBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1515, 1515, 1515))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel27Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel19, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane22, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 890, Short.MAX_VALUE))
                .addGap(987, 987, 987))
            .addGroup(jPanel27Layout.createSequentialGroup()
                .addGap(835, 835, 835)
                .addComponent(backChronicPreAuthTabBtn3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1028, 1028, 1028))
        );
        jPanel27Layout.setVerticalGroup(
            jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel27Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane22, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel27Layout.createSequentialGroup()
                        .addComponent(newnoteBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52)
                        .addComponent(backChronicPreAuthTabBtn3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(amendBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(458, 458, 458))
        );

        NotePane.setViewportView(jPanel27);

        tpane_MedcorInsured.addTab("NOTES", NotePane);

        jLabel1.setText("Payment run date:");

        search.setText("SEARCH");
        search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchActionPerformed(evt);
            }
        });

        paymentTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Payment Id", "Bank Name", "Branch Name", "Branch Code", "Acc Holder", "Acc No", "Acc Type", "Payment Type", "Amount", "Run Date", "Status", "Rejection Reason"
            }
        ));
        jScrollPane4.setViewportView(paymentTable);

        jScrollPane25.setBackground(new java.awt.Color(138, 177, 230));

        jPanel24.setBackground(new java.awt.Color(138, 177, 230));
        jPanel24.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel18.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel18.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel18.setText("PAYMENT RUN DETAILS");
        styledLabel18.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jPanel24.add(styledLabel18);

        jScrollPane25.setViewportView(jPanel24);

        javax.swing.GroupLayout pnlResultLayout = new javax.swing.GroupLayout(pnlResult);
        pnlResult.setLayout(pnlResultLayout);
        pnlResultLayout.setHorizontalGroup(
            pnlResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlResultLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane25, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 897, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 897, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlResultLayout.setVerticalGroup(
            pnlResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlResultLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane25, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 465, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(36, Short.MAX_VALUE))
        );

        jLabel2.setText("Entity Type:");

        cb_EntityType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Practice", "Insured Person" }));
        cb_EntityType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cb_EntityTypeItemStateChanged(evt);
            }
        });

        jLabel35.setText("Provider Number:");

        javax.swing.GroupLayout pnl_ProvNumLayout = new javax.swing.GroupLayout(pnl_ProvNum);
        pnl_ProvNum.setLayout(pnl_ProvNumLayout);
        pnl_ProvNumLayout.setHorizontalGroup(
            pnl_ProvNumLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_ProvNumLayout.createSequentialGroup()
                .addComponent(jLabel35)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                .addComponent(txt_ProvNum, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        pnl_ProvNumLayout.setVerticalGroup(
            pnl_ProvNumLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_ProvNumLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel35)
                .addComponent(txt_ProvNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pnl_ProvNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel23Layout.createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cb_EntityType, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel23Layout.createSequentialGroup()
                                    .addComponent(jLabel1)
                                    .addGap(40, 40, 40)
                                    .addComponent(cb_PayDates, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pnlResult, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel23Layout.createSequentialGroup()
                                .addGap(270, 270, 270)
                                .addComponent(search)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(cb_EntityType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(7, 7, 7)
                        .addComponent(pnl_ProvNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addGap(91, 91, 91)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cb_PayDates, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))))
                .addGap(18, 18, 18)
                .addComponent(search)
                .addGap(11, 11, 11)
                .addComponent(pnlResult, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(213, Short.MAX_VALUE))
        );

        tpane_MedcorInsured.addTab("PAYMENT RUN DETAILS", jPanel23);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tpane_MedcorInsured, javax.swing.GroupLayout.DEFAULT_SIZE, 960, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tpane_MedcorInsured))
        );
    }// </editor-fold>//GEN-END:initComponents

    private DefaultTableModel setViewGridDetail() {
        DefaultTableModel returnModel = null;
        
        if (!LoginPage.neoUserSecurityRestricted) {
            returnModel = new javax.swing.table.DefaultTableModel(
                    new Object[][]{{"Address Details"}, {"Banking Details"}, {"Contact Details"}, {"Contact Preferences"}}, new String[]{""}) {

                boolean[] canEdit = new boolean[]{
                    false
                };

                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            };
        } else {
            returnModel = new javax.swing.table.DefaultTableModel(
                    new Object[][]{{"Address Details"}, {"Contact Details"}, {"Contact Preferences"}}, new String[]{""}) {

                boolean[] canEdit = new boolean[]{
                    false
                };

                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            };
        }
        return returnModel;
    }

    private void tpane_MedcorInsuredStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tpane_MedcorInsuredStateChanged
        int tabIndex = tpane_MedcorInsured.getSelectedIndex();
        System.out.println("tabIndex = " + tabIndex);
        String tabName = tpane_MedcorInsured.getTitleAt(tabIndex);
        System.out.println("tabName = " + tabName);
        paneSelection(
                tabName);
}//GEN-LAST:event_tpane_MedcorInsuredStateChanged

    private void btn_viewAllBenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_viewAllBenActionPerformed
        // TODO add your handling code here:
        String coverNumber = txt_PolicyNumber1.getText();
        String depDesc = cb_InsuredDep2.getSelectedItem().toString();


        int depId = dependentMap.get(depDesc);

        Collection<CoverClaimBenefitLimitStructure> benLimStruct = new ArrayList<CoverClaimBenefitLimitStructure>();
        //Collection<Integer> benLimStruct = null;
        Date benDate = null;
        XMLGregorianCalendar xBenDate = null;

        if (dcb_2011BenefitDateFrom != null) {
            benDate = dcb_2011BenefitDateFrom.getDate();
        } else {
            benDate = new Date(System.currentTimeMillis());
        }
        Date year11 = null;
        try {
            //2011 date check
            year11 = new SimpleDateFormat("yyyy/MM/dd").parse("2011/01/01");
        } catch (ParseException ex) {
            Logger.getLogger(MedcorInsuredPersonPolicyDetails.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (year11 != null) {

            Calendar benefitDate = Calendar.getInstance();
            benefitDate.setTime(benDate);

            Calendar yearEndDate = Calendar.getInstance();
            yearEndDate.setTime(year11);

            if (benefitDate.before(yearEndDate)) {
                NeoMessageDialog.showErrorMessage("Benefit Date Before 2011/01/01 is not Allowed");

            } else {

                xBenDate = UtilMethods.convertDateToXMLGeoCalender(benDate);
                benLimStruct = NeoWebService.getNeoManagerBeanService().getBenefitLimitsForCover(coverNumber, depId, xBenDate);

                int rowSize = benLimStruct.size();
                if (rowSize > 0) {
                    int x = 0;

                    //removing all rows
                    tbl_benLimBook.setModel(new javax.swing.table.DefaultTableModel(
                            new Object[][]{},
                            new String[]{
                                "Limit", "Sub Limit", "Sub- Sub Limit", "Limit Amount", "Amount Used", "Amount Avail.", "Limit Count ", "Count Used", "Count Avail."
                            }) {

                        boolean[] canEdit = new boolean[]{
                            false, false, false, false, false, false, false, false, false
                        };

                        @Override
                        public boolean isCellEditable(int rowIndex, int columnIndex) {
                            return canEdit[columnIndex];


                        }
                    });

                    String mainBenefit = "";
                    String midBenefit = "";
                    String lowBenefit = "";

                    java.text.DecimalFormat desFormat = new java.text.DecimalFormat("#.##");



                    for (CoverClaimBenefitLimitStructure ccb : benLimStruct) {
                        DefaultTableModel benLimModel = (DefaultTableModel) tbl_benLimBook.getModel();
                        System.out.println("loop count - " + x);
                        x++;

                        //override option name for benefit period
                        txt_ProductBLOption.setText(ccb.getOptionName());

                        String mainB = ccb.getMainDesc();
                        System.out.println("main benefit = " + mainBenefit);
                        System.out.println("main b = " + mainB);


                        if (mainB != null && mainB.equals(mainBenefit)) {
                            mainB = "";
                            System.out.println("found same main");


                        } else {
                            int mainBenId = ccb.getMainBenefitId();
                            mainBenefit = mainB;
                            mainB = "";


                            double mainAmount = ccb.getMainLimitAmount();
                            double mainAmountUsed = ccb.getMainLimitUsedAmount();
                            double mainAmountAvail = ccb.getMainLimitAvailAmount();
                            int mainCount = ccb.getMainLimitCount();
                            int mainCountUsed = ccb.getMainLimitUsedCount();
                            int mainCountAvail = ccb.getMainLimitAvailCount();

                            if (mainAmount != 0.0d) {
                                try {
                                    mainAmount = desFormat.parse(desFormat.format(mainAmount)).doubleValue();

                                } catch (ParseException ex) {
                                    System.out.println("exception" + ex);
                                }
                            }
                            if (mainAmountUsed != 0.0d) {
                                try {
                                    mainAmountUsed = desFormat.parse(desFormat.format(mainAmountUsed)).doubleValue();

                                } catch (ParseException ex) {
                                    System.out.println("exception" + ex);
                                }
                            }

                            System.out.println("main benefit found and add");
                            benLimModel.addRow(new Object[]{mainBenefit, "", "", mainAmount, mainAmountUsed, mainAmountAvail, mainCount, mainCountUsed, mainCountAvail});

                        }

                        String midB = ccb.getMidDesc();
                        System.out.println("mid benefit = " + midBenefit);
                        System.out.println("mid b = " + midB);

                        boolean midAdded = false;

                        if (midB != null) {
                            if (midB.equals(midBenefit)) {
                                midB = "";
                                System.out.println("same mid found");

                            } else {
                                int midBenId = ccb.getMidBenefitId();
                                midAdded = true;
                                midBenefit = midB;
                                midB = "";


                                double midAmount = ccb.getMidLimitAmount();
                                double midAmountUsed = ccb.getMidLimitUsedAmount();
                                double midAmountAvail = ccb.getMidLimitAvailAmount();
                                int midCount = ccb.getMidLimitCount();
                                int midCountUsed = ccb.getMidLimitUsedCount();
                                int midCountAvail = ccb.getMidLimitAvailCount();


                                if (midAmount != 0.0d) {
                                    try {
                                        midAmount = desFormat.parse(desFormat.format(midAmount)).doubleValue();

                                    } catch (ParseException ex) {
                                        System.out.println("exception" + ex);

                                    }

                                }
                                if (midAmountUsed != 0.0d) {
                                    try {
                                        midAmountUsed = desFormat.parse(desFormat.format(midAmountUsed)).doubleValue();


                                    } catch (ParseException ex) {
                                        System.out.println("exception" + ex);

                                    }
                                }

                                System.out.println("mid top benefit found and add");
                                benLimModel.addRow(new Object[]{mainB, midBenefit, "", midAmount, midAmountUsed, midAmountAvail, midCount, midCountUsed, midCountAvail});

                            }
                        }

                        //add low
                        lowBenefit = ccb.getLowDesc();
                        System.out.println("low benefit = " + lowBenefit);

                        if (lowBenefit != null && !lowBenefit.equalsIgnoreCase("")) {
                            System.out.println("low benefit found");

                            int lowBenId = ccb.getLowBenefitId();
                            double lowAmount = ccb.getLowLimitAmount();
                            double lowAmountUsed = ccb.getLowLimitUsedAmount();
                            double lowAmountAvail = ccb.getLowLimitAvailAmount();
                            int lowCount = ccb.getLowLimitCount();
                            int lowCountUsed = ccb.getLowLimitUsedCount();
                            int lowCountAvail = ccb.getLowLimitAvailCount();

                            if (lowAmount != 0.0d) {
                                try {
                                    lowAmount = desFormat.parse(desFormat.format(lowAmount)).doubleValue();


                                } catch (ParseException ex) {
                                    System.out.println("exception" + ex);


                                }
                            }
                            if (lowAmountUsed != 0.0d) {
                                try {
                                    lowAmountUsed = desFormat.parse(desFormat.format(lowAmountUsed)).doubleValue();


                                } catch (ParseException ex) {
                                    System.out.println("exception" + ex);


                                }
                            }

                            benLimModel.addRow(new Object[]{mainB, midB, lowBenefit, lowAmount, lowAmountUsed, lowAmountAvail, lowCount, lowCountUsed, lowCountAvail});

                        } else {

                            if (midB != null && midAdded == false) {

                                int midBenId = ccb.getMidBenefitId();
                                lowBenefit = "";
                                midBenefit = midB;
                                midB = "";

                                double midAmount = ccb.getMidLimitAmount();
                                double midAmountUsed = ccb.getMidLimitUsedAmount();
                                double midAmountAvail = ccb.getMidLimitAvailAmount();
                                int midCount = ccb.getMidLimitCount();
                                int midCountUsed = ccb.getMidLimitUsedCount();
                                int midCountAvail = ccb.getMidLimitAvailCount();

                                if (midAmount != 0.0d) {
                                    try {
                                        midAmount = desFormat.parse(desFormat.format(midAmount)).doubleValue();


                                    } catch (ParseException ex) {
                                        System.out.println("exception" + ex);

                                    }
                                }
                                if (midAmountUsed != 0.0d) {
                                    try {
                                        midAmountUsed = desFormat.parse(desFormat.format(midAmountUsed)).doubleValue();

                                    } catch (ParseException ex) {
                                        System.out.println("exception" + ex);


                                    }
                                }

                                System.out.println("mid bottom benefit found and add");
                                benLimModel.addRow(new Object[]{midB, midBenefit, lowBenefit, midAmount, midAmountUsed, midAmountAvail, midCount, midCountUsed, midCountAvail});

                            }
                        }
                    }
                }
            }
        }
}//GEN-LAST:event_btn_viewAllBenActionPerformed

    private void cb_InsuredDep2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cb_InsuredDep2ItemStateChanged
        // TODO add your handling code here:
}//GEN-LAST:event_cb_InsuredDep2ItemStateChanged

    private void cb_EntityTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cb_EntityTypeItemStateChanged
        String eType = cb_EntityType.getSelectedItem().toString();


        if (eType.equals("Practice")) {
            pnl_ProvNum.setVisible(true);


        } else {
            pnl_ProvNum.setVisible(false);
            txt_ProvNum.setText("");


        }
}//GEN-LAST:event_cb_EntityTypeItemStateChanged

    private void searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchActionPerformed
        PaymentRunSearchCriteria prs = new PaymentRunSearchCriteria();
        prs.setEntityNo(CoverNumber);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String eType = cb_EntityType.getSelectedItem().toString();


        if (!eType.trim().equalsIgnoreCase("")) {
            prs.setEntityType(eType);


        }
        if (eType.equals("Practice")) {
            String providerNumber = txt_ProvNum.getText();


            if (!providerNumber.trim().equalsIgnoreCase("")) {
                prs.setEntityNo(providerNumber);


            }
        }
        String payDate = cb_PayDates.getSelectedItem().toString();


        if (!payDate.trim().equals("")) {
            Date pd = null;


            try {
                pd = sdf.parse(payDate);


            } catch (ParseException ex) {
                System.out.println("ParserConfigurationException :" + ex.getMessage());


            }

            XMLGregorianCalendar xDate = UtilMethods.convertDateToXMLGeoCalender(pd);
            prs.setPaymentRunDate(xDate);


        }
        prs.setClearingClaimPayment(false);
        List<EntityPaymentRunDetails> epList = NeoWebService.getNeoManagerBeanService().getPaymentRunDetailForEntity(prs, null);


        int rowSize = 0;
        rowSize = epList.size();


        if (rowSize != 0) {
            addTableResultRows(rowSize, "paymentTable");


            int x = 0;


            for (EntityPaymentRunDetails ep : epList) {
                paymentTable.setValueAt(ep.getPaymentRunId(), x, 0);
                paymentTable.setValueAt(ep.getBankName(), x, 1);
                paymentTable.setValueAt(ep.getBranchName(), x, 2);
                paymentTable.setValueAt(ep.getBranchCode(), x, 3);
                paymentTable.setValueAt(ep.getAccountHolder(), x, 4);
                paymentTable.setValueAt(ep.getAccountNumber(), x, 5);
                paymentTable.setValueAt(ep.getAccountType(), x, 6);
                paymentTable.setValueAt(ep.getPaymentType(), x, 7);
                paymentTable.setValueAt(ep.getPaymentRunAmount(), x, 8);
                paymentTable.setValueAt(ep.getPaymentRunDate(), x, 9);
                paymentTable.setValueAt(ep.getPaymentStatus(), x, 10);
                paymentTable.setValueAt(ep.getPaymentRejectionReason(), x, 11);

                x++;

            }


            pnlResult.setVisible(true);



        } else {
            NeoMessageDialog.showInfoMessage("No payment run details found");
            clearTable();
            pnlResult.setVisible(false);



        }
    }//GEN-LAST:event_searchActionPerformed

    private void backChronicPreAuthTabBtn3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backChronicPreAuthTabBtn3ActionPerformed
        // TODO add your handling code here:
        PolicyActionFrame.policyManagementList.setSelectedIndex(0);
        backButtonAction();
}//GEN-LAST:event_backChronicPreAuthTabBtn3ActionPerformed

    private void backChronicPreAuthTabBtn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backChronicPreAuthTabBtn2ActionPerformed
        // TODO add your handling code here:
        PolicyActionFrame.policyManagementList.setSelectedIndex(0);
        backButtonAction();
}//GEN-LAST:event_backChronicPreAuthTabBtn2ActionPerformed

    private void backChronicPreAuthTabBtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backChronicPreAuthTabBtn1ActionPerformed
        // TODO add your handling code here:
        PolicyActionFrame.policyManagementList.setSelectedIndex(0);
        backButtonAction();
}//GEN-LAST:event_backChronicPreAuthTabBtn1ActionPerformed

    private void backPreAuthTabBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backPreAuthTabBtnActionPerformed
        // TODO add your handling code here:
        // TODO add your handling code here:
        PolicyActionFrame.policyManagementList.setSelectedIndex(0);
        backButtonAction();
    }//GEN-LAST:event_backPreAuthTabBtnActionPerformed

    private void txt_EdiIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_EdiIdActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_txt_EdiIdActionPerformed

    private void txt_AuthNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_AuthNumberActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_txt_AuthNumberActionPerformed

    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed

        ClaimSearchCriteria csc = new ClaimSearchCriteria();
        String scan = txt_ScanIndexNumber.getText();
        String batch = txt_BatchNumber.getText();
        String claim = txt_ClaimNumber.getText();
        String depNo = txt_DependentNo.getText();
        String ediId = txt_EdiId.getText();
        String provDiscipline = txt_provDiscipline.getText();
        String tariffCode = txt_TariffCode.getText();
        String claimStatus = cb_ClaimStatus.getSelectedItem().toString();
        Date processDate = dcb_ProcessDate.getDate();
        Date paymentDate = dcb_PaymentDate.getDate();
        Date serviceFrom = dcb_ServiceDateFrom.getDate();
        Date serviceTo = dcb_ServiceDateTo.getDate();

        csc.setCoverNumber(txt_CoverNumber.getText());
        csc.setProviderNumber(txt_ProviderNumber.getText());
        csc.setPracticeNumber(txt_PracticeNumber.getText());
        csc.setDisciplineId(txt_provDiscipline.getText());
        csc.setTariffCodeNo(txt_TariffCode.getText());



        if (!claimStatus.trim().equalsIgnoreCase("All")) {
            LookupValue cs = claimStatusMap.get(claimStatus);
            System.out.println("ClaimStatus id = " + cs.getId());
            System.out.println("ClaimStatus value = " + cs.getValue());
            csc.setClaimStatus(cs);


        }

        if (processDate != null) {
            XMLGregorianCalendar xmlProcess = UtilMethods.convertDateToXMLGeoCalender(processDate);
            csc.setProcessDateFrom(xmlProcess);


        } else {
            csc.setProcessDateFrom(null);


        }

        if (serviceFrom != null) {
            XMLGregorianCalendar xmlServFrom = UtilMethods.convertDateToXMLGeoCalender(serviceFrom);
            csc.setServiceDateFrom(xmlServFrom);


        } else {
            csc.setServiceDateFrom(null);


        }

        if (serviceTo != null) {
            XMLGregorianCalendar xmlServTo = UtilMethods.convertDateToXMLGeoCalender(serviceTo);
            csc.setServiceDateTo(xmlServTo);


        } else {
            csc.setServiceDateTo(null);


        }

        csc.setAccountNumber(txt_AccountNumber.getText());


        if (!scan.trim().equalsIgnoreCase("")) {
            csc.setScanId(Integer.parseInt(scan));


        }

        if (!batch.trim().equalsIgnoreCase("")) {
            csc.setBatchId(Integer.parseInt(batch));


        }

        if (!claim.trim().equalsIgnoreCase("")) {
            csc.setClaimId(Integer.parseInt(claim));


        }

        if (!depNo.trim().equalsIgnoreCase("")) {
            csc.setDependentCode(Integer.parseInt(depNo));


        }

        csc.setAuthNumber(txt_AuthNumber.getText());


        if (paymentDate != null) {
            XMLGregorianCalendar xmlPay = UtilMethods.convertDateToXMLGeoCalender(paymentDate);
            csc.setPaymentDate(xmlPay);


        } else {
            csc.setPaymentDate(null);


        }

        if (!ediId.trim().equalsIgnoreCase("")) {
            csc.setEdiId(Integer.parseInt(ediId));


        }

        Security _secure = new Security();
        _secure.setCreatedBy(LoginPage.neoUser.getUserId());
        _secure.setLastUpdatedBy(LoginPage.neoUser.getUserId());
        _secure.setSecurityGroupId(2);

        ClaimSearchResult returnList = NeoWebService.getNeoManagerBeanService().fetchBatchClaimAndClaimLineIdsBySearchCriteria(csc, _secure);
        if (returnList != null) {
            if (returnList.getBatchs() != null) {
                if (returnList.getBatchs().size() != 0) {
                    final JidePopup popup = new JidePopup();
                    JFrame frame = new JFrame();
                    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    frame.add(new ClaimEnquiryResult(returnList.getBatchs() ));
                    frame.setSize(915, 750);
                    popup.setMovable(true);
                    frame.setVisible(true);
                    popup.showPopup();
                } else {
                    String msg = returnList.getSearchResultMessage();
                    JOptionPane.showMessageDialog(null, msg+"", "Claims Enquiry", JOptionPane.ERROR_MESSAGE);
                    txt_CoverNumber.setText("");
                    txt_ProviderNumber.setText("");
                    txt_PracticeNumber.setText("");
                }
            } else {
                String msg = returnList.getSearchResultMessage();
                JOptionPane.showMessageDialog(null, msg+"", "Claims Enquiry", JOptionPane.ERROR_MESSAGE);
                txt_CoverNumber.setText("");
                txt_ProviderNumber.setText("");
                txt_PracticeNumber.setText("");
            }
        } else {
            JOptionPane.showMessageDialog(null, "No records found!", "Claims Enquiry", JOptionPane.ERROR_MESSAGE);
            txt_CoverNumber.setText("");
            txt_ProviderNumber.setText("");
            txt_PracticeNumber.setText("");
        }
}//GEN-LAST:event_searchButtonActionPerformed

    private void btn_ViewClaimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ViewClaimActionPerformed
        // TODO add your handling code here:
        if (selectedBenRow != 0) {
            String selectedSysTerm = benefitTable.getValueAt(selectedBenRow, 1).toString();


            for (CoverClaimLimits lims : claimProdLimits) {
                String sysTerm = lims.getSystemTerm();
                System.out.println("systerm = " + sysTerm);


                if (sysTerm != null) {
                    if (sysTerm.equals(selectedSysTerm)) {
                        Collection<ClaimsBatch> returnList = lims.getClaimBatch();
                        System.out.println("claim batch for systerm size = " + returnList.size());



                        if (returnList.size() != 0) {
                            final JidePopup popup = new JidePopup();
                            JFrame frame = new JFrame();
                            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                            frame.add(new SearchResult(returnList));
                            frame.setSize(915, 750);
                            popup.setMovable(true);
                            frame.setVisible(true);
                            popup.showPopup();



                        } else {
                            System.out.println("Claim batch error");



                        }
                        break;


                    }
                }
            }
        }
}//GEN-LAST:event_btn_ViewClaimActionPerformed

    private void backBenefitTabBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBenefitTabBtnActionPerformed
        // TODO add your handling code here:
        PolicyActionFrame.policyManagementList.setSelectedIndex(0);
        backButtonAction();

    }//GEN-LAST:event_backBenefitTabBtnActionPerformed

    private void benefitTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_benefitTableMouseReleased
        // TODO add your handling code here:
        int selected = benefitTable.getSelectedRow();
        selectedBenRow = selected;
        System.out.println("selected = " + selected);
        System.out.println("selectedBenRow = " + selectedBenRow);
}//GEN-LAST:event_benefitTableMouseReleased
    private void searchBenefitActionBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchBenefitActionBtnActionPerformed
        // 2010 Benefits
        if (txt_Option.getText().startsWith("Fundamental")) {
            NeoMessageDialog.showInfoMessage("Benefit Limits for the Option Fundamental is not Supported");



        } else {
            Date date2010 = (new GregorianCalendar(2010, Calendar.DECEMBER, 1)).getTime();
            XMLGregorianCalendar xml2010 = UtilMethods.convertDateToXMLGeoCalender(date2010);
            String coverNumber = txt_PolicyNumber.getText();
            Double limitsUsed = 0.0d;
            Date benFrom = dcb_BenefitDateFrom.getDate();
            Date benTo = dcb_BenefitDateTo.getDate();
            XMLGregorianCalendar benStart = null;
            XMLGregorianCalendar benEnd = null;
            java.text.DecimalFormat desFormat = new java.text.DecimalFormat("#.##");

            Date year10 = null;
            Date year11 = null;
            try {
                //2011 date check
                year10 = new SimpleDateFormat("yyyy/MM/dd").parse("2010/01/01");
                year11 = new SimpleDateFormat("yyyy/MM/dd").parse("2010/12/31");
            } catch (ParseException ex) {
                Logger.getLogger(MedcorInsuredPersonPolicyDetails.class.getName()).log(Level.SEVERE, null, ex);
            }

            Calendar yearStartDate = Calendar.getInstance();
            if (year10 != null) {
                yearStartDate.setTime(year10);
            }


            Calendar yearEndDate = Calendar.getInstance();
            if (year11 != null) {
                yearEndDate.setTime(year11);
            }

            Calendar benefitFrom = Calendar.getInstance();
            benefitFrom.setTime(benFrom);

            Calendar benefitTo = Calendar.getInstance();
            benefitTo.setTime(benTo);


            if (benefitFrom.before(yearStartDate) || benefitTo.after(yearEndDate)) {
                NeoMessageDialog.showErrorMessage("Benefit Date Before 2010/01/01 or after 2010/12/31 is not Allowed");
            } else {


                if (benFrom != null) {
                    benStart = UtilMethods.convertDateToXMLGeoCalender(benFrom);


                }
                if (benTo != null) {
                    benEnd = UtilMethods.convertDateToXMLGeoCalender(benTo);


                }
                String depDesc = cb_InsuredDep.getSelectedItem().toString();


                int depId = dependentMap.get(depDesc);
                List<CoverClaimLimits> ccLimList = NeoWebService.getNeoManagerBeanService().findCoverBenefitsByDate(coverNumber, depId, xml2010, benStart, benEnd);
                System.out.println("ccLimList size = " + ccLimList.size());
                System.out.println("dependentId = " + depId);
                String benDesc = "";


                int rowSize = ccLimList.size();


                if (rowSize > 0) {
                    claimProdLimits = ccLimList;


                    int x = 0;
                    addTableResultRows(
                            rowSize, "benefitTable");


                    for (CoverClaimLimits ccLim : ccLimList) {
                        Object benefit = null;
                        Font Sub = new Font("Tahoma", Font.BOLD, 11);
                        Font defaultFont = new Font("Tahoma", Font.PLAIN, 11);


                        if (ccLim.getDependentId() == depId) {
                            if (!benDesc.equals(ccLim.getBenefitDesc())) {
                                benDesc = ccLim.getBenefitDesc();
                                benefit = benDesc;
                                System.out.println("Found benefit = " + benefit);


                            }
                            System.out.println("");
                            System.out.println("Found systemTerm for benefit = " + ccLim.getSystemTerm());
                            System.out.println("");
                            limitsUsed = ccLim.getAmountPaid();



                            if (ccLim.getLimitAvail() == 0) {
                                //add benfit depleted msg
                                benefitTable.setValueAt("Benefit Limit depleted", x, 3);


                            }

                            try {
                                limitsUsed = desFormat.parse(desFormat.format(limitsUsed)).doubleValue();


                            } catch (ParseException ex) {
                                System.out.println("exception" + ex);


                            }
                            benefitTable.setValueAt(benefit, x, 0);
                            benefitTable.setValueAt(ccLim.getSystemTerm(), x, 1);
                            benefitTable.setValueAt(ccLim.getClaimCount(), x, 2);
                            benefitTable.setValueAt(limitsUsed, x, 5);



                            if (ccLim.getLimitType().getValue().equalsIgnoreCase("value")) {
                                benefitTable.setValueAt(ccLim.getDefaultAmount(), x, 4);
                                benefitTable.setValueAt(ccLim.getLimitAvail(), x, 6);


                            } else {
                                benefitTable.setValueAt(ccLim.getCountAvail(), x, 6);
                                benefitTable.setValueAt(ccLim.getDefaultAmount(), x, 3);


                            }
                            x++;


                        }
                    }

                } else {
                    NeoMessageDialog.showErrorMessage("No Benefit Limits Found");


                }
            }
        }
    }//GEN-LAST:event_searchBenefitActionBtnActionPerformed

    private void cb_InsuredDepItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cb_InsuredDepItemStateChanged
        // TODO add your handling code here:
       /*if (TotalPoliciesInformation.policyNumberTxt.getText() != null) {
        List<OptionVersion> optionVersions = NeoWebService.getNeoManagerBeanService().getOptionVersionsForInsured(TotalPoliciesInformation.policyNumberTxt.getText(), insurerMap.get(insuredPersonList.getSelectedItem()));
        String[] options = new String[optionVersions.size()];
        int st = 0;
        for (OptionVersion optionVersion : optionVersions) {
        options[st] = optionVersion.getDescription();
        OptionMap.put(options[st], optionVersion.getOptionVersionId());
        st++;
        }
        optionList.setModel(new javax.swing.DefaultComboBoxModel(options));
        }*/
}//GEN-LAST:event_cb_InsuredDepItemStateChanged

    private void dcb_BenefitDateFromActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dcb_BenefitDateFromActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_dcb_BenefitDateFromActionPerformed

    private void btn_UnderwritingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_UnderwritingActionPerformed
        UnderwritingViewFrame underView = new UnderwritingViewFrame(coverEntityId);
        underView.setVisible(true);
    }//GEN-LAST:event_btn_UnderwritingActionPerformed
    private void btn_AddDepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AddDepActionPerformed
        df.NewDependentAddAction(CoverNumber, "policy");
    }//GEN-LAST:event_btn_AddDepActionPerformed

    private void btn_ViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ViewActionPerformed
        int selectedRow = tbl_PersonDetails.getSelectedRow();
        String details = tbl_PersonDetails.getValueAt(selectedRow, 0).toString();



        if (details.equals("Address Details")) {
            System.out.println("test Address");
            AddressFrame searchFrame = new AddressFrame();
            AddressFrame.txt_AddressCoverNumber.setText(policyNumberInsTabTxt.getText());
            AddressFrame.txt_AddressCoverName.setText(coverNameInsTabTxt.getText());

            CoverDetails coverDetails = NeoWebService.getNeoManagerBeanService().getPrincipalMemberDetailsToday(CoverNumber);
            coverEntityId =
                    coverDetails.getEntityId();
            AddressFrame.entityid = coverEntityId;
            AddressFrame.entityCommonId = coverDetails.getEntityCommonId();
            AddressFrame.loadAddressDetails();

            final JidePopup popup = new JidePopup();
            popup.setMovable(true);
            searchFrame.setVisible(true);
            popup.showPopup();



        } else if (details.equals("Banking Details")) {
            System.out.println("test Banking");
            System.out.println("test Address");
            CoverDetails coverDetails = NeoWebService.getNeoManagerBeanService().getPrincipalMemberDetailsToday(CoverNumber);
            coverEntityId = coverDetails.getEntityId();
            String coverNum = policyNumberInsTabTxt.getText();
            //boolean addressFlag = true;
            BankDetailsFrame searchFrame = new BankDetailsFrame(coverEntityId, coverNameInsTabTxt.getText(), coverNum);
            BankDetailsFrame.bankCommonId = coverDetails.getEntityCommonId();
            //BankDetailsFrame.btn_BankingSubmit.setVisible(false);


            final JidePopup popup = new JidePopup();
            popup.setMovable(true);
            searchFrame.setVisible(true);
            popup.showPopup();



        } else if (details.equals("Contact Details")) {
            System.out.println("test Contact");
            ContactDetailsFrame searchFrame = new ContactDetailsFrame();
            ContactDetailsFrame.coverNumberContactTxt.setText(policyNumberInsTabTxt.getText());

            CoverDetails coverDetails = NeoWebService.getNeoManagerBeanService().getPrincipalMemberDetailsToday(CoverNumber);
            coverEntityId =
                    coverDetails.getEntityId();
            ContactDetailsFrame.entityId = coverEntityId;
            ContactDetailsFrame.contactCommonId = coverDetails.getEntityCommonId();
            ContactDetailsFrame.btn_ContactSubmit.setVisible(false);
            ContactDetailsFrame.btn_ContactClear.setVisible(false);
            ContactDetailsFrame.loadContactDetails();

            final JidePopup popup = new JidePopup();
            popup.setMovable(true);
            searchFrame.setVisible(true);
            popup.showPopup();




        } else if (details.equals("Contact Preferences")) {
            System.out.println("test ConPref");
            String name = coverNameInsTabTxt.getText();
            ContactPreferenceFrame searchFrame = new ContactPreferenceFrame(coverEntityId, name);
            ContactPreferenceFrame.coverNumberContactPrefTxt.setText(policyNumberInsTabTxt.getText());
            ContactPreferenceFrame.coverNameContactPrefTxt.setText(coverNameInsTabTxt.getText());
            CoverDetails coverDetails = NeoWebService.getNeoManagerBeanService().getPrincipalMemberDetailsToday(CoverNumber);
            coverEntityId = coverDetails.getEntityId();
            ContactPreferenceFrame.entityId = coverEntityId;
            ContactPreferenceFrame.conPrefCommonId = coverDetails.getEntityCommonId();
            ContactPreferenceFrame.loadContactPreferences();

            final JidePopup popup = new JidePopup();
            popup.setMovable(true);
            searchFrame.setVisible(true);
            popup.showPopup();



        }
    }//GEN-LAST:event_btn_ViewActionPerformed

    private void backInsPersonTabBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backInsPersonTabBtnActionPerformed
        // TODO add your handling code here:
        PolicyActionFrame.policyManagementList.setSelectedIndex(0);
        backButtonAction();
}//GEN-LAST:event_backInsPersonTabBtnActionPerformed
    private void listingofPersonsMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listingofPersonsMouseReleased
        int selected = listingofPersons.getSelectedRow();


        int depNo = Integer.parseInt(listingofPersons.getValueAt(selected, 0).toString());


        for (CoverDetails cd : coverMemList) {
            if (cd.getDependentNumber() == depNo) {
                coverEntityId = cd.getEntityId();



            }
        }
        System.out.println("depNo = " + depNo);
        System.out.println("coverEntityId = " + coverEntityId);
}//GEN-LAST:event_listingofPersonsMouseReleased

    private void dcb_2011BenefitDateFromActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dcb_2011BenefitDateFromActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_dcb_2011BenefitDateFromActionPerformed

    private void dcb_2011BenefitDateFromFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dcb_2011BenefitDateFromFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_dcb_2011BenefitDateFromFocusLost

    private void dcb_2011BenefitDateFromItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_dcb_2011BenefitDateFromItemStateChanged
        // TODO add your handling code here:
        String coverNumber = txt_PolicyNumber1.getText();
        Date benDate = dcb_2011BenefitDateFrom.getDate();
        System.out.println("coverNumber for benefit = " + coverNumber);
        txt_PolicyNumber1.setText(coverNumber);
        ArrayList strArr = new ArrayList();

        XMLGregorianCalendar xml = UtilMethods.convertDateToXMLGeoCalender(benDate);
        List<CoverDetails> dependentList = NeoWebService.getNeoManagerBeanService().getAllDependentsForCoverByDate(coverNumber, xml);

       
        for (CoverDetails dep : dependentList) {
            int depNo = dep.getDependentNumber();
            String type = dep.getDependentType();
            String status = dep.getStatus();
            String depType = depNo + " - " + type + " - " + status;
            dependentMap.put(depType, depNo);
            strArr.add(depType);

        }

        CoverDetails cov = NeoWebService.getNeoManagerBeanService().getPrincipalMemberDetailsByDate(coverNumber, xml);


        int depNo = cov.getDependentNumber();
        String type = cov.getDependentType();
        String status = cov.getStatus();
        String depType = depNo + " - " + type + " - " + status;
        dependentMap.put(depType, depNo);
        strArr.add(depType);

        cb_InsuredDep2.setModel(new javax.swing.DefaultComboBoxModel(strArr.toArray()));
        cb_InsuredDep2.setSelectedItem("");

         List<PolicyProduct> ppList = NeoWebService.getNeoManagerBeanService().fetchProductDetailsForPolicyByDate(coverNumber, xml);

        int ppListSize = ppList.size();

        if (ppListSize > 0) {
            PolicyProduct pp = ppList.get(0);
            txt_PolicyProductBL.setText(pp.getProductDesc());
            txt_ProductBLOption.setText(pp.getOptionName());
        }
    }//GEN-LAST:event_dcb_2011BenefitDateFromItemStateChanged

    public void hideMedcorObjects() {

        //hide benefit limits tab for cimas
        //Component benLim = tpane_MedcorInsured.getComponentAt(1);
        //tpane_MedcorInsured.remove(benLim);
        //hide tabs
        Component nonMed = tpane_MedcorInsured.getComponentAt(4);
        Component card = tpane_MedcorInsured.getComponentAt(5);
        Component audit = tpane_MedcorInsured.getComponentAt(6);
        tpane_MedcorInsured.remove(nonMed);
        tpane_MedcorInsured.remove(card);
        tpane_MedcorInsured.remove(audit);

        //hide buttons
        //btn_AddDep.setVisible(false);


    }

    private void backButtonAction() {
        MedcorPolicyManagement.coverNumberTxt.setText(policyNumberInsTabTxt.getText());
        MedcorPolicyManagement.coverDisplayPanel.setVisible(true);
        MedcorPolicyManagement.displayPolicyInformation(policyNumberInsTabTxt.getText());




    }

// Main Method for display All the Tabs
    public static void displayAllInsuredPersonDetails(String coverNumber) {
        System.out.println("Access error");
//        try {
//            displayInsuredPersonTab(coverNumber);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        try {
//            displayBenefitLimits(coverNumber);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        try {
//            displayClaimsHistory(coverNumber);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        try {
//            displayCardsHistory(coverNumber);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        try {
//            displayAuditTrail(coverNumber);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        try {
//            displayNotes(coverNumber);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        try {
//            authhorizationGridDisplay();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }



    }

    /******************** Start Insured Person Tab Details*********************************/
//Insured Person Tab for Display details
    public static void displayInsuredPersonTab(String coverNumber) {

        policyNumberInsTabTxt.setText(coverNumber);
        getCoverPersonTableDisplay(
                coverNumber);
        getCoverDetails(
                coverNumber);



    }

//Cover Persons Table
    public static void getCoverPersonTableDisplay(String coverNumber) {
        System.out.println("getCoverPersonTableDisplay coverNumber = " + coverNumber);
        List<CoverDetails> coverDetailsList = NeoWebService.getNeoManagerBeanService().getCoverDetailsByCoverNumber(coverNumber);
        CoverProductDetails cp = NeoWebService.getNeoManagerBeanService().getCoverProductDetailsByCoverNumber(coverNumber);
        coverEntityId = cp.getEntityId();
        PersonDetails pd = NeoWebService.getNeoManagerBeanService().getPersonDetailsByEntityId(coverEntityId);
        coverNameInsTabTxt.setText(pd.getName());
        coverMemList = coverDetailsList;
        String[] ListPersonscolumns = new String[]{"Insured Number", "Name", "Relationship Type", "Status", "Date of Birth", "Date _ From", "Date _ To"};
        TableFormater.addTableResultsRows(listingofPersons, ListPersonscolumns, coverDetailsList.size());


        int i = 0;


        for (CoverDetails coverDetails : coverDetailsList) {

            listingofPersons.setValueAt(coverDetails.getDependentNumber(), i, 0);
            listingofPersons.setValueAt(coverDetails.getName(), i, 1);
            listingofPersons.setValueAt(coverDetails.getDependentType(), i, 2);
            listingofPersons.setValueAt(coverDetails.getStatus(), i, 3);



            if (coverDetails.getDateOfBirth() != null) {
                String DOB = UtilMethods.convertDateToString(coverDetails.getDateOfBirth().toGregorianCalendar().getTime());
                listingofPersons.setValueAt(DOB, i, 4);


            } else {
                listingofPersons.setValueAt("No Data", i, 4);


            }

            if (coverDetails.getCoverStartDate() != null) {
                String coverStartDate = UtilMethods.convertDateToString(coverDetails.getCoverStartDate().toGregorianCalendar().getTime());
                listingofPersons.setValueAt(coverStartDate, i, 5);


            } else {
                listingofPersons.setValueAt("No Data", i, 5);


            }

            if (coverDetails.getCoverEndDate() != null) {
                String coverEndDate = UtilMethods.convertDateToString(coverDetails.getCoverEndDate().toGregorianCalendar().getTime());
                listingofPersons.setValueAt(coverEndDate, i, 6);


            } else {
                listingofPersons.setValueAt("No Data", i, 6);


            }

            i++;


        }

    }
    //CoverDetails for Insured Person

    public static void getCoverDetails(String CoverNumber) {

        CoverDetails coverDetails = NeoWebService.getNeoManagerBeanService().getPrincipalMemberDetailsToday(CoverNumber);
        coverEntityId =
                coverDetails.getEntityId();
        System.out.println(coverEntityId + "--------entityID");
        List<AttributeTypes> attributeList = new ArrayList<AttributeTypes>();//NeoWebService.getNeoManagerBeanService().getEntityAttributeTypes(coverEntityId);


        for (AttributeTypes attributeTypes : attributeList) {
            if (attributeTypes.getScreenPrompt().equals("Employer")) {
                //set value to txt field
                coverDetailsTable.setValueAt(attributeTypes.getAttributeValue(), 0, 0);


            }

            if (attributeTypes.getScreenPrompt().equals("Payroll Number")) {
                //set value to txt field
                coverDetailsTable.setValueAt(attributeTypes.getAttributeValue(), 0, 1);


            }

            if (attributeTypes.getScreenPrompt().equals("Paypoint")) {
                //set value to combo box
                coverDetailsTable.setValueAt(attributeTypes.getAttributeValue(), 0, 2);


            }

        }
    }

    /******************** END Insured Person Tab Display*********************************/
    /******************** 2010 Benefit Limits *******************************************/
    public static void displayBenefitLimits(String coverNumber) {
        Date date2010 = (new GregorianCalendar(2010, Calendar.DECEMBER, 1)).getTime();
        XMLGregorianCalendar xml2010 = UtilMethods.convertDateToXMLGeoCalender(date2010);
        System.out.println("coverNumber for benefit = " + coverNumber + " : " + xml2010);
        txt_PolicyNumber.setText(coverNumber);
        ArrayList strArr = new ArrayList();
        List<PolicyProduct> ppList = NeoWebService.getNeoManagerBeanService().fetchProductDetailsForPolicyByDate(coverNumber, xml2010);


        int ppListSize = ppList.size();
        System.out.println("policy prod size = " + ppListSize);


        if (ppListSize > 0) {
            PolicyProduct pp = ppList.get(0);
            txt_Product.setText(pp.getProductDesc());
            txt_Status.setText(pp.getCoverStatus().getValue());
            txt_Option.setText(pp.getOptionName());

            List<CoverDetails> dependentList = NeoWebService.getNeoManagerBeanService().getAllDependentsForCoverByDate(coverNumber, xml2010);


            for (CoverDetails dep : dependentList) {
                int depNo = dep.getDependentNumber();
                String type = dep.getDependentType();
                String status = dep.getStatus();
                String depType = depNo + " - " + type + " - " + status;
                dependentMap.put(depType, depNo);
                System.out.println("deptype for dependant - " + depType);
                strArr.add(depType);


            }
            CoverDetails cov = NeoWebService.getNeoManagerBeanService().getPrincipalMemberDetailsByDate(coverNumber, xml2010);


            int depNo = cov.getDependentNumber();
            String type = cov.getDependentType();
            String status = cov.getStatus();
            String depType = depNo + " - " + type + " - " + status;
            dependentMap.put(depType, depNo);
            System.out.println("deptype for main - " + depType);
            strArr.add(depType);

            System.out.println("Dependant list size = " + strArr.size());
            cb_InsuredDep.setModel(new javax.swing.DefaultComboBoxModel(strArr.toArray()));
            cb_InsuredDep.setSelectedItem("");



        } else {
            NeoMessageDialog.showErrorMessage("Policy Product fetch faild");


        }

    }

    //benefit booking and limits = 2011 and beyond...
    public static void displayBenefitLimitsAndBookings(String coverNumber) {
        Date date = new Date();


        Date benDate = dcb_2011BenefitDateFrom.getDate();

        /*Calendar benefitDate = Calendar.getInstance();
        if (benDate != null) {
        benefitDate.setTime(benDate);
        }*/
        XMLGregorianCalendar xml = UtilMethods.convertDateToXMLGeoCalender(date);
        if (benDate != null){
            xml = UtilMethods.convertDateToXMLGeoCalender(benDate);
        }


        System.out.println("coverNumber for benefit = " + coverNumber);
        txt_PolicyNumber1.setText(coverNumber);
        ArrayList strArr = new ArrayList();
        List<PolicyProduct> ppList = NeoWebService.getNeoManagerBeanService().fetchProductDetailsForPolicyByDate(coverNumber, xml);

        int ppListSize = ppList.size();

        if (ppListSize > 0) {
            PolicyProduct pp = ppList.get(0);
            txt_PolicyProductBL.setText(pp.getProductDesc());
            txt_ProductBLOption.setText(pp.getOptionName());

            List<CoverDetails> dependentList = NeoWebService.getNeoManagerBeanService().getAllDependentsForCoverByDate(coverNumber, xml);


            for (CoverDetails dep : dependentList) {
                int depNo = dep.getDependentNumber();
                String type = dep.getDependentType();
                String status = dep.getStatus();
                String depType = depNo + " - " + type + " - " + status;
                dependentMap.put(depType, depNo);
                strArr.add(depType);


            }
            CoverDetails cov = NeoWebService.getNeoManagerBeanService().getPrincipalMemberDetailsByDate(coverNumber, xml);


            int depNo = cov.getDependentNumber();
            String type = cov.getDependentType();
            String status = cov.getStatus();
            String depType = depNo + " - " + type + " - " + status;
            dependentMap.put(depType, depNo);
            strArr.add(depType);

            cb_InsuredDep2.setModel(new javax.swing.DefaultComboBoxModel(strArr.toArray()));
            cb_InsuredDep2.setSelectedItem("");



        } else {
            NeoMessageDialog.showErrorMessage("Policy Product fetch faild");


        }

    }

    /***************Pre Auth ************************************************************/
    public static void authhorizationGridDisplay() {
        try {
            List<PreAuthView> preAuthData = NeoWebService.getNeoManagerBeanService().fetchPDCTableData();



            int Count = preAuthData.size();
            DefaultTableModel displayTableModel = (DefaultTableModel) authorizationTable.getModel();


            if (Count != 0) {
                int i = 0;


                for (PreAuthView preAuthView : preAuthData) {
                    displayTableModel.addRow(new Object[]{});

                    authorizationTable.setValueAt(preAuthView.getPreAuthNumber(), i, 0);
                    authorizationTable.setValueAt(preAuthView.getAuthStatus(), i, 1);
                    authorizationTable.setValueAt(preAuthView.getProviderName(), i, 2);

                    XMLGregorianCalendar admissionDate = preAuthView.getAdmissionDate();
                    authorizationTable.setValueAt(admissionDate.toGregorianCalendar().getTime(), i, 3);



                    int admissionDay = admissionDate.getDay();


                    int los = preAuthView.getLOS();
                    admissionDay =
                            admissionDay + los;


                    if (admissionDay < 31) {
                        admissionDate.setDay(admissionDay);


                    } else {
                        admissionDate.setDay(30);


                    }

                    authorizationTable.setValueAt(admissionDate.toGregorianCalendar().getTime(), i, 4);
                    authorizationTable.setValueAt(preAuthView.getIcd10Codes(), i, 5);
                    authorizationTable.setValueAt(preAuthView.getCptCodes(), i, 6);
                    authorizationTable.setValueAt(preAuthView.getInsuredPersonNumber(), i, 7);
                    authorizationTable.setValueAt(los, i, 8);

                    i++;

                }


            }

        } catch (Exception e) {
            System.out.println("no preAutg Data");


        }

    }

    public static void loadPaymentRunDetails() {
        Collection<XMLGregorianCalendar> dates = NeoWebService.getNeoManagerBeanService().getPaymentRunDates();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        cb_PayDates.removeAllItems();


        for (XMLGregorianCalendar xDate : dates) {
            Date d = xDate.toGregorianCalendar().getTime();
            String date = sdf.format(d);
            //Date date = xDate.toGregorianCalendar().getTime();
            cb_PayDates.addItem(date);


        }
        cb_EntityType.addItem("");
        cb_PayDates.addItem("");
        cb_PayDates.setSelectedItem("");
        cb_EntityType.setSelectedItem("");


    }

    public static void addTableResultRows(int reSize, String tblName) {
        int reinTableRows = 0;


        if (tblName.equals("benefitTable")) {
            reinTableRows = benefitTable.getRowCount();


            if (reinTableRows != 0) {
                //removing all rows
                benefitTable.setModel(new javax.swing.table.DefaultTableModel(
                        new Object[][]{},
                        new String[]{
                            "Benefit Description", "System Term", "Claims Count", "Limit (Count)", "Limit Allowed", "Limit Used", "Limit Available"
                        }) {

                    boolean[] canEdit = new boolean[]{
                        false, false, false, false, false, false, false
                    };

                    @Override
                    public boolean isCellEditable(int rowIndex, int columnIndex) {
                        return canEdit[columnIndex];


                    }
                });
                //adding rows according to request
                DefaultTableModel displayTableModel = (DefaultTableModel) benefitTable.getModel();


                for (int k = 0; k
                        < reSize; k++) {
                    displayTableModel.addRow(new Object[]{});


                }
            } else {
                //adding rows according to request
                DefaultTableModel displayTableModel = (DefaultTableModel) benefitTable.getModel();


                for (int k = 0; k
                        < reSize; k++) {
                    displayTableModel.addRow(new Object[]{});


                }
            }
        }
        if (tblName.equals("paymentTable")) {
            reinTableRows = paymentTable.getRowCount();


            if (reinTableRows != 0) {
                //removing all rows
                paymentTable.setModel(new javax.swing.table.DefaultTableModel(
                        new Object[][]{},
                        new String[]{
                            "Payment Id", "Bank Name", "Branch Name", "Branch Code", "Acc Holder", "Acc No", "Acc Type", "Payment Type", "Amount", "Run Date", "Status", "Rejection Reason"
                        }));

                //adding rows according to request
                DefaultTableModel displayTableModel = (DefaultTableModel) paymentTable.getModel();


                for (int k = 0; k
                        < reSize; k++) {
                    displayTableModel.addRow(new Object[]{});


                }
            } else {
                //adding rows according to request
                DefaultTableModel displayTableModel = (DefaultTableModel) paymentTable.getModel();


                for (int k = 0; k
                        < reSize; k++) {
                    displayTableModel.addRow(new Object[]{});


                }
            }
        }
    }

    public void clearTable() {
        paymentTable.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "Payment Id", "Bank Name", "Branch Name", "Branch Code", "Acc Holder", "Acc No", "Acc Type", "Payment Type", "Amount", "Run Date", "Status", "Rejection Reason"
                }));


    }

    /*********************Claims History ************************************************/
    public static void displayClaimsHistory(String coverNumber) {
        txt_CoverNumber.setText(coverNumber);
        Object[] claimStatus = UtilMethods.getLookupValues("Claim Status", claimStatusMap);
        ArrayList cs = new ArrayList();
        cs.add("All");


        for (Object object : claimStatus) {
            cs.add(object);


        }
        cb_ClaimStatus.setModel(new javax.swing.DefaultComboBoxModel(cs.toArray()));



    }

    /*********************Cards History *************************************************/
    public static void displayCardsHistory(String coverNumber) {
        cardsHistoryInsTxt.setText(coverNumber);
        beneficiaryNameTxt.setText(CoverName);


    }

    /*********************Audit Trail ***************************************************/
    public static void displayAuditTrail(String coverNumber) {
        auditTrailInsTxt.setText(coverNumber);
        auditTrailNameTxt.setText(CoverName);



    }

    /*********************Notes *********************************************************/
    public static void displayNotes(String coverNumber) {
        NotesInsTxt.setText(coverNumber);
        notesNameTxt.setText(CoverName);


    }

    protected XMLGregorianCalendar convert(Date date) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);


        return getDatatypeFactory().newXMLGregorianCalendar(calendar);


    }

    private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();



            } catch (DatatypeConfigurationException ex) {
                Logger.getLogger(CreateBroker.class.getName()).log(Level.SEVERE, null, ex);
            }


        }

        return _datatypeFactory;


    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane AuditTrailPane;
    private javax.swing.JScrollPane BenefitLimitBookingsScrollPane;
    private javax.swing.JScrollPane BenefitLimitsScrollPane;
    private javax.swing.JScrollPane CardsHistorPane;
    private javax.swing.JScrollPane ClaimsHistoryPane;
    private javax.swing.JScrollPane INSUREDPERSONSCORRLPANE;
    private javax.swing.JScrollPane NotePane;
    public static javax.swing.JTextField NotesInsTxt;
    public static com.jidesoft.grid.JideTable NotesTable;
    private javax.swing.JScrollPane PreAuthHistoryPane;
    public static com.jidesoft.swing.JideButton amendBtn;
    public static javax.swing.JTextField auditTrailInsTxt;
    public static javax.swing.JTextField auditTrailNameTxt;
    public static com.jidesoft.grid.JideTable auditTrailTable;
    public static javax.swing.JTable authorizationTable;
    private com.jidesoft.swing.JideButton backBenefitTabBtn;
    private com.jidesoft.swing.JideButton backChronicPreAuthTabBtn1;
    private com.jidesoft.swing.JideButton backChronicPreAuthTabBtn2;
    private com.jidesoft.swing.JideButton backChronicPreAuthTabBtn3;
    private com.jidesoft.swing.JideButton backInsPersonTabBtn;
    private com.jidesoft.swing.JideButton backPreAuthTabBtn;
    public static javax.swing.JTextField beneficiaryNameTxt;
    public static com.jidesoft.grid.JideTable benefitTable;
    public static javax.swing.JButton btn_AddDep;
    public static javax.swing.JButton btn_Underwriting;
    public static javax.swing.JButton btn_View;
    public static javax.swing.JButton btn_ViewClaim;
    public static javax.swing.JButton btn_viewAllBen;
    public static javax.swing.JTextField cardsHistoryInsTxt;
    public static com.jidesoft.grid.JideTable cardsHistoryTable;
    public static javax.swing.JComboBox cb_ClaimStatus;
    public static javax.swing.JComboBox cb_EntityType;
    public static com.jidesoft.swing.AutoCompletionComboBox cb_InsuredDep;
    public static com.jidesoft.swing.AutoCompletionComboBox cb_InsuredDep2;
    public static javax.swing.JComboBox cb_PayDates;
    public static com.jidesoft.grid.JideTable coverDetailsTable;
    public static com.jidesoft.swing.AutoResizingTextArea coverNameInsTabTxt;
    public static com.jidesoft.combobox.DateComboBox dcb_2011BenefitDateFrom;
    public static com.jidesoft.combobox.DateComboBox dcb_BenefitDateFrom;
    public static com.jidesoft.combobox.DateComboBox dcb_BenefitDateTo;
    public static com.jidesoft.combobox.DateComboBox dcb_PaymentDate;
    public static com.jidesoft.combobox.DateComboBox dcb_ProcessDate;
    public static com.jidesoft.combobox.DateComboBox dcb_ServiceDateFrom;
    public static com.jidesoft.combobox.DateComboBox dcb_ServiceDateTo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    public static javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    public static javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    public static javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane17;
    private javax.swing.JScrollPane jScrollPane18;
    private javax.swing.JScrollPane jScrollPane19;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane20;
    private javax.swing.JScrollPane jScrollPane21;
    private javax.swing.JScrollPane jScrollPane22;
    private javax.swing.JScrollPane jScrollPane23;
    private javax.swing.JScrollPane jScrollPane24;
    private javax.swing.JScrollPane jScrollPane25;
    private javax.swing.JScrollPane jScrollPane28;
    private javax.swing.JScrollPane jScrollPane29;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    public static com.jidesoft.grid.JideTable listingofPersons;
    public static com.jidesoft.swing.JideButton newnoteBtn;
    public static javax.swing.JTextField notesNameTxt;
    public static javax.swing.JTable paymentTable;
    public static javax.swing.JPanel pnlResult;
    private javax.swing.JPanel pnl_CardHistory;
    public static javax.swing.JPanel pnl_ProvNum;
    public static javax.swing.JPanel pnl_RefinedSearch;
    public static com.jidesoft.swing.AutoResizingTextArea policyNumberInsTabTxt;
    public static javax.swing.JButton search;
    public static com.jidesoft.swing.JideButton searchBenefitActionBtn;
    public static javax.swing.JButton searchButton;
    private com.jidesoft.swing.StyledLabel styledLabel1;
    private com.jidesoft.swing.StyledLabel styledLabel10;
    private com.jidesoft.swing.StyledLabel styledLabel11;
    private com.jidesoft.swing.StyledLabel styledLabel12;
    private com.jidesoft.swing.StyledLabel styledLabel13;
    private com.jidesoft.swing.StyledLabel styledLabel14;
    private com.jidesoft.swing.StyledLabel styledLabel15;
    private com.jidesoft.swing.StyledLabel styledLabel16;
    private com.jidesoft.swing.StyledLabel styledLabel17;
    private com.jidesoft.swing.StyledLabel styledLabel18;
    private com.jidesoft.swing.StyledLabel styledLabel19;
    private com.jidesoft.swing.StyledLabel styledLabel2;
    private com.jidesoft.swing.StyledLabel styledLabel20;
    private com.jidesoft.swing.StyledLabel styledLabel21;
    private com.jidesoft.swing.StyledLabel styledLabel22;
    private com.jidesoft.swing.StyledLabel styledLabel26;
    private com.jidesoft.swing.StyledLabel styledLabel27;
    private com.jidesoft.swing.StyledLabel styledLabel28;
    private com.jidesoft.swing.StyledLabel styledLabel3;
    private com.jidesoft.swing.StyledLabel styledLabel4;
    private com.jidesoft.swing.StyledLabel styledLabel5;
    private com.jidesoft.swing.StyledLabel styledLabel6;
    private com.jidesoft.swing.StyledLabel styledLabel7;
    private com.jidesoft.swing.StyledLabel styledLabel8;
    private com.jidesoft.swing.StyledLabel styledLabel9;
    public static javax.swing.JTable tbl_PersonDetails;
    public static javax.swing.JTable tbl_benLimBook;
    public static javax.swing.JTabbedPane tpane_MedcorInsured;
    public static javax.swing.JTextField txt_AccountNumber;
    public static javax.swing.JTextField txt_AuthNumber;
    public static javax.swing.JTextField txt_BatchNumber;
    public static javax.swing.JTextField txt_ClaimNumber;
    public static javax.swing.JTextField txt_CoverNumber;
    public static javax.swing.JTextField txt_DependentNo;
    public static javax.swing.JTextField txt_EdiId;
    public static javax.swing.JTextField txt_Option;
    public static javax.swing.JTextField txt_PolicyNumber;
    public static javax.swing.JTextField txt_PolicyNumber1;
    public static javax.swing.JTextField txt_PolicyProductBL;
    public static javax.swing.JTextField txt_PracticeNumber;
    public static javax.swing.JTextField txt_Product;
    public static javax.swing.JTextField txt_ProductBLOption;
    public static javax.swing.JTextField txt_ProvNum;
    public static javax.swing.JTextField txt_ProviderNumber;
    public static javax.swing.JTextField txt_ScanIndexNumber;
    public static javax.swing.JTextField txt_Status;
    public static javax.swing.JTextField txt_TariffCode;
    public static javax.swing.JTextField txt_provDiscipline;
    // End of variables declaration//GEN-END:variables
    private DatatypeFactory _datatypeFactory;
}

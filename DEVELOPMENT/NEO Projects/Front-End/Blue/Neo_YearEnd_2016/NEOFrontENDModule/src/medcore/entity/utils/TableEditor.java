/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package medcore.entity.utils;

import java.awt.Component;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import medcore.entity.ViewCoverPersonAllDetails;

/**
 *
 * @author johanl
 */
public class TableEditor {

    public static void tableDateFormat(String tableName) {

        if (tableName.equals("Address")) {
            ViewCoverPersonAllDetails.addressDetailsTable.getColumnModel().getColumn(3).setCellRenderer(new DefaultTableCellRenderer() {

                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    if (value instanceof Date) {
                        Date date = (Date) value;
                        DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                        //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                        value = format.format(date);
                    }
                    return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                }
            });
            ViewCoverPersonAllDetails.addressDetailsTable.getColumnModel().getColumn(4).setCellRenderer(new DefaultTableCellRenderer() {

                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    if (value instanceof Date) {
                        Date date = (Date) value;
                        DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                        //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                        value = format.format(date);
                    }
                    return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                }
            });

        } else if (tableName.equals("ConPref")) {
            ViewCoverPersonAllDetails.table_ContactPrefferenceDetails.getColumnModel().getColumn(6).setCellRenderer(new DefaultTableCellRenderer() {

                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    if (value instanceof Date) {
                        Date date = (Date) value;
                        DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                        //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                        value = format.format(date);
                    }
                    return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                }
            });
            ViewCoverPersonAllDetails.table_ContactPrefferenceDetails.getColumnModel().getColumn(7).setCellRenderer(new DefaultTableCellRenderer() {

                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    if (value instanceof Date) {
                        Date date = (Date) value;
                        DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                        //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                        value = format.format(date);
                    }
                    return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                }
            });
        }
    }

    public static void addTableResultRows(int reSize, String tableName) {


        int reinTableRows = 0;
        if (tableName.equals("Address")) {
            reinTableRows = ViewCoverPersonAllDetails.addressDetailsTable.getRowCount();
            if (reinTableRows != 0) {
                //removing all rows
                ViewCoverPersonAllDetails.addressDetailsTable.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {}, new String [] {"Address Id", "Address Type", "Postal Code", "Start Date", "End Date"}));
                //adding rows according to request
                DefaultTableModel displayTableModel = (DefaultTableModel) ViewCoverPersonAllDetails.addressDetailsTable.getModel();
                for (int k = 0; k < reSize; k++) {
                    displayTableModel.addRow(new Object[]{});
                }
            } else {
                //adding rows according to request
                DefaultTableModel displayTableModel = (DefaultTableModel) ViewCoverPersonAllDetails.addressDetailsTable.getModel();
                for (int k = 0; k < reSize; k++) {
                    displayTableModel.addRow(new Object[]{});
                }
            }

        } else if (tableName.equals("Contact Preferences")) {
            reinTableRows = ViewCoverPersonAllDetails.table_ContactPrefferenceDetails.getRowCount();
            if (reinTableRows != 0) {
                //removing all rows
                ViewCoverPersonAllDetails.table_ContactPrefferenceDetails.setModel(new javax.swing.table.DefaultTableModel(
                        new Object[][]{},
                        new String[]{
                            "Contact Person", "Communication Type", "Method", "Method Details", "File Layout", "Suppress Mail", "Suppress Mail From", "Suppress Mail To"
                        }) {

                    boolean[] canEdit = new boolean[]{
                        false, false, false, false, false, false, false, false
                    };
                });
                //adding rows according to request
                DefaultTableModel displayTableModel = (DefaultTableModel) ViewCoverPersonAllDetails.table_ContactPrefferenceDetails.getModel();
                for (int k = 0; k < reSize; k++) {
                    displayTableModel.addRow(new Object[]{});
                }
            } else {
                //adding rows according to request
                DefaultTableModel displayTableModel = (DefaultTableModel) ViewCoverPersonAllDetails.table_ContactPrefferenceDetails.getModel();
                for (int k = 0; k < reSize; k++) {
                    displayTableModel.addRow(new Object[]{});
                }
            }

        } else if (tableName.equals("Contact Details")) {
            reinTableRows = ViewCoverPersonAllDetails.table_ContactDetails.getRowCount();
            if (reinTableRows != 0) {
                //removing all rows
                ViewCoverPersonAllDetails.table_ContactDetails.setModel(new javax.swing.table.DefaultTableModel(
                        new Object[][]{},
                        new String[]{
                            "Cover Number", "Contact Details Id", "Method", "Method Detail"
                        }) {

                    boolean[] canEdit = new boolean[]{
                        false, true, false, false
                    };

                    @Override
                    public boolean isCellEditable(int rowIndex, int columnIndex) {
                        return canEdit[columnIndex];
                    }
                });
                //adding rows according to request
                DefaultTableModel displayTableModel = (DefaultTableModel) ViewCoverPersonAllDetails.table_ContactDetails.getModel();
                for (int k = 0; k < reSize; k++) {
                    displayTableModel.addRow(new Object[]{});
                }
            } else {
                //adding rows according to request
                DefaultTableModel displayTableModel = (DefaultTableModel) ViewCoverPersonAllDetails.table_ContactDetails.getModel();
                for (int k = 0; k < reSize; k++) {
                    displayTableModel.addRow(new Object[]{});
                }
            }

        } else if (tableName.equals("Banking Details")) {
            reinTableRows = ViewCoverPersonAllDetails.bankingTable.getRowCount();
            if (reinTableRows != 0) {
                //removing all rows
                ViewCoverPersonAllDetails.bankingTable.setModel(new javax.swing.table.DefaultTableModel(
                        new Object[][]{},
                        new String[]{
                            "Payment Method", "Bank Name", "Branch Name", "Branch Code", "Account Type", "Account Number", "Account Name", "Date From", "Date To"
                        }) {

                    boolean[] canEdit = new boolean[]{
                        false, false, false, false, false, false, false, false, false
                    };
                });
                //adding rows according to request
                DefaultTableModel displayTableModel = (DefaultTableModel) ViewCoverPersonAllDetails.bankingTable.getModel();
                for (int k = 0; k < reSize; k++) {
                    displayTableModel.addRow(new Object[]{});
                }
            } else {
                //adding rows according to request
                DefaultTableModel displayTableModel = (DefaultTableModel) ViewCoverPersonAllDetails.bankingTable.getModel();
                for (int k = 0; k < reSize; k++) {
                    displayTableModel.addRow(new Object[]{});
                }
            }

        }

    }
}

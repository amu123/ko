/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * CreateCompany.java
 *
 * Created on 2009/05/05, 03:28:32
 */
package medcore.entity;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import neo.manager.AddressDetails;
import neo.manager.Country;
import neo.manager.LookupValue;
import neo.manager.NeoSecurityException_Exception;
import neo.manager.Region;
import neo.manager.Road;
import neo.manager.Suburb;
import neo.manager.Town;
import neo.product.utils.NeoWebService;
import za.co.AgilityTechnologies.LoginPage;

/**
 *
 * @author BharathP
 */
public class CreateNewAddressDetails extends javax.swing.JPanel {

    static int entityid = 1;
    public static Map<String, String> addressTypeMap = new HashMap<String, String>();
    public static Map<String, Integer> countryMap = new HashMap<String, Integer>();
    public static Map<String, Integer> regionMap = new HashMap<String, Integer>();
    public static Map<String, Integer> townMap = new HashMap<String, Integer>();
    public static Map<String, Integer> suburbMap = new HashMap<String, Integer>();
    public static Map<String, Integer> roadMap = new HashMap<String, Integer>();
    public static String nameSur = "";
    public static List<neo.manager.AddressDetails> addressStaticList = new ArrayList<neo.manager.AddressDetails>();

    /** Creates new form CreateCompany */
    public CreateNewAddressDetails() {
        initComponents();
        loadAddressDetails();
        tableDateFormat();
    }

    public boolean saveAdressDetails() {

        neo.manager.AddressDetails addressDetatils = new neo.manager.AddressDetails();



        return true;
    }

    public static void loadAddressDetails() {

        try {
            ArrayList typeList = new ArrayList();
            List<LookupValue> lookupValues = NeoWebService.getNeoManagerBeanService().getCodeTableByName(LoginPage.neoUser, "Address_Types");
            for (LookupValue lookupType : lookupValues) {
                String addTypId = lookupType.getId();
                String addTypeValue = lookupType.getValue();
                addressTypeMap.put(addTypeValue, addTypId);
                typeList.add(addTypeValue);
            }
            cb_AddressType.setModel(new javax.swing.DefaultComboBoxModel(typeList.toArray()));
        } catch (NeoSecurityException_Exception nEx) {
            System.out.println("NeoSecurityException = " + nEx.getMessage());
        }

        int addressId = 0;
        String addressType = "";
        String country = "";
        String region = "";
        String town = "";
        String suburb = "";
        String roadName = "";
        String roadNo = "";
        String postalCode = "";
        Date startDate = null;
        Date endDate = null;
        int rowSize = 0;

        List<neo.manager.AddressDetails> addressList = NeoWebService.getNeoManagerBeanService().getAllAddressDetails(entityid);
        addressStaticList = addressList;
        rowSize = addressList.size();
        System.out.println("rowSize = " + rowSize);
        addAddressResultsRows(rowSize);

        int x = 0;
        for (neo.manager.AddressDetails addressDetails : addressList) {

            addressId = addressDetails.getAddressId();
            addressType = addressDetails.getAddressType();
            country = addressDetails.getCountry();
            region = addressDetails.getRegion();
            town = addressDetails.getTown();
            suburb = addressDetails.getSuburb();
            roadName = addressDetails.getRoadName();
            roadNo = addressDetails.getRoadNo();
            postalCode = addressDetails.getPostalCode();
            startDate = addressDetails.getEffectiveStart().toGregorianCalendar().getTime();
            endDate = addressDetails.getEffectiveEnd().toGregorianCalendar().getTime();

            addressDetailsTable.setValueAt(addressId, x, 0);
            addressDetailsTable.setValueAt(addressType, x, 1);
            addressDetailsTable.setValueAt(country, x, 2);
            addressDetailsTable.setValueAt(region, x, 3);
            addressDetailsTable.setValueAt(town, x, 4);
            addressDetailsTable.setValueAt(suburb, x, 5);
            addressDetailsTable.setValueAt(roadName, x, 6);
            addressDetailsTable.setValueAt(roadNo, x, 7);
            addressDetailsTable.setValueAt(postalCode, x, 8);
            addressDetailsTable.setValueAt(startDate, x, 9);
            addressDetailsTable.setValueAt(endDate, x, 10);

            x++;
        }

        companyName.setText(nameSur);

    }

    public static void displayAddressDetails(int selectedAddressId) {
        for (neo.manager.AddressDetails addressDetails : addressStaticList) {

            int addressId = addressDetails.getAddressId();
            String addressLine1 = "";
            String addressLine2 = "";
            String addressLine3 = "";
            String addressLineAppend = "";

            if (addressId == selectedAddressId) {

                cb_AddressType.setSelectedItem(addressDetails.getAddressType());
                cb_Country.setSelectedItem(addressDetails.getCountry());
                cb_Region.setSelectedItem(addressDetails.getRegion());
                cb_Town.setSelectedItem(addressDetails.getTown());
                cb_Suburb.setSelectedItem(addressDetails.getSuburb());
                cb_Road.setSelectedItem(addressDetails.getRoadName());
                txt_RoadNo.setText(addressDetails.getRoadNo());

                addressLine1 = addressDetails.getAddressLine1();
                addressLine2 = addressDetails.getAddressLine2();
                addressLine3 = addressDetails.getAddressLine3();

                addressLineAppend = (addressLine1 + "," + "\n" + addressLine2 + "," + "\n" + addressLine3);
                ta_AddressLines.setText(addressLineAppend);

            }
        }
    }

    public static void tableDateFormat() {
        addressDetailsTable.getColumnModel().getColumn(4).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });
        addressDetailsTable.getColumnModel().getColumn(5).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof Date) {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                    //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        });
    }

    public static void addAddressResultsRows(int reSize) {

        int reinTableRows = addressDetailsTable.getRowCount();

        if (reinTableRows != 0) {
            //removing all rows
            addressDetailsTable.setModel(new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                        "Address Type", "Country", "Region", "Town", "Suburb", "Road Name", "Road Number", "Postal Code"
                    }));
            //adding rows according to request
            DefaultTableModel displayTableModel = (DefaultTableModel) addressDetailsTable.getModel();
            for (int k = 0; k < reSize; k++) {
                displayTableModel.addRow(new Object[]{});
            }
        } else {
            //adding rows according to request
            DefaultTableModel displayTableModel = (DefaultTableModel) addressDetailsTable.getModel();
            for (int k = 0; k < reSize; k++) {
                displayTableModel.addRow(new Object[]{});
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titlePanel = new javax.swing.JPanel();
        titleName = new com.jidesoft.swing.StyledLabel();
        bodyPanel = new javax.swing.JPanel();
        styledLabel1 = new com.jidesoft.swing.StyledLabel();
        entityID = new javax.swing.JTextField();
        styledLabel2 = new com.jidesoft.swing.StyledLabel();
        companyName = new javax.swing.JTextField();
        styledLabel4 = new com.jidesoft.swing.StyledLabel();
        styledLabel5 = new com.jidesoft.swing.StyledLabel();
        styledLabel7 = new com.jidesoft.swing.StyledLabel();
        styledLabel9 = new com.jidesoft.swing.StyledLabel();
        styledLabel10 = new com.jidesoft.swing.StyledLabel();
        styledLabel11 = new com.jidesoft.swing.StyledLabel();
        cb_AddressType = new com.jidesoft.swing.AutoCompletionComboBox();
        cb_Country = new com.jidesoft.swing.AutoCompletionComboBox();
        cb_Region = new com.jidesoft.swing.AutoCompletionComboBox();
        cb_Town = new com.jidesoft.swing.AutoCompletionComboBox();
        cb_Suburb = new com.jidesoft.swing.AutoCompletionComboBox();
        cb_Road = new com.jidesoft.swing.AutoCompletionComboBox();
        styledLabel12 = new com.jidesoft.swing.StyledLabel();
        txt_RoadNo = new javax.swing.JTextField();
        styledLabel3 = new com.jidesoft.swing.StyledLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        ta_AddressLines = new javax.swing.JTextArea();
        styledLabel6 = new com.jidesoft.swing.StyledLabel();
        postalCodetxt = new javax.swing.JTextField();
        btn_Submit = new com.jidesoft.swing.JideButton();
        backButton = new com.jidesoft.swing.JideButton();
        nextButton = new com.jidesoft.swing.JideButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        styledLabel20 = new com.jidesoft.swing.StyledLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        addressDetailsTable = new com.jidesoft.grid.JideTable();
        btn_Update = new com.jidesoft.swing.JideButton();

        setBackground(new java.awt.Color(235, 235, 235));
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(143, 196, 200)));

        Border raisedEdge = BorderFactory.createEtchedBorder(EtchedBorder.RAISED, Color.decode("#EBEBEB"), Color.decode("#99C3FD"));
        titlePanel.setBackground(new java.awt.Color(138, 177, 230));
        titlePanel.setBorder(raisedEdge);
        titlePanel.setBackground(new Color(153,195,253));
        titlePanel.add(titleName,BorderLayout.LINE_START);
        titlePanel.add(titleName);

        titleName.setBackground(new java.awt.Color(255, 255, 255));
        titleName.setForeground(new java.awt.Color(255, 255, 255));
        titleName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleName.setText("ADDRESS DETAILS");
        titleName.setFont(new java.awt.Font("Arial", 1, 13));
        titleName.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titlePanel.add(titleName);

        System.out.println("test inside body panel");
        bodyPanel.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel1.setText("Cover Number");
        styledLabel1.setFont(new java.awt.Font("Arial", 0, 11));

        styledLabel2.setText("Name");

        styledLabel4.setText("Address Type");

        styledLabel5.setText("Country");

        styledLabel7.setText("Region");

        styledLabel9.setText("Town");

        styledLabel10.setText("Suburb");

        styledLabel11.setText("Road");

        cb_Country.setModel(new javax.swing.DefaultComboBoxModel(searchCountry()));
        cb_Country.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cb_CountryItemStateChanged(evt);
            }
        });

        int conID=countryMap.get((String)cb_Country.getSelectedItem());
        cb_Region.setModel(new javax.swing.DefaultComboBoxModel(searchRegion(conID)));
        cb_Region.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cb_RegionItemStateChanged(evt);
            }
        });

        //int regionID=regionMap.get(cb_Region.getSelectedItem());
        cb_Town.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cb_TownItemStateChanged(evt);
            }
        });

        //int townID=townMap.get(cb_Town.getSelectedItem());
        cb_Suburb.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cb_SuburbItemStateChanged(evt);
            }
        });

        //int subID=suburbMap.get(cb_Suburb.getSelectedItem());

        styledLabel12.setText("Road No");

        styledLabel3.setText("Detail");

        ta_AddressLines.setColumns(20);
        ta_AddressLines.setRows(5);
        jScrollPane1.setViewportView(ta_AddressLines);

        styledLabel6.setText("Postal Code");

        javax.swing.GroupLayout bodyPanelLayout = new javax.swing.GroupLayout(bodyPanel);
        bodyPanel.setLayout(bodyPanelLayout);
        bodyPanelLayout.setHorizontalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addGap(126, 126, 126)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(styledLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)
                        .addComponent(styledLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(styledLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(styledLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(styledLabel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(styledLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(styledLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(styledLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(styledLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE))
                    .addComponent(styledLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, bodyPanelLayout.createSequentialGroup()
                        .addComponent(styledLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(bodyPanelLayout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, bodyPanelLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cb_Country, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                                    .addComponent(cb_AddressType, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                                    .addComponent(cb_Region, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                                    .addComponent(cb_Town, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                                    .addComponent(cb_Suburb, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                                    .addComponent(cb_Road, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                                    .addComponent(txt_RoadNo, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)))
                            .addComponent(companyName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                            .addComponent(entityID, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)))
                    .addGroup(bodyPanelLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(postalCodetxt, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addGap(59, 59, 59))
        );
        bodyPanelLayout.setVerticalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(entityID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(companyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_AddressType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_Country, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_Region, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_Town, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_Suburb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_Road, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_RoadNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(postalCodetxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(48, Short.MAX_VALUE))
        );

        btn_Submit.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_Submit.setText("SUBMIT");
        btn_Submit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SubmitActionPerformed(evt);
            }
        });

        backButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/arrow2_left_blue.png"))); // NOI18N
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        nextButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/arrow2_right_green.png"))); // NOI18N
        nextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextButtonActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(235, 235, 235));

        jPanel3.setBackground(new java.awt.Color(138, 177, 230));

        styledLabel20.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel20.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel20.setText("Address Details");
        jPanel3.add(styledLabel20);

        addressDetailsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Address Id", "Address Type", "Country", "Region", "Town", "Suburb", "Road Name", "Road Number", "Postal Code", "Start Date", "End Date"
            }
        ));
        addressDetailsTable.setColumnAutoResizable(true);
        addressDetailsTable.setColumnResizable(true);
        addressDetailsTable.setColumnSelectionAllowed(true);
        addressDetailsTable.setRowResizable(true);
        addressDetailsTable.setScrollRowWhenRowHeightChanges(true);
        addressDetailsTable.setSelectionBackground(new java.awt.Color(138, 177, 230));
        addressDetailsTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                addressDetailsTableMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(addressDetailsTable);
        addressDetailsTable.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        addressDetailsTable.getColumnModel().getColumn(0).setMinWidth(70);
        addressDetailsTable.getColumnModel().getColumn(0).setPreferredWidth(70);
        addressDetailsTable.getColumnModel().getColumn(0).setMaxWidth(70);
        addressDetailsTable.getColumnModel().getColumn(1).setMinWidth(80);
        addressDetailsTable.getColumnModel().getColumn(1).setPreferredWidth(80);
        addressDetailsTable.getColumnModel().getColumn(1).setMaxWidth(80);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 925, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 925, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        btn_Update.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_Update.setText("UPDATE");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titlePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 1013, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(154, 154, 154)
                .addComponent(bodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(326, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(874, 874, 874)
                .addComponent(backButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(nextButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(69, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(280, 280, 280)
                .addComponent(btn_Submit, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(75, 75, 75)
                .addComponent(btn_Update, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(542, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titlePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(bodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_Submit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_Update, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(54, 54, 54)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nextButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(backButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(34, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
    }//GEN-LAST:event_backButtonActionPerformed

    private void nextButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextButtonActionPerformed
    }//GEN-LAST:event_nextButtonActionPerformed

    private void cb_CountryItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cb_CountryItemStateChanged
        // TODO add your handling code here:
        int countryID = countryMap.get((String) cb_Country.getSelectedItem());

        cb_Region.setModel(new javax.swing.DefaultComboBoxModel(searchRegion(countryID)));

}//GEN-LAST:event_cb_CountryItemStateChanged

    private void cb_RegionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cb_RegionItemStateChanged
        // TODO add your handling code here:
        int regionID = regionMap.get(cb_Region.getSelectedItem());
        cb_Town.setModel(new javax.swing.DefaultComboBoxModel(searchTown(regionID)));

}//GEN-LAST:event_cb_RegionItemStateChanged

    private void cb_TownItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cb_TownItemStateChanged
        // TODO add your handling code here:
        if (cb_Town.getSelectedItem() != null) {
            int townID = townMap.get(cb_Town.getSelectedItem());

            cb_Suburb.setModel(new javax.swing.DefaultComboBoxModel(searchSuburb(townID)));
        }

}//GEN-LAST:event_cb_TownItemStateChanged

    private void cb_SuburbItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cb_SuburbItemStateChanged
        // TODO add your handling code here:
        if (cb_Suburb.getSelectedItem() != null) {
            int subID = suburbMap.get(cb_Suburb.getSelectedItem());
            cb_Road.setModel(new javax.swing.DefaultComboBoxModel(searchRoad(subID)));
        }
}//GEN-LAST:event_cb_SuburbItemStateChanged

    private void addressDetailsTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addressDetailsTableMouseReleased
        int selectedRow = addressDetailsTable.getSelectedRow();
        System.out.println("selectedRow = " + selectedRow);
        int selectedAddressId = new Integer(addressDetailsTable.getValueAt(selectedRow, 0).toString());
        System.out.println("selectedAddressId in grid = " + selectedAddressId);
        displayAddressDetails(selectedAddressId);

    }//GEN-LAST:event_addressDetailsTableMouseReleased

    private void btn_SubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SubmitActionPerformed
        neo.manager.AddressDetails addressDetails = new AddressDetails();

        int addressTypeId = 0;
        int countryId = 0;
        int regionId = 0;
        int townId = 0;
        int suburbId = 0;
        int roadId = 0;

        String addressLines = "";
        String addressLine1 = "";
        String addressLine2 = "";
        String addressLine3 = "";
        String postalCode = "";
        String roadNo = "";


        addressLines = ta_AddressLines.getText();
        int firstIndex = addressLines.indexOf(",");
        int lastIndex = addressLines.lastIndexOf(",");
        int eof = addressLines.length();

        addressLine1 = addressLines.substring(0, firstIndex);
        addressLine2 = addressLines.substring((firstIndex +1) , lastIndex);
        addressLine3 = addressLines.substring((lastIndex + 1), eof);

        System.out.println("addressLine1 = "+ addressLine1);
        System.out.println("addressLine2 = "+ addressLine2);
        System.out.println("addressLine3 = "+ addressLine3);

//        addressDetails.setAddressTypeId(addressTypeId);
//        addressDetails.setAddressLine1(addressLine1);
//        addressDetails.setAddressLine2(addressLine2);
//        addressDetails.setAddressLine3(addressLine3);
//        addressDetails.setPostalCode(postalCode);
//        addressDetails.setCountryId(countryId);
//        addressDetails.setRegionId(regionId);
//        addressDetails.setTownId(townId);
//        addressDetails.setSuburbId(suburbId);
//        addressDetails.setRoadId(roadId);
//        addressDetails.setRoadNo(roadNo);



    }//GEN-LAST:event_btn_SubmitActionPerformed
    public String[] searchCountry() {

        List<Country> countryList = NeoWebService.getNeoManagerBeanService().getCountryTable();
        int i = 0;

        String[] conArrList = new String[countryList.size()];
        for (Country country : countryList) {
            conArrList[i] = country.getName();
            countryMap.put(conArrList[i], country.getId());
            i++;
        }

        return conArrList;


    }

    public String[] searchRegion(int countryID) {


        List<Region> regionList = NeoWebService.getNeoManagerBeanService().getRegionTable(countryID);
        int i = 0;

        String[] regionArrList = new String[regionList.size()];
        for (Region region : regionList) {
            regionArrList[i] = region.getName();
            regionMap.put(regionArrList[i], region.getId());
            i++;
        }

        return regionArrList;

    }

    public String[] searchTown(int regionID) {

        List<Town> townList = NeoWebService.getNeoManagerBeanService().getTownTable(regionID);
        int i = 0;

        String[] townArrList = new String[townList.size()];
        for (Town town : townList) {
            townArrList[i] = town.getName();
            townMap.put(townArrList[i], town.getId());
            i++;
        }

        return townArrList;

    }

    public String[] searchSuburb(int townID) {

        List<Suburb> suburbList = NeoWebService.getNeoManagerBeanService().getSuburbTable(townID);
        int i = 0;

        String[] suburbArrList = new String[suburbList.size()];
        for (Suburb suburb : suburbList) {
            suburbArrList[i] = suburb.getName();
            suburbMap.put(suburbArrList[i], suburb.getId());
            i++;
        }

        return suburbArrList;

    }

    public String[] searchRoad(int subID) {

        List<Road> roadList = NeoWebService.getNeoManagerBeanService().getRoadTable(subID);
        int i = 0;

        String[] roadArrList = new String[roadList.size()];
        for (Road road : roadList) {
            roadArrList[i] = road.getName();
            roadMap.put(roadArrList[i], road.getId());
            i++;
        }

        return roadArrList;

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static com.jidesoft.grid.JideTable addressDetailsTable;
    private com.jidesoft.swing.JideButton backButton;
    private javax.swing.JPanel bodyPanel;
    public static com.jidesoft.swing.JideButton btn_Submit;
    public static com.jidesoft.swing.JideButton btn_Update;
    public static com.jidesoft.swing.AutoCompletionComboBox cb_AddressType;
    public static com.jidesoft.swing.AutoCompletionComboBox cb_Country;
    public static com.jidesoft.swing.AutoCompletionComboBox cb_Region;
    public static com.jidesoft.swing.AutoCompletionComboBox cb_Road;
    public static com.jidesoft.swing.AutoCompletionComboBox cb_Suburb;
    public static com.jidesoft.swing.AutoCompletionComboBox cb_Town;
    public static javax.swing.JTextField companyName;
    public static javax.swing.JTextField entityID;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private com.jidesoft.swing.JideButton nextButton;
    public static javax.swing.JTextField postalCodetxt;
    private com.jidesoft.swing.StyledLabel styledLabel1;
    private com.jidesoft.swing.StyledLabel styledLabel10;
    private com.jidesoft.swing.StyledLabel styledLabel11;
    private com.jidesoft.swing.StyledLabel styledLabel12;
    private com.jidesoft.swing.StyledLabel styledLabel2;
    private com.jidesoft.swing.StyledLabel styledLabel20;
    private com.jidesoft.swing.StyledLabel styledLabel3;
    private com.jidesoft.swing.StyledLabel styledLabel4;
    private com.jidesoft.swing.StyledLabel styledLabel5;
    private com.jidesoft.swing.StyledLabel styledLabel6;
    private com.jidesoft.swing.StyledLabel styledLabel7;
    private com.jidesoft.swing.StyledLabel styledLabel9;
    public static javax.swing.JTextArea ta_AddressLines;
    private com.jidesoft.swing.StyledLabel titleName;
    private javax.swing.JPanel titlePanel;
    public static javax.swing.JTextField txt_RoadNo;
    // End of variables declaration//GEN-END:variables
}

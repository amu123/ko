    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.cb_MethodDetails
 */

/*
 * InsuredPersonPolicyDetails.java
 *
 * Created on 2009/07/20, 02:56:31
 */
package medcore.entity;

import java.awt.Component;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import medcor.entity.view.InsuredPersonHistoryFrame;
import medcor.entity.view.PreviousInsurenceViewFrame;
import medcor.entity.view.UnderwritingViewFrame;
import medcore.entity.utils.TableEditor;
import medcor.entity.view.OptionAllocateFrame;
import neo.entity.broker.CreateBroker;
import neo.manager.AddressDetails;
import neo.manager.AttributeTypes;
import neo.manager.Bank;
import neo.manager.BankingDetails;
import neo.manager.Branch;
import neo.manager.ContactDetails;
import neo.manager.ContactPreference;
import neo.manager.Country;
import neo.manager.CoverDetails;
import neo.manager.CoverProductDetails;
import neo.manager.LookupValue;
import neo.manager.NeoSecurityException_Exception;
import neo.manager.PersonDetails;
import neo.manager.Region;
import neo.manager.Road;
import neo.manager.SecurityResponsibility;
import neo.manager.Suburb;
import neo.manager.Town;
import neo.product.utils.NeoWebService;
import neo.util.UtilMethods;
import za.co.AgilityTechnologies.DockingFramework;
import za.co.AgilityTechnologies.LeftMenu.EntityActionsFrame;
import za.co.AgilityTechnologies.LoginPage;
import za.co.AgilityTechnologies.NeoMessageDialog;

/**
 *
 * @author BharathP, johanl
 */
public class ViewCoverPersonAllDetails extends javax.swing.JPanel {

    DockingFramework df = new DockingFramework();
    static SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    //address details
    static int addressCommonId;
    static String address;
    static String addressDetail;
    public static List<neo.manager.AddressDetails> addressStaticList = new ArrayList<neo.manager.AddressDetails>();
    public static Map<String, Integer> addressTypeMap = new HashMap<String, Integer>();
    public static Map<String, Integer> countryMap = new HashMap<String, Integer>();
    public static Map<String, Integer> regionMap = new HashMap<String, Integer>();
    public static Map<String, Integer> townMap = new HashMap<String, Integer>();
    public static Map<String, Integer> suburbMap = new HashMap<String, Integer>();
    public static Map<String, Integer> roadMap = new HashMap<String, Integer>();
    public static Map<String, String> allDetails = new HashMap<String, String>();
    public static Map<String, String> saveDetails = new HashMap<String, String>();
    //Cover Details
    static boolean resignedMemFlag;
    static int entityCommonId;
    static int coverDetailsId;
    static int entityId = 0;
    static int depEntityId = 0;
    static String memCoverNumber = "";
    static String memName = "";
    static String memSurname = "";
    static String memberIdNumber = "";
    static int optionId;
    static int productId;
    static boolean loadCover = false;
    static boolean loadCoverDep = false;
    boolean superuser = false;
    static List<CoverDetails> dependantList = null;
    public static CoverDetails mainMemList = null;
    public Collection<CoverDetails> resignedCoverDetailList = null;
    static CoverDetails depCoverDetail = null;
    public static Map<String, Integer> statusMap = new HashMap<String, Integer>();
    public static Map<String, Integer> statusActiveReasonMap = new HashMap<String, Integer>();
    public static Map<String, Integer> statusSuspendReasonMap = new HashMap<String, Integer>();
    public static Map<String, Integer> statusPendedReasonMap = new HashMap<String, Integer>();
    public static Map<String, Integer> statusResignReasonMap = new HashMap<String, Integer>();
    //Banking Details
    static int bankCommonId;
    public static Map<String, Integer> bankMap = new HashMap<String, Integer>();
    public static Map<String, String> bankBranchMap = new HashMap<String, String>();
    public static Map<String, String> bankBranchCodeMap = new HashMap<String, String>();
    public static List<neo.manager.BankingDetails> bankingStaticList = new ArrayList<neo.manager.BankingDetails>();
    public static Map<String, Integer> bankBranchIdMap = new HashMap<String, Integer>();
    public static Map<String, String> accountTypeMap = new HashMap<String, String>();
    public static Map<String, String> paymentMethodMap = new HashMap<String, String>();
    //contact details
    static int contactCommonId;
    static int selectedContactRow = 0;
    public static boolean mailFlag = false;
    public static Map<Integer, String> methodDetailMap = new HashMap<Integer, String>();
    public static Map<String, Integer> commMethodTypeMap = new HashMap<String, Integer>();
    public static List<ContactDetails> contactStaticList = new ArrayList<ContactDetails>();
    public static Map<Integer, String> conMethodDetailMap = new HashMap<Integer, String>();
    public static Map<Integer, String> contactMethodMap = new HashMap<Integer, String>();
    //contact preference details
    static Date suppFromDate;
    static Date suppToDate;
    static int conPrefCommonId = 0;
    static int conPrefContactDetailId = 0;
    static List<ContactDetails> contactList = null;
    static List<ContactPreference> conPrefList = null;
    public static Map<String, Integer> communicationTypeMap = new HashMap<String, Integer>();
    public static Map<String, Integer> fileLayoutMap = new HashMap<String, Integer>();
    public static Map<String, Integer> prefMethodTypeMap = new HashMap<String, Integer>();
    public static TreeSet<String> prefHashSet = new TreeSet<String>();
    static String emailDetails = "";
   
    //Flags
    static boolean addressFlag = false;
    static boolean bankFlag = false;
    static boolean contactFlag = false;
    static boolean conPrefFlag = false;
    static boolean loadMemberFlag = false;

    /** Creates new form InsuredPersonPolicyDetails */
    public ViewCoverPersonAllDetails() {
        initComponents();
        memCoverNumber = SearchCoverMember.coverMemNumber;
        checkDependent();
        System.out.println("entityCommId = " + SearchCoverMember.entityCommId);
        loadMainCoverDetails();
        hideCoverDetailObjects();
        pnl_Unused.setVisible(false);
        hideBankingDetails();

    }

    public ViewCoverPersonAllDetails(String coverNumber, int coverDetId, int entityCommId, boolean resigned, Collection<CoverDetails> cdList) {
        initComponents();
        CoverDetails mainMem = new CoverDetails();
        ArrayList depList = new ArrayList();
        memCoverNumber = coverNumber;
        entityCommonId = entityCommId;
        coverDetailsId = coverDetId;
        resignedMemFlag = resigned;
        resignedCoverDetailList = cdList;
        if (resignedMemFlag == true) {
            for (CoverDetails cd : cdList) {
                if (cd.getDependentTypeId() == 17 && entityCommonId == cd.getEntityCommonId()) {
                    mainMem = cd;
                } else {
                    if (depList.contains(cd.getDependentNumber()) == false) {
                        depList.add(cd);
                    }
                }
            }
            checkDependent(depList);
            mainMemList = mainMem;
        } else {
            checkDependent();
        }
        System.out.println("memCoverNumber recieved from search = " + memCoverNumber);
        System.out.println("entityCommId recieved from search = " + entityCommonId);
        loadMainCoverDetails();
        hideCoverDetailObjects();
        hideBankingDetails();
        pnl_Unused.setVisible(false);


    }

    public static void checkDependent() {
        List<CoverDetails> cd = NeoWebService.getNeoManagerBeanService().getAllDependentsForCoverByDate(memCoverNumber,SearchCoverMember.coverStartDate);
        System.out.println("number of dependents = " + cd.size());
        if (cd.size() == 0) {
            Component dep = tpane_CoverPersonDetails.getComponentAt(5);
            tpane_CoverPersonDetails.remove(dep);
        }
    }

    public static void checkDependent(List<CoverDetails> cd) {
        System.out.println("number of dependents = " + cd.size());
        if (cd.size() == 0) {
            Component dep = tpane_CoverPersonDetails.getComponentAt(5);
            tpane_CoverPersonDetails.remove(dep);
        }
        dependantList = cd;
    }

    public static void paneSelection(String tabName) {
        System.out.println("tabName = " + tabName);

        if (loadMemberFlag == false) {
            System.out.println("loading member...");
            loadMemberFlag = true;
        } else {
            if (tabName.equals("VIEW COVER PERSON")) {
                System.out.println("loading....." + tabName);
                loadMainCoverDetails();

            } else if (tabName.equals("ADDRESS DETAILS")) {
                TableEditor.tableDateFormat("Address");
                addressFlag = false;
                System.out.println("loading....." + tabName);
                loadAddressDetails();

            } else if (tabName.equals("CONTACT DETAILS")) {
                System.out.println("loading....." + tabName);
                contactFlag = false;
                loadContactDetails();

            } else if (tabName.equals("CONTACT PREFERENCES")) {
                TableEditor.tableDateFormat("ConPref");
                conPrefFlag = false;
                System.out.println("loading....." + tabName);
                loadContactPreferences();

            } else if (tabName.equals("BANKING DETAILS")) {
                System.out.println("loading....." + tabName);
                bankFlag = false;
                loadBankingDetails();

            } else if (tabName.equals("VIEW DEPENDENTS")) {
                System.out.println("loading....." + tabName);
                loadDependantForCover();

            }
        }
    }

    public void hideCoverDetailObjects() {
        //btn_AddressSubmit.setVisible(false);
        //btn_BankingSubmit.setVisible(false);
        //btn_ContactSubmit.setVisible(false);
        //btn_ContactPrefSubmit.setVisible(false);
        btn_DepStatusSave.setVisible(false);
        pnl_Unused.setVisible(false);

        if (!LoginPage.neoUserSecurityRestricted) {
            btn_AddressSubmit.setVisible(true);
            btn_AddressClear.setVisible(true);
            btn_BankingSubmit.setVisible(true);
            btn_BankingClear.setVisible(true);
            btn_ContactSubmit.setVisible(true);
            btn_ContactClear.setVisible(true);
            btn_ContactPrefSubmit.setVisible(true);
            btn_ContactPrefClear.setVisible(true);

        } else {
            btn_AddressSubmit.setVisible(false);
            btn_AddressClear.setVisible(false);
            btn_BankingSubmit.setVisible(false);
            btn_BankingClear.setVisible(false);
            btn_ContactSubmit.setVisible(false);
            btn_ContactClear.setVisible(false);
            btn_ContactPrefSubmit.setVisible(false);
            btn_ContactPrefClear.setVisible(false);
        }


    }

    public void hideBankingDetails() {
        List<SecurityResponsibility> securityList = LoginPage.neoUser.getSecurityResponsibility();
        
        for (SecurityResponsibility sec : securityList) {
            if(sec.getResponsibilityId() == 2 || sec.getResponsibilityId() == 10){
                superuser = true;
                break;
            }
        }

        if (superuser) {
            btn_BankingSubmit.setVisible(true);
            btn_BankingClear.setVisible(true);
        } else {
            btn_BankingSubmit.setVisible(false);
            btn_BankingClear.setVisible(false);
        }

    }
    /*----------------------- Address Detail Methods --------------------- */

    public static void loadAddressDetails() {

        txt_AddressCoverNumber.setText(memCoverNumber);
        txt_AddressCoverName.setText(memName + "" + memSurname);

        cb_AddressType.setSelectedItem("");
        txt_AddressLine1.setText("");
        txt_AddressLine2.setText("");
        txt_AddressLine3.setText("");
        txt_PostalCode.setText("");

        int addressId = 0;
        String addressType = "";
        String postalCode = "";
        Date startDate = null;
        Date endDate = null;
        int rowSize = 0;

        List<neo.manager.AddressDetails> addressList = NeoWebService.getNeoManagerBeanService().getAllAddressDetails(entityId);
        int resultSize = addressList.size();
        if (resultSize != 0) {

            addressStaticList = addressList;
            rowSize = addressList.size();
            System.out.println("rowSize = " + rowSize);
            TableEditor.addTableResultRows(rowSize, "Address");

            int x = 0;
            for (neo.manager.AddressDetails addressDetails : addressList) {

                addressId = addressDetails.getAddressId();
                addressType = addressDetails.getAddressType();
                postalCode = addressDetails.getPostalCode();
                startDate = addressDetails.getEffectiveStart().toGregorianCalendar().getTime();
                endDate = addressDetails.getEffectiveEnd().toGregorianCalendar().getTime();

                addressDetailsTable.setValueAt(addressId, x, 0);
                addressDetailsTable.setValueAt(addressType, x, 1);
                addressDetailsTable.setValueAt(postalCode, x, 2);
                addressDetailsTable.setValueAt(startDate, x, 3);
                addressDetailsTable.setValueAt(endDate, x, 4);                
                x++;
            }
            TableEditor.tableDateFormat("Address");

            AddressDetails ad = addressList.get(0);
            cb_AddressType.setSelectedItem(ad.getAddressType());
            txt_PostalCode.setText(ad.getPostalCode());
            txt_AddressLine1.setText(ad.getAddressLine1());
            txt_AddressLine2.setText(ad.getAddressLine2());
            txt_AddressLine3.setText(ad.getAddressLine3());

        } else {
            System.out.println("No address details");
        }
    }

    public static void displayAddressDetails(int selectedAddressId) {
        TableEditor.tableDateFormat("Address");
        for (neo.manager.AddressDetails addressDetails : addressStaticList) {
            int addressId = addressDetails.getAddressId();

            if (addressId == selectedAddressId) {
                //set entity common id
                addressCommonId = addressDetails.getEntityCommonId();

                //assign array data toe fields
                cb_AddressType.setSelectedItem(addressDetails.getAddressType());
                txt_AddressLine1.setText(addressDetails.getAddressLine1());
                txt_AddressLine2.setText(addressDetails.getAddressLine2());
                txt_AddressLine3.setText(addressDetails.getAddressLine3());

            }
        }
    }

    /**************************VIEW INSURED PERSON********************************/
    // View cover Person
    public static void loadMainCoverDetails() {

        CoverDetails coverDetails = new CoverDetails();
        loadCover = true;
        coverNumberTxt.setText(memCoverNumber);
        System.out.println("principal member coverNumber when loading details = " + memCoverNumber);
        System.out.println("principal member entityCommon when loading details = " + entityCommonId);
        System.out.println("principal member coverDetailsId when loading details = " + coverDetailsId);
        CoverDetails cd = NeoWebService.getNeoManagerBeanService().getPrincipalMemberForCoverNumberWithEComm(memCoverNumber, coverDetailsId);
        System.out.println("cd entity found with ecomm method = " + cd.getEntityId());
        if (cd.getEntityId() == 0) {
            if (resignedMemFlag == true) {
                coverDetails = mainMemList;
            }

        } else {
            coverDetails = cd;
        }
        entityId = coverDetails.getEntityId();
        System.out.println("entityId = " + entityId);
        System.out.println("entityId from details = " + coverDetails.getEntityId());
        PersonDetails pDetails = NeoWebService.getNeoManagerBeanService().getPersonDetailsByEntityId(entityId);
        System.out.println("Person Details Name = " + pDetails.getName());
        System.out.println("Cover Detail Comm Id = " + coverDetails.getEntityCommonId());
        System.out.println("Person Detail Comm Id = " + pDetails.getEntityCommonId());
        mainMemList = coverDetails;

        String depCode = "";
        String title = "";
        String initials = "";
        String maritalStatus = "";
        String idNum = "";
        String gender = "";
        String language = "";
        String incomeCat = "";
        String coverStatus = "";
        String coverStatusReason = "";
        Date appRecieved = null;

        if (coverDetails.getEntityId() == entityId) {
            depCode = String.valueOf(coverDetails.getDependentNumber());
            dependentCodeTxt.setText(depCode);
            dependentType.setText(coverDetails.getDependentType());

            //CoverProductDetails cpDetails = NeoWebService.getNeoManagerBeanService().getCoverProductDetailsByCoverNumber(memCoverNumber);
            policyNameTxt.setText(cd.getProductName());
            optionTxt.setText(cd.getOptionName());
            System.out.println("product = " + cd.getProductName());
            System.out.println("option = " + cd.getOptionName());

            title = pDetails.getTitle();
            titleTxt.setText(title);

            initials = pDetails.getInitials();
            initialsTxt.setText(initials);

            memName = pDetails.getName();
            nameTxt.setText(memName);
            memSurname = pDetails.getSurname();
            surnameTxt.setText(memSurname);

            maritalStatus = pDetails.getMaritalStatus();
            maritalStatusTxt.setText(maritalStatus);

            idNum = pDetails.getIDNumber();
            memberIdNumber = idNum;
            idNumberTxt.setText(idNum);

            gender = pDetails.getGenderDesc();
            genderTxt.setText(gender);

            ageTxt.setText(pDetails.getAge());
            dcb_DateOfBirth.setDate(pDetails.getDateOfBirth().toGregorianCalendar().getTime());

            language = pDetails.getHomeLanguage();
            homeLanguageTxt.setText(language);

            incomeCat = pDetails.getIncomeCategory();
            incomeTxt.setText(incomeCat);

            //entity attrbutes
            List<AttributeTypes> attrTypeList = new ArrayList<AttributeTypes>();//NeoWebService.getNeoManagerBeanService().getEntityAttributeTypes(entityId);
            for (AttributeTypes attributeTypes : attrTypeList) {

                if (attributeTypes.getScreenPrompt().equals("Employer")) {
                    employerTxt.setText(attributeTypes.getAttributeValue());

                } else if (attributeTypes.getScreenPrompt().equals("Payroll Number")) {
                    payRollTxt.setText(attributeTypes.getAttributeValue());

                } else if (attributeTypes.getScreenPrompt().equals("Paypoint")) {
                    cb_Paypoint.setSelectedItem(attributeTypes.getLookupDesc());

                }

            }
            //status info
            coverDateFrom.setDate(coverDetails.getCoverStartDate().toGregorianCalendar().getTime());
            coverDateTo.setDate(coverDetails.getCoverEndDate().toGregorianCalendar().getTime());
            coverStatus = coverDetails.getStatus();

            if (!(coverStatus.equals(""))) {
                txt_memMainStatus.setText(coverStatus);
                System.out.println("coverStatus = " + coverStatus);
                if (coverStatus.equals("Active")) {
                    cb_StatusReason.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Membership Reason Code Active", statusActiveReasonMap)));

                } else if (coverStatus.equals("Suspend")) {
                    cb_StatusReason.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Membership Reason Code Suspend", statusSuspendReasonMap)));

                } else if (coverStatus.equals("Pended")) {
                    cb_StatusReason.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Membership Reason Code Pended", statusPendedReasonMap)));

                } else if (coverStatus.equals("Resign")) {
                    cb_StatusReason.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Membership Reason Code Resign", statusResignReasonMap)));

                }
            }
            try {
                appRecieved = coverDetails.getApplicationRecievedDate().toGregorianCalendar().getTime();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                appRecieved = null;
            }
            dcb_AppStartDate.setDate(appRecieved);
            System.out.println("loadCover status reason = " + coverDetails.getStatusReason());

            coverStatusReason = coverDetails.getStatusReason();
            cb_StatusReason.setSelectedItem(coverStatusReason);

        }

    }
    //View Dependants

    public static void loadDependantForCover() {

        List<CoverDetails> dependentCoverDetails = new ArrayList<CoverDetails>();
        btn_DepStatusSave.setVisible(false);
        loadCoverDep = true;
        coverNumberTxt3.setText(memCoverNumber);
        //PersonDetails pDetails = NeoWebService.getNeoManagerBeanService().getPersonDetailsByEntityId(entityId);
        List<CoverDetails> depCoverDetails = NeoWebService.getNeoManagerBeanService().getAllDependentsForCoverByDate(memCoverNumber,SearchCoverMember.coverStartDate);
        System.out.println("dependant list size = " + depCoverDetails.size());
        if (depCoverDetails.size() == 0) {
            System.out.println("resigned dependent: " + resignedMemFlag);
            if (resignedMemFlag == true) {
                dependentCoverDetails = dependantList;
            }

        } else {
            dependentCoverDetails = depCoverDetails;
            dependantList = dependentCoverDetails;
        }
        System.out.println("dependent details list  = " + dependantList.size());
        ArrayList strArr = new ArrayList();
        for (CoverDetails coverDetails : dependentCoverDetails) {
            int depNum = coverDetails.getDependentNumber();
            strArr.add(depNum);

        }
        cb_ViewDependentNumber.setModel(new javax.swing.DefaultComboBoxModel(strArr.toArray()));

        CoverDetails cDetails = dependantList.get(0);
        depCoverDetail = cDetails;
        int entityID = cDetails.getEntityId();
        depEntityId = entityID;
        System.out.println("entityID for first dependent = " + entityID);
        txt_dependentType.setText(cDetails.getDependentType());

        Date appReceived = null;
        try {
            appReceived = cDetails.getApplicationRecievedDate().toGregorianCalendar().getTime();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        dcb_RegDate.setDate(appReceived);
        //status
        Date today = new Date();
        Date endDate = cDetails.getCoverEndDate().toGregorianCalendar().getTime();

        if (endDate.before(today)) {
            txt_memDepStatus.setText("Resign");
        } else {
            txt_memDepStatus.setText("Active");
        }
        String coverStatus = cDetails.getStatus();
        //txt_memDepStatus.setText(coverStatus);
        System.out.println("coverStatus = " + coverStatus);
        if (coverStatus.equals("Active")) {
            cb_DepStatusReason.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Membership Reason Code Active", statusActiveReasonMap)));

        } else if (coverStatus.equals("Suspend")) {
            cb_DepStatusReason.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Membership Reason Code Suspend", statusSuspendReasonMap)));

        } else if (coverStatus.equals("Pended")) {
            cb_DepStatusReason.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Membership Reason Code Pended", statusPendedReasonMap)));

        } else if (coverStatus.equals("Resign")) {
            cb_DepStatusReason.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Membership Reason Code Resign", statusResignReasonMap)));

        }
        System.out.println("getPersonDetails");
        PersonDetails firstDependentDetails = NeoWebService.getNeoManagerBeanService().getPersonDetailsByEntityId(entityID);
        txt_DepTitle.setText(firstDependentDetails.getTitle());
        txt_DepInitials.setText(firstDependentDetails.getInitials());
        txt_DepName.setText(firstDependentDetails.getName());
        txt_DepSurname.setText(firstDependentDetails.getSurname());
        txt_DepIdType.setText(firstDependentDetails.getIdentificationType());
        txt_DepIdNumber.setText(firstDependentDetails.getIDNumber());
        txt_DepGender.setText(firstDependentDetails.getGenderDesc());
        txt_DepAge.setText(firstDependentDetails.getAge());
        dcb_DOB.setDate(firstDependentDetails.getDateOfBirth().toGregorianCalendar().getTime());
        txt_DepHomeLan.setText(firstDependentDetails.getHomeLanguage());
        txt_DepIncome.setText(firstDependentDetails.getIncomeCategory());
        txt_DepMarital.setText(firstDependentDetails.getMaritalStatus());
        System.out.println("setStatus dates");
        dcb_DepCoverStart.setDate(cDetails.getCoverStartDate().toGregorianCalendar().getTime());
        dcb_DepCoverEnd.setDate(cDetails.getCoverEndDate().toGregorianCalendar().getTime());

    }

    /**********************************CONTACT DETAILS************************************/
    //TODO
    public static void loadContactDetails() {
        coverNumberContactTxt.setText(memCoverNumber);


        //pnl_ContactNewDate.setVisible(false);
        cb_ContactMethodDetails.setVisible(false);
        txt_ContactMethodDetails.setVisible(true);

        List<ContactDetails> contactDetailList = NeoWebService.getNeoManagerBeanService().getAllContactDetails(entityId);
        int resultSize = contactDetailList.size();
        if (resultSize != 0) {
            contactStaticList = contactDetailList;
            int rowSize = contactDetailList.size();
            System.out.println("contact details size = " + rowSize);
            TableEditor.addTableResultRows(rowSize, "Contact Details");

            int x = 0;
            for (ContactDetails contactDetails : contactDetailList) {

                int conId = contactDetails.getContactDetailsId();
                String metDetail = contactDetails.getMethodDetails();
                String commMethod = contactDetails.getCommunicationMethod();
                methodDetailMap.put(conId, metDetail);
                contactMethodMap.put(conId, commMethod);
                table_ContactDetails.setValueAt(memCoverNumber, x, 0);
                table_ContactDetails.setValueAt(conId, x, 1);
                table_ContactDetails.setValueAt(commMethod, x, 2);
                table_ContactDetails.setValueAt(metDetail, x, 3);

                x++;
            }
            ContactDetails cd = contactStaticList.get(0);
            cb_Method.setSelectedItem(cd.getCommunicationMethod());
            txt_ContactMethodDetails.setText(cd.getMethodDetails());

        } else {
            System.out.println("No contact Details");
        }
    }

    public static void displayContactDetails(int cdId, String contactMethod, String conMethodDetail) {
        cb_ContactMethodDetails.setVisible(false);
        txt_ContactMethodDetails.setVisible(true);

        for (ContactDetails cd : contactStaticList) {
            int conDetailId = cd.getContactDetailsId();
            if (conDetailId == cdId) {
                contactCommonId = cd.getEntityCommonId();
                System.out.println("Contact Details Id = " + conDetailId);
                System.out.println("Contact Common Id = " + contactCommonId);

            } else {
                System.out.println("Contact Details Id = " + conDetailId);
                System.out.println("Contact Common Id = " + contactCommonId);
            }
            if (contactMethod.equals("Postal")) {
                cb_ContactMethodDetails.setSelectedItem(contactMethod);

            } else {
                txt_ContactMethodDetails.setText(conMethodDetail);

            }
            cb_Method.setSelectedItem(contactMethod);
        }
        cb_Method.setSelectedItem(contactMethod);
    }

    /**************************************PREFRENCES**************************************/
    public static void loadContactPreferences() {
        conPrefList = null;
        emailDetails = null;

        //coverNumber
        coverNumberContactPrefTxt.setText(memCoverNumber);
        //name
        coverNameContactPrefTxt.setText(memName);
        //contact person
        contactPersonTxt.setText(memName);

        //methodDetailTxt.setVisible(true);
        //cb_MethodDetails.setVisible(false);
        //btn_ContactPrefSubmit.setVisible(false);
        //btn_ContactPrefClear.setVisible(false);
        //pnl_ContactPrefNewDate.setVisible(false);
        System.out.println("entityId = " + entityId);
        List<ContactDetails> contactDetailList = NeoWebService.getNeoManagerBeanService().getAllContactDetails(entityId);
        System.out.println("contactDetailList size = " + contactDetailList.size());
        contactList = contactDetailList;
        ArrayList userMethods = new ArrayList();
        int methodTypeId = 0;
        int MethodLookupId = 0;
        List<ContactPreference> contactPrefList = NeoWebService.getNeoManagerBeanService().getAllContactPreferences(entityId);
        int resultSize = contactPrefList.size();
        if (resultSize != 0) {
            conPrefList = contactPrefList;
            int rowSize = contactPrefList.size();
            TableEditor.addTableResultRows(rowSize, "Contact Preferences");
            //coverNumber
            memCoverNumber = coverNumberContactPrefTxt.getText();
            
            int x = 0;
            for (ContactPreference contactPreference : contactPrefList) {
                if (contactPreference.getCommunicationMethod()!=null){
                    prefHashSet.add(contactPreference.getCommunicationMethod());
                }
                //contact person
                if (contactPreference.getContactPerson()!=null){
                    table_ContactPrefferenceDetails.setValueAt(contactPreference.getContactPerson(), x, 0);
                }
                //communication type
                table_ContactPrefferenceDetails.setValueAt(contactPreference.getCommunicationType(), x, 1);
                //method
                if (contactPreference.getDetailType().equalsIgnoreCase("C")){
                    table_ContactPrefferenceDetails.setValueAt(contactPreference.getCommunicationMethod(), x, 2);
                    allDetails.put(contactPreference.getCommunicationMethod(), contactPreference.getMethodDetail());                    
                    
                }
                else{
                     List<neo.manager.AddressDetails> addressList = NeoWebService.getNeoManagerBeanService().getAllAddressDetails(entityId);
                     
                     AddressDetails ad;
                     if(contactPreference.getContactAddressType()==2){
                        ad = addressList.get(0);
                     }else{
                         ad = addressList.get(1);
                     }
                     table_ContactPrefferenceDetails.setValueAt(ad.getAddressType(), x, 2);
                     address = ad.getAddressType();
                     addressDetail = ad.getAddressLine1() + " " + ad.getAddressLine2() + " " + ad.getAddressLine2() + " " + ad.getAddressLine3()+ " " + ad.getPostalCode();
                }
                //method details
                ContactDetails methodDetails = NeoWebService.getNeoManagerBeanService().getMethodDetailsForContact(entityId, contactPreference);
                table_ContactPrefferenceDetails.setValueAt(methodDetails.getMethodDetails(), x, 3);
                //file layout
                table_ContactPrefferenceDetails.setValueAt(contactPreference.getFileLayout(), x, 4);
                //suppress mail
                table_ContactPrefferenceDetails.setValueAt(contactPreference.getSuppressMail(), x, 5);
                //Suppress Mail From
                if (contactPreference.getSuppressMailDateFrom() != null) {
                    table_ContactPrefferenceDetails.setValueAt(contactPreference.getSuppressMailDateFrom().toGregorianCalendar().getTime(), x, 6);
                }
                //Suppress Mail To
                if (contactPreference.getSuppressMailDateTo() != null) {
                    table_ContactPrefferenceDetails.setValueAt(contactPreference.getSuppressMailDateTo().toGregorianCalendar().getTime(), x, 7);
                }
                x++;
            }

            boolean test = false;
            
            ArrayList<String> commMethods = new ArrayList<String>();
            List<neo.manager.AddressDetails> addressList = NeoWebService.getNeoManagerBeanService().getAllAddressDetails(entityId);
            for (neo.manager.AddressDetails addressDetails : addressList){
                commMethods.add(addressDetails.getAddressType());
                if (addressDetails.getAddressLine1() == null){
                    addressDetails.setAddressLine1("");
                }
                if (addressDetails.getAddressLine2() == null){
                    addressDetails.setAddressLine2("");
                }
                if (addressDetails.getAddressLine3() == null){
                    addressDetails.setAddressLine3("");
                }
                
                if (addressDetails.getPostalCode() == null){
                    addressDetails.setPostalCode("");
                }
                allDetails.put(addressDetails.getAddressType(), addressDetails.getAddressLine1() + " " + addressDetails.getAddressLine2() + " " + addressDetails.getAddressLine3() + " " + addressDetails.getPostalCode());
                saveDetails.put(addressDetails.getAddressType(), "A");
            }
            
            List<ContactDetails> contactList = NeoWebService.getNeoManagerBeanService().getAllContactDetails(entityId);
            for (ContactDetails contact : contactList){
                allDetails.put(contact.getCommunicationMethod(), contact.getMethodDetails());
                saveDetails.put(contact.getCommunicationMethod(), "C");
            }
            if (address!=null){
                commMethods.add(address);
            }
            for (ContactDetails cd : contactDetailList) {
                String method = cd.getCommunicationMethod();
                commMethods.add(method);

            }
            
            
            prefHashSet = new TreeSet(commMethods);

            TableEditor.tableDateFormat("ConPref");
            methodTxt.setModel(new javax.swing.DefaultComboBoxModel(prefHashSet.toArray()));
            ContactPreference cp = conPrefList.get(0);
            contactPersonTxt.setText(cp.getContactPerson());
            cb_CommunicationType.setSelectedItem(cp.getCommunicationType());
            if (cp.getDetailType().equalsIgnoreCase("A")){
                methodTxt.setSelectedItem(address);
            }else{
                methodTxt.setSelectedItem(cp.getCommunicationMethod());
            }
            
            System.out.println("commMethod in load of details = " + cp.getCommunicationMethod() + " : " + cp.getContactDetailsId());
            int contactDetailsId = cp.getContactDetailsId();
            ContactDetails methodDetails = NeoWebService.getNeoManagerBeanService().getMethodDetailsForContact(entityId, cp);
            System.out.println("methodDetails " + methodDetails.getMethodDetails());
            methodDetailTxt.setText(methodDetails.getMethodDetails());
            cb_SuppressMail.setSelectedItem(cp.getSuppressMail());
            cb_FileLayout.setSelectedItem(cp.getFileLayout());
            if (cp.getSuppressMailDateFrom() != null) {
                contactSuppressMailFrom.setDate(cp.getSuppressMailDateFrom().toGregorianCalendar().getTime());
            }
            if (cp.getSuppressMailDateTo() != null) {
                contactSuppressMailTo.setDate(cp.getSuppressMailDateTo().toGregorianCalendar().getTime());
            }
        } else {
            System.out.println("No Contact Preferences");
            String contactPerson = "";
            for (ContactDetails con : contactDetailList) {
                String metDetail = con.getMethodDetails();
                String commMethod = con.getCommunicationMethod();

                System.out.println("metDetail = " + metDetail);
                System.out.println("commMethod = " + commMethod);

                if (commMethod.equalsIgnoreCase("e-mail")) {
                    if (!metDetail.trim().equalsIgnoreCase("")) {
                        emailDetails = metDetail;
                        contactPerson = memName;
                        break;
                    }
                }
            }
            System.out.println("emailDetails = " + emailDetails);
            System.out.println("contactPerson = " + contactPerson);

            /*if (!emailDetails.trim().equalsIgnoreCase("")) {
                methodDetailTxt.setText(emailDetails);
                TableEditor.addTableResultRows(5, "Contact Preferences");

                ArrayList strArr = new ArrayList();
                strArr.add("Claim Statement");
                strArr.add("Contributions Statement");
                strArr.add("Claim Payment");
                strArr.add("Tax Certificate");
                strArr.add("Pre-Auths");

                for (int x = 0; x <= 4; x++) {
                    table_ContactPrefferenceDetails.setValueAt(contactPerson, x, 0);
                    table_ContactPrefferenceDetails.setValueAt(strArr.get(x), x, 1);
                    table_ContactPrefferenceDetails.setValueAt("E-mail", x, 2);
                    table_ContactPrefferenceDetails.setValueAt(emailDetails, x, 3);

                }
            }*/


        }
    }

    public static void displayContactPreferences(int selectedRow) {
        
        String contactPerson = null;
        try {
            if (table_ContactPrefferenceDetails.getValueAt(selectedRow, 0) != null){
               contactPerson = table_ContactPrefferenceDetails.getValueAt(selectedRow, 0).toString();
            }
            String commType = table_ContactPrefferenceDetails.getValueAt(selectedRow, 1).toString();
            String method = table_ContactPrefferenceDetails.getValueAt(selectedRow, 2).toString();
            String methodDetail = table_ContactPrefferenceDetails.getValueAt(selectedRow, 3).toString();
            String fileLayout = table_ContactPrefferenceDetails.getValueAt(selectedRow, 4).toString();
            String suppressMail = table_ContactPrefferenceDetails.getValueAt(selectedRow, 5).toString();
            Date suppressFrom = (Date) table_ContactPrefferenceDetails.getValueAt(selectedRow, 6);
            Date suppressTo = (Date) table_ContactPrefferenceDetails.getValueAt(selectedRow, 7);

            /*if ((contactPerson == null) && (contactPerson.equals(""))) {
            System.out.println("no contact person provided");
            } else {
            contactPersonTxt.setText(contactPerson);
            }*/
            contactPersonTxt.setText(contactPerson);
            cb_CommunicationType.setSelectedItem(commType);
            methodTxt.setSelectedItem(method);
            methodDetailTxt.setText(methodDetail);
            cb_FileLayout.setSelectedItem(fileLayout);
            cb_SuppressMail.setSelectedItem(suppressMail);
            contactSuppressMailFrom.setDate(suppressFrom);
            contactSuppressMailTo.setDate(suppressTo);

            for (ContactPreference conPref : conPrefList) {
                if (commType.equals(conPref.getCommunicationType())) {
                    conPrefCommonId = conPref.getEntityCommonId();
                    conPrefContactDetailId =
                            conPref.getContactDetailsId();
                    System.out.println("conPrefCommonId = " + conPrefCommonId);
                }

            }
            TableEditor.tableDateFormat("ConPref");
        } catch (NullPointerException nEx) {
            System.out.println("Null poin exception occured when displaying contact preferences");
            nEx.printStackTrace();
        }
        

    }

    /************************************BANKING DETAIKS*************************************/
    public static void loadBankingDetails() {

        txt_BankCoverNumber.setText(memCoverNumber);
        txt_AccountHolder.setText(memName + " " + memSurname);



        String paymentMethod = "";
        String bankName = "";
        String branchName = "";
        String branchCode = "";
        String accountType = "";
        String accountName = "";
        String accountNumber = "";
        Date bankDateFrom = null;
        Date bankDateTo = null;

        List<neo.manager.BankingDetails> bankingList = NeoWebService.getNeoManagerBeanService().getAllBankingDetails(entityId);
        int rowSize = bankingList.size();
        if (rowSize != 0) {
            bankingStaticList = bankingList;
            TableEditor.addTableResultRows(rowSize, "Banking Details");
            int x = 0;
            for (neo.manager.BankingDetails bankingDetails : bankingStaticList) {
                paymentMethod = bankingDetails.getPaymentMethod();
                bankName = bankingDetails.getBankName();
                branchName = bankingDetails.getBranchName();
                branchCode = bankingDetails.getBranchCode();
                accountType = bankingDetails.getAccountType();
                accountName = bankingDetails.getAccountName();
                accountNumber = bankingDetails.getAccountNo();
                bankDateFrom = bankingDetails.getEffectiveStartDate().toGregorianCalendar().getTime();
                bankDateTo = bankingDetails.getEffectiveEndDate().toGregorianCalendar().getTime();

                String bankDateFromStr = "";
                String bankDateToStr = "";

                if (bankDateFrom != null) {
                    bankDateFromStr = sdf.format(bankDateFrom);
                }
                if (bankDateTo != null) {
                    bankDateToStr = sdf.format(bankDateTo);
                }

                bankingTable.setValueAt(paymentMethod, x, 0);
                bankingTable.setValueAt(bankName, x, 1);
                bankingTable.setValueAt(branchName, x, 2);
                bankingTable.setValueAt(branchCode, x, 3);
                bankingTable.setValueAt(accountType, x, 4);
                bankingTable.setValueAt(accountNumber, x, 5);
                bankingTable.setValueAt(accountName, x, 6);
                bankingTable.setValueAt(bankDateFromStr, x, 7);
                bankingTable.setValueAt(bankDateToStr, x, 8);

                x++;

            }
            BankingDetails bd = bankingList.get(0);
            cb_PaymentMethod.setSelectedItem(bd.getPaymentMethod());
            cb_BankName.setSelectedItem(bd.getBankName());
            cb_BranchName.setSelectedItem(bd.getBranchName());
            cb_BranchCode.setSelectedItem(bd.getBranchCode());
            cb_AccountType.setSelectedItem(bd.getAccountType());
            txt_AccountName.setText(bd.getAccountName());
            txt_AccountNumber.setText(bd.getAccountNo());
            dcb_EffectiveFrom.setDate(bd.getEffectiveStartDate().toGregorianCalendar().getTime());
            dcb_EffectiveTo.setDate(bd.getEffectiveEndDate().toGregorianCalendar().getTime());

            //force branch name selection
            rb_branchName.setSelected(true);

        } else {
            System.out.println("No banking ");
        }

    }

    public static void displayBankingDetails(int selectedRow) {

        String paymentMethod = bankingTable.getValueAt(selectedRow, 0).toString();
        String bankName = bankingTable.getValueAt(selectedRow, 1).toString();
        String branchName = bankingTable.getValueAt(selectedRow, 2).toString();
        String branchCode = bankingTable.getValueAt(selectedRow, 3).toString();
        String accountType = bankingTable.getValueAt(selectedRow, 4).toString();
        String accountNumber = bankingTable.getValueAt(selectedRow, 5).toString();
        String accountName = bankingTable.getValueAt(selectedRow, 6).toString();

        cb_PaymentMethod.setSelectedItem(paymentMethod);
        cb_BankName.setSelectedItem(bankName);
        cb_BranchName.setSelectedItem(branchName);
        cb_BranchCode.setSelectedItem(branchCode);
        cb_AccountType.setSelectedItem(accountType);
        txt_AccountName.setText(accountName);
        txt_AccountNumber.setText(accountNumber);


        for (BankingDetails bDetails : bankingStaticList) {
            if (accountNumber.equals(bDetails.getAccountNo())) {
                bankCommonId = bDetails.getEntityCommonId();
                dcb_EffectiveFrom.setDate(bDetails.getEffectiveStartDate().toGregorianCalendar().getTime());
                dcb_EffectiveTo.setDate(bDetails.getEffectiveEndDate().toGregorianCalendar().getTime());
            }

        }
    }

    public void saveBankingDetails() {

        BankingDetails bankingDetails = new BankingDetails();
        bankingDetails.setAccountName(txt_AccountName.getText());
        bankingDetails.setAccountNo(txt_AccountNumber.getText());
        if (dcb_EffectiveFrom.getDate() != null) {
            XMLGregorianCalendar bankStartDate = UtilMethods.convertDateToXMLGeoCalender(dcb_EffectiveFrom.getDate());
            bankingDetails.setEffectiveStartDate(bankStartDate);
        }

        if (dcb_EffectiveTo.getDate() != null) {
            XMLGregorianCalendar bankEndDate = UtilMethods.convertDateToXMLGeoCalender(dcb_EffectiveTo.getDate());
            bankingDetails.setEffectiveStartDate(bankEndDate);
        }

        String branchId = "" + cb_BranchName.getSelectedItem();
        if (branchId != null && !branchId.trim().equals("")) {
            bankingDetails.setBankBranchId(bankBranchIdMap.get(branchId));
        }
        String accType = "" + cb_AccountType.getSelectedItem();
        System.out.println("accType selected = " + accType);
        if (accType != null && !accType.trim().equals("")) {
            bankingDetails.setAccountTypeId(accountTypeMap.get(accType));
        }

        bankingDetails.setAccountNo(txt_AccountNumber.getText());
        bankingDetails.setAccountName(txt_AccountName.getText());

        String payMethod = "" + cb_PaymentMethod.getSelectedItem();
        if (payMethod != null && !payMethod.trim().equals("")) {
            bankingDetails.setPaymentMethodId(paymentMethodMap.get(payMethod));
        }

        if(bankFlag){
            bankingDetails.setEntityCommonId(bankCommonId);
        }

        boolean bankUpdated = NeoWebService.getNeoManagerBeanService().saveAndUpdateBankingDetails(entityId, LoginPage.neoUser.getUserId(), bankingDetails);

        if (bankUpdated) {
            NeoMessageDialog.showInfoMessage("Banking Details Successfully Added");

        } else {
            NeoMessageDialog.showInfoMessage("Banking Detail Save Failed");
        }
        loadBankingDetails();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        branchGroup = new javax.swing.ButtonGroup();
        tpane_CoverPersonDetails = new javax.swing.JTabbedPane();
        pnl_viewCoverPerson = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jPanel18 = new javax.swing.JPanel();
        styledLabel26 = new com.jidesoft.swing.StyledLabel();
        btn_InsuredHistory = new com.jidesoft.swing.JideButton();
        btn_PreInsured = new com.jidesoft.swing.JideButton();
        btn_OptionAllocate = new com.jidesoft.swing.JideButton();
        btnSearch = new com.jidesoft.swing.JideButton();
        pnl_coverDetailsPane = new javax.swing.JPanel();
        styledLabel8 = new com.jidesoft.swing.StyledLabel();
        coverNumberTxt = new javax.swing.JTextField();
        styledLabel9 = new com.jidesoft.swing.StyledLabel();
        previousCoverNumberTxt = new javax.swing.JTextField();
        styledLabel10 = new com.jidesoft.swing.StyledLabel();
        styledLabel11 = new com.jidesoft.swing.StyledLabel();
        styledLabel12 = new com.jidesoft.swing.StyledLabel();
        optionTxt = new javax.swing.JTextField();
        dependentCodeTxt = new javax.swing.JTextField();
        policyNameTxt = new javax.swing.JTextField();
        styledLabel13 = new com.jidesoft.swing.StyledLabel();
        dependentType = new javax.swing.JTextField();
        styledLabel14 = new com.jidesoft.swing.StyledLabel();
        titleTxt = new javax.swing.JTextField();
        styledLabel15 = new com.jidesoft.swing.StyledLabel();
        initialsTxt = new javax.swing.JTextField();
        styledLabel16 = new com.jidesoft.swing.StyledLabel();
        nameTxt = new javax.swing.JTextField();
        styledLabel17 = new com.jidesoft.swing.StyledLabel();
        surnameTxt = new javax.swing.JTextField();
        styledLabel18 = new com.jidesoft.swing.StyledLabel();
        maritalStatusTxt = new javax.swing.JTextField();
        styledLabel19 = new com.jidesoft.swing.StyledLabel();
        idNumberTxt = new javax.swing.JTextField();
        styledLabel20 = new com.jidesoft.swing.StyledLabel();
        genderTxt = new javax.swing.JTextField();
        styledLabel21 = new com.jidesoft.swing.StyledLabel();
        ageTxt = new javax.swing.JTextField();
        styledLabel22 = new com.jidesoft.swing.StyledLabel();
        styledLabel23 = new com.jidesoft.swing.StyledLabel();
        homeLanguageTxt = new javax.swing.JTextField();
        payRollTxt = new javax.swing.JTextField();
        styledLabel24 = new com.jidesoft.swing.StyledLabel();
        styledLabel25 = new com.jidesoft.swing.StyledLabel();
        employerTxt = new javax.swing.JTextField();
        styledLabel28 = new com.jidesoft.swing.StyledLabel();
        styledLabel29 = new com.jidesoft.swing.StyledLabel();
        incomeTxt = new javax.swing.JTextField();
        styledLabel30 = new com.jidesoft.swing.StyledLabel();
        styledLabel31 = new com.jidesoft.swing.StyledLabel();
        coverDateFrom = new com.jidesoft.combobox.DateComboBox();
        styledLabel32 = new com.jidesoft.swing.StyledLabel();
        coverDateTo = new com.jidesoft.combobox.DateComboBox();
        styledLabel33 = new com.jidesoft.swing.StyledLabel();
        styledLabel34 = new com.jidesoft.swing.StyledLabel();
        cb_StatusReason = new com.jidesoft.swing.AutoCompletionComboBox();
        cb_Paypoint = new javax.swing.JComboBox();
        dcb_DateOfBirth = new com.jidesoft.combobox.DateComboBox();
        dcb_AppStartDate = new com.jidesoft.combobox.DateComboBox();
        txt_memMainStatus = new javax.swing.JTextField();
        pnl_Unused = new javax.swing.JPanel();
        jideButton8 = new com.jidesoft.swing.JideButton();
        jideButton9 = new com.jidesoft.swing.JideButton();
        jideButton6 = new com.jidesoft.swing.JideButton();
        jideButton5 = new com.jidesoft.swing.JideButton();
        coverStatusSave = new com.jidesoft.swing.JideButton();
        btn_Underwriting = new com.jidesoft.swing.JideButton();
        pnl_ViewAddressDetails = new javax.swing.JPanel();
        btn_AddressClear = new com.jidesoft.swing.JideButton();
        bodyPanel = new javax.swing.JPanel();
        lbl_Number = new com.jidesoft.swing.StyledLabel();
        txt_AddressCoverNumber = new javax.swing.JTextField();
        styledLabel2 = new com.jidesoft.swing.StyledLabel();
        txt_AddressCoverName = new javax.swing.JTextField();
        styledLabel4 = new com.jidesoft.swing.StyledLabel();
        cb_AddressType = new com.jidesoft.swing.AutoCompletionComboBox();
        styledLabel6 = new com.jidesoft.swing.StyledLabel();
        txt_PostalCode = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txt_AddressLine1 = new javax.swing.JTextField();
        txt_AddressLine2 = new javax.swing.JTextField();
        txt_AddressLine3 = new javax.swing.JTextField();
        btn_AddressSubmit = new com.jidesoft.swing.JideButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        addressDetailsTable = new com.jidesoft.grid.JideTable();
        jScrollPane6 = new javax.swing.JScrollPane();
        jPanel21 = new javax.swing.JPanel();
        styledLabel53 = new com.jidesoft.swing.StyledLabel();
        jScrollPane15 = new javax.swing.JScrollPane();
        jPanel33 = new javax.swing.JPanel();
        styledLabel49 = new com.jidesoft.swing.StyledLabel();
        pnl_ViewBankingDetails = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        lbl_Number5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lbl_DateFrom = new javax.swing.JLabel();
        txt_BankCoverNumber = new javax.swing.JTextField();
        cb_PaymentMethod = new javax.swing.JComboBox();
        cb_BankName = new javax.swing.JComboBox();
        cb_BranchCode = new javax.swing.JComboBox();
        cb_AccountType = new javax.swing.JComboBox();
        txt_AccountName = new javax.swing.JTextField();
        dcb_EffectiveFrom = new com.jidesoft.combobox.DateComboBox();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        lbl_DateTo = new javax.swing.JLabel();
        txt_AccountHolder = new javax.swing.JTextField();
        cb_BranchName = new javax.swing.JComboBox();
        txt_AccountNumber = new javax.swing.JTextField();
        dcb_EffectiveTo = new com.jidesoft.combobox.DateComboBox();
        jLabel1 = new javax.swing.JLabel();
        rb_branchName = new javax.swing.JRadioButton();
        rb_branchCode = new javax.swing.JRadioButton();
        btn_BankingSubmit = new com.jidesoft.swing.JideButton();
        jScrollPane8 = new javax.swing.JScrollPane();
        jPanel22 = new javax.swing.JPanel();
        styledLabel65 = new com.jidesoft.swing.StyledLabel();
        btn_BankingClear = new com.jidesoft.swing.JideButton();
        jScrollPane7 = new javax.swing.JScrollPane();
        bankingTable = new com.jidesoft.grid.JideTable();
        jScrollPane14 = new javax.swing.JScrollPane();
        jPanel32 = new javax.swing.JPanel();
        styledLabel48 = new com.jidesoft.swing.StyledLabel();
        pnl_ViewContactDetails = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        lbl_Number1 = new com.jidesoft.swing.StyledLabel();
        coverNumberContactTxt = new javax.swing.JTextField();
        styledLabel43 = new com.jidesoft.swing.StyledLabel();
        cb_Method = new com.jidesoft.swing.AutoCompletionComboBox();
        styledLabel44 = new com.jidesoft.swing.StyledLabel();
        pnl_ContactMethodDetails = new javax.swing.JPanel();
        cb_ContactMethodDetails = new javax.swing.JComboBox();
        txt_ContactMethodDetails = new javax.swing.JTextField();
        btn_ContactClear = new com.jidesoft.swing.JideButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        jPanel20 = new javax.swing.JPanel();
        styledLabel52 = new com.jidesoft.swing.StyledLabel();
        btn_ContactSubmit = new com.jidesoft.swing.JideButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        table_ContactDetails = new com.jidesoft.grid.JideTable();
        jScrollPane12 = new javax.swing.JScrollPane();
        jPanel31 = new javax.swing.JPanel();
        styledLabel47 = new com.jidesoft.swing.StyledLabel();
        pnl_ContactPreferences = new javax.swing.JPanel();
        btn_ContactPrefClear = new com.jidesoft.swing.JideButton();
        btn_ContactPrefSubmit = new com.jidesoft.swing.JideButton();
        jScrollPane10 = new javax.swing.JScrollPane();
        table_ContactPrefferenceDetails = new com.jidesoft.grid.JideTable();
        jPanel8 = new javax.swing.JPanel();
        lbl_Number4 = new com.jidesoft.swing.StyledLabel();
        coverNumberContactPrefTxt = new javax.swing.JTextField();
        styledLabel70 = new com.jidesoft.swing.StyledLabel();
        contactPersonTxt = new javax.swing.JTextField();
        styledLabel71 = new com.jidesoft.swing.StyledLabel();
        styledLabel72 = new com.jidesoft.swing.StyledLabel();
        cb_FileLayout = new com.jidesoft.swing.AutoCompletionComboBox();
        styledLabel83 = new com.jidesoft.swing.StyledLabel();
        methodTxt = new com.jidesoft.swing.AutoCompletionComboBox();
        styledLabel84 = new com.jidesoft.swing.StyledLabel();
        styledLabel87 = new com.jidesoft.swing.StyledLabel();
        cb_SuppressMail = new com.jidesoft.swing.AutoCompletionComboBox();
        styledLabel88 = new com.jidesoft.swing.StyledLabel();
        contactSuppressMailFrom = new com.jidesoft.combobox.DateComboBox();
        styledLabel93 = new com.jidesoft.swing.StyledLabel();
        contactSuppressMailTo = new com.jidesoft.combobox.DateComboBox();
        styledLabel94 = new com.jidesoft.swing.StyledLabel();
        coverNameContactPrefTxt = new javax.swing.JTextField();
        cb_CommunicationType = new com.jidesoft.swing.AutoCompletionComboBox();
        pnl_MethodDetails = new javax.swing.JPanel();
        methodDetailTxt = new javax.swing.JTextField();
        jScrollPane11 = new javax.swing.JScrollPane();
        jPanel30 = new javax.swing.JPanel();
        styledLabel46 = new com.jidesoft.swing.StyledLabel();
        jScrollPane13 = new javax.swing.JScrollPane();
        jPanel23 = new javax.swing.JPanel();
        styledLabel69 = new com.jidesoft.swing.StyledLabel();
        pnl_ViewDependents = new javax.swing.JPanel();
        btn_DepUnderwriting = new com.jidesoft.swing.JideButton();
        jScrollPane9 = new javax.swing.JScrollPane();
        jPanel29 = new javax.swing.JPanel();
        styledLabel45 = new com.jidesoft.swing.StyledLabel();
        jPanel9 = new javax.swing.JPanel();
        styledLabel27 = new com.jidesoft.swing.StyledLabel();
        styledLabel40 = new com.jidesoft.swing.StyledLabel();
        styledLabel41 = new com.jidesoft.swing.StyledLabel();
        styledLabel42 = new com.jidesoft.swing.StyledLabel();
        styledLabel51 = new com.jidesoft.swing.StyledLabel();
        styledLabel54 = new com.jidesoft.swing.StyledLabel();
        styledLabel55 = new com.jidesoft.swing.StyledLabel();
        styledLabel56 = new com.jidesoft.swing.StyledLabel();
        styledLabel57 = new com.jidesoft.swing.StyledLabel();
        styledLabel58 = new com.jidesoft.swing.StyledLabel();
        txt_DepName = new javax.swing.JTextField();
        dcb_DOB = new com.jidesoft.combobox.DateComboBox();
        coverNumberTxt3 = new javax.swing.JTextField();
        txt_DepIncome = new javax.swing.JTextField();
        dcb_DepCoverStart = new com.jidesoft.combobox.DateComboBox();
        styledLabel59 = new com.jidesoft.swing.StyledLabel();
        styledLabel60 = new com.jidesoft.swing.StyledLabel();
        styledLabel61 = new com.jidesoft.swing.StyledLabel();
        styledLabel62 = new com.jidesoft.swing.StyledLabel();
        styledLabel63 = new com.jidesoft.swing.StyledLabel();
        styledLabel64 = new com.jidesoft.swing.StyledLabel();
        styledLabel66 = new com.jidesoft.swing.StyledLabel();
        styledLabel67 = new com.jidesoft.swing.StyledLabel();
        styledLabel68 = new com.jidesoft.swing.StyledLabel();
        jLabel12 = new javax.swing.JLabel();
        cb_DepStatusReason = new com.jidesoft.swing.AutoCompletionComboBox();
        txt_DepSurname = new javax.swing.JTextField();
        txt_DepIdNumber = new javax.swing.JTextField();
        txt_DepAge = new javax.swing.JTextField();
        dcb_DepCoverEnd = new com.jidesoft.combobox.DateComboBox();
        txt_DepInitials = new javax.swing.JTextField();
        dcb_RegDate = new com.jidesoft.combobox.DateComboBox();
        cb_ViewDependentNumber = new javax.swing.JComboBox();
        txt_dependentType = new javax.swing.JTextField();
        txt_DepTitle = new javax.swing.JTextField();
        txt_DepIdType = new javax.swing.JTextField();
        txt_DepGender = new javax.swing.JTextField();
        txt_DepHomeLan = new javax.swing.JTextField();
        txt_DepMarital = new javax.swing.JTextField();
        txt_memDepStatus = new javax.swing.JTextField();
        btn_DepStatusSave = new com.jidesoft.swing.JideButton();

        setBackground(new java.awt.Color(235, 235, 235));
        setPreferredSize(new java.awt.Dimension(1058, 966));

        tpane_CoverPersonDetails.setBackground(new java.awt.Color(235, 235, 235));
        tpane_CoverPersonDetails.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tpane_CoverPersonDetailsStateChanged(evt);
            }
        });

        pnl_viewCoverPerson.setBackground(new java.awt.Color(235, 235, 235));

        jScrollPane3.setBackground(new java.awt.Color(138, 177, 230));

        jPanel18.setBackground(new java.awt.Color(138, 177, 230));
        jPanel18.setForeground(new java.awt.Color(138, 177, 230));

        styledLabel26.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel26.setText("VIEW COVER PERSON DETAILS");
        jPanel18.add(styledLabel26);

        jScrollPane3.setViewportView(jPanel18);

        btn_InsuredHistory.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_InsuredHistory.setButtonStyle(com.jidesoft.swing.JideButton.TOOLBOX_STYLE);
        btn_InsuredHistory.setText("Insured Person History Grid");
        btn_InsuredHistory.setDoubleBuffered(true);
        btn_InsuredHistory.setFocusCycleRoot(true);
        btn_InsuredHistory.setOpaque(true);
        btn_InsuredHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_InsuredHistoryActionPerformed(evt);
            }
        });

        btn_PreInsured.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_PreInsured.setText("Previous Insured Details");
        btn_PreInsured.setOpaque(true);
        btn_PreInsured.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_PreInsuredActionPerformed(evt);
            }
        });

        btn_OptionAllocate.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_OptionAllocate.setText("Option Allocation");
        btn_OptionAllocate.setOpaque(true);
        btn_OptionAllocate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_OptionAllocateActionPerformed(evt);
            }
        });

        btnSearch.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnSearch.setText("Search");
        btnSearch.setOpaque(true);
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        pnl_coverDetailsPane.setBackground(new java.awt.Color(235, 235, 235));
        pnl_coverDetailsPane.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel8.setText("Cover Number");

        coverNumberTxt.setEditable(false);

        styledLabel9.setText("Previous Cover Number");

        previousCoverNumberTxt.setEditable(false);

        styledLabel10.setText("Dependent Code");

        styledLabel11.setText("Policy Name");

        styledLabel12.setText("Option");

        optionTxt.setEditable(false);

        dependentCodeTxt.setEditable(false);

        policyNameTxt.setEditable(false);

        styledLabel13.setText("Dependent Type");

        dependentType.setEditable(false);

        styledLabel14.setText("Title");

        titleTxt.setEditable(false);

        styledLabel15.setText("Initials");

        initialsTxt.setEditable(false);

        styledLabel16.setText("Name");

        nameTxt.setEditable(false);

        styledLabel17.setText("Surname");

        surnameTxt.setEditable(false);

        styledLabel18.setText("Marital Status");

        maritalStatusTxt.setEditable(false);

        styledLabel19.setText("Identification Number");

        idNumberTxt.setEditable(false);

        styledLabel20.setText("Gender");

        genderTxt.setEditable(false);

        styledLabel21.setText("Age");

        ageTxt.setEditable(false);

        styledLabel22.setText("Date of Birth");

        styledLabel23.setText("Home Language");

        homeLanguageTxt.setEditable(false);

        payRollTxt.setEditable(false);

        styledLabel24.setText("Payroll Number");

        styledLabel25.setText("Employer");

        employerTxt.setEditable(false);

        styledLabel28.setText("Pay Point");

        styledLabel29.setText("Income");

        incomeTxt.setEditable(false);

        styledLabel30.setText("Application Received Date");

        styledLabel31.setText("Status Date From");

        coverDateFrom.setButtonVisible(false);
        coverDateFrom.setEditable(false);
        coverDateFrom.setPreferredSize(new java.awt.Dimension(6, 20));

        styledLabel32.setText("Status Date To");

        coverDateTo.setButtonVisible(false);
        coverDateTo.setEditable(false);
        coverDateTo.setPreferredSize(new java.awt.Dimension(6, 20));

        styledLabel33.setText("Status");

        styledLabel34.setText("Reason Code");

        cb_StatusReason.setEditable(false);

        cb_Paypoint.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValues("Paypoint")));
        cb_Paypoint.setEnabled(false);

        dcb_DateOfBirth.setButtonVisible(false);
        dcb_DateOfBirth.setEditable(false);
        dcb_DateOfBirth.setPreferredSize(new java.awt.Dimension(6, 20));

        dcb_AppStartDate.setButtonVisible(false);
        dcb_AppStartDate.setEditable(false);
        dcb_AppStartDate.setPreferredSize(new java.awt.Dimension(6, 20));

        txt_memMainStatus.setEditable(false);

        javax.swing.GroupLayout pnl_coverDetailsPaneLayout = new javax.swing.GroupLayout(pnl_coverDetailsPane);
        pnl_coverDetailsPane.setLayout(pnl_coverDetailsPaneLayout);
        pnl_coverDetailsPaneLayout.setHorizontalGroup(
            pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_coverDetailsPaneLayout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_coverDetailsPaneLayout.createSequentialGroup()
                        .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnl_coverDetailsPaneLayout.createSequentialGroup()
                                .addComponent(styledLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(45, 45, 45))
                            .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(styledLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(styledLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(styledLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(styledLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(styledLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(styledLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(styledLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(styledLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(styledLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(styledLabel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(styledLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(10, 10, 10))
                    .addGroup(pnl_coverDetailsPaneLayout.createSequentialGroup()
                        .addComponent(styledLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(51, 51, 51)))
                .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_memMainStatus, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                    .addComponent(dependentCodeTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                    .addComponent(coverNumberTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                    .addComponent(policyNameTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                    .addComponent(titleTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                    .addComponent(nameTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                    .addComponent(maritalStatusTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                    .addComponent(genderTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                    .addComponent(dcb_DateOfBirth, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                    .addComponent(payRollTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                    .addComponent(incomeTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                    .addComponent(dcb_AppStartDate, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                    .addComponent(coverDateFrom, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE))
                .addGap(63, 63, 63)
                .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46)
                .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(coverDateTo, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(dependentType, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(previousCoverNumberTxt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                        .addComponent(optionTxt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                        .addComponent(initialsTxt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
                        .addComponent(surnameTxt, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(ageTxt, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(idNumberTxt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
                        .addComponent(homeLanguageTxt, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(employerTxt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                        .addComponent(cb_Paypoint, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(cb_StatusReason, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21))
        );
        pnl_coverDetailsPaneLayout.setVerticalGroup(
            pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_coverDetailsPaneLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnl_coverDetailsPaneLayout.createSequentialGroup()
                        .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                            .addComponent(styledLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(previousCoverNumberTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(coverNumberTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(styledLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dependentType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dependentCodeTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnl_coverDetailsPaneLayout.createSequentialGroup()
                        .addGap(79, 79, 79)
                        .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                            .addComponent(styledLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(optionTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(policyNameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(styledLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(initialsTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(titleTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(styledLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(surnameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(styledLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(styledLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(idNumberTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(maritalStatusTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(styledLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(styledLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ageTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(genderTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(homeLanguageTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dcb_DateOfBirth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(employerTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(payRollTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_Paypoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(incomeTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dcb_AppStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(56, 56, 56)
                .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_StatusReason, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_memMainStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnl_coverDetailsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(coverDateFrom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(coverDateTo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(61, Short.MAX_VALUE))
        );

        jideButton8.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jideButton8.setText("Find Docs");
        jideButton8.setOpaque(true);

        jideButton9.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jideButton9.setText("Order Card");
        jideButton9.setOpaque(true);
        jideButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jideButton9ActionPerformed(evt);
            }
        });

        jideButton6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jideButton6.setText("General Welocome Letter");
        jideButton6.setOpaque(true);
        jideButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jideButton6ActionPerformed(evt);
            }
        });

        jideButton5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jideButton5.setText("Add Dependent");
        jideButton5.setOpaque(true);
        jideButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jideButton5ActionPerformed(evt);
            }
        });

        coverStatusSave.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        coverStatusSave.setText("Save");
        coverStatusSave.setOpaque(true);
        coverStatusSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                coverStatusSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnl_UnusedLayout = new javax.swing.GroupLayout(pnl_Unused);
        pnl_Unused.setLayout(pnl_UnusedLayout);
        pnl_UnusedLayout.setHorizontalGroup(
            pnl_UnusedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_UnusedLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jideButton8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jideButton9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnl_UnusedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jideButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jideButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(coverStatusSave, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnl_UnusedLayout.setVerticalGroup(
            pnl_UnusedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_UnusedLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_UnusedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(coverStatusSave, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnl_UnusedLayout.createSequentialGroup()
                        .addGroup(pnl_UnusedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jideButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jideButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jideButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jideButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        btn_Underwriting.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_Underwriting.setButtonStyle(com.jidesoft.swing.JideButton.TOOLBOX_STYLE);
        btn_Underwriting.setText("Underwriting");
        btn_Underwriting.setOpaque(true);
        btn_Underwriting.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_UnderwritingActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnl_viewCoverPersonLayout = new javax.swing.GroupLayout(pnl_viewCoverPerson);
        pnl_viewCoverPerson.setLayout(pnl_viewCoverPersonLayout);
        pnl_viewCoverPersonLayout.setHorizontalGroup(
            pnl_viewCoverPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_viewCoverPersonLayout.createSequentialGroup()
                .addGroup(pnl_viewCoverPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_viewCoverPersonLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnl_viewCoverPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane3)
                            .addComponent(pnl_coverDetailsPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnl_viewCoverPersonLayout.createSequentialGroup()
                        .addGap(299, 299, 299)
                        .addComponent(pnl_Unused, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl_viewCoverPersonLayout.createSequentialGroup()
                        .addGap(131, 131, 131)
                        .addComponent(btn_Underwriting, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_InsuredHistory, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_PreInsured, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_OptionAllocate, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnl_viewCoverPersonLayout.setVerticalGroup(
            pnl_viewCoverPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_viewCoverPersonLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(pnl_coverDetailsPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addGroup(pnl_viewCoverPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_InsuredHistory, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_PreInsured, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_OptionAllocate, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_Underwriting, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addComponent(pnl_Unused, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(128, 128, 128))
        );

        tpane_CoverPersonDetails.addTab("VIEW COVER PERSON", pnl_viewCoverPerson);

        pnl_ViewAddressDetails.setBackground(new java.awt.Color(235, 235, 235));

        btn_AddressClear.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_AddressClear.setText("CLEAR");
        btn_AddressClear.setOpaque(true);
        btn_AddressClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AddressClearActionPerformed(evt);
            }
        });

        System.out.println("test inside body panel");
        bodyPanel.setBackground(new java.awt.Color(235, 235, 235));
        bodyPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbl_Number.setText("Cover Number");
        lbl_Number.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N

        txt_AddressCoverNumber.setEditable(false);

        styledLabel2.setText("Name");

        txt_AddressCoverName.setEditable(false);

        styledLabel4.setText("Address Type");

        cb_AddressType.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Entity Address Types", addressTypeMap)));
        cb_AddressType.setStrict(false);

        styledLabel6.setText("Postal Code");

        jLabel13.setText("Address Line 1");

        jLabel14.setText("Address Line 2");

        jLabel18.setText("Address Line 3");

        javax.swing.GroupLayout bodyPanelLayout = new javax.swing.GroupLayout(bodyPanel);
        bodyPanel.setLayout(bodyPanelLayout);
        bodyPanelLayout.setHorizontalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(bodyPanelLayout.createSequentialGroup()
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbl_Number, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13)
                            .addComponent(jLabel14)
                            .addComponent(jLabel18))
                        .addGap(48, 48, 48)
                        .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_PostalCode, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_AddressLine2)
                            .addComponent(txt_AddressLine1)
                            .addGroup(bodyPanelLayout.createSequentialGroup()
                                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cb_AddressType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txt_AddressCoverNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(44, 44, 44)
                                .addComponent(txt_AddressCoverName, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txt_AddressLine3))))
                .addGap(56, 56, 56))
        );
        bodyPanelLayout.setVerticalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_Number, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_AddressCoverName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_AddressCoverNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_AddressType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txt_AddressLine1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txt_AddressLine2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(txt_AddressLine3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_PostalCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        btn_AddressSubmit.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_AddressSubmit.setText("SUBMIT");
        btn_AddressSubmit.setOpaque(true);
        btn_AddressSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AddressSubmitActionPerformed(evt);
            }
        });

        addressDetailsTable.setBackground(new java.awt.Color(235, 235, 235));
        addressDetailsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Address Id", "Address Type", "Postal Code", "Start Date", "End Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        addressDetailsTable.setColumnAutoResizable(true);
        addressDetailsTable.setColumnResizable(true);
        addressDetailsTable.setRowResizable(true);
        addressDetailsTable.setScrollRowWhenRowHeightChanges(true);
        addressDetailsTable.setSelectionBackground(new java.awt.Color(138, 177, 230));
        addressDetailsTable.getTableHeader().setReorderingAllowed(false);
        addressDetailsTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                addressDetailsTableMouseReleased(evt);
            }
        });
        jScrollPane5.setViewportView(addressDetailsTable);

        jScrollPane6.setBackground(new java.awt.Color(138, 177, 230));

        jPanel21.setBackground(new java.awt.Color(138, 177, 230));
        jPanel21.setForeground(new java.awt.Color(138, 177, 230));

        styledLabel53.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel53.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel53.setText("Address Details");
        jPanel21.add(styledLabel53);

        jScrollPane6.setViewportView(jPanel21);

        jScrollPane15.setBackground(new java.awt.Color(138, 177, 230));

        jPanel33.setBackground(new java.awt.Color(138, 177, 230));
        jPanel33.setForeground(new java.awt.Color(138, 177, 230));
        jPanel33.setMinimumSize(new java.awt.Dimension(160, 24));
        jPanel33.setPreferredSize(new java.awt.Dimension(160, 24));

        styledLabel49.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel49.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel49.setText("VIEW ADDRESS DETAILS");
        jPanel33.add(styledLabel49);

        jScrollPane15.setViewportView(jPanel33);

        javax.swing.GroupLayout pnl_ViewAddressDetailsLayout = new javax.swing.GroupLayout(pnl_ViewAddressDetails);
        pnl_ViewAddressDetails.setLayout(pnl_ViewAddressDetailsLayout);
        pnl_ViewAddressDetailsLayout.setHorizontalGroup(
            pnl_ViewAddressDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_ViewAddressDetailsLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(pnl_ViewAddressDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane15, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bodyPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnl_ViewAddressDetailsLayout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addComponent(btn_AddressSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(47, 47, 47)
                        .addComponent(btn_AddressClear, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 402, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.LEADING))
                .addGap(204, 204, 204))
        );
        pnl_ViewAddressDetailsLayout.setVerticalGroup(
            pnl_ViewAddressDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_ViewAddressDetailsLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(bodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(pnl_ViewAddressDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_AddressSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_AddressClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(376, Short.MAX_VALUE))
        );

        tpane_CoverPersonDetails.addTab("ADDRESS DETAILS", pnl_ViewAddressDetails);

        pnl_ViewBankingDetails.setBackground(new java.awt.Color(235, 235, 235));

        jPanel6.setBackground(new java.awt.Color(235, 235, 235));
        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbl_Number5.setText("Cover Number");

        jLabel2.setText("Payment Method");

        jLabel3.setText("Bank Name");

        jLabel4.setText("Branch Code");

        jLabel5.setText("Account Name");

        jLabel6.setText("Account Type");

        lbl_DateFrom.setText("Date Effective From");

        txt_BankCoverNumber.setEditable(false);
        txt_BankCoverNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_BankCoverNumberActionPerformed(evt);
            }
        });

        cb_PaymentMethod.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapString("Payment Method", paymentMethodMap)));
        cb_PaymentMethod.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cb_PaymentMethodItemStateChanged(evt);
            }
        });

        if(searchAllBanks()!=null){
            cb_BankName.setModel(new javax.swing.DefaultComboBoxModel(searchAllBanks()));
        }
        cb_BankName.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cb_BankNameItemStateChanged(evt);
            }
        });

        cb_AccountType.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapString("Account Type", accountTypeMap)));
        cb_AccountType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cb_AccountTypeItemStateChanged(evt);
            }
        });

        dcb_EffectiveFrom.setPreferredSize(new java.awt.Dimension(6, 20));

        jLabel8.setText("Account Holder");

        jLabel9.setText("Branch Name");

        jLabel10.setText("Account Number");

        lbl_DateTo.setText("Date Effective To");

        txt_AccountHolder.setEditable(false);

        dcb_EffectiveTo.setPreferredSize(new java.awt.Dimension(6, 20));

        jLabel1.setText("Search Branch by");

        rb_branchName.setBackground(new java.awt.Color(255, 255, 255));
        branchGroup.add(rb_branchName);
        rb_branchName.setText("Branch Name");
        rb_branchName.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rb_branchNameItemStateChanged(evt);
            }
        });

        rb_branchCode.setBackground(new java.awt.Color(255, 255, 255));
        branchGroup.add(rb_branchCode);
        rb_branchCode.setText("Branch Code");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel9)
                            .addComponent(jLabel5)
                            .addComponent(lbl_DateFrom))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dcb_EffectiveFrom, javax.swing.GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE)
                            .addComponent(txt_AccountName, javax.swing.GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE)
                            .addComponent(cb_BranchName, 0, 205, Short.MAX_VALUE))
                        .addGap(90, 90, 90)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel4)
                            .addComponent(jLabel10)
                            .addComponent(lbl_DateTo))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_AccountNumber)
                            .addComponent(dcb_EffectiveTo, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cb_BranchCode, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(lbl_Number5)
                            .addComponent(jLabel1))
                        .addGap(42, 42, 42)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(rb_branchName)
                                .addGap(18, 18, 18)
                                .addComponent(rb_branchCode))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cb_BankName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cb_PaymentMethod, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txt_BankCoverNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(90, 90, 90)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel6))
                                .addGap(42, 42, 42)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_AccountHolder, javax.swing.GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
                                    .addComponent(cb_AccountType, 0, 201, Short.MAX_VALUE))))))
                .addGap(92, 92, 92))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_BankCoverNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_AccountHolder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(lbl_Number5))))
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cb_PaymentMethod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cb_AccountType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel6))))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel3))
                    .addComponent(cb_BankName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(rb_branchName)
                    .addComponent(rb_branchCode))
                .addGap(25, 25, 25)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cb_BranchCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cb_BranchName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel9))))
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_AccountName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_AccountNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel10))))
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(dcb_EffectiveFrom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(dcb_EffectiveTo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbl_DateFrom)
                            .addComponent(lbl_DateTo))))
                .addGap(54, 54, 54))
        );

        btn_BankingSubmit.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_BankingSubmit.setText("SUBMIT");
        btn_BankingSubmit.setOpaque(true);
        btn_BankingSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_BankingSubmitActionPerformed(evt);
            }
        });

        jScrollPane8.setBackground(new java.awt.Color(138, 177, 230));

        jPanel22.setBackground(new java.awt.Color(138, 177, 230));
        jPanel22.setForeground(new java.awt.Color(138, 177, 230));

        styledLabel65.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel65.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel65.setText("Banking Details");
        jPanel22.add(styledLabel65);

        jScrollPane8.setViewportView(jPanel22);

        btn_BankingClear.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_BankingClear.setText("CLEAR");
        btn_BankingClear.setOpaque(true);
        btn_BankingClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_BankingClearActionPerformed(evt);
            }
        });

        bankingTable.setBackground(new java.awt.Color(235, 235, 235));
        bankingTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Payment Method", "Bank Name", "Branch Name", "Branch Code", "Account Type", "Account Number", "Account Name", "Date From", "Date To"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        bankingTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                bankingTableMouseReleased(evt);
            }
        });
        jScrollPane7.setViewportView(bankingTable);

        jScrollPane14.setBackground(new java.awt.Color(138, 177, 230));

        jPanel32.setBackground(new java.awt.Color(138, 177, 230));
        jPanel32.setForeground(new java.awt.Color(138, 177, 230));
        jPanel32.setMinimumSize(new java.awt.Dimension(160, 24));
        jPanel32.setPreferredSize(new java.awt.Dimension(160, 24));

        styledLabel48.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel48.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel48.setText("VIEW BANKING DETAILS");
        jPanel32.add(styledLabel48);

        jScrollPane14.setViewportView(jPanel32);

        javax.swing.GroupLayout pnl_ViewBankingDetailsLayout = new javax.swing.GroupLayout(pnl_ViewBankingDetails);
        pnl_ViewBankingDetails.setLayout(pnl_ViewBankingDetailsLayout);
        pnl_ViewBankingDetailsLayout.setHorizontalGroup(
            pnl_ViewBankingDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_ViewBankingDetailsLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(pnl_ViewBankingDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane14, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 867, Short.MAX_VALUE)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 867, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnl_ViewBankingDetailsLayout.createSequentialGroup()
                        .addGap(69, 69, 69)
                        .addComponent(btn_BankingSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(105, 105, 105)
                        .addComponent(btn_BankingClear, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(116, 116, 116))
        );
        pnl_ViewBankingDetailsLayout.setVerticalGroup(
            pnl_ViewBankingDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_ViewBankingDetailsLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(pnl_ViewBankingDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_BankingSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_BankingClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(344, Short.MAX_VALUE))
        );

        tpane_CoverPersonDetails.addTab("BANKING DETAILS", pnl_ViewBankingDetails);

        pnl_ViewContactDetails.setBackground(new java.awt.Color(235, 235, 235));

        jPanel4.setBackground(new java.awt.Color(235, 235, 235));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbl_Number1.setText("Cover Number");

        coverNumberContactTxt.setEditable(false);

        styledLabel43.setText("Contact Method");

        cb_Method.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Communication Method", commMethodTypeMap)));
        cb_Method.setStrict(false);
        cb_Method.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cb_MethodItemStateChanged(evt);
            }
        });

        styledLabel44.setText("Method Detail");

        javax.swing.GroupLayout pnl_ContactMethodDetailsLayout = new javax.swing.GroupLayout(pnl_ContactMethodDetails);
        pnl_ContactMethodDetails.setLayout(pnl_ContactMethodDetailsLayout);
        pnl_ContactMethodDetailsLayout.setHorizontalGroup(
            pnl_ContactMethodDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_ContactMethodDetailsLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(pnl_ContactMethodDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_ContactMethodDetails, javax.swing.GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                    .addComponent(cb_ContactMethodDetails, 0, 226, Short.MAX_VALUE)))
        );
        pnl_ContactMethodDetailsLayout.setVerticalGroup(
            pnl_ContactMethodDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_ContactMethodDetailsLayout.createSequentialGroup()
                .addComponent(cb_ContactMethodDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(txt_ContactMethodDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(styledLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbl_Number1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(41, 41, 41)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cb_Method, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(coverNumberContactTxt)
                            .addComponent(pnl_ContactMethodDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(130, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_Number1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(coverNumberContactTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_Method, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pnl_ContactMethodDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(55, Short.MAX_VALUE))
        );

        btn_ContactClear.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_ContactClear.setText("CLEAR");
        btn_ContactClear.setOpaque(true);
        btn_ContactClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ContactClearActionPerformed(evt);
            }
        });

        jScrollPane4.setBackground(new java.awt.Color(138, 177, 230));

        jPanel20.setBackground(new java.awt.Color(138, 177, 230));
        jPanel20.setForeground(new java.awt.Color(138, 177, 230));

        styledLabel52.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel52.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel52.setText("Contact Details");
        jPanel20.add(styledLabel52);

        jScrollPane4.setViewportView(jPanel20);

        btn_ContactSubmit.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_ContactSubmit.setText("SUBMIT");
        btn_ContactSubmit.setOpaque(true);
        btn_ContactSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ContactSubmitActionPerformed(evt);
            }
        });

        table_ContactDetails.setBackground(new java.awt.Color(235, 235, 235));
        table_ContactDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cover Number", "Contact Details Id", "Method", "Method Detail"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        table_ContactDetails.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                table_ContactDetailsMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(table_ContactDetails);

        jScrollPane12.setBackground(new java.awt.Color(138, 177, 230));

        jPanel31.setBackground(new java.awt.Color(138, 177, 230));
        jPanel31.setForeground(new java.awt.Color(138, 177, 230));
        jPanel31.setMinimumSize(new java.awt.Dimension(160, 24));
        jPanel31.setPreferredSize(new java.awt.Dimension(160, 24));

        styledLabel47.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel47.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel47.setText("VIEW CONTACT DETAILS");
        jPanel31.add(styledLabel47);

        jScrollPane12.setViewportView(jPanel31);

        javax.swing.GroupLayout pnl_ViewContactDetailsLayout = new javax.swing.GroupLayout(pnl_ViewContactDetails);
        pnl_ViewContactDetails.setLayout(pnl_ViewContactDetailsLayout);
        pnl_ViewContactDetailsLayout.setHorizontalGroup(
            pnl_ViewContactDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_ViewContactDetailsLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(pnl_ViewContactDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane12, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_ViewContactDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnl_ViewContactDetailsLayout.createSequentialGroup()
                            .addGap(47, 47, 47)
                            .addComponent(btn_ContactSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(88, 88, 88)
                            .addComponent(btn_ContactClear, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(pnl_ViewContactDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(471, 471, 471))
        );
        pnl_ViewContactDetailsLayout.setVerticalGroup(
            pnl_ViewContactDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_ViewContactDetailsLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(pnl_ViewContactDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_ContactSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_ContactClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(429, 429, 429))
        );

        tpane_CoverPersonDetails.addTab("CONTACT DETAILS", pnl_ViewContactDetails);

        pnl_ContactPreferences.setBackground(new java.awt.Color(235, 235, 235));

        btn_ContactPrefClear.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_ContactPrefClear.setText("CLEAR");
        btn_ContactPrefClear.setOpaque(true);
        btn_ContactPrefClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ContactPrefClearActionPerformed(evt);
            }
        });

        btn_ContactPrefSubmit.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_ContactPrefSubmit.setText("SUBMIT");
        btn_ContactPrefSubmit.setOpaque(true);
        btn_ContactPrefSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ContactPrefSubmitActionPerformed(evt);
            }
        });

        table_ContactPrefferenceDetails.setBackground(new java.awt.Color(235, 235, 235));
        table_ContactPrefferenceDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Contact Person", "Communication Type", "Method", "Method Details", "File Layout", "Suppress Mail", "Suppress Mail From", "Suppress Mail To"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        table_ContactPrefferenceDetails.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                table_ContactPrefferenceDetailsMouseReleased(evt);
            }
        });
        jScrollPane10.setViewportView(table_ContactPrefferenceDetails);

        jPanel8.setBackground(new java.awt.Color(235, 235, 235));
        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbl_Number4.setText("Cover Number");

        coverNumberContactPrefTxt.setEditable(false);

        styledLabel70.setText("Contact Person");

        contactPersonTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                contactPersonTxtMouseReleased(evt);
            }
        });

        styledLabel71.setText("Communication Type");

        styledLabel72.setText("File Layout");

        cb_FileLayout.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("File Layout", fileLayoutMap)));
        cb_FileLayout.setStrict(false);

        styledLabel83.setText("Method");

        methodTxt.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Communication Method", prefMethodTypeMap)));
        methodTxt.setStrict(false);
        methodTxt.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                methodTxtItemStateChanged(evt);
            }
        });

        styledLabel84.setText("Method Detail");

        styledLabel87.setText("Suppress Mail");

        cb_SuppressMail.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValues("YesNo Type")));
        cb_SuppressMail.setStrict(false);
        cb_SuppressMail.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cb_SuppressMailItemStateChanged(evt);
            }
        });

        styledLabel88.setText("Suppress Mail From");

        contactSuppressMailFrom.setPreferredSize(new java.awt.Dimension(6, 20));

        styledLabel93.setText("Suppress Mail To");

        contactSuppressMailTo.setPreferredSize(new java.awt.Dimension(6, 20));

        styledLabel94.setText("Name");

        coverNameContactPrefTxt.setEditable(false);

        cb_CommunicationType.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Communication Type", communicationTypeMap)));
        cb_CommunicationType.setStrict(false);

        javax.swing.GroupLayout pnl_MethodDetailsLayout = new javax.swing.GroupLayout(pnl_MethodDetails);
        pnl_MethodDetails.setLayout(pnl_MethodDetailsLayout);
        pnl_MethodDetailsLayout.setHorizontalGroup(
            pnl_MethodDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(methodDetailTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE)
        );
        pnl_MethodDetailsLayout.setVerticalGroup(
            pnl_MethodDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_MethodDetailsLayout.createSequentialGroup()
                .addComponent(methodDetailTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 20, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel70, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_Number4, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel83, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel87, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel88, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(contactSuppressMailFrom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cb_SuppressMail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(contactPersonTxt)
                    .addComponent(methodTxt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(coverNumberContactPrefTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel94, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel71, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel84, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel72, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel93, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnl_MethodDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(coverNameContactPrefTxt, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cb_CommunicationType, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
                        .addComponent(cb_FileLayout, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(contactSuppressMailTo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(coverNumberContactPrefTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_Number4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel94, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(coverNameContactPrefTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                    .addComponent(contactPersonTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_CommunicationType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel70, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel71, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(methodTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(styledLabel83, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(styledLabel84, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(pnl_MethodDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cb_FileLayout, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_SuppressMail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel87, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel72, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel88, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(contactSuppressMailFrom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(contactSuppressMailTo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel93, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(60, Short.MAX_VALUE))
        );

        jScrollPane11.setBackground(new java.awt.Color(138, 177, 230));

        jPanel30.setBackground(new java.awt.Color(138, 177, 230));
        jPanel30.setForeground(new java.awt.Color(138, 177, 230));
        jPanel30.setMinimumSize(new java.awt.Dimension(160, 24));
        jPanel30.setPreferredSize(new java.awt.Dimension(160, 24));

        styledLabel46.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel46.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel46.setText("VIEW CONTACT PREFERENCES");
        jPanel30.add(styledLabel46);

        jScrollPane11.setViewportView(jPanel30);

        jScrollPane13.setBackground(new java.awt.Color(138, 177, 230));

        jPanel23.setBackground(new java.awt.Color(138, 177, 230));
        jPanel23.setForeground(new java.awt.Color(138, 177, 230));

        styledLabel69.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel69.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel69.setText("Contact Preferences");
        jPanel23.add(styledLabel69);

        jScrollPane13.setViewportView(jPanel23);

        javax.swing.GroupLayout pnl_ContactPreferencesLayout = new javax.swing.GroupLayout(pnl_ContactPreferences);
        pnl_ContactPreferences.setLayout(pnl_ContactPreferencesLayout);
        pnl_ContactPreferencesLayout.setHorizontalGroup(
            pnl_ContactPreferencesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_ContactPreferencesLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(pnl_ContactPreferencesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane13, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane11, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnl_ContactPreferencesLayout.createSequentialGroup()
                        .addGap(83, 83, 83)
                        .addComponent(btn_ContactPrefSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(103, 103, 103)
                        .addComponent(btn_ContactPrefClear, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 464, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane10))
                .addGap(133, 133, 133))
        );
        pnl_ContactPreferencesLayout.setVerticalGroup(
            pnl_ContactPreferencesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_ContactPreferencesLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_ContactPreferencesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_ContactPrefSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_ContactPrefClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(372, 372, 372))
        );

        tpane_CoverPersonDetails.addTab("CONTACT PREFERENCES", pnl_ContactPreferences);

        pnl_ViewDependents.setBackground(new java.awt.Color(235, 235, 235));

        btn_DepUnderwriting.setBackground(new java.awt.Color(183, 204, 244));
        btn_DepUnderwriting.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_DepUnderwriting.setText("Underwriting");
        btn_DepUnderwriting.setOpaque(true);
        btn_DepUnderwriting.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_DepUnderwritingActionPerformed(evt);
            }
        });

        jScrollPane9.setBackground(new java.awt.Color(138, 177, 230));

        jPanel29.setBackground(new java.awt.Color(138, 177, 230));
        jPanel29.setForeground(new java.awt.Color(138, 177, 230));
        jPanel29.setMinimumSize(new java.awt.Dimension(160, 24));
        jPanel29.setPreferredSize(new java.awt.Dimension(160, 24));

        styledLabel45.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel45.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel45.setText("VIEW DEPENDENT DETAILS");
        jPanel29.add(styledLabel45);

        jScrollPane9.setViewportView(jPanel29);

        jPanel9.setBackground(new java.awt.Color(235, 235, 235));
        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel27.setText("Cover Status");

        styledLabel40.setText("Income");

        styledLabel41.setText("Title");

        styledLabel42.setText("Cover Number");

        styledLabel51.setText("Dependent Type");

        styledLabel54.setText("Identification Type");

        styledLabel55.setText("Gender");

        styledLabel56.setText("Date of Birth");

        styledLabel57.setText("Status Date From");

        styledLabel58.setText("Name");

        txt_DepName.setEditable(false);

        dcb_DOB.setButtonVisible(false);
        dcb_DOB.setEditable(false);
        dcb_DOB.setPreferredSize(new java.awt.Dimension(6, 20));

        coverNumberTxt3.setEditable(false);

        txt_DepIncome.setEditable(false);

        dcb_DepCoverStart.setButtonVisible(false);
        dcb_DepCoverStart.setEditable(false);
        dcb_DepCoverStart.setPreferredSize(new java.awt.Dimension(6, 20));

        styledLabel59.setText("Dependent Number");

        styledLabel60.setText("Initials");

        styledLabel61.setText("Surname");

        styledLabel62.setText("Identification Number");

        styledLabel63.setText("Age");

        styledLabel64.setText("Home Language");

        styledLabel66.setText("Reason Code");

        styledLabel67.setText("Status Date To");

        styledLabel68.setText("Registration Date");

        jLabel12.setText("Marital Status");

        cb_DepStatusReason.setEditable(false);

        txt_DepSurname.setEditable(false);

        txt_DepIdNumber.setEditable(false);

        txt_DepAge.setEditable(false);

        dcb_DepCoverEnd.setButtonVisible(false);
        dcb_DepCoverEnd.setEditable(false);
        dcb_DepCoverEnd.setPreferredSize(new java.awt.Dimension(6, 20));

        txt_DepInitials.setEditable(false);

        dcb_RegDate.setButtonVisible(false);
        dcb_RegDate.setEditable(false);
        dcb_RegDate.setPreferredSize(new java.awt.Dimension(6, 20));
        dcb_RegDate.setStretchToFit(true);

        cb_ViewDependentNumber.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                cb_ViewDependentNumberMouseReleased(evt);
            }
        });
        cb_ViewDependentNumber.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cb_ViewDependentNumberItemStateChanged(evt);
            }
        });

        txt_dependentType.setEditable(false);

        txt_DepTitle.setEditable(false);

        txt_DepIdType.setEditable(false);

        txt_DepGender.setEditable(false);

        txt_DepHomeLan.setEditable(false);

        txt_DepMarital.setEditable(false);

        txt_memDepStatus.setEditable(false);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(styledLabel58, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(70, 70, 70))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(styledLabel41, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(styledLabel42, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(styledLabel51, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(styledLabel54, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(styledLabel55, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(styledLabel56, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18))
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addComponent(styledLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(34, 34, 34)))
                                .addGroup(jPanel9Layout.createSequentialGroup()
                                    .addComponent(styledLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(34, 34, 34)))
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addComponent(styledLabel57, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(34, 34, 34)))
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dcb_DepCoverStart, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                            .addComponent(txt_DepIncome, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                            .addComponent(dcb_DOB, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                            .addComponent(txt_DepGender, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                            .addComponent(txt_DepIdType, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                            .addComponent(txt_DepTitle, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                            .addComponent(txt_dependentType, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                            .addComponent(coverNumberTxt3, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                            .addComponent(txt_DepName, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                            .addComponent(txt_memDepStatus, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE))))
                .addGap(67, 67, 67)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel68, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel59, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel60, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel61, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel62, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel63, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel64, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(styledLabel66, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel67, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dcb_DepCoverEnd, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(txt_DepHomeLan, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
                                .addComponent(txt_DepMarital, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
                                .addComponent(cb_ViewDependentNumber, javax.swing.GroupLayout.Alignment.LEADING, 0, 196, Short.MAX_VALUE)
                                .addComponent(txt_DepInitials, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txt_DepIdNumber, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txt_DepSurname, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 175, Short.MAX_VALUE)
                                .addComponent(txt_DepAge, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(dcb_RegDate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addContainerGap(96, Short.MAX_VALUE))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(cb_DepStatusReason, javax.swing.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                        .addGap(36, 36, 36))))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                    .addComponent(styledLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(coverNumberTxt3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel59, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_ViewDependentNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel51, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_dependentType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel68, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dcb_RegDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_DepTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel60, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_DepInitials, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel58, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_DepName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel61, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_DepSurname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel54, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_DepIdType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel62, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_DepIdNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel55, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_DepGender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel63, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_DepAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel56, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dcb_DOB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel64, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_DepHomeLan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_DepIncome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(txt_DepMarital, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel66, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_DepStatusReason, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_memDepStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel57, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dcb_DepCoverStart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel67, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dcb_DepCoverEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(106, 106, 106))
        );

        btn_DepStatusSave.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_DepStatusSave.setText("Save");
        btn_DepStatusSave.setOpaque(true);
        btn_DepStatusSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_DepStatusSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnl_ViewDependentsLayout = new javax.swing.GroupLayout(pnl_ViewDependents);
        pnl_ViewDependents.setLayout(pnl_ViewDependentsLayout);
        pnl_ViewDependentsLayout.setHorizontalGroup(
            pnl_ViewDependentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_ViewDependentsLayout.createSequentialGroup()
                .addGroup(pnl_ViewDependentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_ViewDependentsLayout.createSequentialGroup()
                        .addGap(163, 163, 163)
                        .addComponent(btn_DepUnderwriting, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(320, 320, 320)
                        .addComponent(btn_DepStatusSave, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl_ViewDependentsLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(pnl_ViewDependentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane9))))
                .addContainerGap())
        );
        pnl_ViewDependentsLayout.setVerticalGroup(
            pnl_ViewDependentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_ViewDependentsLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(pnl_ViewDependentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_DepUnderwriting, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_DepStatusSave, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(309, Short.MAX_VALUE))
        );

        tpane_CoverPersonDetails.addTab("VIEW DEPENDENTS", pnl_ViewDependents);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tpane_CoverPersonDetails, javax.swing.GroupLayout.PREFERRED_SIZE, 960, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(88, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tpane_CoverPersonDetails)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tpane_CoverPersonDetailsStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tpane_CoverPersonDetailsStateChanged
        //btn_AddressSubmit.setVisible(false);
        int tabIndex = tpane_CoverPersonDetails.getSelectedIndex();
        System.out.println("tabIndex = " + tabIndex);
        String tabName = tpane_CoverPersonDetails.getTitleAt(tabIndex);
        System.out.println("tabName = " + tabName);
        paneSelection(tabName);
    }//GEN-LAST:event_tpane_CoverPersonDetailsStateChanged

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        EntityActionsFrame.entityActionList.setSelectedIndex(0);
        df.CreateSearchCoverMemberActions();
}//GEN-LAST:event_btnSearchActionPerformed

    private void cb_ViewDependentNumberItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cb_ViewDependentNumberItemStateChanged
        System.out.println("Event = " + evt.getSource());

        int dependentNumber = Integer.parseInt(cb_ViewDependentNumber.getSelectedItem().toString());
        System.out.println("dependenList size = " + dependantList.size());
        int x = 0;
        for (CoverDetails coverDetails : dependantList) {
            System.out.println("Cover Details Iteration = [" + x + "]");
            if (coverDetails.getDependentNumber() == dependentNumber) {
                depCoverDetail = coverDetails;
                depEntityId = coverDetails.getEntityId();
                txt_dependentType.setText(coverDetails.getDependentType());
                dcb_RegDate.setDate(coverDetails.getApplicationRecievedDate().toGregorianCalendar().getTime());
                //status
                Date today = new Date();
                Date endDate = coverDetails.getCoverEndDate().toGregorianCalendar().getTime();

                if (endDate.before(today)) {
                    txt_memDepStatus.setText("Resign");
                } else {
                    txt_memDepStatus.setText("Active");
                }
                String coverStatus = coverDetails.getStatus();
                //txt_memDepStatus.setText(coverStatus);
                System.out.println("coverStatus = " + coverStatus);
                if (coverStatus.equals("Active")) {
                    cb_DepStatusReason.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Membership Reason Code Active", statusActiveReasonMap)));

                } else if (coverStatus.equals("Suspend")) {
                    cb_DepStatusReason.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Membership Reason Code Suspend", statusSuspendReasonMap)));

                } else if (coverStatus.equals("Pended")) {
                    cb_DepStatusReason.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Membership Reason Code Pended", statusPendedReasonMap)));

                } else if (coverStatus.equals("Resign")) {
                    cb_DepStatusReason.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapInt("Membership Reason Code Resign", statusResignReasonMap)));

                }

                PersonDetails pDetails = NeoWebService.getNeoManagerBeanService().getPersonDetailsByEntityId(depEntityId);
                txt_DepTitle.setText(pDetails.getTitle());
                txt_DepInitials.setText(pDetails.getInitials());
                txt_DepName.setText(pDetails.getName());
                txt_DepSurname.setText(pDetails.getSurname());
                txt_DepIdType.setText(pDetails.getIdentificationType());
                txt_DepIdNumber.setText(pDetails.getIDNumber());
                txt_DepGender.setText(pDetails.getGenderDesc());
                txt_DepAge.setText(pDetails.getAge());
                dcb_DOB.setDate(pDetails.getDateOfBirth().toGregorianCalendar().getTime());
                txt_DepHomeLan.setText(pDetails.getHomeLanguage());
                txt_DepIncome.setText(pDetails.getIncomeCategory());
                txt_DepMarital.setText(pDetails.getMaritalStatus());

                dcb_DepCoverStart.setDate(coverDetails.getCoverStartDate().toGregorianCalendar().getTime());
                dcb_DepCoverEnd.setDate(coverDetails.getCoverEndDate().toGregorianCalendar().getTime());
            }

            x++;
        }
    }//GEN-LAST:event_cb_ViewDependentNumberItemStateChanged

    private void coverStatusSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_coverStatusSaveActionPerformed
        CoverDetails coverDetails = new CoverDetails();

        coverDetails.setCoverNumber(coverNumberTxt.getText());
        //status
        coverDetails.setStatusId(statusMap.get(txt_memMainStatus.getText()));
        String coverStatus = txt_memMainStatus.getText();
        System.out.println("coverStatus = " + coverStatus);
        if (coverStatus.equals("Active")) {//getLookupValuesForMapInt statusActiveReasonMap
            int statusReasonId = statusActiveReasonMap.get(cb_StatusReason.getSelectedItem());
            System.out.println("statusReasonId = " + statusReasonId);
            coverDetails.setStatusReasonId(statusReasonId);

        } else if (coverStatus.equals("Suspend")) {
            int statusReasonId = statusSuspendReasonMap.get(cb_StatusReason.getSelectedItem());
            System.out.println("statusReasonId = " + statusReasonId);
            coverDetails.setStatusReasonId(statusReasonId);

        } else if (coverStatus.equals("Pended")) {
            int statusReasonId = statusPendedReasonMap.get(cb_StatusReason.getSelectedItem());
            System.out.println("statusReasonId = " + statusReasonId);
            coverDetails.setStatusReasonId(statusReasonId);

        } else if (coverStatus.equals("Resign")) {
            int statusReasonId = statusResignReasonMap.get(cb_StatusReason.getSelectedItem());
            System.out.println("statusReasonId = " + statusReasonId);
            coverDetails.setStatusReasonId(statusReasonId);

        }

        Date statusStart = coverDateFrom.getDate();
        System.out.println("New Status Date = " + statusStart);
        XMLGregorianCalendar xmlGre = UtilMethods.convertDateToXMLGeoCalender(statusStart);
        System.out.println("New Status Date converted = " + xmlGre);
        coverDetails.setStatusStartDate(xmlGre);


        int statusUpdate = NeoWebService.getNeoManagerBeanService().updateFullCoverStatus(LoginPage.neoUser.getUserId(), entityId, coverDetails);
        System.out.println("statusUpdate = " + statusUpdate);
        if (statusUpdate == 1) {
            JOptionPane.showMessageDialog(null, "Cover Details Successfully Updated", "Cover Details Update Status", JOptionPane.INFORMATION_MESSAGE);

        } else {
            JOptionPane.showMessageDialog(null, "Cover Details Update Failed", "Cover Details Update Status", JOptionPane.ERROR_MESSAGE);

        }

        loadMainCoverDetails();

}//GEN-LAST:event_coverStatusSaveActionPerformed

    private void jideButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jideButton5ActionPerformed
        df.NewDependentAddAction(memCoverNumber, "entity");
    }//GEN-LAST:event_jideButton5ActionPerformed

    @SuppressWarnings("static-access")
    private void btn_OptionAllocateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_OptionAllocateActionPerformed
        OptionAllocateFrame ovFrame = new OptionAllocateFrame();
        ovFrame.txt_CoverName.setText(nameTxt.getText());
        ovFrame.txt_CoverSurname.setText(surnameTxt.getText());
        ovFrame.txt_Status.setText(mainMemList.getStatus());
        ovFrame.optMemCoverNum = memCoverNumber;
        ovFrame.loadBenefitsForOption();
        ovFrame.setVisible(true);

}//GEN-LAST:event_btn_OptionAllocateActionPerformed

    private void jideButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jideButton9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jideButton9ActionPerformed

    private void btn_PreInsuredActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_PreInsuredActionPerformed
        PreviousInsurenceViewFrame preInsured = new PreviousInsurenceViewFrame(memberIdNumber);
        preInsured.loadCoverHistory();
        preInsured.setVisible(true);
}//GEN-LAST:event_btn_PreInsuredActionPerformed

    private void btn_InsuredHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_InsuredHistoryActionPerformed
        InsuredPersonHistoryFrame insuredHistory = new InsuredPersonHistoryFrame(memberIdNumber, coverNumberTxt.getText());
        insuredHistory.setVisible(true);

    }//GEN-LAST:event_btn_InsuredHistoryActionPerformed

    private void btn_UnderwritingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_UnderwritingActionPerformed
        UnderwritingViewFrame underView = new UnderwritingViewFrame(entityId);
        underView.setVisible(true);
}//GEN-LAST:event_btn_UnderwritingActionPerformed

    private void jideButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jideButton6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jideButton6ActionPerformed

    private void cb_ViewDependentNumberMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cb_ViewDependentNumberMouseReleased
        loadCoverDep = true;
    }//GEN-LAST:event_cb_ViewDependentNumberMouseReleased

    private void btn_DepStatusSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_DepStatusSaveActionPerformed
        CoverDetails coverDetails = new CoverDetails();

        coverDetails.setEntityCommonId(mainMemList.getEntityCommonId());
        coverDetails.setEntityId(mainMemList.getEntityId());
        coverDetails.setCoverDetailsId(mainMemList.getCoverDetailsId());
        coverDetails.setProductId(mainMemList.getProductId());
        coverDetails.setOptionId(mainMemList.getOptionId());
        System.out.println("CoverNumber = " + coverNumberTxt.getText());
        coverDetails.setCoverNumber(coverNumberTxt.getText());
        coverDetails.setDependentNumber(mainMemList.getDependentNumber());
        float coverPremium = mainMemList.getCoverPremiumAmount();
        System.out.println("Cover Premium Amout = " + coverPremium);
        coverDetails.setCoverPremiumAmount(coverPremium);
        String depType = mainMemList.getDependentType();
        int depTypeId = mainMemList.getDependentTypeId();
        System.out.println("dependent Type = " + depType);
        System.out.println("dependent Type id = " + depType);
        coverDetails.setDependentType(depType);
        coverDetails.setDependentTypeId(depTypeId);
        coverDetails.setPenaltyLateJoinId(mainMemList.getPenaltyLateJoinId());
        coverDetails.setCoverStartDate(mainMemList.getCoverStartDate());
        coverDetails.setCoverEndDate(mainMemList.getCoverEndDate());
        coverDetails.setApplicationRecievedDate(mainMemList.getApplicationRecievedDate());

        //status
        coverDetails.setStatusId(statusMap.get(txt_memMainStatus.getText()));
        String coverStatus = txt_memMainStatus.getText();
        System.out.println("coverStatus = " + coverStatus);
        if (coverStatus.equals("Active")) {//getLookupValuesForMapInt statusActiveReasonMap
            int statusReasonId = statusActiveReasonMap.get(cb_StatusReason.getSelectedItem());
            System.out.println("statusReasonId = " + statusReasonId);
            coverDetails.setStatusReasonId(statusReasonId);

        } else if (coverStatus.equals("Suspend")) {
            int statusReasonId = statusSuspendReasonMap.get(cb_StatusReason.getSelectedItem());
            System.out.println("statusReasonId = " + statusReasonId);
            coverDetails.setStatusReasonId(statusReasonId);

        } else if (coverStatus.equals("Pended")) {
            int statusReasonId = statusPendedReasonMap.get(cb_StatusReason.getSelectedItem());
            System.out.println("statusReasonId = " + statusReasonId);
            coverDetails.setStatusReasonId(statusReasonId);

        } else if (coverStatus.equals("Resign")) {
            int statusReasonId = statusResignReasonMap.get(cb_StatusReason.getSelectedItem());
            System.out.println("statusReasonId = " + statusReasonId);
            coverDetails.setStatusReasonId(statusReasonId);

        }

        Date statusStart = coverDateFrom.getDate();
        System.out.println("New Status Date = " + statusStart);
        XMLGregorianCalendar xmlGre = UtilMethods.convertDateToXMLGeoCalender(statusStart);
        System.out.println("New Status Date converted = " + xmlGre);
        coverDetails.setStatusStartDate(xmlGre);


        int statusUpdate = NeoWebService.getNeoManagerBeanService().updateCoverStatus(LoginPage.neoUser.getUserId(), depEntityId, coverDetails);
        System.out.println("statusUpdate = " + statusUpdate);
        if (statusUpdate == 1) {
            JOptionPane.showMessageDialog(null, "Cover Details Successfully Updated", "Cover Details Update Status", JOptionPane.INFORMATION_MESSAGE);

        } else {
            JOptionPane.showMessageDialog(null, "Cover Details Update Failed", "Cover Details Update Status", JOptionPane.ERROR_MESSAGE);

        }

        loadDependantForCover();
    }//GEN-LAST:event_btn_DepStatusSaveActionPerformed

    private void btn_DepUnderwritingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_DepUnderwritingActionPerformed
        UnderwritingViewFrame underView = new UnderwritingViewFrame(depEntityId);
        underView.setVisible(true);
}//GEN-LAST:event_btn_DepUnderwritingActionPerformed

    private void btn_AddressClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AddressClearActionPerformed
        // TODO add your handling code here:
        txt_AddressLine1.setText("");
        txt_AddressLine2.setText("");
        txt_AddressLine3.setText("");
        txt_PostalCode.setText("");
        addressFlag = false;
}//GEN-LAST:event_btn_AddressClearActionPerformed

    private void btn_AddressSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AddressSubmitActionPerformed
        AddressDetails addressDetails = new AddressDetails();

        int addressTypeId = 0;
        int countryId = 0;
        int regionId = 0;
        int townId = 0;
        int suburbId = 0;
        int roadId = 0;

        if (addressFlag) {
            addressDetails.setEntityCommonId(addressCommonId);
        }

        String addressTypeName = cb_AddressType.getSelectedItem().toString();
        if (addressTypeName != null) {
            addressTypeId = new Integer(addressTypeMap.get(addressTypeName));
        }
        String addressLine1 = txt_AddressLine1.getText();
        String addressLine2 = txt_AddressLine2.getText();
        String addressLine3 = txt_AddressLine3.getText();
        String postalCode = txt_PostalCode.getText();
        String roadNo = "";//txt_RoadNo.getText();

        System.out.println("addressLine1 = " + addressLine1);
        System.out.println("addressLine2 = " + addressLine2);
        System.out.println("addressLine3 = " + addressLine3);

        addressDetails.setAddressTypeId(addressTypeId);
        addressDetails.setAddressLine1(addressLine1);
        addressDetails.setAddressLine2(addressLine2);
        addressDetails.setAddressLine3(addressLine3);
        addressDetails.setPostalCode(postalCode);
        addressDetails.setCountryId(countryId);
        addressDetails.setRegionId(regionId);
        addressDetails.setTownId(townId);
        addressDetails.setSuburbId(suburbId);
        addressDetails.setRoadId(roadId);
        addressDetails.setRoadNo(roadNo);


        boolean addressUpdated = NeoWebService.getNeoManagerBeanService().saveAndUpdateAddressDetails(entityId, LoginPage.neoUser.getUserId(), addressDetails);

        if (addressUpdated) {
            JOptionPane.showMessageDialog(null, "Address Details Successfully Updated", "Address Update Status", JOptionPane.INFORMATION_MESSAGE);

        } else {
            JOptionPane.showMessageDialog(null, "Address Update Failed", "Address Update Status", JOptionPane.ERROR_MESSAGE);

        }
        loadAddressDetails();
}//GEN-LAST:event_btn_AddressSubmitActionPerformed

    private void addressDetailsTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addressDetailsTableMouseReleased
        addressFlag = true;
        //        btn_AddressSubmit.setVisible(false);
        //        btn_AddressUpdate.setVisible(true);
        //        pnl_AddressNewDate.setVisible(true);
        //        dcb_NewAddressDate.setDate(null);
        int selectedRow = addressDetailsTable.getSelectedRow();
        System.out.println("selectedRow = " + selectedRow);
        int selectedAddressId = new Integer(addressDetailsTable.getValueAt(selectedRow, 0).toString());
        System.out.println("selectedAddressId in grid = " + selectedAddressId);
        displayAddressDetails(selectedAddressId);
}//GEN-LAST:event_addressDetailsTableMouseReleased

    private void cb_MethodItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cb_MethodItemStateChanged

        String selectedContact = cb_Method.getSelectedItem().toString();
        int commTypeId = commMethodTypeMap.get(cb_Method.getSelectedItem());
        ArrayList strArr = new ArrayList();

        if (selectedContact.equals("Mail")) {
            mailFlag = true;
            cb_ContactMethodDetails.setVisible(true);
            txt_ContactMethodDetails.setVisible(false);

            List<AddressDetails> mailDetails = NeoWebService.getNeoManagerBeanService().getMailDetailsForEntity(entityId);
            for (AddressDetails addressDetails : mailDetails) {
                String addressLine = "";
                String addLine1 = addressDetails.getAddressLine1();
                String addLine2 = addressDetails.getAddressLine2();
                String addLine3 = addressDetails.getAddressLine3();
                addressLine = addLine1 + " " + addLine2 + " " + addLine3;

                System.out.println("Entity Mail Details = " + addressLine);
                strArr.add(addressLine);
            }
            cb_ContactMethodDetails.setModel(new javax.swing.DefaultComboBoxModel(strArr.toArray()));
        } else {
            cb_ContactMethodDetails.setVisible(false);
            txt_ContactMethodDetails.setVisible(true);
            txt_ContactMethodDetails.setText("");

        }
}//GEN-LAST:event_cb_MethodItemStateChanged

    private void btn_ContactClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ContactClearActionPerformed
        txt_ContactMethodDetails.setText("");
        contactFlag = false;
    }//GEN-LAST:event_btn_ContactClearActionPerformed

    private void btn_ContactSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ContactSubmitActionPerformed
        ContactDetails cDetails = new ContactDetails();

        if (contactFlag) {
            cDetails.setEntityCommonId(contactCommonId);
        }

        String commMethodId = "" + cb_Method.getSelectedItem();
        int commId = 0;
        if (commMethodId != null && !commMethodId.trim().equals("")) {
            commId = commMethodTypeMap.get(commMethodId);
            System.out.println("commID = " + commId);
        }
        cDetails.setCommunicationMethodId(commId);
        if (mailFlag == true) {
            cDetails.setMethodDetails(cb_ContactMethodDetails.getSelectedItem().toString());
        } else {
            cDetails.setMethodDetails(txt_ContactMethodDetails.getText());
        }
        boolean contactUpdated = NeoWebService.getNeoManagerBeanService().saveAndUpdateContactDetails(entityId, LoginPage.neoUser.getUserId(), cDetails);
        if (contactUpdated) {
            JOptionPane.showMessageDialog(null, "Contact Details Successfully Saved", "Contact Save Status", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Contact Detail Save Failed", "Contact Save Status", JOptionPane.ERROR_MESSAGE);
        }
        loadContactDetails();
}//GEN-LAST:event_btn_ContactSubmitActionPerformed

    private void table_ContactDetailsMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_ContactDetailsMouseReleased

        //contactFlag = false;
        contactFlag = true;
        //btn_ContactSubmit.setVisible(false);
        //btn_ContactClear.setVisible(true);
        //        pnl_ContactNewDate.setVisible(true);
        //        dcb_NewContactDate.setDate(null);
        if (evt.getClickCount() == 1) {
            System.out.println("click");
            int selectedRow = table_ContactDetails.getSelectedRow();
            int contactDetailsId = Integer.parseInt(table_ContactDetails.getValueAt(selectedRow, 1).toString());
            String conMethod = table_ContactDetails.getValueAt(selectedRow, 2).toString();
            String conMethDetail = table_ContactDetails.getValueAt(selectedRow, 3).toString();
            System.out.println("selectedRow in contact table = " + selectedRow);
            System.out.println("selected detailsId in contact table = " + contactDetailsId);
            System.out.println("selected contact method in contact table = " + conMethod);
            displayContactDetails(contactDetailsId, conMethod, conMethDetail);
        }
}//GEN-LAST:event_table_ContactDetailsMouseReleased

    private void btn_ContactPrefClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ContactPrefClearActionPerformed
        methodDetailTxt.setText("");
        contactPersonTxt.setText("");
        conPrefFlag = false;
}//GEN-LAST:event_btn_ContactPrefClearActionPerformed

    private void btn_ContactPrefSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ContactPrefSubmitActionPerformed
        ContactPreference cPref = new ContactPreference();

        int commTypeId = communicationTypeMap.get(cb_CommunicationType.getSelectedItem());
        int fileLayoutId = fileLayoutMap.get(cb_FileLayout.getSelectedItem());
        int suppMailId = 0;
        String suppressMail = cb_SuppressMail.getSelectedItem().toString();
        System.out.println("conPrefFlag " + conPrefFlag);
        /*if (conPrefFlag) {
            cPref.setEntityCommonId(conPrefCommonId);

            //when contact details are updated
            for (ContactDetails con : contactList) {
                if (con.getContactDetailsId() == conPrefContactDetailId) {
                    ContactDetails cDetails = new ContactDetails();
                    System.out.println("con.getMethodDetails() in update = " + con.getMethodDetails());
                    System.out.println("methodDetailTxt.getText() in update = " + methodDetailTxt.getText());

                    if (!con.getMethodDetails().equalsIgnoreCase(methodDetailTxt.getText())) {
                        cDetails = con;
                        cDetails.setCommunicationMethodId(con.getCommunicationMethodId());
                        cDetails.setMethodDetails(methodDetailTxt.getText());
                        cDetails.setEntityCommonId(con.getEntityCommonId());

                        boolean contactUpdated = NeoWebService.getNeoManagerBeanService().saveAndUpdateContactDetails(entityId, LoginPage.neoUser.getUserId(), cDetails);
                        System.out.println("contactUpdated = " + contactUpdated);
                        if (contactUpdated) {
                            System.out.println("Contact Details Successfully Saved");
                            //get new common for updated contact detail
                            String methodText = methodTxt.getSelectedItem().toString();
                            int methodId = prefMethodTypeMap.get(methodText);
                            ContactDetails contactDetails = NeoWebService.getNeoManagerBeanService().getContactDetails(entityId, methodId);
                            conPrefContactDetailId = contactDetails.getContactDetailsId();
                            System.out.println("new conPrefContactDetailId = " + conPrefContactDetailId);
                        } else {
                            System.out.println("Contact Detail Save Failed");
                        }
                        break;
                    }
                }
            }
        }*/

        XMLGregorianCalendar suppMailDateFrom = null;
        XMLGregorianCalendar suppMailDateTo = null;

        Date dFrom = null;
        Date dTo = null;

        if (suppressMail.equals("Yes")) {
            suppMailId = 1;

            dFrom = contactSuppressMailFrom.getDate();
            dTo = contactSuppressMailTo.getDate();

            if (dFrom == null) {
                dFrom = suppFromDate;
            }
            if (dTo == null) {
                dTo = suppToDate;
            }

            suppMailDateFrom = UtilMethods.convertDateToXMLGeoCalender(dFrom);
            suppMailDateTo = UtilMethods.convertDateToXMLGeoCalender(dTo);

        } else {
            suppMailId = 0;
        }
        
        cPref.setEntityCommonId(conPrefCommonId);
        cPref.setSuppressMailDateFrom(suppMailDateFrom);
        cPref.setSuppressMailDateTo(suppMailDateTo);

        cPref.setContactDetailsId(conPrefContactDetailId);

        
        cPref.setCommunicationTypeId(commTypeId);
        cPref.setContactPerson(contactPersonTxt.getText());
        cPref.setFileLayoutId(fileLayoutId);
        cPref.setSuppressMailId(suppMailId);

        String detailType = saveDetails.get(methodTxt.getSelectedItem());
        String method = methodTxt.getSelectedItem().toString();
        if (detailType.equalsIgnoreCase("A")){
            cPref.setDetailType(detailType);
            if (method.equalsIgnoreCase("Physical")){
                cPref.setContactAddressType(1);
            }else{
                cPref.setContactAddressType(2);
            }
        }
        else if(detailType.equalsIgnoreCase("C")) {
            cPref.setDetailType(detailType);
            if (method.equalsIgnoreCase("E-mail")){
                cPref.setContactDetailType(1);
            }
            if (method.equalsIgnoreCase("Fax")){
                cPref.setContactDetailType(2);
            }
            if (method.equalsIgnoreCase("Cell")){
                cPref.setContactDetailType(3);
            }
            if (method.equalsIgnoreCase("Tel (work)")){
                cPref.setContactDetailType(4);
            }
            if (method.equalsIgnoreCase("Tel (home)")){
                cPref.setContactDetailType(5);
            }
            
        }
        
        boolean conPrefSaved = NeoWebService.getNeoManagerBeanService().saveAndUpdateContactPreferences(entityId, LoginPage.neoUser.getUserId(), cPref);

        if (conPrefSaved) {
            JOptionPane.showMessageDialog(null, "Contact Preference Successfully Saved", "Contact Preference Save Status", JOptionPane.INFORMATION_MESSAGE);

        } else {
            JOptionPane.showMessageDialog(null, "Contact Preference Save Failed", "Contact Preference Save Status", JOptionPane.ERROR_MESSAGE);

        }

        loadContactPreferences();
}//GEN-LAST:event_btn_ContactPrefSubmitActionPerformed

    private void table_ContactPrefferenceDetailsMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_ContactPrefferenceDetailsMouseReleased
        conPrefFlag = true;
        //btn_ContactPrefSubmit.setVisible(false);
        //btn_ContactPrefClear.setVisible(true);
        //        pnl_ContactPrefNewDate.setVisible(true);
        //methodDetailTxt.setVisible(true);
        //cb_MethodDetails.setVisible(false);

        if (evt.getClickCount() == 1) {
            System.out.println("click");
            int selectedRow = table_ContactPrefferenceDetails.getSelectedRow();
            /*String commType = table_ContactPrefferenceDetails.getValueAt(selectedRow, 1).toString();
            String conMethod = table_ContactPrefferenceDetails.getValueAt(selectedRow, 2).toString();
            String conMethDetail = table_ContactPrefferenceDetails.getValueAt(selectedRow, 3).toString();
            String suppressMail = table_ContactPrefferenceDetails.getValueAt(selectedRow, 5).toString();
            System.out.println("selectedRow in contact table = " + selectedRow);
            System.out.println("selected contact method in contact table = " + conMethod);*/
            displayContactPreferences(selectedRow);
        }
    }//GEN-LAST:event_table_ContactPrefferenceDetailsMouseReleased

    private void contactPersonTxtMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_contactPersonTxtMouseReleased
       
            //btn_ContactPrefSubmit.setVisible(true);
            //btn_ContactPrefClear.setVisible(false);
            //pnl_ContactPrefNewDate.setVisible(false);
            contactPersonTxt.setText("");
            cb_CommunicationType.setSelectedItem("");
            methodTxt.setSelectedItem("");

            methodDetailTxt.setText("");
            //methodDetailTxt.setVisible(false);
            //cb_MethodDetails.setVisible(true);

            cb_SuppressMail.setSelectedItem("");
            cb_FileLayout.setSelectedItem("");
            contactSuppressMailFrom.setDate(null);
            contactSuppressMailTo.setDate(null);
        
}//GEN-LAST:event_contactPersonTxtMouseReleased

    private void methodTxtItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_methodTxtItemStateChanged
        ArrayList strArr = new ArrayList();
        
        methodDetailTxt.setText(allDetails.get(methodTxt.getSelectedItem().toString()));
        try {
            int methodId = 0;
            String check = methodTxt.getSelectedItem().toString();

            if (!conPrefFlag) {
                //btn_ContactPrefSubmit.setVisible(true);
                //methodDetailTxt.setVisible(false);
                //cb_MethodDetails.setVisible(true);

            } else {
                //btn_ContactPrefSubmit.setVisible(false);
                //methodDetailTxt.setVisible(true);
                //cb_MethodDetails.setVisible(false);
            }

            if (prefHashSet.size() != 0) {
                if (check.equals("")) {
                    methodId = prefMethodTypeMap.get(prefHashSet.iterator().next());

                } else {
                    methodId = prefMethodTypeMap.get(methodTxt.getSelectedItem());

                }
            }
            System.out.println("Method Id = " + methodId);

            if (methodId != 0) {

                ContactDetails contactDetails = NeoWebService.getNeoManagerBeanService().getContactDetails(entityId, methodId);
                strArr.add(contactDetails.getMethodDetails());
                conPrefContactDetailId = contactDetails.getContactDetailsId();
                System.out.println("conPrefContactDetailId in item changed = " + conPrefContactDetailId);

                /*
                List<ContactDetails> cDetailList = NeoWebService.getNeoManagerBeanService().getContactMethodDetailsForEntity(entityId, methodId);
                for (ContactDetails contactDetails : cDetailList) {
                strArr.add(contactDetails.getMethodDetails());
                conPrefContactDetailId = contactDetails.getContactDetailsId();
                System.out.println("conPrefContactDetailId in item changed = " + conPrefContactDetailId);
                }
                 */

                //cb_MethodDetails.setModel(new javax.swing.DefaultComboBoxModel(strArr.toArray()));
                //cb_MethodDetails.setSelectedItem("");
            }

        } catch (NullPointerException nEx) {
            System.out.println("null point exception occured");
        }
}//GEN-LAST:event_methodTxtItemStateChanged

    private void cb_SuppressMailItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cb_SuppressMailItemStateChanged
        String suppressMail = cb_SuppressMail.getSelectedItem().toString();
        if (suppressMail.equals("Yes")) {

            Date fromDate = new Date();
            Date toDate = null;
            String toDateStr = "2999/12/31";

            try {
                toDate = sdf.parse(toDateStr);
            } catch (ParseException ex) {
                System.out.println("error - " + ex.getMessage());
            }
            suppFromDate = fromDate;
            suppToDate = toDate;

            contactSuppressMailFrom.setDate(fromDate);
            contactSuppressMailTo.setDate(toDate);

            contactSuppressMailFrom.setEnabled(true);
            contactSuppressMailTo.setEnabled(true);

        } else {
            contactSuppressMailFrom.setDate(null);
            contactSuppressMailTo.setDate(null);
            contactSuppressMailFrom.setEnabled(false);
            contactSuppressMailTo.setEnabled(false);
        }
    }//GEN-LAST:event_cb_SuppressMailItemStateChanged

    private void txt_BankCoverNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_BankCoverNumberActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_txt_BankCoverNumberActionPerformed

    private void cb_PaymentMethodItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cb_PaymentMethodItemStateChanged
        cb_BankName.setSelectedItem("");
        if (bankFlag == true) {
            //btn_BankingSubmit.setVisible(false);
            //btn_BankingClear.setVisible(true);
            lbl_DateFrom.setVisible(false);
            lbl_DateTo.setVisible(false);
            dcb_EffectiveFrom.setVisible(false);
            dcb_EffectiveTo.setVisible(false);
        }
        cb_BankName.setSelectedItem("");
        cb_BranchName.setSelectedItem("");
        cb_BranchCode.setSelectedItem("");
        cb_AccountType.setSelectedItem("");
        txt_AccountName.setText("");
        txt_AccountNumber.setText("");
        dcb_EffectiveFrom.setDate(null);
        dcb_EffectiveTo.setDate(null);

}//GEN-LAST:event_cb_PaymentMethodItemStateChanged

    private void cb_BankNameItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cb_BankNameItemStateChanged
        int bankID = bankMap.get(cb_BankName.getSelectedItem());
        ArrayList<String[]> returned = searchBranch(bankID);
        String[] branchNames = returned.get(0);
        String[] branchCodes = returned.get(1);

        System.out.println("names returned size - " + branchNames.length);
        System.out.println("codes returned size - " + branchCodes.length);

        if (branchNames.length != 0) {
            Arrays.sort(branchNames, String.CASE_INSENSITIVE_ORDER);
            Arrays.sort(branchCodes, String.CASE_INSENSITIVE_ORDER);

        }

        cb_BranchName.setModel(new javax.swing.DefaultComboBoxModel(branchNames));
        cb_BranchCode.setModel(new javax.swing.DefaultComboBoxModel(branchCodes));

    }//GEN-LAST:event_cb_BankNameItemStateChanged

    private void cb_AccountTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cb_AccountTypeItemStateChanged
        String test = accountTypeMap.get(cb_AccountType.getSelectedItem());
        System.out.println("SELECTED ID VALUE = " + test);
}//GEN-LAST:event_cb_AccountTypeItemStateChanged
    private void cb_BranchNameItemStateChanged(java.awt.event.ItemEvent evt) {
        String branchCode = bankBranchMap.get(cb_BranchName.getSelectedItem());
        cb_BranchCode.setSelectedItem(branchCode);
        //String[] branchCodeStr = {branchCode};
        //cb_BranchCode.setModel(new javax.swing.DefaultComboBoxModel(branchCodeStr));

    }

    private void cb_BranchCodeItemStateChanged(java.awt.event.ItemEvent evt) {
        @SuppressWarnings("element-type-mismatch")
        String branchName = bankBranchCodeMap.get(cb_BranchCode.getSelectedItem());
        cb_BranchName.setSelectedItem(branchName);
    }
    private java.awt.event.ItemListener branchCodeListener = new java.awt.event.ItemListener() {

        public void itemStateChanged(java.awt.event.ItemEvent evt) {
            cb_BranchCodeItemStateChanged(evt);
        }
    };
    private java.awt.event.ItemListener branchNameListener = new java.awt.event.ItemListener() {

        public void itemStateChanged(java.awt.event.ItemEvent evt) {
            cb_BranchNameItemStateChanged(evt);
        }
    };
    private void rb_branchNameItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rb_branchNameItemStateChanged
        // TODO add your handling code here:
        boolean selected = branchGroup.isSelected(rb_branchName.getModel());
        if (selected) {
            //set item listeners to branch name and code
            cb_BranchName.setEnabled(true);
            cb_BranchCode.setEnabled(false);
            cb_BranchName.addItemListener(branchNameListener);
            cb_BranchCode.removeItemListener(branchCodeListener);
        } else {
            cb_BranchName.removeItemListener(branchNameListener);
            cb_BranchCode.addItemListener(branchCodeListener);
            cb_BranchCode.setEnabled(true);
            cb_BranchName.setEnabled(false);
        }
    }//GEN-LAST:event_rb_branchNameItemStateChanged

    private void btn_BankingSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_BankingSubmitActionPerformed
        saveBankingDetails();
}//GEN-LAST:event_btn_BankingSubmitActionPerformed

    private void btn_BankingClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_BankingClearActionPerformed
        bankFlag = false;
        //clear fields
        txt_AccountName.setText("");
        txt_AccountNumber.setText("");
}//GEN-LAST:event_btn_BankingClearActionPerformed

    private void bankingTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bankingTableMouseReleased
        bankFlag = true;
        int selectedRow = bankingTable.getSelectedRow();
        System.out.println("selectedRow = " + selectedRow);
        displayBankingDetails(selectedRow);
}//GEN-LAST:event_bankingTableMouseReleased
    //catch (NeoSecurityException_Exception ex) {
    //    Logger.getLogger(ViewCoverPersonAllDetails.class.getName()).log(Level.SEVERE, null, ex);
    //}

    protected XMLGregorianCalendar convert(Date date) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getDatatypeFactory().newXMLGregorianCalendar(calendar);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static com.jidesoft.grid.JideTable addressDetailsTable;
    public static javax.swing.JTextField ageTxt;
    public static com.jidesoft.grid.JideTable bankingTable;
    public static javax.swing.JPanel bodyPanel;
    private javax.swing.ButtonGroup branchGroup;
    private com.jidesoft.swing.JideButton btnSearch;
    public static com.jidesoft.swing.JideButton btn_AddressClear;
    public static com.jidesoft.swing.JideButton btn_AddressSubmit;
    public static com.jidesoft.swing.JideButton btn_BankingClear;
    public static com.jidesoft.swing.JideButton btn_BankingSubmit;
    public static com.jidesoft.swing.JideButton btn_ContactClear;
    public static com.jidesoft.swing.JideButton btn_ContactPrefClear;
    public static com.jidesoft.swing.JideButton btn_ContactPrefSubmit;
    public static com.jidesoft.swing.JideButton btn_ContactSubmit;
    public static com.jidesoft.swing.JideButton btn_DepStatusSave;
    public static com.jidesoft.swing.JideButton btn_DepUnderwriting;
    private com.jidesoft.swing.JideButton btn_InsuredHistory;
    private com.jidesoft.swing.JideButton btn_OptionAllocate;
    private com.jidesoft.swing.JideButton btn_PreInsured;
    private com.jidesoft.swing.JideButton btn_Underwriting;
    public static javax.swing.JComboBox cb_AccountType;
    public static com.jidesoft.swing.AutoCompletionComboBox cb_AddressType;
    public static javax.swing.JComboBox cb_BankName;
    public static javax.swing.JComboBox cb_BranchCode;
    public static javax.swing.JComboBox cb_BranchName;
    public static com.jidesoft.swing.AutoCompletionComboBox cb_CommunicationType;
    public static javax.swing.JComboBox cb_ContactMethodDetails;
    public static com.jidesoft.swing.AutoCompletionComboBox cb_DepStatusReason;
    public static com.jidesoft.swing.AutoCompletionComboBox cb_FileLayout;
    public static com.jidesoft.swing.AutoCompletionComboBox cb_Method;
    public static javax.swing.JComboBox cb_PaymentMethod;
    public static javax.swing.JComboBox cb_Paypoint;
    public static com.jidesoft.swing.AutoCompletionComboBox cb_StatusReason;
    public static com.jidesoft.swing.AutoCompletionComboBox cb_SuppressMail;
    public static javax.swing.JComboBox cb_ViewDependentNumber;
    public static javax.swing.JTextField contactPersonTxt;
    public static com.jidesoft.combobox.DateComboBox contactSuppressMailFrom;
    public static com.jidesoft.combobox.DateComboBox contactSuppressMailTo;
    public static com.jidesoft.combobox.DateComboBox coverDateFrom;
    public static com.jidesoft.combobox.DateComboBox coverDateTo;
    public static javax.swing.JTextField coverNameContactPrefTxt;
    public static javax.swing.JTextField coverNumberContactPrefTxt;
    public static javax.swing.JTextField coverNumberContactTxt;
    public static javax.swing.JTextField coverNumberTxt;
    public static javax.swing.JTextField coverNumberTxt3;
    private com.jidesoft.swing.JideButton coverStatusSave;
    public static com.jidesoft.combobox.DateComboBox dcb_AppStartDate;
    public static com.jidesoft.combobox.DateComboBox dcb_DOB;
    public static com.jidesoft.combobox.DateComboBox dcb_DateOfBirth;
    public static com.jidesoft.combobox.DateComboBox dcb_DepCoverEnd;
    public static com.jidesoft.combobox.DateComboBox dcb_DepCoverStart;
    public static com.jidesoft.combobox.DateComboBox dcb_EffectiveFrom;
    public static com.jidesoft.combobox.DateComboBox dcb_EffectiveTo;
    public static com.jidesoft.combobox.DateComboBox dcb_RegDate;
    public static javax.swing.JTextField dependentCodeTxt;
    public static javax.swing.JTextField dependentType;
    public static javax.swing.JTextField employerTxt;
    public static javax.swing.JTextField genderTxt;
    public static javax.swing.JTextField homeLanguageTxt;
    public static javax.swing.JTextField idNumberTxt;
    public static javax.swing.JTextField incomeTxt;
    public static javax.swing.JTextField initialsTxt;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel29;
    private javax.swing.JPanel jPanel30;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private com.jidesoft.swing.JideButton jideButton5;
    private com.jidesoft.swing.JideButton jideButton6;
    private com.jidesoft.swing.JideButton jideButton8;
    private com.jidesoft.swing.JideButton jideButton9;
    public static javax.swing.JLabel lbl_DateFrom;
    public static javax.swing.JLabel lbl_DateTo;
    public static com.jidesoft.swing.StyledLabel lbl_Number;
    public static com.jidesoft.swing.StyledLabel lbl_Number1;
    public static com.jidesoft.swing.StyledLabel lbl_Number4;
    public static javax.swing.JLabel lbl_Number5;
    public static javax.swing.JTextField maritalStatusTxt;
    public static javax.swing.JTextField methodDetailTxt;
    public static com.jidesoft.swing.AutoCompletionComboBox methodTxt;
    public static javax.swing.JTextField nameTxt;
    public static javax.swing.JTextField optionTxt;
    public static javax.swing.JTextField payRollTxt;
    public static javax.swing.JPanel pnl_ContactMethodDetails;
    private javax.swing.JPanel pnl_ContactPreferences;
    public static javax.swing.JPanel pnl_MethodDetails;
    private javax.swing.JPanel pnl_Unused;
    private javax.swing.JPanel pnl_ViewAddressDetails;
    private javax.swing.JPanel pnl_ViewBankingDetails;
    private javax.swing.JPanel pnl_ViewContactDetails;
    public static javax.swing.JPanel pnl_ViewDependents;
    public static javax.swing.JPanel pnl_coverDetailsPane;
    public static javax.swing.JPanel pnl_viewCoverPerson;
    public static javax.swing.JTextField policyNameTxt;
    public static javax.swing.JTextField previousCoverNumberTxt;
    public static javax.swing.JRadioButton rb_branchCode;
    public static javax.swing.JRadioButton rb_branchName;
    private com.jidesoft.swing.StyledLabel styledLabel10;
    private com.jidesoft.swing.StyledLabel styledLabel11;
    private com.jidesoft.swing.StyledLabel styledLabel12;
    private com.jidesoft.swing.StyledLabel styledLabel13;
    private com.jidesoft.swing.StyledLabel styledLabel14;
    private com.jidesoft.swing.StyledLabel styledLabel15;
    private com.jidesoft.swing.StyledLabel styledLabel16;
    private com.jidesoft.swing.StyledLabel styledLabel17;
    private com.jidesoft.swing.StyledLabel styledLabel18;
    private com.jidesoft.swing.StyledLabel styledLabel19;
    private com.jidesoft.swing.StyledLabel styledLabel2;
    private com.jidesoft.swing.StyledLabel styledLabel20;
    private com.jidesoft.swing.StyledLabel styledLabel21;
    private com.jidesoft.swing.StyledLabel styledLabel22;
    private com.jidesoft.swing.StyledLabel styledLabel23;
    private com.jidesoft.swing.StyledLabel styledLabel24;
    private com.jidesoft.swing.StyledLabel styledLabel25;
    private com.jidesoft.swing.StyledLabel styledLabel26;
    private com.jidesoft.swing.StyledLabel styledLabel27;
    private com.jidesoft.swing.StyledLabel styledLabel28;
    private com.jidesoft.swing.StyledLabel styledLabel29;
    private com.jidesoft.swing.StyledLabel styledLabel30;
    private com.jidesoft.swing.StyledLabel styledLabel31;
    private com.jidesoft.swing.StyledLabel styledLabel32;
    private com.jidesoft.swing.StyledLabel styledLabel33;
    private com.jidesoft.swing.StyledLabel styledLabel34;
    private com.jidesoft.swing.StyledLabel styledLabel4;
    private com.jidesoft.swing.StyledLabel styledLabel40;
    private com.jidesoft.swing.StyledLabel styledLabel41;
    private com.jidesoft.swing.StyledLabel styledLabel42;
    private com.jidesoft.swing.StyledLabel styledLabel43;
    private com.jidesoft.swing.StyledLabel styledLabel44;
    private com.jidesoft.swing.StyledLabel styledLabel45;
    private com.jidesoft.swing.StyledLabel styledLabel46;
    private com.jidesoft.swing.StyledLabel styledLabel47;
    private com.jidesoft.swing.StyledLabel styledLabel48;
    private com.jidesoft.swing.StyledLabel styledLabel49;
    private com.jidesoft.swing.StyledLabel styledLabel51;
    private com.jidesoft.swing.StyledLabel styledLabel52;
    private com.jidesoft.swing.StyledLabel styledLabel53;
    private com.jidesoft.swing.StyledLabel styledLabel54;
    private com.jidesoft.swing.StyledLabel styledLabel55;
    private com.jidesoft.swing.StyledLabel styledLabel56;
    private com.jidesoft.swing.StyledLabel styledLabel57;
    private com.jidesoft.swing.StyledLabel styledLabel58;
    private com.jidesoft.swing.StyledLabel styledLabel59;
    private com.jidesoft.swing.StyledLabel styledLabel6;
    private com.jidesoft.swing.StyledLabel styledLabel60;
    private com.jidesoft.swing.StyledLabel styledLabel61;
    private com.jidesoft.swing.StyledLabel styledLabel62;
    private com.jidesoft.swing.StyledLabel styledLabel63;
    private com.jidesoft.swing.StyledLabel styledLabel64;
    private com.jidesoft.swing.StyledLabel styledLabel65;
    private com.jidesoft.swing.StyledLabel styledLabel66;
    private com.jidesoft.swing.StyledLabel styledLabel67;
    private com.jidesoft.swing.StyledLabel styledLabel68;
    private com.jidesoft.swing.StyledLabel styledLabel69;
    private com.jidesoft.swing.StyledLabel styledLabel70;
    private com.jidesoft.swing.StyledLabel styledLabel71;
    private com.jidesoft.swing.StyledLabel styledLabel72;
    private com.jidesoft.swing.StyledLabel styledLabel8;
    private com.jidesoft.swing.StyledLabel styledLabel83;
    private com.jidesoft.swing.StyledLabel styledLabel84;
    private com.jidesoft.swing.StyledLabel styledLabel87;
    private com.jidesoft.swing.StyledLabel styledLabel88;
    private com.jidesoft.swing.StyledLabel styledLabel9;
    private com.jidesoft.swing.StyledLabel styledLabel93;
    private com.jidesoft.swing.StyledLabel styledLabel94;
    public static javax.swing.JTextField surnameTxt;
    public static com.jidesoft.grid.JideTable table_ContactDetails;
    public static com.jidesoft.grid.JideTable table_ContactPrefferenceDetails;
    public static javax.swing.JTextField titleTxt;
    public static javax.swing.JTabbedPane tpane_CoverPersonDetails;
    public static javax.swing.JTextField txt_AccountHolder;
    public static javax.swing.JTextField txt_AccountName;
    public static javax.swing.JTextField txt_AccountNumber;
    public static javax.swing.JTextField txt_AddressCoverName;
    public static javax.swing.JTextField txt_AddressCoverNumber;
    public static javax.swing.JTextField txt_AddressLine1;
    public static javax.swing.JTextField txt_AddressLine2;
    public static javax.swing.JTextField txt_AddressLine3;
    public static javax.swing.JTextField txt_BankCoverNumber;
    public static javax.swing.JTextField txt_ContactMethodDetails;
    public static javax.swing.JTextField txt_DepAge;
    public static javax.swing.JTextField txt_DepGender;
    public static javax.swing.JTextField txt_DepHomeLan;
    public static javax.swing.JTextField txt_DepIdNumber;
    public static javax.swing.JTextField txt_DepIdType;
    public static javax.swing.JTextField txt_DepIncome;
    public static javax.swing.JTextField txt_DepInitials;
    public static javax.swing.JTextField txt_DepMarital;
    public static javax.swing.JTextField txt_DepName;
    public static javax.swing.JTextField txt_DepSurname;
    public static javax.swing.JTextField txt_DepTitle;
    public static javax.swing.JTextField txt_PostalCode;
    public static javax.swing.JTextField txt_dependentType;
    public static javax.swing.JTextField txt_memDepStatus;
    public static javax.swing.JTextField txt_memMainStatus;
    // End of variables declaration//GEN-END:variables
    private DatatypeFactory _datatypeFactory;

    public String[] searchCountry() {

        List<Country> countryList = NeoWebService.getNeoManagerBeanService().getCountryTable();
        int i = 0;

        String[] conArrList = new String[countryList.size()];
        for (Country country : countryList) {
            conArrList[i] = country.getName();
            countryMap.put(conArrList[i], country.getId());
            i++;

        }



        return conArrList;


    }

    public String[] searchRegion(int countryID) {


        List<Region> regionList = NeoWebService.getNeoManagerBeanService().getRegionTable(countryID);
        int i = 0;

        String[] regionArrList = new String[regionList.size()];
        for (Region region : regionList) {
            regionArrList[i] = region.getName();
            regionMap.put(regionArrList[i], region.getId());
            i++;

        }



        return regionArrList;

    }

    public String[] searchTown(int regionID) {

        List<Town> townList = NeoWebService.getNeoManagerBeanService().getTownTable(regionID);
        int i = 0;

        String[] townArrList = new String[townList.size()];
        for (Town town : townList) {
            townArrList[i] = town.getName();
            townMap.put(townArrList[i], town.getId());
            i++;

        }



        return townArrList;

    }

    public String[] searchSuburb(int townID) {

        List<Suburb> suburbList = NeoWebService.getNeoManagerBeanService().getSuburbTable(townID);
        int i = 0;

        String[] suburbArrList = new String[suburbList.size()];
        for (Suburb suburb : suburbList) {
            suburbArrList[i] = suburb.getName();
            suburbMap.put(suburbArrList[i], suburb.getId());
            i++;

        }



        return suburbArrList;

    }

    public String[] searchRoad(int subID) {

        List<Road> roadList = NeoWebService.getNeoManagerBeanService().getRoadTable(subID);
        int i = 0;

        String[] roadArrList = new String[roadList.size()];
        for (Road road : roadList) {
            roadArrList[i] = road.getName();
            roadMap.put(roadArrList[i], road.getId());
            i++;

        }



        return roadArrList;

    }
//Search All Banks

    public String[] searchAllBanks() {
        String[] conArrList = null;
        try {
            List<Bank> bankList = NeoWebService.getNeoManagerBeanService().getBankTable(LoginPage.neoUser);
            int i = 0;
            conArrList = new String[bankList.size()];
            for (Bank bank : bankList) {
                conArrList[i] = bank.getName();
                System.out.println(bank.getName() + "----------bank.getName()");
                bankMap.put(conArrList[i], bank.getId());
                i++;
            }
            //sort banks
            Arrays.sort(conArrList, String.CASE_INSENSITIVE_ORDER);

            return conArrList;
        } catch (NeoSecurityException_Exception ex) {
            System.out.println("Exception Caught..." + ex.getMessage());
            ex.printStackTrace();
            NeoMessageDialog.showErrorMessage(ex.getMessage());

        }
        return conArrList;

    }

    public ArrayList<String[]> searchBranch(int bankID) {
        ArrayList<String[]> returnList = new ArrayList<String[]>();
        try {
            List<Branch> branchList = NeoWebService.getNeoManagerBeanService().getBranchTable(LoginPage.neoUser, bankID);
            int i = 0;

            String[] branchCodes = new String[branchList.size()];
            String[] roadArrList = new String[branchList.size()];
            for (Branch branch : branchList) {
                roadArrList[i] = branch.getName();
                branchCodes[i] = branch.getCode();
                bankBranchMap.put(branch.getName(), branch.getCode());
                bankBranchCodeMap.put(branch.getCode(), branch.getName());
                bankBranchIdMap.put(branch.getName(), branch.getId());

                i++;
            }
            returnList.add(roadArrList);
            returnList.add(branchCodes);

            return returnList;

        } catch (NeoSecurityException_Exception ex) {
            Logger.getLogger(ViewCoverPersonAllDetails.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                Logger.getLogger(CreateBroker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return _datatypeFactory;
    }

    public Object[] getLookupValues(String LookupType) {
        try {
            List<LookupValue> CommTypelist = NeoWebService.getNeoManagerBeanService().getCodeTableByName(LoginPage.neoUser, LookupType);

            ArrayList lookupList = new ArrayList();

            for (Iterator<LookupValue> it = CommTypelist.iterator(); it.hasNext();) {
                LookupValue type = it.next();
                lookupList.add(type.getValue());

            }

            return lookupList.toArray();
        } catch (Exception ex) {
            System.out.println("Exception Caught..." + ex.getMessage());
            ex.printStackTrace();
            NeoMessageDialog.showErrorMessage(ex.getMessage());
            return null;
        }
    }
}

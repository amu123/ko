package za.co.AgilityTechnologies.dragandDrop;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.TransferHandler;
import javax.swing.table.TableModel;

import com.jidesoft.grid.SortableTreeTableModel;
import com.jidesoft.grid.TreeTable;
import com.jidesoft.swing.JidePopupMenu;
import java.util.HashMap;
import java.util.Map;
import neo.manager.NeoManagerBean;
import neo.manager.OptionVersion;
import neo.manager.PolicyDetails;
import neo.product.utils.NeoWebService;
import neo.product.utils.RegistrationUtil;
import neo.quotation.createQuotation.AssignOptionstoQuote;
import neo.quotation.createQuotation.CaptureIndividuals;

import za.co.AgilityTechnologies.DockingFramework.DataStore;

/**
 * This class provides the UI for the Tree task.
 * 
 * @author Administrator
 */
@SuppressWarnings("serial")
public class NeoTreeViewPanel extends JPanel implements ActionListener
{
	/**
	 * Member to hold the list of options
	 */
	protected JList _listOfOptions = null;
	
	/**
	 * Select Option button
	 */
	protected JButton _btnSelectOptions = null;
	
	/**
	 * Tree table instance
	 */
	protected TreeTable _treeTable = null;
	
	/**
	 * Pop up menu instance
	 */
	protected JidePopupMenu _popUpMenu = null;
	
	/**
	 * Delete menu item
	 */
	protected JMenuItem _deleteMenuItem = null;
	
	/**
	 * Model instance for Tree table
	 */
	protected NeoTreeTableModel _neoTreeTableModel = null;
	
	/**
	 * Controller instance of Tree table
	 */
	protected NeoTreeTableCtrl _neoTreeTableCtrl = null;
	
	
	/**
	 * Action command for Select Option button
	 */
	public static final String BTN_ACTION_COMMAND_SELECT_OPTION = "BTN_ACTION_COMMAND_SELECT_OPTION";
	
	/**
	 * Action command for Delete Menu Item
	 */
	public static final String MENU_ITEM_ACTION_COMMAND_DELETE_OPTION = "MENU_ITEM_ACTION_COMMAND_DELETE_OPTION";
	
	
	//Dummy Column Names
	public static final String TABLE_COLUMN_NAME_FIRST_NAME = "FirstName";
	public static final String TABLE_COLUMN_NAME_GROUPING = "Grouping";
    public static final String TABLE_COLUMN_ENTITY_ID = "EntityID";
	protected JTable _table = null;
	
	/**
	 * Constructor
	 */
	public NeoTreeViewPanel(String policynumber)
	{
		//Initialize the UI
		this.initialize(policynumber);
		
		//Create the controller
		_neoTreeTableCtrl = new NeoTreeTableCtrl(_neoTreeTableModel, this);
	}
    public void setTable(JTable table)
        {
            _table = table;
            _neoTreeTableModel.setOriginalRows(this.createTreeRows(_table, AssignOptionstoQuote.policyHolder.getText()));
            _treeTable.expandAll();
        }
	

	public List<String> getSelectedOptions()
	{
		if(_listOfOptions == null)
		{
			return null;
		}
		
		Object[] lstOfSelOptions = _listOfOptions.getSelectedValues();
		List<String> lstSelOptions = new ArrayList<String>();
		for(Object selVal : lstOfSelOptions)
		{
			String strValue = (String)selVal;
			lstSelOptions.add(strValue);
		}
		return lstSelOptions;
	}
	
	/**
	 * This method returns the options list to show in the view.
	 * 
	 * @return
	 */
    //List of OptionVersions
	protected ListModel createOptionListModel(int productID)
	{
		ListModel lstModel = new DefaultListModel();
		
		
		

        

        List<OptionVersion> optionsList=NeoWebService.getNeoManagerBeanService().findOptionVersionsForProduct(productID);
		
		 Map<String, Integer> optionVersionMap = new HashMap<String, Integer>();
       int totaloptions=optionsList.size();
       String optionName[]=new String[totaloptions];
       int i=0;
       for(OptionVersion option:optionsList){
           int optionversionID=option.getOptionVersionId();
          optionName[i] = optionversionID+"-"+option.getDescription();

            
            optionVersionMap.put(optionName[i],optionversionID);

       System.out.println(optionName[i]+"-----------optionName[i]");
			((DefaultListModel)lstModel).addElement(optionName[i]);
            i++;
		}
		return lstModel;
	}

    public void updateList(String[] theList) {
//        lstOfOptions.
    }
	
	/**
	 * Inner class for Drag and Drop on Tree table 
	 */
	class NeoTransferHandler extends TransferHandler
	{
		public NeoTransferHandler()
		{
			super();
		}
		
		@Override
		public boolean canImport(TransferSupport support) 
		{
			return true;
		}

		@Override
		public boolean importData(TransferSupport support) 
		{
			Transferable transferable = support.getTransferable();
			String data = null;
			try 
			{
				//Get the drop data on tree table in string format
				data = (String) transferable
						.getTransferData(DataFlavor.stringFlavor);
				
				//Delegate the Call back to Controller
				_neoTreeTableCtrl.onOptionsDrop(data);
				
				//Sort it, after every addition
				((SortableTreeTableModel) _treeTable.getModel()).sortColumn(0,true);
				
				//Clear the tree table selection
				_treeTable.clearSelection();
			} 
			catch (Exception e) 
			{
				return false;
			}
			return false;
		}
	}
	
	/**
	 * This method initializes the the UI
	 */
	protected void initialize(String pno)
	{
		//Set the layout
		this.setLayout(new BorderLayout());
		
		//Create the panel for tree table
		JPanel panelForTree = new JPanel(new BorderLayout());
		
		//Create the tree table model
		_neoTreeTableModel = new NeoTreeTableModel(this.createTreeRows(this.createDummyTable(), "MTN Group"));
		
		//Create the tree table instance
		_treeTable = new TreeTable(_neoTreeTableModel);
		_treeTable.setShowGrid(false);
		_treeTable.expandAll();
		
		_treeTable.addMouseListener(new MouseAdapter(){

			@Override
			public void mouseClicked(MouseEvent me) 
			{
				int b = me.getButton() ;
				
				//Check if the event is Right Click
				if(b == MouseEvent.BUTTON3)
				{
					int selRowIndices[] = _treeTable.getSelectedRows();
					
					//If no rows selected, then ignore
					if(selRowIndices == null)
					{
						super.mouseClicked(me);
						return;
					}
					
					//Flag to check if the "Options" row is selected
					boolean blnAllOptionsRow = true;
					
					//Loop through each selected row.
					for(int i=0; i < selRowIndices.length; i++)
					{
						int selRowIndex = selRowIndices[i];
						
						if(selRowIndex == -1)
							continue;
						
						NeoTreeTableRow selRow = (NeoTreeTableRow) _treeTable.getRowAt(selRowIndex);
						
						if(selRow == null)
						{
							continue;
						}
						
						/*Allow to delete, only if the all the selected rows are
						of type "Options"*/
						if(selRow.getNodeType() != NeoTreeTableRow.NODE_TYPE_OPTION)
						{
							blnAllOptionsRow = false;
						}
					}
					
					//Enable/Disable the Delete menu item
					if(blnAllOptionsRow)
					{
						_deleteMenuItem.setEnabled(true);
					}
					else
					{
						_deleteMenuItem.setEnabled(false);
					}
					
					//Now, show the pop up
					_popUpMenu.show(me.getComponent(), me.getX(), me.getY());
				}
				
				//Call the super
				super.mouseClicked(me);
			}
		});
		
		//Allow sorting
		_treeTable.setSortable(true);
		
		//Set the sorting preference on all the tree table levels
		if(_treeTable.getModel() instanceof SortableTreeTableModel)
		{
			((SortableTreeTableModel) _treeTable.getModel()).setDefaultSortableOption(SortableTreeTableModel.SORTABLE_ALL_LEVELS);
		}
		
		//Allow Drag and Drop
		_treeTable.setDragEnabled(true);
		
		// Set the transfer handler for drag and drop
		_treeTable.setTransferHandler(new NeoTransferHandler());
		
		//Add tree table to tree panel
		panelForTree.add(_treeTable,BorderLayout.CENTER);
		panelForTree.setMinimumSize(new Dimension(500,500));
		
		//Create right panel
		JPanel rightPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		//Create panel for options
		JPanel panelForOptions = new JPanel(new GridBagLayout());
		
		//Create Select Option button
		_btnSelectOptions = new JButton("Select Option");
		_btnSelectOptions.setActionCommand(BTN_ACTION_COMMAND_SELECT_OPTION);
		_btnSelectOptions.addActionListener(this);
		
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		
		//Add Select Options button to panel
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.insets = new Insets(5,5,5,5);
		gridBagConstraints.anchor = GridBagConstraints.NORTH;
		panelForOptions.add(_btnSelectOptions,gridBagConstraints);
		
		//Create List of Options

		

        String policyID=CaptureIndividuals.policyNumber.getText();
        System.out.println(policyID+"----------policyID");
        PolicyDetails pdetails=NeoWebService.getNeoManagerBeanService().getPolicy(policyID);
        System.out.println(pno+"----------policynumber");
//        System.out.println(pdetails.getProductId()+"---------pdetails.getProductId()");
		_listOfOptions = new JList(this.createOptionListModel(pdetails.getProductId()));
		_listOfOptions.setFixedCellWidth(200);
		_listOfOptions.setDragEnabled(true);
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.insets = new Insets(5,5,5,5);
		
		//Create Scroll pane for list of options
		JScrollPane scrollPane = new JScrollPane(_listOfOptions);
		
		//Add scroll pane to panel options
		panelForOptions.add(scrollPane,gridBagConstraints);
		
		//Add panel of options to right panel
		rightPanel.add(panelForOptions);
		
		//Create Split pane
		JSplitPane jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		
		//Add left component as Tree table
		jSplitPane.setLeftComponent(new JScrollPane(panelForTree));
		
		//Add right component as panel of options
		jSplitPane.setRightComponent(new JScrollPane(rightPanel));
		jSplitPane.setResizeWeight(0.25);
		
		//Now, add the split pane to the panel
		this.add(jSplitPane,BorderLayout.CENTER);
		
		//Sort the tree table in Ascending Order
		((SortableTreeTableModel) _treeTable.getModel()).sortColumn(0,true);
		
		//Create the pop up menu and add "Delete" menu item
		_popUpMenu = new JidePopupMenu();
		_deleteMenuItem = new JMenuItem("Delete");
		_deleteMenuItem.setActionCommand(MENU_ITEM_ACTION_COMMAND_DELETE_OPTION);
		_deleteMenuItem.addActionListener(this);
		_popUpMenu.add(_deleteMenuItem);
		
		//Update UI
		this.updateUI();
	}
	
	/**
	 * This is dummy method. For demo purpose. 
	 */
	private JTable createDummyTable()
	{
		String[] colNames = {"First Name","Surname", "Date Of Birth", "Gender", "Insured Relationship", "Grouping"};
		
		String[][] rowsData = { {"Smith","Albert","12-11-1969","Male","Child","Management"}, 
								{"Gibbs","Hary","13-11-1969","Male","Parent","Management"},
								{"Mohan","Raj","13-12-1977","Male","Parent","Staff"},
								{"Bharath","Kumar","11-02-1967","Male","Parent","Staff"}
		};
		
		JTable table = new JTable(rowsData,colNames);
		return table;
	}
	
	/**
	 * This method creates the tree table rows from the given JTable instance
	 * and returns the list of rows.
	 * 
	 * @param table
	 * @param strPolicy
	 * @return
	 */
	protected List createTreeRows(JTable table, String strPolicy)
	{
		//Create the root row
		NeoTreeTableRow rootRow = new NeoTreeTableRow(strPolicy,NeoTreeTableRow.NODE_TYPE_GROUP);
		
		//Initialize the list to return
		List lstRows = new ArrayList();
		
		//Check if table instance is null
		if(table == null || table.getModel() == null)
		{
			return lstRows;
		}
		
		//Get the table model instance
		TableModel tableModel = table.getModel();
		
		/*Get the column index for "First Name" and "Grouping"
		to create the tree table rows.*/
		int indexOfFirstName = -1;
		int indexOfGrouping = -1;
        int indexOfentityID=-1;
        RegistrationUtil registrationUtil = new RegistrationUtil();
        List entityList=new ArrayList();

		
		for(int j=0; j< tableModel.getColumnCount(); j++)
		{
			String strColName = tableModel.getColumnName(j);
            String entityid= tableModel.getColumnName(j);

            //Populate the entity List
            entityList.add(j,entityid);
            System.out.println("TREE PANEL VIEW Index="+j +" EntityId="+entityid);


            if(TABLE_COLUMN_ENTITY_ID.equalsIgnoreCase(entityid)){
                indexOfentityID=j;
            }
            else if(TABLE_COLUMN_NAME_FIRST_NAME.equalsIgnoreCase(strColName))
			{
				indexOfFirstName = j;
			}
			else if(TABLE_COLUMN_NAME_GROUPING.equalsIgnoreCase(strColName))
			{
				indexOfGrouping = j;
			}
			
			//As soon as you got both indices, come out.
			if(indexOfFirstName != -1 && indexOfGrouping != -1)
				break;
		}
		DataStore.entityList = entityList;
		//Check if the indices are found in the given table
		if(indexOfFirstName == -1 || indexOfGrouping == -1)
		{
			return lstRows;
		}
		
		//Now, create the tree table rows using the obtained indices and table data 
		for(int i=0; i < tableModel.getRowCount(); i++)
		{
			String strFirstName = (String) tableModel.getValueAt(i, indexOfFirstName);
			String strGroupName = (String) tableModel.getValueAt(i, indexOfGrouping);
		
			//Check if the First Name and Group Name values are valid for this row.
			if(!NeoUtils.isValidString(strFirstName) || !NeoUtils.isValidString(strGroupName) )
			{
				continue;
			}
			
			//Create the new group row
			NeoTreeTableRow newGroupRow = new NeoTreeTableRow(strGroupName, NeoTreeTableRow.NODE_TYPE_DEPARTMENT);
			
			/*Check if this group already exists. Add, only if it is not there already.
			else, get that row.*/
			if(!NeoUtils.isRowAlreadyExists(rootRow, newGroupRow))
			{
				
				rootRow.addChild(newGroupRow);
			}
			else
			{
				newGroupRow = NeoUtils.getChildRowWithName(rootRow, strGroupName);
				
				if(newGroupRow == null)
				{
					continue;
				}
			}
			
			//Create the new name row
			NeoTreeTableRow newNameRow = new NeoTreeTableRow(strFirstName, NeoTreeTableRow.NODE_TYPE_NAME);
			
			//Check if this new name row already exists for this group. If not add it.
			if(!NeoUtils.isRowAlreadyExists(newGroupRow, newNameRow))
			{
				newGroupRow.addChild(newNameRow);
			}
		}
		
		
		//Now, add the root row to the list to return
		lstRows.add(rootRow);
		
		return lstRows;
	}

	
	public void actionPerformed(ActionEvent ae) 
	{
		//Check if the button pressed is "Select Option"
		if(ae.getActionCommand().equalsIgnoreCase(BTN_ACTION_COMMAND_SELECT_OPTION))
		{
			try 
			{
				//Delegate the Call back to Controller
				_neoTreeTableCtrl.onSelectOptionCB();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		//Check if the menu item pressed is "Delete Option"
		else if(ae.getActionCommand().equalsIgnoreCase(MENU_ITEM_ACTION_COMMAND_DELETE_OPTION))
		{
			try 
			{
				//Hide, the pop up menu.
				_popUpMenu.setVisible(false);
				
				//Delegate the Call back to Controller
				_neoTreeTableCtrl.onDeleteOptionCB();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		//Sort it, after every addition
		((SortableTreeTableModel) _treeTable.getModel()).sortColumn(0,true);
		
		//Clear the tree table selection
		_treeTable.clearSelection();
	}
	
	/**
	 * This method returns the current list of selected 
	 * row indices in Tree table.
	 * 
	 * @return
	 */
	public int[] getSelectedRows()
	{
		if(_treeTable == null)
		{
			return new int[0];
		}
		
		//Get the currently selected rows from view
		int[] selRows = _treeTable.getSelectedRows();
		
		/*Now, get the actual rows. Because, sorting would have
		changes the actual positions of the row in view*/
		for(int i=0; i < selRows.length; i++)
		{
			selRows[i] = _treeTable.getActualRowAt(selRows[i]);
		}
		return selRows;
	}

   public TreeTable getTreeTable()
        {
            return _treeTable;
        }

}

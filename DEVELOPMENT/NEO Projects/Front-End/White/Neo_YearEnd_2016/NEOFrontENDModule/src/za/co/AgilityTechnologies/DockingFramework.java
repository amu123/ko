package za.co.AgilityTechnologies;

import com.jidesoft.action.DefaultDockableBarDockableHolder;
import com.jidesoft.docking.*;
import com.jidesoft.icons.JideIconsFactory;
import com.sun.net.ssl.internal.ssl.Debug;
import de.javasoft.plaf.synthetica.SyntheticaBlueMoonLookAndFeel;
import de.javasoft.plaf.synthetica.SyntheticaLookAndFeel;
import java.io.IOException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import medcore.entity.AddDependent;
import neo.claimsManagement.ClaimBatchBlalancing;
import neo.claimsManagement.ClaimEnquiry;
import neo.claimsManagement.ClaimFiringResults;
import neo.claimsManagement.ClaimHeaderCapturing;
import neo.claimsManagement.DetailedClaimEnquiry;
import neo.claimsManagement.EnquiryResults;
import neo.claimsManagement.reconcilliationOfClaimBacth;
import neo.entity.AddRolesToEntity;
import neo.entity.BankingDetails;
import neo.entity.ContactDetails;
import neo.entity.CreateEntity;
import medcore.entity.CreateNewAddressDetails;
import medcore.entity.SearchCoverMember;
import medcore.entity.ViewCoverPersonAllDetails;
import medcore.policyManagement.MedcorInsuredPersonPolicyDetails;
import neo.claimsManagement.ClaimCriteriaSearch;
import neo.claimsManagement.ClaimEnquiryResult;
import neo.claimsManagement.EDIView;
import neo.claimsManagement.EdiSearch;
import neo.claimsManagement.SearchResult;
import neo.entity.admin.CreateAdministrator;
import neo.entity.agent.CreateAgent;
import neo.entity.agent.CreateRolesToAgent;
import neo.entity.broker.CreateBroker;
import neo.entity.broker.CreateRolesToBroker;
import neo.entity.brokerhouse.CreateBrokerHouse;
import neo.entity.brokerhouse.CreateRolesToBrokerHouse;
import neo.entity.brokerhouse.LinkBrokerHouseToProduct;
import neo.entity.brokersubbranch.CreateBrokerSubBranch;
import neo.entity.brokersubbranch.CreateRolesToBrokerSubbranch;
import neo.entity.company.CreateCompany;
import neo.entity.company.CreateCompanyBankingDetails;
import neo.entity.company.CreateCompanyContact;
import neo.entity.contracts.CreateEntityContract;
import neo.entity.insured.CreateInsured;
import neo.entity.insured.CreateInsuredPerson;
import neo.entity.management.PersonAccountManagement;
import neo.entity.management.policyAccountManagement;
import neo.entity.person.CreatePerson;
import neo.entity.person.CreatePersonAddressDetails;
import neo.entity.person.CreatePersonBankingDetails;
import neo.entity.person.CreatePersonContact;
import neo.entity.policyholder.CreatePolicyholder;
import neo.entity.policyholder.CreateRolesToPolicyHolder;
import neo.entity.practice.CreatePractice;
import neo.entity.practice.CreateRolesToPractice;
import neo.entity.provider.CreateProvider;
import neo.entity.provider.CreateRolesToProvider;
import neo.entity.reinsurer.CreateReInsurer;
import neo.entity.user.CreateUser;
import neo.entity.views.BrokerHouseView;
import neo.entity.views.PreAuthAuthorizations;
import neo.financial.ActivePoliciesnotInvoiced;
import neo.financial.AgeingAnalysis;
import neo.financial.ApproveEndorsement;
import neo.financial.BankFileImport;
import neo.financial.CaptureCoverReceipt;
import neo.financial.CaptureJournals;
import neo.financial.EndorsementScheduletoApprove;
import neo.financial.InvoiceScheduletoApprove;
import neo.financial.MovementnotEndorsment;
import neo.financial.PayRun;
import neo.financial.PayRunResponseFiles;
import neo.financial.PaymentArrangement;
import neo.financial.SearchPremiumManagement;
import neo.financial.DailybankManagementReconcialition;
import neo.financial.ManualMonthEndRunSetup;
import neo.financial.PaymentExtract;
import neo.financial.QlinkRuns;
import neo.financial.RunManualBilling;
import neo.financial.SuspenseAccount;
import neo.financial.approveInvoice;
import neo.financial.chequeReprinting;
import neo.financial.chequeReturnToSender;
import neo.financial.debitOrderRun;
import neo.financial.paymentRun;
import neo.financial.paymentScheduleReconciliation;
import neo.financial.receipting;
import neo.financial.refunds;
import neo.manager.ClaimEdiLog;
import neo.manager.ClaimReversals;
import neo.manager.ClaimsBatch;
import neo.manager.CompanyContract;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBeanService;
import neo.manager.PersonContract;

import neo.policy.InsuredPersonPolicyDetails;
import neo.policy.PolicyManagement;
import neo.policy.TotalPoliciesInformation;



import neo.product.system.*;
import neo.product.utils.NeoWebService;
import neo.quotation.createQuotation.AddIndividualsToExistingPolicy;
import neo.quotation.createQuotation.AssignOptionstoQuote;
import neo.quotation.createQuotation.CaptureIndividuals;
import neo.quotation.createQuotation.CreateNewQuotation;
import neo.quotation.createQuotation.InputUnderwritingDetails;
import neo.quotation.createQuotation.RenewalExistingPolicy;
import neo.quotation.createQuotation.ReviewQuoteInput;
import neo.quotation.createQuotation.ReviewQuoteResults;
import neo.quotation.createQuotation.SendQuotations;
import neo.quotation.manageQuotation.AcceptQuotation;
import neo.quotation.manageQuotation.FollowupCommunication;
import neo.quotation.manageQuotation.ImportCensusDetails;
import neo.quotation.manageQuotation.PolicyInformation;
import neo.quotation.manageQuotation.RejectQuotation;
import neo.quotation.manageQuotation.ReviewCensusPricing;
import neo.quotation.manageQuotation.ReviewOpenQuotations;
import neo.reports.generateReports.GenerateReports;
import neo.systemMaintenence.CreateDisciplineType;
import neo.systemMaintenence.EditLookupTypes;
import neo.systemMaintenence.SystemEntityContract;
import neo.systemMaintenence.FileImport;
import neo.systemMaintenence.NeoUserResponsibilities;
import neo.systemMaintenence.NeoUserRoles;
import neo.systemMaintenence.setExchangeRate;
import neo.systemMaintenence.setSystemCurrency;

import newEntity.SearchPracticeDetails;
import newEntity.SearchProviderDetails;
import newEntity.ViewPracticeDetails;
import newEntity.ViewProviderDetails;
import org.jdesktop.jdic.browser.WebBrowser;
import za.co.AgilityTechnologies.LeftMenu.EntityActionsFrame;
import za.co.AgilityTechnologies.LeftMenu.FinancialActionsFrame;
import za.co.AgilityTechnologies.LeftMenu.ProductActionsFrame;
import za.co.AgilityTechnologies.LeftMenu.ClaimsManagementFrame;
import za.co.AgilityTechnologies.LeftMenu.PolicyActionFrame;
import za.co.AgilityTechnologies.LeftMenu.QuotationsActionFrame;
import za.co.AgilityTechnologies.LeftMenu.ReportLeftFrame;
import za.co.AgilityTechnologies.LeftMenu.SystemMaintenenceFrame;

public class DockingFramework extends DefaultDockableBarDockableHolder {

    private static DockingFramework _frame;
    private static final String PROFILE_NAME = "NEO Project";
    private static WindowAdapter _windowListener;
    private static String docRecordNumber = null;
    private static String docTypeNumber = null;
    private static String[] neoStringArgs;

    public static void EntityCreateUserActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateUser()));
        _frame.setVisible(true);


    }
    protected NeoManagerBeanService port = null;
    protected NeoManagerBeanService regService = null;

    public DockingFramework() {
    }

    public DockingFramework(String[] args) {
        Debug.println("info ", "DockingFramework string [] ");
        DockingFramework.neoStringArgs = args;

    }

    public DockingFramework(String title) throws HeadlessException {
        super(title);
        this.setPreferredSize(getToolkit().getScreenSize());
        this.setMinimumSize(getToolkit().getScreenSize());
        this.setMaximumSize(getToolkit().getScreenSize());
    }

    public static void main(String[] args) {
        com.jidesoft.utils.Lm.verifyLicense("Agility Technologies (Pty) Ltd", "NEO", "K4E7l1.WsIpxCf.igMGw85p34.SVQsu1");
        try {

            com.jidesoft.utils.Lm.verifyLicense("Agility Technologies (Pty) Ltd", "NEO", "K4E7l1.WsIpxCf.igMGw85p34.SVQsu1");


            UIManager.setLookAndFeel(new SyntheticaBlueMoonLookAndFeel());

            SyntheticaLookAndFeel.setFont("Arial", 11);


        } catch (Exception e) {
            e.printStackTrace();
        }
        //EnvConfig.rootDir=args[0];
        DockingFramework df = new DockingFramework();
        df.showDemo("1");
        neoStringArgs = args;
        if (neoStringArgs.length > 0) {
            df.presenceIntegrationProcedure(df, neoStringArgs);
        }
    }

    public static void bodyPanel(int i) {

        BodyPanelRenderer bpr = new BodyPanelRenderer();
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(bpr.bodyRender(i)));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void PersonAccountManagementActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new PersonAccountManagement());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void policyAccountManagementActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new policyAccountManagement());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void setSystemCurrencyActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new setSystemCurrency());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void addpaymentBalanceActions(Object[][] receiptTable) {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new paymentScheduleReconciliation(receiptTable));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void setExchangeRateActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new setExchangeRate());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void ClaimsCaptureActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        //_frame.getDockingManager().getWorkspace().add(new claimCapture());
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ClaimHeaderCapturing()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void ClaimsCaptureActions(int claimId, String UorR, ClaimReversals claimreversal) {
        _frame.getDockingManager().getWorkspace().removeAll();
        //_frame.getDockingManager().getWorkspace().add(new claimCapture());
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ClaimHeaderCapturing(claimId, UorR, claimreversal)));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void SeachClaimsByBatchActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        //_frame.getDockingManager().getWorkspace().add(new claimCapture());
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new SearchResult()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void SeachClaimsActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        //_frame.getDockingManager().getWorkspace().add(new claimCapture());
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new  ClaimEnquiryResult()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void SeachClaimsActions(Collection<ClaimsBatch> batch) {
        _frame.getDockingManager().getWorkspace().removeAll();
        //_frame.getDockingManager().getWorkspace().add(new claimCapture());
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new  ClaimEnquiryResult(batch)));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void SeachClaimsByBatchActions(Collection<ClaimsBatch> batch) {
        _frame.getDockingManager().getWorkspace().removeAll();
        //_frame.getDockingManager().getWorkspace().add(new claimCapture());
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new SearchResult(batch)));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);
    }

    public static void HeaderCaptureActions(int batchId) {
        _frame.getDockingManager().getWorkspace().removeAll();
        //_frame.getDockingManager().getWorkspace().add(new claimCapture());
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ClaimHeaderCapturing(batchId)));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void ClaimBatchBlalancingActions(int _batchId) {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new ClaimBatchBlalancing(_batchId));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void claimAdjudication(int _batchId) {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new ClaimFiringResults(_batchId));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void ClaimsEnquirySearchActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new ClaimEnquiry());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void ClaimsCriteriaSearchActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new ClaimCriteriaSearch());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void ClaimsEdiEnquirySearchActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new EdiSearch());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void ClaimsEdiEnquiryViewActions(Collection<ClaimEdiLog> cel) {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new EDIView(cel));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void ClaimsRefinedSearchActions(String insured, String provider, String practice) {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new DetailedClaimEnquiry(insured, provider, practice));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }



    public static void ClaimsSearchResultsActions(ArrayList list) {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new EnquiryResults(list));
//        _frame.getDockingManager().getWorkspace().add(new EnquiryResults(ArrayList batches));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void CreateDisciplineActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateDisciplineType()));
        _frame.setVisible(true);
    }

    public void CreateEntityContractActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new SystemEntityContract()));
        _frame.setVisible(true);
    }

    public void CreateSearchProviderAction() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new SearchProviderDetails()));
        _frame.setVisible(true);
    }

    public void CreateSearchPracticeAction() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new SearchPracticeDetails()));
        _frame.setVisible(true);
    }

    public void EditLookupTypes() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new EditLookupTypes()));
        _frame.setVisible(true);
    }

    public void EntityContractManagementActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateEntityContract()));
        _frame.setVisible(true);
    }

    public void EntityCreateEntityPersonRolesActions() {
        int EntityType = CreateEntity.entityType.getSelectedIndex();
        AddRolesToEntity art = new AddRolesToEntity();
        switch (EntityType) {
            /*case 1:
            _frame.getDockingManager().getWorkspace().removeAll();
            art.removeAll();
            _frame.getDockingManager().getWorkspace().add(new JScrollPane(art.add(new CreateRolesToCompanyEntity())));
            _frame.setVisible(true);
            break;*/

            case 1:
                _frame.getDockingManager().getWorkspace().removeAll();
                _frame.getDockingManager().getWorkspace().add(new JScrollPane(art.add(new CreateRolesToProvider())));
                _frame.setVisible(true);
                break;

            //Policy Holder
            case 2:
                _frame.getDockingManager().getWorkspace().removeAll();
                _frame.getDockingManager().getWorkspace().add(new JScrollPane(art.add(new CreateRolesToPolicyHolder())));
                _frame.setVisible(true);
                break;

            //Broker
            case 4:
                _frame.getDockingManager().getWorkspace().removeAll();
                _frame.getDockingManager().getWorkspace().add(new JScrollPane(art.add(new CreateRolesToBroker())));
                _frame.setVisible(true);
                break;
            //Agent
            case 7:
                _frame.getDockingManager().getWorkspace().removeAll();
                _frame.getDockingManager().getWorkspace().add(new JScrollPane(art.add(new CreateRolesToAgent())));
                _frame.setVisible(true);
                break;





        }
    }

    public void EntityCreateEntityCompanyRolesActions() {

        int EntityType = CreateEntity.entityType.getSelectedIndex();

        AddRolesToEntity art = new AddRolesToEntity();
        switch (EntityType) {
            case 0:
                _frame.getDockingManager().getWorkspace().removeAll();
                _frame.getDockingManager().getWorkspace().add(new JScrollPane(art.add(new CreateRolesToPractice())));
                _frame.setVisible(true);
                break;
            case 5:
                _frame.getDockingManager().getWorkspace().removeAll();
                _frame.getDockingManager().getWorkspace().add(new JScrollPane(art.add(new CreateRolesToBrokerHouse())));
                _frame.setVisible(true);
                break;

            case 8:
                _frame.getDockingManager().getWorkspace().removeAll();
                _frame.getDockingManager().getWorkspace().add(new JScrollPane(art.add(new CreateRolesToBrokerSubbranch())));
                _frame.setVisible(true);
                break;

        }
    }

    public static void LinkToBrokerHouseToProduct() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new LinkBrokerHouseToProduct()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void EntityCreateBrokerHouseActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateBrokerHouse()));
        _frame.setVisible(true);
    }

    public void EntityCreateEntityActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        JScrollPane scrollPane = new JScrollPane(new CreateEntity());

        scrollPane.getVerticalScrollBar().setUnitIncrement(32);

        _frame.getDockingManager().getWorkspace().add(scrollPane);
        _frame.setVisible(true);

    }

    public static void EntityCreatePersonActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreatePerson()));
        _frame.setVisible(true);

    }

    public static void EntityCreateAdministratorActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateAdministrator()));
        _frame.setVisible(true);

    }

    public static void EntityCreateAgentActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateAgent()));

        _frame.setVisible(true);

    }

    public static void EntityCreateSubBranchActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateBrokerSubBranch()));
        _frame.setVisible(true);
    }

    public static void EntityCreateBrokerActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateBroker()));

        _frame.setVisible(true);

    }

    public static void addJournalActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new CaptureJournals());
        //      _frame.getDockingManager().getWorkspace().add(new addJournal());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void FinanceInvoiceScheduleApproveActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new InvoiceScheduletoApprove()));

        _frame.setVisible(true);

    }

    public static void FinanceRunManualBillingActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new RunManualBilling()));

        _frame.setVisible(true);

    }

    public static void FinanceAgeingAnalysisActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new AgeingAnalysis()));

        _frame.setVisible(true);

    }

    public static void FinanceQlinkRunActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new PaymentExtract()));

        _frame.setVisible(true);

    }

    public static void FinanceManualMonthEndRunSetupActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ManualMonthEndRunSetup()));

        _frame.setVisible(true);

    }

    public static void FinancePaymentSchedulereconActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new receipting());

        _frame.setVisible(true);

    }

    public static void FinanceBankFileImportActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new BankFileImport()));

        _frame.setVisible(true);

    }

    public static void FinancepaymentRunActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new paymentRun()));

        _frame.setVisible(true);

    }

    public static void FinanceChequereprintingRunActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new chequeReprinting()));

        _frame.setVisible(true);

    }

    public static void FinanceChequeReturnSenderRunActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new chequeReturnToSender()));

        _frame.setVisible(true);

    }

    public static void FinancePayrunResponseActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new PayRunResponseFiles()));

        _frame.setVisible(true);

    }

    public static void FinancePaymentArrangementActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new PaymentArrangement()));

        _frame.setVisible(true);

    }

    public static void FinanceSearchPremiumManagementActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new SearchPremiumManagement()));

        _frame.setVisible(true);

    }

    public static void FinanceApproveInvoiceActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new approveInvoice());

        _frame.setVisible(true);

    }

    public static void FinanceRefundsActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new refunds());

        _frame.setVisible(true);

    }

    public static void FinanceActivePoliciesnotInvoicedActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ActivePoliciesnotInvoiced()));

        _frame.setVisible(true);

    }

    public static void FinanceEndorsementScheduletoApproveActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new EndorsementScheduletoApprove()));

        _frame.setVisible(true);

    }

    public static void FinanceDebitOrderRunActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new debitOrderRun()));

        _frame.setVisible(true);

    }

    public static void FinancePayRunActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new PayRun()));

        _frame.setVisible(true);

    }

    public static void FinanceSuspenseAccountActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new SuspenseAccount()));

        _frame.setVisible(true);

    }

    public static void FinanceDailyBankManagementReconActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new DailybankManagementReconcialition()));

        _frame.setVisible(true);

    }

    public static void FinanceApproveEndorsementctions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ApproveEndorsement()));

        _frame.setVisible(true);

    }

    public static void FinanceMovementnotEndorsementctions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new MovementnotEndorsment()));

        _frame.setVisible(true);

    }

    public static void EntityCreateBrokerSubbranchActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateBrokerSubBranch()));

        _frame.setVisible(true);

    }

    public static void EntityCreateInsurerActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateInsuredPerson()));

        _frame.setVisible(true);

    }

    public static void EntityCreateReInsurerActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateReInsurer()));

        _frame.setVisible(true);

    }

    public static void EntityCreateInsurerCompanyActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateInsured()));

        _frame.setVisible(true);

    }

    public static void EntityCreateProviderActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateProvider()));

        _frame.setVisible(true);

    }

    public static void EntityCreateInsuredPersonActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateInsuredPerson()));

        _frame.setVisible(true);

    }

    public static void EntityCreatePolicyHolderActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreatePolicyholder()));

        _frame.setVisible(true);

    }

    public static void EntityCreatePracticeActions() {
        _frame.getDockingManager().getWorkspace().removeAll();

        JScrollPane scrollPane = new JScrollPane(new CreatePractice());

        _frame.getDockingManager().getWorkspace().add(scrollPane);

        _frame.setVisible(true);

    }

    public static void EntityCreateAddCompanyRelationActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        //    _frame.getDockingManager().getWorkspace().add(new JScrollPane(new AddCompanyRelationship()));

        _frame.setVisible(true);

    }

    public static void EntityCreateAddPesonRelationActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        //   _frame.getDockingManager().getWorkspace().add(new JScrollPane(new AddPersonRelationship()));

        _frame.setVisible(true);

    }

    public static void EntityCreateRolesToBrokerActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateRolesToBroker()));

        _frame.setVisible(true);

    }

    public static void EntityCreateRolesToBrokerHouseActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateRolesToBrokerHouse()));

        _frame.setVisible(true);

    }

    public static void EntityCreateRolesToPolicyHolderActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateRolesToPolicyHolder()));

        _frame.setVisible(true);

    }

    public static void EntityCreateRolesToAgentActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateRolesToAgent()));

        _frame.setVisible(true);

    }

    public static void EntityCreateRolesToBrokerSubbranchActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateRolesToBrokerSubbranch()));

        _frame.setVisible(true);

    }

    public static void EntityCreateRolesToProviderActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateRolesToProvider()));

        _frame.setVisible(true);

    }

    public static void EntityCreateRolesToPracticeActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateRolesToPractice()));
        _frame.setVisible(true);

    }

    public static void EntityCreatePersonContactActions() {

        _frame.getDockingManager().getWorkspace().removeAll();

        ContactDetails contactdetails = new ContactDetails();
        CreatePersonContact createPersonContact = new CreatePersonContact();

        contactdetails.removeAll();

        _frame.getDockingManager().getWorkspace().add(new JScrollPane(contactdetails.add(createPersonContact)));

        _frame.setVisible(true);



    /* if (EntityActionsFrame.GLOBALPersonContract.getEntityId()==0) {
    ContactDetails contactdetails = new ContactDetails();
    CreatePersonContact createPersonContact = new CreatePersonContact();

    contactdetails.removeAll();

    _frame.getDockingManager().getWorkspace().add(new JScrollPane(contactdetails.add(createPersonContact)));

    _frame.setVisible(true);
    } else {
    ContactDetails contactdetails = new ContactDetails();
    CreatePersonContact createPersonContact = new CreatePersonContact();

    contactdetails.removeAll();

    _frame.getDockingManager().getWorkspace().add(new JScrollPane(contactdetails.add(createPersonContact)));

    _frame.setVisible(true);

    }
     */


    }

    public static void EntityCreatePersonAddressActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        ContactDetails contactdetails = new ContactDetails();
        contactdetails.removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(contactdetails.add(new CreatePersonAddressDetails())));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void EntityCreatePersonBenkingDetailsActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        BankingDetails contactdetails = new BankingDetails();
        contactdetails.removeAll();

        _frame.getDockingManager().getWorkspace().add(new JScrollPane(contactdetails.add(new CreatePersonBankingDetails())));

        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void EntityCreateCompanyActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new CreateCompany());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void EntityCreateCompanyBankingActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        BankingDetails contactdetails = new BankingDetails();
        contactdetails.removeAll();

        _frame.getDockingManager().getWorkspace().add(new JScrollPane(contactdetails.add(new CreateCompanyBankingDetails())));

        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void EntityCreateCompanyAddressActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        ContactDetails contactdetails = new ContactDetails();
        contactdetails.removeAll();
        //   _frame.getDockingManager().getWorkspace().add(new JScrollPane(contactdetails.add(new CreateCompanyAddressDetails())));
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(contactdetails.add(new CreateNewAddressDetails())));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void EntityCreateCompanyContactActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        ContactDetails contactdetails = new ContactDetails();

        _frame.getDockingManager().getWorkspace().add(new JScrollPane(contactdetails.add(new CreateCompanyContact())));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void ProductCreateBenefitActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateBenefit()));
        _frame.setVisible(true);

    }

    public static void ProductCreateBenefitLimitsActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateBenefitLimits()));
        _frame.setVisible(true);

    }

    public static void ProductCreateActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateProduct(_frame)));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void ProductCreateOptionsActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateOptions(_frame)));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void ProductLinkAllowedProviderBenefitActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new LinkAllowedProviderProduct());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void ProductSearchActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new SearchProduct());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void ProductCreateBenefitThresholdsActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateBenefitThresholds()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void ProductCreateBenefitRulesActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateBenefitRules()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);


    }

    public static void ProductSearchBenefitActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new SearchBenefit()));
        _frame.setVisible(true);


    }

    public static void ProductLinkOptionsBenefitActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new LinkBenefitsOptions()));
        _frame.setVisible(true);
    }

    public static void ProductLinkBenefitToSystemTermsActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new LinkingBenifitToSystemTerm()));
        _frame.setVisible(true);
    }

    public static void ProductLinkBenefitToBenefitActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new LinkingBenefitToBenefit()));
        _frame.setVisible(true);
    }

    public static void ProductLinkSubLimitsToBenefits() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new LinkSubLimitsToBenefits()));
        _frame.setVisible(true);
    }

    public void EntityViewEntityActions(String entityTypeName) {
        if (entityTypeName.equals("BROKER_HOUSE")) {
            _frame.getDockingManager().getWorkspace().removeAll();
            _frame.getDockingManager().getWorkspace().add(new BrokerHouseView());
            //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
            _frame.setVisible(true);
        }
    }

    public void ImportFileActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new FileImport()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);
    }

    public void NeoUserRoleManagementActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new NeoUserRoles()));
        _frame.setVisible(true);
    }

    public void NeoUserResponsibilityManagementActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new NeoUserResponsibilities()));
        _frame.setVisible(true);
    }

    public void NewDependentAddAction(String CoverNumber, String type) {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new AddDependent(CoverNumber, type)));
        _frame.setVisible(true);
    }

    public void PreAuthAuthorizationAction() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new PreAuthAuthorizations()));
        _frame.setVisible(true);
    }

    public void ProductAddDSPAction() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new AddDSPToProduct(_frame)));
        _frame.setVisible(true);
    }

    public void ProductAddDebitOrderRuns() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new DebitOrderRunForProduct(_frame)));
        _frame.setVisible(true);
    }

    public void ProductOptionAddReinsurerAction() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new AddReinsurerToOption(_frame));
        _frame.setVisible(true);
    }

    public void SystemTermBodyActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new SearchSystemTerm());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void ProductCreateSystemParametersBodyAction() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new CreateSystemParameters());
        _frame.setVisible(true);

    }

    public void ProductSearchSystemParametersBodyActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new SearchSystemParameters());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void ProductCreateSystemTermBodyActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new CreateSystemTerm());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void ProductSearchSystemtermBodyActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new SearchSystemTerm());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void addDependentAction() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new AddDependent()));
        _frame.setVisible(true);
    }

    public void individualeReceiptActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        //       _frame.getDockingManager().getWorkspace().add(new individualeReceipt());
        _frame.getDockingManager().getWorkspace().add(new CaptureCoverReceipt());
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);
    }

    public DefaultDockableBarDockableHolder showDemo(String version) {

        if (_frame != null) {
            _frame.toFront();
            return _frame;
        }
        _frame = new DockingFramework("AGHS NEO " + version);
        _frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        _frame.setIconImage(NeoIconsFactory.getImageIcon(new String(NeoIconsFactory.NEOLogos.neosmall)).getImage());

        _windowListener = new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                clearUp();
                if (true) {
                    System.exit(0);
                }
            }
        };
        _frame.addWindowListener(_windowListener);
        //       _frame.getLayoutPersistence().setProfileKey(PROFILE_NAME);
        _frame.getDockingManager().setProfileKey(PROFILE_NAME);

        _frame.getDockingManager().setOutlineMode(DockingManager.TRANSPARENT_OUTLINE_MODE);



        _frame.getContentPane().add(new ButtonsPanel(this), BorderLayout.NORTH);
        //_frame.getContentPane().add(new NeoLogo(), BorderLayout.WEST);
        // add the body(work space)
        _frame.getDockingManager().getWorkspace().add(new Body());

        // add all dockable frames
        // _frame.getDockingManager().addFrame(createSearchFrame());
        //_frame.getDockingManager().addFrame(createTaskActionFrame());

        _frame.getDockingManager().addFrame(createMessageFrame());
        _frame.getDockingManager().removeFrame("PolicyLeftFrame");
        _frame.getDockingManager().removeFrame("ProductLeftFrame");
        _frame.getDockingManager().removeFrame("QuotationLeftFrame");
        if (version.equalsIgnoreCase("1")) {
            // _frame.getDockingManager().addFrame(createDynamicScreenFrame());

            // add menu bar
        }
        //Design frames properties
        _frame.getDockingManager().resetToDefault();
        _frame.getDockingManager().setInitSplitPriority(DockingManager.SPLIT_EAST_WEST_SOUTH_NORTH);
        //   _frame.getDockableBarManager().addDockableBar(NeoCommandBarFactory.createStandardCommandBar());
        _frame.getDockingManager().getWorkspace().setAcceptDockableFrame(false);

        //Design Docableframes properties

        _frame.getDockingManager().setRearrangable(false);
        _frame.getDockingManager().setFloatable(false);
        // _frame.getDockingManager().setHidable(false);
        //  _frame.getLayoutPersistence().loadLayoutData();
        _frame.toFront();
        return _frame;
    }
    //window closing functionality

    private static void clearUp() {
        _frame.removeWindowListener(_windowListener);
        _windowListener = null;

        if (_frame.getDockingManager() != null) {
            _frame.getDockingManager().saveLayoutData();
        }

        _frame.dispose();
        _frame = null;
    }

    protected DockableFrame createDockableFrame(String key, Icon icon) {
        DockableFrame frame = new DockableFrame(key, icon);
        //  frame.setPreferredSize(new Dimension(200, 200));
        return frame;
    }

    protected DockableFrame createFinancialLeftFrame() {
        DockableFrame frame = createDockableFrame("FinancialLeftFrame", NeoIconsFactory.getImageIcon(new String(NeoIconsFactory.SideMenuLogos.users4_add)));
        frame.getContext().setInitMode(DockContext.STATE_FRAMEDOCKED);
        frame.getContext().setInitSide(DockContext.DOCK_SIDE_WEST);
        frame.getContext().setInitIndex(2);
        frame.add(new JScrollPane(new FinancialActionsFrame(this)));
        frame.setTitle("FINANCIAL");
        frame.setTabTitle("FINANCIAL");
        return frame;
    }

    protected DockableFrame createPolicyLeftFrame() {
        DockableFrame frame = createDockableFrame("PolicyLeftFrame", NeoIconsFactory.getImageIcon(new String(NeoIconsFactory.SideMenuLogos.users4_add)));
        frame.getContext().setInitMode(DockContext.STATE_FRAMEDOCKED);
        frame.getContext().setInitSide(DockContext.DOCK_SIDE_WEST);
        frame.getContext().setInitIndex(10);
        //  frame.add(new DynamicPanelCreation());
        frame.add(new JScrollPane(new PolicyActionFrame(this)));
        frame.setTitle("POLICY");
        frame.setTabTitle("POLICY");
        return frame;
    }

    protected DockableFrame createEmailFrame() {


        DockableFrame frame = new DockableFrame("EMAIL", JideIconsFactory.getImageIcon(JideIconsFactory.DockableFrame.FRAME3));
        frame.getContext().setInitMode(DockContext.STATE_FRAMEDOCKED);
        frame.getContext().setInitSide(DockContext.DOCK_SIDE_SOUTH);
        frame.getContext().setInitIndex(2);
        Dimension dimension = new Dimension(900, 800);

        frame.setPreferredSize(dimension);
        frame.setSize(dimension);
        // frame.setSize(1100, 900);
        frame.getContentPane().setLayout(new BorderLayout());

        WebBrowser webBrowser = new WebBrowser();


        try {
            // webBrowser.setURL(new URL("https://centurion.caretech.co.za/exchange"));
            webBrowser.setURL(new URL(NeoWebService.getEmailURL()));
        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
        }

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setPreferredSize(new Dimension(900, 800));
        panel.add(webBrowser, BorderLayout.CENTER);
        panel.setVisible(true);

        frame.add(panel);

        frame.setToolTipText("EMAIL");

        return frame;
    }

    protected DockableFrame createProductLeftFrame() {
        DockableFrame frame = createDockableFrame("ProductLeftFrame", NeoIconsFactory.getImageIcon(new String(NeoIconsFactory.SideMenuLogos.cubes)));

        frame.getContext().setInitMode(DockContext.STATE_FRAMEDOCKED);
        frame.getContext().setInitSide(DockContext.DOCK_SIDE_WEST);
        frame.getContext().setInitIndex(3);
        //  frame.add(new DynamicPanelCreation());
        frame.add(new JScrollPane(new ProductActionsFrame(this)));
        frame.setTitle("PRODUCT");
        frame.setTabTitle("PRODUCT");
        return frame;
    }

    protected DockableFrame createEntityLeftFrame() {
        DockableFrame frame = createDockableFrame("EntityLeftFrame", NeoIconsFactory.getImageIcon(new String(NeoIconsFactory.SideMenuLogos.stockbroker2)));
        frame.getContext().setInitMode(DockContext.STATE_FRAMEDOCKED);
        frame.getContext().setInitSide(DockContext.DOCK_SIDE_WEST);
        frame.getContext().setInitIndex(4);

        Dimension dimension = new Dimension(240, 500);

        frame.setPreferredSize(dimension);
        frame.setSize(dimension);

        //  frame.add(new DynamicPanelCreation());
        frame.add(new JScrollPane(new EntityActionsFrame(this)));
        frame.setTitle("ENTITY");
        frame.setTabTitle("ENTITY");
        return frame;
    }

    protected DockableFrame createQuotationLeftFrame() {
        DockableFrame frame = createDockableFrame("QuotationLeftFrame", NeoIconsFactory.getImageIcon(new String(NeoIconsFactory.SideMenuLogos.briefcase2_document)));
        frame.getContext().setInitMode(DockContext.STATE_FRAMEDOCKED);
        frame.getContext().setInitSide(DockContext.DOCK_SIDE_WEST);
        frame.getContext().setInitIndex(5);
        //  frame.add(new DynamicPanelCreation());
        frame.add(new JScrollPane(new QuotationsActionFrame(this)));
        frame.setTitle("NEW APPLICATION");
        frame.setTabTitle("NEW APPLICATION");
        return frame;
    }

    protected DockableFrame createSearchFrame() {


        DockableFrame frame = createDockableFrame("SEARCH", NeoIconsFactory.getImageIcon(NeoIconsFactory.Standard.FINDSYMBOL));
        frame.getContext().setInitMode(DockContext.STATE_AUTOHIDE);
        frame.getContext().setInitSide(DockContext.DOCK_SIDE_WEST);
        frame.getContext().setInitIndex(1);
        // TODO GDH Search
        //frame.add(new SearchPanel());
        frame.add(new SearchPanel(this));

        frame.setTitle("Search");
        frame.setTabTitle("SEARCH");

        return frame;
    }

    protected DockableFrame createMessageFrame() {

        DockableFrame frame = createDockableFrame("MESSAGES", JideIconsFactory.getImageIcon(JideIconsFactory.DockableFrame.FRAME3));
        frame.getContext().setInitMode(DockContext.STATE_AUTOHIDE);
        frame.getContext().setInitSide(DockContext.DOCK_SIDE_SOUTH);
        frame.getContext().setInitIndex(1);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.add(new TextArea());
        frame.setToolTipText("MESSAGES");
        return frame;
    }

    protected DockableFrame createProviderLeftFrame() {

        DockableFrame frame = createDockableFrame("ProviderLeftFrame", NeoIconsFactory.getImageIcon(new String(NeoIconsFactory.SideMenuLogos.doctor)));

        frame.getContext().setInitMode(DockContext.STATE_FRAMEDOCKED);
        frame.getContext().setInitSide(DockContext.DOCK_SIDE_WEST);
        frame.getContext().setInitIndex(7);
        //  frame.add(new DynamicPanelCreation());
        frame.add(new JScrollPane(new ClaimsManagementFrame()));
        frame.setTitle("CLAIMS MANAGEMENT");
        frame.setTabTitle("CLAIMS MANAGEMENT");
        return frame;
    }

    protected DockableFrame createSystemMaintenanceLeftFrame() {
        DockableFrame frame;


        frame = createDockableFrame("SystemLeftFrame", NeoIconsFactory.getImageIcon(new String(NeoIconsFactory.SideMenuLogos.wrench)));
        frame.getContext().setInitMode(DockContext.STATE_FRAMEDOCKED);
        frame.getContext().setInitSide(DockContext.DOCK_SIDE_WEST);
        frame.getContext().setInitIndex(8);
        //  frame.add(new DynamicPanelCreation());
        frame.add(new JScrollPane(new SystemMaintenenceFrame(this)));
        frame.setTitle("SYSTEM MAINTENANCE");
        frame.setTabTitle("SYSTEM MAINTENANCE");
        return frame;
    }

    protected DockableFrame createReportLeftFrame() {
        DockableFrame frame = createDockableFrame("ReportLeftFrame", NeoIconsFactory.getImageIcon(new String(NeoIconsFactory.SideMenuLogos.briefcase2_document)));
        frame.getContext().setInitMode(DockContext.STATE_FRAMEDOCKED);
        frame.getContext().setInitSide(DockContext.DOCK_SIDE_WEST);
        frame.getContext().setInitIndex(9);
        frame.add(new JScrollPane(new ReportLeftFrame()));
        frame.setTitle("REPORTS");
        frame.setTabTitle("REPORTS");
        return frame;
    }

    public void homeButtonAction() {
        //   _frame.getDockingManager().resetToDefault();
        // _frame.getDockingManager().autohideAll();
        //if(_frame.getDockingManager().getFrame("BODYPANEL").isAvailable())

        _frame.getDockingManager().removeFrame("ScreenToolFrame");
        _frame.getDockingManager().removeFrame("PolicyLeftFrame");
        _frame.getDockingManager().removeFrame("ProductLeftFrame");
        _frame.getDockingManager().removeFrame("QuotationLeftFrame");
        _frame.getDockingManager().removeFrame("EntityLeftFrame");
        _frame.getDockingManager().removeFrame("ProviderLeftFrame");
        _frame.getDockingManager().removeFrame("SystemLeftFrame");
        _frame.getDockingManager().removeFrame("EMAIL");
        _frame.getDockingManager().removeFrame("FinancialLeftFrame");
        _frame.getDockingManager().removeFrame("ReportLeftFrame");


        //if(_frame.getDockingManager().getFrame("BODYPANEL").isActive())
        // _frame.getDockingManager().hideFrame("BODYPANEL");
        // _frame.getDockingManager().removeAllFrames();

        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new Body());
        _frame.setVisible(true);
    }

    public void ReportsButtonAction() {
        //  _frame.getDockingManager().resetToDefault();
        _frame.getDockingManager().autohideAll();
        _frame.getDockingManager().removeFrame("ReportLeftFrame");
        _frame.getDockingManager().addFrame(createReportLeftFrame());
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new Body());
        _frame.setVisible(true);
    }

    public void financialButtonAction() {
        //   _frame.getDockingManager().resetToDefault();
        _frame.getDockingManager().autohideAll();

        _frame.getDockingManager().removeFrame("FinancialLeftFrame");
        _frame.getDockingManager().addFrame(createFinancialLeftFrame());


        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new Body());
        _frame.setVisible(true);
    }

    public void policyButtonAction() {
        //   _frame.getDockingManager().resetToDefault();
        _frame.getDockingManager().autohideAll();

        _frame.getDockingManager().removeFrame("PolicyLeftFrame");
        _frame.getDockingManager().addFrame(createPolicyLeftFrame());

        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new Body());
        //PolicyActionFrame.policyManagementList.setSelectedIndex(0);
        _frame.setVisible(true);
    }

    public void productButtonAction() {
        //   _frame.getDockingManager().resetToDefault();
        _frame.getDockingManager().autohideAll();


        _frame.getDockingManager().removeFrame("ProductLeftFrame");
        _frame.getDockingManager().addFrame(createProductLeftFrame());

        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new Body());
        _frame.setVisible(true);
    }

    public void quotationButtonAction() {
        //  _frame.getDockingManager().resetToDefault();
        _frame.getDockingManager().autohideAll();

        _frame.getDockingManager().removeFrame("QuotationLeftFrame");
        _frame.getDockingManager().addFrame(createQuotationLeftFrame());

        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new Body());
        _frame.setVisible(true);
    }

    public void EntityButtonAction() {
        _frame.getDockingManager().autohideAll();
        _frame.getDockingManager().removeFrame("EntityLeftFrame");
        _frame.getDockingManager().addFrame(createEntityLeftFrame());
        //EntityActionsFrame.InsuredPersonList.setSelectedIndex(0);

        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new Body());
        _frame.setVisible(true);
    }

    public void ProviderButtonAction() {
        //  _frame.getDockingManager().resetToDefault();
        _frame.getDockingManager().autohideAll();
        _frame.getDockingManager().removeFrame("ProviderLeftFrame");
        _frame.getDockingManager().addFrame(createProviderLeftFrame());

        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new Body());
        _frame.setVisible(true);
    }

    public void SystemMaintenanceButtonAction() {
        //   _frame.getDockingManager().resetToDefault();
        _frame.getDockingManager().autohideAll();
        DockableFrame frame = null;

        frame = createSystemMaintenanceLeftFrame();
        _frame.getDockingManager().removeFrame("SystemLeftFrame");
        _frame.getDockingManager().addFrame(frame);

        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new Body());
        _frame.setVisible(true);
    }

    public void MailButtonAction() {


        _frame.getDockingManager().autohideAll();

        _frame.getDockingManager().removeFrame("EMAIL");
        _frame.getDockingManager().addFrame(createEmailFrame());

        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new Body());
        _frame.setVisible(true);

    }

    //Quotation screens link
    public void CreateNewQuotation() {
        _frame.getDockingManager().getWorkspace().removeAll();

        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateNewQuotation()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void RenewalPolicyActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new RenewalExistingPolicy()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void RenewalAddIndividualsPolicyActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new AddIndividualsToExistingPolicy()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void CaptureIndividualsToQuotation() {
        _frame.getDockingManager().getWorkspace().removeAll();
        CaptureIndividuals captureIndividuals = new CaptureIndividuals();

        _frame.getDockingManager().getWorkspace().add(new JScrollPane(captureIndividuals));
        if (CreateNewQuotation.getImportData() != null) {
            captureIndividuals.setImportData(CreateNewQuotation.getImportData());
        }
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void AssignOptionsToQuoteActions() {
        _frame.getDockingManager().getWorkspace().removeAll();

        AssignOptionstoQuote assignOptionsToQuote = new AssignOptionstoQuote();

        // System.out.println(CaptureIndividuals.policyNumber.getText()+"-----CaptureIndividuals.policyNumber.getText()");
        //assignOptionsToQuote.policyHolder.setText(CaptureIndividuals.policyHoldername.getText());
        //assignOptionsToQuote.initialize(CaptureIndividuals.getTable());
        //assignoptionstoQuote.setInputValues(captureIndividuals.getSelectedValues());
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(assignOptionsToQuote));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void InputUnderwritingActions() {
        _frame.getDockingManager().getWorkspace().removeAll();

        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new InputUnderwritingDetails()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void ReviewQuoteInputActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ReviewQuoteInput()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void ReviewQuoteResultsActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ReviewQuoteResults()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void SendQuoteActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new SendQuotations()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void OpenReviewQuoteActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ReviewOpenQuotations()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void FollowupCommQuoteActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new FollowupCommunication()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void RejectQuoteActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new RejectQuotation()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void AcceptQuoteActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new AcceptQuotation(this)));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void ImportCensusQuoteActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ImportCensusDetails(this)));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public void ReviewCensusPriceQuoteActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ReviewCensusPricing()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static void PolicyInformation() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new PolicyInformation()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);

    }

    public static class DataStore {

        public static java.util.List entityList;
        public static java.util.List optionVersionList;
        public static java.util.List benefitList;
    }

    public void RulesEnginButtonAction() {
        _frame.getDockingManager().getWorkspace().removeAll();
        Runtime run = Runtime.getRuntime();
        try {
            // Process pp = run.exec("C:\\Progra~1\\KnowledgePower\\KnowledgePower.exe");
            Process pp = run.exec(NeoWebService.getRulesEnginePath());
        } catch (IOException ex) {
            Logger.getLogger(DockingFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
        _frame.getDockingManager().getWorkspace().add(new Body());
        _frame.setVisible(true);

    }

    public static void GenerateReportsActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new GenerateReports()));
        //_frame.getDockingManager().getWorkspace().add(new FileChooserDemo());
        _frame.setVisible(true);
    }

    //Policy Screen link
    public void CreatenewPolicy() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.setVisible(true);
    }

    public void CreatePolicyDetailsInformation() {
        _frame.getDockingManager().getWorkspace().removeAll();
        // _frame.getDockingManager().getWorkspace().add(new JScrollPane(new TotalPolicyResults()));
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new TotalPoliciesInformation()));

        _frame.setVisible(true);
    }

    public void CreateInsuredPersonPolicyDetailsActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new InsuredPersonPolicyDetails()));

        _frame.setVisible(true);
    }

    public static void PolicyManagementActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new PolicyManagement()));

        _frame.setVisible(true);
    }

    public void presenceIntegrationProcedure(DockingFramework df, String[] args) {
        int entityType = Integer.parseInt(args[0]);//this is already checked
        int entityId = (args.length > 1) ? Integer.parseInt(args[1]) : 0;//this must be the default value
        if (args.length > 3) {
            setDocRecordNumber(args[2]);//set reocrd id
            setDocTypeNumber(args[3]);//set doc type
        }
        if (entityType > 0 && entityType < 12) {
            df.EntityButtonAction();

            df.EntityCreateEntityActions();
            CreateEntity.entityType.setSelectedIndex(entityType);
        }

        if (entityType == 0) {//practice
            presenencPractice(df, entityId);
        } else if (entityType == 1) {//provider
            presenencProvider(df, entityId);
        } else if (entityType == 2) {//policy holder
            presenencPolicyHolder(df, entityId);
        } else if (entityType == 3) {//user
            presenencPersonUser(df, entityId);//checks
        } else if (entityType == 4) {//broker
            presenencBroker(df, entityId);
            if (args.length > 3) {
                CreateBroker.butViewDoc.setEnabled(true);
            }
        } else if (entityType == 5) {//broker_house
            presenencBrokerHouse(df, entityId);
        } else if (entityType == 6) {//insurer
            presenencInsurer(df, entityId);
        } else if (entityType == 7) {//agent
            presenenceAgent(df, entityId);
        } else if (entityType == 8) {//sub_branch
            presenenceSubBranch(df, entityId);
        } else if (entityType == 9) {//administrator
            presenencCompanyAdministrator(df, entityId);//check
        } else if (entityType == 10) {//insured_person
            presenenceInsuredPerson(df, entityId);
        } else if (entityType == 40) {//claims
            df.ProviderButtonAction();
            df.ClaimsCaptureActions();
            reconcilliationOfClaimBacth.viewDoc.setEnabled(true);
        //presenceClaimsScreen(df, entityId);
        } else if (entityType == 30) {//quotation
            df.quotationButtonAction();
            df.AcceptQuoteActions();
            AcceptQuotation.butViewAcceptanceLetter.setEnabled(true);

        //presenceQuotationScreen(df, entityId);
        } else if (entityType == 60) {//finance
            presenceFinanceScreen(df, entityId);
        } else if (entityType == 50) {//pre auth
            presencePreAuthScreen(df, entityId);
        } else if (entityType == 70) {//contributions
            presenceContributionsScreen(df, entityId);
        }
    }//end of presenenceAgent

    /**
     * bring up the claims screen
     * @param df
     * @param entityId
     */
    public void presenceClaimsScreen(DockingFramework df, int entityId) {
        df.ClaimsCaptureActions();
    /*if (entityExists(entityId)) {
    ClaimHeaderCapturing.persondisplayFields(entityId);
    }*/
    }//end of presenceClaimsScreen

    /**
     * bring up the Quotation screen
     * @param df
     * @param entityId
     */
    public void presenceQuotationScreen(DockingFramework df, int entityId) {

        df.AcceptQuoteActions();

    }//end of presenceQuotationScreen

    /**
     * bring up the Finance screen
     * @param df
     * @param entityId
     */
    public void presenceFinanceScreen(DockingFramework df, int entityId) {
        df.ClaimsCaptureActions();
    /*if (entityExists(entityId)) {
    ClaimHeaderCapturing.persondisplayFields(entityId);
    }*/
    }//end of presenceFinanceScreen

    /**
     * bring up the PRE-AUTH screen
     * @param df
     * @param entityId
     */
    public void presencePreAuthScreen(DockingFramework df, int entityId) {
        df.ClaimsCaptureActions();
    /*if (entityExists(entityId)) {
    ClaimHeaderCapturing.persondisplayFields(entityId);
    }*/
    }//end of presencePreAuthScreen

    /**
     * bring up the Contributions screen
     * @param df
     * @param entityId
     */
    public void presenceContributionsScreen(DockingFramework df, int entityId) {
        df.ClaimsCaptureActions();
    /*if (entityExists(entityId)) {
    ClaimHeaderCapturing.persondisplayFields(entityId);
    }*/
    }//end of presenceContributionsScreen

    /**
     * brings up the neo app in the Person or user entity screen
     * @param df
     * @param entityId
     */
    public void presenencPersonUser(DockingFramework df, int entityId) {
        df.EntityCreatePersonActions();
        if (entityExists(entityId)) {
            CreatePerson.persondisplayFields(entityId);
        }
    }//end of presenencPersonUser

    /**
     * brings up the neo app in the Company or administrtor entity screen
     * @param df
     * @param entityId
     */
    public void presenencCompanyAdministrator(DockingFramework df, int entityId) {
        df.EntityCreateAdministratorActions();
        if (entityExists(entityId)) {
            CreateAdministrator.companydisplayFields(entityId);
        }
    }//end of presenencCompanyAdministrator

    /**
     * brings up the neo app in the Practice entity screen
     * @param df
     * @param entityId
     */
    public void presenencPractice(DockingFramework df, int entityId) {
        df.EntityCreatePracticeActions();
        if (entityExists(entityId)) {
            CreatePractice.practicedisplayFields(entityId);
        }
    }//end of presenencPractice

    /**
     * brings up the neo app in the Provider entity screen
     * @param df
     * @param entityId
     */
    public void presenencProvider(DockingFramework df, int entityId) {
        df.EntityCreateProviderActions();
        if (entityExists(entityId)) {
            CreateProvider.persondisplayFields(entityId);
        }
    }//end of presenencProvider

    /**
     * brings up the neo app in the Policy_Holder entity screen
     * @param df
     * @param entityId
     */
    public void presenencPolicyHolder(DockingFramework df, int entityId) {
        df.EntityCreatePolicyHolderActions();
        if (entityExists(entityId)) {
            CreatePolicyholder.persondisplayFields(entityId);
        }
    }//end of presenencPolicyHolder

    /**
     * brings up the neo app in the Broker entity screen
     * @param df
     * @param entityId
     */
    public void presenencBroker(DockingFramework df, int entityId) {
        df.EntityCreateBrokerActions();
        if (entityExists(entityId)) {
            CreateBroker.persondisplayFields(entityId);
        }
    }//end of presenencBroker

    /**
     * brings up the neo app in the Broker_house entity screen
     * @param df
     * @param entityId
     */
    public void presenencBrokerHouse(DockingFramework df, int entityId) {
        df.EntityCreateBrokerHouseActions();
        if (entityExists(entityId)) {
            CreateBrokerHouse.brokerdisplayFields(entityId);
        }
    }//end of presenencBrokerHouse

    /**
     * brings up the neo app in the Insurer entity screen
     * @param df
     * @param entityId
     */
    public void presenencInsurer(DockingFramework df, int entityId) {
        df.EntityCreateInsurerActions();
        if (entityExists(entityId)) {
            CreateInsured.companydisplayFields(entityId);
        }
    }//end of presenencInsurer

    /**
     * brings up the neo app in the Agent entity screen
     * @param df
     * @param entityId
     */
    public void presenenceAgent(DockingFramework df, int entityId) {
        df.EntityCreateAgentActions();
        if (entityExists(entityId)) {
            CreateAgent.persondisplayFields(entityId);
        }
    }//end of presenenceAgent

    /**
     * brings up the neo app in the Sub_Branch entity screen
     * @param df
     * @param entityId
     */
    public void presenenceSubBranch(DockingFramework df, int entityId) {
        df.EntityCreateSubBranchActions();
        if (entityExists(entityId)) {
            CreateBrokerSubBranch.brokerdisplayFields(entityId);
        }
    }//end of presenenceSubBranch

    /**
     * brings up the neo app in the Insured_Persons entity screen
     * @param df
     * @param entityId
     */
    public void presenenceInsuredPerson(DockingFramework df, int entityId) {
        df.EntityCreateInsuredPersonActions();
        if (entityExists(entityId)) {
            CreateInsuredPerson.persondisplayFields(entityId);
        }

    }//end of presenenceInsuredPerson

    public boolean personExists(int entityId) {
        try {
            PersonContract pcontract = NeoWebService.getNeoManagerBeanService().getPersonContract(entityId);
            if (pcontract.getEntityDetails() == null) {
                return false;
            } else {
                Debug.println("INFO", "Perosn Entity : " + entityId + "exists");
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }//end of personExists

    public boolean companyExists(int entityId) {
        try {
            CompanyContract ccontract = NeoWebService.getNeoManagerBeanService().getCompanyContract(entityId);
            if (ccontract.getEntityDetails() == null) {
                return false;
            } else {
                Debug.println("INFO", "Compnay Entity : " + entityId + " exists");
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }//end of companyExists

    public boolean entityExists(int entityId) {

        boolean tf = (personExists(entityId) == false) ? companyExists(entityId) : true;
        if (tf == false) {
            JOptionPane.showMessageDialog(null, "Entity " + entityId + " does not exist", "Information", JOptionPane.INFORMATION_MESSAGE);
        }
        return tf;
    }//end of entityExist

    public static void setDocRecordNumber(String docRecordNumber) {
        DockingFramework.docRecordNumber = docRecordNumber;
    }

    public static String getDocRecordNumber() {
        return DockingFramework.docRecordNumber;
    }

    public static void setDocTypeNumber(String docTypeNumber) {
        DockingFramework.docTypeNumber = docTypeNumber;
    }

    public static String getDocTypeNumber() {
        return DockingFramework.docTypeNumber;
    }


    // Medcor - Entity Methods

    //search cover member
    public void CreateSearchCoverMemberActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new SearchCoverMember()));
        _frame.setVisible(true);

    }
    // Address Details

    public void CreateMemberAddressActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new CreateNewAddressDetails()));
        _frame.setVisible(true);

    }

    // View Cover Details
    public void ViewMemberDetailsActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ViewCoverPersonAllDetails()));
        _frame.setVisible(true);

    }

    public void ViewMemberDetailsActions(String coverNumber, int coverDetailsId, int entityCommId, boolean resigned, Collection<CoverDetails> cdList) {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ViewCoverPersonAllDetails(coverNumber, coverDetailsId, entityCommId, resigned, cdList)));
        _frame.setVisible(true);

    }

    public void ViewProviderDetailsActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ViewProviderDetails()));
        _frame.setVisible(true);

    }
    public void ViewProviderDetailsActions(int entityId) {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ViewProviderDetails(entityId)));
        _frame.setVisible(true);

    }
    public void ViewPracticeDetailsActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ViewPracticeDetails()));
        _frame.setVisible(true);

    }
    public void ViewPracticeDetailsActions(String pracNum) {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new ViewPracticeDetails(pracNum)));
        _frame.setVisible(true);

    }

    //Add Dependent Actions
    public void AddMemberDetailsActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new AddDependent()));
        _frame.setVisible(true);

    }

    // Med-core Policy Management
    public void medcorPolicyManagementActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new medcore.policyManagement.MedcorPolicyManagement()));
        _frame.setVisible(true);

    }

    public void medCorCoverPersonDetailsActions() {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new MedcorInsuredPersonPolicyDetails()));

        _frame.setVisible(true);
    }

    public void medCorCoverPersonDetailsActions(String coverNumber) {
        _frame.getDockingManager().getWorkspace().removeAll();
        _frame.getDockingManager().getWorkspace().add(new JScrollPane(new MedcorInsuredPersonPolicyDetails(coverNumber)));

        _frame.setVisible(true);
    }
}

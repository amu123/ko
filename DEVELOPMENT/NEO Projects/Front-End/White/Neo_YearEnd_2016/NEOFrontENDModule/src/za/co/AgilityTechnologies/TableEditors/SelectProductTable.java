/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.AgilityTechnologies.TableEditors;

/**
 *
 * @author AbigailM
 */
import java.awt.Component;



import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractCellEditor;

import javax.swing.JComboBox;

import javax.swing.JTable;

import javax.swing.table.TableCellEditor;
import neo.product.utils.NeoWebService;



public class SelectProductTable extends AbstractCellEditor implements TableCellEditor {

    private JComboBox cbComponent;

    public SelectProductTable() {

     //  NeoManagerBean neoManagerBean = DelegateWebServices.createNeoManagerBean();
        
        List<neo.manager.Product> prod = NeoWebService.getNeoManagerBeanService().fetchAllProducts();

        ArrayList stArr = new ArrayList();
        int i = 0;

        for (Iterator it = prod.iterator(); it.hasNext();) {

            neo.manager.Product nP = (neo.manager.Product) it.next();

             stArr.add(nP.getProductName());


            i++;

        }


       
        cbComponent = new JComboBox(stArr.toArray());


    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {

        return cbComponent;

    }

    public Object getCellEditorValue() {

        return cbComponent.getSelectedItem();

    }
    public JComboBox getCbComponent(){
        return cbComponent;
    }

}



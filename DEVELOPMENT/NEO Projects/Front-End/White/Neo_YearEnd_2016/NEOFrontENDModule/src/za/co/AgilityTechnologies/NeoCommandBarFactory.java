/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.AgilityTechnologies;

import com.jidesoft.action.*;
import com.jidesoft.icons.JideIconsFactory;

import java.awt.Dimension;
import javax.swing.*;

/**
 */
public class NeoCommandBarFactory extends CommandBarFactory {
    private static boolean _autohideAll = false;
    private static byte[] _fullScreenLayout;


    protected static CommandBar createStandardCommandBar() {
        CommandBar commandBar = new CommandBar("Standard");
        commandBar.setInitSide(DockableBarContext.DOCK_SIDE_NORTH);
      //  commandBar.setInitMode(DockableBarContext.STATE_HORI_DOCKED);
        commandBar.setInitIndex(1);
        commandBar.setStretch(true);
        commandBar.setToolTipText("command bar");
        commandBar.setBorderPainted(true);
        commandBar.setPaintBackground(true);
      //  commandBar.setMargin( new Insets(1, 1, 1, 1));
        //commandBar.setForeground(new java.awt.Color(1, 255, 233));
        commandBar.setBackground(new java.awt.Color(1, 255, 233));
System.out.println(commandBar.getBackground());

       // commandBar.setPaintBackground(true);
       
        //commandBar.
        commandBar.setOpaque(true);
       
        commandBar.add(createButton(JideIconsFactory.getImageIcon(JideIconsFactory.JIDE32)));
        //commandBar.add(createSplitButton(NeoIconsFactory.getImageIcon(NeoIconsFactory.Standard.ADD_NEW_ITEMS)));
        commandBar.add(createButton(NeoIconsFactory.getImageIcon(NeoIconsFactory.Standard.OPEN)));
        commandBar.add(createButton(NeoIconsFactory.getImageIcon(NeoIconsFactory.Standard.SAVE)));
        commandBar.add(createButton(NeoIconsFactory.getImageIcon(NeoIconsFactory.Standard.SAVE_ALL)));
        

        commandBar.add(createButton(NeoIconsFactory.getImageIcon(NeoIconsFactory.Standard.CUT)));
        commandBar.add(createButton(NeoIconsFactory.getImageIcon(NeoIconsFactory.Standard.COPY)));
        commandBar.add(createButton(NeoIconsFactory.getImageIcon(NeoIconsFactory.Standard.PASTE)));
        commandBar.add(createButton(NeoIconsFactory.getImageIcon(NeoIconsFactory.Standard.SOLUTION)));
        commandBar.add(createButton(NeoIconsFactory.getImageIcon(NeoIconsFactory.Standard.PROPERTY)));
        commandBar.add(createButton(NeoIconsFactory.getImageIcon(NeoIconsFactory.Standard.TOOLBOX)));
        //JideSplitButton splitButton = (JideSplitButton) commandBar.add(createSplitButton(NeoIconsFactory.getImageIcon(NeoIconsFactory.Standard.CLASSVIEW)));
        commandBar.addSeparator(new Dimension(5,1));

        commandBar.add(createButton(JideIconsFactory.getImageIcon(JideIconsFactory.JIDE32)));
        //JMenuItem item = splitButton.add(new JMenuItem("Class View", NeoIconsFactory.getImageIcon(NeoIconsFactory.Standard.CLASSVIEW)));
        //item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));

         commandBar.setVisible(true);
         return commandBar;
    }

  
}

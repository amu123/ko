/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.AgilityTechnologies.TableEditors;

/**
 *
 * @author BharathP
 */

 import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import neo.manager.NeoManagerBean;


public class BenefitsProductTable
extends AbstractCellEditor
implements TableCellEditor {

private JComboBox cbComponent;

public BenefitsProductTable() {

    

cbComponent = new JComboBox(new String[] {" ","Core"} );
}

public Component getTableCellEditorComponent(JTable table,
Object value, boolean isSelected, int row, int column) {
return cbComponent;
}

public Object getCellEditorValue() {
return cbComponent.getSelectedItem();
}
}

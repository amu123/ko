/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.AgilityTechnologies.TableEditors;

/**
 *
 * @author johanl
 */

import java.awt.Component;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import neo.product.utils.NeoWebService;



public class OptionType
extends AbstractCellEditor
implements TableCellEditor {

private JComboBox cbComponent;

public OptionType(int prodId) {
    
    List<neo.manager.Option> optType = NeoWebService.getNeoManagerBeanService().findOptionsForProduct(prodId);
        ArrayList stArr = new ArrayList();
        int i = 0;

        for (Iterator it = optType.iterator(); it.hasNext();) {

            neo.manager.Option opt = (neo.manager.Option) it.next();

             stArr.add(opt.getOptionName());


            i++;

        }
        cbComponent = new JComboBox(stArr.toArray());
}

public Component getTableCellEditorComponent(JTable table,
Object value, boolean isSelected, int row, int column) {
return cbComponent;
}

public Object getCellEditorValue() {
return cbComponent.getSelectedItem();
}


}


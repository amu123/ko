/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * PolicyActions.java
 *
 * Created on 2009/02/10, 04:46:24
 */
package za.co.AgilityTechnologies.LeftMenu;

import com.jidesoft.pane.CollapsiblePane;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import neo.manager.CompanyContract;
import neo.manager.PersonContract;
import neo.quotation.manageQuotation.ReviewOpenQuotations;
import za.co.AgilityTechnologies.DockingFramework;

/**
 *
 * @author BharathP
 */
public class QuotationsActionFrame extends javax.swing.JPanel {

    DockingFramework df;
 public static List<Integer> AddInsuredforPolicy = null;
  public static CompanyContract GLOBALCompanyContract = null;
    public static PersonContract GLOBALPersonContract = null;
    public static String searchedPerson = "";
    public static String quoteResults="";
    public static String policyNo="";

     public static Map<Integer, String> underAnsersMap = new HashMap<Integer, String>();

    public QuotationsActionFrame(DockingFramework pDD) {
        df = pDD;
        initComponents();
        
        AddInsuredforPolicy=new ArrayList();
         GLOBALPersonContract = new PersonContract();
        GLOBALCompanyContract = new CompanyContract();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        collapsiblePane4 = new com.jidesoft.pane.CollapsiblePane();
        createQuotationsPane = new com.jidesoft.pane.CollapsiblePane();
        jScrollPane1 = new javax.swing.JScrollPane();
        quotation = new javax.swing.JList();
        manageQuotationsPane = new com.jidesoft.pane.CollapsiblePane();
        jScrollPane2 = new javax.swing.JScrollPane();
        quotationStatus = new javax.swing.JList();

        setBackground(new java.awt.Color(255, 255, 255));

        collapsiblePane4.setBackground(new java.awt.Color(202, 212, 214));
        collapsiblePane4.setBorder(null);
        collapsiblePane4.setHorizontalAlignment(CollapsiblePane.CENTER);
        collapsiblePane4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/SideMenuLogos/briefcase2_document.png"))); // NOI18N
        collapsiblePane4.setTitle("New Applications");
        collapsiblePane4.setFont(new java.awt.Font("Arial", 0, 11));

        createQuotationsPane.setBackground(new java.awt.Color(202, 212, 214));
        createQuotationsPane.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/layout/send_to_back.gif"))); // NOI18N
        createQuotationsPane.setStyle(CollapsiblePane.TREE_STYLE);
        createQuotationsPane.setTitle("Create New Applications");

        quotation.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Create a New Application", "Capture Individuals Details", "Assign Options To New Application", "Input Underwriting Details", "Review New Application Input Data", "Review the Application Results", "Send New Application" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        quotation.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        quotation.setSelectionBackground(new java.awt.Color(138, 177, 230));
        quotation.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                quotationValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(quotation);

        javax.swing.GroupLayout createQuotationsPaneLayout = new javax.swing.GroupLayout(createQuotationsPane.getContentPane());
        createQuotationsPane.getContentPane().setLayout(createQuotationsPaneLayout);
        createQuotationsPaneLayout.setHorizontalGroup(
            createQuotationsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
        );
        createQuotationsPaneLayout.setVerticalGroup(
            createQuotationsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(createQuotationsPaneLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        manageQuotationsPane.setBackground(new java.awt.Color(202, 212, 214));
        manageQuotationsPane.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/layout/send_to_back.gif"))); // NOI18N
        manageQuotationsPane.setStyle(CollapsiblePane.TREE_STYLE);
        manageQuotationsPane.setTitle("Manage Applications");
        manageQuotationsPane.addCollapsiblePaneListener(new com.jidesoft.pane.event.CollapsiblePaneListener() {
            public void paneExpanding(com.jidesoft.pane.event.CollapsiblePaneEvent evt) {
                manageQuotationsPanePaneExpanding(evt);
            }
            public void paneExpanded(com.jidesoft.pane.event.CollapsiblePaneEvent evt) {
            }
            public void paneCollapsing(com.jidesoft.pane.event.CollapsiblePaneEvent evt) {
            }
            public void paneCollapsed(com.jidesoft.pane.event.CollapsiblePaneEvent evt) {
            }
        });

        quotationStatus.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Review Open Application", "Reject Application", "Accept Application", "Import Census Details" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        quotationStatus.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        quotationStatus.setSelectionBackground(new java.awt.Color(138, 177, 230));
        quotationStatus.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                quotationStatusValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(quotationStatus);

        javax.swing.GroupLayout manageQuotationsPaneLayout = new javax.swing.GroupLayout(manageQuotationsPane.getContentPane());
        manageQuotationsPane.getContentPane().setLayout(manageQuotationsPaneLayout);
        manageQuotationsPaneLayout.setHorizontalGroup(
            manageQuotationsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
        );
        manageQuotationsPaneLayout.setVerticalGroup(
            manageQuotationsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(manageQuotationsPaneLayout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout collapsiblePane4Layout = new javax.swing.GroupLayout(collapsiblePane4.getContentPane());
        collapsiblePane4.getContentPane().setLayout(collapsiblePane4Layout);
        collapsiblePane4Layout.setHorizontalGroup(
            collapsiblePane4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(collapsiblePane4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(collapsiblePane4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(createQuotationsPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(manageQuotationsPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        collapsiblePane4Layout.setVerticalGroup(
            collapsiblePane4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(collapsiblePane4Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(createQuotationsPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(manageQuotationsPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(collapsiblePane4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(collapsiblePane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void quotationValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_quotationValueChanged
        // TODO add your handling code here:

        if (evt.toString().toUpperCase().contains("ISADJUSTING= TRUE")) {
            quotationStatus.clearSelection();
            System.out.println("inside event1");

        } else {
            //Create a New Quotation
            if (quotation.getSelectedIndex() == 0) {
                df.CreateNewQuotation();
            }

            //Capture Individuals details
            if (quotation.getSelectedIndex() == 1)
           
            {
                df.CaptureIndividualsToQuotation();
            }




            //Assign Options To A Quote
            if (quotation.getSelectedIndex() == 2) {
                df.AssignOptionsToQuoteActions();
            }

            //Input Underwriting Details
            if (quotation.getSelectedIndex() == 3) {
                df.InputUnderwritingActions();
            }

            //Review Quote Input Data
            if (quotation.getSelectedIndex() == 4) {
                df.ReviewQuoteInputActions();
            }

            //Review the Quotation Results
            if (quotation.getSelectedIndex() == 5) {
                df.ReviewQuoteResultsActions();
            }

            //Send Quotation
            if (quotation.getSelectedIndex() == 6) {
                df.SendQuoteActions();
            }

        }
}//GEN-LAST:event_quotationValueChanged

    private void manageQuotationsPanePaneExpanding(com.jidesoft.pane.event.CollapsiblePaneEvent evt) {//GEN-FIRST:event_manageQuotationsPanePaneExpanding
        // TODO add your handling code here:
        // quotation.clearSelection();
        //  quotationStatus.clearSelection();
}//GEN-LAST:event_manageQuotationsPanePaneExpanding

    private void quotationStatusValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_quotationStatusValueChanged
        // TODO add your handling code here:

        if (evt.toString().toUpperCase().contains("ISADJUSTING= TRUE")) {
            quotation.clearSelection();
            System.out.println("inside event1");

        } else {
            /*Review Open Quotations
            Follow Up Communication
            Reject Quotation
            Accept Quotation
            Import Census Details
            Review Census Pricing
            Reports*/
            /*// Renew an Existing Policy
            if(quotationStatus.getSelectedIndex()==0)
            df.RenewalPolicyActions();

            //   Add Individuals to an Existing Policy
            if(quotationStatus.getSelectedIndex()==1)
            df.RenewalAddIndividualsPolicyActions();*/
            // Review Open Quotations;
            if (quotationStatus.getSelectedIndex() == 0) {
                df.OpenReviewQuoteActions();
                if(!(quoteResults.equalsIgnoreCase("single Quote")))
                ReviewOpenQuotations.getQuotationData();
            }

            /*//  Follow Up Communication;
            if (quotationStatus.getSelectedIndex() == 1) {
            df.FollowupCommQuoteActions();
            }*/
            // Reject Quotation;
            if (quotationStatus.getSelectedIndex() == 1) {
                df.RejectQuoteActions();
            }
            // Accept Quotation;
            if (quotationStatus.getSelectedIndex() == 2) {
                df.AcceptQuoteActions();
            }
            // Import Census Details;
            if (quotationStatus.getSelectedIndex() == 3) {
                df.ImportCensusQuoteActions();
            }
            /*// Review Census Pricing
            if (quotationStatus.getSelectedIndex() == 4) {
            df.ReviewCensusPriceQuoteActions();
            }
            // Reports
            if (quotationStatus.getSelectedIndex() == 5) {
            System.out.println("reports pending");
            }
            //df.ReviewCensusPriceQuoteActions();*/

        }
}//GEN-LAST:event_quotationStatusValueChanged






    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.jidesoft.pane.CollapsiblePane collapsiblePane4;
    private com.jidesoft.pane.CollapsiblePane createQuotationsPane;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    public static com.jidesoft.pane.CollapsiblePane manageQuotationsPane;
    public static javax.swing.JList quotation;
    public static javax.swing.JList quotationStatus;
    // End of variables declaration//GEN-END:variables
/*public static class NeoManagerBeanFactory{
    public static NeoManagerBean neoMangerBean=DelegateWebServices.createNeoManagerBean();


    }*/

}

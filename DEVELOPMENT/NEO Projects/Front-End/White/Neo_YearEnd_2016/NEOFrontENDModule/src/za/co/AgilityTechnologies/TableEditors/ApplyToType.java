/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.AgilityTechnologies.TableEditors;

/**
 *
 * @author johanl
 */
import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

public class ApplyToType
        extends AbstractCellEditor
        implements TableCellEditor {

    private JComboBox cbComponent;

    public ApplyToType() {
        neo.manager.ApplyToType[] applyToType = neo.manager.ApplyToType.values();

        cbComponent = new JComboBox(applyToType);
    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {
        return cbComponent;
    }

    public Object getCellEditorValue() {
        return cbComponent.getSelectedItem();
    }
}


package za.co.AgilityTechnologies.dragandDrop;

import java.util.List;

/**
 * This is an utility class for Tree task.
 * 
 * @author Administrator
 *
 */
public class NeoUtils
{
	/**
	 * This method checks whether any options can be added to the given row.
	 * 
	 * @param givenRow
	 * @return true/false
	 */
	public static boolean canAddOptionsToRow(NeoTreeTableRow givenRow)
	{
		//Check if the node type is "NAME"
		if(givenRow == null) 
		{
			return false;
		}
		
		if( givenRow.getNodeType() == NeoTreeTableRow.NODE_TYPE_NAME || 
			givenRow.getNodeType() == NeoTreeTableRow.NODE_TYPE_DEPARTMENT)
		{
			return true;
		}
		
		return false;
	}
	
	/**
	 * This method checks if the given child row already exists for the give
	 * parent row.
	 * 
	 * @param parentRow
	 * @param childRow
	 * @return
	 */
	public static boolean isRowAlreadyExists(NeoTreeTableRow parentRow, NeoTreeTableRow childRow)
	{
		if(parentRow == null || childRow == null)
		{
			return false;
		}
		
		//Get the Child Option value to add
		String strChildOptionValue = (String) childRow.getValueAt(0);

		//Get the children of the parent row
		List<NeoTreeTableRow> children = (List<NeoTreeTableRow>) parentRow.getChildren();
		if(children == null || children.size() == 0)
		{
			return false;
		}
		
		/* Check, if the child option value already exists as one
		of the children of the parent row. */
		for(NeoTreeTableRow treeTableRow : children)
		{
			String strOptionValue = (String) treeTableRow.getValueAt(0);
			if(strOptionValue.equals(strChildOptionValue))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * This method returns the child row with given name by searching in the given parent row.
	 * It not found, it returns null.
	 * 
	 * @param parentRow
	 * @param strName
	 * @return
	 */
	public static NeoTreeTableRow getChildRowWithName(NeoTreeTableRow parentRow, String strName)
	{
		if(parentRow == null || ! NeoUtils.isValidString(strName))
		{
			return null;
		}
		
		//Get the children of the parent row
		List<NeoTreeTableRow> children = (List<NeoTreeTableRow>) parentRow.getChildren();
		if(children == null || children.size() == 0)
		{
			return null;
		}
		
		/* Check, if the child option value already exists as one
		of the children of the parent row. */
		for(NeoTreeTableRow treeTableRow : children)
		{
			String strOptionValue = (String) treeTableRow.getValueAt(0);
			if(strOptionValue.equals(strName))
			{
				return treeTableRow;
			}
		}
		return null;
	}
	
	
	/**
	 * This method checks if the given string is valid or not.
	 * 
	 * @param strValue
	 * @return true/false
	 */
	public static boolean isValidString(String strValue)
	{
		if(strValue == null || strValue.equalsIgnoreCase("") || strValue.trim().length() == 0)
		{
			return false;
		}
		return true;
	}
}

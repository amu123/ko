/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * private com.jidesoft.swing.AutoCompletionComboBox autoCompletionComboBox1;
 */
package za.co.AgilityTechnologies.TableEditors;

/**
 *
 * @author DeonP
 */
import com.jidesoft.swing.AutoCompletionComboBox;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
//import neo.manager.EntityDetails;
import neo.manager.CompanyDetails;
import neo.manager.EntitySearchCriteria;
import neo.product.utils.NeoWebService;


public class PracticeComboTable
        extends AbstractCellEditor
        implements TableCellEditor {

    private JComboBox cbComponent;
    private com.jidesoft.swing.AutoCompletionComboBox autoCompletionComboBox;

    public PracticeComboTable() {

        
        
        neo.manager.EntitySearchCriteria practiceSearch = new EntitySearchCriteria();

        practiceSearch.setContractType(neo.manager.ContractType.PRACTICE);
        practiceSearch.setEntityType(neo.manager.EntityType.COMPANY);

        List<CompanyDetails> result = NeoWebService.getNeoManagerBeanService().findCompaniesWithSearchCriteria(practiceSearch);

        System.out.println(result. toString());

        

        autoCompletionComboBox = new AutoCompletionComboBox(new ComboPopulator(result).comboPopList.toArray());
        autoCompletionComboBox.addItem("");

    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {
        return autoCompletionComboBox;
    }

    public Object getCellEditorValue() {
        return autoCompletionComboBox.getSelectedItem();

    }

    public class ComboPopulator {
        public int id;
        public String name;

        Collection<ComboPopulator> comboPopList = new ArrayList<ComboPopulator>();

        public ComboPopulator(int id,String name) {
            this.id = id;
            this.name = id + " - " + name;
        }

        public ComboPopulator(Collection<CompanyDetails> list) {
            for (CompanyDetails entity : list) {
              ComboPopulator tmp = new ComboPopulator(entity.getEntityId(),entity.getName());
              comboPopList.add(tmp);
            }
        }

        public String toString() {
            return name;
        }
     }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.AgilityTechnologies.TableEditors;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author gerritj
 */
public class AcceptRejectColumnEditor extends AbstractCellEditor
        implements TableCellEditor, ActionListener{

    static String[] options = {"Reject", "Accept"};
    private JComboBox cbComponent;

    public AcceptRejectColumnEditor() {
        cbComponent = new JComboBox(options);
        cbComponent.addActionListener(this);
    }

    public Object getCellEditorValue() {
        return cbComponent.getSelectedItem();
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        return cbComponent;
    }

    public void actionPerformed(ActionEvent e) {
        System.out.println("event = " + cbComponent.getSelectedItem());
        this.fireEditingStopped();
    }
}

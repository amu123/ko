/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.AgilityTechnologies;

/**
 *
 * @author graydon
 */
import java.awt.Dimension;
import java.util.*;

import javax.swing.*;

import com.jidesoft.grid.*;
import com.jidesoft.swing.JideSwingUtilities;

public class TreeTableTest {
   public static void main(String[] args) {
      JFrame f = new JFrame("Table test!");
      f.setSize(4000, 4000);
      MyTableModel model = new MyTableModel();
      initModel(model);
      TreeTable treeTable = new TreeTable(model);
      treeTable.setExpandAllAllowed(false);
      treeTable.setRowHeight(18);
      treeTable.setShowTreeLines(true);
      treeTable.setShowGrid(false);
      treeTable.setIntercellSpacing(new Dimension(0, 0));

      JScrollPane jP = new JScrollPane(treeTable);
      f.setContentPane(jP);
      f.pack();
      f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      JideSwingUtilities.globalCenterWindow(f);
      f.setVisible(true);
   }

   protected static void initModel(MyTableModel model) {

      ArrayList<ExpandableRow> rows = new ArrayList<ExpandableRow>();
      for (int i = 0; i < 3; i++) {
         Object[] rowData = { "name " + i };
         rows.add(new MyElementRow(i + 1, rowData, false));
      }
      model.setOriginalRows(rows);
   }

   public static class MyTableModel extends TreeTableModel {
      private String[] columnNames;

      public MyTableModel() {
         columnNames = new String[] { "name", "prop1", "prop2", "prop3" };
      }

      @Override
      public String getColumnName(int column) {
         if (column >= 0 && column < columnNames.length)
            return columnNames[column];
         return null;
      }

      public int getColumnCount() {
         return columnNames.length;
      }

   }

   public static class MyElementRow extends DefaultExpandableRow {
      protected Object[] rowValues;
      protected long childs;
      Random r = new Random();

      public MyElementRow(long childs, Object[] values, boolean leafRow) {
         this.childs = childs;
         this.rowValues = values;
         setExpandable(!leafRow);
         if (!leafRow) {
            ArrayList<Row> list = new ArrayList<Row>();
            for (int i = 0; i < childs; i++) {
               Object[] rowData = { "", r.nextInt(20), r.nextBoolean(), r.nextInt(20) };
               Row row = new MyElementRow(i, rowData, true);
               row.setParent(this);
               list.add(row);
            }

            setChildren(list);
         }
      }

      public Object getValueAt(int columnIndex) {
         if (rowValues != null && columnIndex >= 0 && columnIndex < rowValues.length)
            return rowValues[columnIndex];
         return null;
      }

      public boolean hasChildren() {
         return isExpandable() && getChildrenCount() > 0;
      }
   }
}

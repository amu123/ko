package za.co.AgilityTechnologies.dragandDrop;

import com.jidesoft.grid.DefaultExpandableRow;
import java.util.List;

/**
 * This class represents each row/node in the tree table.
 * 
 * @author Administrator
 *
 */
public class NeoTreeTableRow extends DefaultExpandableRow  implements Comparable<NeoTreeTableRow>
{
	/**
	 * Member to the hold the node type
	 */
	protected int _nodeType = -1;
	
	/**
	 * Member to hold the row/node title
	 */
	protected String _strTitle = null;
	
	/*
	 * Node Types
	 */
	public static final int NODE_TYPE_GROUP = 0;
	public static final int NODE_TYPE_DEPARTMENT = 1;
	public static final int NODE_TYPE_NAME = 2;
	public static final int NODE_TYPE_OPTION = 3;
	
        protected List<String> _lstOfBenefits = null;
        
	/**
	 * Constructor
	 * 
	 * @param strTitle
	 * @param nodeType
	 */
	public NeoTreeTableRow(String strTitle, int nodeType)
	{
		super();
		_strTitle = strTitle;
		_nodeType = nodeType;
	}
	
	/**
	 * Returns the Node type
	 * @return
	 */
	public int getNodeType()
	{
		return _nodeType;
	}

	public String getTitle()
	{
		return _strTitle;
	}

	/**
	 * Returns the value at given column index
	 */
	public Object getValueAt(int colIndex) 
	{
		//Always, return title. Since, there is only one column
		return _strTitle;
	}

	public int compareTo(NeoTreeTableRow o) {
		NeoTreeTableRow row = o;
	     return getTitle().compareToIgnoreCase(row.getTitle());
	}
        
        public void setBenefits(List<String> lstOfBenefits)
        {
            _lstOfBenefits = lstOfBenefits;
        }
        
        public List<String> getBenefits()
        {
            return _lstOfBenefits;
        }
}

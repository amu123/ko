/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.AgilityTechnologies.TableEditors;

  import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;


public class OperatorProductTable
extends AbstractCellEditor
implements TableCellEditor {

private JComboBox cbComponent;

public OperatorProductTable() {

cbComponent = new JComboBox(new String[] {"is equal to" ,"is not equal to"} );
}

public Component getTableCellEditorComponent(JTable table,
Object value, boolean isSelected, int row, int column) {
return cbComponent;
}

public Object getCellEditorValue() {
return cbComponent.getSelectedItem();
}


}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * PreviousInsurenceViewFrame.java
 *
 * Created on 2009/10/08, 11:34:13
 */
package medcor.entity.view;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.EntityTimeDetails;
import neo.manager.WaitingPeriodDetails;
import neo.product.utils.NeoWebService;
import neo.util.UtilMethods;
import za.co.AgilityTechnologies.NeoMessageDialog;

/**
 *
 * @author BharathP
 */
public class UnderwritingViewFrame extends javax.swing.JFrame {

    /** Creates new form PreviousInsurenceViewFrame */
    public UnderwritingViewFrame() {
        initComponents();

    }

    public UnderwritingViewFrame(int entityId) {
        initComponents();
        loadCoverUnderwriting(entityId);
        System.out.println("entityId recieved for underwriting = " + entityId);
    }

    public void loadCoverUnderwriting(int entityId) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date startDate = null;
        Date endDate = null;
        DefaultTableModel displayTableModel = (DefaultTableModel) tbl_UnderwritingView.getModel();
        int totRows = displayTableModel.getRowCount();
        if (totRows > 0) {
            tbl_UnderwritingView.setModel(new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                        "Waiting Period", "ICD Code", "Date From", "Date To", "Duration", "PMB Allowed", "Tariff", "Description", "Cost"
                    }) {

                boolean[] canEdit = new boolean[]{
                    true, false, true, true, true, false, true, true, true
                };

                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            });

        }
        int x = 0;
        List<WaitingPeriodDetails> wpDetailsList = NeoWebService.getNeoManagerBeanService().getWaitingPeriodDetails(entityId);
        int waitingListSize = wpDetailsList.size();
        System.out.println("wpDetail size = " + waitingListSize);
        if (waitingListSize > 0) {
            for (WaitingPeriodDetails wpDetails : wpDetailsList) {
                displayTableModel.addRow(new Object[]{});

                tbl_UnderwritingView.setValueAt(wpDetails.getExclusionType().getValue(), x, 0);
                tbl_UnderwritingView.setValueAt(wpDetails.getDiagnosisCode(), x, 1);
                tbl_UnderwritingView.setValueAt(sdf.format(wpDetails.getEffectiveStartDate().toGregorianCalendar().getTime()), x, 2);
                tbl_UnderwritingView.setValueAt(sdf.format(wpDetails.getEffectiveEndDate().toGregorianCalendar().getTime()), x, 3);
                //calculate duration
                startDate = wpDetails.getEffectiveStartDate().toGregorianCalendar().getTime();
                endDate = wpDetails.getEffectiveEndDate().toGregorianCalendar().getTime();
                XMLGregorianCalendar xmlFrom = UtilMethods.convertDateToXMLGeoCalender(startDate);
                XMLGregorianCalendar xmlTo = UtilMethods.convertDateToXMLGeoCalender(endDate);
                EntityTimeDetails etd = NeoWebService.getNeoManagerBeanService().getTimeDifference(xmlFrom, xmlTo);
                System.out.println("year = " + etd.getYears());
                System.out.println("month = " + etd.getMonths());
                System.out.println("day = " + etd.getDays());

                int newMonth = 0;
                int newYear = 0;

                if ((etd.getDays() == 30) | (etd.getDays() == 31) | (etd.getDays() == 27) | (etd.getDays() == 28)) {
                    newMonth = etd.getMonths() + 1;
                    if (newMonth == 12) {
                        newMonth = 0;
                        newYear = etd.getYears() + 1;
                    }

                } else {
                    newMonth = etd.getMonths();
                    newYear = etd.getYears();
                }

                String timeDiff = "";
                if (!(newYear == 0)) {
                    timeDiff = timeDiff + " " + newYear + " year(s)";
                }
                if (!(newMonth == 0)) {
                    timeDiff = timeDiff + " " + newMonth + " month(s)";
                }
                tbl_UnderwritingView.setValueAt(timeDiff, x, 4);
                tbl_UnderwritingView.setValueAt(wpDetails.getPbmAllowed().getValue(), x, 5);

                x++;
            }
        } else {
            NeoMessageDialog.showInfoMessage("No Waiting Period");
            System.out.println("before");
            this.setVisible(false);
            this.dispose();
            System.out.println("after");
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titlePanel1 = new javax.swing.JPanel();
        styledLabel2 = new com.jidesoft.swing.StyledLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_UnderwritingView = new com.jidesoft.grid.JideTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Underwriting Details");
        setBackground(new java.awt.Color(235, 235, 235));
        setName("frame"); // NOI18N
        setState(2);

        titlePanel1.setBackground(new java.awt.Color(138, 177, 230));

        styledLabel2.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel2.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        styledLabel2.setText("UNDERWRITING DETAILS");
        styledLabel2.setFont(new java.awt.Font("Arial", 1, 14));
        styledLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titlePanel1.add(styledLabel2);

        tbl_UnderwritingView.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Waiting Period", "ICD Code", "Date From", "Date To", "Duration", "PMB Allowed", "Tariff", "Description", "Cost"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, true, true, true, false, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tbl_UnderwritingView);
        tbl_UnderwritingView.getColumnModel().getColumn(6).setMinWidth(0);
        tbl_UnderwritingView.getColumnModel().getColumn(6).setPreferredWidth(0);
        tbl_UnderwritingView.getColumnModel().getColumn(6).setMaxWidth(0);
        tbl_UnderwritingView.getColumnModel().getColumn(7).setMinWidth(0);
        tbl_UnderwritingView.getColumnModel().getColumn(7).setPreferredWidth(0);
        tbl_UnderwritingView.getColumnModel().getColumn(7).setMaxWidth(0);
        tbl_UnderwritingView.getColumnModel().getColumn(8).setMinWidth(0);
        tbl_UnderwritingView.getColumnModel().getColumn(8).setPreferredWidth(0);
        tbl_UnderwritingView.getColumnModel().getColumn(8).setMaxWidth(0);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 834, Short.MAX_VALUE)
                    .addComponent(titlePanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 834, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titlePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new UnderwritingViewFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private com.jidesoft.swing.StyledLabel styledLabel2;
    private com.jidesoft.grid.JideTable tbl_UnderwritingView;
    private javax.swing.JPanel titlePanel1;
    // End of variables declaration//GEN-END:variables
}

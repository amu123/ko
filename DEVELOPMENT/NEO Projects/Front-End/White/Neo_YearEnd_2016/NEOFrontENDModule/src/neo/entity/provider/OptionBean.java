/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neo.entity.provider;

import java.io.Serializable;

/**
 *
 * @author johanl
 */
public class OptionBean implements Serializable{

    private String optionName;

    public OptionBean(){}
    
    public OptionBean(String optionName){
        this.optionName = optionName;

    }
    public void setOptionName(String optionName){
        this.optionName = optionName;

    }
    public String getOptionName(){
        return this.optionName;

    }


}

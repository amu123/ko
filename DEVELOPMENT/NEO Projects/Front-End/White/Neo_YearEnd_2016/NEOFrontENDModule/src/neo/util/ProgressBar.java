package neo.util;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.ProgressMonitor;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import neo.entity.search.SearchDocsFrame;

/**
 *
 * @author philipb
 */
public class ProgressBar implements ActionListener {

    static ProgressMonitor pMon;
    static int counter = 0;
    public boolean done=false;
    public int canceled=0;
    public String docType;
    public String indNo;
   public Timer timer=null;

    public void ProgressBar(Component parentComponent,String documentType,String indNumber) {
        docType=documentType;
        indNo=indNumber;
        counter = 0;
        UIManager.put("ProgressMonitor.progressText", "Document Retrievel");
        UIManager.put("OptionPane.cancelButtonText", "Cancel");
        pMon = new ProgressMonitor(parentComponent, "Quotaion",
                "Initializing . . .", 0, 100);       
        // Fire a timer every once in a while to update the progress.
        timer = new Timer(2000, this);
        timer.start();
    }

    public void actionPerformed(ActionEvent e) {
        // Invoked by the timer every half second. Simply place
        // the progress monitor update on the event queue.
        SwingUtilities.invokeLater(new Update());
    }

    class Update implements Runnable {

        public void run() {           
            if(counter==100){
                new SearchDocsFrame().viewDoc(docType, indNo);
                timer.stop();
            }
            if (pMon.isCanceled()) {
                pMon.close();
                canceled=1;
                counter=100;
                timer.stop();
            }            
            pMon.setProgress(counter);
            pMon.setNote("Document Retrievel is " + counter + "% complete");
            counter += 2;            
        }
    }
}

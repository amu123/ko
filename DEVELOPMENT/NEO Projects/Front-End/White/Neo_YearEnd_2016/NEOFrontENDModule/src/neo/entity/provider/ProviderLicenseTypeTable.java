/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neo.entity.provider;

/**
 *
 * @author BharathP
 */

import za.co.AgilityTechnologies.TableEditors.*;
  import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;



public class ProviderLicenseTypeTable
extends AbstractCellEditor
implements TableCellEditor {

private JComboBox cbComponent;

public ProviderLicenseTypeTable() {
//neo.manager.ProviderLicenseType ptypes[]=ProviderLicenseType.values();
//cbComponent = new JComboBox(ptypes);
}

public Component getTableCellEditorComponent(JTable table,
Object value, boolean isSelected, int row, int column) {
return cbComponent;
}

public Object getCellEditorValue() {
return cbComponent.getSelectedItem();
}


}


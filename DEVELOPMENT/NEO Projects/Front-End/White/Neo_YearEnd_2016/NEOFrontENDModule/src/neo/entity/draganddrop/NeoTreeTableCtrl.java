package neo.entity.draganddrop;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JOptionPane;

/**
 * This class acts as a controller class for Tree task
 * 
 * @author Administrator
 *
 */
public class NeoTreeTableCtrl
{
	/**
	 * Tree table model instance
	 */
	NeoTreeTableModel _neoTreeTableModel = null;
	
	/**
	 * Tree view instance
	 */
	NeoTreeViewPanel _neoTreeViewPanel = null;
	
	
	/**
	 * Constructor
	 * 
	 * @param neoTreeTableModel
	 * @param neoTreeViewPanel
	 */
	public NeoTreeTableCtrl(NeoTreeTableModel neoTreeTableModel,NeoTreeViewPanel neoTreeViewPanel )
	{
		_neoTreeTableModel = neoTreeTableModel;
		_neoTreeViewPanel = neoTreeViewPanel;
	}

	/**
	 * Call back for <strong>Select Option</strong> button action.
	 * 
	 * @throws Exception
	 */
	public void onSelectOptionCB() throws Exception
	{
		if(_neoTreeViewPanel == null || _neoTreeTableModel == null)
		{
			return;
		}
		
		int []selRowIndices = _neoTreeViewPanel.getSelectedRows();
		
		if(selRowIndices == null)
		{
			return;
		}
		
		List<NeoTreeTableRow> lstOfSelectedRows = new ArrayList<NeoTreeTableRow>();
		NeoTreeTableRow selectedRow = null;
		
		//Store all the selected rows
		for(int selRowIndex : selRowIndices)
		{
			selectedRow = (NeoTreeTableRow) _neoTreeTableModel.getRowAt(selRowIndex);
			//Check if we can add the options to this row.
			if( (selRowIndex == -1) || !NeoUtils.canAddOptionsToRow(selectedRow) )
			{
				continue;
			}
			
			if(selectedRow.getNodeType() == NeoTreeTableRow.NODE_TYPE_ROLE)
			{
				List<NeoTreeTableRow> childRows = (List<NeoTreeTableRow>) selectedRow.getChildren();
				for(NeoTreeTableRow childRow : childRows)
				{
					lstOfSelectedRows.add(childRow);
				}
			}
			else
			{
				lstOfSelectedRows.add(selectedRow);
			}
		}
		List<String> lstSelValues = _neoTreeViewPanel.getSelectedOptions();
		this.addOptionsToRows(lstOfSelectedRows, lstSelValues);
	}
	
	
	private void addOptionsToRows(List<NeoTreeTableRow> lstOfRows, List<String> lstOfOptions)
	{
		for(NeoTreeTableRow eachRow : lstOfRows)
		{
			for(String strOption : lstOfOptions)
			{
				NeoTreeTableRow newChildRow = new NeoTreeTableRow(strOption,NeoTreeTableRow.NODE_TYPE_NAME);
				//Check if the row already exists
				if(! NeoUtils.isRowAlreadyExists(eachRow, newChildRow))
				{
					eachRow.addChild(newChildRow);
				}
				else
				{
					String strMsg = "The Option: " + newChildRow.getTitle() + 
						" already assigned to insurance holder - " + eachRow.getTitle();
					JOptionPane.showMessageDialog(null, strMsg, "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}
	
	/**
	 * Call back for <strong>Drop</strong> action on tree table.
	 * 
	 * @param strData
	 * @throws Exception
	 */
	public void onOptionsDrop(String strData) throws Exception
	{
		if(_neoTreeViewPanel == null || _neoTreeTableModel == null)
		{
			return;
		}
		
		if(! NeoUtils.isValidString(strData))
		{
			return;
		}
		
		//Get the options to be dropped on tree table.
		String[] values = strData.split(Character.toString(new Character('\n')));
		
		if(values == null)
		{
			return;
		}
		NeoTreeTableRow selectedRow = null;
		
		List<String> lstSelValues = new ArrayList<String>();
		
		for(String strVal : values)
		{
			lstSelValues.add(strVal);
		}
		
		//Get the selected row
		int []selRowIndices = _neoTreeViewPanel.getSelectedRows();
		selectedRow = (NeoTreeTableRow) _neoTreeTableModel.getRowAt(selRowIndices[0]);
		
		//Check if we can add the options to this row.
		if( ! NeoUtils.canAddOptionsToRow(selectedRow) )
		{
			return;
		}
		
		List<NeoTreeTableRow> lstOfSelectedRows = new ArrayList<NeoTreeTableRow>();
		if(selectedRow.getNodeType() == NeoTreeTableRow.NODE_TYPE_ROLE)
		{
			List<NeoTreeTableRow> childRows = (List<NeoTreeTableRow>) selectedRow.getChildren();
			for(NeoTreeTableRow childRow : childRows)
			{
				lstOfSelectedRows.add(childRow);
			}
		}
		else
		{
			lstOfSelectedRows.add(selectedRow);
		}
		
		this.addOptionsToRows(lstOfSelectedRows, lstSelValues);
			
	}
	
	public void onDeleteOptionCB()
	{
		//Get the selected row
		int []selRowIndices = _neoTreeViewPanel.getSelectedRows();
		
		List<NeoTreeTableRow> lstOfSelectedRows = new ArrayList<NeoTreeTableRow>();
		NeoTreeTableRow selectedRow = null;
		for(int selRowIndex : selRowIndices)
		{
			selectedRow = (NeoTreeTableRow) _neoTreeTableModel.getRowAt(selRowIndex);
			lstOfSelectedRows.add(selectedRow);
		}
		
		//Store all the selected rows
		for(NeoTreeTableRow selRow : lstOfSelectedRows)
		{
			//Check if we can add the options to this row.
			if( selRow.getNodeType() == NeoTreeTableRow.NODE_TYPE_NAME )
			{
				selRow.getParent().removeChild(selRow);
			}
		}
		
	}
}

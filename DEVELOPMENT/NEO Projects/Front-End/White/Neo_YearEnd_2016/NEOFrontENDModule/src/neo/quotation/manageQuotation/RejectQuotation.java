/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ReviewQuoteResults.java
 *
 * Created on 2009/05/28, 10:57:42
 */

package neo.quotation.manageQuotation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import neo.manager.EntityType;
import neo.manager.PolicyDetails;
import neo.product.utils.NeoWebService;
import za.co.AgilityTechnologies.LeftMenu.QuotationsActionFrame;


/**
 *
 * @author BharathP
 */
public class RejectQuotation extends javax.swing.JPanel {

    /** Creates new form ReviewQuoteResults */
    public RejectQuotation() {

        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        styledLabel1 = new com.jidesoft.swing.StyledLabel();
        styledLabel17 = new com.jidesoft.swing.StyledLabel();
        styledLabel18 = new com.jidesoft.swing.StyledLabel();
        styledLabel19 = new com.jidesoft.swing.StyledLabel();
        styledLabel8 = new com.jidesoft.swing.StyledLabel();
        styledLabel12 = new com.jidesoft.swing.StyledLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        checkBoxList1 = new com.jidesoft.swing.CheckBoxList();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jideButton1 = new com.jidesoft.swing.JideButton();
        jideButton2 = new com.jidesoft.swing.JideButton();
        jPanel2 = new javax.swing.JPanel();
        styledLabel2 = new com.jidesoft.swing.StyledLabel();
        styledLabel3 = new com.jidesoft.swing.StyledLabel();
        dateandTime = new javax.swing.JTextField();
        username = new javax.swing.JTextField();
        styledLabel4 = new com.jidesoft.swing.StyledLabel();
        productName = new javax.swing.JTextField();
        styledLabel5 = new com.jidesoft.swing.StyledLabel();
        totalPremium = new javax.swing.JTextField();
        styledLabel6 = new com.jidesoft.swing.StyledLabel();
        policyHolder = new javax.swing.JTextField();
        styledLabel7 = new com.jidesoft.swing.StyledLabel();
        policyPeriod = new javax.swing.JTextField();
        styledLabel9 = new com.jidesoft.swing.StyledLabel();
        policyStartDate = new javax.swing.JTextField();
        styledLabel10 = new com.jidesoft.swing.StyledLabel();
        quoteNumber = new javax.swing.JTextField();
        styledLabel11 = new com.jidesoft.swing.StyledLabel();
        policyEndDate = new javax.swing.JTextField();

        setBackground(new java.awt.Color(235, 235, 235));

        jPanel1.setBackground(new java.awt.Color(138, 177, 230));

        styledLabel1.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel1.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel1.setText("REJECT APPLICATION");
        styledLabel1.setFont(new java.awt.Font("Arial", 1, 14));
        jPanel1.add(styledLabel1);

        styledLabel8.setText("Reason(s) for Rejection");
        styledLabel8.setFont(new java.awt.Font("Arial", 1, 12));

        styledLabel12.setText("Comments");
        styledLabel12.setFont(new java.awt.Font("Arial", 1, 12));

        checkBoxList1.setBackground(new java.awt.Color(235, 235, 235));
        checkBoxList1.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Price", "Benefits", "Price & Benefits", "Relationships", "Customer Service", "Other" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        checkBoxList1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        checkBoxList1.setLayoutOrientation(javax.swing.JList.VERTICAL_WRAP);
        checkBoxList1.setSelectionBackground(new java.awt.Color(138, 177, 230));
        jScrollPane1.setViewportView(checkBoxList1);

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        jideButton1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jideButton1.setText("Back to Review Open Applications");
        jideButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jideButton1ActionPerformed(evt);
            }
        });

        jideButton2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jideButton2.setText("Mail as PDF");

        jPanel2.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel2.setText("Date and Time Stamp");

        styledLabel3.setText("User Name");

        styledLabel4.setText("Product Name");

        styledLabel5.setText("Total Policy Premium");

        totalPremium.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totalPremiumActionPerformed(evt);
            }
        });

        styledLabel6.setText("Policy Holder");

        styledLabel7.setText("Policy Period");

        styledLabel9.setText("Policy Start Date");

        styledLabel10.setText("Application Number");

        styledLabel11.setText("Policy End Date");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(461, 461, 461)
                        .addComponent(styledLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(policyEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(36, 36, 36)
                        .addComponent(dateandTime, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(163, 163, 163)
                        .addComponent(styledLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(username, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(styledLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(36, 36, 36)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(quoteNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(163, 163, 163)
                                .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(policyStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(policyHolder)
                                    .addComponent(productName, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE))
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(163, 163, 163)
                                        .addComponent(styledLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(totalPremium, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                        .addGap(150, 150, 150)
                                        .addComponent(styledLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(policyPeriod, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dateandTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(username, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(productName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totalPremium, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(policyHolder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(policyPeriod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(policyStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(quoteNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(policyEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 881, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(727, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(71, 71, 71))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addComponent(styledLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(98, 98, 98)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jideButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(160, 160, 160)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(styledLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(198, 198, 198)
                        .addComponent(jideButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(133, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(73, 73, 73))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(59, 59, 59)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jideButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(140, 140, 140)
                        .addComponent(styledLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(styledLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(styledLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jideButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void totalPremiumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totalPremiumActionPerformed
        // TODO add your handling code here:
     

    }//GEN-LAST:event_totalPremiumActionPerformed

    private void jideButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jideButton1ActionPerformed
        // TODO add your handling code here:
           QuotationsActionFrame.quotationStatus.setSelectedIndex(0);
    }//GEN-LAST:event_jideButton1ActionPerformed
public static void displayRejectQuote(String quoteNumbers){
    PolicyDetails pd=NeoWebService.getNeoManagerBeanService().getPolicy(quoteNumbers);
     String policyholdname = null;
     EntityType entityType = NeoWebService.getNeoManagerBeanService().getEntityType(pd.getPolicyHolder());
        if (entityType.value().equalsIgnoreCase("COMPANY")) {
            policyholdname = NeoWebService.getNeoManagerBeanService().getCompanyDetails(pd.getPolicyHolder()).getName();
        }
        if (entityType.value().equalsIgnoreCase("PERSON")) {
            policyholdname = NeoWebService.getNeoManagerBeanService().getPersonDetails(pd.getPolicyHolder()).getName();
        }

        policyHolder.setText(policyholdname);
   
        int productID = pd.getProduct();

        productName.setText(NeoWebService.getNeoManagerBeanService().findProductWithID(productID).getProductName());
        quoteNumber.setText(quoteNumbers);
        policyPeriod.setText(pd.getPolicyPeriod());
        Date startDate = pd.getStartDate().toGregorianCalendar().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String stDate = dateFormat.format(startDate);
        policyStartDate.setText(stDate);
        Date endDate = pd.getEndDate().toGregorianCalendar().getTime();
        DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd");
        String edDate = dateFormat1.format(endDate);
        policyEndDate.setText(edDate);
        username.setText("Smith");
         Date todayDate=new Date();
        DateFormat fdf = new SimpleDateFormat("yyyy/MM/dd");
        String generateDate=fdf.format(todayDate);
        dateandTime.setText(generateDate);

        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
         Double totalamount=(Double)ReviewOpenQuotations.quotationResults.getValueAt(ReviewOpenQuotations.quotationResults.getSelectedRow(), 7);
        totalPremium.setText(String.valueOf(totalamount));
   
}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.jidesoft.swing.CheckBoxList checkBoxList1;
    public static javax.swing.JTextField dateandTime;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private com.jidesoft.swing.JideButton jideButton1;
    private com.jidesoft.swing.JideButton jideButton2;
    public static javax.swing.JTextField policyEndDate;
    public static javax.swing.JTextField policyHolder;
    public static javax.swing.JTextField policyPeriod;
    public static javax.swing.JTextField policyStartDate;
    public static javax.swing.JTextField productName;
    public static javax.swing.JTextField quoteNumber;
    private com.jidesoft.swing.StyledLabel styledLabel1;
    private com.jidesoft.swing.StyledLabel styledLabel10;
    private com.jidesoft.swing.StyledLabel styledLabel11;
    private com.jidesoft.swing.StyledLabel styledLabel12;
    private com.jidesoft.swing.StyledLabel styledLabel17;
    private com.jidesoft.swing.StyledLabel styledLabel18;
    private com.jidesoft.swing.StyledLabel styledLabel19;
    private com.jidesoft.swing.StyledLabel styledLabel2;
    private com.jidesoft.swing.StyledLabel styledLabel3;
    private com.jidesoft.swing.StyledLabel styledLabel4;
    private com.jidesoft.swing.StyledLabel styledLabel5;
    private com.jidesoft.swing.StyledLabel styledLabel6;
    private com.jidesoft.swing.StyledLabel styledLabel7;
    private com.jidesoft.swing.StyledLabel styledLabel8;
    private com.jidesoft.swing.StyledLabel styledLabel9;
    public static javax.swing.JTextField totalPremium;
    public static javax.swing.JTextField username;
    // End of variables declaration//GEN-END:variables

}

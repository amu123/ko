/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neo.entity.provider;

/**
 *
 * @author BharathP
 */

import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;


public class ProviderAccreditationGroupTable
extends AbstractCellEditor
implements TableCellEditor {

private JComboBox cbComponent;

public ProviderAccreditationGroupTable() {

cbComponent = new JComboBox(new String[] {"Provider" } );
}

public Component getTableCellEditorComponent(JTable table,
Object value, boolean isSelected, int row, int column) {
return cbComponent;
}

public Object getCellEditorValue() {
return cbComponent.getSelectedItem();
}


}


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neo.entity.practice;

/**
 *
 * @author BharathP
 */

  import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import neo.manager.PracticeStatus;


public class PracticeStatusTable
extends AbstractCellEditor
implements TableCellEditor {

private JComboBox cbComponent;


public PracticeStatusTable() {

   PracticeStatus [] practiceStatus = neo.manager.PracticeStatus.values();

  cbComponent = new JComboBox(practiceStatus );
}

public Component getTableCellEditorComponent(JTable table,
Object value, boolean isSelected, int row, int column) {
return cbComponent;
}

public Object getCellEditorValue() {
return cbComponent.getSelectedItem();
}


}


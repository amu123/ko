/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.entity;

/**
 *
 * @author Abigail
 */
import java.awt.Component;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

public class DatePickerCellTable
        extends AbstractCellEditor
        implements TableCellEditor {

    private com.jidesoft.combobox.DateComboBox cbComponent;

    public DatePickerCellTable() {

        cbComponent = new com.jidesoft.combobox.DateComboBox();
    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {
             if(value instanceof Date)
                {
                    Date date = (Date) value;
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                     //DateFormat format = new SimpleDateFormat("MMM d, yyyy");
                    value = format.format(date);
                }
        return cbComponent;
    }

    public Object getCellEditorValue() {
        
        String DATE_FORMAT = "yyyy/MM/dd";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        String adate = sdf.format(cbComponent.getDate());
        return adate;
        
     
    }
}


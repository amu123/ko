/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * EnquiryResults.java
 *
 * Created on 2009/06/15, 12:04:23
 */
package neo.claimsManagement;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.List;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import neo.manager.Claim;
import neo.manager.ClaimLine;
import neo.manager.ClaimsBatch;
//import neo.manager.CptCode;
//import neo.manager.Icd10;
import za.co.AgilityTechnologies.DockingFramework;

/**
 *
 * @author gerritj
 */
public class EnquiryResults extends javax.swing.JPanel {

    ClaimsBatch batch = null;
    ArrayList<Claim> cls = null;
    int table1SelectedRow = 0;

    /** Creates new form EnquiryResults */
    public EnquiryResults(ArrayList batches) {
        initComponents();
        System.out.println("batch size = " + batches.size());
        cls = new ArrayList<Claim>();
        if (batches != null) {
            ArrayList<Claim> claims = null;
            for (int i = 0; i < batches.size(); i++) {
                ClaimsBatch batch = (ClaimsBatch)batches.get(i);
                claims = (ArrayList<Claim>)batch.getClaims();
                for (int j = 0; j < claims.size(); j++) {
                    cls.add(claims.get(j));

                }

            }
            jTable3.setModel(new javax.swing.table.DefaultTableModel(buildObjectArrayForClaimLines(cls.get(0)),
                    new String [] {
                "Insured Person", "Claimed amount", "Dependant Number", "Lab Invoice nr", "Laboratory order number ", "Service date from ", "Service date to", "Time in", "Time out", "Diagnoses /ICD 10/9", "Referring Provider Diagnoses/ICD10", "Assistant Practice number", "CPT codes", "Tariff", "Modifier", "Multiplier", "Units", "NDC", "NDC description", "Daily dosage", "Days of therapy ", "Mixture", "Mixture code", "Mixture group identifier", "Dosage", "Quantity", "Tooth number", "Tooth surface", "Lenses RX", "Lens Prescription: Spherical power", "Lens prescription cylindrical power ", "Visual Acuite", "Visual Fields", "Lens prescription diameter (only for contact lenses", "Lens prescription recurring ", "Intra-Ocular Pressure", "Patient Height", "Patient Weight", "Claimline Status"
            }) {
            });
            jTable1.setModel(new javax.swing.table.DefaultTableModel(
                    buildObjectArrayForClaim(cls),
                    new String [] {
                "Practice", "Provider", "Dr Type", "ClaimTotal"
            }));
        }
    }

    private Object[][] buildObjectArrayForClaim(ArrayList<Claim> clc) {
        Object[][] objs = new Object[clc.size()][5];
      /*  for (int i = 0; i < clc.size(); i++) {
            Claim c = (Claim) clc.get(i);
            objs[i][0] = c.getPractice();
            objs[i][1] = c.getProvider();
            objs[i][2] = c.getClaimCategory();
            objs[i][3] = c.getClaimTotal();
//            objs[i][4] = c.get;

        }*/
        return objs;

    }

    private Object[][] buildObjectArrayForClaimLines(Claim _claim) {
        ArrayList<ClaimLine> clc = (ArrayList<ClaimLine>) _claim.getClaimLines();
        Object[][] objs = new Object[clc.size()][42];
        for (int i = 0; i < clc.size(); i++) {
      /*      ClaimLine line = clc.get(i);
            objs[i][0] = line.getInsuredPerson();
            objs[i][1] = line.getClaimedAmount();
            objs[i][2] = line.getDepartmentCode();
            objs[i][3] = line.getLabInvoiceNumber();
            objs[i][4] = line.getLabOrderNumber();
//            objs[i][4] = line.get;
            objs[i][5] = line.getServiceDateFrom();
            objs[i][6] = line.getServiceDateTo();
            objs[i][7] = line.getTimeIn();
            objs[i][8] = line.getTimeOut();

            ArrayList<Icd10> col = (ArrayList) line.getIcd10();
            String icd = "";
            for (int j = 0; j < col.size(); j++) {
                if (j != 0) {
                    icd += ",";
                }
                Icd10 icd10 = col.get(j);
                icd += icd10.getIcd10();

            }
            objs[i][9] = icd;

            objs[i][10] = line.getReferingICD10();
            objs[i][11] = line.getAssistantPracticeNumber();

            ArrayList<CptCode> cptCol = (ArrayList) line.getCptCodes();
            String ctp = "";
            for (int k = 0; k < cptCol.size(); k++) {
                if (k != 0) {
                    ctp += ",";
                }
                CptCode ctpme = cptCol.get(k);
                ctp += ctpme.getCptCode();

            }
            objs[i][12] = ctp;

            objs[i][13] = line.getTarrif();

            ArrayList<String> modCol = (ArrayList) line.getModifiers();
            String mods = "";
            for (int j = 0; j < modCol.size(); j++) {
                if (j != 0) {
                    mods += ",";
                }
                String mod = modCol.get(j);
                mods += mod;
            }
            objs[i][14] = mods;

//            objs[i][15] = line.get;
            objs[i][16] = line.getUnits();
            objs[i][17] = line.getNdc();
            objs[i][18] = line.getNdcDescription();
            objs[i][19] = line.getDailyDosage();
            objs[i][20] = line.getDaysOfTherapy();
            objs[i][21] = line.getMixture();
            objs[i][22] = line.getMixtureCode();
            objs[i][23] = line.getMixtureGroupIdentifier();
            objs[i][24] = line.getDosage();
            objs[i][25] = line.getQuantity();
//            objs[i][26] = line.get;
//            objs[i][27] = line.get;
//            objs[i][28] = line.get;
//            objs[i][29] = line.get;
//            objs[i][30] = line.get;
//            objs[i][31] = line.get;
//            objs[i][32] = line.get;
//            objs[i][33] = line.get;
//            objs[i][34] = line.get;
//            objs[i][35] = line.get;
            objs[i][36] = line.getPatientHeight();
            objs[i][37] = line.getPatientWeight();
            //just for now
            String s = "";
            if(line.getClaimLineStatusId() == 1){
                s = "Captured";
            }else if(line.getClaimLineStatusId() == 2){
                s= "Processed";
            }else if(line.getClaimLineStatusId() == 3){
                s= "Accepted";
            }else if(line.getClaimLineStatusId() == 4){
                s= "Rejected";
            }else if(line.getClaimLineStatusId() == 5){
                s= "Refered";
            }
            objs[i][38] = s;
       */ }
        return objs;
    }

    public EnquiryResults() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        styledLabel3 = new com.jidesoft.swing.StyledLabel();

        setBackground(new java.awt.Color(235, 235, 235));

        jLabel1.setText("Header");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Practice Number", "Provider Number", "Category", "ClaimTotal"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTable1MouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jLabel2.setText("Claim Detail");

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, "1", "1", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Insured Person Number", "Claimed Amount", "Dependant Number", "Lab Invoice Number", "Laboratory Order Number ", "Service Date From ", "Service DateTo", "Time In", "Time Out", "Diagnoses /ICD 10/9", "Referring Provider Diagnoses/ICD10", "Assistant Practice Number", "CPT Codes", "Tariff", "Modifier", "Multiplier", "Units", "NDC", "NDC Description", "Daily Dosage", "Days of Therapy ", "Mixture", "Mixture Code", "Mixture Group Identifier", "Dosage", "Quantity", "Tooth Number", "Tooth Surface", "Lenses RX", "Lens Prescription: Spherical Power", "Lens prescription cylindrical Power ", "Visual Acute", "Visual Fields", "Lens Prescription Diameter (only for contact lenses", "Lens Prescription Recurring ", "Intra-Ocular Pressure", "Patient Height", "Patient Weight", "Claimline Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTable3.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane6.setViewportView(jTable3);

        jButton1.setText("Search again");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(138, 177, 230));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel3.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel3.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel3.setText("Enquiry Result");
        styledLabel3.setFont(new java.awt.Font("Arial", 1, 14));
        jPanel1.add(styledLabel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 677, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(69, 69, 69))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 657, Short.MAX_VALUE)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 657, Short.MAX_VALUE))
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(572, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addContainerGap(18, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseReleased
        int rowNum = jTable1.getSelectedRow();
        Claim c = cls.get(rowNum);
        jTable3.setModel(new javax.swing.table.DefaultTableModel(buildObjectArrayForClaimLines(c),
                new String [] {
                "Insured Person", "Claimed amount", "Dependant Number", "Lab Invoice nr", "Laboratory order number ", "Service date from ", "Service date to", "Time in", "Time out", "Diagnoses /ICD 10/9", "Referring Provider Diagnoses/ICD10", "Assistant Practice number", "CPT codes", "Tariff", "Modifier", "Multiplier", "Units", "NDC", "NDC description", "Daily dosage", "Days of therapy ", "Mixture", "Mixture code", "Mixture group identifier", "Dosage", "Quantity", "Tooth number", "Tooth surface", "Lenses RX", "Lens Prescription: Spherical power", "Lens prescription cylindrical power ", "Visual Acuite", "Visual Fields", "Lens prescription diameter (only for contact lenses", "Lens prescription recurring ", "Intra-Ocular Pressure", "Patient Height", "Patient Weight", "Claimline Status"
            }) {
        });
        table1SelectedRow = rowNum;
    }//GEN-LAST:event_jTable1MouseReleased

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        new DockingFramework().ClaimsRefinedSearchActions("", "", "");
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable3;
    private com.jidesoft.swing.StyledLabel styledLabel3;
    // End of variables declaration//GEN-END:variables
}

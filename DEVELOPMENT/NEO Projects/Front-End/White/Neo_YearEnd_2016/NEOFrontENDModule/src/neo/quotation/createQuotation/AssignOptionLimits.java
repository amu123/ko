/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * CreateCompany.java
 *
 * Created on 2009/05/05, 03:28:32
 */

package neo.quotation.createQuotation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.net.URL;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.xml.namespace.QName;

import za.co.AgilityTechnologies.TableEditors.RelationshipTableColumnEditor;
import za.co.AgilityTechnologies.TableEditors.GenderTableColumnEditor;
import za.co.AgilityTechnologies.dragandDrop.NeoTreeViewPanel;

/**
 *
 * @author BharathP
 */
public class AssignOptionLimits extends javax.swing.JPanel {

    /** Creates new form CreateCompany */
    public AssignOptionLimits() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titlePanel = new javax.swing.JPanel();
        titleName = new com.jidesoft.swing.StyledLabel();
        styledLabel1 = new com.jidesoft.swing.StyledLabel();
        policyHolder = new javax.swing.JTextField();
        styledLabel2 = new com.jidesoft.swing.StyledLabel();
        brokerName = new javax.swing.JTextField();
        styledLabel3 = new com.jidesoft.swing.StyledLabel();
        policyNumber = new javax.swing.JTextField();
        jideButton1 = new com.jidesoft.swing.JideButton();
        jideButton2 = new com.jidesoft.swing.JideButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jideTable1 = new com.jidesoft.grid.JideTable();
        jPanel1 = new javax.swing.JPanel();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(156, 233, 233)));
        setFont(new java.awt.Font("Arial", 0, 11));

        Border raisedEdge = BorderFactory.createEtchedBorder(EtchedBorder.RAISED, Color.decode("#EBEBEB"), Color.decode("#99C3FD"));
        titlePanel.setBorder(raisedEdge);
        titlePanel.setBackground(new Color(153,195,253));
        titlePanel.add(titleName,BorderLayout.LINE_START);
        titlePanel.add(titleName);
        titlePanel.setLayout(new java.awt.BorderLayout());

        titleName.setForeground(new java.awt.Color(255, 255, 255));
        titleName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleName.setText("ASSIGN OPTION LIMITS");
        titleName.setFont(new java.awt.Font("Arial", 1, 14));
        titleName.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titlePanel.add(titleName, java.awt.BorderLayout.CENTER);

        styledLabel1.setText("Policy Holder");

        policyHolder.setEditable(false);

        styledLabel2.setText("Broker/Agent Name");

        brokerName.setEditable(false);

        styledLabel3.setText("Quotation Number");

        jideButton1.setBorder(null);
        jideButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/arrow2_left_blue.png"))); // NOI18N

        jideButton2.setBorder(null);
        jideButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/arrow2_right_green.png"))); // NOI18N

        jideTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Option Name", "Benefit Name", "System Term", "Family or Beneficiary", "Limit Type", "Default Limit Value"
            }
        ));
        jideTable1.setColumnSelectionAllowed(true);
        jScrollPane1.setViewportView(jideTable1);
        jideTable1.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 401, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 242, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titlePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 851, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(styledLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(policyNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(brokerName, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(policyHolder, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(487, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(762, 762, 762)
                .addComponent(jideButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(jideButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 841, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(159, 159, 159)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(287, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titlePanel, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(policyHolder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(brokerName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(policyNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(277, 277, 277)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jideButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jideButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(227, 227, 227)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(161, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField brokerName;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private com.jidesoft.swing.JideButton jideButton1;
    private com.jidesoft.swing.JideButton jideButton2;
    private com.jidesoft.grid.JideTable jideTable1;
    private javax.swing.JTextField policyHolder;
    private javax.swing.JTextField policyNumber;
    private com.jidesoft.swing.StyledLabel styledLabel1;
    private com.jidesoft.swing.StyledLabel styledLabel2;
    private com.jidesoft.swing.StyledLabel styledLabel3;
    private com.jidesoft.swing.StyledLabel titleName;
    private javax.swing.JPanel titlePanel;
    // End of variables declaration//GEN-END:variables

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neo.entity.practice;

/**
 *
 * @author BharathP
 */

import neo.entity.provider.*;
import za.co.AgilityTechnologies.TableEditors.*;
  import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;


public class PracticeDiscountApplyMethod
extends AbstractCellEditor
implements TableCellEditor {

private JComboBox cbComponent;

public PracticeDiscountApplyMethod() {

cbComponent = new JComboBox(new String[] {"File" , "Discipline Type", "Practice", "BHF file", "Dentistry"} );
}

public Component getTableCellEditorComponent(JTable table,
Object value, boolean isSelected, int row, int column) {
return cbComponent;
}

public Object getCellEditorValue() {
return cbComponent.getSelectedItem();
}


}


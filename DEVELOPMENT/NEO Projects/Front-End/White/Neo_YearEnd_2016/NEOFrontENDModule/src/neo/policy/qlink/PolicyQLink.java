/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neo.policy.qlink;

/**
 *
 * @author lesliej
 */
public class PolicyQLink {
    int seqNumber;
    String fileCreated;
    String transactionType;
    String reconDate;
    String amount;

    QLinkTransctionDetail qLinkTransDet;

    public PolicyQLink(int seq, String fileCreated,String transactionType,String reconDate,String amount) {
        this.seqNumber = seq;
        this.fileCreated = fileCreated;
        this.transactionType = transactionType;
        this.reconDate = reconDate;
        this.amount = amount;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.product.system;

/**
 *
 * @author AbigailM
 */
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import neo.manager.Currencies;
import neo.product.utils.NeoWebService;


public class CurrenciesTable extends AbstractCellEditor implements TableCellEditor {

    private JComboBox cbComponent;

    public CurrenciesTable() {

        List<Currencies> curList = NeoWebService.getNeoManagerBeanService().getCurrencyTable();
        ArrayList<String> stArr = new ArrayList<String>();
        int i = 0;
        for (Currencies cur : curList) {
            stArr.add(cur.getCode());
            i++;
        }

        cbComponent = new JComboBox(stArr.toArray());
   
    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {

        return cbComponent;

    }

    public Object getCellEditorValue() {

        return cbComponent.getSelectedItem();

    }
}



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neo.entity.practice;

/**
 *
 * @author Abigail
 */

  import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;


public class PracticeDispensingStatusTable
extends AbstractCellEditor
implements TableCellEditor {

private JComboBox cbComponent;

public PracticeDispensingStatusTable() {
   neo.manager.DispensingStatus [] status = neo.manager.DispensingStatus.values();

cbComponent = new JComboBox(status );
}

public Component getTableCellEditorComponent(JTable table,
Object value, boolean isSelected, int row, int column) {
return cbComponent;
}

public Object getCellEditorValue() {
return cbComponent.getSelectedItem();
}


}


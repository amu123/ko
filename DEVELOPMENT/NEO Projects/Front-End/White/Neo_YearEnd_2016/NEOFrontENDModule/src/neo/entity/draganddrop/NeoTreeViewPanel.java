package neo.entity.draganddrop;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListModel;
import javax.swing.TransferHandler;

import com.jidesoft.grid.SortableTreeTableModel;
import com.jidesoft.grid.TreeTable;
import com.jidesoft.plaf.LookAndFeelFactory;
import com.jidesoft.swing.JidePopupMenu;


/**
 * This class provides the UI for the Tree task.
 * 
 * @author Administrator
 */
@SuppressWarnings("serial")
public class NeoTreeViewPanel extends JPanel implements ActionListener
{
	/**
	 * Member to hold the list of options
	 */
	protected JList _listOfOptions = null;
	
	/**
	 * Select Option button
	 */
	protected JButton _btnSelectOptions = null;
	
	/**
	 * Tree table instance
	 */
	protected TreeTable _treeTable = null;
	
	/**
	 * Pop up menu instance
	 */
	protected JidePopupMenu _popUpMenu = null;
	
	/**
	 * Delete menu item
	 */
	protected JMenuItem _deleteMenuItem = null;
	
	/**
	 * Model instance for Tree table
	 */
	protected NeoTreeTableModel _neoTreeTableModel = null;
	
	/**
	 * Controller instance of Tree table
	 */
	protected NeoTreeTableCtrl _neoTreeTableCtrl = null;
	
	
	/**
	 * Action command for Select Option button
	 */
	public static final String BTN_ACTION_COMMAND_SELECT_OPTION = "BTN_ACTION_COMMAND_SELECT_OPTION";
	
	/**
	 * Action command for Delete Menu Item
	 */
	public static final String MENU_ITEM_ACTION_COMMAND_DELETE_OPTION = "MENU_ITEM_ACTION_COMMAND_DELETE_OPTION";
	
	
	//Dummy Column Names
	public static final String TABLE_COLUMN_NAME_FIRST_NAME = "First Name";
	public static final String TABLE_COLUMN_NAME_GROUPING = "Grouping";
	
	
	/**
	 * Constructor
	 */
	public NeoTreeViewPanel()
	{
		//Initialize the UI
		this.initialize();
		
		//Create the controller
		_neoTreeTableCtrl = new NeoTreeTableCtrl(_neoTreeTableModel, this);
        updateUI();
	}
	
	/**
	 * This method returns the list of the currently selected options.
	 * 
	 * @return
	 */
	public List<String> getSelectedOptions()
	{
		if(_listOfOptions == null)
		{
			return null;
		}
		
		Object[] lstOfSelOptions = _listOfOptions.getSelectedValues();
		List<String> lstSelOptions = new ArrayList<String>();
		for(Object selVal : lstOfSelOptions)
		{
			String strValue = (String)selVal;
			lstSelOptions.add(strValue);
		}
		return lstSelOptions;
	}
	
	/**
	 * This method returns the options list to show in the view.
	 * 
	 * @return
	 */
	protected ListModel createOptionListModel(List lstOfOptions)
	{
		ListModel lstModel = new DefaultListModel();
		
		if(lstOfOptions == null)
		{
			return lstModel;
		}
		
		/**********Remove this block. Added it for demo purpose*********************************/
       
        lstOfOptions.add("Insured Person");
		lstOfOptions.add("Broker");
		lstOfOptions.add("Provider");
		
       
		/**********Remove this block. Added it for demo purpose*********************************/
		
		//Sort the obtained list
		Collections.sort(lstOfOptions);
		
		//Add each item to the list model
		for(int i=0; i<lstOfOptions.size(); i++)
		{
			String strOption = (String) lstOfOptions.get(i);
			((DefaultListModel)lstModel).addElement(strOption);
		}
		return lstModel;
	}
	
	/**
	 * Inner class for Drag and Drop on Tree table 
	 */
	class NeoTransferHandler extends TransferHandler
	{
		public NeoTransferHandler()
		{
			super();
		}
		
		@Override
		public boolean canImport(TransferSupport support) 
		{
			return true;
		}

		@Override
		public boolean importData(TransferSupport support) 
		{
			Transferable transferable = support.getTransferable();
			String data = null;
			try 
			{
				//Get the drop data on tree table in string format
				data = (String) transferable
						.getTransferData(DataFlavor.stringFlavor);
				
				//Delegate the Call back to Controller
				_neoTreeTableCtrl.onOptionsDrop(data);
				
				//Sort it, after every addition
				((SortableTreeTableModel) _treeTable.getModel()).sortColumn(0,true);
				
				//Clear the tree table selection
				_treeTable.clearSelection();
			} 
			catch (Exception e) 
			{
				return false;
			}
			return false;
		}
	}
	
	/**
	 * This method initializes the the UI
	 */
	protected void initialize()
	{
		//Set the layout
		this.setLayout(new BorderLayout());
		
		//Create the panel for tree table
		JPanel panelForTree = new JPanel(new BorderLayout());
		
		//Create the tree table model
	
		//Create the tree table instance
		_treeTable = new TreeTable(_neoTreeTableModel);
		_treeTable.setShowGrid(false);
		_treeTable.expandAll();
		
		_treeTable.addMouseListener(new MouseAdapter(){

			@Override
			public void mouseClicked(MouseEvent me) 
			{
				int b = me.getButton() ;
				
				//Check if the event is Right Click
				if(b == MouseEvent.BUTTON3)
				{
					int selRowIndices[] = _treeTable.getSelectedRows();
					
					//If no rows selected, then ignore
					if(selRowIndices == null)
					{
						super.mouseClicked(me);
						return;
					}
					
					//Flag to check if the "Options" row is selected
					boolean blnAllOptionsRow = true;
					
					//Loop through each selected row.
					for(int i=0; i < selRowIndices.length; i++)
					{
						int selRowIndex = selRowIndices[i];
						
						if(selRowIndex == -1)
							continue;
						
						NeoTreeTableRow selRow = (NeoTreeTableRow) _treeTable.getRowAt(selRowIndex);
						
						if(selRow == null)
						{
							continue;
						}
						
						/*Allow to delete, only if the all the selected rows are
						of type "Options"*/
						if(selRow.getNodeType() != NeoTreeTableRow.NODE_TYPE_NAME)
						{
							blnAllOptionsRow = false;
						}
					}
					
					//Enable/Disable the Delete menu item
					if(blnAllOptionsRow)
					{
						_deleteMenuItem.setEnabled(true);
					}
					else
					{
						_deleteMenuItem.setEnabled(false);
					}
					
					//Now, show the pop up
					_popUpMenu.show(me.getComponent(), me.getX(), me.getY());
				}
				
				//Call the super
				super.mouseClicked(me);
			}
		});
		
		//Allow sorting
		_treeTable.setSortable(true);
		
		//Set the sorting preference on all the tree table levels
		if(_treeTable.getModel() instanceof SortableTreeTableModel)
		{
			((SortableTreeTableModel) _treeTable.getModel()).setDefaultSortableOption(SortableTreeTableModel.SORTABLE_ALL_LEVELS);
		}
		
		//Allow Drag and Drop
		_treeTable.setDragEnabled(true);
		
		// Set the transfer handler for drag and drop
		_treeTable.setTransferHandler(new NeoTransferHandler());
		
		//Add tree table to tree panel
		panelForTree.add(_treeTable,BorderLayout.CENTER);
		panelForTree.setMinimumSize(new Dimension(500,500));
		
		//Create right panel
		JPanel rightPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		//Create panel for options
		JPanel panelForOptions = new JPanel(new GridBagLayout());
		
		//Create Select Option button
		_btnSelectOptions = new JButton("ADD ROLE TO PERSON");
		_btnSelectOptions.setActionCommand(BTN_ACTION_COMMAND_SELECT_OPTION);
		_btnSelectOptions.addActionListener(this);
		
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		
		//Add Select Options button to panel
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.insets = new Insets(5,5,5,5);
		gridBagConstraints.anchor = GridBagConstraints.NORTH;
		panelForOptions.add(_btnSelectOptions,gridBagConstraints);
		
		//Create List of Options
		_listOfOptions = new JList(this.createOptionListModel(new ArrayList()));
		_listOfOptions.setFixedCellWidth(200);
		_listOfOptions.setDragEnabled(true);
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.insets = new Insets(5,5,5,5);
		
		//Create Scroll pane for list of options
		JScrollPane scrollPane = new JScrollPane(_listOfOptions);
		
		//Add scroll pane to panel options
		panelForOptions.add(scrollPane,gridBagConstraints);
		
		//Add panel of options to right panel
		rightPanel.add(panelForOptions);
		
		//Create Split pane
		JSplitPane jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		
		//Add left component as Tree table
		jSplitPane.setLeftComponent(new JScrollPane(panelForTree));
		
		//Add right component as panel of options
		jSplitPane.setRightComponent(new JScrollPane(rightPanel));
		jSplitPane.setResizeWeight(0.25);
		
		//Now, add the split pane to the panel
		this.add(jSplitPane,BorderLayout.CENTER);
		
		//Sort the tree table in Ascending Order
		((SortableTreeTableModel) _treeTable.getModel()).sortColumn(0,true);
		
		//Create the pop up menu and add "Delete" menu item
		_popUpMenu = new JidePopupMenu();
		_deleteMenuItem = new JMenuItem("Delete");
		_deleteMenuItem.setActionCommand(MENU_ITEM_ACTION_COMMAND_DELETE_OPTION);
		_deleteMenuItem.addActionListener(this);
		_popUpMenu.add(_deleteMenuItem);
		
		//Update UI
		this.updateUI();
	}
	

	/**
	 * This method creates the tree table rows from the given JTable instance
	 * and returns the list of rows.
	 * 
	 * @param table
	 * @param strPolicy
	 * @return
	 */
	
		
		
	
	
	
	
	
	public void actionPerformed(ActionEvent ae) 
	{
		//Check if the button pressed is "Select Option"
		if(ae.getActionCommand().equalsIgnoreCase(BTN_ACTION_COMMAND_SELECT_OPTION))
		{
			try 
			{
				//Delegate the Call back to Controller
				_neoTreeTableCtrl.onSelectOptionCB();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		//Check if the menu item pressed is "Delete Option"
		else if(ae.getActionCommand().equalsIgnoreCase(MENU_ITEM_ACTION_COMMAND_DELETE_OPTION))
		{
			try 
			{
				//Hide, the pop up menu.
				_popUpMenu.setVisible(false);
				
				//Delegate the Call back to Controller
				_neoTreeTableCtrl.onDeleteOptionCB();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		//Sort it, after every addition
		((SortableTreeTableModel) _treeTable.getModel()).sortColumn(0,true);
		
		//Clear the tree table selection
		_treeTable.clearSelection();
	}
	
	public static void main(String[] args)
	{
		LookAndFeelFactory.installDefaultLookAndFeelAndExtension();
		JFrame frame = new JFrame();
		frame.setTitle("Tree Task");
		NeoTreeViewPanel demoTreePanel = new NeoTreeViewPanel();
		frame.add(demoTreePanel);
		frame.setMinimumSize(new Dimension(800,400));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Get the screen size
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();  
		
		//Center to desktop screen
		int x = (screenSize.width - frame.getWidth()) / 2;  
		int y = (screenSize.height - frame.getHeight()) / 2;  
		frame.setLocation(x, y);

		frame.setVisible(true);
	}
	
	/**
	 * This method returns the current list of selected 
	 * row indices in Tree table.
	 * 
	 * @return
	 */
	public int[] getSelectedRows()
	{
		if(_treeTable == null)
		{
			return new int[0];
		}
		
		//Get the currently selected rows from view
		int[] selRows = _treeTable.getSelectedRows();
		
		/*Now, get the actual rows. Because, sorting would have
		changes the actual positions of the row in view*/
		for(int i=0; i < selRows.length; i++)
		{
			selRows[i] = _treeTable.getActualRowAt(selRows[i]);
		}
		return selRows;
	}



}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ClaimHeaderCapturing.java
 *
 * Created on 2009/06/23, 11:15:34
 */
package neo.claimsManagement;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.entity.search.SearchDocsFrame;
import neo.manager.Claim;
import neo.manager.ClaimReceipt;
import neo.manager.ClaimReversals;
import neo.manager.ClaimsBatch;
import neo.manager.CompanyDetails;
import neo.manager.CoverDetails;
import neo.manager.Currencies;
import neo.manager.PersonDetails;
import neo.manager.Product;
import neo.manager.ProviderDetails;
import neo.manager.Security;
import neo.product.utils.NeoWebService;
import neo.util.UtilMethods;
import neo.util.dateConverter;
import za.co.AgilityTechnologies.DockingFramework;
import za.co.AgilityTechnologies.LoginPage;

/**
 *
 * @author DeonP
 */
public class ClaimHeaderCapturing extends javax.swing.JPanel {

    DockingFramework dockingFrame;
    int updatingClaimId = 0;
    int updatingClaimBatchId = 0;
    Date today = null;
    int batchid;
    List<PersonDetails> result;
    List<CompanyDetails> resultPrac;
    public static Map<String, String> Currency = new HashMap<String, String>();
    public static ProviderDetails providerDet = null;
    public static ProviderDetails referingProviderDet = null;
    public static ProviderDetails practiceDet = null;
    public static Collection<ClaimReceipt> claimReceipt = null;
    private Collection<Integer> batchIds = new ArrayList<Integer>();
    String UpdateOrReverse = "";
    int reversedClaimId = 0;
    ClaimReversals claimreversal;

    /** Creates new form ClaimHeaderCapturing */
    public ClaimHeaderCapturing() {
        initComponents();
        List<Currencies> cur = NeoWebService.getNeoManagerBeanService().getCurrencyTable();
        for (Currencies cDetails : cur) {
            System.out.println("currency: " + cDetails.getCode());
            currencyBox.addItem(cDetails.getCode());
        }
        styledLabel31.setVisible(false);
        memberNumber.setVisible(false);
        styledLabel26.setVisible(false);
        transactionTypeBox.setVisible(false);
        batchNumber.setVisible(false);
        styledLabel18.setVisible(false);
        dateComboBox1.setDate(getToday());
        currencyBox.setSelectedItem("ZAR");
        listComboBox1.setSelectedItem("Practice");
        jLabel3.setVisible(false);
        productCombo.setVisible(false);
    }

    public ClaimHeaderCapturing(int claimId, String UorR, ClaimReversals claimreversal) {
        this.reversedClaimId = claimId;
        initComponents();
        List<Currencies> cur = NeoWebService.getNeoManagerBeanService().getCurrencyTable();
        for (Currencies cDetails : cur) {
            System.out.println("currency: " + cDetails.getCode());
            currencyBox.addItem(cDetails.getCode());
        }
        this.claimreversal = claimreversal;
        styledLabel31.setVisible(false);
        memberNumber.setVisible(false);
        styledLabel26.setVisible(false);
        transactionTypeBox.setVisible(false);
        batchNumber.setVisible(false);
        styledLabel18.setVisible(false);
        dateComboBox1.setDate(getToday());
        currencyBox.setSelectedItem("ZAR");
        listComboBox1.setSelectedItem("Practice");
        Claim c = NeoWebService.getNeoManagerBeanService().fetchClaimForClaimLine(claimId);
        productCombo.setSelectedIndex(0);
        coverNumber.setText(c.getCoverNumber());
        practiceNumber.setText(c.getPracticeNo());
        updatingClaimId = c.getClaimId();
        updatingClaimBatchId = c.getBatchId();
        UpdateOrReverse = UorR;
        jPanel5.setVisible(false);
        jScrollPane4.setVisible(false);
        jButton2.setVisible(false);

        ProviderDetails practiceDetails = NeoWebService.getNeoManagerBeanService().getPracticeDetailsForEntityByNumber(practiceNumber.getText());
        if (practiceDetails != null) {
            practiceDet = practiceDetails;
        }
        providerNumber.setText(c.getServiceProviderNo());
        ProviderDetails providerDetails = NeoWebService.getNeoManagerBeanService().getProviderDetailsForEntityByNumber(providerNumber.getText());
        if (providerDetails != null) {
            providerDet = providerDetails;
            category.setText(providerDetails.getProviderCategory().getValue());
        }
        accountNumber.setText(c.getPatientRefNo());
        refProviderNumber.setText(c.getReferingProviderNo());
        ProviderDetails refProviderDetails = NeoWebService.getNeoManagerBeanService().getProviderDetailsForEntityByNumber(refProviderNumber.getText());
        if (refProviderDetails != null) {
            referingProviderDet = refProviderDetails;
        }
        dateComboBox1.setDate(c.getReceiveDate().toGregorianCalendar().getTime());
        claimTotal.setText(c.getClaimSubmittedTotal() + "");
        indexNumber.setText(c.getScanIndexNumber());
        if (c.getMotivationReceived() != null) {
            if (c.getMotivationReceived().equals("Y")) {
                motivation.setSelected(true);
            }
        }
        String recepient = NeoWebService.getNeoManagerBeanService().getValueForId(15, c.getRecipient() + "");
        listComboBox1.setSelectedItem(recepient);
        if (c.getReceipt().size() > 0) {
            cb_capturedReceipt.setSelected(true);
        }

        CoverDetails cd = NeoWebService.getNeoManagerBeanService().getPrincipalMemberDetailsToday(coverNumber.getText());
        int principalNumber = cd.getEntityId();
        memberNumber.setText(principalNumber + "");
        claimReceipt = c.getReceipt();
        if (UorR.equals("REVERSE")) {
            System.out.println("reverse claim");
          // ReverseClaimPopup rcp =  new ReverseClaimPopup(claimId, "Correction");
           //rcp.setVisible(true);
        }
        jLabel3.setVisible(false);
        productCombo.setVisible(false);
    }

    public ClaimHeaderCapturing(int batchId) {
        this.batchid = batchId;



//        neo.manager.EntitySearchCriteria providerSearch = new EntitySearchCriteria();
//        providerSearch.setEntityType(neo.manager.EntityType.PERSON);
//        providerSearch.setContractType(neo.manager.ContractType.PROVIDER);
        //       result = NeoWebService.getNeoManagerBeanService().findPersonsWithSearchCriteria(providerSearch);

//        Object [] Provi = new ComboPopulatorProvider(result).comboPopList.toArray();
//        for (int i = 0; i < Provi.length; i++) {
//            referringProviderBox.addItem(Provi[i]);
//            providerBox.addItem(Provi[i]);
//        }

        //       neo.manager.EntitySearchCriteria practiceSearch = new EntitySearchCriteria();
        //       practiceSearch.setContractType(neo.manager.ContractType.PRACTICE);
        //      practiceSearch.setEntityType(neo.manager.EntityType.COMPANY);
        //     resultPrac = NeoWebService.getNeoManagerBeanService().findCompaniesWithSearchCriteria(practiceSearch);
        //practiceBox = new AutoCompletionComboBox(new ComboPopulator(resultPrac).comboPopList.toArray());
//        ComboPopulator [] pract = new ComboPopulator(resultPrac).comboPopList.toArray();
//        for (int i = 0; i < pract.length; i++) {
//            practiceBox.addItem(pract[i]);
//        }
        initComponents();
        batchNumber.setText("" + batchId);

        List<Currencies> cur = NeoWebService.getNeoManagerBeanService().getCurrencyTable();
        for (Currencies cDetails : cur) {
            System.out.println("currency: " + cDetails.getCode());
            currencyBox.addItem(cDetails.getCode());
        }
        styledLabel31.setVisible(false);
        memberNumber.setVisible(false);
        styledLabel26.setVisible(false);
        transactionTypeBox.setVisible(false);
        dateComboBox1.setDate(getToday());
    //currencyBox.setSelectedItem("ZAR");
    }
    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        styledLabel18 = new com.jidesoft.swing.StyledLabel();
        batchNumber = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        styledLabel7 = new com.jidesoft.swing.StyledLabel();
        jPanel5 = new javax.swing.JPanel();
        styledLabel9 = new com.jidesoft.swing.StyledLabel();
        jButton2 = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        headerTable = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        styledLabel30 = new com.jidesoft.swing.StyledLabel();
        styledLabel29 = new com.jidesoft.swing.StyledLabel();
        styledLabel28 = new com.jidesoft.swing.StyledLabel();
        styledLabel27 = new com.jidesoft.swing.StyledLabel();
        styledLabel26 = new com.jidesoft.swing.StyledLabel();
        styledLabel25 = new com.jidesoft.swing.StyledLabel();
        styledLabel24 = new com.jidesoft.swing.StyledLabel();
        styledLabel23 = new com.jidesoft.swing.StyledLabel();
        styledLabel22 = new com.jidesoft.swing.StyledLabel();
        styledLabel21 = new com.jidesoft.swing.StyledLabel();
        styledLabel19 = new com.jidesoft.swing.StyledLabel();
        styledLabel20 = new com.jidesoft.swing.StyledLabel();
        dateComboBox1 = new com.jidesoft.combobox.DateComboBox();
        claimTotal = new javax.swing.JTextField();
        accountNumber = new javax.swing.JTextField();
        indexNumber = new javax.swing.JTextField();
        category = new javax.swing.JTextField();
        currencyBox = new com.jidesoft.combobox.ListComboBox();
        transactionTypeBox = new com.jidesoft.combobox.ListComboBox();
        listComboBox1 = new com.jidesoft.combobox.ListComboBox();
        jPanel4 = new javax.swing.JPanel();
        styledLabel8 = new com.jidesoft.swing.StyledLabel();
        memberNumber = new javax.swing.JTextField();
        styledLabel31 = new com.jidesoft.swing.StyledLabel();
        jButton3 = new javax.swing.JButton();
        providerSearch = new javax.swing.JButton();
        refProviderSearch = new javax.swing.JButton();
        motivation = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        practiceNumber = new javax.swing.JTextField();
        providerNumber = new javax.swing.JTextField();
        refProviderNumber = new javax.swing.JTextField();
        coverNumber = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        cb_capturedReceipt = new javax.swing.JCheckBox();
        insertReceipt = new javax.swing.JButton();
        productCombo = new com.jidesoft.swing.AutoCompletionComboBox();
        jLabel3 = new javax.swing.JLabel();
        butGetClaim = new javax.swing.JButton();
        btn_Save = new javax.swing.JButton();

        setBackground(new java.awt.Color(235, 235, 235));

        styledLabel18.setText("Reference Nr / Batch Nr");

        batchNumber.setEditable(false);

        jPanel3.setBackground(new java.awt.Color(138, 177, 230));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel7.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel7.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel7.setText("Header Capturing");
        styledLabel7.setFont(new java.awt.Font("Arial", 1, 14));
        jPanel3.add(styledLabel7);

        jPanel5.setBackground(new java.awt.Color(138, 177, 230));
        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel9.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel9.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel9.setText("Header Information");
        styledLabel9.setFont(new java.awt.Font("Arial", 1, 14));
        jPanel5.add(styledLabel9);

        jButton2.setText("Submit");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        headerTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Claim Number", "Cover Number", "Principal Number", "Practice Number", "Provider Number", "Category", "Date Received", "Claim Total", "Motivation Received", "Lines Captured", "Provider Type Id"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        headerTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                headerTableMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(headerTable);
        headerTable.getColumnModel().getColumn(2).setResizable(false);
        headerTable.getColumnModel().getColumn(2).setPreferredWidth(0);

        jPanel1.setBackground(new java.awt.Color(235, 235, 235));

        styledLabel30.setText("Receipt Amount");

        styledLabel29.setText("Receipt By");

        styledLabel28.setText("Scan Index Number");

        styledLabel27.setText("Category");

        styledLabel26.setText("Transaction Type");

        styledLabel25.setText("Account Number");

        styledLabel24.setText("Currency");

        styledLabel23.setText("Claim Total");

        styledLabel22.setText("Date Received");

        styledLabel21.setText("Referring Provider Number");

        styledLabel19.setText("Practice Number");

        styledLabel20.setText("Provider Number");

        dateComboBox1.setFormat(new SimpleDateFormat("yyyy/MM/dd"));
        dateComboBox1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                dateComboBox1FocusLost(evt);
            }
        });

        category.setEditable(false);
        category.setFocusable(false);

        currencyBox.setEditable(false);

        listComboBox1.setModel(new javax.swing.DefaultComboBoxModel(UtilMethods.getLookupValuesForMapString("Practice Entity", Currency)));
        listComboBox1.setEditable(false);

        jPanel4.setBackground(new java.awt.Color(138, 177, 230));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        styledLabel8.setBackground(new java.awt.Color(255, 255, 255));
        styledLabel8.setForeground(new java.awt.Color(255, 255, 255));
        styledLabel8.setText("Header Information");
        styledLabel8.setFont(new java.awt.Font("Arial", 1, 14));
        jPanel4.add(styledLabel8);

        memberNumber.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                memberNumberFocusLost(evt);
            }
        });

        styledLabel31.setText("Principal Number");

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/view_add.png"))); // NOI18N
        jButton3.setFocusable(false);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        providerSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/view_add.png"))); // NOI18N
        providerSearch.setFocusable(false);
        providerSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                providerSearchActionPerformed(evt);
            }
        });

        refProviderSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/view_add.png"))); // NOI18N
        refProviderSearch.setFocusable(false);
        refProviderSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refProviderSearchActionPerformed(evt);
            }
        });

        motivation.setBackground(new java.awt.Color(235, 235, 235));
        motivation.setText("Motivation Received");
        motivation.setFocusable(false);

        jLabel1.setText("Motivation");

        practiceNumber.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                practiceNumberFocusLost(evt);
            }
        });

        providerNumber.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                providerNumberFocusLost(evt);
            }
        });

        refProviderNumber.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                refProviderNumberFocusLost(evt);
            }
        });

        coverNumber.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                coverNumberFocusLost(evt);
            }
        });

        jLabel2.setText("Cover Number");

        cb_capturedReceipt.setBackground(new java.awt.Color(235, 235, 235));
        cb_capturedReceipt.setText("Receipt Captured");
        cb_capturedReceipt.setEnabled(false);
        cb_capturedReceipt.setFocusable(false);

        insertReceipt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/za/co/AgilityTechnologies/vsnet/NEOLogos/box_add.png"))); // NOI18N
        insertReceipt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                insertReceiptActionPerformed(evt);
            }
        });

        List<neo.manager.Product> prodList =NeoWebService.getNeoManagerBeanService().fetchAllProducts();

        int total=prodList.size();

        String aproduct[]=new String[total];
        int i=0;
        for(Product listprod : prodList){
            aproduct[i]=listprod.getProductName();
            listprod.getProductName();
            i++;
        }
        productCombo.setModel(new javax.swing.DefaultComboBoxModel(aproduct));
        productCombo.setSelectedItem("");

        jLabel3.setText("Product");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 355, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(styledLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(styledLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)
                            .addComponent(styledLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(productCombo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(motivation, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(indexNumber, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                            .addComponent(listComboBox1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                            .addComponent(coverNumber, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                            .addComponent(currencyBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                            .addComponent(claimTotal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                            .addComponent(dateComboBox1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                            .addComponent(accountNumber, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                            .addComponent(category, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                            .addComponent(practiceNumber, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                            .addComponent(providerNumber, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                            .addComponent(refProviderNumber, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                            .addComponent(cb_capturedReceipt, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(memberNumber, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                            .addComponent(transactionTypeBox, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(providerSearch, 0, 0, Short.MAX_VALUE)
                                    .addComponent(refProviderSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 32, Short.MAX_VALUE)
                                    .addComponent(insertReceipt, javax.swing.GroupLayout.PREFERRED_SIZE, 32, Short.MAX_VALUE))
                                .addGap(302, 302, 302))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jButton3, 0, 0, Short.MAX_VALUE)
                                .addContainerGap())))))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {accountNumber, category, claimTotal, coverNumber, currencyBox, dateComboBox1, indexNumber, listComboBox1, memberNumber, practiceNumber, productCombo, providerNumber, refProviderNumber, transactionTypeBox});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jButton3, providerSearch, refProviderSearch});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(productCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(2, 2, 2)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(styledLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(practiceNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(styledLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(providerNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(category, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(accountNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(styledLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(refProviderNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(dateComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(claimTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(currencyBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(styledLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(coverNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(12, 12, 12)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(providerSearch)
                        .addGap(82, 82, 82)
                        .addComponent(refProviderSearch)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(indexNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(motivation))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(styledLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(listComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cb_capturedReceipt)
                    .addComponent(styledLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(insertReceipt))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(memberNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(transactionTypeBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {accountNumber, category, claimTotal, coverNumber, currencyBox, dateComboBox1, indexNumber, jButton3, listComboBox1, memberNumber, practiceNumber, productCombo, providerNumber, providerSearch, refProviderNumber, refProviderSearch, transactionTypeBox});

        butGetClaim.setText("Get Claim");
        butGetClaim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butGetClaimActionPerformed(evt);
            }
        });

        btn_Save.setText("Save");
        btn_Save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 1118, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(243, 243, 243)
                .addComponent(styledLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(batchNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(589, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(371, 371, 371)
                .addComponent(butGetClaim)
                .addGap(28, 28, 28)
                .addComponent(btn_Save)
                .addContainerGap(585, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 1098, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 1098, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(1043, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(209, 209, 209)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(266, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(batchNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(styledLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(butGetClaim)
                    .addComponent(btn_Save))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addContainerGap(16, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    private DatatypeFactory _datatypeFactory;

    private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {
            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                ex.printStackTrace();
            }
        }
        return _datatypeFactory;
    }

    protected XMLGregorianCalendar convert(String asDate) {
        DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        java.util.Date date = null;
        try {
            date = format.parse(asDate);
        } catch (java.text.ParseException ex) {
        }
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getDatatypeFactory().newXMLGregorianCalendar(calendar);
    }

    private void btn_SaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SaveActionPerformed

        Claim claim = new Claim();

        claim.setPracticeNo(practiceNumber.getText());
        if (practiceDet != null) {
            // claim.setPracticeId(practiceDet.getEntityId()); // TODO practice Search
        }
        claim.setServiceProviderNo(providerNumber.getText());
        //claim.setServiceProviderId(providerDet.getEntityId());
        claim.setReferingProviderNo(refProviderNumber.getText());
        //claim.setReferingProviderId(referingProviderDet.getEntityId());
        if (dateComboBox1.getSelectedItem() != null) {
            if (dateComboBox1.getSelectedItem() instanceof GregorianCalendar) {
                claim.setReceiveDate(getDatatypeFactory().newXMLGregorianCalendar((GregorianCalendar) dateComboBox1.getSelectedItem()));
            } else if (dateComboBox1.getSelectedItem() instanceof String) {
                String date = (String) dateComboBox1.getSelectedItem();
                XMLGregorianCalendar xmlDate = (convert(date));
                claim.setReceiveDate(xmlDate);
            }
        }
         if (claimTotal.getText() == null || claimTotal.getText().trim().equals("")) {
             JOptionPane.showConfirmDialog(this, "Claim Total is a mandatory field", "Failed", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
             return;
         } else {
            claim.setClaimSubmittedTotal(new Double(claimTotal.getText()));
         }
        

        Currencies selCur = NeoWebService.getNeoManagerBeanService().getCurrencyForCode(currencyBox.getSelectedItem() + "");
        claim.setCurrencyId(selCur.getId());

        if (accountNumber.getText() == null || accountNumber.getText().trim().equals("")) {
            JOptionPane.showConfirmDialog(this, "Account Number is a mandatory field", "Failed", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
            return;
        } else {
            if (accountNumber.getText().contains("<") || accountNumber.getText().contains(">")) {
                JOptionPane.showConfirmDialog(this, "\'<\' \'>\' not allowed in account number field", "Failed", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
                return;
            }
            
            claim.setPatientRefNo(accountNumber.getText());
            claim.setClaimReferencenumber(accountNumber.getText());
        }

        claim.setScanIndexNumber(indexNumber.getText());

        String recepient = NeoWebService.getNeoManagerBeanService().getIdForValue(15, listComboBox1.getSelectedItem() + "");
        claim.setRecipient(recepient);
        if (claimReceipt != null) {
            Collection<ClaimReceipt> claimreceipts = claim.getReceipt();
            claimreceipts.addAll(claimReceipt);
            System.out.println("claimReceipt" + claimReceipt.size());
        }

        claim.setCoverNumber(coverNumber.getText());
        //claim.setCoverId(0);  // TODO cover id
        if (motivation.isSelected()) {
            claim.setMotivationReceived("Y");
        } else {
            claim.setMotivationReceived("N");
        }

        //claim.setEffectiveStartDate(new dateConverter().convertDateXML(getToday()));

        Security _secure = new Security();
        _secure.setCreatedBy(LoginPage.neoUser.getUserId());
        _secure.setLastUpdatedBy(LoginPage.neoUser.getUserId());
        _secure.setSecurityGroupId(2);
        System.out.println("U&R " + UpdateOrReverse);
        if (UpdateOrReverse.equals("")) {
            if (batchIds.size() < 1) {
                ClaimsBatch currentBatch = new ClaimsBatch();
                currentBatch.setBatchTypeId(1); //paper batch
                if (dateComboBox1.getSelectedItem() != null) {
                    if (dateComboBox1.getSelectedItem() instanceof GregorianCalendar) {
                        currentBatch.setDateReceived(getDatatypeFactory().newXMLGregorianCalendar((GregorianCalendar) dateComboBox1.getSelectedItem()));
                    } else if (dateComboBox1.getSelectedItem() instanceof String) {
                        String date = (String) dateComboBox1.getSelectedItem();
                        XMLGregorianCalendar xmlDate = (convert(date));
                        currentBatch.setDateReceived(xmlDate);
                    }
                }
                if (!productCombo.getSelectedItem().equals("")) {
                    neo.manager.Product pro = NeoWebService.getNeoManagerBeanService().findProductWithDesc(productCombo.getSelectedItem().toString());
                    currentBatch.setProductId(pro.getProductId());
                    System.out.println("Product ID : " + pro.getProductId());
                }
                //currentBatch.setEffectiveStartDate(new dateConverter().convertDateXML(getToday()));

                currentBatch.setBatchId(NeoWebService.getNeoManagerBeanService().saveBatch(currentBatch, _secure));

               batchid = currentBatch.getBatchId();
               batchIds.add(batchid);
            }


            if (batchIds.size() != 0) {
                claim.setBatchId(batchid);
                claim.setClaimId(NeoWebService.getNeoManagerBeanService().saveClaim(claim, batchid, _secure));
                if (claim.getClaimId() != 0) {
                    String practiceTypeId = "";
                    if (providerDet != null) {
                        practiceTypeId = providerDet.getDisciplineType().getId();
                    }
                    DefaultTableModel tableModel = (DefaultTableModel) headerTable.getModel();
                    tableModel.addRow(new Object[]{
                                claim.getClaimId(),
                                claim.getCoverNumber(),
                                memberNumber.getText(),
                                practiceNumber.getText(),
                                providerNumber.getText(),
                                category.getText(),
                                new dateConverter().convertToYYYMMD(claim.getReceiveDate().toGregorianCalendar().getTime()),
                                claim.getClaimSubmittedTotal(),
                                motivation.isSelected(),
                                false,
                                practiceTypeId});

                    JOptionPane.showConfirmDialog(this, "Claim " + claim.getClaimId() + " saved with Batch ID: " + batchid, "Saved", JOptionPane.CLOSED_OPTION, JOptionPane.INFORMATION_MESSAGE);
                    coverNumber.setText("");
                    memberNumber.setText("");
                    practiceNumber.setText("");
                    providerNumber.setText("");
                    refProviderNumber.setText("");
                    claimTotal.setText("");
                    //currencyBox.setSelectedItem("");
                    accountNumber.setText("");
                    transactionTypeBox.setSelectedItem("");
                    category.setText("");
                    indexNumber.setText("");
                    // listComboBox1.setSelectedItem("");
                    cb_capturedReceipt.setSelected(false);
                    motivation.setSelected(false);
                    dateComboBox1.setDate(today);
                } else {
                    JOptionPane.showConfirmDialog(this, "Saving of claim failed", "Failed", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showConfirmDialog(this, "Failed to Create Batch For Claim", "Failed", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
            }
        } else if (UpdateOrReverse.equals("UPDATE")) {
            claim.setBatchId(updatingClaimBatchId);
            claim.setClaimId(updatingClaimId);

            // TODO update
           // NeoWebService.getNeoManagerBeanService().updateClaimByID(claim, _secure);
        } else if (UpdateOrReverse.equals("REVERSE")) {
            System.out.println("REVERSE");
            claim.setBatchId(updatingClaimBatchId);
            //No longer used
            //NeoWebService.getNeoManagerBeanService().reverseAndCorrectClaim(reversedClaimId, claim, claimreversal, _secure);
            JOptionPane.showConfirmDialog(this, "Claim Reversed", "Reversed", JOptionPane.CLOSED_OPTION, JOptionPane.INFORMATION_MESSAGE);
        } else {
            System.out.println("ERROR");
        }

}//GEN-LAST:event_btn_SaveActionPerformed

    @SuppressWarnings({"unchecked", "static-access"})
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        //NeoWebService.getNeoManagerBeanService().sendSmsesForBatch(batchid);
        //new DockingFramework().ClaimBatchBlalancingActions(batchid);
        for (Integer batch : batchIds) {
            Security _secure = new Security();
            _secure.setCreatedBy(LoginPage.neoUser.getUserId());
            _secure.setLastUpdatedBy(LoginPage.neoUser.getUserId());
            _secure.setSecurityGroupId(LoginPage.neoUser.getSecurityGroupId());
            NeoWebService.getNeoManagerBeanService().processRulesInBatch(batch, _secure);
        }
        JOptionPane.showConfirmDialog(this, "Claim Submitted!!", "Success", JOptionPane.CLOSED_OPTION, JOptionPane.INFORMATION_MESSAGE);
        dockingFrame.ClaimsCaptureActions();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void headerTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_headerTableMouseClicked
        if (evt.getClickCount() == 2) {
            int row = headerTable.getSelectedRow();
            int claimid = new Integer("" + headerTable.getModel().getValueAt(row, 0));
            int principalMember = new Integer("" + headerTable.getModel().getValueAt(row, 2));
            System.out.println("claim: " + principalMember);
            String provNumber = "" + headerTable.getModel().getValueAt(row, 4);
            String catagory = "" + headerTable.getModel().getValueAt(row, 5);
            String memNumber = "" + headerTable.getModel().getValueAt(row, 1);
            String practiceTypeId = "" + headerTable.getModel().getValueAt(row, 10);
            new DetailDataCapturingFrame(catagory, claimid, this, row, principalMember, memNumber, provNumber, practiceTypeId).setVisible(true);
        }
    }//GEN-LAST:event_headerTableMouseClicked

    private void dateComboBox1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dateComboBox1FocusLost
        if (dateComboBox1.getDate() != null) {
            if (dateComboBox1.getDate().after(today)) {
                //dateCombo.setSelectedItem("");
                dateComboBox1.hidePopup();
                //dateCombo.setDate(today);
                JOptionPane.showConfirmDialog(this, "Date In Future!", "Warning", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_dateComboBox1FocusLost

    private void memberNumberFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_memberNumberFocusLost
        if (!memberNumber.getText().equals("")) {
            PersonDetails det = NeoWebService.getNeoManagerBeanService().getPersonDetailsByEntityId(new Integer(memberNumber.getText()));
            if (det.getGenderDesc() != null) {
                // JOptionPane.showConfirmDialog(this,"Insured person valid", "Valid", JOptionPane.CLOSED_OPTION, JOptionPane.INFORMATION_MESSAGE);
            } else {
                //   JOptionPane.showConfirmDialog(this, "Insured person invalid", "Invalid", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
            }
        }


}//GEN-LAST:event_memberNumberFocusLost

    private void butGetClaimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butGetClaimActionPerformed
        if (reconcilliationOfClaimBacth.batchClaims.getClaimsList().size() == 0) {
            reconcilliationOfClaimBacth.batchClaims = NeoWebService.getNeoManagerBeanService().getClaimsIndNoForBatch(reconcilliationOfClaimBacth.batchClaims);
        }
        if (reconcilliationOfClaimBacth.batchClaims.getClaimsList() != null) {
            new SearchDocsFrame().viewDoc("6", reconcilliationOfClaimBacth.batchClaims.getClaimsList().get(docClaimsController) + "");
        }
}//GEN-LAST:event_butGetClaimActionPerformed

    private void providerSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_providerSearchActionPerformed
        new searchProvider("provider").setVisible(true);

}//GEN-LAST:event_providerSearchActionPerformed

    private void refProviderSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refProviderSearchActionPerformed
        new searchProvider("refering provider").setVisible(true);

}//GEN-LAST:event_refProviderSearchActionPerformed

    private void coverNumberFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_coverNumberFocusLost
        if (!coverNumber.getText().equals("")) {
            int principalNumber = 0;
            Collection<CoverDetails> cds = NeoWebService.getNeoManagerBeanService().getResignedMemberByCoverNumber(coverNumber.getText());
            if (cds.size() == 0) {
                JOptionPane.showConfirmDialog(this, "Invalid Cover Number", "Invalid", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
            }
            for (CoverDetails cd : cds) {
                if (cd.getDependentTypeId()==17) {
                    principalNumber = cd.getEntityId();
                }
            }
            
            System.out.println("memberNumber" + principalNumber);
            memberNumber.setText(principalNumber + "");
        }
}//GEN-LAST:event_coverNumberFocusLost

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        new searchPractice().setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void insertReceiptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_insertReceiptActionPerformed
        new claimReceiptCapture().setVisible(true);
}//GEN-LAST:event_insertReceiptActionPerformed

    private void practiceNumberFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_practiceNumberFocusLost
        if (!practiceNumber.getText().equals("")) {
            ProviderDetails practiceDetails = NeoWebService.getNeoManagerBeanService().getPracticeDetailsForEntityByNumber(practiceNumber.getText());
            if (practiceDetails == null) {
                JOptionPane.showConfirmDialog(this, "Invalid Practice Number", "Invalid", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
            } else {
                practiceDet = practiceDetails;
                if (practiceDet.getCashPractice().getId().equals("1") || practiceDet.getProviderCategory().getValue().equalsIgnoreCase("Pharmacy")){
                    listComboBox1.setSelectedItem("Insured Person");
//                    listComboBox1.enable(false);
                }else{
                    listComboBox1.enable(true);
                    listComboBox1.setSelectedItem("Practice");
                }
            }
        }
    }//GEN-LAST:event_practiceNumberFocusLost

    private void providerNumberFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_providerNumberFocusLost
        if (!providerNumber.getText().equals("")) {
            ProviderDetails providerDetails = NeoWebService.getNeoManagerBeanService().getProviderDetailsForEntityByNumber(providerNumber.getText());
            if (providerDetails == null) {
                JOptionPane.showConfirmDialog(this, "Invalid Provider Number", "Invalid", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
            } else {
                providerDet = providerDetails;
                category.setText(providerDetails.getProviderCategory().getValue());
            }
        }
    }//GEN-LAST:event_providerNumberFocusLost

    private void refProviderNumberFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_refProviderNumberFocusLost
        if (!refProviderNumber.getText().equals("")) {
            ProviderDetails refProviderDetails = NeoWebService.getNeoManagerBeanService().getProviderDetailsForEntityByNumber(refProviderNumber.getText());
            if (refProviderDetails == null) {
                JOptionPane.showConfirmDialog(this, "Invalid Refering Provider Number", "Invalid", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
            } else {
                referingProviderDet = refProviderDetails;
            }
        }
    }//GEN-LAST:event_refProviderNumberFocusLost

    /*
    private class ComboPopulator {

    public int id;
    public String name;
    Collection<ComboPopulator> comboPopList = new ArrayList<ComboPopulator>();

    public ComboPopulator(int id, String name) {
    this.id = id;
    this.name = id + " - " + name;
    }

    public ComboPopulator(Collection<CompanyDetails> list) {
    for (CompanyDetails entity : list) {
    ComboPopulator tmp = new ComboPopulator(entity.getEntityId(), entity.getName());
    comboPopList.add(tmp);
    }
    }

    public String toString() {
    return name;
    }
    }


    private class ComboPopulatorProvider {

    public int id;
    public String name;
    public String type;
    Collection<ComboPopulatorProvider> comboPopList = new ArrayList<ComboPopulatorProvider>();

    public ComboPopulatorProvider(int id, String name, String type) {
    this.id = id;
    this.name = id + " - " + name;
    this.type = type;
    }

    public ComboPopulatorProvider(Collection<PersonDetails> list) {
    for (PersonDetails entity : list) {
    List<ProviderDispensing> l = entity.getProviderDispensing();
    String type = "";
    for (Iterator<ProviderDispensing> it = l.iterator(); it.hasNext();) {
    ProviderDispensing providerDispensing = it.next();
    type = providerDispensing.getDisciplineDescription();
    }
    ComboPopulatorProvider tmp = new ComboPopulatorProvider(entity.getEntityId(), entity.getName(), type);
    comboPopList.add(tmp);
    }
    }

    public String toString() {
    return name;
    }
    }
     */
    public Date getToday() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        today = calendar.getTime();
        return today;
    }
    public static int docClaimsController = 0;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField accountNumber;
    private javax.swing.JTextField batchNumber;
    private javax.swing.JButton btn_Save;
    public static javax.swing.JButton butGetClaim;
    public static javax.swing.JTextField category;
    public static javax.swing.JCheckBox cb_capturedReceipt;
    private javax.swing.JTextField claimTotal;
    private javax.swing.JTextField coverNumber;
    private com.jidesoft.combobox.ListComboBox currencyBox;
    private com.jidesoft.combobox.DateComboBox dateComboBox1;
    public static javax.swing.JTable headerTable;
    private javax.swing.JTextField indexNumber;
    private javax.swing.JButton insertReceipt;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane4;
    private com.jidesoft.combobox.ListComboBox listComboBox1;
    private javax.swing.JTextField memberNumber;
    private javax.swing.JCheckBox motivation;
    public static javax.swing.JTextField practiceNumber;
    private com.jidesoft.swing.AutoCompletionComboBox productCombo;
    public static javax.swing.JTextField providerNumber;
    private javax.swing.JButton providerSearch;
    public static javax.swing.JTextField refProviderNumber;
    private javax.swing.JButton refProviderSearch;
    private com.jidesoft.swing.StyledLabel styledLabel18;
    private com.jidesoft.swing.StyledLabel styledLabel19;
    private com.jidesoft.swing.StyledLabel styledLabel20;
    private com.jidesoft.swing.StyledLabel styledLabel21;
    private com.jidesoft.swing.StyledLabel styledLabel22;
    private com.jidesoft.swing.StyledLabel styledLabel23;
    private com.jidesoft.swing.StyledLabel styledLabel24;
    private com.jidesoft.swing.StyledLabel styledLabel25;
    private com.jidesoft.swing.StyledLabel styledLabel26;
    private com.jidesoft.swing.StyledLabel styledLabel27;
    private com.jidesoft.swing.StyledLabel styledLabel28;
    private com.jidesoft.swing.StyledLabel styledLabel29;
    private com.jidesoft.swing.StyledLabel styledLabel30;
    private com.jidesoft.swing.StyledLabel styledLabel31;
    private com.jidesoft.swing.StyledLabel styledLabel7;
    private com.jidesoft.swing.StyledLabel styledLabel8;
    private com.jidesoft.swing.StyledLabel styledLabel9;
    private com.jidesoft.combobox.ListComboBox transactionTypeBox;
    // End of variables declaration//GEN-END:variables
}

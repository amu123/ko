/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.product.utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javax.xml.namespace.QName;
import neo.manager.NeoManagerBean;
import neo.manager.NeoManagerBeanService;

/**
 *
 * @author AbigailM
 */
public class NeoWebService extends Object {

    private static final int _iWS_URL = 0;
    private static final int _iWS_NAMESPACE = 1;
    private static final int _iWS_SERVICENAME = 2;
    private static final int _iWS_PORTNAME = 3;
    private static final int _iWS_EXPLORE = 4;
    private static final int _iWS_DASHBOARD = 5;
    private static final int _iWS_WAREHOUSE = 6;
    private static final int _iWS_RULESENGINEPATH = 7;
    private static final int _iWS_PDCCALLS = 8;
    private static final int _iWS_EMAIL = 9;
    private static final int _iWS_VIEWDOC_URL = 10;
    private static final int _iWS_VIEWDOC_PATH = 11;
    private static final int _iWS_CLAIMVIEW_DOC = 12;
    private static final int _iWS_REPORT_URL = 13;
    private static final int _iWS_REPORT_PATH = 14;
    private static final int _iWS_CONFIG = 15;
    protected static String WS_URL = "";
    protected static String WS_NAMESPACE = "";
    protected static String WS_SERVICENAME = "";
    protected static String WS_PORTNAME = "";
    protected static String I_EXPLORE = "";
    protected static String DASHBOARD_URL = "";
    protected static String WAREHOUSE_URL = "";
    protected static String RULESENGINE_PATH = "";
    protected static String PDCCALLS_URL = "";
    protected static String MAIL_URL = "";
    protected static String VIEWDOC_URL = "";
    protected static String VIEWDOC_PATH = "";
    protected static String CLAIM_VIEW_DOC = "";
    protected static String REPORT_URL = "";
    protected static String REPORT_PATH = "";
    protected static String CONFIGURATION = "";
    //protected static String NEO_RESOURCES_PATH = "resources";
    private static NeoManagerBean neoManagerBeanService = null;

    private static void init() {
      
        InputStream stream = null;
        try {
            stream = getResourceStream("NeoConfig.txt");
        } catch (Exception exception) {
            System.out.println("Exception finding config file : NeoConfig.txt - " + exception.getMessage());
        }
        try {
            DataInputStream in = new DataInputStream(stream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            String readLine;
            int iCounter = 0;

            while ((strLine = br.readLine()) != null) {
                readLine = strLine;

                switch (iCounter) {
                    case _iWS_URL: {
                        WS_URL = formatString(iCounter, readLine);
                        break;
                    }
                    case _iWS_NAMESPACE: {
                        WS_NAMESPACE = formatString(iCounter, readLine);
                        break;
                    }
                    case _iWS_SERVICENAME: {
                        WS_SERVICENAME = formatString(iCounter, readLine);
                        break;
                    }
                    case _iWS_PORTNAME: {
                        WS_PORTNAME = formatString(iCounter, readLine);
                        break;
                    }
                    case _iWS_EXPLORE: {
                        I_EXPLORE = formatString(iCounter, readLine);

                        break;
                    }
                    case _iWS_DASHBOARD: {
                        DASHBOARD_URL = formatString(iCounter, readLine);

                        break;
                    }
                    case _iWS_WAREHOUSE: {
                        WAREHOUSE_URL = formatString(iCounter, readLine);

                        break;
                    }

                    case _iWS_RULESENGINEPATH: {
                        RULESENGINE_PATH = formatString(iCounter, readLine);

                        break;
                    }

                    case _iWS_PDCCALLS: {
                        PDCCALLS_URL = formatString(iCounter, readLine);

                        break;
                    }
                    case _iWS_EMAIL: {
                        MAIL_URL = formatString(iCounter, readLine);

                        break;
                    }
                    case _iWS_VIEWDOC_URL: {
                        VIEWDOC_URL = formatString(iCounter, readLine);
                        break;
                    }
                    case _iWS_VIEWDOC_PATH: {
                        VIEWDOC_PATH = formatString(iCounter, readLine);
                        break;
                    }

                    case _iWS_CLAIMVIEW_DOC: {
                        CLAIM_VIEW_DOC = formatString(iCounter, readLine);
                        break;
                    }

                    case _iWS_REPORT_URL: {
                        REPORT_URL = formatString(iCounter, readLine);
                        break;
                    }

                    case _iWS_REPORT_PATH: {
                        REPORT_PATH = formatString(iCounter, readLine);
                        break;
                    }

                    case _iWS_CONFIG:  {
                        CONFIGURATION = formatString(iCounter, readLine);
                        break;
                    }

                    default: {
                        System.out.println("Entry not found for " + iCounter + " : " + readLine);
                        break;
                    }
                }

                iCounter++;
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        System.out.println("WS_URL=" + WS_URL);
        System.out.println("WS_NAMESPACE=" + WS_NAMESPACE);
        System.out.println("WS_SERVICENAME=" + WS_SERVICENAME);
        System.out.println("WS_PORTNAME=" + WS_PORTNAME);
   }

    public static NeoManagerBean getNeoManagerBeanService() {
        
        if (neoManagerBeanService == null)
        {
            try {
                init();
                URL wsUrl = new URL(WS_URL);
                QName wsQName = new QName(WS_NAMESPACE, WS_SERVICENAME);
                NeoManagerBeanService regService = new NeoManagerBeanService(wsUrl, wsQName);
                neoManagerBeanService = (NeoManagerBean) regService.getPort(new QName(WS_NAMESPACE,
                        WS_PORTNAME), NeoManagerBean.class);

            } catch (Exception e) {
                System.out.println("Please make sure " + WS_URL + " is accessible and JBOSS server is running." + e.toString());
                e.printStackTrace();
            }
        }
        return neoManagerBeanService;
    }

    public static String getIExplorer() {
        init();
        return I_EXPLORE;
    }

    public static String getDashBoardURL() {
        init();
        return DASHBOARD_URL;
    }

    public static String getWareHouseURL() {
        init();
        return WAREHOUSE_URL;
    }

    public static String getRulesEnginePath() {
        init();
        return RULESENGINE_PATH;
    }

    public static String getPdcCalssURL() {
        init();
        return PDCCALLS_URL;
    }

    public static String getEmailURL() {
        init();
        return MAIL_URL;
    }

    public static String getViewDocURL() {
        init();
        return VIEWDOC_URL;
    }

    public static String getViewDocPath() {
        init();
        return VIEWDOC_PATH;
    }

    public static String getClaimViewDoc() {
        init();
        return CLAIM_VIEW_DOC;
    }

    public static String getReportURL() {
        init();
        return REPORT_URL;
    }

    public static String getReportPath() {
        init();
        return REPORT_PATH;
    }

    public static String getConfiguration() {
        init();
        return CONFIGURATION;
    }

    public static String formatString(int _pCounter, String incomingString) {
        String returnString = "";

        try {
            switch (_pCounter) {
                case _iWS_URL: {
                    returnString = incomingString.substring(7, incomingString.length());
                    break;
                }
                case _iWS_NAMESPACE: {
                    returnString = incomingString.substring(13, incomingString.length());
                    break;
                }
                case _iWS_SERVICENAME: {

                    returnString = incomingString.substring(15, incomingString.length());
                    break;
                }
                case _iWS_PORTNAME: {
                    returnString = incomingString.substring(12, incomingString.length());
                    break;
                }

                case _iWS_EXPLORE: {
                    returnString = incomingString.substring(10, incomingString.length());
                    break;
                }
                case _iWS_DASHBOARD: {
                    returnString = incomingString.substring(14, incomingString.length());
                    break;
                }
                case _iWS_WAREHOUSE: {
                    returnString = incomingString.substring(14, incomingString.length());
                    break;
                }

                case _iWS_RULESENGINEPATH: {
                    returnString = incomingString.substring(17, incomingString.length());
                    break;
                }

                case _iWS_PDCCALLS: {
                    returnString = incomingString.substring(13, incomingString.length());
                    break;
                }

                case _iWS_EMAIL: {
                    returnString = incomingString.substring(9, incomingString.length());
                    break;
                }
                case _iWS_VIEWDOC_URL: {
                    returnString = incomingString.substring(12, incomingString.length());
                    break;
                }
                case _iWS_VIEWDOC_PATH: {
                    returnString = incomingString.substring(13, incomingString.length());
                    break;
                }
                case _iWS_CLAIMVIEW_DOC: {
                    returnString = incomingString.substring(15, incomingString.length());
                    break;
                }
                case _iWS_REPORT_URL: {
                    returnString = incomingString.substring(11, incomingString.length());
                    break;
                }
                case _iWS_REPORT_PATH: {
                    returnString = incomingString.substring(12, incomingString.length());
                    break;
                }
                case _iWS_CONFIG: {
                    returnString = incomingString.substring(14, incomingString.length());
                    break;
                }

                default: {
                    System.out.println("Entry not found for " + _pCounter + " : " + incomingString);
                    break;
                }
            }
        } catch (Exception e) {
            System.out.println("Error formatting " + incomingString + ". Error = " + e.toString());
            e.printStackTrace();
        }

        return returnString;
    }

    public static InputStream getResourceStream(String file) {
		String path = file;
        System.out.println("Path : " + path);
		return NeoWebService.class.getResourceAsStream(path);
        //String name = NeoWebService.class.getResource(path).toString();
        //System.out.println("Name : " + name);
		//return new File(ClassLoader.getSystemClassLoader().getResource(path).getFile());
	}

}

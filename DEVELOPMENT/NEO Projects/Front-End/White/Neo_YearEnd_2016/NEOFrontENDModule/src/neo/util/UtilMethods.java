/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neo.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.LookupValue;
import neo.product.utils.NeoWebService;
import neo.utils.tables.TableFormater;
import za.co.AgilityTechnologies.LoginPage;
import za.co.AgilityTechnologies.NeoMessageDialog;

/**
 *
 * @author whauger
 */
public class UtilMethods {
    private static DatatypeFactory _datatypeFactory;

     public static Object[] getLookupValues(String lookupType) {
        try {
            List<LookupValue> LookupTypelist = NeoWebService.getNeoManagerBeanService().getCodeTableByName(LoginPage.neoUser, lookupType);

            ArrayList lookupList = new ArrayList();

            for (LookupValue lookup : LookupTypelist) {
                lookupList.add(lookup.getValue());
            }
            return lookupList.toArray();
        } catch (Exception ex) {
            System.out.println("Exception Caught..." + ex.getMessage());
            ex.printStackTrace();
            NeoMessageDialog.showErrorMessage(ex.getMessage());
            return null;
        }
    }

    public static Object[] getLookupValues(String lookupType, Map<String, LookupValue> lookupMap) {
        try {
            List<LookupValue> LookupTypelist = NeoWebService.getNeoManagerBeanService().getCodeTableByName(LoginPage.neoUser, lookupType);

            ArrayList lookupList = new ArrayList();

            for (LookupValue lookup : LookupTypelist) {
                lookupList.add(lookup.getValue());
                lookupMap.put(lookup.getValue(), lookup);
            }
            return lookupList.toArray();
        } catch (Exception ex) {
            System.out.println("Exception Caught..." + ex.getMessage());
            ex.printStackTrace();
            NeoMessageDialog.showErrorMessage(ex.getMessage());
            return null;
        }
    }

    public static Object[] getLookupValuesById(String lookupType, Map<String, LookupValue> lookupMap) {
        try {
            List<LookupValue> LookupTypelist = NeoWebService.getNeoManagerBeanService().getCodeTableByName(LoginPage.neoUser, lookupType);

            ArrayList lookupList = new ArrayList();

            for (LookupValue lookup : LookupTypelist) {
                lookupList.add(lookup.getValue());
                lookupMap.put(lookup.getId(), lookup);
            }
            return lookupList.toArray();
        } catch (Exception ex) {
            System.out.println("Exception Caught..." + ex.getMessage());
            ex.printStackTrace();
            NeoMessageDialog.showErrorMessage(ex.getMessage());
            return null;
        }
    }

    public static Object[] getLookupValuesForMapInt(String lookupType, Map<String, Integer> lookupMap) {
        try {
            List<LookupValue> LookupTypelist = NeoWebService.getNeoManagerBeanService().getCodeTableByName(LoginPage.neoUser, lookupType);

            ArrayList lookupList = new ArrayList();

            for (LookupValue lookup : LookupTypelist) {
                lookupList.add(lookup.getValue());

                lookupMap.put(lookup.getValue(), Integer.parseInt(lookup.getId()));
            }
            return lookupList.toArray();
        } catch (Exception ex) {
            System.out.println("Exception Caught..." + ex.getMessage());
            ex.printStackTrace();
            NeoMessageDialog.showErrorMessage(ex.getMessage());
            return null;
        }
    }
    public static Object[] getLookupValuesForMapString(String lookupType, Map<String, String> lookupMap) {
        try {
            List<LookupValue> LookupTypelist = NeoWebService.getNeoManagerBeanService().getCodeTableByName(LoginPage.neoUser, lookupType);

            ArrayList lookupList = new ArrayList();

            for (LookupValue lookup : LookupTypelist) {
                lookupList.add(lookup.getValue());

                lookupMap.put(lookup.getValue(), lookup.getId());
            }
            return lookupList.toArray();
        } catch (Exception ex) {
            System.out.println("Exception Caught..." + ex.getMessage());
            ex.printStackTrace();
            NeoMessageDialog.showErrorMessage(ex.getMessage());
            return null;
        }
    }
    public static String convertDateToString(java.util.Date vdate) {

        String DATE_FORMAT = "yyyy/MM/dd";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        String adate = sdf.format(vdate);

        return adate;
    }

    public static XMLGregorianCalendar convertDateToXMLGeoCalender(Date date) {
        try {
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            return getDatatypeFactory().newXMLGregorianCalendar(calendar);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(TableFormater.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static DatatypeFactory getDatatypeFactory() throws DatatypeConfigurationException {
        if (_datatypeFactory == null) {
            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
            }
        }
        return _datatypeFactory;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neo.policy.qlink;

/**
 *
 * @author lesliej
 */
public class QLinkTransctionDetail {
    String transactionType;
    String payrollNumber;
    String policyNumber;
    String persalRef;
    String transactionAmt;
    String transactionStartDate;
    String transactionEndDate;
    String filecreatedDate;

    String qLinkReconStatus;
    String qLinkReconErrorCode;
    String qLinkReconDate;

    String persalReconStatus;
    String persalErrorCode;
    String persalRecondDate;
    String reasonCode;
    String reasonCodeDesc;

    public QLinkTransctionDetail(String transactionType,String payrollNumber,String policyNumber,String persalRef,String transactionAmt,
                                String transactionStartDate,String transactionEndDate,String filecreatedDate,String qLinkReconStatus,
                                String qLinkReconErrorCode,String qLinkReconDate,String persalReconStatus,String persalErrorCode,
                                String persalRecondDate,String reasonCode,String reasonCodeDesc) {

            this.transactionType = transactionType  ;
            this.payrollNumber =  payrollNumber;
            this.policyNumber =policyNumber  ;
            this.persalRef = persalRef;
            this.transactionAmt = transactionAmt ;
            this.transactionStartDate = transactionStartDate ;
            this.transactionEndDate = transactionEndDate ;
            this.filecreatedDate = filecreatedDate ;
            this.qLinkReconStatus =  qLinkReconStatus;
            this.qLinkReconErrorCode =  qLinkReconErrorCode;
            this.qLinkReconDate =  qLinkReconDate;
            this.persalReconStatus =  persalReconStatus;
            this.persalErrorCode =  persalErrorCode;
            this.persalRecondDate =  persalRecondDate;
            this.reasonCode =  reasonCode;
            this.reasonCodeDesc = reasonCodeDesc;

    }


}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.product.system;

/**
 *
 * @author AbigailM
 */
import java.awt.Component;



import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractCellEditor;

import javax.swing.JComboBox;

import javax.swing.JTable;

import javax.swing.table.TableCellEditor;
//import neo.manager.Definition;
import neo.manager.NeoManagerBean;
import neo.product.utils.NeoWebService;

public class BenefitPenalytTable extends AbstractCellEditor implements TableCellEditor {

    private JComboBox cbComponent;

    public BenefitPenalytTable() {

      /*  NeoManagerBean neoManagerBean = null;

        NeoWebService neoWebService = new NeoWebService();
        neoManagerBean = NeoWebService.getNeoManagerBeanService();
        List<Definition> def = neoManagerBean.fetchAllDefinitions();

        String[] stArr = new String[def.size()];
        int i = 0;

        for (Iterator it = def.iterator(); it.hasNext();) {

            Definition nP = (Definition) it.next();

            stArr[i] = nP.getDescription();

            i++;

        }
         cbComponent = new JComboBox(stArr); */

        cbComponent = new JComboBox(new String[] {"Claim" ,"Claim line"} );
      

    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {

        return cbComponent;

    }

    public Object getCellEditorValue() {

        return cbComponent.getSelectedItem();

    }
}



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.product.system;

import java.awt.Component;
import java.util.Map;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import neo.util.UtilMethods;

public class FieldNameTypeTable extends AbstractCellEditor implements TableCellEditor {

    private JComboBox cbComponent;

    public FieldNameTypeTable() {

        //neo.manager.FieldNameType[] fieldValues = neo.manager.FieldNameType.values();

        cbComponent = new JComboBox(UtilMethods.getLookupValues("Field Name Type"));
      
    }

    public FieldNameTypeTable(Map lookupValues) {

        //neo.manager.FieldNameType[] fieldValues = neo.manager.FieldNameType.values();

        cbComponent = new JComboBox(UtilMethods.getLookupValues("Field Name Type", lookupValues));

    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {

        return cbComponent;

    }

    public Object getCellEditorValue() {

        return cbComponent.getSelectedItem();

    }
}



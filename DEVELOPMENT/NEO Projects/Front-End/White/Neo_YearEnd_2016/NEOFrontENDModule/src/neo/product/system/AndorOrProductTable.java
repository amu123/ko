/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.product.system;

import java.awt.Component;
import java.util.Map;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import neo.util.UtilMethods;

public class AndorOrProductTable extends AbstractCellEditor implements TableCellEditor {

    private JComboBox cbComponent;

    public AndorOrProductTable() {

        //neo.manager.LogicalOperandType[] logicalValues = neo.manager.LogicalOperandType.values();

        cbComponent = new JComboBox(UtilMethods.getLookupValues("Logical Operand Type"));

    }

    public AndorOrProductTable(Map lookupValues) {

        //neo.manager.LogicalOperandType[] logicalValues = neo.manager.LogicalOperandType.values();

        cbComponent = new JComboBox(UtilMethods.getLookupValues("Logical Operand Type", lookupValues));

    }

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {

        return cbComponent;

    }

    public Object getCellEditorValue() {

        return cbComponent.getSelectedItem();

    }
}



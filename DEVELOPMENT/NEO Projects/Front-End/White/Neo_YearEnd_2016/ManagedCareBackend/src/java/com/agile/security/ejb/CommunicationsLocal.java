/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.security.ejb;

import com.agile.security.util.EmailContent;
import javax.ejb.Local;

/**
 *
 * @author gerritj
 */
@Local
public interface CommunicationsLocal {

    public void sendPasswordEmail(String emailAddress, String Username, String password);
    //public void sendConfirmNewUserRegistrationEmail(String emailAddress,String Username);
    //public void sendConfirmNewUserActivationEmail(String emailAddress,String Username, String password);
    //public void sendRejectUserEmail(String email, String username);

    public void sendAttachment(String email, String content, byte[] attachment);

    public boolean sendEmailWithOrWithoutAttachment(EmailContent content);
    
    public boolean sendEmailViaLoader(EmailContent content);
    
    public boolean MailValidation(Object objValidate, CommunicationsBean.ValidationType valType);
    
    public boolean sendEmailWithAttachmentsViaLoader(EmailContent content, String resourceLocation);
}

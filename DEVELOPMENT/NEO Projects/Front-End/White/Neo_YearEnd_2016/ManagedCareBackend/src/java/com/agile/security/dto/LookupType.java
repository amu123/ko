/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.security.dto;
public class LookupType {

	private int id;
	private String value;

	public LookupType() {

	}

	public LookupType(int id, String value) {
		this.id = id;
		this.value = value;
	}

	public int getId()
	{
		return this.id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getValue()
	{
		return this.value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}
}

package com.agile.security.dto;

public class WorkItem {
	private int workitemId;
	private int externalReference;
	private int severity;
	private int workitemTypeId;
	
	public int getWorkitemId() {
		return workitemId;
	}
	public void setWorkitemId(int workitemId) {
		this.workitemId = workitemId;
	}
	public int getExternalReference() {
		return externalReference;
	}
	public void setExternalReference(int externalReference) {
		this.externalReference = externalReference;
	}
	public int getSeverity() {
		return severity;
	}
	public void setSeverity(int severity) {
		this.severity = severity;
	}
	public int getWorkitemTypeId() {
		return workitemTypeId;
	}
	public void setWorkitemTypeId(int workitemTypeId) {
		this.workitemTypeId = workitemTypeId;
	}
}

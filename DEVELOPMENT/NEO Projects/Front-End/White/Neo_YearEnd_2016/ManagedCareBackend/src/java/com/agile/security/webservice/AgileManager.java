/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.security.webservice;

import com.agile.security.dto.AuthDetails;
import com.agile.security.dto.BackendMethod;
import com.agile.security.dto.DentalCode;
import com.agile.security.dto.DentalTCode;
import com.agile.security.dto.LookupType;
import com.agile.security.dto.LookupValue;
import com.agile.security.dto.MenuItem;
import com.agile.security.dto.NeoUser;
import com.agile.security.dto.OpticalCode;
import com.agile.security.dto.TCode;
import com.agile.security.dto.WorkItem;
import com.agile.security.dto.WorkItemAndComment;
import com.agile.security.dto.WorkList;
import com.agile.security.dto.eAuthSearchCriteria;
import com.agile.security.dto.eAuthSearchResult;
import com.agile.security.ejb.AgileAccessLocal;
import com.agile.security.ejb.CodeTables;
import com.agile.security.ejb.CommunicationsBean;
import com.agile.security.ejb.CommunicationsLocal;
import com.agile.security.util.EmailContent;
import java.util.Collection;
import java.util.List;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.naming.InitialContext;

/**
 *
 * @author gerritj
 */
@WebService()
@Stateless()
public class AgileManager {
//    @EJB
//    private AgileAccessLocal ejbRef;

    private static  InitialContext iContext = null;
    private static AgileAccessLocal ejbRef = null;
    private static CommunicationsLocal ejbRefComms = null;
    private static String localBean = "/AgileAccessBean/local";
    private static String commsBean = "/CommunicationsBean/local";
    // Add business logic below. (Right-click in editor and choose
    // "Web Service > Add Operation"
    private static CodeTables table = null;
    

    static {
        try {
            iContext = new InitialContext();
            ejbRef = (AgileAccessLocal) iContext.lookup(localBean);
            ejbRefComms = (CommunicationsLocal) iContext.lookup(commsBean);
            table = CodeTables.getInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @WebMethod(operationName = "authenticateUser")
    public NeoUser authenticateUser(@WebParam(name = "userName") String userName, @WebParam(name = "password") String password) throws Exception {
        return ejbRef.authenticateUser(userName, password);
    }

    @WebMethod(operationName = "getBackendMethodsForResp")
    public Collection<BackendMethod> getBackendMethodsForResp(@WebParam(name = "respId") int respId) {
        return ejbRef.getBackendMethodsForResp(respId);
    }

    @WebMethod(operationName = "getMenuItemsForResp")
    public Collection<MenuItem> getMenuItemsForResp(@WebParam(name = "respId") int respId) {
        return ejbRef.getMenuItemsForResp(respId);
    }

    @WebMethod(operationName = "getResponsibilities")
    public NeoUser getResponsibilities(@WebParam(name = "user") NeoUser user) {
        return ejbRef.getResponsibilities(user);
    }

    @WebMethod(operationName = "getUser")
    public NeoUser getUser(@WebParam(name = "userName") String userName, @WebParam(name = "password") String password) {
        return ejbRef.getUser(userName, password);
    }

    @WebMethod(operationName = "getUserByUsername")
    public NeoUser getUserByUsername(String userName){
        return ejbRef.getUserByUsername(userName);
    }

    @WebMethod(operationName = "getUserById")
    public NeoUser getUserById(@WebParam(name = "id") int id) {
        return ejbRef.getUserById(id);
    }

    @WebMethod(operationName = "getAllResponsibilities")
    public List<LookupValue> getAllResponsibilities() {
        return ejbRef.getAllResponsibilities();
    }

    @WebMethod(operationName = "insertWorkItem")
    public int insertWorkItem(@WebParam(name = "_workItem") WorkItem _workItem) {
        return ejbRef.insertWorkItem(_workItem);
    }

    @WebMethod(operationName = "insertWorkList")
    public int insertWorkList(@WebParam(name = "_workList") WorkList _workList) {
        return ejbRef.insertWorkList(_workList);
    }

    @WebMethod(operationName = "getAllWorklistItemsForType")
    public List<WorkItemAndComment> getAllWorklistItemsForType(@WebParam(name = "type") int type) {
        return ejbRef.getAllWorklistItemsForType(type);
    }

    @WebMethod(operationName = "createInactiveUser")
    public int createInactiveUser(@WebParam(name = "user") NeoUser user, @WebParam(name = "comments") String comments) {
        return ejbRef.createInactiveUser(user, comments);
    }

    @WebMethod(operationName = "activateuserAndAssignResponsibilities")
    @Oneway
    public void activateuserAndAssignResponsibilities(@WebParam(name = "userId") int userId, @WebParam(name = "workItemId") int workItemId, @WebParam(name = "responsibilities") Collection<Integer> responsibilities) {
        ejbRef.activateuserAndAssignResponsibilities(userId, workItemId, responsibilities);
    }

    @WebMethod(operationName = "rejectUser")
    @Oneway
    public void rejectUser(@WebParam(name = "userId") int userId, @WebParam(name = "workItemId") int workItemId) {
        ejbRef.rejectUser(userId, workItemId);
    }

    @WebMethod(operationName = "findCodeTableForLookupType")
    public Collection<LookupValue> findCodeTableForLookupType(@WebParam(name = "typeId") int typeId) {
        return ejbRef.findCodeTableForLookupType(typeId);
    }

    @WebMethod(operationName = "fetchSimpleLookupTypes")
    public Collection<LookupType> fetchSimpleLookupTypes() {
        return ejbRef.fetchSimpleLookupTypes();
    }

    @WebMethod(operationName = "fetchLookupValueByName")
    public Collection<LookupValue> fetchLookupValueByName(String name) {
        table = CodeTables.getInstance();
        return ejbRef.fetchLookupValueByName(name, table);
    }

    @WebMethod(operationName = "findTCodeTable")
    public Collection<TCode> findTCodeTable(String tCode) {
        return ejbRef.findTCodeTable(tCode);
    }

    @WebMethod(operationName = "findTCodeTablesForDentalCode")
    public Collection<DentalTCode> findTCodeTablesForDentalCode(int dentalCode) {
        return ejbRef.findTCodeTablesForDentalCode(dentalCode);
    }

    @WebMethod(operationName = "findOpticalProtocolsByCode")
    public Collection<OpticalCode> findOpticalProtocolsByCode(int code) {
        return ejbRef.findOpticalProtocolsByCode(code);
    }

    @WebMethod(operationName = "findOpticalProtocolsByDiscipline")
    public Collection<OpticalCode> findOpticalProtocolsByDiscipline(String discipline) {
        return ejbRef.findOpticalProtocolsByDiscipline(discipline);
    }

    @WebMethod(operationName = "findDentalProtocolsByCode")
    public Collection<DentalCode> findDentalProtocolsByCode(int code) {
        return ejbRef.findDentalProtocolsByCode(code);
    }

    @WebMethod(operationName = "findDentalProtocolsByDiscipline")
    public Collection<DentalCode> findDentalProtocolsByDiscipline(String discipline) {
        return ejbRef.findDentalProtocolsByDiscipline(discipline);
    }
   
    @WebMethod(operationName = "findOpticalTreatmentForCode")
    public OpticalCode findOpticalTreatmentForCode(String discipline, int code) {
        return ejbRef.findOpticalTreatmentForCode(discipline, code);
    }

    @WebMethod(operationName = "findDentalTreatmentForCode")
    public DentalCode findDentalTreatmentForCode(String discipline, int code) {
        return ejbRef.findDentalTreatmentForCode(discipline, code);
    }

     @WebMethod(operationName = "insertAuth")
    public String insertAuth(AuthDetails authDetails) {
         return ejbRef.insertAuthorisation(authDetails);
    }

    @WebMethod(operationName = "searchAuth")
    public Collection<eAuthSearchResult> searchAuthDetails(eAuthSearchCriteria search) {
        return ejbRef.searchAuthDetails(search);
    }

    /*************** Communications *****************/
    @WebMethod(operationName= "sendAttachment")
    public void sendAttachment(String emailAddress, String content, byte[] attachment){
        ejbRefComms.sendAttachment(emailAddress, content, attachment);
    }

    @WebMethod(operationName= "sendEmailWithOrWithoutAttachment")
    public boolean sendEmailWithOrWithoutAttachment(EmailContent content){
        return ejbRefComms.sendEmailWithOrWithoutAttachment(content);
    }
    
    @WebMethod(operationName= "MailValidation")
    public boolean MailValidation(Object objValidate, CommunicationsBean.ValidationType valType){
        return ejbRefComms.MailValidation(objValidate, valType);
    }
    @WebMethod(operationName= "sendEmailWithAttachmentsViaLoader")
    public boolean sendEmailWithAttachmentsViaLoader(EmailContent content, String resourceLocation) {
        return ejbRefComms.sendEmailWithAttachmentsViaLoader(content, resourceLocation);
    }
    @WebMethod(operationName= "sendEmailViaLoader")
    public boolean sendEmailViaLoader(EmailContent content) {
        return ejbRefComms.sendEmailViaLoader(content);
    }

    @WebMethod(operationName= "getNeoResoUsers")
    public Collection<String> getNeoResoUserList(){
        return ejbRef.getNeoResoUserList();
    }
    
    @WebMethod(operationName= "getUsersByRespId")
    public Collection<String> getNeoUserListByRespId(int respId){
        return ejbRef.getNeoUserListByRespId(respId);
    }

    @WebMethod(operationName= "getValueFromTableById")
    public String getValueFromCodeTableForId(int tableId, String id){
        return CodeTables.getInstance().getIdFromCodeTableForValue(tableId, id);
    }

    @WebMethod(operationName= "getValueFromTableByName")
    public String getValueFromCodeTableForId(String tableName, String id){
        return CodeTables.getInstance().getIdFromCodeTableForValue(tableName, id);
    }

    @WebMethod(operationName= "sendPasswordEmail")
    public void sendPasswordEmail(String emailAddress,String Username, String password){
        ejbRefComms.sendPasswordEmail(emailAddress,Username, password);
    }

}

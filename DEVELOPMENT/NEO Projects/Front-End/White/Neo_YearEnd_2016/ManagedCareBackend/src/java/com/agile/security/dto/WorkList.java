package com.agile.security.dto;

import java.util.Date;

public class WorkList {
	
            private int worklistId;
            private int referedToUser;
            private int referedToGroup;
            private Date referedDate;
            private Date startDate;
            private Date endDate;
            private String comments;
            private int referedByUser;
            private WorkItem workItem;
            
			public WorkItem getWorkItem() {
				return workItem;
			}
			public void setWorkItem(WorkItem workItem) {
				this.workItem = workItem;
			}
			public int getWorklistId() {
				return worklistId;
			}
			public void setWorklistId(int worklistId) {
				this.worklistId = worklistId;
			}
			public int getReferedToUser() {
				return referedToUser;
			}
			public void setReferedToUser(int referedToUser) {
				this.referedToUser = referedToUser;
			}
			public int getReferedToGroup() {
				return referedToGroup;
			}
			public void setReferedToGroup(int referedToGroup) {
				this.referedToGroup = referedToGroup;
			}
			public Date getReferedDate() {
				return referedDate;
			}
			public void setReferedDate(Date referedDate) {
				this.referedDate = referedDate;
			}
			public Date getStartDate() {
				return startDate;
			}
			public void setStartDate(Date startDate) {
				this.startDate = startDate;
			}
			public Date getEndDate() {
				return endDate;
			}
			public void setEndDate(Date endDate) {
				this.endDate = endDate;
			}
			public String getComments() {
				return comments;
			}
			public void setComments(String comments) {
				this.comments = comments;
			}
			public int getReferedByUser() {
				return referedByUser;
			}
			public void setReferedByUser(int referedByUser) {
				this.referedByUser = referedByUser;
			}
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.security.ejb;

import com.agile.security.util.EmailContent;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.channels.FileChannel;
import java.sql.Date;
import java.text.MessageFormat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.ejb.Stateless;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

/**
 *
 * @author gerritj
 */
@Stateless
public class CommunicationsBean implements CommunicationsLocal {

    /**
     * ***Production Mail Server****
     */
//    protected static final String SMTP_SERVER = "172.20.96.64";
    /**
     * *****************************
     */
    /**
     * ***UAT Mail Server****
     */
//    protected static final String SMTP_SERVER = "ctmail01.caretech.co.za";  //DONT USE THIS ONE...
    protected static String SMTP_SERVER = "163.197.230.180";      //Dev & Test Environment
    /**
     * **********************
     */
    private static final String defaultEmail = Preferences.userRoot().get("za.co.neo.communication.EMAIL_DEFAULT_ADDRESS", "ERROR - KEY NOT FOUND");
    private static final String live = Preferences.userRoot().get("za.co.neo.communication.TRIGGER_COMMUNICATION", "ERROR - KEY NOT FOUND"); //Flag if its in prod or not
    private static final String neoConfigSMTP = Preferences.userRoot().get("za.co.neo.communication.SMTP_SERVER", "ERROR - KEY NOT FOUND");
    protected static final String ADMIN_EMAIL_USERNAME = "admin@agilityghs.co.za";
    protected static final String STATEMENT_EMAIL_USERNAME = "statements@resomed.co.za";
    protected static final String ADMIN_EMAIL_PASSWORD = "Admin@oasis";
//    protected static final String DOMAIN_USER = "caretech\\lesliej";
//    protected static final String DOMAIN_PASSWORD = "qwerty";
    String memberNumber = "";
    String memberTitle = "";
    String memberInitials = "";
    String memberName = "";
    String memberSurname = "";
    private static boolean emailAddressFromValid = true;
    private static boolean emailAddressToValid = true;

    private String getDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(cal.getTime());
    }

    public static boolean isLive() {
        if (live.equalsIgnoreCase("true")) {
            return true;
        } else {
            return false;
        }
    }

    public void sendAttachment(String emailAddress, String content, byte[] attachment) {
        if (live != null && (!live.equalsIgnoreCase("ERROR - KEY NOT FOUND"))) {
            System.out.println("Sending mail...");
            Properties props = new Properties();
            props.setProperty("mail.transport.protocol", "smtp");

            if (live.equalsIgnoreCase("true")) {
                if (!neoConfigSMTP.equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                    SMTP_SERVER = neoConfigSMTP;
                } else {
                    SMTP_SERVER = "172.20.96.64";
                }
            } else if (!neoConfigSMTP.equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                SMTP_SERVER = neoConfigSMTP;
            } else {
                org.apache.log4j.Logger.getLogger(this.getClass()).error("Propperties: SMTP Server is not set correctly");
                return;
            }

            props.setProperty("mail.host", SMTP_SERVER);
            //for 163.197.230.180
//        props.setProperty("mail.user", "bulkmail@kosrelay.co.za");
//        props.setProperty("mail.password", "Bulkmail01");

//        props.setProperty("mail.user", "caretech\\lesliej");
//        props.setProperty("mail.password", "qwerty");
            try {

                //Session mailSession = Session.getInstance(props, auth);
                Session mailSession = Session.getInstance(props);

                //mailSession.setDebug(true);
                Transport transport = mailSession.getTransport();

                MimeMessage emailMsg = new MimeMessage(mailSession);
                emailMsg.setSubject("Statement");
                emailMsg.setFrom(new InternetAddress(STATEMENT_EMAIL_USERNAME));

                //ADD RECIPIENT TO MESSAGE
                if (live.equalsIgnoreCase("true")) {
                    emailMsg.addRecipient(Message.RecipientType.TO, new InternetAddress(emailAddress));
                } else if (defaultEmail != null && !defaultEmail.equalsIgnoreCase("null") && !defaultEmail.equalsIgnoreCase("") && !defaultEmail.equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                    emailMsg.addRecipient(Message.RecipientType.TO, new InternetAddress(defaultEmail));
                } else {
                    org.apache.log4j.Logger.getLogger(this.getClass()).error("Propperties: Default Email is not set correctly");
                    return;
                }

                //
                // This mail will have to 2 parts, the body and the attachment
                //
                MimeMultipart multipart = new MimeMultipart("related");

                /**
                 * *************
                 * BODY *********************
                 */
                BodyPart messageBodyPart = new MimeBodyPart();
                messageBodyPart.setContent(content, "text/plain");

                // add it
                multipart.addBodyPart(messageBodyPart);

                /**
                 * *************
                 * ATTACHMENT *********************
                 */
                // HEADER IMAGE
                messageBodyPart = new MimeBodyPart();
                messageBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(attachment, "application/octet-stream")));
                messageBodyPart.setFileName("Statement.pdf");

                // add it
                multipart.addBodyPart(messageBodyPart);

                // put everything together
                emailMsg.setContent(multipart);

                //transport.connect(DOMAIN_USER, DOMAIN_PASSWORD);
                transport.connect();

                transport.sendMessage(emailMsg, emailMsg.getRecipients(Message.RecipientType.TO));

                transport.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            org.apache.log4j.Logger.getLogger(this.getClass()).error("Propperties: Communication Trigger is not set correctly");
        }
    }
//Method can send emails without attachements as well as emails with a max of e attachements

    public boolean sendEmailWithOrWithoutAttachment(EmailContent content) {
        boolean isValid = false;
        //Validate member email address
        try {
            new InternetAddress(content.getEmailAddressFrom()).validate();
            isValid = true;
        } catch (Exception ex) {
            //Member Mail failed now trying contact details for call tracking
            try {
                content.setEmailAddressFrom(content.getEmailAddressFrom().trim());
                new InternetAddress(content.getEmailAddressFrom()).validate();
                isValid = true;
            } catch (Exception ex2) {
                org.apache.log4j.Logger.getLogger(this.getClass()).info("MAIL| Member does not have a valid email address");
            }
        }

        if (isValid == true) {
            isValid = false;
            try {
                new InternetAddress(content.getEmailAddressTo()).validate();
                isValid = true;
            } catch (Exception ex) {
                //Member Mail failed now trying contact details for call tracking
                try {
                    content.setEmailAddressTo(content.getEmailAddressTo().trim());
                    new InternetAddress(content.getEmailAddressTo()).validate();
                    isValid = true;
                } catch (Exception ex2) {
                    org.apache.log4j.Logger.getLogger(this.getClass()).info("MAIL| Member does not have a valid email address");
                }
            }
        }

        boolean result = false;
        if (isValid) {
            if (live != null && (!live.equalsIgnoreCase("ERROR - KEY NOT FOUND"))) {
                System.out.println("Sending mail...");

                if (content.getMemberCoverNumber() != null && !content.getMemberCoverNumber().equals("")) {
                    memberNumber = content.getMemberCoverNumber();
                }
                if (content.getMemberTitle() != null && !content.getMemberTitle().equals("")) {
                    memberTitle = UppercaseFirstLetters(content.getMemberTitle().toLowerCase());
                }
                if (content.getMemberInitials() != null && !content.getMemberInitials().equals("")) {
                    memberInitials = content.getMemberInitials().toUpperCase();
                }
                if (content.getName() != null && !content.getName().equals("")) {
                    memberName = UppercaseFirstLetters(content.getName().toLowerCase());
                }
                if (content.getSurname() != null && !content.getSurname().equals("")) {
                    memberSurname = UppercaseFirstLetters(content.getSurname().toLowerCase());
                }

                try {
                    Session mailSession = Session.getInstance(getMailProperties(content), null);

                    MimeMessage emailMsg = new MimeMessage(mailSession);
                    emailMsg.setSubject(content.getSubject());
                    emailMsg.setFrom(new InternetAddress(content.getEmailAddressFrom()));

                    if (live.equalsIgnoreCase("true")) {
                        emailMsg.addRecipient(Message.RecipientType.TO, new InternetAddress(content.getEmailAddressTo()));
                    } else if (defaultEmail != null && !defaultEmail.equalsIgnoreCase("null") && !defaultEmail.equalsIgnoreCase("") && !defaultEmail.equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                        emailMsg.addRecipient(Message.RecipientType.TO, new InternetAddress(defaultEmail));
                    } else {
                        org.apache.log4j.Logger.getLogger(this.getClass()).error("Propperties: Default Email is not set correctly");
                        return false;
                    }

                    MimeMultipart multipart = new MimeMultipart("related");

                    //Set the content of the email
                    multipart.addBodyPart(getMessage(content));
                    //multipart.addBodyPart(getHTMLHeader());
                    multipart.addBodyPart(getEmailHeaderImage(content.getProductId(), content.getType()));
                    //multipart.addBodyPart(getEmailSubHeaderImage());
                    multipart.addBodyPart(getEmailFooterImage(content.getProductId(), content.getType()));

                    if (content.getProductId() == 2) {
                        //get digital sigature
                        multipart.addBodyPart(getSignature(content.getProductId()));
                    }

                    //Add Attachement if attachement is required 
                    if (content.getFirstAttachmentData() != null && !content.getFirstAttachmentData().equals("")) {
                        BodyPart messageBodyPart1 = new MimeBodyPart();
                        messageBodyPart1.setDataHandler(new DataHandler(new ByteArrayDataSource(content.getFirstAttachmentData(), content.getFirstAttachment().getContentType())));
                        messageBodyPart1.setFileName(content.getFirstAttachment().getName());
                        multipart.addBodyPart(messageBodyPart1);
                    }
                    //Add Attachement if attachement is required 
                    if (content.getSecondAttachmentData() != null && !content.getSecondAttachmentData().equals("")) {
                        BodyPart messageBodyPart2 = new MimeBodyPart();
                        messageBodyPart2.setDataHandler(new DataHandler(new ByteArrayDataSource(content.getSecondAttachmentData(), content.getSecondAttachment().getContentType())));
                        messageBodyPart2.setFileName(content.getSecondAttachment().getName());
                        multipart.addBodyPart(messageBodyPart2);
                    }
                    //Add Attachement if attachement is required 
                    if (content.getTheThirdAttachmentData() != null && !content.getTheThirdAttachmentData().equals("")) {
                        BodyPart messageBodyPart3 = new MimeBodyPart();
                        messageBodyPart3.setDataHandler(new DataHandler(new ByteArrayDataSource(content.getTheThirdAttachmentData(), content.getTheThirdAttachment().getContentType())));
                        messageBodyPart3.setFileName(content.getTheThirdAttachment().getName());
                        multipart.addBodyPart(messageBodyPart3);
                    }

                    emailMsg.setContent(multipart);

                    //Print object to the log
                    //content.toString();
                    Transport.send(emailMsg);

                    result = true;
                    System.out.println("MAIL| Mail sent sucessfully <<<<<<<<<");
                } catch (Exception e) {
                    result = false;
                    e.printStackTrace();
                }
            } else {
                org.apache.log4j.Logger.getLogger(this.getClass()).error("Propperties: Communication Trigger is not set correctly");
            }
        }

        return result;
    }

    //<editor-fold defaultstate="collapsed" desc="SEND EMAIL VIA LOADER">
    /*  SEND EMAIL VIA LOADER
     *  Loader that's used for already prepared Mails:
     *  RULES:
     *    - content.getEmailAddressFrom() | content.getEmailAddressTo() can be null
     *    - content.getEmailAddressFrom() will OVERIDE the EML's "From"
     *    - content.getEmailAddressTo() will APPEND the EML's "To" list
     *    - content.getSubject() will OVERIDE the EML's "Subject"
     */
 /*CREATING NEEDED VARIABLES*/
    String presetLoc = "";
    boolean useContentFrom = true;
    boolean useContentTo = true;
    boolean isValid = false;
    String resourceLoc = "";
    protected static File EMLDest = new File("C:/temp/EML_Handler/");
    protected String reportLoc = "";

    /*CREATING NEEDED METHODS*/
    public enum ValidationType {
        EXTERNAL_FILES, //Checks if the external Files exists etc.
        EMAIL_CONTENT       //Checks if all the mandatory fields are filled.
    }

    //Validation Flag
    public void setValid(boolean isValid) {
        if (this.isValid == true) {
            this.isValid = isValid;
        }
    }

    public boolean getValid() {
        return isValid;
    }

    public boolean sendEmailViaLoader(EmailContent content) {
        isValid = true;
        org.apache.log4j.Logger.getLogger(this.getClass()).info("In sendEmailViaLoader");
        presetLoc = "";

        //INIT Poduct ID conditions
        if (content.getProductId() == 1) {
            presetLoc = "C:/EmailImages/Resolution Health/Preset/";
        } else if (content.getProductId() == 2) {
            presetLoc = "C:/EmailImages/Spectramed/Preset/";
        }

        //VALIDATION
        setValid(validation(content, ValidationType.EMAIL_CONTENT));
        boolean result = false;
        if (getValid()) {
            if (live != null && (!live.equalsIgnoreCase("ERROR - KEY NOT FOUND"))) {
                org.apache.log4j.Logger.getLogger(this.getClass()).info("Sending mail...");

                try {
                    Session mailSession = Session.getInstance(getMailProperties(content), null);

                    InputStream source = source = new FileInputStream(presetLoc);
                    MimeMessage message = new MimeMessage(mailSession, source);

                    //VALIDATE
                    if (message.getFrom() == null || message.getFrom()[0] == null) {
                        org.apache.log4j.Logger.getLogger(this.getClass()).error("message.getFrom is empty: \n"
                                + "--------------------------------------------------------\n"
                                + "Please insert \"From: <your@email.com>\" in the EML file.\n"
                                + "--------------------------------------------------------\n");
                        return false;
                    }
                    if (Message.RecipientType.TO == null) {
                        org.apache.log4j.Logger.getLogger(this.getClass()).error("RecipientType.TO is empty: \n"
                                + "--------------------------------------------------------\n"
                                + "Please insert \"To: <your@email.com>\" in the EML file.\n"
                                + "--------------------------------------------------------\n");
                        return false;
                    }
                    if (message.getSubject() == null || message.getSubject().equalsIgnoreCase("")) {
                        org.apache.log4j.Logger.getLogger(this.getClass()).warn("Sending Mail Without a Subject: \n"
                                + "--------------------------------------------------------\n"
                                + "Please insert \"Subject: 'Here's a subject'\" in the EML file.\n"
                                + "--------------------------------------------------------\n");
                    }
                    System.out.println("--");
                    if (!(content.getSubject() == null || content.getSubject().length() < 3)) {
                        message.setSubject(content.getSubject());
                    }
                    System.out.println("Subject : " + message.getSubject());
                    if (useContentFrom == true) {
                        message.setFrom(new InternetAddress(content.getEmailAddressFrom().trim()));
                    } else {
                        for (int i = 0; i < message.getFrom().length; i++) {
                            System.out.println("From : " + message.getFrom()[i]);
                        }
                    }
                    //ADD RECIPIENT TO MESSAGE
                    if (live.equalsIgnoreCase("true")) {
                        message.addRecipient(Message.RecipientType.TO, new InternetAddress(content.getEmailAddressTo()));
                    } else if (defaultEmail != null && !defaultEmail.equalsIgnoreCase("null") && !defaultEmail.equalsIgnoreCase("") && !defaultEmail.equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                        message.addRecipient(Message.RecipientType.TO, new InternetAddress(defaultEmail));
                    } else {
                        org.apache.log4j.Logger.getLogger(this.getClass()).error("Propperties: Default Email is not set correctly");
                    }
                    for (int i = 0; i < message.getRecipients(Message.RecipientType.TO).length; i++) {
                        System.out.println("To : " + message.getRecipients(Message.RecipientType.TO)[i]);
                    }

                    Transport.send(message);

                    result = true;
                    System.out.println("MAIL| Mail sent sucessfully <<<<<<<<<");
                } catch (Exception e) {
                    result = false;
                    e.printStackTrace();
                }
            } else {
                org.apache.log4j.Logger.getLogger(this.getClass()).error("Propperties: Communication Trigger is not set correctly");
            }
        }

        return result;
    }

    private boolean validation(Object objValidate, ValidationType valType) {
        if (getValid()) {
            useContentFrom = false;
            switch (valType) {
                case EMAIL_CONTENT:
                    if (objValidate instanceof EmailContent) {
                        EmailContent content = (EmailContent) objValidate;

                        if (content.getMemberCoverNumber() != null && !content.getMemberCoverNumber().equals("")) {
                            memberNumber = content.getMemberCoverNumber();
                        }
                        if (content.getMemberTitle() != null && !content.getMemberTitle().equals("")) {
                            memberTitle = UppercaseFirstLetters(content.getMemberTitle().toLowerCase());
                        }
                        if (content.getMemberInitials() != null && !content.getMemberInitials().equals("")) {
                            memberInitials = content.getMemberInitials().toUpperCase();
                        }
                        if (content.getName() != null && !content.getName().equals("")) {
                            memberName = UppercaseFirstLetters(content.getName().toLowerCase());
                        }
                        if (content.getSurname() != null && !content.getSurname().equals("")) {
                            memberSurname = UppercaseFirstLetters(content.getSurname().toLowerCase());
                        }

                        presetLoc += loadMailFile(content);
                        System.out.println("EML Location: " + presetLoc);
                        File filelocation = new File(presetLoc);
                        setValid(filelocation.exists());

                        if (getValid()) {
                            //Validate Email Addresses
                            if (content.getEmailAddressFrom() != null || !content.getEmailAddressFrom().contains("@")) {
                                try {
                                    new InternetAddress(content.getEmailAddressFrom()).validate();
                                    useContentFrom = true;
                                } catch (Exception ex) {
                                    //Member Mail failed now trying contact details for call tracking
                                    try {
                                        content.setEmailAddressFrom(content.getEmailAddressFrom().trim());
                                        new InternetAddress(content.getEmailAddressFrom()).validate();
                                        useContentFrom = true;
                                    } catch (Exception ex2) {
                                        org.apache.log4j.Logger.getLogger(this.getClass()).warn("MAIL| EmailAddressFrom does not have a valid email address - will use EML's \"From:\"");
                                        System.out.println("Error Message [FROM] #1: " + ex.getMessage());
                                        System.out.println("Error Message [FROM] #2: " + ex2.getMessage());
                                    }
                                }
                            } else {
                                org.apache.log4j.Logger.getLogger(this.getClass()).warn("MAIL| EmailAddressFrom is empty - will use EML's \"From:\"");
                            }

                            try {
                                new InternetAddress(content.getEmailAddressTo()).validate();
                            } catch (Exception ex) {
                                //Member Mail failed now trying contact details for call tracking
                                try {
                                    content.setEmailAddressTo(content.getEmailAddressTo().trim());
                                    new InternetAddress(content.getEmailAddressTo()).validate();
                                } catch (Exception ex2) {
                                    org.apache.log4j.Logger.getLogger(this.getClass()).warn("MAIL| EmailAddressFrom does not have a valid email address - will use EML's \"To:\"");
                                    System.out.println("Error Message #1: " + ex.getMessage());
                                    System.out.println("Error Message #2: " + ex2.getMessage());
                                }
                            }
                        } else {
                            org.apache.log4j.Logger.getLogger(this.getClass()).error("EML File locaiton no found");
                        }
                    } else {
                        org.apache.log4j.Logger.getLogger(this.getClass()).info("Expected <EmailContent> Object, but found incorrect Object Type");
                    }
                    break;

                case EXTERNAL_FILES:
                    break;
                default:
                    System.out.println("Invalid month.");
                    break;
            }
        }
        return getValid();
    }
    //</editor-fold>

    private String loadMailFile(EmailContent content) {
        String locTypeEml = "NO PATH FOUND.";
        //CONTENT TYPE
        if (content.getType().equalsIgnoreCase("welcomeLetter")) {
            locTypeEml = "Zurreal_WelcomeLetter.eml";
            return locTypeEml;
        } else if (content.getType().contains("Overage_Dependant_Letter")) { //Overage_Dependant_Letter-22 <--Example
            //<editor-fold defaultstate="collapsed" desc="Overage Dependent Letter">
            String[] resultParts = content.getType().split("-");
            if (resultParts.length >= 2 && content.getProductId() == 1) {
                int depAge = Integer.parseInt(resultParts[1]);
                if (depAge == 21) {
                    locTypeEml = "Overage_Dependant_Letter_21.eml";
                } else if (depAge >= 22 && depAge <= 24) {
                    locTypeEml = "Overage_Dependant_Letter_22-24.eml";
                } else if (depAge == 25) {
                    locTypeEml = "Overage_Dependant_Letter_25.eml";
                }
            } else if (content.getProductId() == 2) {
                locTypeEml = "Overage_Dependant_Letter.eml";
            }
            return locTypeEml;

            //</editor-fold>
        }

        org.apache.log4j.Logger.getLogger(this.getClass()).warn("NO PATH FOUD FOR TYPE [" + content.getType() + "]");
        return locTypeEml;
    }

    //<editor-fold defaultstate="collapsed" desc="SEND EMAIL WITH ATTACHMENTS VIA LOADER">
    /*
        resourceLocation arraylist:
            -   0: PDF File
     */
    public boolean sendEmailWithAttachmentsViaLoader(EmailContent content, String resourceLocation) {
        String indexDrive = Preferences.userRoot().get("za.co.neo.document.INDEX_DRIVE", "ERROR - KEY NOT FOUND");
        isValid = true;
        org.apache.log4j.Logger.getLogger(this.getClass()).info("In sendEmailWithAttachmentsViaLoader");
        presetLoc = "";

        //INIT Poduct ID conditions
        if (content.getProductId() == 1) {
            presetLoc = "C:/EmailImages/Resolution Health/Preset/";
            content.setEmailAddressFrom("noreply@resomed.co.za");
        } else if (content.getProductId() == 2) {
            presetLoc = "C:/EmailImages/Spectramed/Preset/";
            content.setEmailAddressFrom("noreply@spectramed.co.za");
        }

        presetLoc += loadMailFile(content);

        setValid(MailValidation(content, ValidationType.EMAIL_CONTENT));
        setValid(MailValidation(presetLoc, ValidationType.EXTERNAL_FILES));
        reportLoc = indexDrive + resourceLocation;
        setValid(MailValidation(reportLoc, ValidationType.EXTERNAL_FILES));
        if (getValid()) {
            setValid(EMLFileConstruct(content));
        }

        return getValid();
    }

    public boolean EMLFileConstruct(EmailContent content) {
        String fileBase = "C:/temp/EML_Handler/";
        EMLDest = new File(fileBase + content.getType() + ".eml");

        int counter = 1;
        while (EMLDest.exists() && counter <= 100) {
            EMLDest = new File(fileBase + content.getType() + "_" + counter + ".eml");
            counter++;
        }
        if (counter >= 101) {
            System.out.println("--------------------------------------------------------------");
            System.out.println("Too many EML files found in [" + fileBase + "]");
            System.out.println("--------------------------------------------------------------");
            setValid(false);
        }

        File source = new File("C:/");
        if (presetLoc.isEmpty()) {
            System.out.println("--------------------------------------------------------------");
            System.out.println("No Resource Path Found");
            System.out.println("--------------------------------------------------------------");
        }
        source = new File(presetLoc);
        FileChannel inputChannel = null;
        FileChannel outputChannel = null;
        try {
            inputChannel = new FileInputStream(source).getChannel();
            outputChannel = new FileOutputStream(EMLDest).getChannel();
            outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
        } catch (IOException ex) {
            System.out.println("--------------------------------------------------------------");
            System.out.println("File Exception: " + ex);
            System.out.println("--------------------------------------------------------------");
            setValid(false);
        } finally {
            try {
                inputChannel.close();
                outputChannel.close();
            } catch (IOException ex) {
                System.out.println("--------------------------------------------------------------");
                System.out.println("Error When Closing File: " + ex);
                System.out.println("--------------------------------------------------------------");
                setValid(false);
            } catch (NullPointerException ex) {
                System.out.println("--------------------------------------------------------------");
                System.out.println("Could not close connection: " + ex);
                System.out.println("--------------------------------------------------------------");
            }
        }
        resourceLoc = EMLDest.getAbsolutePath();
        setValid(EMLChange(content));
        if (getValid()) {
            setValid(sendEMLWithAttachment(content));
        }

        if (getValid()) {
            EMLClose();
        }
        return getValid();
    }

    public boolean sendEMLWithAttachment(EmailContent content) {
        boolean valid = true;
        if (getValid()) {

            if (getValid()) {
                try {
                    Session mailSession = Session.getInstance(getMailProperties(content), null);

                    InputStream source = source = new FileInputStream(resourceLoc);
                    MimeMessage message = new MimeMessage(mailSession, source);

                    //VALIDATE
                    //EMAIL ADDRESS FROM
                    if ((message.getFrom() == null || message.getFrom()[0] == null) && (emailAddressFromValid == false)) {
                        System.out.println("--------------------------------------------------------------");
                        System.out.println("message.getFrom is empty: \n"
                                + "--------------------------------------------------------\n"
                                + "Please insert \"From: <your@email.com>\" in the EML file.\n"
                                + "--------------------------------------------------------\n");
                        System.out.println("--------------------------------------------------------------");
                        valid = false;
                        return false;
                    }

                    //EMAIL ADDRESS TO
                    if ((Message.RecipientType.TO == null) && (emailAddressToValid == false)) {
                        System.out.println("--------------------------------------------------------------");
                        System.out.println("RecipientType.TO is empty: \n"
                                + "--------------------------------------------------------\n"
                                + "Please insert \"To: <your@email.com>\" in the EML file.\n"
                                + "--------------------------------------------------------\n");
                        System.out.println("--------------------------------------------------------------");
                        valid = false;
                        return false;
                    }

                    //SUBJECT
                    if (!(content.getSubject() == null || content.getSubject().length() < 3)) {
                        message.setSubject(content.getSubject());
                    } else if (message.getSubject() == null || message.getSubject().equalsIgnoreCase("")) {
                        System.out.println("--------------------------------------------------------------");
                        System.out.println("Subject is empty: \n"
                                + "--------------------------------------------------------\n"
                                + "Please insert \"Subject: 'Example subject'\" in the EML file.\n"
                                + "--------------------------------------------------------\n");
                        System.out.println("--------------------------------------------------------------");
                        valid = false;
                        return false;
                    }
                    System.out.println("Subject:\t" + message.getSubject());

                    if (emailAddressFromValid) {
                        message.setFrom(new InternetAddress(content.getEmailAddressFrom().trim()));
                    } else {
                        for (Address from : message.getFrom()) {
                            System.out.println("From:\t" + from);
                        }
                    }

                    //ADD RECIPIENT TO MESSAGE
                    if (isLive()) {
                        message.addRecipient(Message.RecipientType.TO, new InternetAddress(content.getEmailAddressTo()));
                    } else if (defaultEmail != null && !defaultEmail.equalsIgnoreCase("null") && !defaultEmail.equalsIgnoreCase("") && !defaultEmail.equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                        message.addRecipient(Message.RecipientType.TO, new InternetAddress(defaultEmail));
                    } else {
                        System.out.println("--------------------------------------------------------------");
                        System.out.println("Properties: \"Default Email\" is not set correctly");
                        System.out.println("--------------------------------------------------------------");
                    }
                    String tabbing = "Actual To:\t- ";
                    String actualTo = message.getRecipients(Message.RecipientType.TO)[0].toString();
                    if (actualTo.length() > 4) {
                        System.out.println(tabbing + actualTo);
                        tabbing = "\t\t- ";
                    }
                    for (int i = 1; i < message.getRecipients(Message.RecipientType.TO).length; i++) {
                        actualTo = message.getRecipients(Message.RecipientType.TO)[i].toString();
                        if (actualTo.length() > 4) {
                            System.out.println(tabbing + actualTo);
                            tabbing = "\t\t- ";
                        }
                    }

                    //ADD ATTACHMENTS TO MESSAGE
                    String sAttachment = "";
                    sAttachment = reportLoc;
                    if (sAttachment.contains(".pdf")) {
                        MimeBodyPart messageBodyPart
                                = new MimeBodyPart();
                        Multipart newMessage = (Multipart) message.getContent();
                        messageBodyPart.setContent(newMessage);
                        Multipart multipart = new MimeMultipart();
                        multipart.addBodyPart(messageBodyPart);
                        MimeBodyPart attachment = new MimeBodyPart();
                        DataSource dSource = new FileDataSource(sAttachment);
                        attachment.setDataHandler(new DataHandler(dSource));
                        attachment.setFileName(content.getType().substring(0, content.getType().lastIndexOf("-")) + ".pdf");
                        multipart.addBodyPart(attachment);
                        message.setContent(multipart);
                    }
                    Transport.send(message);
                    if (getValid() && valid) {
                        System.out.println("--------------------");
                        System.out.println("MAIL| >>>>>>>>> Sent");
                        System.out.println("--------------------");
                    } else {
                        System.out.println("----------------------");
                        System.out.println("MAIL| Failed <<<<<<<<<");
                        System.out.println("----------------------");
                    }
                } catch (FileNotFoundException e) {
                    System.out.println("--------------------------------------------------------------");
                    System.out.println("NO FILE FOUND FOR TYPE [" + e.getMessage() + "]");
                    System.out.println("--------------------------------------------------------------");
                } catch (MessagingException e) {
                    System.out.println("--------------------------------------------------------------");
                    System.out.println("Message Error [" + e.getMessage() + "]");
                    System.out.println("--------------------------------------------------------------");
                } catch (IOException ex) {
                    System.out.println(ex);
                }
            }
        }
        return getValid();
    }

    public void EMLClose() {
        System.gc(); //To Remove the lock on this system
        File fResourseLoc = new File(resourceLoc);
        if (fResourseLoc.exists()) {
            boolean result = false;
            int attempts = 0;
            while (result == false && attempts <= 1000) {
                result = fResourseLoc.delete();
                attempts++;
            }
            if (attempts > 1000) {
                System.out.println("--------------------------------------------------------------");
                System.out.println("Max Attempts Reached to delete the temp file: " + EMLDest.getAbsolutePath());
                System.out.println("--------------------------------------------------------------");
            }
        }
    }

    private static String getMonth(int month, boolean shortDate) {
        String sMonth = "";
        switch (month) {
            case 0:
                if (shortDate) {
                    sMonth = "Jan";
                } else {
                    sMonth = "January";
                }
                break;
            case 1:
                if (shortDate) {
                    sMonth = "Feb";
                } else {
                    sMonth = "February";
                }
                break;
            case 2:
                if (shortDate) {
                    sMonth = "Mar";
                } else {
                    sMonth = "March";
                }
                break;
            case 3:
                if (shortDate) {
                    sMonth = "Apr";
                } else {
                    sMonth = "April";
                }
                break;
            case 4:
                if (shortDate) {
                    sMonth = "May";
                } else {
                    sMonth = "May";
                }
                break;
            case 5:
                if (shortDate) {
                    sMonth = "Jun";
                } else {
                    sMonth = "June";
                }
                break;
            case 6:
                if (shortDate) {
                    sMonth = "Jul";
                } else {
                    sMonth = "July";
                }
                break;
            case 7:
                if (shortDate) {
                    sMonth = "Aug";
                } else {
                    sMonth = "August";
                }
                break;
            case 8:
                if (shortDate) {
                    sMonth = "Sep";
                } else {
                    sMonth = "September";
                }
                break;
            case 9:
                if (shortDate) {
                    sMonth = "Oct";
                } else {
                    sMonth = "October";
                }
                break;
            case 10:
                if (shortDate) {
                    sMonth = "Nov";
                } else {
                    sMonth = "November";
                }
                break;
            case 11:
                if (shortDate) {
                    sMonth = "Dec";
                } else {
                    sMonth = "December";
                }
                break;
        }
        return sMonth;
    }

    private boolean EMLChange(EmailContent content) {
        boolean valid = true;
        boolean inHTML = false;
        String newResourceLoc = resourceLoc + "_new.eml";

        //<editor-fold defaultstate="collapsed" desc="PHASE 1 - Removing the random '=' symbols form the EML">
        BufferedReader br = null;
        BufferedWriter bw = null;
        try {
            br = new BufferedReader(new FileReader(resourceLoc));
            bw = new BufferedWriter(new FileWriter(newResourceLoc));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.contains("<html")) {
                    inHTML = true;
                }

                if (line.contains("/html>")) {
                    inHTML = false;
                }

                if (inHTML) {
                    if (line.endsWith("=")) {
                        if (line != null && line.length() > 0 && line.charAt(line.length() - 1) == '=') {
                            line = line.substring(0, line.length() - 1);
                        }
                    }
                }

                bw.write(line);

                if (inHTML == false) {
                    bw.newLine();
                }
            }
        } catch (Exception e) {
            valid = false;
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                valid = false;
            }
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException e) {
                valid = false;
            }
        }

        File oldFile = new File(resourceLoc);
        oldFile.delete();

        File newFile = new File(newResourceLoc);
        newFile.renameTo(oldFile);
        System.gc();
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="PHASE 2 - Replace certain words">
        br = null;
        bw = null;
        Calendar cal = null;
        try {
            br = new BufferedReader(new FileReader(resourceLoc));
            bw = new BufferedWriter(new FileWriter(newResourceLoc));
            String line;

            while ((line = br.readLine()) != null) {
                if (line.contains(":MemNo:") && content.getMemberCoverNumber() != null) {
                    line = line.replace(":MemNo:", content.getMemberCoverNumber());
                }
                if (line.contains(":MemTitle:") && content.getMemberTitle() != null) {
                    line = line.replace(":MemTitle:", content.getMemberTitle());
                }
                if (line.contains(":MemInitials:") && content.getMemberInitials() != null) {
                    line = line.replace(":MemInitials:", content.getMemberInitials());
                }
                if (line.contains(":MemName:") && content.getName() != null) {
                    line = line.replace(":MemName:", content.getName());
                }
//                if (line.contains(":MemSecName:") && content.getSecondName() != null) {
//                    line = line.replace(":MemSecName:", content.getSecondName());
//                }
                if (line.contains(":MemSurname:") && content.getSurname() != null) {
                    line = line.replace(":MemSurname:", content.getSurname());
                }
                if (line.contains(":DepName:") && content.getDepName() != null) {
                    line = line.replace(":DepName:", content.getDepName());
                }
                if (line.contains(":DepSecName:") && content.getDepSecondName() != null) {
                    line = line.replace(":DepSecName:", content.getDepSecondName());
                }
                if (line.contains(":DepSurname:") && content.getDepSurname() != null) {
                    line = line.replace(":DepSurname:", content.getDepSurname());
                }
                if (line.contains(":DepBDay:") && content.getDepBDay() != null) {
                    Calendar bDayCal = content.getDepBDay();
                    String day = String.valueOf(bDayCal.get(Calendar.DAY_OF_MONTH));
                    String month = getMonth(bDayCal.get(Calendar.MONTH), false);
                    String year = String.valueOf(bDayCal.get(Calendar.YEAR));
                    String sBDayCal = day + " " + month + " " + year;
                    line = line.replace(":DepBDay:", sBDayCal);
                }
                if (line.contains(":OptionName:") && content.getOptionName() != null) {
                    line = line.replace(":OptionName:", content.getOptionName());
                }
                if (line.contains(":GrpCode:") && content.getGroupCode() != null) {
                    line = line.replace(":GrpCode:", content.getGroupCode());
                }
                if (line.contains(":DD:")) {
                    if (cal == null) {
                        cal = new GregorianCalendar();
                    }
                    int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
                    line = line.replace(":DD:", String.valueOf(dayOfMonth));
                }
                if (line.contains(":MM:")) {
                    if (cal == null) {
                        cal = new GregorianCalendar();
                    }
                    String month = getMonth(cal.get(Calendar.MONTH), false);
                    line = line.replace(":MM:", month);
                }
                if (line.contains(":M:")) {
                    if (cal == null) {
                        cal = new GregorianCalendar();
                    }
                    String month = getMonth(cal.get(Calendar.MONTH), true);
                    line = line.replace(":M:", month);
                }
                if (line.contains(":YYYY:")) {
                    if (cal == null) {
                        cal = new GregorianCalendar();
                    }
                    int year = cal.get(Calendar.YEAR);
                    line = line.replace(":YYYY:", String.valueOf(year));
                }

                bw.write(line);
                bw.newLine();
            }
        } catch (Exception e) {
            System.out.println("--------------------------------------------------------------");
            System.out.println("Error when replacing the content: " + e.getMessage());
            System.out.println("--------------------------------------------------------------");
            valid = false;
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                valid = false;
            }
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException e) {
                valid = false;
            }
        }

        oldFile = new File(resourceLoc);
        oldFile.delete();

        newFile = new File(newResourceLoc);
        newFile.renameTo(oldFile);
        System.gc();
        //</editor-fold>

        return valid;
    }

    public boolean MailValidation(Object objValidate, ValidationType valType) {
        boolean valid = true;
        EmailContent content = null;
        switch (valType) {
            case EMAIL_CONTENT:
                if (objValidate instanceof EmailContent) {
                    content = (EmailContent) objValidate;
                    try {
                        //******************GETTER ITERATOR
                        BeanInfo info = Introspector.getBeanInfo(content.getClass(), Object.class);
                        PropertyDescriptor[] props = info.getPropertyDescriptors();
                        for (PropertyDescriptor pd : props) {
                            String name = pd.getName();
                            Method getter = pd.getReadMethod();

                            Object value = getter.invoke(content);

                            if (String.valueOf(value).equalsIgnoreCase("null") || String.valueOf(value).equals("0") || String.valueOf(name).equalsIgnoreCase("emailAddressTo")
                                    || String.valueOf(name).equalsIgnoreCase("emailAddressFrom") || String.valueOf(name).equalsIgnoreCase("type")
                                    || String.valueOf(name).equalsIgnoreCase("productId")) {
                                continue;
                            }
                            if (name.length() < 9) {
                                System.out.println("- " + name + "\t\t:\t[" + value + "]");
                            } else {
                                System.out.println("- " + name + "\t:\t[" + value + "]");
                            }
                        }
                        //******************GETTER ITERATOR
                        if (valid) {
                            //Validate Email Addresses
                            if (content.getEmailAddressFrom() == null || !content.getEmailAddressFrom().contains("@")) {
                            } else {
                                try {
                                    new InternetAddress(content.getEmailAddressFrom()).validate();
                                } catch (Exception ex) {
                                    emailAddressFromValid = false;
                                    System.out.println("--------------------------------------------------------------");
                                    System.out.println("Error Message [FROM]: " + ex.getMessage());
                                    System.out.println("--------------------------------------------------------------");
                                    //Subject might be set on the EML side
                                }
                            }

                        } else {
                            valid = false;
                        }
                    } catch (IntrospectionException ex) {
                        System.out.println(ex.getMessage());
                    } catch (IllegalAccessException ex) {
                        System.out.println(ex.getMessage());
                    } catch (IllegalArgumentException ex) {
                        System.out.println(ex.getMessage());
                    } catch (InvocationTargetException ex) {
                        System.out.println(ex.getMessage());
                    }
                } else {
                    System.out.println("--------------------------------------------------------------");
                    System.out.println("Found incorrect Object Type, Expected <EmailContent> Object");
                    System.out.println("--------------------------------------------------------------");
                }
                break;

            case EXTERNAL_FILES:
                valid = ValidateMailFile(String.valueOf(objValidate));
                break;
            default:
                System.out.println("Invalid Validation Type");
                break;
        }
        return valid;
    }

    private static boolean ValidateMailFile(String location) {
        boolean valid = false;
        if (!location.isEmpty()) {
            File filelocation = new File(location);
            valid = (filelocation.exists());
        }
        if (!valid) {
            System.out.println("--------------------------------------------------------------");
            System.out.println("NO PATH FOUND FOR TYPE [" + location + "]");
            System.out.println("--------------------------------------------------------------");
        }
        return valid;
    }

    //</editor-fold>
    public Properties getMailProperties(EmailContent content) {
        Properties props = new Properties();

        if (live.equalsIgnoreCase("true")) {
            if (!neoConfigSMTP.equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                SMTP_SERVER = neoConfigSMTP;
            } else {
                SMTP_SERVER = "172.20.96.64";
            }
        } else if (!neoConfigSMTP.equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
            SMTP_SERVER = neoConfigSMTP;
        } else {
            org.apache.log4j.Logger.getLogger(this.getClass()).error("Propperties: SMTP Server is not set correctly");
            return null;
        }
        props.put("mail.smtp.host", SMTP_SERVER);
        String LetterType = content.getType();

        //Determine msg for email according to type of document ('claim', 'statement', 'preAuth', 'benefit', 'survey', 'memberCert', 'custom')
        if (SMTP_SERVER.equalsIgnoreCase("172.20.96.64")) {
            if (LetterType.equalsIgnoreCase("claim")) {
                props.setProperty("mail.user", "claims@agilityrelay.co.za");
                props.setProperty("mail.password", "Claicoza.2015!");
            } else if (LetterType.equalsIgnoreCase("statement")) {
                props.setProperty("mail.user", "membership@agilityrelay.co.za");
                props.setProperty("mail.password", "Membcoza.2015!");
            } else if (LetterType.equalsIgnoreCase("preAuth")) {
                props.setProperty("mail.user", "preauth@agilityrelay.co.za ");
                props.setProperty("mail.password", "Preucoza.2015!");
            } else if (LetterType.equalsIgnoreCase("benefit")) {
                props.setProperty("mail.user", "membership@agilityrelay.co.za");
                props.setProperty("mail.password", "Membcoza.2015!");
            } else if (LetterType.equalsIgnoreCase("tax")) {
                props.setProperty("mail.user", "tax@agilityrelay.co.za");
                props.setProperty("mail.password", "Taxcoza.2015!");
            } else if (LetterType.equalsIgnoreCase("survey")) {
                props.setProperty("mail.user", "clientservices@agilityrelay.co.za");
                props.setProperty("mail.password", "Cliecoza.2015!");
            } else if (LetterType.equalsIgnoreCase("memberCert")) {
                props.setProperty("mail.user", "membership@agilityrelay.co.za");
                props.setProperty("mail.password", "Membcoza.2015!");
            } else if (LetterType.equalsIgnoreCase("custom")) {
                props.setProperty("mail.user", "clientservices@agilityrelay.co.za");
                props.setProperty("mail.password", "Cliecoza.2015!");
                props.setProperty("mail.password", "Membcoza.2015!");
            } else if (LetterType.equalsIgnoreCase("updateToContactDetails")) {
                props.setProperty("mail.user", "membership@agilityrelay.co.za");
                props.setProperty("mail.password", "Membcoza.2015!");
            } else if (LetterType.equalsIgnoreCase("welcomeLetter")) {
                props.setProperty("mail.user", "membership@agilityrelay.co.za");
                props.setProperty("mail.password", "Membcoza.2015!");
            }
        }
        return props;
    }

    public static String UppercaseFirstLetters(String str) {
        String upperCase[] = str.split(" ");
        String finalRet = "";
        for (int i = 0; i < upperCase.length; i++) {
            upperCase[i] = upperCase[i].substring(0, 1).toUpperCase() + upperCase[i].substring(1).toLowerCase();
            finalRet += upperCase[i] + " ";
        }
        return finalRet.trim();
    }

    public void sendPasswordEmail(String emailAddress, String Username, String password) {
        if (live != null && (live.equalsIgnoreCase("ERROR - KEY NOT FOUND"))) {
            System.out.println("Sending mail...");
            Properties props = new Properties();
            props.setProperty("mail.transport.protocol", "smtp");
            if (live.equalsIgnoreCase("true")) {
                if (!neoConfigSMTP.equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                    SMTP_SERVER = neoConfigSMTP;
                } else {
                    SMTP_SERVER = "172.20.96.64";
                }
            } else if (!neoConfigSMTP.equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                SMTP_SERVER = neoConfigSMTP;
            } else {
                org.apache.log4j.Logger.getLogger(this.getClass()).error("Propperties: SMTP Server is not set correctly");
                return;
            }
            props.setProperty("mail.host", SMTP_SERVER);
            //props.setProperty("mail.user", "caretech\\lesliej");
            //props.setProperty("mail.password", "qwerty");

            try {

                Authenticator auth = new PasswordAuthenication();
//            Session mailSession = Session.getInstance(props, auth);
                Session mailSession = Session.getInstance(props);

                //mailSession.setDebug(true);
                Transport transport = mailSession.getTransport();

                MimeMessage emailMsg = new MimeMessage(mailSession);
                emailMsg.setSubject("Forgotten password");
                emailMsg.setFrom(new InternetAddress(ADMIN_EMAIL_USERNAME));

                //ArrayList<ClaimLineCom> tmpClaimLine = (ArrayList<ClaimLineCom>) claimCoverMail.getClaimCom().getClaimLinesCom();
                //ADD RECIPIENT TO MESSAGE
                if (live.equalsIgnoreCase("true")) {
                    emailMsg.addRecipient(Message.RecipientType.TO, new InternetAddress(emailAddress));
                } else if (defaultEmail != null && !defaultEmail.equalsIgnoreCase("null") && !defaultEmail.equalsIgnoreCase("") && !defaultEmail.equalsIgnoreCase("ERROR - KEY NOT FOUND")) {
                    emailMsg.addRecipient(Message.RecipientType.TO, new InternetAddress(defaultEmail));
                } else {
                    org.apache.log4j.Logger.getLogger(this.getClass()).error("Propperties: Default Email is not set correctly");
                    return;
                }

                //
                // This HTML mail have to 2 part, the BODY and the embedded images and CSS
                //
                MimeMultipart multipart = new MimeMultipart("related");

                // first part  (the html)
                BodyPart messageBodyPart = new MimeBodyPart();

                StringBuilder htmlText = new StringBuilder().append("<head>");

                htmlText.append("<style type=\"text/css\">");

                htmlText.append("table { border: 1px #0278c0 solid;} " + '\n');
                htmlText.append("td {border: 1px #4a6077 solid;} " + '\n');
                htmlText.append("tr {border: 1px #4a6077 solid;} " + '\n');
                htmlText.append("th { border: 1px #4a6077 solid;background-color: #005294;color: #ffffff;} " + '\n');
                htmlText.append(".mainFrame { width:800px; } " + '\n');
                htmlText.append("</style></head><div class=\"mainFrame\">");

                //htmlText.append("<img src=\"cid:header\"></br><img src=\"cid:subheader\">");
                //DATE
                //htmlText.append("<p align=\"left\">" + getDate() + "</p>");
                htmlText.append("<br>");
                htmlText.append("<p align=\"left\">Dear ").append(Username).append(",</p>");
                htmlText.append("<p align=\"left\">Your NEO access password is: ").append(password).append("</p>");
                htmlText.append("<p align=\"left\">Regards<br/>Agility Global Health Solutions" + "</p>");
                htmlText.append("<br>");

                htmlText.append("<img src=\"cid:footer\"></div>");

                //System.out.println(htmlText.toString());
                messageBodyPart.setContent(htmlText.toString(), "text/html");

                // add it
                multipart.addBodyPart(messageBodyPart);

                // HEADER IMAGE
                /*messageBodyPart = new MimeBodyPart();
                 messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\header.jpg")));
                 messageBodyPart.setHeader("Content-ID", "<header>");*/
                // add it
                //multipart.addBodyPart(messageBodyPart);
                // SUBHEADER IMAGE
//            messageBodyPart = new MimeBodyPart();
//            messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\subheader.jpg")));
//            messageBodyPart.setHeader("Content-ID", "<subheader>");
                // add it
                //multipart.addBodyPart(messageBodyPart);
                // FOOTER IMAGE
                messageBodyPart = new MimeBodyPart();
                messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\footer.jpg")));
                messageBodyPart.setHeader("Content-ID", "<footer>");
                // add it
                multipart.addBodyPart(messageBodyPart);

                // put everything together
                emailMsg.setContent(multipart);

                //            transport.connect(DOMAIN_USER, DOMAIN_PASSWORD);
                transport.connect();

                transport.sendMessage(emailMsg, emailMsg.getRecipients(Message.RecipientType.TO));

                transport.close();

            } catch (Exception e) {
                e.printStackTrace();

            }
        } else {
            org.apache.log4j.Logger.getLogger(this.getClass()).error("Propperties: Communication Trigger is not set correctly");
        }
    }

    private BodyPart getEmailHeaderImage(int productId, String type) throws Exception {

        BodyPart messageBodyPart = new MimeBodyPart();
        System.out.println("getEmailHeaderImage productId = " + productId);

        if (type.equalsIgnoreCase("survey")) {
            if (productId == 1) {
                messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\Reso Survey Header.png")));
            } else if (productId == 2) {
                messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\Spectramed Survey Header.jpg")));
            }
        } else if (type.equalsIgnoreCase("memberCert")) {
            if (productId == 1) {
                messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\RESO_MCT_header.jpg")));
            } else if (productId == 2) {
                messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\Spectramed Header MCT.jpg")));
            }
        } else if (type.equalsIgnoreCase("updateToContactDetails")) {
            if (productId == 1) {
                messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\RESO_MCT_header.jpg")));
            } else if (productId == 2) {
                messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\Spectramed Header MCT.jpg")));
            }
        } else if (productId == 1) {
            messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\web_header.jpg")));
        } else if (productId == 2) {
            messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\Spectramed Header.png")));
        } else {
            messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\Agility Header.png")));
        }

        System.out.println("MAIL| Retrieved header for mail...");
        messageBodyPart.setHeader("Content-ID", "<header>");
        return messageBodyPart;
    }

    private BodyPart getEmailFooterImage(int productId, String type) throws Exception {

        BodyPart messageBodyPart = new MimeBodyPart();

        if (type.equalsIgnoreCase("survey")) {
            if (productId == 1) {
                messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\Reso Survey Footer.png")));
            } else if (productId == 2) {
                messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\Spectramed Survey Footer.jpg")));
            }
        } else if (type.equalsIgnoreCase("memberCert")) {
            if (productId == 1) {
                messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\Reso Footer 100dpi.png")));
            } else if (productId == 2) {
                messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\Spectramed Footer MCT.png")));
            }
        } else if (type.equalsIgnoreCase("updateToContactDetails")) {
            if (productId == 1) {
                messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\Reso Footer 100dpi.png")));
            } else if (productId == 2) {
                messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\Spectramed Footer MCT.png")));
            }
        } else if (productId == 1) {
            messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\web_footer.jpg")));
        } else if (productId == 2) {
            messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\Spectramed footer.png")));
        } else {
            messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\Agility Footer.png")));
        }
        System.out.println("MAIL| Retrieved footer for mail...");
        messageBodyPart.setHeader("Content-ID", "<footer>");
        return messageBodyPart;
    }

    private BodyPart getSignature(int productId) throws Exception {
        BodyPart messageBodyPart = new MimeBodyPart();
        if (productId == 2) {
            messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\Spectramed signiture.png")));
            System.out.println("MAIL| Email digital signature retreived for spectramed...");
        }
        messageBodyPart.setHeader("Content-ID", "<signature>");
        return messageBodyPart;
    }

    //Determines the appropriate message according to the type of document
    private BodyPart getMessage(EmailContent content) throws MessagingException {
        String header = "";
        String schemeName = "";
        String footer = "";
        String callCentre = "";
        String website = "";
        String enquiriesEmail = "";
        String cardRequestEmail = "";
        String contributionEmail = "";
        String applicationEmail = "";
        String amendEmail = "";
        String LetterType = content.getType();

        int productId = content.getProductId();

        if (productId == 0) {
            schemeName = "AgilityGHS";
            header = "cid:header";
            footer = "cid:footer";
            callCentre = "0861 791 6425";
            website = "www.agilityghs.com";
            enquiriesEmail = "";
        } else if (productId == 1) {
            schemeName = "Resolution Health";
            header = "cid:header";
            footer = "cid:footer";
            callCentre = "0861 796 6400";
            website = "www.resomed.co.za";
            enquiriesEmail = "clientservices@resomed.co.za";
            cardRequestEmail = "cardrequests@resomed.co.za";
            contributionEmail = "contribution@resomed.co.za";
            applicationEmail = "application@resomed.co.za";
            amendEmail = "amend@resomed.co.za";
        } else if (productId == 2) {
            schemeName = "Spectramed";
            header = "cid:header";
            footer = "cid:footer";
            callCentre = "0861 497 497";
            website = "www.spectramed.co.za";
            enquiriesEmail = "enquiries@spectramed.co.za";
        }

        BodyPart messageBodyPart = new MimeBodyPart();
        StringBuilder htmlText = new StringBuilder().append("<head>");
        htmlText.append("<style type=\"text/css\">");

        htmlText.append("table { border: 1px #0278c0 solid;} " + '\n');
        htmlText.append("td {border: 1px #4a6077 solid;} " + '\n');
        htmlText.append("tr {border: 1px #4a6077 solid;} " + '\n');
        htmlText.append("th { border: 1px #4a6077 solid; background-color: #005294;color: #ffffff;} " + '\n');
        htmlText.append(".mainFrame { width:800px; max-width:800px } " + '\n');
        htmlText.append("</style></head><div style=\"max-width:800px; font-size:14px; font-family:Calibri; white-space: pre;\" class=\"mainFrame\">");
        //Heeader
        htmlText.append("<img src=\"").append(header).append("\"></br>");

        String msg = null;

        System.out.println("MAIL| Email Letter Type is: " + LetterType + "...");
        //Determine msg for email according to type of document ('claim', 'statement', 'preAuth', 'benefit', 'survey', 'memberCert', 'updateToContactDetails', 'custom')
        if (LetterType.equalsIgnoreCase("claim")) {
            msg = "<b style=\"font-size:18px; font-weight: bold;\">Dear " + schemeName + " Member/Provider,</b>"
                    + "<br/><br/>"
                    + "Please find attached your " + schemeName + " claims statement which provides you with detailed<br/>"
                    + " information on both your claims and payments processed during the last payment run. "
                    + "<br/><br/>"
                    + ":replace:"
                    + "<br/><br/>"
                    + getKindRegards(schemeName, productId, LetterType);
        } else if (LetterType.equalsIgnoreCase("statement")) {
            msg = "<b style=\"font-size:18px; font-weight: bold;\">Dear " + schemeName + " Member/Provider,</b>"
                    + "<br/><br/>"
                    + "Please find attached your " + schemeName + " statement."
                    + "<br/><br/>"
                    + ":replace:"
                    + "<br/><br/>"
                    + getKindRegards(schemeName, productId, LetterType);
        } else if (LetterType.equalsIgnoreCase("preAuth")) {
            msg = "<b style=\"font-size:18px; font-weight: bold;\">Dear " + schemeName + " Member/Provider,</b>"
                    + "<br/><br/>"
                    + "We have attached an important document regarding your  request for pre-authorisation.<br/>"
                    + "Please make sure that you are satisfied with the contents thereof and keep it in a safe place."
                    + "<br/><br/>"
                    + ":replace:"
                    + "<br/><br/>"
                    + getKindRegards(schemeName, productId, LetterType);
        } else if (LetterType.equalsIgnoreCase("benefit")) {
            msg = "<b style=\"font-size:18px; font-weight: bold;\">Dear " + schemeName + " Member/Provider,</b>"
                    + "<br/><br/>"
                    + "Please find attached your " + schemeName + " benefit letter."
                    + "<br/><br/>"
                    + ":replace:"
                    + "<br/><br/>"
                    + getKindRegards(schemeName, productId, LetterType);
        } else if (LetterType.equalsIgnoreCase("tax")) {
            msg = "<b style=\"font-size:18px; font-weight: bold;\">Dear Member,</b><br/><br/>"
                    + "Herewith please find your " + schemeName + " tax certificate.<br/><br/>"
                    + "Please note this certificate must be issued with the Tax certificate you should have<br/>"
                    + "received reflecting claims and contribution details.<br/><br/>"
                    + ":replace:"
                    + "<br/><br/>"
                    + getKindRegards(schemeName, productId, LetterType);
        } else if (LetterType.equalsIgnoreCase("survey")) {
            if (productId == 1) {
                msg = "<b style=\"font-size:18px; font-weight: bold;\">Dear valued Member,                                                      [" + memberNumber + "]</b><br/><br/>"
                        + "Your experience with the Scheme is extremely important to us and we're always looking at ways in <br/>"
                        + "which we can further enhance the service we provide to our members. To help us do this, please<br/>"
                        + "rate the service you received from us by answering a single question via the link below.<br/><br/>"
                        + "<a href=\"http://agilitysurvey.polldaddy.com/s/agility-resolution-short-survey?p=1\">Rate our service</a><br/><br/>"
                        + "As appreciation for your effort in making time available to rate our service, you will automatically be<br/>"
                        + "entered in a monthly lucky draw where you can win a free 6-month <i style=\"font-style: italic;\"><b style=\"font-weight:bold;\">Zurreal</b> Platinum</i> membership! <br/>"
                        + "Can't wait? <a href=\"http://bit.ly/1INJefi\">Click here</a> to learn more about this amazing rewards programme.<br/><br/>"
                        + getKindRegards(schemeName, productId, LetterType)
                        + "<br/><div style=\"font-size:12px;\"><b style=\"font-size:14px;\">*</b> Terms & Conditions apply (<a href=\"http://www.resomed.co.za/index.php?option=com_content&view=article&layout=edit&id=216\">Click here</a> for terms & conditions).</div>";
            } else if (productId == 2) {
                msg = "<b style=\"font-size:18px; font-weight: bold;\">Dear " + memberName + "</b><br/><br/>"
                        + "Membership nr: " + memberNumber + " <br/><br/>"
                        + "You recently interacted with the Agility contact centre regarding your Spectramed medical scheme<br/>"
                        + "membership. Your service experience is extremely important to us and we want to make sure we<br/>"
                        + "consistently offer the best possible service to you. To help us do this, please rate the service you received<br/>"
                        + "by answering a single question via the link below. <br/><br/>"
                        + "<a href=\"http://agilitysurvey.polldaddy.com/s/agility-spectramed-short-survey?p=1\">Rate our service</a><br/><br/>"
                        + "In appreciation for your efforts, you will automatically be entered in a monthly lucky draw where you can<br/>"
                        + "win a free 6-month <i style=\"font-style: italic;\"><b style=\"font-weight:bold;\">Zurreal</b>Platinum</i> membership. Terms and conditions apply (<a href=\"http://www.spectramed.co.za/node/197\">click here</a>).<br/><br/>"
                        + "<a href=\"http://www.zurreal.co.za/#!zurreal-wellbeing-and-rewards-programme/c1z64\">Click here</a> to learn more about <i style=\"font-style: italic;\"><b style=\"font-weight:bold;\">Zurreal</b>Platinum.</i> <br/><br/>"
                        + getKindRegards(schemeName, productId, LetterType);
            }
        } else if (LetterType.equalsIgnoreCase("updateToContactDetails")) {
            if (productId == 1) {
                msg = "<b style=\"font-size:12pt; font-weight: bold;\">Dear " + memberTitle + " " + memberInitials + " " + memberSurname + "</b><br/><br/>"
                        + "Membership nr: " + memberNumber + " <br/><br/>"
                        + "<span style=\"font-family: Calibri; font-size:14px;\">As requested, attached please find your " + schemeName + " Medical Scheme Updated Contact Details."
                        + "<br/><br/>"
                        + "This is an unattended mailbox, please do not reply to this email.  Should you require any further assistance, please don't hesitate to contact<br/>"
                        + "our customer service centre on " + callCentre + ", or email us at " + enquiriesEmail + "."
                        + "<br/><br/>"
                        + "Please remember that you can also log onto your personal member profile on our website at " + website + ", should you require<br/>"
                        + "more information."
                        + "<br/><br/>"
                        + getKindRegards(schemeName, productId, LetterType)
                        + "<br/><br/><br/><br/>";
            } else if (productId == 2) {
                msg = "<b style=\"font-size:12pt; font-weight: bold;\">Dear " + memberTitle + " " + memberInitials + " " + memberSurname + "</b><br/><br/>"
                        + "Membership nr: " + memberNumber + " <br/><br/>"
                        + "<span style=\"font-family: Calibri; font-size:10pt;\">As requested, attached please find your " + schemeName + " Medical Scheme Updated Contact Details."
                        + "<br/><br/>"
                        + "This is an unattended mailbox, please do not reply to this email.  Should you require any further assistance, please don't hesitate to <br/>"
                        + "contact our customer service centre on " + callCentre + ", or email us at " + enquiriesEmail + "."
                        + "<br/><br/>"
                        + "Please remember that you can also log onto your personal member profile on our website at " + website + ", should you<br/>"
                        + "require more information."
                        + "<br/><br/>"
                        + getKindRegards(schemeName, productId, LetterType);
            }
        } else if (LetterType.equalsIgnoreCase("memberCert")) {
            if (productId == 1) {
                msg = "<b style=\"font-size:14pt; font-weight: bold;\">Dear valued Resolution Health member,</b><br/><br/>"
                        + "Please do not reply to this email as this email address is dedicated to the distribution of membership certificates only.<br/>"
                        + "Should you have any queries related to your membership certificate or Scheme membership, please refer to the list of other specialised<br/>email addresses that the Scheme has set up for more specific enquiries:<br/><br/>"
                        + "&#8226; For <b style=\\\"font-size:18px; font-weight: bold;\\\">general enquiries</b> relating to Resolution Health, email clientservices@resomed.co.za<br/><br/>"
                        + "&#8226; Should your query relate to your Resolution Health membership card, please send your request or query to cardrequests@resomed.co.za<br/>"
                        + "<br/>"
                        + "&#8226; All <b style=\\\"font-size:18px; font-weight: bold;\\\">contribution-related</b> queries can be directed to contribution@resomed.co.za. This includes any queries relating to:<br/>"
                        + "	o Proof of your contribution payments<br/>"
                        + "	o Your monthly Resolution Health premiums<br/>"
                        + "	o The suspension of your Resolution Health cover<br/>"
                        + "	o Debit order returns<br/>"
                        + "	o The reconciliation of contributions<br/>"
                        + "	o Any refunds (savings and contributions)"
                        + "<br/><br/>"
                        + "&#8226; For <b style=\\\"font-size:18px; font-weight: bold;\\\">new member applications</b>, or to add a dependant to your existing Resolution Health cover, simply pop us an email<br/>"
                        + "   at application@resomed.co.za<br/><br/>"
                        + "&#8226; The amend@resomed.co.za inbox is dedicated to the following <b style=\\\"font-size:18px; font-weight: bold;\\\">changes or updates</b> that you may wish to make as an<br/>"
                        + "   existing Resolution Health member:<br/>"
                        + "	o Updating your contact details<br/>"
                        + "	o Updating your banking details<br/>"
                        + "	o Changing the inception date of your Resolution Health cover<br/>"
                        + "	o Changing or updating the principal member<br/>"
                        + "	o Requesting that changes be made to your Resolution Health Membership Certificate<br/>"
                        + "	o Requesting a copy of your Resolution Health Membership Certificate<br/>"
                        + "	o Requesting that changes be made to your Resolution Health Tax Certificate<br/>"
                        + "	o Requesting a copy of your Resolution Health Tax Certificate"
                        + "<br/><br/>"
                        + getKindRegards(schemeName, productId, LetterType)
                        + "<br/><br/><br/><br/>";
            } else if (productId == 2) {
                msg = "<b style=\"font-family: Calibri; font-size:12pt; font-weight: bold;\">Dear Member,</b><br/><br/>"
                        + "<span style=\"font-family: Calibri; font-size:10pt; \">As requested, attached please find your " + schemeName + " Medical Scheme membership certificate."
                        + "<br/><br/>"
                        + "This is an unattended mailbox, please do not reply to this email.  Should you require any further assistance, please don't hesitate to <br/>"
                        + "contact our customer service centre on " + callCentre + ", or email us at " + enquiriesEmail + "."
                        + "<br/><br/>"
                        + "Please remember that you can also log onto your personal member profile on our website at " + website + ", should you<br/>"
                        + "require more information."
                        + "<br/><br/>"
                        + getKindRegards(schemeName, productId, LetterType);
            }
        } else if (LetterType.equalsIgnoreCase("custom")) {
            if (content.getContent() != null && !content.getContent().equals("")) {
                msg = content.getContent();
            }
        }
        //Determine call centre and enquireies for appropriate scheme. 
        //All ':replace:' occurrences will be replaced by one of the following
        String replaced = null;
        if ((productId == 1 || productId
                == 2)) {
            replaced = msg.replace(":replace:", "Please do not reply to this email. Should you require any further assistance, please don't hesitate to <br/>"
                    + " contact our customer call centre on " + callCentre + ", or email us at " + enquiriesEmail + "."
                    + "<br/><br/>"
                    + "Please remember that you can also log onto your personal member profile on our website at<br/>"
                    + website + ", should you require more information.");
        } else {
            replaced = msg.replace(":replace:", "Please do not reply to this email. Should you require any further assistance, please don't hesitate to <br/> contact our customer service centre on " + callCentre);
        }

        //Set Message
        htmlText.append(
                "<br/>");
        htmlText.append(
                "<p align=\"left\">").append(replaced).append("</p>");
        if (LetterType.equalsIgnoreCase("updateToContactDetails") || LetterType.equalsIgnoreCase("memberCert")) { //the footer BG is white thus it looks like the signature and the footer is far away
            if (productId == 2) {
            } else {
                htmlText.append("<br/>");
            }
        } else {
            htmlText.append("<br/>");
        }

        //Footer
        htmlText.append(
                "<img src=\"").append(footer).append("\"></div>");

        System.out.println(htmlText);

        messageBodyPart.setContent(htmlText.toString(), "text/html");

        return messageBodyPart;
    }

    public String getKindRegards(String schemeName, int productId, String type) {
        String regards = "";
        if (type.equalsIgnoreCase("survey")) {
            if (productId == 1) {
                regards = "Yours in health,<br/><br/>"
                        + "The member satisfaction team<br/>";
            } else if (productId == 2) {
                regards = "Yours in health,<br/><br/><br/>"
                        + "The Agility member satisfaction team<br/>";
            }
        } else if (type.equalsIgnoreCase("memberCert")) {
            if (productId == 2) {
                String signature = "cid:signature";
                regards = "Kind regards,<br/>"
                        + "<img src=\"" + signature + "\"></span>";
            } else if (productId == 1) {
                regards = "Should you wish to have a chat with one of our friendly consultants, please don't hesitate to get in touch on <b style=\\\"font-size:18px; font-weight: bold;\\\">0861 796 6400</b>.<br/><br/>"
                        + "Yours in health and wellness,<br/><br/>"
                        + "The Resolution Health Team.";
            }
        } else if (type.equalsIgnoreCase("updateToContactDetails")) {
            if (productId == 2) {
                String signature = "cid:signature";
                regards = "Kind regards,<br/>"
                        + "<img src=\"" + signature + "\"><br/></span>";
            } else if (productId == 1) {
                regards = "Yours in health and wellness,<br/><br/>"
                        + "The Resolution Health Team.";
            }
        } else if (productId == 2) {
            String signature = "cid:signature";
            regards = "Kind regards,<br/>"
                    + "<img src=\"" + signature + "\"><br/>";
        } else if (productId == 1) {
            regards = "Yours in health,<br/><br/>"
                    + "The " + schemeName + " team";
        }

        if (regards != null && !regards.equals("")) {
            System.out.println("MAIL| Successfully returned kind regards for email...");
        } else {
            System.out.println("MAIL| Failed to returned kind regards for email...");
        }

        return regards;
    }

    /*
     public void sendConfirmNewUserRegistrationEmail(String emailAddress, String Username) {
     System.out.println("Sending mail...");
     Properties props = new Properties();
     props.setProperty("mail.transport.protocol", "smtp");
     props.setProperty("mail.host", SMTP_SERVER);
     //props.setProperty("mail.user", "caretech\\lesliej");
     //props.setProperty("mail.password", "qwerty");

     try {

     Authenticator auth = new PasswordAuthenication();
     //            Session mailSession = Session.getInstance(props, auth);
     Session mailSession = Session.getInstance(props);

     mailSession.setDebug(true);
     Transport transport = mailSession.getTransport();

     MimeMessage emailMsg = new MimeMessage(mailSession);
     emailMsg.setSubject("Confirm New User Registration");
     emailMsg.setFrom(new InternetAddress(ADMIN_EMAIL_USERNAME));

     //ArrayList<ClaimLineCom> tmpClaimLine = (ArrayList<ClaimLineCom>) claimCoverMail.getClaimCom().getClaimLinesCom();

     //ADD RECIPIENT TO MESSAGE
     emailMsg.addRecipient(Message.RecipientType.TO, new InternetAddress(emailAddress));

     //
     // This HTML mail have to 2 part, the BODY and the embedded images and CSS
     //
     MimeMultipart multipart = new MimeMultipart("related");

     // first part  (the html)
     BodyPart messageBodyPart = new MimeBodyPart();

     StringBuilder htmlText = new StringBuilder().append("<head>");

     htmlText.append("<style type=\"text/css\">");

     htmlText.append("table { border: 1px #0278c0 solid;} " + '\n');
     htmlText.append("td {border: 1px #4a6077 solid;} " + '\n');
     htmlText.append("tr {border: 1px #4a6077 solid;} " + '\n');
     htmlText.append("th { border: 1px #4a6077 solid;background-color: #005294;color: #ffffff;} " + '\n');
     htmlText.append(".mainFrame { width:800px; } " + '\n');
     htmlText.append("</style></head><div class=\"mainFrame\">");


     //htmlText.append("<img src=\"cid:header\"></br><img src=\"cid:subheader\">");

     //DATE
     //            htmlText.append("<p align=\"left\">" + getDate() + "</p>");

     htmlText.append("<br>");
     htmlText.append("<p align=\"left\">Dear " + Username + ",</p>");
     htmlText.append("<p align=\"left\">Your have been registered on the system. An activation confirmation email will be sent to you later with your password</p>");
     htmlText.append("<br>");

     htmlText.append("<img src=\"cid:footer\"></div>");

     //			System.out.println(htmlText.toString());

     messageBodyPart.setContent(htmlText.toString(), "text/html");

     // add it
     multipart.addBodyPart(messageBodyPart);

     // HEADER IMAGE
     messageBodyPart = new MimeBodyPart();
     messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\header.jpg")));
     messageBodyPart.setHeader("Content-ID", "<header>");
     // add it
     multipart.addBodyPart(messageBodyPart);

     // SUBHEADER IMAGE
     messageBodyPart = new MimeBodyPart();
     messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\subheader.jpg")));
     messageBodyPart.setHeader("Content-ID", "<subheader>");
     // add it
     multipart.addBodyPart(messageBodyPart);

     // FOOTER IMAGE
     messageBodyPart = new MimeBodyPart();
     messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\footer.jpg")));
     messageBodyPart.setHeader("Content-ID", "<footer>");
     // add it
     multipart.addBodyPart(messageBodyPart);

     // put everything together
     emailMsg.setContent(multipart);


     //            transport.connect(DOMAIN_USER, DOMAIN_PASSWORD);
     transport.connect();

     transport.sendMessage(emailMsg, emailMsg.getRecipients(Message.RecipientType.TO));

     transport.close();

     } catch (Exception e) {
     e.printStackTrace();

     }

     }

     public void sendConfirmNewUserActivationEmail(String emailAddress, String Username, String password) {
     System.out.println("Sending mail...");
     Properties props = new Properties();
     props.setProperty("mail.transport.protocol", "smtp");
     props.setProperty("mail.host", SMTP_SERVER);
     //props.setProperty("mail.user", "caretech\\lesliej");
     //props.setProperty("mail.password", "qwerty");

     try {

     Authenticator auth = new PasswordAuthenication();
     //            Session mailSession = Session.getInstance(props, auth);
     Session mailSession = Session.getInstance(props);

     mailSession.setDebug(true);
     Transport transport = mailSession.getTransport();

     MimeMessage emailMsg = new MimeMessage(mailSession);
     emailMsg.setSubject("Confirm New User Activation");
     emailMsg.setFrom(new InternetAddress(ADMIN_EMAIL_USERNAME));

     //ArrayList<ClaimLineCom> tmpClaimLine = (ArrayList<ClaimLineCom>) claimCoverMail.getClaimCom().getClaimLinesCom();

     //ADD RECIPIENT TO MESSAGE
     emailMsg.addRecipient(Message.RecipientType.TO, new InternetAddress(emailAddress));

     //
     // This HTML mail have to 2 part, the BODY and the embedded images and CSS
     //
     MimeMultipart multipart = new MimeMultipart("related");

     // first part  (the html)
     BodyPart messageBodyPart = new MimeBodyPart();

     StringBuilder htmlText = new StringBuilder().append("<head>");

     htmlText.append("<style type=\"text/css\">");

     htmlText.append("table { border: 1px #0278c0 solid;} " + '\n');
     htmlText.append("td {border: 1px #4a6077 solid;} " + '\n');
     htmlText.append("tr {border: 1px #4a6077 solid;} " + '\n');
     htmlText.append("th { border: 1px #4a6077 solid;background-color: #005294;color: #ffffff;} " + '\n');
     htmlText.append(".mainFrame { width:800px; } " + '\n');
     htmlText.append("</style></head><div class=\"mainFrame\">");


     //htmlText.append("<img src=\"cid:header\"></br><img src=\"cid:subheader\">");

     //DATE
     //            htmlText.append("<p align=\"left\">" + getDate() + "</p>");

     htmlText.append("<br>");
     htmlText.append("<p align=\"left\">Dear " + Username + ",</p>");
     htmlText.append("<p align=\"left\">Your have been activated in the system. Your password is: " + password + "</p>");
     htmlText.append("<br>");

     htmlText.append("<img src=\"cid:footer\"></div>");

     //			System.out.println(htmlText.toString());

     messageBodyPart.setContent(htmlText.toString(), "text/html");

     // add it
     multipart.addBodyPart(messageBodyPart);

     // HEADER IMAGE
     messageBodyPart = new MimeBodyPart();
     messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\header.jpg")));
     messageBodyPart.setHeader("Content-ID", "<header>");
     // add it
     multipart.addBodyPart(messageBodyPart);

     // SUBHEADER IMAGE
     //            messageBodyPart = new MimeBodyPart();
     //            messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\subheader.jpg")));
     //            messageBodyPart.setHeader("Content-ID", "<subheader>");
     // add it
     multipart.addBodyPart(messageBodyPart);

     // FOOTER IMAGE
     messageBodyPart = new MimeBodyPart();
     messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\footer.jpg")));
     messageBodyPart.setHeader("Content-ID", "<footer>");
     // add it
     multipart.addBodyPart(messageBodyPart);

     // put everything together
     emailMsg.setContent(multipart);


     //            transport.connect(DOMAIN_USER, DOMAIN_PASSWORD);
     transport.connect();

     transport.sendMessage(emailMsg, emailMsg.getRecipients(Message.RecipientType.TO));

     transport.close();

     } catch (Exception e) {
     e.printStackTrace();

     }

     }

     public void sendRejectUserEmail(String emailAddress, String Username) {

     System.out.println("Sending mail...");
     Properties props = new Properties();
     props.setProperty("mail.transport.protocol", "smtp");
     props.setProperty("mail.host", SMTP_SERVER);
     //props.setProperty("mail.user", "caretech\\lesliej");
     //props.setProperty("mail.password", "qwerty");

     try {

     Authenticator auth = new PasswordAuthenication();
     //            Session mailSession = Session.getInstance(props, auth);
     Session mailSession = Session.getInstance(props);

     mailSession.setDebug(true);
     Transport transport = mailSession.getTransport();

     MimeMessage emailMsg = new MimeMessage(mailSession);
     emailMsg.setSubject("User rejected");
     emailMsg.setFrom(new InternetAddress(ADMIN_EMAIL_USERNAME));

     //ArrayList<ClaimLineCom> tmpClaimLine = (ArrayList<ClaimLineCom>) claimCoverMail.getClaimCom().getClaimLinesCom();

     //ADD RECIPIENT TO MESSAGE
     emailMsg.addRecipient(Message.RecipientType.TO, new InternetAddress(emailAddress));

     //
     // This HTML mail have to 2 part, the BODY and the embedded images and CSS
     //
     MimeMultipart multipart = new MimeMultipart("related");

     // first part  (the html)
     BodyPart messageBodyPart = new MimeBodyPart();

     StringBuilder htmlText = new StringBuilder().append("<head>");

     htmlText.append("<style type=\"text/css\">");

     htmlText.append("table { border: 1px #0278c0 solid;} " + '\n');
     htmlText.append("td {border: 1px #4a6077 solid;} " + '\n');
     htmlText.append("tr {border: 1px #4a6077 solid;} " + '\n');
     htmlText.append("th { border: 1px #4a6077 solid;background-color: #005294;color: #ffffff;} " + '\n');
     htmlText.append(".mainFrame { width:800px; } " + '\n');
     htmlText.append("</style></head><div class=\"mainFrame\">");


     //htmlText.append("<img src=\"cid:header\"></br><img src=\"cid:subheader\">");

     //DATE
     htmlText.append("<p align=\"left\">" + getDate() + "</p>");

     htmlText.append("<br>");
     htmlText.append("<p align=\"left\">Dear user " + Username + ",</p>");
     htmlText.append("<p align=\"left\">Your request for access to the system have been rejected:</p>");
     htmlText.append("<br>");

     htmlText.append("<img src=\"cid:footer\"></div>");

     //			System.out.println(htmlText.toString());

     messageBodyPart.setContent(htmlText.toString(), "text/html");

     // add it
     multipart.addBodyPart(messageBodyPart);
 
     // HEADER IMAGE
     messageBodyPart = new MimeBodyPart();
     messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\header.jpg")));
     messageBodyPart.setHeader("Content-ID", "<header>");
     // add it
     multipart.addBodyPart(messageBodyPart);

     // SUBHEADER IMAGE
     //            messageBodyPart = new MimeBodyPart();
     //            messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\subheader.jpg")));
     //            messageBodyPart.setHeader("Content-ID", "<subheader>");
     // add it
     multipart.addBodyPart(messageBodyPart);

     // FOOTER IMAGE
     messageBodyPart = new MimeBodyPart();
     messageBodyPart.setDataHandler(new DataHandler(new FileDataSource("C:\\EmailImages\\footer.jpg")));
     messageBodyPart.setHeader("Content-ID", "<footer>");
     // add it
     multipart.addBodyPart(messageBodyPart);

     // put everything together
     emailMsg.setContent(multipart);


     //            transport.connect(DOMAIN_USER, DOMAIN_PASSWORD);
     transport.connect();

     transport.sendMessage(emailMsg, emailMsg.getRecipients(Message.RecipientType.TO));

     transport.close();

     } catch (Exception e) {
     e.printStackTrace();

     }

     }
     */
}

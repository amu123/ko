/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.security.dto;

import java.io.Serializable;

/**
 *
 * @author gerritj
 */
public class LookupValue implements Serializable {

    public static final long serialVersionUID = 1L;
    private String id;
    private String value;

    public LookupValue() {
        this.id = "";
        this.value = "";
    }

    public LookupValue(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LookupValue other = (LookupValue) obj;
        if (!id.equals(other.id)) {
            return false;
        }
        if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.security.dto;

/**
 *
 * @author whauger
 */
public class TCode {

    private String tcode;
    private String description;
    private int labCode;
    private String labCodeDescription;
    private int quantity;
    private double tariff;

    public TCode() {

    }

    public TCode(String tcode, String description, int labCode, String labCodeDescription, int quantity, double tariff) {
        this.tcode = tcode;
        this.description = description;
        this.labCode = labCode;
        this.labCodeDescription = labCodeDescription;
        this.quantity = quantity;
        this.tariff = tariff;
    }

    public String getTcode() {
        return tcode;
    }

    public void setTcode(String tcode) {
        this.tcode = tcode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLabCode() {
        return labCode;
    }

    public void setLabCode(int labCode) {
        this.labCode = labCode;
    }

    public String getLabCodeDescription() {
        return labCodeDescription;
    }

    public void setLabCodeDescription(String labCodeDescription) {
        this.labCodeDescription = labCodeDescription;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTariff() {
        return tariff;
    }

    public void setTariff(double tariff) {
        this.tariff = tariff;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.security.dto;

import java.util.Collection;

/**
 *
 * @author gerritj
 */
public class MenuItem {

    private int menuItemId;
    private String displayName;
    private String description;
    private MenuItemAttributes attributes;
    private int parentMenuItem;
    private String permission;

    public int getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(int menuItemId) {
        this.menuItemId = menuItemId;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }


    public int getParentMenuItem() {
        return parentMenuItem;
    }

    public void setParentMenuItem(int parentMenuItem) {
        this.parentMenuItem = parentMenuItem;
    }
    

    public MenuItemAttributes getAttributes() {
        return attributes;
    }

    public void setAttributes(MenuItemAttributes attributes) {
        this.attributes = attributes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    


}

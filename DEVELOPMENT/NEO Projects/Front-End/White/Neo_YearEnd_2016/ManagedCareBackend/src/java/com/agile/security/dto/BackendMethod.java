/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.security.dto;

/**
 *
 * @author gerritj
 */
public class BackendMethod {

    private int methodId;
    private String methodName;

    public int getMethodId() {
        return methodId;
    }

    public void setMethodId(int methodId) {
        this.methodId = methodId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    

}

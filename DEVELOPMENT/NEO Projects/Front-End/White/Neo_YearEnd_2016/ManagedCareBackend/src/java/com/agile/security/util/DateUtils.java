/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.security.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author gerritj
 */
public class DateUtils {


    public static String getToday(){
        String today = "";
        Date d = new Date();
        today = new SimpleDateFormat("yyyyMMdd").format(d);
        //System.out.println(today);
        return today;
    }

    public static String getTomorrow(){
        String tomorrow = "";
        Date d = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        cal.add(Calendar.DAY_OF_WEEK, 1);
        Date c = cal.getTime();
        tomorrow = new SimpleDateFormat("yyyyMMdd").format(c);
        //System.out.println(tomorrow);
        return tomorrow;
    }

    public static void main(String args[]){
        getToday();
        getTomorrow();
    }
}

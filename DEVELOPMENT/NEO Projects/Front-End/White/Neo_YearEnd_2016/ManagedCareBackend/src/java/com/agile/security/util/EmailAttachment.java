/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.security.util;

/**
 *
 * @author whauger
 */
public class EmailAttachment {

    private String contentType;
    private String name;

    public String getContentType() {
      return contentType;
    }

    public String getName() {
      return name;
    }
   
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package com.agile.security.dto;

import java.util.Collection;

public class SecurityResponsibility {

	private int responsibilityId;
	private String description;
	Collection<MenuItem> menuItems;
	private int requestGroupId;
    Collection<BackendMethod> backendMethods;

    public Collection<BackendMethod> getBackendMethods() {  
        return backendMethods;
    }

    public void setBackendMethods(Collection<BackendMethod> backendMethods) {
        this.backendMethods = backendMethods;
    }

    public Collection<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(Collection<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
	
	public SecurityResponsibility() {
		
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getResponsibilityId() {
		return responsibilityId;
	}

	public void setResponsibilityId(int responsibilityId) {
		this.responsibilityId = responsibilityId;
	}

	public int getRequestGroupId() {
		return requestGroupId;
	}

	public void setRequestGroupId(int requestGroupId) {
		this.requestGroupId = requestGroupId;
	}

	
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.security.dto;

/**
 *
 * @author whauger
 */
public class DentalTCode {

    private int dentalCode;
    private String tCode;

    public DentalTCode() {
    }

    public DentalTCode(int dentalCode, String tCode) {
        this.dentalCode = dentalCode;
        this.tCode = tCode;
    }

    public int getDentalCode() {
        return dentalCode;
    }

    public void setDentalCode(int dentalCode) {
        this.dentalCode = dentalCode;
    }

    public String getTCode() {
        return tCode;
    }

    public void setTCode(String tCode) {
        this.tCode = tCode;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.security.exception;

import java.io.Serializable;
import javax.xml.ws.WebFault;

/**
 *
 * @author gerritj
 */
@WebFault()
public class AgileException extends Exception implements Serializable {

    public static final long serialVersionUID = 1L;

    public AgileException() {
        super();
    }

    public AgileException(String message) {
        super(message);
    }

    public AgileException(String message, Throwable cause) {
        super(message, cause);
    }
}

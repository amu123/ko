package com.agile.security.dto;

import java.util.Collection;
import java.util.Date;

public class NeoUser {
	
	private int userId;
	private String name;
	private String password;
	private Date passwordExpiryDate;
	private int passAccessLeft;
	private int securityGroupId;
	private Collection<SecurityResponsibility> securityResponsibility;
	private String errorMsg = "";
    private String emailAddress;
    private String username;
    private String surname;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    
       

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPassAccessLeft() {
        return passAccessLeft;
    }

    public void setPassAccessLeft(int passAccessLeft) {
        this.passAccessLeft = passAccessLeft;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getPasswordExpiryDate() {
        return passwordExpiryDate;
    }

    public void setPasswordExpiryDate(Date passwordExpiryDate) {
        this.passwordExpiryDate = passwordExpiryDate;
    }

    public int getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(int securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    public Collection<SecurityResponsibility> getSecurityResponsibility() {
        return securityResponsibility;
    }

    public void setSecurityResponsibility(Collection<SecurityResponsibility> securityResponsibility) {
        this.securityResponsibility = securityResponsibility;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    
}
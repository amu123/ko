/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.security.ejb;

import com.agile.security.db.DatabaseQueries;
import com.agile.security.dto.AuthDetails;
import com.agile.security.dto.BackendMethod;
import com.agile.security.dto.DentalCode;
import com.agile.security.dto.DentalTCode;
import com.agile.security.dto.LookupType;
import com.agile.security.dto.LookupValue;
import com.agile.security.dto.MenuItem;
import com.agile.security.dto.MenuItemAttributes;
import com.agile.security.dto.NeoUser;
import com.agile.security.dto.OpticalCode;
import com.agile.security.dto.SecurityResponsibility;
import com.agile.security.dto.TCode;
import com.agile.security.dto.WorkItem;
import com.agile.security.dto.WorkItemAndComment;
import com.agile.security.dto.WorkList;
import com.agile.security.dto.eAuthSearchCriteria;
import com.agile.security.dto.eAuthSearchResult;
import com.agile.security.exception.AgileException;
import com.agile.security.util.DateUtils;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author gerritj
 */
@Stateless
public class AgileAccessBean implements AgileAccessLocal, AgileAccessRemote {

    @Resource(mappedName = "java:jdbc/NeoSecurity")
    DataSource dataSource;

    private void cleanup(Connection connection,
            PreparedStatement preparedStatement) {
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    public NeoUser authenticateUser(String userName, String password) throws Exception {
        NeoUser user = new NeoUser();
        user = getUser(userName, password);
        if (user == null) {
            System.out.println("Authentication failed for user " + userName);
            throw new Exception("Authentication Failure!");
        }
        user = getResponsibilities(user);
        return user;
    }

    public NeoUser getUser(String userName, String password) {
        Connection conn = null;
        PreparedStatement stat = null;
        NeoUser user = null;
        try {
            conn = dataSource.getConnection();
            stat = conn.prepareStatement(DatabaseQueries.FETCH_USER);
            stat.setString(1, userName);
            stat.setString(2, password);
            java.sql.Date dd = new java.sql.Date(new Date().getTime());
            stat.setDate(3, dd);
            stat.setDate(4, dd);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                user = new NeoUser();
                user.setUserId(rs.getInt(1));
                user.setUsername(rs.getString(2));
                user.setEmailAddress(rs.getString(5));
                user.setPassAccessLeft(rs.getInt(7));
                Date passwordDate = rs.getDate(8);
                Calendar cal = Calendar.getInstance();
                cal.setTime(passwordDate);
                cal.add(Calendar.HOUR, (rs.getInt(9) * 24));
                Date d = new Date(cal.getTimeInMillis());
                user.setPasswordExpiryDate(d);
                user.setName(rs.getString(12));
                user.setSurname(rs.getString(13));

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cleanup(conn, stat);
        }
        return user;
    }

    public NeoUser getUserByUsername(String userName) {
        Connection conn = null;
        PreparedStatement stat = null;
        NeoUser user = null;
        try {
            conn = dataSource.getConnection();
            stat = conn.prepareStatement(DatabaseQueries.FETCH_USER_BY_NAME);
            stat.setString(1, userName);
            java.sql.Date dd = new java.sql.Date(new Date().getTime());
            stat.setDate(2, dd);
            stat.setDate(3, dd);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                user = new NeoUser();
                user.setUserId(rs.getInt(1));
                user.setUsername(rs.getString(2));
                user.setEmailAddress(rs.getString(5));
                user.setPassAccessLeft(rs.getInt(7));
                Date passwordDate = rs.getDate(8);
                Calendar cal = Calendar.getInstance();
                cal.setTime(passwordDate);
                cal.add(Calendar.HOUR, (rs.getInt(9) * 24));
                Date d = new Date(cal.getTimeInMillis());
                user.setPasswordExpiryDate(d);
                user.setName(rs.getString(12));
                user.setSurname(rs.getString(13));

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cleanup(conn, stat);
        }
        return user;
    }

    public NeoUser getUserById(int id) {
        Connection conn = null;
        PreparedStatement stat = null;
        NeoUser user = null;
        try {
            conn = dataSource.getConnection();
            //USER_ID, USER_NAME, DESCRIPTION, PASSWORD, EMAIL_ADDRESS, DESCRIPTION, PASSWORD_ACCESS_LEFT, PASSWORD_DATE, PASSWORD_LIFESPAN_DAYS, USER_START_DATE, USER_END_DATE
            stat = conn.prepareStatement(DatabaseQueries.FETCH_USER_BY_ID);
            stat.setInt(1, id);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                user = new NeoUser();
                user.setUserId(rs.getInt(1));
                user.setUsername(rs.getString(2));
                user.setEmailAddress(rs.getString(5));
                user.setPassAccessLeft(rs.getInt(7));
                Date passwordDate = rs.getDate(8);
                Calendar cal = Calendar.getInstance();
                cal.setTime(passwordDate);
                cal.add(Calendar.HOUR, (rs.getInt(9) * 24));
                Date d = new Date(cal.getTimeInMillis());
                user.setPasswordExpiryDate(d);
                user.setName(rs.getString(12));
                user.setSurname(rs.getString(13));


            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cleanup(conn, stat);
        }
        return user;
    }

    public NeoUser getResponsibilities(NeoUser user) {
        Connection conn = null;
        PreparedStatement stat = null;
        try {
            conn = dataSource.getConnection();
            stat = conn.prepareStatement(DatabaseQueries.FETCH_RESP_FOR_USER);
            stat.setInt(1, user.getUserId());
            ResultSet rs = stat.executeQuery();
            ArrayList<SecurityResponsibility> sr = new ArrayList<SecurityResponsibility>();
            while (rs.next()) {
                SecurityResponsibility s = new SecurityResponsibility();
                s.setResponsibilityId(rs.getInt(3));
                s.setDescription(rs.getString(4));
                s.setMenuItems(getMenuItemsForResp(s.getResponsibilityId()));
                s.setBackendMethods(getBackendMethodsForResp(s.getResponsibilityId()));
                sr.add(s);

            }
            user.setSecurityResponsibility(sr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cleanup(conn, stat);
        }
        return user;
    }

    public Collection<MenuItem> getMenuItemsForResp(int respId) {
        Connection conn = null;
        PreparedStatement stat = null;
        ArrayList<MenuItem> mi = null;
        try {
            conn = dataSource.getConnection();
            stat = conn.prepareStatement(DatabaseQueries.FETCH_MENU_ITEMS_FOR_RESP);
            stat.setInt(1, respId);
            ResultSet rs = stat.executeQuery();
            mi = new ArrayList<MenuItem>();
            while (rs.next()) {
                MenuItem m = new MenuItem();
                m.setDisplayName(rs.getString(9));
                m.setDescription(rs.getString(10));
                m.setParentMenuItem(rs.getInt(11));
                m.setMenuItemId(rs.getInt(6));

                MenuItemAttributes at = new MenuItemAttributes();
                at.setAttributeId(rs.getInt(1));
                at.setAction(rs.getString(2));
                at.setBigImageName(rs.getString(4));
                at.setSmallImageName(rs.getString(5));

                m.setAttributes(at);
                mi.add(m);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cleanup(conn, stat);
        }
        return mi;
    }

    public Collection<BackendMethod> getBackendMethodsForResp(int respId) {
        Connection conn = null;
        PreparedStatement stat = null;
        ArrayList<BackendMethod> mi = null;
        try {
            conn = dataSource.getConnection();
            stat = conn.prepareStatement(DatabaseQueries.FETCH_METHODS_FOR_RESP);
            stat.setInt(1, respId);
            ResultSet rs = stat.executeQuery();
            mi = new ArrayList<BackendMethod>();
            while (rs.next()) {
                BackendMethod bm = new BackendMethod();
                bm.setMethodId(rs.getInt(2));
                bm.setMethodName(rs.getString(3));

                mi.add(bm);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cleanup(conn, stat);
        }
        return mi;
    }

    public List<LookupValue> getAllResponsibilities() {
        Connection conn = null;
        PreparedStatement stat = null;
        ArrayList<LookupValue> lv = null;
        try {
            conn = dataSource.getConnection();
            stat = conn.prepareStatement(DatabaseQueries.FETCH_ALL_RESPONSIBILITIES);
            ResultSet rs = stat.executeQuery();
            lv = new ArrayList<LookupValue>();
            while (rs.next()) {
                LookupValue l = new LookupValue();
                l.setId("" + rs.getInt(1));
                l.setValue(rs.getString(2));
                lv.add(l);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cleanup(conn, stat);
        }
        return lv;
    }

    /***********************WORKFLOWISH***********************************/
    public int insertWorkItem(WorkItem _workItem) {
        int workItemId = 0;

        if (_workItem == null) {
            return workItemId;
        }

        Connection conn = null;
        PreparedStatement stat = null;

        try {
            conn = dataSource.getConnection();

            workItemId = getNextSequenceValue("workitem_id_seq");
            stat = conn.prepareStatement(DatabaseQueries.INSERT_WORKITEM);
            stat.setInt(1, workItemId);
            stat.setInt(2, _workItem.getExternalReference());
            stat.setInt(3, _workItem.getSeverity());
            stat.setInt(4, _workItem.getWorkitemTypeId());

            stat.execute();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            cleanup(conn, stat);
        }

        return workItemId;
    }

    public int insertWorkList(WorkList _workList) {
        int workListId = 0;

        if (_workList == null) {
            return workListId;
        }
        //for oracle
        //workListId = getNextSequenceNumber(DatabaseQueries.WORKLIST_SEQUENCE);

        Connection conn = null;
        PreparedStatement stat = null;

        try {
            conn = dataSource.getConnection();

            workListId = getNextSequenceValue("worklist_id_seq");
            stat = conn.prepareStatement(DatabaseQueries.INSERT_WORKLIST);
            stat.setInt(1, workListId);
            stat.setInt(2, _workList.getWorkItem().getWorkitemId());
            stat.setInt(3, _workList.getReferedToUser());
            stat.setInt(4, _workList.getReferedToGroup());
            stat.setDate(5, new java.sql.Date(new Date().getTime()));
            stat.setDate(6, null);
            stat.setDate(7, null);
            stat.setString(8, _workList.getComments());
            stat.setInt(9, _workList.getReferedByUser());
            stat.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            cleanup(conn, stat);
        }

        return workListId;

    }

    public int createInactiveUser(NeoUser user, String comments) {
        System.out.println("IOn backend");
        int user_id = insertUser(user);

        WorkItem item = new WorkItem();
        item.setExternalReference(user_id);
        item.setSeverity(3);
        item.setWorkitemTypeId(1);
        int workitem_id = insertWorkItem(item);
        item.setWorkitemId(workitem_id);

        WorkList list = new WorkList();
        list.setComments(comments);
        list.setReferedDate(new Date());
        list.setReferedToGroup(1);
        list.setWorkItem(item);
        insertWorkList(list);

        return 0;
    }

    private int insertUser(NeoUser user) {
        Connection conn = null;
        PreparedStatement stat = null;
        try {
            conn = dataSource.getConnection();

            int user_id = getNextSequenceValue("USER_ID_SEQ");

            stat = conn.prepareStatement(DatabaseQueries.INSERT_USER);
            //USER_ID, USER_NAME ,DESCRIPTION ,EMAIL_ADDRESS ,pASSWORD ,PASSWORD_ACCESS_LEFT ,PASSWORD_DATE ,PASSWORD_LIFESPAN_DAYS ,USER_START_DATE ,USER_END_DATE
            stat.setInt(1, user_id);
            stat.setString(2, user.getUsername());
            stat.setString(3, "");
            stat.setString(4, user.getEmailAddress());
            stat.setString(5, user.getPassword());
            stat.setInt(6, 0); // not active yet
            stat.setDate(7, new java.sql.Date(new Date().getTime()));
            stat.setInt(8, 30);
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.HOUR, -24);
            Date yesterday = new Date(cal.getTimeInMillis());
            stat.setDate(9, new java.sql.Date(new Date().getTime()));
            stat.setDate(10, new java.sql.Date(yesterday.getTime()));
            stat.setString(11, user.getName());
            stat.setString(12, user.getSurname());
            int res = stat.executeUpdate();

            return user_id;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cleanup(conn, stat);
        }
        return 0;
    }

    public List<WorkItemAndComment> getAllWorklistItemsForType(int type) {
        Connection conn = null;
        PreparedStatement stat = null;
        ArrayList<WorkItemAndComment> wic = null;
        try {
            conn = dataSource.getConnection();
            stat = conn.prepareStatement(DatabaseQueries.FETCH_OPEN_WORKITEMS_FOR_TYPE);
            stat.setInt(1, type);
            ResultSet rs = stat.executeQuery();
            wic = new ArrayList<WorkItemAndComment>();
            while (rs.next()) {
                WorkItemAndComment wItemComment = new WorkItemAndComment();
                wItemComment.setComment(rs.getString(1));
                WorkItem wi = new WorkItem();
                wi.setWorkitemId(rs.getInt(2));
                wi.setExternalReference(rs.getInt(3));
                wi.setSeverity(rs.getInt(4));
                wi.setWorkitemTypeId(rs.getInt(5));

                wItemComment.setWorkitem(wi);
                wic.add(wItemComment);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cleanup(conn, stat);
        }
        return wic;
    }

    public void rejectUser(int userId, int workItemId) {
        closeWorkItem(workItemId);
        deleteUser(userId);
    }

    //TODO change to have 1 connection and parse to private method
    public void activateuserAndAssignResponsibilities(int userId, int workItemId, Collection<Integer> responsibilities) {
        activateUser(userId);
        assignResponsibilitiesToUser(userId, responsibilities);
        closeWorkItem(workItemId);
    }

    private void assignResponsibilitiesToUser(int userId, Collection<Integer> responsibilities) {
        Connection conn = null;
        PreparedStatement stat = null;
        try {

            int seq = getNextSequenceValue("USR_RESPOSIBILITY_ID_seq");
            conn = dataSource.getConnection();
            stat = conn.prepareStatement(DatabaseQueries.INSERT_RESPONSIBILITY);
            //USR_RESPOSIBILITY_ID ,USER_ID ,RESPONSIBILITY_ID ,RESPONSIBILITY_START_DATE ,RESPONSIBILITY_END_DATE
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.YEAR, 5);
            Date fiveYearsFromNow = new Date(cal.getTimeInMillis());
            stat.setInt(1, seq);
            stat.setInt(2, userId);
            stat.setDate(4, new java.sql.Date(new Date().getTime()));
            stat.setDate(5, new java.sql.Date(fiveYearsFromNow.getTime()));
            ArrayList<Integer> resps = (ArrayList) responsibilities;
            for (int i = 0; i < resps.size(); i++) {
                stat.setInt(3, resps.get(i));
                stat.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cleanup(conn, stat);
        }
    }

    private void activateUser(int userId) {
        Connection conn = null;
        PreparedStatement stat = null;
        try {
            conn = dataSource.getConnection();
            stat = conn.prepareStatement(DatabaseQueries.UPDATE_USER_ACTIVE);
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.YEAR, 5);
            Date fiveYearsFromNow = new Date(cal.getTimeInMillis());
            stat.setDate(1, new java.sql.Date(fiveYearsFromNow.getTime()));
            stat.setInt(2, userId);
            stat.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cleanup(conn, stat);
        }
    }

    private void deleteUser(int userId) {
        Connection conn = null;
        PreparedStatement stat = null;
        try {
            conn = dataSource.getConnection();
            stat = conn.prepareStatement(DatabaseQueries.DELETE_USER);
            stat.setInt(1, userId);
            stat.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cleanup(conn, stat);
        }
    }

    private void closeWorkItem(int workitemId) {
        Connection conn = null;
        PreparedStatement stat = null;
        try {
            conn = dataSource.getConnection();
            stat = conn.prepareStatement(DatabaseQueries.CLOSE_WORKITEM);
            //start
            stat.setDate(1, new java.sql.Date(new Date().getTime()));
            //end
            stat.setDate(2, new java.sql.Date(new Date().getTime()));
            //workitem
            stat.setInt(3, workitemId);
            stat.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cleanup(conn, stat);
        }
    }

    private int getNextSequenceValue(String seqName) {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            CallableStatement cs = conn.prepareCall("{call getNextSequenceValue(?,?) }");

            cs.setString(1, seqName);
            cs.registerOutParameter(2, Types.INTEGER);


            cs.executeUpdate();
            int value = cs.getInt(2);
            cs.close();
            return value;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return 0;
    }

    public Collection<LookupType> fetchSimpleLookupTypes() {
        Connection connection = null;
        List<LookupType> simpleLookupTypes = new ArrayList<LookupType>();
        PreparedStatement neoLookupTypeStmt = null;
        ResultSet rs = null;

        try {
            connection = dataSource.getConnection();
            neoLookupTypeStmt = connection.prepareStatement(DatabaseQueries.LOOKUP_TYPE_TABLE_IND);
            neoLookupTypeStmt.setString(1, "N");
            rs = neoLookupTypeStmt.executeQuery();
            while (rs.next()) {
                simpleLookupTypes.add(new LookupType(rs.getInt(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        //throw new NeoRuntimeException("SQL Statement Failed while fetching system types", e);
        } finally {
            cleanup(connection, neoLookupTypeStmt);
        }

        return simpleLookupTypes;
    }

    public Collection<LookupValue> findCodeTableForLookupType(int typeId) {
        Connection connection = null;
        List<LookupValue> selectedLookupType = new ArrayList<LookupValue>();
        PreparedStatement neoLookupTypeStmt = null;
        ResultSet rs = null;

        try {
            connection = dataSource.getConnection();
            neoLookupTypeStmt = connection.prepareStatement(DatabaseQueries.LOOKUP_VALUE_TYPE_ID);
            neoLookupTypeStmt.setInt(1, typeId);
            rs = neoLookupTypeStmt.executeQuery();
            while (rs.next()) {
                selectedLookupType.add(new LookupValue(rs.getString(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        //throw new NeoRuntimeException("SQL Statement Failed while fetching selected system type", e);
        } finally {
            cleanup(connection, neoLookupTypeStmt);
        }

        return selectedLookupType;
    }

    public Collection<LookupType> fetchAllLookupTypes() {
        Connection connection = null;
        List<LookupType> lookupTypes = new ArrayList<LookupType>();
        PreparedStatement neoLookupTypeStmt = null;
        ResultSet rs = null;

        //if (typeSearchCriteria.getType() != 0) {
        //	neoMaintenanceLogger.info("Contract Type == null finding companies per contract");
        //	neoSystemTypeStmt = DatabaseQueries.SYSTEM_TYPE;
        //}

        try {
            connection = dataSource.getConnection();
            String query = DatabaseQueries.LOOKUP_TYPE;
            neoLookupTypeStmt = connection.prepareStatement(query);
            rs = neoLookupTypeStmt.executeQuery();
            while (rs.next()) {
                lookupTypes.add(new LookupType(rs.getInt(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        //throw new NeoRuntimeException("SQL Statement Failed while fetching system types", e);
        } finally {
            cleanup(connection, neoLookupTypeStmt);
        }
        return lookupTypes;
    }

    public Collection<LookupValue> fetchLookupValueByName(String name, CodeTables table) {
        return table.getCodeTable(name);
    }

    public Collection<TCode> findTCodeTable(String tCode) {
        Connection connection = null;
        List<TCode> tcodeTables = new ArrayList<TCode>();
        PreparedStatement neoLookupTypeStmt = null;
        ResultSet rs = null;

        try {
            connection = dataSource.getConnection();
            String query = DatabaseQueries.FIND_TCODE_GRID_TCODE;
            neoLookupTypeStmt = connection.prepareStatement(query);
            neoLookupTypeStmt.setString(1, tCode);
            rs = neoLookupTypeStmt.executeQuery();
            while (rs.next()) {
                tcodeTables.add(new TCode(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getDouble(6)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        //throw new NeoRuntimeException("SQL Statement Failed while fetching system types", e);
        } finally {
            cleanup(connection, neoLookupTypeStmt);
        }

        return tcodeTables;
    }

    public Collection<DentalTCode> findTCodeTablesForDentalCode(int dentalCode) {
        Connection connection = null;
        List<DentalTCode> tcodeList = new ArrayList<DentalTCode>();
        PreparedStatement neoLookupTypeStmt = null;
        ResultSet rs = null;

        try {
            connection = dataSource.getConnection();
            String query = DatabaseQueries.FIND_TCODE_GRID_DENTAL_CODE;
            neoLookupTypeStmt = connection.prepareStatement(query);
            neoLookupTypeStmt.setInt(1, dentalCode);
            rs = neoLookupTypeStmt.executeQuery();
            while (rs.next()) {
                tcodeList.add(new DentalTCode(rs.getInt(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        //throw new NeoRuntimeException("SQL Statement Failed while fetching system types", e);
        } finally {
            cleanup(connection, neoLookupTypeStmt);
        }

        return tcodeList;
    }

    public Collection<OpticalCode> findOpticalProtocolsByCode(int code) {
        Connection connection = null;
        List<OpticalCode> opticalProtocol = new ArrayList<OpticalCode>();
        PreparedStatement neoLookupTypeStmt = null;
        ResultSet rs = null;

        try {
            connection = dataSource.getConnection();
            String query = DatabaseQueries.FIND_OPTICAL_PROTOCOL_CODE;
            neoLookupTypeStmt = connection.prepareStatement(query);
            neoLookupTypeStmt.setInt(1, code);
            rs = neoLookupTypeStmt.executeQuery();
            while (rs.next()) {
                opticalProtocol.add(new OpticalCode(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getDouble(4), rs.getDouble(5), rs.getString(6)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        //throw new NeoRuntimeException("SQL Statement Failed while fetching system types", e);
        } finally {
            cleanup(connection, neoLookupTypeStmt);
        }

        return opticalProtocol;
    }

    public Collection<OpticalCode> findOpticalProtocolsByDiscipline(String discipline) {
        Connection connection = null;
        List<OpticalCode> opticalProtocol = new ArrayList<OpticalCode>();
        PreparedStatement neoLookupTypeStmt = null;
        ResultSet rs = null;

        try {
            connection = dataSource.getConnection();
            String query = DatabaseQueries.FIND_OPTICAL_PROTOCOL_DISCIPLINE;
            neoLookupTypeStmt = connection.prepareStatement(query);
            neoLookupTypeStmt.setString(1, discipline);
            rs = neoLookupTypeStmt.executeQuery();
            while (rs.next()) {
                opticalProtocol.add(new OpticalCode(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getDouble(4), rs.getDouble(5), rs.getString(6)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        //throw new NeoRuntimeException("SQL Statement Failed while fetching system types", e);
        } finally {
            cleanup(connection, neoLookupTypeStmt);
        }

        return opticalProtocol;
    }

    public Collection<DentalCode> findDentalProtocolsByCode(int code) {
        Connection connection = null;
        List<DentalCode> dentalProtocol = new ArrayList<DentalCode>();
        PreparedStatement neoLookupTypeStmt = null;
        ResultSet rs = null;

        try {
            connection = dataSource.getConnection();
            String query = DatabaseQueries.FIND_DENTAL_PROTOCOL_CODE;
            neoLookupTypeStmt = connection.prepareStatement(query);
            neoLookupTypeStmt.setInt(1, code);
            rs = neoLookupTypeStmt.executeQuery();
            while (rs.next()) {
                dentalProtocol.add(new DentalCode(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getDouble(4), rs.getString(5)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        //throw new NeoRuntimeException("SQL Statement Failed while fetching system types", e);
        } finally {
            cleanup(connection, neoLookupTypeStmt);
        }

        return dentalProtocol;
    }

    public Collection<DentalCode> findDentalProtocolsByDiscipline(String discipline) {
        Connection connection = null;
        List<DentalCode> dentalProtocol = new ArrayList<DentalCode>();
        PreparedStatement neoLookupTypeStmt = null;
        ResultSet rs = null;

        try {
            connection = dataSource.getConnection();
            String query = DatabaseQueries.FIND_DENTAL_PROTOCOL_DISCIPLINE;
            neoLookupTypeStmt = connection.prepareStatement(query);
            neoLookupTypeStmt.setString(1, discipline);
            rs = neoLookupTypeStmt.executeQuery();
            while (rs.next()) {
                dentalProtocol.add(new DentalCode(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getDouble(4), rs.getString(5)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        //throw new NeoRuntimeException("SQL Statement Failed while fetching system types", e);
        } finally {
            cleanup(connection, neoLookupTypeStmt);
        }

        return dentalProtocol;
    }

    public Collection<eAuthSearchResult> searchAuthDetails(eAuthSearchCriteria search) {
        Collection<eAuthSearchResult> result = new ArrayList<eAuthSearchResult>();
        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;

        //GENERATE DYNAMIC QUERY
        String searchQ = DatabaseQueries.SEARCH_AUTH;

        // SEARCH BY AUTH NUMBER
        if ((search.getAuthNumber() != null) && (!search.getAuthNumber().trim().equalsIgnoreCase(""))) {
            searchQ = searchQ + " AND LOWER(ad.auth_code) = '" + search.getAuthNumber() + "'";
        }

        //SEARCH BY MEMBER NUMBER
        if ((search.getMemberNumber() != null) && (!search.getMemberNumber().trim().equalsIgnoreCase(""))) {
            searchQ = searchQ + " AND LOWER(ad.member_no) = '" + search.getMemberNumber() + "'";
        }

        //SEARCH BY DEPENDANT CODE
        if ((search.getDependantNumber() != null) && (!search.getDependantNumber().trim().equalsIgnoreCase(""))) {
            searchQ = searchQ + " AND ad.dependant_code = '" + search.getDependantNumber() + "'";
        }

        //SEARCH BY AUTH DATE
        if (search.getAuthDate() != null) {
            searchQ = searchQ + " AND ad.authorisation_date = '" + search.getAuthDate() + "'";
        }

        //SEARCH BY AUTH VALID FROM
        if (search.getValidFrom() != null) {
            searchQ = searchQ + " AND ad.date_valid_from = '" + search.getValidFrom() + "'";
        }

        //SEARCH BY AUTH VALID TO
        if (search.getValidTo() != null) {
            searchQ = searchQ + " AND ad.date_valid_to = '" + search.getValidTo() + "'";
        }

        searchQ = searchQ + " ORDER BY ad.member_no, ad.dependant_code, ad.authorisation_date ASC";

        //Check if where clause is present
        if(searchQ.indexOf("where") == -1 && searchQ.indexOf("WHERE") == -1)
        {
            searchQ = searchQ.replaceFirst("[aA][nN][dD]", "WHERE");
        }

        System.out.println("eAuth search Q = " + searchQ);

        try {
            c = dataSource.getConnection();
            ps = c.prepareStatement(searchQ);
            resultSet = ps.executeQuery();

            while (resultSet.next()) {
                eAuthSearchResult ea = new eAuthSearchResult();
                int authId = resultSet.getInt("auth_id");
                String name = resultSet.getString("fullnames");
                String surname = resultSet.getString("surname");
                String dependentCode = resultSet.getString("dependant_code");
                String memeberName = name + " " + surname + " " + dependentCode;
                ea.setMemberName(memeberName);
                ea.setAuth_id(authId);
                ea.setAuthNo(resultSet.getString("auth_code"));
                ea.setMemberNo(resultSet.getString("member_no"));
                ea.setDepNo(resultSet.getString("dependant_code"));
                ea.setAuthDate(resultSet.getString("authorisation_date"));
                ea.setValidFrom(resultSet.getString("date_valid_from"));
                ea.setValidTo(resultSet.getString("date_valid_to"));
                ea.setAuthStatus(resultSet.getString("auth_status"));

                result.add(ea);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;

        } finally {
            cleanup(c, ps);
        }
        return result;
    }
    public String insertAuthorisation(AuthDetails authDetails) {
        Connection connection = null;
        PreparedStatement stmt = null;
        System.out.println("In insert auth details");
        try {
            int seq = getNextSequenceValue("Auth_Details_seq");
            connection = dataSource.getConnection();
            String query = DatabaseQueries.INSERT_AUTH;
            stmt = connection.prepareStatement(query);
            stmt.setInt(1, seq);
            stmt.setString(2, authDetails.getMember_no());
            stmt.setString(3, authDetails.getDependant_code());
            stmt.setString(4, authDetails.getSurname());
            stmt.setString(5, authDetails.getFullnames());
            int authCount = 500 + getAuthCountForToday(connection);
            String authCode = "BBR" + DateUtils.getToday() + authCount + "H";
            stmt.setString(6, authCode);
            stmt.setString(7, authDetails.getAuth_type());
            stmt.setString(8, authDetails.getAdmission_date());
            stmt.setString(9, authDetails.getAdmission_time());
            stmt.setString(10, authDetails.getDischarge_date());
            stmt.setString(11, authDetails.getDischarge_time());
            stmt.setString(12, authDetails.getLength_of_stay());
            if (authDetails.getModified_date() != null) {
                stmt.setTimestamp(13, new java.sql.Timestamp(authDetails.getModified_date().getTime()));
            } else {
                stmt.setTimestamp(13, new java.sql.Timestamp(new Date().getTime()));
            }
            stmt.setString(14, authDetails.getHospital_provider_no());
            stmt.setString(15, authDetails.getHospital_provider());
            stmt.setString(16, authDetails.getReferring_provider_no());
            stmt.setString(17, authDetails.getReferring_provider());
            stmt.setString(18, authDetails.getTreating_provider_no());
            stmt.setString(19, authDetails.getTreating_provider());
            stmt.setString(20, authDetails.getPrimary_icd());
            stmt.setString(21, authDetails.getSecondary_icd());
            stmt.setString(22, authDetails.getTertiary_icd());
            stmt.setString(23, authDetails.getModified_by());
            stmt.setString(24, authDetails.getAuth_status());
            stmt.setString(25, authDetails.getPMB());
            stmt.setString(26, authDetails.getCo_payment());
            stmt.setString(27, authDetails.getEstimated_cost());
            stmt.setString(28, authDetails.getRelease_payment_indicator());
            stmt.setString(29, authDetails.getMaternity_indicator());
            stmt.setString(30, authDetails.getFunder());
            stmt.setString(31, authDetails.getLOC());
            stmt.setString(32, authDetails.getAnaesthetist_code());
            stmt.setString(33, authDetails.getAnaesthetist_name());
            stmt.setString(34, authDetails.getTheatre_time());
            stmt.setString(35, authDetails.getHospital_indicator());
            stmt.setString(36, authDetails.getScheme());
            stmt.setString(37, authDetails.getScheme_option());
            stmt.setString(38, authDetails.getAuthorisation_date());
            stmt.setString(39, authDetails.getCaller_name());
            stmt.setString(40, authDetails.getCaller_relationship());
            stmt.setString(41, authDetails.getCaller_reason());
            stmt.setString(42, authDetails.getCaller_contact());
            stmt.setString(43, authDetails.getLab_provider_no());
            stmt.setString(44, authDetails.getLab_provider());
            stmt.setString(45, authDetails.getTarrif_list());
            stmt.setString(46, authDetails.getLab_code_list());
            stmt.setString(47, authDetails.getQuantity());
            stmt.setString(48, authDetails.getTooth_number());
            stmt.setString(49, authDetails.getDate_valid_from());
            stmt.setString(50, authDetails.getDate_valid_to());
            stmt.setString(51, authDetails.getNotes());
            stmt.setString(52, authDetails.getProcedure_list());
            stmt.setString(53, authDetails.getNumber_of_days());
            stmt.setString(54, authDetails.getPrevious_lens_rx());
            stmt.setString(55, authDetails.getCurrent_lens_rx());
            stmt.setString(56, authDetails.getPlanned_duration());
            stmt.setString(57, authDetails.getDeposit_amount());
            stmt.setString(58, authDetails.getFirst_installment_amount());
            stmt.setString(59, authDetails.getRemaining_installment_amount());
            stmt.setTimestamp(60, new java.sql.Timestamp(new Date().getTime()));
            stmt.setNull(61, Types.DATE);
            stmt.executeUpdate();
            return authCode;
        } catch (SQLException e) {
            e.printStackTrace();
        //throw new NeoRuntimeException("SQL Statement Failed while fetching system types", e);
        } finally {
            cleanup(connection, stmt);
        }
        return null;
    }

    private int getAuthCountForToday(Connection con) {
        int count = 0;
        try {
            PreparedStatement stmt = con.prepareStatement("select count(*) from auth_details where creation_date >= ? and creation_date < ?");
            stmt.setString(1, DateUtils.getToday());
            stmt.setString(2, DateUtils.getTomorrow());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            stmt.close();
            rs.close();
            return count;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    public OpticalCode findOpticalTreatmentForCode(String discipline, int code) {
        Connection connection = null;
        OpticalCode opticalCode = null;
        PreparedStatement neoLookupTypeStmt = null;
        ResultSet rs = null;

        try {
            connection = dataSource.getConnection();
            String query = DatabaseQueries.FIND_OPTICAL_PROTOCOL_DISCIPLINE_CODE;
            neoLookupTypeStmt = connection.prepareStatement(query);
            neoLookupTypeStmt.setString(1, discipline);
            neoLookupTypeStmt.setInt(2, code);
            rs = neoLookupTypeStmt.executeQuery();
            while (rs.next()) {
                opticalCode = new OpticalCode(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getDouble(4), rs.getDouble(5), rs.getString(6));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        //throw new NeoRuntimeException("SQL Statement Failed while fetching system types", e);
        } finally {
            cleanup(connection, neoLookupTypeStmt);
        }

        return opticalCode;
    }

    public DentalCode findDentalTreatmentForCode(String discipline, int code) {
        Connection connection = null;
        DentalCode dentalCode = null;
        PreparedStatement neoLookupTypeStmt = null;
        ResultSet rs = null;

        try {
            connection = dataSource.getConnection();
            String query = DatabaseQueries.FIND_DENTAL_PROTOCOL_DISCIPLINE_CODE;
            neoLookupTypeStmt = connection.prepareStatement(query);
            neoLookupTypeStmt.setString(1, discipline);
            neoLookupTypeStmt.setInt(2, code);
            rs = neoLookupTypeStmt.executeQuery();
            while (rs.next()) {
                dentalCode = new DentalCode(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getDouble(4), rs.getString(5));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        //throw new NeoRuntimeException("SQL Statement Failed while fetching system types", e);
        } finally {
            cleanup(connection, neoLookupTypeStmt);
        }

        return dentalCode;
    }

    public Collection<String> getNeoResoUserList(){
        Collection<String> resoUserList = new ArrayList<String>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
        String userQ = DatabaseQueries.GET_RESO_USERS;
        try{
            conn = dataSource.getConnection();
            ps = conn.prepareStatement(userQ);
            ps.setDate(1, today);
            ps.setDate(2, today);

            rs = ps.executeQuery();

            while(rs.next()){
                int userId = 0;
                String userName = "";
                String idName = "";
                //get values
                userId = rs.getInt("USER_ID");
                userName = rs.getString("USER_NAME");
                //add values to map
                idName = userId + "|" + userName;
                resoUserList.add(idName);
            }

        }catch(SQLException se){
            System.out.println("SQLException in getNeoResoUserList "+se.getMessage());
            se.printStackTrace();
            return null;
        } finally {
            cleanup(conn, ps);
        }
        return resoUserList;
    }

    public Collection<String> getNeoUserListByRespId(int respId){
        Collection<String> resoUserList = new ArrayList<String>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
        String userQ = DatabaseQueries.GET_USERS_RESPOSIBILITY_ID;
        try{
            conn = dataSource.getConnection();
            ps = conn.prepareStatement(userQ);
            ps.setDate(1, today);
            ps.setDate(2, today);
            ps.setInt(3, respId);

            rs = ps.executeQuery();

            while(rs.next()){
                int userId = 0;
                String userName = "";
                String idName = "";
                //get values
                userId = rs.getInt("USER_ID");
                userName = rs.getString("USER_NAME");
                //add values to map
                idName = userId + "|" + userName;
                resoUserList.add(idName);
            }

        }catch(SQLException se){
            System.out.println("SQLException in getNeoUserListByRespId "+se.getMessage());
            se.printStackTrace();
            return null;
        } finally {
            cleanup(conn, ps);
        }
        return resoUserList;
    }
}

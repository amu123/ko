/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.security.dto;

/**
 *
 * @author gerritj
 */
public class MenuItemAttributes {

    private int attributeId;
    private String action;
    private String bigImageName;
    private String smallImageName;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(int attributeId) {
        this.attributeId = attributeId;
    }

    public String getBigImageName() {
        return bigImageName;
    }

    public void setBigImageName(String bigImageName) {
        this.bigImageName = bigImageName;
    }

    public String getSmallImageName() {
        return smallImageName;
    }

    public void setSmallImageName(String smallImageName) {
        this.smallImageName = smallImageName;
    }

    

}

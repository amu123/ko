/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.security.ejb;

import com.agile.security.dto.AuthDetails;
import com.agile.security.dto.BackendMethod;
import com.agile.security.dto.DentalCode;
import com.agile.security.dto.DentalTCode;
import com.agile.security.dto.LookupType;
import com.agile.security.dto.LookupValue;
import com.agile.security.dto.MenuItem;
import com.agile.security.dto.NeoUser;
import com.agile.security.dto.OpticalCode;
import com.agile.security.dto.TCode;
import com.agile.security.dto.WorkItem;
import com.agile.security.dto.WorkItemAndComment;
import com.agile.security.dto.WorkList;
import com.agile.security.exception.AgileException;
import java.util.Collection;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author gerritj
 */
@Remote
public interface AgileAccessRemote {

    /*
    NeoUser authenticateUser(String userName, String password) throws AgileException;

    public Collection<BackendMethod> getBackendMethodsForResp(int respId);

    public Collection<MenuItem> getMenuItemsForResp(int respId);

    public NeoUser getResponsibilities(NeoUser user);

    public NeoUser getUser(String userName, String password);

    public NeoUser getUserById(int id);

    public List<LookupValue> getAllResponsibilities();
    */
    /*****************worklist***********/
    /*
    public int insertWorkItem(WorkItem _workItem);

    public int insertWorkList(WorkList _workList);

    public List<WorkItemAndComment> getAllWorklistItemsForType(int type);
    */
    /************************************/
    /*
    public int createInactiveUser(NeoUser user, String comments);

    public void activateuserAndAssignResponsibilities(int userId, int workItemId, Collection<Integer> responsibilities);

    public void rejectUser(int userId, int workItemId);
    */

    /*********************LOOKUPVALUE*********************/
    /*
    public Collection<LookupValue> findCodeTableForLookupType(int typeId);

    public Collection<LookupType> fetchSimpleLookupTypes();

    public Collection<LookupValue> fetchLookupValueByName(String name, CodeTables table);
    */
    /*********************GRIDS***************************/
    /*
    public Collection<TCode> findTCodeTable(String tCode);
    public Collection<DentalTCode> findTCodeTablesForDentalCode(int dentalCode);
    public Collection<OpticalCode> findOpticalProtocolsByCode(int code);
    public Collection<OpticalCode> findOpticalProtocolsByDiscipline(String discipline);
    public Collection<DentalCode> findDentalProtocolsByCode(int code);
    public Collection<DentalCode> findDentalProtocolsByDiscipline(String discipline);
    public OpticalCode findOpticalTreatmentForCode(String discipline, int code);
    public DentalCode findDentalTreatmentForCode(String discipline, int code);
    */

    /**********************AUTH***************************/
    //public String insertAuthorisation(AuthDetails authDetails);
}

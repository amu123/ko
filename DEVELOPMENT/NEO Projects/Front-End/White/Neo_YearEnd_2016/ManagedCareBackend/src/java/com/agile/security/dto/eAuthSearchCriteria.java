/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.security.dto;

import java.util.Date;

/**
 *
 * @author johanl
 */
public class eAuthSearchCriteria {

    private String authNumber;
    private String memberNumber;
    private String dependantNumber;
    private String authDate;
    private String validFrom;
    private String validTo;
    
    public eAuthSearchCriteria(){}

     public String getAuthDate() {
        return authDate;
    }

    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    public String getAuthNumber() {
        return authNumber;
    }

    public void setAuthNumber(String authNumber) {
        this.authNumber = authNumber;
    }

    public String getDependantNumber() {
        return dependantNumber;
    }

    public void setDependantNumber(String dependantNumber) {
        this.dependantNumber = dependantNumber;
    }

    public String getMemberNumber() {
        return memberNumber;
    }

    public void setMemberNumber(String memberNumber) {
        this.memberNumber = memberNumber;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

}

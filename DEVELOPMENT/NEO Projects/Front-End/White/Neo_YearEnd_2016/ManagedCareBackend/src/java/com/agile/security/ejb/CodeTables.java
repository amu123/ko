/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.security.ejb;

import com.agile.security.dto.LookupType;
import com.agile.security.dto.LookupValue;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.naming.InitialContext;

/**
 *
 * @author gerritj
 */
public class CodeTables {

	private static CodeTables table = null;

	private HashMap<Integer, Collection<LookupValue>> values = null;

	private Collection<LookupType> lookupTypes = null;

	protected CodeTables() {

	}

	public static CodeTables getInstance() {
		if (table == null) {
			table = new CodeTables();
			table.loadData();
		}
		return table;
	}

	private void loadData() {
		values = new HashMap<Integer, Collection<LookupValue>>();

        try {
            String localBean = "/AgileAccessBean/local";
            InitialContext iContext = new InitialContext();
            AgileAccessLocal accessBean = (AgileAccessLocal) iContext.lookup(localBean);

            //Get all lookup types
            lookupTypes = accessBean.fetchAllLookupTypes();

            //Load standard code tables (enums)
            Collection<LookupType> simplelookupTypes = accessBean.fetchSimpleLookupTypes();
            Iterator<LookupType> simple = simplelookupTypes.iterator();
            while (simple.hasNext())
            {
                LookupType current = (LookupType) simple.next();
                int type = current.getId();
                //int type = this.getLookupTypeId("Frequency");
                Collection<LookupValue> table = accessBean.findCodeTableForLookupType(type);
                if (table != null && table.size() > 0) {
                    System.out.println("Found code table " + current.getValue());
                    values.put(new Integer(type), table);
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
		
	}

	//Normal code tables
	public Collection<LookupValue> getCodeTable(int tableId)
	{
		return values.get(new Integer(tableId));
	}

	public Collection<LookupValue> getCodeTable(String tableName)
	{
		int typeId = this.getLookupTypeId(tableName);
		return values.get(new Integer(typeId));
	}

	public String getValueFromCodeTableForId(int tableId, String id)
	{
		Collection<LookupValue> table = values.get(new Integer(tableId));
		return this.getLookupValueName(table, id);
	}

	public String getValueFromCodeTableForId(String tableName, String id)
	{
		int typeId = this.getLookupTypeId(tableName);
		Collection<LookupValue> table = values.get(new Integer(typeId));
		return this.getLookupValueName(table, id);
	}

	public String getIdFromCodeTableForValue(int tableId, String value)
	{
		Collection<LookupValue> table = values.get(new Integer(tableId));
		return this.getLookupValueId(table, value);
	}

	public String getIdFromCodeTableForValue(String tableName, String value)
	{
		int typeId = this.getLookupTypeId(tableName);
		Collection<LookupValue> table = values.get(new Integer(typeId));
		return this.getLookupValueId(table, value);
	}

	

	//Lookup groups
//	public Collection<LookupGroup> getLookupGroups()
//	{
//		return lookupGroups;
//	}
//
//	public String getLookupGroupName(int groupId)
//	{
//		LookupGroup group = null;
//		Iterator<LookupGroup> iter = lookupGroups.iterator();
//		while (iter.hasNext())
//		{
//			group = iter.next();
//			if (group.getId() == groupId)
//			{
//				return group.getValue();
//			}
//		}
//		return "";
//	}
//
//	public int getLookupGroupId(String groupName)
//	{
//		LookupGroup group = null;
//		Iterator<LookupGroup> iter = lookupGroups.iterator();
//		while (iter.hasNext())
//		{
//			group = iter.next();
//			if (group.getValue().equalsIgnoreCase(groupName))
//			{
//				return group.getId();
//			}
//		}
//		return 0;
//	}
//
//	public Collection<LookupType> findLookupTypesForGroup(LookupGroup group)
//	{
//		return new MaintenanceImpl().findLookupTypesForGroup(group);
//	}
//
//	public Collection<LookupType> getGroupTable(int groupId)
//	{
//		String groupName = this.getLookupGroupName(groupId);
//		LookupGroup group = new LookupGroup(groupId, groupName);
//		return this.findLookupTypesForGroup(group);
//	}
//
//	public Collection<LookupType> getGroupTable(String groupName)
//	{
//		int groupId = this.getLookupGroupId(groupName);
//		LookupGroup group = new LookupGroup(groupId, groupName);
//		return this.findLookupTypesForGroup(group);
//	}

	//Lookup types
	public Collection<LookupType> getLookupTypes()
	{
		return lookupTypes;
	}

	public String getLookupTypeName(int typeId)
	{
		LookupType type = null;
		Iterator<LookupType> iter = lookupTypes.iterator();
		while (iter.hasNext())
		{
			type = (LookupType) iter.next();
			if (type.getId() == typeId)
			{
				return type.getValue();
			}
		}
		return "";
	}

	public int getLookupTypeId(String typeName)
	{
		LookupType type = null;
		Iterator<LookupType> iter = lookupTypes.iterator();
		while (iter.hasNext())
		{
			type = (LookupType) iter.next();
			if (type.getValue().equalsIgnoreCase(typeName))
			{
				return type.getId();
			}
		}
		return 0;
	}

	private String getLookupValueName(Collection<LookupValue> table, String typeId)
	{
		LookupValue type = null;
		Iterator<LookupValue> iter = table.iterator();
		while (iter.hasNext())
		{
			type = iter.next();
			if (type.getId().equalsIgnoreCase(typeId))
			{
				return type.getValue();
			}
		}
		return "";
	}

	private String getLookupValueId(Collection<LookupValue> table, String typeName)
	{
		LookupValue type = null;
		Iterator<LookupValue> iter = table.iterator();
		while (iter.hasNext())
		{
			type = iter.next();
			if (type.getValue().equalsIgnoreCase(typeName))
			{
				return type.getId();
			}
		}
		return "";
	}

	
}

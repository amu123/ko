/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.security.dto;

/**
 *
 * @author gerritj
 */
public class WorkItemAndComment {

    private WorkItem workitem;
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public WorkItem getWorkitem() {
        return workitem;
    }

    public void setWorkitem(WorkItem workitem) {
        this.workitem = workitem;
    }

}

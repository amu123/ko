/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.security.dto;

import java.util.Date;

/**
 *
 * @author gerritj
 */
public class AuthDetails {

    private int auth_id;
    private String member_no;
    private String dependant_code;
    private String surname;
    private String fullnames;
    private String auth_code;
    private String auth_type;
    private String admission_date;
    private String admission_time;
    private String discharge_date;
    private String discharge_time;
    private String length_of_stay;
    private Date modified_date;
    private String hospital_provider_no;
    private String hospital_provider;
    private String referring_provider_no;
    private String referring_provider;
    private String treating_provider_no;
    private String treating_provider;
    private String primary_icd;
    private String secondary_icd;
    private String tertiary_icd;
    private String modified_by;
    private String auth_status;
    private String PMB;
    private String co_payment;
    private String estimated_cost;
    private String release_payment_indicator;
    private String maternity_indicator;
    private String funder;
    private String LOC;
    private String anaesthetist_code;
    private String anaesthetist_name;
    private String theatre_time;
    private String hospital_indicator;
    private String scheme;
    private String scheme_option;
    private String authorisation_date;
    private String caller_name;
    private String caller_relationship;
    private String caller_reason;
    private String caller_contact;
    private String lab_provider_no;
    private String lab_provider;
    private String tarrif_list;
    private String lab_code_list;
    private String quantity;
    private String tooth_number;
    private String date_valid_from;
    private String date_valid_to;
    private String notes;
    private String procedure_list;
    private String number_of_days;
    private String previous_lens_rx;
    private String current_lens_rx;
    private String planned_duration;
    private String deposit_amount;
    private String first_installment_amount;
    private String remaining_installment_amount;

    public String getAnaesthetist_name() {
        return anaesthetist_name;
    }

    public void setAnaesthetist_name(String anaesthetist_name) {
        this.anaesthetist_name = anaesthetist_name;
    }

    public String getLOC() {
        return LOC;
    }

    public void setLOC(String LOC) {
        this.LOC = LOC;
    }

    public String getFunder() {
        return funder;
    }

    public void setFunder(String funder) {
        this.funder = funder;
    }

    public String getPMB() {
        return PMB;
    }

    public void setPMB(String PMB) {
        this.PMB = PMB;
    }

    public String getAdmission_date() {
        return admission_date;
    }

    public void setAdmission_date(String admission_date) {
        this.admission_date = admission_date;
    }

    public String getAdmission_time() {
        return admission_time;
    }

    public void setAdmission_time(String admission_time) {
        this.admission_time = admission_time;
    }

    public String getHospital_indicator() {
        return hospital_indicator;
    }

    public void setHospital_indicator(String hospital_indicator) {
        this.hospital_indicator = hospital_indicator;
    }

    public String getAnaesthetist_code() {
        return anaesthetist_code;
    }

    public void setAnaesthetist_code(String anaesthetist_code) {
        this.anaesthetist_code = anaesthetist_code;
    }

    public String getAuth_code() {
        return auth_code;
    }

    public void setAuth_code(String auth_code) {
        this.auth_code = auth_code;
    }

    public String getAuth_status() {
        return auth_status;
    }

    public void setAuth_status(String auth_status) {
        this.auth_status = auth_status;
    }

    public String getAuth_type() {
        return auth_type;
    }

    public void setAuth_type(String auth_type) {
        this.auth_type = auth_type;
    }

    public String getAuthorisation_date() {
        return authorisation_date;
    }

    public void setAuthorisation_date(String authorisation_date) {
        this.authorisation_date = authorisation_date;
    }

    public String getCaller_contact() {
        return caller_contact;
    }

    public void setCaller_contact(String caller_contact) {
        this.caller_contact = caller_contact;
    }

    public String getCaller_name() {
        return caller_name;
    }

    public void setCaller_name(String caller_name) {
        this.caller_name = caller_name;
    }

    public String getCaller_reason() {
        return caller_reason;
    }

    public void setCaller_reason(String caller_reason) {
        this.caller_reason = caller_reason;
    }

    public String getCaller_relationship() {
        return caller_relationship;
    }

    public void setCaller_relationship(String caller_relationship) {
        this.caller_relationship = caller_relationship;
    }

    public String getCo_payment() {
        return co_payment;
    }

    public void setCo_payment(String co_payment) {
        this.co_payment = co_payment;
    }

    public String getCurrent_lens_rx() {
        return current_lens_rx;
    }

    public void setCurrent_lens_rx(String current_lens_rx) {
        this.current_lens_rx = current_lens_rx;
    }

    public String getDate_valid_from() {
        return date_valid_from;
    }

    public void setDate_valid_from(String date_valid_from) {
        this.date_valid_from = date_valid_from;
    }

    public String getDate_valid_to() {
        return date_valid_to;
    }

    public void setDate_valid_to(String date_valid_to) {
        this.date_valid_to = date_valid_to;
    }

    public String getDependant_code() {
        return dependant_code;
    }

    public void setDependant_code(String dependant_code) {
        this.dependant_code = dependant_code;
    }

    public String getDeposit_amount() {
        return deposit_amount;
    }

    public void setDeposit_amount(String deposit_amount) {
        this.deposit_amount = deposit_amount;
    }

    public String getDischarge_date() {
        return discharge_date;
    }

    public void setDischarge_date(String discharge_date) {
        this.discharge_date = discharge_date;
    }

    public String getDischarge_time() {
        return discharge_time;
    }

    public void setDischarge_time(String discharge_time) {
        this.discharge_time = discharge_time;
    }

    public String getEstimated_cost() {
        return estimated_cost;
    }

    public void setEstimated_cost(String estimated_cost) {
        this.estimated_cost = estimated_cost;
    }

    public String getFirst_installment_amount() {
        return first_installment_amount;
    }

    public void setFirst_installment_amount(String first_installment_amount) {
        this.first_installment_amount = first_installment_amount;
    }

    public String getFullnames() {
        return fullnames;
    }

    public void setFullnames(String fullnames) {
        this.fullnames = fullnames;
    }

    public String getHospital_provider() {
        return hospital_provider;
    }

    public void setHospital_provider(String hospital_provider) {
        this.hospital_provider = hospital_provider;
    }

    public String getHospital_provider_no() {
        return hospital_provider_no;
    }

    public void setHospital_provider_no(String hospital_provider_no) {
        this.hospital_provider_no = hospital_provider_no;
    }

    public String getLab_code_list() {
        return lab_code_list;
    }

    public void setLab_code_list(String lab_code_list) {
        this.lab_code_list = lab_code_list;
    }

    public String getLab_provider() {
        return lab_provider;
    }

    public void setLab_provider(String lab_provider) {
        this.lab_provider = lab_provider;
    }

    public String getLab_provider_no() {
        return lab_provider_no;
    }

    public void setLab_provider_no(String lab_provider_no) {
        this.lab_provider_no = lab_provider_no;
    }

    public String getLength_of_stay() {
        return length_of_stay;
    }

    public void setLength_of_stay(String length_of_stay) {
        this.length_of_stay = length_of_stay;
    }

    public String getMaternity_indicator() {
        return maternity_indicator;
    }

    public void setMaternity_indicator(String maternity_indicator) {
        this.maternity_indicator = maternity_indicator;
    }

    public String getMember_no() {
        return member_no;
    }

    public void setMember_no(String member_no) {
        this.member_no = member_no;
    }

    public String getModified_by() {
        return modified_by;
    }

    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public Date getModified_date() {
        return modified_date;
    }

    public void setModified_date(Date modified_date) {
        this.modified_date = modified_date;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNumber_of_days() {
        return number_of_days;
    }

    public void setNumber_of_days(String number_of_days) {
        this.number_of_days = number_of_days;
    }

    public String getPlanned_duration() {
        return planned_duration;
    }

    public void setPlanned_duration(String planned_duration) {
        this.planned_duration = planned_duration;
    }

    public String getPrevious_lens_rx() {
        return previous_lens_rx;
    }

    public void setPrevious_lens_rx(String previous_lens_rx) {
        this.previous_lens_rx = previous_lens_rx;
    }

    public String getPrimary_icd() {
        return primary_icd;
    }

    public void setPrimary_icd(String primary_icd) {
        this.primary_icd = primary_icd;
    }

    public String getProcedure_list() {
        return procedure_list;
    }

    public void setProcedure_list(String procedure_list) {
        this.procedure_list = procedure_list;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getReferring_provider() {
        return referring_provider;
    }

    public void setReferring_provider(String referring_provider) {
        this.referring_provider = referring_provider;
    }

    public String getReferring_provider_no() {
        return referring_provider_no;
    }

    public void setReferring_provider_no(String referring_provider_no) {
        this.referring_provider_no = referring_provider_no;
    }

    public String getRelease_payment_indicator() {
        return release_payment_indicator;
    }

    public void setRelease_payment_indicator(String release_payment_indicator) {
        this.release_payment_indicator = release_payment_indicator;
    }

    public String getRemaining_installment_amount() {
        return remaining_installment_amount;
    }

    public void setRemaining_installment_amount(String remaining_installment_amount) {
        this.remaining_installment_amount = remaining_installment_amount;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getScheme_option() {
        return scheme_option;
    }

    public void setScheme_option(String scheme_option) {
        this.scheme_option = scheme_option;
    }

    public String getSecondary_icd() {
        return secondary_icd;
    }

    public void setSecondary_icd(String secondary_icd) {
        this.secondary_icd = secondary_icd;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getTarrif_list() {
        return tarrif_list;
    }

    public void setTarrif_list(String tarrif_list) {
        this.tarrif_list = tarrif_list;
    }

    public String getTertiary_icd() {
        return tertiary_icd;
    }

    public void setTertiary_icd(String tertiary_icd) {
        this.tertiary_icd = tertiary_icd;
    }

    public String getTheatre_time() {
        return theatre_time;
    }

    public void setTheatre_time(String theatre_time) {
        this.theatre_time = theatre_time;
    }

    public String getTooth_number() {
        return tooth_number;
    }

    public void setTooth_number(String tooth_number) {
        this.tooth_number = tooth_number;
    }

    public String getTreating_provider() {
        return treating_provider;
    }

    public void setTreating_provider(String treating_provider) {
        this.treating_provider = treating_provider;
    }

    public String getTreating_provider_no() {
        return treating_provider_no;
    }

    public void setTreating_provider_no(String treating_provider_no) {
        this.treating_provider_no = treating_provider_no;
    }

    public int getAuth_id() {
        return auth_id;
    }

    public void setAuth_id(int auth_id) {
        this.auth_id = auth_id;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.security.util;

import java.io.File;
import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 *
 * @author whauger
 */
public class EmailContent {

    private String content;
    private String contentType;
    private String subject;
    private String emailAddressFrom;
    private String emailAddressTo;
    private EmailAttachment firstAttachment;
    private EmailAttachment secondAttachment;
    public EmailAttachment thirdAttachment;
    private int productId;
    private String type;
    private byte[] firstAttachmentData;
    private byte[] secondAttachmentData;
    public byte[] thirdAttachmentData;
    public String memberNumber;
    public String memberName;
    public String memberSurname;
    private String memberTitle;
    private String memberInitials;
    private int depEntityId;
    private String depName;
    private String depSecondName;
    private String depSurname;
    private GregorianCalendar depBDay;
    private int age;
    private String idNumber;
    private String groupCode;
    private String optionName;
    private ArrayList resourceName = new ArrayList();
    private ArrayList resourceLocation = new ArrayList();
    

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * @param contentType the contentType to set
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the emailAddressFrom
     */
    public String getEmailAddressFrom() {
        return emailAddressFrom;
    }

    /**
     * @param emailAddressFrom the emailAddressFrom to set
     */
    public void setEmailAddressFrom(String emailAddressFrom) {
        this.emailAddressFrom = emailAddressFrom;
    }

    /**
     * @return the emailAddressTo
     */
    public String getEmailAddressTo() {
        return emailAddressTo;
    }

    /**
     * @param emailAddressTo the emailAddressTo to set
     */
    public void setEmailAddressTo(String emailAddressTo) {
        this.emailAddressTo = emailAddressTo;
    }

    /**
     * @return the attachmentDetails
     */
    public EmailAttachment getFirstAttachment() {
        return firstAttachment;
    }

    /**
     * @param attachmentDetails the attachmentDetails to set
     */
    public void setFirstAttachment(EmailAttachment firstAttachment) {
        this.firstAttachment = firstAttachment;
    }

    /**
     * @return the productId
     */
    public int getProductId() {
        return productId;
    }

    /**
     * @param productId the productId to set
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the attachmentData
     */
    public byte[] getFirstAttachmentData() {
        return firstAttachmentData;
    }

    /**
     * @param attachmentData the attachmentData to set
     */
    public void setFirstAttachmentData(byte[] firstAttachmentData) {
        this.firstAttachmentData = firstAttachmentData;
    }

    /**
     * @return the secondAttachment
     */
    public EmailAttachment getSecondAttachment() {
        return secondAttachment;
    }

    /**
     * @param secondAttachment the secondAttachment to set
     */
    public void setSecondAttachment(EmailAttachment secondAttachment) {
        this.secondAttachment = secondAttachment;
    }

    /**
     * @return the secondAttachmentData
     */
    public byte[] getSecondAttachmentData() {
        return secondAttachmentData;
    }

    /**
     * @param secondAttachmentData the secondAttachmentData to set
     */
    public void setSecondAttachmentData(byte[] secondAttachmentData) {
        this.secondAttachmentData = secondAttachmentData;
    }

    /**
     * @return the thirdAttachment
     */
    public EmailAttachment getTheThirdAttachment() {
        return thirdAttachment;
    }

    /**
     * @param thirdAttachment the thirdAttachment to set
     */
    public void setTheThirdAttachment(EmailAttachment thirdAttachment) {
        this.thirdAttachment = thirdAttachment;
    }

    /**
     * @return the thirdAttachmentData
     */
    public byte[] getTheThirdAttachmentData() {
        return thirdAttachmentData;
    }

    /**
     * @param thirdAttachmentData the thirdAttachmentData to set
     */
    public void setTheThirdAttachmentData(byte[] thirdAttachmentData) {
        this.thirdAttachmentData = thirdAttachmentData;
    }

    /**
     * @return the Name
     */
    public String getName() {
        return memberName;
    }

    /**
     * @param Name the Name to set
     */
    public void setName(String Name) {
        this.memberName = Name;
    }

    /**
     * @return the Surname
     */
    public String getSurname() {
        return memberSurname;
    }

    /**
     * @param Surname the Surname to set
     */
    public void setSurname(String Surname) {
        this.memberSurname = Surname;
    }

    /**
     * @return the memberCoverNumber
     */
    public String getMemberCoverNumber() {
        return memberNumber;
    }

    /**
     * @param memberCoverNumber the memberCoverNumber to set
     */
    public void setMemberCoverNumber(String memberNumber) {
        this.memberNumber = memberNumber;
    }

    /**
     * @return the memberTitle
     */
    public String getMemberTitle() {
        return memberTitle;
    }

    /**
     * @param memberTitle the memberTitle to set
     */
    public void setMemberTitle(String memberTitle) {
        this.memberTitle = memberTitle;
    }

    /**
     * @return the memberInitials
     */
    public String getMemberInitials() {
        return memberInitials;
    }

    /**
     * @param memberInitials the memberInitials to set
     */
    public void setMemberInitials(String memberInitials) {
        this.memberInitials = memberInitials;
    }
    
    public int getDepEntityId() {
        return depEntityId;
    }

    public void setDepEntityId(int depEntityId) {
        this.depEntityId = depEntityId;
    }

    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        if (depName != null) {
            this.depName = depName.trim();
        }
    }

    public String getDepSecondName() {
        return depSecondName;
    }

    public void setDepSecondName(String depSecondName) {
        if (depSecondName != null) {
            this.depSecondName = depSecondName.trim();
        }
    }

    public String getDepSurname() {
        return depSurname;
    }

    public void setDepSurname(String depSurname) {
        if (depSurname != null) {
            this.depSurname = depSurname.trim();
        }
    }

    public GregorianCalendar getDepBDay() {
        return depBDay;
    }

    public void setDepBDay(GregorianCalendar depBDay) {
        if (depBDay != null) {
            this.depBDay = depBDay;
        }
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        if (idNumber != null) {
            this.idNumber = idNumber;
        }
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        if (optionName != null) {
            this.optionName = optionName.trim();
        }
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    /**
     * Retrieves all the needed files required to generate a mail.<br/>NOTE: The method setType() helps to retrieve a specific resource.
     *
     * @param resourceName
     * @return
     */
    public ArrayList getResourceLocation(String resourceName) {
        ArrayList arrResource = new ArrayList();
        for (int i = 0; i < this.resourceName.size(); i++) {
            if (this.resourceName.get(i).equals(resourceName)) {
                arrResource.add(this.resourceLocation.get(i));
            }
        }
        return arrResource;
    }

    /**
     * Removes added location from the list.<br/>This is normally used for Replacing the old path with a custom path
     *
     * @param resourceLocation
     */
    public void removeResourceLocation(String resourceLocation) {
        boolean status = false;
        for (int i = 0; i < this.resourceLocation.size(); i++) {
            if (this.resourceLocation.get(i).equals(resourceLocation)) {
                this.resourceLocation.remove(i);
                this.resourceName.remove(i);
                status = true;
            }
        }

        if (!status) {
            System.out.println("--------------------------------------------------------------");
            System.out.println("Resource: [" + resourceLocation + "] does not exist");
            System.out.println("--------------------------------------------------------------");
        }
    }

    /**
     * MANDATORY field when using EML files.<br/>NOTE: The method setType() helps to store a specific resource.
     *
     * @param resourceName
     * @param resourceLocation
     */
    public void addResourceLocation(String resourceName, String resourceLocation) {
        if (resourceName != null && !resourceName.isEmpty() && resourceLocation != null && !resourceLocation.isEmpty()) {
            File resourceLoc = new File(resourceLocation);
            if (resourceLoc.exists()) {
                this.resourceName.add(resourceName);
                this.resourceLocation.add(resourceLocation);
            }
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.security.dto;

/**
 *
 * @author whauger
 */
public class DentalCode {

    private String disciplineType;
    private int code;
    private String description;
    private double tariff;
    private String rules;

    public DentalCode() {
    }

    public DentalCode(String disciplineType, int code, String description, double tariff, String rules) {
        this.disciplineType = disciplineType;
        this.code = code;
        this.description = description;
        this.tariff = tariff;
        this.rules = rules;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisciplineType() {
        return disciplineType;
    }

    public void setDisciplineType(String disciplineType) {
        this.disciplineType = disciplineType;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public double getTariff() {
        return tariff;
    }

    public void setTariff(double tariff) {
        this.tariff = tariff;
    }

}

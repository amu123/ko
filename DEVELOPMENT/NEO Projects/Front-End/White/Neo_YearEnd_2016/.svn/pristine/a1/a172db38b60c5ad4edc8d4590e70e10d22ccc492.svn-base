/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.neo.command;

import com.koh.network_management.command.NeoUser;
import flexjson.JSONSerializer;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import za.co.agile.command.FECommand;
import za.co.resolution.NeoContractNetwork;
import za.co.resolution.NeoContractPracticeNetwork;


/**
 *
 * @author Administrator
 */
public class MoveContractNetworkCommand extends FECommand {
    
    private static Logger loggerNL = Logger.getLogger(MoveContractNetworkCommand.class);
    
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        loggerNL.info("**************** DEBUG MoveContractNetworkCommand *******************");
        NeoUser user = (NeoUser) request.getSession().getAttribute("networkMaintanceNeoUser");
        
        String entityCommonId = request.getParameter("entityCommonId");
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String networkId = request.getParameter("networkId");
        String entityId = request.getParameter("entityId");
        String networkSwitch = request.getParameter("networkSwitch");
        
        //EASY JSON OBJECT FOR RESPONSE
        RespondObject respondObject = null;
        
        
        loggerNL.info("Moving entity Common Id : "   + entityCommonId);
        loggerNL.info("startDate : "   + startDate);
        loggerNL.info("endDate : "   + endDate);
        loggerNL.info("new networkId : "   + networkId);
        loggerNL.info("networkSwitch : "   + networkSwitch);
        
        if (networkSwitch.equalsIgnoreCase("network")) {
            
            loggerNL.info("processExistingPracticeToNetwork");
            respondObject = processExistingPracticeToNetwork(response, startDate,endDate
                    , Integer.parseInt(networkId),Long.parseLong(entityCommonId),user);
        
        } else if (networkSwitch.equalsIgnoreCase("nonNetwork"))  {
        
            loggerNL.info("processAddPracticeToNetwork");
            respondObject = processAddPracticeToNetwork(response, startDate, endDate,Integer.parseInt(networkId)
                    ,Long.parseLong(entityCommonId),user);
        } else {
            loggerNL.error("********** networkSwitch invalid value *********************");
            
        }
        
        PrintWriter out = null;

        try {
            out = response.getWriter();

            //WRITE RESPONSE OBJECT AS JSON AS RESPONSE
            out.println(new JSONSerializer()
                 .deepSerialize(respondObject));

        } catch (Exception ex) {
            loggerNL.error(ex);
        } finally {
            out.close();
        } 
        
        loggerNL.info("******************************************");
        
        return null;
    }

    private RespondObject processExistingPracticeToNetwork(HttpServletResponse response,String startDate
            ,String endDate,Integer networkId,Long entityCommonId,NeoUser user) {
        
        
        RespondObject respondObject = new RespondObject();
        
        List<NeoContractPracticeNetwork> networkHistory = 
                neoContractPracticeNetworkJpaController.findNeoContractPracticeNetworkByEntityCommonId(entityCommonId);
         
        //LAST CONTRACT NETWORK FOR PROVIDER
        NeoContractPracticeNetwork lastRecord = networkHistory.get(0);

        Date newStartDate = null;
        Date newEndDate = null;
        Date newStartDateMinus1 = null;
        
        try {
            newStartDate = sdfDate.parse(startDate);
            newEndDate = sdfDate.parse(endDate);
            
            Calendar c = Calendar.getInstance();
            c.setTime(sdfDate.parse(startDate));
            c.add(Calendar.DATE, -1);  // number of days to add
            
            newStartDateMinus1 = c.getTime();  // dt is now the new date
            loggerNL.info("StartDate - 1 : " + sdfDate.format(newStartDateMinus1));
            
        } catch (ParseException ex) {
            loggerNL.error(ex);
            respondObject.done = 0;
            return respondObject;
        }
        
        //END PREVIOUS RECORD
        lastRecord.setEndDate(newStartDateMinus1);
        
        try {
            loggerNL.info("Starting update of old record updated...");
            neoContractPracticeNetworkJpaController.edit(lastRecord);
            loggerNL.info("Old record updated");
        } catch (Exception ex) {
            loggerNL.error(ex);
            respondObject.done = 0;
            return respondObject;
        }
        
         //LAST CONTRACT NETWORK FOR PROVIDER
        NeoContractPracticeNetwork newContractPracticeNetwork = new NeoContractPracticeNetwork();
        
        newContractPracticeNetwork.setCreatedBy(new BigInteger("" + user.getId()));
        newContractPracticeNetwork.setCreationDate(new Date());

        newContractPracticeNetwork.setEffectiveStartDate(newStartDate);
        newContractPracticeNetwork.setStartDate(newStartDate);

        newContractPracticeNetwork.setEffectiveEndDate(newEndDate);
        newContractPracticeNetwork.setEndDate(newEndDate);
        
        newContractPracticeNetwork.setLastUpdatedBy(new BigInteger("" + user.getId()));
        newContractPracticeNetwork.setLastUpdateDate(new Date());

        newContractPracticeNetwork.setSecurityGroupId(new BigInteger("" + user.getSecurityGroupId()));
        
        newContractPracticeNetwork.setEntityId(networkHistory.get(0).getEntityId());
        
        loggerNL.info("Finding NeoContractNetwork for networkId : " + networkId);
        //FIND CONTRACT NETWORK FOR CHOSEN NETWORK BY NETWORK ID
        NeoContractNetwork contractNetwork = neoContractNetworkJpaController.findNeoContractNetworkByNetworkId(networkId);
        
        loggerNL.info("NetworkName : " + contractNetwork.getNetworkDescription());
        newContractPracticeNetwork.setNetworkId(contractNetwork);
            
        try {
            loggerNL.info("Starting insert of new record...");
            neoContractPracticeNetworkJpaController.create(newContractPracticeNetwork);
            loggerNL.info("record inserted!");
            respondObject.done = 1;
        } catch (Exception ex) {
            loggerNL.error(ex);
            respondObject.done = 0;
        }
        
        return respondObject;
    }
    
    
    //ADD NON_NETWORK PRACTICE TO PRACTICE
    private RespondObject processAddPracticeToNetwork(HttpServletResponse response,
            String startDate,String endDate,Integer networkId,Long entityCommonId,NeoUser user) {
        
        RespondObject respondObject = new RespondObject();
        
        Date newStartDate = null;
        Date newEndDate = null;

        try {
            newStartDate = sdfDate.parse(startDate);
            newEndDate = sdfDate.parse(endDate);
        } catch (ParseException ex) {
            loggerNL.error(ex);
            respondObject.done = 0;
            return respondObject;
        }
        
        //LAST CONTRACT NETWORK FOR PROVIDER
        NeoContractPracticeNetwork newContractPracticeNetwork = new NeoContractPracticeNetwork();
        
        newContractPracticeNetwork.setCreatedBy(new BigInteger(user.getId() + ""));
        newContractPracticeNetwork.setCreationDate(new Date());

        newContractPracticeNetwork.setEffectiveStartDate(newStartDate);
        newContractPracticeNetwork.setStartDate(newStartDate);

        newContractPracticeNetwork.setEffectiveEndDate(newEndDate);
        newContractPracticeNetwork.setEndDate(newEndDate);

        newContractPracticeNetwork.setSecurityGroupId(new BigInteger(user.getSecurityGroupId() + ""));
        
        newContractPracticeNetwork.setLastUpdatedBy(new BigInteger(user.getId() + ""));
        newContractPracticeNetwork.setLastUpdateDate(new Date());
        
        loggerNL.info("Finding NeoContractNetwork for networkId : " + networkId);
        //FIND CONTRACT NETWORK FOR CHOSEN NETWORK BY NETWORK ID
        NeoContractNetwork contractNetwork = neoContractNetworkJpaController.findNeoContractNetworkByNetworkId(networkId);
        
        Long entityId;
        try {
            //FIND NEO_ENTITY_COMMON RECORD TO RETREIVE ENTITY_ID
            entityId = neoEntityCommonJpaController.retreiveEntityIdByCommonId(entityCommonId, sdfDate.parse("2999/12/31"));
            loggerNL.info("entityId for practice  : " + entityId);
            
            //SET ENTITY ID FOR NEW CONTRACT PRACTICE NETWORK
            newContractPracticeNetwork.setEntityId(entityId);
            
        } catch (ParseException ex) {
            loggerNL.error(ex);
            respondObject.done = 0;
            return respondObject;
        }
        
        if (contractNetwork!=null) {
            
            loggerNL.info("NetworkName : " + contractNetwork.getNetworkDescription());
            newContractPracticeNetwork.setNetworkId(contractNetwork);
            
            try {
                loggerNL.info("Starting insert of new record...");
                neoContractPracticeNetworkJpaController.create(newContractPracticeNetwork);
                loggerNL.info("record inserted!");
                respondObject.done = 1;

            } catch (Exception ex) {
                loggerNL.error(ex);
                respondObject.done = 0;
            }

        } else {
            respondObject.done = 0;
        }
        
        return respondObject;

        
    
    }
    
    @Override
    public String getName() {
        return "MoveContractNetworkCommand";
    }
    
    private class RespondObject {
    
        int done;
    
    }
}

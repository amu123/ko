var agileTabLinks = new Array();
var agileContentDivs = new Array();

function initAgileTabs() {

    // Grab the tab links and content divs from the page
    var tabListItems = $("#tabs").children();
    
    for ( var i = 0; i < tabListItems.length; i++ ) {
        if ( tabListItems[i].nodeName == "LI" ) {
            var tabLink = getFirstChildWithTagName( tabListItems[i], 'A' );
            var id = getAgileHash( tabLink.getAttribute('href') );
            agileTabLinks[id] = tabLink;
            agileContentDivs[id] = document.getElementById( id );
        }
    }

    // Assign onclick event to tabs and select the first tab 
    i = 0;
    for (id in agileTabLinks ) {
        agileTabLinks[id].onclick = showAgileTab;
        agileTabLinks[id].onfocus = function() { 
            this.blur()
        };
        if ( i == 0 ) agileTabLinks[id].className = 'selected';
        i++;
    }

    // Hide all content divs except the first
    i = 0;
    for (id in agileContentDivs ) {
        if ( i != 0 ) agileContentDivs[id].className = 'tabContent hide';
        i++;
    }

    i = 0;
    for (id in agileTabLinks) {
        showSelectedAgileTab(id);
        break;
    }
}

function showAgileTab() {
    var selectedId = getAgileHash( this.getAttribute('href') );
    showSelectedAgileTab(selectedId);
    return false;
}

function showAgileTabs(value) {
    if (eval(value) == true) {
        document.getElementById('tabs').className = "";
    } else {
        document.getElementById('tabs').className = "hide";
    }
    return false;
}

function showSelectedAgileTab(selectedId) {
    for ( var id in agileContentDivs ) {
        if ( id == selectedId ) {
            agileTabLinks[id].className = 'selected';
            agileContentDivs[id].className = 'tabContent';
        } else {
            agileTabLinks[id].className = '';
            agileContentDivs[id].className = 'tabContent hide';
        }
    }
    
    getAgileContent(selectedId);
    
   
/*if (agileContentDivs[selectedId].innerHTML.indexOf("Loading...", 0) > -1) {
        
        
        
    }*/
}

function getAgileContent(selectedId) {
    return getTabContents(selectedId);
}

function getFirstChildWithTagName( element, tagName ) {
    for ( var i = 0; i < element.childNodes.length; i++ ) {
        if ( element.childNodes[i].nodeName == tagName ) return element.childNodes[i];
    }
    return null;
}

function getAgileHash( url ) {
    var hashPos = url.indexOf ( '#' );
    return url.substring( hashPos + 1 );
}
    
function getElementsByClassName(className) {// Get a list of all elements in the document
    if (document.getElementsByClassName) {
        return document.getElementsByClassName(className);
    } else {
        var retnode = [];
        var myclass = new RegExp('\\b'+className+'\\b');
        var elem = document.getElementsByTagName('*');
        for (var i = 0; i < elem.length; i++) {
            var classes = elem[i].className;
            if (myclass.test(classes)) retnode.push(elem[i]);
        }
        return retnode;
    }
}

function getTabContents(selectedId) {
    
    var form = document.forms["TabControllerForm"];
    var url = form.action + "?" + encodeFormForSubmit(form)+ "&tab=" + escape(selectedId) +  "&nocache=" + new Date().getTime();
    //JQUERY GET CALL TO RETREIVE TAB CONTENT
    $.get(url, function(result){
        //WHEN DONE
        var newdiv = document.createElement("div");
        newdiv.innerHTML = result;
        var children = agileContentDivs[selectedId].childNodes; //childNodes retrieves the Div tag and the "loading..." text
        for (var i=0; i <= children.length; i++) {
            agileContentDivs[selectedId].removeChild(children[0]); //when deleting the child the other item replaces the 0 index (so do not say children[i])
        }
        agileContentDivs[selectedId].appendChild(newdiv);
    });
}
function getPageContentsForReIndex(form, url, main_div, targetId) {
    var new_url;
    var params;
    var encodeVal;
    if (form != null) {
        $(form).mask("Loading...");
        new_url = form.action;
        encodeVal = encodeFormForSubmitForReIndex(form);
        params = encodeVal + '&main_div=' + main_div + '&target_div=' + targetId +  "&nocache=" + new Date().getTime();
    } else {
        new_url = url;
        params = '?main_div=' + main_div + '&target_div=' + targetId;
    }
    //JQUERY GET
    $.post(new_url, params, function(response){
        var newdiv = document.createElement("div");
        newdiv.innerHTML = response;
        elem = document.getElementById(targetId);
        if (elem != null) {
            var children = elem.childNodes;
            for (var i=0; i < children.length; i++) {
                elem.removeChild(children[i]);
            }
            elem.appendChild(newdiv);

            if (elem.style.display=="none") elem.style.display = "block";
        }
        
        elem = document.getElementById(main_div);
        if (elem != null) {
            elem.style.display = "none";
        }
        if (form != null) {
            //UNMASK FORM
            $(form).unmask();
        }
    });
    
}
function getPageContents(form, url, main_div, targetId) {
    var new_url;
    var params;
    var encodeVal;
    if (form != null) {
        $(form).mask("Loading...");
        new_url = form.action;
        encodeVal = encodeFormForSubmit(form);
        params = encodeVal + '&main_div=' + main_div + '&target_div=' + targetId +  "&nocache=" + new Date().getTime();
    } else {
        new_url = url;
        params = '?main_div=' + main_div + '&target_div=' + targetId;
    }
    //JQUERY GET
    $.get(new_url, params, function(response){
        //        var newdiv = document.createElement("div");
        //        newdiv.innerHTML = response;
        elem = document.getElementById(targetId);
        if (elem != null) {
            //            var children = elem.childNodes;
            //            for (var i=0; i < children.length; i++) {
            //                elem.removeChild(children[i]);
            //            }
            //            elem.appendChild(newdiv);
            $('#'+targetId).empty().html(response);
            if ($('#'+targetId).css('display') == "none") $('#'+targetId).css('display', 'block');

            if (elem.style.display=="none") elem.style.display = "block";
        }
        
        elem = document.getElementById(main_div);
        if (elem != null) {
            elem.style.display = "none";
        }
        if (form != null) {
            //UNMASK FORM
            $(form).unmask();
        }
    });
    
}
function encodeFormForSubmitForReIndex(form) {
    var encStr = "formName=" + escape(form.name) + "&";
    for (var i=0; i < form.elements.length; i++) {
        var element = form.elements[i];
        var elementType = element.type.toUpperCase();
        var elementVal = "";
        if (!element.name) continue;
        encStr += element.name + "=";
        if (elementType == "CHECKBOX") {
            elementVal = element.checked ? element.checked : "false";
        } else {
            elementVal = element.value;
        }
        encStr += encodeURIComponent(elementVal ? elementVal : "");
        if (i < form.elements.length - 1) encStr += "&";
    }
    return encStr;
}
function encodeFormForSubmit(form) {
    var encStr = "formName=" + escape(form.name) + "&";
    for (var i=0; i < form.elements.length; i++) {
        var element = form.elements[i];
        var elementType = element.type.toUpperCase();
        var elementVal = "";
        if (!element.name) continue;
        encStr += element.name + "=";
        if (elementType == "CHECKBOX") {
            elementVal = element.checked ? element.checked : "false";
        } else {
            elementVal = element.value;
        }
        encStr += escape(elementVal ? elementVal : "");
        if (i < form.elements.length - 1) encStr += "&";
    }
    return encStr;
}

function updateFormFromJSON(obj) {
    for (i in obj) {
        var elem = obj[i].id;
        var element = document.getElementById(elem);
        if (element != null) {
            if (element.nodeName == "LABEL") {
                element.innerHTML = obj[i].value;
            } else if (element.nodeName == "INPUT" && element.type.toUpperCase() == "CHECKBOX" ) {
                element.checked = obj[i].value;
            } else {
                element.value = obj[i].value;
            }
        } else {
            var elements = getElementsByClassName(obj[i].id);
            if (elements != null && elements.length > 0) {
                for (j in elements) {
                    elements[j].innerHTML = obj[i].value;
                }
            }
        }
    }
}

function isJSONData(string) {
    var json;
    try {
        json = eval('(' + string + ')');
    } catch (exception) {
        json = null;
    }
    return json;
}

function submitFormWithAjaxPost(form, targetId, btn, mainDiv, popupDiv) {
    
    
    if (btn != undefined) {
        
        btn.disabled = true;
    }
    
    var params = encodeFormForSubmit(form);
    
    if ($(form).attr("name")=="MemberAppNotesAddForm") {
        var noteDetails = CKEDITOR.instances.MemberAppNotesAdd_details.getData();
        CKEDITOR.instances.MemberAppNotesAdd_details.setData('');
        noteDetails = getPlainText(noteDetails);
        params = params += "&noteDetails=" + noteDetails;
    }
    if ($(form).attr("name")=="EmployerAppNotesAddForm") {
        var noteDetails = CKEDITOR.instances.EmployeerAppNotesAdd_details.getData();
        CKEDITOR.instances.EmployeerAppNotesAdd_details.setData('');
        noteDetails = getPlainText(noteDetails);
        params = params += "&noteDetails=" + noteDetails;
    }
    if ($(form).attr("name")=="BrokerFirmAppNotesAddForm") {
        var noteDetails = CKEDITOR.instances.BrokerFirmAppNotesAdd_details.getData();
        CKEDITOR.instances.BrokerFirmAppNotesAdd_details.setData('');
        noteDetails = getPlainText(noteDetails);
        params = params += "&noteDetails=" + noteDetails;
    }
    if ($(form).attr("name")=="BrokerAppNotesAddForm") {
        var noteDetails = CKEDITOR.instances.BrokerAppNotesAdd_details.getData();
        CKEDITOR.instances.BrokerAppNotesAdd_details.setData('');
        noteDetails = getPlainText(noteDetails);
        params = params += "&noteDetails=" + noteDetails;
    }
    if ($(form).attr("name")=="BrokerConsultantAppNotesAddForm") {
        var noteDetails = CKEDITOR.instances.BrokerConsultantAppNotesAdd_details.getData();
        CKEDITOR.instances.BrokerConsultantAppNotesAdd_details.setData('');
        noteDetails = getPlainText(noteDetails);
        params = params += "&noteDetails=" + noteDetails;
    }
    if($(form).attr("name")=="UnionAppNotesAddForm"){
        var noteDetails = CKEDITOR.instances.UnionAppNotesAdd_details.getData();
        CKEDITOR.instances.UnionAppNotesAdd_details.setData('');
        noteDetails = getPlainText(noteDetails);
        params = params += "&noteDetails=" + noteDetails;        
    }
    
    var formId = $(form).attr('id');
     
    //MASK SUBMITED FORM UNTIL POST IS COMPLETED
    
    $(form).mask("Processing...");
    
    //POST FORM TO BACKEND
    $.post(form.action, params,function(result){
        
        updateFormFromData(result, targetId, mainDiv, popupDiv);
            
        if ($(form).attr("name")=="MemberCoverDetailsForm") { 
            $("#newStartDate").val("");
            $("#newEmployerStartDate").val("");
            $("#newBrokerStartDate").val("");
            
            $("#CoverDetails_optionIdStartDate").css("visibility","hidden");
            $("#CoverDetails_employerStartDate").css("visibility","hidden");
            $("#CoverDetails_brokerStartDate").css("visibility","hidden");
            
        }
        
        if (btn != undefined) {
            btn.disabled = false;
        }
        
        //UNMASK FORM
        if ($('#formId').unmask != null) $(form).unmask();
        
    });
    
}
function submitFormWithAjaxPostAdd(form, targetId, btn, mainDiv, popupDiv) {
    
    
    if (btn != undefined) {
        
        btn.disabled = true;
    }
    
    var params = encodeFormForSubmit(form);
    var formId = $(form).attr('id');
     
    //MASK SUBMITED FORM UNTIL POST IS COMPLETED
    
    // $(form).mask("Processing...");
    
    //POST FORM TO BACKEND
    $.post(form.action, params,function(result){
        
        updateFormFromData(result, targetId, mainDiv, popupDiv);
        
        if ($(form).attr("name")=="MemberCoverDetailsForm") { 
            $("#newStartDate").val("");
            $("#newEmployerStartDate").val("");
            $("#newBrokerStartDate").val("");
            
            $("#CoverDetails_optionIdStartDate").css("visibility","hidden");
            $("#CoverDetails_employerStartDate").css("visibility","hidden");
            $("#CoverDetails_brokerStartDate").css("visibility","hidden");
            
        }
        
        if (btn != undefined) {
            btn.disabled = false;
        }
        
    //UNMASK FORM
    //  if ($('#formId').unmask != null) $(form).unmask();
        
    });
    
}


function updateFormFromData(string, targetId, mainDiv, popupDiv) {
   
    if (!(string == null || string.length == 0)) {
        jsonData = isJSONData(string);
        console.log(JSON.stringify(jsonData));
        if (jsonData) {
            if (jsonData.validation_errors) updateFormFromJSON(jsonData.validation_errors);
            if (jsonData.update_fields) updateFormFromJSON(jsonData.update_fields);
            if (jsonData.enable_tabs) showAgileTabs(jsonData.enable_tabs);
            if (jsonData.message) alert(jsonData.message);
            if (jsonData.forward) {
                window.location = jsonData.forward;
            }
        } else {
            if (targetId != null) {
                $('#'+targetId).empty().html(string);
                if ($('#'+targetId).css('display') == "none") $('#'+targetId).css('display', 'block');
                
            //                elem = document.getElementById(targetId);
            //                if (elem != null) {
            //                    if (elem.style.display=="none") elem.style.display = "block";
            //                    var newdiv = document.createElement("div");
            //                    newdiv.innerHTML = string;
            //                    var children = elem.childNodes;
            //                    var childLength = children.length;
            //                    for (var i=childLength-1; i > -1; i--) {
            //                        elem.removeChild(children[i]);
            //                    }
            //                    elem.appendChild(newdiv);
            //                }
            }
            if (mainDiv != null && popupDiv != null) {
                swapDivVisbible(popupDiv, mainDiv);
            }
        }
    }
        
}

function submitFormWithAjaxAdd(form, targetId, btn, mainDiv, popupDiv) {
    
    
    if (btn != undefined) {
        
        btn.disabled = true;
    }
    
    var params = encodeFormForSubmit(form);
    var formId = $(form).attr('id');
     
    //MASK SUBMITED FORM UNTIL POST IS COMPLETED
    
    $(form).mask("Processing...");
    
    //POST FORM TO BACKEND
    $.post(form.action, params,function(result){
        
        updateFormFromData(result, targetId, mainDiv, popupDiv);
        
        if ($(form).attr("name")=="MemberCoverDetailsForm") { 
            $("#newStartDate").val("");
            $("#newEmployerStartDate").val("");
            $("#newBrokerStartDate").val("");
            
            $("#CoverDetails_optionIdStartDate").css("visibility","hidden");
            $("#CoverDetails_employerStartDate").css("visibility","hidden");
            $("#CoverDetails_brokerStartDate").css("visibility","hidden");
            
        }
        
        if (btn != undefined) {
            btn.disabled = false;
        }
        
        //UNMASK FORM
        if ($('#formId').unmask != null) $(form).unmask();
        
    });
    
}

function showElement(value, elemName) {
    elem = document.getElementById(elemName);
    elem.className = value ? '' : 'hide';
}

function setElemStyle(elemId, value) {
    elem = document.getElementById(elemId);
    elem.style.display=value;
}

function validateCellNo(value, elemId) {
    elem = document.getElementById(elemId).value;
    elemError = document.getElementById(elemId + "_error");
    if (value == "1"){
        if (elem == ""){
            elemError.innerHTML = "Cell number is required";
        }
    }else{
        elemError.innerHTML = "";
    }

}

function showActivateButton() {
    document.getElementById('MemberAppActiveButton').style.display='block';
}
function swapBrokerAndConsultant(value) {
    if (value == "1"){
        document.getElementById('brokerDetails').style.display='block';
        document.getElementById('brokerConsultantDetails').style.display='none';
        document.getElementById('saveBroker').value ='broker';
    }else{
        document.getElementById('brokerConsultantDetails').style.display='block';
        document.getElementById('brokerDetails').style.display='none';
        document.getElementById('saveBroker').value ='consultant';
    }
}
function toogleDocument(lookUpID, element2Toggle){
    if(lookUpID != ""){
        addDocument(lookUpID, element2Toggle);
    }else{
        $("#"+element2Toggle).empty();
    }
}
  
function addDocument(lookUpID, element2Toggle){                
    var element2ToggleJQ = "#"+element2Toggle;
    if(lookUpID == "212"){
      $('#documentUpload').show();
  } else {
      $('#documentUpload').hide();
  }
    var url="/ManagedCare/AgileController";
    var data = {
        'opperation':'LoadDocumentDropDownListCommand',
        'lookUpID':lookUpID
    };
        
        
    $.get(url, data, function(resultTxt){
        if (resultTxt!=null) {           
            $(element2ToggleJQ).empty();
            $(element2ToggleJQ).append("<option value=\"\"></option>");
            var lookUps = resultTxt.split("&");
            for (var i = 0; i < lookUps.length; i++) {
                var lookupArr = lookUps[i].split(":");
                $(element2ToggleJQ).append("<option value=\"" + lookupArr[0] + "\">" + lookupArr[1] + "</option>");          
            }      

        }
    }, "text/xml");  
}


function hideDependentType(value) {
    if (value == "1"){
        document.getElementById('MAI_newDependantTypeRow2').style.display='none';
        document.getElementById('MAI_newDependantTypeRow1').style.display='block';
    }else{
        document.getElementById('MAI_newDependantTypeRow1').style.display='none';
        document.getElementById('MAI_newDependantTypeRow2').style.display='block';
    }
}

function swapDivVisbible(elem1, elem2) {
    document.getElementById(elem1).style.display='none';
    document.getElementById(elem2).style.display='block';
}

function swapDivVisibleReIndex(elem1, elem2){
    document.getElementById(elem1).style.display='none';
    document.getElementById(elem2).style.display='block';
    document.getElementById("searchButton").click();
}

function setFormValues(formName, values) {
    var form = document.forms[formName];
    for (i in values) {
        elem = form[i];
        if (elem != null) {
            elem.value = values[i];
        }
    }
}

function getValues(prNum, prName, prNetw) {
    var practiceNumber = document.getElementById('prcNr').value;
    var practiceName = document.getElementById('prName').value;
    var practiceNetwork = document.getElementById('prNetw').value;
    
    var data = [];
    
    data[practiceNumber] = prNum;
    data[practiceName] = prName;
    data[practiceNetwork] = prNetw;
    
    setElemValues(data);
}

function setElemValues(values) {
    for (i in values) {
        var element = document.getElementById(i);
        if (element != null) {
            if (element.nodeName == "LABEL") {
                element.innerHTML = values[i];
            } else if (element.nodeName == "INPUT" && element.type.toUpperCase() == "CHECKBOX" ) {
                element.checked = values[i];
            } else {
                element.value = values[i];
            }
        }        
    }
}

function setFormValuesFromForm(formName, formName2,formName3, values) {
    var form = document.forms[formName];
    var form2 = document.forms[formName2];
    var form3 = document.forms[formName3];
    for (i in values) {
        elem = form[i];
        elem2 = form2[values[i]];
        elem3 = form3[values[i]]
        if (elem != null && elem2 != null && elem3 !=null) {
            elem.value = elem2.value  = elem3.value;
        }
    }
}

function addTableRow(tableId, form, values, requiredFields) {
    if (requiredFields) {
        valid = true;//checkRequiredFields(form, requiredFields);
        if (!valid) {
            alert ("Please fill in required fields");
            return;
        }
    }
    var table = document.getElementById(tableId);
    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);
    var colCounter = 0;
    for (i in values) {
        var cell = row.insertCell(colCounter);
        aname = tableId + "_" + rowCount + "_" + colCounter;
        switch (values[i]) {
            case "text" :
                element = document.createElement("input");
                element.type = "text";
                element.name = aname;
                element.value = i;
                cell.appendChild(element);
                break;
            case "hidden" :
                element = document.createElement("input");
                element.type = "hidden";
                element.name = aname;
                element.value = i;
                cell.appendChild(element);
            case "button" :
                element = document.createElement("input");
                element.type = "button";
                element.name = aname;
                element.value = i;
                if (i == "Remove") {
                    element.onclick = function() { 
                        deleteTableRow(tableId, rowCount);
                    };
                }
                //                    cell.appendChild(element);
                break;
            case "label" :
                element = document.createElement("label");
                element.innerHTML = i;
                cell.appendChild(element);
            case "formValue" :
                formElem = form[i];
                if (formElem.nodeName == "SELECT") {
                    value = formElem.options[formElem.selectedIndex].innerHTML
                } else {
                    value = form[i].value;
                }
                //                    element = document.createElement("label");
                //                    element.innerHTML = value;
                //                    cell.appendChild(element);
                cell.innerHTML = value;
                element = document.createElement("input");
                element.type = "hidden";
                element.name = name;
                element.value = form[i].value;
                cell.appendChild(element);
        }
        colCounter++;
    }

}

function deleteTableRow(tableId, row) {
    var table = document.getElementById(tableId);
    table.deleteRow(row);

}

function checkRequiredFields(form, values) {
    valid = true;
    for (i in values) {
        elem = form[values[i]].value;
        if (elem == null || elem == "") {
            valid = false;
            break;
        }
    }
    return valid;
}

function paging_changePage(pagingForm, pagingDiv, newPage, oldPage) {
    var elem;
    if (oldPage != null) {
        elem = document.getElementById(pagingDiv + "_" + oldPage);
        elem.style.display = 'none';
    }
    if (newPage != null) {
        elem = document.getElementById(pagingDiv + "_"  + newPage);
        if (elem != null) {
            elem.style.display = 'block';
        } else {
            paging_getNextPage(pagingForm, pagingDiv, newPage);
            elem = document.getElementById(pagingDiv + "_"  + newPage);
            if (elem != null) {
                elem.style.display = 'block';
            }
        }
    }
}

function paging_getNextPage(pagingForm, pagingDiv, newPage) {
    var url = pagingForm.action + "?" + encodeFormForSubmit(pagingForm)+ "&pageNum="+ newPage+  "&nocache=" + new Date().getTime();
    $.get(url, function(response){
        var newDiv = document.createElement("div");
        var newDivName = pagingDiv + "_" + newPage;
        newDiv.innerHTML = response;
        newDiv.setAttribute("id",newDivName );
        document.getElementById(pagingDiv).appendChild(newDiv);
    });
}

function toggle_table_group(btn, id) {
    var tblElem = document.getElementById(id).style.display;
    if (tblElem == "none") {
        document.getElementById(id).style.display = 'table-row-group';
        btn.value = '-';
    } else {
        document.getElementById(id).style.display = 'none';
        btn.value = '+';
    }
}

function validateNeoDateField(field, required, min, max) {
    var fieldName = getNeoFieldName(field);
    var fieldVal = $("#" + fieldName).val();
    if (required && !validateNeoRequiredField(fieldName)) {
        return false;
    }
    var validformat=/^\d{4}\/\d{2}\/\d{2}$/;
    if (!validformat.test(fieldVal)) {
        $("#" + fieldName + "_error").text(fieldVal + " is an invalid date");
        return false;
    }
    var year = fieldVal.split("/")[0];
    var month = fieldVal.split("/")[1] - 1;
    var day = fieldVal.split("/")[2];
    var date = new Date(year, month, day);
    if (date.getFullYear() != year || date.getMonth() != month || date.getDate() != day) {
        $("#" + fieldName + "_error").text("Invalid date");
        return false;
    }
    if (min) {
        var minDate = getNeoDate(min);
        if (date < minDate) {
            $("#" + fieldName + "_error").text("Date is too far in the past");
            return false;
        }
    }
    if (max) {
        var maxDate = getNeoDate(max);
        if (date > maxDate) {
            $("#" + fieldName + "_error").text("Date is too far in the future");
            return false;
        }
    }
    $("#" + fieldName + "_error").text("");
    return true;
}

function validateNeoStringField(field, required, min, max) {
    var fieldName = getNeoFieldName(field);
    var fieldVal = $("#" + fieldName).val();
    if (required && !validateNeoRequiredField(fieldName)) {
        return false;
    }
    if (min) {
        if (fieldVal.length < min) {
            $("#" + fieldName + "_error").text("must be at least " + min + " characters");
            return false;
        }
    }
    if (max) {
        if (fieldVal.length > max) {
            $("#" + fieldName + "_error").text("must be at most " + max + " characters");
            return false;
        }
    }
    $("#" + fieldName + "_error").text("");
    return true;
}

function validateNeoAmountField(field, required, min, max) {
    var fieldName = getNeoFieldName(field);
    var fieldVal = $("#" + fieldName).val();
    if (required && !validateNeoRequiredField(fieldName)) {
        return false;
    }
    var amtVal = parseFloat(fieldVal);
    if (isNaN(amtVal)) {
        $("#" + fieldName + "_error").text(fieldVal + " is an invalid amount");
        return false;
    }
    if (min) {
        if (amtVal < min) {
            $("#" + fieldName + "_error").text("must be at least " + min);
            return false;
        }
    }
    if (max) {
        if (amtVal > max) {
            $("#" + fieldName + "_error").text("must be at most " + max);
            return false;
        }
    }
    $("#" + fieldName + "_error").text("");
    return true;
}

function validateNeoIntField(field, required, min, max) {
    var fieldName = getNeoFieldName(field);
    var fieldVal = $("#" + fieldName).val();
    if (required && !validateNeoRequiredField(fieldName)) {
        return false;
    }
    var amtVal = parseInt(fieldVal);
    if (isNaN(amtVal)) {
        $("#" + field + "_error").text(fieldVal + " is an invalid number");
        return false;
    }
    if (min) {
        if (amtVal < min) {
            $("#" + fieldName + "_error").text("must be at least " + min);
            return false;
        }
    }
    if (max) {
        if (amtVal > max) {
            $("#" + fieldName + "_error").text("must be at most " + max);
            return false;
        }
    }
    $("#" + fieldName + "_error").text("");
    return true;
}

function getNeoFieldName(field) {
    var objType = Object.prototype.toString.call(field).replace(/^\[object (.+)\]$/,"$1").toLowerCase();
    var fieldName;
    if ("string" == objType) {
        fieldName = field;
    } else {
        fieldName = $(field).attr('id');
    } 
    return fieldName;
}

function getNeoLabeFromField(field) {
    var lbl = $("#" + field).closest('td').prev('td').find('label');
    return lbl.text();
}

function getNeoDate(date) {
    var year = date.split("/")[0];
    var month = date.split("/")[1] - 1;
    var day = date.split("/")[2];
    return new Date(year, month, day);
}

function validateNeoRequiredField(field) {
    var valid = true;
    if ($("#" + field).val().length == 0) {
        var lbl = $("#" + field).closest('td').prev('td').find('label');
        $("#" + field + "_error").text(lbl.text() + " is required");
        //                    $("#refundReason_error").text("Refund Reason is required");
        valid = false;
    } else {
        var lbl = $("#" + field).closest('td').prev('td').find('label');
        $("#" + field + "_error").text("");
    }
    return valid;
}

function displayNeoNotification(html, type) {
    $('.neonotification').hide();
    $('.neonotification').removeClass('neonotificationerror neonotificationsuccess');
    $('.neonotification').addClass(type);
    $('.neonotification').html(html);
    $('.neonotification').show();
    $('.neonotification').fadeOut(10000);
}

function displayNeoErrorNotification(html) {
    displayNeoNotification('<span>error:</span>  &nbsp; &nbsp;' + html, 'neonotificationerror');
}

function displayNeoSuccessNotification(html) {
    displayNeoNotification('<span>success:</span> &nbsp; &nbsp;' + html, 'neonotificationsuccess');
}

function toggleNeoPanel(panel, command, action, id, reload) {
    if ($(panel).hasClass('neopanelcollapsed')) {
        $(panel).removeClass('neopanelcollapsed').addClass('neopanel');
        if (command != undefined) {
            if ($(panel).children('.neopanelcontent').text() == '' || reload == true) {
                var url = "/ManagedCare/AgileController?opperation=" + command;
                if (action != undefined) {
                    url = url + "&command=" + action;
                }
                if (id != undefined) {
                    url = url + "&id=" + id;
                }
                url = url + "&nocache=" + new Date().getTime(); 
                $(panel).children('.neopanelcontent').empty().html("<div class='neonotificationnormal neonotificationnotice'><span>Loading</span></div>");
                $.get(url, function(result){
                    $(panel).children('.neopanelcontent').empty().html(result);
                });
            }
        }
    } else {
        $(panel).removeClass('neopanel').addClass('neopanelcollapsed');
    }
    
}

function toggleNeoOptionPanel(panel, command, action, id, reload) {
    if ($(panel).hasClass('neooptionpanelcollapsed')) {
        $(panel).removeClass('neooptionpanelcollapsed').addClass('neooptionpanel');
        if (command !== undefined) {
            if ($(panel).children('.neopanelcontent').text() === '' || reload === true) {
                var url = "/ManagedCare/AgileController?opperation=" + command;
                if (action !== undefined) {
                    url = url + "&command=" + action;
                }
                if (id !== undefined) {
                    url = url + "&id=" + id;
                }
                url = url + "&nocache=" + new Date().getTime(); 
                $(panel).children('.neopanelcontent').empty().html("<div class='neonotificationnormal neonotificationnotice'><span>Loading</span></div>");
                $.get(url, function(result){
                    $(panel).children('.neopanelcontent').empty().html(result);
                });
            }
        }
    } else {
        $(panel).removeClass('neooptionpanel').addClass('neooptionpanelcollapsed');
    }
}

function NeoOptionPanelSelect(command, action, id, size) {
    if (command !== undefined) {
        var url = "/ManagedCare/AgileController?opperation=" + command;
        if (action !== undefined) {
            url = url + "&command=" + action;
        }
        if (id !== undefined) {
            url = url + "&id=" + id;
        }
        if (size !== undefined) {
            parent.workflowFrameset.cols = size;
        }
        url = url + "&nocache=" + new Date().getTime();
        $.get(url, function (result) {
            document.getElementById("neoOptionPanelSelect_Content").innerHTML = result;
        });
    }
}

function toggle_table_group(btn, id) {
    var tblElem = document.getElementById(id).style.display;
    if (tblElem == "none") {
        document.getElementById(id).style.display = 'table-row-group';
        btn.value = '-';
    } else {
        document.getElementById(id).style.display = 'none';
        btn.value = '+';
    }
}

function reIndexFileGeneric(){
    var selectedItem = document.getElementById('Document Type');
    var documentType = selectedItem.options[selectedItem.selectedIndex].value;
    var url =  "/ManagedCare/AgileController";
    var data = {
        "opperation":"DocumentReIndexCommand",
        "nocache": new Date().getTime(),
        "indexEntityNumber": document.getElementById('memberNumber').value,
        "indexDocType": documentType,
        "indexEntityType": document.getElementById('indexEntityType').value,
        "indexFileLocation": document.getElementById('indexFileLocation').value,
        "indexRefNumber":'None',
        "indexID": document.getElementById('indexID').value
    }
    $('#documentReIndexing').mask("Processing...");
    $.post(url, data, function(response){
        if(response == 'Successful'){
            if ($('#documentReIndexing').unmask != null) $('#documentReIndexing').unmask();
            swapDivVisibleReIndex('overlay','main_div');         
        }else{
            if ($('#documentReIndexing').unmask != null) $('#documentReIndexing').unmask();
            $("#errortext").text(response);
        }
    },'TEXT');
}

function reIndexFile(){

    var selectedItem = document.getElementById('Document Type');
    var documentType = selectedItem.options[selectedItem.selectedIndex].value;
    var url =  "/ManagedCare/AgileController";
    var data = {
        "opperation":"DocumentReIndexCommand",
        "nocache": new Date().getTime(),
        "indexEntityNumber": document.getElementById('memberNumber').value,
        "indexDocType": documentType,
        "indexEntityType": document.getElementById('indexEntityType').value,
        "indexFileLocation": document.getElementById('indexFileLocation').value,
        "indexRefNumber":document.getElementById('claimNumber').value,
        "indexID": document.getElementById('indexID').value
    }
    $('#documentReIndexing').mask("Processing...");
    $.post(url, data, function(response){
        if(response == 'Successful'){
            if ($('#documentReIndexing').unmask != null) $('#documentReIndexing').unmask();
            swapDivVisibleReIndex('overlay','main_div');         
        }else{
            if ($('#documentReIndexing').unmask != null) $('#documentReIndexing').unmask();
            $("#errortext").text(response);
        }        
    },'TEXT');
}  

function getTableHeaderAsArray(tbl) {
    var $table = $('#'+tbl);
    var $headerCells = $table.find('thead th');
    var headers = [];
    $headerCells.each(function(k,v) {
        headers[headers.length] = $(this).text();
    });
    return headers;
}

function getTableRowsAsArray(tbl) {
    var $table = $('#'+tbl);
    var $rows = $table.find('tbody tr');
    var rows = [];

    $rows.each(function(row,v) {
        $(this).find('td').each(function(cell, v) {
            if (typeof rows[row] === 'undefined') rows[row] = [];
            rows[row][cell] = $(this).text();
        });
    });
    return rows;
}

function refreshText(){
   
   var selectedType = document.getElementById("ConcesType");
    var selectedText = selectedType.options[selectedType.selectedIndex].text;
        
        
    if(selectedText=='No'){
            
        
       document.getElementById('ConcessionStart').value="";
        document.getElementById('ConcessionEnd').value="";
           
                    
    }

}
  
function refreshBrokerText(){
   
    var selectedType = document.getElementById("ConcesType");
    var selectedText = selectedType.options[selectedType.selectedIndex].text;
        
       
     if(selectedText=='No'){
      
        document.getElementById('FirmApp_ConcessionStart').value="";
        document.getElementById('FirmApp_ConcessionEnd').value="";
    }
  

  
}  
      
function refreshEmployerText(){
   
    var selectedType = document.getElementById("ConcesType");
    var selectedText = selectedType.options[selectedType.selectedIndex].text;
        
        
    if(selectedText=='No'){
      
        document.getElementById('EmployerUnderwriting_startDate').value="";
        document.getElementById('EmployerUnderwriting_endDate').value="";
    }

  
  


}

function getPlainText( strSrc ) {
    var resultStr = "";

    // Ignore the <p> tag if it is in very start of the text
    if(strSrc.indexOf('<p>') == 0)
        resultStr = strSrc.substring(3);
    else
        resultStr = strSrc;

    // Replace <p> with one newline
    resultStr = resultStr.replace(/<p>/gi, "\r\n");
    // Replace <br /> with one newline
    resultStr = resultStr.replace(/<br \/>/gi, "\r\n");
    resultStr = resultStr.replace(/<br>/gi, "\r\n");
    //replace &nbsp with a blank
    resultStr = resultStr.replace(/^(&nbsp;|<br>)+/, '');
    resultStr = resultStr.replace(/&nbsp;/g, ''); 
    //replace &quot; with a \"
    resultStr = resultStr.replace(/&quot;/g,'\"');

    //-+-+-+-+-+-+-+-+-+-+-+
    // Strip off other HTML tags.
    //-+-+-+-+-+-+-+-+-+-+-+
    resultStr = escape(resultStr); 
    return  resultStr.replace( /<[^<|>]+?>/gi,'' );
}

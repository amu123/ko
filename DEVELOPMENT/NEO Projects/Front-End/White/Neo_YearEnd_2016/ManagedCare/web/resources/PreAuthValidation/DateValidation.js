/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/************** PreAuth Date Validtion *************/
function GetDVXmlHttpObject(){
    if (window.ActiveXObject){
        // code for IE6, IE5
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (window.XMLHttpRequest){
        // code for IE7+, Firefox, Chrome, Opera, Safari
        return new XMLHttpRequest();
    }
    return null;
}

//main
function validatePreAuthDate(date, element, ap, apm, at, skip){
    var dateF;
    var dateT;
    var fe;
    var te;
    var locHideButtonValue;
    var hospitalFound = false;
    var orthoFound = false;
    var proceed = false;

    //required fields
    var authType = at;
    var authPeriod = ap;
    var authMonthPeriod = apm;

    //clear fields
    $("#authFromDate_error").text("");
    $("#authType_error").text("");
    $("#authToDate_error").text("");
    $("#locDateFrom_error").text("");
    $("#locDateTo_error").text("");
    $("#dcDateFrom_error").text("");
    $("#dcDateTo_error").text("");

    //pre validation
    if(authPeriod != "" && authPeriod != "null" && authPeriod != "99"){
        proceed = true;
    }else{
        $("#authPeriod_error").text("Auth Period is Mandatory");
    }
    if(authMonthPeriod != "" && authMonthPeriod != "null"){
        proceed = true;
    }else{
        $("#authMonthPeriod_error").text("Auth Period Month is Mandatory");
    }
    if(authType != "null" && authType != "99" && authType != ""){
        proceed = true;
    }else{
        $("#authType_error").text("Auth Period is Mandatory");
    }

    if(proceed == true){

        if(authType == "4" || authType == "11"){
            hospitalFound = true;
            var str = $("#calType:checked").val();
            if(str == "on"){
                fe = "locDateFrom";
                te = "locDateTo";

                locHideButtonValue = "locCalButton";

            }else{
                fe = "dcDateFrom";
                te = "dcDateTo";

                locHideButtonValue = "dCalButton";
            }
            //get dates
            if(element == te){
                dateF = $("#"+fe+"").val();
                dateT = date;

            }else{
                dateF = date;
                dateT = $("#"+te+"").val();
            }
        }else{
            if(authType == "3"){
                orthoFound = true;
            }
            fe = "authFromDate";
            te = "authToDate";
            //get dates
            if(element == "authToDate"){
                dateF = $("#authFromDate").val();
                dateT = date;

            }else{
                dateF = date;
            }
        }

        xhr = GetDVXmlHttpObject();
        if (xhr==null)
        {
            alert ("Your browser does not support XMLHTTP!");
            return;
        }
        //add form values to command operation in url
        var formValues = "&dateF="+dateF
        + "&dateT="+dateT
        + "&fe="+fe
        + "&te="+te
        + "&authType="+at
        + "&authPeriod="+ap
        + "&authMonthPeriod="+apm
        + "&skip="+skip
        + "&id="+new Date().getTime();

        var url="/ManagedCare/AgileController?opperation=PreAuthDateValidation"+formValues;
        xhr.onreadystatechange=function(){
            if(xhr.readyState == 4 && xhr.status == 200){
                var rText = xhr.responseText.split('|');
                var result = rText[0];
                if(result == "Done"){
                    $("#authFromDate_error").text("");
                    $("#authType_error").text("");
                    $("#authToDate_error").text("");
                    $("#locDateFrom_error").text("");
                    $("#locDateTo_error").text("");
                    $("#dcDateFrom_error").text("");
                    $("#dcDateTo_error").text("");

                    if(hospitalFound){
                        $("#"+locHideButtonValue+"").show();
                    }
                    if(orthoFound){
                        $("#orthoTDButton").show();
                    }

                }else if(result == "Error"){
                    var errorResponse = xhr.responseText.substring(xhr.responseText.indexOf('|')+1, xhr.responseText.length);
                    var mandatoryList = errorResponse.split("|");
                    for(i=0;i<mandatoryList.length;i++) {
                        var elementError = mandatoryList[i].split(":");
                        var element = elementError[0];
                        var errorMsg = elementError[1];

                        $("#"+element+"_error").text(errorMsg);

                        if(hospitalFound){
                            $("#"+locHideButtonValue+"").hide();
                        }
                        if(orthoFound){
                            $("#orthoTDButton").hide();
                        }
                    }
                }
            }
        };
        xhr.open("POST", url, true);
        xhr.send(null);
    }
}

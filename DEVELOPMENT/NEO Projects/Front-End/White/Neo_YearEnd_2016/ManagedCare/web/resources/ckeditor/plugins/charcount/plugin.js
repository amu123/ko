CKEDITOR.plugins.add( 'charcount',
{
   init : function( editor )
   {
      var defaultLimit = 'unlimited';
      var defaultFormat = '<span class="cke_charcount_count">%count%</span> of <span class="cke_charcount_limit">%limit%</span> characters';
      var limit = defaultLimit;
      var format = defaultFormat;

      var intervalId;
      var lastCount = 0;
      var limitReachedNotified = false;
      var limitRestoredNotified = false;
      
      
      if ( true )
      {   
         function counterId( editor )
         {
//            return 'cke_charcount_' + editor.name;
            return editor.name;
         }
         
         function counterElement( editor )
         {
            return document.getElementById( counterId(editor) );
         }
         
         function updateCounter( editor )
         {
            var count = editor.getData().length;
            if( count == lastCount ){
               return true;
            } else {
               lastCount = count;
            }
            if( !limitReachedNotified && count > limit ){
               limitReached( editor );
            } else if( !limitRestoredNotified && count < limit ){
               limitRestored( editor );
            }
            
            var html = format.replace('%count%', count).replace('%limit%', limit);
            counterElement(editor).innerHTML = html;
         }
         
         function limitReached( editor )
         {
            limitReachedNotified = true;
            limitRestoredNotified = false;
            editor.setUiColor( '#FFC4C4' );
         }
         
         function limitRestored( editor )
         {
            limitRestoredNotified = true;
            limitReachedNotified = false;
            editor.setUiColor( '#C4C4C4' );
         }

         editor.on( 'themeSpace', function( event )
         {
            if ( event.data.space == 'bottom' )
            {
               event.data.html += '<div id="'+counterId(event.editor)+'" class="cke_charcount"' +
                  ' title="' + CKEDITOR.tools.htmlEncode( 'Character Counter' ) + '"' +
                  '>&nbsp;</div>';
            }
         }, editor, null, 100 );
         
         editor.on( 'instanceReady', function( event )
         {
            if( editor.config.charcount_limit != undefined )
            {
               limit = editor.config.charcount_limit;
            }
            
            if( editor.config.charcount_format != undefined )
            {
               format = editor.config.charcount_format;
            }
            
            
         }, editor, null, 100 );
         
         editor.on( 'dataReady', function( event )
         {
            var count = event.editor.getData().length;
            if( count > limit ){
               limitReached( editor );
            }
            updateCounter(event.editor);
         }, editor, null, 100 );
         
         editor.on( 'key', function( event )
         {
            //updateCounter(event.editor);
         }, editor, null, 100 );
         
         function updater()
             {
                updateCounter(editor);             
             }
             editor.on( 'focus', function( event )
             {
                editorHasFocus = true;
                intervalId = window.setInterval(updater, 1000, event.editor);
             }, editor, null, 100 );
         
         editor.on( 'blur', function( event )
         {
            editorHasFocus = false;
            if( intervalId )
               clearInterval(intervalId);
         }, editor, null, 100 );
      }
   }
});

//CKEDITOR.plugins.add('charcount',{
//  init:function(editor){
//    // si les elements compteur et maxlength son declarer alors on compte
//    if (editor.config.MaxLength) {
//      var stri_label_charcount = (editor.lang.labelCharcount) ? editor.lang.labelCharcount : "Character counter";
//      addEventOnkey(editor);
//    }
//    else {
//      stri_label_charcount = "";
//    }
//    editor.ui.addButton('CharCount',{
//      label:stri_label_charcount, 
//      command:'charcount'
//    });
//  }
//});
//
//function addEventOnkey(editor) {
//  // compte a la fois a la saisi et sur le collé
//  editor.on( 'contentDom', function() {
//    $('.cke_button_charcount .cke_label').html(editor.config.MaxLength-(editor.getData().length));
//    editor.document.on( 'keyup', function(event) {
//      compteur(editor);
//    });
//  });
//}
//
//function compteur(editor) {
//  $('.cke_button_charcount .cke_label').html(editor.config.MaxLength-(editor.getData().length));
//  
//  // si le compteur de caractere atteind 0 on tronque le text et on previent l'utilisateur
//  if ($('.cke_button_charcount .cke_label').html() < 0) {
//    // on prend la valeur max et on tronque a 10 caractere de moins (balise HTML + marge)
//    editor.setData(editor.getData().substr(0,(editor.config.MaxLength-10)));
//    
//    // on afficher le compteur a 10 car il replace automatiquement la balise fermante ex : </p>
//    $('.cke_button_charcount .cke_label').html("10");
//    
//    // on cherche le texte d'alert dans le fichier lang de l'utilisateur de CKEditor
//    var stri_alert_text = (editor.lang.limiteAtteinte) ? editor.lang.limiteAtteinte : "Text too long !"
//     alert(stri_alert_text);
//     
//     editor.focus();
//  }
//}
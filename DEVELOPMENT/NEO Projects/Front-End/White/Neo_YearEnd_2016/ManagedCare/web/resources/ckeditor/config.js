/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
     config.language = 'EN';
    // config.uiColor = '#AADC6E';
    config.height = 170;     // pixels.
    config.width = '100%';   // CSS unit (percent).
    config.maxLength = 4000;
    // and add the external plugin ;)
    config.extraPlugins = 'charcount';
    config.charcount_limit = '4000';
    config.autoParagraph = false;
    config.removePlugins = 'save,print,preview,find,about,maximize,showblocks';
//    config.toolbar = 'TinyBare';
    config.toolbar_BasicBar = [
        [ 'Copy', 'Past','-' , 'Find', 'Replace','-', 'Scayt']
    ];
    
    config.toolbar_Basic = [
        { name: 'document', items: ['NewPage'] },
        { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', '-', 'Undo', 'Redo' ] },
        { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
        { name: 'paragraph', items: [ 'Language' ] },
        { name: 'about', items: [ 'About' ] }  
    ];
    config.disableNativeSpellChecker = false;
    config.scayt_autoStartup = true;
    config.fillEmptyBlocks = false;
    config.tabSpaces = 0;
};

//CKEDITOR.config.toolbar_TinyBare = [
//    ['Source', 'Maximize', 'PasteFromWord', '-', 'NumberedList', 'BulletedList', 'Bold', 'Italic', 'Underline', 'TextColor', 'BGColor'], '-', ['CharCount']
//];




<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <script type="text/javascript">
 
 
 
 var codesite_token = "125d90c7016dbfd9c828c03b95ed61d0";
 
 
 var logged_in_user_email = "whauger@gmail.com";
 
 </script>
 
 
 <title>core.js - 
 datejs -
 
 Project Hosting on Google Code</title>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
 
 <link type="text/css" rel="stylesheet" href="http://www.gstatic.com/codesite/ph/1676283234471223295/css/ph_core.css">
 
 <link type="text/css" rel="stylesheet" href="http://www.gstatic.com/codesite/ph/1676283234471223295/css/ph_detail.css" >
 
 
 <link type="text/css" rel="stylesheet" href="http://www.gstatic.com/codesite/ph/1676283234471223295/css/d_sb_20080522.css" >
 
 
 
<!--[if IE]>
 <link type="text/css" rel="stylesheet" href="http://www.gstatic.com/codesite/ph/1676283234471223295/css/d_ie.css" >
<![endif]-->
 <style type="text/css">
 .menuIcon.off { background: no-repeat url(http://www.gstatic.com/codesite/ph/images/dropdown_sprite.gif) 0 -42px }
 .menuIcon.on { background: no-repeat url(http://www.gstatic.com/codesite/ph/images/dropdown_sprite.gif) 0 -28px }
 .menuIcon.down { background: no-repeat url(http://www.gstatic.com/codesite/ph/images/dropdown_sprite.gif) 0 0; }
 </style>
</head>
<body class="t4">
 <div id="gaia">
 
 <span>
 
 <b>whauger@gmail.com</b>
 
 
 | <a href="/u/whauger/" id="projects-dropdown" onclick="return false;">My favorites</a><img width="14" height="14" class="menuIcon off" id="menuIcon-projects-dropdown" src="http://www.gstatic.com/codesite/ph/images/cleardot.gif"/>
 
 | <a href="/u/whauger/" onclick="_CS_click('/gb/ph/profile');" title="Profile, Updates, and Settings">Profile</a>
 | <a href="https://www.google.com/accounts/Logout?continue=http%3A%2F%2Fcode.google.com%2Fp%2Fdatejs%2Fsource%2Fbrowse%2Ftrunk%2Fbuild%2Fcore.js" onclick="_CS_click('/gb/ph/signout');">Sign out</a>
 
 </span>

 </div>
 <div class="gbh" style="left: 0pt;"></div>
 <div class="gbh" style="right: 0pt;"></div>
 
 
 <div style="height: 1px"></div>
<!--[if IE 6]>
<div style="text-align:center;">
Support browsers that contribute to open source, try <a href="http://www.firefox.com">Firefox</a> or <a href="http://www.google.com/chrome">Google Chrome</a>.
</div>
<![endif]-->
 <table style="padding:0px; margin: 20px 0px 0px 0px; width:100%" cellpadding="0" cellspacing="0">
 <tr style="height: 58px;">
 
 <td style="width: 55px; text-align:center;">
 <a href="/p/datejs/">
 
 
 
 <img src="/p/datejs/logo?logo_id=1238229949" alt="Logo">
 
 
 </a>
 </td>
 
 <td style="padding-left: 0.8em">
 
 <div id="pname" style="margin: 0px 0px -3px 0px">
 <a href="/p/datejs/" style="text-decoration:none; color:#000">datejs</a>
 </div>
 
 <div id="psum">
 <i><a id="project_summary_link" href="/p/datejs/" style="text-decoration:none; color:#000">A JavaScript Date Library</a></i>
 </div>
 
 </td>
 <td style="white-space:nowrap; text-align:right">
 
 <form action="/hosting/search">
 <input size="30" name="q" value="">
 <input type="submit" name="projectsearch" value="Search projects" >
 </form>
 
 </tr>
 </table>



 
<table id="mt" cellspacing="0" cellpadding="0" width="100%" border="0">
 <tr>
 <th onclick="if (!cancelBubble) _go('/p/datejs/');">
 <div class="tab inactive">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <a onclick="cancelBubble=true;" href="/p/datejs/">Project&nbsp;Home</a>
 </div>
 </div>
 </th><td>&nbsp;&nbsp;</td>
 
 
 
 
 <th onclick="if (!cancelBubble) _go('/p/datejs/downloads/list');">
 <div class="tab inactive">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <a onclick="cancelBubble=true;" href="/p/datejs/downloads/list">Downloads</a>
 </div>
 </div>
 </th><td>&nbsp;&nbsp;</td>
 
 
 
 
 
 <th onclick="if (!cancelBubble) _go('/p/datejs/w/list');">
 <div class="tab inactive">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <a onclick="cancelBubble=true;" href="/p/datejs/w/list">Wiki</a>
 </div>
 </div>
 </th><td>&nbsp;&nbsp;</td>
 
 
 
 
 
 <th onclick="if (!cancelBubble) _go('/p/datejs/issues/list');">
 <div class="tab inactive">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <a onclick="cancelBubble=true;" href="/p/datejs/issues/list">Issues</a>
 </div>
 </div>
 </th><td>&nbsp;&nbsp;</td>
 
 
 
 
 
 <th onclick="if (!cancelBubble) _go('/p/datejs/source/checkout');">
 <div class="tab active">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <a onclick="cancelBubble=true;" href="/p/datejs/source/checkout">Source</a>
 </div>
 </div>
 </th><td>&nbsp;&nbsp;</td>
 
 
 <td width="100%">&nbsp;</td>
 </tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0" class="st">
 <tr>
 
 
 
 
 
 
 <td>
 <div class="st2">
 <div class="isf">
 
 
 
 <span class="inst1"><a href="/p/datejs/source/checkout">Checkout</a></span> |
 <span class="inst2"><a href="/p/datejs/source/browse/">Browse</a></span> |
 <span class="inst3"><a href="/p/datejs/source/list">Changes</a></span> |
 
 <form action="http://www.google.com/codesearch" method="get" style="display:inline"
 onsubmit="document.getElementById('codesearchq').value = document.getElementById('origq').value + ' package:http://datejs\\.googlecode\\.com'">
 <input type="hidden" name="q" id="codesearchq" value="">
 <input maxlength="2048" size="35" id="origq" name="origq" value="" title="Google Code Search" style="font-size:92%">&nbsp;<input type="submit" value="Search Trunk" name="btnG" style="font-size:92%">
 
 
 
 </form>
 </div>
</div>

 </td>
 
 
 
 
 
 <td height="4" align="right" valign="top" class="bevel-right">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 </td>
 </tr>
</table>
<script type="text/javascript">
 var cancelBubble = false;
 function _go(url) { document.location = url; }
</script>


<div id="maincol"
 
>

 
<!-- IE -->



<div class="expand">


<style type="text/css">
 #file_flipper { display: inline; float: right; white-space: nowrap; }
 #file_flipper.hidden { display: none; }
 #file_flipper .pagelink { color: #0000CC; text-decoration: underline; }
 #file_flipper #visiblefiles { padding-left: 0.5em; padding-right: 0.5em; }
</style>
<div id="nav_and_rev" class="heading">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner" id="bub">
 <div class="bub-top">
 <div class="pagination" style="margin-left: 2em">
 <table cellpadding="0" cellspacing="0" class="flipper">
 <tbody>
 <tr>
 
 <td>
 <ul class="leftside">
 
 <li><a href="/p/datejs/source/browse/trunk/build/core.js?r=190" title="Previous">&lsaquo;r190</a></li>
 
 </ul>
 </td>
 
 <td><b>r195</b></td>
 
 </tr>
 </tbody>
 </table>
 </div>
 
 <div class="" style="vertical-align: top">
 <div class="src_crumbs src_nav">
 <strong class="src_nav">Source path:&nbsp;</strong>
 <span id="crumb_root">
 
 <a href="/p/datejs/source/browse/">svn</a>/&nbsp;</span>
 <span id="crumb_links" class="ifClosed"><a href="/p/datejs/source/browse/trunk/">trunk</a><span class="sp">/&nbsp;</span><a href="/p/datejs/source/browse/trunk/build/">build</a><span class="sp">/&nbsp;</span>core.js</span>
 
 
 </div>
 
 </div>
 <div style="clear:both"></div>
 </div>
 </div>
</div>

<style type="text/css">
 
  tr.inline_comment {
 background: #fff;
 vertical-align: top;
 }
 div.draft, div.published {
 padding: .3em;
 border: 1px solid #999; 
 margin-bottom: .1em;
 font-family: arial, sans-serif;
 max-width: 60em;
 }
 div.draft {
 background: #ffa;
 } 
 div.published {
 background: #e5ecf9;
 }
 div.published .body, div.draft .body {
 padding: .5em .1em .1em .1em;
 max-width: 60em;
 white-space: pre-wrap;
 white-space: -moz-pre-wrap;
 white-space: -pre-wrap;
 white-space: -o-pre-wrap;
 word-wrap: break-word;
 }
 div.draft .actions {
 margin-left: 1em;
 font-size: 90%;
 }
 div.draft form {
 padding: .5em .5em .5em 0;
 }
 div.draft textarea, div.published textarea {
 width: 95%;
 height: 10em;
 font-family: arial, sans-serif;
 margin-bottom: .5em;
 }


 
 .nocursor, .nocursor td, .cursor_hidden, .cursor_hidden td {
 background-color: white;
 height: 2px;
 }
 .cursor, .cursor td {
 background-color: darkblue;
 height: 2px;
 display: '';
 }

</style>
<div class="fc">
 


<table class="opened" id="review_comment_area"><tr>
<td id="nums">
<pre><table width="100%"><tr class="nocursor"><td></td></tr></table></pre>

<pre><table width="100%"><tr id="gr_svn191_1"><td id="1"><a href="#1">1</a></td></tr
><tr id="gr_svn191_2"><td id="2"><a href="#2">2</a></td></tr
><tr id="gr_svn191_3"><td id="3"><a href="#3">3</a></td></tr
><tr id="gr_svn191_4"><td id="4"><a href="#4">4</a></td></tr
><tr id="gr_svn191_5"><td id="5"><a href="#5">5</a></td></tr
><tr id="gr_svn191_6"><td id="6"><a href="#6">6</a></td></tr
><tr id="gr_svn191_7"><td id="7"><a href="#7">7</a></td></tr
><tr id="gr_svn191_8"><td id="8"><a href="#8">8</a></td></tr
><tr id="gr_svn191_9"><td id="9"><a href="#9">9</a></td></tr
><tr id="gr_svn191_10"><td id="10"><a href="#10">10</a></td></tr
><tr id="gr_svn191_11"><td id="11"><a href="#11">11</a></td></tr
><tr id="gr_svn191_12"><td id="12"><a href="#12">12</a></td></tr
><tr id="gr_svn191_13"><td id="13"><a href="#13">13</a></td></tr
><tr id="gr_svn191_14"><td id="14"><a href="#14">14</a></td></tr
><tr id="gr_svn191_15"><td id="15"><a href="#15">15</a></td></tr
><tr id="gr_svn191_16"><td id="16"><a href="#16">16</a></td></tr
><tr id="gr_svn191_17"><td id="17"><a href="#17">17</a></td></tr
><tr id="gr_svn191_18"><td id="18"><a href="#18">18</a></td></tr
><tr id="gr_svn191_19"><td id="19"><a href="#19">19</a></td></tr
><tr id="gr_svn191_20"><td id="20"><a href="#20">20</a></td></tr
><tr id="gr_svn191_21"><td id="21"><a href="#21">21</a></td></tr
><tr id="gr_svn191_22"><td id="22"><a href="#22">22</a></td></tr
><tr id="gr_svn191_23"><td id="23"><a href="#23">23</a></td></tr
><tr id="gr_svn191_24"><td id="24"><a href="#24">24</a></td></tr
><tr id="gr_svn191_25"><td id="25"><a href="#25">25</a></td></tr
><tr id="gr_svn191_26"><td id="26"><a href="#26">26</a></td></tr
><tr id="gr_svn191_27"><td id="27"><a href="#27">27</a></td></tr
><tr id="gr_svn191_28"><td id="28"><a href="#28">28</a></td></tr
><tr id="gr_svn191_29"><td id="29"><a href="#29">29</a></td></tr
><tr id="gr_svn191_30"><td id="30"><a href="#30">30</a></td></tr
><tr id="gr_svn191_31"><td id="31"><a href="#31">31</a></td></tr
><tr id="gr_svn191_32"><td id="32"><a href="#32">32</a></td></tr
><tr id="gr_svn191_33"><td id="33"><a href="#33">33</a></td></tr
><tr id="gr_svn191_34"><td id="34"><a href="#34">34</a></td></tr
><tr id="gr_svn191_35"><td id="35"><a href="#35">35</a></td></tr
><tr id="gr_svn191_36"><td id="36"><a href="#36">36</a></td></tr
><tr id="gr_svn191_37"><td id="37"><a href="#37">37</a></td></tr
><tr id="gr_svn191_38"><td id="38"><a href="#38">38</a></td></tr
><tr id="gr_svn191_39"><td id="39"><a href="#39">39</a></td></tr
><tr id="gr_svn191_40"><td id="40"><a href="#40">40</a></td></tr
><tr id="gr_svn191_41"><td id="41"><a href="#41">41</a></td></tr
><tr id="gr_svn191_42"><td id="42"><a href="#42">42</a></td></tr
><tr id="gr_svn191_43"><td id="43"><a href="#43">43</a></td></tr
><tr id="gr_svn191_44"><td id="44"><a href="#44">44</a></td></tr
><tr id="gr_svn191_45"><td id="45"><a href="#45">45</a></td></tr
><tr id="gr_svn191_46"><td id="46"><a href="#46">46</a></td></tr
><tr id="gr_svn191_47"><td id="47"><a href="#47">47</a></td></tr
><tr id="gr_svn191_48"><td id="48"><a href="#48">48</a></td></tr
></table></pre>

<pre><table width="100%"><tr class="nocursor"><td></td></tr></table></pre>
</td>
<td id="lines">
<pre class="prettyprint"><table width="100%"><tr class="cursor_stop cursor_hidden"><td></td></tr></table></pre>

<pre class="prettyprint lang-js"><table><tr
id=sl_svn191_1><td class="source">/**<br></td></tr
><tr
id=sl_svn191_2><td class="source"> * @version: 1.0 Alpha-1<br></td></tr
><tr
id=sl_svn191_3><td class="source"> * @author: Coolite Inc. http://www.coolite.com/<br></td></tr
><tr
id=sl_svn191_4><td class="source"> * @date: 2008-05-13<br></td></tr
><tr
id=sl_svn191_5><td class="source"> * @copyright: Copyright (c) 2006-2008, Coolite Inc. (http://www.coolite.com/). All rights reserved.<br></td></tr
><tr
id=sl_svn191_6><td class="source"> * @license: Licensed under The MIT License. See license.txt and http://www.datejs.com/license/. <br></td></tr
><tr
id=sl_svn191_7><td class="source"> * @website: http://www.datejs.com/<br></td></tr
><tr
id=sl_svn191_8><td class="source"> */<br></td></tr
><tr
id=sl_svn191_9><td class="source">(function(){var $D=Date,$P=$D.prototype,$C=$D.CultureInfo,p=function(s,l){if(!l){l=2;}<br></td></tr
><tr
id=sl_svn191_10><td class="source">return(&quot;000&quot;+s).slice(l*-1);};$P.clearTime=function(){this.setHours(0);this.setMinutes(0);this.setSeconds(0);this.setMilliseconds(0);return this;};$P.setTimeToNow=function(){var n=new Date();this.setHours(n.getHours());this.setMinutes(n.getMinutes());this.setSeconds(n.getSeconds());this.setMilliseconds(n.getMilliseconds());return this;};$D.today=function(){return new Date().clearTime();};$D.compare=function(date1,date2){if(isNaN(date1)||isNaN(date2)){throw new Error(date1+&quot; - &quot;+date2);}else if(date1 instanceof Date&amp;&amp;date2 instanceof Date){return(date1&lt;date2)?-1:(date1&gt;date2)?1:0;}else{throw new TypeError(date1+&quot; - &quot;+date2);}};$D.equals=function(date1,date2){return(date1.compareTo(date2)===0);};$D.getDayNumberFromName=function(name){var n=$C.dayNames,m=$C.abbreviatedDayNames,o=$C.shortestDayNames,s=name.toLowerCase();for(var i=0;i&lt;n.length;i++){if(n[i].toLowerCase()==s||m[i].toLowerCase()==s||o[i].toLowerCase()==s){return i;}}<br></td></tr
><tr
id=sl_svn191_11><td class="source">return-1;};$D.getMonthNumberFromName=function(name){var n=$C.monthNames,m=$C.abbreviatedMonthNames,s=name.toLowerCase();for(var i=0;i&lt;n.length;i++){if(n[i].toLowerCase()==s||m[i].toLowerCase()==s){return i;}}<br></td></tr
><tr
id=sl_svn191_12><td class="source">return-1;};$D.isLeapYear=function(year){return((year%4===0&amp;&amp;year%100!==0)||year%400===0);};$D.getDaysInMonth=function(year,month){return[31,($D.isLeapYear(year)?29:28),31,30,31,30,31,31,30,31,30,31][month];};$D.getTimezoneAbbreviation=function(offset){var z=$C.timezones,p;for(var i=0;i&lt;z.length;i++){if(z[i].offset===offset){return z[i].name;}}<br></td></tr
><tr
id=sl_svn191_13><td class="source">return null;};$D.getTimezoneOffset=function(name){var z=$C.timezones,p;for(var i=0;i&lt;z.length;i++){if(z[i].name===name.toUpperCase()){return z[i].offset;}}<br></td></tr
><tr
id=sl_svn191_14><td class="source">return null;};$P.clone=function(){return new Date(this.getTime());};$P.compareTo=function(date){return Date.compare(this,date);};$P.equals=function(date){return Date.equals(this,date||new Date());};$P.between=function(start,end){return this.getTime()&gt;=start.getTime()&amp;&amp;this.getTime()&lt;=end.getTime();};$P.isAfter=function(date){return this.compareTo(date||new Date())===1;};$P.isBefore=function(date){return(this.compareTo(date||new Date())===-1);};$P.isToday=function(){return this.isSameDay(new Date());};$P.isSameDay=function(date){return this.clone().clearTime().equals(date.clone().clearTime());};$P.addMilliseconds=function(value){this.setMilliseconds(this.getMilliseconds()+value);return this;};$P.addSeconds=function(value){return this.addMilliseconds(value*1000);};$P.addMinutes=function(value){return this.addMilliseconds(value*60000);};$P.addHours=function(value){return this.addMilliseconds(value*3600000);};$P.addDays=function(value){this.setDate(this.getDate()+value);return this;};$P.addWeeks=function(value){return this.addDays(value*7);};$P.addMonths=function(value){var n=this.getDate();this.setDate(1);this.setMonth(this.getMonth()+value);this.setDate(Math.min(n,$D.getDaysInMonth(this.getFullYear(),this.getMonth())));return this;};$P.addYears=function(value){return this.addMonths(value*12);};$P.add=function(config){if(typeof config==&quot;number&quot;){this._orient=config;return this;}<br></td></tr
><tr
id=sl_svn191_15><td class="source">var x=config;if(x.milliseconds){this.addMilliseconds(x.milliseconds);}<br></td></tr
><tr
id=sl_svn191_16><td class="source">if(x.seconds){this.addSeconds(x.seconds);}<br></td></tr
><tr
id=sl_svn191_17><td class="source">if(x.minutes){this.addMinutes(x.minutes);}<br></td></tr
><tr
id=sl_svn191_18><td class="source">if(x.hours){this.addHours(x.hours);}<br></td></tr
><tr
id=sl_svn191_19><td class="source">if(x.weeks){this.addWeeks(x.weeks);}<br></td></tr
><tr
id=sl_svn191_20><td class="source">if(x.months){this.addMonths(x.months);}<br></td></tr
><tr
id=sl_svn191_21><td class="source">if(x.years){this.addYears(x.years);}<br></td></tr
><tr
id=sl_svn191_22><td class="source">if(x.days){this.addDays(x.days);}<br></td></tr
><tr
id=sl_svn191_23><td class="source">return this;};var $y,$m,$d;$P.getWeek=function(){var a,b,c,d,e,f,g,n,s,w;$y=(!$y)?this.getFullYear():$y;$m=(!$m)?this.getMonth()+1:$m;$d=(!$d)?this.getDate():$d;if($m&lt;=2){a=$y-1;b=(a/4|0)-(a/100|0)+(a/400|0);c=((a-1)/4|0)-((a-1)/100|0)+((a-1)/400|0);s=b-c;e=0;f=$d-1+(31*($m-1));}else{a=$y;b=(a/4|0)-(a/100|0)+(a/400|0);c=((a-1)/4|0)-((a-1)/100|0)+((a-1)/400|0);s=b-c;e=s+1;f=$d+((153*($m-3)+2)/5)+58+s;}<br></td></tr
><tr
id=sl_svn191_24><td class="source">g=(a+b)%7;d=(f+g-e)%7;n=(f+3-d)|0;if(n&lt;0){w=53-((g-s)/5|0);}else if(n&gt;364+s){w=1;}else{w=(n/7|0)+1;}<br></td></tr
><tr
id=sl_svn191_25><td class="source">$y=$m=$d=null;return w;};$P.getISOWeek=function(){$y=this.getUTCFullYear();$m=this.getUTCMonth()+1;$d=this.getUTCDate();return p(this.getWeek());};$P.setWeek=function(n){return this.moveToDayOfWeek(1).addWeeks(n-this.getWeek());};$D._validate=function(n,min,max,name){if(typeof n==&quot;undefined&quot;){return false;}else if(typeof n!=&quot;number&quot;){throw new TypeError(n+&quot; is not a Number.&quot;);}else if(n&lt;min||n&gt;max){throw new RangeError(n+&quot; is not a valid value for &quot;+name+&quot;.&quot;);}<br></td></tr
><tr
id=sl_svn191_26><td class="source">return true;};$D.validateMillisecond=function(value){return $D._validate(value,0,999,&quot;millisecond&quot;);};$D.validateSecond=function(value){return $D._validate(value,0,59,&quot;second&quot;);};$D.validateMinute=function(value){return $D._validate(value,0,59,&quot;minute&quot;);};$D.validateHour=function(value){return $D._validate(value,0,23,&quot;hour&quot;);};$D.validateDay=function(value,year,month){return $D._validate(value,1,$D.getDaysInMonth(year,month),&quot;day&quot;);};$D.validateMonth=function(value){return $D._validate(value,0,11,&quot;month&quot;);};$D.validateYear=function(value){return $D._validate(value,0,9999,&quot;year&quot;);};$P.set=function(config){if($D.validateMillisecond(config.millisecond)){this.addMilliseconds(config.millisecond-this.getMilliseconds());}<br></td></tr
><tr
id=sl_svn191_27><td class="source">if($D.validateSecond(config.second)){this.addSeconds(config.second-this.getSeconds());}<br></td></tr
><tr
id=sl_svn191_28><td class="source">if($D.validateMinute(config.minute)){this.addMinutes(config.minute-this.getMinutes());}<br></td></tr
><tr
id=sl_svn191_29><td class="source">if($D.validateHour(config.hour)){this.addHours(config.hour-this.getHours());}<br></td></tr
><tr
id=sl_svn191_30><td class="source">if($D.validateMonth(config.month)){this.addMonths(config.month-this.getMonth());}<br></td></tr
><tr
id=sl_svn191_31><td class="source">if($D.validateYear(config.year)){this.addYears(config.year-this.getFullYear());}<br></td></tr
><tr
id=sl_svn191_32><td class="source">if($D.validateDay(config.day,this.getFullYear(),this.getMonth())){this.addDays(config.day-this.getDate());}<br></td></tr
><tr
id=sl_svn191_33><td class="source">if(config.timezone){this.setTimezone(config.timezone);}<br></td></tr
><tr
id=sl_svn191_34><td class="source">if(config.timezoneOffset){this.setTimezoneOffset(config.timezoneOffset);}<br></td></tr
><tr
id=sl_svn191_35><td class="source">if(config.week&amp;&amp;$D._validate(config.week,0,53,&quot;week&quot;)){this.setWeek(config.week);}<br></td></tr
><tr
id=sl_svn191_36><td class="source">return this;};$P.moveToFirstDayOfMonth=function(){return this.set({day:1});};$P.moveToLastDayOfMonth=function(){return this.set({day:$D.getDaysInMonth(this.getFullYear(),this.getMonth())});};$P.moveToNthOccurrence=function(dayOfWeek,occurrence){var shift=0;if(occurrence&gt;0){shift=occurrence-1;}<br></td></tr
><tr
id=sl_svn191_37><td class="source">else if(occurrence===-1){this.moveToLastDayOfMonth();if(this.getDay()!==dayOfWeek){this.moveToDayOfWeek(dayOfWeek,-1);}<br></td></tr
><tr
id=sl_svn191_38><td class="source">return this;}<br></td></tr
><tr
id=sl_svn191_39><td class="source">return this.moveToFirstDayOfMonth().addDays(-1).moveToDayOfWeek(dayOfWeek,+1).addWeeks(shift);};$P.moveToDayOfWeek=function(dayOfWeek,orient){var diff=(dayOfWeek-this.getDay()+7*(orient||+1))%7;return this.addDays((diff===0)?diff+=7*(orient||+1):diff);};$P.moveToMonth=function(month,orient){var diff=(month-this.getMonth()+12*(orient||+1))%12;return this.addMonths((diff===0)?diff+=12*(orient||+1):diff);};$P.getOrdinalNumber=function(){return Math.ceil((this.clone().clearTime()-new Date(this.getFullYear(),0,1))/86400000)+1;};$P.getTimezone=function(){return $D.getTimezoneAbbreviation(this.getUTCOffset());};$P.setTimezoneOffset=function(offset){var here=this.getTimezoneOffset(),there=Number(offset)*-6/10;return this.addMinutes(there-here);};$P.setTimezone=function(offset){return this.setTimezoneOffset($D.getTimezoneOffset(offset));};$P.hasDaylightSavingTime=function(){return(Date.today().set({month:0,day:1}).getTimezoneOffset()!==Date.today().set({month:6,day:1}).getTimezoneOffset());};$P.isDaylightSavingTime=function(){return(this.hasDaylightSavingTime()&amp;&amp;new Date().getTimezoneOffset()===Date.today().set({month:6,day:1}).getTimezoneOffset());};$P.getUTCOffset=function(){var n=this.getTimezoneOffset()*-10/6,r;if(n&lt;0){r=(n-10000).toString();return r.charAt(0)+r.substr(2);}else{r=(n+10000).toString();return&quot;+&quot;+r.substr(1);}};$P.getElapsed=function(date){return(date||new Date())-this;};if(!$P.toISOString){$P.toISOString=function(){function f(n){return n&lt;10?&#39;0&#39;+n:n;}<br></td></tr
><tr
id=sl_svn191_40><td class="source">return&#39;&quot;&#39;+this.getUTCFullYear()+&#39;-&#39;+<br></td></tr
><tr
id=sl_svn191_41><td class="source">f(this.getUTCMonth()+1)+&#39;-&#39;+<br></td></tr
><tr
id=sl_svn191_42><td class="source">f(this.getUTCDate())+&#39;T&#39;+<br></td></tr
><tr
id=sl_svn191_43><td class="source">f(this.getUTCHours())+&#39;:&#39;+<br></td></tr
><tr
id=sl_svn191_44><td class="source">f(this.getUTCMinutes())+&#39;:&#39;+<br></td></tr
><tr
id=sl_svn191_45><td class="source">f(this.getUTCSeconds())+&#39;Z&quot;&#39;;};}<br></td></tr
><tr
id=sl_svn191_46><td class="source">$P._toString=$P.toString;$P.toString=function(format){var x=this;if(format&amp;&amp;format.length==1){var c=$C.formatPatterns;x.t=x.toString;switch(format){case&quot;d&quot;:return x.t(c.shortDate);case&quot;D&quot;:return x.t(c.longDate);case&quot;F&quot;:return x.t(c.fullDateTime);case&quot;m&quot;:return x.t(c.monthDay);case&quot;r&quot;:return x.t(c.rfc1123);case&quot;s&quot;:return x.t(c.sortableDateTime);case&quot;t&quot;:return x.t(c.shortTime);case&quot;T&quot;:return x.t(c.longTime);case&quot;u&quot;:return x.t(c.universalSortableDateTime);case&quot;y&quot;:return x.t(c.yearMonth);}}<br></td></tr
><tr
id=sl_svn191_47><td class="source">var ord=function(n){switch(n*1){case 1:case 21:case 31:return&quot;st&quot;;case 2:case 22:return&quot;nd&quot;;case 3:case 23:return&quot;rd&quot;;default:return&quot;th&quot;;}};return format?format.replace(/(\\)?(dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|S)/g,function(m){if(m.charAt(0)===&quot;\\&quot;){return m.replace(&quot;\\&quot;,&quot;&quot;);}<br></td></tr
><tr
id=sl_svn191_48><td class="source">x.h=x.getHours;switch(m){case&quot;hh&quot;:return p(x.h()&lt;13?(x.h()===0?12:x.h()):(x.h()-12));case&quot;h&quot;:return x.h()&lt;13?(x.h()===0?12:x.h()):(x.h()-12);case&quot;HH&quot;:return p(x.h());case&quot;H&quot;:return x.h();case&quot;mm&quot;:return p(x.getMinutes());case&quot;m&quot;:return x.getMinutes();case&quot;ss&quot;:return p(x.getSeconds());case&quot;s&quot;:return x.getSeconds();case&quot;yyyy&quot;:return p(x.getFullYear(),4);case&quot;yy&quot;:return p(x.getFullYear());case&quot;dddd&quot;:return $C.dayNames[x.getDay()];case&quot;ddd&quot;:return $C.abbreviatedDayNames[x.getDay()];case&quot;dd&quot;:return p(x.getDate());case&quot;d&quot;:return x.getDate();case&quot;MMMM&quot;:return $C.monthNames[x.getMonth()];case&quot;MMM&quot;:return $C.abbreviatedMonthNames[x.getMonth()];case&quot;MM&quot;:return p((x.getMonth()+1));case&quot;M&quot;:return x.getMonth()+1;case&quot;t&quot;:return x.h()&lt;12?$C.amDesignator.substring(0,1):$C.pmDesignator.substring(0,1);case&quot;tt&quot;:return x.h()&lt;12?$C.amDesignator:$C.pmDesignator;case&quot;S&quot;:return ord(x.getDate());default:return m;}}):this._toString();};}());<br></td></tr
></table></pre>

<pre class="prettyprint"><table width="100%"><tr class="cursor_stop cursor_hidden"><td></td></tr></table></pre>
</td>
</tr></table>



 <div id="log">
 <div style="text-align:right">
 <a class="ifCollapse" href="#" onclick="_toggleMeta('', 'p', 'datejs', this)">Show details</a>
 <a class="ifExpand" href="#" onclick="_toggleMeta('', 'p', 'datejs', this)">Hide details</a>
 </div>
 <div class="ifExpand">
 
 <div class="pmeta_bubble_bg" style="border:1px solid white">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <div id="changelog">
 <p>Change log</p>
 <div>
 <a href="/p/datejs/source/detail?spec=svn195&r=191">r191</a>
 by ge...@coolite.com
 on May 13, 2008
 &nbsp; <a href="/p/datejs/source/diff?spec=svn195&r=191&amp;format=side&amp;path=/trunk/build/core.js&amp;old_path=/trunk/build/core.js&amp;old=190">Diff</a>
 </div>
 <pre>--------------------

2008-05-12 [geoffrey.mcgill]
<a href="/p/datejs/source/detail?r=191">Revision #191</a>

1.  Added .same() function to sugarpak.js.
The new .same() function will compare two
date objects to
        determine if they occur on/in
exactly the same instance of the given
date part.

...</pre>
 </div>
 
 
 
 
 
 
 <script type="text/javascript">
 var detail_url = '/p/datejs/source/detail?r=191&spec=svn195';
 var publish_url = '/p/datejs/source/detail?r=191&spec=svn195#publish';
 // describe the paths of this revision in javascript.
 var changed_paths = [];
 var changed_urls = [];
 
 changed_paths.push('/trunk/CHANGELOG.txt');
 changed_urls.push('/p/datejs/source/browse/trunk/CHANGELOG.txt?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/core.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/core.js?r=191&spec=svn195');
 
 var selected_path = '/trunk/build/core.js';
 
 
 changed_paths.push('/trunk/build/date-af-ZA.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-af-ZA.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-AE.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-AE.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-BH.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-BH.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-DZ.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-DZ.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-KW.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-KW.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-LY.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-LY.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-OM.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-OM.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-SA.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-SA.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-TN.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-TN.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-YE.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-YE.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-az-Cyrl-AZ.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-az-Cyrl-AZ.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-be-BY.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-be-BY.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-bg-BG.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-bg-BG.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-bs-Latn-BA.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-bs-Latn-BA.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-cs-CZ.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-cs-CZ.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-da-DK.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-da-DK.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-de-CH.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-de-CH.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-de-LI.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-de-LI.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-dv-MV.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-dv-MV.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-029.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-029.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-BZ.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-BZ.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-CA.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-CA.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-JM.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-JM.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-NZ.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-NZ.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-PH.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-PH.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-TT.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-TT.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-US.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-US.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-ZA.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-ZA.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-ZW.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-ZW.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-AR.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-AR.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-CL.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-CL.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-CO.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-CO.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-CR.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-CR.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-DO.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-DO.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-EC.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-EC.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-ES.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-ES.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-GT.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-GT.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-HN.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-HN.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-MX.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-MX.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-PE.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-PE.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-PY.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-PY.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-SV.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-SV.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-UY.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-UY.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-et-EE.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-et-EE.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-eu-ES.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-eu-ES.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-fi-FI.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-fi-FI.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-fr-CA.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-fr-CA.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-fr-FR.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-fr-FR.js?r=191&spec=svn195');
 
 
 function getCurrentPageIndex() {
 for (var i = 0; i < changed_paths.length; i++) {
 if (selected_path == changed_paths[i]) {
 return i;
 }
 }
 }
 function getNextPage() {
 var i = getCurrentPageIndex();
 if (i < changed_paths.length - 1) {
 return changed_urls[i + 1];
 }
 return null;
 }
 function getPreviousPage() {
 var i = getCurrentPageIndex();
 if (i > 0) {
 return changed_urls[i - 1];
 }
 return null;
 }
 function gotoNextPage() {
 var page = getNextPage();
 if (!page) {
 page = detail_url;
 }
 window.location = page;
 }
 function gotoPreviousPage() {
 var page = getPreviousPage();
 if (!page) {
 page = detail_url;
 }
 window.location = page;
 }
 function gotoDetailPage() {
 window.location = detail_url;
 }
 function gotoPublishPage() {
 window.location = publish_url;
 }
</script>
 
 <style type="text/css">
 #review_nav {
 border-top: 3px solid white;
 padding-top: 6px;
 margin-top: 1em;
 }
 #review_nav td {
 vertical-align: middle;
 }
 #review_nav select {
 margin: .5em 0;
 }
 </style>
 <div id="review_nav">
 <table><tr><td>Go to:&nbsp;</td><td>
 <select name="files_in_rev" onchange="window.location=this.value">
 
 <option value="/p/datejs/source/browse/trunk/CHANGELOG.txt?r=191&amp;spec=svn195"
 
 >/trunk/CHANGELOG.txt</option>
 
 <option value="/p/datejs/source/browse/trunk/build/core.js?r=191&amp;spec=svn195"
 selected="selected"
 >/trunk/build/core.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-af-ZA.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-af-ZA.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-AE.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-AE.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-BH.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-BH.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-DZ.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-DZ.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-KW.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-KW.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-LY.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-LY.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-OM.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-OM.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-SA.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-SA.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-TN.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-TN.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-YE.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-YE.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-az-Cyrl-AZ.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-az-Cyrl-AZ.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-be-BY.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-be-BY.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-bg-BG.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-bg-BG.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-bs-Latn-BA.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-bs-Latn-BA.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-cs-CZ.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-cs-CZ.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-da-DK.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-da-DK.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-de-CH.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-de-CH.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-de-LI.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-de-LI.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-dv-MV.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-dv-MV.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-029.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-029.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-BZ.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-BZ.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-CA.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-CA.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-JM.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-JM.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-NZ.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-NZ.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-PH.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-PH.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-TT.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-TT.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-US.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-US.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-ZA.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-ZA.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-ZW.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-ZW.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-AR.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-AR.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-CL.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-CL.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-CO.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-CO.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-CR.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-CR.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-DO.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-DO.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-EC.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-EC.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-ES.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-ES.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-GT.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-GT.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-HN.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-HN.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-MX.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-MX.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-PE.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-PE.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-PY.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-PY.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-SV.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-SV.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-UY.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-UY.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-et-EE.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-et-EE.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-eu-ES.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-eu-ES.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-fi-FI.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-fi-FI.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-fr-CA.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-fr-CA.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-fr-FR.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-fr-FR.js</option>
 
 </select>
 </td></tr></table>
 
 
 




 
 </div>
 
 
 </div>
 <div class="round1"></div>
 <div class="round2"></div>
 <div class="round4"></div>
 </div>
 <div class="pmeta_bubble_bg" style="border:1px solid white">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <div id="older_bubble">
 <p>Older revisions</p>
 
 
 <div class="closed" style="margin-bottom:3px;" >
 <img class="ifClosed" onclick="_toggleHidden(this)" src="http://www.gstatic.com/codesite/ph/images/plus.gif" >
 <img class="ifOpened" onclick="_toggleHidden(this)" src="http://www.gstatic.com/codesite/ph/images/minus.gif" >
 <a href="/p/datejs/source/detail?spec=svn195&r=190">r190</a>
 by ge...@coolite.com
 on May 12, 2008
 &nbsp; <a href="/p/datejs/source/diff?spec=svn195&r=190&amp;format=side&amp;path=/trunk/build/core.js&amp;old_path=/trunk/build/core.js&amp;old=189">Diff</a>
 <br>
 <pre class="ifOpened">--------------------
2008-05-12 [geoffrey.mcgill]
<a href="/p/datejs/source/detail?r=190">Revision #190</a>

1.  Added .today() equality check
...</pre>
 </div>
 
 <div class="closed" style="margin-bottom:3px;" >
 <img class="ifClosed" onclick="_toggleHidden(this)" src="http://www.gstatic.com/codesite/ph/images/plus.gif" >
 <img class="ifOpened" onclick="_toggleHidden(this)" src="http://www.gstatic.com/codesite/ph/images/minus.gif" >
 <a href="/p/datejs/source/detail?spec=svn195&r=189">r189</a>
 by ge...@coolite.com
 on May 07, 2008
 &nbsp; <a href="/p/datejs/source/diff?spec=svn195&r=189&amp;format=side&amp;path=/trunk/build/core.js&amp;old_path=/trunk/build/core.js&amp;old=187">Diff</a>
 <br>
 <pre class="ifOpened">--------------------
2008-05-07 [geoffrey.mcgill]
<a href="/p/datejs/source/detail?r=189">Revision #189</a>

1.  Fixed bug in TimePeriod. See http:
...</pre>
 </div>
 
 <div class="closed" style="margin-bottom:3px;" >
 <img class="ifClosed" onclick="_toggleHidden(this)" src="http://www.gstatic.com/codesite/ph/images/plus.gif" >
 <img class="ifOpened" onclick="_toggleHidden(this)" src="http://www.gstatic.com/codesite/ph/images/minus.gif" >
 <a href="/p/datejs/source/detail?spec=svn195&r=187">r187</a>
 by ge...@coolite.com
 on May 05, 2008
 &nbsp; <a href="/p/datejs/source/diff?spec=svn195&r=187&amp;format=side&amp;path=/trunk/build/core.js&amp;old_path=/trunk/build/core.js&amp;old=0">Diff</a>
 <br>
 <pre class="ifOpened">[No log message]</pre>
 </div>
 
 
 <a href="/p/datejs/source/list?path=/trunk/build/core.js&start=191">All revisions of this file</a>
 </div>
 </div>
 <div class="round1"></div>
 <div class="round2"></div>
 <div class="round4"></div>
 </div>
 <div class="pmeta_bubble_bg" style="border:1px solid white">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <div id="fileinfo_bubble">
 <p>File info</p>
 
 <div>Size: 10066 bytes,
 48 lines</div>
 
 <div><a href="http://datejs.googlecode.com/svn/trunk/build/core.js">View raw file</a></div>
 </div>
 
 </div>
 <div class="round1"></div>
 <div class="round2"></div>
 <div class="round4"></div>
 </div>
 </div>
 </div>


</div>
</div>

 <script src="http://www.gstatic.com/codesite/ph/1676283234471223295/js/prettify/prettify.js"></script>

<script type="text/javascript">prettyPrint();</script>

<script src="http://www.gstatic.com/codesite/ph/1676283234471223295/js/source_file_scripts.js"></script>

 <script type="text/javascript" src="http://kibbles.googlecode.com/files/kibbles-1.3.1.comp.js"></script>
 <script type="text/javascript">
 var lastStop = null;
 var initilized = false;
 
 function updateCursor(next, prev) {
 if (prev && prev.element) {
 prev.element.className = 'cursor_stop cursor_hidden';
 }
 if (next && next.element) {
 next.element.className = 'cursor_stop cursor';
 lastStop = next.index;
 }
 }
 
 function pubRevealed(data) {
 updateCursorForCell(data.cellId, 'cursor_stop cursor_hidden');
 if (initilized) {
 reloadCursors();
 }
 }
 
 function draftRevealed(data) {
 updateCursorForCell(data.cellId, 'cursor_stop cursor_hidden');
 if (initilized) {
 reloadCursors();
 }
 }
 
 function draftDestroyed(data) {
 updateCursorForCell(data.cellId, 'nocursor');
 if (initilized) {
 reloadCursors();
 }
 }
 function reloadCursors() {
 kibbles.skipper.reset();
 loadCursors();
 if (lastStop != null) {
 kibbles.skipper.setCurrentStop(lastStop);
 }
 }
 // possibly the simplest way to insert any newly added comments
 // is to update the class of the corresponding cursor row,
 // then refresh the entire list of rows.
 function updateCursorForCell(cellId, className) {
 var cell = document.getElementById(cellId);
 // we have to go two rows back to find the cursor location
 var row = getPreviousElement(cell.parentNode);
 row.className = className;
 }
 // returns the previous element, ignores text nodes.
 function getPreviousElement(e) {
 var element = e.previousSibling;
 if (element.nodeType == 3) {
 element = element.previousSibling;
 }
 if (element && element.tagName) {
 return element;
 }
 }
 function loadCursors() {
 // register our elements with skipper
 var elements = CR_getElements('*', 'cursor_stop');
 var len = elements.length;
 for (var i = 0; i < len; i++) {
 var element = elements[i]; 
 element.className = 'cursor_stop cursor_hidden';
 kibbles.skipper.append(element);
 }
 }
 function toggleComments() {
 CR_toggleCommentDisplay();
 reloadCursors();
 }
 function keysOnLoadHandler() {
 // setup skipper
 kibbles.skipper.addStopListener(
 kibbles.skipper.LISTENER_TYPE.PRE, updateCursor);
 // Set the 'offset' option to return the middle of the client area
 // an option can be a static value, or a callback
 kibbles.skipper.setOption('padding_top', 50);
 // Set the 'offset' option to return the middle of the client area
 // an option can be a static value, or a callback
 kibbles.skipper.setOption('padding_bottom', 100);
 // Register our keys
 kibbles.skipper.addFwdKey("n");
 kibbles.skipper.addRevKey("p");
 kibbles.keys.addKeyPressListener(
 'u', function() { window.location = detail_url; });
 kibbles.keys.addKeyPressListener(
 'r', function() { window.location = detail_url + '#publish'; });
 
 kibbles.keys.addKeyPressListener('j', gotoNextPage);
 kibbles.keys.addKeyPressListener('k', gotoPreviousPage);
 
 
 }
 window.onload = function() {keysOnLoadHandler();};
 </script>


<!-- code review support -->
<script src="http://www.gstatic.com/codesite/ph/1676283234471223295/js/code_review_scripts.js"></script>
<script type="text/javascript">
 
 // the comment form template
 var form = '<div class="draft"><div class="header"><span class="title">Draft comment:</span></div>' +
 '<div class="body"><form onsubmit="return false;"><textarea id="$ID">$BODY</textarea><br>$ACTIONS</form></div>' +
 '</div>';
 // the comment "plate" template used for both draft and published comment "plates".
 var draft_comment = '<div class="draft" ondblclick="$ONDBLCLICK">' +
 '<div class="header"><span class="title">Draft comment:</span><span class="actions">$ACTIONS</span></div>' +
 '<pre id="$ID" class="body">$BODY</pre>' +
 '</div>';
 var published_comment = '<div class="published">' +
 '<div class="header"><span class="title"><a href="$PROFILE_URL">$AUTHOR:</a></span><div>' +
 '<pre id="$ID" class="body">$BODY</pre>' +
 '</div>';

 function showPublishInstructions() {
 var element = document.getElementById('review_instr');
 if (element) {
 element.className = 'opened';
 }
 }
 function revsOnLoadHandler() {
 // register our source container with the commenting code
 var paths = {'svn191': '/trunk/build/core.js'}
 CR_setup('', 'p', 'datejs', '', 'svn195', paths,
 '125d90c7016dbfd9c828c03b95ed61d0', CR_BrowseIntegrationFactory);
 // register our hidden ui elements with the code commenting code ui builder.
 CR_registerLayoutElement('form', form);
 CR_registerLayoutElement('draft_comment', draft_comment);
 CR_registerLayoutElement('published_comment', published_comment);
 
 CR_registerActivityListener(CR_ACTIVITY_TYPE.REVEAL_DRAFT_PLATE, showPublishInstructions);
 
 CR_registerActivityListener(CR_ACTIVITY_TYPE.REVEAL_PUB_PLATE, pubRevealed);
 CR_registerActivityListener(CR_ACTIVITY_TYPE.REVEAL_DRAFT_PLATE, draftRevealed);
 CR_registerActivityListener(CR_ACTIVITY_TYPE.DISCARD_DRAFT_COMMENT, draftDestroyed);
 
 
 
 
 
 
 
 
 
 var initilized = true;
 reloadCursors();
 }
 window.onload = function() {keysOnLoadHandler(); revsOnLoadHandler();};
</script>

<script type="text/javascript" src="http://www.gstatic.com/codesite/ph/1676283234471223295/js/dit_scripts_20081013.js"></script>

 
 <script type="text/javascript" src="http://www.gstatic.com/codesite/ph/1676283234471223295/js/core_scripts_20081103.js"></script>
 <script type="text/javascript" src="/js/codesite_product_dictionary_ph.pack.04102009.js"></script>
 
 
 
 
 
 </div>
<div id="footer" dir="ltr">
 
 <div class="text">
 
 &copy;2009 Google -
 <a href="/">Code Home</a> -
 <a href="/projecthosting/terms.html">Terms of Service</a> -
 <a href="http://www.google.com/privacy.html">Privacy Policy</a> -
 <a href="/more/">Site Directory</a> -
 <a href="/p/support/">Project Hosting Help</a>
 
 </div>
</div>
<script type="text/javascript">
/**
 * Reports analytics.
 * It checks for the analytics functionality (window._gat) every 100ms
 * until the analytics script is fully loaded in order to invoke siteTracker.
 */
function _CS_reportAnalytics() {
 window.setTimeout(function() {
 if (window._gat) {
 try {
 siteTracker = _gat._getTracker(CS_ANALYTICS_ACCOUNT);
 siteTracker._trackPageview();
 } catch (e) {}
 var projectTracker = _gat._getTracker("UA-2699273-4");
projectTracker._initData();
projectTracker._trackPageview();
 } else {
 _CS_reportAnalytics();
 }
 }, 100);
}
</script>

 
 
 <div class="hostedBy" style="margin-top: -20px;">
 <span style="vertical-align: top;">Hosted by</span>
 <a href="/hosting/">
 <img src="http://www.gstatic.com/codesite/ph/images/google_code_tiny.png" width="107" height="24" alt="Google Code">
 </a>
 </div>
 
 
 
 


 
 </body>
</html>


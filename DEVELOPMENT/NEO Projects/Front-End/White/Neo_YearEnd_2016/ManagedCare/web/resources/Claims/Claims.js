function ValidateAndSaveClaimHeader() {
    
}

function selectClaimProvider(ProviderNumber, message, isError, providerCategory) {
    setClaimProviderFields(ProviderNumber, message, isError, providerCategory);
    swapDivVisbible('Search_Provider_Div', 'Main_Claim_Div');
}

function setClaimProviderFields(ProviderNumber, message, isError, providerCategory) {
    $('#ClaimHeader_ProviderNumber').val(ProviderNumber);
    $('#ClaimHeader_ProviderNumber_error').text(message);
    $('#ClaimHeader_ProviderNumber_error').removeClass();
    $('#ClaimHeader_Category').val(providerCategory);
    if (isError) {
        $('#ClaimHeader_ProviderNumber_error').addClass('error');
    }
}

function selectClaimRefProvider(ProviderNumber, message, isError) {
    setClaimRefProviderFields(ProviderNumber, message, isError);
    swapDivVisbible('Search_Ref_Provider_Div', 'Main_Claim_Div');
}

function setClaimRefProviderFields(ProviderNumber, message, isError) {
    $('#ClaimHeader_ReferringProviderNumber').val(ProviderNumber);
    $('#ClaimHeader_ReferringProviderNumber_error').text(message);
    $('#ClaimHeader_ReferringProviderNumber_error').removeClass();
    if (isError) {
        $('#ClaimHeader_ReferringProviderNumber_error').addClass('error');
    }
}

function selectClaimPractice(ProviderNumber, message, isError) {
    setClaimPracticeFields(ProviderNumber, message, isError);
    swapDivVisbible('Search_Practice_Div', 'Main_Claim_Div');
}

function selectClaimMember(MemberNumber, message, isError) {
    setClaimMemberFields(MemberNumber, message, isError);
    swapDivVisbible('Search_Member_Div', 'Main_Claim_Div');
}

function setClaimPracticeFields(ProviderNumber, message, isError) {
    $('#ClaimHeader_PracticeNumber').val(ProviderNumber);
    $('#ClaimHeader_PracticeNumber_error').text(message);
    $('#ClaimHeader_PracticeNumber_error').removeClass();
    if (isError) {
        $('#ClaimHeader_PracticeNumber_error').addClass('error');
    }
}
function setClaimMemberFields(MemberNumber, message, isError) {
    $('#ClaimHeader_CoverNumber').val(MemberNumber);
    $('#ClaimHeader_CoverNumber_error').text(message);
    $('#ClaimHeader_CoverNumber_error').removeClass();
    if (isError) {
        $('#ClaimHeader_CoverNumber_error').addClass('error');
    }
}

function findClaimCoverNumber(coverNumber) {
    if (coverNumber == "") {
        $('#ClaimHeader_CoverNumber_error').text('Cover Number Required ');
        $('#ClaimHeader_CoverNumber_error').addClass('error');
    } else {
        var url = "/ManagedCare/AgileController?opperation=ClaimHeaderCommand&command=findClaimCoverNumber&coverNumber=" + coverNumber;
        $.get(url, function(data) {
            $('#ClaimHeader_CoverNumber_error').removeClass();
            if (data != '') {
                var dataArr = data.split("|");
                $('#ClaimHeader_CoverNumber').val(dataArr[0]);
                $('#ClaimHeader_CoverNumber_error').text(dataArr[1]);
            } else {
                $('#ClaimHeader_CoverNumber_error').text('Invalid Cover Number: ' + coverNumber);
                $('#ClaimHeader_CoverNumber_error').addClass('error');
                $('#ClaimHeader_CoverNumber').val('');
            }
        });
    }
}

function findClaimPracticeNumber(practiceNumber) {
    if (practiceNumber == "") {
        $('#ClaimHeader_PracticeNumber_error').text('Practice Required ');
        $('#ClaimHeader_PracticeNumber_error').addClass('error');
    } else {
        var url = "/ManagedCare/AgileController?opperation=ClaimHeaderCommand&command=findClaimPractice&practiceNumber=" + practiceNumber;
        $.get(url, function(data) {
            if (data == '') {
                setClaimPracticeFields('', 'Invalid Practice Number: ' + practiceNumber, true);
            } else {
                var dataArr = data.split("|");
                if(dataArr[0] === "Cash"){
                    $('#ClaimHeader_ReceiptBy').val(5).change();
                    $('#ClaimHeader_ReceiptBy').disabled = true;
                }
                setClaimPracticeFields(practiceNumber, dataArr[1], false);
            }
        });
    }
}

function findClaimProviderNumber(providerNumber) {
    if (providerNumber == "") {
        $('#ClaimHeader_ProviderNumber_error').text('Provider Number Required ');
        $('#ClaimHeader_ProviderNumber_error').addClass('error');
    } else {
        var url = "/ManagedCare/AgileController?opperation=ClaimHeaderCommand&command=findClaimProvider&providerNumber=" + providerNumber;
        $.get(url, function(data) {
            if (data == '') {
            setClaimProviderFields('', 'Invalid Provider Number: ' + providerNumber, true, '');
            } else {
                var res = data.split("||");
            setClaimProviderFields(providerNumber, res[0], false, res[1]);
            }
        });
    }
}

function findClaimRefProviderNumber(providerNumber) {
    if (providerNumber == "") {
        $('#ClaimHeader_ReferringProviderNumber_error').text('');
        $('#ClaimHeader_ReferringProviderNumber_error').addClass('error');
    } else {
        var url = "/ManagedCare/AgileController?opperation=ClaimHeaderCommand&command=findClaimProvider&providerNumber=" + providerNumber;
        $.get(url, function(data) {
            if (data == '') {
            setClaimRefProviderFields('', 'Invalid Provider Number: ' + providerNumber, true);
            } else {
                var res = data.split("||");
            setClaimRefProviderFields(providerNumber, res[0], false);
            }
        });
    }
}

function validateClaimHeader() {
    var valid = true;
    valid = valid && validateNeoDateField('ClaimHeader_DateReceived', true, null);
    if (!valid) {
        $('#ClaimHeader_DateReceived_error').addClass("error");
    }
    var res = true;;
    var amount = validateAmountField('ClaimHeader_ClaimTotal');
    if (amount == null){
        var value = $("#ClaimHeader_ClaimTotal").val(); 
        $('#ClaimHeader_ClaimTotal_error').text(value + " is an invalid amount");
        $('#ClaimHeader_ClaimTotal_error').addClass("error");
        $("#ClaimHeader_ClaimTotal").val("");
        res = false;
    } else {
        $("#ClaimHeader_ClaimTotal").val(amount);
        $('#ClaimHeader_ClaimTotal_error').text("");
        $('#ClaimHeader_ClaimTotal_error').removeClass();
    }
//    if (!res) {
//        var value = $("#ClaimHeader_ClaimTotal").val(); 
//        $('#ClaimHeader_ClaimTotal_error').text(value + " is an invalid amount")
//        $('#ClaimHeader_ClaimTotal_error').addClass("error");
//    }
    valid = valid && res;
    res = validateNeoRequiredField('ClaimHeader_AccountNumber');
    if (!res) {
        $('#ClaimHeader_AccountNumber_error').addClass("error");
    }
    valid = valid && res;
    if ($('#ClaimHeader_CoverNumber').val() == "") {
        $('#ClaimHeader_CoverNumber_error').text('Cover Number is required');
        $('#ClaimHeader_CoverNumber_error').addClass("error");
        valid = false;
    }
    if ($('#ClaimHeader_PracticeNumber').val() == "") {
        $('#ClaimHeader_PracticeNumber_error').text('Practice Number is required');
        $('#ClaimHeader_PracticeNumber_error').addClass("error");
        valid = false;
    }
    if ($('#ClaimHeader_ProviderNumber').val() == "") {
        $('#ClaimHeader_ProviderNumber_error').text('Provider Number is required');
        $('#ClaimHeader_ProviderNumber_error').addClass("error");
        valid = false;
    }
    return valid;
}

function saveClaimHeader(form, targetId, btn, update) {
    if (!validateClaimHeader()) {
        displayNeoErrorNotification('There are validation errors');
        return;
    }
    if (btn != undefined) {
        btn.disabled = true;
    }
    var params = encodeFormForSubmit(form);
    var formId = $(form).attr('id');
    //$(form).mask("Processing...");
    $.post(form.action, params,function(result){
        displayNeoSuccessNotification('Claim Header Saved');
        var providerNumber = $('#ClaimHeader_ProviderNumber').val();
        var coverNumber = $('#ClaimHeader_CoverNumber').val();
        resetClaimHeader();
        updateFormFromData(result, targetId);
        var batchId = $('#ClaimHeader_batchId').val();
        params="batchId=" + batchId;
        var url = "/ManagedCare/AgileController?opperation=ClaimHeaderCommand&command=fetchBatchAndClaims&batchId=" + batchId + "&update=" + update;
        $.post(url, function(data) {
            updateFormFromData(data, 'ClaimHeader_Summary');
        });
        var claimId = $('#ClaimHeader_claimId').val();
        var fileName = $('#ClaimHeader_ClaimDocument').val();
        uploadClaimDocument(providerNumber, coverNumber, batchId,claimId,fileName,"true");
        if (btn != undefined) {
            btn.disabled = false;
        }
        if ($('#' + formId).unmask != null) $(form).unmask();
        
        if(update == "true"){
            swapDivVisbible('Edit_Claims_Div', 'DetailedClaimLine_Div');
        }
    });
}

/*function changeClaimLineCategory(val) {
    alert("Claim Category = " + val);
    table_group_show_hide('ClaimLineTable_INV', (val === 8 || val === 9 || val === 10));
    table_group_show_hide('ClaimLineTable_Time', (val === 5 || val === 10 || val === 7));
    table_group_show_hide('ClaimLineTable_CPT', (val === 5 || val === 10));
    table_group_show_hide('ClaimLineTable_Dental', (val === 3 || val === 10));
    table_group_show_hide('ClaimLineTable_Optical', (val === 2 || val === 10));
}*/

function captureClaimLine(claimId, category, coverNumber, practiceTypeId, update, practiceNumber, providerNumber) {
    var new_url;
    new_url = "/ManagedCare/AgileController?opperation=ClaimLineCommand&command=NewClaimLine&claimId=" + claimId + "&category=" + category + "&coverNumber=" + coverNumber + "&update=" + update + "&practiceNumber=" + practiceNumber + "&providerNumber=" + providerNumber;
    if(update === "true"){
        $.get(new_url, function(response){
            $('#Edit_Claim_Lines_Div').empty().html(response);
            $('#Edit_Claim_Lines_Div').show();
            $('#Edit_Claims_Div').hide();
            $('#ClaimLine_Practice_Type').val(practiceTypeId);
        });
    } else {
        $.get(new_url, function(response){
            $('#Capture_Claim_Lines_Div').empty().html(response);
            $('#Capture_Claim_Lines_Div').show();
            $('#Capture_Claims_Div').hide();
            $('#ClaimLine_Practice_Type').val(practiceTypeId);
        });
    }
} 

function validateClaimLine() {
    var valid = true;
    var res = true;
    var icd = $("#ClaimLine_Diagnosis").val();
    var cpt = $("#ClaimLine_CPT").val();
    var tariff = $("#ClaimLine_Tariff").val();
    var category = $("#ClaimLine_Category").val();
    var timeIn = $("#ClaimLine_Time_In").val();
    var timeOut = $("#ClaimLine_Time_Out").val();
    document.getElementById("icdList").value = "";
    document.getElementById("icdRefList").value = "";
    document.getElementById("toothList").value = "";
    document.getElementById("cptList").value = "";
    
    /*if ($('#ClaimHeader_ProviderNumber').val() == "") {
        $('#ClaimHeader_ProviderNumber_error').text('Provider Number is required');
        $('#ClaimHeader_ProviderNumber_error').addClass("error");
        valid = false;
    }*/
    
    var $dd = $('#ClaimLine_Diagnosis_DropDown');
    var icdList = "";
    var ddlvalues;
//    if($dd.find('option').length < 1){
        //alert("empty");
//        $('#ClaimLine_Diagnosis_error').text('ICD code is required');
//        $('#ClaimLine_Diagnosis_error').addClass("error");
//        valid = false;
//    } else {
        ddlvalues = $('#ClaimLine_Diagnosis_DropDown option').map(function() {
            return $(this).val();
        });
        document.getElementById("icdList").value = ddlvalues.get();
//    }
    
    //if($('#ClaimLine_Diagnosis').val() == "") {
        //$('#ClaimLine_Diagnosis_error').text('ICD code is required');
        //$('#ClaimLine_Diagnosis_error').addClass("error");
       // valid = false;
   // }
    
    $dd = $("#ClaimLine_Ref_Diagnosis_DropDown");
    if($dd.find('option').length > 0){
        ddlvalues = $('#ClaimLine_Ref_Diagnosis_DropDown option').map(function() {
            return $(this).val();
        });
        document.getElementById("icdRefList").value = ddlvalues.get();
    }
    
    if($('#ClaimLine_Tariff').val() == "") {
        $('#ClaimLine_Tariff_error').text('Tariff code is required');
        $('#ClaimLine_Tariff_error').addClass("error");
        valid = false;
    }
    
    if(category == 8 || category == 9 || category == 10 ) {
        if($('#ClaimLine_Invoice_Number').val() == "") {
            $('#ClaimLine_Invoice_Number_error').text('Invoice Number is required');
            $('#ClaimLine_Invoice_Number_error').addClass("error");
            valid = false;
        }

        if($('#ClaimLine_Order_Number').val() == "") {
            $('#ClaimLine_Order_Number_error').text('Order Number is required');
            $('#ClaimLine_Order_Number_error').addClass("error");
            valid = false;
        }
    }
    
    //if(tariff == '' || tariff == null) {
    //    res = false;
    //    valid = valid && res;
    //}
    if(category == 2) {
        //alert($('#ClaimLine_Lens_Left').val());
        //alert($('#ClaimLine_Lens_Right').val());
        if($('#ClaimLine_Lens_Left').val() == ""){
            $('#ClaimLine_Lens_Left_error').text('Left Lense Value is required');
            $('#ClaimLine_Lens_Left_error').addClass("error");
            valid = false;
        }
        
        if($('#ClaimLine_Lens_Right').val() == ""){
            $('#ClaimLine_Lens_Right_error').text('Right Lense Value is required');
            $('#ClaimLine_Lens_Right_error').addClass("error");
            valid = false;
        }
    }
    
//    if (category == 8 || category == 3) {
        $dd = $('#ClaimLine_Tooth_Numbers_DropDown');
//        if($dd.find('option').length < 1){
//            //alert("empty");
//            $('#ClaimLine_Tooth_Numbers_error').text('Please enter a tooth number');
//            $('#ClaimLine_Tooth_Numbers_error').addClass("error");
//            valid = false;
//        } else {
            ddlvalues = $('#ClaimLine_Tooth_Numbers_DropDown option').map(function() {
                return $(this).val();
            });
            document.getElementById("toothList").value = ddlvalues.get();
//        }
//        
//        //if($('#ClaimLine_Tooth_Numbers').val() == ""){
//            //$('#ClaimLine_Tooth_Numbers_error').text('Please enter a tooth number');
//            //$('#ClaimLine_Tooth_Numbers_error').addClass("error");
//          //  valid = false;
//       // }
//     }
     
    if(category == 5 || category == 10) {
        $dd = $('#ClaimLine_CPT_DropDown');
        if($dd.find('option').length < 1){
            //alert("empty");
            $('#ClaimLine_CPT_error').text('CPT code is required');
            $('#ClaimLine_CPT_error').addClass("error");
            valid = false;
        } else {
            ddlvalues = $('#ClaimLine_CPT_DropDown option').map(function() {
                return $(this).val();
            });
            document.getElementById("cptList").value = ddlvalues.get();
        }
        
        //if($('#ClaimLine_CPT').val() == "") {
        //$('#ClaimLine_CPT_error').text('CPT code is required');
        //$('#ClaimLine_CPT_error').addClass("error");
        //valid = false;
        //}
    }
    
    if(category == 5) {
        /*if ($("#ClaimLine_Time_In").val() == "" ) {
            $('#ClaimLine_Time_In_error').text('Time In is required');
            $('#ClaimLine_Time_In_error').addClass("error");
            valid = false;
        } 
        
        if ($("#ClaimLine_Time_Out").val() == "") {
            $('#ClaimLine_Time_Out_error').text('Time Out is required');
            $('#ClaimLine_Time_Out_error').addClass("error");
            valid = false;
        }*/
        res = validateTime($("#ClaimLine_Time_In").val(),"ClaimLine_Time_In");
        valid = valid && res;
        res = validateTime($("#ClaimLine_Time_Out").val(),"ClaimLine_Time_Out");
        valid = valid && res;
        res = validateNeoDateField('ClaimLine_DateEnd', true, null);
        valid = valid && res;
    }
    
    var amount = validateAmountField('ClaimLine_Claimed_Amount');
    if (amount == null){
        var value = $("#ClaimLine_Claimed_Amount").val(); 
        $('#ClaimLine_Claimed_Amount_error').text(value + " is an invalid amount");
        $('#ClaimLine_Claimed_Amount_error').addClass("error");
        $("#ClaimLine_Claimed_Amount").val("");
        res = false;
    } else {
        $("#ClaimLine_Claimed_Amount").val(amount);
        $('#ClaimLine_Claimed_Amount_error').text("");
        $('#ClaimLine_Claimed_Amount_error').removeClass();
    }
    valid = valid && res;
    
    res = checkDateTo();
    valid = valid && res;
    
    res = validateNeoDateField('ClaimLine_DateStart', true, null);
    valid = valid && res;
//    res = validateNeoDateField('ClaimLine_DateEnd', true, null);
//    valid = valid && res;
    res =  validateNeoIntField('ClaimLine_Units', true);
    valid = valid && res;
    //res = validateNeoAmountField('ClaimLine_Claimed_Amount', true);
    //valid = valid && res;
    res = validateNeoIntField('ClaimLine_Multiplier', true);
    valid = valid && res;
    res = validateNeoIntField('ClaimLine_Quantity', true);
    valid = valid && res;
    return valid;
}

function saveClaimLine(form, targetId, btn,update) {
    if (!validateClaimLine()) {
        displayNeoErrorNotification('There are validation errors');
        return;
    }
    if (btn != undefined) {
        btn.disabled = true;
    }
    if (btn != undefined) {
        btn.disabled = true;
    }
    var params = encodeFormForSubmit(form);
    var formId = $(form).attr('id');
    //$(form).mask("Processing...");
    $.post(form.action, params,function(result){
        displayNeoSuccessNotification('Claim Line Saved');
        resetClaimLine();
        updateFormFromData(result, targetId);
        var claimId = $('#ClaimLine_claimId').val();

        
        var url = "/ManagedCare/AgileController?opperation=ClaimLineCommand&command=fetchClaimLines&claimId=" + claimId;
        $.post(url, function(data) {
            updateFormFromData(data, 'ClaimLine_Summary');
        });
        
        var batchId = $('#ClaimHeader_batchId').val();
        var url = "/ManagedCare/AgileController?opperation=ClaimHeaderCommand&command=fetchBatchAndClaims&batchId=" + batchId + "&update=" + update;
        $.post(url, function(data) {
            updateFormFromData(data, 'ClaimHeader_Summary');
        });
        
        if (btn != undefined) {
            btn.disabled = false;
        }
        if ($('#'+formId).unmask != null) $(form).unmask();
    });
}

function resetClaimHeader() {
    $('#ClaimHeader_CoverNumber').val('');
    $('#ClaimHeader_PracticeNumber').val('');
    $('#ClaimHeader_ProviderNumber').val('');
    $('#ClaimHeader_Category').val('');
    $('#ClaimHeader_AccountNumber').val('');
    $('#ClaimHeader_ReferringProviderNumber').val('');
    $('#ClaimHeader_ClaimTotal').val('');
    $('#ClaimHeader_ScanIndexNumber').val('');
    $('#ClaimHeader_CoverNumber_error').text('');
    $('#ClaimHeader_PracticeNumber_error').text('');
    $('#ClaimHeader_ProviderNumber_error').text('');
    $('#ClaimHeader_Category_error').text('');
    $('#ClaimHeader_AccountNumber_error').text('');
    $('#ClaimHeader_ReferringProviderNumber_error').text('');
    $('#ClaimHeader_ClaimTotal_error').text('');
    $('#ClaimHeader_ScanIndexNumber_error').text('');
    $('#ClaimHeader_ReceiptBy').disabled = false;
}

function resetClaimLine() {
    //$('#ClaimLine_DateStart').val('');
    //$('#ClaimLine_DateEnd').val('');
    $('#ClaimLine_Invoice_Number').val('');
    $('#ClaimLine_Order_Number').val('');
    //$('#ClaimLine_Time_In').val('');
    //$('#ClaimLine_Time_Out').val('');
    $('#ClaimLine_Diagnosis').val('');
    $('#ClaimLine_Ref_Diagnosis').val('');
    //$('#ClaimLine_CPT').val('');
    $('#ClaimLine_Tariff').val('');
    $('#ClaimLine_Multiplier').val('1');
    $('#ClaimLine_Units').val('1');
    $('#ClaimLine_Quantity').val('1');
    $('#ClaimLine_Authorization').val('');
    $('#ClaimLine_Tooth_Numbers').val('');
    $('#ClaimLine_Lens_Left').val('');
    $('#ClaimLine_Lens_Right').val('');
    $('#ClaimLine_Claimed_Amount').val('');
    $('#ClaimLine_Invoice_Number_error').text('');
    $('#ClaimLine_Order_Number_error').text('');
    $('#ClaimLine_Time_In_error').text('');
    $('#ClaimLine_Time_Out_error').text('');
    $('#ClaimLine_Diagnosis_error').text('');
    $('#ClaimLine_Ref_Diagnosis_error').text('');
    $('#ClaimLine_CPT_error').text('');
    $('#ClaimLine_Tariff_error').text('');
    $('#ClaimLine_Multiplier_error').text('');
    $('#ClaimLine_Units_error').text('');
    $('#ClaimLine_Quantity_error').text('');
    $('#ClaimLine_Authorization_error').text('');
    $('#ClaimLine_Tooth_Numbers_error').text('');
    $('#ClaimLine_Lens_Left_error').text('');
    $('#ClaimLine_Lens_Right_error').text('');
    $('#ClaimLine_Claimed_Amount_error').text('');
    $('#ClaimLine_DateStart_error').text('');
    $('#ClaimLine_DateEnd_error').text('');
    $("#ClaimLine_Nappi").attr('checked', false);
    $("#ClaimLine_Modifier").attr('checked', false);
    //$("#ClaimLine_Diagnosis_DropDown").empty();
    //$("#ClaimLine_Ref_Diagnosis_DropDown").empty();
    $("#ClaimLine_CPT_DropDown").empty();
    $("#ClaimLine_Tooth_Numbers_DropDown").empty();
}

function processClaimBatch(batchId,update,btn) {
    var url = "/ManagedCare/AgileController?opperation=ClaimHeaderCommand&command=processClaimBatch&batchId=" + batchId;
    if (btn != undefined) {
        btn.disabled = true;
    }
    //$('#Main_Claim_Div').mask("Processing...");
    $.post(url, function(result){
        updateFormFromData(result);
        $('#ClaimHeader_batchId').val('');
        resetClaimHeader();
        $('#ClaimHeader_Summary').empty();
        //$('#Main_Claim_Div').unmask();
        if (btn != undefined) {
            btn.disabled = false;
        }
        if(update == "true"){
            //alert("update = " + update);
            swapDivVisbible('Edit_Claims_Div', 'DetailedClaimLine_Div');
        }
    });    
}

function findICD(icd) {
    var field = $(icd).attr('id') + '_error'
    var result = "";
    if ($(icd).val() == "") {
        $('#' + field).text('');
        $('#' + field).addClass('error');
    } else {
        var url = "/ManagedCare/AgileController?opperation=ClaimLineCommand&command=findICD&icd=" + $(icd).val();
//        $.get(url, function(data) {
//            $('#' + field).removeClass();
//            if (data != '') {
//                $('#' + field).html(data);
//                valid = data;
//                return valid;
//            } else {
//                $('#' + field).text('Invalid ICD: ' + $(icd).val());
//                $('#' + field).addClass('error');
//                valid = data;
//                return valid;
//            }
//        });
        $.ajax({
            url: url,
            type: "get",
            async: false,
            success : function(data){
                $('#' + field).removeClass();
                if (data != '') {
                    $('#' + field).html(data);
                    result = data;
                    //alert("data = " + data + "\nvalid = " + result);
                    //return valid;
                } else {
                    $('#' + field).text('Invalid ICD: ' + $(icd).val());
                    $('#' + field).addClass('error');
                    result = data;
                    //alert("data = " + data + "\nvalid = " + result);
                    //return valid;
                }
            }
        });
    }
    return result;
}

function findTariff(tariff) {
    var field = 'ClaimLine_Tariff_error'; 
    var coverNum = $('#ClaimLine_CoverNumber').val();
    var serviceDate = $('#ClaimLine_DateStart').val();
    var providerNum = $('#ClaimLine_Provider_No').val();
    var practiceNum = $('#ClaimLine_Practice_No').val();
    if (tariff == "") {
        $('#' + field).text('Tariff is required');
        $('#' + field).addClass('error');
    } else {
        var url = "/ManagedCare/AgileController?opperation=ClaimLineCommand&command=findTariff&tariff=" + tariff + "&practiceTypeId=" + $('#ClaimLine_Practice_Type').val() + "&coverNum=" + coverNum + "&serviceDate=" + serviceDate+ "&providerNum=" + providerNum+ "&practiceNum=" + practiceNum;
        $.get(url, function(data) {
            $('#' + field).removeClass();
            if (data != '') {
                $('#' + field).html(data);
            } else {
                //$('#ClaimLine_Tariff').val("");
                $('#' + field).text(tariff + " - Invalid Tariff");
                $('#' + field).addClass('error');
            }
        });
    }
}

function findCPT(cpt) {
    var field = 'ClaimLine_CPT_error'; 
    var result = "";
    if (cpt == "") {
        $('#' + field).text('CPT is required');
        $('#' + field).addClass('error');
    } else {
        var url = "/ManagedCare/AgileController?opperation=ClaimLineCommand&command=findCPT&cpt=" + cpt;
//        $.get(url, function(data) {
//            $('#' + field).removeClass();
//            if (data != '') {
//                $('#' + field).html(data);
//            } else {
//                $('#' + field).text('Invalid CPT code: ' + cpt);
//                $('#' + field).addClass('error');
//            }
//        });
        $.ajax({
            url: url,
            type: "get",
            async: false,
            success: function(data){
                $('#' + field).removeClass();
                if(data != ''){
                    $('#' + field).html(data);
                    result = data;
                    //alert("data = " + data + "\nvalid = " + result);
                } else {
                    $('#' + field).text('Invalid CPT code: ' + cpt);
                    $('#' + field).addClass('error');
                    result = data;
                    //alert("data = " + data + "\nvalid = " + result);
                }
            }
        });
    }
    return result;
}

function validateTime(time, element) {
    var valid = true;
    var field = element + "_error";
    if(time == "") {
        $('#' + field).text('Please enter a Time');
        $('#' + field).addClass('error');
        valid = false;
    } else {
        var url = "/ManagedCare/AgileController?opperation=ClaimLineCommand&command=ValidateTime&time=" + time;
        $.get(url, function(data) {
            $('#' + field).text("");
            $('#' + field).removeClass();
            var resultArr = data.split("|");
            if(resultArr[0] == 'error') {
                $('#' + field).text(resultArr[1]);
                $('#' + field).addClass('error');
                $('#' + element).val("");
                valid = false;
            } else if(resultArr[0] == "done") {
                $('#' + element).val(resultArr[1]);
                $('#' + field).text("");
                $('#' + field).removeClass();
            }
        });
    }
    
    return valid;
}

function captureNappi(claimLines, coverNumber, capOrEdit) {
    var url;
    url = "/ManagedCare/AgileController?opperation=ClaimLineNdcCommand&command=NewNappiLine&claimLines=" + claimLines + "&coverNumber=" + coverNumber + "&capOrEdit=" + capOrEdit;
    //alert("capture Nappi Url = " + url);
    $.get(url, function(response){
        if(capOrEdit === "edit"){
            $('#Edit_Claim_Ndc_Div').empty().html(response);
            $('#Edit_Claim_Ndc_Div').show();
            $('#Edit_Claim_Lines_Div').hide();
        } else {
            $('#Capture_Claim_Ndc_Div').empty().html(response);
            $('#Capture_Claim_Ndc_Div').show();
            $('#Capture_Claim_Lines_Div').hide();
            //$('#ClaimLine_Practice_Type').val(practiceTypeId);
        }
    });

    swapDivVisbible('Capture_Claim_Lines_Div', 'Capture_Claim_Ndc_Div');
}

function returnFromNappi(save){
    var url = "/ManagedCare/AgileController?opperation=ClaimLineNdcCommand&command=getTotals";
    $.get(url, function(response){
        if(response !== null && response !== ""){
            $('#ClaimLine_Claimed_Amount').val(response);
        }
        if(save === 'true'){
            swapDivVisbible('Capture_Claim_Ndc_Div', 'Capture_Claim_Lines_Div');
        } else {
            swapDivVisbible('Edit_Claim_Ndc_Div', 'Edit_Claim_Lines_Div');
        }
    });
}

function validateNappiLine(){
    var valid = true;
    var res = true;
    
    if($('#ClaimLine_NappiCode').val() == "") {
        $('#ClaimLine_NappiCode_error').text('Nappi Code is required');
        $('#ClaimLine_NappiCode_error').addClass("error");
        valid = false;
    }
    
    res = validateNeoIntField('ClaimLine_NappiQuantity', true);
    valid = valid && res;
    
    res = validateNeoIntField('ClaimLine_NappiDosage', true);
    valid = valid && res;
    
    var amount = validateAmountField('ClaimLine_NappiAmount');
    if (amount == null){
        var value = $("#ClaimLine_NappiAmount").val(); 
        $('#ClaimLine_NappiAmount_error').text(value + " is an invalid amount");
        $('#ClaimLine_NappiAmount_error').addClass("error");
        $("#ClaimLine_NappiAmount").val("");
        res = false;
    } else {
        $("#ClaimLine_NappiAmount").val(amount);
        $('#ClaimLine_NappiAmount_error').text("");
        $('#ClaimLine_NappiAmount_error').removeClass();
    }
    
    valid = valid && res;
    
    return valid;
}

function saveNappi(form, targetId, btn) {
    if (!validateNappiLine()) {
        displayNeoErrorNotification('There are validation errors');
        return;
    }
    $("#ClaimLine_Nappi").attr('checked', true);

    if (btn != undefined) {
        btn.disabled = true;
    }
    if (btn != undefined) {
        btn.disabled = true;
    }

    var params = encodeFormForSubmit(form);
    var formId = $(form).attr('id');
    //$(form).mask("Processing...");
    $.post(form.action, params, function(result) {
        displayNeoSuccessNotification('Nappi Code Saved');
        resetNappi();
        //alert("result nappi = " + result + "\ntargetId = " + targetId);
        updateFormFromData(result, targetId);
        var claimLine = $('#ClaimLine_claimId');

        var url = "/ManagedCare/AgileController?opperation=ClaimLineNdcCommand&command=fetchNappiLines&claimLineClaimNappi=" + claimLine;
        $.post(url, function(data) {
            //alert("fetchNappiLines data = " + data);
            updateFormFromData(data, 'ClaimLine_NDC_Summary');
        });

        /*var batchId = $('#ClaimHeader_batchId').val();
         var url = "/ManagedCare/AgileController?opperation=ClaimHeaderCommand&command=fetchBatchAndClaims&batchId=" + batchId;
         $.post(url, function(data) {
         updateFormFromData(data, 'ClaimHeader_Summary');
         });*/

        if (btn != undefined) {
            btn.disabled = false;
        }
        if ($('#' + formId).unmask != null)
            $(form).unmask();
    });
}

function resetNappi() {
    $('#ClaimLine_NappiCode').val('');
    $('#ClaimLine_NappiQuantity').val('1');
    $('#ClaimLine_NappiDosage').val('1');
    $('#ClaimLine_NappiAmount').val('');
    $('#ClaimLine_NappiPrimary').attr('checked', false);
    $('#ClaimLine_NappiCode_error').text('');
    $('#ClaimLine_NappiQuantity_error').text('');
    $('#ClaimLine_NappiDosage_error').text('');
    $('#ClaimLine_NappiAmount_error').text('');
    $('#ClaimLine_NappiPrimary_error').text('');
}

function findNappi(nappi) {
    var field = 'ClaimLine_NappiCode_error'; 
    var practice = $('#ClaimLine_Practice_Type').val();
    if (nappi == "") {
        $('#' + field).text('Nappi is required');
        $('#' + field).addClass('error');
    } else {
        var url = "/ManagedCare/AgileController?opperation=ClaimLineNdcCommand&command=findNappi&nappi=" + nappi + "&practiceType=" + practice;
        $.get(url, function(data) {
            $('#' + field).removeClass();
            if (data != '') {
                $('#' + field).html(data);
            } else {
                $('#' + field).text('Invalid Nappi: ' + nappi);
                $('#' + field).addClass('error');
                $('#ClaimLine_NappiCode').val("");
            }
        });
    }
}

function addDropDown(element,value, e) {
    //check to see if blank is added and stops
    if(value == ""){ 
        return;
    }
    var edit = false;
    if(e === 'edit'){
        edit = true;
    } else {
        if(e && e.which){
            charCode = e.which;
        }else if(window.event){
            e = window.event;
            charCode = e.keyCode;
        }
    }
    
    if(charCode == 13 || edit) {
        var charCode;
        var field = $(element).attr('id');
        var valid = true;
        var error = field + "_error";
        $('#' + error).removeClass();
        var select;
        var code = value.charAt(0).toUpperCase() + value.slice(1);
        if(field === "ClaimLine_Tooth_Numbers") {
            if(!validTooth(field,value)){
                valid = false;
                $('#' + error).text('Please enter a valid number');
                $('#' + error).addClass('error');
            } else {
                select = document.getElementById('ClaimLine_Tooth_Numbers_DropDown');
                $('#ClaimLine_Tooth_Numbers_DropDown').show();
            }
        } else if(field === "ClaimLine_CPT") {
            value = findCPT(value);
            //alert("value = " + value);
            if(value.indexOf("Invalid") != -1){
                //alert("Not Valid");
                valid = false;
                $('#' + error).text(value);
                $('#' + error).addClass('error');
            } else {
                select = document.getElementById('ClaimLine_CPT_DropDown');
                $('#ClaimLine_CPT_DropDown').show();
                $('#' + error).html(code);
            }
        } else if(field === "ClaimLine_Diagnosis"){
            value = findICD(element);
            //alert("value in IF = " + value);
            if(value.indexOf("Invalid") != -1){
                //alert("not valid");
                valid = false;
                $('#' + error).text(value);
                $('#' + error).addClass('error');
            } else {
                select = document.getElementById('ClaimLine_Diagnosis_DropDown');
                $('#ClaimLine_Diagnosis_DropDown').show();
                $('#' + error).html(code);
            }
        } else if(field === "ClaimLine_Ref_Diagnosis") {
            value = findICD(element);
            //findICD(element);
            if(value.indexOf("Invalid") != -1){
                //alert("Not Valid");
                valid = false;
                $('#' + error).text(value);
                $('#' + error).addClass('error');
            } else {
                select = document.getElementById('ClaimLine_Ref_Diagnosis_DropDown');
                $('#ClaimLine_Ref_Diagnosis_DropDown').show();
                $('#' + error).html(code);
            }
        }
        if(valid){
        var options = value;
        //var i;
        //for (i = 0; i < options.length; i++) {
            var opt = options;
            var el = document.createElement("option");
            el.textContent = opt;
            el.value = code;
            select.options.add(el);
            //select.appendChild(el);
            el.innerHTML = opt;
            el.textContent = opt;
            el.value = code;
            $('#' + field).val('');
            $('#' + select.id).val(code).change();
        //}
        }
    }
}

function validTooth(field,value){
    var valid = true;
    var res = true;
    var i = 0;
    for(i = 0; i < value.length;i++){
        if(value.charAt(i) == "," || value.charAt(i) == " "){
            res = false;
        }
    }
    valid = res && valid;
    res = validateNeoIntField(field,false);
    valid = valid && res;
    
    //alert("valid = " + valid);
    return valid;
}

function changeClaimLineCategory(val) {
        //alert("claimCategoryVal = " + val);
        var capOrEditStartLoad = $('#capOrEditStartLoad').val();
        var capOrEdit = $('capOrEdit').val();
        var url = "/ManagedCare/AgileController?opperation=ClaimLineCommand&command=clearSessionLists&capOrEditStartLoad=" + capOrEditStartLoad + "&capOrEdit=" + capOrEdit;
        $.ajax({url:url,type:'get'});
        resetClaimLine();
        $('#ClaimLineTable_Dental').hide();
        $('#ClaimLineTable_Optical').hide();
//        $('#ClaimLineTable_Modifier').hide();
        $('#ClaimLineTable_CPT').hide();
        $('#ClaimLineTable_INV').hide();
        $('#ClaimLineTable_Time').hide();
        $('#ClaimLineTable_Diag_side').show();
        $('#ClaimLineTable_Date_side').show();
        $('#ClaimLineTable_Diag_under').hide();
        $('#ClaimLineTable_Date_under').hide();
        $('#ClaimLine_DateEnd_div').hide();
        //$('#ClaimLine_Tooth_Numbers_DropDown').hide();
        //$('#ClaimLine_CPT_DropDown').hide();
        //$('#ClaimLine_Diagnosis_DropDown').hide();
        //$('#ClaimLine_Ref_Diagnosis_DropDown').hide();
        //$('#ClaimLineTable_Nappi_Details').hide();

        if (val == 5) {
            $('#ClaimLineTable_Time').show();
            $('#ClaimLine_DateEnd_div').show();
        }

        if (val == 5 || val == 10) {
            $('#ClaimLineTable_CPT').show();
        }

//        if (val == 2 || val == 3 || val == 5 || val == 7 || val == 8 || val == 9 || val == 10) {
//            $('#ClaimLineTable_Modifier').show();
//        }

        if (val == 8 || val == 9 || val == 10) {
            $('#ClaimLineTable_INV').show();
        }

        if (val == 2) {
            $('#ClaimLineTable_Optical').show();
        }

        if (val == 8 || val == 3) {
            $('#ClaimLineTable_Dental').show();
        }

        /*if (val == 1 || val == 4 || val == 6 || val == 11) {
         $('#ClaimLineTable_Diag_under').show();
         $('#ClaimLineTable_Date_under').show();
         $('#ClaimLineTable_Diag_side').hide();
         $('#ClaimLineTable_Date_side').hide();
         }*/
    }
    
    function removeDropDown(element, e, value, field){
        var charCode;
        $('#' + field + "_error").removeClass();
        $('#' + field + "_error").text("");

        if(e && e.which){
            charCode = e.which;
        }else if(window.event){
            e = window.event;
            charCode = e.keyCode;
        }
        
        //check for the delete key
        if(charCode == 46) {
            var select;
            
            if(value != ""){
                element.remove(element.selectedIndex);
                element.selectedIndex = 0;
                var val = element.options[element.selectedIndex].value;
                if(field != "ClaimLine_Tooth_Numbers"){
                    $('#' + field + "_error").text(val);
                }
            }
        }
    }
    
    function displayDropDown(target,val){
        var result;
        var error = target + "_error";
        $('#' + error).removeClass();
        
        if(target === "ClaimLine_Diagnosis"){
            //result = findICD(val);
            $("#" + target + "_error").html(val.val());
        } else if(target === "ClaimLine_Ref_Diagnosis") {
            //result = findICD(val);
            $("#" + target + "_error").html(val.val());
        } else if(target === "ClaimLine_CPT"){
            //result = findCPT(val.val());
            $("#" + target + "_error").html(val.val());
        }
    }
    
    function validateAmountField(field){
    var fieldName = getNeoFieldName(field);
    var value = $("#" + fieldName).val();
    
    if(value.startsWith(".")){
  	value = "0" + value;
    }

    var regex = /^[0-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
    if (regex.test(value))
    {
        //Input is valid, check the number of decimal places
        var twoDecimalPlaces = /\.\d{2}$/g;
        var oneDecimalPlace = /\.\d{1}$/g;
        var noDecimalPlacesWithDecimal = /\.\d{0}$/g;
        
        if(value.match(twoDecimalPlaces ))
        {
            //all good, return as is
            return value;
        }
        if(value.match(noDecimalPlacesWithDecimal))
        {
            //add two decimal places
            return value+'00';
        }
        if(value.match(oneDecimalPlace ))
        {
            //ad one decimal place
            return value+'0';
        }
        //else there is no decimal places and no decimal
        return value+".00";
    }
    return null;
}
function validateModifierLine(){
    var valid = true;
    var res = true;

    if($('#ClaimLine_ModifierCode').val() == "") {
        $('#ClaimLine_ModifierCode_error').text('Modifier Code is required');
        $('#ClaimLine_ModifierCode_error').addClass("error");
        valid = false;
    }
    
    valid = valid && res;
    res = validateNeoIntField('ClaimLine_ModifierUnits', true);
    valid = valid && res;
    res = validateNeoIntField('ClaimLine_ModifierTime', true);
    valid = valid && res;
    
    return valid;
}

function saveModifier(form, targetId, btn) {
    if (!validateModifierLine()) {
        displayNeoErrorNotification('There are validation errors');
        return;
    }
    $("#ClaimLine_Modifier").attr('checked', true);

    if (btn != undefined) {
        btn.disabled = true;
    }
    if (btn != undefined) {
        btn.disabled = true;
    }

    var params = encodeFormForSubmit(form);
    var formId = $(form).attr('id');
    //$(form).mask("Processing...");
    $.post(form.action, params, function(result) {
        displayNeoSuccessNotification('Modifier Saved');
        resetModifier();
        updateFormFromData(result, targetId);
        var claimLine = $('#ClaimLine_claimId');

        var url = "/ManagedCare/AgileController?opperation=ClaimLineModifierCommand&command=fetchModifierLines";
        $.post(url, function(data) {
            updateFormFromData(data, 'ClaimLine_Modifier_Summary');
        });

        /*var batchId = $('#ClaimHeader_batchId').val();
         var url = "/ManagedCare/AgileController?opperation=ClaimHeaderCommand&command=fetchBatchAndClaims&batchId=" + batchId;
         $.post(url, function(data) {
         updateFormFromData(data, 'ClaimHeader_Summary');
         });*/

        if (btn != undefined) {
            btn.disabled = false;
        }
        if ($('#' + formId).unmask != null)
            $(form).unmask();
    });
}

function resetModifier(){
    $('#ClaimLine_ModifierCode').val('');
    $('#ClaimLine_ModifierUnits').val('1');
    $('#ClaimLine_ModifierTime').val('0');
    $('#ClaimLine_ModifierCode_error').text('');
    $('#ClaimLine_ModifierUnits_error').text('');
    $('#ClaimLine_ModifierTime_error').text('');
}

function captureModifier(capOrEdit) {
    var url;
    url = "/ManagedCare/AgileController?opperation=ClaimLineModifierCommand&command=NewModifierLine&capOrEdit=" + capOrEdit;
    //alert("modifier url = " + url);
    $.get(url, function(response){
       //(capOrEdit)
        if(capOrEdit === 'edit'){
            $('#Edit_Claim_Modifier_Div').empty().html(response);
            $('#Edit_Claim_Modifier_Div').show();
            $('#Edit_Claim_Lines_Div').hide();
        } else{
            $('#Capture_Claim_Modifier_Div').empty().html(response);
            $('#Capture_Claim_Modifier_Div').show();
            $('#Capture_Claim_Lines_Div').hide();
            //$('#ClaimLine_Practice_Type').val(practiceTypeId);
        }
    });

    swapDivVisbible('Capture_Claim_Lines_Div', 'Capture_Claim_Modifier_Div');
}

function removeNappiFromList(index) {
    //alert("index = " + index);
    var url="/ManagedCare/AgileController?opperation=ClaimLineNdcCommand&command=removeNappiFromList&nappiIndex=" + index;
    
    $.get(url,function(result){
        updateFormFromData(result, null);

        var url = "/ManagedCare/AgileController?opperation=ClaimLineNdcCommand&command=fetchNappiLines";
        $.post(url, function(data) {
            updateFormFromData(data, 'ClaimLine_NDC_Summary');
        });
    });
}

function removeModifierFromList(index) {
    //alert("index = " + index);
    var url="/ManagedCare/AgileController?opperation=ClaimLineModifierCommand&command=removeModifierFromList&modifierIndex=" + index;
    
    $.get(url,function(result){
        updateFormFromData(result, null);

        var url = "/ManagedCare/AgileController?opperation=ClaimLineModifierCommand&command=fetchModifierLines";
        $.post(url, function(data) {
            updateFormFromData(data, 'ClaimLine_Modifier_Summary');
        });
    });
}

function modifyNappiFromList(index){
    
    //var error = "#saveTariff_error";
    var url="/ManagedCare/AgileController?opperation=ClaimLineNdcCommand&command=modifyNappiFromList&nappiIndex="+index;
//    var data = {
//        'tariffIndex':index,
//        'id':new Date().getTime()
//    }; 
    
    $.get(url/*, data*/, function(resultTxt){
        if (resultTxt!==null) {
            var resTxtArr = resultTxt.split("|");
            var result = resTxtArr[0];
            if(result === ""){
                displayNeoErrorNotification('Error retriving values');
            }else if(result === "Done"){
                //hide row
                //var hideElement = "#tariffTagTable tr[id=tariffRow"+index+"]";
                //$(hideElement).hide();
                var url = "/ManagedCare/AgileController?opperation=ClaimLineNdcCommand&command=fetchNappiLines";
                $.post(url, function(data) {
                    updateFormFromData(data, 'ClaimLine_NDC_Summary');
                });

                var nappiCode = resTxtArr[1];
                var nappiDosage = resTxtArr[2];
                var nappiQuantity = resTxtArr[3];
                var nappiAmount = resTxtArr[4];
                var nappiPrimary = resTxtArr[5];
                
                //set and get nappi
                $("#ClaimLine_NappiCode").val(nappiCode); 
                findNappi(nappiCode);
                
                //set the rest
                $("#ClaimLine_NappiDosage").val(nappiDosage); 
                $("#ClaimLine_NappiQuantity").val(nappiQuantity); 
                $("#ClaimLine_NappiAmount").val(nappiAmount); 
                
                //set the primary indicator
                if(nappiPrimary == 1){
                    $('#ClaimLine_NappiPrimary').attr('checked', false);
                }
            }        
        }
    });
//    data = null;
}

function modifyModifierFromList(index){
   //alert("index = " + index);
    //var error = "#saveTariff_error";
    var url="/ManagedCare/AgileController?opperation=ClaimLineModifierCommand&command=modifyModifierFromList&modifierIndex="+index;
//    var data = {
//        'tariffIndex':index,
//        'id':new Date().getTime()
//    }; 
    
    $.get(url/*, data*/, function(resultTxt){
        if (resultTxt!==null) {
            var resTxtArr = resultTxt.split("|");
            var result = resTxtArr[0];
            if(result === ""){
                displayNeoErrorNotification('Error retriving values');
            }else if(result === "Done"){
                //hide row
                //var hideElement = "#tariffTagTable tr[id=tariffRow"+index+"]";
                //$(hideElement).hide();
                var url = "/ManagedCare/AgileController?opperation=ClaimLineModifierCommand&command=fetchModifierLines";
                $.post(url, function(data) {
                    updateFormFromData(data, 'ClaimLine_Modifier_Summary');
                });

                var modfierCode = resTxtArr[1];
                var modifierUnits = resTxtArr[2];
                var modifierTime = resTxtArr[3];
                
                //set and get nappi
                $("#ClaimLine_ModifierCode").val(modfierCode); 
                
                //set the rest
                $("#ClaimLine_ModifierUnits").val(modifierUnits); 
                $("#ClaimLine_ModifierTime").val(modifierTime); 
            }        
        }
    });
//    data = null;
}

function checkDate(field,required){
    //alert("field = " + field + "\nrequired = " + required);
    if(validateNeoDateField(field,required,null)){
        //alert("true");
        $("#ClaimLine_DateEnd").val($("#ClaimLine_DateStart").val());
    }
}

function setProductOptionValues(){
    //alert("triggerd");
    var optVal = document.createElement('OPTION');
    optVal.value = "99";
    optVal.text = "";
    $("#ClaimHeader_SearchMemberSchemeOptions option").add(optVal);
    toggleProduct($("#ClaimHeader_SearchMemberScheme").val());
}

function toggleProduct(prodId){
    //alert("hierso = " + prodId);
    if(prodId != "99"){
        //alert("prodId = " + prodId);
        addProductOption(prodId);
    }
}

function addProductOption(prodId){
    //alert("prodId = " + prodId)
    xhr = GetXmlHttpObject();
    if(xhr==null){
        alert("ERROR: Browser Incompatability");
        return;
    }
    var url = "/ManagedCare/AgileController?opperation=GetProductOptionCommand&prodId="+prodId;
    //alert("url = " + url);
    xhr.onreadystatechange=function(){
        if(xhr.readyState == 4 && xhr.statusText == "OK"){
            $("#ClaimHeader_SearchMemberSchemeOptions").empty();
            $("#ClaimHeader_SearchMemberSchemeOptions").append("<option value=\"99\"></option>");
            var xmlDoc = GetDOMParser(xhr.responseText);
            //print value from servlet
            $(xmlDoc).find("ProductOptions").each(function (){
                //set product details
                $(this).find("Option").each(function (){
                    var prodOpt = $(this).text();
                    var opt = prodOpt.split("|");
                    //Add option to dependant dropdown
                    var optVal = document.createElement('OPTION');
                    optVal.value = opt[0];
                    optVal.text = opt[1];
                    document.getElementById("ClaimHeader_SearchMemberSchemeOptions").options.add(optVal);

                });
            });
        }
    }
    xhr.open("POST",url,true);
    xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
    xhr.send(null);
}

function searchResults(btn) {
    if (btn != undefined) {
        
        btn.disabled = true;
    }
    var count = 0;
    var uniqueSearch = false;
    if ($('#memNum').val() !== "" && $('#memNum').val() !== "undefined") {
        count++;
        //alert("memNum = ." + $('#memNum').val() + ".");
    }

    if ($('#depNum').val() !== "" && $('#depNum').val() !== "undefined") {
        count++;
        //alert("depNum = ." + $('#depNum').val() + ".");
    }

    if ($('#providerNumber').val() != "" && $('#providerNumber').val() != "undefined") {
        count++;
        //alert("providerNumber = ." + $('#providerNumber').val() + ".");
    }

    if ($('#discId').val() !== "" && $('#discId').val() !== "undefined") {
        count++;
        //alert("discId = ." + $('#discId').val() + ".");
    }

    if ($('#practiceNumber').val() !== "" && $('#practiceNumber').val() !== "undefined") {
        count++;
        //alert("practiceNumber = ." + $('#practiceNumber').val() + ".");
    }

//    if ($('#disciplineType').val() !== "" && $('#disciplineType').val() !== "99") {
//        count++;
//        //alert("disciplineType = ." + $('#disciplineType').val() + ".");
//    }

    if ($('#processFrom').val() !== "" && $('#processFrom').val() !== "undefined") {
        count++;
        //alert("processFrom = ." + $('#processFrom').val() + ".");
    }
 
    if ($('#processTo').val() != "" && $('#processTo').val() != "undefined") {
        count++;
        //alert("processTo = ." + $('#processTo').val() + ".");
    }

    if ($('#serviceFrom').val() !== "" && $('serviceFrom').val() !== "undefined") {
        count++;
        //alert("serviceFrom = ." + $('#serviceFrom').val() + ".");
    }

    if ($('#serviceTo').val() !== "" && $('#serviceTo').val() !== "undefined") {
        count++;
        //alert("serviceTo = ." + $('#serviceTo').val() + ".");
    }

    if ($('#accNumber').val() !== "" && $('#accNumber').val() !== "undefined") {
        count++;
        //alert("accNumber = ." + $('#accNumber').val() + ".");
    }

    if ($('#scanNum').val() !== "" && $('#scanNum').val() !== "undefined") {
        count++;
        //alert("scanNum = ." + $('#scanNum').val() + ".");
    }

    if ($('#batchNumber').val() !== "" && $('#batchNumber').val() !== "undefined") {
        count++;
        uniqueSearch = true;
        //alert("batchNumber = ." + $('#batchNumber').val() + ".");
    }

    if ($('#claimId').val() !== "" && $('#claimId').val() !== "undefined") {
        count++;
        uniqueSearch = true;
        //alert("claimId = ." + $('#claimId').val() + ".");
    }

    if ($('#authNumber').val() !== "" && $('#authNumber').val() !== "undefined") {
        count++;
        //alert("authNumber = ." + $('#authNumber').val() + ".");
    }

    if ($('#paymentDate').val() !== "" && $('#paymentDate').val() !== "undefined") {
        count++;
        //alert("paymentDate = ." + $('#paymentDate').val() + ".");
    }

    if ($('#ediClaim').val() !== "" && $('#ediClaim').val() !== "undefined") {
        count++;
        //alert("ediClaim = ." + $('#ediClaim').val() + ".");
    }

    if ($('#tariffCode').val() !== "" && $('#tariffCode').val() !== "undefined") {
        count++;
        //alert("tariffCode = ." + $('#tariffCode').val() + ".");
    }

    if ($('#claimStatus').val() !== "" && $('#claimStatus').val() !== "99") {
        count++;
        //alert("claimStatus = ." + $('#claimStatus').val() + ".");
    }

    if ($('#toothNum').val() !== "" && $('#toothNum').val() !== "undefined") {
        count++;
        //alert("toothNum = ." + $('#toothNum').val() + ".");
    }
 
    //alert("Count = " + count);
    if (count >= 2 || uniqueSearch) {
        submitWithActionOnScreen('SearchClaimsCommand', '/NClaims/ViewClaims.jsp');
    } else {
        alert("Please Enter more search Criteria");
        if (btn != undefined) {
            btn.disabled = false;
        }
    }
}

function editClaimLine(form, claimLineId,reverse){
    //alert("edit Called + form = " + form);
    var formId = $(form).attr('id');
    //$(form).mask("Processing...");


//    var url = "/ManagedCare/AgileController";
//    var data = {'opperation': 'EditClaimCommand','command': 'editClaimLine' ,'id': new Date().getTime()};
//    //alert("url = "+url);
//    $.get(url, data, function(resultTxt) {
//        //alert(resultTxt);
//        updateFormFromData(resultTxt);
//    });
    var claimId = $("#ClaimLine_claimId").val();
    var url;
    url = "/ManagedCare/AgileController?opperation=EditClaimCommand&command=editClaimLine&claimLineId=" + claimLineId + "&claimID=" + claimId + "&reverse=" + reverse;
    $.ajax({
            url: url,
            type: "get",
            async: false,
            success: function(response){
                //alert(response);
                $('#Edit_Claim_Lines_Div').empty().html(response);
                $('#DetailedClaimLine_Div').hide();
                $('#Edit_Claim_Lines_Div').show();
                //alert("editClaimLine Done");
            }
    });

    var url;
    url = "/ManagedCare/AgileController?opperation=EditClaimCommand&command=populateClaimLine&claimLineId=" + claimLineId;
    $.get(url, function(resultTxt){
        //alert(resultTxt);
        if(resultTxt != null){
            var resTxtArr = resultTxt.split("|");
            //alert("resTxtArr = " + resTxtArr.size());
            $('#ClaimLine_Category').val(resTxtArr[0]);//category
            changeClaimLineCategory(resTxtArr[0]);
            $('#ClaimLine_CoverNumber').val(resTxtArr[1]);//cover
            //$('#ClaimLine_Dependant').val(resTxtArr[2]);//depCode
            $('#ClaimLine_DateStart').val(resTxtArr[2]);//serviceFrom
            $('#ClaimLine_DateEnd').val(resTxtArr[3]);//ServiceTO
            //$('#ClaimLine_Diagnosis').val(resTxtArr[4]);//icd10
            //var icdArr = resTxtArr[4];
            var icdArr = resTxtArr[4].split(",");
            
            for(var i = 0;i < icdArr.length;i++){
                //alert("icd" + i + " = " + icdArr[i]);
                $('#ClaimLine_Diagnosis').val(icdArr[i]);//icd10
                addDropDown('#ClaimLine_Diagnosis',icdArr[i],'edit');
            }
            $('#ClaimLine_Ref_Diagnosis').val(resTxtArr[5]);//reficd10
            //var refIcdArr = resTxtArr[5];
            var refIcdArr = resTxtArr[5].split(",");
            
            for(var i = 0;i < refIcdArr.length;i++){
                //alert("refIcd" + i + " = " + refIcdArr[i]);
                addDropDown('#ClaimLine_Ref_Diagnosis',refIcdArr[i],'edit');
            }
            $('#ClaimLine_Tariff').val(resTxtArr[6]);//tarrif
            $('#modList').val(resTxtArr[7]);//modifier
            var empty = true;
            if(resTxtArr[7].toString() === '[]'){
                empty = true;
            } else {
                empty = false;
            }
            if(empty){
                $("#ClaimLine_Modifier").attr('checked', false);
            } else {
                $("#ClaimLine_Modifier").attr('checked', true);
            }
            $('#ClaimLine_Multiplier').val(resTxtArr[8]);//multiplier
            $('#ClaimLine_Units').val(resTxtArr[9]);//units
            $('#ClaimLine_Quantity').val(resTxtArr[10]);//quantity
            $('#ClaimLine_Authorization').val(resTxtArr[11]);//authnumbers
            $('#claimLineNdcList').val(resTxtArr[12]);//ndcs
            empty = true;
            if(resTxtArr[12].toString() === '[]'){
                empty = true;
            } else {
                empty = false;
            }
            if(empty){
                $("#ClaimLine_Nappi").attr('checked', false);
            } else {
                $("#ClaimLine_Nappi").attr('checked', true);
            }
            
            $('#ClaimLine_Claimed_Amount').val(resTxtArr[13]);//claimedAmount
            $('#ClaimLine_Lens_Left').val(resTxtArr[14]);//eyes right
            $('#ClaimLine_Lens_Right').val(resTxtArr[15]);//eyes left
            $('#ClaimLine_Tooth_Numbers').val(resTxtArr[16]);//toothNumbers
            var toothArr = resTxtArr[16].split(",");
            for(var i = 0; i < toothArr.length; i++){
                //alert("toothArr" + i + " = " + toothArr[i]);
                addDropDown('#ClaimLine_Tooth_Numbers',toothArr[i],'edit');
            }
            $('#ClaimLine_Invoice_Number').val(resTxtArr[17]);//labInvoice
            $('#ClaimLine_Order_Number').val(resTxtArr[18]);//labOrder
            $('#ClaimLine_Time_In').val(resTxtArr[19]);//timeIn
            $('#ClaimLine_Time_Out').val(resTxtArr[20]);//timeOut
            $('#ClaimLine_CPT').val(resTxtArr[21]);//cptCodes
            var cptArr = resTxtArr[21].split(",");
            for(var i = 0; i < cptArr.length; i++){
                //alert("cptArr" + i + " = " + cptArr[i]);
                addDropDown('#ClaimLine_CPT',cptArr[i],'edit');
            }
            
            $('#ClaimLine_Practice_Type').val(resTxtArr[22]);
            $('#ClaimLine_Provider_No').val(resTxtArr[23]);
            $('#ClaimLine_Practice_No').val(resTxtArr[24]);
            //alert("populateClaimLine Done");
            var claimId = $('#ClaimLine_claimId').val();
            //alert("claimId = " + claimId);

            var url = "/ManagedCare/AgileController?opperation=ClaimLineCommand&command=fetchClaimLines&claimId=" + claimId;
            $.post(url, function(data) {
                updateFormFromData(data, 'ClaimLine_Summary');
            });
        }
        if ($('#'+formId).unmask != null) $(form).unmask();
    });
    
    
}

function submitWithActionOnScreen(action, onScreen) {
    document.getElementById('opperation').value = action;
    document.getElementById('onScreen').value = onScreen;
    var mem = $("#policyHolderNumber_text").val();
    if(mem != null && mem != "" && mem != "null"){
        document.getElementById('policyHolderNumber').value = mem;
    }
    document.forms[0].submit();
}

function updateClaimLine(form, targetId, btn) {
    if (!validateClaimLine()) {
        displayNeoErrorNotification('There are validation errors');
        return;
    }
    if (btn != undefined) {
        btn.disabled = true;
    }
    if (btn != undefined) {
        btn.disabled = true;
    }
    
    var params = encodeFormForSubmit(form);
    //alert("params = " + params);
    var formId = $(form).attr('id');
    //$(form).mask("Processing...");
    $.post(form.action, params,function(result){
        displayNeoSuccessNotification('Claim Line Saved');
        resetClaimLine();
        //alert("update result = " + result);
        updateFormFromData(result, targetId);
        //alert("capOrEditStartLoad = " + $('#capOrEditStartLoad').val());
        $('#capOrEditStartLoad').val(false);
        //alert("capOrEditStartLoad = " + $('#capOrEditStartLoad').val());
        var claimId = $('#ClaimLine_claimId').val();
        //alert("ClaimLine_claimId = " + claimId);
        $('#claimLineId').val('');

        var url;
        url = "/ManagedCare/AgileController?opperation=EditClaimCommand&command=editClaimLine&claimLineId=";
        $.ajax({
            url: url,
            type: "get",
            async: false,
            success: function(response){
                //alert(response);
                $('#Edit_Claim_Lines_Div').empty().html(response);
                //alert("editClaimLine Done");
            }
        });
    
        var url = "/ManagedCare/AgileController?opperation=ClaimLineCommand&command=fetchClaimLines&claimId=" + claimId + "&update=true";
        $.post(url, function(data) {
            updateFormFromData(data, 'ClaimLine_Summary');
        });
        
        var batchId = $('#ClaimHeader_batchId').val();
        //alert("batchId = " + batchId);
        var url = "/ManagedCare/AgileController?opperation=EditClaimCommand&command=fetchBatchAndClaims&batchId=" + batchId;
        $.post(url, function(data) {
            updateFormFromData(data, 'ClaimHeader_Summary');
        });
        
        if (btn != undefined) {
            btn.disabled = false;
        }
        if ($('#'+formId).unmask != null) $(form).unmask();
    });
}

function returnToClaims(action){
    window.onbeforeunload = null;
    //document.getElementById('opperation').value = action;
    //document.forms[0].submit();
    submitWithAction(action);
}

function addClaimLine(form){
    //alert("form = " + form);
    var url;
    url = "/ManagedCare/AgileController?opperation=EditClaimCommand&command=addClaimLine";
    $.ajax({
            url: url,
            type: "get",
            async: false,
            success: function(response){
                $('#Edit_Claim_Lines_Div').empty().html(response);
                $('#DetailedClaimLine_Div').hide();
                $('#Edit_Claim_Lines_Div').show();
                $('#ClaimLine_Practice_Type').val($('#test').val());
            }
    });
    
    var claimId = $('#ClaimLine_claimId').val();

            var url = "/ManagedCare/AgileController?opperation=ClaimLineCommand&command=fetchClaimLines&claimId=" + claimId;
            $.post(url, function(data) {
                updateFormFromData(data, 'ClaimLine_Summary');
            });
}

//function updateOrSaveClaimLine(form, targetId, btn){
//    var updateOrSave;
//    if($('#claimLineId').val() === ""){
//        updateOrSave = 'save';
//        saveClaimLine(form,targetId,btn, 'true');
//    } else {
//        updateOrSave = 'update';
//        updateClaimLine(form,targetId,btn);
//    }
//    $('#claimLineId').val('');
//}

function displayPaymentRunButtons(preRun) {
    hideAll();
    var prun = getPaymentRunStatus();
    //alert("prun = " + prun);
    if(prun){
        $('#Pre_pay_run').show();
        $('#Post_pay_run').hide();
    } else {
        $('#Post_pay_run').show();
        $('#Pre_pay_run').hide();
    }
}

 function getPaymentRunStatus(){
    var returnVal;
    var url="/ManagedCare/AgileController?opperation=EditClaimCommand&command=getPaymentRunStatus";
    
    $.ajax({
        url: url,
        type: "get",
        async: false,
        success: function(result){
            if(result === "Run"){
                returnVal = false;
            } else if (result === "NoRun"){
                returnVal = true;
            }
        }
    });
    return returnVal;
}

function submitRejectAction(btn){ 
    //alert("submitRejectAction");
    if (btn != undefined) {
        btn.disabled = true;
    }
    $("#rejCL_error").text("");
    var str = $("#clRejReason").val();
    if (str !== null && str !== "" && str !== "99") {
        var url = "/ManagedCare/AgileController";
        var data = {'opperation': 'RejectClaimLine','rejectionReason':str, 'id': new Date().getTime()};
        $.get(url,data, function(resultTxt) {
            if(resultTxt != null) {
               var resTxtArr = resultTxt.split("|");
                    var result = resTxtArr[0];
                    if (result == "Error") {
                        $("#rejCL_error").text(resTxtArr[1]);
                    } else if (result == "Done") {
                        $("#rejCL_error").text(resTxtArr[1]);
                    }
            }
            if (btn != undefined) {
                btn.disabled = false;
            }
        });
    } else {
        $("#rejCL_error").text("Rejection reason is required to complete this process");
        if (btn != undefined) {
            btn.disabled = false;
        }
    }
}

function submitReverseAndRejectAction(btn){ 
    if (btn != undefined) {
        btn.disabled = true;
    }
    $("#revAndRejectClaim_error").text("");
    var rejectionReason = $("#clRevAndRejReasonRej").val();
    var reversalReason = $("#clRevAndRejReasonRev").val();
    //alert("rejectionReason = " + rejectionReason);
    //alert("reversalReason = " + reversalReason);
    if(reversalReason !== null && reversalReason !== "" && reversalReason !== "99"){
        if(rejectionReason !== null && rejectionReason !== "" && rejectionReason !=="99"){
            var url = "/ManagedCare/AgileController";
            var data = {'opperation': 'ReverseAndRejectClaim','rejectionReason': rejectionReason, 'reversalReason': reversalReason, 'id': new Date().getTime()};
            $.get(url,data,function(resultTxt) {
               //alert("resultTxt = " + resultTxt); 
               if(resultTxt != null) {
                   var resTxtArr = resultTxt.split("|");
                   var result = resTxtArr[0];
                   if(result === "Error"){
                       $("#revAndRejectClaim_error").text(resTxtArr[1]);
                   } else if (result === "Done") {
                       $("#revAndRejectClaim_error").text(resTxtArr[1]);
                   } else {
                       window.open(resultTxt,'_blank');
                   }
               }
               if (btn != undefined) {
                    btn.disabled = false;
                }
            });
        } else {
            $("#revAndRejectClaim_error").text("Rejection reason is required to complete this process");
            if (btn != undefined) {
                btn.disabled = false;
            }
        }
    } else {
        $("#revAndRejectClaim_error").text("Reversal reason is required to complete this process");
        if (btn != undefined) {
            btn.disabled = false;
        }
    }
}

function submitReverseAndCorrectAction(btn){ 
    if (btn != undefined) {
        btn.disabled = true;
    }
    //alert("submitReverseAndCorrectAction");
    $("#revAndCorrectClaim_error").text("");
    var str = $("#clRevAndCorReason").val();
    var reverseWholeClaim = $('#clRevAndCor').attr('checked');
    if (str !== null && str !== "" && str !== "99") {
        var url = "/ManagedCare/AgileController";
        if(reverseWholeClaim === true){
            var data = {'opperation': 'ReverseAndCorrectClaim','reversalReason':str,'reverseAndCorrect': false, 'id': new Date().getTime()};
            $.get(url,data, function(resultTxt) {
                if(resultTxt != null) {
                    var resTxtArr = resultTxt.split("|");
                    var result = resTxtArr[0];
                    if (result == "Error") {
                        $("#revAndCorrectClaim_error").text(resTxtArr[1]);
                    } else if (result == "Done") {
                        $("#revAndCorrectClaim_error").text(resTxtArr[1]);
                    }
                }
                if (btn != undefined) {
                    btn.disabled = false;
                }
            });
        } else {
            var claimLineId = $("#memberDetailedClaimLineId").val();
            editClaimLine(null,claimLineId,'true');
        }
    } else {
        $("#revAndCorrectClaim_error").text("Reverse reason is required to complete this process");
        if (btn != undefined) {
            btn.disabled = false;
        }
    }
}

function submitDeleteAction(){ 
    //alert("submitDeleteAction");
}

function submitOverrideAction(btn){
    //alert("submitOverrideAction");
    $("#overCL_error").text("");
    var staleClaim = $("#overrideStale").attr('checked');
    if (btn != undefined) {
        btn.disabled = true;
    }
    
    var url = "/ManagedCare/AgileController";
    var data = {'opperation': 'OverrideClaim', 'staleClaim': staleClaim, 'id': new Date().getTime()};
    $.get(url,data, function(resultTxt) {
        if(resultTxt != null) {
           var resTxtArr = resultTxt.split("|");
                var result = resTxtArr[0];
                if (result == "Error") {
                    $("#overCL_error").text(resTxtArr[1]);
                } else if (result == "Done") {
                    $("#overCL_error").text(resTxtArr[1]);
                }
        }
        if (btn != undefined) {
            btn.disabled = false;
        }
    });
}

function submitReverseAction(btn) {
    //alert("validateProvider str = "+str);
    if (btn != undefined) {
        btn.disabled = true;
    }
    var str = $("#clRevReason").val();
    var reverseWholeClaim = $('#clRev').attr('checked');
    $("#revCL_error").text("");
    if (str !== null && str !== "" && str !== "99") {
        if(reverseWholeClaim == false){
            var url = "/ManagedCare/AgileController";
            var data = {'opperation': 'ReverseClaimLine', 'reversalReason': str, 'id': new Date().getTime()};
            //alert("url = "+url);
            $.get(url, data, function(resultTxt) {
                if (resultTxt != null) {
                    //alert("resultTxt = "+resultTxt);
                    var resTxtArr = resultTxt.split("|");
                    var result = resTxtArr[0];
                    if (result == "Error") {
                        $("#revCL_error").text(resTxtArr[1]);
                    } else if (result == "Done") {
                        $("#revCL_error").text(resTxtArr[1]);
                    }
                }
                if (btn != undefined) {
                    btn.disabled = false;
                }
            });
        } else {
            var url = "/ManagedCare/AgileController";
            var data = {'opperation': 'ReverseClaim', 'reversalReason': str, 'memClaimId': '', 'id': new Date().getTime()};
            //alert("url = "+url);
            $.get(url, data, function(resultTxt) {
                if (resultTxt != null) {
                    //alert("resultTxt = "+resultTxt);
                    var resTxtArr = resultTxt.split("|");
                    var result = resTxtArr[0];
                    if (result == "Error") {
                        $("#revCL_error").text(resTxtArr[1]);
                    } else if (result == "Done") {
                        $("#revCL_error").text(resTxtArr[1]);
                    }
                }
                if (btn != undefined) {
                    btn.disabled = false;
                }
            });
        }
    }else{
        $("#revCL_error").text("Reversal reason is required to complete this process");
        if (btn != undefined) {
            btn.disabled = false;
        }
    }
}

function editClaim(form){
    var url;
    url = "/ManagedCare/AgileController?opperation=EditClaimCommand&command=editClaim";
    $.ajax({
            url: url,
            type: "get",
            async: false,
            success: function(response){
                //alert(response);
                $('#Edit_Claims_Div').empty().html(response);
                $('#DetailedClaimLine_Div').hide();
                $('#Edit_Claims_Div').show();
                //alert("editClaimLine Done");
            }
    });
    var batchId = $('#ClaimHeader_batchId').val();
    //alert("batchID = " + batchId);
    var url = "/ManagedCare/AgileController?opperation=ClaimHeaderCommand&command=fetchBatchAndClaims&batchId=" + batchId + "&update=true";
        $.post(url, function(data) {
            updateFormFromData(data, 'ClaimHeader_Summary');
        });
}

function processClaimBatchUpdate(batchId){
    //processClaimBatch(batchId,true);
    //swapDivVisbible('Edit_Claims_Div', 'DetailedClaimLine_Div');
}

function reverseAndCorrectClaimLine(form, targetId, btn) {
    if (!validateClaimLine()) {
        displayNeoErrorNotification('There are validation errors');
        return;
    }
    if (btn != undefined) {
        btn.disabled = true;
    }
    if (btn != undefined) {
        btn.disabled = true;
    }
    
    $("#revAndCorrectClaim_error").text("");
    var reverseReason = $("#clRevAndCorReason").val();
    //alert("reverseReason = " + reverseReason);
    var claimLineId = $('#claimLineId').val();
    //alert("claimLineId = " + claimLineId)
    var url = "/ManagedCare/AgileController";
    var params = encodeFormForSubmit(form);
    //alert("params = " + params);
    params = params.replace("EditClaimCommand", "ReverseAndCorrectClaim");
    params = params + "&reversalReason=" + reverseReason + "&reverseAndCorrect=true";
    //alert("params = " + params);
    var data = {'opperation': 'ReverseAndCorrectClaim','reversalReason':reverseReason, 'reverseAndCorrect': true ,'params': params,'id': new Date().getTime()};
    $.get(url,params, function(resultTxt) {
        //alert("resultTxt = " + resultTxt);
        if(resultTxt != null) {
            var resTxtArr = resultTxt.split("|");
            var result = resTxtArr[0];
            swapDivVisbible('Edit_Claim_Lines_Div', 'DetailedClaimLine_Div');
            //alert("result = " + result);
            if (result == "Error") {
                $("#revAndCorrectClaim_error").text(resTxtArr[1]);
            } else if (result == "Done") {
                $("#revAndCorrectClaim_error").text(resTxtArr[1]);
            }
            
            if (btn != undefined) {
                btn.disabled = false;
            }
        }
    });
}

function exportClaim() {
    var claimId = $('#claimId').val();
    var url = "/ManagedCare/AgileController?opperation=EditClaimCommand&command=exportClaim";
    $.ajax({
            url: url,
            type: "get",
            success: function(response){
                //$("#exportClaim_error").text("File exported");
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(new Blob([response], {type: 'text/csv'}));
                a.download = "Claim_" + claimId + ".csv";

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);
            },
            error: function(response,errmsg, error) {
                $("#exportClaim_error").text("Error exporting claim.");
                //alert(response.toString());
            }
    });
}

function fetchClaimNotes(){
    //alert("fetchClaimNotes Called");
    var url = "/ManagedCare/AgileController?opperation=ClaimNotesCommand&command=FetchCaimNotes";
    $.get(url,function(data){
        updateFormFromData(data,"ClaimsNotes_Summary");
    });
}

function viewClaimNote(noteId, searched){
    //alert("viewClaimNote Called " + noteId);
    var url = "/ManagedCare/AgileController?opperation=ClaimNotesCommand&command=ViewNote&noteId=" + noteId;
    $.get(url, function(response){
            $('#Claim_Notes_View_Div').empty().html(response);
            $('#Claim_Notes_View_Div').show();
            $('#DetailedClaimNotes_Div').hide();
            $('#Search_Notes_Div').hide();
            $('#Claim_Notes_Add_Div').hide();
    });
    if (searched){
        $('#ClaimNotes_SearchBachNumber').val('');
        $('#ClaimNotes_SearchClaimNumber').val('');
        $('#ClaimNotes_SearchPracticeNumber').val('');
        $('#ClaimNotes_SearchCoverNumber').val('');
    }
}

function addClaimNote(){
    //alert("addClaimNote Called");
    var url = "/ManagedCare/AgileController?opperation=ClaimNotesCommand&command=AddNote";
    $.get(url, function(response){
            $('#Claim_Notes_Add_Div').empty().html(response);
            $('#Claim_Notes_Add_Div').show();
            $('#DetailedClaimNotes_Div').hide();
    });
    
    var url = "/ManagedCare/AgileController?opperation=ClaimNotesCommand&command=FetchCaimNotes";
    $.get(url,function(data){
        updateFormFromData(data,"ClaimsNotes_Summary");
    });
 
}

function returnToViewNotes(){
    var url = "/ManagedCare/AgileController?opperation=ClaimNotesCommand&command=ReturnToViewNotes";
    $.get(url, function(response){
            $('#DetailedClaimNotes_Div').empty().html(response);
            $('#DetailedClaimNotes_Div').show();
            $('#Claim_Notes_Add_Div').hide();
    });
}

function saveClaimNote(form, targetId, btn) {
    
    if (btn != undefined) {
        btn.disabled = true;
    }
    if (btn != undefined) {
        btn.disabled = true;
    }
    var params = encodeFormForSubmit(form);
    //alert("params = " + params);
    var noteDetails = CKEDITOR.instances.notes.getData();
    //var noteDetails = CKEDITOR.instances.notes.editable().getText();
    CKEDITOR.instances.notes.setData('');
    noteDetails = getPlainText(noteDetails);
    params = params += "&noteDetails=" + noteDetails;
    var formId = $(form).attr('id');
    if(noteDetails === ""){
        alert("please enter a note");
        if (btn != undefined) {
            btn.disabled = false;
        }
        return;
    }
    //$(form).mask("Processing...");
    $.post(form.action, params, function(result) {
        displayNeoSuccessNotification('Claim Note Saved');
        $('#notes').val('');
        //alert("result nappi = " + result + "\ntargetId = " + targetId);
        updateFormFromData(result, targetId);

        var url = "/ManagedCare/AgileController?opperation=ClaimNotesCommand&command=FetchCaimNotes";
        $.post(url, function(data) {
            //alert("fetchNappiLines data = " + data);
            updateFormFromData(data, 'ClaimsNotes_Summary');
        });

        /*var batchId = $('#ClaimHeader_batchId').val();
         var url = "/ManagedCare/AgileController?opperation=ClaimHeaderCommand&command=fetchBatchAndClaims&batchId=" + batchId;
         $.post(url, function(data) {
         updateFormFromData(data, 'ClaimHeader_Summary');
         });*/

        if (btn != undefined) {
            btn.disabled = false;
        }
        if ($('#' + formId).unmask != null)
            $(form).unmask();
    });
}

function getPlainText( strSrc ) {
    var resultStr = "";

    // Ignore the <p> tag if it is in very start of the text
    if(strSrc.indexOf('<p>') == 0)
        resultStr = strSrc.substring(3);
    else
        resultStr = strSrc;

    // Replace <p> with one newline
    resultStr = resultStr.replace(/<p>/gi, "\r\n");
    // Replace <br /> with one newline
    resultStr = resultStr.replace(/<br \/>/gi, "\r\n");
    resultStr = resultStr.replace(/<br>/gi, "\r\n");
    //replace &nbsp with a blank
    resultStr = resultStr.replace(/^(&nbsp;|<br>)+/, '');
    resultStr = resultStr.replace(/&nbsp;/g, ''); 
    //replace &quot; with a \"
    resultStr = resultStr.replace(/&quot;/g,'\"');

    //-+-+-+-+-+-+-+-+-+-+-+
    // Strip off other HTML tags.
    //-+-+-+-+-+-+-+-+-+-+-+
    resultStr = escape(resultStr); 
    return  resultStr.replace( /<[^<|>]+?>/gi,'' );
}

function uploadClaimDocument(providerNumber, memberNumber, batchId, claimId, fileName, save) {
    var documents = document.getElementById("ClaimHeader_ClaimDocument");
    var file = documents.files[0];

    var url = "/ManagedCare/AgileController?opperation=SubmitClaimDocumentCommand&command=save&batchId=" + batchId + "&memberNumber=" + memberNumber + "&providerNumber=" + providerNumber + "&claimId=" + claimId + "&fileName=" + fileName + "&file=" + file + "&documents=" + documents;
    $.post(url);
    document.getElementById("ClaimHeader_ClaimDocument").value = "";


    //console.log("File name: " + file.name);
    //console.log("File size: " + file.size);
}

function changeRecipient(claimId, recipient, batchId){
    var url = "/ManagedCare/AgileController?opperation=EditClaimCommand&command=changeRecipient&claimId=" + claimId + "&recipient=" + recipient;
    $.post(url, function(result) {
        if(result == "true"){
            alert("Recipient Updated successfully");
            url = "/ManagedCare/AgileController?opperation=ClaimHeaderCommand&command=fetchBatchAndClaims&batchId=" + batchId + "&update=true";
            $.post(url, function(data) {
                updateFormFromData(data, 'ClaimHeader_Summary');
            });
        } else {
            alert("Problem with updating of Recipient");
        }
    });
    
    
}

function checkDateTo(){
    var end = $('#ClaimLine_DateEnd').val();
    var start = $('#ClaimLine_DateStart').val();
    if(start > end){
        $('#ClaimLine_DateEnd_error').text('Service To Date Cannot Be Before Service From Date');
        return false;
    }
    return true;
}
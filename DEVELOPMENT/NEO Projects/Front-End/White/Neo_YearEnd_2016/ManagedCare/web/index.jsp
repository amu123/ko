<%-- 
    Document   : index
    Created on : 2009/09/28, 10:48:48
    Author     : gerritj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<HTML>
    <HEAD>
        <c:if test="${applicationScope.Client == 'Sechaba'}">
            <link rel="icon" type="image/x-icon" href="./resources/sechabafavicon.ico" />
        </c:if>
        <TITLE>Managed Care</TITLE>
        <script type='text/javascript' src="./resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" language="JavaScript">
            try {
                JSON;
                $(function () {
                    var url = "${pageContext.request.contextPath}/AgileController";
                    var data = {};
                    $.get(url, data, function (resultTxt) {});
                    var data = null;
                });
            } catch (e) {
                alert("Compatibility View Detected\n\n*Please ensure that Compatibility View is turned off.");
            }
        </script>
    </HEAD>
    <!--<FRAMESET rows="190, 100%, 30">-->

    <FRAMESET rows="130, 100%">
        <FRAME id="header" name="header" src="Header.jsp" frameborder="0" scrolling="no"/>
        <FRAMESET id="workflowFrameset" framespacing="0" name="workflowFrameset" COLS="0, 100%">
            <FRAME id="workflow" name="workflow" src="Workflow.jsp" frameborder="0" scrolling="no"/>
            <FRAMESET cols="220, 100%" >
                <FRAME id="leftMenu" name="leftMenu" src="LeftMenu.jsp" frameborder="0"/>
                <FRAME id="worker" name="worker" src="Login.jsp" frameborder="0"/>
            </FRAMESET>
        </FRAMESET>
    </FRAMESET>
</HTML>

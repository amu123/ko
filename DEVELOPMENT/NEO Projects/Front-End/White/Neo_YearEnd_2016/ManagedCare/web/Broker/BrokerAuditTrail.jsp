<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="BrokerFirmAuditForm">
    <input type="hidden" name="opperation" id="BrokerAudit_opperation" value="ViewBrokerAuditTrailCommand" />
    <input type="hidden" name="onScreen" id="BrokerAudit_onScreen" value="BrokerAudit" />
    <input type="hidden" name="brokerEntityId" id="brokerAuditEntityId" value="${requestScope.brokerEntityId}"
    <input type="hidden" name="brokerTile" id="brokerAuditTile" value="${requestScope.brokerTile}" />
    <agiletags:HiddenField elementName="EmployerAudit_employerEntityId"/>
    <label class="header">Broker Audit Trail</label>
    <hr>

      <table id="EmployerAuditTable">
          <tr>
              <td width="160" style="font-size: 1.0em;">Details</td>
              <td>
                  <select style="width:215px" name="auditList" id="auditList" onChange="submitFormWithAjaxPost(this.form, 'FirmAuditDetails');">
                      <option value="99" selected>&nbsp;</option>
                      <option value="BROKER_DETAILS">Broker Details</option>
                      <option value="ADDRESS_DETAILS">Address Details</option>
                      <option value="CONTACT_DETAILS">Contact Details</option>
                      <option value="BROKER_ACCREDITATION_DETAILS">Accreditation Details</option>
                      <option value="BROKER_HISTORY">History Details</option>
                      <option value="BROKER_CONSULTANT_DETAILS">Broker Consultant Details</option>
                  </select>
              </td>
                  
          </tr>
      </table>          
 </agiletags:ControllerForm>
 <div style ="display: none"  id="FirmAuditDetails"></div>
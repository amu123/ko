
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
    <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
    <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
    <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
    <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
    <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
    <!--<script type='text/javascript' src="${pageContext.request.contextPath}/resources/LoadBrokerCoverDetailsCommand.js"></script>-->
    
<!--    <script>
        
function exportRun(btn) {
    console.log("SSSSS");
    var url = "/ManagedCare/AgileController?opperation=LoadBrokerCoverDetailsCommand";
    $.get(url, function (response) {
        //var result = resultTxt.trim();
        
        var a = window.document.createElement('a');

        a.href = window.URL.createObjectURL(new Blob([response], {type: 'text/csv'}));
        a.download = "exportPayRun.csv";

        // Append anchor to body.
        document.body.appendChild(a);
        a.click();

        // Remove anchor from body
        document.body.removeChild(a);

    });
}

    </script>-->
    <agiletags:ControllerForm name="BrokerDetailsForm">
        <input type="hidden" name="opperation" id="opperation" value="LoadBrokerCoverDetailsCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="MemberDependantApp" />
        <input type="hidden" name="command" id="BrokerSearchCommand" value="BrokerSearch" />
        <input type="hidden" name="memberAppNumber" id="memberAppDepNumber" value="${memberAppNumber}" />
        <input type="hidden" name="entityId" id="entityId" value="${memberCoverEntityId}" />
        <input type="hidden" name="buttonPressed" id="memberAppDepButtonPressed" value="" />
        <input type="hidden" name="depNo" id="memberAppDepNo" value="" />
        <input type="hidden" name="maxDep" value="${maxDep}" />
        <input type="hidden" name="brokerEntityId" id="brokerAccreEntityId" value="${requestScope.brokerEntityId}" />
        <input type="hidden" name="brokerTile" id="brokerAccreTile" value="${requestScope.brokerEntityId}" />
        <input type="hidden" name="brokerContactPerson" id="brokerContactContactPerson" value="${requestScope.brokerEntityId}" />
        <input type="hidden" name="brokerRegion" id="brokerCovRegion" value="${requestScope.brokerRegion}" />
<!--        <input type="hidden" name="exp_start_date" id="exp_start_date" value="" />
        <input type="hidden" name="exp_end_date" id="exp_end_date" value="" />-->
        <!--<input type="hidden" name="buttonPressedExport" id="buttonPressedExport" value="Export" />-->

        <label class="header">Cover Details</label>
        <br>
        <br>
        <table id="depListTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%">
            <tr>
                <th>Member Number</th>
                <th>Name & Surname</th>
                <th>Initials</th>
                <th>ID Number</th>
                <th>Status</th>
                <th>Start Date</th>
                <th>End Date</th>                         
            </tr>
            <c:forEach var="entry" items="${accreList}">

                <tr>
                    <td class="label">${entry.coverNumber}</td>
                    <td class="label">${entry.memberName} ${entry.memberSurname}</td>
                    <td class="label">${entry.memberInitial}</td>
                    <td class="label">${entry.memberIdNumber}</td>
                    <td class="label">${entry.status == "1" ? "Active" : entry.status == "2" ? "Suspend" : entry.status == "3" ? "Pended" : "Resign"}</td>
                    <td class="label"><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveStartDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.day}"/></td>
                    <td class="label"><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveEndDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.day}"/></td>                            
                </tr>

            </c:forEach>
        </table>
        <!--        <input type="hidden" name="opperation" id="opperationExp" value="LoadBrokerCoverDetailsCommand" />
                <input type="hidden" name="exp_start_date" id="exp_start_date" value="" />
                <input type="hidden" name="exp_end_date" id="exp_end_date" value="" />
                <input type="hidden" name="buttonPressedExport" id="buttonPressedExport" value="Export" />-->
        <!--<td><input type="submit" value="Export To Excel" onclick="exportRun(this)"></td>-->
        <agiletags:ButtonOpperationLabelError displayName="Export To Excel" elementName="exportRun" type="button" javaScript="onClick=\"exportRun(this);\""/>
    </agiletags:ControllerForm>
</html>
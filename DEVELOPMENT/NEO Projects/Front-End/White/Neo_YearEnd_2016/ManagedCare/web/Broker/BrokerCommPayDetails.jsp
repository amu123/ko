<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<br>
        <label class="subheader">Broker Commission Payment Details</label>
        <c:choose>
            <c:when test="${empty CommissionDetails}">
                <br>
                <span><label>No Commission details available</label></span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Payment Date</th>
                        <th>Contribution Date</th>
                        <th>Member Number</th>
                        <th>Member Name</th>
                        <th>Commission</th>
                        <th>Vat</th>
                        <th>Paid</th>
                    </tr>
                    <c:set var="groupTest" value=""></c:set>
                    <c:forEach var="entry" items="${CommissionDetails}">
                        <c:if test="${groupTest != entry.employerCode}">
                            <tr>
                                <td colspan="7"><label><b>${entry.employerCode} - ${entry.employerName}</b></label></td>
                            </tr>
                        </c:if>
                        <tr>
                            <td><label>${entry.chequeRunDate}</label></td>
                            <td><label>${entry.contribDate}</label></td>
                            <td><label>${entry.coverNumber}</label></td>
                            <td><label>${entry.memberName}</label></td>
                            <td align="right"><label>${agiletags:formatStringAmount(entry.commAmount)}</label></td>
                            <td align="right"><label>${agiletags:formatStringAmount(entry.commVat)}</label></td>
                            <td align="right"><label>${agiletags:formatStringAmount(entry.commPaid)}</label></td>
                        </tr>
                        <c:set var="groupTest" value="${entry.employerCode}"></c:set>
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>

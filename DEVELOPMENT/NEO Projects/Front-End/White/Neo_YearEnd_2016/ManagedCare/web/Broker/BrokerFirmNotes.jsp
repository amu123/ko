<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="BroherFirmAppNotesForm">
    <input type="hidden" name="opperation" id="BrokerFirmAppNotes_opperation" value="BrokerFirmNotesCommand" />
    <input type="hidden" name="onScreen" id="BrokerFirmAppNotes_onScreen" value="BrokerFirmAppNotes" />
    <input type="hidden" name="memberAppNumber" value="${memberAppNumber}" />
    <input type="hidden" name="noteId" id="BrokerFirmAppNotes_note_id" value="" />
    <input type="hidden" name="buttonPressed" id="BrokerFirmAppNotes_button_pressed" value="" />
     <input type="hidden" name="brokerFirmEntityId" id="brokerFirmANotesEntityId" value="${brokerFirmEntityId}" />
    <label class="header">Broker Firm Notes</label>
    <hr>

      <table id="BrokerFirmAppNotesTable">
          <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Details"  elementName="notesListSelect" lookupId="215" mandatory="no" errorValueFromSession="no" javaScript="onchange=\"document.getElementById('BrokerFirmAppNotes_button_pressed').value='SelectButton';submitFormWithAjaxPost(this.form, 'BrokerFirmAppNotesDetails');\"" />        </tr>
      </table>          
 <div style ="display: none"  id="BrokerFirmAppNotesDetails"></div>
 </agiletags:ControllerForm>
   
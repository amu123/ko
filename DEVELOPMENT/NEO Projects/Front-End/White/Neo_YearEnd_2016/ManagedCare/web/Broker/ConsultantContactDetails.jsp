
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="MemberApplicationForm">
    <input type="hidden" name="opperation" id="CussultantApp_opperation" value="SaveConsultantContactDetailsCommand" />
    <input type="hidden" name="onScreen" id="MemberApp_onScreen" value="Consultant Application" />
    <input type="hidden" name="consultantEntityId" id="consultantContactEntityId" value="${requestScope.consultantEntityId}" />
    <input type="hidden" name="consultantRegion" id="consultantConRegion" value="${requestScope.consultantRegion}" />


    <agiletags:HiddenField elementName="MemberApp_app_number"/>

    <label class="subheader">Consultant Contact Details</label>
    <hr>
      <table id="BrokerContactDetailsTable">        
        <tr id="ConsApp_ContactDetails_telNoRow"><agiletags:LabelTextBoxErrorReq displayName="Telephone Number" elementName="ConsApp_ContactDetails_telNo" valueFromRequest="ConsApp_ContactDetails_telNo" mandatory="ConsApp_ContactDetails_telNo"/>        </tr>
        <tr id="ConsApp_ContactDetails_faxNoRow"><agiletags:LabelTextBoxErrorReq displayName="Fax No" elementName="ConsApp_ContactDetails_faxNo" valueFromRequest="ConsApp_ContactDetails_faxNo" mandatory="no"/>        </tr>
        <tr id="ConsApp_ContactDetails_emailRow"><agiletags:LabelTextBoxErrorReq displayName="Email Address" elementName="ConsApp_ContactDetails_email" valueFromRequest="ConsApp_ContactDetails_email" mandatory="no"/>        </tr>
        <tr id="ConsApp_ContactDetails_altEmailRow"><agiletags:LabelTextBoxErrorReq displayName="Alternative Email Address" elementName="ConsApp_ContactDetails_altEmail" valueFromRequest="ConsApp_ContactDetails_altEmail" mandatory="no"/>        </tr>
        <tr id="ConsApp_brokerRegionRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Region"  elementName="ConsApp_ContactDetails_region" lookupId="251" mandatory="no" errorValueFromSession="yes" sort="yes"/>        </tr>
      </table>

    <br>
    <label class="subheader">Postal Address</label>
    <hr>
      <table id="BrokerPostalAddressTable">
        <tr id="ConsApp_PostalAddress_addressLine1Row"><agiletags:LabelTextBoxErrorReq displayName="Line 1" elementName="ConsApp_PostalAddress_addressLine1" valueFromRequest="ConsApp_PostalAddress_addressLine1" mandatory="no"/>        </tr>
        <tr id="ConsApp_PostalAddress_addressLine2Row"><agiletags:LabelTextBoxErrorReq displayName="Line 2" elementName="ConsApp_PostalAddress_addressLine2" valueFromRequest="ConsApp_PostalAddress_addressLine2" mandatory="no"/>        </tr>
        <tr id="ConsApp_PostalAddress_addressLine3Row"><agiletags:LabelTextBoxErrorReq displayName="Line 3" elementName="ConsApp_PostalAddress_addressLine3" valueFromRequest="ConsApp_PostalAddress_addressLine3" mandatory="no"/>        </tr>
        <tr id="ConsApp_PostalAddress_postalCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Postal Code" elementName="ConsApp_PostalAddress_postalCode" valueFromRequest="ConsApp_PostalAddress_postalCode" mandatory="no"/>        </tr>
      </table>

    <br>
    <label class="subheader">Physical Address</label>
    <hr>
      <table id="BrokerPhysicalAddressTable">
        <tr id="ConsApp_ResAddress_addressLine1Row"><agiletags:LabelTextBoxErrorReq displayName="Line 1" elementName="ConsApp_ResAddress_addressLine1" valueFromRequest="ConsApp_ResAddress_addressLine1" mandatory="no"/>        </tr>
        <tr id="ConsApp_ResAddress_addressLine2Row"><agiletags:LabelTextBoxErrorReq displayName="Line 2" elementName="ConsApp_ResAddress_addressLine2" valueFromRequest="ConsApp_ResAddress_addressLine2" mandatory="no"/>        </tr>
        <tr id="ConsApp_ResAddress_addressLine3Row"><agiletags:LabelTextBoxErrorReq displayName="Line 3" elementName="ConsApp_ResAddress_addressLine3" valueFromRequest="ConsApp_ResAddress_addressLine3" mandatory="no"/>        </tr>
        <tr id="ConsApp_ResAddress_physicalCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Physical Code" elementName="ConsApp_ResAddress_postalCode" valueFromRequest="ConsApp_ResAddress_postalCode" mandatory="no"/>        </tr>
      </table>

    
    <hr>
    <table id="ConsAppSaveTable">
        <tr>
           <td><input type="reset" value="Reset"></td>
           <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'SaveConsultantContactDetailsCommand')"></td>
        </tr>
    </table>

  </agiletags:ControllerForm>



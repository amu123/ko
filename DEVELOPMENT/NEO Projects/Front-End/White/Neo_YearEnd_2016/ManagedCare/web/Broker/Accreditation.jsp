
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<agiletags:ControllerForm name="BrokerFirmDependantAppForm">
    <input type="hidden" name="opperation" id="opperation" value="AddBrokerFirmAccreditationCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberDependantApp" />
    <input type="hidden" name="memberAppNumber" id="memberAppDepNumber" value="${memberAppNumber}" />
    <input type="hidden" name="buttonPressed" id="memberAppDepButtonPressed" value="" />
    <input type="hidden" name="depNo" id="memberAppDepNo" value="" />
    <input type="hidden" name="maxDep" value="${maxDep}" />
    <input type="hidden" name="brokerFirmEntityId" id="brokerFirmAccreEntityId" value="${requestScope.brokerFirmEntityId}" />
    <input type="hidden" name="brokerFirmTile" id="brokerFirmAccreTile" value="${requestScope.brokerFirmTile}" />
    <input type="hidden" name="brokerFirmContactPerson" id="brokerFirmContactContactPerson" value="${requestScope.brokerFirmContactPerson}" />
    <input type="hidden" name="brokerFirmContactSurname" id="brokerFirmContactContactSurname" value="${requestScope.brokerFirmContactSurname}" />
    <input type="hidden" name="firmRegion" id="firmRegionAcc" value="${requestScope.firmRegion}" />
    <input type="hidden" name="selectedBrokerAccredIDFlag" id="selectedBrokerAccredID" value="" />
    <input type="hidden" name="accreditationTypeFlag" id="accreditationType" value="" />
    <input type="hidden" name="accreditationNumberFlag" id="accreditationNumber" value="" />
    <input type="hidden" name="applicationDateFlag" id="applicationDate" value="" />
    <input type="hidden" name="authorizationDateFlag" id="authorizationDate" value="" />
    <input type="hidden" name="startDateFlag" id="startDate" value="" />
    <input type="hidden" name="endDateFlag" id="endDate" value="" />

    <label class="header">Accreditation</label>
    <br>
    <input type="button" name="AddDependantButton" value="Add Accreditation" onclick="document.getElementById('memberAppDepButtonPressed').value = this.value;
        getPageContents(this.form, null, 'main_div', 'overlay');"/>
    <br>
    <table id="depListTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%">
        <tr>
            <th>Accreditation Types</th>
            <th>Accreditation Number</th>
            <th>Application Date</th>
            <th>Authorization Date</th>
            <th>Start Date</th>
            <th>End Date</th>       
            <th>Edit</th>
            <th>Remove</th>
        </tr>
        <c:forEach var="entry" items="${accreList}">
            <tr>
                <td class="label">${entry.accreditationType == 1 ? "FSB registration" : entry.accreditationType == 2 ? "Council accreditation" : ""}</td>
                <td class="label">${entry.accreditationNumber}</td>
                <td class="label">${agiletags:formatXMLGregorianDate(entry.applicationDate)}</td>
                <td class="label">${agiletags:formatXMLGregorianDate(entry.authorisationDate)}</td>
                <td class="label"><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveStartDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.day}"/></td>
                <td class="label"><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveEndDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.day}"/></td>
                <td class="label"><input type="button" name="EditBrokerButton" value="Edit" onclick="document.getElementById('memberAppDepButtonPressed').value = this.value; document.getElementById('selectedBrokerAccredID').value = '${entry.brokerAccredId}'; document.getElementById('accreditationType').value = '${entry.accreditationType}'; document.getElementById('accreditationNumber').value = '${entry.accreditationNumber}'; document.getElementById('applicationDate').value = '${agiletags:formatXMLGregorianDate(entry.applicationDate)}'; document.getElementById('authorizationDate').value = '${agiletags:formatXMLGregorianDate(entry.authorisationDate)}'; document.getElementById('startDate').value = '<fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveStartDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.day}"/>'; document.getElementById('endDate').value = '<fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveEndDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.day}"/>'; getPageContents(this.form,null,'main_div','overlay');"/></td>
                <td class="label"><input type="button" name="RemoveBrokerButton" value="Remove" onclick="document.getElementById('memberAppDepButtonPressed').value = this.value; document.getElementById('selectedBrokerAccredID').value = '${entry.brokerAccredId}'; document.getElementById('accreditationType').value = '${entry.accreditationType}'; document.getElementById('accreditationNumber').value = '${entry.accreditationNumber}'; document.getElementById('applicationDate').value = '${agiletags:formatXMLGregorianDate(entry.applicationDate)}'; document.getElementById('authorizationDate').value = '${agiletags:formatXMLGregorianDate(entry.authorisationDate)}'; document.getElementById('startDate').value = '<fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveStartDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.day}"/>'; document.getElementById('endDate').value = '<fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveEndDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.day}"/>'; submitFormWithAjaxPost(this.form, 'Accreditation', null, '${main_div}', '${target_div}');"/></td>
            </tr>
        </c:forEach>
    </table>
    <div style="display:none">
        <table>                
            <tr id="FrimApp_brokerAccreditationTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Accreditation Type"  elementName="FrimApp_brokerAccreditationType_" lookupId="210" mandatory="yes" errorValueFromSession="yes"/></tr>
            <tr id="FirmrApp_brokerAccreditationNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Accreditation Number" elementName="FirmrApp_brokerAccreditationNumber_" valueFromRequest="FirmrApp_brokerAccreditationNumber" mandatory="yes"/></tr>
            <tr id="FirmApp_brokerApplicationDateRow"><agiletags:LabelTextBoxDateReq  displayname="Application Date" elementName="FirmApp_brokerApplicationDate_" valueFromSession="yes" mandatory="no"/></tr> 
            <tr id="FirmApp_brokerAuthorisationDateRow"><agiletags:LabelTextBoxDateReq  displayname="Authorisation Date" elementName="FirmApp_brokerAuthorisationDate_" valueFromSession="yes" mandatory="no"/></tr>        
            <tr id="FirmApp_brokerStartDateRow"><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="FirmApp_brokerStartDate_" valueFromSession="yes" mandatory="yes"/></tr> 
            <tr id="FirmApp_brokerEndDateRow"><agiletags:LabelTextBoxDateReq  displayname="End Date" elementName="FirmApp_brokerEndDate_" valueFromSession="yes" mandatory="yes"/></tr> 
        </table>
    </div>
</agiletags:ControllerForm>

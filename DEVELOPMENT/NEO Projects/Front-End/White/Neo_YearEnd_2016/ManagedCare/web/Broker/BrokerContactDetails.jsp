<%-- 
    Document   : BrokerContactDetails
    Created on : 2012/06/18, 11:37:49
    Author     : princes
--%>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="MemberApplicationForm">
    <input type="hidden" name="opperation" id="MemberApp_opperation" value="SaveBrokerContactDetailsCommand" />
    <input type="hidden" name="onScreen" id="MemberApp_onScreen" value="Member Application" />
    <input type="hidden" name="brokerEntityId" id="brokerContactEntityId" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="brokerTitle" id="brokerTitle" value="${requestScope.brokerTitle}" />
    <input type="hidden" name="brokerContactPerson" id="brokerCPerson" value="${requestScope.brokerContactPerson}" />
    <input type="hidden" name="brokerContactSurname" id="brokerCPerson" value="${requestScope.brokerContactSurname}" />
    <input type="hidden" name="brokerRegion" id="brokerContactRegion" value="${requestScope.brokerRegion}" />

    <agiletags:HiddenField elementName="MemberApp_app_number"/>

    <label class="subheader">Broker Contact Details</label>
    <hr>
      <table id="BrokerContactDetailsTable">
        <tr id="BrokerApp_ContactDetails_titleRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Title"  elementName="BrokerApp_ContactDetails_title" lookupId="24" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="BrokerApp_ContactDetails_contactPersonRow"><agiletags:LabelTextBoxErrorReq displayName="Contact Person" elementName="BrokerApp_ContactDetails_contactPerson" valueFromRequest="BrokerApp_ContactDetails_contactPerson" mandatory="no"/>        </tr>
        <tr id="BrokerApp_ContactDetails_telNoRow"><agiletags:LabelTextBoxErrorReq displayName="Telephone Number" elementName="BrokerApp_ContactDetails_telNo" valueFromRequest="BrokerApp_ContactDetails_telNo" mandatory="BrokerApp_ContactDetails_telNo"/>        </tr>
        <tr id="BrokerApp_ContactDetails_faxNoRow"><agiletags:LabelTextBoxErrorReq displayName="Fax No" elementName="BrokerApp_ContactDetails_faxNo" valueFromRequest="BrokerApp_ContactDetails_faxNo" mandatory="no"/>        </tr>
        <tr id="BrokerApp_ContactDetails_emailRow"><agiletags:LabelTextBoxErrorReq displayName="Email Address" elementName="BrokerApp_ContactDetails_email" valueFromRequest="BrokerApp_ContactDetails_email" mandatory="no"/>        </tr>
        <tr id="BrokerApp_ContactDetails_altEmailRow"><agiletags:LabelTextBoxErrorReq displayName="Alternative Email Address" elementName="BrokerApp_ContactDetails_altEmail" valueFromRequest="BrokerApp_ContactDetails_altEmail" mandatory="no"/>        </tr>
        <tr id="BrokerApp_ContactDetails_altEmailRow"><agiletags:LabelTextBoxErrorReq displayName="Cell" elementName="BrokerApp_ContactDetails_cell" valueFromRequest="BrokerApp_ContactDetails_cell" mandatory="no"/>        </tr>
        <tr id="BrokerApp_ContactDetails_regionRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Region"  elementName="BrokerApp_ContactDetails_region" lookupId="251" mandatory="no" errorValueFromSession="yes" sort="yes"/>        </tr>
      </table>

    <br>
    <label class="subheader">Postal Address</label>
    <hr>
      <table id="BrokerPostalAddressTable">
        <tr id="BrokerApp_PostalAddress_addressLine1Row"><agiletags:LabelTextBoxErrorReq displayName="Line 1" elementName="BrokerApp_PostalAddress_addressLine1" valueFromRequest="BrokerApp_PostalAddress_addressLine1" mandatory="no"/>        </tr>
        <tr id="BrokerApp_PostalAddress_addressLine2Row"><agiletags:LabelTextBoxErrorReq displayName="Line 2" elementName="BrokerApp_PostalAddress_addressLine2" valueFromRequest="BrokerApp_PostalAddress_addressLine2" mandatory="no"/>        </tr>
        <tr id="BrokerApp_PostalAddress_addressLine3Row"><agiletags:LabelTextBoxErrorReq displayName="Line 3" elementName="BrokerApp_PostalAddress_addressLine3" valueFromRequest="BrokerApp_PostalAddress_addressLine3" mandatory="no"/>        </tr>
        <tr id="BrokerApp_PostalAddress_postalCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Postal Code" elementName="BrokerApp_PostalAddress_postalCode" valueFromRequest="BrokerApp_PostalAddress_postalCode" mandatory="no"/>        </tr>
      </table>

    <br>
    <label class="subheader">Physical Address</label>
    <hr>
      <table id="BrokerPhysicalAddressTable">
        <tr id="BrokerApp_ResAddress_addressLine1Row"><agiletags:LabelTextBoxErrorReq displayName="Line 1" elementName="BrokerApp_ResAddress_addressLine1" valueFromRequest="BrokerApp_ResAddress_addressLine1" mandatory="no"/>        </tr>
        <tr id="BrokerApp_ResAddress_addressLine2Row"><agiletags:LabelTextBoxErrorReq displayName="Line 2" elementName="BrokerApp_ResAddress_addressLine2" valueFromRequest="BrokerApp_ResAddress_addressLine2" mandatory="no"/>        </tr>
        <tr id="BrokerApp_ResAddress_addressLine3Row"><agiletags:LabelTextBoxErrorReq displayName="Line 3" elementName="BrokerApp_ResAddress_addressLine3" valueFromRequest="BrokerApp_ResAddress_addressLine3" mandatory="no"/>        </tr>
        <tr id="BrokerApp_ResAddress_physicalCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Physical Code" elementName="BrokerApp_ResAddress_postalCode" valueFromRequest="BrokerApp_ResAddress_postalCode" mandatory="no"/>        </tr>
      </table>

    
    <hr>
    <table id="BrokerAppSaveTable">
        <tr>
           <td><input type="reset" value="Reset"></td>
           <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'SaveBrokerContactDetailsCommand')"></td>
        </tr>
    </table>

  </agiletags:ControllerForm>



<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="MemberApplicationForm">
    <input type="hidden" name="opperation" id="MemberApp_opperation" value="SaveBankingDetailsCommand" />
    <input type="hidden" name="onScreen" id="MemberApp_onScreen" value="Broker Application" />
    <input type="hidden" name="brokerEntityId" id="brokerDetailsEntityId" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="brokerTitle" id="brokerTitle" value="${requestScope.brokerTitle}" />
    <input type="hidden" name="brokerContactPerson" id="brokerCPerson" value="${requestScope.brokerContactPerson}" />
    <input type="hidden" name="brokerContactSurname" id="brokerCSurname" value="${requestScope.brokerContactSurname}" />
    <input type="hidden" name="brokerRegion" id="brokerBRegion" value="${requestScope.brokerRegion}" />

    <agiletags:HiddenField elementName="MemberApp_app_number"/>

    <label class="subheader">Broker Banking Details</label>
    <hr>
      <table id="BrokerBankingTable">
        <tr id="BrokerBankApp_bankRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Name of Bank"  elementName="BrokerBankApp_bank" lookupId="bank" mandatory="yes" errorValueFromSession="yes" javaScript="onChange=\"toggleBranch(this.value, 'BrokerBankApp_branch')\""/>        </tr>
        <tr id="BrokerBankApp_branchRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Branch"  elementName="BrokerBankApp_branch" lookupId="branch" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="BrokerBankApp_accountTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Account Type"  elementName="BrokerBankApp_accountType" lookupId="37" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="BrokerBankApp_accountHolderRow"><agiletags:LabelTextBoxErrorReq displayName="Account Holder" elementName="BrokerBankApp_accountHolder" valueFromRequest="BrokerBankApp_accountHolder" mandatory="yes"/>        </tr>
        <tr id="BrokerBankApp_accountNoRow"><agiletags:LabelTextBoxErrorReq displayName="Account Number" elementName="BrokerBankApp_accountNo" valueFromRequest="BrokerBankApp_accountNo" mandatory="yes"/>        </tr>
      </table>

    
    <hr>
    <table id="BrokerAppSaveTable">
        <tr>
           <td><input type="reset" value="Reset"></td>
           <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'SaveBankingDetailsCommand')"></td>
        </tr>
    </table>

  </agiletags:ControllerForm>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Firm Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty FirmSearchResults}">
            <span>No Firm found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Firm Code</th>
                        <th>Name</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${FirmSearchResults}">
                        <tr>
                            <td><label>${entry.brokerFirmCode}</label></td>
                            <td><label>${entry.brokerFirmName}</label></td>
                            <td><input type="button" value="Select" onclick="setElemValues({'${diagCodeId}':'${entry.brokerFirmCode}','${diagNameId}':'${agiletags:escapeStr(entry.brokerFirmName)}','brokerFirmEntityId':'${entry.entityId}'});updateFormFromData(' ','${target_div}');swapDivVisbible('${target_div}','${main_div}');"/></td> 
                        </tr>
                    </c:forEach>

                </table>
            </c:otherwise>
        </c:choose>
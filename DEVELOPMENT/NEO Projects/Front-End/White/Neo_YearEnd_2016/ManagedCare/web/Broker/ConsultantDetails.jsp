
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="ConsultantDetailsForm">
    <input type="hidden" name="opperation" id="ConsultantApp_opperation" value="SaveConsultantApplicationCommand" />
    <input type="hidden" name="onScreen" id="ConsultantApp_onScreen" value="Borker Consultant Application" />
    <input type="hidden" name="consultantEntityId" id="consultantDetailsEntityId" value="${requestScope.consultantEntityId}" />
    <input type="hidden" name="consultantRegion" id="consultantDetailsRegion" value="${requestScope.consultantRegion}" />
    <agiletags:HiddenField elementName="MemberApp_app_number"/>

    <label class="subheader">Details of Consultant</label>
    <hr>
      <table id="ConsultantDetailsTable">
        <tr id="ConsApp_brokerCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Consultant Code" elementName="ConsApp_brokerCode" valueFromRequest="ConsApp_brokerCode" mandatory="yes"/>        </tr>        
        <tr id="ConsApp_brokerInitialsRow"><agiletags:LabelTextBoxErrorReq displayName="Initials" elementName="ConsApp_brokerInitials" valueFromRequest="ConsApp_brokerInitials" mandatory="no"/>        </tr>
        <tr id="ConsApp_brokerNameRow"><agiletags:LabelTextBoxErrorReq displayName="Name" elementName="ConsApp_brokerName" valueFromRequest="ConsApp_brokerName" mandatory="yes"/>        </tr>
        <tr id="ConsApp_brokerSurnameRow"><agiletags:LabelTextBoxErrorReq displayName="Surname" elementName="ConsApp_brokerSurname" valueFromRequest="ConsApp_brokerSurname" mandatory="yes"/>        </tr>
        <tr id="ConsApp_brokerPrefferedNameRow"><agiletags:LabelTextBoxErrorReq displayName="Preferred Name" elementName="ConsApp_brokerPrefferedName" valueFromRequest="ConsApp_brokerPrefferedName" mandatory="no"/>        </tr>
        <tr id="ConsApp_brokerIdNumberRow"><agiletags:LabelTextBoxErrorReq displayName="ID Number" elementName="ConsApp_brokerIdNumber" valueFromRequest="ConsApp_brokerIdNumber" mandatory="no"/>        </tr>
        <tr id="ConsApp_brokerDateOfBirthRow"><agiletags:LabelTextBoxDateReq  displayname="Date Of Birth" elementName="ConsApp_brokerDateOfBirth" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="ConsApp_brokerStatusRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Status"  elementName="ConsApp_brokerStatus" lookupId="211" mandatory="yes" errorValueFromSession="yes"/>
        <tr id="ConsApp_brokerRegionRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Region"  elementName="ConsApp_brokerRegion" lookupId="251" mandatory="no" errorValueFromSession="yes" sort="yes"/>        </tr>
      </table>

    <br>
    <table id="ConsultantAppSaveTable">
        <tr>
           <td><input type="reset" value="Reset"></td>
           <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'SaveConsultantApplicationCommand')"></td>
        </tr>
    </table>

  </agiletags:ControllerForm>


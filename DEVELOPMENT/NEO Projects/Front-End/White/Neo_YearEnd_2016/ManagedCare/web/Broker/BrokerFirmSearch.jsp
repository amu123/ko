<%-- 
    Document   : BrokerFirmSearch
    Created on : 2012/06/19, 04:18:46
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>

        <script>
            function viewMemberApp(form, id, target) {
                submitFormWithAjaxPost(form, target);
            }

            function setMemberAppValues(id, name, entityId, fName, title, lastName, region) {
                document.getElementById("firmCoderRes").value = id;
                document.getElementById("firmNameRes").value = name;
                document.getElementById("brokerFirmEntityId").value = entityId;
                document.getElementById("brokerFirmContactTile").value = title;
                document.getElementById("brokerFirmPerson").value = fName;
                document.getElementById("brokerFirmSurname").value = lastName;
                document.getElementById("firmRegion").value = region;
                
            }
            
        </script>
    </head>
    <body>

    <label class="header">Broker Firm Search</label>
    <hr>
    <br>
     <agiletags:ControllerForm name="EmployerSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="BrokerFirmSearchCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="BrokerFirmSearch" />
        <table>
            <tr><agiletags:LabelTextBoxError displayName="Broker Firm Code" elementName="brokerFirmCode"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Broker Firm Name" elementName="brokerFirmName"/></tr>
            <tr><td colspan="2" align="right"><input type="button" value="Search" onclick="viewMemberApp(this.form, 0, 'BrokerFirmSearchResults')"></td></tr>
        </table>
      </agiletags:ControllerForm>

     <agiletags:ControllerForm name="BrokerFirmSearchResultsForm">
        <input type="hidden" name="opperation" id="search_opperation" value="BrokerFirmViewCommand" />
        <input type="hidden" name="onScreen" id="search_onScreen" value="BrokerFirmSearchResults" />
        <input type="hidden" name="firmCode" id="firmCoderRes" value="">
        <input type="hidden" name="firmName" id="firmNameRes" value="">
        <input type="hidden" name="brokerFirmEntityId" id="brokerFirmEntityId" value="">
        <input type="hidden" name="brokerFirmTile" id="brokerFirmContactTile" value="" />
        <input type="hidden" name="brokerFirmContactPerson" id="brokerFirmPerson" value="" />
        <input type="hidden" name="brokerFirmContactSurname" id="brokerFirmSurname" value="" />
        <input type="hidden" name="firmRegion" id="firmRegion" value="" />

      <div style ="display: none"  id="BrokerFirmSearchResults"></div>
      </agiletags:ControllerForm>

    </body>
</html>


<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

  <agiletags:ControllerForm name="ConsultantAccreditationForm">
    <input type="hidden" name="opperation" id="opperation" value="AddConsultantAccreditationCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberDependantApp" />
    <input type="hidden" name="memberAppNumber" id="memberAppDepNumber" value="${memberAppNumber}" />
    <input type="hidden" name="buttonPressed" id="consultantAppDepButtonPressed" value="" />
    <input type="hidden" name="depNo" id="memberAppDepNo" value="" />
    <input type="hidden" name="maxDep" value="${maxDep}" />
    <input type="hidden" name="consultantEntityId" id="consultantAccreEntityId" value="${requestScope.consultantEntityId}" />
    <input type="hidden" name="consultantRegion" id="consultantAccRegion" value="${requestScope.consultantRegion}" />

    <label class="header">Accreditation</label>
    <br>
<input type="button" name="AddAccreditationButton" value="Add Accreditation" onclick="document.getElementById('consultantAppDepButtonPressed').value = this.value; getPageContents(this.form,null,'main_div','overlay');"/>
<br>
                <table id="accListTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%">
                    <tr>
                        <th>Accreditation Type</th>
                        <th>Accreditation Number</th>
                        <th>Application Date</th>
                        <th>Authorization Date</th>
                        <th>Start Date</th>
                        <th>End Date</th>                         
                    </tr>
                    <c:forEach var="entry" items="${accreList}">

                        <tr>
                            <td>${entry.accreditationType == 1 ? "FSB registration" : entry.accreditationType == 2 ? "Council accreditation" : ""}</td>
                            <td>${entry.accreditationNumber}</td>
                            <td><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.applicationDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.applicationDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.applicationDate.day}"/></td>
                            <td><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.authorisationDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.authorisationDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.authorisationDate.day}"/></td>
                            <td><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveStartDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.day}"/></td>
                            <td><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveEndDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.day}"/></td>                            
                        </tr>
  
                    </c:forEach>
                </table>
  </agiletags:ControllerForm>


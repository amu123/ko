<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

<agiletags:ControllerForm name="BrokerAccreditationForm">
    <input type="hidden" name="opperation" id="opperation" value="AddBrokerFirmAccreditationCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberDependantAppAdd" />
    <input type="hidden" name="memberAppNumber" id="memberAppNumber" value="${memberAppNumber}" />
    <input type="hidden" name="memberAppDepNumber" id="memberAppDepNumber" value="${memberAppDepNumber}" />
    <input type="hidden" name="target_div" value="${target_div}" />
    <input type="hidden" name="main_div" value="${main_div}" />
    <input type="hidden" name="buttonPressed" id="memberAppDepAddButtonPressed" value="" />
    <input type="hidden" name="brokerFirmEntityId" id="brokerFirmAddAccreEntityId" value="${requestScope.brokerFirmEntityId}" />
    <input type="hidden" name="brokerFirmTile" id="brokerFirmAddAccreTile" value="${requestScope.brokerFirmTile}" />
    <input type="hidden" name="brokerFirmContactPerson" id="brokerFirmContactAccrePerson" value="${requestScope.brokerFirmContactPerson}" />
    <input type="hidden" name="brokerFirmContactSurname" id="brokerFirmContactAccreSurname" value="${requestScope.brokerFirmContactSurname}" />
    <input type="hidden" name="firmRegion" id="firmRegionAcc2" value="${requestScope.firmRegion}" />
    <input type="hidden" name="updateOrSave" id="updateOrSaveBackend" value="" />

    <label class="header">Edit Accreditation</label>
    <br>
    <br>
    <label class="subheader">${memberAppDepNumber == 1? "Spouse / Partner / " : ""}${memberAppDepNumber}</label>
    <hr>
    <table>                
        <tr id="FrimApp_brokerAccreditationTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Accreditation Type"  elementName="FrimApp_brokerAccreditationType" lookupId="210" mandatory="yes" errorValueFromSession="yes"/></tr>
        <tr id="FirmrApp_brokerAccreditationNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Accreditation Number" elementName="FirmrApp_brokerAccreditationNumber" valueFromRequest="FirmrApp_brokerAccreditationNumber" mandatory="yes"/></tr>
        <tr id="FirmApp_brokerApplicationDateRow"><agiletags:LabelTextBoxDateReq  displayname="Application Date" elementName="FirmApp_brokerApplicationDate" valueFromSession="yes" mandatory="no"/></tr> 
        <tr id="FirmApp_brokerAuthorisationDateRow"><agiletags:LabelTextBoxDateReq  displayname="Authorisation Date" elementName="FirmApp_brokerAuthorisationDate" valueFromSession="yes" mandatory="no"/></tr>        
        <tr id="FirmApp_brokerStartDateRow"><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="FirmApp_brokerStartDate" valueFromSession="yes" mandatory="yes"/></tr> 
        <tr id="FirmApp_brokerEndDateRow"><agiletags:LabelTextBoxDateReq  displayname="End Date" elementName="FirmApp_brokerEndDate" valueFromSession="yes" mandatory="yes"/></tr> 
    </table>
    <hr>
    <table id="MemberAppDepSaveTable">
        <tr>
            <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}', '${main_div}');"></td>
            <td><input type="button" value="Save" onclick="document.getElementById('memberAppDepAddButtonPressed').value = 'SaveButton'; document.getElementById('updateOrSaveBackend').value = 'update';
                   submitFormWithAjaxPost(this.form, 'Accreditation', null, '${main_div}', '${target_div}');"></td>
        </tr>
    </table>
</agiletags:ControllerForm>

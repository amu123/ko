<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Broker Consultant Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty brokerConsultantResults}">
            <span>No Consultant found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Consultant Code</th>
                        <th>Name</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${brokerConsultantResults}">
                        <tr>
                            <td><label>${entry['consultantCode']}</label></td>
                            <td><label>${entry['fstName']}</label></td>
                            <td><input type="button" value="Select" onclick="setElemValues({'${diagCodeId}':'${entry['consultantCode']}','${diagNameId}':'${agiletags:escapeStr(entry['fstName'])}','consultantAddHistoryEntityId':'${entry['entityId']}'});updateFormFromData(' ','${target_div}');swapDivVisbible('${target_div}','${main_div}');"/></td> 
                        </tr>
                    </c:forEach>

                </table>
            </c:otherwise>
        </c:choose>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

  <agiletags:ControllerForm name="BrokerAndBrokerConsultantForm">
    <input type="hidden" name="opperation" id="opperation" value="AddBrokerConsultantHistoryCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberDependantAppAdd" />
    <input type="hidden" name="buttonPressed" id="brokerConsultantValuesButtonPressed" value="" />
    <input type="hidden" name="brokerEntityId" id="brokerConsultantAddHistoryEntityId" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="consultantEntityId" id="consultantAddHistoryEntityId" value="${requestScope.consultantEntityId}" />
<!--    <input type="hidden" name="brokerTile" id="brokerFirmAddAccreTile" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="brokerContactPerson" id="brokerFirmContactAccrePerson" value="${requestScope.brokerEntityId}" />-->
    <br/>
    <br/>
    <label class="header">Link Broker Consultant To Broker</label>
    <br>
     <table>                        
        <tr><agiletags:LabelTextSearchTextDiv displayName="Broker Consultant Code" elementName="Disorder_diagCode" mandatory="yes" onClick="document.getElementById('brokerConsultantValuesButtonPressed').value = 'SearchConsultantButton'; getPageContents(document.forms['BrokerAndBrokerConsultantForm'],null,'overlay','overlay2');"/>        </tr>
        <tr><agiletags:LabelTextDisplay displayName="Broker Consultant Name" elementName="Disorder_diagName" valueFromSession="no" javaScript="" /></tr>
        <tr id="brokerApp_ConsultantStartDate"><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="BrokerApp_ConsultantHistoryStartDate" valueFromSession="yes" mandatory="yes"/>        </tr> 
     </table>
    <hr>
    <table id="MemberAppDepSaveTable">
        <tr>
           <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
           <td><input type="button" value="Save" onclick="document.getElementById('brokerConsultantValuesButtonPressed').value='SaveButton';submitFormWithAjaxPost(this.form, 'brokerConsultantListGrid' , null,'${main_div}', '${target_div}');"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>

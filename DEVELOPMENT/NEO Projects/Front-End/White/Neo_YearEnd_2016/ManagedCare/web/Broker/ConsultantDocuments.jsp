
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
<link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>
<script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
<script type='text/javascript' src="${pageContext.request.contextPath}/resources/Membership/MemberMaintenance.js"></script>
<script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
<script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
<br>
<label class="subheader">View Document</label>
<hr>
<br>
<c:if test="${applicationScope.Client == 'Agility'}">
<c:choose>
    <c:when test="${empty indexList}">
        <span class="label">No Document found</span>
    </c:when>
    <c:otherwise>
<label id="DocView_Msg" class="label"></label>
        <agiletags:ControllerForm name="documentIndexing">

            <input type="hidden" name="folderList" id="folderList" value="" />
            <input type="hidden" name="fileLocation" id="fileLocation" value="" />
            <input type="hidden" name="opperation" id="opperation" value="MemberDocumentDisplayViewCommand" />
            <input type="hidden" name="searchCalled" id="searchCalled" value=""/>
            
            <input type="hidden" name="onScreen" id="onScreen" value="" />

            <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                
                <tr>
                    <th>Member Number</th>
                    <th>Type</th>
                    <th>Location</th>
                    <th>Current Date</th>                                              
                    <th></th>
                    <th></th>
                </tr>
                <c:forEach var="entry" items="${indexList}">
                    <tr class="label">
                        <td>${entry['entityNumber']}</td>
                        <td>${entry['docType']}</td>
                        <td>${entry['location']}</td>
                        <td><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.generationDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.generationDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.generationDate.day}"/></td>
                        <td><button type="submit" name="com${entry['indexId']}"  onClick="document.getElementById('fileLocation').value = '${entry['location']}';">View</button></td>
                        <td><input id="printB" type="button" value="Print" onClick="submitActionAndFile(this.form, 'AddToPrintPoolCommand','${entry['indexId']}'); submitFormWithAjaxPost(this.form, 'MemberDocuments');" /></td>
                    </tr>
                </c:forEach>

            </table>
        </agiletags:ControllerForm>
    </c:otherwise>
</c:choose>
</c:if>
<c:if test="${applicationScope.Client == 'Sechaba'}">
<iframe name="uploadFrame" id="uploadFrame" style="display: none"></iframe>    
<form id="uploadForm" enctype="multipart/form-data" target="uploadFrame" method="POST" action="/ManagedCare/AgileController?opperation=MemberDocumentUploadCommand&memberNumber=${entityId}&entityType=2">
    <input type="hidden" name="docuType" id="docuType" value="" />
</form>
<agiletags:ControllerForm name="documentSorting">
    <input type="hidden" name="opperation" id="opperation" value="MemberDocumentDisplayViewCommand" />
    <input type="hidden" name="fileLocation" id="fileLocation" value="" />
    <input type="hidden" name="opperationParam" id="opperationParam" value="" />
    <input type="hidden" name="memberNumber" id="memberNumber" value="${entityId}" />
    <table>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Documents"  elementName="docKind" lookupId="254" errorValueFromSession="no" javaScript="onChange=\"toogleDocument(this.value, 'docType');\""/></tr>                                    
        <tr><agiletags:DocumentsList displayName="Type"  elementName="docType" mandatory="no" errorValueFromSession="yes" parentElement="docKind" /></tr>
        <c:if test="${applicationScope.Client == 'Sechaba'}">
            <tr id="documentUpload" style="display: none">
                <td><label>Document</label></td>
                <td><input type="file" name="uploadDoc" id="uploadDoc" size="75" form="uploadForm" /></td>
                <td><input type="submit" id="submitFile" value="Upload Document" form="uploadForm" onclick="document.getElementById('docuType').value = document.getElementById('docType').value; document.getElementById('submitFile').disabled;"/></td>
            </tr>
        </c:if>
    </table>
    <!--<tr><td align="left" width="160px"><label class="label" >Type:</label></td>
        <td><select name="docType" id="docType" style="width:215px"></select></td></tr>-->                                   

    <label class="subheader">Broker Firm Documents</label>    
    <table>
        <td><label>Start Date:</label></td>
        <td><input name="minDate" id="minDate" size="30" value="${minDate}"></td>
        <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('minDate', this);"/></td>
        <td>&nbsp;</td>
        <td><label>End Date:</label></td>
        <td><input name="maxDate" id="maxDate" size="30" value="${maxDate}"></td>
        <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('maxDate', this);"/></td>
        <td>&nbsp;</td>
        <td><input type="button" name="SearchButton" id="searchButton" value="Search" onclick="document.getElementById('opperationParam').value = 'sort';submitActionAndFile(this.form, 'DocumentFilterCommand', null); submitFormWithAjaxPost(this.form, 'MemberDocumentDetails');"/></td>
    </table>
    <div style ="display: none"  id="MemberDocumentDetails"></div>
</agiletags:ControllerForm>
</c:if>


<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<agiletags:ControllerForm name="ConsultantHistoryForm">
    <input type="hidden" name="opperation" id="opperation" value="AddConsultantHistoryCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberDependantApp" />
    <input type="hidden" name="memberAppNumber" id="memberAppDepNumber" value="${memberAppNumber}" />
    <input type="hidden" name="buttonPressed" id="consultantHistoryButtonPressed" value="" />
    <input type="hidden" name="depNo" id="memberAppDepNo" value="" />
    <input type="hidden" name="consultantEntityId" id="consultantHistoryEntityId" value="${requestScope.consultantEntityId}" />
    <input type="hidden" name="consultantRegion" id="consultantCovRegion" value="${requestScope.consultantRegion}" />

    <label class="header">Cover Details</label>
    <br/>
    <br/>
    <table id="depListTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%">
        <tr>
            <th>Member Number</th>
            <th>Name</th>
            <th>Initials</th>
            <th>ID Number</th>
            <th>Start Date</th>
            <th>End Date</th>
        </tr>
        <c:forEach var="entry" items="${memList}">

            <tr>
                <td>${entry.coverNumber}</td>
                <td>${entry.memberName} ${entry.memberSurname}</td>
                <td>${entry.memberInitial}</td>
                <td>${entry.memberIdNumber}</td>
                <td><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveStartDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.day}"/></td>
                <td><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveEndDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.day}"/></td>                            
            </tr>

        </c:forEach>
    </table>
</agiletags:ControllerForm>


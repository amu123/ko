<%-- 
    Document   : BrokerFirmDetails
    Created on : 2012/06/18, 10:17:45
    Author     : princes
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript">
                $(function() {
                    alert("Came from change screen : " + ${sessionScope.cameFromChangeInd});
                    alert("here");
                });
                
                $(document).ready(function(){
                resizeContent();
                
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });       
            });
                        
            function hideRows(id) {
                document.getElementById(id).style.display='none';
                document.getElementById(id+ 'plus').style.display='';
                document.getElementById(id+ 'minus').style.display='none';
            }
            function showRows(id) {
                document.getElementById(id).style.display='';
                document.getElementById(id+ 'minus').style.display='';
                document.getElementById(id+ 'plus').style.display='none';

            }
            
        
        </script>
    </head>
    <agiletags:ControllerForm name="BrokerFirmForm">
        <input type="hidden" name="opperation" id="MemberApp_opperation" value="SaveBrokerFirmApplicationCommand" />
        <input type="hidden" name="onScreen" id="MemberApp_onScreen" value="Borker Firm Application" />
        <input type="hidden" name="brokerFirmEntityId" id="brokerFirmDetailsEntityId" value="${requestScope.brokerFirmEntityId}" />
        <input type="hidden" name="brokerFirmTile" id="brokerFirmDetailsTitle" value="${requestScope.brokerFirmTile}" />
        <input type="hidden" name="brokerFirmContactPerson" id="brokerFirmContactDetailsPerson" value="${requestScope.brokerFirmContactPerson}" />
        <input type="hidden" name="brokerFirmContactSurname" id="brokerFirmContactDetailsSurname" value="${requestScope.brokerFirmContactSurname}" />
        <input type="hidden" name="firmRegion" id="firmRegionDetiails" value="${requestScope.firmRegion}" />
        <input type="hidden" name="VIPBroker" id="VIPBroker" value="${requestScope.VIPBroker}" />      
        <%--<input type="hidden" name="FirmApp_CommisionInd" id="FirmApp_CommisionInd" value="${requestScope.FirmApp_CommisionInd}" />--%>
            
        <agiletags:HiddenField elementName="MemberApp_app_number"/>

        <label class="subheader">Details of Broker Firm</label>
        <hr>
        <table id="BrokerFirmDetailsTable">
            <tr id="FirmApp_alternativeCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Broker Firm Code" elementName="FirmApp_firmCode" valueFromRequest="FirmApp_firmCode" mandatory="yes"/>        </tr>
            <tr id="FirmApp_alternativeCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Alternative Code" elementName="FirmApp_alternativeCode" valueFromRequest="FirmApp_alternativeCode" mandatory="no"/>        </tr>
            <tr id="FirmApp_tradingNameRow"><agiletags:LabelTextBoxErrorReq displayName="Broker Firm Name" elementName="FirmApp_firmName" valueFromRequest="FirmApp_firmName" mandatory="yes"/>        </tr>
            <tr id="FirmApp_tradingNameRow"><agiletags:LabelTextBoxErrorReq displayName="Trading Name" elementName="FirmApp_tradingName" valueFromRequest="FirmApp_tradingName" mandatory="no"/>       </tr>
            <tr id="FirmApp_vipBrokerRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="VIP Broker"  elementName="FirmApp_vipBroker" lookupId="67" errorValueFromSession="yes" firstIndexSelected="yes"/>        </tr>
            <tr id="FirmApp_concessionRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Concession"  elementName="ConcesType" lookupId="264" mandatory="no" errorValueFromSession="yes" firstIndexSelected="yes" javaScript="onChange=\"refreshBrokerText();\""/></tr> 
            <tr id="FirmApp_concessionStartRow"><agiletags:LabelTextBoxDateReq  displayname="Concession Start" elementName="FirmApp_ConcessionStart"  valueFromSession="yes" mandatory="yes"/></tr>
            <tr id="FirmApp_concessionEndRow"><agiletags:LabelTextBoxDateReq  displayname="Concession End " elementName="FirmApp_ConcessionEnd" valueFromSession="yes" mandatory="no"/></tr> 
            <tr id="FirmApp_registeredVatRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Registered For Vat"  elementName="FirmApp_registeredVat" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <tr id="FirmApp_registeredNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Company Registration Number" elementName="FirmApp_registeredCompanyNumber" valueFromRequest="FirmApp_registeredCompanyNumber" mandatory="no"/>        </tr>
            <tr id="FirmApp_registeredNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Vat Registration Number" elementName="FirmApp_registeredVatNumber" valueFromRequest="FirmApp_registeredVatNumber" mandatory="no"/>        </tr>
            <tr id="FirmApp_CommisionIndRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Commission"  elementName="FirmApp_CommisionInd" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>  </tr>
                <%--<tr id="FirmApp_CommisionIndRow"><agiletags:LabelTextSearchWithNoText displayName="Commission" elementName="commissionInd" mandatory="no" onClick="submitWithAction('ChangePayIndicatorCommand');"  /> --%>
            
            <tr id="FirmApp_contractStatusRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Contract Status"  elementName="FirmApp_contractStatus" lookupId="211" mandatory="yes" errorValueFromSession="yes"/>
            <tr id="FirmApp_contractStartDateRow"><agiletags:LabelTextBoxDateReq  displayname="Contract Start Date" elementName="FirmApp_contractStartDate" valueFromSession="yes" mandatory="no"/>        </tr>
            <tr id="FirmApp_contractEndDateRow"><agiletags:LabelTextBoxDateReq  displayname="Contract End Date" elementName="FirmApp_contractEndDate" valueFromSession="yes" mandatory="no"/>        </tr>
            <tr id="FirmApp_titleRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Title"  elementName="FirmApp_title" lookupId="24" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <tr id="FirmApp_firstNameRow"><agiletags:LabelTextBoxErrorReq displayName="First Name" elementName="FirmApp_firstName" valueFromRequest="FirmApp_firstName" mandatory="no"/>        </tr>        
            <tr id="FirmApp_surnameRow"><agiletags:LabelTextBoxErrorReq displayName="Surname" elementName="FirmApp_surname" valueFromRequest="FirmApp_surname" mandatory="no"/>        </tr>       
            <tr id="BrokerApp_regionRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Region"  elementName="FirmApp_region" lookupId="251" mandatory="no" errorValueFromSession="yes" sort="yes"/>        </tr>
        </table>

        <br>
        <table id="MemberAppSaveTable">
            <tr>
                <td><input type="reset" value="Reset"></td>
                <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'BrokerFirmApplicationDetails');"></td>
            </tr>
        </table>

    </agiletags:ControllerForm>

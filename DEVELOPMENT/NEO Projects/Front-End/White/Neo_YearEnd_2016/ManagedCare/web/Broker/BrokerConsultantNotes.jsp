<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="BroherConsultantAppNotesForm">
    <input type="hidden" name="opperation" id="BrokerConsultantAppNotes_opperation" value="BrokerConsultantNotesCommand" />
    <input type="hidden" name="onScreen" id="BrokerConsultantAppNotes_onScreen" value="BrokerConsultantAppNotes" />
    <input type="hidden" name="memberAppNumber" value="${memberAppNumber}" />
    <input type="hidden" name="noteId" id="BrokerConsultantAppNotes_note_id" value="" />
    <input type="hidden" name="buttonPressed" id="BrokerConsultantAppNotes_button_pressed" value="" />
     <input type="hidden" name="consultantEntityId" id="brokerConsultantNotesEntityId" value="${consultantEntityId}" />
     <input type="hidden" name="consultantRegion" id="consultantNoteRegion" value="${requestScope.consultantRegion}" />
    <label class="header">Broker Consultant Notes</label>
    <hr>

      <table id="BrokerConsultantAppNotesTable">
          <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Details"  elementName="notesListSelect" lookupId="217" mandatory="no" errorValueFromSession="no" javaScript="onchange=\"document.getElementById('BrokerConsultantAppNotes_button_pressed').value='SelectButton';submitFormWithAjaxPost(this.form, 'BrokerConsultantAppNotesDetails');\"" />        </tr>
      </table>          
 <div style ="display: none"  id="BrokerConsultantAppNotesDetails"></div>
 </agiletags:ControllerForm>
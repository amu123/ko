
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

  <agiletags:ControllerForm name="BrokerHistoryForm">
    <input type="hidden" name="opperation" id="opperation" value="AddBrokerHistoryCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberDependantApp" />
    <input type="hidden" name="memberAppNumber" id="memberAppDepNumber" value="${memberAppNumber}" />
    <input type="hidden" name="buttonPressed" id="brokerHistoryButtonPressed" value="" />
    <input type="hidden" name="depNo" id="memberAppDepNo" value="" />
    <input type="hidden" name="brokerEntityId" id="brokerHistoryEntityId" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="brokerTile" id="brokerHistoryTile" value="${requestScope.brokerTile}" />
    <input type="hidden" name="brokerContactPerson" id="brokerContactHistoryPerson" value="${requestScope.brokerContactPerson}" />
    <input type="hidden" name="brokerRegion" id="brokerHRegion" value="${requestScope.brokerRegion}" />

    <label class="header">Broker History</label>
    <br>
<input type="button" name="AddHistoryButton" value="Add History" onclick="document.getElementById('brokerHistoryButtonPressed').value = this.value; getPageContents(this.form,null,'main_div','overlay');"/>
<br>
                <table id="depListTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%">
                    <tr>
                        <th>Firm Code</th>
                        <th>Firm Name</th>
                        <th>Firm Start Date</th>
                        <th>Firm End Date</th>                         
                    </tr>
                    <c:forEach var="entry" items="${accreList}">

                        <tr>
                            <td class="label">${entry.firmCode}</td>
                            <td class="label">${entry.firmName}</td>
                            <td class="label"><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveStartDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.day}"/></td>
                            <td class="label"><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveEndDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.day}"/></td>                            
                        </tr>
  
                    </c:forEach>
                </table>
  </agiletags:ControllerForm>

<%-- 
    Document   : BrokerDetails
    Created on : 2012/06/21, 10:57:33
    Author     : princes
--%>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="BrokerFirmForm">
    <input type="hidden" name="opperation" id="MemberApp_opperation" value="SaveBrokerApplicationCommand" />
    <input type="hidden" name="onScreen" id="MemberApp_onScreen" value="Borker Firm Application" />
    <input type="hidden" name="brokerEntityId" id="brokerDetailsEntityId" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="brokerTitle" id="brokerTitle" value="${requestScope.brokerTitle}" />
    <input type="hidden" name="brokerContactPerson" id="brokerCPerson" value="${requestScope.brokerContactPerson}" />
    <input type="hidden" name="brokerContactSurname" id="brokerCPerson" value="${requestScope.brokerContactSurname}" />
    <input type="hidden" name="brokerRegion" id="brokerDRegion" value="${requestScope.brokerRegion}" />

    <agiletags:HiddenField elementName="MemberApp_app_number"/>

    <label class="subheader">Details of Broker</label>
    <hr>
      <table id="BrokerDetailsTable">
        <tr id="BrokerApp_brokerCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Broker Code" elementName="BrokerApp_brokerCode" valueFromRequest="BrokerApp_brokerCode" mandatory="yes"/>        </tr>
        <tr id="BrokerApp_alternativeCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Alternative Code" elementName="BrokerApp_alternativeCode" valueFromRequest="BrokerApp_alternativeCode" mandatory="no"/>        </tr>
        <tr id="BrokerApp_brokerInitialsRow"><agiletags:LabelTextBoxErrorReq displayName="Initials" elementName="BrokerApp_brokerInitials" valueFromRequest="BrokerApp_brokerInitials" mandatory="yes"/>        </tr>
        <tr id="BrokerApp_brokerNameRow"><agiletags:LabelTextBoxErrorReq displayName="Name" elementName="BrokerApp_brokerName" valueFromRequest="BrokerApp_brokerName" mandatory="yes"/>        </tr>
        <tr id="BrokerApp_brokerSurnameRow"><agiletags:LabelTextBoxErrorReq displayName="Surname" elementName="BrokerApp_brokerSurname" valueFromRequest="BrokerApp_brokerSurname" mandatory="yes"/>        </tr>
        <tr id="BrokerApp_brokerPrefferedNameRow"><agiletags:LabelTextBoxErrorReq displayName="Preferred Name" elementName="BrokerApp_brokerPrefferedName" valueFromRequest="BrokerApp_brokerPrefferedName" mandatory="no"/>        </tr>
        <tr id="BrokerApp_brokerIdNumberRow"><agiletags:LabelTextBoxErrorReq displayName="ID Number" elementName="BrokerApp_brokerIdNumber" valueFromRequest="BrokerApp_brokerIdNumber" mandatory="no"/>        </tr>
        <tr id="BrokerApp_brokerDateOfBirthRow"><agiletags:LabelTextBoxDateReq  displayname="Date Of Birth" elementName="BrokerApp_brokerDateOfBirth" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="BrokerApp_brokerStatusRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Status"  elementName="BrokerApp_brokerStatus" lookupId="211" mandatory="yes" errorValueFromSession="yes"/>
        <tr id="BrokerApp_brokerAlternativeIDRow"><agiletags:LabelTextBoxErrorReq displayName="Alternative ID" elementName="BrokerApp_brokerAlternativeID" valueFromRequest="BrokerApp_brokerAlternativeID" mandatory="no"/>        </tr>
        <tr id="BrokerApp_correspondanceLanguageRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Language"  elementName="BrokerApp_correspondanceLanguage" lookupId="31" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="BrokerApp_titleRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Title"  elementName="BrokerApp_title" lookupId="24" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="BrokerApp_contactNameRow"><agiletags:LabelTextBoxErrorReq displayName="Contact Name" elementName="BrokerApp_contactName" valueFromRequest="BrokerApp_contactName" mandatory="no"/>        </tr>        
        <tr id="BrokerApp_contactSurnameRow"><agiletags:LabelTextBoxErrorReq displayName="Contact Surname" elementName="BrokerApp_contactSurname" valueFromRequest="BrokerApp_contactSurname" mandatory="no"/>        </tr>
        <tr id="BrokerApp_regionRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Region"  elementName="BrokerApp_region" lookupId="251" mandatory="no" errorValueFromSession="yes" sort="yes"/>        </tr>
      </table>

    <br>
    <table id="MemberAppSaveTable">
        <tr>
           <td><input type="reset" value="Reset"></td>
           <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'SaveBrokerApplicationCommand')"></td>
        </tr>
    </table>

  </agiletags:ControllerForm>


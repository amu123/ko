<%-- 
    Document   : UnpaidClaims
    Created on : 28 Apr 2015, 7:58:43 AM
    Author     : dewaldo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script language="JavaScript">
            $(document).ready(function () {
                $("#button").click(function () {
                    $("#panel").slideToggle("slow");
                });
                $("#button").click();
            });

        </script>
        <style> 
            #pane {
                padding: 5px;
                text-align: center;
                background-color:#999999;
            }

            #button {
                padding: 5px;
                text-align: center;
                <c:if test="${applicationScope.Client == 'Sechaba'}">
                background-color:#6cc24a;
                </c:if>
                <c:if test="${applicationScope.Client == 'Agility'}">
                background-color:#660000;
                </c:if>
                border: solid 1px #636363;
                color: white;
            }

            #panel {
                border: solid 1px #636363;
                text-align: center;
                padding: 20px;
                display: none;
            }
        </style>
    </head>
    <body onload="">
        <table width=100% height=100%>
            <tr valign="left">
                <td width="50px">

                </td>
                <td align="left">
                    <label class="header">Claims Unpaid's - Choose File to Process</label>
                    <hr>
                    <form action="/ManagedCare/AgileController?opperation=SubmitUnpaidsDocumentCommand&schemeOption=1" enctype="multipart/form-data" method="POST">
                        <agiletags:ProductFileUploader action="SubmitUnpaidsDocumentCommand" />                     
                        <div id="Message" style="width:500px;">                        
                            <c:set var="msg" scope="request"  value="${requestScope.Message}"/>
                            <c:if test="${!empty msg && msg ne ''}">
                                <br/>
                                <br/>
                                <div id="button">Results</div>
                                <div id="panel">${requestScope["Message"]}</div> 
                            </c:if>
                        </div>
                        <br/><br/>
                    </form>  
                </td>
            </tr>
        </table>
    </body>
</html>

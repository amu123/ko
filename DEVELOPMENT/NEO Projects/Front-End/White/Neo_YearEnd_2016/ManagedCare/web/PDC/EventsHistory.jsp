<%-- 
    Document   : EventsHistory
    Created on : 2010/04/16, 04:11:22
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type="text/javascript" language="JavaScript">

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.forms[0].submit();
            }

            function submitPageAction(action, pageDirection) {
                document.getElementById('opperation').value = action;
                document.getElementById('pageDirection').value = pageDirection;
                document.forms[0].submit();
            }

            function ClearValidation() {
                $("#dateFrom_error").text("");
                $("#dateTo_error").text("");
            }

            function ClearValidationContents() {
                document.getElementById('dateFrom').value = "";
                document.getElementById('dateTo').value = "";
            }

            function submitFromWithAction(action) {
                var dateFrom = $("#dateFrom").val();
                var dateTo = $("#dateTo").val();
                var searchPdcType = $("#searchPdcType").val();
                ClearValidation();
                var error = "";

                /*if ((dateFrom != null && dateFrom != "") ){
                 error = "notnull"
                 
                 }
                 if ((dateTo != null && dateTo != "")){
                 error = "notnull";
                 
                 }*/

                if ((searchPdcType !== null && searchPdcType !== "")) {
                    error = "notnull";

                }


                if (error !== "") {
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                } else {
                    $("#dateFrom_error").text("Please enter date");
                    $("#dateTo_error").text("Please enter date");
                }
            }

            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <!-- content goes here -->
                    <agiletags:PDCTabs tabSelectedIndex="link_EV" />
                    <fieldset id="pageBorder">
                        <div class="">
                            <br/>
                            <br/>
                            <label class="header">Event History</label>
                            <br/>
                            <br/>
                            <agiletags:ControllerForm name="eventsHistory">
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <input type="hidden" name="listIndex" id="listIndex" value="" />
                                <input type="hidden" name="pageDirection" id="pageDirection" value="" />
                                <table> 
                                    <tr><agiletags:LabelTextBoxDateTime elementName="dateFrom" displayName="Date From" mandatory="no"/></tr>
                                    <tr><agiletags:LabelTextBoxDateTime elementName="dateTo" displayName="Date To" mandatory="no"/></tr>
                                    <tr><agiletags:LabelNeoLookupValueDropDown displayName="Risk Factor" elementName="searchPdcType" lookupId="178"/></tr>   
                                </table>
                                <table>
                                    <tr>
                                        <agiletags:ButtonOpperation align="right" span="1" type="button" commandName="PreviewClaimHistory" displayname="Preview" javaScript="onClick=\"submitFromWithAction('PreviewClaimHistory');\""/>
                                        <td></td><td><button type="button" onClick="ClearValidationContents()" value=""  >Cancel</button></td>
                                    </tr>
                                </table>
                                <br/>
                                <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                    <agiletags:EventHistoryTable/>
                                </table>
                            </agiletags:ControllerForm>
                        </div></fieldset>
                </td></tr></table>
    </body>
</html>

<%-- 
    Document   : PDCDocumentGeneration
    Created on : 19 Feb 2015, 11:11:59 AM
    Author     : shimanem
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>

        <script>
            $(document).ready(function () {
                var check = <%=session.getAttribute("type")%>;
                if (check != null)
                {
                    if (check == 290) {
                        document.getElementById("optSelectID").selectedIndex = 1;
                        $('#optSelectID').change();
                    }
                    if (check == 291) {
                        document.getElementById("optSelectID").selectedIndex = 2;
                        $('#optSelectID').change();
                    }
                }
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function generateDoc(docId, docName, command) {
                document.getElementById('docId').value = docId;
                document.getElementById('docName').value = docName;
                document.getElementById('treatmentTypeID').value = document.getElementById('optSelectID').value;
                document.getElementById('opperation').value = command;

                document.forms[0].submit();
            }

            function refreshScreen(screen)
            {
                $("#opperation").val("RefreshCommand");
                $("#refreshScreen").val(screen);
                document.forms[0].submit();
            }

            function showOrHide(id)
            {
                if (id === '290')
                {
                    //alert("Showing CDL Grid");
                    document.getElementById("chronicConditionsOther").style.display = 'none';
                    document.getElementById("chronicConditionsNormal").style.display = 'block';
                } else if (id === '291')
                {
                    //alert("Showing Chronic Conditions Grid");
                    document.getElementById("chronicConditionsNormal").style.display = 'none';
                    document.getElementById("chronicConditionsOther").style.display = 'block';
                }
            }
        </script>        
    </head>
    <body>
        <agiletags:PDCTabs tabSelectedIndex="link_DGN" />
        <fieldset id="pageBorder">
            <div class="" >
                <!-- content goes here -->
                <br/>
                <br/>
                <label class="header">Member Document</label><br/>
                <table>
                    <agiletags:PDCDocDropDownSelectTag id="optSelect" javascript="onChange =\"showOrHide(this.value);\"" displayName="Choose CDL Type "/>     
                </table>
                <hr><br/>
                <agiletags:ControllerForm name="documentGeneration">
                    <table width=100% height=100%>
                        <tr valign="left">
                            <td width="50px"></td>
                            <td align="left">
                                <label class="header">Document Generation</label>
                                <br/><br/>
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <input type="hidden" name="onScreen" id="onScreen" value="" />
                                <input type="hidden" name="refreshScreen" id="refreshScreen" value="" />
                                <input type="hidden" name="docId" id="docId" value="" />                                
                                <input type="hidden" name="treatmentTypeID" id="treatmentTypeID" value="" />                                
                                <input type="hidden" name="docName" id="docName" value="" />

                                <div id="chronicConditionsNormal" style="display: none">
                                    <agiletags:PDCDocGenerationTable />
                                </div>
                                <div id="chronicConditionsOther" style="display: none">
                                    <agiletags:PDCDocGenerationOtherCDL />
                                </div>
                            </td>
                        </tr>
                    </table>
                </agiletags:ControllerForm>
            </div>
        </fieldset>
    </body>
</html>

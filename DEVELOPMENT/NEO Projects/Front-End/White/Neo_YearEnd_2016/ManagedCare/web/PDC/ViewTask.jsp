<%-- 
    Document   : ViewTask
    Created on : 2010/04/16, 01:57:48
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">PDC Workbench - View Task</label>
                    <br/><br/>

                    <agiletags:ControllerForm name="viewTask">
                        <table>
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <tr><agiletags:LabelTextBoxError displayName="Task" elementName="task" valueFromSession="yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Due Date" elementName="dueDate" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Policy Holder" elementName="policyHolder" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Policy Number" elementName="policyNumber" valueFromSession="yes"/></tr>                           
                            <table>
                                <tr>
                                    <agiletags:ButtonOpperation align="left" displayname="Back" commandName="BackToWorkbenchCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('BackToWorkbenchCommand')\";"/>
                                    <agiletags:ButtonOpperation align="left" displayname="Close Task" commandName="CloseTaskCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('CloseTaskCommand')\";"/>
                                </tr>
                            </table>
                        </table>
                    </agiletags:ControllerForm>
                    <br/>
                </td></tr></table>
    </body>
</html>

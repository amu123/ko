<%-- 
    Document   : 
    Created on : Feb 16, 2012, 4:25:36 PM
    Author     : Princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type="text/javascript" language="JavaScript">

            $(document).ready(function () {
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function checkForWorkBenchError() {
                var riskFactorValue = document.getElementById("riskFactor").selectedIndex;

                if (riskFactorValue >= 1 && riskFactorValue <= 2) {
                    $("#riskFactor_error").text("Patient will be removed from workbench");
                } else {
                    $("#riskFactor_error").text("");
                }
            }
        </script>
    </head>

    <body>  
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <!-- content goes here -->
                    <c:if test="${!empty sessionScope.pdcManualFlag && (sessionScope.pdcManualFlag eq false || sessionScope.pdcManualFlag eq 'false')}">
                        <agiletags:PDCTabs tabSelectedIndex="link_RR" />
                    </c:if>
                    <fieldset id="pageBorder">
                        <div class="content_area" >
                            <label class="header">Risk Rate Patient</label>
                            <br/>
                            <br/>
                            <agiletags:ControllerForm name="eventsHistory">

                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <table>
                                    <tr><agiletags:LabelTextBoxError displayName="Policy Number" elementName="policyNumber" valueFromSession="yes"/></tr>                                                                                                
                                    <tr><agiletags:LabelTextBoxError displayName="Name" elementName="patientName" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="surname" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Risk Date" elementName="riskDate" valueFromSession="yes" enabled="no" readonly="yes"/></tr>
                                    <tr><agiletags:LabelNeoLookupValueDropDownError errorValueFromSession="yes" displayName="Risk Factor" elementName="riskFactor" lookupId="176" javaScript="onChange=\"checkForWorkBenchError()\""/></tr>


                                    <tr><agiletags:LabelNeoLookupValueDropDown displayName="Risk Rate Reason" elementName="riskReason" lookupId="177"/></tr>


                                    <tr><agiletags:LabelTextAreaError displayName="Notes" elementName="riskRatingNotes" valueFromSession="yes" maxlength="950"/></tr>

                                </table>
                                <br/>
                                <table>
                                    <tr>                                    
                                        <agiletags:ButtonOpperation align="left" displayname="Save" commandName="SaveRiskRateCommand" span="1" type="button" javaScript="onClick=\"submitWithAction('SaveRiskRateCommand')\";"/>
                                        <agiletags:ButtonOpperation commandName="BackToWorkbenchCommand" align="left" displayname="Back" span="1" type="button"  javaScript="onClick=\"submitWithAction('BackToWorkbenchCommand')\";"/>
                                    </tr>
                                </table>
                            </agiletags:ControllerForm>
                        </div></fieldset>
                </td></tr></table>
    </body>
</html>

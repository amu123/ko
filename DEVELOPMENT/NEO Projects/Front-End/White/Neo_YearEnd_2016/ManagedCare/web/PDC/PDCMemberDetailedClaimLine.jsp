<%-- 
    Document   : PDCMemberDetailedClaimLine
    Created on : Feb 20, 2012, 1:49:04 PM
    Author     : Princes
--%>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>

            function submitWithAction(action, claimLineId, paidAmount, dependentCode) {
                document.getElementById('opperation').value = action;
                document.getElementById('memberDetailedClaimLineId').value = claimLineId;
                document.getElementById('paidAmount').value = paidAmount;
                document.getElementById('dependentCode').value = dependentCode;
                document.getElementById('onScreen').value = "PDCMemberDetailedClaimLine.jsp";
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <!-- content goes here -->
                    <table>
                        <tr valign="top"></tr>
                        <agiletags:ControllerForm name="claimsHistory">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="memberDetailedClaimLineId" id="memberDetailedClaimLineId" value="" />
                            <input type="hidden" name="paidAmount" id="paidAmount" value="" />
                            <input type="hidden" name="dependentCode" id="dependentCode" value="" />
                        </agiletags:ControllerForm>
                    </table>
                    <agiletags:MemberClaimLineClaimLineTablePDC />
                    <br/>
                    <table>
                        <tr><agiletags:ButtonOpperation align="right" span="5" type="button" commandName="ReturnToClaimHistoryCommand" displayname="Return" javaScript="onClick=\"submitWithAction('ReturnToClaimHistoryCommand');\""/></tr>
                    </table>
                </td></tr></table>
    </body>
</html>

<%--
    Document   : MemberSearch
    Created on : 2012/02/16, 02:39:31
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script language="JavaScript">

            var error = true;

//            $(function() {
//
//                var searchVal = document.getElementById('searchCalled').value;
//                if ((searchVal == '') || (searchVal = null))
//                {
//                    document.getElementById('tblTags').style.visibility = 'hidden';
//                }
//                if (searchVal == 'policyNumber') {
//                    getCoverByNumber(document.getElementById('policyNumber_text').value, 'CoverDependantResultCommand');
//                    document.getElementById('searchCalled').value = '';
//                }
//
//            });

            $(document).ready(function () {
                var searchVal = document.getElementById('searchCalled').value;
                if (searchVal == 'policyNumber') {
                    getCoverByNumber(document.getElementById('policyNumber_text').value, 'CoverDependantResultCommand');
                } else if ((searchVal == '') || (searchVal = null))
                {
                    document.getElementById('tblTags').style.visibility = 'hidden';
                }
                document.getElementById('searchCalled').value = 'antiReload';
                //hideAll();
            });


            function GetXmlHttpObject() {
                if (window.ActiveXObject) {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            //member search
            /* function getCoverByNumber(str, element){
             //document.getElementById('memberButton').disabled = true;
             $("#policyNumber_error").text("");
             if(str != null && str != ""){
             xhr = GetXmlHttpObject();
             if(xhr==null){
             alert("ERROR: Browser Incompatability");
             return;
             }
             var url = "/ManagedCare/AgileController";
             url+="?opperation=GetMemberByNumberCommand&number="+ str;
             xhr.onreadystatechange=function(){
             if(xhr.readyState == 4 && xhr.statusText == "OK"){
             var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
             if(result == "Error"){
             $("#"+element+"_error").text("No Cover Found");
             error = true;
             }else{
             error = false;
             document.getElementById('memberButton').disabled = false;
             }
             }
             }
             xhr.open("POST",url,true);
             //xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
             xhr.send(null);
             }else{
             $("#"+element+"_error").text("Please enter a Cover Number");
             }
             }*/

            //cover search
            function tblSelection(ID, command) {
                document.getElementById('listIndex').value = ID;
                var url = "/ManagedCare/AgileController";
                var exact = document.getElementById("exactCoverNum").value;
                var data = {
                    //replace this with your command name
                    'opperation': command,
                    'ID_text': ID,
                    'exactCoverNum': exact,
                    'id': new Date().getTime()
                };
                $.get(url, data, function (resultTxt) {
                    if (resultTxt !== null) {
                        var resTxtArr = resultTxt.split("|");
                        var result = resTxtArr[0];
                        if (result === "Done") {
                            submitScreenRefresh("/PDC/PolicyHolderDetails.jsp");
                        } else {
                        }
                    } else {
                    }
                });
                data = null;
            }

            function getCoverByNumber(ID, command) {
                if (ID == null || ID == "" || ID == "null") {
                    $("#policyNumber_error").text("Please enter member number");
                } else
                {
                    if (ID != '')
                    {
                        document.getElementById('tblTags').style.visibility = 'visible';
                        document.getElementById('searchCalled').value = 'policyNumber';
                    } else
                    {
                        document.getElementById('tblTags').style.visibility = 'hidden';
                        document.getElementById('searchCalled').value = '';
                    }
                    var url = "/ManagedCare/AgileController";
                    var data = {
                        //replace this with your command name
                        'opperation': command,
                        'ID_text': ID,
                        'id': new Date().getTime()
                    };
                    $.get(url, data, function (resultTxt) {
                        if (resultTxt !== null) {
                            var resTxtArr = resultTxt.split("|");
                            var result = resTxtArr[0];
                            if (result === "Done") {
                                submitScreenRefresh("/PDC/MemberSearch.jsp");
                            } else {
                            }
                        } else {
                        }
                    });
                }
                data = null;
            }

            function submitScreenRefresh(screen) {
                $("#opperation").val("RefreshCommand");
                $("#refreshScreen").val(screen);
                document.forms[0].submit();
            }

            function validateMember(action) {
                //var memberNo = $("#policyNumber_text").val();
                //validate
                //getCoverByNumber(memberNo, 'policyNumber');
                if (error == false) {
                    submitWithAction(action);
                }

            }


            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.getElementById('onScreen').value = onScreen;
                document.forms[0].submit();
            }

            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithTblSelection(index, action) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.getElementById('dependentNumber').value = index;
                document.getElementById('coverNumber').value = document.getElementById('policyNumber_text').value; 
                document.forms[0].submit();
            }


            function displayDepJoinDate(depCode) {
                xhr = GetXmlHttpObject();
                if (xhr == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=DisplayCoverDepJoinDate&depCode=" + depCode;
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var resText = xhr.responseText;
                        var strArr = resText.split('|');
                        var result = strArr[0];
                        if (result == "Done") {
                            $("#depJoinDate").text(strArr[1]);
                            $("#depJoinDateRow").show();
                            document.getElementById('memberButton').disabled = false;
                        } else {
                            $("#depListValues_error").val(strArr[1]);
                            document.getElementById('memberButton').disabled = true;
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function GetDOMParser(xmlStr) {
                var xmlDoc;
                if (window.DOMParser) {
                    parser = new DOMParser();
                    xmlDoc = parser.parseFromString(xmlStr, "text/xml");
                } else {
                    xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async = "false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }


            function navigateToRiskrate(depNo, covNo, name, surname) {
                document.getElementById('dependentNumber').value = depNo;
                document.getElementById('coverNumber').value = covNo;
                document.getElementById('patName').value = name;
                document.getElementById('patSurname').value = surname;
                document.getElementById('manualRatingFlag').value = 'true';
                submitWithAction("ViewRatePatientCommand");
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <label class="header">Search Member</label>
                    <br>
                    <table>
                        <agiletags:ControllerForm name="ViewClaimsForm" >

                            <input type="hidden" name="opperation" id="opperation" value="" />

                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="coverNumber" id="coverNumber" value="" />
                            <input type="hidden" name="depTypeId" id="depTypeId" value="" />
                            <input type="hidden" name="entityId" id="entityId" value="" />
                            <input type="hidden" name="depNumber" id="depNumber" value="" />
                            <input type="hidden" name="optionId" id="optionId" value="" />
                            <input type="hidden" name="entityCommon" id="entityCommon" value="" />
                            <input type="hidden" name="refreshScreen" id="refreshScreen" value="" />
                            <input type="hidden" name="patName" id="patName" value="" />
                            <input type="hidden" name="patSurname" id="patSurname" value="" />
                            <input type="hidden" name="manualRatingFlag" id="manualRatingFlag" value="" />

                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="exactCoverNum" id="exactCoverNum" value="1" />
                            <input type="hidden" name="listIndex" id="listIndex" value=""/>
                            <input type="hidden" name="endDate" id="endDate" value="" />
                            <input type="hidden" name="startDate" id="startDate" value="" />

                            <input type="hidden" name="dateFrom" id="dateFrom" value=" " /><!-- Used for the VieOpenBenchmarkCommand.java -->
                            <input type="hidden" name="dateTo" id="dateTo" value=" " /><!-- Used for the VieOpenBenchmarkCommand.java -->
                            <input type="hidden" name="pdcType" id="pdcType" value=" " /><!-- Used for the VieOpenBenchmarkCommand.java -->
                            <input type="hidden" name="riskRating" id="riskRating" value=" " /><!-- Used for the VieOpenBenchmarkCommand.java -->
                            <input type="hidden" name="pdcUser" id="pdcUser" value=" " /><!-- Used for the VieOpenBenchmarkCommand.java -->
                            <input type="hidden" name="eventStatus" id="eventStatus" value=" " /><!-- Used for the VieOpenBenchmarkCommand.java -->
                            <input type="hidden" name="dependentNumber" id="dependentNumber" value="" />

<!-- OLD ONE <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Member Number" elementName="policyNumber" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/PDC/MemberSearch.jsp" mandatory="yes" javaScript="onChange=\"getCoverByNumber(this.value,'policyNumber');\""/></tr> -->

                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Member Number" elementName="policyNumber" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/PDC/MemberSearch.jsp" mandatory="yes" javaScript="onChange=\"getCoverByNumber(this.value, 'CoverDependantResultCommand');\""/></tr>

                            <!--<tr><agiletags:EntityCoverDependantDropDown displayName="Cover Dependants" elementName="depListValues" javaScript="onChange=\"displayDepJoinDate(this.value);\"" mandatory="no" valueFromSession="yes"/></tr>
                            <tr>
                                <td colspan="2" align="right">
                                    <button id="memberButton" name="memberButton" type="button" onClick="validateMember('ViewPolicyDetailsCommand')" value="" disabled >View</button>
                                </td>                                
                            </tr> -->

                            <table id="tblTags" width=100% height=100%>
                                <tr valign="top">
                                    <td width="50px" align="left">
                                        <br/><br/>
                                        <!-- content goes here -->
                                        <label class="header">Cover Dependants</label>
                                        <br/>
                                        <br/>
                                        <HR color="#666666" WIDTH="100%" align="left">
                                        <br/>

                                        <agiletags:CoverDependantResultTag sessionAttribute="" commandName="" javaScript="onClick=\"submitWithTblSelection(this.value,'ViewWorkBenchCommand');\"" onScreen="" buttonDisplay="Select" onPage="MemberS"/> 

                                        <br/><br/>
                                        <!-- content goes here -->
                                        <label class="header">Waiting Periods</label>
                                        <br/>
                                        <br/>
                                        <HR color="#666666" WIDTH="100%" align="left">
                                        <br/>
                                        <agiletags:CoverDependantWaitingPeriodTag sessionAttribute="" commandName="" javaScript="" onScreen="/PDC/MemberSearch.jsp" /> 
                                        <br/><br/>
                                    </td>
                                </tr>
                            </table>
                        </agiletags:ControllerForm>
                    </table>
                </td>
            </tr>
        </table>


    </body>
</html>

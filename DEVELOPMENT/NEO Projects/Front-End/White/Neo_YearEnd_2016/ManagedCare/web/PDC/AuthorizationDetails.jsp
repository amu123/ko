<%-- 
    Document   : AuthorizationDetails
    Created on : 2010/04/16, 02:27:03
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" language="JavaScript">

            var lastVisible = '1';
            function showOrHide(index) {
                document.getElementById(lastVisible).style.display = 'none';
                document.getElementById(index).style.display = 'block';
                lastVisible = index;
            }

            $(document).ready(function () {
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });

                var resultTxt = $("#pdcAuthResultMsg").val();
                if (resultTxt != null && resultTxt != "" && resultTxt != "null") {
                    $("#pdcAuthResultMsgTxt").text(resultTxt);
                } else {
                    $("#pdcAuthResultMsgTxt").text("");
                }
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithActionOne(action) {
                var dateFrom = $("#dateFrom").val();
                var dateTo = $("#dateTo").val();

                var valid = true;
                ClearValidation();

                if (dateFrom != null && dateFrom != "" && dateFrom != "null") {
                    valid = validateDate(dateFrom);
                    if (!valid) {
                        $("#dateFrom_error").text("Incorrect Date Format");
                    }
                    if (dateTo == null || dateTo == "" || dateTo == "null") {
                        valid = false;
                        $("#dateTo_error").text("Please enter to date");
                    }
                }
                if (dateTo != null && dateTo != "" && dateTo != "null") {
                    valid = validateDate(dateTo);
                    if (!valid) {
                        $("#dateTo_error").text("Incorrect Date Format");
                    }
                    if (dateFrom == null || dateFrom == "" || dateFrom == "null") {
                        valid = false;
                        $("#dateFrom_error").text("Please enter to date");
                    }
                }

                if (valid) {
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }
            }

            function validateDate(dateString) {

                //  check for valid numeric strings
                var strValidChars = "0123456789/";
                var strChar;
                var strChar2;

                if (dateString.length < 10 || dateString.length > 10)
                    return false;

                //  test strString consists of valid characters listed above
                for (i = 0; i < dateString.length; i++)
                {
                    strChar = dateString.charAt(i);
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        return false;
                    }
                }

                // test if slash is at correct position (yyyy/mm/dd)
                strChar = dateString.charAt(4);
                strChar2 = dateString.charAt(7);
                if (strChar != '/' || strChar2 != '/')
                    return false;

                return true;
            }

            function submitWithAction(action, index, code) {
                document.getElementById('opperation').value = action;
                document.getElementById('authNumber').value = index;
                document.getElementById('authCode').value = code;
                document.forms[0].submit();
            }

            function ClearValidation() {
                $("#dateFrom_error").text("");
                $("#dateTo_error").text("");
            }

            function ClearValidationContents() {
                document.getElementById('dateFrom').value = "";
                document.getElementById('dateTo').value = "";
            }


        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <!-- content goes here -->
                    <agiletags:PDCTabs tabSelectedIndex="link_Auth" />
                    <fieldset id="pageBorder">
                        <div class="content_area" >
                            <label class="header">Authorisation Details</label>
                            <br/>
                            <br/>
                            <agiletags:ControllerForm name="authorizationDetails">
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <input type="hidden" name="authNumber" id="authNumber" value="" />
                                <input type="hidden" name="authCode" id="authCode" value="" />
                                <input type="hidden" name="pdcAuthResultMsg" id="pdcAuthResultMsg" value="${requestScope.pdcAuthResultMsg}" />

                                <table>
                                    <tr><agiletags:LabelTextBoxDate displayname="Date From" elementName="dateFrom" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxDate displayname="Date To" elementName="dateTo" valueFromSession="yes"/></tr>
                                </table>
                                <br/>
                                <table>
                                    <tr>
                                        <agiletags:ButtonOpperation align="right" span="1" type="button" commandName="PreviewAuthCommand" displayname="Preview" javaScript="onClick=\"submitWithActionOne('PreviewAuthCommand');\""/>                                
                                        <td></td><td><button type="button" onClick="ClearValidationContents()" value=""  >Cancel</button></td>

                                    </tr>
                                </table>
                                <br/>
                                <table>
                                    <tr><agiletags:DropDownPerPatientLabel displayName="Authorization Type" elementName="authList" lookupId="89"  sessionValue="authList" javaScript="onchange=\"showOrHide(this.value);\""/></tr>
                                </table>
                                <br/>
                                <label id="pdcAuthResultMsgTxt" class="red"></label></br></br>
                                <div id="1" style="display:none">
                                    <br/>
                                    <label class="header">Optical Authorisation(s)</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:AuthorizationHistoryOpticalTable/>
                                    </table>
                                </div>

                                <div id="2" style="display:none">
                                    <br/>
                                    <label class="header">Dental Authorisation(s)</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:AuthorizationDentalFirstTabel/>
                                    </table>
                                </div>

                                <div id="7" style="display:none">
                                    <br/>
                                    <label class="header">Tariff Authorisation(s)</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:AuthorizationHistoryTariffFirstTable/>
                                    </table>
                                </div>

                                <div id="5" style="display:none">
                                    <br/>
                                    <label class="header">Condition Specific Authorisation(s)</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:AuthorizationOrhtodontic/>
                                    </table>
                                </div>

                                <div id="6" style="display:none">
                                    <br/>
                                    <label class="header">Condition Specific Authorisation(s)</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:AuthorizationNappi/>
                                    </table>
                                </div>

                                <div id="4" style="display:none">
                                    <br/>
                                    <label class="header">Hospital Authorisation(s)</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:AuthorizationHospitalFirstTable/>
                                    </table>
                                </div>

                                <div id="3" style="display:none">
                                    <br/>
                                    <label class="header">Orthodontic Authorisation(s)</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:AuthorizationOrhtodontic/>
                                    </table>
                                </div>

                                <div id="8" style="display:none">
                                    <br/>
                                    <label class="header">Above Benefit Authorisation(s)</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:AuthorizationHistoryAboveBenefitTable/>
                                    </table>
                                </div>

                                <div id="9" style="display:none">
                                    <br/>
                                    <label class="header">Radiology Authorisation(s)</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:PDCAuthRadiologyTag/>
                                    </table>
                                </div>

                                <div id="10" style="display:none">
                                    <br/>
                                    <label class="header">Trauma Counseling Authorisation(s)</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:PDCAuthTraumaTag/>
                                    </table>
                                </div>

                                <div id="11" style="display:none">
                                    <br/>
                                    <label class="header">Hospice Care Authorisation(s)</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:PDCAuthHospiceCareTag/>
                                    </table>
                                </div>

                                <div id="12" style="display:none">
                                    <br/>
                                    <label class="header">International Travel Authorisation(s)</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:PDCAuthInternationalTravelTag/>
                                    </table>
                                </div>
                            </agiletags:ControllerForm>
                        </div></fieldset>
                </td></tr></table>
    </body>
</html>
<%-- 
    Document   : WorkbenchDetails
    Created on : 2010/04/15, 12:34:11
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Event Details</title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script> 
        <script type="text/javascript" language="JavaScript">
            $(document).ready(function () {
                console.log("${applicationScope.Client == 'Sechaba'}")
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function pageLoad() {
            <%String activeMember = session.getAttribute("activeMember") + "";%>
                $("#msg").text('<% out.print(activeMember);%>');

            }
        </script>
    </head>
    <body onload="pageLoad();">
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">

                    <agiletags:PDCTabs tabSelectedIndex="link_WB" />
                    <fieldset id="pageBorder">
                        <div class="content_area">
                            <label class="header">Workbench - Details</label>
                            <br/>
                            <br/>
                            <agiletags:ControllerForm name="workbenchDetails">
                                <input type="hidden" name="opperation" id="opperation" value="" />

                                <table>
                                    <tr>
                                        <td width="300"></td>
                                        <td align="right"><label  id="msg" class="red"><b></b></label></td>
                                    </tr>
                                </table>
                                <br/>

                                <table>
                                    <!-- Exits elsewhere in the PDC module
                                               
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Description" elementName="descrition" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Event Status" elementName="eventStatus" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Patient Number" elementName="patientNumber" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Patient Name" elementName="patientName" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Gender" elementName="patientGender" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Age" elementName="patientAge" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Event Date" elementName="eventDate" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Risk Rating" elementName="riskRating" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Source" elementName="source" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Product Name" elementName="productName" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Option Name" elementName="optionName" valueFromSession="yes"/></tr>
                                    -->

                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Authorization Number" elementName="authorizationNumber" valueFromSession="yes"/></tr>
                                    <c:choose>
                                        <c:when test="${applicationScope.Client == 'Sechaba'}">
                                            <tr><agiletags:LabelTextBoxError readonly="yes" displayName="PCC Type" elementName="pdcType" valueFromSession="yes"/></tr>
                                        </c:when>    
                                        <c:otherwise>
                                            <tr><agiletags:LabelTextBoxError readonly="yes" displayName="PDC Type" elementName="pdcType" valueFromSession="yes"/></tr>
                                        </c:otherwise>
                                    </c:choose>
                                    <tr><agiletags:LabelTextAreaError readonly="yes" displayName="Risk Rating Reason" elementName="riskRatingReason" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Risk Rating Rule ID" elementName="RRRid" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Assigned Date" elementName="assignedDate" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Assigned By" elementName="assignedBy" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Assigned To" elementName="assignedTo" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="yes" displayName="ICD10 Code Description" elementName="ICDdescrition" valueFromSession="yes"/></tr>
                                </table>
                                <br/>
                                <table>
                                    <tr>
                                        <!--Moved to a pdc tab
                                        <agiletags:ButtonOpperation align="left" displayname="Re-Assign" commandName="ViewReAssignPHMCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('ViewReAssignPHMCommand')\";"/>
                                        <agiletags:ButtonOpperation align="left" displayname="View Policy Holder Details" commandName="ViewPolicyHolderDetailsCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('ViewPolicyHolderDetailsCommand')\";"/>  
                                        -->
                                    </tr>
                                </table>
                            </agiletags:ControllerForm>
                        </div></fieldset>
                </td></tr></table>
    </body>
</html>

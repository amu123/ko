<%-- 
    Document   : PolicyHolderAddressDetails
    Created on : 2010/04/15, 03:12:42
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Policy Holder Address Details</title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" language="JavaScript">

            $(document).ready(function () {
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <!-- content goes here -->
                    <agiletags:PDCTabs tabSelectedIndex="link_AD" />
                    <fieldset id="pageBorder">
                        <div class="content_area" >
                            <label class="header">Dependant Address Details</label>
                            <br/>
                            <br/>
                            <agiletags:ControllerForm name="policyHolderAddressDetails">
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <table>
                                    <label class="subheader">Physical Address</label>
                                    <br/>
                                    <br/>
                                    <table>
                                        <tr><agiletags:LabelTextBoxError displayName="Line 1" elementName="PhysicalAddress"  valueFromSession="Yes"/></tr>
                                        <tr><agiletags:LabelTextBoxError displayName="Line 2" elementName="PhysicalAddress1"  valueFromSession="Yes"/></tr>
                                        <tr><agiletags:LabelTextBoxError displayName="Line 3" elementName="patientPhysicalAddress2"  valueFromSession="Yes"/></tr>
                                        <tr><agiletags:LabelTextBoxError displayName="Physical Code" elementName="PhysicalCode"  valueFromSession="Yes"/></tr>
                                    </table>
                                    <br/>
                                    <br/>
                                    <label class="subheader">Postal Address</label>
                                    <br/>
                                    <table>
                                        <tr><agiletags:LabelTextBoxError displayName="Line 1" elementName="PostalAddress"  valueFromSession="Yes"/></tr>
                                        <tr><agiletags:LabelTextBoxError displayName="Line 2" elementName="PostalAddress1"  valueFromSession="Yes"/></tr>
                                        <tr><agiletags:LabelTextBoxError displayName="Line 3" elementName="PostalAddress2"  valueFromSession="Yes"/></tr>
                                        <tr><agiletags:LabelTextBoxError displayName="Postal Code" elementName="PostalCode"  valueFromSession="Yes"/></tr>
                                    </table>
                                    <br/>
                                    <br/>
                                    <label class="subheader">Contact Details</label>
                                    <br/>
                                    <table>
                                        <tr><agiletags:LabelTextBoxError displayName="Home Telephone Number" elementName="homeNumber"  valueFromSession="Yes"/></tr>
                                        <tr><agiletags:LabelTextBoxError displayName="Fax Number" elementName="faxNumber"  valueFromSession="Yes"/></tr>
                                        <tr><agiletags:LabelTextBoxError displayName="Work Number" elementName="workNumber"  valueFromSession="Yes"/></tr>
                                        <tr><agiletags:LabelTextBoxError displayName="Cell Phone Number" elementName="cellNumber"  valueFromSession="Yes"/></tr>
                                        <tr><agiletags:LabelTextBoxError displayName="Email" elementName="email"  valueFromSession="Yes"/></tr>
                                    </table>

                                    <table>
                                        <c:if test="${sessionScope.isPrinciple == false}">
                                            <agiletags:ButtonOpperation align="left" displayname="Update Address" commandName="SavePdcContactDetails" span="3" type="button" javaScript="onClick=\"submitWithAction('SavePdcContactDetails')\";"/>
                                        </c:if>
                                    </table>
                                </agiletags:ControllerForm>
                        </div></fieldset>
                </td></tr></table>
    </body>
</html>

<%-- 
    Document   : RiskRateHistory
    Created on : Feb 17, 2012, 9:15:32 AM
    Author     : Princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script>
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

        </script>
        <style type="text/css">
            .btn {
                color:#666666;
                font: bold 84% 'trebuchet ms',helvetica,sans-serif;
            }
        </style>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">
                    <!-- content goes here -->

                    <br/>
                    <br/>
                    <label class="header">Risk Rate History</label>
                    <br/>
                    <br/>
                    <agiletags:ControllerForm name="RiskRateHistory">
                        <table>

                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <table>
                                <tr><agiletags:LabelTextBoxError displayName="Policy Number" elementName="policyNumber" valueFromSession="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Name" elementName="patientName" valueFromSession="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="surname" valueFromSession="yes"/></tr>
                                <br/>
                                <tr><td><agiletags:ButtonOpperation align="left" displayname="Back" commandName="ViewRatePatientCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('ViewRatePatientCommand')\";"/></td></tr>

                            </table>
                            <br/>
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:RiskRateHistoryTable/>
                            </table>


                        </table>
                    </agiletags:ControllerForm>
                    <br/>
                </td></tr></table>
    </body>
</html>

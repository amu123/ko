<%-- 
    Document   : ClaimsHistory
    Created on : 2010/04/19, 04:12:34
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" language="JavaScript">


            $(document).ready(function () {
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });


            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithActionForm(action) {
                var valid = true;
                $("#notes_error").text("");

                var note = $("#notes").val();
                if (note == "") {
                    $("#notes_error").text("Please enter the text");
                    valid = false;
                }
                if (valid) {
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }

            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <!-- content goes here -->
                    <agiletags:PDCTabs tabSelectedIndex="link_MN" />
                    <fieldset id="pageBorder">
                        <div class="content_area" >
                            <c:choose>
                                <c:when test="${applicationScope.Client == 'Sechaba'}">
                                    <label class="header">PCC Specific Notes</label>
                                </c:when>    
                                <c:otherwise>
                                    <label class="header">PDC Specific Notes</label>
                                </c:otherwise>
                            </c:choose>
                            <br/>
                            <table>
                                <agiletags:ControllerForm name="savePdcNotes" validate="yes">
                                    <input type="hidden" name="opperation" id="opperation" value="" /> 
                                    <input type="hidden" name="entityId" id="entityId" value="${entityId}" />
                                    <input type="hidden" name="noteId" id="PdcAppNotes_note_id" value="" />
                                    <agiletags:HiddenField elementName="searchCalled" />

                                    <tr><agiletags:LabelTextAreaErrorBig valueFromSession="Yes" displayName="Notes" elementName="notes"/></tr>
                                    <br/>
                                    <br/>
                                    <tr><td align="right"><agiletags:ButtonOpperationLabelError elementName="saveNoteButton" commandName="AddPdcNoteToList" displayName="Save" type="button" javaScript="onClick=\"submitWithActionForm('AddPdcNoteToList');\""/></td></tr>
                                    </agiletags:ControllerForm>

                                <tr><agiletags:PdcNoteListDisplay commandName="" javaScript="" sessionAttribute="pdcNoteList" /></tr>
                            </table>
                        </div></fieldset>
                </td></tr></table>
    </body>
</html>


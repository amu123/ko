<%-- 
    Document   : CaptureClaims_SearchMemberResults
    Created on : 14 Apr 2016, 2:48:08 PM
    Author     : janf
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="100%">
    <thead>
        <tr>
            <th>Member Number</th>
            <th>Name</th>
            <th>Surname</th>
            <th>Scheme</th>
            <th>Plan</th>
            <th>Status</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="item" items="${SearchMembersResults}">
            <tr>
                <td><label>${item.memberNumber}</label></td>
                <td><label>${item.name}</label></td>
                <td><label>${item.surname}</label></td>
                <td><label>${item.scheme}</label></td>
                <td><label>${item.plan}</label></td>
                <td><label>${item.status}</label></td>
                <td><input type="button" value="Select" onclick="selectClaimMember('${item.memberNumber}','${item.surname},${item.name}',false);"></td>
            </tr>
        </c:forEach>
    </tbody>
</table>
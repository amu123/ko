<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="100%">
    <thead>
        <tr>
            <th>Provider ID</th>
            <th>Provider Number</th>
            <th>Initials</th>
            <th>Name</th>
            <th>Surname</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="item" items="${SearchProviderResults}">
            <tr>
                <td><label>${item.entityId}</label></td>
                <td><label>${item.providerNumber}</label></td>
                <td><label>${item.initials}</label></td>
                <td><label>${item.name}</label></td>
                <td><label>${item.surname}</label></td>
                <td><input type="button" value="Select" onclick="selectClaimProvider('${item.providerNumber}','${item.surname}, ${item.initials}',false,'${item.providerCategory.value}');"></td>
            </tr>
        </c:forEach>
    </tbody>
</table>
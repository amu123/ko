<%-- 
    Document   : ClaimsNotesDetails
    Created on : 26 May 2016, 12:43:45 PM
    Author     : janf
--%>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<agiletags:ControllerForm name="ClaimsNotesForm" >
    <input type="hidden" name="opperation" id="opperation" value="ClaimNotesCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="ClaimsNotesDetails" />
    <input type="hidden" name="claimId" id="claimId" value="${claimId}" />
    <hr>
    <label class="subheader">Claims Notes Details</label>
    <div id="ClaimsNotes_Summary">
        <jsp:include page="ClaimsNotes_NoteSummary.jsp" />
    </div> 
    <table>
        <td><input type="button" name="AddNotesButton" value="Add Note" onclick="addClaimNote();"/></td>
        <!--<td><input type="button" name="SearchNotesButton" value="Search" onclick="swapDivVisbible('DetailedClaimNotes_Div', 'Search_Notes_Div');"/></td>-->
        <tr><agiletags:ButtonOpperation align="right" span="5" type="button" commandName="ReturnClaimDetails" displayname="Return" javaScript="onClick=\"submitWithActionReturn('ReturnClaimDetails');\""/></tr>
    </table>
</agiletags:ControllerForm>
<script>
        $(function(){
            fetchClaimNotes();
        });
        
        function submitWithActionReturn(action) {
            //alert("action = " + action);
            document.getElementById('opperation').value = action;
            document.getElementById('onScreen').value = "ClaimsNotesDetails.jsp";
            document.forms[0].submit();
        }

</script>

<%-- 
    Document   : EditClaims_ClaimLine
    Created on : 03 May 2016, 4:30:51 PM
    Author     : janf
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>

<nt:NeoPanel title="Edit Claim Line ${claimLineId}"  collapsed="false" reload="false" contentClass="neopanelcontentwhite">
    <c:set var="a" value="adsss"></c:set>
    <agiletags:ControllerForm name="EditClaimLineForm">
        <input type="hidden" name="opperation" id="ClaimLine_opperation" value="EditClaimCommand" />
        <input type="hidden" name="onScreen" id="ClaimHeader_onScreen" value="ClaimLine" />
        <input type="hidden" name="ClaimLine_claimId" id="ClaimLine_claimId" value="${claimId}" />
        <input type="hidden" name="ClaimLine_Practice_Type" id="ClaimLine_Practice_Type" value="" />
        <input type="hidden" name="ClaimLine_Practice_No" id="ClaimLine_Practice_No" value="${ClaimLine_Practice_No}" />
        <input type="hidden" name="ClaimLine_Provicer_No" id="ClaimLine_Provider_No" value="${ClaimLine_Provider_No}" />
        <input type="hidden" name="test" id="test" value="${test}" />
        <input type="hidden" name="command" id="ClaimHeader_command" value="updateClaimLine" />
        <input type="hidden" name="claimLineNdcList" id="claimLineNdcList" value="${empty claimLineNdcList ? null : claimLineNdcList}}"/>
        <input type="hidden" name="icdList" id="icdList" value=""/>
        <input type="hidden" name="icdRefList" id="icdRefList" value=""/>
        <input type="hidden" name="toothList" id="toothList" value=""/>
        <input type="hidden" name="cptList" id="cptList" value=""/>
        <input type="hidden" name="modList" id="modList" value="${empty modList ? null : modList}"/>
        <input type="hidden" name="capOrEdit" id="capOrEdit" value="edit" />
        <input type="hidden" name="capOrEditStartLoad" id="capOrEditStartLoad" value="${empty startLoad ? true : false}" />
        <input type="hidden" name="claimLineId" id="claimLineId" value="${claimLineId}"/>
        <input type="hidden" name="reverse" id="reverse" value="${reverse}" />
        <input type="hidden" name="editClaimLineId" id="editClaimLineId" value=${editClaimLineId}"" />

        <table id="ClaimLineTable">
            <tbody>
                <tr><nt:NeoTextField displayName="Claim Category" elementName="ClaimLine_Category" lookupId="59" onchange="changeClaimLineCategory($(this).val());"  /></tr> 
            </tbody>
            <tbody id="ClaimLineTable_Person">
                <tr><nt:NeoTextField displayName="Cover Number" elementName="ClaimLine_CoverNumber" disabled="disabled" readonly="readonly" value="${coverNumber}" /></tr>
                <tr>
                    <td><label>Dependant</label></td>
                    <td>
                        <select id="ClaimLine_Dependant" name="ClaimLine_Dependant">
                            <c:forEach var="item" items="${covList}">
                                <option value="${item.entityId}_${item.dependentNumber}">${item.dependentNumber} - ${item.name} - ${agiletags:formatXMLGregorianDate(item.dateOfBirth)}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
            </tbody>
            <tbody id="ClaimLineTable_Date_side">
                <tr>
                    <nt:NeoTextField valueFromSession="true" displayName="Service Date From" elementName="ClaimLine_DateStart" mandatory="true" type="date" onblur="checkDate(\"ClaimLine_DateStart\",true);" />
            <tbody id="ClaimLine_DateEnd_div"><nt:NeoTextField displayName="Service Date To" elementName="ClaimLine_DateEnd" mandatory="true" type="date" onblur="validateNeoDateField(\"ClaimLine_DateEnd\",true,null);checkDateTo();" /></tbody>
                </tr>
            </tbody>
            <tbody id="ClaimLineTable_Time">
                <tr><nt:NeoTextField displayName="Time In" elementName="ClaimLine_Time_In" mandatory="true" onblur="validateTime($(this).val(),\"ClaimLine_Time_In\");" /></tr>
                <tr><nt:NeoTextField displayName="Time Out" elementName="ClaimLine_Time_Out" mandatory="true" onblur="validateTime($(this).val(),\"ClaimLine_Time_Out\");" /></tr>
            </tbody>
            <tbody id="ClaimLineTable_INV"> 
                <tr><nt:NeoTextField displayName="Laboratory Invoice Number" elementName="ClaimLine_Invoice_Number" mandatory="true"/></tr>
                <tr><nt:NeoTextField displayName="Laboratory Order Number" elementName="ClaimLine_Order_Number" mandatory="true"/></tr>
            </tbody>
            <tbody id="ClaimLineTable_Diag_side">
                <tr>
                    <nt:NeoTextField displayName="Diagnosis /ICD 10" elementName="ClaimLine_Diagnosis"  mandatory="false"  onkeypress="addDropDown(this, this.value, event)"/> 
                    <td><select id="ClaimLine_Diagnosis_DropDown" name="ClaimLine_Diagnosis_DropDown" onkeydown="removeDropDown(this, event, this.value,'ClaimLine_Diagnosis')" onchange="displayDropDown('ClaimLine_Diagnosis',$(this))" onblur="displayDropDown('ClaimLine_Diagnosis',$(this))"></select></td>
                </tr>
                <tr>
                    <nt:NeoTextField displayName="Referring Provider Diagnosis/ICD10" elementName="ClaimLine_Ref_Diagnosis"  mandatory="false" onkeypress="addDropDown(this, this.value, event)" /> 
                    <td><select id="ClaimLine_Ref_Diagnosis_DropDown" name="ClaimLine_Ref_Diagnosis_DropDown" onkeydown="removeDropDown(this, event, this.value,'ClaimLine_Ref_Diagnosis')" onchange="displayDropDown('ClaimLine_Ref_Diagnosis',$(this))" onblur="displayDropDown('ClaimLine_Ref_Diagnosis',$(this))"></select></td>
                </tr>
            </tbody>
            <tbody id="ClaimLineTable_Tariff">
                <tr><nt:NeoTextField displayName="Tariff Code" elementName="ClaimLine_Tariff"  mandatory="true" onblur="findTariff($(this).val());" /></tr>
            </tbody>
            <tbody id="ClaimLineTable_CPT"> 
                <tr>
                    <nt:NeoTextField displayName="CPT Code" elementName="ClaimLine_CPT"  mandatory="true"  onkeypress="addDropDown(this, this.value, event)"/>
                <td><select id="ClaimLine_CPT_DropDown" name="ClaimLine_CPT_DropDown" onkeydown="removeDropDown(this, event, this.value,'ClaimLine_CPT')" onchange="displayDropDown('ClaimLine_CPT',$(this))" onblur="displayDropDown('ClaimLine_CPT',$(this))"></select></td>
                </tr>
            </tbody>
            <tbody id="ClaimLineTable_Modifier">
                <tr>
                    <nt:NeoTextField displayName="Modifier" elementName="ClaimLine_Modifier" type="checkbox" readonly="readonly" disabled="disabled" />
                    <td><input type="button" value="Add Modifier" onclick="captureModifier('edit')"></td>
                </tr>
            </tbody>
            <tbody id="ClaimLineTable_NDC">
                <tr>
                    <nt:NeoTextField displayName="Nappi" elementName="ClaimLine_Nappi" type="checkbox" readonly="readonly" disabled="disabled" />
                    <td><input type="button" value="Add NDC" onclick="captureNappi(null,null,'edit')"></td>
                </tr>
            </tbody>
            <tr><nt:NeoTextField displayName="Multiplier" elementName="ClaimLine_Multiplier"  mandatory="true" value="1" /></tr>
            <tr><nt:NeoTextField displayName="Units" elementName="ClaimLine_Units"  mandatory="true" value="1"  /></tr>
            <tr><nt:NeoTextField displayName="Quantity" elementName="ClaimLine_Quantity"  mandatory="true" value="1" /></tr>
            <tr><nt:NeoTextField displayName="Authorization" elementName="ClaimLine_Authorization" /></tr>
        </tbody>
        <tbody id="ClaimLineTable_Dental">
            <tr>
                <nt:NeoTextField displayName="Tooth Numbers" elementName="ClaimLine_Tooth_Numbers" mandatory="false" onkeypress="addDropDown(this, this.value, event)" />
                <td><select id="ClaimLine_Tooth_Numbers_DropDown" name="ClaimLine_Tooth_Numbers_DropDown" onkeydown="removeDropDown(this, event, this.value,'ClaimLine_Tooth_Numbers')"  ></select></td>
            </tr>
        </tbody>
        <tbody id="ClaimLineTable_Optical">
            <tr><nt:NeoTextField displayName="Lens RX (Left)" elementName="ClaimLine_Lens_Left" mandatory="true"  /></tr>
            <tr><nt:NeoTextField displayName="Lens RX (Right)" elementName="ClaimLine_Lens_Right" mandatory="true"  /></tr>
        </tbody>
        <tbody id="ClaimLineTable_Amount">
            <tr><nt:NeoTextField displayName="Claim Amount" elementName="ClaimLine_Claimed_Amount" mandatory="true" value=""/></tr>
        </tbody>
        <tbody id="ClaimLineTable_Buttons">
            <tr>
                <!--<td><input type="button" value="Close" onclick="swapDivVisbible('Edit_Claim_Lines_Div', 'DetailedClaimLine_Div')"></td>-->
                <c:choose>
                        <c:when test="${reverse == 'true'}">
                            <td><input type="button" value="Reverse and Correct" onclick="reverseAndCorrectClaimLine(this.form, null, this);"></td>
                        </c:when>
                        <c:otherwise>
                            <td><input type="button" value="Back" onclick="swapDivVisbible('Edit_Claim_Lines_Div', 'Edit_Claims_Div')"></td>
                            <td><input type="button" value="Save" onclick="updateClaimLine(this.form, null, this);"></td>
                        </c:otherwise>
                    </c:choose>
            </tr>
        </tbody>
    </table>
</agiletags:ControllerForm>

</nt:NeoPanel>

<div id="ClaimLine_Summary">
    <jsp:include page="EditClaims_ClaimLineSummary.jsp" />
</div> 
<script>
    //changeClaimLineCategory({ClaimLine_Category});
    $(function() {
        var category = $("#ClaimLine_Category").val();
        changeClaimLineCategory(category);
    });

      //in claims.js
  /*  function changeClaimLineCategory(val) {
        alert("claimCategoryVal = " + val);
        resetClaimLine();
        $('#ClaimLineTable_Dental').hide();
        $('#ClaimLineTable_Optical').hide();
        $('#ClaimLineTable_Modifier').hide();
        $('#ClaimLineTable_CPT').hide();
        $('#ClaimLineTable_INV').hide();
        $('#ClaimLineTable_Time').hide();
        $('#ClaimLineTable_Diag_side').show();
        $('#ClaimLineTable_Date_side').show();
        $('#ClaimLineTable_Diag_under').hide();
        $('#ClaimLineTable_Date_under').hide();
        $('#ClaimLine_Tooth_Numbers_DropDown').hide();
        $('#ClaimLine_CPT_DropDown').hide();
        $('#ClaimLine_Diagnosis_DropDown').hide();
        $('#ClaimLine_Ref_Diagnosis_DropDown').hide();
        //$('#ClaimLineTable_Nappi_Details').hide();

        if (val == 5 || val == 7) {
            $('#ClaimLineTable_Time').show();
        }

        if (val == 5 || val == 10) {
            $('#ClaimLineTable_CPT').show();
        }

        if (val == 2 || val == 3 || val == 5 || val == 7 || val == 8 || val == 9 || val == 10) {
            $('#ClaimLineTable_Modifier').show();
        }

        if (val == 8 || val == 9 || val == 10) {
            $('#ClaimLineTable_INV').show();
        }

        if (val == 2) {
            $('#ClaimLineTable_Optical').show();
        }

        if (val == 8 || val == 3) {
            $('#ClaimLineTable_Dental').show();
        }

        /*if (val == 1 || val == 4 || val == 6 || val == 11) {
         $('#ClaimLineTable_Diag_under').show();
         $('#ClaimLineTable_Date_under').show();
         $('#ClaimLineTable_Diag_side').hide();
         $('#ClaimLineTable_Date_side').hide();
         }
    }
    
    function addDropDown(element, e, value) {
    var charCode;
    var field = $(element).attr('id');
    var valid = true;
    var error = field + "_error";
 
    if(e && e.which){
        charCode = e.which;
    }else if(window.event){
        e = window.event;
        charCode = e.keyCode;
    }
    
    if(charCode == 13) {
        alert("charcode = " + charCode + "\nValue = " + value + "\nelement = " + element + "\nField = " + field);
        var select;
        if(field === "ClaimLine_Tooth_Numbers") {
            select = document.getElementById('ClaimLine_Tooth_Numbers_DropDown');
            $('#ClaimLine_Tooth_Numbers_DropDown').show();
            alert(validateNeoIntField(field,false));
            if(!validateNeoIntField(field,false)){
                valid = false;
                $('#' + error).text('Please enter a valid number');
            }
        } else if(field === "ClaimLine_CPT") {
            findCPT(value);
            select = document.getElementById('ClaimLine_CPT_DropDown');
            $('#ClaimLine_CPT_DropDown').show();
        } else if(field === "ClaimLine_Diagnosis"){
            value = findICD(element);
            alert("value in IF = " + value);
            //findICD(element);
            select = document.getElementById('ClaimLine_Diagnosis_DropDown');
            $('#ClaimLine_Diagnosis_DropDown').show();
        } else if(field === "ClaimLine_Ref_Diagnosis") {
            //value = findICD(element);
            findICD(element);
            select = document.getElementById('ClaimLine_Ref_Diagnosis_DropDown');
            $('#ClaimLine_Ref_Diagnosis_DropDown').show();
        }
        alert("valid claim line = " + valid + "\nvalue = " + value);
        if(valid){
        var options = value;//["Asian", "Black", "White"];
        //var i;
        //for (i = 0; i < options.length; i++) {
            var opt = options;
            alert("options = " + options);
            var el = document.createElement("option");
            el.textContent = opt;
            el.value = opt;
            select.appendChild(el);
            $('#' + field).val('');
        //}
        }
    }
}*/

</script>

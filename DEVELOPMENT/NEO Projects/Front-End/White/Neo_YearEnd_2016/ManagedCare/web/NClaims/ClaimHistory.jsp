<%-- 
    Document   : ClaimHistory
    Created on : 2014/05/09, 10:28:07
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Claims/Claims.js"></script>
        <script>

            function submitReturnWithAction(action) {
                document.getElementById('opperation').value = action;
                document.getElementById('onScreen').value = "ClaimHistory.jsp";
                document.forms[0].submit();
            }

            function submitWithAction(action, claimLineId, paidAmount, depCode) {
                document.getElementById('opperation').value = action;
                document.getElementById('memberDetailedClaimLineId').value = claimLineId;
                document.getElementById('memberDetailedClaimPaidAmount').value = paidAmount;
                document.getElementById('memberDetailedClaimDepCode').value = depCode;
                document.forms[0].submit();
            }

            function submitPageAction(action, pageDirection) {
                document.getElementById('opperation').value = action;
                document.getElementById('mcPageDirection').value = pageDirection;
                document.getElementById('onClaimLineScreen').value = 'ClaimHistory.jsp';
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
    <c:if test="${applicationScope.Client == 'Sechaba'}">
        <div style="border-style: solid; border-color: #6cc24a">
            </c:if>
    <c:if test="${applicationScope.Client == 'Agility'}">
        <div style="border-style: solid; border-color: #660000">
    </c:if>
        <agiletags:ControllerForm name="ClaimHistoryForm">
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Claim Lines</label>
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="claimsHistoryMem">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="memberDetailedClaimLineId" id="memberDetailedClaimLineId" value="" />
                            <input type="hidden" name="memberDetailedClaimPaidAmount" id="memberDetailedClaimPaidAmount" value="" />
                            <input type="hidden" name="memberDetailedClaimDepCode" id="memberDetailedClaimDepCode" value="" />
                            <input type="hidden" name="mcPageDirection" id="mcPageDirection" value="" />
                            <input type="hidden" name="onClaimLineScreen" id="onClaimLineScreen" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="claimId" id="claimId" value="${sessionScope.memClaimId}" />
                        </agiletags:ControllerForm>
                    </table>
                    <br/>
                    <!--<HR color="#666666" WIDTH="50%" align="left">-->
                    <br/>
                    <agiletags:ClaimLineDetailsTable />
                    <br/>
                    <table>
                        <!--<tr><agiletags:ButtonOpperationLabelError displayName="Export Claim" elementName="exportClaim" type="button" javaScript="onClick=\"exportClaim();\""/></tr>
                        <tr><agiletags:ButtonOpperationLabelError displayName="Claim Notes" elementName="claimNotes" type="button" javaScript="onClick=\"submitWithAction('ForwardToClaimNotes');\""/></tr>-->
                        <tr>
                            <agiletags:ButtonOpperation align="right" span="5" type="button" commandName="ReturnClaimDetails" displayname="Return" javaScript="onClick=\"submitReturnWithAction('ReturnClaimDetails');\""/>
                            <agiletags:ButtonOpperation displayname="Claim Notes" type="button" javaScript="onClick=\"submitWithAction('ForwardToClaimNotes');\"" align="left" commandName="claimNotes" span="1"/>
                            <agiletags:ButtonOpperation displayname="Export Claim"  type="button" javaScript="onClick=\"exportClaim();\"" align="left" span="1" commandName="exportClaim" />
                        </tr>
                    </table>
                </td></tr></table>
            </agiletags:ControllerForm>
            </div>
    </body>
</html>
<%-- 
    Document   : CaptureClaims_ModifierSummary
    Created on : 11 Apr 2016, 11:02:10 AM
    Author     : janf
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>

<nt:NeoPanel title="Modifiers for Claim Line"  collapsed="false" reload="false">
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="100%">
        <thead>
            <tr>
                <th>Modifier</th>
                <th>Units</th>
                <th>Time</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="item" items="${modList}" varStatus="loop">
                <tr>
                    <td><label>${item.modifier}</label></td>
                    <td><label>${item.quantity}</label></td>
                    <td><label>${item.totalTime}</label></td>
                    <td><label><input type="button" value="Select" onclick="modifyModifierFromList(${loop.index})"</label></td>
                    <td><label><input type="button" value="Delete" onclick="removeModifierFromList(${loop.index})"</label></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</nt:NeoPanel>    


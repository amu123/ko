<%-- 
    Document   : ViewClaims
    Created on : 2014/05/08, 12:07:47
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script> 
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Claims/Claims.js"></script>
        <script type="text/javascript">

            $(function() {
                //For Practice validation
                var searchVal = document.getElementById('searchCalled').value;
                if (searchVal == 'practiceNumber') {
                    validatePractice(document.getElementById('practiceNumber_text').value, 'practiceNumber');
                }
                if (searchVal == 'policyHolderNumber') {
                    getCoverByNumber(document.getElementById('policyHolderNumber_text').value, 'policyHolderNumber');
                }
                document.getElementById('searchCalled').value = '';

                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });
                
                var revError = $("#reverseError").val();
                $("#revError").text("");
                if(revError != null && revError != "" && revError != "null"){
                    $("#revError").text(revError);
                }

            });

            function GetXmlHttpObject() {
                if (window.ActiveXObject) {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function GetDOMParser(xmlStr) {
                var xmlDoc;
                if (window.DOMParser) {
                    parser = new DOMParser();
                    xmlDoc = parser.parseFromString(xmlStr, "text/xml");
                } else {
                    xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async = "false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }

            //cover search
            function getCoverByNumber(str, element) {
                if (str != null && str != "") {
                    xhr = GetXmlHttpObject();
                    if (xhr == null) {
                        alert("ERROR: Browser Incompatability");
                        return;
                    }
                    var url = "/ManagedCare/AgileController";
                    url += "?opperation=GetCoverDetailsByNumber&number=" + str + "&element=" + element;
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState == 4 && xhr.statusText == "OK") {
                            $("#depListValues").empty();
                            $("#depListValues").append("<option value=\"99\"></option>");
                            var xmlDoc = GetDOMParser(xhr.responseText);
                            //print value from servlet
                            $(xmlDoc).find("EAuthCoverDetails").each(function() {
                                //set product details
                                var prodName = $(this).find("ProductName").text();
                                var optName = $(this).find("OptionName").text();

                                //extract dependent info from tag
                                $(this).find("DependantInfo").each(function() {
                                    var depInfo = $(this).text();
                                    var nameSurnameType = depInfo.split("|");
                                    //Add option to dependant dropdown
                                    var optVal = document.createElement('OPTION');
                                    optVal.value = nameSurnameType[0];
                                    optVal.text = nameSurnameType[1];
                                    document.getElementById("depListValues").options.add(optVal);
                                });
                                //DISPLAY MEMBER DETAIL LIST
                                //$(document).find("#schemeName").text(prodName);
                                //$(document).find("#schemeOptionName").text(optName);
                                //error check
                                $("#" + element + "_error").text("");
                                var errorText = $(this).text();
                                var errors = errorText.split("|");
                                if (errors[0] == "ERROR") {
                                    $("#" + element + "_error").text(errors[1]);
                                }

                            });
                        }
                    }
                    xhr.open("POST", url, true);
                    xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                    xhr.send(null);
                }
            }

            //practice search
            function validatePractice(str, element) {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var error = "#" + element + "_error";
                if (str.length > 0) {
                    var url = "/ManagedCare/AgileController";
                    url = url + "?opperation=FindPracticeDetailsByCode&provNum=" + str + "&element=" + element;
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState == 4 && xhr.statusText == "OK") {
                            var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                            $(document).find(error).text("");
                            if (result == "Error") {
                                $(document).find(error).text("No such provider");
                            } else if (result == "Done") {
                                $(document).find(error).text("");
                            }
                        }
                    };
                    xhr.open("POST", url, true);
                    xhr.send(null);
                } else {
                    $(document).find(error).text("");
                }
            }

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, claimId, providerNo) {
                document.getElementById('opperation').value = action;
                document.getElementById('memClaimId').value = claimId;
                document.getElementById('memClaimProvNum').value = providerNo;
                document.forms[0].submit();
            }

            function submitReverseAction(action, claimId) {
                var rrVal = "#revReason_" + claimId;
                var rrErrVal = rrVal + "_error";
                var rr = $(rrVal).val();
                $(rrErrVal).text("");
                if (rr !== null && rr !== "" && rr !== "99") {
                    document.getElementById('opperation').value = action;
                    document.getElementById('memClaimId').value = claimId;
                    document.getElementById('reversalReason').value = rr;
                    document.forms[0].submit();
                } else {
                    $(rrErrVal).text("Reversal reason is required to complete this process");
                }

            }


            function submitPageAction(action, pageDirection) {
                document.getElementById('opperation').value = action;
                document.getElementById('mcPageDirection').value = pageDirection;
                document.forms[0].submit();
            }
            
        </script>
    </head>
    <body>
        <c:if test="${applicationScope.Client == 'Sechaba'}">
        <div style="border-style: solid; border-color: #6cc24a">
            </c:if>
    <c:if test="${applicationScope.Client == 'Agility'}">
        <div style="border-style: solid; border-color: #660000">
    </c:if>
        <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">
                    <!--<label class="header">Claim Search</label>-->

                    <nt:NeoPanel title="Claim Search"  collapsed="true" reload="false" contentClass="neopanelcontentwhite">
                    <table>
                        <agiletags:ControllerForm name="memberDetails">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="mcPageDirection" id="mcPageDirection" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="memClaimId" id="memClaimId" value=""/>
                            <input type="hidden" name="memClaimProvNum" id="memClaimProvNum" value=""/>
                            <input type="hidden" name="reversalReason" id="reversalReason" value=""/>
                            <input type="hidden" name="reverseError" id="reverseError" value="${sessionScope.revError}"/>
                            <input type="hidden" name="policyHolderNumber" id="policyHolderNumber" value=""/>

                            <tr>
                                <nt:NeoTextField valueFromSession="true" displayName="Cover Number" elementName="memNum"/>
                                <nt:NeoTextField valueFromSession="true" displayName="Dependant Number" elementName="depNum"/>
                            </tr>
                            <tr>
                                <nt:NeoTextField valueFromSession="true" displayName="Provider Number" elementName="providerNumber"  searchFunc=""/> <!--type="search"-->
                                <nt:NeoTextField valueFromSession="true" displayName="Provider Discipline ID" elementName="discId"/>
                            </tr>
                            <tr>
                                <nt:NeoTextField valueFromSession="true" displayName="Practice Number" elementName="practiceNumber"  searchFunc=""/> <!--type="search"-->
                                <!--<nt:NeoTextField displayName="Discipline Type" elementName="disciplineType" lookupId="59" value="99" />-->
                            </tr>
                            <tr>
                                <nt:NeoTextField valueFromSession="true" displayName="Process Date From" elementName="processFrom" type="date"/>
                                <nt:NeoTextField valueFromSession="true" displayName="Process Date To" elementName="processTo" type="date"/>
                            </tr>
                            <tr>
                                <nt:NeoTextField valueFromSession="true" displayName="Service Date From" elementName="serviceFrom" type="date"/>
                                <nt:NeoTextField valueFromSession="true" displayName="Service Date To" elementName="serviceTo" type="date"/>
                            </tr>
                            <tr>
                                <nt:NeoTextField valueFromSession="true" displayName="Account Number" elementName="accNumber"/>
                                <nt:NeoTextField valueFromSession="true" displayName="Scan Number" elementName="scanNum"/>
                            </tr>
                            <tr>
                                <nt:NeoTextField valueFromSession="true" displayName="Batch Number" elementName="batchNumber"/>
                                <nt:NeoTextField valueFromSession="true" displayName="Claim Id" elementName="claimId"/>
                            </tr>
                            <tr>
                                <nt:NeoTextField valueFromSession="true" displayName="Authorisation Number" elementName="authNumber"/>
                            </tr>
                            <tr>
                                <nt:NeoTextField valueFromSession="true" displayName="Payment Date" elementName="paymentDate" type="date"/>
                                <!--<nt:NeoTextField valueFromSession="true" displayName="Payment Run Id" elementName="payrunId"/>-->
                            </tr>
                            <tr>
                                <nt:NeoTextField valueFromSession="true" displayName="Tarrif Code" elementName="tariffCode"/>
                                <!--<nt:NeoTextField valueFromSession="true" displayName="Claim Status" elementName="claimStatus" lookupId="83"/>-->
                                <agiletags:LabelNeoLookupValueDropDownError displayName="Claim Status" elementName="claimStatus" lookupId="83"/>
                            </tr>
                            <tr>
                                <nt:NeoTextField valueFromSession="true" displayName="EDI File Number" elementName="ediClaim"/>
                                <nt:NeoTextField valueFromSession="true" displayName="Tooth Number" elementName="toothNum"/>
                            </tr>
                            
                            <!--<tr><agiletags:LabelTextBoxError  displayName="Member Number" elementName="memNum"  mandatory="no" javaScript=""/>
                            <agiletags:EntityCoverDependantDropDown displayName="Member Dependants" elementName="depList" javaScript="" mandatory="no"  /></tr>
                            <tr><agiletags:LabelTextSearchText  displayName="Provider Number" elementName="providerNum" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/NClaims/ViewClaims.jsp" mandatory="no" javaScript=""/>
                            <agiletags:LabelTextBoxError  displayName="Provider Discipline ID" elementName="discId" mandatory="no" javaScript=""/></tr>
                            <tr><agiletags:LabelTextBoxError  displayName="Account Number" elementName="accNumber"   mandatory="no" javaScript=""/>
                            <agiletags:LabelTextBoxError  displayName="Batch Number" elementName="batchNumber"  mandatory="no" javaScript=""/>
                            <tr><agiletags:LabelTextSearchText  displayName="Practice Number" elementName="practiceNum" searchFunction="yes" searchOperation="ForwardToSearchPracticeCommand" onScreen="/Claims/MemberClaimsDetails.jsp" mandatory="no" javaScript=""/>
                            <agiletags:EntityCoverDependantDropDown displayName="EDI File Number" elementName="ediClaim" javaScript="" mandatory="no"  />
                            <tr><agiletags:LabelTextBoxDate displayname="Process Date From" elementName="processFrom"/>
                            <agiletags:LabelTextBoxDate displayname="Process Date To" elementName="processTo"/>
                            <tr><agiletags:LabelTextBoxDate displayname="Service Date From" elementName="serviceFrom"/>
                            <agiletags:LabelTextBoxDate displayname="Servive Date To" elementName="serviceTo"/>
                            <tr><agiletags:LabelTextBoxError displayName="Claim Id" elementName="ClaimId"/>
                            <agiletags:LabelTextBoxError displayName="Authoristion Number" elementName="authNumber"/>
                            <tr><agiletags:LabelTextBoxDate displayname="Treatment Date From" elementName="treatmentFrom"/>
                            <agiletags:LabelTextBoxDate displayname="Treatment Date To" elementName="treatmentTo"/>
                            <tr>
                                <agiletags:LabelNeoLookupValueDropDownError displayName="Discipline Type" elementName="disciplineType" lookupId="59"/><td></td>
                                <tr><agiletags:LabelTextBoxDate displayname="Payment Date" elementName="paymentDate"/>
                                <tr><agiletags:LabelTextBoxError displayName="Scan Number" elementName="scanNum"   mandatory="no" javaScript=""/>
                                <agiletags:LabelNeoLookupValueDropDownError displayName="Claim Status" elementName="claimStatus" lookupId="83"/>
                                <tr><agiletags:LabelTextBoxError displayName="Tariff Code" elementName="tariffCode"  mandatory="no" javaScript=""/>
                                <agiletags:LabelTextBoxError displayName="Tooth Number" elementName="toothNum"  mandatory="no" javaScript=""/>
                            </tr>-->
                            <c:if test="${userRestriction != true}">
                                <tr><agiletags:ButtonOpperation align="left" span="1" type="button" commandName="SearchClaimCommand" displayname="Search Claims" javaScript="onClick=\"searchResults(this);\""/></tr>
                            </c:if>
                            <tr><td><label class="error" id="revError"></label></td></tr>
                         </agiletags:ControllerForm>
                    </table>
                    </nt:NeoPanel>
                    <!--<br/>
                    <<HR color="#666666" WIDTH="50%" align="left">
                    <br/>
                    <!--\<jsp:include page="ClaimSearchDetails.jsp" />
                    \<agiletags:ViewMemberClaimSearchResult sessionAttribute="memCCClaimSearchResult" />-->
                    <nt:NeoPanel title="Search Results"  collapsed="false" reload="true" contentClass="neopanelcontentwhite">
                        <agiletags:ViewClaimSearchResults sessionAttribute="viewClaimSearchResults"/>
                    </nt:NeoPanel>
                    <agiletags:NClaimHeaderTag />
                    </div>
                    <c:if test="${userRestriction == true}"><span class="neonotificationnormal neonotificationwarning">Content contained is confidential. Please refer to Team leader for assistance</span></c:if>

                </td></tr></table>
                    </div>
    </body>
</html>


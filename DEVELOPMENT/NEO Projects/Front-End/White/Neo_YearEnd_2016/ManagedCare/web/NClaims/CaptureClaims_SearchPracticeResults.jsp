<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="100%">
    <thead>
        <tr>
            <th>Practice ID</th>
            <th>Practice Number</th>
            <th>Practice Name</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="item" items="${SearchPracticeResults}">
            <tr>
                <td><label>${item.entityId}</label></td>
                <td><label>${item.practiceNumber}</label></td>
                <td><label>${item.practiceName}</label></td>
                <td><input type="button" value="Select" onclick="selectClaimPractice('${item.practiceNumber}','${item.practiceName}',false);"></td>
            </tr>
        </c:forEach>
    </tbody>
</table>
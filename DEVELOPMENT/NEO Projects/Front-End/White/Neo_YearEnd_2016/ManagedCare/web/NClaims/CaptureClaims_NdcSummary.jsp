<%-- 
    Document   : CaptureClaims_NdcSummary
    Created on : 10 Mar 2016, 9:29:25 AM
    Author     : janf
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>

<nt:NeoPanel title="NDC codes for Claim Line"  collapsed="false" reload="false">
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="100%">
        <thead>
            <tr>
                <th>NDC Code</th>
                <!--<th>NDC Description</th>-->
                <th>Quantity</th>
                <th>Dosage</th>
                <th>NDC Amount</th>
                <th>Primary NDC</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="item" items="${NdcLine}" varStatus="loop">
                <c:set var="nappiDesc" value="nappiDesc${item.ndcCode}"></c:set>
                <c:set var="desc" value="${requestScope[nappiDesc]}"></c:set>
                <tr>
                    <td><label>${item.ndcCode}</label></td>
                    <!--<td><label>${desc} + ${nappiDesc} + ${requestScope[nappiDesc]}</label></td>-->
                    <td><label>${item.quantity}</label></td>
                    <td><label>${item.dosage}</label></td>
                    <td><label>${item.amount}</label></td>
                    <td><label>${item.primaryNdcIndicator}</label></td>
                    <td><label><input type="button" value="Select" onclick="modifyNappiFromList(${loop.index})"</label></td>
                    <td><label><input type="button" value="Delete" onclick="removeNappiFromList(${loop.index})"</label></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</nt:NeoPanel>    

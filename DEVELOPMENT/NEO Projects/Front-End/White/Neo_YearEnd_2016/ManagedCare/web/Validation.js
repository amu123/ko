/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * remove all errors from fields ending with _error
 */
function clearErrors() {
    for (i = 0; i < document.forms[0].elements.length; i++)
    {
        var name = document.forms[0].elements[i].name;
        name += '_error';
        if (document.getElementById(name.toString()) != null) {
            document.getElementById(name.toString()).innerText = '';
        }
    }

}

/**
 * Check mandatory fields
 */
function check_mandatory(form, field, errorField) {

    if (document.getElementById(field).value.length == 0) {
        document.getElementById(errorField).innerText = document.getElementById(errorField).innerText + 'Field is mandatory; ';
        return false;
    } else {
        return true;
    }
}

/**
 * Check if field in numberic
 */
function isNumeric(form, field, errorField) {

    var value = document.getElementById(field).value;
    if (value == null || !value.toString().match(/^[-]?\d*\.?\d*$/) || value == '') {
        document.getElementById(errorField).innerText = document.getElementById(errorField).innerText + 'Field must be numeric; ';
        return false;
    } else {
        return true;
    }
}




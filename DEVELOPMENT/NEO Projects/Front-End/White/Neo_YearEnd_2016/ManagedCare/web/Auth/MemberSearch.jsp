<%-- 
    Document   : MemberSearch
    Created on : 2009/12/10, 08:13:17
    Author     : gerritj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript">

            function hideRows(id) {
                document.getElementById(id).style.display = 'none';
                document.getElementById(id + 'plus').style.display = '';
                document.getElementById(id + 'minus').style.display = 'none';
            }
            function showRows(id) {
                document.getElementById(id).style.display = '';
                document.getElementById(id + 'minus').style.display = '';
                document.getElementById(id + 'plus').style.display = 'none';

            }

            function setProductOptionValues() {
                var optVal = document.createElement('OPTION');
                optVal.value = "99";
                optVal.text = "";
                $("#schemeOption option").add(optVal);
                toggleProduct($("#scheme").val());
            }

            function GetXmlHttpObject() {
                if (window.ActiveXObject) {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function GetDOMParser(xmlStr) {
                var xmlDoc;
                if (window.DOMParser) {
                    parser = new DOMParser();
                    xmlDoc = parser.parseFromString(xmlStr, "text/xml");
                } else {
                    xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async = "false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }

            function toggleProduct(prodId) {
                if (prodId != "99") {
                    addProductOption(prodId);
                }
            }

            function addProductOption(prodId) {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("ERROR: Browser Incompatability");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=GetProductOptionCommand&prodId=" + prodId;
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        $("#schemeOption").empty();
                        $("#schemeOption").append("<option value=\"99\"></option>");
                        var xmlDoc = GetDOMParser(xhr.responseText);
                        //print value from servlet
                        $(xmlDoc).find("ProductOptions").each(function () {
                            //set product details
                            $(this).find("Option").each(function () {
                                var prodOpt = $(this).text();
                                var opt = prodOpt.split("|");
                                //Add option to dependant dropdown
                                var optVal = document.createElement('OPTION');
                                optVal.value = opt[0];
                                optVal.text = opt[1];
                                document.getElementById("schemeOption").options.add(optVal);

                            });
                        });
                    }
                }
                xhr.open("POST", url, true);
                xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                xhr.send(null);

            }
        </script>
    </head>
    <body onload="setProductOptionValues();">
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Member Search</label>
                    <br/><br/>
                    <table>
                        <agiletags:ControllerForm name="searchMember">
                            <tr><agiletags:LabelTextBoxError displayName="Member Number" elementName="memNo"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Identification Number" elementName="idNumber"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Name" elementName="name"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="surname"/></tr>
                            <tr><agiletags:LabelTextBoxDate displayname="Date Of Birth" elementName="dateOfBirth"/></tr>
                            <!--<tr><agiletags:LabelTextBoxError displayName="Scheme" elementName="scheme"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Plan" elementName="plan"/></tr>-->
                            <tr><agiletags:LabelProductDropDown displayName="Scheme" elementName="scheme" javaScript="onblur=\"toggleProduct(this.value);\"" /></tr>
                            <tr><agiletags:LabelDropDown displayName="Scheme Option" elementName="schemeOption" /></tr>
                            <tr><agiletags:ButtonOpperationLabelError type="submit" elementName="searchMem" commandName="SearchMemberCommand" displayName="Search" mandatory="no" valueFromSession="yes" javaScript="" /></tr>
                        </agiletags:ControllerForm>
                    </table>
                    <br/>
                    <HR color="#666666" WIDTH="50%" align="left">
                    <br/>
                    <agiletags:MemberSearchTable commandName="AllocateMemberToSessionCommand" javaScript=""/>
                    <br/>
                    <br/>
                    <b><agiletags:ErrorTag /></b>
                    <!-- content ends here -->
                </td></tr></table>

    </body>
</html>

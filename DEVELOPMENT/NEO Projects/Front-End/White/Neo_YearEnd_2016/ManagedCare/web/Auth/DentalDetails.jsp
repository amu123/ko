<%-- 
    Document   : DentalDetails
    Created on : 2009/12/09, 08:54:58
    Author     : gerritj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script language="JavaScript">

            function toggle(id) {
                if(document.getElementById('hospAdmission').checked){
                    document.getElementById(id).style.display = '';
                }else{
                    document.getElementById(id).style.display = 'none';
                }
            }
            function clearElement(element) {
                part = element + '_text';
                document.getElementById(element).value = '';
                document.getElementById(part).value = '';
            }
            function submitWithAction(action) {
                alert(action);
                alert(document.getElementById('opperation'));
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body onload="toggle('hospInfo');">
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Dental Detail</label>
                    </br></br>
                    <agiletags:ControllerForm name="dentalDetail">
                    <table border="0">
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <input type="hidden" name="searchCalled" id="searchCalled" value="" />
                        <input type="hidden" name="onScreen" id="onScreen" value="" />
                        <tr><agiletags:LabelTextSearchTextCancel onScreen="/Auth/DentalDetails.jsp" valueFromSession="Yes" displayName="Treating Provider" elementName="treatingProv" searchOpperation="ForwardToSearchProviderCommand"/></tr>
                        <tr><agiletags:LabelTextSearchTextCancel onScreen="/Auth/DentalDetails.jsp" valueFromSession="Yes" displayName="Referring Provider" elementName="referringProv" searchOpperation="ForwardToSearchProviderCommand"/></tr>
                        <tr><agiletags:LabelTextSearchTextCancel onScreen="/Auth/DentalDetails.jsp" valueFromSession="Yes" displayName="Lab Provider" elementName="labProv" searchOpperation="ForwardToSearchPracticeCommand"/></tr>
                        <tr><agiletags:LabelTextSearchTextCancel onScreen="/Auth/DentalDetails.jsp" valueFromSession="Yes" displayName="Primary ICD10" elementName="picd10" searchOpperation="ForwardToSearchICD10Command"/></tr>
                        <tr><agiletags:LabelTextSearchDropCancell searchOpperation="ForwardToSearchTariffCommand" onScreen="/Auth/DentalDetails.jsp" displayName="Tariff" elementName="tariff_list"/></tr>
                        <tr><agiletags:LabelTextSearchDropCancell searchOpperation="ForwardToSearchTariffCommand" onScreen="/Auth/DentalDetails.jsp" displayName="Lab Codes" elementName="labCode_list"/></tr>
                        <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Quantity" elementName="quantity"/></tr>
                        <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Tooth Number" elementName="toothNumber"/></tr>
                        <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Amount Claimed" elementName="amountClaimed"/></tr>
                        <tr><agiletags:LabelTextBoxDate valueFromSession="Yes" displayname="Date Valid From" elementName="validFrom"/></tr>
                        <tr><agiletags:LabelTextBoxDate valueFromSession="Yes" displayname="Date Valid To" elementName="validTo"/></tr>
                        <tr><agiletags:LabelTextAreaError valueFromSession="Yes" displayName="Notes" elementName="notes"/></tr>
                        <tr><agiletags:LabelCheckboxSingle displayName="Hospital Authorisation" value="1" elementName="hospAdmission" javascript="onclick=\"toggle('hospInfo');\""/></tr>
                        <!--
                        <tr><td><input type="checkbox" name="hospAdmission" id="hospAdmission" onclick="toggle('hospInfo');"><label class="label">Hospital Authorisation</label></input></td></tr>
                        -->
                        <tr><td colspan="3"><div id="hospInfo" style="display:none">
                            <table>
                            <tr><agiletags:LabelTextSearchTextCancel onScreen="/Auth/DentalDetails.jsp" valueFromSession="Yes" displayName="Facility Provider" elementName="facilityProv" searchOpperation="ForwardToSearchPracticeCommand"/></tr>
                            <tr><agiletags:LabelTextSearchTextCancel onScreen="/Auth/DentalDetails.jsp" valueFromSession="Yes" displayName="Aneasthetist" elementName="aneasthetist" searchOpperation="ForwardToSearchPracticeCommand"/></tr>
                            <tr><agiletags:LabelTextSearchDropCancell searchOpperation="ForwardToSearchICD10Command" onScreen="/Auth/DentalDetails.jsp"  displayName="Secondary ICD10" elementName="sicd10_list"/></tr>
                            <tr><agiletags:LabelTextSearchDropCancell searchOpperation="ForwardToSearchICD10Command" onScreen="/Auth/DentalDetails.jsp" displayName="Co-Morbidity ICD10" elementName="comicd10_list"/></tr>
                            <tr><agiletags:LabelLookupValueDropDown displayName="PMB" lookupId="6" elementName="pmb"/></tr>
                            <tr><agiletags:LabelLookupValueDropDown displayName="Co-Payment" lookupId="6" elementName="coPay"/></tr>
                            <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Theatre Time" elementName="theatreTime"/></tr>
                            <tr><agiletags:LabelLookupValueDropDown displayName="Authorisation Status" lookupId="9" elementName="status"/></tr>
                            <tr><td>&nbsp;</td><tr>
                            <tr><td colspan="3"><label class="subheader">Funder Details</label></td></tr>
                            <tr><td><agiletags:LabelLookupRadioButtonGroup lookupId="7" elementName="funderDetails"/></td></tr>
                            </table>
                        </div></td></tr>
                        <tr><agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Reset" javaScript="onClick=\"submitWithAction('ReloadDentalDetailsCommand')\";"/>
                            <agiletags:ButtonOpperation type="button" align="left" commandName="" displayname="Save" span="1" javaScript="onClick=\"submitWithAction('SaveAuthDetailsCommand')\";"/></tr>
                    </table>
                    </agiletags:ControllerForm>
                    <!-- content ends here -->
        </td></tr></table>
    </body>
</html>

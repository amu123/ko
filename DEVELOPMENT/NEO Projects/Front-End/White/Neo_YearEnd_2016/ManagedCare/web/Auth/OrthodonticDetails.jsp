<%-- 
    Document   : OrthodonticDetails
    Created on : 2009/12/17, 09:21:10
    Author     : whauger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript">

            function clearElement(element) {
                part = element + '_text';
                document.getElementById(element).value = '';
                document.getElementById(part).value = '';
            }
            function submitWithAction(action) {
                alert(action);
                prompt("action", action);
                alert(document.getElementById('opperation'));
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Orthodontic Detail</label>
                    <br></br><br></br>
                    <agiletags:ControllerForm name="orthodonticDetail">
                    <table>
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <input type="hidden" name="searchCalled" id="searchCalled" value="" />
                        <input type="hidden" name="onScreen" id="onScreen" value="" />
                        <tr><agiletags:LabelTextSearchTextCancel onScreen="/Auth/OrthodonticDetails.jsp" valueFromSession="Yes" displayName="Treating Provider" elementName="treatingProv" searchOpperation="ForwardToSearchProviderCommand"/></tr>
                        <tr><agiletags:LabelTextSearchTextCancel onScreen="/Auth/OrthodonticDetails.jsp" valueFromSession="Yes" displayName="Referring Provider" elementName="referringProv" searchOpperation="ForwardToSearchProviderCommand"/></tr>
                        <tr><agiletags:LabelTextSearchTextCancel onScreen="/Auth/OrthodonticDetails.jsp" valueFromSession="Yes" displayName="Primary ICD10" elementName="picd10" searchOpperation="ForwardToSearchICD10Command"/></tr>
                        <tr><agiletags:LabelTextSearchTextCancel onScreen="/Auth/OrthodonticDetails.jsp" valueFromSession="Yes" displayName="Tariff" elementName="tariff" searchOpperation="ForwardToSearchOrthoPlanCommand"/></tr>
                        <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Plan Duration" elementName="numMonths"/></tr>
                        <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Total Amount Claimed" elementName="amountClaimed"/></tr>
                        <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Deposit Amount" elementName="depositAmount"/></tr>
                        <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="First Installment Amount" elementName="firstAmount"/></tr>
                        <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Remaining Installment Amount" elementName="remainAmount"/></tr>
                        <tr><agiletags:LabelTextBoxDate valueFromSession="Yes" displayname="Date Valid From" elementName="authFromDate"/></tr>
                        <tr><agiletags:LabelTextBoxDate valueFromSession="Yes" displayname="Date Valid To" elementName="authToDate"/></tr>
                        <tr><agiletags:LabelTextAreaError valueFromSession="Yes" displayName="Notes" elementName="notes"/></tr>
                        <tr><agiletags:LabelLookupValueDropDown displayName="Authorisation Status" lookupId="9" elementName="status"/></tr>
                        <tr><agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Reset" javaScript="onClick=\"submitWithAction('ReloadOrthodonticDetailsCommand')\";"/>
                            <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Save" javaScript="onClick=\"submitWithAction('SaveOrthoAuthDetailsCommand')\";"/></tr>
                            </table>
                            </agiletags:ControllerForm>
                        <p>The settlements of all claims are subject to the validity of membership of the patient to the scheme, as well as limits available.
If tariff codes are specified, the member may be liable for any other tariff codes that may be used.
If an amount is specified the member may be liable for any amount exceeding the specified amount.
Any services rendered outside of the effective period have to be separately motivated for.
All benefits, whether approved or not, will at all times be subject to the rules of the scheme.
All treatments and procedures are funded according to the National Health Reference Price List as was provided by the Council of Medical Schemes
</p>


                    <!-- content ends here -->
        </td></tr></table>
    </body>
</html>

<%-- 
    Document   : OpticalDetails
    Created on : 2009/12/09, 08:55:08
    Author     : gerritj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript">
            
            function clearElement(element) {
                part = element + '_text';
                document.getElementById(element).value = '';
                document.getElementById(part).value = '';
            }
            function submitWithAction(action) {
                 alert(action);
                 alert(document.getElementById('opperation'));
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Optical Detail</label>
                    </br></br>
                    <agiletags:ControllerForm name="opticalDetail">
                    <table>
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <input type="hidden" name="searchCalled" id="searchCalled" value="" />
                        <input type="hidden" name="onScreen" id="onScreen" value="" />
                        <tr><agiletags:LabelTextSearchTextCancel onScreen="/Auth/OpticalDetails.jsp" valueFromSession="Yes" displayName="Facility provider" elementName="facilityProv" searchOpperation="ForwardToSearchPracticeCommand"/></tr>
                        <tr><agiletags:LabelTextSearchTextCancel onScreen="/Auth/OpticalDetails.jsp" valueFromSession="Yes" displayName="Treating provider" elementName="treatingProv" searchOpperation="ForwardToSearchProviderCommand"/></tr>
                        <tr><agiletags:LabelTextSearchTextCancel onScreen="/Auth/OpticalDetails.jsp" valueFromSession="Yes" displayName="Referring provider" elementName="referringProv" searchOpperation="ForwardToSearchProviderCommand"/></tr>
                        <tr><agiletags:LabelTextSearchTextCancel onScreen="/Auth/OpticalDetails.jsp" valueFromSession="Yes" displayName="Primary ICD10" elementName="picd10" searchOpperation="ForwardToSearchICD10Command"/></tr>
                        <tr><agiletags:LabelTextSearchDropCancell searchOpperation="ForwardToSearchICD10Command" onScreen="/Auth/OpticalDetails.jsp"  displayName="Secondary ICD10" elementName="sicd10_list"/></tr>
                        <tr><agiletags:LabelTextSearchDropCancell searchOpperation="ForwardToSearchICD10Command" onScreen="/Auth/OpticalDetails.jsp" displayName="Co-Morbidity ICD10" elementName="comicd10_list"/></tr>
                        <tr><agiletags:LabelTextSearchDropCancell searchOpperation="ForwardToSearchTariffCommand" onScreen="/Auth/OpticalDetails.jsp" displayName="Tariff" elementName="tariff_list"/></tr>
                        <tr><agiletags:LabelTextSearchDropCancell searchOpperation="ForwardToSearchProcedureCommand" onScreen="/Auth/OpticalDetails.jsp" displayName="Procedure" elementName="procedure_list"/></tr>
                        <tr><agiletags:LabelTextBoxDate valueFromSession="Yes" displayname="Authorisation from date" elementName="authFromDate"/></tr>
                        <tr><agiletags:LabelTextBoxError displayName="Number of days" elementName="numDays"/></tr>
                        <tr><agiletags:LabelTextBoxError displayName="Previous lens RX" elementName="prevLens"/></tr>
                        <tr><agiletags:LabelTextBoxError displayName="Current lens RX" elementName="curLens"/></tr>
                        <tr><agiletags:LabelTextAreaError displayName="Notes" elementName="notes"/></tr>
                        <tr><agiletags:LabelLookupValueDropDown displayName="Authorisation Status" lookupId="9" elementName="status"/></tr>
                        <tr><agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Reset" javaScript="onClick=\"submitWithAction('ReloadOpticalDetailsCommand')\";"/>
                            <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Next" javaScript="onClick=\"submitWithAction('SaveCallCenterOpticalDetailCommand')\";"/></tr>
                            </table>
                            </agiletags:ControllerForm>
                        <p>The settlements of all claims are subject to the validity of membership of the patient to the scheme, as well as limits available.
If tariff codes are specified, the member may be liable for any other tariff codes that may be used.
If an amount is specified the member may be liable for any amount exceeding the specified amount.
Any services rendered outside of the effective period have to be separately motivated for.
All benefits, whether approved or not, will at all times be subject to the rules of the scheme.
All treatments and procedures are funded according to the National Health Reference Price List as was provided by the Council of Medical Schemes
</p>
                    

                    <!-- content ends here -->
        </td></tr></table>
    </body>
</html>

<%-- 
    Document   : LogNewCall
    Created on : 2010/03/22, 02:26:26
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ page import="java.util.Date" %>
<%@ page import="com.koh.calltrack.command.GenerateCallCoverCommand" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="neo.manager.NeoUser" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.tools.min.js"></script>
        <script language="JavaScript">

            $(document).ready(function () {
                hideSubCategory();
                $("#missing-fields").hide();

                $("#doneButon").click(function () {
                    if ($("#callQ_0").is(":checked")) {
                        // $("#disciplineCategory").show();
                        // $("#benefitSub").show();
                        // $("#claimSub").hide();
                        // $("#membershipSub").hide();
                        // $("#UnderwritingSub").hide();
                        //The logic for the new list
                        $("#claimsFC").show();
                        $("#claimsSC").show();
                        $("#claimsTC").show();
                    }
                    if ($("#callQ_1").is(":checked")) {
                        // $("#disciplineCategory").show();
                        //  $("#claimSub").show();
                        // $("#benefitSub").hide();
                        // $("#membershipSub").hide();
                        //  $("#UnderwritingSub").hide();
                        //The logic for the new list
                        $("#statementsFC").show();
                        $("#statementsTC").show();

                    }
                    if ($("#callQ_2").is(":checked")) {
                        // $("#membershipSub").show();
                        // $("#UnderwritingSub").hide();
                        //  $("#benefitSub").hide();
                        //  $("#claimSub").hide();
                        //  $("#disciplineCategory").hide();
                        // The logic for the new list
                        var product = document.getElementById('product').value;
                        if (product === '1') {
                            $("#benefitsFCReso").show();
                        }
                        else {
                            $("#benefitsFCSpec").show();
                        }
                        $("#benefitsSC").show();
                        $("#benefitsTC").show();
                    }
                    if ($("#callQ_3").is(":checked")) {
                        //$("#UnderwritingSub").show();
                        // $("#membershipSub").hide();
                        //  $("#benefitSub").hide();
                        //  $("#claimSub").hide();
                        //  $("#disciplineCategory").hide();
                        // The logic for the new list
                        $("#prenmiumsFC").show();
                        $("#premiumsTC").show();

                    }
                    if ($("#callQ_4").is(":checked")) {
                        $("#membershipFC").show();
                        $("#memberShipTC").show();

                    }
                    if ($("#callQ_5").is(":checked")) {
                        $("#serviceProviderFC").show();
                        $("#serviceProviderTC").show();

                    }
//                    if ($("#callQ_6").is(":checked")) {
//                        $("#foundationFC").show();
//                        $("#foundationSC").show();
//                    }
                    if ($("#callQ_6").is(":checked")) {
                        $("#chronicFC").show();
                        $("#chronicSC").show();
                    }
                    if ($("#callQ_7").is(":checked")) {
                        $("#oncologyFC").show();
                        $("#oncologySC").show();
                    }
                    if ($("#callQ_8").is(":checked")) {
                        $("#wellcareFC").show();
                        $("#wellcareSC").show();
                    }
                    $("#queryCategories").hide();
                    $("#doneButon").hide();
                });

                checkMandatoryFields();

            });


            function loadNewCallPage() {
                //toggle first
                toggleCallType();
                // toggleReferred();
                setCallDate();


            <%
                Date today = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
                String date = sdf.format(today);
                String trackDate = sdf2.format(today);
                String ctNumber = "CT" + trackDate + "N#";
                NeoUser user = (NeoUser) session.getAttribute("persist_user");
                session.setAttribute("uName", user.getUsername());
                session.setAttribute("disDate", date);
                // session.setAttribute("callTrackNumb    er", ctNumber);
            %>
                //alert(document.getElementById('searchCalled').value);
                if (document.getElementById('searchCalled').value == 'memberNumber') {
                    //alert(document.getElementById('memberNumber_text').value);
                    getPrincipalMemberForNumber(document.getElementById('memberNumber_text').value, 'memberNumber');
                }
                if (document.getElementById('searchCalled').value == 'providerNumber') {
                    //alert(document.getElementById('providerNumber_text').value);
                    validateProvider(document.getElementById('providerNumber_text').value, 'providerNumber');
                }
            }


            function toggleCallType() {
                var value = document.getElementById('callType').value;
                if (value == '2') {
                    var state = document.getElementById('providerType').style.display;
                    document.getElementById('providerType').style.display = '';
                    document.getElementById('popCover').style.display = '';
                    document.getElementById('popCoverSearch').style.display = 'none';
                    document.getElementById('memberGrid').style.display = 'none';
                    document.getElementById('popProvider').style.display = 'none';
                    document.getElementById('popProviderSearch').style.display = '';
                } else {
                    document.getElementById('providerType').style.display = 'none';
                    document.getElementById('popCover').style.display = 'none';
                    document.getElementById('popCoverSearch').style.display = '';
                    document.getElementById('memberGrid').style.display = '';
                    document.getElementById('popProvider').style.display = '';
                    document.getElementById('popProviderSearch').style.display = 'none';
                }
            }

            function submitWithAction(action) {
                alert('submitWithAction(action)');
                checkMandatoryFields();
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }


            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }


            function hideRows(id) {
                document.getElementById(id).style.display = 'none';
                document.getElementById(id + 'plus').style.display = '';
                document.getElementById(id + 'minus').style.display = 'none';
            }


            function showRows(id) {
                document.getElementById(id).style.display = '';
                document.getElementById(id + 'minus').style.display = '';
                document.getElementById(id + 'plus').style.display = 'none';

            }



            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function getPrincipalMemberForNumber(str, element) {
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=GetMemberByNumberCommand&number=" + str + "&element=" + element;
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element);
                };
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function validateMember(str, element) {
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=MemberDetailsCoverSearchCommand&memberNumber=" + str + "&exactCoverNum=1";
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element)
                };
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function validateProvider(str, element) {
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=FindPracticeByCodeCommand&providerNumber=" + str;
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element)
                };
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function stateChanged(element) {
                if (xmlhttp.readyState == 4)
                {
                    // alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    // alert(result);
                    if (result == "FindPracticeByCodeCommand") {
                        var name = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=') + 1, xmlhttp.responseText.lastIndexOf('|'));
                        var discipline = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=') + 1, xmlhttp.responseText.lastIndexOf('$'));
                        document.getElementById('discipline').value = discipline;
                        document.getElementById('provName').value = name;
                        document.getElementById(element + '_error').innerHTML = '';
                        // document.getElementById('searchCalled').value = "";
                    } else if (result == "GetMemberByNumberCommand") {

                        var resultArr = xmlhttp.responseText.split("|");
                        var numberArr = resultArr[1].split("=");
                        var emailArr = resultArr[2].split("=");
                        var productArr = resultArr[3].split("=");
                        var entityArr = resultArr[4].split("=");

                        var number = numberArr[1];
                        var productId = productArr[1];
                        var email = emailArr[1];
                        var entityID = entityArr[1];

                        document.getElementById(element + '_error').innerHTML = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('productId').value = productId;
                        document.getElementById('entityID_text').value = entityID;

                        if (email != null && email != "") {
                            document.getElementById('callerContact').value = email;
                        } else if (number != null && number != "") {
                            document.getElementById('callerContact').value = number;
                        }

                        //document.getElementById('searchCalled').value = "";
            <%
                session.setAttribute("exactCoverNum", "1");
            %>
                        submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                    } else if (result == "Error") {
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|') + 1, xmlhttp.responseText.length);
                        if (forCommand == "FindPracticeByCodeCommand") {
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById('provName').value = '';
                            document.getElementById('discipline').value = '';
                            // document.getElementById('searchCalled').value = "";
                        } else if (forCommand == "GetMemberByNumberCommand") {
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element + '_text').value = '';
                            //document.getElementById('searchCalled').value = "";
                            //submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                        }
                    }
                }
            }


            function hideSubCategory() {
                $("#disciplineCategory").hide();
                $("#benefitSub").hide();
                $("#claimSub").hide();
                $("#membershipSub").hide();
                $("#UnderwritingSub").hide();
                $("#queryCategories").show();

                // new categories
                $("#claimsFC").hide();
                $("#claimsSC").hide();
                $("#claimsTC").hide();
                $("#statementsFC").hide();
                $("#statementsTC").hide();
                $("#benefitsFCReso").hide();
                $("#benefitsFCSpec").hide();
                $("#benefitsSC").hide();
                $("#benefitsTC").hide();
                $("#prenmiumsFC").hide();
                $("#premiumsTC").hide();
                $("#membershipFC").hide();
                $("#memberShipTC").hide();
                $("#serviceProviderFC").hide();
                $("#serviceProviderTC").hide();
                $("#chronicFC").hide();
                $("#chronicSC").hide();
                $("#oncologyFC").hide();
                $("#oncologySC").hide();
                $("#wellcareFC").hide();
                $("#wellcareSC").hide();
                $("#foundationFC").hide();
                $("#foundationSC").hide();

            }
            function checkMandatoryFields() {
            <%  if ((session.getAttribute("mandatoryfailed")) != null && (!session.getAttribute("mandatoryfailed").toString().equalsIgnoreCase("null")) && (!session.getAttribute("mandatoryfailed").toString().equalsIgnoreCase(""))) {

            %>

                $("#missing-fields").dialog({modal: true, width: 550,
                    buttons: {
                        'Done': function() {
                            $(this).dialog('close');
                        }
                    }
                });

            <%
                    session.setAttribute("mandatoryfailed", null);

                }%>

            }

            function setCallDate() {

                var date = new Date();

                var year = date.getFullYear();
                var month = (date.getMonth() + 1);
                var day = date.getDate();

                if (month < 10) {
                    month = "0" + month;
                }

                if (day < 10) {
                    day = "0" + day;
                }

                var dateStr = "CT" + year + "" + month + "" + day + "N#";
                $("#callTrackNumber").val(dateStr);
                //document.getElementById('callTrackId').value = dateStr;
                $("#callTrackId").val(dateStr);
                //var authFromDate = $("#authFromDate").val(dateStr);

            }


        </script>
    </head>

    <body onload="loadNewCallPage(); if(parent.document.getElementById('canRefresh').value === '1'){parent.document.getElementById('canRefresh').value = '0'; submitWithAction('ReloadCallLogCommand');}">
        <div id="missing-fields" title="Missing Fields"><p>You must select at least one of the Query Categories</p></div>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Log New Call</label>
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="saveCallInfo" validate="yes">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="productId" id="productId" value="" />
                            <input type="hidden" name="product" id="product" value="${product}" />
                            <input type="hidden" name="entityID_text" id="entityID_text" value="" />

                            <agiletags:HiddenField elementName="searchCalled"/>
                            <!-- <input type="hidden" name="searchCalled" id="searchCalled" value="" /> -->

                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <table>

                                <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Call Type" elementName="callType" lookupId="108" javaScript="onchange=\"toggleCallType();\"" errorValueFromSession="yes" mandatory="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Call Number" elementName="callTrackNumber" valueFromSession="yes" readonly="yes"/></tr>
                                <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Call Status" elementName="callStatus" lookupId="105" javaScript="" errorValueFromSession="yes" mandatory="yes" /></tr>
                                <!-- <tr align="left" id="referedUsers" style="display:none">
                                <agiletags:LabelNeoUserDropDown displayName="Referred Users" elementName="refUser"/>
                            </tr>-->
                                <!-- -->
                                <tr id="popCoverSearch" style="display:none" align="left">
                                    <agiletags:LabelTextSearchText valueFromSession="yes" displayName="Member Number" elementName="memberNumber" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/Calltrack/LogNewCall.jsp" mandatory="yes" javaScript="onChange=\"getPrincipalMemberForNumber(this.value, 'memberNumber');\""/>
                                </tr>
                                <tr id="popProvider" style="display:none" align="left">
                                    <agiletags:LabelTextBoxError valueFromSession="yes" displayName="Provider Number" elementName="provNum" javaScript="onChange=\"validateProvider(this.value, 'provNum');\""/>
                                </tr>

                                <tr id="popProviderSearch" style="display:none" align="left">
                                    <agiletags:LabelTextSearchText valueFromSession="yes" displayName="Provider Number" elementName="providerNumber" searchFunction="yes" searchOperation="ForwardToSearchPracticeCommand" onScreen="/Calltrack/LogNewCall.jsp" mandatory="yes" javaScript="onChange=\"validateProvider(this.value, 'providerNumber');\""/>
                                </tr>
                                <tr id="popCover" style="display:none" align="left">
                                    <agiletags:LabelTextBoxError valueFromSession="yes" displayName="Member Number" elementName="memberNum" javaScript="onChange=\"getPrincipalMemberForNumber(this.value, 'memberNum');\""/>
                                </tr>

                                <tr id="providerType" style="display:none">
                                    <agiletags:LabelTextBoxError displayName="Provider Name" elementName="provName"  valueFromSession="yes"/>
                                    <agiletags:LabelTextBoxError displayName="Discipline" elementName="discipline" valueFromSession="yes" readonly="yes"/>
                                </tr>

                                <tr><agiletags:LabelTextBoxError displayName="Caller Name" elementName="callerName" mandatory="yes" valueFromSession="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Contact Details" elementName="callerContact" mandatory="yes" valueFromSession="yes"/></tr>
                                <agiletags:LabelNeoLookupValueCheckbox displayName="Contact Type" elementName="queryMethod" lookupId="150" javascript="" />

                                <tr><td colspan="6"><div id="memberGrid" style="display:none">
                                            <agiletags:MemberCoverSearchTag commandName="" javaScript="" />
                                        </div></td></tr>
                                <div>
                                <tr id="queryCategories"><agiletags:LabelNeoLookupValueCheckbox displayName="Query Categories" elementName="callQ" lookupId="151" javascript="" /></tr>
                                    <!-- <input type="hidden" name="callsQuery" id="callsQuery" value="" >-->
                                </div>
                                <div>
                                                                    <tr id="doneButon"><td colspan="5" align="left">
                                                                            <button name="opperation" type="button" value="Done">Done</button>
                                                                        </td></tr>
                                </div>

                <!-- <tr><agiletags:ButtonOpperation commandName="" align="left" displayname="Done" span="1" type="button"  javaScript="onClick=\"submitWithAction('SaveCallTrackDetailsCommand')\";"/></tr> -->
                                <!--<div>
                                    <tr id="disciplineCategory"><agiletags:LabelNeoLookupValueCheckbox displayName="Discipline" elementName="disciplineCategory" lookupId="143" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="benefitSub"><agiletags:LabelNeoLookupValueCheckbox displayName="Benefit Sub Category" elementName="benefitSub" lookupId="144" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="claimSub"><agiletags:LabelNeoLookupValueCheckbox displayName="Claim Sub Category" elementName="claimSub" lookupId="145" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="membershipSub"><agiletags:LabelNeoLookupValueCheckbox displayName="Membership Sub Category" elementName="membershipSub" lookupId="146" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="UnderwritingSub"><agiletags:LabelNeoLookupValueCheckbox displayName="Underwriting Sub Category" elementName="underwritingSub" lookupId="147" javascript=""/></tr>
                                </div> -->
                                <!-- This is the part of the new lookups -->
                                <!-- Claims -->
                                <div>
                                    <tr class="claimsLeve11" id="claimsFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Claims First Category" elementName="claimsFC" lookupId="152" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr class="claimsLevel2" id="claimsSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Claims Second Category" elementName="claimsSC" lookupId="153" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr class="claimsLeve3" id="claimsTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Claims Third Category" elementName="claimsTC" lookupId="154" javascript=""/></tr>
                                </div>

                                <!-- Statement -->
                                <div>
                                    <tr id="statementsFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Statements First Category" elementName="statementsFC" lookupId="155" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="statementsTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Statements Second Category" elementName="statementsTC" lookupId="156" javascript=""/></tr>
                                </div>

                                <!-- Benefits -->
                                <div>
                                    <tr id="benefitsFCReso"><agiletags:LabelNeoLookupValueCheckbox displayName="Benefits First Category" elementName="benefitsFCReso" lookupId="157" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="benefitsFCSpec"><agiletags:LabelNeoLookupValueCheckbox displayName="Benefits First Category" elementName="benefitsFCSpec" lookupId="284" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="benefitsSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Benefits Second Category" elementName="benefitsSC" lookupId="158" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="benefitsTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Benefits Third Category" elementName="benefitsTC" lookupId="159" javascript=""/></tr>
                                </div>

                                <!-- Premiums -->
                                <div>
                                    <tr id="prenmiumsFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Premiums First Category" elementName="prenmiumsFC" lookupId="160" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="premiumsTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Premiums Second Category" elementName="premiumsTC" lookupId="161" javascript=""/></tr>
                                </div>

                                <!-- Membership -->
                                <div>
                                    <tr id="membershipFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Membership First Category" elementName="membershipFC" lookupId="162" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="memberShipTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Membership Second Category" elementName="memberShipTC" lookupId="163" javascript=""/></tr>
                                </div>

                                <!-- Service Provider -->
                                <div>
                                    <tr id="serviceProviderFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Service Provider First Category" elementName="serviceProviderFC" lookupId="164" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="serviceProviderTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Service Provider Second Category" elementName="serviceProviderTC" lookupId="165" javascript=""/></tr>
                                </div>
                               
                                <!-- Chronic -->
                                <div>
                                    <tr id="chronicFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Chronic First Category" elementName="chronicFC" lookupId="357" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="chronicSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Chronic Second Category" elementName="chronicSC" lookupId="358" javascript=""/></tr>
                                </div>
                                
                                <!-- Oncology -->
                                <div>
                                    <tr id="oncologyFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Oncology First Category" elementName="oncologyFC" lookupId="359" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="oncologySC"><agiletags:LabelNeoLookupValueCheckbox displayName="Oncology Second Category" elementName="oncologySC" lookupId="360" javascript=""/></tr>
                                </div>
                                
                                <!-- Wellcare -->
                                <div>
                                    <tr id="wellcareFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Wellcare First Category" elementName="wellcareFC" lookupId="361" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="wellcareSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Wellcare Second Category" elementName="wellcareSC" lookupId="362" javascript=""/></tr>
                                </div>

                                <!-- Foundation -->
                                <div>
                                    <tr id="foundationFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Foundation First Category" elementName="foundationFC" lookupId="166" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="foundationSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Foundation Second Category" elementName="foundationSC" lookupId="167" javascript=""/></tr>
                                </div>
                            </table>
                            <table>
                                <!-- End of the new lookups -->
                                <tr><agiletags:LabelTextAreaErrorBig displayName="Notes" elementName="notes" valueFromSession="Yes" /></tr> 
                                <tr><agiletags:LabelTextBoxError displayName="Requested By" elementName="disDate" valueFromSession="Yes" enabled="no"/></tr>

                                <tr>
                                    <agiletags:ButtonOpperation commandName="SaveCallTrackDetailsCommand" align="left" displayname="Submit" span="1" type="button"  javaScript="onClick=\"submitWithAction('SaveCallTrackDetailsCommand'),checkMandatoryFields()\";"/>
                                    <agiletags:ButtonOpperation commandName="ReloadCallLogCommand" align="left" displayname="Cancel" span="1" type="button"  javaScript="onClick=\"submitWithAction('ReloadCallLogCommand')\";"/>
                                </tr>
                            </table>
                            <table  align="right">
                                <tr>
                                    <agiletags:LabelTextBoxError displayName="User Name" elementName="uName" valueFromSession="Yes" enabled="no"/>
                                </tr>
                                <tr>
                                    <agiletags:LabelTextBoxError displayName="Date" elementName="disDate" valueFromSession="Yes" enabled="no"/>
                                </tr>
                            </table>
                        </agiletags:ControllerForm>
                    </table>

                    <!-- content ends here -->
                </td></tr></table>
    </body>
</html>

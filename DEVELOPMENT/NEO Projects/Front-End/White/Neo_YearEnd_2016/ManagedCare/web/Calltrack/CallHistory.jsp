<%-- 
    Document   : CallHistory
    Created on : 2010/03/22, 02:26:38
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript">
            function hideRows(id) {
                document.getElementById(id).style.display = 'none';
                document.getElementById(id + 'plus').style.display = '';
                document.getElementById(id + 'minus').style.display = 'none';
            }
            function showRows(id) {
                document.getElementById(id).style.display = '';
                document.getElementById(id + 'minus').style.display = '';
                document.getElementById(id + 'plus').style.display = 'none';

            }

            function submitWithAction01(action) {
                var dateFrom = $("#dateFrom").val();
                var dateTo = $("#dateTo").val();

                clearErrors();
                var error1 = "";
                var error2 = "";

                if (dateFrom != null && dateFrom != "") {
                    error1 = "notnull";
                }

                if (dateTo != null && dateTo != "") {
                    error2 = "notnull";
                }
                if (error1 != "" && error2 != "") {
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                } else if (error1 != "") {
                    $("#dateFrom_error").text("");
                    $("#dateTo_error").text("Please enter the date");
                } else if (error2 != "") {
                    $("#dateFrom_error").text("Please enter the date");
                    $("#dateTo_error").text("");
                } else {
                    $("#dateFrom_error").text("Please enter the date");
                    $("#dateTo_error").text("Please enter the date");
                }
            }

            function clearErrors() {
                $("#dateFrom_error").text("");
                $("#dateTo_error").text("");
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('trackId').value = index;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>

        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Call History Search</label>
                    </br>
                    </br>
                    <table>
                        <agiletags:ControllerForm name="searchCallHistoryForm">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="trackId" id="trackId" value="" />

                            <tr><agiletags:LabelTextBoxError displayName="Call Reference No" elementName="refNo" /></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Provider Number" elementName="provNum"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Member Number" elementName="memNum"/></tr>
                            <tr><agiletags:LabelTextBoxDateReq displayname="Date From" elementName="dateFrom" valueFromSession="yes"/><tr>
                            <tr><agiletags:LabelTextBoxDateReq displayname="Date To" elementName="dateTo" valueFromSession="yes"/></tr>

                            <tr><agiletags:ButtonOpperation commandName="SeachCallTrackHistoryCommand" align="left" displayname="Search" span="1" type="button" javaScript="onClick=\"submitWithAction('SeachCallTrackHistoryCommand')\";" /></tr>
                        </agiletags:ControllerForm>
                    </table>
                    </br>
                    <HR color="#666666" WIDTH="50%" align="left">
                    </br>
                    <agiletags:CallHistoryResultTag commandName="AllocateCallHistoryToSessionCommand" />

                    <!-- content ends here -->
                </td></tr></table>
    </body>
</html>

<%--
    Document   : ProviderCallTrackingHistory
    Created on : 2010/04/15, 03:21:28
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="neo.manager.NeoUser" %>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>

            <%
                Date today = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
                String date = sdf.format(today);
                String trackDate = sdf2.format(today);
                //  String ctNumber = "CT" + trackDate + "N#";
                NeoUser user = (NeoUser) session.getAttribute("persist_user");
                session.setAttribute("uName", user.getName());
                session.setAttribute("disDate", date);
                // session.setAttribute("callTrackNumber", ctNumber);
%>
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->

                    <label class="header">Provider Call Tracking History</label>
                    <table>
                        <agiletags:ControllerForm name="ProviderCallHistory">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:CallWorkbenchResultTableTag commandName="AllocateCallHistoryToSessionCommand" />

                            <tr>
                                <td colspan="2">&nbsp;</td><agiletags:LabelTextBoxError displayName="User Name" elementName="uName" valueFromSession="Yes" enabled="yes" />
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td><agiletags:LabelTextBoxError displayName="Date" elementName="disDate" valueFromSession="Yes" enabled="yes"/>
                            </tr>
                        </table>
                <tr>
                    <agiletags:ButtonOpperation commandName="ReloadCallLogCommand" align="left" displayname="NewCall" span="1" type="button"  javaScript="onClick=\"submitWithAction('ReloadCallLogCommand')\";"/>
                    <agiletags:ButtonOpperation commandName="ReloadCallLogCommand" align="left" displayname="Cancel" span="1" type="button"  javaScript="onClick=\"submitWithAction('ReloadCallLogCommand')\";"/>
                </tr>

            </agiletags:ControllerForm>
        </table>

        <!-- content ends here -->
    </td></tr></table>
</body>
</html>


<%-- 
    Document   : WorkflowUpdateCallTracking
    Created on : 2010/03/22, 02:26:26
    Author     : gerritr
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>

        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.tools.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/Workflow/WorkflowTabUtil.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Workflow/Workflow.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script language="JavaScript">

            $(document).ready(function () {
                hideSubCategory();
                $("#missing-fields").hide();

                $("#doneButon").click(function () {
                    if ($("#callQ_0").is(":checked")) {
                        //The logic for the new list
                        $("#claimsFC").show();
                        $("#claimsSC").show();
                        $("#claimsTC").show();
                    }
                    if ($("#callQ_1").is(":checked")) {
                        //The logic for the new list
//                        $("#backOfficeFC").show(); Call Centre Admin
//                        $("#backOfficeSC").show();

                    }
                    if ($("#callQ_2").is(":checked")) {
                        //The logic for the new list
                        $("#groupsFC").show();
//                        $("#groupSC").show();

                    }
                    if ($("#callQ_3").is(":checked")) {
                        //The logic for the new list
                        $("#brokerFC").show();
//                        $("#brokerSC").show();

                    }
                    if ($("#callQ_4").is(":checked")) {
                        //The logic for the new list
                        $("#wellnessFC").show();

                    }
                    if ($("#callQ_5").is(":checked")) {
                        //The logic for the new list
                        $("#statementsFC").show();
                        $("#statementsTC").show();

                    }
                    if ($("#callQ_6").is(":checked")) {
                        // The logic for the new list
                        var product = document.getElementById('product').value;
                        if (product === '1') {
                            $("#benefitsFCReso").show();
                        } else {
                            $("#benefitsFCSpec").show();
                        }
                        $("#benefitsSC").show();
                        $("#benefitsTC").show();
                    }
                    if ($("#callQ_7").is(":checked")) {
                        // The logic for the new list
                        $("#prenmiumsFC").show();
                        $("#premiumsTC").show();

                    }
                    if ($("#callQ_8").is(":checked")) {
                        $("#membershipFC").show();
                        $("#memberShipTC").show();

                    }
                    if ($("#callQ_9").is(":checked")) {
                        $("#serviceProviderFC").show();
                        $("#serviceProviderTC").show();

                    }
                    if ($("#callQ_10").is(":checked")) {
                        $("#chronicFC").show();
                        $("#chronicSC").show();
                    }
                    if ($("#callQ_11").is(":checked")) {
                        $("#oncologyFC").show();
                        $("#oncologySC").show();
                    }
                    if ($("#callQ_12").is(":checked")) {
                        $("#wellcareFC").show();
                        $("#wellcareSC").show();
                    }
                    $("#queryCategories").hide();
                    $("#doneButon").hide();
                });

                checkMandatoryFields();

            });


            function loadNewCallPage() {
                //toggle first
                toggleCallType();
                // toggleReferred();
                setCallDate();


                //alert(document.getElementById('searchCalled').value);
                if (document.getElementById('searchCalled').value == 'memberNumber') {
                    //alert(document.getElementById('memberNumber_text').value);
                    getPrincipalMemberForNumber(document.getElementById('memberNumber_text').value, 'memberNumber');
                }
                if (document.getElementById('searchCalled').value == 'providerNumber') {
                    //alert(document.getElementById('providerNumber_text').value);
                    validateProvider(document.getElementById('providerNumber_text').value, 'providerNumber');
                }
            }

            function populateRequests() {
                document.getElementById('callType').value = '${callType}';
                document.getElementById('callStatus').value = '${callStatus}';
                document.getElementById('notes').value = '${notes}';
            }

            function toggleCallType() {
                var value = document.getElementById('callType').value;
                if (value == '2') {
                    var state = document.getElementById('providerType').style.display;
                    document.getElementById('providerType').style.display = '';
                    document.getElementById('popCover').style.display = '';
                    document.getElementById('popCoverSearch').style.display = 'none';
                    document.getElementById('memberGrid').style.display = 'none';
                    document.getElementById('popProvider').style.display = 'none';
                    document.getElementById('popProviderSearch').style.display = '';
                } else {
                    document.getElementById('providerType').style.display = 'none';
                    document.getElementById('popCover').style.display = 'none';
                    document.getElementById('popCoverSearch').style.display = '';
                    document.getElementById('memberGrid').style.display = '';
                    document.getElementById('popProvider').style.display = '';
                    document.getElementById('popProviderSearch').style.display = 'none';
                }
            }

            function submitWithAction(action) {
                checkMandatoryFields();
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }


            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }


            function hideRows(id) {
                document.getElementById(id).style.display = 'none';
                document.getElementById(id + 'plus').style.display = '';
                document.getElementById(id + 'minus').style.display = 'none';
            }


            function showRows(id) {
                document.getElementById(id).style.display = '';
                document.getElementById(id + 'minus').style.display = '';
                document.getElementById(id + 'plus').style.display = 'none';

            }



            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function getPrincipalMemberForNumber(str, element, form) {
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                document.getElementById('opperation').value = 'GetMemberByNumberCommand';
                encodeVal = encodeFormForSubmit(form);
                var url = "/ManagedCare/AgileController?";
                url += encodeVal + "&number=" + str + "&element=" + element;
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element);
                }
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function validateMember(str, element) {
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=WorkflowMemberDetailsCoverSearchCommand&memberNumber=" + str + "&exactCoverNum=1";
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element);
                };
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function encodeFormForSubmit(form) {
                var encStr = "formName=" + escape(form.name) + "&";
                for (var i = 0; i < form.elements.length; i++) {
                    var element = form.elements[i];
                    var elementType = element.type.toUpperCase();
                    var elementVal = "";
                    if (!element.name)
                        continue;
                    encStr += element.name + "=";
                    if (elementType == "CHECKBOX") {
                        elementVal = element.checked ? element.checked : "false";
                    } else {
                        elementVal = element.value;
                    }
                    encStr += escape(elementVal ? elementVal : "");
                    if (i < form.elements.length - 1)
                        encStr += "&";
                }
                return encStr;
            }

            function validateProvider(str, element) {
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=WorkflowFindPracticeByCodeCommand&providerNumber=" + str;
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element)
                };
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function stateChanged(element) {
                if (xmlhttp.readyState == 4)
                {
                    // alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    // alert(result);
                    if (result == "WorkflowFindPracticeByCodeCommand") {
                        var name = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=') + 1, xmlhttp.responseText.lastIndexOf('|'));
                        var discipline = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=') + 1, xmlhttp.responseText.lastIndexOf('$'));
                        document.getElementById('discipline').value = discipline;
                        document.getElementById('provName').value = name;
                        document.getElementById(element + '_error').innerHTML = '';
                    } else if (result == "GetMemberByNumberCommand") {
                        var resultArr = xmlhttp.responseText.split("|");
                        var numberArr = resultArr[1].split("=");
                        var emailArr = resultArr[2].split("=");
                        var productArr = resultArr[3].split("=");
                        var entityArr = resultArr[4].split("=");

                        var number = numberArr[1];
                        var productId = productArr[1];
                        var email = emailArr[1];
                        var entityID = entityArr[1];

                        document.getElementById(element + '_error').innerHTML = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('productId').value = productId;
                        document.getElementById('entityID_text').value = entityID;
                        var sEntity_Id = "" + entityID.toString();
                        sEntity_Id = sEntity_Id.slice(0, -3);
                        document.getElementById('memberEntityId').value = sEntity_Id;
                        document.getElementById('memberCoverEntityId').value = sEntity_Id;

                        if (email != null && email != "") {
                            document.getElementById('callerContact').value = email;
                        } else if (number != null && number != "") {
                            document.getElementById('callerContact').value = number;
                        }

                        //document.getElementById('searchCalled').value = "";
            <%
                request.setAttribute("exactCoverNum", "1");
            %>
                        submitWithAction("WorkflowMemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/WorkflowUpdateCallTracking.jsp");
                    } else if (result == "Error") {
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|') + 1, xmlhttp.responseText.length);
                        if (forCommand == "WorkflowFindPracticeByCodeCommand") {
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById('provName').value = '';
                            document.getElementById('discipline').value = '';
                        } else if (forCommand == "GetMemberByNumberCommand") {
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element + '_text').value = '';
                        }
                    }
                }
            }


            function hideSubCategory() {
                $("#disciplineCategory").hide();
                $("#benefitSub").hide();
                $("#claimSub").hide();
                $("#membershipSub").hide();
                $("#UnderwritingSub").hide();
                $("#queryCategories").show();

                // new categories
                $("#claimsFC").hide();
                $("#claimsSC").hide();
                $("#claimsTC").hide();
                $("#statementsFC").hide();
                $("#statementsTC").hide();
                $("#wellnessFC").hide();
                $("#benefitsFCReso").hide();
                $("#benefitsFCSpec").hide();
                $("#benefitsSC").hide();
                $("#benefitsTC").hide();
                $("#prenmiumsFC").hide();
                $("#premiumsTC").hide();
                $("#membershipFC").hide();
                $("#memberShipTC").hide();
                $("#serviceProviderFC").hide();
                $("#serviceProviderTC").hide();
                $("#chronicFC").hide();
                $("#chronicSC").hide();
                $("#oncologyFC").hide();
                $("#oncologySC").hide();
                $("#wellcareFC").hide();
                $("#wellcareSC").hide();
                $("#backOfficeFC").hide();
                $("#backOfficeSC").hide();
                $("#groupsFC").hide();
                $("#groupsSC").hide();
                $("#brokerFC").hide();
                $("#brokerSC").hide();
                $("#foundationFC").hide();
                $("#foundationSC").hide();

            }
            function checkMandatoryFields() {
            <%  if ((request.getAttribute("mandatoryfailed")) != null && (!request.getAttribute("mandatoryfailed").toString().equalsIgnoreCase("null")) && (!request.getAttribute("mandatoryfailed").toString().equalsIgnoreCase(""))) {

            %>

                $("#missing-fields").dialog({modal: true, width: 550,
                    buttons: {
                        'Done': function () {
                            $(this).dialog('close');
                        }
                    }
                });

            <%                    request.setAttribute("mandatoryfailed", null);

                }%>

            }

            function setCallDate() {

                var date = new Date();

                var year = date.getFullYear();
                var month = (date.getMonth() + 1);
                var day = date.getDate();

                if (month < 10) {
                    month = "0" + month;
                }

                if (day < 10) {
                    day = "0" + day;
                }

                var dateStr = "CT" + year + "" + month + "" + day + "N#";
                $("#callTrackNumber").val(dateStr);
                //document.getElementById('callTrackId').value = dateStr;
                $("#callTrackId").val(dateStr);
                //var authFromDate = $("#authFromDate").val(dateStr);

            }


            function ForwardToCC(number, type) {
                if (type != undefined && type === 'Member') {
                    window.open('/ManagedCare/AgileController?opperation=ViewCoverClaimsCommand&policyNumber_text=' + number + '&onScreen=MemberCCLogin', 'worker');
                } else if (type != undefined && type === 'Provider') {
                    window.open('/ManagedCare/AgileController?opperation=ViewProviderDetailsCommand&provNum_text=' + number + '&onScreen=MemberCCLogin', 'worker');
                }
            }


        </script>
    </head>

    <body onload="parent.document.getElementById('canRefresh').value = 0; populateRequests();">
        <div align="Center">
            <div id="missing-fields" title="Missing Fields"><p>You must select at least one of the Query Categories</p></div>
            <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                        <!-- content goes here -->
                        <label class="header">Log New Call</label>
                        <br/>
                        <table>
                            <agiletags:ControllerForm name="saveCallInfo" validate="yes">
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <input type="hidden" name="productId" id="productId" value="" />
                                <input type="hidden" name="product" id="product" value="${product}" />
                                <input type="hidden" name="actionDesc" id="actionDesc" value="" />
                                <input type="hidden" name="entityID_text" id="entityID_text" value="" />
                                <input type="hidden" name="memberCoverEntityId" id="memberCoverEntityId" value="${memberCoverEntityId}" />
                                <input type="hidden" name="memberEntityId" id="memberEntityId" value="" />
                                <input type="hidden" name="onScreen" id="onScreen" value="Calltrack/WorkflowUpdateCallTracking.jsp" />

                                <agiletags:HiddenField elementName="searchCalled"/>

                                <table>

                                    <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Call Type" elementName="callType" lookupId="108" javaScript="onchange=\"toggleCallType();\"" errorValueFromSession="yes" mandatory="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Call Number" elementName="callTrackNumber" valueFromRequest="callTrackNumber" readonly="yes"/></tr>
                                    <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Call Status" elementName="callStatus" lookupId="105" javaScript="" errorValueFromSession="yes" mandatory="yes" /></tr>

                                    <tr id="popCoverSearch" style="display:none" align="left">
                                        <td align="left" width="160px">
                                            <label>Member Number:</label>
                                        </td>
                                        <td align="left" width="200px">
                                            <input type="text" id="memberNumber_text" name="memberNumber_text" value="${memberNumber_text}" size="30" onchange="getPrincipalMemberForNumber(this.value, 'memberNumber', this.form);">
                                        </td>
                                        <td>
                                            <label class="red">*</label>
                                        </td>
                                        <td align="left">
                                            <img src="/ManagedCare/resources/ForwardToCC.gif" width="28" height="28" alt="Search" border="0" onclick="ForwardToCC(document.getElementById('memberNumber_text').value, 'Member');">
                                        </td>
                                        <td width="200px" align="left">
                                            <label id="memberNumber_error" class="error"></label>
                                        </td>
                                    </tr>
                                    <tr id="popProvider" style="display:none" align="left">
                                        <td align="left" width="160px">
                                            <label>Provider Number:</label>
                                        </td>
                                        <td align="left" width="200px">
                                            <input type="text" id="provNum" name="provNum" value="${provNum}" size="30" onchange="validateProvider(this.value, 'provNum');">
                                        </td>
                                        <td>
                                            <label class="red">*</label>
                                        </td>
                                        <td align="left">
                                            <img src="/ManagedCare/resources/ForwardToCC.gif" width="28" height="28" alt="Search" border="0" onclick="ForwardToCC(document.getElementById('provNum').value, 'Provider');">
                                        </td>
                                        <td width="200px" align="left">
                                            <label id="providerNumber_error" class="error"></label>
                                        </td>
                                    </tr>
                                    <tr id="popProviderSearch" style="display:none" align="left">
                                        <td align="left" width="160px">
                                            <label>Provider Number:</label>
                                        </td>
                                        <td align="left" width="200px">
                                            <input type="text" id="providerNumber_text" name="providerNumber_text" value="${providerNumber_text}" size="30" onchange="validateProvider(this.value, 'providerNumber');">
                                        </td>
                                        <td>
                                            <label class="red">*</label>
                                        </td>
                                        <td align="left">
                                            <img src="/ManagedCare/resources/ForwardToCC.gif" width="28" height="28" alt="Search" border="0" onclick="ForwardToCC(document.getElementById('providerNumber_text').value, 'Provider');">
                                        </td>
                                        <td width="200px" align="left">
                                            <label id="providerNumber_error" class="error"></label>
                                        </td>
                                    </tr>
                                    <tr id="popCover" style="display:none" align="left">
                                        <td align="left" width="160px">
                                            <label>Member Number:</label>
                                        </td>
                                        <td align="left" width="200px">
                                            <input type="text" id="memberNum" name="memberNum" value="${memberNum}" size="30" onchange="getPrincipalMemberForNumber(this.value, 'memberNumber', this.form);">
                                        </td>
                                        <td>
                                            <label class="red">*</label>
                                        </td>
                                        <td align="left">
                                            <img src="/ManagedCare/resources/ForwardToCC.gif" width="28" height="28" alt="Search" border="0" onclick="ForwardToCC(document.getElementById('memberNum').value, 'Member');">
                                        </td>
                                        <td width="200px" align="left">
                                            <label id="memberNumber_error" class="error"></label>
                                        </td>
                                    <tr id="providerType" style="display:none">
                                        <agiletags:LabelTextBoxError displayName="Provider Name" elementName="provName"  valueFromRequest="provName"/>
                                        <agiletags:LabelTextBoxError displayName="Discipline" elementName="discipline" valueFromRequest="discipline" readonly="yes"/>
                                    </tr>

                                    <tr><agiletags:LabelTextBoxError displayName="Caller Name" elementName="callerName" mandatory="yes" valueFromRequest="callerName"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Contact Details" elementName="callerContact" mandatory="yes" valueFromRequest="callerContact"/></tr>
                                    <agiletags:LabelNeoLookupValueCheckbox displayName="Contact Type" elementName="queryMethod" lookupId="150" javascript="" />

                                    <tr><td colspan="6"><div id="memberGrid" style="display:none">
                                                <agiletags:MemberCoverSearchTag commandName="" javaScript="" />
                                            </div></td></tr>
                                    <div>
                                        <tr id="queryCategories"><agiletags:LabelNeoLookupValueCheckbox displayName="Query Categories" elementName="callQ" lookupId="151" javascript="" /></tr>
                                    </div>
                                    <div>
                                        <tr id="doneButon"><td colspan="5" align="left">
                                                <button name="opperation" type="button" value="Done">Done</button>
                                            </td></tr>
                                    </div>

                                    <!-- Claims -->
                                    <div>
                                        <tr class="claimsLeve11" id="claimsFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Claims First Category" elementName="claimsFC" lookupId="152" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr class="claimsLevel2" id="claimsSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Claims Second Category" elementName="claimsSC" lookupId="153" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr class="claimsLeve3" id="claimsTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Claims Third Category" elementName="claimsTC" lookupId="154" javascript=""/></tr>
                                    </div>

                                    <!-- Back Office -->
                                    <div>
                                        <tr id="backOfficeFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Back Office First Category" elementName="backOfficeFC" lookupId="155" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr id="backOfficeSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Back Office Second Category" elementName="backOfficeSC" lookupId="156" javascript=""/></tr>
                                    </div>

                                    <!-- Groups -->
                                    <div>
                                        <tr id="groupsFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Groups First Category" elementName="groupsFC" lookupId="363" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr id="groupsSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Groups Second Category" elementName="groupsSC" lookupId="156" javascript=""/></tr>
                                    </div>

                                    <!-- Brokers -->
                                    <div>
                                        <tr id="brokerFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Brokers First Category" elementName="brokerFC" lookupId="364" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr id="brokerSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Brokers Second Category" elementName="brokerSC" lookupId="156" javascript=""/></tr>
                                    </div>

                                    <!-- Wellness -->
                                    <div>
                                        <tr id="wellnessFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Wellness First Category" elementName="wellnessFC" lookupId="365" javascript=""/></tr>
                                    </div>
                                    
                                    <!-- Statement -->
                                    <div>
                                        <tr id="statementsFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Statements First Category" elementName="statementsFC" lookupId="155" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr id="statementsTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Statements Second Category" elementName="statementsTC" lookupId="156" javascript=""/></tr>
                                    </div>

                                    <!-- Benefits -->
                                    <div>
                                        <tr id="benefitsFCReso"><agiletags:LabelNeoLookupValueCheckbox displayName="Benefits First Category" elementName="benefitsFCReso" lookupId="157" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr id="benefitsFCSpec"><agiletags:LabelNeoLookupValueCheckbox displayName="Benefits First Category" elementName="benefitsFCSpec" lookupId="284" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr id="benefitsSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Benefits Second Category" elementName="benefitsSC" lookupId="158" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr id="benefitsTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Benefits Third Category" elementName="benefitsTC" lookupId="159" javascript=""/></tr>
                                    </div>

                                    <!-- Premiums -->
                                    <div>
                                        <tr id="prenmiumsFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Premiums First Category" elementName="prenmiumsFC" lookupId="160" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr id="premiumsTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Premiums Second Category" elementName="premiumsTC" lookupId="161" javascript=""/></tr>
                                    </div>

                                    <!-- Membership -->
                                    <div>
                                        <tr id="membershipFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Membership First Category" elementName="membershipFC" lookupId="162" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr id="memberShipTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Membership Second Category" elementName="memberShipTC" lookupId="163" javascript=""/></tr>
                                    </div>

                                    <!-- Service Provider -->
                                    <div>
                                        <tr id="serviceProviderFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Service Provider First Category" elementName="serviceProviderFC" lookupId="164" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr id="serviceProviderTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Service Provider Second Category" elementName="serviceProviderTC" lookupId="165" javascript=""/></tr>
                                    </div>

                                    <!-- Chronic -->
                                    <div>
                                        <tr id="chronicFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Chronic First Category" elementName="chronicFC" lookupId="357" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr id="chronicSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Chronic Second Category" elementName="chronicSC" lookupId="358" javascript=""/></tr>
                                    </div>

                                    <!-- Oncology -->
                                    <div>
                                        <tr id="oncologyFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Oncology First Category" elementName="oncologyFC" lookupId="359" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr id="oncologySC"><agiletags:LabelNeoLookupValueCheckbox displayName="Oncology Second Category" elementName="oncologySC" lookupId="360" javascript=""/></tr>
                                    </div>

                                    <!-- Wellcare -->
                                    <div>
                                        <tr id="wellcareFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Wellcare First Category" elementName="wellcareFC" lookupId="361" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr id="wellcareSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Wellcare Second Category" elementName="wellcareSC" lookupId="362" javascript=""/></tr>
                                    </div>

                                    <!-- Foundation -->
                                    <div>
                                        <tr id="foundationFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Foundation First Category" elementName="foundationFC" lookupId="166" javascript=""/></tr>
                                    </div>
                                    <div>
                                        <tr id="foundationSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Foundation Second Category" elementName="foundationSC" lookupId="167" javascript=""/></tr>
                                    </div>
                                </table>
                                <table>
                                    <!-- End of the new lookups -->
                                    <tr><agiletags:LabelTextAreaErrorBig displayName="Notes" elementName="notes" valueFromSession="no" /></tr> 
                                    <tr><agiletags:LabelTextBoxError displayName="Requested By" readonly="yes" elementName="reqDate" valueFromRequest="disDate"/></tr>

                                    <tr>
                                        <agiletags:ButtonOpperation commandName="WorkflowSaveCallTrackDetailsCommand" align="left" displayname="Submit" span="1" type="button"  javaScript="onClick=\"submitWithAction('WorkflowSaveCallTrackDetailsCommand'),checkMandatoryFields()\";"/>
                                        <agiletags:ButtonOpperation commandName="WorkflowReloadCallLogCommand" align="left" displayname="Cancel" span="1" type="button"  javaScript="onClick=\"submitWithAction('WorkflowReloadCallLogCommand')\";"/>
                                    </tr>
                                </table>
                                <table  align="right">
                                    <tr>
                                        <agiletags:LabelTextBoxError displayName="User Name" elementName="uName" readonly="yes" valueFromRequest="uName"/>
                                    </tr>
                                    <tr>
                                        <agiletags:LabelTextBoxError displayName="Date" elementName="disDate" readonly="yes" valueFromRequest="disDate"/>
                                    </tr>
                                </table>
                            </agiletags:ControllerForm>
                        </table>

                        <!-- content ends here -->
                    </td></tr></table>
        </div>

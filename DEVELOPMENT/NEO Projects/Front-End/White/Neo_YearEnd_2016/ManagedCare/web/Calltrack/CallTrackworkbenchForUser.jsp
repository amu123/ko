<%--
    Document   : CallTrackworkbenchForUser
    Created on : 2010/04/28, 02:57:35
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/JavaScript" language="JavaScript">

            function submitWithAction01(action) {
            var dateFrom = $("#dateFrom").val();
            var dateTo = $("#dateTo").val();

            clearErrors();
            var error1= "";
            var error2 = "";

            if(dateFrom != null && dateFrom != "") {
            error1 = "notnull";
            }

            if(dateTo != null && dateTo != "") {
            error2 = "notnull";
            }
            if(error1 != "" && error2 != ""){
            document.getElementById('opperation').value = action;
            document.forms[0].submit();
            }else if(error1 != ""){
            $("#dateFrom_error").text("");
            $("#dateTo_error").text("Please enter the date");
            }else if(error2 != "") {
            $("#dateFrom_error").text("Please enter the date");
            $("#dateTo_error").text("");
            }else {
            $("#dateFrom_error").text("Please enter the date");
            $("#dateTo_error").text("Please enter the date");
            }
            }

            function clearErrors() {
            $("#dateFrom_error").text("");
            $("#dateTo_error").text("");
            }

            function submitWithAction(action, index) {
            document.getElementById('opperation').value = action;
            document.getElementById('trackId').value = index;
            document.forms[0].submit();
            }

            function enableButton()
            {
            document.callButton.sub.disabled=true;
            if(document.callButton.chk.checked==true)
            {
            document.callButton.sub.disabled=false;
            }
            if(document.callButton.chk.checked==false)
            {
            document.callButton.sub.enabled=false;
            }
            }
        </script>
    </head>
    <body>
        <%
            request.setAttribute("CallWorkbench", "true");
        %>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Call Workbench</label>
                    <table>
                        <agiletags:ControllerForm name="CallWorkBench">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="trackId" id="trackId" value="" />
                            <agiletags:CallworkbenchResultTableForUserTag commandName="AllocateCallTrackWorkbench" />
                            <table> 
                                <tr><agiletags:LabelTextBoxDate displayname="Date From" elementName="dateFrom" valueFromSession="yes" mandatory="yes"/><tr>
                                <tr><agiletags:LabelTextBoxDate displayname="Date To" elementName="dateTo" valueFromSession="yes" mandatory="yes"/></tr>
                                <tr>
                                    <agiletags:ButtonOpperation align="right" span="1" type="button" commandName="CallWorkbenchCommand" displayname="Preview" javaScript="onClick=\"submitWithAction01('CallWorkbenchCommand');\""/>
                                    <agiletags:ButtonOpperation align="right" span="1" type="button" commandName="CancelCallWorkbenchCommand" displayname="Cancel" javaScript="onClick=\"submitWithAction('CancelCallWorkbenchCommand');\""/>
                                </tr>
                            </table>
                        </agiletags:ControllerForm>
                    </table>
                </td></tr></table>
    </body>
</html>


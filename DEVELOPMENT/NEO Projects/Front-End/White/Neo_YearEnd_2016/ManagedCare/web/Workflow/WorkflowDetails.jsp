<%-- 
    Document   : WorkflowDetails
    Created on : 18 Jul 2016, 2:11:19 PM
    Author     : janf
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/Workflow/WorkflowTabUtil.js"></script>
<script type='text/javascript' src="${pageContext.request.contextPath}/resources/Workflow/Workflow.js"></script>
<script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
<%--<agiletags:WorkflowTabs wftSelectedIndex="link_WfSearch"/>--%>
<label class="subheader">${wfPanel.subject}</label>
<br/>
<br/>
<table style="width:100%">
    <c:if test="${not empty isComplaint && isComplaint == '1'}">
        <tr style="text-align: left">
            <td style="text-align: center" colspan="2">
                <label class="labelTextDisplay" style="color: red">Complaint</label>
            </td>
        </tr>
    </c:if>
    <tr style="text-align: left">
        <td align="left" width="160px"><label class="labelTextDisplay">Status:</label></td>
        <td align="left" width="200px"><label class="labelTextDisplay">${wfPanel.statusName}</label></td>
    </tr>
    <c:if test="${not empty resolutionReason}">
        <tr style="text-align: left">
            <td align="left" width="160px">
                <label class="labelTextDisplay">Resolution Reason:</label>
            </td>
            <td align="left" width="200px">
                <label class="labelTextDisplay">${resolutionReason}</label>
            </td>
        </tr>
    </c:if>

    <tr style="text-align: left">
        <c:if test="${empty entityNumber}">
            <td style="text-align: center" colspan="2">
                <label>Entity Number not provided</label>
            </td>
        </c:if>
        <c:if test="${not empty entityNumber}">
            <td align="left" width="160px">
                <label class="labelTextDisplay">Entity Number:</label>
            </td>
            <td align="left" width="200px">
                <label class="labelTextDisplay">${entityNumber}</label>
            </td>
        </c:if>
    </tr>

    <c:if test="${not empty wfPanel.entityName}">
        <tr style="text-align: left">
            <td align="left" width="160px">
                <label class="labelTextDisplay">Entity Name:</label>
            </td>
            <td align="left" width="200px">
                <label>${wfPanel.entityName}</label>
            </td>
        </tr>
    </c:if>

    <tr style="border-bottom: #000000 thin"/>
    <table style="width: 100%">
        <c:forEach var="entry" items="${arrWfItemDetails}" varStatus="loop">
            <c:if test="${not empty entry.notes}">
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td align="left" width="100%">
                        <label>Date: ${agiletags:formatXMLGregorianDate(entry.creationDate)}</label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <textarea style="font-family:arial;" id="wfNote${loop.index}" name="wfNote${loop.index}" cols="56" wrap="true" rows="10" readonly="true">${entry.notes}</textarea>
                    </td>
                </tr>
            </c:if>
        </c:forEach>
    </table>
</table>
<br/>
<table style="width: 100%">
    <tr style="text-align: left">
        <td align="left" width="160px">
            <label>Last Updated: ${agiletags:formatXMLGregorianDate(wfPanel.lastUpdateDate)}</label>
        </td>
        <td align="right" width="200px">
            <c:if test="${not empty userName}">
                <label>Assigned to: ${userName}</label>
            </c:if>

            <c:if test="${empty userName}">
                <label>Assigned to: Unassigned</label>
            </c:if>
        </td>
    </tr>
</table>
<c:choose>
    <c:when test="${workflowSearch == null}">       
        <td><button name="opperation" type="button" onClick="switchWFTab('WorkflowContentCommand', 'Workflow/WorkflowSearch.jsp', '30%, 70%')" value="">Return</button></td>
    </c:when>
    <c:otherwise>
        <td><button name="opperation" type="button" onClick="submitWithAction('ForwardToMemberCommunicationCommand');" value="">Return</button></td>
   </c:otherwise>
</c:choose>

<agiletags:ControllerForm name="workbenchDetails" action="/ManagedCare/AgileController">
    <input type="hidden" name="opperation" id="opperation" value="WorkflowContentCommand" size="30" />
    <input type="hidden" name="application" id="application" value="0" size="30" />
    <input type="hidden" name="onScreen" id="onScreen" value="Workflow/WorkflowSearch.jsp" size="30" />
    <input type="hidden" name="itemId" id="itemId" value="" size="30"/>
</agiletags:ControllerForm>
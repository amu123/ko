<%-- 
    Document   : WorkflowOptPnlContent
    Created on : 2016/07/17, 11:21:49
    Author     : gerritr
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<label class="subheader">${wfPanel.subject}</label>
<br/>
<br/>
<agiletags:ControllerForm name="OptionPanelContent">
    <input type="hidden" name="fileLocation" id="fileLocation" value="" />
    <input type="hidden" name="opperation" id="opperation" value="WorkflowEmailViewCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="Workflow/WorkflowMyWorkbench.jsp" size="30" />

    <table style="width:100%">
        <c:if test="${not empty isComplaint && isComplaint == '1'}">
            <tr style="text-align: left">
                <td style="text-align: center" colspan="3">
                    <label class="labelTextDisplay" style="color: red">Complaint</label>
                </td>
            </tr>
        </c:if>
            
            <tr style="text-align: left">
                <c:if test="${not empty wfPanel.subTypeValue}">
                    <td align="left" width="20%"><label class="labelTextDisplay">SubType:</label></td>
                    <td align="center" width="60%"><label class="labelTextDisplay">${wfPanel.subTypeValue}</label></td>
                    <td align="left" width="20%">&nbsp;</td>
                </c:if>
                <c:if test="${empty wfPanel.subTypeValue}">
                    <td align="left" width="20%"><label class="labelTextDisplay">SubType:</label></td>
                    <td align="center" width="60%"><label class="labelTextDisplay">None</label></td>
                    <td align="left" width="20%">&nbsp;</td>
                </c:if>    
        </tr>
            
        <tr style="text-align: left">
            <td align="left" width="20%"><label class="labelTextDisplay">Status:</label></td>
            <td align="center" width="60%"><label class="labelTextDisplay">${wfPanel.statusName}</label></td>
            <td align="left" width="20%">&nbsp;</td>
        </tr>
        
        <tr style="text-align: left">
            <td align="left" width="160px">
                <label class="labelTextDisplay">Creation Date:</label>
            </td>
            <td align="left" width="200px" colspan="2">
                <label>${agiletags:formatXMLGregorianDate(wfPanel.creationDate)}</label>
            </td>
        </tr>
        
        <tr style="text-align: left">
            <c:if test="${empty entityNumber}">
                <td style="text-align: center" colspan="3">
                    <label>Entity Number not provided</label>
                </td>
            </c:if>
            <c:if test="${not empty entityNumber}">
                <td align="left" width="160px">
                    <label class="labelTextDisplay">Entity Number:</label>
                </td>
                <td align="left" width="200px" colspan="2">
                    <label style="color:blue; cursor:pointer" onclick="clearAllFromSession();
                            window.open('/ManagedCare/AgileController?opperation=ViewCoverClaimsCommand&policyNumber_text=${entityNumber}&onScreen=MemberCCLogin', 'worker');">${entityNumber}</label>
                </td>
            </c:if>
        </tr>

        <c:if test="${not empty wfPanel.entityName}">
            <tr style="text-align: left">
                <td align="left" width="160px">
                    <label class="labelTextDisplay">Entity Name:</label>
                </td>
                <td align="left" width="200px" colspan="2">
                    <label>${wfPanel.entityName}</label>
                </td>
            </tr>
        </c:if>

        <tr style="border-bottom: #000000 thin"/>
        <table style="width: 100%">

            <c:forEach var="entry" items="${arrWfItemDetails}" varStatus="loop">
                <c:if test="${not empty entry.notes}">
                    <tr><td colsapn="2">&nbsp;</td></tr>
                    <tr>
                        <td align="left" width="25%">
                            <label>Date: ${agiletags:formatXMLGregorianDateTime(entry.creationDate)}</label>
                        </td>
<!--                        <td align="left" width="25%">
                            <c:if test="${not empty entry.userName}">
                                <label>From: ${entry.createdBy}</label>
                            </c:if>
                            <c:if test="${empty entry.userName}">
                                <label>From: Unassigned</label>
                            </c:if>    
                        </td>-->
                        <td align="left" width="25%">
                            <label>to: ${entry.queueDesc}</label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <textarea style="font-family:arial;" id="wfNote${loop.index}" name="wfNote${loop.index}" cols="56" wrap="true" rows="10" readonly="true">${entry.notes}</textarea>
                        </td>
                    </tr>
                </c:if>
            </c:forEach>
        </table>
    </table>
    <br/>
    <table style="width: 100%">
        <tr style="text-align: left">
            <td align="left" width="40%">
                <label>Last Updated: ${agiletags:formatXMLGregorianDate(wfPanel.lastUpdateDate)}</label>
            </td>
            <td align="center" width="20%">
                <c:if test="${not empty emlLocation}">
                    <input type="button" id="btnDownload" value="View Mail" onClick="submitActionAndViewFile(this.form, 'WorkflowEmailViewCommand', '${emlLocation}');">
                </c:if>
            </td>
            <td align="right" width="40%">
                <c:if test="${not empty userName}">
                    <label>Assigned to: ${userName}</label>
                </c:if>

                <c:if test="${empty userName}">
                    <label>Assigned to: Unassigned</label>
                </c:if>
            </td>
        </tr>
    </table>
</agiletags:ControllerForm>
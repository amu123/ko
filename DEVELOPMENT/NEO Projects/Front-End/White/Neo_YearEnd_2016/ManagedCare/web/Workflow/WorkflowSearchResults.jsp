<%-- 
    Document   : WorkflowSearchResults
    Created on : Aug 26, 2016, 8:06:30 PM
    Author     : janf
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Workflow/Workflow.js"></script>
        <title>JSP Page</title>
    </head>
    <body>
        
        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
            <tr>
                <th align="left">Reference Number</th>
                <th align="left">Entity Number</th>
                <th align="left">Entity Name</th>
                <th align="left">Workflow Queue</th>
                <th align="left">Assigned User</th>
                <th align="left">Workflow Status</th>
                <th align="left">Subject</th>
                <th align="left">Start Date</th>
                <th align="left">Last Updated Date</th>
                <th align="left">Action</th>

            </tr>
            <c:forEach var="item" items="${viewWorkflowSearchResults}" varStatus="loop">
                <c:set var="assignedUser" value="userAssigned${item.refNum}"></c:set>
                    <tr>
                        <td><label class="label">${item.refNum}</label></td>
                    <td><label class="label">${item.entityNumber}</label></td>
                    <td><label class="label">${item.entityName}</label></td>
                    <td><label class="label">${item.queueDesc}</label></td>
                    <td><label class="label">${item.userID}</label></td>
                    <td><label class="label">${item.statusName}</label></td>
                    <td><label class="label">${item.subject}</label></td>
                    <td><label class="label">${agiletags:formatXMLGregorianDate(item.creationDate)}</label></td>
                    <td><label class="label">${agiletags:formatXMLGregorianDate(item.lastUpdateDate)}</label></td>
                    
                        <c:choose>
                            <c:when test="${workflowSearch == null}">
                            <td><button name="opperation" type="button" onClick="viewSelectedWorkflowItem('ViewWorkbenchDetails', '${item.itemID}')" value="">Details</button></td>
                        </c:when>
                        <c:otherwise>
                            <input type="hidden" name="workflowSearch" id="workflowSearch" value="${workflowSearch}" />
                            <input type="hidden" id="itemId" name="itemId" value="${item.itemID}"/>
                            
                            <td><button name="opperation" type="button" onClick="submitWithAction('ViewWorkbenchDetails')" value="ViewWorkbenchDetails">Details</button></td>
                            
                        </c:otherwise>
                    </c:choose>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>

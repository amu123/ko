<%-- 
    Document   : WorkflowChangeStatus
    Created on : 2016/08/12, 14:21:49
    Author     : gerritr
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>


<agiletags:ControllerForm name="WorkflowChangeStatus" action="/ManagedCare/AgileController">
    <input type="hidden" name="opperation" value="WorkflowContentCommand" size="30" />
    <input type="hidden" name="onScreen" value="Workflow/WorkflowMyWorkbench.jsp" size="30" />
    <input type="hidden" name="actionDesc" value="" size="30" />
    <input type="hidden" name="workflowStatus" value="" size="30" />
    <input type="hidden" name="workflowResolutionReason" value="" size="30" />
    <c:if test="${not empty wfPanel.itemID}">
        <input type="hidden" name="itemId" value="${wfPanel.itemID}" size="30" />
    </c:if>

    <div align="Center">
        <table>
            <tr>
                <td colspan="6" style="text-align: center">
                    <label class="subheader">Change Status</label>
                </td>
            </tr>
            <tr style="border-bottom: #000000 thin">
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr style="border-bottom: #000000 thin"/>
            <c:if test="${not empty wfPanel.refNum}">
                <tr style="text-align: left">
                    <td align="left" width="160px">
                        <label>Reference Number:</label>
                    </td>
                    <td align="left" width="200px">
                        <label>${wfPanel.refNum}</label>
                    </td>
                </tr>
            </c:if>
            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Workflow Status" elementName="workflowStatus" lookupId="350" javaScript="onChange=\"if(this.value !== '${wfPanel.statusID}'){buttonEnable(this.value, 'btnChange')}else{document.getElementById('btnChange').disabled = true;}; if(this.value === '5'){document.getElementById('wfResolutionReason').style.display = ''}else{document.getElementById('wfResolutionReason').style.display = 'none'}\""/></tr>
            <tr style="border-bottom: #000000 thin"/>
            <tr id="wfResolutionReason" style="display: none" ><agiletags:LabelNeoLookupValueDropDownError displayName="Resolution reasons" elementName="workflowResolutionReason" lookupId="369" javaScript="" /></tr>
            <tr style="border-bottom: #000000 thin"/>
            <tr>
                <td align="left" width="100%" colspan="3">
                    <label id="wfNoteDate">Note:</label>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="3">
                    <textarea style="font-family:arial;" id="wfNote" name="wfNote" cols="56" wrap="true" rows="10"></textarea>
                </td>
            </tr>
            <tr style="border-bottom: #000000 thin"/>
        </table>

        <table>
            <tr>
                <td>
                    <input type="button" id="btnChange" value="Change Status" disabled="true" onClick="document.getElementsByName('actionDesc')[0].value = 'WorkflowStatusChange'; document.getElementsByName('workflowStatus')[0].value = document.getElementById('workflowStatus').value; document.getElementsByName('workflowResolutionReason')[0].value = document.getElementById('workflowResolutionReason').value; submitWithActionOnScreen('WorkflowContentCommand', 'Workflow/WorkflowMyWorkbench.jsp', '34.2%, 65.8%');"> 
                </td>
            </tr>
        </table>
        <br>
        <br/>
    </div>
</agiletags:ControllerForm>

<%-- 
    Document   : WorkflowSearch
    Created on : 14 Jul 2016, 3:35:28 PM
    Author     : janf
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html style="height:100%; border-right: medium solid #6cc24a;">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/Workflow/WorkflowTabUtil.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Workflow/Workflow.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
    </head>
    <body>
        <agiletags:ControllerForm name="workflow" action="/ManagedCare/AgileController">
            <div align="Center">
                <agiletags:WorkflowTabs wftSelectedIndex="link_WfSearch"/>
                <label class="header">Workflow Search</label>
                <br/><br/>
                <fieldset  id="pageBorder" style="padding-left: 0; padding-right: 0; padding-top:0; padding-bottom: 0;  margin-left: 0; overflow: auto; height: calc(100vh - 110px);">
                    <div id="myWorkbench_Conent" style="height:100%; position: relative">
                        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                                    <nt:NeoPanel title="Workflow Search"  collapsed="true" reload="false" contentClass="neopanelcontentwhite">
                                        <table>
                                            <input type="hidden" name="mcPageDirection" id="mcPageDirection" value="" />
                                            <input type="hidden" name="workflowSearch" id="workflowSearch" value="${workflowSearch}" />
                                            <agiletags:HiddenField elementName="searchCalled" />

                                            <tr><nt:NeoTextField valueFromSession="true" displayName="Reference Number" elementName="refNum"/></tr>
                                            <tr><nt:NeoTextField valueFromSession="true" displayName="Entity Number" elementName="entNum"/></tr>
                                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Workflow Queue" elementName="workQueue" lookupId="353"/></tr>
                                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Status" elementName="workStatus" lookupId="350"/></tr>

                                            <tr><agiletags:ButtonOpperation align="left" span="1" type="button" commandName="SearchWorkflowCommand" displayname="Search Workflow" javaScript="onClick=\"searchResults(this);\""/></tr>
                                            <tr><td><label class="error" id="revError"></label></td></tr>
                                        </table>
                                    </nt:NeoPanel>

                                    <nt:NeoPanel title="Workflow Results"  collapsed="false" reload="true" contentClass="neopanelcontentwhite">
                                        <div id="workflow_SearchResult_Content">
                                            <%--<agiletags:ViewWorkflowSearchResults sessionAttribute="viewWorkflowSearchResults" />--%>
                                            <jsp:include page="WorkflowSearchResults.jsp"></jsp:include>
                                        </div>
                                    </nt:NeoPanel>
                                </td></tr></table>
                    </div>
                    <div id="myWorkbench_Conent_WF_Details" style="display: none"></div>
                </fieldset>
            </div>
        </div>
            <input type="hidden" name="opperation" id="opperation" value="WorkflowContentCommand" size="30" />
            <input type="hidden" name="application" id="application" value="0" size="30" />
            <input type="hidden" name="onScreen" id="onScreen" value="Workflow/WorkflowSearch.jsp" size="30" />
            <input type="hidden" name="itemId" id="itemId" value="" size="30"/>
        </agiletags:ControllerForm>
    </body>
</html>


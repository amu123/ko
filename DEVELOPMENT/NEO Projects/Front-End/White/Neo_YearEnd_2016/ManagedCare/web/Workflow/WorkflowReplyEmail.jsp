<%-- 
    Document   : WorkflowReplyEmail
    Created on : Aug 25, 2016, 8:49:53 AM
    Author     : janf
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>


<agiletags:ControllerForm name="WorkflowReplyEmail" action="/ManagedCare/AgileController">
    <input type="hidden" name="opperation" value="WorkflowContentCommand" size="30" />
    <input type="hidden" name="onScreen" value="Workflow/WorkflowMyWorkbench.jsp" size="30" />
    <input type="hidden" name="actionDesc" value="" size="30" />
    <input type="hidden" name="workflowStatus" value="" size="30" />
    <c:if test="${not empty wfPanel.itemID}">
        <input type="hidden" name="itemId" value="${wfPanel.itemID}" size="30" />
    </c:if>

    <div align="Center">
        <table>
            <tr>
                <td colspan="6" style="text-align: center">
                    <label class="subheader">Send Reply E-mail</label>
                </td>
            </tr>
            <tr style="border-bottom: #000000 thin">
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr style="border-bottom: #000000 thin"/>
            <c:if test="${not empty wfPanel.refNum}">
                <tr style="text-align: left">
                    <td align="left" width="160px">
                        <label>Reference Number:</label>
                    </td>
                    <td align="left" width="200px">
                        <label>${wfPanel.refNum}</label>
                    </td>
                </tr>
            </c:if>
            <c:if test="${not empty wfPanel.entityName}">
                <tr style="text-align: left">
                    <td align="left" width="160px">
                        <label>Name: </label>
                    </td>
                    <td align="left" width="200px">
                        <label>${wfPanel.entityName}</label>
                    </td>
                </tr>
            </c:if>
            <c:if test="${not empty wfPanel.subject}">
                <tr style="text-align: left">
                    <td align="left" width="160px">
                        <label>Subject:</label>
                    </td>
                    <td align="left" width="200px">
                        <label>${wfPanel.subject}</label>
                    </td>
                </tr>
            </c:if>

            <tr style="border-bottom: #000000 thin"/>
            <tr>
                <td align="left" width="100%" colspan="3">
                    <label id="wfNoteDate">Reply Mail:</label>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="3">
                    <textarea style="font-family:arial;" id="wfNote" name="wfNote" cols="56" wrap="true" rows="10" onchange="buttonEnable(this.value, 'btnChange')"></textarea>
                </td>
            </tr>
            <tr style="border-bottom: #000000 thin"/>
        </table>

        <table>
            <tr>
                <td>
                    <input type="button" id="btnChange" value="Send Reply" disabled="true" onClick="document.getElementsByName('actionDesc')[0].value = 'WorkflowSendEmail'; submitWithActionOnScreen('WorkflowContentCommand', 'Workflow/WorkflowMyWorkbench.jsp', '34.2%, 65.8%');"> 
                </td>
            </tr>
        </table>
        <br>
        <br/>
    </div>
</agiletags:ControllerForm>
<%-- 
    Document   : WorkflowCallTracking
    Created on : 2016/07/06, 11:21:49
    Author     : gerritr
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html style="height:100%; border-right: medium solid #6cc24a;">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>

        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Workflow/Workflow.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/Workflow/WorkflowTabUtil.js"></script>

    </head>
    <body>
        <div align="Center">
            <agiletags:WorkflowTabs wftSelectedIndex="link_WfCallTracking" />

            <label class="header">Call Tracking</label>
            <br/><br/>
            <fieldset id="pageBorder" style="padding-left: 0; padding-right: 0; padding-top:0; padding-bottom: 0;  margin-left: 0; overflow: auto; height: calc(100vh - 110px);">
                <agiletags:ControllerForm name="workflowCallTrack" action="/ManagedCare/AgileController">
                    <div id="callTracking_Conent" style="height:100%">
                        <br/>
                        <table>
                            <c:forEach var="entry" items="${wfCallTrack}">
                                <tr>
                                    <td>
                                        <input type="submit" value="${entry.id}" onclick="document.getElementById('actionDesc').value = '${entry.id}'; document.getElementById('canRefresh').value = '1'; submitWithinDiv('${entry.value}');" />     
                                    </td>
                                </tr>
                            </c:forEach>
<!--                            <tr>
                                <td>
                                    <input type="submit" value="Log a New New Call2" onclick="document.getElementById('onScreen').value = 'Calltrack/WorkflowLogNewCall2.jsp'; submitForm(this.form);" />     
                                </td>
                            </tr>-->

                        </table>
                        <br/>
                    </div>
                    <input type="hidden" id="opperation" name="opperation" value="WorkflowContentCommand" size="30" />
                    <input type="hidden" id="onScreen" name="onScreen" value="" size="30" />
                    <input type="hidden" id="canRefresh" name="canRefresh" value="1" size="30" />
                    <input type="hidden" id="actionDesc" name="actionDesc" value="" size="30" />
                </agiletags:ControllerForm>
            </fieldset>
        </div>
    </body>
</html>

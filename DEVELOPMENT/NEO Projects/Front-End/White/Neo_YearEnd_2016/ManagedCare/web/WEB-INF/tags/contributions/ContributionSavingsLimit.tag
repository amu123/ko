<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@attribute name="items" required="true" type="java.util.Collection" %>
<%@attribute name="showHeader" required="false" type="java.lang.Boolean" %>

    <c:if test="${!empty savingsLimits}">
        <c:if test="${showHeader != false}">
            <label class="subheader">Medical Savings Benefit</label>
        </c:if>
        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" >
            <tr>
                <th>Option</th>
                <th>Benefit</th>
                <c:if test="${!empty hasInt}">
                    <th>Interest</th>
                </c:if>
                <c:if test="${!empty hasJnl}">
                    <th>Journals</th>
                </c:if>
                <th>Claims Used</th>
                <c:if test="${!empty hasClaim}">
                    <th>Claims Recovered</th>
                </c:if>
                <c:if test="${!empty hasSav}">
                    <th>Savings Refunded</th>
                </c:if>
                <c:if test="${!empty hasContrib}">
                    <th>Contributions Recovered</th>
                </c:if>
                <c:if test="${!empty hasSavRecov}">
                <th>Savings Recovered</th>    
                </c:if>
                <th>Balance</th>
            </tr>
            <c:forEach var="sav" items="${savingsLimits}">
            <tr>
                <td><label>${sav.optionName}</label></td>
                <td align="right"><label>${agiletags:formatStringAmount(sav.available)}</label></td>
                <c:if test="${!empty hasInt}">
                    <td align="right"><label>${agiletags:formatStringAmount(sav.interest)}</label></td>
                </c:if>
                <c:if test="${!empty hasJnl}">
                    <td align="right"><label>${agiletags:formatStringAmount(sav.jnl)}</label></td>
                </c:if>
                <td align="right"><label>${agiletags:formatStringAmount(sav.claimTotal)}</label></td>
                <c:if test="${!empty hasClaim}">
                    <td align="right"><label>${agiletags:formatStringAmount(sav.claimsRecovered)}</label></td>
                </c:if>
                <c:if test="${!empty hasSav}">
                    <td align="right"><label>${agiletags:formatStringAmount(sav.savingsPaid)}</label></td>
                </c:if>
                <c:if test="${!empty hasContrib}">
                    <td align="right"><label>${agiletags:formatStringAmount(sav.contribRecovered)}</label></td>
                </c:if> 
                <c:if test="${!empty hasSavRecov}">
                <td align="right"><label>${agiletags:formatStringAmount(sav.savingsRecovered)}</label></td>  
                </c:if> 
                <td align="right"><label>${agiletags:formatStringAmount(sav.balance)}</label></td>
            </tr>
            </c:forEach>
        </table>
        <br>
    </c:if>


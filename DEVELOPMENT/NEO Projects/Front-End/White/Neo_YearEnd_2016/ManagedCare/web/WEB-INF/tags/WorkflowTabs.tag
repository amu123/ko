<%-- 
    Document   : WorkflowTabs
    Created on : 2016/07/06, 18:25:52
    Author     : gerritr
--%>

<%@tag description="Workflow menu item tabs tag" pageEncoding="UTF-8"%>
<%@ taglib prefix="wft" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="wftSelectedIndex"%>

<%-- any content can be specified here e.g.: --%>
<div id="workflow" class="workflow_area">
    <ol id="tabs">
        <li><a id="link_WfSearch" onClick="switchWFTab('WorkflowContentCommand','Workflow/WorkflowSearch.jsp','30%, 70%');" href="#Search" >Search</a></li>
        <li><a id="link_WfMyWorkbench" onClick="switchWFTab('WorkflowContentCommand', 'Workflow/WorkflowMyWorkbench.jsp','34.2%, 65.8%');" href="#MyWorkbench">My Workbench</a></li>
        <li><a id="link_WfCallTracking" onClick="switchWFTab('WorkflowContentCommand', 'Workflow/WorkflowCallTracking.jsp','20%, 80%');" href="#CallTracking">Call Tracking</a></li>
        <li><a id="link_WfMailRetrieval" onClick="switchWFTab('WorkflowContentCommand', 'Workflow/WorkflowMailRetrieval.jsp','20%, 80%');" href="#Email">Email</a></li>
    </ol>
    <hr/>
</div>
<script>setSelectedWfTab('${wftSelectedIndex}');</script>
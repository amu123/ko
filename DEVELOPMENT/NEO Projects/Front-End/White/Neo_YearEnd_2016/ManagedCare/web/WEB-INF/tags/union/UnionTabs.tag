<%-- 
    Document   : UnionTabs
    Created on : Jul 5, 2016, 2:22:58 PM
    Author     : shimanem
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="tabSelectedIndex"%>
<%@attribute name="isClearingUser"%>

<%-- any content can be specified here e.g.: --%>
<div id="header" class="header_area">
    <ol id="tabs">
        <li><a id="link_union_UD" onClick="submitWithAction('ViewUnionDetailsCommand');" href="#UnionDetails" >Union Details</a></li>
        <li><a id="link_union_GH" onClick="submitWithAction('ViewGroupHistoryCommand');" href="#GroupHistory">Group History</a></li>
<!--        <li><a id="link_union_MH" onClick="submitWithAction('ViewUnionMemberHistoryCommand');"href="#MemberHistory">Member History</a></li>
        <li><a id="link_union_DC" onClick="submitWithAction('ViewUnionDocuments');" href="#Documents">Documents</a></li>-->        
        <li><a id="link_union_NT" onClick="submitWithAction('ViewUnionNotes');" href="#Notes">Notes</a></li>
        <li><a id="link_union_AT" onClick="submitWithAction('ViewUnionAuditTrail');" href="#AuditTrail">Audit Trail</a></li>
    </ol>
</div><br/>
<script>setSelectedCCTab('${tabSelectedIndex}');</script>
<br/>
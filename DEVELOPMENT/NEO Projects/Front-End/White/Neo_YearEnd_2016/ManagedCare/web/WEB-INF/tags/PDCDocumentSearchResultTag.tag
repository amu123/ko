<%-- 
    Document   : PDCDocumentSearchResultTag
    Created on : 25 Mar 2015, 12:14:44 PM
    Author     : shimanem
--%>
<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<td>
    <div class="content_area" >

        <div id="docsResults">
            <input type="hidden" name="folderList" id="folderList" value="" />
            <input type="hidden" name="fileLocation" id="fileLocation" value="" />
            <input type="hidden" name="opperation" id="opperation" value="" />
            <agiletags:HiddenField elementName="searchCalled"/>
            <input type="hidden" name="onScreen" id="onScreen" value="" />

            <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                <c:choose>
                    <c:when test="${sessionScope.docIndexList != null}" >
                        <tr>
                            <th>Member Number</th>
                            <th>Document Type</th>
                            <th>Document Location</th>
                            <th>Document Creation Date</th>
                            <th></th>
                            <th></th>
                        </tr>
                        <c:forEach var="list" items="${sessionScope.docIndexList}">        
                            <tr>
                                <td><label class="label">${list.entityNumber}</label></td>
                                <td><label class="label">${list.docType}</label></td>
                                <td><label class="label">${list.location}</label></td>
                                <td><label class="label">${list.generationDate}</label></td>
                                <td><button type="button" name="" onclick="submitActionAndView('PDCDocumentDisplayViewCommand', '${list.location}', 'Preview');">View</button></td>
                                <td><button type="button" name="" onclick="submitActionAndView('PDCDocumentDisplayViewCommand', '${list.location}', 'Send')">Send Email</button></td>
                            </tr>
                        </c:forEach>
                    </c:when>                       
                    <c:otherwise>
                        <span class="label" style="color: #FF0000; width: 100px;">No Document Found!</span>
                    </c:otherwise>
                </c:choose>
            </table>
        </div>

    </div>
</td>


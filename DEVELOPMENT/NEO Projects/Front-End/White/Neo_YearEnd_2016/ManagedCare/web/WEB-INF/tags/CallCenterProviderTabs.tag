<%-- 
    Document   : CallCenterProviderTabs
    Created on : 2013/01/22, 06:15:04
    Author     : johanl
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="cctSelectedIndex"%>
<%@attribute name="isClearingUser"%>

<%-- any content can be specified here e.g.: --%>
<div id="header" class="header_area">
    <ol id="tabs">

        <li><a id="link_prov_PD" onClick="submitWithAction('ViewProviderDetailsCommand');" href="#Practice Details" >Practice Details</a></li>
        <li><a id="link_prov_AD" onClick="submitWithAction('ViewPracticeAddressDetailsCommand');" href="#AddressDetail">Address Details</a></li>
        <li><a id="link_prov_BD" onClick="submitWithAction('ViewPracticeBankingDetailsCommand');"href="#BankingDetail">Banking Details</a></li>
        <li><a id="link_prov_CD" onClick="submitWithAction('ForwardToSearchPracticeClaimsCommand');" href="#Claims Details">Claims Details</a></li>
        <li><a id="link_prov_Pay" onClick="submitWithAction('ViewPracticePaymentDetailsCommand');" href="#Payment Details">Payment Details</a></li>
        <li><a id="link_prov_SD" onClick="submitWithAction('ForwardToProviderStatementCommand');" href="#Statements">Statements</a></li>
        ${isClearingUser == false ? "<li><a id=\"link_prov_Auth\" onClick=\"submitWithAction('ViewPracticeAuthorisationCommand');\" href=\"#Authorisation\">Authorisation</a></li>" : ''}
        <li><a id="link_prov_Comm" onClick="submitWithAction('ViewProviderCommunicationCommand');" href="#ProviderCommunication">Communications</a></li>
        <li><a id="link_prov_Docs" onClick="submitWithAction('ViewProviderDocumentsCommand');" href="#ProviderDocuments">Documents</a></li>
        
    </ol>
</div>
<script>setSelectedCCTab('${cctSelectedIndex}');</script>
<br/>
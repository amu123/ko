<%@tag description="Tag tutorial Amukelani" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="items" required="false" type="java.util.Collection"%>
<%@attribute name="showHeader" required="false" type="java.lang.Boolean"%>


<%-- any content can be specified here e.g.: --%>
<h2>${message}</h2>

<c:if test="${!empty sessionScope.items}">
    <c:if test="${showHeader != false}">
        <label class="subheader">Search Amukelanim</label>
    </c:if>
        <table class="list" style="border-style: none; border-collapse: collapse; border-width: 1px">
            <tr>
                <th>Source Description</th>
                <th>Product Description</th>
                <th>Queue Description</th>
                <th>Status Description</th>
                <th>Username</th>
                <th>Subject</th>
                <th>Entity Name</th>
            </tr>
            
            <c:forEach var="am" items="${sessionScope.items}">
                <tr>
                    <td><label>${am.sourceDescription}</label></td>
                    <td><label>${am.productDescription}</label></td>
                    <td><label>${am.queueDescription}</label></td>
                    <td><label>${am.statuseDescription}</label></td>
                    <td><label>${am.username}</label></td>
                    <td><label>${am.subject}</label></td>
                    <td><label>${am.entityName}</label></td>
                </tr>
            </c:forEach>
        </table>
</c:if>
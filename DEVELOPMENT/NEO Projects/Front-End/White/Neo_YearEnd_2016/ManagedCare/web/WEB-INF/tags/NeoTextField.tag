<%@tag dynamic-attributes="dynAttr" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<%@attribute name="elementName" required="true" %>           
<%@attribute name="displayName" required="false" %>           
<%@attribute name="mandatory" required="false" %>
<%@attribute name="type" required="false"%>
<%@attribute name="value" required="false" %>
<%@attribute name="errorValue" required="false" %>
<%@attribute name="searchFunc" required="false" %>
<%@attribute name="lookupId" required="false" %>
<%@attribute name="currency" required="false" %>

<c:set var="errorElemName" value="${elementName}_error" />
<c:set var="type" value="${(empty type) ? 'text' : (fn:trim(type))}" />
<c:set var="value" value="${(empty value) ? (empty requestScope[elementName] ? sessionScope[elementName] : requestScope[elementName]) : value}" />
<c:set var="errorValue" value="${(empty errorValue) ? (empty requestScope[errorElemName] ? sessionScope[errorElemName] : requestScope[errorElemName]) : errorValue}" />
<c:set var="mandatory" value="${empty mandatory ? 'no' : (fn:toLowerCase(mandatory))}" />

<c:set var="onClick" value="" />
<c:set var="icon" value="" />
<c:if test="${type eq 'date'}">
    <c:set var="icon" value="/ManagedCare/resources/Calendar.gif" />
    <c:set var="onClick" value="displayDatePicker('${elementName}', this);" />
    <c:set var="type" value="text" />
</c:if>
<c:if test="${type eq 'search'}">
    <c:set var="icon" value="/ManagedCare/resources/Search.gif" />
    <c:if test="${empty searchFunc}">
        <c:set var="onClick" value="searchInputField('${elementName}', this);" />
    </c:if>
    <c:if test="${not empty searchFunc}">
        <c:set var="onClick" value="${searchFunc}" />
    </c:if>
    <c:set var="type" value="text" />
</c:if>

<td align="left" width="160px"><label>${displayName}</label></td>
<td align="left" width="200px">
    <c:choose>
        <c:when test="${not empty lookupId}">
            <c:set var="lookups" value="${agiletags:retrieveLookupValues(lookupId)}" />
            <select id="${elementName}" name="${elementName}" on
                <c:forEach var="item" items="${dynAttr}">
                    ${item.key}='${item.value}' 
                </c:forEach>
            > 
            <c:forEach var="ax" items="${lookups}">
                <option value="${ax.id}" ${ax.id eq value ? ' selected ' : ''} >${ax.value}</option>
            </c:forEach>
            </select>
        </c:when>
        <c:when test="${not empty currency}">
            <c:set var="lookups" value="${agiletags:retrieveLookupValues(currency)}" />
            <select id="${elementName}" name="${elementName}" on
                <c:forEach var="item" items="${dynAttr}">
                    ${item.key}='${item.value}' 
                </c:forEach>
            > 
            <c:forEach var="ax" items="${lookups}">
                <option value="${ax.id}" ${ax.id eq 1 ? ' selected ' : ''} >${ax.code}</option>
            </c:forEach>
            </select>
        </c:when>
        <c:otherwise>
            <input type="${type}" id="${elementName}" name="${elementName}" value="${value}" size="30"
                <c:forEach var="item" items="${dynAttr}">
                    ${item.key}='${item.value}' 
                </c:forEach>
            >
            
        </c:otherwise>
    </c:choose>
    
</td>
<td><label class="${mandatory eq 'true' || mandatory eq 'yes' ? 'red' : ''}">${mandatory eq 'true' || mandatory eq 'yes' ? '*' : ''}</label></td>
<td>
    <c:if test="${not empty icon}">
        <img src="${icon}" height="28" width="28" alt="Calendar" onclick="${onClick}" />
    </c:if>
</td>
<td><label class="error" id="${errorElemName}">${errorValue}</label></td>

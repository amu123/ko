<%-- 
    Document   : PDCCarepathSchedule
    Created on : May 27, 2016, 2:51:37 PM
    Author     : nick
--%>

<%@tag description="Display All Tasks Per Disease In Day-Planner Format" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ attribute name="javascript" rtexprvalue="true" required="false" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
    <tr>
        <th style="text-align: left">
            <c:if test="${sessionScope.pdcCurrentSelectedYearID != 0}">
                <c:choose>
                    <c:when test="${applicationScope.Client == 'Sechaba'}">
                        <a href="#" class="btnGreen" 
                            onclick="submitYearChangeWithAction('SwitchCarePathYearCommand', '-');">
                            <<</a>
                    </c:when>
                    <c:otherwise>
                        <a href="#" class="btn" 
                            onclick="submitYearChangeWithAction('SwitchCarePathYearCommand', '-');">
                            <<</a>
                    </c:otherwise>
                </c:choose>
            </c:if>
        </th>
        <th style="text-align: left" colspan="20">
            <c:choose>
                <c:when test="${sessionScope.pdcCurrentSelectedYearID == 1}">
                    ${sessionScope.pdcUsableYears[0]}
                </c:when>
                <c:when test="${sessionScope.pdcCurrentSelectedYearID == 2}">
                    ${sessionScope.pdcUsableYears[1]}
                </c:when>
            </c:choose>
        </th>
        <th style="text-align: center" colspan="20">${sessionScope.pdcCurrentSelectedYear}</th>
        <th style="text-align: right" colspan="20">
            <c:choose>
                <c:when test="${sessionScope.pdcCurrentSelectedYearID == 0}">
                    ${sessionScope.pdcUsableYears[1]}
                </c:when>
                <c:when test="${sessionScope.pdcCurrentSelectedYearID == 1}">
                    ${sessionScope.pdcUsableYears[2]}
                </c:when>
            </c:choose>
        </th>
        <th style="text-align: right">
            <c:if test="${sessionScope.pdcCurrentSelectedYearID != 2}">
                <c:choose>
                    <c:when test="${applicationScope.Client == 'Sechaba'}">
                        <a href="#" class="btnGreen" 
                            onclick="submitYearChangeWithAction('SwitchCarePathYearCommand', '+');">
                            >></a>
                    </c:when>
                    <c:otherwise>
                        <a href="#" class="btn" 
                            onclick="submitYearChangeWithAction('SwitchCarePathYearCommand', '+');">
                            >></a>
                    </c:otherwise>
                </c:choose>
            </c:if>
        </th>
    </tr>
    <tr>
        <th></th>
            <c:forEach var="month" items="${sessionScope.pdcAllMonths}">
            <th colspan="5">${month.value}</th>
            </c:forEach>
        <th></th>
    </tr>
    <tr>
        <th></th>
            <c:forEach var="month" items="${sessionScope.pdcAllMonths}">
                <c:forEach var="week" items="${sessionScope.pdcUsableWeeks}">
                <th>W${week}</th>
                </c:forEach>
            </c:forEach>
        <th></th>
    </tr>
    <tr>
        <th></th>
            <c:forEach var="m" items="${sessionScope.pdcAllMonths}">
                <c:forEach var="w" items="${sessionScope.pdcUsableWeeks}">
                <td>
                    <table>
                        <c:forEach var="entry" items="${sessionScope.depCarePathTaskAssignedMap}">
                            <c:forEach var="task" items="${entry.value}">
                                <c:if test="${task.taskYear == sessionScope.pdcCurrentSelectedYear}">
                                    <c:if test="${task.taskMonth == m.id}">
                                        <c:if test="${task.taskWeek == w}">
                                            <tr>
                                                <c:choose>
                                                    <c:when test="${task.closedDate != null}">
                                                        <td style="background-color: #999999;">
                                                            <div class="profile">
                                                                <!-- HIDDEN / POP-UP DIV -->
                                                                <div class="div pop-up" style="background-color: #999999; color: black">
                                                                    <h3>Task Details(Closed):</h3>
                                                                    <p>
                                                                        Condition: ${entry.key.value} <br/>
                                                                        Task Type: ${task.taskType.value}
                                                                    </p>
                                                                    <p>
                                                                        Description: ${task.taskDesc}
                                                                    </p>
                                                                </div>
                                                                <a title="Task${task.taskId}" class="trigger" id="Task${task.taskId}" onmouseover="setHoveringTaskID(${task.taskId})"><span>X</span></a>
                                                            </div>
                                                        </td>
                                                    </c:when>
                                                    <c:when test="${task.taskType.id == 1 && (task.closedDate == null)}">
                                                        <td style="background-color: #8ed9f6;">
                                                            <div class="profile">
                                                                <!-- HIDDEN / POP-UP DIV -->
                                                                <div class="div pop-up" style="background-color: #8ed9f6; color: black">
                                                                    <h3>Task Details:</h3>
                                                                    <p>
                                                                        Condition: ${entry.key.value} <br/>
                                                                        Task Type: ${task.taskType.value}
                                                                    </p>
                                                                    <p>
                                                                        Description: ${task.taskDesc}
                                                                    </p>
                                                                </div>
                                                                        <a href="#modal-decide" title="Task${task.taskId}" class="trigger" id="Task${task.taskId}" onmouseover="setHoveringTaskID(${task.taskId})" onclick="setSelectedTaskIdOnForm(${task.taskId});"><span>X</span></a>
                                                            </div>
                                                        </td>
                                                    </c:when>
                                                    <c:when test="${task.taskType.id == 2 && (task.closedDate == null)}">
                                                        <td style="background-color: lightgreen">
                                                            <div class="profile">
                                                                <!-- HIDDEN / POP-UP DIV -->
                                                                <div class="pop-up" style="background-color: lightgreen; color: black">
                                                                    <h3>Task Details:</h3>
                                                                    <p>
                                                                        Condition: ${entry.key.value} <br/>
                                                                        Task Type: ${task.taskType.value}
                                                                    </p>
                                                                    <p>
                                                                        Description: ${task.taskDesc}
                                                                    </p>
                                                                </div>
                                                                <a href="#modal-decide" title="Task${task.taskId}" class="trigger" id="Task${task.taskId}" onmouseover="setHoveringTaskID(${task.taskId})" onclick="setSelectedTaskIdOnForm(${task.taskId});"><span>X</span></a>
                                                            </div>
                                                        </td>
                                                    </c:when>
                                                    <c:when test="${task.taskType.id == 3 && (task.closedDate == null)}">
                                                        <td style="background-color: #f5aca6">
                                                            <div class="profile">
                                                                <!-- HIDDEN / POP-UP DIV -->
                                                                <div class="pop-up" style="background-color: #f5aca6; color: black">
                                                                    <h3>Task Details:</h3>
                                                                    <p>
                                                                        Condition: ${entry.key.value} <br/>
                                                                        Task Type: ${task.taskType.value}
                                                                    </p>
                                                                    <p>
                                                                        Description: ${task.taskDesc}
                                                                    </p>
                                                                </div>
                                                                <a href="#modal-decide" title="Task${task.taskId}" class="trigger" id="Task${task.taskId}" onmouseover="setHoveringTaskID(${task.taskId})" onclick="setSelectedTaskIdOnForm(${task.taskId});"><span>X</span></a>
                                                            </div>
                                                        </td>
                                                    </c:when>
                                                    <c:when test="${task.taskType.id == 4 && (task.closedDate == null)}">
                                                        <td style="background-color: steelblue">
                                                            <div class="profile">
                                                                <!-- HIDDEN / POP-UP DIV -->
                                                                <div class="pop-up" style="background-color: steelblue; color: black">
                                                                    <h3>Task Details:</h3>
                                                                    <p>
                                                                        Condition: ${entry.key.value} <br/>
                                                                        Task Type: ${task.taskType.value}
                                                                    </p>
                                                                    <p>
                                                                        Description: ${task.taskDesc}
                                                                    </p>
                                                                </div>
                                                                <a href="#modal-decide" title="Task${task.taskId}" class="trigger" id="Task${task.taskId}" onmouseover="setHoveringTaskID(${task.taskId})" onclick="setSelectedTaskIdOnForm(${task.taskId});"><span>X</span></a>
                                                            </div>
                                                        </td>
                                                    </c:when>
                                                    <c:when test="${task.taskType.id == 5 && (task.closedDate == null)}">
                                                        <td style="background-color: hotpink">
                                                            <div class="profile">
                                                                <!-- HIDDEN / POP-UP DIV -->
                                                                <div class="pop-up" style="background-color: hotpink; color: black">
                                                                    <h3>Task Details:</h3>
                                                                    <p>
                                                                        Condition: ${entry.key.value} <br/>
                                                                        Task Type: ${task.taskType.value}
                                                                    </p>
                                                                    <p>
                                                                        Description: ${task.taskDesc}
                                                                    </p>
                                                                </div>
                                                                <a href="#modal-decide" title="Task${task.taskId}" class="trigger" id="Task${task.taskId}" onmouseover="setHoveringTaskID(${task.taskId})" onclick="setSelectedTaskIdOnForm(${task.taskId});"><span>X</span></a>
                                                            </div>
                                                        </td>
                                                    </c:when>
                                                    <c:when test="${task.taskType.id == 6 && (task.closedDate == null)}">
                                                        <td style="background-color: #f2c779">
                                                            <div class="profile">
                                                                <!-- HIDDEN / POP-UP DIV -->
                                                                <div class="pop-up" style="background-color: #f2c779; color: black">
                                                                    <h3>Task Details:</h3>
                                                                    <p>
                                                                        Condition: ${entry.key.value} <br/>
                                                                        Task Type: ${task.taskType.value}
                                                                    </p>
                                                                    <p>
                                                                        Description: ${task.taskDesc}
                                                                    </p>
                                                                </div>
                                                                <a href="#modal-decide" title="Task${task.taskId}" class="trigger" id="Task${task.taskId}" onmouseover="setHoveringTaskID(${task.taskId})" onclick="setSelectedTaskIdOnForm(${task.taskId});"><span>X</span></a>
                                                            </div>
                                                        </td>
                                                              
                                                    </c:when>
                                                </c:choose>
                                                <div class="div clear"></div>        
                                            </tr>
                                        </c:if>
                                    </c:if>
                                </c:if>
                            </c:forEach>
                        </c:forEach>
                    </table>
                </td>
            </c:forEach>
        </c:forEach>
        <th></th>
    </tr>                            
</table>
<br/>
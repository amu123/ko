<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@attribute name="items" required="true" type="java.util.Collection" %>
<%@attribute name="showHeader" required="false" type="java.lang.Boolean" %>
<%@attribute name="showMethod" required="false" type="java.lang.Boolean" %>
<%@attribute name="showType" required="false" type="java.lang.Boolean" %>


<c:if test="${!empty items}">
    <c:if test="${showHeader != false}">
        <label class="subheader">Communications Log</label>
    </c:if>
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th>Date</th>
            <c:if test="${showType != false}">
                <th>Type</th>
            </c:if>
            <c:if test="${showMethod != false}">
                <th>Communication Method</th>
            </c:if>
            <th>Contact Details</th>
            <th>Status</th>
        </tr>
        <c:forEach var="rec" items="${items}">
            <tr>
                <td><label>${agiletags:formatXMLGregorianDate(rec.attemptDate)}</label></td>
                <c:if test="${showType != false}">
                    <td><label>${rec.logTypeDescription}</label></td>
                </c:if>
                <c:if test="${showMethod != false}">
                    <td><label>${rec.communicationTypeDescription}</label></td>
                </c:if>
                <td><label>${rec.contactDetails}</label></td>
                <td><label>${rec.status}</label></td>
            </tr>
        </c:forEach>
    </table>
    <br>
</c:if>
<c:if test="${empty items}">
    <label>No details available</label>
</c:if>    

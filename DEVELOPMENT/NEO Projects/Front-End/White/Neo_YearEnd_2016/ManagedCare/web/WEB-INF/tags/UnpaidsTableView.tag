<%-- 
    Document   : UnpaidsTableView
    Created on : 29 Apr 2015, 12:58:40 PM
    Author     : dewaldo
--%>

<%@tag description="UnpaidsTableView"  dynamic-attributes="dynattrs" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<c:if test="${!empty sessionScope.typeUnpaids && sessionScope.typeUnpaids ne ''}">
    <c:if test="${!empty sessionScope.unpaids && sessionScope.unpaids ne ''}">
        <div id="unpaids">
            <label class="header">Resolution Health Unpaid's</label><hr/>
            <table>
                <tr>
                    <td>
                        <div id="change" style="position:absolute;">
                            <button id="actionButton" name="save" onclick="submitWithAction('SaveUnpaidsCommand');">Save Results</button>
                        </div>
                    </td>
                </tr>
            </table>
            <br/><br/>
            <table class="list" style="position:absolute; border-style:none; border-collapse:collapse; border-width:1px; width:100%;">
                <tr>
                    <th></th>
                    <th>File ID</th>
                    <th>Date</th>
                    <th>Sequence Number</th>
                    <th>Status</th>
                    <th>Rejection Reason</th>
                </tr>
                <c:forEach var="entry" items="${sessionScope.unpaids}"> 
                    <c:set var="count" value="${count + 1}"/>
                    <fmt:parseNumber var="id" integerOnly="true" type="number" value="${entry.fileID}" />
                    <fmt:parseNumber var="sequence" integerOnly="true" type="number" value="${entry.sequenceNumber}"/>
                    <fmt:parseDate value="${entry.date}" var="parsedEmpDate" pattern="yyyyMMdd" />
                    <fmt:formatDate var="dateToUse" pattern="yyyy/MM/dd" value="${parsedEmpDate}"/>
                    <tr>
                        <td><label><c:out value="${count}"/></label></td>
                        <td><label><c:out value="${fn:trim(id)}"/></label></td>
                        <td><label><c:out value="${fn:trim(dateToUse)}"/></label></td>
                        <td><label><c:out value="${fn:trim(sequence)}"/></label></td>
                        <td><label>${fn:trim(entry.status)}</label></td>
                        <td><label>${fn:trim(entry.rejectionReason)}</label></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <br/><br/>
    </c:if>

    <c:if test="${empty sessionScope.unpaids && sessionScope.unpaids eq ''}">
        <div id="unpaids">
            <label class="header">No Resolution Health Unpaid's found...</label>
            <hr/><hr/>
        </div>
        <br/><br/>
    </c:if>

    <!-- Not needed
    <c:if test="${!empty sessionScope.warnings && sessionScope.warnings ne ''}">
        <div id="unpaids">
            <label class="header">Resolution Health Unpaid's With Warnings</label><hr/>
            <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px; width:100%;">
                <tr>
                    <th></th>
                    <th>File ID</th>
                    <th>Date</th>
                    <th>Sequence Number</th>
                    <th>Status</th>
                    <th>Rejection Reason</th>
                </tr>
        <c:set var="count" value="0" scope="page"/>
        <c:forEach var="entry" items="${sessionScope.warnings}">  
            <c:set var="count" value="${count + 1}"/>
            <fmt:parseNumber var="id" integerOnly="true" type="number" value="${entry.fileID}" />
            <fmt:parseNumber var="sequence" integerOnly="true" type="number" value="${entry.sequenceNumber}"/>
            <fmt:parseDate value="${entry.date}" var="parsedEmpDate" pattern="yyyyMMdd" />
            <fmt:formatDate var="dateToUse" pattern="yyyy/MM/dd" value="${parsedEmpDate}"/>
            <tr>
                <td><label><c:out value="${count}"/></label></td>
                <td><label><c:out value="${fn:trim(id)}"/></label></td>
                <td><label><c:out value="${fn:trim(dateToUse)}"/></label></td>
                <td><label><c:out value="${fn:trim(sequence)}"/></label></td>
                <td><label>${fn:trim(entry.status)}</label></td>
                <td><label>${fn:trim(entry.rejectionReason)}</label></td>
            </tr>
        </c:forEach>
    </table>
</div>
<br/><br/>
    </c:if>

    <c:if test="${empty sessionScope.warnings && sessionScope.warnings eq ''}">
        <div id="rejected">
            <label class="header">No Resolution Health Unpaid's with warnings found...</label>
            <hr/><hr/>
        </div>
        <br/><br/>
    </c:if>
    -->

</c:if>

<c:if test="${!empty sessionScope.typeAccepted && sessionScope.typeAccepted ne ''}">
    <c:if test="${!empty sessionScope.rejected && sessionScope.rejected ne ''}">
        <div id="rejected">
            <label class="header">Resolution Health Rejected's</label><hr/>
            <table>
                <tr>
                    <td>
                        <div id="change" style="position:absolute;">
                            <button id="actionButton" name="save" onclick="submitWithAction('SaveUnpaidsCommand');">Save Results</button>
                        </div>
                    </td>
                </tr>
            </table>
            <br/><br/>
            <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px; width:100%;">
                <tr>
                    <th></th>
                    <th>File ID</th>
                    <th>Date</th>
                    <th>Sequence Number</th>
                    <th>Status</th>
                    <th>Rejection Reason</th>
                </tr>
                <c:forEach var="entry" items="${sessionScope.rejected}">  
                    <c:set var="count" value="${count + 1}"/>
                    <fmt:parseNumber var="id" integerOnly="true" type="number" value="${entry.fileID}" />
                    <fmt:parseNumber var="sequence" integerOnly="true" type="number" value="${entry.sequenceNumber}"/>
                    <fmt:parseDate value="${entry.date}" var="parsedEmpDate" pattern="yyyyMMdd" />
                    <fmt:formatDate var="dateToUse" pattern="yyyy/MM/dd" value="${parsedEmpDate}"/>
                    <tr>
                        <td><label><c:out value="${count}"/></label></td>
                        <td><label><c:out value="${fn:trim(id)}"/></label></td>
                        <td><label><c:out value="${fn:trim(dateToUse)}"/></label></td>
                        <td><label><c:out value="${fn:trim(sequence)}"/></label></td>
                        <td><label>${fn:trim(entry.status)}</label></td>
                        <td><label>${fn:trim(entry.rejectionReason)}</label></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <br/><br/>
    </c:if>

    <c:if test="${empty sessionScope.rejected && sessionScope.rejected eq ''}">
        <div id="rejected">
            <label class="header">No Resolution Health Rejected's found...</label>
            <hr/><hr/>
        </div>
        <br/><br/>
    </c:if>

    <!--No need for this at the moment, we only care about the rejected
    <c:if test="${!empty sessionScope.accepted && sessionScope.accepted ne ''}">
        <div id="accepted">
            <label class="labelTextDisplay">Resolution Health Accepted's</label><hr/>
            <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px; width:100%;">
                <tr>
                    <th></th>
                    <th>File ID</th>
                    <th>Date</th>
                    <th>Sequence Number</th>
                    <th>Status</th>
                </tr>
        <c:forEach var="entry" items="${sessionScope.accepted}">  
            <c:set var="count" value="${count + 1}"/>
            <fmt:parseNumber var="id" integerOnly="true" type="number" value="${entry.fileID}" />
            <fmt:parseNumber var="sequence" integerOnly="true" type="number" value="${entry.sequenceNumber}"/>
            <fmt:parseDate value="${entry.date}" var="parsedEmpDate" pattern="yyyyMMdd" />
            <fmt:formatDate var="dateToUse" pattern="yyyy/MM/dd" value="${parsedEmpDate}"/>
            <tr>
                <td><label><c:out value="${count}"/></label></td>
                <td><label><c:out value="${fn:trim(id)}"/></label></td>
                <td><label><c:out value="${fn:trim(dateToUse)}"/></label></td>
                <td><label><c:out value="${fn:trim(sequence)}"/></label></td>
                <td><label>${fn:trim(entry.status)}</label></td>
                <td><label>${fn:trim(entry.rejectionReason)}</label></td>
            </tr>
        </c:forEach>
    </table>
</div> 
<br/><br/>
    </c:if>
    
    <c:if test="${empty sessionScope.accepted && sessionScope.accepted eq ''}">
    <div id="accepted">
        <label class="header">No Resolution Health Accepted's found...</label>
        <hr/><hr/>
    </div>
    <br/><br/>
    </c:if>
    -->

</c:if>

<c:if test="${!empty sessionScope.typeNormal && sessionScope.typeNormal ne ''}">
    <c:if test="${!empty sessionScope.Normal && sessionScope.Normal ne ''}">
        <div id="Normal">
            <label class="header">Spectramed Normal Unpaid's</label><hr/>
            <table>
                <tr>
                    <td>
                        <div id="change" style="position:absolute;">
                            <button id="actionButton" name="save" onclick="submitWithAction('SaveUnpaidsCommand');">Save Results</button>
                        </div>
                    </td>
                </tr>
            </table>
            <br/><br/>
            <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px; width:100%;">
                <tr>
                    <th></th>
                    <th>Reference</th>
                    <th>Date</th>
                    <th>Sequence Number</th>
                    <th>Rejection Reason</th>
                </tr>
                <c:forEach var="entry" items="${sessionScope.Normal}">
                    <c:set var="count" value="${count + 1}"/>
                    <fmt:parseNumber var="sequence" integerOnly="true" type="number" value="${entry.sequenceNumber}"/>
                    <fmt:parseDate value="${entry.date}" var="parsedEmpDate" pattern="yyyyMMdd" />
                    <fmt:formatDate var="dateToUse" pattern="yyyy/MM/dd" value="${parsedEmpDate}"/>
                    <tr>
                        <td><label><c:out value="${count}"/></label></td>
                        <td><label>${fn:trim(entry.referenceType)}</label></td>
                        <td><label><c:out value="${fn:trim(dateToUse)}"/></label></td>
                        <td><label><c:out value="${fn:trim(sequence)}"/></label></td>
                        <td><label>${fn:trim(entry.rejectionReason)}</label></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <br/><br/>
    </c:if>

    <c:if test="${empty sessionScope.Normal && sessionScope.Normal eq ''}">
        <div id="Normal">
            <label class="header">No Spectramed Normal Unpaid's found...</label>
            <hr/><hr/>
        </div>
        <br/><br/>
    </c:if>

</c:if>


<c:if test="${!empty sessionScope.typeVetReport && sessionScope.typeVetReport ne ''}">
    <c:if test="${!empty sessionScope.VetReport && sessionScope.VetReport ne ''}">
        <div id="VET">
            <label class="header">Spectramed VET Report Unpaid's</label><hr/>
            <table>
                <tr>
                    <td>
                        <div id="change" style="position:absolute;">
                            <button id="actionButton" name="save" onclick="submitWithAction('SaveUnpaidsCommand');">Save Results</button>
                        </div>
                    </td>
                </tr>
            </table>
            <br/><br/>
            <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px; width:100%;">
                <tr>
                    <th></th>
                    <th>Reference</th>
                    <th>Entity Details</th>
                    <th>Date</th>
                    <th>Rejection Reason</th>
                </tr>
                <c:forEach var="entry" items="${sessionScope.VetReport}" varStatus="Counter">
                    <c:set var="count" value="${count + 1}"/>
                    <fmt:parseNumber var="sequence" integerOnly="true" type="number" value="${entry.sequenceNumber}"/>
                    <fmt:parseDate value="${entry.date}" var="parsedEmpDate" pattern="yyyyMMdd" />
                    <fmt:formatDate var="dateToUse" pattern="yyyy/MM/dd" value="${parsedEmpDate}"/>
                    <tr>
                        <td><label><c:out value="${count}"/></label></td>
                        <td><label>${fn:trim(entry.referenceType)}</label></td>
                        <td><label>${fn:trim(entry.entityDetails)}</label></td>
                        <td><label><c:out value="${fn:trim(dateToUse)}"/></label></td>
                        <td><label>${fn:trim(entry.rejectionReason)}</label></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <br/><br/>
    </c:if>

    <c:if test="${empty sessionScope.VetReport && sessionScope.VetReport eq ''}">
        <div id="VET">
            <label class="header">No Spectramed VET Report Unpaid's found...</label>
            <hr/><hr/>
        </div>
        <br/><br/>
    </c:if>
</c:if>


<%-- 
    Document   : BiometricTableTag
    Created on : 26 Feb 2015, 12:21:55 PM
    Author     : dewaldvdb
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<%@ attribute name="headerType" rtexprvalue="true" required="false" %>
<%@ attribute name="showEditBtn" rtexprvalue="true" required="false" %>
<%@ attribute name="javaScript" rtexprvalue="true" required="false" %>

<%-- any content can be specified here e.g.: --%>
<c:if test="${empty headerType}">
    <c:set var="headerType" value="header"/>
</c:if>
<c:if test="${empty showEditBtn}">
    <c:set var="showEditBtn" value="false"/>
</c:if>
    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:0px;">
        <label class="${headerType}">View Info for Biometric</label><br/><br/>
            <tr>
                <th class="label" align="left" WIDTH=21%><b>Member Number:</b></th>
                <td class="label">${sessionScope.ViewSpecificBiometric.memberNumber}</td>
                <th class="label" align="left" WIDTH=21%><b>Cover Dependants:</b></th>
                <td class="label">${sessionScope.ViewSpecificBiometric.dependantCode}</td>
            </tr>
            <tr>
                <th class="label" align="left"><b>Date Measured:</b></th>
                <td class="label">${agiletags:formatXMLGregorianDate(sessionScope.ViewSpecificBiometric.dateMeasured)}</td>
                <th class="label" align="left"><b>Date Received:</b></th>
                <td class="label">${fn:replace(agiletags:formatXMLGregorianDate(sessionScope.ViewSpecificBiometric.dateReceived),'2999/12/31','')}</td>
            </tr>
            <tr>
                <th class="label" align="left"><b>Blood Pressure Systolic(mmHg):</b></th>
                <td class="label">${sessionScope.ViewSpecificBiometric.bloodPressureSystolic}</td>
                <th class="label" align="left"><b>Blood Pressure Diastolic(mmHg):</b></th>
                <td class="label">${sessionScope.ViewSpecificBiometric.bloodPressureDiastolic}</td>
            </tr>
            <tr>
                <th class="label" align="left"><b>Weight (KG):</b></th>
                <td class="label">${sessionScope.ViewSpecificBiometric.weight}</td>
                <th class="label" align="left"><b>Height (M):</b></th>
                <td class="label">${sessionScope.ViewSpecificBiometric.height}</td>
            </tr>
            <tr>
                <th class="label" align="left"><b>BMI:</b></th>
                <td class="label">${sessionScope.ViewSpecificBiometric.bmi}</td>
                <th class="label" align="left"><b>Alcohol Units Per Week?:</b></th>
                <td class="label">${sessionScope.ViewSpecificBiometric.alcoholUnitsPerWeek}</td>
            </tr>
            <c:if test="${sessionScope.ViewSpecificBiometric.currentSmoker == 'true' || sessionScope.ViewSpecificBiometric.currentSmoker == '1'}">
            <tr>
                <th class="label" align="left"><b>Current Smoker:</b></th>
                <td class="label">Yes</td>
                <th class="label" align="left"><b>Smoker Cigarettes Per Day:</b></th>
                <td class="label">${sessionScope.ViewSpecificBiometric.cigarettesPerDay}</td>
                <!--<th align="left"></th>-->
                <!--<td></td>-->
            </tr>
            </c:if>
            <c:if test="${sessionScope.ViewSpecificBiometric.exSmoker == 'true' || sessionScope.ViewSpecificBiometric.exSmoker == '1'}">
            <tr>
                <th class="label" align="left"><b>Ex Smoker</b></th>
                <td class="label">Yes</td>
                <th class="label" align="left"><b>Years Since Stopped:</b></th>
                <td class="label">${sessionScope.ViewSpecificBiometric.yearsSinceStopped}</td>
            </tr>
            </c:if>
            <tr>
                <th class="label" align="left"><b>Exercise Per Week:</b></th>
                <td class="label">${sessionScope.ViewSpecificBiometric.exercisePerWeek}</td>
                <th class="label" align="left"><b>Treating Provider:</b></th>
                <td class="label">${sessionScope.ViewSpecificBiometric.treatingProvider}</td>
            </tr>
            <tr>
                <th class="label" align="left"><b>ICD10:</b></th>
                <td class="label">${sessionScope.icd10_text}</td>
                <th class="label" align="left"><b>Source:</b></th>
                    <c:choose>
                        <c:when test="${sessionScope.ViewSpecificBiometric.source == 1}">
                        <td class="label">Internal</td>
                    </c:when>
                    <c:when test="${sessionScope.ViewSpecificBiometric.source == 2}">
                        <td class="label">External</td>
                    </c:when>
                    <c:otherwise>
                        <td class="label"></td>
                    </c:otherwise>
                </c:choose>
            </tr>
            <tr></tr>
    </table>
        <c:if test="${showEditBtn eq 'true'}">
        <button name="opperation" type="button" ${javaScript} value="">Edit</button>
        </c:if>
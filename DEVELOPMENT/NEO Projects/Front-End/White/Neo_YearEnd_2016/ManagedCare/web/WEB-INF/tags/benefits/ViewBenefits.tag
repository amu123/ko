<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@attribute name="items" required="true" type="java.util.Collection" %>
<%@attribute name="selectCommand" required="true" type="java.lang.String" %>
<%@attribute name="showAll" required="false" type="java.lang.Boolean" %>

<table id="benefitsTable" class="list" style="border-collapse:collapse; border-width:1px;" width="100%">
    <thead>
        <tr>
            <th rowspan="2" style="border-right-color: #FFFFFF; ">Limit</th>
            <th rowspan="2" style="border-right-color: #FFFFFF; ">Sub-Limit</th>
            <th rowspan="2" style="border-right-color: #FFFFFF; ">Sub-Sub-Limit</th>
            <th colspan='3' style='border-right-color: #FFFFFF; border-bottom-color:#FFFFFF;'>Amount</th>
            <th rowspan="2" style="border-right-color: #FFFFFF; ">Cycle</th>
            <th colspan='3' style='border-right-color: #FFFFFF; border-bottom-color:#FFFFFF;'>Count</th>
            <th rowspan="2" style="border-right-color: #FFFFFF; ">Action</th>
        </tr>
        <tr>
            <th style='border-right-color: #FFFFFF;'>Limit</th>
            <th style='border-right-color: #FFFFFF;'>Used</th>
            <th style='border-right-color: #FFFFFF;'>Available</th>
            <th style='border-right-color: #FFFFFF;'>Limit</th>
            <th style='border-right-color: #FFFFFF;'>Used</th>
            <th style='border-right-color: #FFFFFF;'>Available</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="entry" items="${items}">
            <c:if test="${entry.display == 1 || showAll == true}">
                <c:set var="lblClass" value="${entry.familyLimit == 1 ? 'noteLabel' : 'label'}"/>
            <tr>
                <td><label class="${lblClass}" title="${entry.benefitDescription}">${entry.level == 1 ? entry.benefitComment : ""}</label></td>
                <td><label class="${lblClass}" title="${entry.benefitDescription}">${entry.level == 2 ? entry.benefitComment : ""}</label></td>
                <td><label class="${lblClass}" title="${entry.benefitDescription}">${entry.level == 3 ? entry.benefitComment : ""}</label></td>
                <td align="right">
                    <label class="${lblClass}">
                        ${agiletags:formatBigDecimal(entry.limitAmount) eq '9999999.99'? (entry.limitCount > 0 ? 'Not Applicable': 'Unlimited') : agiletags:formatBigDecimal(entry.limitAmount)}
                    </label>
                </td>
                <td align="right"><label class="${lblClass}">${agiletags:formatBigDecimal(entry.usedAmount)}</label></td>
                <td align="right">
                    <label class="${lblClass}">
                        ${agiletags:formatBigDecimal(entry.limitAmount) eq '9999999.99' ? (entry.limitCount > 0 ? 'Not Applicable': 'Unlimited') : agiletags:formatBigDecimal(entry.availAmount)}
                    </label>
                </td>
                <td><label class="${lblClass}" >${entry.cycle}</label></td>
                <c:if test="${agiletags:formatBigDecimal(entry.limitCount) eq '0.00'}">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </c:if>
                <c:if test="${agiletags:formatBigDecimal(entry.limitCount) ne '0.00'}">
                    <td align="right"><label class="${lblClass}">${entry.limitCount}</label></td>
                    <td align="right"><label class="${lblClass}">${entry.usedCount}</label></td>
                    <td align="right"><label class="${lblClass}">${entry.availCount}</label></td>
                </c:if>
                <td>
                <c:choose>
                    <c:when test="${selectCommand == 'SetBenefitToAuth'}">
                        <c:if test="${entry.availAmount > 0 || entry.availCount > 0}">
                            <button type="button" onclick="submitWithActionSave('${selectCommand}','${entry.benefitCode}', '${entry.benefitDescription}');">Select</button>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${entry.usedAmount > 0 || entry.usedCount > 0}">
                            <button type="button" onclick="submitBDDAction('${selectCommand}','${entry.benefitId}','memberBenefits','${memBenDepCode == null ? -1 : memBenDepCode}','${entry.benefitType == 3 ? "true" : "false"}');">Select</button>
                        </c:if>
                    </c:otherwise>        
                </c:choose>  
                </td>
            </tr>
            </c:if>
        </c:forEach>

    </tbody>
</table>

<c:if test="${!empty memberBenSavingsContribution}">
    <br/>
    <div id="contributionTable" style="display:none">
        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
            <tr>
                <th align="left">Benefit Description</th>
                <th align="left">Limit Amount</th>
                <th align="left">Available Amount</th>
                <th align="left">Used Amount</th>
            </tr>    
            <tr>
                <td><label class="label">Advance Savings</label></td>
                <td><label class="label">${agiletags:formatDouble(memberBenSavingsContribution.advanceSavLimitAmount)}</label></td>
                <td><label class="label">${agiletags:formatDouble(memberBenSavingsContribution.advanceSavLimitAvail)}</label></td>
                <td><label class="label">${agiletags:formatDouble(memberBenSavingsContribution.advanceSavLimitUsed)}</label></td>
            </tr>
            <tr>
                <td><label class="label">Risk Savings</label></td>
                <td><label class="label">${agiletags:formatDouble(memberBenSavingsContribution.riskSavLimitAmount)}</label></td>
                <td><label class="label">${agiletags:formatDouble(memberBenSavingsContribution.riskSavLimitAvail)}</label></td>
                <td><label class="label">${agiletags:formatDouble(memberBenSavingsContribution.riskSavLimitUsed)}</label></td>
            </tr>

            <tr>
                <td><label class="label">Contributed Savings</label></td>
                <td><label class="label">${agiletags:formatDouble(memberBenSavingsContribution.contributionAmount)}</label></td>
                <td><label class="label">${agiletags:formatDouble(memberBenSavingsContribution.contributionAvail)}</label></td>
                <td><label class="label">${agiletags:formatDouble(memberBenSavingsContribution.contributionUsed)}</label></td>
            </tr>

            <tr>
                <td align="right" colspan="4" >
                    <button name="closeContri" type="button" onClick="closeContributionGrid();">Close</button>
                </td>
            </tr>

        </table>
    </div>    
</c:if>

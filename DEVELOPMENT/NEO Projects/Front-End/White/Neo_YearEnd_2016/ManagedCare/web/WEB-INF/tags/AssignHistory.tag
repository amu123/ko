<%-- 
    Document   : AssignHistory
    Created on : 05 Feb 2015, 12:23:46 PM
    Author     : dewaldo
--%>

<%@tag description="AssignHistory"  dynamic-attributes="dynattrs" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>


<c:if test="${!empty sessionScope.assignHistory}">
    <div id="historyTable">
        <label class="header">History</label><hr/>
        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px; width:100%;">
            <tr>
                <th>Assigned To</th>
                <th>Reason for Assignment</th>
                <th>Assignment Details</th>
                <th>Assigned Date</th>
            </tr>
            <c:forEach var="entry" items="${sessionScope.assignHistory}">  
                <tr>
                    <td><label>${entry.assignedTo}</label></td>
                    <td><label>${entry.reasonForAssigned}</label></td>
                    <td><label>${entry.assigmentDetails}</label></td>
                    <td><label>${entry.assignedDate}</label></td>
                </tr>
            </c:forEach>
        </table>
    </div> 
</c:if>

<c:if test="${empty sessionScope.assignHistory}">
    <label id="historyTable" class="header">No History Available</label>
</c:if>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@attribute name="items" required="true" type="java.util.Collection" %>
<%@attribute name="showHeader" required="false" type="java.lang.Boolean" %>

<c:if test="${!empty items}">
    <c:if test="${showHeader != false}">
        <label class="subheader">Receipt Refunds</label>
    </c:if>
        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
            <tr>
                <th>User</th>
                <th>Action Date</th>
                <th>Refund Date</th>
                <th>Amount</th>
                <th>Reason</th>
                <th>Statement Date</th>
                <th>Statement Description</th>
            </tr>
            <c:forEach var="rr" items="${items}">
                <tr>
                    <td><label>${rr['USER_NAME']}</label></td>
                    <td><label>${rr['ACTION_DATE']}</label></td>
                    <td><label>${rr['REFUND_DATE']}</label></td>
                    <td align="right"><label>${rr['REFUND_AMOUNT']}</label></td>
                    <td><label>${rr['REFUND_REASON']}</label></td>
                    <td><label>${rr['STATEMENT_DATE']}</label></td>
                    <td><label>${rr['STATEMENT_DESCRIPTION']}</label></td>
                </tr>
            </c:forEach>
        </table>
        <br>
    </c:if>  
                                    
<%-- 
    Document   : ClaimLineDetailTag
    Created on : 24 Jan 2013, 4:20:56 PM
    Author     : marcelp
--%>

<%@tag description="call center menu item tabs tag" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="claim"%>

<label class="header">Detailed ClaimLine Results</label>
<br/><br/>
<table id="main" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
    <thead>
        <tr>
            <th>Dependant Code</th>
            <th>Treatment Date</th>
            <th>Tariff Code</th>
            <th>Tariff Description</th>
            <th>Nappi Code</th>
            <th>Nappi Description</th>
            <th>Quantity</th>
            <th>Claimed Amount</th>
            <th>Tariff Amount</th>
            <th>Paid Amount</th>
            <th>Members Liability</th>
            <c:if test="${sessionScope.ClaimLineObject.isPractice == true}">
                <th>Overpaid Amount</th>
            </c:if>
        </tr>
    </thead>
    <c:set var="items" value="${sessionScope.ClaimLineObject}"/>
    <tbody>
        <tr>
            <td><label>${items.dependantCode}</label></td>
            <td><label>${items.treatmentDate}</label></td>
            <td><label>${items.tariffCode}</label></td>
            <td><label>${items.tariffDescription}</label></td>
            <td><label>${items.nappiCode}</label></td>
            <td><label>${items.nappiDescription}</label></td>
            <td><label>${items.units}</label></td>
            <td><label>${items.claimedAmount}</label></td>
            <td><label>${items.tariffAmount}</label></td>
            <td><label>${items.paidAmount}</label></td>
            <td><label>${sessionScope.MemberLiability}</label></td>
            <c:if test="${sessionScope.ClaimLineObject.isPractice == true}">
                <c:set var="claim" value="${sessionScope.ClaimLineObject}"/>
                <td><label>${claim.overPaidAmount}</label></td>
            </c:if>
        </tr>
    </tbody>
</table>
<br/>
<table id="main" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
    <thead>
        <tr> 
            <th>Operator</th>
            <th>ICD Code</th>
            <th>ICD Description</th>
            <th>Pre-Auth Number</th>
            <th>Benefit</th>
            <th>Process Date</th>
            <th>Co-payment</th>
            <th>Threshold</th>
            <th>Tooth Number</th>
        </tr>
    </thead>
    <tr>
        <td><label>${items.operator}</label></td>
        <td><label>${items.icdCode}</label></td>
        <td><label>${items.icdDescription}</label></td>
        <td><label>${items.preAuthNumbers}</label></td>
        <td><label>${items.benefitDesc}</label></td>
        <td><label>${items.processDate}</label></td>
        <td align="right"><label>${items.copayment}</label></td>
        <td align="right"><label>${items.threshold}</label></td>
        <td><label>${items.toothNumber}</label></td>
    </tr>
</table>

<c:if test="${!empty sessionScope.ClaimLineObject.lstClaimLineOverride}">
    <br/>
    <table table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th>Unit</th>
            <th>Multiplier</th>
            <th>Override ID</th>
            <th>Description</th>
            <th>User</th>
            <th>Date Override</th>

            <c:forEach items="${sessionScope.ClaimLineObject.lstClaimLineOverride}" var="overr">
            <tr>
                <td><label>${overr.units}</label></td>
                <td><label>${overr.multiplier}</label></td>
                <td><label>${overr.overrideId}</label></td>
                <td><label>${overr.description}</label></td>
                <td><label>${overr.userName}</label></td>
                <td><label>${overr.dateOverriden}</label></td>
            </tr>
        </c:forEach>
    </tr>
</table>
</c:if>
        
<c:if test="${!empty sessionScope.ClaimLineObject.lstClaimLineRuleMessages}">
    <br/>
    <table table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th>Rejection Code</th>
            <th>Severity</th>
            <th>Message</th>

            <c:forEach items="${sessionScope.ClaimLineObject.lstClaimLineRuleMessages}" var="entry">
            <tr>
                <td><label>${entry.messageCode}</label></td>
                <td><label>${entry.severity}</label></td>
                <td><label>${entry.longMsgDescription}</label></td>
            </tr>
        </c:forEach>
    </tr>
</tbody>
</table>
</c:if>

<c:if test="${!empty sessionScope.ClaimLineObject.lstClaimLineModifiers}">
    <br/>
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th>Modifier</th>
            <th>Tariff Amount</th>
        </tr>

        <c:forEach items="${sessionScope.ClaimLineObject.lstClaimLineModifiers}" var="modi">
            <tr>
                <td><label>${modi.modifier}</label></td>
                <td><label>${modi.modifierAmount}</label></td>
            </tr>
        </c:forEach>
    </table>
</c:if>

<c:if test="${!empty sessionScope.ClaimLineObject.reversalCode}" var="rev">
    <br/>
    <c:set var="rejected" value="${sessionScope.ClaimLineObject}"/>
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th>Reversal Code</th>
            <th>Reversal Description</th>
        </tr>
        <tr>
            <td><label>${rejected.reversalCode}</label></td>
            <td><label>${rejected.reversalDesc}</label></td>
        </tr>
    </table>
</c:if>






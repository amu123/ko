<%-- 
    Document   : PDCDocDropDownSelectTag
    Created on : 11 Mar 2015, 9:27:18 AM
    Author     : shimanem
--%>
<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="javascript" rtexprvalue="true" required="true" %>
<%@attribute name="id" rtexprvalue="true" required="true"%>
<%@ attribute name="displayName" rtexprvalue="true" required="true" %>
<%-- The list of normal or fragment attributes can be specified here: --%>
<td class="label" align="left" width="160px">${displayName}:</td>
<td align="left" width="200px">
    <select style="width:200px" id="optSelectID" name="optSelect" ${javascript}>
        <option value="0"></option>
        <option value="290" id="optCDL">Normal CDL</option>
        <option value="291" id="optCDLOther">Other Chronic Conditions</option>
    </select> 
</td>
<%-- 
    Document   : UnionSearchResultsTag
    Created on : Jul 6, 2016, 10:12:54 AM
    Author     : shimanem
--%>
<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<br/>
<HR color="#666666" WIDTH="100%" align="left">
<br/>
<label class="header">Union Search Result</label><br><br>
<c:if test="${not empty sessionScope.unionListDetails}">
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th align="left">Union Number</th>
            <th align="left">Union Name</th>
            <th align="left">Inception Date</th>
            <th align="left">Termination Date</th>
            <th align="left">Representative Name</th>
            <th align="left">Representative Number</th>
            <th align="left">&nbsp;</th>
            <th>&nbsp;</th>
        </tr>
        <c:forEach items="${unionListDetails}" var="union">
            <tr>                                    
                <td><label class="label">${union.unionNumber}</label></td>
                <td><label class="label">${union.unionName}</label></td>
                <td><label class="label">${agiletags:formatXMLGregorianDate(union.inceptionDate)}</label></td>
                <td><label class="label">${agiletags:formatXMLGregorianDate(union.terminationDate)}</label></td>
                <td><label class="label">${union.representativeName}&nbsp;${union.representativeSurname}</label></td>
                <td><label class="label">${union.representativeNumber}</label></td>
                <td><button value="UnionViewCommand" type="button" name="opperation" width="100px" onclick="submitUnionDetails('${union.unionID}', '${union.unionNumber}', '${union.unionName}', '${union.representativeNumber}', 'UnionViewCommand');">View</button></td>
            </tr>
        </c:forEach>
    </table>
</c:if>
<c:if test="${empty sessionScope.unionListDetails && sessionScope.unionListDetails eq ''}">
    <label class="red">No Results Found</label>
</c:if>
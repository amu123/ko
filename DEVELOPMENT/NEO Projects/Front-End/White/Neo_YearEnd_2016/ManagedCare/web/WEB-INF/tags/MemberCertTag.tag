<%-- 
    Document   : MemberCertTag
    Created on : 2013/01/28, 10:42:23
    Author     : johanl
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<td>
    <div>
        <c:if test="${!empty memberNum_text}">
            <c:if test="${empty indexList}">
                <input type="button" value="Generate New Certificate" id="btn_GenMemCert" onclick="submitWithActionGeneration('MemberCertificateGenerationCC','generate',null);" /><br/><br/>
                <label class="error">No Member Certificate Generated</label><br/>
            </c:if>        
            <c:if test="${!empty indexList}">
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Member Number</th>
                        <th>Type</th>
                        <th>Location</th>
                        <th>Current Date</th>                                              
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${indexList}">
                        <tr class="label">
                            <td>${entry['member']}</td>
                            <td>${entry['docType']}</td>
                            <td>${entry['location']}</td>
                            <td><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.currDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.currDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.currDate.day}"/></td>
                        <td><input id="viewB" type="button" value="View"  onClick="submitWithActionGeneration('MemberCertificateGenerationCC','view','${entry['location']}');" /></td>
                        </tr>
                    </c:forEach>

                </table>
            </c:if>        
        </c:if>
    </div>
</td>
<br/>
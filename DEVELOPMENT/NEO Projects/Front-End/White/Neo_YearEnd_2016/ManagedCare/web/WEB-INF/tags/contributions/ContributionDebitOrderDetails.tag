<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@attribute name="items" required="true" type="java.util.Collection" %>
<%@attribute name="showHeader" required="false" type="java.lang.Boolean" %>

<c:if test="${!empty items}">
    <c:if test="${showHeader != false}">
        <label class="subheader">Debit Order Details</label>
    </c:if>
        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
            <tr>
                <th>Account Holder</th>
                <th>Payment Run Date</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Rejection Reason</th>
            </tr>
            <c:forEach var="doh" items="${items}">
                <tr>
                    <td align="right"><label>${doh.bankAccHolder}</label></td>
                    <td align="right"><label>${doh.paymentRunDate}</label></td>
                    <td align="right"><label>${agiletags:formatBigDecimal(doh.paymentRunAmount)}</label></td>
                    <td align="right"><label>${doh.paymentStatus}</label></td>
                    <td align="right"><label>${doh.rejectionReason}</label></td>
                </tr>
            </c:forEach>
        </table>
        <br>
    </c:if>  
                                    
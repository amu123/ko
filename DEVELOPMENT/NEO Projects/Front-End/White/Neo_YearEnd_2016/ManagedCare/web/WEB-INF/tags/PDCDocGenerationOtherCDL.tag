<%-- 
    Document   : PDCDocGenerationOtherCDL
    Created on : 10 Mar 2015, 3:24:48 PM
    Author     : shimanem
--%>
<%@tag description=""  dynamic-attributes="dynattrs" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
    <tr>
        <th>Document Type</th>
        <th>Document Name</th>
        <th>Output File</th>
        <th></th>
    </tr>
    <c:forEach var="entry" items="${sessionScope.documentOtherCDLMap}">
        <c:set var="docName" value="${fn:replace(entry.value, ' ', '')}${entry.key}" />
        <tr>
            <td><label class="label">Benefit Letter</label></td>
            <td><label id="docPDC_hiv" class="label">${entry.value}</label></td>
                <c:choose>
                    <c:when test="${sessionScope[docName] != null}">
                    <td><label class="label">${sessionScope[docName]}</label></td>
                    </c:when>
                    <c:otherwise>
                    <td></td>
                </c:otherwise>            
            </c:choose>
            <td><input id="cardButton" type="button" value="Generate" onclick="generateDoc(${entry.key}, '${fn:replace(entry.value, ' ', '')}', 'PDCDocumentGenerationCommand');"/></td>
        </tr>
    </c:forEach>

</table>
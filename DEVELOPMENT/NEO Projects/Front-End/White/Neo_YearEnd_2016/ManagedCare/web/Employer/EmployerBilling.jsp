<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<body>
<agiletags:ControllerForm name="EmployerBillingForm">
    <input type="hidden" name="opperation" id="EmployerBilling_opperation" value="SaveEmployerBillingCommand" />
    <input type="hidden" name="onScreen" id="EmployerBilling_onScreen" value="" />
    <input type="hidden" name="EmployerGroup_entityId" id="EmployerGroup_Billing_entityId" value="${EmployerGroup_entityId}" />
    <input type="hidden" name="splitType" id="splitType" value="${EmployerBilling_splitValue}" />
    <label class="header">Employer Billing</label>
    <hr>

    <table id="EmployerBillingTable">
        <tr id="EmployerBilling_methodRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Billing Method"  elementName="EmployerBilling_method" lookupId="182" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerBilling_scheduleRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Schedule Day"  elementName="EmployerBilling_scheduleDate" lookupId="183" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerBilling_fileFormatRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="File Format"  elementName="EmployerBilling_fileFormat" lookupId="195" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerBilling_sortIndRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Sort Method"  elementName="EmployerBilling_sortInd" lookupId="196" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerBilling_nameRow"><agiletags:LabelTextBoxErrorReq displayName="Contact Person" elementName="EmployerBilling_name" valueFromRequest="EmployerBilling_name" mandatory="no"/>        </tr>
        <tr id="EmployerBilling_designationRow"><agiletags:LabelTextBoxErrorReq displayName="Designation" elementName="EmployerBilling_designation" valueFromRequest="EmployerBilling_designation" mandatory="no"/>        </tr>
        <tr id="EmployerBilling_telRow"><agiletags:LabelTextBoxErrorReq displayName="Telephone No." elementName="EmployerBilling_tel" valueFromRequest="EmployerBilling_tel" mandatory="no"/>        </tr>
        <tr id="EmployerBilling_emailRow"><agiletags:LabelTextBoxErrorReq displayName="Email" elementName="EmployerBilling_email" valueFromRequest="EmployerBilling_email" mandatory="no"/>        </tr>
        <tr id="EmployerBilling_activeIndRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Active Members"  elementName="EmployerBilling_activeInd" lookupId="184" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerBilling_paidByIndRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Paid By"  elementName="EmployerBilling_paidBy" lookupId="207" mandatory="no" errorValueFromSession="yes"/>        </tr>
    </table>
    <table id="EmployerBillingSplitTable">
        <tr id="EmployerBilling_splitByIndRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Split Refers To"  elementName="EmployerBilling_splitBy" lookupId="209" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerBilling_splitTpeIndRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Split Type"  elementName="EmployerBilling_splitType" lookupId="208" mandatory="no" errorValueFromSession="yes"  javaScript="onChange=\"hideMaxAmount(this.value)\""/>        </tr>
        <tr id="EmployerBilling_splitValueRow"><agiletags:LabelTextBoxErrorReq displayName="Split Value" elementName="EmployerBilling_splitValue" valueFromRequest="EmployerBilling_splitValue" mandatory="no"/>        </tr>
        <c:choose>
            <c:when test="${EmployerBilling_splitType != 2}">
                <tr id="EmployerBilling_maxAmountRow" style="display:none"><agiletags:LabelTextBoxErrorReq displayName="Maximum Amount" elementName="EmployerBilling_maxAmount" valueFromRequest="EmployerBilling_maxAmount" mandatory="no" javaScript=""/></tr>
            </c:when>
            <c:otherwise>
                <tr id="EmployerBilling_maxAmountRow"><agiletags:LabelTextBoxErrorReq displayName="Maximum Amount" elementName="EmployerBilling_maxAmount" valueFromRequest="EmployerBilling_maxAmount" mandatory="no" javaScript=""/></tr>
            </c:otherwise>
        </c:choose>
    </table>

    <br>
    <label class="subheader">Payment Details</label>
    <hr>
    <table id="EmployerPaymentTable">
        <tr id="EmployerBilling_Payment_methodRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Payment Method"  elementName="EmployerBilling_Payment_paymentMethod" lookupId="188" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerBilling_Payment_bankRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Name of Bank"  elementName="EmployerBilling_Payment_bankName" lookupId="bank" mandatory="yes" errorValueFromSession="no" javaScript="onChange=\"toggleBranch(this.value, 'EmployerBilling_Payment_bankBranchId')\""/>        </tr>        
        <tr id="EmployerBilling_Payment_branchRow"><agiletags:LabelNeoBranchDropDownErrorReq displayName="Branch"  elementName="EmployerBilling_Payment_bankBranchId"  mandatory="no" errorValueFromSession="yes" parentElement="EmployerBilling_Payment_bankName"/>        </tr>
        <tr id="EmployerBilling_Payment_accountTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Account Type"  elementName="EmployerBilling_Payment_accountType" lookupId="37" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerBilling_Payment_accountHolderRow"><agiletags:LabelTextBoxErrorReq displayName="Account Holder" elementName="EmployerBilling_Payment_accountHolder" valueFromRequest="EmployerBilling_Payment_accountHolder" mandatory="no"/>        </tr>
        <tr id="EmployerBilling_Payment_accountNoRow"><agiletags:LabelTextBoxErrorReq displayName="Account no." elementName="EmployerBilling_Payment_accountNumber" valueFromRequest="EmployerBilling_Payment_accountNumber" mandatory="no"/>        </tr>
    </table>

    <br>
    <label class="subheader">Authorisation Details</label>
    <hr>
    <table id="EmployerAuthTable">
        <tr id="EmployerBilling_Payment_name1Row"><agiletags:LabelTextBoxErrorReq displayName="Full Name" elementName="EmployerBilling_Payment_name1" valueFromRequest="EmployerBilling_Payment_name1" mandatory="no"/>        </tr>
        <tr id="EmployerBilling_Payment_surname1Row"><agiletags:LabelTextBoxErrorReq displayName="Surname" elementName="EmployerBilling_Payment_surname1" valueFromRequest="EmployerBilling_Payment_surname1" mandatory="no"/>        </tr>
        <tr id="EmployerBilling_Payment_designation1Row"><agiletags:LabelTextBoxErrorReq displayName="Designation" elementName="EmployerBilling_Payment_designation1" valueFromRequest="EmployerBilling_Payment_designation1" mandatory="no"/>        </tr>
        <tr><td colspan="5"><hr></td></tr>
        <tr id="EmployerBilling_Payment_name2Row"><agiletags:LabelTextBoxErrorReq displayName="Full Name" elementName="EmployerBilling_Payment_name2" valueFromRequest="EmployerBilling_Payment_name2" mandatory="no"/>        </tr>
        <tr id="EmployerBilling_Payment_surname2Row"><agiletags:LabelTextBoxErrorReq displayName="Surname" elementName="EmployerBilling_Payment_surname2" valueFromRequest="EmployerBilling_Payment_surname2" mandatory="no"/>        </tr>
        <tr id="EmployerBilling_Payment_designation2Row"><agiletags:LabelTextBoxErrorReq displayName="Designation" elementName="EmployerBilling_Payment_designation2" valueFromRequest="EmployerBilling_Payment_designation2" mandatory="no"/>        </tr>
    </table>

    <hr>
    <table id="EmployerBillingSaveTable">
        <tr>
            <td><input type="reset" value="Reset"></td>
            <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'EmployerBilling', this)"></td>
        </tr>
    </table>
</agiletags:ControllerForm>
</body>

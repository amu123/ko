<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<agiletags:ControllerForm name="EmployerBranchForm">
    <input type="hidden" name="opperation" id="EmployerBranch_opperation" value="EmployerBranchCommand" />
    <input type="hidden" name="onScreen" id="EmployerBranch_onScreen" value="EmployerBranch" />
    <input type="hidden" name="entityId" id="EmployerBranch_entityId" value="${EmployerBranch_entityId}" />
    <input type="hidden" name="branchId" id="EmployerBranch_branchId" value="" />
    <input type="hidden" name="branchName" id="EmployerBranch_branchName" value="" />
    <input type="hidden" name="branchNumber" id="EmployerBranch_branchNumber" value="" />
    <agiletags:HiddenField elementName="EmployerBranch_entityId"/>

    <label class="subheader">Branch List</label> 
    <hr>
    <input name="EmployerBranchAddButton" type="submit" value="Add Branch"/>
    <table id="EmployerBranchTable"  class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th>Branch Number</th>
            <th>Branch Name</th>
            <th></th>
        </tr>
        <c:forEach var="entry" items="${EmployerBranchList}">
            <tr>
                <td style="width: 100px">${entry.employerNumber}</td>
                <td style="width: 200px">${entry.employerName}</td>
                <td style="width: 100px;"><input type="submit" value="View" onclick="this.form['branchId'].value='${entry.entityId}';this.form['branchName'].value='${agiletags:escapeStr(entry.employerName)}';this.form['branchNumber'].value='${entry.employerNumber}';"></td>
            </tr>
        </c:forEach>
    </table>


</agiletags:ControllerForm>
<form

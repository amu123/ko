<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="EmployerBrokerForm">
    <input type="hidden" name="opperation" id="EmployerBroker_opperation" value="SaveEmployerIntermediaryCommand" />
    <input type="hidden" name="onScreen" id="EmployerBroker_onScreen" value="" />
    <input type="hidden" name="EmployerGroup_entityId" id="EmployerGroup_Broker_entityId" value="${EmployerGroup_entityId}" />
    <label class="header">Intermediary Details</label>
    <hr>

      <table id="EmployerBrokerTable">
        <tr id="EmployerBroker_brokerRefNoRow"><agiletags:LabelTextBoxErrorReq displayName="Broker Ref No." elementName="EmployerBroker_brokerRefNo" valueFromRequest="EmployerBroker_brokerRefNo" mandatory="no"/>        </tr>
        <tr id="EmployerBroker_brokerNameRow"><agiletags:LabelTextBoxErrorReq displayName="Name of Broker" elementName="EmployerBroker_branchCode" valueFromRequest="EmployerBroker_branchCode" mandatory="no"/>        </tr>
        <tr id="EmployerBroker_brokerageNameRow"><agiletags:LabelTextBoxErrorReq displayName="Brokerage Name" elementName="EmployerBroker_brokerageName" valueFromRequest="EmployerBroker_brokerageName" mandatory="no"/>        </tr>
        <tr id="EmployerBroker_rhRefNoRow"><agiletags:LabelTextBoxErrorReq displayName="Resolution Brokerage Code" elementName="EmployerBroker_rhRefNo" valueFromRequest="EmployerBroker_rhRefNo" mandatory="no"/>        </tr>
        <tr id="EmployerBroker_telRow"><agiletags:LabelTextBoxErrorReq displayName="Telephone No." elementName="EmployerBroker_tel" valueFromRequest="EmployerBroker_tel" mandatory="no"/>        </tr>
        <tr id="EmployerBroker_emailRow"><agiletags:LabelTextBoxErrorReq displayName="Email Address" elementName="EmployerBroker_email" valueFromRequest="EmployerBroker_email" mandatory="no"/>        </tr>
        <tr id="EmployerBroker_faxRow"><agiletags:LabelTextBoxErrorReq displayName="Fax No." elementName="EmployerBroker_fax" valueFromRequest="EmployerBroker_fax" mandatory="no"/>        </tr>
      </table>

    <hr>
    <table id="EmployerGroupSaveTable">
        <tr>
           <td><input type="reset" value="Reset"></td>
           <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'EmployerDetails', this)"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>

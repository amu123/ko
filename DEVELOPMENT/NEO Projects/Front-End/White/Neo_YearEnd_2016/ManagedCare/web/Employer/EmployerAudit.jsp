<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="EmployerAuditForm">
    <input type="hidden" name="opperation" id="EmployerAudit_opperation" value="EmployerAuditCommand" />
    <input type="hidden" name="onScreen" id="EmployerAudit_onScreen" value="EmployerAudit" />
    <agiletags:HiddenField elementName="EmployerAudit_employerEntityId"/>
    <label class="header">Employer Audit Trail</label>
    <hr>

      <table id="EmployerAuditTable">
          <tr>
              <td  class="label"width="160">Details</td>
              <td>
                  <select style="width:215px" name="auditList" id="auditList" onChange="submitFormWithAjaxPost(this.form, 'EmployerAuditDetails');">
                      <option value="99" selected>&nbsp;</option>
                      <option value="EMPLOYER_GROUP">Group Details</option>
                      <option value="ADDRESS_DETAILS">Address Details</option>
                      <option value="BANK_DETAILS">Bank Details</option>
                      <option value="CONTACT_DETAILS">Contact Details</option>
                      <option value="BILLING_DETAILS">Billing Details</option>
                      <option value="ELIGIBILITY_DETAILS">Eligibility Details</option>
                      <option value="COMMUNICATION_DETAILS">Communication Details</option>
                      <option value="INTERMEDIARY_DETAILS">Intermediary Details</option>
                      <option value="UNDERWRITTING_DETAILS">Underwriting Details</option>
                      <option value="STATUS_UPDATE">Status Update</option>
                      <option value="EMPLOYER_ALL">All</option>
                  </select>
              </td>
              
          </tr>
      </table>          
 </agiletags:ControllerForm>
 <div style ="display: none"  id="EmployerAuditDetails"></div>
   
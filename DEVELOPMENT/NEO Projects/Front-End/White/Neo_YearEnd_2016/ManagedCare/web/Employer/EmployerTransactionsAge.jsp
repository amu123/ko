<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags/contributions" %>
<nt:ContributionAgeAnalysis items="${ageAnalysis}" showMember="true" showHeader="false"/>
<c:if test="${applicationScope.Client == 'Sechaba'}">
<agiletags:ButtonOpperationLabelError displayName="Export" elementName="export" type="button" javaScript="onClick=\"exportExcel('Age');\""/>
</c:if>

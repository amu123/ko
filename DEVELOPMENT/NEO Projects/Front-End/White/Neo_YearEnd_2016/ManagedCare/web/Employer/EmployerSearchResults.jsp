<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Group Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty EmployerSearchResults}">
            <span>No Groups found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Group Name</th>
                        <th>Group Code</th>
                        <th>Inception Date</th>
                        <th>Termination Date</th>
                        <th>Action </th>
                    </tr>
                    <c:forEach var="entry" items="${EmployerSearchResults}">
                        <tr>
                            <td><label class="label">${entry.employerName}</label></td>
                            <td><label class="label">${entry.employerNumber}</label></td>
                            <td><label class="label">${entry.startDate}</label></td>
                            <td><label class="label">${entry.endDate == null ? "&nbsp;" : entry.endDate}</label></td>
                            <td><button type="submit" value="${entry.entityId}" onclick="setEmpValues(this.value,'${agiletags:escapeStr(entry.employerName)}','${entry.employerNumber}' );">View</button></td>
                        </tr>
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>

  
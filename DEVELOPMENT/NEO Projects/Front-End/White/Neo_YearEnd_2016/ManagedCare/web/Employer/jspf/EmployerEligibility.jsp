  <agiletags:ControllerForm name="EmployerEligibilityForm">
    <input type="hidden" name="opperation" id="opperation" value="" />
    <input type="hidden" name="onScreen" id="onScreen" value="" />

    <label class="header">EmployerEligibility</label>

    <fieldset>
      <table id="EmployerEligibilityTable">
        <tr id="eligibilityIdRow"><agiletags:LabelNeoLookupValueDropDownError displayName="Eligibility"  elementName="eligibilityId" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="employerEntityIdRow"><agiletags:LabelNeoLookupValueDropDownError displayName="EmployerEntity"  elementName="employerEntityId" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="productOptionsIdRow"><agiletags:LabelNeoLookupValueDropDownError displayName="ProductOptions"  elementName="productOptionsId" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="totalStaffCountRow"><agiletags:LabelTextBoxError displayName="TotalStaffCount" elementName="totalStaffCount" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="totalStaffEligibleRow"><agiletags:LabelTextBoxError displayName="TotalStaffEligible" elementName="totalStaffEligible" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="totalStaffParticipatingRow"><agiletags:LabelTextBoxError displayName="TotalStaffParticipating" elementName="totalStaffParticipating" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="totalPensionersRow"><agiletags:LabelTextBoxError displayName="TotalPensioners" elementName="totalPensioners" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="totalPensionersEligibleRow"><agiletags:LabelTextBoxError displayName="TotalPensionersEligible" elementName="totalPensionersEligible" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="totalPensionersParticipatingRow"><agiletags:LabelTextBoxError displayName="TotalPensionersParticipating" elementName="totalPensionersParticipating" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="availableToAllIndRow"><agiletags:LabelNeoLookupValueDropDownError displayName="AvailableToAll"  elementName="availableToAllInd" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="hrCorrospondenceIndRow"><agiletags:LabelNeoLookupValueDropDownError displayName="HrCorrospondence"  elementName="hrCorrospondenceInd" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="membershipCardsIndRow"><agiletags:LabelNeoLookupValueDropDownError displayName="MembershipCards"  elementName="membershipCardsInd" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="communicateWithMembersIndRow"><agiletags:LabelNeoLookupValueDropDownError displayName="CommunicateWithMembers"  elementName="communicateWithMembersInd" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="ageLimitRow"><agiletags:LabelTextBoxError displayName="AgeLimit" elementName="ageLimit" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="effectiveStartDateRow"><agiletags:LabelTextBoxDateTime  displayName="EffectiveStartDate" elementName="EffectiveStartDate" valueFromSession="no" mandatory="no"/>        </tr>
        <tr id="effectiveEndDateRow"><agiletags:LabelTextBoxDateTime  displayName="EffectiveEndDate" elementName="EffectiveEndDate" valueFromSession="no" mandatory="no"/>        </tr>
      </table>
    </fieldset>
  </agiletags:ControllerForm>

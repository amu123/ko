<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<c:if test="${not empty bankStatement}">
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th>Date</th>
            <th>Transaction Ref</th>
            <th>Description</th>
            <th>Statement Amount</th>
            <th>Alloc Amount</th>
            <th>Unalloc Amount</th>
            <th>Refund Amount</th>
        </tr>
        <c:forEach var="rec" items="${bankStatement}">
            <tr>
                <td><label>${rec['STATEMENT_DATE']}</label></td>
                <td><label>${rec['TRANSACTION_REF_NO']}</label></td>
                <td><label>${rec['TRANSACTION_DESCRIPTION']}</label></td>
                <td align="right"><label>${agiletags:formatBigDecimal(rec['AMOUNT'])}</label></td>
                <td align="right"><label>${agiletags:formatBigDecimal(rec['ALLOCATED_AMOUNT'])}</label></td>
                <td align="right"><label>${agiletags:formatBigDecimal(rec['UNALLOCATED_AMOUNT'])}</label></td>
                <td align="right"><label>${agiletags:formatBigDecimal(rec['REFUND_AMOUNT'])}</label></td>
            </tr>
        </c:forEach>
    </table>
</c:if>
<c:if test="${empty bankStatement}">
    <label>No Bank Statements available</label>
</c:if>
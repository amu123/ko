<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>

<nt:NeoPanel title="Age Analysis" command="EmployerTransactionCommand" action="AgeAnalysis" actionId="${employerEntityId}"/>
<nt:NeoPanel title="Unallocated Receipts" command="EmployerTransactionCommand" action="Unallocated" actionId="${employerEntityId}"/>
<nt:NeoPanel title="Bank Statements" command="EmployerTransactionCommand" action="Bank" actionId="${employerEntityId}"/>
<nt:NeoPanel title="Debit Orders" command="EmployerTransactionCommand" action="DebitOrders" actionId="${employerEntityId}"/>
<nt:NeoPanel title="Receipt Refunds" command="EmployerTransactionCommand" action="Refunds" actionId="${employerEntityId}"/>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<agiletags:ControllerForm name="BrokerSearchForm">
    <input type="hidden" name="opperation" id="opperation" value="AdministratorSearchCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="DisorderSearch" />
    <input type="hidden" name="target_div" value="${target_div}" />
    <input type="hidden" name="main_div" value="${main_div}" />
    <input type="hidden" name="diagCode" id="icd10DiagCode" value="" />
    <input type="hidden" name="diagName" id="icd10DiagName" value="" />
    <input type="hidden" name="diagCodeId" id="icd10DiagCodeId" value="${diagCodeId}" />
    <input type="hidden" name="diagNameId" id="icd10DiagNameId" value="${diagNameId}" />
    <br/>
    <br/>
    <label class="subheader">Administrator Search</label>
    <hr>
    <table>
        <tr><agiletags:LabelTextBoxError displayName="Name" elementName="admName"/></tr>
        <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="admSurname"/></tr>
        <tr><agiletags:LabelTextBoxError displayName="Username" elementName="admUsername"/></tr>
        <tr><agiletags:LabelTextBoxError displayName="Email" elementName="admEmail"/></tr>
        <tr>
            <td><input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
            <td align="right"><input type="button" value="Search" onclick="submitFormWithAjaxPost(this.form, 'AdministratorSearchResults');"></td>
        </tr>
    </table>

    <div style ="display: none"  id="AdministratorSearchResults"></div>
</agiletags:ControllerForm>



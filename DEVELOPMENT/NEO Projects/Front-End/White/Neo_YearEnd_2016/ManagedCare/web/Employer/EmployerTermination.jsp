
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

  <agiletags:ControllerForm name="EmployerTerminationForm">
    <input type="hidden" name="opperation" id="opperation" value="EmployerTerminationCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="TerminateGroup" />
    <input type="hidden" name="employerEntityId" id="employerEntityId" value="${requestScope.employerEntityId}" />
    <input type="hidden" name="target_div" value="${target_div}" />
    <input type="hidden" name="main_div" value="${main_div}" />

    <label class="header">Terminate Group</label>
    <hr>
    <br>
     <table>
         <tr><agiletags:LabelTextBoxDateReq  displayname="Termination Date" elementName="Employer_termination_date" valueFromSession="yes" mandatory="yes" javascript="onclick=\"document.getElementById('EmployerTerminateButton').style.display='block'\""/></tr>
      </table>
      
    <hr>
    <table id="EmployerTerminationDateTable">
        <tr>
            <td><input type="button" value="Cancel" onclick="if (document.getElementById('EmployerTerminateButton').style.display == 'none') {window.location='/ManagedCare/Security/Welcome.jsp'; } else {swapDivVisbible('${target_div}','${main_div}');}"></td>
           <td><input id="EmployerTerminateButton" type="button" value="Save" onclick="this.style.display='none'; submitFormWithAjaxPost(this.form, '${target_div}', this,'${main_div}', '${target_div}');"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>
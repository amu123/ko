
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>


<agiletags:ControllerForm name="MemberForm">
    <input type="hidden" name="opperation" id="opperation" value="EmployerTabContentsCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberList" />
    <input type="hidden" name="tab" value="Members" />
    <input type="hidden" name="employerEntityId" id="membersEntityId" value="${requestScope.employerEntityId}" />
    <input type="hidden" name="employerParentEntityId" id="employerEntityId" value="${requestScope.parentEntityId}" />
    <input type="hidden" name="employerStatus" id="employerStatus" value="${requestScope.empStatus}" />
    <label class="header">Members History</label>
    <hr>
    <div id="PageContents" >
        <jsp:directive.include file="/Employer/Members_1.jsp"></jsp:directive.include>
        </div>
        <br>
        <c:if test="${applicationScope.Client == 'Sechaba'}">
        <agiletags:ButtonOpperationLabelError displayName="Export" elementName="export" type="button" javaScript="onClick=\"exportExcel('Member');\""/>
        </c:if>
</agiletags:ControllerForm>
</body>
</html>

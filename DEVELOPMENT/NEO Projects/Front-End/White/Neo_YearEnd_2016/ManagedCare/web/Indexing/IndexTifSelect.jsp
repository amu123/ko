<%-- 
    Document   : IndexDocument
    Created on : 02 Jul 2012, 3:34:17 PM
    Author     : Christo
--%>
<%@page import="com.koh.serv.PropertiesReader"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<% String folderList = (String) request.getAttribute("folderList");
   String srcDrive = (String) request.getAttribute("srcDrive");
    System.out.println("-------------Tiff JSP------------------");
    System.out.println("folderList in page = " + folderList);
    System.out.println("srcDrive in page = " + srcDrive);
    if (folderList == null) {
        folderList = new PropertiesReader().getProperty("DocumentIndexScanFolder");
    }
    if (srcDrive == null) {
        srcDrive = new PropertiesReader().getProperty("DocumentIndexScanDrive");
    }
    String listOrWork = (String) request.getAttribute("listOrWork");
    if (listOrWork == null) {
        listOrWork = "List";
    }
    String errorMsg = (String) request.getAttribute("errorMsg");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script language="JavaScript">
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }            
            function setParams(){
                document.getElementById('folderList').value =  '<%=folderList%>';
                document.getElementById('listOrWork').value = '<%=listOrWork%>';
            }
            function refreshFileListByProfile(){ 
                var selectedVal = document.getElementById("profiletype").options[document.getElementById("profiletype").selectedIndex].value;
                document.getElementById('retrievalType').value=selectedVal; 
            }
        </script>
    </head>
    <body>

        <table width=100% height=100%><tr valign="left"><td width="50px"></td><td align="left">
                    <label class="header">Document Indexing</label>
                    <br/><br/><br/>
                    <table>
                        <% if (errorMsg != null) {%>
                        <tr><td><label id="error" class="error"><%=errorMsg%></label></td></tr>
                        <% }%>
                        <agiletags:ControllerForm name="documentIndexing"> 
                            <input type="hidden" name="folderList" id="folderList" value="" />
                            <input type="hidden" name="listOrWork" id="listOrWork" value="" />
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="retrievalType" id="retrievalType" value=""/>
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />


                            <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                            <tr><td rowspan="1" style="vertical-align: top">
                                    <table>
                                       
                                        <tr><agiletags:DocumentIndexDropdownType displayName="Profile Type" elementName="profiletype"/></tr>
                                        <tr><agiletags:DocumentIndexingFileList  command="memberNumber" multiple="true" /></tr>
                                    </table> </td><td>
                                </td>
                            </tr>
                         </agiletags:ControllerForm>
                    </table>
                </td></tr></table>

    </body>
</html>


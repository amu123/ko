<%-- 
    Document   : IndexDocument
    Created on : 02 Jul 2012, 3:34:17 PM
    Author     : Christo
--%>
<%@page import="com.koh.serv.PropertiesReader"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<% String folderList = (String) request.getAttribute("folderList");
System.out.println("-------------JSP------------------");
    System.out.println("folderList in page = " + folderList);
    if (folderList == null) {
        folderList = new PropertiesReader().getProperty("DocumentIndexScanFolder");
    }
    String listOrWork = (String)request.getAttribute("listOrWork");
    if(listOrWork == null){
        listOrWork = "List";
    }
String errorMsg = (String)request.getAttribute("errorMsg");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script language="JavaScript">
            var idClicked = "";
            $(document).ready(function(){
            $("#profileIndexButton").click(function () {
                idClicked = "profileIndexButton";
            });
            
             $("#documentIndexing").submit(function(){
                    document.getElementById('profileIndexButton').disabled = true;
             });
            });
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }            
            function setParams(){
                document.getElementById('folderList').value =  '<%=folderList%>';
                document.getElementById('listOrWork').value = '<%=listOrWork%>';
            }
            function refreshText(){
                var selectedType = document.getElementById("entitytype");
                var selectedText = selectedType.options[selectedType.selectedIndex].text;
                if(selectedText==''){
                    document.getElementById('memberType').style.display='none';
                    document.getElementById('memberNumber').style.display='none';
                }else{
                    document.getElementById('memberType').innerHTML = selectedText+' Number:';
                    document.getElementById('memberType').style.display='';
                    document.getElementById('memberNumber').style.display='';
                }
            }
            
        </script>
    </head>
    <body>

        <table width=100% height=100%><tr valign="left"><td width="50px"></td><td align="left">
                    <label class="header">Document Indexing For <%=request.getSession().getAttribute("profile")%></label>
                    <br/><br/><br/>
                    <table>
                        <% if(errorMsg != null){ %>
                        <tr><td><label id="error" class="error"><%=errorMsg%></label></td></tr>
                        <% } %>
                        <agiletags:ControllerForm name="documentIndexing"> 
                        <input type="hidden" name="folderList" id="folderList" value="" />
                        <input type="hidden" name="listOrWork" id="listOrWork" value="" />
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />


                            <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                            <tr><td>
                                    <table>
                                        <tr><agiletags:DocumentIndexingFileList  command="memberNumber" multiple="false" /></tr>
                                    </table> </td><td>
                                    <table>
                                        <tr><agiletags:LabelNeoFilteredLookupValueDropDown displayName="Entity Type" elementName="entitytype" lookupId="15"/></tr>
                                        <tr> <td><label style="display: none;" id="memberType">Member Type:</label></td>
                                            <td><input type="text" id="memberNumber" name="memberNumber" size="32"  style="display: none;"/></td></tr>
                                        <tr><agiletags:DocumentIndexDropdownType displayName="Document Type" elementName="docType"/></tr>
                                    </table>
                                </td></tr>
                            </agiletags:ControllerForm>
                    </table>
                </td></tr></table>

    </body>
</html>


<%-- 
    Document   : LeftMenu
    Created on : 2009/09/28, 11:36:57
    Author     : gerritj
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- <script  src="./resources/dhtmlxcommon.js"></script>
        <script  src="./resources/dhtmlxtabbar.js"></script>
        <script  src="./resources/dhtmlxtabbar_start.js"></script>
<link rel="STYLESHEET" type="text/css" href="./resources/dhtmlxtabbar.css"> -->
        <link rel="stylesheet" href="./resources/treeMenu/jquery.treeview.css" />
        <script type="text/javascript" src="./resources/treeMenu/jquery.min.js"></script>
        <script src="./resources/treeMenu/jquery.cookie.js" type="text/javascript"></script>
        <script src="./resources/treeMenu/jquery.treeview.js" type="text/javascript"></script>
        <script type="text/javascript" src="./resources/treeMenu/demo.js"></script>



    </head>
    <body>
        <!--<h3>Menu:</h3>

        <div id="treecontrol">
            <a title="Collapse the entire tree below" href="#"> Collapse All</a> |
            <a title="Expand the entire tree below" href="#"> Expand All</a> |
            <a title="Toggle the tree below, opening closed branches, closing open branches" href="#">Toggle All</a>
        </div>

        <ul id="red" class="treeview-red">
            <li><span>Entity</span>
                <ul>
                    <li><span>Create</span></li>

                    
                </ul>
            </li>
            <li><span>Product</span>

                <ul>

                    <li><span>Benifit</span>
                        <ul>
                            <li><span>Add</span>
                                <ul>
                                    <li><span><a href="">Something</a></span></li>
                            </ul></li>
                            <li><span>Update</span></li>
                            <li><span>Daelete</span></li>
                            <li><span>Insert</span></li>
                        </ul>
                    </li>


                </ul>
            </li>
        </ul>
        -->
        <!--<div id="a_tabbar" style="width:270px; height:490px;">

        <script>

            tabbar=new dhtmlXTabBar("a_tabbar","top");

            tabbar.setSkin('dhx_skyblue');
            tabbar.setImagePath("./resources/");
            tabbar.setAlign("left");
            tabbar.enableTabCloseButton(true);
            tabbar.addTab("a1","Entity","110px");
            tabbar.addTab("a2","Claims","110px");
            tabbar.addTab("a3","Product","110px");
            tabbar.setTabActive("a1");

        </script>
        -->
        <form action="/ManagedCare/AgileController" name="leftMenu">
            <input type="hidden" name="opperation" value="LeftMenuCommand" size="30" />
            <input type="hidden" name="application" value="0" size="30" />
        </form>
        <c:if test="${applicationScope.Client == 'Sechaba'}">
            <img src="/ManagedCare/resources/KOH_powered by.jpg" alt="PowerdBy" style="bottom: 0; position: absolute; z-index: -1">
        </c:if>
        <c:if test="${applicationScope.Client == 'Agility'}">
            <img src="/ManagedCare/resources/KO_powered by.jpg" alt="PowerdBy" style="bottom: 0; position: absolute; z-index: -1">
        </c:if>
        
    </body>
</html>

<%-- 
    Document   : Claims_PaymentRun
    Created on : 13 Jun 2016, 10:18:08 AM
    Author     : janf
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script> 
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Automation/Automation.js"></script>
        <script type="text/javascript">

        </script>
    </head>
    <body>
        <div class="neonotification"></div>
        <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">
                    <!--<label class="header">Claim Search</label>-->
                    <br/><br/>
                    <table>
                        <agiletags:ControllerForm name="ClaimsPaymentRun">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="PaymentRunSummary" id="PaymentRunSummary" value="${PaymentRunSummary}" />

                            <tr>
                                <nt:NeoTextField valueFromSession="true" displayName="Process Date From" elementName="processFrom" type="date" mandatory="yes"/>
                                <nt:NeoTextField valueFromSession="true" displayName="Process Date To" elementName="processTo" type="date" mandatory="yes"/>
                            </tr>

                            <tr>
                                <agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Scheme"  elementName="productId" lookupId="product" mandatory="yes"/>
                            </tr>

                            <c:if test="${userRestriction != true}">
                                <tr><agiletags:ButtonOpperation align="left" span="1" type="button" commandName="" displayname="Run" javaScript="onClick=\"paymentRun(this);\""/></tr>
                            </c:if>
                        </agiletags:ControllerForm>
                    </table>
                    <br><br>
                    <table>
                        <tr><nt:NeoTextField elementName="paymentRunLines" displayName="Payment line counts to be made" disabled="disabled" /></tr>
                        <tr><nt:NeoTextField elementName="paymentRunValue" displayName="Total payment run value" disabled="disabled" /></tr>
                    </table>
                    <nt:NeoPanel title="Payment Run Summary"  collapsed="false" reload="true" contentClass="neopanelcontentwhite">
                        <div id="PaymentRun_Summary">
                            <jsp:include page="PaymentRun_DetailSummary.jsp" />
                        </div>  
                    </nt:NeoPanel>
                    <table>
                        <tr>
                            <agiletags:ButtonOpperationLabelError displayName="Export" elementName="exportRun" type="button" javaScript="onClick=\"exportRun(this);\""/>
                            <agiletags:ButtonOpperationLabelError displayName="Approve Payment Run" elementName="approveRun" type="button" javaScript="onClick=\"approvePaymentRun(this);\""/>
                        </tr>
                    </table>
                    <c:if test="${userRestriction == true}"><span class="neonotificationnormal neonotificationwarning">Content contained is confidential. Please refer to Team leader for assistance</span></c:if>
                </td></tr></table>
    </body>
</html>


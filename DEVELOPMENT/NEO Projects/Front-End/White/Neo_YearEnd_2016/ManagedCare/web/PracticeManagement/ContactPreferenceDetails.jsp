<%-- 
    Document   : ContactPreferenceDetails
    Created on : 2012/05/30, 04:07:16
    Author     : johanl
--%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<label class="subheader">Contact Preference Details</label>
<table id="ContactDetailsTable">
    <tr id="MemberDetails_claimProcRow"><agiletags:LabelNeoConPrefDropdownReq commType="1" displayName="Claim Processing" elementName="ConPrefDetails_claimProc" javaScript="" /></tr>
<tr id="MemberDetails_contriStmtRow"><agiletags:LabelNeoConPrefDropdownReq commType="2" displayName="Contributions Statement" elementName="ConPrefDetails_contriStmt" javaScript="" /></tr>
<tr id="MemberDetails_claimStmtRow"><agiletags:LabelNeoConPrefDropdownReq commType="3" displayName="Claim Statement" elementName="ConPrefDetails_claimStmt" javaScript="" /></tr>
<tr id="MemberDetails_claimPayRow"><agiletags:LabelNeoConPrefDropdownReq commType="4" displayName="Claim Payment" elementName="ConPrefDetails_claimPay" javaScript="" /></tr>
<tr id="MemberDetails_taxCertRow"><agiletags:LabelNeoConPrefDropdownReq commType="5" displayName="Tax Certificate" elementName="ConPrefDetails_taxCert" javaScript="" /></tr>
<tr id="MemberDetails_preAuthRow"><agiletags:LabelNeoConPrefDropdownReq commType="6" displayName="Pre-Auths" elementName="ConPrefDetails_preauth" javaScript="" /></tr>
<tr id="MemberDetails_otherRow"><agiletags:LabelNeoConPrefDropdownReq commType="7" displayName="Other" elementName="ConPrefDetails_other" javaScript="" /></tr>
</table>
<br/>
<table id="MemberPersonalSaveTable">
    <tr>
        <td><input type="reset" value="Reset"></td>
        <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'ContactPrefPage')"></td>
    </tr>
</table>      

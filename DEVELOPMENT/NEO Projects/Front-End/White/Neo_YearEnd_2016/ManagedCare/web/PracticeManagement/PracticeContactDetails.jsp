<%-- 
    Document   : PracticeContactDetails
    Created on : 04 Mar 2014, 11:17:11 AM
    Author     : almaries
--%>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<div id="tabTableLayout">
    <agiletags:ControllerForm name="PracticeContactDetailsForm">
        <input type="hidden" name="opperation" id="PracticeContact_opperation" value="SavePracticeContactDetailsCommand" />
        <input type="hidden" name="onScreen" id="PracticeContact_onScreen" value="PracticeContactDetails" />
        <div id="ContactDetailsPage"><jsp:include page="ContactDetails.jsp"/></div>
    </agiletags:ControllerForm>  

</div>


<%-- 
    Document   : ContactDetails
    Created on : 2012/05/30, 04:06:55
    Author     : johanl
--%>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<label class="subheader">Member Contact Details</label>
<HR WIDTH="80%" align="left">
<br/>
<label class="subheader">Contact Details</label>
<table id="ContactDetailsTable">
    <tr id="MemberDetails_emailRow"><agiletags:LabelTextBoxErrorReq displayName="Email Address" elementName="ContactDetails_email" valueFromRequest="ContactDetails_email" mandatory="no"/>        </tr>
    <tr id="MemberDetails_faxRow"><agiletags:LabelTextBoxErrorReq displayName="Fax Number" elementName="ContactDetails_faxNo" valueFromRequest="ContactDetails_faxNo" mandatory="no"/>        </tr>
    <tr id="MemberDetails_cellRow"><agiletags:LabelTextBoxErrorReq displayName="Cell phone Number" elementName="ContactDetails_cell" valueFromRequest="ContactDetails_cell" mandatory="no"/>        </tr>
    <tr id="MemberDetails_telWorkRow"><agiletags:LabelTextBoxErrorReq displayName="Telephone Number(Work)" elementName="ContactDetails_telNoWork" valueFromRequest="ContactDetails_telNoWork" mandatory="no"/>        </tr>
    <tr id="MemberDetails_telHomeRow"><agiletags:LabelTextBoxErrorReq displayName="Telephone Number(Home)" elementName="ContactDetails_telNoHome" valueFromRequest="ContactDetails_telNoHome" mandatory="no"/>        </tr>
</table>
<br/>
<table id="MemberPersonalSaveTable">
    <tr>
        <td><input type="reset" value="Reset"></td>
        <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'ContactPrefPage')"></td>
    </tr>
</table>       


<%-- 
    Document   : PracticeNotes
    Created on : Jul 28, 2016, 2:48:03 PM
    Author     : janf
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
    <body onload="fetchPracticeNotes()">
        <div>
            <div id="Practice_notes_details">
                <agiletags:ControllerForm name="ProviderNotesForm" >
                    <input type="hidden" name="opperation" id="opperation" value="PracticeManagementNotesCommand" />
                    <input type="hidden" name="onScreen" id="onScreen" value="PracticeNotes" />
                    <input type="hidden" name="entityId" id="entityId" value="${entityId}" />
                    <hr>
                    <label class="subheader">Practice Notes Details</label>
                    <div id="PracticeNotes_Summary">
                        <jsp:include page="PracticeNotesSummary.jsp" />
                    </div> 
                    <table>
                        <td><input type="button" name="AddNotesButton" value="Add Note" onclick="addPracticeNote();"/></td>
                    </table>
                </agiletags:ControllerForm>
            </div>
            <div id="Practice_Notes_Add_Div" style="display:none;">

            </div>
            <div  id="Practice_Notes_View_Div" style="display:none;">
                <agiletags:ControllerForm name="ViewPracticeNotesForm">
                    <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                                <label class="header">View Note</label>
                                <br/>
                                <hr>
                                <table>
                                    <input type="hidden" name="opperation" id="opperation" value="PracticeManagementNotesCommand" />
                                    <input type="hidden" name="onScreen" id="onScreen" value="addNote" />
                                    <input type="hidden" name="command" id="command" value="SaveNote" />
                                    <input type="hidden" name="entityId" id="entityId" value="${entityId}"/>

                                    <tr>
                                        <td align="left" width="160px"><label>Note:</label></td>
                                        <td align="left">
                                            <label id="noteDetails"></label>
                                        </td>
                                    </tr>
                                    <br/>
                                </table>
                                <hr>
                        <tr><input type="button" value="Close" onclick="swapDivVisbible('Practice_Notes_View_Div', 'Practice_notes_details');"></tr>

                        </td></tr></table>
                    </agiletags:ControllerForm>
            </div>
        </div>
    </body>
<%-- 
    Document   : PracticeNotesSummary
    Created on : Jul 28, 2016, 2:52:48 PM
    Author     : janf
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>

<nt:NeoPanel title="Notes for Practice"  collapsed="false" reload="false">
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>User</th>
                <th>Note</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="item" items="${PracticeNotes}">
                <tr>
                    <td><label>${item.creationDate}</label></td>
                    <td><label>${item.userName}</label></td>
                    <td><label>${item.shortNote}</label></td>
                    <td><input type="button" value="View" onclick="viewPracticeNote(${item.noteId})"></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</nt:NeoPanel>   
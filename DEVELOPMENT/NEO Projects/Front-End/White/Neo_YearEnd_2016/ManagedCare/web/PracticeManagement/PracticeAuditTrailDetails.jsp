<%-- 
    Document   : PracticeAuditTrailDetails
    Created on : 11 Mar 2014, 10:42:56 AM
    Author     : almaries
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div id="tabTableLayout">
    <br>
    <label class="subheader">Practice Audit Trail Details</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty PracticeAuditDetails}">
                <span class="label">No Audit details available</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <c:forEach var="headerMap" items="${PracticeAuditDetails[0]}">
                            <th>${headerMap.key}</th>
                        </c:forEach>           
                    </tr>
                    <c:forEach var="entry" items="${PracticeAuditDetails}">
                        <tr>
                            <c:forEach var="item" items="${entry}">
                                <td><label>${item.value}</label></td>
                            </c:forEach>
                        </tr>
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>
</div>

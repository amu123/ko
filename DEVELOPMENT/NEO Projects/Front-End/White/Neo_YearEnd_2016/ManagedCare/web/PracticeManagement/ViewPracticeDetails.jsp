<%-- 
    Document   : ViewPracticeDetails
    Created on : 06 Mar 2014, 11:58:19 AM
    Author     : almaries
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
<agiletags:ControllerForm name="MemberApplicationForm">
    <input type="hidden" name="opperation" id="MemberApp_opperation" value="SaveBrokerFirmBankingDetails" />
    <input type="hidden" name="onScreen" id="MemberApp_onScreen" value="Member Application" />
    <input type="hidden" name="commonEntityId" id="commonEntityId" value="${requestScope.PracticeDetails_entityCommId}" />
    <input type="hidden" name="practiceTypeId" id="practiceTypeId" value="${requestScope.PracticeDetails_practiceType}" />
    <input type="hidden" name="oldRestriction" id="oldRestriction" value="${requestScope.oldRestriction}" />
    
    <br/>
    <label class="subheader">Practice Details</label>
    <hr>
    <table id="BrokerFirmBankingTable">        
        <tr id="PracticeDetails_disciplineRow"><agiletags:LabelTextBoxErrorReq displayName="Discipline" elementName="PracticeDetails_discipline" valueFromRequest="PracticeDetails_discipline" mandatory="yes" readonly="yes" />         </tr>        
        <tr id="PracticeDetails_subDisciplineRow"><agiletags:LabelTextBoxErrorReq displayName="Sub Discipline"  elementName="PracticeDetails_subDiscipline" valueFromRequest="PracticeDetails_subDiscipline" mandatory="yes" readonly="yes" />        </tr>
        <tr id="PracticeDetails_practiceNoRow"><agiletags:LabelTextBoxErrorReq displayName="Practice Number" elementName="PracticeDetails_practiceNo" valueFromRequest="PracticeDetails_practiceNo" mandatory="yes" readonly="yes" />        </tr>
        <tr id="PracticeDetails_providerNoRow"><agiletags:LabelTextBoxErrorReq displayName="Provider Number" elementName="PracticeDetails_providerNo" valueFromRequest="PracticeDetails_providerNo" mandatory="yes" readonly="yes" />        </tr>
        <tr id="PracticeDetails_practiceNameRow">
        <c:choose>
            <c:when test="${sessionScope.PracticeDetails_practiceName != null}">
                <agiletags:LabelTextBoxErrorReq displayName="Practice Name" elementName="PracticeDetails_practiceName" valueFromSession="yes" mandatory="yes" javaScript="onChange=\"nameChanged();\""/>     
            </c:when>
            <c:otherwise>
                <agiletags:LabelTextBoxErrorReq displayName="Practice Name" elementName="PracticeDetails_practiceName" valueFromRequest="PracticeDetails_practiceName" mandatory="yes" javaScript="onChange=\"nameChanged();\""/>     
            </c:otherwise>
        </c:choose>            
        </tr>   
        <tr id="PracticeDetails_groupIndRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Group Practice Ind" elementName="PracticeDetails_groupInd" errorValueFromSession="yes" mandatory="yes" lookupId="67"/>        </tr> 
        <tr id="PracticeDetails_numberOfPartnersRow"><agiletags:LabelTextBoxErrorReq displayName="Number Of Partners" elementName="PracticeDetails_numberOfPartners" valueFromRequest="PracticeDetails_numberOfPartners" mandatory="yes" readonly="yes" />        </tr> 
        <tr id="PracticeDetails_effectiveDateRow"><agiletags:LabelTextBoxErrorReq displayName="Effective Date" elementName="PracticeDetails_effectiveDate" valueFromRequest="PracticeDetails_effectiveDate" mandatory="yes" readonly="yes" />        </tr> 
        <tr id="PracticeDetails_cashPracticeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Restrictions" elementName="PracticeDetails_cashPractice" errorValueFromSession="yes" mandatory="yes" lookupId="356" javaScript="onChange=\"restrictionChange();\""/>        </tr> 
    </table>
    <br/>
    <label class="subheader">Provider Details</label>
    <hr>
    <table id="BrokerFirmBankingTable">        
        <tr id="PracticeDetails_councilNoRow"><agiletags:LabelTextBoxErrorReq displayName="Council No"  elementName="PracticeDetails_councilNo" valueFromRequest="PracticeDetails_councilNo" mandatory="yes"/>        </tr>
        <tr id="PracticeDetails_providerTitleRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Title"  elementName="PracticeDetails_providerTitle"  mandatory="yes" errorValueFromSession="yes" lookupId="24"/>         </tr>        
        <tr id="PracticeDetails_providerInitialsRow"><agiletags:LabelTextBoxErrorReq displayName="Initials"  elementName="PracticeDetails_providerInitials" valueFromRequest="PracticeDetails_providerInitials" mandatory="yes"/>        </tr>
        <tr id="PracticeDetails_providerFirstNameRow"><agiletags:LabelTextBoxErrorReq displayName="First Name" elementName="PracticeDetails_providerFirstName" valueFromRequest="PracticeDetails_providerFirstName" mandatory="yes"/>        </tr>
        <tr id="PracticeDetails_providerSurnameRow"><agiletags:LabelTextBoxErrorReq displayName="Surname" elementName="PracticeDetails_providerSurname" valueFromRequest="PracticeDetails_providerSurname" mandatory="yes"/>        </tr>   
        <tr id="PracticeDetails_providerIdNoRow"><agiletags:LabelTextBoxErrorReq displayName="Id Number" elementName="PracticeDetails_providerIdNo" valueFromRequest="PracticeDetails_providerIdNo" mandatory="yes"/>        </tr> 
    </table>
    <br>
    <table id="tabTableLayout" align="left">
        <input type="button" value="Return" onclick="submitGenericReload('ReloadPreauthGeneric', '/PracticeManagement/ViewPractice.jsp');"></td>
        <input id="updateButton" type="button" value="Update" disabled onclick="submitWithAction('UpdatePracticeNameCommand');" />
   </table>
</agiletags:ControllerForm>
</td></tr></table>

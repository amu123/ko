<%-- 
    Document   : PracticeBankingDetails
    Created on : 04 Mar 2014, 11:18:17 AM
    Author     : almaries
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<div id="tabTableLayout">
    <agiletags:ControllerForm name="MemberBankingDetailsForm">
        <input type="hidden" name="opperation" id="PracticeBanking_opperation" value="SavePracticeBankingDetailsCommand" />
        <input type="hidden" name="onScreen" id="PracticeBanking_onScreen" value="PracticeBankingDetails" />

        <label class="subheader">Practice Banking Details</label>
        <HR WIDTH="80%" align="left">
        <br/>
        <table id="ContribTable">
            <tr id="BankingDetails_bankNameRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Name of Bank"  elementName="BankingDetails_bankName" lookupId="bank" mandatory="yes" errorValueFromSession="yes" javaScript="onChange=\"toggleBranch(this.value, 'BankingDetails_bankBranchId')\""/>        </tr>

            <tr id="BankingDetails_bankBranchIdRow"><agiletags:LabelNeoBranchDropDownErrorReq displayName="Branch"  elementName="BankingDetails_bankBranchId"  mandatory="yes" errorValueFromSession="yes" parentElement="BankingDetails_bankName"/>        </tr>
            <tr id="BankingDetails_accountTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Account Type"  elementName="BankingDetails_accountType" lookupId="37" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <tr id="BankingDetails_accountHolderRow"><agiletags:LabelTextBoxErrorReq displayName="Account Holder" elementName="BankingDetails_accountHolder" valueFromRequest="BankingDetails_accountHolder" mandatory="no"/>        </tr>
            <tr id="MemberDetails_accountNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Account Number" elementName="BankingDetails_accountNumber" valueFromRequest="BankingDetails_accountNumber" mandatory="no"/>        </tr>
            <tr id="MemberDetails_paymentMethodRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Payment Method"  elementName="BankingDetails_paymentMethod" lookupId="38" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <%--<c:choose>--%>
                <%--<c:when test="${BankingDetails_Cash_Practice_checked || BankingDetails_Cash_Practice_checked == 'true'}">--%>
<!--                    <tr id="MemberDetails_Cash_Practice">
                        <td><label>Cash Practice</label></td>
                        <td><input type="checkbox" id="MemberDetails_Cash_Practice" name="BankingDetails_Cash_Practice" checked/></td>
                    </tr>-->
                <%--</c:when>--%>
                <%--<c:otherwise>--%>
<!--                    <tr id="MemberDetails_Cash_Practice">
                        <td><label>Cash Practice</label></td>
                        <td><input type="checkbox" id="MemberDetails_Cash_Practice" name="BankingDetails_Cash_Practice"/></td>
                    </tr>-->
                <%--</c:otherwise>--%>
            <%--</c:choose>--%>
        </table>
        <br/>
        <table id="MemberPersonalSaveTable">
            <tr>
                <td><input type="reset" value="Reset"></td>
                <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'PracticeBankingDetails')"></td>
            </tr>
        </table>

    </agiletags:ControllerForm>
</div>

<%-- 
    Document   : PracticeDetails
    Created on : 05 Mar 2014, 11:54:28 AM
    Author     : almaries
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>


        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Membership/MemberMaintenance.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/ckeditor/ckeditor.js"></script>

        <style>
            #header { position: fixed; top: 0; left: 8px; right: 8px; z-index: 999;background: #fff;}
            #content {position: relative; margin-top: 100px;}
        </style>
        <script>
            $(document).ready(function() {
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });
            });

            function nameChanged(){                    // Check PracticeDetails_practiceName
               var name = "${sessionScope.provNumName}"; 
               var newName = document.getElementById('PracticeDetails_practiceName').value;
               document.getElementById('newPracticeName').value = newName;
               var flag = null;
               if(name !== newName){
                   flag = true;
                   $('#updateButton').removeAttr('disabled');
               } else {
                   flag = false;
                   $('#updateButton').attr('disabled', 'disabled');
               }
            }
            
            function restrictionChange(){
                var restriction = document.getElementById('oldRestriction').value;;
                var newRestriction = document.getElementById('PracticeDetails_cashPractice').value;
                document.getElementById('newRestriction').value = newRestriction;
                document.getElementById('oldRestriction').value = restriction;
                console.log("old = " + restriction + " - NEW = " + newRestriction);
                if(restriction !== newRestriction){
                    $('#updateButton').removeAttr('disabled');
                } else {
                    $('#updateButton').attr('disabled', 'disabled');
                }
            }
            
            function resizeContent() {
                $('#content').css('margin-top', $('#header').height());
            }
            
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;                                
                document.getElementById('commonEntityId1').value = document.getElementById('commonEntityId').value; // send request values
                document.getElementById('practiceTypeId1').value = document.getElementById('practiceTypeId').value; // from ViewPracticeDetails.jsp
                document.forms[0].submit();
            }

        </script>
    </head>
    <body onload="initAgileTabs();">

    <agiletags:ControllerForm name="TabControllerForm" >
        <input type="hidden" name="opperation" id="opperation" value="PracticeManagementTabContentCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="" />
        <input type="hidden" name="provNumEntityId" id="provNumEntityId" value="${sessionScope.provNumEntityId}" />  
        <input type="hidden" name="newPracticeName" id="newPracticeName" value="" />
        <input type="hidden" name="newRestriction" id="newRestriction" value="" />
        <input type="hidden" name="oldRestriction" id="oldRestriction" value="" />
        <input type="hidden" name="commonEntityId1" id="commonEntityId1" value="abc" />
        <input type="hidden" name="practiceTypeId1" id="practiceTypeId1" value="abc" />
        
    </agiletags:ControllerForm>
    <div id="main_div">
        <div id="header">
            <ol id="tabs" >
                <li><a href="#PracticeDetails"  >Practice Details</a></li>
                <li><a href="#PracticeAddressDetails"  >Address Details</a></li>
                <li><a href="#PracticeBankingDetails"  >Banking Details</a></li>
                <li><a href="#PracticeContactDetails"  >Contact Details</a></li>
                <li><a href="#PracticeNotes" >Notes</a></li>
                <li><a href="#AuditTrail"  >Audit Trail</a></li>
            </ol>
            <table width="100%" bgcolor="#dddddd"  style=" border: 1px solid #c9c3ba;">
                <tr>
                    <td width="375"><span ><label><b>Practice Number:</b></label> <label id="member_header_memberName" >${sessionScope.provNum}</label></span></td>
                    <td class="label"><b>Practice Name:</b> ${sessionScope.provNumName}</td>
                    <td class="label"><b>Network:</b> ${sessionScope.provNumNetwork}</td>
                    <td><label></label></td>
                </tr>
            </table>
        </div>

        <div id="content">
            <div class="tabContent" id="PracticeDetails">Loading...</div>
            <div class="tabContent hide" id="PracticeAddressDetails">Loading...</div>
            <div class="tabContent hide" id="PracticeBankingDetails">Loading...</div>
            <div class="tabContent hide" id="PracticeContactDetails">Loading...</div>
            <div class="tabContent hide" id="AuditTrail">Loading...</div>
            <div class="tabContent hide" id="PracticeNotes">Loading...</div>
        </div>
            <br/>       
    <div id="overlay" style="display:none;"></div>
    <div id="overlay2" style="display:none;"></div>
</body>
</html>

<%-- 
    Document   : GenericFileLoader
    Created on : 2011/01/06, 11:03:21
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.tools.min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>

<script language="JavaScript">

    $(document).ready(function() {
        $("#dialog-uploaded").hide();
        $("#dialog-not-uploaded").hide();

        displayDialog();
        displayError();
    });

    function submitWithAction(action) {

        alert("submitWithAction");
        document.getElementById('opperation').value = action;
        document.forms[0].submit();
    }

    function displayDialog() {
            <%  if ((session.getAttribute("stagingStatusSuccess")) != null && (!session.getAttribute("stagingStatusSuccess").equals("null"))) {
            %>

                    $("#dialog-uploaded").dialog({ modal: true, width:550,
                        buttons: {
                            'Done': function() {
                                $(this).dialog('close');
                            }
                        }
                    });
                    <%
            session.setAttribute("stagingStatusSuccess", null);
        }%>
            }

            function displayError() {
            <%  if ((session.getAttribute("stagingStatusError")) != null && (!session.getAttribute("stagingStatusError").equals("null"))) {
            %>

                    $("#dialog-not-uploaded").dialog({ modal: true, width:550,
                        buttons: {
                            'Done': function() {
                                $(this).dialog('close');
                            }
                        }
                    });
                    <%
            session.setAttribute("stagingStatusError", null);
        }%>

            }
                
</script>
    </head>
    <body>
        <div id="dialog-uploaded" title="File Uploaded"><p>File Uploaded successfully</p></div>
        <div id="dialog-not-uploaded" title="File Not Uploaded"><label class="error">File not uploaded, check the errors!</label></div>


       <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">

       <label class="header">Upload File</label>
       <table>
           <agiletags:MultipartControllerForm name="DataLoader">
               <input type="hidden" name="opperation" id="opperation" value=""/>
               <tr><agiletags:LabelFileBrowseError displayName="Load File" elementName="filename" valueFromSession="yes" /></tr>

               <tr>
                   <agiletags:ButtonOpperation align="left" commandName="CancelUploadCommand" displayname="Cancel" span="1" type="submit"  javaScript="onClick=\"submitWithAction('CancelUploadCommand')"/>
                   <agiletags:ButtonOpperation align="left" commandName="LoadDataFileCommand" displayname="Upload" span="1"  type="submit" />
               </tr>

               </agiletags:MultipartControllerForm>
       </table>
       </td></tr></table>
    </body>
</html>

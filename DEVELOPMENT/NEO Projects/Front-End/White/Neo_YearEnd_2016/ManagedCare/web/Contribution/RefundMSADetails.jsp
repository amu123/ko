<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions' %>
<label class="header">Refund MSA Balance</label>
<hr>
<br>
<table>
    <tr>
        <td width="160"><label>Member Number:</label></td>
        <td><label> ${coverNo}</label></td>
    </tr>
    <tr>
        <td><label>Member Name:</label></td>
        <td><label> ${memberName} ${memberSurname}</label></td>
    </tr>
</table>
<br>
<c:choose>
    <c:when test="${empty savingsLimits}">
        <span><label>No savings details for this member</label></span>
        <br>
        <input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}', '${main_div}');">
    </c:when>
    <c:when test="${empty hasSavingsDue}">
        <span><label>No savings refund due for this member</label></span>
        <br>
        <input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}', '${main_div}');">
    </c:when>
    <c:otherwise>
        <label class="subheader">Medical Savings Account</label>
        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" >
            <tr>
                <th>Option</th>
                <th>Benefit</th>
                    <c:if test="${!empty hasInt}">
                    <th>Interest</th>
                    </c:if>
                    <c:if test="${!empty hasJnl}">
                    <th>Journals</th>
                    </c:if>
                <th>Claims Used</th>
                    <c:if test="${!empty hasClaim}">
                    <th>Claims Recovered</th>
                    </c:if>
                    <c:if test="${!empty hasSav}">
                    <th>Savings Refunded</th>
                    </c:if>
                    <c:if test="${!empty hasContrib}">
                    <th>Contributions Recovered</th>
                    </c:if>                        
                <th>Balance</th>
                <th></th>
            </tr>
            <c:forEach var="sav" items="${savingsLimits}">
                <tr>
                    <td><label>${sav.optionName}</label></td>
                    <td align="right"><label>${agiletags:formatStringAmount(sav.available)}</label></td>
                            <c:if test="${!empty hasInt}">
                        <td align="right"><label>${agiletags:formatStringAmount(sav.interest)}</label></td>
                            </c:if>
                            <c:if test="${!empty hasJnl}">
                        <td align="right"><label>${agiletags:formatStringAmount(sav.jnl)}</label></td>
                            </c:if>
                    <td align="right"><label>${agiletags:formatStringAmount(sav.claimTotal)}</label></td>
                            <c:if test="${!empty hasClaim}">
                        <td align="right"><label>${agiletags:formatStringAmount(sav.claimsRecovered)}</label></td>
                            </c:if>
                            <c:if test="${!empty hasSav}">
                        <td align="right"><label>${agiletags:formatStringAmount(sav.savingsPaid)}</label></td>
                            </c:if>
                            <c:if test="${!empty hasContrib}">
                        <td align="right"><label>${agiletags:formatStringAmount(sav.contribRecovered)}</label></td>
                            </c:if>                        
                    <td align="right"><label>${agiletags:formatStringAmount(sav.balance)}</label></td>
                            <c:if test="${sav.id == '0'}">
                        <td><input type="button" value="Refund" onclick="setRefundVals(${sav.optionId}, '${sav.optionName}',${sav.balance});"></td>
                        </c:if>
                </tr>
            </c:forEach>
        </table>
        <table>
            <c:if test="${empty hasPosBalance}">
                <tr><td><input type="button" value="Return" onclick="swapDivVisbible('${target_div}', '${main_div}');"></td></tr>
                    </c:if>
        </table>
        <br>

        <div id="refund_div" style="display:none">
            <table>
                <input type="hidden" name="refundOptionId" id="refundOptionId" value="">
                <input type="hidden" name="refundAmount" id="refundAmount" value="">
                <tr><agiletags:LabelTextBoxError displayName="Option" elementName="refundOption" readonly="yes" valueFromSession="yes"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="BI Reference Number" elementName="biRefNum" mandatory="yes" valueFromSession="yes"/></tr>
                <tr><agiletags:LabelTextBoxDate displayname="Refund Date" elementName="refundDate" mandatory="yes"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="Claims Recovered Amount" elementName="refundClaims" javaScript="onBlur='calculateRefundVal()'"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="Savings Refund Amount" elementName="refundSavings" readonly="yes" valueFromSession="yes" mandatory="yes"/></tr>
                <tr>
                    <td><input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}', '${main_div}');"></td>
                    <td><input type="button" value="Save" onclick="saveRefund(this);"> </td>
                </tr>
            </table>
        </div>

    </c:otherwise>
</c:choose>





<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script>
            function transferDOAlloc(contribTranId) {
                document.getElementById('TransferReverseCommand').value = 'TransferDOAlloc';
                setTransferDOFields(contribTranId);
                formVal = document.getElementById("PaymentAllocationForm");
                getPageContents(formVal, null, 'main_div', 'overlay');
            }
            
            function saveTransferDOAlloc(btn) {
                btn.disabled = true;
                document.getElementById('TransferReverseCommand').value = 'SaveDO' + btn.value;
                document.getElementById('transferReverseValue').value = document.getElementById('transferReverseVal').value;
                formVal = document.getElementById("PaymentAllocationForm");
                var params = encodeFormForSubmit(formVal);
                $(formVal).mask("Saving...");
                $.post(formVal.action, params,function(result){
                    updateFormFromData(result);
                    $(formVal).unmask();
                    if (document.getElementById('transferReverseStatus').value == 'ok') {
                        contribTranId = document.getElementById('contribTranId').value;
                        swapDivVisbible('overlay','main_div');
                        document.getElementById('btnTransfer_' + contribTranId).disabled = true;
                    } else {
                        btn.disabled = false;
                    }
                });
            }
            
            function setTransferDOFields(contribTranId) {
                document.getElementById('entityId').value = document.getElementById('entityId_'+contribTranId).value;
                document.getElementById('optionId').value = document.getElementById('optionId_'+contribTranId).value;
                document.getElementById('entityTypeId').value = document.getElementById('entityTypeId_'+contribTranId).value;
                document.getElementById('contribDate').value = document.getElementById('contribDate_'+contribTranId).value;
                document.getElementById('paymentNarrative').value = document.getElementById('paymentNarrative_'+contribTranId).value;
                document.getElementById('amount').value = document.getElementById('amount_'+contribTranId).value;
                document.getElementById('contribTranId').value = document.getElementById('contribTranId_'+contribTranId).value;
            }
            
        </script>
    </head>
    <body>
        <div id="main_div" >
     <agiletags:ControllerForm name="PaymentAllocationForm">
        <input type="hidden" name="opperation" id="opperation" value="TransferReversePayAllocCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="TransferReversePayAlloc" />
        <input type="hidden" name="command" id="TransferReverseCommand" value="" />
        <input type="hidden" name="coverNumber" id="coverNumber" value="${coverNo}" />
        <input type="hidden" name="entityId" id="entityId" value="${entityId}" />
        <input type="hidden" name="optionId" id="optionId" value="" />
        <input type="hidden" name="memberName" id="memberName" value="${memberName}" />
        <input type="hidden" name="memberSurname" id="memberSurname" value="${memberSurname}" />
        <input type="hidden" name="entityTypeId" id="entityTypeId" value="" />
        <input type="hidden" name="contribDate" id="contribDate" value="" />
        <input type="hidden" name="paymentNarrative" id="paymentNarrative" value="" />
        <input type="hidden" name="statementDate" id="statementDate" value="" />
        <input type="hidden" name="statementDescription" id="statementDescription" value="" />
        <input type="hidden" name="amount" id="amount" value="" />
        <input type="hidden" name="contribTranId" id="contribTranId" value="">
        <input type="hidden" name="contribReceiptsId" id="contribReceiptsId" value="">
        <input type="hidden" name="statementId" id="statementId" value="">
        <input type="hidden" name="statementLineId" id="statementLineId" value="">
        <input type="hidden" name="transferReverseValue" id="transferReverseValue" value="">
        <input type="hidden" name="transferReverseStatus" id="transferReverseStatus" value="">
     </agiletags:ControllerForm>
        
    <label class="header">Transfer Debit Order Allocation</label>
    <hr>
    <table>
        <tr>
            <td width="160"><label>Member Number:</label></td>
            <td><label> ${coverNo}</label></td>
        </tr>
        <tr>
            <td><label>Member Name:</label></td>
            <td><label> ${memberName} ${memberSurname}</label></td>
        </tr>
    </table>
        <br>
        <div id="PageContents" >
            <c:if test="${empty memberPayAlloc}">
                <Label>No Debit Order to Transfer</Label>
            </c:if>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Contribution Period</th>
                        <th>Payment Narrative</th>
                        <th>Debit Order Date</th>
                        <th>Amount</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${memberPayAlloc}">
                        <tr>
                            <td><label>${entry['contribDate']}</label></td>
                            <td><label>${entry['narrative']}</label></td>
                            <td><label>${entry['statementDate']}</label></td>
                            <td align="right"><label>${agiletags:formatStringAmount(entry['amount'])}</label></td>
                            <td><button type="button" id="btnTransfer_${entry['contribTranId']}" onclick="transferDOAlloc(${entry['contribTranId']});">Transfer</button></td>
                        <input type="hidden" id="contribTranId_${entry['contribTranId']}" value="${entry['contribTranId']}">
                        <input type="hidden" id="entityId_${entry['contribTranId']}" value="${entry['entityId']}">
                        <input type="hidden" id="optionId_${entry['contribTranId']}" value="${entry['optionId']}">
                        <input type="hidden" id="entityTypeId_${entry['contribTranId']}" value="${entry['entityTypeId']}">
                        <input type="hidden" id="statementDate_${entry['contribTranId']}" value="${entry['statementDate']}">
                        <input type="hidden" id="contribDate_${entry['contribTranId']}" value="${entry['contribDate']}">
                        <input type="hidden" id="paymentNarrative_${entry['contribTranId']}" value="${entry['narrative']}">
                        <input type="hidden" id="amount_${entry['contribTranId']}" value="${entry['amount']}">
                        </tr>
                    </c:forEach>
                </table>
        </div>
        <br>
        </div>
      <div id="overlay" style="display:none;"></div>
      <div id="overlay2" style="display:none;"></div>
    
    </body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty MemberSearchResults && empty EmployerSearchResults}">
            <span>No results found</span>
            </c:when>
            <c:otherwise>
                <c:choose>
                    <c:when test="${empty EmployerSearchResults}">
                        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                            <tr>
                                <th>Member Number</th>
                                <th>Initials</th>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>DOB</th>
                                <th>ID Number</th>
                                <th>Outstanding Amount</th>
                                <th>Unallocated Amount</th>
                                <th></th>
                            </tr>
                            <c:forEach var="entry" items="${MemberSearchResults}">
                                <tr>
                                    <td><label>${entry['coverNumber']}</label></td>
                                    <td><label>${entry['initials']}</label></td>
                                    <td><label>${entry['name']}</label></td>
                                    <td><label>${entry['surname']}</label></td>
                                    <td><label>${entry['DOB']}</label></td>
                                    <td><label>${entry['idNumber']}</label></td>
                                    <td><label>${agiletags:formatStringAmount(entry['outstandingAmount'])}</label></td>
                                    <td><label>${agiletags:formatStringAmount(entry['unallocatedAmount'])}</label></td>
                                    <td><input type="submit" value="Select" onclick="setMemVals('${entry['entityId']}','${entry['coverNumber']}', '${agiletags:escapeStr(entry['name'])}', '${agiletags:escapeStr(entry['surname'])}', '${entry['unallocatedAmount']}');"></td>
                                </tr>
                            </c:forEach>
                        </table>
                    </c:when>
                    <c:otherwise>
                        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                            <tr>
                                <th>Group Number</th>
                                <th>Name</th>
                                <th>Outstanding Amount</th>
                                <th>Unallocated Amount</th>
                                <th></th>
                            </tr>
                            <c:forEach var="entry2" items="${EmployerSearchResults}">
                                <tr>
                                    <td><label>${entry2['employerNumber']}</label></td>
                                    <td><label>${entry2['employerName']}</label></td>
                                    <td><label>${agiletags:formatStringAmount(entry2['outstandingAmount'])}</label></td>
                                    <td><label>${agiletags:formatStringAmount(entry2['unallocatedAmount'])}</label></td>
                                    <td><input type="submit" value="Select" onclick="setEmpVals('${entry2['entityId']}', '${agiletags:escapeStr(entry2['employerName'])}', '${entry2['employerNumber']}', '${entry2['unallocatedAmount']}')"></td>
                                </tr>
                            </c:forEach>
                        </table>
                    </c:otherwise>
                </c:choose>
            </c:otherwise>
        </c:choose>

   
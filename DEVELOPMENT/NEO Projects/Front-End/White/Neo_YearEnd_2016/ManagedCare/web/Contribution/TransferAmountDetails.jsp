<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
    <label class="header">Transfer Amount</label>
    <hr>
    <br>
    <table>
        <tr>
            <td width="160"><label>Member Number:</label></td>
            <td><label> ${coverNo}</label></td>
        </tr>
        <tr>
            <td><label>Member Name:</label></td>
            <td><label> ${memberName} ${memberSurname}</label></td>
        </tr>
    </table>
        <br>
            <c:if test="${empty MemberContribResults}">
                <label>No Contribution Periods with balances available</label>
            </c:if>    
            <c:if test="${!empty MemberContribResults}">
                <c:set var="posBalDropDown" value=""/>
                <c:set var="negBalCount" value="0"/>
                <c:forEach items="${MemberContribResults}" var="items">
                    <c:set var="balAmt" value="${items['BAL']*1.0}"/>
                    <c:if test="${balAmt gt 0}">
                        <c:set var="optVar" value="<option value='${fn:replace(items['CONTRIBUTION_DATE'], '/', '')}_${items['ENTITY_ID']}_${items['ENTITY_TYPE_ID']}_${items['OPTION_ID']}'>${items['CONTRIBUTION_DATE']} [${items['ENTITY_NAME']}] R${items['BAL']}</option>"/>
                        <c:set var="posBalDropDown" value="${posBalDropDown}${optVar}"/>
                    </c:if>
                    <c:if test="${balAmt lt 0}">
                         <c:set var="negBalCount" value="${negBalCount + 1}"/>
                    </c:if>
                </c:forEach>
                <c:if test="${negBalCount eq 0 || empty posBalDropDown}">
                    <label>No Contribution Periods with balances available</label>
                </c:if>
                <c:if test="${!(negBalCount eq 0 || empty posBalDropDown)}">
                    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                        <thead>
                            <tr>
                                <th>Contribution Date</th>
                                <th>Option</th>
                                <th>Entity</th>
                                <th>Balance</th>
                                <th>Transfer To</th>
                                <th>Transfer Amount</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${MemberContribResults}" var="items">
                                <c:set var="balAmt" value="${items['BAL']*1.0}"/>
                                <c:if test="${balAmt lt 0}">
                                    <tr>
                                        <td><label>${items['CONTRIBUTION_DATE']}</label></td>
                                        <td><label>${items['OPTION_NAME']}</label></td>
                                        <td><label>${items['ENTITY_NAME']}</label></td>
                                        <td align="right"><label>${agiletags:formatStringAmount(items['BAL'])}</label></td>
                                        <td><select style="width:200px;" name="contribFrom_${fn:replace(items['CONTRIBUTION_DATE'], '/', '')}_${items['ENTITY_ID']}_${items['ENTITY_TYPE_ID']}_${items['OPTION_ID']}">${posBalDropDown}</select></td>
                                        <td><input name="contribAmt_${fn:replace(items['CONTRIBUTION_DATE'], '/', '')}_${items['ENTITY_ID']}_${items['ENTITY_TYPE_ID']}_${items['OPTION_ID']}" onblur="validatetransferAmt(this);"></td>
                                        <td class="errortd"><label></label></td>
                                    </tr>
                                </c:if>
                            </c:forEach>
                        </tbody>
                    </table>
                    <br>
                    <input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}','${main_div}');">
                    <input type="button" value="Save" onclick="save(this);">
                    
                </c:if>
            </c:if>    

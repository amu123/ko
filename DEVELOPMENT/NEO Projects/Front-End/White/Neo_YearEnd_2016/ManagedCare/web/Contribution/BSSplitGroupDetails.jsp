<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div>
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th>Group Number</th>
            <th>Group Name</th>
            <th>Amount</th>
        </tr>
        <c:forEach var="entry" items="${SplitGroupList}">
            <tr>
            <input type="hidden" name="split_name_${entry.entityId}" value="${entry.employerName}">
            <input type="hidden" name="split_number_${entry.entityId}" value="${entry.employerNumber}">
                <td><label>${entry.employerNumber}</label></td>
                <td><label>${entry.employerName}</label></td>
                <td><input size="10" type="text" name="split_id_${entry.entityId}" id="contrib_${entry.entityId}" class="splitAlloc" onchange="changeValue(this)" value="${entry.amount}"></td>
            </tr>
        </c:forEach>
    </table>
</div>
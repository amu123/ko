<%--
    Document   : Employer Search
    Created on : 2012/03/16, 11:06:14
    Author     : yuganp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script>
            function validateAndSubmit(form, targetId, value) {
                var today = new Date();
                var dateStr = document.getElementById('Contribution_Date').value;
                var inputDate = new Date(dateStr);
                
                if(inputDate.getFullYear() !== today.getFullYear()) {
                    alert("Invalid Year!");                   
                } else {
                    var pastDate = new Date();
                    pastDate.setMonth(pastDate.getMonth() - 1);
                    var futureDate = new Date();
                    futureDate.setMonth(futureDate.getMonth() + 2);
                    
                    if(inputDate.getMonth() >= pastDate.getMonth() && inputDate.getMonth() <= futureDate.getMonth()) {
                        document.getElementById('contribCommand').value = 'genContrib'; 
                        submitFormWithAjaxPost(form, targetId, value);
                    } else {
                        alert("Invalid date selected!");   
                    }
                }
            }
        </script>
    </head>
    <body>

     <agiletags:ControllerForm name="EmployerSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="GenerateContributionCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="Contribution" />
        <input type="hidden" name="command" id="contribCommand" value="" />
    <label class="header">Generate Contributions</label>
    <hr>
    <br>
        <table>
            <tr><agiletags:LabelTextBoxDateReq  displayname="Contribution Date" elementName="Contribution_Date" valueFromSession="yes" mandatory="yes"/></tr>
        <tr><td colspan="2" align="right"><input type="button" value="Generate" id="generateBtn" onclick="validateAndSubmit(this.form, null, this)"></td></tr>
        </table>
<!--
    <label class="header">Generate Invoice</label>
    <hr>
    <br>
        <table>
        <tr><agiletags:LabelTextBoxDateReq  displayname="Invoice Date" elementName="Invoice_Date" valueFromSession="yes" mandatory="yes"/>        </tr>
        <tr><td colspan="2" align="right"><input type="button" value="Generate" onclick="document.getElementById('contribCommand').value = 'genInvoice';submitFormWithAjaxPost(this.form, null, this)"></td></tr>
        </table>
    <label class="header">Generate Group Billing Statement</label>
    <hr>
    <br>
        <table>
        <tr><agiletags:LabelTextBoxDateReq  displayname="Statement Date" elementName="Statement_Date" valueFromSession="yes" mandatory="yes"/>        </tr>
        <tr><td colspan="2" align="right"><input type="button" value="Generate" onclick="document.getElementById('contribCommand').value = 'genStatement';document.forms[0].submit();"></td></tr>
        </table>
-->
      </agiletags:ControllerForm>
    

    </body>
</html>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <label class="header">Transfer Debit Order Allocation</label>
    <hr>
    <br>
    <table>
        <tr>
            <td width="160"><label>Member Number:</label></td>
            <td><label> ${coverNo}</label></td>
        </tr>
        <tr>
            <td><label>Member Name:</label></td>
            <td><label> ${memberName} ${memberSurname}</label></td>
        </tr>
        <tr>
            <td width="160"><label>Contribution Period:</label></td>
            <td><label> ${contribDate}</label></td>
        </tr>
        <tr>
            <td width="160"><label>Payment Narrative:</label></td>
            <td><label> ${paymentNarrative}</label></td>
        </tr>
        <tr>
            <td width="160"><label>Amount:</label></td>
            <td><label> ${amount}</label></td>
        </tr>
    </table>
    <table>
        <tr><agiletags:LabelTextBoxDate displayname="New Contribution Period" elementName="transferReverseVal" mandatory="yes"/></tr>
        <tr>
            <td><input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
            <td><input type="button" value="Transfer" onclick="saveTransferDOAlloc(this);"> </td>
        </tr>
    </table>
      

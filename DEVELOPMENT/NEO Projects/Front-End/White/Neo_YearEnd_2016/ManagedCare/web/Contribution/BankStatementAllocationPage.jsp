<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<c:if test="${pageVal == 1}">
        <div id="page_${pageVal}" style="display:${pageVal == 1 ? 'block' : 'none'};">
</c:if>
            <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="100%">
                    <tr>
                        <th>Reference</th>
                        <th>Description</th>
                        <th>Amount</th>
                        <th>Assigned To</th>
                        <th>Search Member</th>
                        <th>Search Group</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${BankStatement}">
                        <tr id="BSRow_${entry.statementId}_${entry.statementLineId}" ${entry.allocatedInd == 'R' ? " class='rowred' " : entry.allocatedInd == 'P' ? " class='rowgreen' " : " "}>
                        <input type="hidden" id ="BSCover_${entry.statementId}_${entry.statementLineId}" name="BSCover_${entry.statementId}_${entry.statementLineId}" value="">
                        <input type="hidden" name="BSDate_${entry.statementId}_${entry.statementLineId}" value="${agiletags:formatXMLGregorianDate(BankStatement[0].statementDate)}">
                        <input type="hidden" name="BSAmount_${entry.statementId}_${entry.statementLineId}" value="${agiletags:formatBigDecimal(entry.amount)}">
                            <td><label>${entry.transactionRefNo}</label></td>
                            <td style="white-space: normal;"><label>${entry.transactionDescription}</label></td>
                            <td align="right"><label>${agiletags:formatBigDecimal(entry.amount)}</label></td>
                            <td style="white-space: normal;"><label id="BSELbl_${entry.statementId}_${entry.statementLineId}">${entry.allocatedInd == 'S' || entry.allocatedInd == 'R' ? "Suspense Acct" : " "}</label></td>
                            <td width="130px">
                                <input size="10" type="text" onchange="findEntity('FindMember', this, ${entry.statementId}, ${entry.statementLineId}, ${entry.productId});">
                                <img title="Search Member" align="center" src="/ManagedCare/resources/search_trans.gif"  onclick="searchEntity('SearchMember', ${entry.statementId}, ${entry.statementLineId});">
                            </td>
                            <td width="174px">
                                <input size="10" type="text" onchange="findEntity('FindEmployer', this, ${entry.statementId}, ${entry.statementLineId}, ${entry.productId});">
                                <img title="Search Group" align="center" src="/ManagedCare/resources/search_trans.gif" onclick="searchEntity('SearchEmployer', ${entry.statementId}, ${entry.statementLineId});">
                                <img title="Split Group" align="center" width="28" height="28" src="/ManagedCare/resources/users_small.gif" onclick="splitGroup(${entry.statementId}, ${entry.statementLineId}, ${entry.amount}, '${agiletags:escapeStr(entry.transactionDescription)}', '${agiletags:formatXMLGregorianDate(entry.statementDate)}');">
                            </td>
                            <td>
                                <img title="Mark as Suspense" align="center" src="/ManagedCare/resources/question.gif" onclick="rejectLine(${entry.statementId}, ${entry.statementLineId}, 'R');">
                                &nbsp;&nbsp;
                                <img title="Mark as Persal Suspense"  align="center" src="/ManagedCare/resources/exclaim.gif" onclick="rejectLine(${entry.statementId}, ${entry.statementLineId}, 'P');">
                            </td>
                        </tr>
                    </c:forEach>

                </table>
        <br>
        <table width="90%">
            <tr>
                <td></td>
                <td width="70"><button type="button" class="nav-button" style='visibility:${pageVal == 1 ? "hidden" : ""}' onclick="changePage('${pageVal -1}','${pageVal}','${statementId}');"><img src="/ManagedCare/resources/left_grey.png"></button></td>
                <td width="40">Page ${pageVal}</td>
                <td width="70"><button class="nav-button"  type="button" style='visibility:${lastPage == "yes"  ? "hidden" : ""}'  onclick="changePage('${pageVal +1}','${pageVal}','${statementId}');"><img src="/ManagedCare/resources/right_grey.png"></button> </td>
                <td></td>
            </tr>
        </table>
</div>

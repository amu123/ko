<%-- 
    Document   : BankStatementAllocation
    Created on : 2012/07/05, 05:07:38
    Author     : yuganp
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <style>
            .rowgrey {background-color: #cccccc}
            .rowred {background-color: #ffcccc}
            .rowblue {background-color: #ccffff}
        </style>
        <script>
            function setRowClass(stId, lineId, cName) {
                document.getElementById("BSRow_" + stId + "_" + lineId).className = cName;
            }
            
            function searchEntity(searchCommand, searchStr, stId, lineId) {
                var url = "/ManagedCare/AgileController?opperation=ContributionBankStatementCommand&command=" + searchCommand + "&number="+ searchStr+"&statementId=" + stId + "&lineId=" + lineId;
                var http = getHttpRequestObject();
                http.open("GET", url, true);
                http.onreadystatechange = function() {
                    if (http.readyState == 4) {
                        updateFormFromData(http.responseText);
                        if (document.getElementById('BankStatementEntityId_'+ stId + "_" + lineId ).value != "") {
                            setRowClass(stId, lineId, "rowgrey");
                        } else {
                            setRowClass(stId, lineId, "");
                        }
                    }
                }
                http.send(null);
            }
            
            function rejectLine(stId, lineId) {
                setRowClass(stId, lineId, "rowred");
                document.getElementById('BankStatementEntityLabel_'+ stId + "_" + lineId ).innerHTML = 'Rejected';
                document.getElementById('BankStatementEntityId_'+ stId + "_" + lineId ).value = '';
                document.getElementById('BankStatementStatusInd_'+ stId + "_" + lineId ).value = 'R';
                document.getElementById('BankStatementEntityType_'+ stId + "_" + lineId ).value = '';
            }
        </script>
            
    </head>
    <body>
     <agiletags:ControllerForm name="BankStatementSearchEntityForm">
        <input type="hidden" name="opperation" id="opperation" value="ContributionBankStatementCommand" />
        <input type="hidden" name="command" id="BankStatementSearchCommand" value="SearchEntity" />
        <input type="hidden" name="entityLabel" value="" />
        <input type="hidden" name="entityId" value="" />
        <input type="hidden" name="entityForm" value="BankStatementForm" />
     </agiletags:ControllerForm>
        
     <agiletags:ControllerForm name="BankStatementForm">
        <input type="hidden" name="opperation" id="opperation" value="ContributionBankStatementCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="BankStatementList" />
        <input type="hidden" name="command" id="BankStatementCommand" value="" />
        <input type="hidden" name="statementId" id="BankStatementId" value="${entry.statementId}" />

    <label class="header">Bank Statement Allocation</label>
    <hr>
    <br>
    <c:if test="${! empty BankStatement}">
        <label>Date:  ${agiletags:formatXMLGregorianDate(BankStatement[0].statementDate)}</label><br>
        <label>Bank: ${BankStatement[0].bankName}</label><br>
        <label>Account Number: ${BankStatement[0].accountNumber}</label><br>
        <br>
    </c:if>
        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="100%">
                    <tr>
                        <th>Reference</th>
                        <th>Description</th>
                        <th>Amount</th>
                        <th>Assigned To</th>
                        <th>Search Member</th>
                        <th>Search Group</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${BankStatement}">
                        <tr id="BSRow_${entry.statementId}_${entry.statementLineId}">
                            <td style="white-space: nowrap"><label>${entry.transactionRefNo}</label></td>
                            <td><label>${entry.transactionDescription}</label></td>
                            <td style="white-space: nowrap" align="right"><label>${agiletags:formatBigDecimal(entry.amount)}</label></td>
                            <td>
                                <label id="BankStatementEntityLabel_${entry.statementId}_${entry.statementLineId}"></label>
                                <input type="hidden" id="BankStatementEntityId_${entry.statementId}_${entry.statementLineId}" name="BankStatementEntityId_${entry.statementId}_${entry.statementLineId}" value="">
                                <input type="hidden" id="BankStatementStatusInd_${entry.statementId}_${entry.statementLineId}" name="BankStatementStatusInd_${entry.statementId}_${entry.statementLineId}" value="">
                                <input type="hidden" id="BankStatementEntityType_${entry.statementId}_${entry.statementLineId}" name="BankStatementEntityType_${entry.statementId}_${entry.statementLineId}" value="">
                            </td>
                            <td style="white-space: nowrap"><input size="10" type="text" onchange="searchEntity('FindMember', this.value, ${entry.statementId}, ${entry.statementLineId});"><img align="center" width="22" height="22" src="/ManagedCare/resources/Search.gif" onclick="setRowClass(${entry.statementId}, ${entry.statementLineId}, '');"></td>
                            <td style="white-space: nowrap"><input size="10" type="text" onchange="searchEntity('FindEmployer', this.value, ${entry.statementId}, ${entry.statementLineId});"><img align="center" width="22" height="22"  src="/ManagedCare/resources/Search.gif" onclick="setRowClass(${entry.statementId}, ${entry.statementLineId}, 'rowgrey');"></td>
                            <td style="white-space: nowrap"><img align="center" width="22" height="22"  src="/ManagedCare/resources/Close.gif" onclick="rejectLine(${entry.statementId}, ${entry.statementLineId});"></td>
                        </tr>
                    </c:forEach>

                </table>
     </agiletags:ControllerForm>
    
    </body>
</html>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <label class="header">Refund Contribution</label>
    <hr>
    <br>
    <table>
        <tr>
            <td width="160"><label>Member Number:</label></td>
            <td><label> ${coverNo}</label></td>
        </tr>
        <tr>
            <td><label>Member Name:</label></td>
            <td><label> ${memberName} ${memberSurname}</label></td>
        </tr>
    </table>
    <table>
        <tr><agiletags:LabelTextBoxError displayName="Refund Reason" elementName="refundReason" mandatory="yes"/></tr>
        <tr><agiletags:LabelTextBoxDate displayname="Contribution Period" elementName="refundDate" mandatory="yes"/></tr>
        <tr><agiletags:LabelTextBoxError displayName="Amount" elementName="refundAmount" mandatory="yes"/></tr>
        <tr>
            <td><input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
            <td><input type="button" value="Refund" onclick="saveRefund(this);"> </td>
        </tr>
    </table>
      

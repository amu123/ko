<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script>
            function writeOff(lineId) {
//                document.getElementById('TransferReverseCommand').value = 'SelectWriteOff';
                setWriteOffFields(lineId);
//                formVal = document.getElementById("PaymentAllocationForm");
                swapDivVisbible('main_div', 'overlay')
//                getPageContents(formVal, null, 'main_div', 'overlay');
            }
            
            function saveWriteOff(btn) {
                btn.disabled = true;
                document.getElementById('TransferReverseCommand').value = 'SaveWriteOff';
                document.getElementById('paymentNarrative').value = document.getElementById('WriteOffReason').value;
                document.getElementById('amount').value = document.getElementById('AmountToWriteOff').value;
                formVal = document.getElementById("WriteOffForm");
                var params = encodeFormForSubmit(formVal);
                $(formVal).mask("Saving...");
                $.post(formVal.action, params,function(result){
                    updateFormFromData(result);
                    $(formVal).unmask();
                    if (document.getElementById('transferReverseStatus').value == 'ok') {
                        lineId = document.getElementById('lineId').value;
                        document.getElementById('btn_' + lineId).disabled = true;
                        swapDivVisbible('overlay','main_div');
                        btn.disabled = false;
                    } else {
                        btn.disabled = false;
                    }
                });
            }
            
            function setWriteOffFields(lineId) {
                document.getElementById('lineId').value = lineId;
                document.getElementById('optionId').value = document.getElementById('optionId_'+lineId).value;
                document.getElementById('contribDate').value = document.getElementById('contribDate_'+lineId).innerHTML;
                document.getElementById('balance').value = document.getElementById('balance_'+lineId).innerHTML;
                document.getElementById('contributionDate').innerHTML = document.getElementById('contribDate_'+lineId).innerHTML;
                document.getElementById('contribBalance').innerHTML = document.getElementById('balance_'+lineId).innerHTML;
                document.getElementById('AmountToWriteOff').value = document.getElementById('balance_'+lineId).innerHTML;
            }
            
        </script>
    </head>
    <body>
        <div id="main_div" >
     <agiletags:ControllerForm name="WriteOffForm">
        <input type="hidden" name="opperation" id="opperation" value="RefundContribCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="WriteOffCommand" />
        <input type="hidden" name="command" id="TransferReverseCommand" value="" />
        <input type="hidden" name="coverNumber" id="coverNumber" value="${coverNo}" />
        <input type="hidden" name="entityId" id="entityId" value="${entityId}" />
        <input type="hidden" name="optionId" id="optionId" value="" />
        <input type="hidden" name="memberName" id="memberName" value="${memberName}" />
        <input type="hidden" name="memberSurname" id="memberSurname" value="${memberSurname}" />
        <input type="hidden" name="contribDate" id="contribDate" value="" />
        <input type="hidden" name="paymentNarrative" id="paymentNarrative" value="" />
        <input type="hidden" name="amount" id="amount" value="" />
        <input type="hidden" name="balance" id="balance" value="" />
        <input type="hidden" name="transferReverseStatus" id="transferReverseStatus" value="">
        <input type="hidden" name="lineId" id="lineId" value="" />
     </agiletags:ControllerForm>
        
    <label class="header">Write-Off Contributions</label>
    <hr>
    <table>
        <tr>
            <td width="160"><label>Member Number:</label></td>
            <td><label> ${coverNo}</label></td>
        </tr>
        <tr>
            <td><label>Member Name:</label></td>
            <td><label> ${memberName} ${memberSurname}</label></td>
        </tr>
    </table>
        <br>
        <div id="PageContents" >
            <c:if test="${empty writeOffs}">
                <Label>No Contributions to Write-off</Label>
            </c:if>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Contribution Period</th>
                        <th>Raised</th>
                        <th>Paid</th>
                        <th>Journal</th>
                        <th>Balance</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${writeOffs}">
                        <c:set var="lineId" value="${fn:replace(entry['contribDate'],'/','')}"></c:set>
                        <tr>
                            <td><label id="contribDate_${lineId}">${entry['contribDate']}</label></td>
                            <td align="right"><label id="raised_${lineId}">${agiletags:formatStringAmount(entry['raised'])}</label></td>
                            <td align="right"><label id="paid_${lineId}">${agiletags:formatStringAmount(entry['paid'])}</label></td>
                            <td align="right"><label id="jnl_${lineId}">${agiletags:formatStringAmount(entry['jnl'])}</label></td>
                            <td align="right"><label id="balance_${lineId}">${agiletags:formatStringAmount(entry['balance'])}</label></td>
                            <td><button type="button" id="btn_${lineId}" onclick="writeOff('${lineId}');">Write-Off</button></td>
                            <input type="hidden" id="optionId_${lineId}" value="${entry['optionId']}">
                        </tr>
                    </c:forEach>
                </table>
        </div>
        <br>
        </div>
      <div id="overlay" style="display:none;">
        <label class="header">Write-Off Contributions</label>
        <hr>
        <table>
            <tr>
                <td width="160"><label>Member Number:</label></td>
                <td><label> ${coverNo}</label></td>
            </tr>
            <tr>
                <td><label>Member Name:</label></td>
                <td><label> ${memberName} ${memberSurname}</label></td>
            </tr>
            <tr>
                <td><label>Contribution Date:</label></td>
                <td><label id="contributionDate"></label></td>
            </tr>
            <tr>
                <td><label>Balance</label></td>
                <td><label id="contribBalance"></label></td>
            </tr>
            <tr><agiletags:LabelTextBoxError displayName="Amount To WriteOff" elementName="AmountToWriteOff" mandatory="yes"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Write Off Reason" elementName="WriteOffReason" mandatory="yes"/></tr>
            <tr>
                <td><input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('overlay','main_div');"></td>
                <td><input type="button" value="Save" onclick="saveWriteOff(this);"> </td>
            </tr>
        </table>
          
      </div>
      <div id="overlay2" style="display:none;"></div>
    
    </body>
</html>

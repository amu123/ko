<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Search Results</label>
    <hr>
    <br>
    <c:choose>
        <c:when test="${empty SearchResults}">
            <Lable>No results found</lable>
        </c:when>
        <c:otherwise>
            <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                <tr>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Amount</th>
                    <th>Current Assigned to</th>
                    <th>New Assigned to</th>
                    <th>Search Member</th>
                    <th>Search Group</th>
                </tr>
                <c:forEach var="entry" items="${SearchResults}">
                    <tr id="URRow_${entry.contribReceiptsId}" >
                        <input type="hidden" id ="URCover_${entry.contribReceiptsId}" name="URCover_${entry.contribReceiptsId}" value="">
                        <td><label>${entry['date']}</label></td>
                        <td style="white-space: normal;"><label>${entry['description']}</label></td>
                        <td align="right"><label>${agiletags:formatStringAmount(entry['amount'])}</label></td>
                        <td style="white-space: normal;"><label>${entry['currentAssign']}</label></td>
                        <td style="white-space: normal;"><label id="URELbl_${entry.contribReceiptsId}"></label></td>
                        <td>
                            <input size="10" type="text" onchange="findEntity('FindMember', this, ${entry.contribReceiptsId});">
<!--                            <img align="center" src="/ManagedCare/resources/search_trans.gif"  onclick="searchEntity('SearchMember', ${entry.contribReceiptsId});"> -->
                        </td>
                        <td>
                            <input size="10" type="text" onchange="findEntity('FindEmployer', this, ${entry.contribReceiptsId});">
<!--                            <img align="center" src="/ManagedCare/resources/Search.gif" onclick="searchEntity('SearchEmployer', ${entry.contribReceiptsId});"> -->
                        </td>
                    </tr>
                </c:forEach>
            </table>
        <br>
            <input type="button" value="Transfer" onclick="saveDetails(this.form, this);">
        </c:otherwise>
    </c:choose>
   
<%-- 
    Document   : BankStatementList
    Created on : 2012/07/05, 05:06:33
    Author     : yuganp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
    </head>
    <body>
     <agiletags:ControllerForm name="BankStatementForm">
        <input type="hidden" name="opperation" id="opperation" value="ContributionBankStatementCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="BankStatementList" />
        <input type="hidden" name="command" id="BankStatementCommand" value="" />
        <input type="hidden" name="statementId" id="BankStatementId" value="" />

    <label class="header">Unallocated Bank Statement List</label>
    <hr>
    <br>
        <table>
          <tr>
              <td width="160"><Label>Type</Label></td>
              <td>
                  <select style="width:215px" name="includeSuspense" id="includeSuspense">
                      <option value="N" selected>Unallocated</option>
                      <option value="R">Suspense</option>
                      <option value="P">Persal - Suspense</option>
                  </select>
              </td>
                  
          </tr>
        </table>
    <input type="button" value="Search" onclick="document.getElementById('BankStatementCommand').value = this.value; submitFormWithAjaxPost(this.form, 'BankStatementListDiv', this);">
    <div id="BankStatementListDiv"></div>
     </agiletags:ControllerForm>
    </body>
</html>

<%-- 
    Document   : BankStatementAllocation
    Created on : 2012/07/05, 05:07:38
    Author     : yuganp
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <style>
            td {white-space: nowrap}
            .rowgrey {background-color: #cccccc}
            .rowred {background-color: #ffcccc}
            .rowblue {background-color: #ccffff}
            .rowgreen {background-color: aquamarine}
            .nav-button {border: none; background-color: transparent;}
            .splitAlloc{text-align:right;}
        </style>
        <script>
            function setRowClass(stId, lineId, cName) {
                document.getElementById("BSRow_" + stId + "_" + lineId).className = cName;
            }
            
            function createHiddenElement(row, stId, lineId, elemType) {
                elemName = elemType + "_" + stId + "_" + lineId;
                if (document.getElementById(elemName) == null) {
                    element = document.createElement("input");
                    element.type = "hidden";
                    element.name = elemName;
                    element.setAttribute("id", elemName);
                    row.appendChild(element);
                }
            }
            
            function createElements(stId, lineId) {
                var row = document.getElementById("BSRow_" + stId + "_" + lineId);
                createHiddenElement(row, stId, lineId, "BSEId");
                createHiddenElement(row, stId, lineId, "BSStatusInd");
                createHiddenElement(row, stId, lineId, "BSEType");
            }
            
            function findEntity(searchCommand, searchStr, stId, lineId, productId) {
                var url = "/ManagedCare/AgileController?opperation=ContributionBankStatementCommand&command=" + searchCommand + "&number="+ searchStr.value +"&statementId=" + stId + "&lineId=" + lineId + "&productId=" + productId;
                $.get(url, function(response){
                        searchStr.value = "";
                        createElements(stId, lineId);
                        updateFormFromData(response);
                        if (document.getElementById('BSEId_'+ stId + "_" + lineId ).value != "") {
                            setRowClass(stId, lineId, "rowgrey");
                        } else {
                            setRowClass(stId, lineId, "");
                        }
                });
            }
            
            function searchEntity(searchCommand, stId, lineId) {
                formVal = document.getElementById("BankStatementSearchEntityForm");
                document.getElementById("BankStatementSearchCommand").value = searchCommand;
                document.getElementById("bsStatementId").value = stId;
                document.getElementById("bsLineId").value = lineId;
                document.getElementById("entityForm").value = 'BankStatementForm';
                getPageContents(formVal, null, 'main_div', 'overlay');
            }
            
            function searchSplitGroup(stId, lineId) {
                formVal = document.getElementById("BankStatementSearchEntityForm");
                document.getElementById("BankStatementSearchCommand").value = 'SearchEmployer';
                document.getElementById("bsStatementId").value = stId;
                document.getElementById("bsLineId").value = lineId;
                document.getElementById("entityForm").value = 'SplitGroupForm';
                getPageContents(formVal, null, 'overlay2', 'overlay');
            }
            
            function splitGroup(stId, lineId, lineAmount, lineDescription, statementDate) {
                formVal = document.getElementById("BankStatementSearchEntityForm");
                document.getElementById("BankStatementSearchCommand").value = 'SplitGroup';
                document.getElementById("bsStatementId").value = stId;
                document.getElementById("bsLineId").value = lineId;
                document.getElementById("bsLineAmount").value = lineAmount;
                document.getElementById("bsLineDescription").value = lineDescription;
                document.getElementById("statementDate").value = statementDate;
                getPageContents(formVal, null, 'main_div', 'overlay2');
            }
            
            function updateLineFromSearch(entityId, stId, lineId, bseType, description, coverNo) {
                createElements(stId, lineId);
                setRowClass(stId, lineId, "rowgrey");
                document.getElementById('BSELbl_'+ stId + "_" + lineId ).innerHTML = description;
                document.getElementById('BSStatusInd_'+ stId + "_" + lineId ).value = 'A';
                document.getElementById('BSEId_'+ stId + "_" + lineId ).value = entityId;
                document.getElementById('BSEType_'+ stId + "_" + lineId ).value = bseType;
                if (bseType == 'M') {
                    document.getElementById('BSCover_'+ stId + "_" + lineId ).value = coverNo;
                }
            }
            
            function rejectLine(stId, lineId, lineType) {
                createElements(stId, lineId);
                if (document.getElementById('BSStatusInd_'+ stId + "_" + lineId ).value == lineType) {
                    setRowClass(stId, lineId, "");
                    document.getElementById('BSELbl_'+ stId + "_" + lineId ).innerHTML = '';
                    document.getElementById('BSStatusInd_'+ stId + "_" + lineId ).value = '';
                } else {
                    setRowClass(stId, lineId, lineType == 'R' ? "rowred" : "rowgreen");
                    document.getElementById('BSELbl_'+ stId + "_" + lineId ).innerHTML = (lineType == 'R' ? 'Suspense Acct' : 'Suspense Acct - Persal');
                    document.getElementById('BSStatusInd_'+ stId + "_" + lineId ).value = lineType;
                }
                document.getElementById('BSEId_'+ stId + "_" + lineId ).value = '';
                document.getElementById('BSEType_'+ stId + "_" + lineId ).value = '';
            }
            
            function changePage(newPage, oldPage, statementId) {
                if (oldPage != null) {
                    var elem = document.getElementById("page_" + oldPage);
                    elem.style.display = 'none';
                }
                if (newPage != null) {
                    var elem = document.getElementById("page_" + newPage);
                    if (elem != null) {
                        elem.style.display = 'block';
                    } else {
                        getNextPage(newPage,statementId);
                        elem = document.getElementById("page_" + newPage);
                        if (elem != null) {
                            elem.style.display = 'block';
                        }
                    }
                }
            }
            
            function getNextPagex(newPage, statementId) {
                var url = "/ManagedCare/AgileController?opperation=ContributionBankStatementCommand&command=Select&pageNum="+ newPage+"&statementId=" + statementId +"&nocache=" + new Date().getTime();;
                var http = getHttpRequestObject();
                http.open("GET", url, true);
                http.onreadystatechange = function() {
                    if (http.readyState == 4) {
                        var newDiv = document.createElement("div");
                        var newDivName = "page_" + newPage;
                        newDiv.innerHTML = http.responseText;
                        newDiv.setAttribute("id",newDivName );
                        document.getElementById("PageContents").appendChild(newDiv);
                    }
                }
                http.send(null);
            }
            
            function getNextPage(newPage, statementId) {
                incSusp = document.getElementById("includeSuspense").value;
                var url = "/ManagedCare/AgileController?opperation=ContributionBankStatementCommand&command=Select&pageNum="+ newPage+"&statementId=" + statementId + "&includeSuspense=" + incSusp +"&nocache=" + new Date().getTime();
                $.get(url, function(response){
                        var newDiv = document.createElement("div");
                        var newDivName = "page_" + newPage;
                        newDiv.innerHTML = response;
                        newDiv.setAttribute("id",newDivName );
                        document.getElementById("PageContents").appendChild(newDiv);
//                    alert(response);
//                    jQuery('<div/>',{
//                        id:"page_" + newPage,
//                        html:response
//                    }).appendTo("#PageContents");
                });
            }
            
            function saveDetails(form, btn) {
                btn.disabled = true;
                form['command'].value = "Save";
                var params = encodeFormForSubmit(form);
                $(form).mask("Saving...");

                $.post(form.action, params,function(result){
                    updateFormFromData(result);
                    if (btn != undefined) {
                        btn.disabled = false;
                    }
                    $(form).unmask();
                    window.location='/ManagedCare/Security/Welcome.jsp'; 
                });
            }
            
            function addSplitGroup(groupId, groupNumber, groupName) {
                formVal = document.getElementById("GroupSplitForm");
                formVal['command'].value = 'AddSplitGroup';
                formVal['addGroupName'].value = groupName;
                formVal['addGroupId'].value = groupId;
                formVal['addGroupNumber'].value = groupNumber;
                submitFormWithAjaxPost(formVal, 'GroupSplitResults');
            }
            
            function updateLineFromSplit(form, btn, stId, lineId, bseType) {
                remain = parseFloat($('#remainingAmount').text());
                if (remain != 0) {
                    alert ("Remaining amount must be zero");
                } else {
                    var dispStr = "";
                    var idStrList = null;
                    $(".splitAlloc").each( function(){
                        entityId = (this).name.substr(9);
                        groupNum = form['split_number_' + entityId].value;
                        groupAmt = $(this).val();
                        dispStr = dispStr + "<b>" + groupNum + ":</b> " + groupAmt + "<br>";
                        idStrList = (idStrList == null? "" : idStrList + ",") + entityId + ":" + groupAmt;
                    });
                    createElements(stId, lineId);
                    setRowClass(stId, lineId, "rowgreen");
                    document.getElementById('BSELbl_'+ stId + "_" + lineId ).innerHTML = dispStr;
                    document.getElementById('BSStatusInd_'+ stId + "_" + lineId ).value = 'A';
                    document.getElementById('BSEId_'+ stId + "_" + lineId ).value = idStrList;
                    document.getElementById('BSEType_'+ stId + "_" + lineId ).value = bseType;
                    swapDivVisbible('overlay2','main_div');
                }
            }

            function calculateBalance() {
                total = parseFloat($('#unallocatedAmount').text());
                allocated = 0;
                $(".splitAlloc").each( function(){
                    tmp=parseFloat($(this).val());
                    if (isNaN(tmp)){tmp=0;};
                    allocated= allocated + tmp;     
                });
                $('#allocatedAmount').text(allocated.toFixed(2));
                $('#remainingAmount').text((total - allocated).toFixed(2));
            }
            
            function changeValue(item) {
                tmp=parseFloat(item.value);
                if (isNaN(tmp)){item.value = ''} else {item.value = tmp.toFixed(2)};
                calculateBalance();
            }
            
            function ClearValidation(){
                $("#scheme_error").text("");
            }
            
            function validateAndSearchGroup(form, id) {
                ClearValidation();
                var scheme = $("#scheme").val();
                
                if(scheme == null || scheme == "" || scheme == "99"){
                    $("#scheme_error").text("Scheme is mandatory");
                }
                else{
                    submitFormWithAjaxPost(form, id);
                }
            }
        </script>
            
    </head>
    <body>
     <agiletags:ControllerForm name="BankStatementSearchEntityForm">
        <input type="hidden" name="opperation" id="opperation" value="ContributionBankStatementCommand" />
        <input type="hidden" name="command" id="BankStatementSearchCommand" value="SearchEntity" />
        <input type="hidden" name="entityLabel" value="" />
        <input type="hidden" name="entityId" value="" />
        <input type="hidden" name="bsStatementId" id="bsStatementId" value="" />
        <input type="hidden" name="bsLineId" id="bsLineId" value="" />
        <input type="hidden" name="bsLineAmount" id="bsLineAmount" value="" />
        <input type="hidden" name="bsLineDescription" id="bsLineDescription" value="" />
        <input type="hidden" name="statementDate" id="statementDate" value="" />
        <input type="hidden" name="entityForm" id="entityForm"  value="BankStatementForm" />
     </agiletags:ControllerForm>
        
        <div id="main_div" >
     <agiletags:ControllerForm name="BankStatementForm">
        <input type="hidden" name="opperation" id="opperation" value="ContributionBankStatementCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="BankStatementList" />
        <input type="hidden" name="command" id="BankStatementCommand" value="" />
        <input type="hidden" name="statementId" id="BankStatementId" value="${statementId}" />
        <input type="hidden" name="pageNum" id="pageNum" value="1">
        <input type="hidden" name="includeSuspense" id="includeSuspense" value="${includeSuspense}">
    <label class="header">Bank Statement Allocation</label>
    <hr>
    <c:if test="${! empty BankStatement}">
        <label>Date:  ${agiletags:formatXMLGregorianDate(BankStatement[0].statementDate)}</label><br>
        <label>Bank: ${BankStatement[0].bankName}</label><br>
        <label>Account Number: ${BankStatement[0].accountNumber}</label><br>
        <c:if test="${! empty suspType}">
            <label>Allocation Type: ${suspType}</label><br>
        </c:if>
        <br>
    </c:if>
        <div id="PageContents" >
            <jsp:directive.include file="/Contribution/BankStatementAllocationPage.jsp"></jsp:directive.include>
        </div>
        <br>
        <input type="button" value="Save" onclick="saveDetails(this.form, this)">
     </agiletags:ControllerForm>
        </div>
      <div id="overlay" style="display:none;"></div>
      <div id="overlay2" style="display:none;"></div>
     
    </body>
</html>

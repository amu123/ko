<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css">
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script >
            function setMemVals(entityId, coverNo, mName, mSurname, optionId) {
                document.getElementById('entityId').value = entityId;
                document.getElementById('RefundSearchCommand').value = 'SelectMemberWriteOff';
                document.getElementById('coverNo').value = coverNo;
                document.getElementById('memberName').value = mName;                
                document.getElementById('memberSurname').value = mSurname;
                document.getElementById('optionId').value = optionId;
                formVal = document.getElementById("WriteOffSearchForm");
                formVal.submit();
                //getPageContents(formVal, null, 'main_div', 'overlay');
            }
            
            function saveWriteOff(btn) {
                btn.disabled = true;
                document.getElementById('RefundSearchCommand').value = 'SaveMemberMSARefund';
                formVal = document.getElementById("WriteOffSearchForm");
                var params = encodeFormForSubmit(formVal);
                $(formVal).mask("Saving...");
                $.post(formVal.action, params,function(result){
                    updateFormFromData(result);
                    $(formVal).unmask();
                    if (document.getElementById('transferRefundStatus').value == 'ok') {
                        swapDivVisbible('overlay','main_div');
                    } else {
                        btn.disabled = false;
                    }
                });
            }
            
            function validateAndSearchMember(frm, btn) {
                document.getElementById('RefundSearchCommand').value = 'SearchWriteOff';
                var isValid = false;
                
                if ($('#memNo').val().length > 0 || $('#idNo').val().length > 0 ) {
                    isValid = true;
                } else {
                    var valCount = $('#initials').val().length > 0 ? 1 : 0;
                    valCount = valCount + ( $('#surname').val().length > 0 ? 1 : 0);
                    valCount = valCount + ($('#dob').val().length > 0 ? 1 : 0);
                    if (valCount > 1 ) {
                        isValid = true;
                    }
                }
                
                if (isValid) {
                    $('#btn_MemSearch_error').text("");
                    submitFormWithAjaxPost(frm, 'SearchListDiv', btn);
                } else {
                    $('#btn_MemSearch_error').text("Please enter some search criteria");
                }
            }
            
            
        </script>
    </head>
    <body>
     <agiletags:ControllerForm name="WriteOffSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="RefundContribCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="WriteOffSearch" />
        <input type="hidden" name="command" id="RefundSearchCommand" value="" />
        <input type="hidden" name="entityId" id="entityId" value="" />
        <input type="hidden" name="coverNo" id="coverNo" value="" />
        <input type="hidden" name="memberName" id="memberName" value="" />
        <input type="hidden" name="memberSurname" id="memberSurname" value="" />
        <input type="hidden" name="optionId" id="optionId" value="" />
        <input type="hidden" name="transferRefundStatus" id="transferRefundStatus" value="">
        <div id="main_div">
            <label class="header">Write Off Contributions Search</label>
            <hr>
            <br>
            <table id="MemberTable">
                <tr><agiletags:LabelTextBoxError displayName="Member Number" elementName="memNo"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="ID Number" elementName="idNo"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="Initials" elementName="initials"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="surname"/></tr>
                <tr><agiletags:LabelTextBoxDate displayname="Date of Birth" elementName="dob"/></tr>
                <tr><td colspan="2" align="right"><input type="button" value="Search" onclick="validateAndSearchMember(this.form, this);"></td>
                    <td></td>
                    <td></td>
                    <td><label id="btn_MemSearch_error" class="error"></label></td>
                </tr>
            </table>
            <div id="SearchListDiv"></div>
        </div>
        <div id="overlay"></div>
     </agiletags:ControllerForm>
    </body>
</html>

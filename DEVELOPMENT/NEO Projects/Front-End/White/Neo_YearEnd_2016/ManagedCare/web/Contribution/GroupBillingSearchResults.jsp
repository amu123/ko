<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty EmployerSearchResults}">
            <span>No results found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Group Number</th>
                        <th>Name</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry2" items="${EmployerSearchResults}">
                        <tr>
                            <td><label>${entry2['employerNumber']}</label></td>
                            <td><label>${entry2['employerName']}</label></td>
                            <td><input type="submit" value="Select" onclick="document.getElementById('EmployerSearchCommand').value = 'SelectEmployer'; document.getElementById('entityId').value = '${entry2['entityId']}';"></td>
                        </tr>
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>

   
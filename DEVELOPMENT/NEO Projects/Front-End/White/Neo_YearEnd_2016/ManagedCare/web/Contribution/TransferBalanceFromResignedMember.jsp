<%-- 
    Document   : TransferBalanceFromResignedMember
    Created on : 2013/05/24, 12:31:06
    Author     : yuganp
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script>
            function getCoverDetails(type) {
                if ($('#' + type).val() == '') {
                    $('#' + type + 'Lbl').html('');
                    $('#' + type).val('');
                    $('#transferBtn').attr("disabled", true);;
                } else {
                    var tmp = $('#' + type).val();
                    setFormValues("TransferBalFromResignedMemberForm", {"command" : "findMember", "type" : type});
                    var form = document.forms["TransferBalFromResignedMemberForm"];
                    var url = form.action + "?" + encodeFormForSubmit(document.forms["TransferBalFromResignedMemberForm"]);
                    $('#' + type).val('');
                    $(form).mask("Prcoessing...");
                    $.get(url, function(result){
                        updateFormFromData(result);
                        document.getElementById(type + 'Lbl').className = $('#memberStatus').val() == '' ? '' : 'red';
//                        $('#' + type).val(tmp);
                        if ($('#coverFrom').val() == '' || $('#coverTo').val() == '' || $('#transferAmount').val() == '') {
                            $('#transferBtn').attr("disabled", true);;
                        } else {
                            $('#transferBtn').removeAttr('disabled');
//                            $('#transferBtn').disabled = false;
                        }
                        $(form).unmask();
                    });
                }
            }
            
            function transferBalance() {
                if ($('#coverFrom').val() == '' || $('#coverTo').val() == '') {
                    alert('Please select both members');
                } else if ($('#transferAmount').val() == '') {
                    alert ("Balance is Zero");
                } else if ($('#coverFrom').val() == $('#coverTo').val())  {
                    alert ("Member numbers must be different")
                } else {
                    $('#transferBtn').attr("disabled", true);;
                    form = document.forms["TransferBalFromResignedMemberForm"];
                    form['command'].value = "Save";
                    var params = encodeFormForSubmit(form);
                    $(form).mask("Saving...");

                    $.post(form.action, params,function(result){
                        updateFormFromData(result);
                        $('#transferBtn').removeAttr('disabled');
                        $(form).unmask();
//                        window.location='/ManagedCare/Security/Welcome.jsp'; 
                    });
                }
            }
        </script>
    </head>
    <body>
        <label class="header">Transfer Balance from Resigned Member to Active Member</label>
        <hr>
        <br>
     <agiletags:ControllerForm name="TransferBalFromResignedMemberForm">
        <input type="hidden" name="opperation" id="opperation" value="TransferBalFromResignedMemberCommand" />
        <input type="hidden" name="command" id="TransferBalanceCommand" value="" />
        <input type="hidden" name="type" id="coverType" value="">
        <input type="hidden" name="memberStatus" id="memberStatus" value="">
        <table id="ErrorsTable">
            <tr>
                <td width="160"><label class="label">From Member</label></td>
                <td>
                    <input type="text" name="coverFrom" id="coverFrom" onblur="getCoverDetails('coverFrom');">
                </td>
                <td>
                    <label id="coverFromLbl" class="">&nbsp;</label>
                </td>
            </tr>
            <tr>
                <td width="160"><label class="label">To Member</label></td>
                <td>
                    <input type="text" name="coverTo" id="coverTo" onblur="getCoverDetails('coverTo');">
                </td>
                <td>
                    <label id="coverToLbl" class="">&nbsp;</label>
                </td>
            </tr>
            <tr>
                <td width="160"><label class="label">Amount to be transferred</label></td>
                <td>
                    <input type="text" name="transferAmount" id="transferAmount" readonly="true" disabled="true">
                </td>
                <td>
                    <label id="transferAmountLbl" class="">&nbsp;</label>
                </td>
            </tr>
            <tr>
                <td><button type="button" disabled id="transferBtn" onclick="transferBalance();">Transfer</button></td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
     </agiletags:ControllerForm>

    </body>
</html>


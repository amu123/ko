<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <label class="header">Refund Group Contribution</label>
    <hr>
    <br>
    <table>
        <tr>
            <td width="160"><label>Group:</label></td>
            <td><label> ${employerName}</label></td>
        </tr>
    </table>
    <table>
        <tr><agiletags:LabelTextBoxError displayName="Refund Reason" elementName="refundReason" mandatory="yes" javaScript="onblur='validateNeoStringField(this, true, 1, 40)';"/></tr>
        <jsp:useBean id="now" class="java.util.Date"/>
        <c:set var="maxDate" value="${agiletags:formatDate(agiletags:dateAdd(now,'month',1))}" />
        <tr><agiletags:LabelTextBoxDate displayname="Contribution Period" elementName="refundDate" mandatory="yes" javascript="onblur='validateNeoDateField(this,true, \"2012/01/01\",\"${maxDate}\")';"/></tr>
        <tr><agiletags:LabelTextBoxError displayName="Amount" elementName="refundAmount" mandatory="yes" javaScript="onchange='validateNeoAmountField(this, true, 0.01);calculateBalance();'"/></tr>
        <tr>
            <td width="160"><label>Allocated Amount:</label></td>
            <td><input size="30" align="right" style="text-align: right;" type="text" disabled readonly id="allocatedAmountId" value="0.00"></td>
        </tr>
        <tr>
            <td width="160"><label>Remaining Amount:</label></td>
            <td><input size="30" align="right" style="text-align: right;" type="text" disabled readonly id="remainingAmountId" value="0.00"></td>
        </tr>
        <tr>
            <td><input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
            <td><input type="button" value="Refund" onclick="saveEmployerRefund(this);"> </td>
        </tr>
    </table>
                <br>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Member Number</th>
                        <th>Surname</th>
                        <th>First Name</th>
                        <th>Balance</th>
                        <th>Refund Amount</th>
                    </tr>
                    <c:forEach var="entry" items="${employerMemberList}">
                        <tr>
                            <td><label>${entry['COVER_NUMBER']}</label></td>
                            <td><label>${entry['SURNAME']}</label></td>
                            <td><label>${entry['FIRST_NAME']}</label></td>
                            <td align="right"><label>${agiletags:formatStringAmount(entry['BAL'])}</label></td>
                            <td><input type="text"  style="text-align: right;" id="contrib_${entry['COVER_NUMBER']}" name="contrib_${entry['COVER_NUMBER']}"  class="contribAlloc" onchange="changeValue(this)"></td>
                        </tr>
                    </c:forEach>
                </table>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty MemberSearchResults}">
            <span>No results found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Member Number</th>
                        <th>Initials</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>DOB</th>
                        <th>ID Number</th>
                        <th>Option</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${MemberSearchResults}">
                        <tr>
                            <td><label>${entry['coverNumber']}</label></td>
                            <td><label>${entry['initials']}</label></td>
                            <td><label>${entry['name']}</label></td>
                            <td><label>${entry['surname']}</label></td>
                            <td><label>${entry['DOB']}</label></td>
                            <td><label>${entry['idNumber']}</label></td>
                            <td><label>${entry['optionName']}</label></td>
                            <td><input type="submit" value="Select" onclick="setMemVals('${entry['entityId']}','${entry['coverNumber']}', '${agiletags:escapeStr(entry['name'])}', '${agiletags:escapeStr(entry['surname'])}');"></td>
                        </tr>
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>

   
<%-- 
    Document   : BankStatementList
    Created on : 2012/07/05, 05:06:33
    Author     : yuganp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script >
            function setMemVals(entityId, coverNo, mName, mSurname, amount) {
                document.getElementById('entityId').value = entityId;
                document.getElementById('RecieptsSearchCommand').value = 'SelectMember';
                document.getElementById('coverNo').value = coverNo;
                document.getElementById('amount').value = amount;                
                document.getElementById('memberName').value = mName;                
                document.getElementById('memberSurname').value = mSurname;                
            }
            
            function setEmpVals(entityId, gName, gNumber, amount) {
                document.getElementById('RecieptsSearchCommand').value = 'SelectEmployer';
                document.getElementById('entityId').value = entityId;
                document.getElementById('amount').value = amount;                
                document.getElementById('groupName').value = gName;                
                document.getElementById('groupNumber').value = gNumber;                
            }
            
            function validateAndSearchMember(frm, btn) {
                document.getElementById('RecieptsSearchCommand').value = 'Search';
                var isValid = false;
                
                if ($('#memNo').val().length > 0 || $('#idNo').val().length > 0 ) {
                    isValid = true;
                } else {
                    var valCount = $('#initials').val().length > 0 ? 1 : 0;
                    valCount = valCount + ( $('#surname').val().length > 0 ? 1 : 0);
                    valCount = valCount + ($('#dob').val().length > 0 ? 1 : 0);
                    if (valCount > 1 ) {
                        isValid = true;
                    }
                }
                
                if (isValid) {
                    $('#btn_MemSearch_error').text("");
                    submitFormWithAjaxPost(frm, 'UnallocatedtListDiv', btn);
                } else {
                    $('#btn_MemSearch_error').text("Please enter some search criteria");
                }
            }
            
            
            function validateAndSearchEmployer(frm, btn) {
                document.getElementById('RecieptsSearchCommand').value = 'Search';
                var isValid = false;
                
                if ($('#employerNumber').val().length > 0 || $('#employerName').val().length > 0 ) {
                    isValid = true;
                } 
                if (isValid) {
                    $('#btn_EmpSearch_error').text("");
                    submitFormWithAjaxPost(frm, 'UnallocatedtListDiv', btn);
                } else {
                    $('#btn_EmpSearch_error').text("Please enter some search criteria");
                }
            }
            
        </script>
    </head>
    <body>
     <agiletags:ControllerForm name="ReciptsSearchtForm">
        <input type="hidden" name="opperation" id="opperation" value="ContributionReciptsSearchCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="ReceiptsSearch" />
        <input type="hidden" name="command" id="RecieptsSearchCommand" value="" />
        <input type="hidden" name="entityId" id="entityId" value="" />
        <input type="hidden" name="coverNo" id="coverNo" value="" />
        <input type="hidden" name="memberName" id="memberName" value="" />
        <input type="hidden" name="memberSurname" id="memberSurname" value="" />
        <input type="hidden" name="amount" id="amount" value="" />
        <input type="hidden" name="groupName" id="groupName" value="" />
        <input type="hidden" name="groupNumber" id="groupNumber" value="" />
        <label class="header">Unallocated Receipts Search</label>
        <hr>
        <br>
        <table>
          <tr>
              <td width="160"><Label>Type</Label></td>
              <td>
                  <select style="width:215px" name="ReceiptsType" id="ReceiptsType" onChange="setElemStyle('MemberTable',this.value == 'M'? 'table' : 'none');setElemStyle('EmployerTable',this.value == 'E'? 'table' : 'none');">
                      <option value="M" selected>Member</option>
                      <option value="E">Group</option>
                  </select>
              </td>
                  
          </tr>
        </table>
        <table id="MemberTable">
            <tr><agiletags:LabelTextBoxError displayName="Member Number" elementName="memNo"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="ID Number" elementName="idNo"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Initials" elementName="initials"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="surname"/></tr>
            <tr><agiletags:LabelTextBoxDate displayname="Date of Birth" elementName="dob"/></tr>
            <tr><td colspan="2" align="right"><input type="button" value="Search" onclick="validateAndSearchMember(this.form, this);"></td>
                <td></td>
                <td></td>
                <td><label id="btn_MemSearch_error" class="error"></label></td>
            </tr>
        </table>
        <table id="EmployerTable" style="display:none;">
            <tr><agiletags:LabelTextBoxError displayName="Group Number" elementName="employerNumber"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Group Name" elementName="employerName"/></tr>
            <tr><agiletags:LabelTextBoxDate displayname="Contribution Period" elementName="contribPeriod"/></tr>
                <tr><td colspan="2" align="right"><input type="button" value="Search" onclick="validateAndSearchEmployer(this.form, this);"></td>
                    <td></td>
                    <td></td>
                    <td><label id="btn_EmpSearch_error" class="error"></label></td>
                </tr>
        </table>

    <div id="UnallocatedtListDiv"></div>
     </agiletags:ControllerForm>
    </body>
</html>

<%-- 
    Document   : MemberClaims
    Created on : 2011/05/13, 02:39:31
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script language="JavaScript">
           
          var error = true;
          
            $(document).ready(function(){
                
                $("#policyNumber_text").blur(function(){
                    getCoverByNumber($(this).val(), 'policyNumber');
                });
        
                
                var searchVal = $("#searchCalled").val(); // document.getElementById('searchCalled').value;
                if (searchVal == 'policyNumber'){
                    getCoverByNumber($("#policyNumber_text").val(), 'policyNumber');
                } 
                
                //OVERRIDE DEFAULT SUBMIT BEHAVIOUR
                $("#ViewClaimsForm").submit(function(){
                    
                    validateMember('ViewCoverClaimsCommand');
                    
                    //getCoverByNumber($(this).val(), 'policyNumber');
                    return true;
                });
        
            });
           
            //member search
            function getCoverByNumber(str, element){
                
                document.getElementById('memberButton').disabled = true;
                $("#policyNumber_error").text("");
                
                if(str != null && str != ""){
                    
                    var url =  "/ManagedCare/AgileController";
                    
                    var data = {
                        "opperation":"GetMemberByNumberCommand",
                        "number":str,
                        "nocache": new Date().getTime()
                    }
                    
                    $.get(url, data, function(response){
                        
                        var result = response.substring(0, response.indexOf('|'));
                        if (result != null){
                        
                        if(result == "Error"){
                            
                            $("#"+element+"_error").text("No Cover Found");
                            error = true;
                        
                    }else if(result == "Access"){
                            
                            $("#"+element+"_error").text("User access denied. Please contact your supervisor");
                            error = true;
                        
                    }              
                    else{
                            error = false;
                            $("#memberButton").removeAttr('disabled');
                            $("#"+element+"_error").text("");
                        }            
                    }
                },'TEXT');
                    
                }else{
                    
                    $("#"+element+"_error").text("Please enter a Cover Number");
                }
            }

            
            function validateMember(action){
                
                if(error == false){
                    submitWithAction(action);
                }                
                
            }


            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.getElementById('onScreen').value = "MemberCCLogin";
                document.forms[0].submit();
                $("#memberButton").attr("disabled", "disabled");
            }

            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                // Session Here for exactCoverNumber
                document.forms[0].submit();
                $("#memberButton").attr("disabled", "disabled");
            }


        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">

                    <label class="header">View Member</label>
                    <br>
                    <table>
                        <agiletags:ControllerForm name="ViewClaimsForm" >
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />

                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Member Number" elementName="policyNumber" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/Claims/MemberClaims.jsp" mandatory="yes"/></tr>
                            <tr>
                                <td colspan="2" align="right">
                                    <button id="memberButton" name="memberButton" type="submit" value="" disabled >View</button>
                                </td>
                            </tr>
                            
                        </agiletags:ControllerForm>
                    </table>

                </td></tr></table>
    </body>
</html>

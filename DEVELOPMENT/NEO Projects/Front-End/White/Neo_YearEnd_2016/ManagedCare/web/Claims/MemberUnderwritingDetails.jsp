<%-- 
    Document   : MemberBankingDetails
    Created on : 2011/08/19, 02:05:39
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions' %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>


        <script  language="JavaScript">
            
            $(document).ready(function(){
                
                LoadTable();
                
                $("#dependantSelect").change(function(){
                    LoadTable();
                });
                
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });            
                  
            });

            function LoadTable() {
                
                var url =  "/ManagedCare/AgileController";
                    
                var data = {
                    "opperation":"LoadMemberUnderwritingCommand",
                    "dependantSelect":$("#dependantSelect").val()
                }

                $.get(url,data,function(response){
                    $("#underwritingTable").html(response);
                });

            }
            
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitGenericReload(action, onScreen){
                document.getElementById("onScreen").value = onScreen;
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }
            

        </script>
    </head>
    <body>
        <table width=100% height=100%>
            <tr valign="center">
                <td width="5px"></td>
                <td align="left">

                    <agiletags:CallCenterTabs cctSelectedIndex="link_UD" />
                    <fieldset id="pageBorder">
                        <div class="content_area" >

                            <label class="header">Underwriting Details</label>
                            <br/><br/>

                            <div id="tabTableLayout">
                                <agiletags:ControllerForm name="MemberUnderwritingForm">

                                    <input type="hidden" name="opperation" id="opperation" value="" />
                                    <input type="hidden" name="buttonPressed" id="buttonPressedUnderwriting" value="" />
                                    <input type="hidden" name="depCoverId" id="depCoverId" value="" />
                                    <input type="hidden" name="depLJP" id="depLJP" value="" />
                                    <input type="hidden" name="writingEntityId" id="writingEntityId" value="" />
                                    <input type="hidden" name="exclutionType" id="exclutionType" value="" />
                                    <input type="hidden" name="exclutionId" id="exclutionId" value="" />
                                    <input type="hidden" name="writingPMB" id="writingPMB" value="" />
                                    <input type="hidden" name="writingStartDate" id="writingStartDate" value="" />

                                    <c:if test="${warningW == 'claims'}" >
                                        <span style="color: red">Warning:Cover has claims</span>
                                    </c:if>
                                    <c:if test="${warningW == 'pre'}" >
                                        <span style="color: red">Warning:Cover has pre authorizations</span>
                                    </c:if>
                                    <c:if test="${warningW == 'both'}" >
                                        <span style="color: red">Warning:Cover has claims and pre authorizations</span>
                                    </c:if>
                                    <br/>
                                    <br/>
                                    <label class="subheader">Late Joiner Penalty</label>
                                    <agiletags:CoverDependentPenaltyJoinDisplay sessionAttribute="MemberCoverDependantDetails" commandName="" javaScript="memberDepSelect" onScreen="/Member/MemberUnderwriting.jsp" />
                                    <br/><br/>

                                    <label class="subheader">Waiting Periods</label>
                                    <agiletags:CoverDependentWaitingPeriodDisplay sessionAttribute="MemberCoverDependantDetails" commandName="" javaScript="memberDepSelect" onScreen="/Member/MemberUnderwriting.jsp"/>
                                    <br/><br/>
                                   <agiletags:CoverConcessionDisplay  sessionAttribute="MemberCoverDependantDetails" commandName="" javaScript="memberDepSelect" onScreen="/Member/MemberUnderwriting.jsp"/>
                                    <br/><br/>
                                    <label class="subheader">Condition Specific</label>


                                    <agiletags:CoverDependentConditionSpecificDisplay sessionAttribute="MemberCoverDependantDetails" commandName="" javaScript="memberDepSelect" onScreen="/Member/MemberUnderwriting.jsp"/>
                                </agiletags:ControllerForm>
                            </div> 
                        </div>
                    </fieldset>
                </td>
            </tr>
        </table>
    </body>
</html>

<%-- 
    Document   : PracticeBankingDetails
    Created on : 2011/08/17, 02:18:51
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript">
            
            $(function() {
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });        
                                
            });
            
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>

        <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">
                    
                    <agiletags:CallCenterProviderTabs isClearingUser="${sessionScope.persist_user_ccClearing}" cctSelectedIndex="link_prov_BD" />

                    <fieldset id="pageBorder">
                        <div class="content_area" >                 
                            <label class="header">Banking Details</label>
                            <br/><br/>
                            <table>
                                <agiletags:ControllerForm name="practiceDetails">
                                    <input type="hidden" name="opperation" id="opperation" value="" />
                                    <agiletags:HiddenField elementName="searchCalled" />
                                    <input type="hidden" name="onScreen" id="onScreen" value="" />
                                    <input type="hidden" name="listIndex" id="listIndex" value=""/>

                                    <tr><agiletags:LabelTextBoxError displayName="Practice Number" elementName="practiceNumber"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="First Name" elementName="practiceName"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="practiceSurname"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Bank Name" elementName="pracBankName"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Branch Code" elementName="pracBranchCode"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Branch Name" elementName="pracBranchName"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Account Number" elementName="pracAccountNumber"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Account Name" elementName="pracAccountName"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Account Type" elementName="pracAccountType"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Effective Date" elementName="pracEffectBankDate" valueFromSession="Yes" readonly="yes"/></tr>

                                </agiletags:ControllerForm>
                            </table>
                        </div></fieldset>
                </td></tr></table>
    </body>
</html>

<%-- 
    Document   : PaymentRunStatements
    Created on : Oct 20, 2011, 12:56:16 PM
    Author     : Johan-NB
--%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script language="JavaScript">

            $(function() {toggleStmtType();});

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction1(action, actionParam) {
                document.getElementById('opperation').value = action;
                document.getElementById('opperationParam').value = actionParam;
                document.getElementById('onScreen').value = "payRunStatement";
                var stmtDate = $("#stmtDateStr").text();
                document.getElementById('stmtDates').value = stmtDate;
                                
                document.forms[0].submit();
            }

            function submitWithAction(action, forField, onScreen) {
                document.getElementById('opperation').value = action;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('onScreen').value = onScreen;
                document.forms[0].submit();
            }
            
            function toggleStmtType(){
                var str = "<%= session.getAttribute("payRunStmtType")%>";
                
                $("#memRow").hide();
                $("#memEmail").hide();
                $("#provRow").hide();
                $("#pracEmail").hide();
                
                if (str == "1"){
                    $("#memRow").show();
                    $("#memEmail").show();
                    $("#provRow").hide();
                    $("#pracEmail").hide();
                }else if (str == "2"){
                    $("#memRow").hide();
                    $("#memEmail").hide();
                    $("#provRow").show();
                    $("#pracEmail").show();
                }
                document.getElementById('stmtType').value = str;
                
            }

            //screen validtion
            function validateStatementRequest(action, actionParam, validateEmail){
                //alert("Inside here validateStatementRequest");
                var errorCount = 0;
                var errorMsg = "";

                //validate email
                if(validateEmail == "yes"){ 
                    var email1 = $("#practiceEmailAddress").val();
                    var email2 = $("#memberEmailAddress").val();
                    var mailError = "";
                    var mailEmpty = false;
                    var checkOther = false;
                    if(email1 == null || email1 == ""){
                        mailEmpty = true
                        mailError = "#practiceEmailAddress";
                        checkOther = true;
                    }else{
                        mailEmpty = false;                        
                    }
                    
                    if(checkOther){
                        if(email2 == null || email2 == ""){
                            mailEmpty = true
                            mailError = "#memberEmailAddress";
                        }else{
                            mailEmpty = false;
                        }                        
                    }
                    
                    if(mailEmpty == true){
                        errorCount++;
                        errorMsg = mailError+ "_error-Please Enter a Email Address|";                        
                    }

                }
                
                //print errors
                if(errorCount > 0){
                    //alert(errorCount);
                    var errors = errorMsg.split("|");
                    for(i=0; i<errors.length; i++) {
                        var error = errors[i].split("-");
                        $(error[0]).text(error[1]);
                    }

                }else{
                    submitWithAction1(action, actionParam);
                }
            }


        </script>
    </head>
    <body>

        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">

                    <label class="header">Payment Run Statements</label>
                    <br/><br/>
                    <table>
                        <agiletags:ControllerForm name="getPayRunStmt">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                            <input type="hidden" name="stmtType" id="stmtType" value="" />
                            <input type="hidden" name="stmtDates" id="stmtDates" value="" />
                            <table>
                                <tr><agiletags:LabelTextDisplay displayName="Statement Type" elementName="stmtTypeStr" valueFromSession="yes" /></tr>
                                <tr id="memRow"><agiletags:LabelTextBoxError displayName="Member Number" elementName="policyHolderNumber" readonly="Yes" valueFromSession="Yes" javaScript="" /></tr>
                                <tr id="provRow"><agiletags:LabelTextBoxError displayName="Provider Number" elementName="practiceNumber"  readonly="Yes" valueFromSession="Yes" javaScript="" /></tr>                                
                                <tr><agiletags:LabelTextDisplay displayName="Statement Dates" elementName="stmtDateStr" valueFromSession="yes"/></tr>
                                <tr id="memEmail"><agiletags:LabelTextBoxError displayName="E-Mail Address" elementName="memberEmailAddress" valueFromSession="Yes"/></tr>
                                <tr id="pracEmail"><agiletags:LabelTextBoxError displayName="E-Mail Address" elementName="practiceEmailAddress" valueFromSession="Yes"/></tr>
                            </table>
                            <table>
                                <tr>
                                    <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="ReturnMemberPayRunStatement" displayname="Return" javaScript="onClick=\"submitWithAction('ReturnMemberPayRunStatement')\";"/>
                                    <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="SubmitClaimCallCenterStatementCommand" displayname="Preview" javaScript="onClick=\"validateStatementRequest('SubmitClaimCallCenterStatementCommand', 'Preview', 'no')\";"/>
                                    <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="SubmitClaimCallCenterStatementCommand" displayname="Send" javaScript="onClick=\"validateStatementRequest('SubmitClaimCallCenterStatementCommand', 'Send', 'yes')\";"/>
                                </tr>
                            </table>
                        </agiletags:ControllerForm>

                    </table>

                </td></tr></table>
    </body>
</html>

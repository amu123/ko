<%-- 
    Document   : MemberNominatedProviderDetails
    Created on : Aug 17, 2016, 12:32:47 PM
    Author     : charlh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script>
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <div id="main_div">
            <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">
                        <agiletags:CallCenterTabs cctSelectedIndex="link_DSP" />
                        <!--<label class="header">Principle Member Details</label>-->
                        <hr>
                        <agiletags:ControllerForm name="MemberNominatedProviderForm">
                            <input type="hidden" name="opperation" id="opperation" value="SaveMemberNominatedProviderCommand" />
                            <input type="hidden" name="buttonPressed" id="DSPButtonPressed" value="" />
                            <table>
                                <tr><agiletags:EntityCoverDependantDropDown displayName="Select Dependant" elementName="depListValues" javaScript="" mandatory="no" valueFromSession="yes" /></tr>
                                <tr><agiletags:LabelTextBoxErrorReq displayName="Dependant Name"  elementName="DSP_dependantName" mandatory="no"  valueFromRequest="DSP_dependantName"/></tr>
                                <tr><agiletags:LabelTextBoxErrorReq displayName="Dependant Surname"  elementName="DSP_dependantSurname" mandatory="no"  valueFromRequest="DSP_dependantSurname"/></tr>
                                <tr><agiletags:LabelTextBoxErrorReq displayName="Dependant Code"  elementName="DSP_dependantCode" mandatory="no"  valueFromRequest="DSP_dependantCode"/></tr>
                            </table>
                            <br/><br/>
                            <label class="subheader">Nominated Practitioner Details</label>
                            <br/><br/>
                            <table>
                                <tr><agiletags:LabelTextSearchWithNoText displayName="GP Name" elementName="DSP_doctorPracticeName" mandatory="no" onClick="document.getElementById('DSPButtonPressed').value = 'doctorSearch'; getPageContents(document.forms['MemberNominatedProviderForm'],null,'main_div','overlay');"/></tr>  
                                <tr><agiletags:LabelTextBoxErrorReq displayName="GP Practice Number" elementName="DSP_doctorPracticeNumber" mandatory="no" valueFromRequest="DSP_doctorPracticeNumber"/></tr>
                                <!--<tr><agiletags:LabelTextBoxErrorReq displayName="BHF Practice Number"  elementName="DSP_doctorBHFPracticeNumber" mandatory="no" readonly="yes" valueFromRequest="DSP_doctorBHFPracticeNumber"/></tr>-->
                                <tr><agiletags:LabelTextBoxErrorReq displayName="Network"  elementName="DSP_doctorPracticeNetwork" mandatory="no" readonly="yes" valueFromRequest="DSP_doctorPracticeNetwork"/></tr>
                                <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="DSP_doctorPracticeStartDate" valueFromSession="yes" mandatory="no"/></tr>
                                <tr><agiletags:LabelTextBoxDateReq  displayname="End Date" elementName="DSP_doctorPracticeEndDate" valueFromSession="yes" mandatory="no"/></tr>
                            </table>
                            <br/>
                            <table>
                                <tr><agiletags:LabelTextSearchWithNoText displayName="Dentist Name" elementName="DSP_dentistPracticeName" mandatory="no" onClick="document.getElementById('DSPButtonPressed').value = 'dentistSearch'; getPageContents(document.forms['MemberNominatedProviderForm'],null,'main_div','overlay');"/></tr>
                                <tr><agiletags:LabelTextBoxErrorReq displayName="Dentist Practice Number" elementName="DSP_dentistPracticeNumber" mandatory="no" valueFromRequest="DSP_dentistPracticeNumber"/>
                                <!--<tr><agiletags:LabelTextBoxErrorReq displayName="BHF Practice Number"  elementName="DSP_dentistBHFPracticeNumber" mandatory="no"  valueFromRequest="DSP_dentistBHFPracticeNumber"/></tr>-->
                                <tr><agiletags:LabelTextBoxErrorReq displayName="Network"  elementName="DSP_dentistPracticeNetwork" mandatory="no" readonly="yes" valueFromRequest="DSP_dentistPracticeNetwork"/></tr>
                                <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="DSP_dentistPracticeStartDate" valueFromSession="yes" mandatory="no"/></tr>
                                <tr><agiletags:LabelTextBoxDateReq  displayname="End Date" elementName="DSP_dentistPracticeEndDate" valueFromSession="yes" mandatory="no"/></tr>
                            </table>
                            <br/>
                            <table>
                                <tr><agiletags:LabelTextSearchWithNoText displayName="Optometrist Name" elementName="DSP_optometristPracticeName" mandatory="no" onClick="document.getElementById('DSPButtonPressed').value = 'optometristSearch'; getPageContents(document.forms['MemberNominatedProviderForm'],null,'main_div','overlay');"/></tr>
                                <tr><agiletags:LabelTextBoxErrorReq displayName="Optometrist Practice Number"  elementName="DSP_optometristPracticeNumber" mandatory="no"  valueFromRequest="DSP_optometristPracticeNumber"/></tr>
                                <!--<tr><agiletags:LabelTextBoxErrorReq displayName="BHF Practice Number"  elementName="DSP_optometristBHFPracticeNumber" mandatory="no"  valueFromRequest="DSP_optometristBHFPracticeNumber"/></tr>-->
                                <tr><agiletags:LabelTextBoxErrorReq displayName="Network"  elementName="DSP_optometristPracticeNetwork" mandatory="no" readonly="yes" valueFromRequest="DSP_optometristPracticeNetwork"/></tr>
                                <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="DSP_optometristPracticeStartDate" valueFromSession="yes" mandatory="no"/></tr>
                                <tr><agiletags:LabelTextBoxDateReq  displayname="End Date" elementName="DSP_optometristPracticeEndDate" valueFromSession="yes" mandatory="no"/></tr>
                            </table>
                            <br/><br/>
                            <table id="MemberAppSaveTable">
                                <tr>
                                    <td><input type="reset" value="Reset"></td>
                                    <td><input type="button" value="Save" onclick="document.getElementById('DSPButtonPressed').value = 'SaveDSP'; submitFormWithAjaxPost(this.form, 'main_div', this);"></td>
                                </tr>
                            </table>
                            <label class="subheader">Principle Member Nominated Practitioner History</label>
                            <br/>
                            <table>
                                <tr><td><agiletags:MemberClaimsSearchTable commandName="" javaScript="" /></td></tr>
                            </table>
                            <hr>

                            <c:forEach items="allDependantsList" var="entry">

                            </c:forEach>
                        </agiletags:ControllerForm>
                    </td>
            </table>
        </div>
        <div id="overlay" style="display: none"></div>
    </body>
</html>
<%-- 
    Document   : MemberPaymentDetails
    Created on : 2011/08/19, 12:03:25
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script language="JavaScript">

            $(function(){
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });            
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            
            function submitPDDAction(action, payRunStmtDate, onScreen){
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('payRunStmtDate').value = payRunStmtDate;
                document.getElementById('opperation').value = action;
                document.getElementById('returnScreen').value = "/Claims/MemberPaymentDetails.jsp";
                document.forms[0].submit();                
            }

            function setSession(value, elementName) {
                var url = "/ManagedCare/AgileController";
                var data = {
                    //replace this with your command name
                    'opperation': 'CaptureSessionCommand',
                    'value_text': value,
                    'elementName_text': elementName,
                    'id': new Date().getTime()
                };
                
                $.get(url, data, function() {
                    submitWithAction('ProductIDSelectionCommand');
                });
                data = null;
            }
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
            <agiletags:CallCenterTabs cctSelectedIndex="link_PR" />
                <fieldset id="pageBorder">
                    <div class="content_area" >
                        <label class="header">Payment Run Details</label>
                        <br/><br/>
                        <table>
                                <agiletags:ProductIdDropdownTag javascript="onchange=\"setSession(this.value, 'ProductIDSelection');\""/>
                        </table>
                        <br/>
                        <table>
                            <agiletags:ControllerForm name="ViewClaimsForm">
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <agiletags:HiddenField elementName="searchCalled" />
                                <input type="hidden" name="onScreen" id="onScreen" value="" />
                                <input type="hidden" name="payRunStmtDate" id="payRunStmtDate" value="" />
                                <input type="hidden" name="returnScreen" id="returnScreen" value="" />
                                <input type="hidden" name="folderList" id="folderList" value="" />
                                <input type="hidden" name="fileLocation" id="fileLocation" value="" />
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <tr><agiletags:ViewPaymentRunDetailsTable commandName="ViewPayRunStatements" javaScript=""/></tr>
                            </agiletags:ControllerForm>
                        </table>
                    </div>
                </fieldset>
        </td></tr></table>
    </body>
</html>

<%-- 
    Document   : ProviderViewDocuments
    Created on : Aug 31, 2016, 10:54:09 PM
    Author     : janf
--%>
<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags"%>
<link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
<link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>
<script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
<script type='text/javascript' src="${pageContext.request.contextPath}/resources/Membership/MemberMaintenance.js"></script>
<script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
<script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
<agiletags:CallCenterProviderTabs isClearingUser="${sessionScope.persist_user_ccClearing}" cctSelectedIndex="link_prov_Docs"/>
<br/><br/>
<label class="header">Provider Document</label>
<hr>

<agiletags:ControllerForm name="documentSorting">
    <input type="hidden" name="opperation" id="opperation" value="MemberDocumentDisplayViewCommand" />
    <input type="hidden" name="fileLocation" id="fileLocation" value="" />
    <input type="hidden" name="opperationParam" id="opperationParam" value="" />
    <input type="hidden" name="memberNumber" id="memberNumber" value="${entityId}" />
    <table>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Documents"  elementName="docKind" lookupId="254" errorValueFromSession="no" javaScript="onChange=\"toogleDocument(this.value, 'docType');\""/></tr>                                    
        <tr><agiletags:DocumentsList displayName="Type"  elementName="docType" mandatory="no" errorValueFromSession="yes" parentElement="docKind" /></tr>
        <c:if test="${applicationScope.Client == 'Sechaba'}">
            <tr id="documentUpload" style="display: none">
                <td><label>Document</label></td>
                <td><input type="file" name="uploadDoc" id="uploadDoc" size="75" form="uploadForm" /></td>
                <td><input type="submit" id="submitFile" value="Upload Document" form="uploadForm" onclick="document.getElementById('docuType').value = document.getElementById('docType').value; document.getElementById('submitFile').disabled;"/></td>
            </tr>
        </c:if>
    </table>
    <!--<tr><td align="left" width="160px"><label class="label" >Type:</label></td>
        <td><select name="docType" id="docType" style="width:215px"></select></td></tr>-->                                   

    <label class="subheader">Provider Documents</label>    
    <table>
        <td><label>Start Date:</label></td>
        <td><input name="minDate" id="minDate" size="30" value="${minDate}"></td>
        <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('minDate', this);"/></td>
        <td>&nbsp;</td>
        <td><label>End Date:</label></td>
        <td><input name="maxDate" id="maxDate" size="30" value="${maxDate}"></td>
        <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('maxDate', this);"/></td>
        <td>&nbsp;</td>
        <td><input type="button" name="SearchButton" id="searchButton" value="Search" onclick="document.getElementById('opperationParam').value = 'sort';submitActionAndFile(this.form, 'DocumentFilterCommand', null); submitFormWithAjaxPost(this.form, 'MemberDocumentDetails');"/></td>
    </table>
    <div style ="display: none"  id="MemberDocumentDetails"></div>
</agiletags:ControllerForm>
<iframe name="uploadFrame" id="uploadFrame" style="display: none"></iframe>    
<form id="uploadForm" enctype="multipart/form-data" target="uploadFrame" method="POST" action="/ManagedCare/AgileController?opperation=MemberDocumentUploadCommand&memberNumber=${entityId}&entityType=3">
    <input type="hidden" name="docuType" id="docuType" value="" />
</form>

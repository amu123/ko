<%-- 
    Document   : CallCenterAuditTrail
    Created on : 25 Sep 2013, 2:34:04 PM
    Author     : pavaniv
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script>
            function submitWithAction(action, auditField) {
                document.forms[0].elements["opperation"].value = action;
                document.getElementById('auditField').value = auditField;
                document.forms[0].submit();
                
            }
       
        </script>
    </head>
    <body onload="initAgileTabs();">

        <agiletags:CallCenterTabs cctSelectedIndex="link_AT" />
        <agiletags:ControllerForm name="policyHolderAudit">
            <input type="hidden" name="opperation" id="opperation" value="CustomerCareAuditCommand" />
            <input type="hidden" name="auditField" id="auditField" value="" />
            <input type="hidden" name="buttonPressed" id="buttonPressed" value="" />
            <input type="hidden" name="memberEntityId" id="memberEntityId" value="${sessionScope.coverEntityId}" />
            <div id="tabTableLayout">
                <label class="header">Member Audit Trail</label>
                <hr>

                <table id="MemberAppAuditTable">
                    <tr>
                        <td width="160" class="label">Details</td>
                        <td>
                            <select style="width:215px" name="AuditList" id="AuditList" onChange="submitWithAction('CustomerCareAuditCommand', this.value);">
                                <option value="99" selected>&nbsp;</option>
                                <option value="PERSON_DETAILS">Member Details</option>
                                <option value="ADDRESS_DETAILS">Address Details</option>
                                <option value="BANK_DETAILS">Bank Details</option>
                                <option value="CONTACT_DETAILS">Contact Details</option>
                                <option value="CONTACT_PREF_DETAILS">Contact Preference Details</option>
                                <option value="COVER_DETAILS">Cover Details</option>
                                <option value="MEMBER_GROUP">Member Group</option>
                                <option value="MEMBER_BROKER">Member Broker</option>
                                <option value="MEMBER_UNDERWRITING">Member Underwriting</option>
                                <option value="MEMBER_ALL">All</option>
                            </select>
                        </td>
                    </tr>

                </table>

            </agiletags:ControllerForm>
        </div>
        <%
            String SelectOption = request.getParameter("AuditList");

        %> 
        <script>
            document.getElementById("AuditList").value = '<% out.print(SelectOption);%>';
        </script>
        <br>                          
        <div id="tabTableLayout">
            <br>
            <label class="subheader">Member Audit Trail Details</label>
            <hr>
            <br>
            <c:choose>
                <c:when test="${empty CallCenterAuditTrail}">

                    <span class="label">No Audit details available</span>

                </c:when>
                <c:otherwise>
                    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                        <tr>
                            <c:forEach var="headerMap" items="${CallCenterAuditTrail[0]}">
                                <th>${headerMap.key}</th>
                            </c:forEach>           
                        </tr>
                        <c:forEach var="entry" items="${CallCenterAuditTrail}">
                            <tr>
                                <c:forEach var="item" items="${entry}">
                                    <td><label>${item.value}</label></td>
                                </c:forEach>
                            </tr>
                        </c:forEach>
                    </table>
                </c:otherwise>
            </c:choose>
        </div> 
    </body>
</html>
<%-- 
    Document   : PreAuthCallCenterConfirmation
    Created on : 2011/08/04, 01:48:31
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ page import="neo.manager.NeoUser" %>
<%@ page import="neo.manager.SecurityResponsibility" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="../resources/styles.css"/>
        <script type="text/javascript" src="../resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="../resources/jQuery/jquery-1.4.2.js"></script>

        <%

            String authStatus = "" + session.getAttribute("authStatus");
            System.out.println("auth type on confirmation = " + authStatus);
            if (authStatus.trim().equalsIgnoreCase("reject")) {

        %>

        <style type="text/css">
            body {
                background-image: url(../resources/rejected_auth.jpg);
                background-position: top center;
                background-repeat: no-repeat;
            }

            @media print {
                body {
                    background-image: url(../resources/rejected_auth.jpg);
                    background-position: top center;
                    background-repeat: no-repeat;
                }
                #AuthButtonRow, #footer, #nav, #sidenav, .print, .search, .breadcrumb, .noprint {display: none;}
            }

        </style>
        <% } else if (authStatus.trim().equalsIgnoreCase("pend")) {%>
        <style type="text/css">
            body {
                background-image: url(../resources/pended_auth.jpg);
                background-position: top center;
                background-repeat: no-repeat;
            }

            @media print {
                body {
                    background-image: url(../resources/pended_auth.jpg);
                    background-position: top center;
                    background-repeat: no-repeat;
                }
                #AuthButtonRow, #footer, #nav, #sidenav, .print, .search, .breadcrumb, .noprint {display: none;}
            }

        </style>        

        <% } else {%>
        <style type="text/css" media="print">
            #AuthButtonRow, #footer, #nav, #sidenav, .print, .search, .breadcrumb, .noprint {display: none;}

        </style>

        <% }%>

        <script type="text/javascript" language="JavaScript">

            $(document).ready(function () {
            <%
                NeoUser user = (NeoUser) session.getAttribute("persist_user");
                for (SecurityResponsibility security : user.getSecurityResponsibility()) {
                    if (security.getResponsibilityId() == 9 || security.getResponsibilityId() == 11) {%>
                $("#createAuth").hide();
                $("#updateAuth").hide();
            <%} else {%>
                $("#createAuth").show();
                $("#updateAuth").show();

            <%}
                }%>
            });

            function GetXmlHttpObject() {
                if (window.ActiveXObject) {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function GenerateConfirmation() {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("ERROR: Browser Incompatability");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=ViewPreAuthConfirmation";
                xhr.onreadystatechange = function () {};
                xhr.open("POST", url, true);
                xhr.send(null);
            }


            function ForwardRequest(command) {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("ERROR: Browser Incompatability");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=" + command;
                xhr.onreadystatechange = function () {};
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function hideRows(id) {
                $("#" + id + "plus").show();
                $("#" + id + "minus").hide();
                var hiddenRows = $("tr[id^=" + id + "hidden]");
                for (i = 0; i < hiddenRows.size(); i++) {
                    $(hiddenRows[i]).hide();
                }

            }
            function showRows(id) {
                $("#" + id + "plus").hide();
                $("#" + id + "minus").show();
                var hiddenRows = $("tr[id^=" + id + "hidden]");
                for (i = 0; i < hiddenRows.size(); i++) {
                    $(hiddenRows[i]).show();
                }
            }

            function printConfirmation() {
                parent.frames["worker"].print();

            }

            function submitWithAction(action) {
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById("onScreen").value = onScreen;
                document.getElementById("searchCalled").value = forField;
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }

        </script>

    </head>

    <body id="confimationBody">

        <agiletags:ControllerForm name="preAuthConfirm">
            <input type="hidden" name="opperation" id="opperation" value="" />
            <agiletags:HiddenField elementName="searchCalled" />
            <input type="hidden" name="onScreen" id="onScreen" value="" />
            <input type="hidden" name="listIndex" id="listIndex" value=""/>
        </agiletags:ControllerForm>
        <div>
            
    <c:if test="${applicationScope.Client == 'Sechaba'}">
            <img src="../resources/Sechaba logo.png"  alt="Agility_Logo" align="middle"/>
            </c:if>
            <c:if test="${applicationScope.Client == 'Agility'}">
            <img src="../resources/Agility GHS logo.png"  alt="Agility_Logo" align="middle"/>
            </c:if>
            &nbsp;&nbsp;
            <label class="confiramtionHeader">Authorisation</label>
        </div>
        <br/>
        <div>
            <label class="confirmationSubHeaders">Practice Details</label>
        </div>
        <br/>
        <table align="center" width="100%">
            <tr>
                <agiletags:LabelTextDisplay displayName="Provider Number" elementName="treatingProvider_text" boldDisplay="yes" valueFromSession="yes" />
                <agiletags:LabelTextDisplay displayName="Provider Tel" elementName="treatingProviderTel" boldDisplay="yes" valueFromSession="yes" />
            </tr>
            <tr>
                <agiletags:LabelTextDisplay displayName="Provider Name" elementName="treatingProviderName" boldDisplay="yes" valueFromSession="yes" />
                <agiletags:LabelTextDisplay displayName="Provider Fax" elementName="treatingProviderFax" boldDisplay="yes" valueFromSession="yes" />
            </tr>
        </table>
        <br/>
        <div>
            <label class="confirmationSubHeaders">Member Details</label>
        </div>
        <br/>
        <table align="center" width="100%">
            <tr>
                <agiletags:LabelTextDisplay displayName="Member Number" elementName="memberNum_text" boldDisplay="yes" valueFromSession="yes" />
                <agiletags:LabelTextDisplay displayName="Dependant Number" elementName="depListValues" boldDisplay="yes" valueFromSession="yes" />
            </tr>
            <tr>
                <agiletags:LabelTextDisplay displayName="Name" elementName="memberName" boldDisplay="yes" valueFromSession="yes" />
                <agiletags:LabelTextDisplay displayName="Surname" elementName="memberSurname" boldDisplay="yes" valueFromSession="yes" />
            </tr>
            <tr>
                <agiletags:LabelTextDisplay displayName="Date of Birth" elementName="memberDOB" boldDisplay="yes" valueFromSession="yes" />
                <agiletags:LabelTextDisplay displayName="Id Number" elementName="memberIdNum" boldDisplay="yes" valueFromSession="yes" />
            </tr>
            <tr>
                <agiletags:LabelTextDisplay displayName="Patient Age" elementName="memberAge" boldDisplay="yes" valueFromSession="yes" />
                <agiletags:LabelTextDisplay displayName="Gender" elementName="memberGender" boldDisplay="yes" valueFromSession="yes" />
            </tr>
            <tr>
                <agiletags:LabelTextDisplay displayName="Scheme" elementName="schemeName" boldDisplay="yes" valueFromSession="yes" />
                <agiletags:LabelTextDisplay displayName="Scheme Option" elementName="schemeOptionName" boldDisplay="yes" valueFromSession="yes" />
            </tr>

            <tr>
                <agiletags:LabelTextDisplay displayName="Contact" elementName="memberContact" boldDisplay="yes" valueFromSession="yes" />
                <agiletags:LabelTextDisplay displayName="Status" elementName="memberStatus" boldDisplay="yes" valueFromSession="yes" />
            </tr>
        </table>
        <br/>
        <div>
            <label class="confirmationSubHeaders">Authorisation Details</label>
        </div>
        <br/>
        <table>
            <agiletags:AuthConfirmationByType />

        </table>
        <br/>
        <hr size="3px" bgcolor="#0000CC"/>
        <table>
            <tr>
                <c:if test="${!empty sessionScope.returnFlag && (sessionScope.returnFlag eq true || sessionScope.returnFlag eq 'true')}">
                    <td align="right" colspan="4">
                        <button name="opperation" type="button" onclick="submitWithAction('ViewSubWorkBenchCommand')" value="ViewSubWorkBenchCommand">Return</button>
                    </td>
                </c:if>
                <c:if test="${empty sessionScope.returnFlag || sessionScope.returnFlag eq '' || sessionScope.returnFlag eq null}">
                    <td align="right" colspan="4">
                        <button name="btn_return" type="button" onClick="submitWithAction('ReturnMemberCCPreAuth');" value="" >Return to Authorisation</button>
                    </td>
                </c:if>
            </tr>
        </table>            
    </body>
</html>

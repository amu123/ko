<%-- 
    Document   : GenericMemberPreAuthorisation
    Created on : 2011/08/18, 01:54:49
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="neo.manager.NeoUser" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" language="JavaScript">
            //set current user
            <%
                NeoUser user = (NeoUser) session.getAttribute("persist_user");
                session.setAttribute("currentUser", user.getName() + " " + user.getSurname());
            %>
            function CheckSearchCommands() {
                //reload ajax commands for screen population
                var searchVal = document.getElementById('searchCalled').value;
                if (searchVal == 'memberNum') {
                    getCoverByNumber(document.getElementById('memberNum_text').value, 'memberNum');
                } else if (searchVal == 'treatingProvider') {
                    validateProvider(document.getElementById('treatingProvider_text').value, 'treatingProvider');
                } else if (searchVal == 'primaryICD') {
                    getICD10ForCode(document.getElementById("primaryICD_text").value, 'primaryICD');
                } else if (searchVal == 'referringProvider') {
                    validateProvider(document.getElementById('referringProvider_text').value, 'referringProvider');
                }
                document.getElementById('searchCalled').value = '';
                toggleAuth(document.getElementById("authType").value);
                $("#depJoinDateRow").hide();
                //setCurrentUser();
                setAuthDate();
            }

            /*function setCurrentUser(){
             var userDetail = "<%= session.getAttribute("currentUser")%>";
             $("#currentUser").val(userDetail);
             
             }*/

            function GetXmlHttpObject() {
                if (window.ActiveXObject) {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function GetDOMParser(xmlStr) {
                var xmlDoc;
                if (window.DOMParser) {
                    parser = new DOMParser();
                    xmlDoc = parser.parseFromString(xmlStr, "text/xml");
                } else {
                    xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async = "false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }
            //member search
            function getCoverByNumber(str, element) {
                var ap = $("#authPeriod").val();
                if (ap != "99") {
                    $(document).find("#schemeName").text("");
                    $(document).find("#schemeOptionName").text("");
                    if (str != null && str != "") {
                        xhr = GetXmlHttpObject();
                        if (xhr == null) {
                            alert("ERROR: Browser Incompatability");
                            return;
                        }
                        var url = "/ManagedCare/AgileController";
                        url += "?opperation=GetCoverDetailsByNumber&number=" + str + "&element=" + element + "&exactCoverNum=1";
                        xhr.onreadystatechange = function () {
                            if (xhr.readyState == 4 && xhr.statusText == "OK") {
                                $("#depListValues").empty();
                                $("#depListValues").append("<option value=\"99\"></option>");
                                var xmlDoc = GetDOMParser(xhr.responseText);
                                //print value from servlet
                                $(xmlDoc).find("EAuthCoverDetails").each(function () {
                                    //set product details
                                    var prodName = $(this).find("ProductName").text();
                                    var optName = $(this).find("OptionName").text();

                                    //extract dependent info from tag
                                    $(this).find("DependantInfo").each(function () {
                                        var depInfo = $(this).text();
                                        var nameSurnameType = depInfo.split("|");
                                        //Add option to dependant dropdown
                                        var optVal = document.createElement('OPTION');
                                        optVal.value = nameSurnameType[0];
                                        optVal.text = nameSurnameType[1];
                                        document.getElementById("depListValues").options.add(optVal);
                                    });
                                    //DISPLAY MEMBER DETAIL LIST
                                    $(document).find("#schemeName").text(prodName);
                                    $(document).find("#schemeOptionName").text(optName);
                                    //error check
                                    $("#" + element + "_error").text("");
                                    var errorText = $(this).text();
                                    var errors = errorText.split("|");
                                    if (errors[0] == "ERROR") {
                                        $("#" + element + "_error").text(errors[1]);
                                    }

                                });
                            }
                        }
                        xhr.open("POST", url, true);
                        //xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                        xhr.send(null);
                    }
                }
            }

            function clearTariffList() {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=ClearAuthTariffsFromList";
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        if (result == "Done") {
                            $("#AuthTariff").hide();
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            //provider search
            function validateProvider(str, element) {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var error = "#" + element + "_error";
                if (str.length > 0) {
                    var url = "/ManagedCare/AgileController";
                    url = url + "?opperation=FindProviderDetailsByCode&provNum=" + str + "&element=" + element;
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.statusText == "OK") {
                            var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                            $(document).find(error).text("");
                            if (result == "Error") {
                                $(document).find(error).text("No such provider");
                            } else if (result == "Done") {
                                var description = xhr.responseText.substring(xhr.responseText.lastIndexOf('=') + 1, xhr.responseText.lastIndexOf('$'));
                                $("#providerDesc").val(description);
                                $(document).find(error).text("");
                            }
                        }
                    };
                    xhr.open("POST", url, true);
                    xhr.send(null);
                } else {
                    $(document).find(error).text("");
                }
            }

            function getDefaultLOCForDisplay(command) {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=LoadDefaultHospitalLOC";
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        if (result == "Done") {
                            submitWithAction(command);
                        } else if (result == "Error") {
                            alert(result);
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function getLabCodesForDisplay(command) {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=GetAuthLabCodeByTariffCommand&sesVal=tariffListArray";
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        if (result == "Done") {
                            submitWithAction(command);
                        } else if (result == "Error") {
                            alert(result);
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function getOrthoPlanForTariff(command) {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=GetAuthOrthoPlanDetails&sesVal=tariffListArray";
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        if (result == "Error") {
                            var error = "#authTariffButton_error";
                            $(document).find(error).text("No Orthodontic Plan for Tariff");
                        } else if (result == "Done") {
                            submitWithAction(command);
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function setICDToUpper(icd) {
                var icd = $.trim(icd);
                var tc = icd.charAt(0).toUpperCase();
                var sub = tc + icd.substring(1, icd.length);
                return sub;
            }

            function getICD10ForCode(str, element) {
                xhr = GetXmlHttpObject();
                if (xhr == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var str = $.trim(str);
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=GetICD10DetailsByCode&code=" + str + "&element=" + element;
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        var description = xhr.responseText.substring(xhr.responseText.lastIndexOf('=') + 1, xhr.responseText.lastIndexOf('$'));
                        var error = "#" + element + "_error";
                        var jqElement = "#" + element + "_text";
                        $(document).find('#icdDescription').text(description);
                        $(error).text("");
                        if (result == "Error") {
                            $(error).text("No such diagnosis");
                        } else {
                            var up = setICDToUpper(str);
                            $(jqElement).val(up);
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function validateMandatory(command) {
                ClearValidation();
                xhr = GetXmlHttpObject();
                if (xhr == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=ValidateGenericAuth";
                //get form values
                var authDate = $(document).find("#authDate").val();
                var memNo = $(document).find("#memberNum_text").val();
                var deplist = $("#depListValues").val();
                var treatProv = $(document).find("#treatingProvider_text").val();
                var pICD = $(document).find("#primaryICD_text").val();
                var reqName = $(document).find("#requestorName").val();
                var reqRelationship = $("#requestorRelationship").val();
                var reqReason = $(document).find("#requestorReason").val();
                var reqContact = $(document).find("#requestorContact").val();
                var authType = $("#authType").val();
                var authFrom = $("#authFromDate").val();
                var authTo = $("#authToDate").val();
                var authStatus = $("#authStatus").val();
                var authPeriod = $("#authPeriod").val();

                url = url + "&authDate=" + authDate + "&memberNum_text=" + memNo + "&depListValues=" + deplist + "&treatingProvider_text=" + treatProv;
                url = url + "&primaryICD_text=" + pICD + "&requestorName=" + reqName + "&requestorRelationship=" + reqRelationship;
                url = url + "&requestorReason=" + reqReason + "&requestorContact=" + reqContact + "&authType=" + authType;
                url = url + "&authFromDate=" + authFrom + "&authToDate=" + authTo + "&authStatus=" + authStatus + "&authPeriod=" + authPeriod;


                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));

                        if (result == "ERROR") {
                            var errorResponse = xhr.responseText.substring(xhr.responseText.indexOf('|') + 1, xhr.responseText.length)
                            var mandatoryList = errorResponse.split("|");
                            for (i = 0; i < mandatoryList.length; i++) {
                                var elementError = mandatoryList[i].split(":");
                                var element = elementError[0];
                                var errorMsg = elementError[1];

                                $(document).find("#" + element + "_error").text(errorMsg);
                            }

                        } else if (result == "Done") {
                            if (authType == '2' || authType == '13') {
                                getLabCodesForDisplay(command);

                            } else if (authType == '3') {
                                getOrthoPlanForTariff(command);

                            } else if (authType == '4') {
                                getDefaultLOCForDisplay(command);

                            } else {
                                submitWithAction(command);
                            }
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function displayDepJoinDate(depCode) {
                xhr = GetXmlHttpObject();
                if (xhr == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=DisplayCoverDepJoinDate&depCode=" + depCode;
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var resText = xhr.responseText;
                        var strArr = resText.split('|');
                        var result = strArr[0];
                        if (result == "Done") {
                            $("#depJoinDate").text(strArr[1]);
                            $("#depJoinDateRow").show();
                        } else {
                            $("#depListValues_error").val(strArr[1]);
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function ClearValidation() {

                $("#authType_error").text("");
                $("#authStatus_error").text("");
                $("#authFromDate_error").text("");
                $("#authToDate_error").text("");
                $("#authDate_error").text("");
                $("#memberNum_error").text("");
                $("#depListValues_error").text("");
                $("#treatingProvider_error").text("");
                $("#primaryICD_error").text("");
                $("#authTariffButton_error").text("");
                $("#requestorName_error").text("");
                $("#requestorRelationship_error").text("");
                $("#requestorReason_error").text("");
                $("#requestorContact_error").text("");

            }


            function setAuthDate() {
            <%
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
                Date today = new Date();
                String strDate = sdf.format(today);
                String strDate2 = sdf2.format(today);
                session.setAttribute("authDate", strDate2);
                session.setAttribute("authFromDate", strDate);
            %>
            }

            function toggleAuth(value) {
                $("#BenefitHeader").hide();
                $("#AuthBenefit").hide();
                $("#AuthBenButton").hide();

                if (value == '7') {
                    $("#AuthFrom").show();
                    $("#AuthTo").show();
                    $("#AuthNotes").show();
                    $("#AuthStatus").show();
                    $("#TariffSave").show();
                    $("#NormalSave").hide();
                    $("#BenefitSave").hide();
                    $("#AuthTButton").show();
                    $("#AuthTariff").show();
                    $("#TariffHeader").show();
                    $("#NappiSave").hide();
                    $("#NappiButton").hide();

                    //super user validation
                    var isSup = "<%= session.getAttribute("persist_user_sup")%>";
                    if (isSup == "true") {
                        $("#BenefitHeader").show();
                        $("#AuthBenefit").show();
                        $("#AuthBenButton").show();
                    }

                } else if (value == '9') {
                    $("#AuthFrom").show();
                    $("#AuthTo").show();
                    $("#AuthNotes").show();
                    $("#AuthStatus").hide();
                    $("#TariffSave").show();
                    $("#NormalSave").hide();
                    $("#BenefitSave").hide();
                    $("#AuthTButton").show();
                    $("#AuthTariff").show();
                    $("#TariffHeader").show();
                    $("#NappiSave").hide();
                    $("#NappiButton").hide();

                } else if (value == '8') {
                    $("#AuthFrom").show();
                    $("#AuthTo").show();
                    $("#AuthNotes").show();
                    $("#AuthStatus").show();
                    $("#TariffSave").hide();
                    $("#NormalSave").hide();
                    $("#BenefitSave").show();
                    $("#AuthTButton").show();
                    $("#AuthTariff").show();
                    $("#TariffHeader").show();
                    $("#NappiSave").hide();
                    $("#NappiButton").hide();

                } else if (value == '4' || value == '5') {
                    $("#NormalSave").show();
                    $("#AuthFrom").hide();
                    $("#AuthTo").hide();
                    $("#AuthNotes").hide();
                    $("#AuthStatus").hide();
                    $("#TariffSave").hide();
                    $("#BenefitSave").hide();
                    $("#AuthTButton").hide();
                    $("#AuthTariff").hide();
                    $("#TariffHeader").hide();
                    $("#NappiSave").hide();
                    $("#NappiButton").hide();

                } else if (value == '6') {
                    $("#AuthFrom").show();
                    $("#AuthTo").show();
                    $("#AuthNotes").show();
                    $("#AuthStatus").show();
                    $("#TariffSave").hide();
                    $("#NormalSave").hide();
                    $("#BenefitSave").hide();
                    $("#AuthTButton").hide();
                    $("#AuthTariff").hide();
                    $("#TariffHeader").hide();
                    $("#NappiSave").show();
                    $("#NappiButton").show();


                } else {
                    $("#l").show();
                    $("#AuthFrom").hide();
                    $("#AuthTo").hide();
                    $("#AuthNotes").hide();
                    $("#AuthStatus").hide();
                    $("#TariffSave").hide();
                    $("#BenefitSave").hide();
                    $("#AuthTButton").show();
                    $("#AuthTariff").show();
                    $("#TariffHeader").show();
                    $("#NappiButton").hide();
                    $("#NappiSave").hide();
                }
            }

            function validateAuthPeriodForTariff(action, forField, onScreen) {
                var ap = $("#authPeriod").val();
                $("#authPeriod_error").text("");
                if (ap != null && ap != "99") {
                    //check radiology provider
                    var authType = $("#authType").val();
                    if (authType == '9') {
                        //check provider discipline for 025 or 038
                        var pDescStr = $("#providerDesc").val();
                        var pd = pDescStr.substring(0, pDescStr.indexOf("-") - 1);
                        alert(pd);
                        if (pd == '025' || pd == '038') {
                            submitWithAction(action, forField, onScreen);
                        } else {
                            $("#treatingProvider_error").text("Radiology Auth not allowed for " + pd + " provider, only 025 or 038")
                        }
                    } else {
                        submitWithAction(action, forField, onScreen);
                    }
                } else {
                    $("#authPeriod_error").text("Auth Period Required for tariff allocation");
                }
            }

            //new date validation
            function validateADRequest(date, element) {
                var ap = $("#authPeriod").val();
                var at = $("#authType").val();
                validatePreAuthDate(date, element, ap, at, "no");

            }

            function clearElement(element) {
                part = element + "_text";
                document.getElementById(element).value = "";
                document.getElementById(part).value = "";
            }
            function submitWithAction(action) {
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById("onScreen").value = onScreen;
                document.getElementById("basketOnScreen").value = onScreen;
                document.getElementById("searchCalled").value = forField;
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body onload="CheckSearchCommands();">
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <agiletags:ClaimsTabbedButton buttonCommand2="ViewMemberPreAuthorisationCommand" buttonDisplay2="Authorisation"
                                                  buttonCommand3="ViewMemberAddressDetailsCommand" buttonDisplay3="Address Details"
                                                  buttonCommand4="ForwardToMemberBenefitsCommand" buttonDisplay4="Benefit Details"
                                                  buttonCommand5="ForwardToMemberClaimsCommand" buttonDisplay5="Claims"
                                                  buttonCommand6="ViewMemberPaymentDetailsCommand" buttonDisplay6="Payment Details"
                                                  buttonCommand7="ViewMemberBankingDetailsCommand" buttonDisplay7="Banking Details"
                                                  buttonCommand8="ForwardToMemberStatementsCommand" buttonDisplay8="Statements"
                                                  buttonCommand1="ForwardToMemberDetailsCommand" buttonDisplay1="Policy Holder Details"  disabledDisplay="Authorisation" numberOfButtons="8"/>

                    <div align="left"><label class="header">Authorisation Details</label></div>
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="saveAuth" validate="yes" >
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="basketOnScreen" id="basketOnScreen" value="" />
                            <input type="hidden" name="providerDesc" id="providerDesc" value="" />
                            <!-- Product Details -->
                            <tr><agiletags:LabelTextDisplay displayName="Scheme" elementName="schemeName" valueFromSession="yes" javaScript="" /></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Scheme Option" elementName="schemeOptionName" valueFromSession="yes" javaScript="" /></tr>

                            <!-- Auth type -->
                            <tr><agiletags:PreAuthPeriodIndicator displayName="Auth Period" elementName="authPeriod" javaScript="" mandatory="yes" valueFromSession="yes" /></tr>
                            <tr><agiletags:LabelNeoLookupValueDropDownError mandatory="yes" displayName="Authorisation Type" elementName="authType" lookupId="89" javaScript="onChange=\"toggleAuth(this.value);\"" errorValueFromSession="yes" /></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Authorisation Date" valueFromSession="Yes" elementName="authDate" mandatory="Yes" javaScript="" readonly="yes"/></tr>

                            <!-- Cover Details -->
                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Member Number" elementName="memberNum" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/PreAuth/GenericAuthorisation.jsp" mandatory="yes" javaScript="onChange=\"getCoverByNumber(this.value, 'memberNum');\""/></tr>
                            <tr><agiletags:EntityCoverDependantDropDown displayName="Cover Dependants" elementName="depListValues" javaScript="onChange=\"displayDepJoinDate(this.value);\"" mandatory="Yes" valueFromSession="yes"/></tr>
                            <tr id="depJoinDateRow"><agiletags:LabelTextDisplay boldDisplay="no" displayName="Dependant Join Date" elementName="depJoinDate" javaScript="" valueFromSession="yes" /></tr>
                            <!-- Cover Exclusions -->
                            <tr><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="View Exclusions" elementName="ViewExclusions" valueFromSession="yes" javaScript="onClick=\"submitWithAction('GetDependentCoverExclusions','getExButton','/PreAuth/GenericAuthorisation.jsp')\";" /></tr>
                            <tr><agiletags:AuthExclusionListTableTag javaScript="onClick=\"submitWithAction('ReloadCoverExclusionCommand','excludeButton','/PreAuth/GenericAuthorisation.jsp')\";" commandName="" /></tr>
                            <tr><td colspan="5"></td></tr>
                            <!-- Providers -->
                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Treating Provider" elementName="treatingProvider" searchFunction="yes" searchOperation="ForwardToSearchProviderCommand" onScreen="/PreAuth/GenericAuthorisation.jsp" mandatory="yes" javaScript="onChange=\"validateProvider(this.value, 'treatingProvider');\""/></tr>
                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Referring Provider" elementName="referringProvider" searchFunction="yes" searchOperation="ForwardToSearchProviderCommand" onScreen="/PreAuth/GenericAuthorisation.jsp" mandatory="no" javaScript="onChange=\"validateProvider(this.value, 'referringProvider');\""/></tr>
                            <!-- ICD -->
                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Primary ICD10" elementName="primaryICD" searchFunction="yes" searchOperation="ForwardToSearchPreAuthICD10" onScreen="/PreAuth/GenericAuthorisation.jsp" mandatory="yes" javaScript="onChange=\"getICD10ForCode(this.value, 'primaryICD');\""/></tr>
                            <tr id="showdescription"><agiletags:LabelTextDisplay displayName="ICD10 Description" elementName="icdDescription" valueFromSession="yes" javaScript=""/></tr>
                            <tr><td colspan="5"></td></tr>
                            <tr><td colspan="5"></td></tr>
                            <!-- Tariff Details -->
                            <tr id="TariffHeader"><td colspan="5" align="left"><label class="subheader">Tariff Details Allocated</label></td></tr>
                            <tr id="AuthTariff"><agiletags:AuthTariffListDisplayTag commandName="" sessionAttribute="tariffListArray"/></tr>
                            <tr id="AuthTButton"><agiletags:ButtonOpperationLabelError elementName="authTariffButton" type="button" commandName="ForwardToAuthTariffCommand" valueFromSession="no" displayName="Add New Tariff" mandatory="yes" javaScript="onClick=\"validateAuthPeriodForTariff('ForwardToAuthTariffCommand','authTariffButton','/PreAuth/GenericAuthorisation.jsp')\";"/></tr>
                            <!--Nappi Grid -->
                            <tr><td colspan="5"></td></tr>
                            <tr id="NappiButton"><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Allocate Medicine to Auth" elementName="nappiButton" javaScript="onClick=\"submitWithAction('ForwardToAuthNewNappi','nappiButton','/PreAuth/GenericAuthorisation.jsp');\"" mandatory="no" /></tr>
                            <tr><td colspan="5"></td></tr>

                            <!-- Benefit Allocation   -->
                            <tr id="BenefitHeader"><td colspan="5" align="left"><label class="subheader">Benefit Allocated To Auth</label></td></tr>
                            <tr id="AuthBenefit"><agiletags:AuthBenefitListDisplay commandName="" sessionAttribute="benAllocated"/></tr>
                            <tr id="AuthBenButton"><agiletags:ButtonOpperationLabelError elementName="authBenefitButton" type="button" commandName="ForwardToBenefitAllocation" valueFromSession="yes" displayName="Allocate Benefit" mandatory="yes" javaScript="onClick=\"submitWithAction('ForwardToBenefitAllocation','authBenefitButton','/PreAuth/GenericAuthorisation.jsp')\";"/></tr>

                            <!-- Requestor Details -->
                            <tr><agiletags:LabelTextBoxError mandatory="yes" displayName="Requestor Name" valueFromSession="Yes" elementName="requestorName" /></tr>
                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Requestor Relationship" elementName="requestorRelationship" lookupId="91" mandatory="yes" javaScript=""  errorValueFromSession="yes"/></tr>

                            <tr><agiletags:LabelTextBoxError mandatory="yes" displayName="Requestor Reason" valueFromSession="Yes" elementName="requestorReason"/></tr>
                            <tr><agiletags:LabelTextBoxError mandatory="yes" displayName="Requestor Contact" valueFromSession="Yes" elementName="requestorContact"/></tr>

                            <tr id="AuthFrom"><agiletags:LabelTextBoxDate displayname="Auth From Date" valueFromSession="Yes" elementName="authFromDate" mandatory="Yes" javascript="onblur=\"validateADRequest(this.value, this.id);\""/></tr>
                            <tr id="AuthTo"><agiletags:LabelTextBoxDate displayname="Auth To Date" valueFromSession="Yes" elementName="authToDate" mandatory="Yes" javascript="onblur=\"validateADRequest(this.value, this.id);\""/></tr>
                            <tr id="AuthNotes"><td colspan="5">
                                    <table width="100%">
                                        <tr><td colspan="5"></td></tr>
                                        <tr><td colspan="5" align="left"><label class="subheader">Auth Note Details</label></td></tr>
                                        <tr><agiletags:AuthNoteListDisplay sessionAttribute="authNoteList" commandName="" javaScript="" /></tr>
                                        <tr><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Manage Notes" elementName="authNoteButton" javaScript="onClick=\"submitWithAction('ForwardToAuthSpecificNote','authNoteButton','/PreAuth/GenericAuthorisation.jsp');\"" mandatory="no" /></tr>
                                    </table>
                                </td>
                            </tr>

                            <tr id="AuthStatus"><agiletags:LabelNeoLookupValueDropDownError displayName="Auth Status" elementName="authStatus" lookupId="111" mandatory="yes" errorValueFromSession="yes"/></tr>

                            <tr id="NormalSave">
                                <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Reset" javaScript="onClick=\"submitWithAction('ReloadGenericPreAuthCommand','reload','/PreAuth/GenericAuthorisation.jsp')\";"/>
                                <agiletags:ButtonOpperation type="button" align="left" commandName="" displayname="Next" span="1" javaScript="onClick=\"validateMandatory('SaveCallCenterPreAuth1Command')\";"/>
                            </tr>

                            <tr id="TariffSave">
                                <td colspan="5" align="left">
                                    <button name="opperation" type="button" onClick="submitWithAction('ReloadGenericPreAuthCommand', 'reload', '/PreAuth/GenericAuthorisation.jsp');" value="">Reset</button>
                                    <button name="opperation" type="button" onClick="validateMandatory('SavePreAuthDetailsCommand');" value="">Save</button>
                                </td>
                            </tr>
                            <tr id="BenefitSave">
                                <td colspan="5" align="left">
                                    <button name="opperation" type="button" onClick="submitWithAction('ReloadGenericPreAuthCommand', 'reload', '/PreAuth/GenericAuthorisation.jsp');" value="">Reset</button>
                                    <button name="opperation" type="button" onClick="validateMandatory('SavePreAuthDetailsCommand');" value="">Save</button>
                                </td>
                            </tr>
                            <tr id="NappiSave">
                                <td colspan="5" align="left">
                                    <button name="opperation" type="button" onClick="submitWithAction('ReloadGenericPreAuthCommand', 'reload', '/PreAuth/GenericAuthorisation.jsp');" value="">Reset</button>
                                    <button name="opperation" type="button" onClick="validateMandatory('SavePreAuthDetailsCommand');" value="">Save</button>
                                </td>
                            </tr>
                        </agiletags:ControllerForm>

                    </table>
                    <br/>
                    <table>
                        <tr><td colspan="4">
                                <label class="subheader">Disclaimer:</label>
                                <p>Authorisation is subject to available benefits</p>
                            </td></tr>
                    </table>
                    <br/>
                    <table align="right">
                        <tr><agiletags:LabelTextDisplay boldDisplay="yes" displayName="Current User" elementName="currentUser" javaScript="" valueFromSession="yes" /></tr>
                    </table>
                    <!-- content ends here -->
                </td></tr></table>
    </body>
</html>

<%-- 
    Document   : MemberDetailsTabbedPage
    Created on : 2011/08/03, 04:14:12
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <link rel="stylesheet" href="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>

        <script>
            $(function() {
                $( "#tabs" ).tabs();
                var selected = $("#tabs").tabs('option', 'selected');
                alert("selected " + selected);

                //For PreAuthorization
                var searchVal = document.getElementById('searchCalled').value;
                if (searchVal == 'memberNum'){
                    getCoverByNumber(document.getElementById('memberNum_text').value, 'memberNum');
                }
                document.getElementById('searchCalled').value = '';

                var optVal = document.createElement('OPTION');
                optVal.value = "99";
                optVal.text = "";
                $("#schemeOption option").add(optVal);
                toggleProduct($("#scheme").val());
                var resultMsg = <%= session.getAttribute("searchReturnMsg")%>;
                if(resultMsg != null && resultMsg != ""){
                    $("#searchReturnMsg").text(resultMsg);
                }else{
                    $("#searchReturnMsg").text("");
                }

            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function getPrincipalMemberForNumber(str, element){
                xmlhttp=GetXmlHttpObject();
                if (xmlhttp==null)
                {
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=GetMemberByNumberCommand&number="+ str + "&element=" + element;
                xmlhttp.onreadystatechange=function(){stateChanged(element)};
                xmlhttp.open("POST",url,true);
                xmlhttp.send(null);
            }


            function stateChanged(element){
                if (xmlhttp.readyState==4)
                {
                    // alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    // alert(result);
                    if(result == "FindPracticeByCodeCommand"){
                        var name = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=')+1, xmlhttp.responseText.lastIndexOf('|'));
                        var discipline = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=')+1, xmlhttp.responseText.lastIndexOf('$'));
                        document.getElementById('discipline').value = discipline;
                        document.getElementById('provName').value = name;
                        document.getElementById(element + '_error').innerHTML = '';
                        document.getElementById('searchCalled').value = "";
                    }else if(result == "GetMemberByNumberCommand"){
                                                        
                        var resultArr = xmlhttp.responseText.split("|");
                        var numberArr = resultArr[1].split("=");
                                                                
                        var number = numberArr[1];
                        document.getElementById(element + '_error').innerHTML = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('searchCalled').value = "";
                        submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                    }else if(result == "Error"){
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|')+1, xmlhttp.responseText.length);
                        if(forCommand == "FindPracticeByCodeCommand"){
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById('provName').value = '';
                            document.getElementById('discipline').value = '';
                            document.getElementById('searchCalled').value = "";
                        } else if(forCommand == "GetMemberByNumberCommand"){
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element + '_text').value = '';
                            document.getElementById('searchCalled').value = "";
                            submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                        }
                    }
                }
            }

            // For Authorization
            function toggleProduct(prodId){
                if(prodId != "99"){
                    addProductOption(prodId);
                }
            }

            function addProductOption(prodId){
                alert("prodId : " + prodId);
                xhr = GetXmlHttpObject();
                if(xhr==null){
                    alert("ERROR: Browser Incompatability");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=GetProductOptionCommand&prodId="+prodId;
                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        $("#schemeOption").empty();
                        $("#schemeOption").append("<option value=\"99\"></option>");
                        var xmlDoc = GetDOMParser(xhr.responseText);
                        //print value from servlet
                        $(xmlDoc).find("ProductOptions").each(function (){
                            //set product details
                            $(this).find("Option").each(function (){
                                var prodOpt = $(this).text();
                                var opt = prodOpt.split("|");
                                //Add option to dependant dropdown
                                var optVal = document.createElement('OPTION');
                                optVal.value = opt[0];
                                optVal.text = opt[1];
                                document.getElementById("schemeOption").options.add(optVal);

                            });
                        });
                    }
                }
                xhr.open("POST",url,true);
                xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                xhr.send(null);

            }

            function getCoverByNumber(str, element){
                if(str != null && str != ""){
                    xhr = GetXmlHttpObject();
                    if(xhr==null){
                        alert("ERROR: Browser Incompatability");
                        return;
                    }
                    var url = "/ManagedCare/AgileController";
                    url+="?opperation=GetCoverDetailsByNumber&number="+ str + "&element="+element + "&exactCoverNum=1";
                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            $("#depListValues").empty();
                            $("#depListValues").append("<option value=\"99\"></option>");
                            var xmlDoc = GetDOMParser(xhr.responseText);
                            //print value from servlet
                            $(xmlDoc).find("EAuthCoverDetails").each(function (){
                                //set product details
                                var prodName = $(this).find("ProductName").text();
                                var optName = $(this).find("OptionName").text();

                                //extract dependent info from tag
                                $(this).find("DependantInfo").each(function(){
                                    var depInfo = $(this).text();
                                    var nameSurnameType = depInfo.split("|");
                                    //Add option to dependant dropdown
                                    var optVal = document.createElement('OPTION');
                                    optVal.value = nameSurnameType[0];
                                    optVal.text = nameSurnameType[1];
                                    document.getElementById("depListValues").options.add(optVal);
                                });
                                //DISPLAY MEMBER DETAIL LIST
                                $(document).find("#schemeName").text(prodName);
                                $(document).find("#schemeOptionName").text(optName);
                                //error check
                                $("#"+element+"_error").text("");
                                var errorText = $(this).text();
                                var errors = errorText.split("|");
                                if(errors[0] == "ERROR"){
                                    $("#"+element+"_error").text(errors[1]);
                                }

                            });
                        }
                    }
                    xhr.open("POST",url,true);
                    xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                    xhr.send(null);
                }
            }

            function hideRows(id) {
                document.getElementById(id).style.display='none';
                document.getElementById(id+ 'plus').style.display='';
                document.getElementById(id+ 'minus').style.display='none';
            }
            function showRows(id) {
                document.getElementById(id).style.display='';
                document.getElementById(id+ 'minus').style.display='';
                document.getElementById(id+ 'plus').style.display='none';

            }
            function GetDOMParser(xmlStr){
                var xmlDoc;
                if(window.DOMParser){
                    parser=new DOMParser();
                    xmlDoc= parser.parseFromString(xmlStr,"text/xml");
                }else{
                    xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async="false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }
            function submitWithAction1(action, index) {
                alert('Inside submitWithAction1');
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.forms[0].submit();
            }
        </script>

    </head>
    <body>
        <div class="demo">

            <div id="tabs">
                <ul>
                    <li><a href="#policyHolderTab">Policy Holder</a></li>
                    <li><a href="#authorizatiionTab">Authorization</a></li>
                    <li><a href="#addressTab">Address Details</a></li>
                    <li><a href="#benefitsTab">Benefits Details</a></li>
                    <li><a href="#claimsTab">Claims</a></li>
                    <li><a href="#paymentTab">Payment Details</a></li>
                    <li><a href="#bankingTab">Banking Details</a></li>
                </ul>
                <agiletags:ControllerForm name="tabHistory">
                    <div id="policyHolderTab">
                        <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">
                                    <!-- controller form claimsHistory-->

                                    <input type="hidden" name="opperation" id="opperation" value="" />
                                    <input type="hidden" name="listIndex" id="listIndex" value=""/>
                                    <input type="hidden" name="searchCalled" id="searchCalled" value="" />
                                    <input type="hidden" name="onScreen" id="onScreen" value="" />

                            <tr><agiletags:LabelTextBoxError displayName="Membership Number" elementName="policyHolderNumber" valueFromSession="Yes" javaScript="onChange=\"getPrincipalMemberForNumber(this.value, 'policyHolderNumber');\""/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Title" elementName="memberTitle" valueFromSession="Yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Initials" elementName="memberInitials" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Name" elementName="memberName" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="memberSurname" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Gender" elementName="memberGender" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Date of Birth" elementName="dateOfBirth" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="ID Number" elementName="idNumber" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Option" elementName="option" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Group" elementName="group" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Status" elementName="status" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Join Date" elementName="joinDate" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Benefit Date" elementName="benefitDate" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Resigned Date" elementName="resignedDate" valueFromSession="yes"/></tr>

                            <!-- end the controller form-->
                            <br/>
                            <table>
                                <agiletags:MemberClaimsSearchTable commandName="AllocateMemberToSessionCommand" javaScript=""/>
                            </table>
                            </td></tr></table>
                    </div>
                    <div id="authorizatiionTab">
                        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                                    <!-- content goes here -->
                                    <label class="header">Authorisation Search</label>
                                    <br/><br/>
                                    <table>
                                        <!-- The controller form searchAuthForm -->
                                        <input type="hidden" name="opperation" id="opperation" value="" />
                                        <agiletags:HiddenField elementName="searchCalled" />
                                        <input type="hidden" name="onScreen" id="onScreen" value="" />

                                        <tr><agiletags:LabelTextBoxError displayName="Authorisation Number" elementName="authNo"/></tr>
                                        <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Auth Type" lookupId="89" elementName="authType"/></tr>
                                        <tr><agiletags:LabelTextBoxDate displayname="Authorisation Date" elementName="authDate"/></tr>

                                        <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Member Number" elementName="memberNum" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/PreAuth/PreAuthSearch.jsp" mandatory="no" javaScript="onblur=\"getCoverByNumber(this.value, 'memberNum');\""/></tr>
                                        <tr><agiletags:EntityCoverDependantDropDown displayName="Cover Dependants" elementName="depListValues" javaScript="" mandatory="no" valueFromSession="yes" /></tr>

                                        <tr><agiletags:LabelTextBoxError displayName="First Name" elementName="name"  /></tr>
                                        <tr><agiletags:LabelTextBoxError displayName="Last Name" elementName="surname" /></tr>

                                        <tr><agiletags:LabelProductDropDown displayName="Scheme" elementName="scheme" javaScript="onblur=\"toggleProduct(this.value);\"" /></tr>
                                        <tr><agiletags:LabelDropDown displayName="Scheme Option" elementName="schemeOption" /></tr>

                                        <tr><agiletags:ButtonOpperation type="button" align="left" span="1" commandName="PreAuthDetailsCommand" displayname="Search" javaScript="onClick=\"submitWithAction('PreAuthDetailsCommand');\"" /></tr>

                                        <!-- end the cotroller form -->
                                    </table>
                                    <br/>
                                    <HR color="#666666" WIDTH="50%" align="left">
                                    <br/>
                                    <agiletags:PreAuthSearchResultTable commandName="ForwardToCallCenterPreAuthCommand" javascript="onClick=\"submitWithAction('ForwardToCallCenterPreAuthCommand');\""/>

                                    <!-- content ends here -->
                                </td></tr></table>
                    </div>
                    <div id="addressTab">

                        <table>
                            <!-- The policyHolderAddressDetails controller form-->
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <table>
                                <label class="subheader">Physical Address</label>
                                <br/>
                                <br/>
                                <table>
                                    <tr><agiletags:LabelTextBoxError displayName="" elementName="memberPhysicalAddressLine1"  valueFromSession="Yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="" elementName="memberPhysicalAddressLine2"  valueFromSession="Yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="" elementName="memberPhysicalAddressLine3"  valueFromSession="Yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Physical City" elementName="memberPhysicalCityAddrees"  valueFromSession="Yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Physical Code" elementName="memberPhysicalCodeAddress"  valueFromSession="Yes"/></tr>
                                </table>
                                <br/>
                                <br/>
                                <label class="subheader">Postal Address</label>
                                <br/>
                                <table>
                                    <tr><agiletags:LabelTextBoxError displayName="" elementName="memberPostalAddressLine1"  valueFromSession="Yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="" elementName="memberPostalAddressLine2"  valueFromSession="Yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="" elementName="memberPostalAddressLine3"  valueFromSession="Yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Postal City" elementName="memberPostalCityAddress"  valueFromSession="Yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Postal Code" elementName="memberPostalCodeAddress"  valueFromSession="Yes"/></tr>
                                </table>
                                <br/>
                                <br/>
                                <label class="subheader">Contact Details</label>
                                <br/>
                                <table>
                                    <tr><agiletags:LabelTextBoxError displayName="Home Telephone Number" elementName="memberHomeTelNumber"  valueFromSession="Yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Fax Number" elementName="memberFaxNo"  valueFromSession="Yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Work Number" elementName="memberWorkNo"  valueFromSession="Yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Next of Kin" elementName="memberNextOfKinDetails"  valueFromSession="Yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Contact Person Tel" elementName="memberContactPersonTelNo"  valueFromSession="Yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Cell Phone Number" elementName="memberCellNo"  valueFromSession="Yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Email" elementName="memberEmailAddress"  valueFromSession="Yes"/></tr>
                                </table>

                                <table>
                                    <agiletags:ButtonOpperation align="left" displayname="Update Address" commandName="UpdateMemberAddressDetailsCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('UpdateMemberAddressDetailsCommand')\";"/>
                                </table>
                            </table>
                            <!-- end controller form-->
                        </table>

                    </div>
                    <div id="benefitsTab">
                        <br/>
                        <label class="header">Benefits Details</label>
                        <table>
                            <tr><agiletags:LabelTextBoxError displayName="Policy Number" elementName="policyHolderNumber"  valueFromSession="Yes" javaScript="onblur=\"getCoverByNumber(this.value, 'policyHolderNumber');\""/></tr>
                            <tr><agiletags:EntityCoverDependantDropDown displayName="Cover Dependants" elementName="depListValues" javaScript="" mandatory="no" valueFromSession="yes" /></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Product" elementName="memberProductName"  valueFromSession="Yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Option" elementName="memberOptionName"  valueFromSession="Yes"/></tr>

                            <table>
                                <tr><agiletags:ButtonOpperation align="left" displayname="View Benefits" commandName="ViewMemberBenefitsCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('ViewMemberBenefitsCommand')\";"/></tr>
                            </table>

                            <agiletags:ViewBenefitsTable commandName="ViewMemberBenefitsCommand" javaScript=""/>

                        </table>
                    </div>
                    <div id="claimsTab">
                        <table>
                            <tr><agiletags:ViewMemberClaimsTable  commandName="ViewMemberClaimsDetailsCommand" javaScript=""/></tr>
                        </table>
                    </div>
                    <div id="paymentTab">
                        <table>
                            <tr><agiletags:ViewPaymentRunDetailsTable commandName="ViewCoverClaimsCommand" javaScript=""/></tr>
                        </table>
                    </div>

                    <div id="bankingTab">

                        <table>
                            <tr><agiletags:LabelTextBoxError displayName="Policy Holder Number" elementName="policyHolderNumber"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="First Name" elementName="memberName"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="memberSurname"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Bank Name" elementName="bankName"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Branch Code" elementName="branchCode"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Branch Name" elementName="branchName"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Account Number" elementName="accountNumber"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Account Name" elementName="accountName"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Account Type" elementName="accountType"  valueFromSession="Yes" readonly="yes"/></tr>

                        </table>
                    </div>
                </agiletags:ControllerForm>
            </div>

        </div><!-- End demo -->
    </body>
</html>

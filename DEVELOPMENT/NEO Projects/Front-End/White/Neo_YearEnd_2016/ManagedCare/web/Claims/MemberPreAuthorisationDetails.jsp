<%-- 
    Document   : MemberPreAuthorisationDetails
    Created on : 2011/08/18, 01:42:39
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <link rel="stylesheet" href="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>
        <script>

            $(function() {

                //For PreAuthorization
                var resultMsg = <%= session.getAttribute("searchReturnMsg")%>;
                if(resultMsg != null && resultMsg != ""){
                    $("#searchReturnMsg").text(resultMsg);
                }else{
                    $("#searchReturnMsg").text("");
                }
                
                var covNum = "<%= session.getAttribute("policyHolderNumber")%>";
                if(covNum != "" && covNum != null){
                    getCoverByNumber(covNum, 'depListValues');
                }
                
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });            

            });
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.forms[0].submit();
            }

            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function getPrincipalMemberForNumber(str, element){
                xmlhttp=GetXmlHttpObject();
                if (xmlhttp==null)
                {
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=GetMemberByNumberCommand&number="+ str + "&element=" + element;
                xmlhttp.onreadystatechange=function(){stateChanged(element)};
                xmlhttp.open("POST",url,true);
                xmlhttp.send(null);
            }

            function stateChanged(element){
                if (xmlhttp.readyState==4)
                {
                    // alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    // alert(result);
                    if(result == "FindPracticeByCodeCommand"){
                        var name = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=')+1, xmlhttp.responseText.lastIndexOf('|'));
                        var discipline = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=')+1, xmlhttp.responseText.lastIndexOf('$'));
                        document.getElementById('discipline').value = discipline;
                        document.getElementById('provName').value = name;
                        document.getElementById(element + '_error').innerHTML = '';
                        document.getElementById('searchCalled').value = "";
                    }else if(result == "GetMemberByNumberCommand"){
                                                        
                        var resultArr = xmlhttp.responseText.split("|");
                        var numberArr = resultArr[1].split("=");
                                                                
                        var number = numberArr[1];
                        document.getElementById(element + '_error').innerHTML = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('searchCalled').value = "";
                        submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                    }else if(result == "Error"){
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|')+1, xmlhttp.responseText.length);
                        if(forCommand == "FindPracticeByCodeCommand"){
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById('provName').value = '';
                            document.getElementById('discipline').value = '';
                            document.getElementById('searchCalled').value = "";
                        } else if(forCommand == "GetMemberByNumberCommand"){
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element + '_text').value = '';
                            document.getElementById('searchCalled').value = "";
                            submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                        }
                    }
                }
            }

            function getCoverByNumber(str, element){
                if(str != null && str != ""){
                    xhr = GetXmlHttpObject();
                    if(xhr==null){
                        alert("ERROR: Browser Incompatability");
                        return;
                    }
                    var url = "/ManagedCare/AgileController";
                    url+="?opperation=GetCoverDetailsByNumber&number="+ str + "&element="+element + "&exactCoverNum=1";
                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            $("#depListValues").empty();
                            $("#depListValues").append("<option value=\"99\"></option>");
                            var xmlDoc = GetDOMParser(xhr.responseText);
                            //print value from servlet
                            $(xmlDoc).find("EAuthCoverDetails").each(function (){
                                //set product details
                                var prodName = $(this).find("ProductName").text();
                                var optName = $(this).find("OptionName").text();

                                //extract dependent info from tag
                                $(this).find("DependantInfo").each(function(){
                                    var depInfo = $(this).text();
                                    var nameSurnameType = depInfo.split("|");
                                    //Add option to dependant dropdown
                                    var optVal = document.createElement('OPTION');
                                    optVal.value = nameSurnameType[0];
                                    optVal.text = nameSurnameType[1];
                                    document.getElementById("depListValues").options.add(optVal);
                                });
                                //DISPLAY MEMBER DETAIL LIST
                                $(document).find("#schemeName").text(prodName);
                                $(document).find("#schemeOptionName").text(optName);
                                //error check
                                $("#"+element+"_error").text("");
                                var errorText = $(this).text();
                                var errors = errorText.split("|");
                                if(errors[0] == "ERROR"){
                                    $("#"+element+"_error").text(errors[1]);
                                }

                            });
                        }
                    }
                    xhr.open("POST",url,true);
                    xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                    xhr.send(null);
                }
            }

            function hideRows(id) {
                document.getElementById(id).style.display='none';
                document.getElementById(id+ 'plus').style.display='';
                document.getElementById(id+ 'minus').style.display='none';
            }
            function showRows(id) {
                document.getElementById(id).style.display='';
                document.getElementById(id+ 'minus').style.display='';
                document.getElementById(id+ 'plus').style.display='none';

            }

            function GetDOMParser(xmlStr){
                var xmlDoc;
                if(window.DOMParser){
                    parser=new DOMParser();
                    xmlDoc= parser.parseFromString(xmlStr,"text/xml");
                }else{
                    xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async="false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">

                    <agiletags:CallCenterTabs cctSelectedIndex="link_Auth" />
                    <fieldset id="pageBorder">
                        <div class="content_area" >

                            <label class="header">Authorisation Details</label>
                            <br/><br/>
                            <table>
                                <agiletags:ControllerForm name="practiceDetails">

                                    <input type="hidden" name="opperation" id="opperation" value="" />
                                    <agiletags:HiddenField elementName="searchCalled" />
                                    <input type="hidden" name="onScreen" id="onScreen" value="" />
                                    <input type="hidden" name="listIndex" id="listIndex" value=""/>

                                    <tr><agiletags:LabelTextBoxError displayName="Authorisation Number" elementName="authNo"/></tr>
                                    <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Auth Type" lookupId="89" elementName="authType"/></tr>
                                    <tr><agiletags:LabelTextBoxDate displayname="Authorisation Date" elementName="authDate"/></tr>
                                    <tr><agiletags:EntityCoverDependantDropDown displayName="Cover Dependants" elementName="depListValues" javaScript="" mandatory="no" valueFromSession="yes" /></tr>
                                     <c:if test="${userRestriction != true}">
                                    <tr><agiletags:ButtonOpperation type="button" align="left" span="1" commandName="MemberPreAuthDetailsCommand" displayname="Search" javaScript="onClick=\"submitWithAction('MemberPreAuthDetailsCommand');\"" /></tr>
                                     </c:if>
                                </agiletags:ControllerForm>
                            </table>
                            <br/>
                            <HR color="#666666" WIDTH="50%" align="left">
                            <br/>
                             <c:if test="${userRestriction != true}">       
                            <agiletags:PreAuthSearchResultTable commandName="ForwardToMemberPreAuthorisationCommand" javascript="onClick=\"submitWithAction('ForwardToMemberPreAuthorisationCommand');\""/>
                            </c:if>
                            </div>
                       <c:if test="${userRestriction == true}"><span class="neonotificationnormal neonotificationwarning">Content contained is confidential. Please refer to Team leader for assistance</span></c:if>

                    </fieldset>
                </td></tr></table>
    </body>
</html>

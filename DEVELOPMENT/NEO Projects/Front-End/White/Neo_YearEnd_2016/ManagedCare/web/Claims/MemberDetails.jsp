<%-- 
    Document   : MemberDetails
    Created on : 2011/08/18, 08:04:26
    Author     : josephm
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script>
            
            $(document).ready(function(){  
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });
            });            
            
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.forms[0].submit();
            }

            function reloadCoverHistory(coverNum, coverStartDate, coverEndDate){
                document.getElementById('opperation').value = "ReloadCoverHistoryByDate";
                document.getElementById('startDate').value = coverStartDate;
                document.getElementById('endDate').value = coverEndDate;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">

                    <agiletags:CallCenterTabs cctSelectedIndex="link_PH" />
                    <fieldset id="pageBorder">
                        <div>
                            <agiletags:ControllerForm name="practiceDetails">
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <agiletags:HiddenField elementName="searchCalled" />
                                <input type="hidden" name="onScreen" id="onScreen" value="" />
                                <input type="hidden" name="listIndex" id="listIndex" value=""/>
                                <input type="hidden" name="endDate" id="endDate" value="" />
                                <input type="hidden" name="startDate" id="startDate" value="" /> 
                                <input type="hidden" name="exactCoverNum" id="exactCoverNum" value="1" />
                            </agiletags:ControllerForm>
                            <c:set var="PayDate" value="${sessionScope.memberPaymentMethod != 'Electronic Transfer' ? sessionScope.memberPaymentDate  : ' '}" />
                            
                            <c:if test="${warningW == true}" >
                                <span style="color: red">Warning: Please note membership cover is subject to a waiting period. Please refer to underwriting tab.</span>
                                <br/>
                            </c:if>
                            <br/>
                            
                            <c:if test="${!(empty sessionScope.memberBrokerCode && empty sessionScope.memberEmployerName)}">
                                <input type="hidden" name="memberEntityId" id="memberEntityId" value="" />
                                <c:if test="${applicationScope.Client == 'Sechaba'}">
                                    <label class="header">Alternative Membership Numbers</label>
                                    <agiletags:AlternativeMemberNumbers items="${alt_MemNum_Details}" />
                                </c:if>
                                <HR WIDTH="80%" align="left">
<!--                                <table>
                                    <tr><td><label>Scheme  : </label></td><td class="label"><b>${sessionScope.productDescription}</b></td></tr>
                                    <tr>
                                        <td width="160"><label>Current Broker  : </label></td><td class="label">${sessionScope.memberBrokerCode}</td>
                                        <%--<c:if test="${sessionScope.memberVIPBroker == '1'}" >--%>
                                            <td><div class="vipBroker" style="width: 50">(VIP)</div></td>
                                        <%--</c:if>--%>
                                    </tr>
                                    <tr><td><label>Current Employer  : </label></td><td  class="label">${sessionScope.memberEmployerName}</td></tr>
                                    <tr><td><label>Employer Type : </label></td><td  class="label">${sessionScope.memberEmployerType}</td></tr>
                                    <tr><td><label>Billing Method : </label></td><td  class="label">${sessionScope.memberEmployerBillMethod}</td></tr>
                                    <tr><td><label>Payment Method : </label></td><td  class="label">${sessionScope.memberPaymentMethod} ${PayDate}</td></tr>
                                    <tr><td><label>Administrator : </label></td><td  class="label">${sessionScope.memberEmployerAdministrator}</td></tr>
                                    <tr><td><label>Client : </label></td><td  class="label">${sessionScope.Client}</td></tr>
                                        <%--<c:if test="${applicationScope.Client == 'Sechaba'}">--%>
                                            <tr><td><label>Member Category : </label></td><td  class="label">${memberCategory}</td></tr>
                                            <tr><td><label>Union : </label></td><td  class="label">${sessionScope.memberUnionLink}</td></tr>
                                        <%--</c:if>--%>
                                    <%--<c:if test="${sessionScope.showHandedOverFlag == 'true'}" >--%>
                                        <tr><td><label>Debt Collection : </label></td><td><div class="vipBroker" style="width: 130px">Member Handed Over</div></td></tr>
                                        <tr><td><label>Date Handed Over : </label></td><td  class="label">${sessionScope.debtHandedOverDate}</td></tr>
                                    <%--</c:if>--%>
                                </table>-->
                                <br/>
                            </c:if>
                            <label class="header">Cover History</label>
                            <br/>
                            <table>
                                <tr><td><agiletags:PricipleMemberHistoryGrid sessionAttribute="principleHistoryDetails" javaScript=""/></td></tr>
                            </table>
                            <br/>
                            <label class="header">Cover Details</label>
                            <br/>
                            <table>
                                <tr><td><agiletags:MemberClaimsSearchTable commandName="" javaScript="" /></td></tr>
                            </table>
                        </div>
                    </fieldset>
                </td></tr></table>
    </body>
</html>

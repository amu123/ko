<%-- 
    Document   : MemberBenefitClaimResult
    Created on : Oct 6, 2011, 12:24:31 PM
    Author     : Johan-NB
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>
            
            function GetXmlHttpObject(){
                if (window.ActiveXObject){
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest){
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }
            
            function GetDOMParser(xmlStr){
                var xmlDoc;
                if(window.DOMParser){
                    parser=new DOMParser();
                    xmlDoc= parser.parseFromString(xmlStr,"text/xml");
                }else{
                    xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async="false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }            
            
            function submitReturnWithAction(action) {
                document.getElementById('opperation').value = action;
                document.getElementById('onScreen').value = "MemberBenefitClaimResult.jsp";                
                document.forms[0].submit();
            }

            function submitWithAction(action, claimLineId, paidAmount, depCode) {
                document.getElementById('opperation').value = action;
                document.getElementById('memberDetailedClaimLineId').value = claimLineId;
                document.getElementById('memberDetailedClaimPaidAmount').value = paidAmount;
                document.getElementById('memberDetailedClaimBenDepCode').value = depCode;
                document.getElementById('onBenefitScreen').value = 'MemberBenefitClaimResult.jsp';
                document.forms[0].submit();
            }
            
            function submitPageAction(action, pageDirection) {
                document.getElementById('opperation').value = action;
                document.getElementById('mbcPageDirection').value = pageDirection;
                document.getElementById('onBenefitScreen').value = 'MemberBenefitClaimResult.jsp';
                document.forms[0].submit();
            }          
            
            function submitFilterWithAction(action){
                document.getElementById('opperation').value = action;
                document.getElementById('onBenefitScreen').value = 'MemberBenefitClaimResult.jsp';
                document.forms[0].submit();
            }
            
            function popCoverDep(){
                
                var str = '<%= session.getAttribute("policyHolderNumber")%>';
                
                if(str != null && str != ""){
                    xhr = GetXmlHttpObject();
                    if(xhr==null){
                        alert("ERROR: Browser Incompatability");
                        return;
                    }
                    var url = "/ManagedCare/AgileController";
                    url+="?opperation=GetCoverDetailsByNumber&number="+ str + "&element="+element + "&exactCoverNum=1";
                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            $("#depListValues").empty();
                            $("#depListValues").append("<option value=\"99\"></option>");
                            var xmlDoc = GetDOMParser(xhr.responseText);
                            //print value from servlet
                            $(xmlDoc).find("EAuthCoverDetails").each(function (){
                                //extract dependent info from tag
                                $(this).find("DependantInfo").each(function(){
                                    var depInfo = $(this).text();
                                    var nameSurnameType = depInfo.split("|");
                                    //Add option to dependant dropdown
                                    var optVal = document.createElement('OPTION');
                                    optVal.value = nameSurnameType[0];
                                    optVal.text = nameSurnameType[1];
                                    document.getElementById("depListValues").options.add(optVal);
                                });
                            });
                        }
                    }
                    xhr.open("POST",url,true);
                    xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                    xhr.send(null);
                }
            }            
            
            function validateDate(str){
                $("#tFromDate_error").text("");
                
                //  check for valid numeric strings
                var strValidChars = "0123456789/";
                var strChar;
                var strChar2;
                var blnResult = true;

                if (str.length < 10 || str.length > 10) blnResult = false;

                //  test strString consists of valid characters listed above
                for (i = 0; i < str.length; i++)
                {
                    strChar = str.charAt(i);
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        blnResult = false;
                    }
                }

                // test if slash is at correct position (yyyy/mm/dd)
                strChar = str.charAt(4);
                strChar2 = str.charAt(7);
                if (strChar != '/' || strChar2 != '/') blnResult = false;

                if(blnResult == false){
                    $("#tFromDate_error").text("Error: Incorrect Date");
                }

            }
            
            //practice search
            function validatePractice(str, element){
                xhr = GetXmlHttpObject();
                if (xhr==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var error = "#"+element+"_error";
                if (str.length > 0) {
                    var url="/ManagedCare/AgileController";
                    url=url+"?opperation=FindPracticeDetailsByCode&provNum="+str+"&element="+element;
                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                            $(document).find(error).text("");
                            if(result == "Error"){
                                $(document).find(error).text("No such provider");
                            }else if(result == "Done"){
                                $(document).find(error).text("");
                            }
                        }
                    };
                    xhr.open("POST",url,true);
                    xhr.send(null);
                } else {
                    $(document).find(error).text("");
                }
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Benefit Claim Line Detail</label>
                    <br/>
                    <HR color="#666666" WIDTH="50%" align="left">
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="claimsHistoryMem">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="memberDetailedClaimLineId" id="memberDetailedClaimLineId" value="" />
                            <input type="hidden" name="memberDetailedClaimPaidAmount" id="memberDetailedClaimPaidAmount" value="" />
                            <input type="hidden" name="memberDetailedClaimBenDepCode" id="memberDetailedClaimBenDepCode" value="" />
                            <input type="hidden" name="mbcPageDirection" id="mbcPageDirection" value="" />
                            <input type="hidden" name="onBenefitScreen" id="onBenefitScreen" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <!--<table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                            </table>-->
                            <tr><td><label class="subheader">Claim Line Filter</label></td></tr>
                            <tr><td></td></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Claim Number" elementName="claimId" /></tr>
                            <tr><agiletags:EntityCoverDependantDropDown displayName="Cover Dependants" elementName="depListValues" javaScript="" mandatory="no" valueFromSession="yes" /></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Tariff Code" elementName="tariffCode" /></tr>
                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Practice Number" elementName="pracNum" searchFunction="yes" searchOperation="ForwardToSearchPracticeCommand" onScreen="/Claims/MemberBenefitClaimResult.jsp" mandatory="no" javaScript="onChange=\"validatePractice(this.value, 'practiceNumber');\""/></tr>
                            <tr><agiletags:LabelTextBoxDate displayname="Treatment From Date" elementName="tFromDate" javascript="onChange=\"validateDate(this.value);\"" /></tr>
                            <tr><agiletags:ButtonOpperation align="left" displayname="Filter Results" span="" type="button" commandName="" javaScript="onClick=\"submitFilterWithAction('ViewMemberClaimsBenefitsCommand');\"" /></tr>

                        </agiletags:ControllerForm>
                    </table>
                    <br/>
                    <HR color="#666666" WIDTH="50%" align="left">
                    <br/>
                    <agiletags:MemberBenefitClaimLineDetailsTable />
                    <br/>
                    <table>
                        <tr><agiletags:ButtonOpperation align="right" span="5" type="button" commandName="ReturnMemberClaimDetails" displayname="Return" javaScript="onClick=\"submitReturnWithAction('ReturnMemberClaimDetails');\""/></tr>
                    </table>
                </td></tr></table>
    </body>
</html>

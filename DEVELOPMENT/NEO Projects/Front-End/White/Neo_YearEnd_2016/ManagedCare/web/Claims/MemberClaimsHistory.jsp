<%-- 
    Document   : MemberClaimsHistory
    Created on : 2011/07/08, 11:53:31
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function getPrincipalMemberForNumber(str, element) {
                alert("In getPrincipalMemberForNumber");
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=GetMemberByNumberCommand&number=" + str + "&element=" + element;
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element);
                };
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }


            function stateChanged(element) {
                alert("reached");
                if (xmlhttp.readyState == 4)
                {
                    // alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    // alert(result);
                    if (result == "FindPracticeByCodeCommand") {
                        var name = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=') + 1, xmlhttp.responseText.lastIndexOf('|'));
                        var discipline = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=') + 1, xmlhttp.responseText.lastIndexOf('$'));
                        document.getElementById('discipline').value = discipline;
                        document.getElementById('provName').value = name;
                        document.getElementById(element + '_error').innerHTML = '';
                        document.getElementById('searchCalled').value = "";
                    } else if (result == "GetMemberByNumberCommand") {

                        var resultArr = xmlhttp.responseText.split("|");
                        var numberArr = resultArr[1].split("=");

                        var number = numberArr[1];
                        document.getElementById(element + '_error').innerHTML = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('searchCalled').value = "";
                        submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                    } else if (result == "Error") {
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|') + 1, xmlhttp.responseText.length);
                        if (forCommand == "FindPracticeByCodeCommand") {
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById('provName').value = '';
                            document.getElementById('discipline').value = '';
                            document.getElementById('searchCalled').value = "";
                        } else if (forCommand == "GetMemberByNumberCommand") {
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element + '_text').value = '';
                            document.getElementById('searchCalled').value = "";
                            submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                        }
                    }
                }
            }


        </script>

        <style type="text/css">
            .btn {
                color:#666666;
                font: bold 84% 'trebuchet ms',helvetica,sans-serif;
            }
        </style>

    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">
                    <!-- content goes here -->
                    <agiletags:ClaimsTabbedButton buttonCommand2="ViewMemberPreAuthorisationCommand" buttonDisplay2="Authorisation"
                                                  buttonCommand3="ViewMemberAddressDetailsCommand" buttonDisplay3="Address Details"
                                                  buttonCommand4="ForwardToMemberBenefitsCommand" buttonDisplay4="Benefit Details"
                                                  buttonCommand5="ForwardToMemberClaimsCommand" buttonDisplay5="Claims"
                                                  buttonCommand6="ViewMemberPaymentDetailsCommand" buttonDisplay6="Payment Details"
                                                  buttonCommand7="ViewMemberBankingDetailsCommand" buttonDisplay7="Banking Details"
                                                  buttonCommand8="ForwardToMemberStatementsCommand" buttonDisplay8="Statements"
                                                  buttonCommand1="ForwardToMemberDetailsCommand" buttonDisplay1="Policy Holder Details"  disabledDisplay="Policy Holder Details" numberOfButtons="10"/>                    
                    <br/><br/>
                    <label class="header">Policy Holder - Details</label>
                    <br/><br/>
                    <table>
                        <agiletags:ControllerForm name="claimsHistory">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="searchCalled" id="searchCalled" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />

                            <tr><agiletags:LabelTextBoxError displayName="Membership Number" elementName="policyHolderNumber" valueFromSession="Yes" javaScript="onChange=\"getPrincipalMemberForNumber(this.value, 'policyHolderNumber');\""/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Title" elementName="memberTitle" valueFromSession="Yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Initials" elementName="memberInitials" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Name" elementName="memberName" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="memberSurname" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Gender" elementName="memberGender" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Date of Birth" elementName="dateOfBirth" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="ID Number" elementName="idNumber" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Option" elementName="option" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Group" elementName="group" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Status" elementName="status" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Join Date" elementName="joinDate" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Option Date" elementName="benefitDate" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Resigned Date" elementName="resignedDate" valueFromSession="yes"/></tr>

                        </agiletags:ControllerForm>
                    </table>

                    <br/>
                    <agiletags:MemberClaimsSearchTable commandName="AllocateMemberToSessionCommand" javaScript=""/>
                </td></tr></table>
    </body>
</html>

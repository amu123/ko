<%-- 
    Document   : MemberCCClaimsDetails
    Created on : 2011/09/14, 08:02:27
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">

                    <agiletags:ClaimsTabbedButton buttonCommand2="ViewMemberPreAuthorisationCommand" buttonDisplay2="Authorisation"
                                         buttonCommand3="ViewMemberAddressDetailsCommand" buttonDisplay3="Address Details"
                                         buttonCommand4="ForwardToMemberBenefitsCommand" buttonDisplay4="Benefit Details"
                                         buttonCommand5="ForwardToMemberClaimsCommand" buttonDisplay5="Claims"
                                         buttonCommand6="ViewMemberPaymentDetailsCommand" buttonDisplay6="Payment Details"
                                         buttonCommand7="ViewMemberBankingDetailsCommand" buttonDisplay7="Banking Details"
                                         buttonCommand8="ForwardToMemberStatementsCommand" buttonDisplay8="Statements"
                                         buttonCommand1="ForwardToMemberDetailsCommand" buttonDisplay1="Policy Holder Details"  disabledDisplay="Claims" numberOfButtons="10"/>

                    <br/><br/>
                    <table>
                        <agiletags:ControllerForm name="memberDetails">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="listIndex" id="listIndex" value=""/>
                            
                        </agiletags:ControllerForm>
                    </table>
                    <br/>
                    <agiletags:ViewMemberClaimsTable commandName="ViewMemberClaimsDetailsCommand" javaScript=""/>
                </td></tr></table>
    </body>
</html>

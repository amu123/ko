<%-- 
    Document   : PracticeDetailedClaimLine
    Created on : Sep 27, 2011, 11:12:12 AM
    Author     : Johan-NB
--%>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>
            
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.getElementById('onScreen').value = "PracticeDetailedClaimLine.jsp";
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <!-- content goes here -->
                    <table>
                        <tr valign="top"></tr>
                        <agiletags:ControllerForm name="claimsHistory">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                        </agiletags:ControllerForm>
                    </table>
                    <agiletags:ClaimLineDetailTag />
                    <br/>
                    <table>
                        <tr><agiletags:ButtonOpperation align="right" span="5" type="button" commandName="ReturnPracticeClaimDetails" displayname="Return" javaScript="onClick=\"submitWithAction('ReturnPracticeClaimDetails');\""/></tr>
                    </table>
                </td></tr></table>
    </body>
</html>

<%-- 
    Document   : PracticeDetails
    Created on : 2011/08/17, 10:19:48
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript">
            
            $(function() {
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });        
                window.scrollTo(0,document.body.scrollHeight);          
            });
            
            function showHistoryTable(networkDescription, id) {
                if(networkDescription != null && id != null){
                    id = id.replace('btnpdg_', "");
                    var lblName = 'lblpdg_' + id;
                    var lblNameValue = document.getElementById(lblName).innerHTML;
                    
                    document.getElementById('selectedProductName').value = lblNameValue;
                    document.getElementById('selectedNetworkDescription').value = networkDescription;
                    document.getElementById('Show_NetworkIndicatorHistoryGridTag').value = "true";
                    
                    submitWithAction('ViewNetworkIndicatorHistoryCommand');
                    
                    submitScreenRefresh("/Claims/PracticeDetails.jsp");
                }
            }
            
            function submitScreenRefresh(screen) {
                    $("#opperation").val("RefreshCommand");
                    $("#refreshScreen").val(screen);
                    document.forms[0].submit();
            }
            
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.forms[0].submit();
            }

        </script>

    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">
            <agiletags:CallCenterProviderTabs isClearingUser="${sessionScope.persist_user_ccClearing}" cctSelectedIndex="link_prov_PD" />

            <fieldset id="pageBorder">
                <div class="content_area" >
                    <label class="header">Practice Details</label>
                    <br/><br/>
                    <table>
                        <agiletags:ControllerForm name="practiceDetails">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="refreshScreen" id="refreshScreen" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="listIndex" id="listIndex" value=""/>
                            <input type="hidden" name="selectedProductName" id="selectedProductName" value=""/>
                            <input type="hidden" name="selectedNetworkDescription" id="selectedNetworkDescription" value=""/>
                            <input type="hidden" name="Show_NetworkIndicatorHistoryGridTag" id="Show_NetworkIndicatorHistoryGridTag" value="false"/>

                            <tr><agiletags:LabelTextBoxError displayName="Practice Number" elementName="practiceNumber" valueFromSession="yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Practice Name" elementName="practiceName"  valueFromSession="yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Discipline Type" elementName="disciplineType" valueFromSession="yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Network Indicator" elementName="networkIndicator" valueFromSession="yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Contract Start Date" elementName="contractStart" valueFromSession="yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Contract End Date" elementName="contractEnd" valueFromSession="yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Dispensing Status" elementName="dispensingStatus" valueFromSession="yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Status" elementName="status" valueFromSession="yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Restrictions " elementName="restrictions" valueFromSession="yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="BHF Start Date" elementName="startDate" valueFromSession="yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="BHF End Date" elementName="endDate" valueFromSession="yes" readonly="yes"/></tr>
                        </agiletags:ControllerForm>
                    </table>
                    <table>
                        <tr><td><agiletags:PracticeDetailsGrid commandName="pdg" javaScript="onClick=\"showHistoryTable(this.value, this.id);\""/></td></tr>
                    </table>
                    <c:if test="${!empty sessionScope.Show_NetworkIndicatorHistoryGridTag && sessionScope.Show_NetworkIndicatorHistoryGridTag == 'true'}">
                        <br/><br/>
                    <table width="100%">
                        <tr><td><agiletags:NetworkIndicatorHistoryGridTag/></td></tr>
                    </table>
                    </c:if>
                </div></fieldset>
        </td></tr></table>
    </body>
</html>

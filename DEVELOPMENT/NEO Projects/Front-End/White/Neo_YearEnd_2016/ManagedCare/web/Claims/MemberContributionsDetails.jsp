<%-- 
    Document   : MemberBankingDetails
    Created on : 2011/08/19, 02:05:39
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib  prefix="nt" tagdir="/WEB-INF/tags/contributions" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>

        <script  language="JavaScript">
            
            $(function(){
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });            
            })
            
            function submitWithAction(action) {
                $("#opperation").val(action);
                //document.getElementById('opperation').value = action;
                $("#whatever").submit();
                //document.forms[0].submit();
                
            }
             function toggle_contrib_table_group(btn, id) {
                var tblElem = document.getElementById(id).style.display;
                if (tblElem == "none") {
                    document.getElementById(id).style.display = 'table-row-group';
                    btn.value = '-';
                } else {
                    document.getElementById(id).style.display = 'none';
                    btn.value = '+';
                }
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%>
            <tr valign="center">
                <td width="5px"></td>
                <td align="left">

                    <agiletags:CallCenterTabs cctSelectedIndex="link_Contri" />
                    <fieldset id="pageBorder">
                        <div class="content_area" >
                            <nt:ContributionAgeAnalysis items="${memberAgeAnalysis}" productID="${sessionScope.ageAnalysisProductID}"/>
                            <nt:ContributionSavingsLimit items="${savingsLimits}"/>
                            <nt:ContributionUnallocatedReceipts items="${Receipts}"/>
                            <nt:ContributionDebitOrderDetails items="${debitOrder}" />
                            <br/>
                            <label class="header">Member Contributions Details</label>
                            <br/>
                            <div id="tabTableLayout">
                                <HR WIDTH="100%" align="left">
                                        <agiletags:ControllerForm name="whatever">
                                            <input type="hidden" name="opperation" id="opperation" value="ViewMemberContributionsCommand" />
                                            <input type="hidden" name="listIndex" id="listIndex" value=""/>
                                            <table>
                                                <td><label>Start Date</label></td>
                                                <td><input name="contrib_start_date" id="contrib_start_date" size="10" value="${contrib_start_date}"></td>
                                                <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('contrib_start_date', this);"/></td>
                                                <td>&nbsp;</td>
                                                <td><label>End Date</label></td>
                                                <td><input name="contrib_end_date" id="contrib_end_date" size="10" value="${contrib_end_date}"></td>
                                                <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('contrib_end_date', this);"/></td>
                                                <td>&nbsp;</td>
                                                <td><label>Action Date</label></td>
                                                <td><input name="contrib_ad_date" id="contrib_ad_date" size="10" value="${contrib_ad_date}"></td>
                                                <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('contrib_ad_date', this);"/></td>
                                                <td>&nbsp;</td>
                                                <td><button type="submit">Search</button></td>
                                            </table>
                                        </agiletags:ControllerForm>
                                        <HR WIDTH="100%" align="left">
                                        <br>
                                        <div id="ContribTranResultsx">
                                            <nt:ContributionTransactionHistory items="${Contributions}"/>
                                        </div>
                        </div>
                    </fieldset>
                </td></tr></table>
    </body>
</html>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script language="JavaScript">
            var EmailBtnName = "";
            $(function() {
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });
            });

            $(document).ready(function() {
                document.getElementById('emailAddress').value = "${memberEmailAddress}";
                $('#emailAddress').hide();
                $('#emailAddress_label').hide();
                $('#btnEmailCurrent').hide();
                document.getElementById("emailAddress").disabled = true;
                if (EmailBtnName != "") {
                    document.getElementById(EmailBtnName).disabled = false;
                }
                if ($('#docType').val() === 'MCT')
                {
                    $('#emailAddress_label').show();
                    $('#emailAddress').show();
                    $('#btnEmailCurrent').show();
                    if (!${empty indexList}) {
                        document.getElementById("emailAddress").disabled = false;
                        if (EmailBtnName != "") {
                            document.getElementById(EmailBtnName).disabled = false;
                        }
                    }
                }
            });


            function submitWithAction(action, fileLoc) {
                document.forms[0].elements["opperation"].value = action;
                document.getElementById('fileLocation').value = fileLoc;
                document.forms[0].submit();
            }

            function swapDocType(value) {
                if (value == "1") {
                    document.getElementById('IncomingDetails').style.display = 'block';
                    document.getElementById('outGoingDetails').style.display = 'none';

                } else if (value == "2") {
                    document.getElementById('outGoingDetails').style.display = 'block';
                    document.getElementById('IncomingDetails').style.display = 'none';
                } else {
                    document.getElementById('outGoingDetails').style.display = 'none';
                    document.getElementById('IncomingDetails').style.display = 'none';
                }
            }
            function submitActionAndFile(thisForm, action, fileLocation) {
                thisForm.opperation.value = action;
                thisForm.fileLocation.value = fileLocation;
                thisForm.submit();

            }
            function toogleDocument(lookUpID, element2Toggle) {
                //Hide Email Field [only used for membership certificates
                document.getElementById('emailAddress').value = "";
                $('#emailAddress_label').hide(200);
                $('#emailAddress').hide(200);
                $('#btnEmailCurrent').hide(100);
                document.getElementById("emailAddress").disabled = true;
                if (EmailBtnName != "") {
                    document.getElementById(EmailBtnName).disabled = true;
                }

                if (lookUpID != "") {
                    addDocument(lookUpID, element2Toggle);
                } else {
                    $("#" + element2Toggle).empty();
                }
            }

            function addDocument(lookUpID, element2Toggle) {
                var element2ToggleJQ = "#" + element2Toggle;
                if(lookUpID == "212"){
                    $('#documentUpload').show();
                } else {
                    $('#documentUpload').hide();
                }

                var url = "/ManagedCare/AgileController";
                var data = {
                    'opperation': 'LoadDocumentDropDownListCommand',
                    'lookUpID': lookUpID
                };


                $.get(url, data, function(resultTxt) {
                    if (resultTxt != null) {
                        $(element2ToggleJQ).empty();
                        $(element2ToggleJQ).append("<option value=\"\"></option>");
                        var lookUps = resultTxt.split("&");
                        for (var i = 0; i < lookUps.length; i++) {
                            var lookupArr = lookUps[i].split(":");
                            $(element2ToggleJQ).append("<option value=\"" + lookupArr[0] + "\">" + lookupArr[1] + "</option>");
                        }
                    }
                }, "text/xml");


            }
            function refreshScreen() {
                if ($('#docType').val() === 'MCT')
                {
                    $('#emailAddress_label').show(200);
                    $('#emailAddress').show(200);
                    $('#btnEmailCurrent').show(100);
                    document.getElementById('emailAddress').value = "${memberEmailAddress}";
                }
                else
                {
                    document.getElementById('emailAddress').value = "";
                    $('#emailAddress_label').hide(200);
                    $('#emailAddress').hide(200);
                    $('#btnEmailCurrent').hide(100);
                    document.getElementById("emailAddress").disabled = true;
                }
            }

            function onScreenAction(command, index) {
                EmailBtnName = 'btnEmail' + index;
                document.getElementById(EmailBtnName).disabled = true;
                document.getElementById(EmailBtnName).value = "Sending...";
                var url = "/ManagedCare/AgileController";
                var data = {
                    'opperation': command,
                    'emailAddress': document.getElementById('emailAddress').value,
                    'btnIndex': index
                };
                $.get(url, data, function(resultTxt) {
                    if (resultTxt != null) {
                        document.getElementById(EmailBtnName).value = resultTxt;
                    }
                });

                var data = null;
            }
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <agiletags:CallCenterTabs cctSelectedIndex="link_Docs" />
                    <fieldset id="pageBorder">  
                        <div class="content_area" >
                            <label class="header">Member Document</label>
                            <hr>                            
                            <agiletags:ControllerForm name="documentSorting">
                                <input type="hidden" name="opperation" id="opperation" value="CallCenterDocumentListCommand" />
                                <input type="hidden" name="fileLocation" id="fileLocation" value="" />
                                <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                                <input type="hidden" name="onScreen" id="onScreen" value="" />
                                
                                <table>
                                    <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Documents"  elementName="docKind" lookupId="254" errorValueFromSession="no" javaScript="onChange=\"toogleDocument(this.value, 'docType');\""/></tr>                                    
                                    <tr><agiletags:DocumentsList displayName="Type"  elementName="docType" mandatory="no" errorValueFromSession="yes" parentElement="docKind" javaScript="onChange=\"refreshScreen();\""/></tr>
                                    <c:if test="${applicationScope.Client == 'Sechaba'}">
                                        <tr id="documentUpload" style="display: none">
                                            <td><label>Document</label></td>
                                            <td><input type="file" name="uploadDoc" id="uploadDoc" size="75" form="uploadForm" /></td>
                                            <td><input type="submit" id="submitFile" value="Upload Document" form="uploadForm" onclick="document.getElementById('docuType').value = document.getElementById('docType').value; document.getElementById('submitFile').disabled;"/></td>
                                        </tr>
                                    </c:if>
                                    <tr><agiletags:LabelTextBoxError displayName="E-Mail Address" elementName="emailAddress"/></tr>
                                </table>
                                <label class="subheader">Member Documents</label>    
                                <table>
                                    <td><label>Start Date:</label></td>
                                    <td><input name="minDate" id="minDate" size="30" value="${minDate}"></td>
                                    <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('minDate', this);"/></td>
                                    <td>&nbsp;</td>
                                    <td><label>End Date:</label></td>
                                    <td><input name="maxDate" id="maxDate" size="30" value="${maxDate}"></td>
                                    <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('maxDate', this);"/></td>
                                    <td>&nbsp;</td>
                                    <td><input type="button" name="SearchButton" value="Search" onclick="submitWithAction('CallCenterDocumentListCommand', null);"/></td>
                                </table>
                                <div style ="display: none"  id="MemberDocumentDetails"></div>
                            </agiletags:ControllerForm>
                        </div>

                        <div class="content_area" >
                            <label class="subheader">Search Results</label>
                            <hr>
                            <c:choose>
                                <c:when test="${empty indexList}">
                                    <span class="label">No Document found</span>
                                </c:when>
                                <c:otherwise>
                                    <div id="docList">
                                        <table>
                                            <agiletags:ControllerForm name="documentIndexing">
                                                <input type="hidden" name="folderList" id="folderList" value="" />
                                                <input type="hidden" name="fileLocation" id="fileLocation" value="" />
                                                <input type="hidden" name="opperation" id="opperation" value="" />
                                                <agiletags:HiddenField elementName="searchCalled"/>
                                                <input type="hidden" name="onScreen" id="onScreen" value="" />
                                            </agiletags:ControllerForm>
                                            <tr><agiletags:MemberDocumentList command="CallDocumentViewCommand"  confirmCommand="CallDocumentViewCommand"/></tr>
                                        </table>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </fieldset>
                </td></tr></table>
                <iframe name="uploadFrame" id="uploadFrame" style="display: none"></iframe>    
                <form id="uploadForm" enctype="multipart/form-data" target="uploadFrame" method="POST" action="/ManagedCare/AgileController?opperation=MemberDocumentUploadCommand&memberNumber=${memberNumber}&entityType=5">
                    <input type="hidden" name="docuType" id="docuType" value="" />
                </form>
    </body>
</html>

<%-- 
    Document   : MemberStatements
    Created on : 2011/09/09, 01:53:18
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script language="JavaScript">
            $(document).ready(function() {
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });
                document.getElementById('memberNo').value = "<%=session.getAttribute("policyHolderNumber")%>";
                document.getElementById('stmtType').selectedIndex = 1;
                toggleStmtType(1);
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction1(action, actionParam) {
                document.getElementById('stmtDatesCorrect').value = $("#stmtDates").val();
                ;
                document.getElementById('opperation').value = action;
                document.getElementById('opperationParam').value = actionParam;
                document.forms[0].submit();
            }

            function submitWithAction(action, forField, onScreen) {
                document.getElementById('opperation').value = action;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('onScreen').value = onScreen;
                document.forms[0].submit();
            }

            function toggleStmtType(flagForBlur) {
                var str = "1";
                var memberNo = "<%=session.getAttribute("policyHolderNumber")%>";

                if (str === "1") {
                    $("#memRow").show();
                    $("#dateRow").show();
                    getPrincipalMemberForNumber(memberNo, "memberNo", "flag");
                }

                function toggleStmtDate() {
                    $("#dateRow").load();
                    $("#dateRow").show();
                }

                function getPrincipalMemberForNumber(str, element, statementScreenFlag) {
                    xmlhttp = GetXmlHttpObject();
                    if (xmlhttp === null)
                    {
                        alert("Your browser does not support XMLHTTP!");
                        return;
                    }
                    if (str.length > 0) {
                        var url = "/ManagedCare/AgileController";
                        url = url + "?opperation=GetMemberByNumberAndDirCommand&number=" + str + "&element=" + element + "&flag=" + statementScreenFlag;
                        var errorCheck = <%=session.getAttribute("memberNo_error")%>;

                        if (errorCheck === null || errorCheck === "") {
                            xmlhttp.onreadystatechange = function() {
                                stateChanged(element);
                            };
                            toggleStmtDate();
                            xmlhttp.open("POST", url, true);
                            xmlhttp.send(null);
                        } else {
                            clearFields();
                        }

                    } else {
                        clearFields();
                    }
                } 

                function clearFields() {
                    $("#stmtDates").empty();
                    document.getElementById('emailAddress').value = '';
                }


                function GetXmlHttpObject()
                {
                    if (window.XMLHttpRequest)
                    {
                        // code for IE7+, Firefox, Chrome, Opera, Safari
                        return new XMLHttpRequest();
                    }
                    if (window.ActiveXObject)
                    {
                        // code for IE6, IE5
                        return new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    return null;
                }

                function stateChanged(element) {
                    if (xmlhttp.readyState === 4)
                    {
                        //alert(xmlhttp.responseText);
                        var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                        //alert(result);
                        if (result === "GetMemberByNumberAndDirCommand") {

                            var resultArr = xmlhttp.responseText.split("|");
                            var numberArr = resultArr[1].split("=");
                            var emailArr = resultArr[2].split("=");
                            var productArr = resultArr[3].split("=");

                            var number = numberArr[1];
                            var email = emailArr[1];
                            var product = productArr[1];

                            document.getElementById(element + '_error').innerText = '';
                            document.getElementById(element).value = number;
                            document.getElementById('emailAddress').value = email;
                            document.getElementById('productId').value = product;
                            var dateList = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('#') + 1, xmlhttp.responseText.lastIndexOf('*'));

                            var selectObj = document.getElementById("stmtDates");
                            var selectParentNode = selectObj.parentNode;
                            var newSelectObj = selectObj.cloneNode(false); // Make a shallow copy
                            selectParentNode.replaceChild(newSelectObj, selectObj);

                            document.getElementById("stmtDates").add(new Option('', '99'));

                            var dateSplit = dateList.split('!');
                            for (index = 0; index < dateSplit.length; index++) {
                                var opt = document.createElement("option");
                                opt.text = dateSplit[index];
                                opt.value = dateSplit[index];
                                document.getElementById("stmtDates").add(opt);
                            }

                        }
                        } else if (result === "Error") {
                            var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|') + 1, xmlhttp.responseText.length);
                            if (forCommand === "GetMemberByNumberAndDirCommand") {
                                document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                                //document.getElementById(element + '_text').value = number;
                                // document.getElementById('emailAddress').value = email;
                                //document.getElementById('searchCalled').value = '';
                            } 
                        } else if (result === "Access") {
                            var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|') + 1, xmlhttp.responseText.length);
                            if (forCommand === "GetMemberByNumberAndDirCommand") {
                                document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                                //document.getElementById(element + '_text').value = number;
                                // document.getElementById('emailAddress').value = email;
                                //document.getElementById('searchCalled').value = '';
                            }
                        }
                    }
                }

                //screen validtion
                function validateStatementRequest(action, actionParam, validateEmail) {
                    var stmtType = "1";
                    var errorCount = 0;
                    var errorMsg = "";

                    if (stmtType === "99") {
                        errorCount++;
                        errorMsg = "#stmtType_error-Please Select a Statement Type|";

                    } else {
                        //statement type number validation
                        if (stmtType === "1") {
                            var memNo = $("#memberNo_text").val();
                            if (memNo === null || memNo === "") {
                                errorCount++;
                                errorMsg = "#memberNo_error-Please Enter a Member Number|";
                            }

                        } 
                        //satement date validation
                        var stmtDate = $("#stmtDates").val();
                        if (stmtDate === "99" || stmtDate === null) {
                            errorCount++;
                            errorMsg = "#stmtDates_error-Please Select a Statement Date|";
                        }

                        //validate email
                        if (validateEmail === "yes") {
                            var email = $("#emailAddress").val();
                            if (email === null || email === "") {
                                errorCount++;
                                errorMsg = "#emailAddress_error-Please Enter a Email Address|";
                            }
                        }
                    }
                    //print errors
                    if (errorCount > 0) {
                        var errors = errorMsg.split("|");
                        for (i = 0; i < errors.length; i++) {
                            var error = errors[i].split("-");
                            $(error[0]).text(error[1]);
                        }

                    } else {
                        submitWithAction1(action, actionParam);
                    }
            }

        </script>
    </head>
    <body>

        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">

                    <agiletags:CallCenterTabs cctSelectedIndex="link_SD" />       
                    <fieldset id="pageBorder">
                        <div class="content_area" >

                            <label class="header">Member Statements</label>
                            <br/><br/>
                            <table>
                                <agiletags:ControllerForm name="getMemberStmt">
                                    <input type="hidden" name="opperation" id="opperation" value="" />
                                    <agiletags:HiddenField elementName="searchCalled"/>
                                    <input type="hidden" name="onScreen" id="onScreen" value="" />
                                    <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                                    <input type="hidden" name="productId" id="productId" value="" />
                                    <input type="hidden" name="exactCoverNum" id="exactCoverNum" value="1" />
                                    <input type="hidden" name="stmtDatesCorrect" id="stmtDatesCorrect" value="" />

                                    <table> 
                                        <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Statement Type" elementName="stmtType" errorValueFromSession="yes" lookupId="169" javaScript="onChange=\"toggleStmtType(1);\"" mandatory="yes" disable="yes"/></tr>
                                        <tr id="memRow"><agiletags:LabelTextBoxError displayName="Member Number" elementName="memberNo" valueFromSession="Yes" enabled="no"/></tr>
                                        <tr id="dateRow"><agiletags:LabelDropDown displayName="Statement Dates" elementName="stmtDates" errorValueFromSession="yes" mandatory="no" javaScript="" /></tr>
                                        <tr id="emailRow"><agiletags:LabelTextBoxError displayName="E-Mail Address" elementName="emailAddress"/></tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="SubmitClaimCallCenterStatementCommand" displayname="Preview" javaScript="onClick=\"validateStatementRequest('SubmitClaimCallCenterStatementCommand', 'Preview', 'no')\";"/>
                                            <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="SubmitClaimCallCenterStatementCommand" displayname="Send" javaScript="onClick=\"validateStatementRequest('SubmitClaimCallCenterStatementCommand', 'Send', 'yes')\";"/>
                                        </tr>
                                    </table>
                                </agiletags:ControllerForm>

                        </div>
                    </fieldset>
                </td></tr></table>
    </body>
</html>

<%-- 
    Document   : AuxUserDetailsCallCenter
    Created on : 2009/12/04, 09:54:59
    Author     : gerritj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <agiletags:UserDetailsTable header="User Details"/>
                    <HR color="#666666" WIDTH="50%" align="left">
                    <table>
                        <tr><td><label class="header">Update Details</label></td></tr>
                        <tr><agiletags:LabelTextBoxError displayName="Name" elementName="name" valueFromRequest="username"/></tr>
                        <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="surname" valueFromRequest="usersurname"/></tr>
                    <tr><agiletags:LabelLookupValueDropDown displayName="Department:" elementName="department" lookupId="1"/><td></td></tr>
                    <tr><agiletags:ButtonOpperation type="submit" align="left" commandName="UpdateProfileCommand" displayname="Update" span="3"/></tr>
                    </table>
                    <!-- content ends here -->
        </td></tr></table>
    </body>
</html>

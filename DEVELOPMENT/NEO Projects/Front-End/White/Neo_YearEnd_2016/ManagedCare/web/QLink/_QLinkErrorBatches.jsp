<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

        <c:choose>
            <c:when test="${empty BatchResults}">
            <span>No results found</span>
            </c:when>
            <c:otherwise>
                <agiletags:ControllerForm name="QLinkErrorsBatchForm">
                    <input type="hidden" name="opperation" id="opperation" value="QLinkErrorsCommand" />
                    <input type="hidden" name="command" id="QLinkErrorsBatchCommand" value="selectBatch" />

                    <table id="BatchTable">
                        <tr>
                            <td width="160"><label class="label">Batch</label></td>
                            <td>
                                <select name="batch" onchange="submitFormWithAjaxPost(this.form, 'BatchDetailsDiv', this);">
                                    <option value="">-- Please select a Batch --</option>
                                    <c:forEach  var="entry2" items="${BatchResults}">
                                       <option value="${entry2['QLINK_BATCH_ID']}">Batch Seq: ${entry2['PAYROLL_BATCH_SEQ']} - Salary Month: ${entry2['SALARY_MONTH']} - Batch Date: ${entry2['CREATION_DATE']} </option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                    </table>
                </agiletags:ControllerForm>
                    <div id="BatchDetailsDiv"></div>
            </c:otherwise>
        </c:choose>
<%-- 
    Document   : PreAuthBasketNappiSearch
    Created on : 2010/09/21, 05:07:07
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script language="JavaScript">

            function checkNappiCode(command){
                var code = $("#code").val();
                var decr = $("#description").val();
                if(code != null || decr != null){
                    //var check = medRegex(code);
                    //if(check == true){
                        $("#searchButton_error").text("");
                        submitWithAction(command);
                   // }else{
                       // $("#searchButton_error").text("Invalid Nappi Code");
                   // }
                }else{
                    submitWithAction(command);
                }
            }
            function medRegex(str){
                var check1 = /^[0-9]+$/;
                var numCheck = check1.test(str);
                return numCheck;
            }

            function submitWithAction(action) {
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Search Medicine</label>
                    <br></br>
                    <table>
                        <agiletags:ControllerForm name="searchBasketTariff">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Provider Category" elementName="provCat" lookupId="59" mandatory="yes" javaScript=""  errorValueFromSession="yes"/></tr>
                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Plan Line" elementName="planYear" lookupId="93" javaScript="onblur=\"getBasketDetails(this.value);\"" /></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Code" elementName="code" /></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Description" elementName="description" /></tr>
                            <tr><agiletags:ButtonOpperationLabelError type="button" elementName="searchButton" commandName="" displayName="Search" javaScript="onclick=\"checkNappiCode('SearchPreAuthNappi');\"" /></tr>
                        </agiletags:ControllerForm>
                    </table>
                    <br></br>
                    <HR color="#666666" WIDTH="50%" align="left">
                    <agiletags:AuthNappiSearchResult sessionAttribute="" commandName="AllocateNappiToSession" javaScript="" />
                    <!-- content ends here -->
        </td></tr></table>
    </body>
</html>

<%-- 
    Document   : GenericAuth_Update
    Created on : 2010/07/11, 06:33:08
    Author     : johanl
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="neo.manager.NeoUser" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" language="JavaScript">

            //set current user
            <%
                NeoUser us2 = (NeoUser) session.getAttribute("persist_user");
                session.setAttribute("currentUser", us2.getName() + " " + us2.getSurname());
            %>

            var providerChanged = false;

            $(function () {
                var as = $("#authStatus").val();
                checkAuthRejectStatus(as);
                CheckSearchCommands();
                if ($("#authType").val() === null) {
                    $("#ProductHeader").hide();
                    $("#ProductTable").hide();
                    $("#ProductButton").hide();
                }
            });

            function CheckSearchCommands() {
                //reload ajax commands for screen population
                var searchVal = document.getElementById('searchCalled').value;
                if (searchVal == 'treatingProvider') {
                    validateTreatProvider(document.getElementById('treatingProvider_text').value, 'treatingProvider');

                } else if (searchVal == 'referringProvider') {
                    validateProvider(document.getElementById('referringProvider_text').value, 'referringProvider');

                } else if (searchVal == 'primaryICD') {
                    getICD10ForCode(document.getElementById("primaryICD_text").value, 'primaryICD');
                }
                document.getElementById('searchCalled').value = '';
                var authType = <%= session.getAttribute("authType")%>;
                toggleAuth(authType);

                if (searchVal == 'authTariffButton' && authType == '3') {
                    loadProv();
                    getOrthoPlanForTariff();
                }
                togglePMBOnLoad();

            }

            function setCoPayAccordingToPMB() {
                //set copay zero if pmb
                var pmbFound = $("#pmbFound").val();
                var isPMB = $("#isPMB").val();
                if (pmbFound === "yes" && isPMB === "yes") {
                    $("#coPay").val("0.0");
                }
            }

            function togglePMB() {
                var pmb = $("#pmb").val();
                if (pmb == "1") {
                    $("#coPay").val("0.0");
                    $("#coPay_error").text("");

                } else {
                    xhr = GetXmlHttpObject();
                    if (xhr == null) {
                        alert("Your browser does not support XMLHTTP!");
                        return;
                    }

                    var url = "/ManagedCare/AgileController?opperation=GetTariffCoPayDetails";
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.statusText == "OK") {
                            $("#authTariffButton_error").text("");
                            var rText = xhr.responseText.split("|");
                            var result = rText[0];
                            //readonly remove validation
                            var readOnlyRemove = rText[1];
                            if (readOnlyRemove == "yes") {
                                $("#coPay").attr('readonly', false);
                            } else {
                                $("#coPay").attr('readonly', true);
                            }

                            if (result == "Error") {
                                $("#authTariffButton_error").text("No Tariff(s) Found");
                                $("#coPay").val("0.0");

                            } else if (result == "Done") {
                                $("#coPay").val(rText[2]);
                                $("#coPay_error").text(rText[3]);
                            }
                        }
                    };
                    xhr.open("POST", url, true);
                    xhr.send(null);
                }
            }

            function GetXmlHttpObject() {
                if (window.ActiveXObject) {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function GetDOMParser(xmlStr) {
                var xmlDoc;
                if (window.DOMParser) {
                    parser = new DOMParser();
                    xmlDoc = parser.parseFromString(xmlStr, "text/xml");
                } else {
                    xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async = "false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }

            function clearTariffList() {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=ClearAuthTariffsFromList";
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        if (result == "Done") {
                            $("#AuthTariff").hide();
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            //provider search
            function validateProvider(str, element) {
                var error = "#" + element + "_error";
                if (str.length > 0) {
                    var url = "/ManagedCare/AgileController";
                    var data = {'opperation': 'FindProviderDetailsByCode', 'provNum': str, 'element': element, 'id': new Date().getTime()};
                    //alert("url = "+url);
                    $.get(url, data, function (resultTxt) {
                        if (resultTxt != null) {
                            //alert("resultTxt = "+resultTxt);
                            var resTxtArr = resultTxt.split("|");
                            var result = resTxtArr[0];
                            if (result == "Error") {
                                $(document).find(error).text("No such provider");
                            } else if (result == "Done") {

                                //set provider name  
                                var descArr = resTxtArr[1].split("=");
                                var pracName = descArr[1];

                                if (element == "referringProvider") {
                                    $("#referringProviderName").text(pracName);

                                } else if (element == "treatingProvider") {
                                    //set network
                                    var descArr = resTxtArr[3].split("=");
                                    var net = descArr[1];
                                    var length = net.length;
                                    var end = length - 3;
                                    var network = net.substr(0, end);
                                    $("#treatingProviderNetwork").text(network);
                                }
                                $(document).find(error).text("");
                            }
                        }
                    });
                } else {
                    $(document).find(error).text("");
                }
            }

            function returnValProvider(str, element) {
                var action = "ForwardToAuthTariffCommand";
                var forField = "authTariffButton";
                var onScreen = "/PreAuth/GenericAuth_Update.jsp";

                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("Your browser does not support XMLHTTP!");
                    returnVal = false;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=FindProviderDetailsByCode&provNum=" + str + "&element=" + element;
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var error = "#" + element + "_error";
                        var resultTxt = xhr.responseText;
                        var resTxtArr = resultTxt.split("|");
                        var result = resTxtArr[0];
                        //$(document).find(error).text("");
                        if (result == "Error") {
                            $(document).find(error).text("No such provider");

                        } else if (result == "Done") {
                            $(document).find(error).text("");
                            var descArr = resTxtArr[2].split("=");
                            var descipline = descArr[1];
                            $("#providerDesc").val(descipline);

                            //set network
                            if (element == "treatingProvider") {
                                var descArr = resTxtArr[3].split("=");
                                var net = descArr[1];
                                var length = net.length;
                                var end = length - 3;
                                var network = net.substr(0, end);
                                $("#treatingProviderNetwork").text(network);
                            }

                            var provDesc = descipline;
                            if (provDesc == "") {
                                submitWithAction(action, forField, onScreen);
                            } else {
                                //check radiology provider
                                var authType = "<%= session.getAttribute("authType")%>";
                                if (authType == '9') {
                                    //check provider discipline for 025, 038 or 056
                                    var pDescStr = $("#providerDesc").val();
                                    var pd = pDescStr.substring(0, pDescStr.indexOf("-") - 1);
                                    //alert(pd);
                                    if (pd == '025' || pd == '038' || pd == '056') {
                                        submitWithAction(action, forField, onScreen);
                                    } else {
                                        $("#treatingProvider_error").text("Radiology Auth not allowed for " + pd + " provider, only 025, 038, 056");
                                    }
                                } else {
                                    submitWithAction(action, forField, onScreen);
                                }
                            }
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);

            }

            function validateTreatProvider(str, element) {
                var error = "#" + element + "_error";
                if (str.length > 0) {
                    var url = "/ManagedCare/AgileController";
                    var data = {'opperation': 'FindProviderDetailsByCode', 'provNum': str, 'element': element, 'id': new Date().getTime()};
                    //alert("url = "+url);
                    $.get(url, data, function (resultTxt) {
                        if (resultTxt != null) {
                            //alert("resultTxt = "+resultTxt);
                            var resTxtArr = resultTxt.split("|");
                            var result = resTxtArr[0];
                            if (result == "Error") {
                                $(document).find(error).text("No such provider");
                            } else if (result == "Done") {
                                //set provider name
                                var descArr = resTxtArr[1].split("=");
                                var pracName = descArr[1];

                                $("#treatingProviderName").text(pracName);

                                //set descipline
                                var descArr = resTxtArr[2].split("=");
                                var descipline = descArr[1];
                                $("#providerDesc").val(descipline);

                                //set network
                                var descArr = resTxtArr[3].split("=");
                                var net = descArr[1];
                                var length = net.length;
                                var end = length - 3;
                                var network = net.substr(0, end);
                                $("#treatingProviderNetwork").text(network);

                                $(document).find(error).text("");
                                clearTariffList();
                            }
                        }
                    });
                } else {
                    $(document).find(error).text("");
                }
            }

            function getLabCodesForDisplay(command) {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=GetAuthLabCodeByTariffCommand&sesVal=tariffListArray";
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        if (result == "Done") {
                            submitWithAction(command);
                        } else if (result == "Error") {
                            alert(result);
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function getOrthoPlanForTariff() {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=GetAuthOrthoPlanDetails&sesVal=tariffListArray";
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        if (result == "Error") {
                            var error = "#authTariffButton_error";
                            $(document).find(error).text("No Orthodontic Plan for Tariff");
                        } else if (result == "Done") {

                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function validateMandatory(command, btnElement) {
                var btnElem = btnElement;
                //getProviderBeforeTariffDisplay('ForwardToAuthTariffCommand','authTariffButton','/PreAuth/GenericAuth_Update.jsp');
                xhr = GetXmlHttpObject();
                if (xhr == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=ValidateGenericAuthUpdate";
                //get form values

                var treatProv = $(document).find("#treatingProvider_text").val();
                var reqName = $(document).find("#requestorName").val();
                var reqRelationship = $("#requestorRelationship").val();
                var reqReason = $(document).find("#requestorReason").val();
                var reqContact = $(document).find("#requestorContact").val();
                var authFrom = $("#authFromDate").val();
                var authTo = $("#authToDate").val();
                var authStatus = $("#authStatus").val();
                var authType = <%= session.getAttribute("authType")%>;
                var pICD = $(document).find("#primaryICD_text").val();
                var authPeriod = $("#authPeriod").val();
                var authMonthPeriod = $("#authMonthPeriod").val();
                var pmb = $("#pmb").val();
                var copayAmount = $("#coPay").val();
                var isPMB = $("#isPMB").val();
                var foundPMB = $("#pmbFound").val();

                url = url + "&treatingProvider_text=" + treatProv + "&requestorName=" + reqName + "&requestorRelationship=" + reqRelationship;
                url = url + "&requestorReason=" + reqReason + "&requestorContact=" + reqContact;
                url = url + "&authFromDate=" + authFrom + "&authToDate=" + authTo + "&authStatus=" + authStatus + "&authPeriod=" + authPeriod + "&authMonthPeriod=" + authMonthPeriod;
                url = url + "&primaryICD_text=" + pICD;
                url = url + "&pmb=" + pmb + "&copay=" + copayAmount + "&isPMB=" + isPMB + "&foundPMB=" + foundPMB;

                if (authType == "7") {
                    var benSubType = $("#benSubType").val();
                    url = url + "&benSubType=" + benSubType;
                }

                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        ClearValidation();
                        if (result == "ERROR") {
                            var errorResponse = xhr.responseText.substring(xhr.responseText.indexOf('|') + 1, xhr.responseText.length)
                            var mandatoryList = errorResponse.split("|");
                            for (i = 0; i < mandatoryList.length; i++) {
                                var elementError = mandatoryList[i].split(":");
                                var element = elementError[0];
                                var errorMsg = elementError[1];

                                $(document).find("#" + element + "_error").text(errorMsg);
                            }

                        } else if (result == "OK") {
                            document.getElementById(btnElem).disabled = true;
                            if (authType == '2' || authType == '13') {
                                getLabCodesForDisplay(command);
                            } else {
                                submitWithAction(command);
                            }
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function getProviderBeforeTariffDisplay() {
                var provNumber = $("#treatingProvider_text").val();
                var provError = $("#treatingProvider_error").text();

                if (provError == "") {
                    if (provNumber != "") {
                        returnValProvider(provNumber, 'treatingProvider');
                    } else {
                        $("#authTariffButton_error").text("Provider Number Incorrect/Empty");
                    }
                }

            }

            function loadProv() {
                var provNumber = $("#treatingProvider_text").val();
                if (provNumber != "") {
                    validateProvider(provNumber, 'treatingProvider');
                } else {
                    $("#authTariffButton_error").text("Provider Number Incorrect/Empty");
                }
            }


            function ClearValidation() {

                $("#authPeriod_error").text("");
                $("#authMonthPeriod_error").text("");
                $("#authType_error").text("");
                $("#authStatus_error").text("");
                $("#authFromDate_error").text("");
                $("#authToDate_error").text("");
                $("#authDate_error").text("");
                $("#memberNum_error").text("");
                $("#depListValues_error").text("");
                $("#treatingProvider_error").text("");
                $("#primaryICD_error").text("");
                $("#authTariffButton_error").text("");
                $("#requestorName_error").text("");
                $("#requestorRelationship_error").text("");
                $("#requestorReason_error").text("");
                $("#requestorContact_error").text("");
                $("#pmb_error").text("");

            }

            function setICDToUpper(icd) {
                var icd = $.trim(icd);
                var tc = icd.charAt(0).toUpperCase();
                var sub = tc + icd.substring(1, icd.length);
                return sub;
            }

            function getICD10ForCode(str, element) {
                xhr = GetXmlHttpObject();
                if (xhr == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var str = $.trim(str);
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=GetICD10DetailsByCode&code=" + str + "&element=" + element;
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        var description = xhr.responseText.substring(xhr.responseText.lastIndexOf('=') + 1, xhr.responseText.lastIndexOf('$'));
                        var error = "#" + element + "_error";
                        var jqElement = "#" + element + "_text";
                        var descr = "#" + element + "Description";
                        $(document).find(descr).text(description);
                        $(error).text("");
                        if (result == "Error") {
                            $(error).text("No such diagnosis");
                        } else {
                            var up = setICDToUpper(str);
                            $(jqElement).val(up);
                            reloadPMBDetails();
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function reloadPMBDetails() {
                //get form values
                var pIcd = $("#primaryICD_text").val();
                var secIcd = $("#secondaryICD_text").val();
                var coMorIcd = $("#coMorbidityICD_text").val();

                var url = "${pageContext.request.contextPath}/AgileController";
                var data = {
                    'opperation': 'GetPMBByICDCommand',
                    'primaryICD_text': pIcd,
                    'secondaryICD_text': secIcd,
                    'coMorbidityICD_text': coMorIcd,
                    'id': new Date().getTime()
                };

                $.get(url, data, function (resultTxt) {
                    if (resultTxt !== null) {
                        var resTxtArr = resultTxt.split("|");
                        var result = resTxtArr[0];
                        if (result === "Done") {
                            //force refresh for nursing
                            var aType = $("#authType").val();
                            if (aType === "12" || aType === "11" || aType === "9") {
                                submitICDForcedRefreshWithScreen("/PreAuth/GenericAuth_Update.jsp");
                            }
                        }
                    }
                });
                data = null;

            }

            function toggleNullPMB() {
                var pmb = $("#pmb").val();
                if (pmb == null || pmb == "" || pmb == "99") {
                    $("#pmb option[value=0]").attr('selected', true);
                }

            }

            function togglePMBOnLoad() {
                var pmb = $("#pmb").val();
                var isPMB = $("#isPMB").val();
                var hasTariff = $("#tariffsExists").val();
                if (pmb == "99" || hasTariff != "yes") {

                } else if (pmb == "1" && isPMB == "yes") {
                    $("#coPay").val("0.0");
                    $("#coPay_error").text("");

                } else {
                    xhr = GetXmlHttpObject();
                    if (xhr == null) {
                        alert("Your browser does not support XMLHTTP!");
                        return;
                    }

                    var url = "/ManagedCare/AgileController?opperation=GetTariffCoPayDetails";
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.statusText == "OK") {
                            $("#authTariffButton_error").text("");
                            var rText = xhr.responseText.split("|");
                            var result = rText[0];
                            //readonly remove validation
                            var readOnlyRemove = rText[1];
                            if (readOnlyRemove == "yes") {
                                $("#coPay").attr('readonly', false);
                            } else {
                                $("#coPay").attr('readonly', true);
                            }

                            if (result == "Error") {
                                $("#authTariffButton_error").text("No Tariff(s) Found");
                                $("#coPay").val("0.0");

                            } else if (result == "Done") {
                                $("#coPay").val(rText[2]);
                                $("#coPay_error").text(rText[3]);
                            }
                            setCoPayAccordingToPMB();
                        }
                    };
                    xhr.open("POST", url, true);
                    xhr.send(null);
                }
            }

            function toggleAuth(value) {

                $("#BenefitHeader").hide();
                $("#AuthBenefit").hide();
                $("#AuthBenButton").hide();
                $("#treatProvRow").show();
                $("#benefitSubRow").hide();
                $("#srCopayRow").hide();
                $("#srPMBRow").hide();
                $("#overrideRow").hide();
                $("#ProductHeader").hide();
                $("#ProductTable").hide();
                $("#ProductButton").hide();

                if (value == '7') {
                    $("#AuthFrom").show();
                    $("#AuthTo").show();
                    $("#AuthNotes").show();
                    $("#AuthStatus").show();
                    $("#TariffSave").show();
                    $("#InterTravSave").hide();
                    $("#NormalSave").hide();
                    $("#BenefitSave").hide();
                    $("#AuthTButton").show();
                    $("#AuthTariff").show();
                    $("#TariffHeader").show();
                    $("#NappiSave").hide();
                    $("#NappiButton").hide();
                    $("#TraumaSave").hide();
                    $("#HospiceSave").hide();
                    $("#benefitSubRow").show();
                    $("#ProductHeader").show();
                    $("#ProductTable").show();
                    $("#ProductButton").show();

                    //super user validation
                    var isSup = "<%= session.getAttribute("persist_user_sup")%>";
                    var isTL = "<%= session.getAttribute("persist_user_teamLead")%>";
                    if (isSup == "true" || isTL == "true") {
                        $("#BenefitHeader").show();
                        $("#AuthBenefit").show();
                        $("#AuthBenButton").show();
                        $("#overrideRow").show();
                        var override = $("#overrideInd").val();
                        if (override === "99") {
                            document.getElementById("overrideInd").selectedIndex = "2";
                        }
                    } else {
                        $("#BenefitHeader").show();
                        $("#AuthBenefit").show();
                    }


                } else if (value == '12') {
                    $("#AuthFrom").show();
                    $("#AuthTo").show();
                    $("#AuthNotes").show();
                    $("#AuthStatus").show();
                    $("#InterTravSave").show();
                    $("#TariffSave").hide();
                    $("#NormalSave").hide();
                    $("#BenefitSave").hide();
                    $("#AuthTButton").hide();
                    $("#AuthTariff").hide();
                    $("#TariffHeader").hide();
                    $("#NappiSave").hide();
                    $("#NappiButton").hide();
                    $("#TraumaSave").hide();
                    $("#HospiceSave").hide();
                    $("#treatProvRow").hide();

                    //super user validation
                    var isSup = "<%= session.getAttribute("persist_user_sup")%>";
                    var isTL = "<%= session.getAttribute("persist_user_teamLead")%>";
                    if (isSup == "true" || isTL == "true") {
                        $("#BenefitHeader").show();
                        $("#AuthBenefit").show();
                        $("#AuthBenButton").show();
                    } else {
                        $("#BenefitHeader").show();
                        $("#AuthBenefit").show();
                    }


                } else if (value == '9') {
                    $("#AuthFrom").show();
                    $("#AuthTo").show();
                    $("#AuthNotes").show();
                    $("#AuthStatus").show();
                    $("#TariffSave").show();
                    //TARIFF COPAYMENTS
                    $("#srCopayRow").show();
                    //$("#srPMBRow").show(); 
                    toggleNullPMB();

                    var isSup = $("#isSupU").val();
                    var isTL = $("#isTeamL").val();
                    if (isSup == "yes" || isTL == "yes") {
                        $("#coPay").attr('readonly', false);
                    } else {
                        $("#coPay").attr('readonly', true);
                    }

                    //sey copay default
                    var copay = $("#coPay").val();
                    if (copay == null || copay == "" || copay == "null") {
                        $("#coPay").val("0.0");
                    }

                    $("#NormalSave").hide();
                    $("#BenefitSave").hide();
                    $("#AuthTButton").show();
                    $("#AuthTariff").show();
                    $("#TariffHeader").show();
                    $("#NappiSave").hide();
                    $("#NappiButton").hide();
                    $("#TraumaSave").hide();
                    $("#HospiceSave").hide();
                    $("#InterTravSave").hide();

                } else if (value == '10') {
                    $("#AuthFrom").show();
                    $("#AuthTo").show();
                    $("#AuthNotes").show();
                    $("#AuthStatus").show();
                    $("#TariffSave").hide();
                    $("#NormalSave").hide();
                    $("#BenefitSave").hide();
                    $("#AuthTButton").show();
                    $("#AuthTariff").show();
                    $("#TariffHeader").show();
                    $("#NappiSave").hide();
                    $("#NappiButton").hide();
                    $("#TraumaSave").show();
                    $("#HospiceSave").hide();
                    $("#InterTravSave").hide();

                } else if (value == '8') {
                    $("#AuthFrom").show();
                    $("#AuthTo").show();
                    $("#AuthNotes").show();
                    $("#AuthStatus").show();
                    $("#TariffSave").hide();
                    $("#NormalSave").hide();
                    $("#BenefitSave").show();
                    $("#AuthTButton").show();
                    $("#AuthTariff").show();
                    $("#TariffHeader").show();
                    $("#TraumaSave").hide();
                    $("#HospiceSave").hide();
                    $("#InterTravSave").hide();

                } else if (value == '4' || value == '5' || value == '6' || value == '11') {
                    $("#NormalSave").show();
                    $("#AuthFrom").hide();
                    $("#AuthTo").hide();
                    $("#AuthNotes").hide();
                    $("#AuthStatus").hide();
                    $("#TariffSave").hide();
                    $("#BenefitSave").hide();
                    $("#AuthTButton").hide();
                    $("#AuthTariff").hide();
                    $("#TariffHeader").hide();
                    $("#TraumaSave").hide();
                    $("#HospiceSave").hide();
                    $("#InterTravSave").hide();

                } else {
                    $("#NormalSave").show();
                    $("#AuthFrom").hide();
                    $("#AuthTo").hide();
                    $("#AuthNotes").hide();
                    $("#AuthStatus").hide();
                    $("#TariffSave").hide();
                    $("#BenefitSave").hide();
                    $("#AuthTButton").show();
                    $("#AuthTariff").show();
                    $("#TariffHeader").show();
                    $("#TraumaSave").hide();
                    $("#HospiceSave").hide();
                    $("#InterTravSave").hide();

                }
            }

            function validateTrauma(command, btnElement) {
                var btnElem = btnElement;
                var authType = '<%= session.getAttribute("authType")%>';
                var authPeriod = '<%= session.getAttribute("authPeriod")%>';
                var memNo = '<%= session.getAttribute("memberNum_text")%>';
                var depCode = '<%= session.getAttribute("depListValues")%>';
                var fromDate = '<%= session.getAttribute("authFromDate")%>';

                xhr = GetXmlHttpObject();
                if (xhr == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=ValidateTraumaCounselling&authType=" + authType + "&authPeriod=" + authPeriod + "&memNo=" + memNo + "&depCode=" + depCode + "&fromDate=" + fromDate;
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        if (result == "ERROR") {
                            var errorResponse = xhr.responseText.substring(xhr.responseText.indexOf('|') + 1, xhr.responseText.length)
                            var mandatoryList = errorResponse.split("|");
                            for (i = 0; i < mandatoryList.length; i++) {
                                var elementError = mandatoryList[i].split(":");
                                var element = elementError[0];
                                var errorMsg = elementError[1];

                                $(document).find("#" + element + "_error").text(errorMsg);
                            }

                        } else if (result == "Done") {
                            validateMandatory(command, btnElem);

                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function removeAllocatedBenefits() {
                xhr = GetXmlHttpObject();
                if (xhr == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=RemoveAllocatedBenefit";
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        if (result == "No") {
                            $("#AuthBenefit").show();
                        } else if (result == "Done") {
                            $("#AuthBenefit").hide();
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);

            }

            //new date validation
            function validateADRequest(date, element) {
                var ap = $("#authPeriod").text();
                ap = $.trim(ap);
                //var at = $("#authType").text();
                var at = '<%= session.getAttribute("authType")%>';

                if (at == "7" || at == "12") {
                    removeAllocatedBenefits();
                }

                validatePreAuthDate(date, element, ap, at, "no");

            }

            function validateBenefitSub(str) {
                if (str == "99") {
                    $("#benSubType_error").text("Please select a sub type");
                } else {
                    $("#benSubType_error").text("");
                }

            }

            function clearElement(element) {
                part = element + "_text";
                document.getElementById(element).value = "";
                document.getElementById(part).value = "";
            }
            function submitWithAction(action) {
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById("onScreen").value = onScreen;
                document.getElementById("searchCalled").value = forField;
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }

            //                function submitMemberDepWithAction(action, dep, onScreen){
            //                    var url="/ManagedCare/AgileController";
            //                    
            //                    var ap = $("#authPeriod").val();
            //                    var apm = $("#authMonthPeriod").val();
            //                    
            //                    var data = {
            //                        'opperation':action,
            //                        'onScreen':onScreen,
            //                        'authPeriod':ap,
            //                        'authMonthPeriod':apm,
            //                        'id':new Date().getTime()};
            //                    //alert("url = "+url); 
            //                    $.get(url, data, function(resultTxt){
            //                        if(resultTxt != null) {
            //                            submitGenericReload('ReloadPreauthGeneric', onScreen);
            //                        }
            //                    });
            //                }

            function submitMemberDepWithAction(action, dep, onScreen) {
                var url = "/ManagedCare/AgileController";
                var data = {'opperation': action, 'depListValues': dep, 'onScreen': onScreen, 'id': new Date().getTime()};
                //alert("url = "+url); 
                $.get(url, data, function (resultTxt) {
                    if (resultTxt != null) {
                        submitGenericReload('ReloadPreauthGeneric', onScreen);
                    }
                });
            }

            function submitGenericReload(action, onScreen) {
                document.getElementById("onScreen").value = onScreen;
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }

            function validateCopay(str) {
                var strChar;
                var valid = true;
                var strValidChars = "0123456789.";

                for (i = 0; i < str.length; i++) {
                    strChar = str.charAt(i);
                    if (strValidChars.indexOf(strChar) == -1) {
                        valid = false;
                    }
                }
                if (valid == false) {
                    $("#coPay_error").text("Invalid Amount");
                } else {
                    $("#coPay_error").text("");
                }
            }

            function checkAuthRejectStatus(status) {
                if (status === "2") {
                    $("#authRejReasonRow").show();
                } else {
                    $("#authRejReasonRow").hide();
                }
            }

            function submitICDForcedRefreshWithScreen(screen) {
                $("#opperation").val("RefreshCommand");
                $("#refreshScreen").val(screen);
                document.forms[0].submit();
            }

            function validateADSession(date, element) {
                var at = $("#authType").val();
                var ap = $("#authPeriod").val();
                var apm = $("#authMonthPeriod").val();
                validatePreAuthDate(date, element, ap, at, apm, "no");
            }

            function validateBenForward(screen, button, page) {
                //only check dep for now
                $("#memberNum_error").text("");
                var dep = "<%= session.getAttribute("depListValues")%>";

                if (dep != null && dep != "" && dep != "null") {
                    submitWithAction(screen, button, page);
                } else {
                    $("#memberNum_error").text("Please select dependant for benefit allocation");
                }

            }
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top" ><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <div align="left"><label class="header">Authorisation Details</label></div>
                    <br/>
                    <agiletags:ControllerForm name="saveAuth" validate="yes" >
                        <table>
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="providerDesc" id="providerDesc" value="${empty providerDesc ? "" : providerDesc}" />
                            <input type="hidden" name="isPMB" id="isPMB" value="${empty sessionScope.savedAuthPMBs ? "no" : "yes"}" />
                            <input type="hidden" name="tariffsExists" id="tariffsExists" value="${empty sessionScope.tariffListArray ? "no" : "yes"}" />
                            <input type="hidden" name="pmbFound" id="pmbFound" value="${empty sessionScope.authPMBDetails ? "no" : "yes"}" />
                            <input type="hidden" name="authType" id="authType" value="${sessionScope.authType}"/>
                            <input type="hidden" name="refreshScreen" id="refreshScreen" value="" />
                            <!-- Product Details -->
                            <!--<tr><agiletags:LabelTextDisplay displayName="Scheme" elementName="schemeName" valueFromSession="yes" javaScript="" /></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Scheme Option" elementName="schemeOptionName" valueFromSession="yes" javaScript="" /></tr>-->

                            <!-- Auth type -->
                            <!--<tr><agiletags:LabelTextDisplay displayName="Auth Period" elementName="authPeriod" javaScript="" valueFromSession="yes" /></tr>-->
                            <tr><agiletags:LabelTextDisplay displayName="Authorisation Type" elementName="authType_text" valueFromSession="yes" /></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Authorisation Date" elementName="authDate_text" valueFromSession="yes" /></tr>

                            <!-- Cover Details -->
                            <tr><agiletags:LabelTextDisplay displayName="Member Number" elementName="memberNum_label" valueFromSession="yes" /></tr>
                            <!--<tr><agiletags:LabelTextDisplay displayName="Cover Dependants" elementName="depListValues_text" valueFromSession="yes" /></tr>-->
                            <tr><agiletags:PreAuthPeriodIndicator displayName="Auth Period" elementName="authPeriod" javaScript="" mandatory="yes" valueFromSession="yes" /></tr>
                            <tr><agiletags:PreAuthPeriodMonthIndicator displayName="" elementName="authMonthPeriod" javaScript="" mandatory="yes" valueFromSession="yes" /></tr>
                            <tr><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Validate Cover" elementName="valCover" valueFromSession="yes" javaScript="onClick=\"submitMemberDepWithAction('SetSelectedCoverDep','valCov','/PreAuth/GenericAuth_Update.jsp')\";" /></tr>
                            <tr><td colspan="5"></td></tr>
                        </table>
                        <table>            
                            <tr><agiletags:PreAuthMemberSearchGrid sessionAttribute="covMemListDetails" commandName="" javaScript="" onScreen="/PreAuth/GenericAuth_Update.jsp" specificDependant="yes" /></tr>
                        </table>
                        <table>   
                            <tr><td colspan="5"></td></tr>

                            <!-- Cover Exclusions -->
                            <tr><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="View Exclusions" elementName="ViewExclusions" valueFromSession="yes" javaScript="onClick=\"submitWithAction('GetDependentCoverExclusions','getEx','/PreAuth/GenericAuth_Update.jsp')\";" /></tr>
                            <tr><agiletags:AuthExclusionListTableTag javaScript="onClick=\"submitWithAction('ReloadCoverExclusionCommand','reload','/PreAuth/GenericAuth_Update.jsp')\";" commandName="" /></tr>

                            <tr id="overrideRow"><agiletags:LabelNeoLookupValueDropDownError mandatory="yes" displayName="Override" elementName="overrideInd" lookupId="67" javaScript="" errorValueFromSession="yes" /></tr>
                            <tr id="benefitSubRow"><agiletags:LabelNeoLookupValueDropDownError mandatory="yes" displayName="Benefit Sub Type" elementName="benSubType" lookupId="179" javaScript="onChange=\"validateBenefitSub(this.value);\"" errorValueFromSession="yes" /></tr>
                            <!-- Providers -->
                            <tr id="treatProvRow"><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Treating Provider" elementName="treatingProvider" searchFunction="yes" searchOperation="ForwardToSearchProviderCommand" onScreen="/PreAuth/GenericAuth_Update.jsp" mandatory="yes" javaScript="onChange=\"validateTreatProvider(this.value, 'treatingProvider');\""/></tr>
                            <tr id="treatProvNameRow"><agiletags:LabelTextDisplay displayName="Provider Name" elementName="treatingProviderName" valueFromSession="yes" javaScript=""/></tr>
                            <tr><td colspan="5"></td></tr>
                            <tr id="treatProvNetworkRow"><agiletags:LabelTextDisplay displayName="Network" elementName="treatingProviderNetwork" valueFromSession="yes" javaScript=""/></tr>
                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Referring Provider" elementName="referringProvider" searchFunction="yes" searchOperation="ForwardToSearchProviderCommand" onScreen="/PreAuth/GenericAuth_Update.jsp" mandatory="no" javaScript="onChange=\"validateProvider(this.value, 'referringProvider');\""/></tr>
                            <tr id="refProvNameRow"><agiletags:LabelTextDisplay displayName="Provider Name" elementName="referringProviderName" valueFromSession="yes" javaScript=""/></tr>

                            <!-- ICD -->
                            <tr><agiletags:LabelTextDisplay displayName="Admission ICD10" elementName="admissionICD_text" valueFromSession="yes" /></tr>
                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Primary ICD10" elementName="primaryICD" searchFunction="yes" searchOperation="ForwardToSearchPreAuthICD10" onScreen="/PreAuth/GenericAuth_Update.jsp" mandatory="yes" javaScript="onChange=\"getICD10ForCode(this.value, 'primaryICD');\""/></tr>
                            <tr id="showdescription"><agiletags:LabelTextDisplay displayName="ICD10 Description" elementName="primaryICDDescription" valueFromSession="yes" javaScript=""/></tr>
                            <tr id="AuthFrom"><agiletags:LabelTextBoxDate displayname="Auth From Date" valueFromSession="Yes" elementName="authFromDate" mandatory="Yes" javascript="onblur=\"validateADRequest(this.value, this.id);\""/></tr>
                            <tr id="AuthTo"><agiletags:LabelTextBoxDate displayname="Auth To Date" valueFromSession="Yes" elementName="authToDate" mandatory="Yes" javascript="onblur=\"validateADRequest(this.value, this.id);\""/></tr>
                            <tr><td colspan="5"></td></tr>
                            <tr><td colspan="5"></td></tr>
                            <!-- Tariff Details -->
                            <tr id="TariffHeader"><td colspan="5" align="left"><label class="subheader">Tariff Details Allocated</label></td></tr>
                            <tr id="AuthTariff"><agiletags:AuthTariffListDisplayTag commandName="" sessionAttribute="tariffListArray"/></tr>
                            <tr id="AuthTButton"><agiletags:ButtonOpperationLabelError elementName="authTariffButton" type="button" commandName="ForwardToAuthTariffCommand" valueFromSession="no" displayName="Add New Tariff" mandatory="yes" javaScript="onClick=\"getProviderBeforeTariffDisplay();\""/></tr>
                            <tr><td colspan="5"></td></tr>
                            <tr><td colspan="5"></td></tr>

                            <!--Nappi Auth-->
<!--                            <tr id="NappiAuth"><td colspan="5"></td></tr>
                            <tr id="ProductHeader"><td colspan="5" align="left"><label class="subheader">Product Details Allocated</label></td></tr>
                            <tr id="ProductTable"><agiletags:AuthProductListDisplay commandName="" sessionAttribute="authProductListDetails" javaScript="" /></tr>
                            <tr id="ProductButton"><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Add New Product" elementName="productButton" javaScript="onClick=\"submitWithAction('ForwardToAuthNappiProduct','productButton','/PreAuth/GenericAuth_Update.jsp');\"" mandatory="no" /></tr>-->

                            <!-- Benefit Allocation  -->
                            <tr id="BenefitHeader"><td colspan="5" align="left"><label class="subheader">Benefit Allocated To Auth</label></td></tr>
                            <tr id="AuthBenefit"><agiletags:AuthBenefitListDisplay commandName="" sessionAttribute="benAllocated" javaScript="" /></tr>
                            <tr id="AuthBenButton"><agiletags:ButtonOpperation align="left" displayname="Allocate Benefit" commandName="ViewPreAuthBenefitsCommand" span="3" type="button" javaScript="onClick=\"validateBenForward('ViewPreAuthBenefitsCommand','authBenefitButton','/PreAuth/GenericAuth_Update.jsp')\";"/></tr>

                        </table>
                        <!--Tariif Copayment-->
                        <br/>
                        <table>
                            <c:choose>
                                <c:when test="${empty sessionScope.authPMBDetails}"></c:when>
                                <c:when test="${sessionScope.authPMBDetails == null}"></c:when>
                                <c:when test="${sessionScope.authType != '9'}"></c:when>
                                <c:otherwise>
                                    <tr id="PMBHeader"><td colspan="5" align="left"><label class="subheader">Auth PMB Details</label></td></tr>
                                    <tr id="PMBTable"><agiletags:AuthPMBListDisplay commandName="" javaScript="" /></tr>
                                    <tr id="PMBButton"><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Managed PMB(s)" elementName="pmbButton" javaScript="onClick=\"submitWithAction('ForwardToAuthSpecificPMB','pmbButton','/PreAuth/GenericAuth_Update.jsp');\"" mandatory="yes" /></tr>
                                </c:otherwise>
                            </c:choose>
                        </table>  
                        <table>
                            <br/>
                            <tr id="srPMBRow"><agiletags:LabelNeoLookupValueDropDownError displayName="PMB" elementName="pmb" lookupId="67" mandatory="yes"  errorValueFromSession="yes" javaScript="onchange=\"togglePMB();\"" /></tr>

                            <c:if test="${applicationScope.Client != 'Sechaba'}">
                                <tr id="srCopayRow"><agiletags:LabelTextBoxError displayName="Co-Payment" elementName="coPay" valueFromSession="yes" readonly="yes" javaScript="onchange=\"validateCopay(this.value);\"" /></tr>
                            </c:if>
                            <!-- Requestor Details -->
                            <tr><agiletags:LabelTextBoxError mandatory="yes" displayName="Requestor Name" valueFromSession="Yes" elementName="requestorName" /></tr>
                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Requestor Relationship" elementName="requestorRelationship" lookupId="91" mandatory="yes" javaScript="" errorValueFromSession="yes" /></tr>

                            <tr><agiletags:LabelTextBoxError mandatory="yes" displayName="Requestor Reason" valueFromSession="Yes" elementName="requestorReason"/></tr>
                            <tr><agiletags:LabelTextBoxError mandatory="yes" displayName="Requestor Contact" valueFromSession="Yes" elementName="requestorContact"/></tr>

                            <tr id="AuthNotes"><td colspan="5">
                                    <table width="100%">
                                        <tr><td colspan="5"></td></tr>
                                        <tr><td colspan="5" align="left"><label class="subheader">Auth Note Details</label></td></tr>
                                        <tr><agiletags:AuthNoteListDisplay sessionAttribute="authNoteList" commandName="" javaScript="" /></tr>
                                        <tr><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Manage Notes" elementName="authNoteButton" javaScript="onClick=\"submitWithAction('ForwardToAuthSpecificNote','authNoteButton','/PreAuth/GenericAuth_Update.jsp');\"" mandatory="no" /></tr>
                                    </table>
                                </td>
                            </tr>

                            <tr id="AuthStatus"><agiletags:LabelNeoLookupValueDropDownError displayName="Auth Status" elementName="authStatus" lookupId="111" mandatory="yes" errorValueFromSession="yes" javaScript="onchange=\"checkAuthRejectStatus(this.value);\""/></tr>
                            <tr id="authRejReasonRow"><agiletags:LabelNeoLookupValueDropDown elementName="authRejReason" displayName="Auth Reject Reason" lookupId="282" /></tr>                                                                                    

                            <tr id="NormalSave">
                                <td colspan="5" align="left">
                                    <button name="opperation" id="btnNormalReset" type="button" onClick="submitWithAction('ReloadGenericPreAuthCommand', 'reload', '/PreAuth/GenericAuth_Update.jsp');">Reset</button>
                                    <button name="opperation" id="btnNormalSave" type="button" onClick="validateMandatory('SaveCallCenterPreAuthUpdate', this.id);">Next</button>
                                </td>
                            </tr>

                            <tr id="TariffSave">
                                <td colspan="5" align="left">
                                    <button name="opperation" id="btnTariffReset" type="button" onClick="submitWithAction('ReloadGenericPreAuthCommand', 'reload', '/PreAuth/GenericAuth_Update.jsp');" value="">Reset</button>
                                    <button name="opperation" id="btnTariffSave" type="button" onClick="validateMandatory('SaveUpdatedPreAuthDetailsCommand', this.id);" value="">Save</button>
                                    <button name="opperation" type="button" onClick="" value=""> View Confirmation</button>
                                </td>
                            </tr>
                            <tr id="InterTravSave">
                                <td colspan="5" align="left">
                                    <button name="opperation" id="btnInterTravelReset" type="button" onClick="submitWithAction('ReloadGenericPreAuthCommand', 'reload', '/PreAuth/GenericAuth_Update.jsp');" value="">Reset</button>
                                    <button name="opperation" id="btnInterTravelSave" type="button" onClick="validateMandatory('SaveUpdatedPreAuthDetailsCommand', this.id);" value="">Save</button>
                                    <button name="opperation" type="button" onClick="" value=""> View Confirmation</button>
                                </td>
                            </tr>
                            <tr id="TraumaSave">
                                <td colspan="5" align="left">
                                    <button name="opperation" id="btnTraumaResetReset" type="button" onClick="submitWithAction('ReloadGenericPreAuthCommand', 'reload', '/PreAuth/GenericAuth_Update.jsp');" value="">Reset</button>
                                    <button name="opperation" id="btnTraumaSave" type="button" onClick="validateTrauma('SaveUpdatedPreAuthDetailsCommand', this.id);" value="">Save</button>
                                </td>
                            </tr>
                            <tr id="BenefitSave">
                                <td colspan="5" align="left">
                                    <button name="opperation" id="btnBenefitReset" type="button" onClick="submitWithAction('ReloadGenericPreAuthCommand', 'reload', '/PreAuth/GenericAuth_Update.jsp');" value="">Reset</button>
                                    <button name="opperation" id="btnBenefitSave" type="button" onClick="validateMandatory('SaveUpdatedPreAuthDetailsCommand', this.id);" value="">Save</button>
                                    <button name="opperation" type="button" onClick="" value=""> View Confirmation</button>
                                </td>
                            </tr>
                        </table>
                    </agiletags:ControllerForm>
                    <br/>
                    <table>
                        <tr><td colspan="4">
                                <label class="subheader">Disclaimer:</label><br/>
                                <p>Authorisation is subject to available benefits</p>
                            </td></tr></table>
                    <br/>
                    <table align="right">
                        <tr><agiletags:LabelTextDisplay boldDisplay="yes" displayName="Last Updated By" elementName="lastUpUser" javaScript="" valueFromSession="yes" /></tr>
                        <tr><agiletags:LabelTextDisplay boldDisplay="yes" displayName="Current User" elementName="currentUser" javaScript="" valueFromSession="yes" /></tr>
                    </table>
                    <!-- content ends here -->
                </td></tr></table>
    </body>
</html>

<%-- 
    Document   : OpticalDetails
    Created on : 2010/05/11, 08:55:08
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="neo.manager.NeoUser" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript">
            //set current user
            <%
                NeoUser user = (NeoUser) session.getAttribute("persist_user");
                session.setAttribute("currentUser", user.getName() + " " + user.getSurname());
            %>

                $(function() {
                    $("#authRejReasonRow").hide();                                                                            
                    CheckSearchCommands();
                });

                function GetXmlHttpObject(){
                    if (window.ActiveXObject){
                        // code for IE6, IE5
                        return new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    if (window.XMLHttpRequest){
                        // code for IE7+, Firefox, Chrome, Opera, Safari
                        return new XMLHttpRequest();
                    }
                    return null;
                }

                function setICDToUpper(icd){
                    var icd = $.trim(icd);
                    var tc = icd.charAt(0).toUpperCase();
                    var sub = tc + icd.substring(1, icd.length);
                    return sub;
                }

                function getICD10ForCode(str, element){
                    xhr=GetXmlHttpObject();
                    if (xhr==null)
                    {
                        alert ("Your browser does not support XMLHTTP!");
                        return;
                    }
                    var url="/ManagedCare/AgileController";
                    url=url+"?opperation=GetICD10DetailsByCode&code="+str+"&element="+element;
                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                            var error = "#"+element+"_error";
                            var jqElement = "#"+element+"_text";
                            $(error).text("");
                            if(result == "Error"){
                                $(error).text("No such diagnosis");
                            }else{
                                var up = setICDToUpper(str);
                                $(jqElement).val(up);
                                var description = xhr.responseText.substring(xhr.responseText.lastIndexOf('=')+1, xhr.responseText.lastIndexOf('$'));
                                $("#icdDescription3").text(description);
                                $("#icd3DescRow").show();
                            }
                        }
                    };
                    xhr.open("POST",url,true);
                    xhr.send(null);
                }

                function CheckSearchCommands(){
                    //reload ajax commands for screen population
                    $("#icd2DescRow").hide();
                    $("#icd3DescRow").hide();
                    var searchVal = document.getElementById('searchCalled').value;
                    if (searchVal == 'facilityProv'){
                        validatePractice(document.getElementById('facilityProv_text').value, 'facilityProv');

                    }else if(searchVal == 'secondaryICD'){
                        getMultiICD10ForCode(document.getElementById("secondaryICD_text").value, 'secondaryICD');

                    }else if(searchVal == 'coMorbidityICD'){
                        getICD10ForCode(document.getElementById("coMorbidityICD_text").value, 'coMorbidityICD');
                    }
                    document.getElementById('searchCalled').value = '';
                }

                //practice search
                function validatePractice(str, element){
                    //alert("validateProvider str = "+str);
                    var error = "#"+element+"_error";
                    if (str.length > 0) {
                        var url="/ManagedCare/AgileController";
                        var data = {'opperation':'FindPracticeDetailsByCode','provNum':str,'element':element,'id':new Date().getTime()};
                        //alert("url = "+url);
                        $.get(url, data, function(resultTxt){
                            if (resultTxt!=null) {
                                //alert("resultTxt = "+resultTxt);
                                var resTxtArr = resultTxt.split("|");
                                var result = resTxtArr[0];
                                if(result == "Error"){
                                    $(document).find(error).text("No such provider");
                                }else if(result == "Done"){
                                    //set provider name
                                    var descArr = resTxtArr[1].split("=");
                                    var pracName = descArr[1];
                                    $("#facilityProvName").text(pracName);  
                                    $(document).find(error).text("");
                                    
                                    //set network
                                    var descArr = resTxtArr[3].split("=");
                                    var net = descArr[1];
                                    var length = net.length;
                                    var end = length - 3;                                       
                                    var network = net.substr(0, end);
                                    $("#facilityProvNetwork").text(network);  
                                }
                            }
                        });
                    }
                }
                
                function setMultiICDToUpper(icds){
                    var returnStr = "";
                    var newIcd;
                    var icdList = icds.split(",");
                    if(icdList.length == 1){
                        return returnStr = setICDToUpper(icds);
                    }else{
                        for(i = 0; i < icdList.length; i++){
                            var icd = icdList[i];
                            newIcd = setICDToUpper(icd);
                            returnStr = returnStr + newIcd + ",";
                        }
                        return returnStr.substring(0, returnStr.length - 1);
                    }
              
                }

                function getMultiICD10ForCode(str, element){
                    xhr=GetXmlHttpObject();
                    if (xhr==null)
                    {
                        alert ("Your browser does not support XMLHTTP!");
                        return;
                    }
                    var url="/ManagedCare/AgileController";
                    url=url+"?opperation=GetMultipleICD10DetailsByCode&code="+str+"&element="+element;
                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            var rText = xhr.responseText.split("|");
                            var result = rText[0];
                            var error = "#"+element+"_error";
                            var jqElement = "#"+element+"_text";
                            $(error).text("");
                            if(result == "Error"){
                                $(error).text(rText[1]);
                            }else if(result == "Done"){
                                var up = setMultiICDToUpper(str);
                                $(jqElement).val(up);
                                var description = rText[1];
                                //set descriptions in order of codes
                                $("#icdDescription2").text(description);
                                $("#icd2DescRow").show();
                            }
                        }
                    };
                    xhr.open("POST",url,true);
                    xhr.send(null);
                }

                function validateOptical(command){
                    clearOpticalMandatory();
                    xhr = GetXmlHttpObject();
                    if (xhr==null)
                    {
                        alert ("Your browser does not support XMLHTTP!");
                        return;
                    }
                    var url="/ManagedCare/AgileController?opperation=ValidateOpticalDetails";
                    //get form values
                    var secIcd = $("#secondaryICD_text").val();
                    var comIcd = $("#coMorbidityICD_text").val();
                    var afd = $("#authFromDate").val();
                    var atd = $("#authToDate").val();
                    var numDays = $("#numDays").val();
                    var prevLens = $("#prevLens").val();
                    var curLens = $("#curLens").val();
                    var as = $("#authStatus").val();

                    var sIcd;
                    var cIcd;

                    if(secIcd != null && secIcd != ""){
                        sIcd = secIcd;
                    }else{
                        sIcd = "null";
                    }
                    if(comIcd != null && comIcd != ""){
                        cIcd = comIcd;
                    }else{
                        cIcd = "null";
                    }

                    url = url + "&afd="+afd+"&atd="+atd+"&as="+as+"&secIcd="+sIcd+"&comIcd="+cIcd;
                    url = url + "&numDays="+numDays+"&prevLens="+prevLens+"&curLens="+curLens;

                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));

                            if(result == "Error"){
                                var errorResponse = xhr.responseText.substring(xhr.responseText.indexOf('|')+1, xhr.responseText.length)
                                var mandatoryList = errorResponse.split("|");
                                for(i=0;i<mandatoryList.length;i++) {
                                    var elementError = mandatoryList[i].split(":");
                                    var element = elementError[0];
                                    var errorMsg = elementError[1];
                                    $(document).find("#"+element+"_error").text(errorMsg);
                                }

                            }else if(result == "Done"){
                                document.getElementById('saveButton').disabled = true;
                                //$("#saveButton").hide();
                                submitWithAction(command);
                            }
                        }
                    };
                    xhr.open("POST",url,true);
                    xhr.send(null);
                }

                function clearOpticalMandatory(){
                    $("#secondaryICD_error").text("");
                    $("#coMorbidityICD_error").text("");
                    $("#authFromDate_error").text("");
                    $("#authToDate_error").text("");
                    $("#authStatus_error").text("");
                    $("#numDays_error").text("");
                    $("#prevLens_error").text("");
                    $("#curLens_error").text("");
                }

                //new date validation
                function validateADSession(date, element){
                    var at = '<%= session.getAttribute("authType")%>';
                    var ap = '<%= session.getAttribute("authPeriod")%>';
                    validatePreAuthDate(date, element, ap, at, "no");

                }

                function clearElement(element) {
                    var part = element + '_text';
                    document.getElementById(element).value = '';
                    document.getElementById(part).value = '';
                }
                function submitWithAction(action) {
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }
                function submitWithAction(action, forField, onScreen) {
                    document.getElementById('onScreen').value = onScreen;
                    document.getElementById('searchCalled').value = forField;
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }
                
                function checkAuthRejectStatus(status){
                    if(status === "2"){
                        $("#authRejReasonRow").show();
                    }else{
                        $("#authRejReasonRow").hide();
                    }
                }                                
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Optical Detail</label>
                    <br/>
                    <agiletags:ControllerForm name="opticalDetail">
                        <table>
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Facility Provider" elementName="facilityProv" searchFunction="yes" searchOperation="ForwardToSearchPracticeCommand" onScreen="/PreAuth/OpticalDetails.jsp" mandatory="no" javaScript="onChange=\"validatePractice(this.value, 'facilityProv');\""/></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Provider Name" elementName="facilityProvName" valueFromSession="yes" javaScript=""/></tr>
                            <tr><td colspan="5"></td></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Network" elementName="facilityProvNetwork" valueFromSession="yes" javaScript=""/></tr>
                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Secondary ICD10" elementName="secondaryICD" searchFunction="yes" searchOperation="ForwardToSearchPreAuthMultiICD10" onScreen="/PreAuth/OpticalDetails.jsp" mandatory="no" javaScript="onChange=\"getMultiICD10ForCode(this.value, 'secondaryICD');\""/></tr>
                            <tr id="icd2DescRow"><agiletags:LabelTextDisplay displayName="ICD10 Description" elementName="icdDescription2" valueFromSession="yes" javaScript=""/></tr>
                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Co-Morbidity ICD10" elementName="coMorbidityICD" searchFunction="yes" searchOperation="ForwardToSearchPreAuthICD10" onScreen="/PreAuth/OpticalDetails.jsp" mandatory="no" javaScript="onChange=\"getICD10ForCode(this.value, 'coMorbidityICD');\""/></tr>
                            <tr id="icd3DescRow"><agiletags:LabelTextDisplay displayName="ICD10 Description" elementName="icdDescription3" valueFromSession="yes" javaScript=""/></tr>

                            <tr><agiletags:LabelTextBoxDate valueFromSession="Yes" displayname="Authorisation From Date" elementName="authFromDate" mandatory="yes" javascript="onblur=\"validateADSession(this.value, this.id);\"" /></tr>
                            <tr><agiletags:LabelTextBoxDate valueFromSession="Yes" displayname="Authorisation To Date" elementName="authToDate" mandatory="yes" javascript="onblur=\"validateADSession(this.value, this.id);\""/></tr>

                            <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Number of days" elementName="numDays" mandatory="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Previous lens RX" elementName="prevLens" mandatory="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Current lens RX" elementName="curLens" mandatory="yes"/></tr>

                            <tr><td colspan="5"></td></tr>
                            <tr><td colspan="5" align="left"><label class="subheader">Auth Note Details</label></td></tr>
                            <tr><agiletags:AuthNoteListDisplay sessionAttribute="authNoteList" commandName="" javaScript="" /></tr>
                            <tr><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Manage Notes" elementName="authNoteButton" javaScript="onClick=\"submitWithAction('ForwardToAuthSpecificNote','authNoteButton','/PreAuth/OpticalDetails.jsp');\"" mandatory="no" /></tr>

                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Auth Status" elementName="authStatus" lookupId="111" mandatory="yes" errorValueFromSession="yes" javaScript="onchange=\"checkAuthRejectStatus(this.value);\""/></tr>
                            <tr id="authRejReasonRow"><agiletags:LabelNeoLookupValueDropDown elementName="authRejReason" displayName="Auth Reject Reason" lookupId="282" /></tr>                                                                                                                
                            <tr><td colspan="5"></td></tr>
                            <tr><td colspan="5"></td></tr>
                            <tr>
                                <td colspan="5" align="left">
                                    <button name="opperation" type="button" onClick="submitWithAction('ReturnToGenericAuth','return','/PreAuth/GenericAuthorisation.jsp');" value="">Back</button>
                                    <button name="opperation" type="button" onClick="submitWithAction('ReloadGenericPreAuthCommand','reload','/PreAuth/OpticalDetails.jsp');" value="">Reset</button>
                                    <button id="saveButton" name="saveButton" type="button" onClick="validateOptical('SavePreAuthDetailsCommand');" value="">Save</button>
                                </td>
                            </tr>
                        </table>
                    </agiletags:ControllerForm>

                    <br/>
                    <table>
                        <tr><td colspan="4">
                                <label class="subheader">Disclaimer:</label><br/>
                                <p>The settlements of all claims are subject to the validity of membership of the patient to the scheme, as well as limits available.
                                    If tariff codes are specified, the member may be liable for any other tariff codes that may be used.
                                    If an amount is specified the member may be liable for any amount exceeding the specified amount.
                                    Any services rendered outside of the effective period have to be separately motivated for.
                                    All benefits, whether approved or not, will at all times be subject to the rules of the scheme.
                                    All treatments and procedures are funded according to the National Health Reference Price List as was provided by the Council of Medical Schemes
                                </p>
                            </td></tr>
                    </table>
                    <br/>
                    <table align="right">
                        <tr><agiletags:LabelTextDisplay boldDisplay="yes" displayName="Current User" elementName="currentUser" javaScript="" valueFromSession="yes" /></tr>
                    </table>
                    <!-- content ends here -->
                </td></tr></table>
    </body>
</html>

<%-- 
    Document   : EmailConfirmation
    Created on : 2012/12/13, 04:52:45
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script language="JavaScript">
              
            function validateEmail(str) {
                var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if(!regex.test(str)) {
                    return false;
                } else {
                    return true;
                }
            }
            
            function validateEmailBeforeSend(action){
                
                var treatE = $("#treatProvEmail").val();
                var facE = $("#facProvEmail").val(); 
                var meme = $("#memberEmail").val(); 
                var ad = $("#addEmail").val();
                
                var validEmails = true;
                var emails = new String;
                
                //clear email fields
                $("#treatProvEmail_error").text("");
                $("#facProvEmail_error").text("");
                $("#memberEmail_error").text("");
                $("#addEmail_error").text("");
                
                
                //validate treating provider email
                if(treatE !== null && treatE !== ""){
                    var valid = validateEmail(treatE);
                    if(!valid){
                        $("#treatProvEmail_error").text("Incorrect email format");
                        validEmails = false;
                    }else{
                        emails = emails + treatE +"|";
                    }
                }
                //validate facility provider email
                if(facE !== null && facE !== ""){
                    var valid = validateEmail(facE);
                    if(!valid){
                        $("#facProvEmail_error").text("Incorrect email format");
                        validEmails = false;
                    }else{
                        emails = emails + facE +"|";
                    }
                }
                //validate member email
                if(meme !== null && meme !== ""){
                    var valid = validateEmail(meme);
                    if(!valid){
                        $("#memberEmail_error").text("Incorrect email format");
                        validEmails = false;
                    }else{
                        emails = emails + meme +"|";
                    }
                }
                //validate addtional email
                if(ad !== null && ad !== ""){
                    var valid = validateEmail(ad);
                    if(!valid){
                        $("#addEmail_error").text("Incorrect email format");
                        validEmails = false;
                    }else{
                        emails = emails + ad +"|";
                    }
                }
                
                if(validEmails){
                    submitWithAction(action, emails);
                }
                
            }
            
            function submitWithAction2(action, emails) {
                document.getElementById('opperation').value = action;
                document.getElementById('emails').value = emails;
                document.forms[0].submit();

            }
            
            function submitWithAction(action, emails) {
                document.getElementById('opperation').value = action;
                document.getElementById('emails').value = emails;
                document.forms[0].submit();

            }
            
            function submitWithActionAll(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top" ><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <div align="left"><label class="header">Email Confirmation</label></div>
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="emailConf" validate="yes">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="emails" id="emails" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />

                            <tr><agiletags:LabelTextBoxError displayName="Service Provider" elementName="treatProvEmail" valueFromSession="yes" enabled="yes" javaScript="" />
                            <tr><agiletags:LabelTextBoxError displayName="Hospital" elementName="facProvEmail" valueFromSession="yes" enabled="yes" javaScript="" />
                            <tr><agiletags:LabelTextBoxError displayName="Member" elementName="memberEmail" valueFromSession="yes" enabled="yes" javaScript="" />
                            <tr><agiletags:LabelTextBoxError displayName="Additional" elementName="addEmail" valueFromSession="yes" enabled="yes" javaScript="" />

                            </agiletags:ControllerForm>
                    </table>
                    <br/>
                    <table>
                        <tr><agiletags:ButtonOpperationLabelError displayName="Send" elementName="doneButton" type="button" commandName="" javaScript="onClick=\"validateEmailBeforeSend('SendAuthConfirmation');\"" valueFromSession="yes" /></tr>
                        <tr><agiletags:ButtonOpperationLabelError displayName="Return" elementName="returnButton" type="button" commandName="" javaScript="onClick=\"submitWithAction2('GetAuthConfirmationDetails');\"" valueFromSession="yes" /></tr>
                    </table>
                </td></tr></table>
    </body>
</html>

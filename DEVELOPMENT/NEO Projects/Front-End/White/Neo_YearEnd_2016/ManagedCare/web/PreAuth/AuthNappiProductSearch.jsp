<%-- 
    Document   : AuthNappiProductSearch
    Created on : 14 May 2015, 10:48:21 AM
    Author     : martins
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.scrollTo-1.4.2-min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script language="JavaScript">

                function GetXmlHttpObject() {
                    if (window.ActiveXObject) {
                        // code for IE6, IE5
                        return new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    if (window.XMLHttpRequest) {
                        // code for IE7+, Firefox, Chrome, Opera, Safari
                        return new XMLHttpRequest();
                    }
                    return null;
                }      
                
                function clearElement(element) {
                    part = element + '_text';
                    document.getElementById(element).value = '';
                    document.getElementById(part).value = '';
                }

                function submitWithAction(action) {
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }
                
                function submitWithAction(action, forField, onScreen) {
                    //document.getElementById('onScreen').value = onScreen;
                    document.getElementById('searchCalled').value = forField;
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }
                
                function SelectProdList(arrayNum){
                        $("#selected").val(arrayNum);
                        submitWithAction('ForwardToAuthNappiProduct');
                }

                function Checkfields(){
                    if($("#productCode").val() === null && $("#productName").val() === null || $("#productCode").val() === "" && $("#productName").val() === ""){
                        $("#productName_error").text("Please Refine Search");
                    }else{
                        $("#productName_error").text("");
                        submitWithAction('AuthGetNappiList');
                    }   
                }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Search Pre-Auth Product</label>
                    <br/><br/>
            <agiletags:ControllerForm name="ProductDetails" validate="yes">
                <input type="hidden" name="opperation" id="opperation" value="" />
                <agiletags:HiddenField elementName="searchCalled" />
                <input type="hidden" name="selected" id="selected" value="" />

                <table>
                    <tr><td><agiletags:LabelTextBoxError displayName="Product Code" elementName="productCode" valueFromSession="yes" /></td></tr>
                    <tr><td><agiletags:LabelTextBoxError displayName="Product Name" elementName="productName" valueFromSession="yes" /></td></tr>    
                    <tr><td><agiletags:ButtonOpperation commandName="searchProduct" align="" displayname="Search" span="3" type="button"  javaScript="onClick=Checkfields();"/></td></tr>
                    <tr><td><agiletags:AuthProductSearchResultTag commandName="" sessionAttribute="authProductTableTag" javaScript=""  /></td></tr>
                </table>


            </agiletags:ControllerForm>
            <br/>
            <table align="right">
                <tr><agiletags:LabelTextDisplay boldDisplay="yes" displayName="Current User" elementName="currentUser" javaScript="" valueFromSession="yes" /></tr>
            </table>
        </td></tr></table>
    </body>
</html>
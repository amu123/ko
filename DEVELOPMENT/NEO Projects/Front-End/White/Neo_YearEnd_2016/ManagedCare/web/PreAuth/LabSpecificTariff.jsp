<%-- 
    Document   : LabSpecificTariff
    Created on : 2010/06/02, 04:57:06
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.scrollTo-1.4.2-min.js"></script>
        <script language="JavaScript">
            
            $(function(){
                var error = $("#doubError_error").val();
               if(error != null && error != "null" && error != ""){
                   $("#doubError").text(error);
                   $.scrollTo($("#doubError"),600);
                   $("#doubError_error").val("");
               }
               
            });

            function GetXmlHttpObject(){
                if (window.ActiveXObject){
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest){
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

 
            function CalculateTariffQuantity(str,session){
                xhr=GetXmlHttpObject();
                if(xhr==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=CalculateTariffQuantityCommand&quantity="+str+"&tariffType="+ session;
                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        var resText = xhr.responseText;
                        var strArr = new Array();
                        strArr = resText.split('|');

                        var tQuan = new String(strArr[1]);
                        var tAmount = new String(strArr[2]);

                        $("#tariffQuantity"+session).val(tQuan);
                        $("#tariffAmount"+session).val(tAmount);
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function hideRows(id) {
                $("#"+id+"plus").show();
                $("#"+id+"minus").hide();
                var hiddenRows = $("tr[id^="+id+"hidden]");
                for(i = 0; i < hiddenRows.size(); i++){
                    $(hiddenRows[i]).hide();
                }

            }
            function showRows(id) {
                $("#"+id+"plus").hide();
                $("#"+id+"minus").show();
                var hiddenRows = $("tr[id^="+id+"hidden]");
                for(i = 0; i < hiddenRows.size(); i++){
                    $(hiddenRows[i]).show();
                }
            }

            function selectAllRows(tariff) {
                $("#labsTable input[id=labCheckAll"+tariff+"]").click(function() {
                    var checked = $(this).is(":checked");
                    $("#labsTable input[id="+tariff+"boxes]").each(function(){
                        if(checked === true){
                            $(this).attr('checked', true);
                        }else{
                            $(this).attr('checked', false);
                        } 
                        
                        
                    });
                });
            };
            
            function showAllRows(tariff) {
                $("#labsTable input[id=labExpandAll"+tariff+"]").click(function() {
                    var checked = $(this).is(":checked");        
                    $("#labsTable input[id="+tariff+"boxes]").each(function(){
                        if(checked === true){                         
                            $("#trSpace"+tariff+"").hide();                            
                            $(this).closest("tr").show();
                        }else{
                            $("#trSpace"+tariff+"").show();
                            $(this).closest("tr").hide();
                        } 
                        
                        
                    });
                });
            };
            
            function testCB(id){
                var cbVal;
                
                if($("#"+id).is(":checked")){
                    $("#"+id).attr("checked",false);
                    $("#"+id).attr("value", "off");
                    cbVal = $("#"+id).val();
                }else{
                    $("#"+id).attr("checked",true);
                    $("#"+id).attr("value", "on");
                    cbVal = $("#"+id).val();
                }
                alert(cbVal);
            }
            
            function validateDouble(str){               
                var check1 = /^[-+]?\d{1,4}(\.\d{1,2})?$/;
                var numCheck = check1.test(str);
                return numCheck;
            }

            function validateLabDoub(str, element){
                var check = validateDouble(str);
                if(str === null || str === "" || str === "null"){
                    check == false;
                } 
                var errorField = "#doubError";
                var jqVal = "#"+element;               
                var errorVals = $(jqVal).closest("tr").attr("id");
                var errorValsSplit = errorVals.split("|");
                var errorValTariff = errorValsSplit[0].replace("Row", "");
                if(check == false){
                    $(errorField).text("Invalid Amount on Tariff "+errorValTariff + ", Lab Code " + errorValsSplit[1] + ", Amount : "+str);
                    //$(errorField).text("Invalid Amount "+str);
                    $.scrollTo($(errorField),600);
                    $("#doubError").val("");
                }else{
                    $(errorField).text("");
                }
            }

            function clearElement(element) {
                part = element + '_text';
                document.getElementById(element).value = '';
                document.getElementById(part).value = '';
            }
            function submitWithActionLabSave(action) {
                
                var errorFound = $("#doubError_error").val();
                if(errorFound != null && errorFound != "" && errorFound != "null"){
                    $.scrollTo($("#doubError"),600);
                    $("#doubError").val("");
                }else{
                
                    var indexSelectionsLab = new String;
                    var indexSelectionsNoLab = new String;
                    
                    //do lab
                    $('#labsTable input[type="checkbox"]:checked').each(function(){
                    
                        if($(this).attr("checked") === true && $(this).attr("value") === "on"){
                    
                            var parentRow = $(this).closest("tr").attr("id");
                            parentRow = parentRow.replace("Row", "");
                            var subSelectorArr = parentRow.split("|");
                            var subSelector = subSelectorArr[0]+subSelectorArr[1];
                            //parentRow = subSelectorArr[0]+ "|" + subSelectorArr[1];
                            //var quan = $(this).closest("tr").find("#tariffQuantity"+subSelector+"").val();
                            var amount = $(this).closest("tr").find("#tariffAmount"+subSelector+"").val();
                            parentRow = parentRow + "|" + amount;
                            indexSelectionsLab = indexSelectionsLab + parentRow + "%";
                        }
                    });
                
                    //do no lab
                    $('#noLabsTable input[type="checkbox"]:checked').each(function(){
                        if($(this).attr("checked") === true && $(this).attr("value") === "on"){

                            var parentRow = $(this).closest("tr").attr("id");
                            parentRow = parentRow.replace("Row", "");
                            var tooth = $(this).closest("tr").find("#toothNum"+parentRow+"").val(); 
                            parentRow = parentRow + "|" + tooth;
                            indexSelectionsNoLab = indexSelectionsNoLab + parentRow + "%";

                    
                        }
                    });
                
                    if(indexSelectionsLab !== null && indexSelectionsLab !== ""){
                        indexSelectionsLab = indexSelectionsLab.substr(0, indexSelectionsLab.length-1);                                       
                        document.getElementById('selectedLabValues').value = indexSelectionsLab;
                        $("#saveLabs_error").text("");                       
                    }    
                
                    if(indexSelectionsNoLab !== null && indexSelectionsNoLab !== ""){
                        indexSelectionsNoLab = indexSelectionsNoLab.substr(0, indexSelectionsNoLab.length-1);                                       
                        document.getElementById('selectedNoLabValues').value = indexSelectionsNoLab;
                        $("#saveNoLabs_error").text("");
                    }                 
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();                
                    $("#doubError").text("");
                    $("#doubError_error").val("");
                }
            }
            function submitWithAction(action, forField, onScreen) {         
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <label class="header">Authorisation Specific Dental Code Details</label>
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="saveLabTariff" validate="yes">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="selectedLabValues" id="selectedLabValues" value="" />
                            <input type="hidden" name="selectedNoLabValues" id="selectedNoLabValues" value="" />
                            <input type="hidden" name="doubError_error" id="doubError_error" value="${sessionScope.doubError_error}" />
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <tr><td><label id="doubError" class="error" ></label></td></tr>
                            <tr><agiletags:AuthLabTariffListTableTag sessionAttribute="DentalLabCodeList" commandName="ReturnLabToAuthCommand" /></tr>
                        </agiletags:ControllerForm>
                    </table>
                </td></tr></table>
    </body>
</html>

<%-- 
    Document   : OrthodonticDetails
    Created on : 2009/12/17, 09:21:10
    Author     : whauger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="neo.manager.NeoUser" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript">
            //set current user
            <%
                        NeoUser user = (NeoUser) session.getAttribute("persist_user");
                        session.setAttribute("currentUser", user.getName() + " " + user.getSurname());
            %>

                $(function() {
                    $("#authRejReasonRow").hide();                                                                            
                });

                function GetXmlHttpObject(){
                    if (window.ActiveXObject){
                        // code for IE6, IE5
                        return new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    if (window.XMLHttpRequest){
                        // code for IE7+, Firefox, Chrome, Opera, Safari
                        return new XMLHttpRequest();
                    }
                    return null;
                }

                function SetPlannedDate(){
                    var authfrom = $("#authFromDate").val();
                    var duration = $("#planDuration").val();
                    $("#orthoTDButton").hide();
                    $("#planDuration_error").text("");
                    $("#authFromDate_error").text("");
                    if(authfrom != null && authfrom != ""){
                        if(duration != null && duration != ""){
                            var check = validateNumber(duration);
                            if(check == true){
                                xhr = GetXmlHttpObject();
                                if (xhr==null){
                                    alert ("Your browser does not support XMLHTTP!");
                                    return;
                                }
                                var url="/ManagedCare/AgileController";
                                url=url+"?opperation=CalculateOrthoPlanDate&authfrom="+authfrom+"&duration="+duration;
                                xhr.onreadystatechange=function(){
                                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                                        var rText = xhr.responseText.split("|");
                                        var result = rText[0];
                                        if(result == "Done"){
                                            $("#authToDate").val(rText[1]);
                                        }else if (result == "Error"){
                                            $("#authFromDate").val(rText[1])
                                        }
                                    }
                                };
                                xhr.open("POST",url,true);
                                xhr.send(null);
                            }else{
                                $("#planDuration_error").text("Invalid Duration");
                            }
                        }else{
                            $("#planDuration_error").text("Empty Duration Not Allowed");
                        }
                    }else{
                        $("#authFromDate_error").text("Empty From Date Not Allowed");
                    }

                }
            
                function validateNumber(str){
                    var strChar;
                    var valid = true;
                    var strValidChars = "0123456789";
                
                    for (i = 0; i < str.length; i++){
                        strChar = str.charAt(i);
                        if (strValidChars.indexOf(strChar) == -1){
                            valid = false;
                        }
                    }
                    return valid;
                }
                function validateAC(str){
                    var strChar;
                    var valid = true;
                    var strValidChars = "0123456789.";

                    for (i = 0; i < str.length; i++){
                        strChar = str.charAt(i);
                        if (strValidChars.indexOf(strChar) == -1){
                            valid = false;
                        }
                    }
                    if(valid == false){
                        $("#amountClaimed_error").text("Invalid Amount");
                    }else{
                        $("#amountClaimed_error").text("");
                    }

                }

                function validateOrtho(command){
                    clearOrthoMandatory();
                    xhr = GetXmlHttpObject();
                    if (xhr==null)
                    {
                        alert ("Your browser does not support XMLHTTP!");
                        return;
                    }
                    var url="/ManagedCare/AgileController?opperation=ValidateOrthoDetails";
                    //get form values
                    var pd = $("#planDuration").val();
                    var ac = $("#amountClaimed").val();
                    var afd = $("#authFromDate").val();
                    var atd = $("#authToDate").val();
                    var as = $("#authStatus").val();

                    url = url + "&afd="+afd+"&atd="+atd+"&as="+as+"&pd="+pd+"&ac="+ac;

                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));

                            if(result == "Error"){
                                var errorResponse = xhr.responseText.substring(xhr.responseText.indexOf('|')+1, xhr.responseText.length)
                                var mandatoryList = errorResponse.split("|");
                                for(i=0;i<mandatoryList.length;i++) {
                                    var elementError = mandatoryList[i].split(":");
                                    var element = elementError[0];
                                    var errorMsg = elementError[1];
                                    $(document).find("#"+element+"_error").text(errorMsg);
                                }

                            }else if(result == "Done"){
                                document.getElementById('saveButton').disabled = true;
                                //$("#saveButton").hide();
                                submitWithAction(command);
                            }
                        }
                    };
                    xhr.open("POST",url,true);
                    xhr.send(null);
                }

                function clearOrthoMandatory(){
                    $("#planDuration_error").text("");
                    $("#amountClaimed_error").text("");
                    $("#authFromDate_error").text("");
                    $("#authToDate_error").text("");
                    $("#authStatus_error").text("");

                }

                function clearElement(element) {
                    part = element + '_text';
                    document.getElementById(element).value = '';
                    document.getElementById(part).value = '';
                }
                function submitWithAction(action) {
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }
                function submitWithAction(action, forField, onScreen) {
                    document.getElementById('onScreen').value = onScreen;
                    document.getElementById('searchCalled').value = forField;
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }

                function validateADSession(date, element){
                    var at = '<%= session.getAttribute("authType")%>';
                    var ap = '<%= session.getAttribute("authPeriod")%>';
                    validatePreAuthDate(date, element, ap, at, "no");

                }

                function checkAuthRejectStatus(status){
                    if(status === "2"){
                        $("#authRejReasonRow").show();
                    }else{
                        $("#authRejReasonRow").hide();
                    }
                }
        </script>
    </head>
    <body onload="SetPlannedDate();">
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Orthodontic Detail</label>
                    <br/><br/>
                    <agiletags:ControllerForm name="orthodonticDetail">
                        <table>
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Plan Duration" elementName="planDuration" javaScript="onChange=\"SetPlannedDate();\"" mandatory="yes" /></tr>
                            <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Total Amount Claimed" elementName="amountClaimed" mandatory="yes" javaScript="onChange=\"validateAC(this.value);\""/></tr>
                            <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Deposit Amount" elementName="depositAmount"/></tr>
                            <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="First Installment Amount" elementName="firstAmount"/></tr>
                            <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Remaining Installment Amount" elementName="remainAmount"/></tr>
                            <tr><agiletags:LabelTextBoxDate valueFromSession="Yes" displayname="Date Valid From" elementName="authFromDate" mandatory="yes" javascript="onblur=\"validateADSession(this.value, this.id);\"" /></tr>
                            <tr id="orthoTDButton"><agiletags:ButtonOpperation displayname="Generate To Date" span="1" align="left" type="button" commandName="" javaScript="onclick=\"SetPlannedDate();\"" /></tr>
                            <tr><agiletags:LabelTextBoxDate valueFromSession="Yes" displayname="Date Valid To" elementName="authToDate" mandatory="yes" javascript="onblur=\"validateADSession(this.value, this.id);\"" /></tr>

                            <tr><td colspan="5"></td></tr>
                            <tr><td colspan="5" align="left"><label class="subheader">Auth Note Details</label></td></tr>
                            <tr><agiletags:AuthNoteListDisplay sessionAttribute="authNoteList" commandName="" javaScript="" /></tr>
                            <tr><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Manage Notes" elementName="authNoteButton" javaScript="onClick=\"submitWithAction('ForwardToAuthSpecificNote','authNoteButton','/PreAuth/OrthodonticDetails.jsp');\"" mandatory="no" /></tr>

                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Auth Status" elementName="authStatus" lookupId="111" mandatory="yes" javaScript="" errorValueFromSession="yes" javaScript="onchange=\"checkAuthRejectStatus(this.value);\""/></tr>
                            <tr id="authRejReasonRow"><agiletags:LabelNeoLookupValueDropDown elementName="authRejReason" displayName="Auth Reject Reason" lookupId="282" /></tr>                                                                                                                                                                        
                            <tr><td colspan="5"></td></tr>
                            <tr><td colspan="5"></td></tr>
                            <tr>
                                <td colspan="5" align="left">
                                    <button name="opperation" type="button" onClick="submitWithAction('ReturnToGenericAuth','return','/PreAuth/GenericAuthorisation.jsp');" value="">Back</button>
                                    <button name="opperation" type="button" onClick="submitWithAction('ReloadGenericPreAuthCommand','reload','/PreAuth/OrthodonticDetails.jsp');" value="">Reset</button>
                                    <button id="saveButton" name="saveButton" type="button" onClick="validateOrtho('SavePreAuthDetailsCommand');" value="">Save</button>
                                </td>
                            </tr>
                        </table>
                    </agiletags:ControllerForm>
                    <br/>
                    <table>
                        <tr><td colspan="4">
                                <label class="subheader">Disclaimer:</label><br/>
                                <p>The settlements of all claims are subject to the validity of membership of the patient to the scheme, as well as limits available.
                                    If tariff codes are specified, the member may be liable for any other tariff codes that may be used.
                                    If an amount is specified the member may be liable for any amount exceeding the specified amount.
                                    Any services rendered outside of the effective period have to be separately motivated for.
                                    All benefits, whether approved or not, will at all times be subject to the rules of the scheme.
                                    All treatments and procedures are funded according to the National Health Reference Price List as was provided by the Council of Medical Schemes
                                </p>
                            </td></tr>
                    </table>
                    <br/>
                    <table align="right">
                        <tr><agiletags:LabelTextDisplay boldDisplay="yes" displayName="Current User" elementName="currentUser" javaScript="" valueFromSession="yes" /></tr>
                    </table>
                    <!-- content ends here -->
                </td></tr></table>
    </body>
</html>

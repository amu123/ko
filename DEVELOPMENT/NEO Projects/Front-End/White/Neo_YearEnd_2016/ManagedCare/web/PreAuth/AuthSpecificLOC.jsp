<%-- 
    Document   : AuthSpecificLOC
    Created on : 2010/06/30, 02:25:26
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/authdatetimepicker_css.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" language="JavaScript">
            
            $(function() {
                toggelCalType();
                var admissionDate = document.getElementById('admissionD').value;

                //set loc from date = session admissionDate
                $("#dcDateFrom").val(admissionDate);
                $("#locDateFrom").val(admissionDate); 
                 
                var saveError = $("#locDateMapError").val();
                if(saveError !== null && saveError !== "" && saveError !== "null"){
                    $("#saveLoc_error").text(saveError);
                }else{
                    $("#saveLoc_error").text("");
                }
                
                //set onclick function 
                /*$("#dcDateFrom").click(function(){
                    $("#dcDateFrom").val("");
                });

                $("#locDateFrom").click(function(){
                    $("#locDateFrom").val("");
                });*/
                
                var admission = $("#locAdmisssion").val();
                if(admission !== null){
                    $("#dcDateFrom").val(admission);
                    $("#locDateFrom").val(admission);
                }
                
                //var inptUsed = 15;
                //var OPDUsed = 21;
                console.log("authType: "+${sessionScope.authType});
                console.log("awTypeNew: "+ '${sessionScope.awTypeNew}');
                
                <c:if test="${sessionScope.authType == '4' && sessionScope.awTypeNew == 'PSY'}">
                    $("#OPDAvailable").val(15);
                    $("#inptAvailable").val(${sessionScope.inptLimitCount});
                    $("#OPDBen").val(0);//value from benefits
                    $("#inpdBen").val(${sessionScope.inptUsedCount});//value from benefits
                    $("#inptUsed").val(${sessionScope.inptUsedCount});
                    $("#OPDUsed").val(0);
                    psychiatricCalc();
                </c:if>    
            });
            
            
            function GetXmlHttpObject(){
                if (window.ActiveXObject){
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest){
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }
            
            function GetDOMParser(xmlStr){
                var xmlDoc;
                if(window.DOMParser){
                    parser=new DOMParser();
                    xmlDoc= parser.parseFromString(xmlStr,"text/xml");
                }else{
                    xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async="false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }            

            function getICD10ForCode(str, element){
                var xhr=GetXmlHttpObject();
                if (xhr===null)
                {
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=GetICD10DetailsByCode&code="+str+"&element="+element;
                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        if(result == "Error"){
                            var error = "#"+element+"_error";
                            $(document).find(error).text("No such diagnosis");
                        }
                    }
                };
                xhr.open("POST",url,true);
                xhr.send(null);
            }

            function setLOCToHospitalDetails(command){
                var xhr=GetXmlHttpObject();
                if (xhr===null)
                {
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController?opperation=SetHospitalLevelOfCare";
                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        var rText = xhr.responseText.split('|');
                        var result = rText[0];
                        if(result == "Error"){
                            var error = "#backButton_error";
                            $(error).text(rText[1]);
                        }else{
                            submitWithAction(command);
                        }
                    }
                };
                xhr.open("POST",url,true);
                xhr.send(null);
            }

            function validateProvider(str, element){
                var xhr = GetXmlHttpObject();
                if (xhr===null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=FindProviderDetailsByCode&provNum="+str+"&element="+element;
                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        var rText = xhr.responseText.split("|");
                        var result = rText[0];
                        // alert(result);
                        if(result == "Error"){
                            var error = "#"+element+"_error";
                            $(document).find(error).text(rText[1]);
                        }
                    }
                };
                xhr.open("POST",url,true);
                xhr.send(null);
            }


            function CalculateLevelOfCare(){
                var calType = $("#calType:checked").val();
                var returnStr = new String;
                var errorFlag = false;
                var url="/ManagedCare/AgileController";

                if(calType == "on"){
                    var addUrl = false;
                    var lcDateFrom = $("#locDateFrom").val();
                    var appLos = $("#appHospLos").val();
                    var los = $("#hospLos").val();
                    returnStr = "#locDateTo";

                    //los validation
                    if(los != null && los != ""){
                        //day case check
                        var loc = $("#hospLoc option:selected").text();
                        if(loc == "Day Case" && los != "1") {
                            errorFlag = true;
                            addUrl = false;
                            $("#hospLos_error").text("Invalid LOS : Requires 1 day");

                        }else{
                            errorFlag = false;
                            addUrl = true;
                            $("#hospLos_error").text("");
                        }
                        $("#dcHospLos_error").text("");
                    }else{
                        errorFlag = true;
                        addUrl = false;
                        $("#hospLos_error").text("Empty LOS Not Allowed");
                    }
                    
                    //app los validation
                    if(appLos != null && appLos != ""){
                        //day case check
                        var loc = $("#hospLoc option:selected").text();
                        if(loc == "Day Case" && los != "1") {
                            errorFlag = true;
                            addUrl = false;
                            $("#appHospLos_error").text("Invalid Approved LOS : Requires 1 day");

                        }else{
                            errorFlag = false;
                            addUrl = true;
                            $("#appHospLos_error").text("");
                        }
                        $("#dcAppLos_error").text("");
                    }else{
                        errorFlag = true;
                        addUrl = false;
                        $("#appHospLos_error").text("Empty Approved LOS Not Allowed");
                    }

                    //from date validation
                    if(lcDateFrom === null || lcDateFrom === ""){
                        errorFlag = true;
                        addUrl = false;
                        $("#lcDateFrom_error").text("Empty From Date Not Allowed");
                    }else{
                        addUrl = true;
                        $("#lcDateFrom_error").text("");
                    }

                    if(addUrl == true){
                        url=url+"?opperation=GetHospitalLengthOfStayDate&dateFrom="+lcDateFrom+"&appHospLos="+appLos+"&hospLos="+los;
                    }
                    
                }else{
                    var dateFrom = $("#dcDateFrom").val();
                    var dateTo = $("#dcDateTo").val();
                    returnStr = "#dcHospLos";
                    var addUrl = false;

                    if(dateFrom == null || dateFrom == ""){
                        errorFlag = true;
                        addUrl = false;
                        $("#dcDateFrom_error").text("Empty From Date Not Allowed");
                    }else{
                        addUrl = true;
                        $("#dcDateFrom_error").text("");
                    }
                    //validate to date
                    if(dateTo == null || dateTo == ""){
                        errorFlag = true;
                        addUrl = false;
                        $("#dcDateTo_error").text("Empty To Date Not Allowed");
                    }else{
                        addUrl = true;
                        $("#dcDateTo_error").text("");
                    }

                    if(addUrl == true){
                        url=url+"?opperation=CalculateLOSFromDates&locFrom="+dateFrom+"&locTo="+dateTo;
                    }

                }
                //add loc type
                var locType = $("#hospLoc").val();
                url=url+"&locType="+locType;
                
                if(errorFlag === false){
                    var xhr=GetXmlHttpObject();
                    if(xhr===null){
                        alert ("Your browser does not support XMLHTTP!");
                        return;
                    }
                    xhr.onreadystatechange=function(){
                        if(xhr.readyState === 4 && xhr.statusText === "OK"){
                            var rText = xhr.responseText.split("|");
                            var result = rText[0];
                            if(result === "Done"){
                                $("#hospLoc_error").text("");
                                $(returnStr).val(rText[1]);
                                $("#estimatedCost").val(rText[2]);
                                <c:if test="${sessionScope.authType == '4' && sessionScope.awTypeNew == 'PSY'}">
                                    psychiatricCalc();
                                </c:if>
                            }else{
                                $("#hospLoc_error").text(result);
                            }
                        }

                    };
                    xhr.open("POST", url, true);
                    xhr.send(null);
                }
                
               
            }

            function ModifyLOCFromList(index){
                var error = "#saveLoc_error";
                var url="/ManagedCare/AgileController";
                var data = {
                    'opperation':'ModifyAuthLOCDetails',
                    'locIndex':index,
                    'id':new Date().getTime()
                }; 
    
                $.get(url, data, function(resultTxt){
                    if (resultTxt!==null) {
                        var resTxtArr = resultTxt.split("|");
                        var result = resTxtArr[0];
                        if(result === "Error"){
                            $(error).text("Level of Care Modification Error Occurred");
                
                        }else if(result === "Done"){
                            //hide row
                            //var hideElement = "#locTagTable tr[id=locRow"+index+"]";
                            //$(hideElement).hide();
                
                            $("#calType").attr('checked', false);
                            toggelCalType();
                            $("#hospLoc").val(resTxtArr[1]);
                            //addDowngradeOptions(rText[1]);
                            //$("#locDown").val(resTxtArr[2]);
                            $("#locDateFrom").val(resTxtArr[2]);
                            $("#dcDateFrom").val(resTxtArr[2]);
                            $("#estimatedCost").val(resTxtArr[3]);
                            $("#dcDateTo").val(resTxtArr[4]);
                            //$("#savingAchieved").val(resTxtArr[5]);                            
                            setLOCDesc();
                            validateADSession(resTxtArr[2], 'locDateFrom');
                            //setLOCDownDesc();                  
                        }        
                    }
                });    
                data = null;
            }

            function RemoveLOCFromList(index){
                var error = "#saveLoc_error";
                submitLocWithAction("RemoveAuthLOCFromList", index);
                
            }

            function setLOCDesc(){
                var locType = $("#hospLoc").val();
                
                if(locType != "99"){
                    var str = $("#hospLoc option:selected").text();
                    $("#locDesc").val(str);
                
                    if(str == "Day Case"){
                        $("#calType").attr('checked', true);
                        toggelCalType();
                        $("#locDateTo").val("");
                        $("#hospLos").val("");
                    }
                    addDowngradeOptions(locType);
                }
            }
            
            function setLOCDownDesc(){
                var downType = $("#locDown").val();
                
                if(downType != "99"){
                    var str = $("#locDown option:selected").text();
                    $("#downDesc").val(str);
                }
            }
            
            function addDowngradeOptions(locType){
                xhr = GetXmlHttpObject();
                if(xhr==null){
                    alert("ERROR: Browser Incompatability");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=AddLOCDowngrades&locType="+locType;
                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        $("#locDown").empty();
                        $("#locDown").append("<option value=\"99\"></option>");
                        var xmlDoc = GetDOMParser(xhr.responseText);
                        //print value from servlet
                        $(xmlDoc).find("DowngradeOptions").each(function (){
                            //set product details
                            $(this).find("Option").each(function (){
                                var prodOpt = $(this).text();
                                var opt = prodOpt.split("|");
                                //Add option to dependant dropdown
                                var optVal = document.createElement('OPTION');
                                optVal.value = opt[0];
                                optVal.text = opt[1];
                                document.getElementById("locDown").options.add(optVal);

                            });
                        });
                    }
                }
                xhr.open("POST",url,true);
                xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                xhr.send(null);

            }            

            function validateDate(dt, element){

                var valid = validateLOCDates(dt);
                var error = "#"+element+"_error";
                if(valid == false){
                    $(error).text("Incorrect Date (YYYY/MM/DD HH:MM:SS)");
                }else{
                    $(error).text("");
                }

            }

            function validateLOCDates (dt) {
                //  check for valid numeric strings
                var strValidDateChars = "0123456789/";
                var strValidTimeChars = "0123456789:";

                var strChar;
                var strChar2;
                var blnResult = true;
                
                var dateTime = new String(dt);
                var dateString = new String(dateTime.substring(0, dateTime.indexOf(" ")));
                var timeString = new String(dateTime.substring(dateTime.indexOf(" ")+1, dateTime.length));

                if (dateString.length < 10 || dateString.length > 10){
                    blnResult = false;
                }
                if (timeString.length < 8 || timeString.length > 8){
                    blnResult = false;
                }

                //test strString consists of valid characters listed above
                for (i = 0; i < dateString.length; i++){
                    strChar = dateString.charAt(i);
                    if (strValidDateChars.indexOf(strChar) == -1){
                        blnResult = false;
                    }
                }

                for (j = 0; j < timeString.length; j++){
                    strChar = timeString.charAt(j);
                    if (strValidTimeChars.indexOf(strChar) == -1){
                        blnResult = false;
                    }
                }

                //test if slash is at correct position (yyyy/mm/dd)
                strChar = dateString.charAt(4);
                strChar2 = dateString.charAt(7);
                if (strChar != '/' || strChar2 != '/'){
                    blnResult = false;
                }

                strChar = timeString.charAt(2);
                strChar2 = timeString.charAt(5);
                if (strChar != ':' || strChar2 != ':') {
                    blnResult = false;
                }
                return blnResult;

            }

            function validateLOCAdd(command){

                var locError = $("#hospLoc_error").text();
                var estCostError = $("#estimatedCost_error").text();
                var savAchError = $("#savingAchieved_error").text();
                //var onScreen = '<%= session.getAttribute("onScreen")%>';
                //alert(onScreen);
                if(locError == "" && estCostError == "" && savAchError == ""){

                    var calType = $("#calType:checked").val();
                    var loc = $("#hospLoc").val();
                    var ec = $("#estimatedCost").val();
                    var sa = $("#savingAchieved").val();
                    var at = '<%= session.getAttribute("authType")%>';
                    var ap = '<%= session.getAttribute("authPeriod")%>';

                    var locDateFrom = new String;
                    var locDateTo = new String;
                    var los = new String;
                    var fe = new String;
                    var te = new String;
                    //error fields
                    var locDateFromError = new String;
                    var locDateToError = new String;
                    var locLOSError = new String;

                    if (calType == "on"){
                        //clear error fields
                        locDateFromError = "#locDateFrom_error";
                        locDateToError = "#locDateTo_error";
                        locLOSError = "#hospLos_error";

                        $(locDateFromError).text("");
                        $(locDateToError).text("");
                        $(locLOSError).text("");

                        locDateFrom = $("#locDateFrom").val();
                        locDateTo = $("#locDateTo").val();
                        los = $("#hospLos").val();

                    }else{
                        //clear error fields
                        locDateFromError = "#dcDateFrom_error";
                        locDateToError = "#dcDateTo_error";
                        locLOSError = "#dcHospLos_error";

                        $(locDateFromError).text("");
                        $(locDateToError).text("");
                        $(locLOSError).text("");

                        locDateFrom = $("#dcDateFrom").val();
                        locDateTo = $("#dcDateTo").val();
                        los = $("#dcHospLos").val();

                    }

                    var valid = true;

                    if(loc == "99"){
                        valid = false;
                        $("#hospLoc_error").text("Level of Care is Mandatory");
                    }
                    
                    /*var updateInd = onScreen.substr(onScreen.indexOf("_", 0), onScreen.length - 4);
                    alert(updateInd);
                    if(updateInd == "Update"){
                        if(loc == "99"){
                            valid = false;
                            $("#hospLoc_error").text("Level of Care Downgrade is Mandatory on Update");
                        }
                    }*/

                    if(locDateFrom == "" || locDateFrom == null){
                        valid = false;
                        $(locDateFromError).text("LOC From Date is Mandatory");
                        $("#locSaveBuuton").hide();
                    }else{
                        valid = true;
                        $("#locSaveBuuton").show();
                    }

                    if(locDateTo == "" || locDateTo == null){
                        valid = false;
                        $(locDateToError).text("LOC To Date is Mandatory");
                        $("#locSaveBuuton").hide();
                    }else{
                        valid = true;
                        $("#locSaveBuuton").show();
                    }

                    if(los == null || los == ""){
                        valid = false;
                        $(locLOSError).text("Length of Stay is Mandatory");
                    }else{
                        $("#hospLos_error").text("");
                        var checkLos = validateLOSDouble(los);
                        if(checkLos == false){
                            $(locLOSError).text("Invalid LOS - "+los);
                        }else{
                            $(locLOSError).text("");
                        }
                    }

                    if (ec != null && ec != ""){
                        var check = validateDouble(ec);
                        if(check == false){
                            valid = false;
                            $("#estimatedCost_error").text("Invalid Amount : "+ec);
                        }else{
                            $("#estimatedCost_error").text("");
                        }
                    }
                    
                    if (sa != null && sa != ""){
                        var check = validateDouble(sa);
                        if(check == false){
                            valid = false;
                            $("#savingAchieved_error").text("Invalid Amount : "+sa);
                        }else{
                            $("#savingAchieved_error").text("");
                        }
                    }
                    
                    var psyCheck = validatePsy();
                    if(psyCheck){
                       valid = true; 
                    }else{
                       valid = false;                         
                    }
                    

                    if(valid == true){
                        submitWithAction(command);
                    }else{
                        $("#locSaveBuuton").hide();
                    }
                }

            }

            function toggelCalType(){
                var str = $("#calType:checked").val();
                if(str == "on"){
                    $("#locCalDateFrom").show();
                    $("#locCalLOS").show();
                    $("#locCalLOS2").show();
                    $("#locCalDateTo").show();
                    $("#dCalDateFrom").hide();
                    $("#dCalDateTo").hide();
                    $("#dCalButton").hide();
                    $("#dCalLOS").hide();
                    $("#dCalLOS2").hide();

                }else{
                    $("#dCalDateFrom").show();
                    $("#dCalDateTo").show();
                    $("#dCalButton").show();
                    $("#dCalLOS").show();
                    $("#dCalLOS2").show();
                    $("#locCalDateFrom").hide();
                    $("#locCalLOS").hide();
                    $("#locCalLOS2").hide();
                    $("#locCalDateTo").hide();
                }
                $("#locCalButton").hide();
                $("#dCalButton").hide();
            }

            function validateLOSDouble(str){
                var strValidChars = "0123456789.";
                var strValidDoub = "05";
                var strChar1;
                var strChar2;
                var strChar3;
                
                var pointCheck = str.indexOf(".");
                if(pointCheck == -1){
                    for (j = 0; j < str.length; j++){
                        strChar3 = str.charAt(j);
                        if (strValidChars.indexOf(strChar3) == -1){
                            return false;
                        }
                    }
                }else{

                    var doub = str.split(".");
                    if(doub.length > 2 || doub.length == 0){
                        return false;

                    }else{

                        var num = doub[0];
                        var point = doub[1];
                        //number check
                        for (j = 0; j < num.length; j++){
                            strChar1 = num.charAt(j);
                            if (strValidChars.indexOf(strChar1) == -1){
                                return false;
                            }
                        }
                        //double check
                        strChar2 = point.charAt(0);
                        if (strValidDoub.indexOf(strChar2) == -1){
                            return false;
                        }

                    }
                }
            }

            function validateDouble(str){
                var strValidChars = "0123456789.";
                var strChar;
                for (j = 0; j < str.length; j++){
                    strChar = str.charAt(j);
                    if (strValidChars.indexOf(strChar) == -1){
                        return false;
                    }
                }
                return true;

            }

            function validateECDoub(str, element){
                var check = validateDouble(str);
                var errorField = "#"+element+"_error";
                
                if(check == false){
                    $(errorField).text("Invalid Amount : "+str);
                    $("#locSaveBuuton").hide();
                }else{
                    $(errorField).text("");
                    $("#locSaveBuuton").show();
                }
            }
            //new date validation
            function validateADSession(date, element){
                var at = '<%= session.getAttribute("authType")%>';
                var ap = '<%= session.getAttribute("authPeriod")%>';
                var amp = '<%= session.getAttribute("authMonthPeriod")%>';
                validatePreAuthDate(date, element, ap, amp, at, "yes");

            }

            function clearElement(element) {
                part = element + '_text';
                document.getElementById(element).value = '';
                document.getElementById(part).value = '';
            }
            function submitLocWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('locIndex').value = index;
                document.forms[0].submit();
            }
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            
            function psychiatricCalc(){
                var OPDAvailable = $("#OPDAvailable").val();
                var inptAvailable = $("#inptAvailable").val();
                var OPDUsed  = $("#OPDUsed").val();
                var v1 = $("#inpdBen").val()*1;
                var v2 = $("#dcHospLos").val()*1;
                var inptUsed = v1 + v2;
                var proOPD;
                var proInpt;
                var totPro;
                var OPD2Available;
                var inpt2Available;

                proOPD = OPDUsed/OPDAvailable*100;                
                proInpt = inptUsed/inptAvailable*100;
                totPro = (100-(proOPD + proInpt));
                OPD2Available = 15 * totPro / 100;                    
                inpt2Available = 21 * totPro / 100;                    
                proOPD = parseFloat(proOPD).toFixed(2);
                proInpt = parseFloat(proInpt).toFixed(2);
                totPro = parseFloat(totPro).toFixed(2);
                
                if((OPD2Available%1) == 0.5){
                   //display halfday
                   console.log("halfday");
                }else{
                    //remove remainder
                    OPD2Available = Math.floor(OPD2Available);
                }
                if((inpt2Available%1) == 0.5){
                   //display halfday
                }else{
                    //remove remainder
                    inpt2Available = Math.floor(inpt2Available);
                }
                
                $("#inptUsed").val(inptUsed);
                $("#OPDUsed").val(OPDUsed);
                $("#proOPD").val(proOPD);
                $("#proInpt").val(proInpt);
                $("#totPro").val(totPro);
                $("#OPD2Available").val(OPD2Available);
                $("#inpt2Available").val(inpt2Available);
                
                validatePsy();
                
            }
            
            function validatePsy(){
                var valid = false;
//                var validOPD = false;
                var validInpt = false;
                //15 days out of hospital temp removed
//                if($("#OPD2Available").val() <= 0){
//                    $("#OPD2Available_error").text("No more OPD visits avaiable");
//                    validOPD = false;
//                }else{
//                    $("#OPD2Available_error").text("");
//                    validOPD = true;
//                }
                
                if($("#inpt2Available").val() < 0){
                    $("#inpt2Available_error").text("No more in-pt days avaiable");
                    validInpt = false;
                }else{
                    $("#inpt2Available_error").text("");
                    validInpt = true;
                }
                
                if(validInpt){//validOPD && 
                    valid = true;
                    $("#locSaveBuuton").show();
                }else{
                    valid = false;
                }
                
                return valid;
            }

        </script>
    </head>
    <body onload="toggelCalType();">
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Hospital Level Of Care Details</label>
                    <br/><br/>
                    <table>
                        <agiletags:ControllerForm name="LOCDetails" validate="yes">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="locDesc" id="locDesc" value="" />
                            <input type="hidden" name="downDesc" id="downDesc" value="" />
                            <input type="hidden" name="locIndex" id="locIndex" value="" />
                            <input type="hidden" name="admissionD" id="admissionD" value="${sessionScope.admissionDateTime}" />
                            <input type="hidden" name="inpdBen" id="inpdBen" value="" />
                            <input type="hidden" name="OPDBen" id="OPDBen" value="" />
                            
                            <tr><agiletags:AuthLOCDropDown displayName="Level of Care" elementName="hospLoc" mandatory="yes" javaScript="onblur=\"setLOCDesc();\"" errorValueFromSession="yes"/></tr>
                            
                            <tr><td><label class="label">Calculate by LOS</label></td><td><input type="checkbox" id="calType" name="calType" onclick="toggelCalType(this.value);" /></td></tr>
                            <tr id="locCalDateFrom"><agiletags:LabelTextBoxDateTime displayName="Date From" elementName="locDateFrom" valueFromSession="yes" mandatory="yes" javaScript="onblur=\"validateADSession(this.value, this.id);\""/></tr>
                            <tr id="locCalLOS"><agiletags:LabelTextBoxError displayName="Length Of Stay" elementName="hospLos" valueFromSession="yes" mandatory="yes" javaScript="onblur=\"validateLOSDouble(this.value);\""/></tr>
                            <tr id="locCalLOS2"><agiletags:LabelTextBoxError displayName="Approved Length Of Stay" elementName="appHospLos" valueFromSession="yes" mandatory="yes" javaScript="onblur=\"validateLOSDouble(this.value);\""/></tr>
                            <tr id="locCalButton"><agiletags:ButtonOpperation displayname="Generate To Date" span="1" align="left" commandName="" type="button" javaScript="onclick=\"CalculateLevelOfCare();\"" /></tr>
                            <tr id="locCalDateTo"><agiletags:LabelTextBoxError displayName="Date To" elementName="locDateTo" valueFromSession="yes" readonly="yes" mandatory="no" javaScript=""/></tr>

                            <tr id="dCalDateFrom"><agiletags:LabelTextBoxDateTime displayName="Date From" elementName="dcDateFrom" valueFromSession="yes" mandatory="yes" javaScript="onblur=\"validateADSession(this.value, this.id);\""/></tr>
                            <tr id="dCalDateTo"><agiletags:LabelTextBoxDateTime displayName="Date To" elementName="dcDateTo" valueFromSession="yes" mandatory="yes" javaScript="onblur=\"validateADSession(this.value, this.id);\""/></tr>
                            <tr id="dCalButton"><agiletags:ButtonOpperation displayname="Calculate LOS" span="1" align="left" commandName="" type="button" javaScript="onclick=\"CalculateLevelOfCare();\""/></tr>
                            <tr id="dCalLOS"><agiletags:LabelTextBoxError displayName="Length Of Stay" elementName="dcHospLos" valueFromSession="yes" mandatory="yes" readonly="yes" javaScript=""/></tr>
                            <tr id="dCalLOS2"><agiletags:LabelTextBoxError displayName="Approved Length Of Stay" elementName="dcAppLos" valueFromSession="yes" mandatory="yes" javaScript="onblur=\"validateLOSDouble(this.value);\""/></tr>

                            <tr><agiletags:LabelTextBoxError displayName="Estimated Cost" elementName="estimatedCost" valueFromSession="yes" mandatory="no" javaScript="onblur=\"validateECDoub(this.value, 'estimatedCost');\""/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Saving Achieved" elementName="savingAchieved" valueFromSession="yes" mandatory="no" javaScript="onblur=\"validateECDoub(this.value, 'savingAchieved');\""/></tr>
                            <br/>
                                
                            <c:if test="${sessionScope.authType == '4' && sessionScope.awTypeNew == 'PSY'}">
                                <tr><agiletags:LabelTextDisplay displayName="Psychiatric Benefit Utilisation Calculation" elementName="psychiatricLable"  boldDisplay="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError readonly="yes" displayName="No of OPD Visits(PMB) available" elementName="OPDAvailable" valueFromSession="yes" /></tr>
                                <tr><agiletags:LabelTextBoxError readonly="yes" displayName="No of In-pt Days(PMB) available" elementName="inptAvailable" valueFromSession="yes" /></tr>
                                <tr><agiletags:LabelTextBoxError readonly="no" displayName="No of OPD Visits(PMB) Used" elementName="OPDUsed" valueFromSession="yes" javaScript="onblur=\"psychiatricCalc()\"" /></tr>
                                <tr><agiletags:LabelTextBoxError readonly="no" displayName="No of In-pt Days(PMB) Used" elementName="inptUsed" valueFromSession="yes" javaScript="onblur=\"psychiatricCalc()\"" /></tr>
                                <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Proportion OPD Visits Used(%)" elementName="proOPD" valueFromSession="yes" /></tr>
                                <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Proportion In-pt Days Used(%)" elementName="proInpt" valueFromSession="yes" /></tr>
                                <tr><agiletags:LabelTextBoxError readonly="yes" displayName="Total Proportion Available(%)" elementName="totPro" valueFromSession="yes" /></tr>
                                <tr><agiletags:LabelTextBoxError readonly="yes" displayName="No of OPD Visits Available" elementName="OPD2Available" valueFromSession="yes" /></tr>
                                <tr><agiletags:LabelTextBoxError readonly="yes" displayName="No of In-pt Days Available" elementName="inpt2Available" valueFromSession="yes" /></tr>
                            </c:if>  
                            
                            <tr id="locSaveBuuton"><agiletags:ButtonOpperation align="right" commandName="" displayname="Save" span="2  " type="button" javaScript="onClick=\"validateLOCAdd('AddAuthLOSToSessionCommand');\"" /></tr>
                            
                           
                        </agiletags:ControllerForm>
                    </table>
                    <br/>
                    <table>
                        <tr><agiletags:AuthLevelOfCareListTableTag commandName="" sessionAttribute="AuthLocList" javaScript="" /></tr>
                        <tr><agiletags:ButtonOpperationLabelError commandName="" displayName="Continue" type="button" elementName="backButton" javaScript="onClick=\"setLOCToHospitalDetails('ReturnToHospitalAuth')\";" /></tr>
                        <tr><td colspan="4">
                                <label class="subheader">Notice:</label><br/>
                                <p>Hospital Authorisations Require A Minimum of One Level Of Care</p>
                            </td></tr>
                    </table>
                </td></tr></table>
    </body>
</html>

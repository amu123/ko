<%-- 
    Document   : AuthSpecificCPT
    Created on : 2010/06/14, 11:13:29
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <!--<script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.scrollTo-1.4.2-min.js"></script>-->
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        
        <script language="JavaScript">

            function CheckSearchCommands(){
                //reload ajax commands for screen population
                var searchVal = document.getElementById('searchCalled').value;
                if (searchVal == 'cptCode'){
                    getCPTByCode(document.getElementById('cptCode_text').value, 'cptCode');

                }else{
                    $("#cptCode_text").val("");
                    $("#cptCodeDesc").text("");
                }
                document.getElementById('searchCalled').value = '';
            }

            function GetXmlHttpObject(){
                if (window.ActiveXObject){
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest){
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function getCPTByCode(str, element){
                xhr=GetXmlHttpObject();
                if(xhr==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                if (str.length > 0) {
                    var url="/ManagedCare/AgileController";
                    url=url+"?opperation=GetCPTByCodeCommand&code="+str;

                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            var rText = xhr.responseText.split("|");
                            var response = rText[0];
                            if(response == "OK"){
                                var codeDesc = rText[2];
                                $("#"+element+"Desc").text(codeDesc);
                                var error = "#"+element+"_error";
                                $(document).find(error).text("");

                            }else{
                                var error = "#"+element+"_error";
                                $(document).find(error).text(rText[1]);
                            }
                        }
                    };
                    xhr.open("POST", url, true);
                    xhr.send(null);
                } else {
                    $("#"+element+"Desc").text("");
                }
            }

            function validateProvider(str, element){
                xhr = GetXmlHttpObject();
                if (xhr==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=FindProviderDetailsByCode&provNum="+str+"&element="+element;
                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        var rText = xhr.responseText.split("|");
                        var result = rText[0];
                        if(result == "Error"){
                            var error = "#"+element+"_error";
                            $(document).find(error).text(rText[1]);
                        }else{
                            var ProvNameSur = rText[1].split("=");
                            var nameSur = ProvNameSur[1];
                            $("#"+element+"NameSur").text(nameSur);
                        }
                    }
                };
                xhr.open("POST",url,true);
                xhr.send(null);
            }

            function saveCptToList(){
                xhr = GetXmlHttpObject();
                if (xhr==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController?opperation=AddAuthCPTToSessionCommand";

                var cptCode = $("#cptCode_text").val();
                var cptCodeDesc = $("#cptCodeDesc").text();
                var tTime = $("#theatreTime").val();
                var procLink = $("#procLink").val();

                url=url+"&cptCode="+cptCode+"&cptDesc="+cptCodeDesc+
                    "&tTime="+tTime+"&procLink="+procLink;

                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        var rText = xhr.responseText.split("|");
                        var result = rText[0];
                        if(result == "Error"){
                            var error = "#"+element+"_error";
                            $(document).find(error).text(rText[1]);
                        }
                    }
                };
                xhr.open("POST",url,true);
                xhr.send(null);
            }

            function ModifyCPTList(index){
                var x = document.forms[index];
                var cptDesc = new String();
                var cpt = new String();

                for (var i=0; i<x.length; i++) {
                    if (x.elements[i].id == "cptCode"){
                        cpt = x.elements[i].value;
                    }
                    if (x.elements[i].id == "cptDesc"){
                        cptDesc = x.elements[i].value;
                    }
                }

                var selected = $("tr[id=cptRow"+cpt+index+"]");
                for(var j = 0; j < selected.size(); j++){
                    $(selected[j]).hide();
                }

                xhr=GetXmlHttpObject();
                if(xhr==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=ModifyAuthCPTCommand&cpt="+cpt+"&desc="+cptDesc;

                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        var rText = xhr.responseText.split("|");
                        var result = rText[0];
                        if(result == "Done"){
                            $("#cptCode_text").val(rText[1]);
                            $("#theatreTime").val(rText[2]);
                            $("#procLink").val(rText[3]);
                            $("#cptCodeDesc").text(rText[4]);

                        }
                    }

                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function RemoveCPTFromList(index){
                var x = document.forms[index];
                var cpt = new String();

                for (var i=0; i<x.length; i++)
                {
                    if (x.elements[i].id == "cptCode")
                        cpt = x.elements[i].value;

                }

                xhr=GetXmlHttpObject();
                if(xhr==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=RemoveAuthCPTFromList&cptCode="+cpt;
                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        var rText = xhr.responseText.split("|");
                        var result = rText[0];
                        if(result == "OK"){
                            var selected = $("tr[id=cptRow"+rText[1]+index+"]");
                            for(var j = 0; j < selected.size(); j++){
                                $(selected[j]).hide();
                            }
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function validateCPTAdd(command){
            
                var tError = $("#theatreTime_error").text();
                var cptError = $("#cptCode_error").text();
            
                if(tError == "" && cptError == ""){
                    ClearValidation();

                    var cptCode = $("#cptCode_text").val();
                    var tTime = $("#theatreTime").val();
                    var valid = true;

                    if(cptCode == "" || cptCode == null){
                        valid = false;
                        $("#cptCode_error").text("Cpt code is mandatory");
                    }
            
                    if(tTime != ""){
                        var tCheck = validateTheatreTime(tTime);
                        if(tCheck == false){
                            valid = false;
                            $("#theatreTime_error").text("Please enter the duration");
                        }
                    }                
                    if(valid == true){
                        submitWithAction(command,'','');
                    }                
                
                }           

            }
        
            function ClearValidation(){
                $("#theatreTime_error").text("");
                $("#cptCode_error").text("");
            }

            function validateTheatreTime(str){
                var strValidChars = "0123456789";
                var strChar;

                for (j = 0; j < str.length; j++){
                    strChar = str.charAt(j);
                    if (strValidChars.indexOf(strChar) == -1){
                        return false;
                    }
                }
                return true;
            }

            function ClearValidation(){
                $("#cptCode_error").text("");
                $("#theatreTime_error").text("");
            }

            function clearElement(element) {
                part = element + '_text';
                document.getElementById(element).value = '';
                document.getElementById(part).value = '';
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById('opperation').value = action;
                document.getElementById('searchCalled').value = forField;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <label class="header">Authorisation Specific CPT Details</label>
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="saveAuthTariff" validate="yes">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <tr><agiletags:LabelTextSearchText displayName="CPT Code" elementName="cptCode" valueFromSession="yes" searchFunction="yes" searchOperation="ForwardToCPTSearch" onScreen="/PreAuth/AuthSpecificCPT.jsp" mandatory="yes" javaScript="onChange=\"getCPTByCode(this.value, 'cptCode');\"" /></tr>
                            <tr><agiletags:LabelTextDisplay displayName="CPT Description" elementName="cptCodeDesc" valueFromSession="yes" javaScript=""/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Theatre Time" elementName="theatreTime" valueFromSession="yes" /></tr>
                            <tr><agiletags:LabelTextBoxDateTime displayName="Theatre Date" elementName="theatreDateTime" valueFromSession="yes" mandatory="no" /></tr>
                            <tr><agiletags:LabelNeoLookupValueDropDown displayName="Procedure Link" elementName="procLink" lookupId="126" /></tr>
                            <tr><agiletags:ButtonOpperation align="right" commandName="" displayname="Save" span="2" type="button" javaScript="onClick=\"validateCPTAdd('AddAuthCPTToSessionCommand');\"" /></tr>
                        </agiletags:ControllerForm>
                        <br/>
                        <tr><agiletags:AuthCPTListTabelTag commandName="" javaScript="" sessionAttribute="authCPTListDetails" /></tr>
                        <tr><agiletags:ButtonOpperation align="right" commandName="" displayname="Continue" span="2" type="button" javaScript="onClick=\"submitWithAction('CalculateCoPaymentAmounts','','');\"" /></tr>
                    </table>
                </td></tr></table>
    </body>
</html>

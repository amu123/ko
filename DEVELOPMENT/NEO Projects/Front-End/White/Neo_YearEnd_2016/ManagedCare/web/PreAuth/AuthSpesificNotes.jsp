<%-- 
    Document   : AuthSpesificNotes
    Created on : Sep 27, 2010, 12:02:23 PM
    Author     : Johan-NB
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" language="JavaScript">

            function CheckSearchCommands(){
                //reload ajax commands for screen population
                var searchVal = document.getElementById('searchCalled').value;

            }

            function GetXmlHttpObject(){
                if (window.ActiveXObject){
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest){
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function clearElement(element) {
                part = element + '_text';
                document.getElementById(element).value = '';
                document.getElementById(part).value = '';
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById('opperation').value = action;
                document.getElementById('searchCalled').value = forField;
                document.forms[0].submit();
            }


        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <label class="header">Authorisation Specific Notes</label>
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="saveAuthNotes" validate="yes">
                            <input type="hidden" name="opperation" id="opperation" value="" /> 
                            <agiletags:HiddenField elementName="searchCalled" />
                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Note Type" elementName="noteType" mandatory="yes" lookupId="137" /></tr>
                            <tr><agiletags:LabelTextAreaErrorBig valueFromSession="Yes" displayName="Notes" elementName="notes" /></tr>
                            <tr><agiletags:ButtonOpperationLabelError elementName="saveNoteButton" commandName="" displayName="Save" type="button" javaScript="onClick=\"submitWithAction('AddAuthNoteToList','','');\"" /></tr>
                        </agiletags:ControllerForm>
                        <br/>
                        <tr><agiletags:AuthNoteListDisplay commandName="" javaScript="" sessionAttribute="authNoteList" /></tr>
                        <tr><agiletags:ButtonOpperation align="right" commandName="" displayname="Continue" span="2" type="button" javaScript="onClick=\"submitWithAction('ReturnToHospitalAuth','','');\"" /></tr>
                    </table>
                </td></tr></table>
    </body>
</html>

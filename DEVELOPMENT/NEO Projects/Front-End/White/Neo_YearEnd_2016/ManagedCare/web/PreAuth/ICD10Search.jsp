<%-- 
    Document   : ICD10Search
    Created on : 25 May 2015, 3:08:00 PM
    Author     : martins
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.scrollTo-1.4.2-min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script language="JavaScript">
            
            function GetXmlHttpObject() {
                if (window.ActiveXObject) {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }      
            
            function submitWithAction(action) {
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
            }
            
            function submitWithAction(action, forField, onScreen) {
                document.getElementById('opperation').value = action;
                document.getElementById('searchCalled').value = forField;
                document.forms[0].submit();
            }
            
            
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
    <!-- content goes here -->
    <label class="header">Search ICD10</label>
    </br></br>
    <agiletags:ControllerForm name="searchICD">
        <input type="hidden" name="selected" id="selected" value="" />
        <input type="hidden" name="onScreen" id="onScreen" value="" />
        <table>
            <tr><agiletags:LabelTextBoxError displayName="Code" elementName="code"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Description" elementName="description"/></tr>
            <tr><agiletags:ButtonOpperation type="submit" align="left" span="3" commandName="SearchPreAthICD10Command" displayname="Search"/></tr>
        
        </table>
    </agiletags:ControllerForm>
    </br>
    <HR color="#666666" WIDTH="50%" align="left">
    <agiletags:ICD10SearchResultTable commandName="AllocatePreAuthICD10ToSessionCommand"/>
    <!-- content ends here -->
</td></tr></table>
</body>
</html>
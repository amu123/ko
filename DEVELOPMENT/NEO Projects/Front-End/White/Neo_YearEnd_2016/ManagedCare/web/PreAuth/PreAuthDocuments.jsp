<%-- 
    Document   : PreAuthDocuments
    Created on : Sep 3, 2016, 1:32:16 PM
    Author     : janf
--%>

<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags"%>
<link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
<link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>
<script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
<script type='text/javascript' src="${pageContext.request.contextPath}/resources/Membership/MemberMaintenance.js"></script>
<script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
<script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
<label class="header">Preauth Documents</label>
<hr>
<iframe name="uploadFrame" id="uploadFrame" style="display: none"></iframe>    
<form id="uploadForm" enctype="multipart/form-data" target="uploadFrame" method="POST" action="/ManagedCare/AgileController?opperation=MemberDocumentUploadCommand&authNumber=${authNumber}&entityType=7">
    <input type="hidden" name="docuType" id="docuType" value="34" />
</form>
<agiletags:MultipartControllerForm name="documentSorting" action="/ManagedCare/AgileController?opperation=PreAuthDocumentUploadCommand&authNumber=${authNumber}&entityType=7&authId=${authCode}&returnFlag=${returnFlag}&docType=34">
    <input type="hidden" name="authNumber" id="authNumber" value="${authNumber}" />
    <input type="hidden" name="docuType" id="docuType" value="34" />
    <table>
        <c:if test="${applicationScope.Client == 'Sechaba'}">
            <tr id="documentUpload">
                <td><label>Document</label></td>
                <td><input type="file" name="uploadDoc" id="uploadDoc" size="75" /></td>
                <td><input type="submit" id="submitFile" value="Upload Document" onclick="document.getElementById('submitFile').disabled;"/></td>
            </tr>
        </c:if>
    </table>  
</agiletags:MultipartControllerForm>
<label class="subheader">PreAuth Documents</label>    
<div id="PreAuthDocumentDetails">
    <c:choose>
        <c:when test="${empty indexList}">
            <span class="label">No Document found</span>
        </c:when>
        <c:otherwise>
            <label id="DocView_Msg" class="label"></label>
            <agiletags:ControllerForm name="documentIndexing">
                <input type="hidden" name="folderList" id="folderList" value="" />
                <input type="hidden" name="fileLocation" id="fileLocation" value="" />
                <input type="hidden" name="opperation" id="opperation" value="MemberDocumentDisplayViewCommand" />
                <input type="hidden" name="searchCalled" id="searchCalled" value=""/>
                <input type="hidden" name="onScreen" id="onScreen" value="" />
                <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                <input type="hidden" name="entityType" id="entityType" value="" />
                <input type="hidden" name="docType" id="docType" value="" />
                <input type="hidden" name="entityNumber" id="entityNumber" value="">
                <input type="hidden" name="refNumber" id="refNumber" value="">
                <input type="hidden" name="indexID" id="indexID" value="">
                <c:set var="Email" value="${requestScope.ContactDetails_email}"/>
                <input type="hidden" name="emailAddress" id="emailAddress" value="${Email}">

                <br/>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Auth Number</th>
                        <th>Type</th>
                            <c:if test="${requestScope.docKind == '212' }">
                            <th>Reference Number</th>
                            </c:if>
                        <th>Location</th>
                        <th>Current Date</th>  
                        <th></th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${indexList}">
                        <tr class="label">
                            <td>${entry['entityNumber']}</td>
                            <td>${entry['docType']}</td>
                            <c:if test="${requestScope.docKind == '212'}">
                                <td>${entry['reference']}</td>
                            </c:if>
                            <td>${entry['location']}</td>
                            <td>${agiletags:formatXMLGregorianDate(entry.generationDate)}</td>
                            <td><input id="viewB" type="submit" value="View"  onClick="submitActionAndViewFile(this.form, 'MemberDocumentDisplayViewCommand', '${entry['location']}');" /></td>
                            <td><input id="printB" type="button" value="Print" onClick="submitActionAndFile(this.form, 'AddToPrintPoolCommand', '${entry['indexId']}'); submitFormWithAjaxPost(this.form, 'MemberDocuments');" style="display: none"/></td>
                        </tr>
                    </c:forEach>
                </table>
            </agiletags:ControllerForm>
        </c:otherwise>
    </c:choose>
</div>


<agiletags:ControllerForm name="ReturnToAuthConfForm">
    <input type="hidden" id="authNumber" name="authNumber" value="${authNumber}" />
    <input type="hidden" id="authCode" name="authCode" value="${authCode}" />
    <input type="hidden" id="authId" name="authId" value="${authCode}" />
    <input type="hidden" id="returnFlag" name="returnFlag" value="${returnFlag}" />
    <c:if test="${sessionScope.docReturnFlag == 'Reject'}">
        <button name="opperation" type="submit" onClick="submitWithActionAndDocType(this.form, 'ForwardToPreAuthRejectedConfirmation', '34');" value="ForwardToPreAuthRejectedConfirmation">Return</button>
    </c:if>
    <c:if test="${sessionScope.docReturnFlag == 'Confirmation'}">
        <button name="opperation" type="submit" onClick="submitWithActionAndDocType(this.form, 'ForwardToPreAuthConfirmation', '34');" value="ForwardToPreAuthConfirmation">Return</button>
    </c:if>
</agiletags:ControllerForm>

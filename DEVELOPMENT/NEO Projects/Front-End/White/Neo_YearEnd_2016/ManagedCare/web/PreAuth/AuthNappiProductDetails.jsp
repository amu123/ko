<%-- 
    Document   : AuthNappiProductDetails
    Created on : 14 May 2015, 10:47:56 AM
    Author     : martins
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="neo.manager.NeoUser" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.scrollTo-1.4.2-min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script language="JavaScript">
            //set current user
            <%
                NeoUser user = (NeoUser) session.getAttribute("persist_user");
                session.setAttribute("currentUser", user.getName() + " " + user.getSurname());
            %>
            //var isFacilityMandatory = false;

            $(function() {
                CheckStat();
                CalcPrice();
                $('#price').attr('readonly', true);
            <c:if test="${sessionScope.persist_user_teamLead == 'true' || sessionScope.persist_user_sup == 'true'}">
                $('#price').attr('readonly', false);
            </c:if>

                    });
                    
                    function GetXmlHttpObject() {
                        if (window.ActiveXObject) {
                            // code for IE6, IE5
                            return new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        if (window.XMLHttpRequest) {
                            // code for IE7+, Firefox, Chrome, Opera, Safari
                            return new XMLHttpRequest();
                        }
                        return null;
                    }

                    function CalcPrice() {      
                        <c:if test="${sessionScope.unitPrice != null}">
                                    $("#unitPrice").val(${sessionScope.unitPrice});
                                    $("#fixPrice").val(${sessionScope.fixPrice});
                                    var n = $("#quantity").val() * $("#unitPrice").val();
                                    $("#price").val(n.toFixed(2));
                        </c:if>
                    }

                function CheckStat() {
                    var stat = $("#status").val();
                    if (stat === "2" || stat === "Reject") {
                        $("#reject").show();
                    } else {
                        $("#reject").hide();
                        $("#rejectionReason").val(99);
                    }
                }

                function setICDToUpper() {
                    var text = $("#icd_text").val();
                    var icd = $.trim(text);
                    var tc = icd.charAt(0).toUpperCase();
                    var sub = tc + icd.substring(1, icd.length);
                    $("#icd_text").val(sub);

                    getICD10ForCode(sub, "icd");
                }

                function getICD10ForCode(str, element) {
                    xhr = GetXmlHttpObject();
                    if (xhr == null)
                    {
                        alert("Your browser does not support XMLHTTP!");
                        return;
                    }
                    var url = "/ManagedCare/AgileController";
                    url = url + "?opperation=GetICD10DetailsByCode&code=" + str + "&element=" + element;
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState == 4 && xhr.statusText == "OK") {
                            var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                            var error = "#" + element + "_error";
                            var jqElement = "#" + element + "_text";
                            var descr = "#" + element + "Description";
                            $(error).text("");
                            if (result == "Error") {
                                $(error).text("No such diagnosis");
                            }
                        }
                    };

                    xhr.open("POST", url, true);
                    xhr.send(null);
                }

                function clearElement(element) {
                    part = element + '_text';
                    document.getElementById(element).value = '';
                    document.getElementById(part).value = '';
                }

                function RemoveProductNappiFromList(arrayNum) {
                    $("#remove").val(arrayNum);
                    submitWithAction("RemoveNappiProdFromListCommand");
                }

                function ModProductNappiList(arrayNum) {
                    $("#mod").val(arrayNum);
                    submitWithAction("ModNappiProdListCommand");
                }

                function getProductCode() {
                    $("#productCode").val();
                    submitWithAction("GetNappiProductFromCode");
                }

                function submitWithAction(action) {
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Authorisation  Specific Product Detail</label>
                    <br/><br/>
                    <agiletags:ControllerForm name="ProductDetails" validate="yes">
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <agiletags:HiddenField elementName="searchCalled" />
                        <input type="hidden" name="remove" id="remove" value="" />
                        <input type="hidden" name="mod" id="mod" value="" />
                        <input type="hidden" name="fixPrice" id="fixPrice" value="" />
                        <input type="hidden" name="unitPrice" id="unitPrice" value="" />
                        <table>
                            <tr><td><agiletags:LabelTextSearchText displayName="Product Code" elementName="productCode" valueFromSession="yes" onScreen="/PreAuth/AuthNappiProductDetails.jsp" searchFunction="yes"  searchOperation="ForwardToSearchProduct" javaScript="onBlur=\"getProductCode()\""/></td></tr>
                            <tr><td><agiletags:LabelTextDisplay displayName="Product Description" elementName="prodDesc" valueFromSession="yes" javaScript="" /></td></tr>
                            <tr><td><agiletags:LabelTextBoxError displayName="Quantity" elementName="quantity" valueFromSession="yes" javaScript="onBlur=\"CalcPrice()\";" /></td></tr>    
                            <tr><td><agiletags:LabelTextBoxError displayName="Price" elementName="price" valueFromSession="yes" javaScript="onBlur=\CalcPrice()\";"  /></td></tr>    
                            <tr><td><agiletags:LabelTextSearchText displayName="ICD" elementName="icd" valueFromSession="yes" onScreen="/PreAuth/AuthNappiProductDetails.jsp" searchFunction="yes" searchOperation="ForwardToICD10Search" javaScript="onBlur=\"setICDToUpper()\";"/></td></tr>
                            <tr><td><agiletags:LabelTextDisplay displayName="ICD Description" elementName="icdDescription" valueFromSession="yes" /></td></tr>
                            <tr><td><agiletags:LabelNeoLookupValueDropDownError displayName="Status" elementName="status" lookupId="111" errorValueFromSession="yes" javaScript="onBlur=\"CheckStat()\";" /></td></tr>       
                            <tr id="reject"><td><agiletags:LabelNeoLookupValueDropDownError displayName="Rejection Reason" elementName="rejectionReason" lookupId="84" /></td></tr>    
                            <tr><td><agiletags:LabelTextBoxDateTime displayName="Date From" elementName="dateFrom" valueFromSession="yes"/></td></tr>
                            <tr><td><agiletags:LabelTextBoxDateTime displayName="Date To" elementName="dateTo"  valueFromSession="yes"/></td></tr>
                            <tr><td><agiletags:ButtonOpperation commandName="saveProduct" align="" displayname="Save" span="3" type="button"  javaScript="onClick=\"submitWithAction('SaveProductCommand')\";"/></td></tr>
                            <tr><td><agiletags:AuthProductTableTag commandName="" sessionAttribute="authProductTableTag" javaScript=""/></td></tr>
                            <tr><td><tr><td><agiletags:ButtonOpperation commandName="continueProduct" align="" displayname="Continue" span="3" type="button"  javaScript="onClick=\"submitWithAction('ForwardToAuthHospDetailsCommand')\";"/></td></tr>
                        </table>
                    </agiletags:ControllerForm>
                    <br/>
                    <table align="right">
                        <tr><agiletags:LabelTextDisplay boldDisplay="yes" displayName="Current User" elementName="currentUser" javaScript="" valueFromSession="yes" /></tr>
                    </table>
                </td></tr></table>
    </body>
</html>

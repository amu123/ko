<%-- 
    Document   : NappiAuthSpecific
    Created on : Dec 24, 2010, 11:15:40 AM
    Author     : Johan-NB
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" language="JavaScript">

            function GetXmlHttpObject(){
                if (window.ActiveXObject){
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest){
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function getNappiForCode(){
                $("#napCode_error").text("");
                var str = $("#napCode_text").val();
                xhr=GetXmlHttpObject();
                if(xhr==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=GetAuthNappiByCodeCommand&napCode="+str;

                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        var resText = xhr.responseText;
                        var strArr = new Array();
                        strArr = resText.split('|');
                        var msg = strArr[0];
                        if(msg == "Done"){
                            var tCode = new String();
                            tCode = strArr[1];
                            var tDesc = new String();
                            tDesc = strArr[2];

                            $("#napDesc").text(tDesc);
                            $("#napCode_text").val(tCode);

                        }else if(msg == "Error"){
                            var tDesc = new String();
                            tDesc = strArr[1];
                            $("#napCode_error").text(tDesc);
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

    
            function ModifyNewNappiFromList(index){
                var x = document.forms[index];
                var nDesc = new String();
                var nCode = new String();
                for (var i=0; i<x.length; i++)
                {
                    if (x.elements[i].id == "nTableDesc")
                        nDesc = x.elements[i].value;

                    if (x.elements[i].id == "nTableCode")
                        nCode = x.elements[i].value;
                }

                var selected = $("tr[id=newNappiRow"+nCode+index+"]");
                for(var j = 0; j < selected.size(); j++){
                    $(selected[j]).hide();
                }

                xmlhttp=GetXmlHttpObject();
                if(xmlhttp==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=ModifyAuthNewNappiCommand&nTableDesc="+nDesc+"&nTableCode="+nCode;
                xmlhttp.onreadystatechange=function(){stateChanged("")};
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }
            
            function RemoveNewNappiFromList(index){
                var x = document.forms[index];
                var nDesc = new String();
                var nCode = new String();
                for (var i=0; i<x.length; i++)
                {
                    if (x.elements[i].id == "nTableDesc")
                        nDesc = x.elements[i].value;

                    if (x.elements[i].id == "nTableCode")
                        nCode = x.elements[i].value;
                }

                var selected = $("tr[id=newNappiRow"+nCode+index+"]");
                for(var j = 0; j < selected.size(); j++){
                    $(selected[j]).hide();
                }

                xhr=GetXmlHttpObject();
                if(xhr==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }

                var url="/ManagedCare/AgileController";
                url=url+"?opperation=RemoveAuthNewNappiCommand&nTableDesc="+nDesc+"&nTableCode="+nCode;
                xhr.onreadystatechange=function(){};
                xhr.open("POST", url, true);
                xhr.send(null);
            }
            function ClearTariffFieldsFromSession(){
                xmlhttp=GetXmlHttpObject();
                xmlhttp.overrideMimeType('text/xml');
                if(xmlhttp==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }

                var url="/ManagedCare/AgileController";
                url=url+"?opperation=ClearAuthTariffCommand";
                xmlhttp.onreadystatechange=function(){stateChanged("clearTariffFields")};
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function ValidateTariffAmount(str){
                var check1 = /^[-+]?\d{1,9}(\.\d{1,2})?$/;
                var numCheck = check1.test(str);
                if(numCheck == false){
                    $("#tariffAmount_error").text("Invalid tariff amount - "+str);
                }else{
                    $("#tariffAmount_error").text("");
                }
            }

            function stateChanged(element){

                if (xmlhttp.readyState==4)
                {
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    if(result == "GetAuthTariffByCodeCommand"){

                    }else if(result == "ModifyAuthNewNappiCommand"){
                        var resText = xmlhttp.responseText;
                        var strArr = new Array();
                        strArr = resText.split('|');
                        var tCode = new String();
                        tCode = strArr[1];
                        var tDesc = new String();
                        tDesc = strArr[2];

                        $("#napDesc").text(tDesc);
                        $("#napCode_text").val(tCode);

                    }else if(result == "RemoveNewNappiFromList"){

                        $.find('#tariffRow').hide().slideUp('slow').fadeOut();
                        $.remove('#tariffRow');

                    }else if(result == "ClearTariffFieldsFromSession"){
                        $("#napDesc").text('');
                        $("#napCode_text").val('');

                    }else if(result == "Error"){
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|')+1, xmlhttp.responseText.length);
                        if(forCommand == "GetProviderByNumberCommand"){
                            document.getElementById(element + '_text').value = '';
                            document.getElementById(element).value = '';
                        } else if(forCommand == "GetICD10ByCodeCommand"){
                            document.getElementById(element + '_text').value = '';
                            document.getElementById(element).value = '';
                        }
                    }
                }
            }

            function clearElement(element) {
                part = element + '_text';
                document.getElementById(element).value = '';
                document.getElementById(part).value = '';
            }
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <label class="header">Authorisation Specific Nappi Details</label>
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="saveAuthTariff">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />

                            <tr><agiletags:LabelTextSearchText displayName="Nappi Code" elementName="napCode" valueFromSession="yes" searchFunction="yes" searchOperation="ForwardToNewAuthNappiSearch" onScreen="/PreAuth/NappiAuthSpecific.jsp" mandatory="yes" javaScript="onChange=\"getNappiForCode();\""  /></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Nappi Description" elementName="napDesc" valueFromSession="yes" javaScript="" /></tr>
                            <tr><agiletags:ButtonOpperation align="right" commandName="" displayname="Save" span="2" type="button" javaScript="onClick=\"submitWithAction('AddNewAuthNappiToSessionCommand');\"" /></tr>

                        </agiletags:ControllerForm>
                        <br/>
                        <tr><agiletags:AuthNewNappiListTableTag sessionAttribute="newNappiListArray" /></tr>
                        <tr><agiletags:ButtonOpperation align="right" commandName="" displayname="Continue" span="1" type="button" javaScript="onClick=\"submitWithAction('ReturnAuthTariffCommand');\"" /></tr>

                    </table>
                </td></tr></table>
    </body>
</html>
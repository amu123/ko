<%-- 
    Document   : ConditionSpecificAuth
    Created on : 2010/06/06, 04:23:41
    Author     : johanl
--%>

<%@page import="neo.manager.AuthTariffDetails"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script language="JavaScript">

            function CheckFields() {
                $("#authRejReasonRow").hide();
                toggleSub();
                $("#BasketPrefICD").hide();

                //check if product id = 2 and enable copay trigger in accordance
                var prodID = '<%= session.getAttribute("scheme")%>';
                var basketTariffs = '<%= session.getAttribute("AuthBasketTariffsSize")%>';

                if (prodID === '2') {
                    checkIfOHAuthExists(prodID);

                    $("#spmOHCopayTriggerRow").show();
                    $("#spmOHCopayValueRow").show();
                } else {
                    $("#spmOHCopayTriggerRow").hide();
                    $("#spmOHCopayValueRow").hide();
                }

                if (basketTariffs === null || basketTariffs === 'null'
                        || basketTariffs === '' || basketTariffs === 0) {
                    document.getElementById("spmOHCopayTrigger").disabled = true;
                } else {
                    document.getElementById("spmOHCopayTrigger").disabled = false;
                }
            }

            function GetXmlHttpObject() {
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject) {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function toggleSub() {
                var value = $("#authSub").val();
                if (value == "27") {// || value == "2") {
                    //$("#HIVBasket1").show();
                    $("#HIVBasket2").show();
                    $("#HIVBasket3").show();
                    $("#HIVBasket4").show();
                    $("#HIVBasket5").show();
                    //$("#BasketButton").show();
                    //$("#NappiButton").hide();

                } else {
                    //$("#HIVBasket1").hide();
                    $("#HIVBasket2").hide();
                    $("#HIVBasket3").hide();
                    $("#HIVBasket4").hide();
                    $("#HIVBasket5").hide();
                    //$("#BasketButton").show();
                    //$("#NappiButton").show();
                }

                var prodID = '<%= session.getAttribute("scheme")%>';

                if (prodID === "2" && value !== "99") {
                    //check for duplicate auth on selected sub type specifically
                    checkIfTypeSpecificOHAuthExists(prodID, value);
                }
            }

            function loadBasketDetailsForAuth() {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }

                var authSub = $("#authSub").val();
                var planYear = $("#planYear").val();

                var url = "/ManagedCare/AgileController?opperation=GetAuthBasketDetails";
                url = url + "&authSub=" + authSub + "&planYear=" + planYear;

                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        if (result == "Done") {
                            //$("#BasketButton").show();
                            $("#NappiButton").show();
                        } else if (result == "Error") {
                            //alert(result);
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }


            function getBasketDetails(year) {
                $("planYear_error").text("");
                if (year == "" || year == null || year == "99") {
                    $("planYear_error").text("Please select plan year to determine basket");
                } else if (year == "1" || year == "2") {
                    loadBasketDetailsForAuth();

                }
            }


            function ClearValidation() {
                $("#authSub_error").text("");
                $("#authFromDate_error").text("");
                $("#authToDate_error").text("");
                $("#basketButton_error").text("");
            }

            function validateMandatory(action) {
                ClearValidation();
                xhr = GetXmlHttpObject();
                if (xhr == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=ValidateConditionSpecific";
                var authFrom = $("#authFromDate").val();
                var authTo = $("#authToDate").val();
                var authSub = $("#authSub").val();
                var authStatus = $("#authStatus").val();
                url = url + "&authFromDate=" + authFrom + "&authToDate=" + authTo + "&authSub=" + authSub;
                url = url + "&authStatus=" + authStatus;

                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));

                        if (result == "Error") {
                            var errorResponse = xhr.responseText.substring(xhr.responseText.indexOf('|') + 1, xhr.responseText.length)
                            var mandatoryList = errorResponse.split("|");
                            for (i = 0; i < mandatoryList.length; i++) {
                                var elementError = mandatoryList[i].split(":");
                                var element = elementError[0];
                                var errorMsg = elementError[1];

                                if (element == "picd") {
                                    $("#BasketPrefICD").show();
                                    $("#basketICDButton_error").text("Primary ICD Captured For this Condition is incorrect");

                                } else {
                                    $(document).find("#" + element + "_error").text(errorMsg);
                                }
                            }

                        } else if (result == "Done") {
                            submitWithAction(action);
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);

            }

            function submitWithAction(action) {
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }

            function submitOnScreenWithAction(action, forField, onScreen) {
                document.getElementById("onScreen").value = onScreen;
                document.getElementById("searchCalled").value = forField;
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, forField, onScreen) {
                document.getElementById("basketOnScreen").value = onScreen;
                document.getElementById("searchCalled").value = forField;
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }

            function validateADSession(date, element) {
                var at = '<%= session.getAttribute("authType")%>';
                var ap = '<%= session.getAttribute("authPeriod")%>';
                validatePreAuthDate(date, element, ap, at, "no");

            }
            
            function validateDouble(str){
                var strValidChars = "0123456789.";
                var strChar;
                for (j = 0; j < str.length; j++){
                    strChar = str.charAt(j);
                    if (strValidChars.indexOf(strChar) == -1){
                        return false;
                    }
                }
                return true;

            }

            function validateECDoub(str, element){
                var check = validateDouble(str);
                var errorField = "#"+element+"_error";
                
                if(check == false){
                    $(errorField).text("Invalid Amount : "+str);
                    $("#saveBtn").hide();
                }else{
                    $(errorField).text("");
                    $("#saveBtn").show();
                }
            }

            function checkAuthRejectStatus(status) {
                if (status === "2") {
                    $("#authRejReasonRow").show();
                } else {
                    $("#authRejReasonRow").hide();
                }
            }

            function triggerCondSpecCopay(yesNo) {
                if (yesNo === "1") {
                    document.getElementById("spmOHCopayValue").value = "30.00";
                } else {
                    document.getElementById("spmOHCopayValue").value = "0.00";
                }
            }

            function checkIfOHAuthExists(prodID) {
                var url = "/ManagedCare/AgileController";

                var data;
                if (prodID === "2") {
                    data = {'opperation': 'CheckIfMemberHasOHAuth', 'id': new Date().getTime()};

                    $.get(url, data, function (resultTxt) {
                        if (resultTxt !== null) {
                            var resTxtArr = resultTxt.split("|");
                            var result = resTxtArr[0];
                            if (result === "Success") {
                                if (resTxtArr[1] === "true") {
                                    document.getElementById("spmOHCopayTrigger").value = "0";
                                    document.getElementById("spmOHCopayValue").value = "0.00";
                                } else {
                                    document.getElementById("spmOHCopayTrigger").value = "1";
                                    document.getElementById("spmOHCopayValue").value = "30.00";
                                }
                            }
                        }
                    });
                }
            }

            function checkIfTypeSpecificOHAuthExists(prodID, subType) {
                var url = "/ManagedCare/AgileController";

                var data;
                if (prodID === "2") {
                    data = {'opperation': 'CheckIfMemberHasOHAuth', 'id': new Date().getTime(), 'subType': subType};

                    $.get(url, data, function (resultTxt) {
                        if (resultTxt !== null) {
                            var resTxtArr = resultTxt.split("|");
                            var result = resTxtArr[0];
                            if (result === "Success") {
                                if (resTxtArr[1] === "true") {
                                    document.getElementById("spmOHCopayValue_error").innerHTML = "Duplicate OneHealth authorisation found for this condition, please contact them to update.";
                                    document.getElementById("saveBtn").disabled = true;
                                } else {
                                    document.getElementById("spmOHCopayValue_error").innerHTML = "";
                                    document.getElementById("saveBtn").disabled = false;
                                }
                            }
                        }
                    });
                }
            }
        </script>
    </head>
    <body onload="CheckFields();">
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Condition Specific Details</label>
                    <br/><br/>

                    <agiletags:ControllerForm name="conditionSpec" validate="yes">
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <agiletags:HiddenField elementName="searchCalled"/>
                        <input type="hidden" name="basketOnScreen" id="basketOnScreen" value="" />
                        <input type="hidden" name="onScreen" id="onScreen" value="" />
                        <table>
                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Auth Sub Type" elementName="authSub" lookupId="92" javaScript="onChange=\"toggleSub();\"" mandatory="yes" errorValueFromSession="yes" /></tr>
                                <!--<tr><agiletags:LabelTextBoxError displayName="Saving Achieved" elementName="savingAchieved" valueFromSession="yes" mandatory="no" javaScript="onblur=\"validateECDoub(this.value, 'savingAchieved');\""/></tr>-->
                                <tr><agiletags:LabelTextBoxDate displayname="Date From" elementName="authFromDate" valueFromSession="yes" mandatory="yes"/></tr>
                                <tr><agiletags:LabelTextBoxDate displayname="Date To" elementName="authToDate" valueFromSession="yes" mandatory="yes"/></tr>
                            </table>
                            <br/>
                            <table>
                                <tr id="BasketPrefICD"><agiletags:ButtonOpperationLabelError type="button" displayName="View Prefered Basket ICD" elementName="basketICDButton" javaScript="onClick=\"submitWithAction('ForwardToBasketICD','basketICDButton','/PreAuth/ConditionSpecificAuth.jsp');\"" /></tr>
                            </table>
                            <br/>
                            <table>
                                <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Plan Year" elementName="planYear" lookupId="93" mandatory="yes" errorValueFromSession="yes" javaScript="onblur=\"getBasketDetails(this.value);\"" /></tr>
                            </table>
                            <br/>
                            <table>
                                <tr id="HIVBasket2"><agiletags:LabelNeoLookupValueDropDownError displayName="Patient Type" elementName="patientType" lookupId="94" /></tr>
                                <tr id="HIVBasket3"><agiletags:LabelNeoLookupValueDropDownError displayName="Baseline Test" elementName="baseline" lookupId="67" /></tr>
                                <tr id="HIVBasket4"><agiletags:LabelNeoLookupValueDropDownError displayName="Positive" elementName="positive" lookupId="67" /></tr>
                                <tr id="HIVBasket5"><agiletags:LabelTextBoxError displayName="CD 4 Count" elementName="cd4Count" valueFromSession="yes" /></tr>
                            </table>
                            <br/>
                            <table>
                                <tr><td colspan="5" align="left"><label class="subheader">Allocated Tariff to Auth Basket</label></td></tr>
                                <tr><agiletags:AuthBasketTariffDisplay sessionAttribute="AuthBasketTariffs" /></tr>
                                <tr id="BasketButton"><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Manage Auth Basket" elementName="basketButton" javaScript="onClick=\"submitWithAction('ForwardToAuthSpecificBasket','basketButton','/PreAuth/ConditionSpecificAuth.jsp');\"" mandatory="no" /></tr>
                            </table>
                            <br/>
                            <!--<table>
                                <tr><td colspan="5" align="left"><label class="subheader">Allocated Medicine to Auth</label></td></tr>
                                <tr><agiletags:AuthNappiDisplayTag commandName="" sessionAttribute="AuthNappiTariffs" /></tr>
                                <tr id="NappiButton"><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Manage Auth Medicine" elementName="nappiButton" javaScript="onClick=\"submitWithAction('ForwardToAuthSpecificNappi','nappiButton','/PreAuth/ConditionSpecificAuth.jsp');\"" mandatory="no" /></tr>
                            </table>
                            <br/>-->
                            <table>
                                <tr><td colspan="5" align="left"><label class="subheader">Auth Note Details</label></td></tr>
                                <tr><agiletags:AuthNoteListDisplay sessionAttribute="authNoteList" commandName="" javaScript="" /></tr>
                                <tr><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Manage Notes" elementName="authNoteButton" javaScript="onClick=\"submitOnScreenWithAction('ForwardToAuthSpecificNote','authNoteButton','/PreAuth/ConditionSpecificAuth.jsp');\"" mandatory="no" /></tr>
                                <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Auth Status" elementName="authStatus" lookupId="111" mandatory="yes" errorValueFromSession="yes" javaScript="onchange=\"checkAuthRejectStatus(this.value);\"" /></tr>
                                <tr id="authRejReasonRow"><agiletags:LabelNeoLookupValueDropDown elementName="authRejReason" displayName="Auth Reject Reason" lookupId="282" /></tr>	                        
                                <c:if test="${applicationScope.Client != 'Sechaba'}">

                                    <tr id="spmOHCopayTriggerRow"><agiletags:LabelNeoLookupValueDropDownError displayName="30% Co-payment" elementName="spmOHCopayTrigger" lookupId="67" mandatory="yes" errorValueFromSession="yes" javaScript="onchange=\"triggerCondSpecCopay(this.value);\"" /></tr>
                                    <tr id="spmOHCopayValueRow"><agiletags:LabelTextBoxError mandatory="no" displayName="Co-payment Percentage" valueFromSession="yes" elementName="spmOHCopayValue" readonly="yes"/></tr>
                                </c:if>
                            </table>
                            <br/>
                            <table>
                                <tr>
                                    <td colspan="5" align="left">
                                        <button name="opperation" type="button" onClick="submitOnScreenWithAction('ReturnToGenericAuth', 'return', '/PreAuth/GenericAuthorisation.jsp');" value="" >Back</button>
                                        <button name="opperation" type="button" onClick="submitWithAction('ReloadGenericPreAuthCommand', '', '/PreAuth/ConditionSpecificAuth.jsp');" value="">Reset</button>
                                        <button name="opperation" type="button" onClick="validateMandatory('SavePreAuthDetailsCommand');" value="" id="saveBtn">Save</button>
                                    </td>
                                </tr>
                            </table>
                    </agiletags:ControllerForm>
                </td></tr></table>
    </body>
</html>

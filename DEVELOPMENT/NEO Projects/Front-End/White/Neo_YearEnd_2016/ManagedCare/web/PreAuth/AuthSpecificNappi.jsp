<%-- 
    Document   : AuthSpecificBasketTariff
    Created on : 2010/06/30, 09:31:33
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script language="JavaScript">

            function GetXmlHttpObject(){
                if (window.ActiveXObject){
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest){
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }

                return null;
            }

            function getTariffForCode(){
                var str = document.getElementById("tariffCode").value;
                xmlhttp=GetXmlHttpObject();
                if(xmlhttp==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=GetAuthTariffByCodeCommand&tariffCode="+str;

                xmlhttp.onreadystatechange=function(){stateChanged("")};
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function CalculateTariffQuantity(str){
                xhr=GetXmlHttpObject();
                if(xhr==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=CalculateTariffQuantityCommand&quantity="+str+"&tariffType=clinic";
                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        var resText = xhr.responseText;
                        var strArr = new Array();
                        strArr = resText.split('|');

                        var tQuan = new String(strArr[1]);
                        var tAmount = new String(strArr[100]);

                        document.getElementById("tariffQuantity").value = tQuan;
                        document.getElementById("tariffAmount").value = tAmount;
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }
            function ModifyNappiFromList(index){
                var x = document.forms[index];
                var napHidden = new String();
                for (var i=0; i<x.length; i++)
                {
                    if (x.elements[i].id == "napHidden")
                        napHidden = x.elements[i].value;

                }

                xhr=GetXmlHttpObject();
                if(xhr==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=ModifyAuthNappiFromList&napHidden="+napHidden;
                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        var rText = xhr.responseText.split("|");
                        var result = rText[0];
                        if(result == "Done"){
                            $("#provCatType option:selected").text(rText[1]);
                            $("#nCode").val(rText[2]);
                            $("#nDesc").val(rText[3]);
                            $("#nAmount").val(rText[4]);
                            $("#nDosage").val(rText[5]);
                            $("#nQuan").val(rText[6]);
                            $("#nFreq").val(rText[7]);

                            var selected = $("tr[id=napRow"+napHidden+index+"]");
                            for(var j = 0; j < selected.size(); j++){
                                $(selected[j]).hide();
                            }

                        }else if (result == "Error"){
                            alert(result);
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function validateFreq(str){
                if(str != null && str != ""){
                    var valCheck = freqRegex(str);
                    if(valCheck == true){
                        $("#napGrid_error").text("");
                    }else if (valCheck == false){
                        $("#napGrid_error").text("Invalid Frequency");
                    }
                }else{
                    $("#napGrid_error").text("Empty Frequency Not Allowed");
                }
            }

            function freqRegex(str){
                var check1 = /^[0-9]+$/;
                var numCheck = check1.test(str);
                return numCheck;
            }

            function calNewAmount(str){
                if(str != null && str != ""){
                    xhr=GetXmlHttpObject();
                    if(xhr==null){
                        alert ("Your browser does not support XMLHTTP!");
                        return;
                    }
                    var url="/ManagedCare/AgileController";
                    url=url+"?opperation=CalculateNappiQuantityCommand&napQuan="+str;
                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            var rText = xhr.responseText.split("|");
                            var result = rText[0];
                            if(result == "Done"){
                                $("#napGrid_error").text("");
                                $("#nQuan").val(rText[1]);
                                $("#nAmount").val(rText[2]);
                                
                            }else if(result == "Error"){
                                $("#napGrid_error").text("Empty Quantity Not Allowed");
                            }
                        }
                    };
                    xhr.open("POST", url, true);
                    xhr.send(null);
                }else{
                    $("#napGrid_error").text("Empty Quantity Not Allowed");
                }
            }

            function ClearTariffFieldsFromSession(){
                xhr=GetXmlHttpObject();
                if(xhr==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=ClearAuthTariffCommand";
                xhr.onreadystatechange=function(){stateChanged("clearTariffFields")};
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function clearElement(element) {
                part = element + '_text';
                document.getElementById(element).value = '';
                document.getElementById(part).value = '';
            }
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <label class="header">Authorisation Specific Medicine Details</label>
                    <br/><br/>
                    <table>
                        <agiletags:ControllerForm name="saveBasketTariff">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <tr><td colspan="5" align="left"><label class="subheader">Allocated Medicine to Auth</label></td></tr>
                            <tr><agiletags:AuthNappiDisplayTag commandName="" sessionAttribute="AuthNappiTariffs" /></tr>
                            <tr><agiletags:ButtonOpperation align="right" commandName="" displayname="Continue" span="1" type="button" javaScript="onClick=\"submitWithAction('ReturnAuthBasketTariffCommand')\";" /></tr>

                        </agiletags:ControllerForm>
                    </table>
                </td></tr></table>
    </body>
</html>

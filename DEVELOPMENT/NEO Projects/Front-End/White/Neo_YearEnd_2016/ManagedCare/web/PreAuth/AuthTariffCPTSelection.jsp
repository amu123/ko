<%-- 
    Document   : AuthTariffCPTSelection
    Created on : 2012/09/22, 03:55:09
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="neo.manager.NeoUser" %>
<%@ page import="neo.manager.SecurityResponsibility" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>        
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>  
        <script type="text/javascript" language="JavaScript">
            
            $(function() {
                $("#tcptSelection input[id=selectAll]").click(function() {
                    var checked = $(this).is(":checked");   
                    $('#tcptSelection input[type="checkbox"]').each(function(){
                        if(checked === true){
                            $(this).attr('checked', true);
                        }else{
                            $(this).attr('checked', false);
                        }                       
                    });
                });
            });
            
            //            function checkSelected(index){
            //                var valueChecked = $("#tcptSelection tr[id=tcRow"+index+"]").find("input:checkbox").is(":checked");
            //                alert("valueChecked = "+valueChecked);
            //                
            //            }
            
            function submitWithAction(action) {
                var indexSelections = new String;
                var selected = false;
                
                $("#doneButton_error").text("");
                $('#tcptSelection input[id="boxes"]:checked').each(function(){
                    var parentRow = $(this).closest("tr").attr("id");
                    parentRow = parentRow.replace("tcRow", "");
                    indexSelections = indexSelections + parentRow + "|";
                });
                
                $('#tcptSelection input[id="boxesSelected"]:checked').each(function(){
                    selected = true;
                });

                if(selected === false || (indexSelections !== null && indexSelections !== "")){
                    if(indexSelections !== null && indexSelections !== ""){
                   
                        indexSelections = indexSelections.substr(0, indexSelections.length-1);
                    
                        document.getElementById('opperation').value = action;
                        document.getElementById('selectedValues').value = indexSelections;
                        document.forms[0].submit();                    
                    
                    }else{
                        $("#doneButton_error").text("Please Select a CPT to be mapped");
                    }               
                }else{
                    document.getElementById('opperation').value = action;
                    //document.getElementById('selectedValues').value = "";
                    document.forms[0].submit();
                }

            }
            
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td align="left">
                    <!-- content goes here -->
                    <fieldset id="pageBorder">
                        <legend><div align="left"><label class="header">Auth Tariff CPT Selection</label></div></legend>
                        <br/>

                        <table>
                            <agiletags:ControllerForm name="tcptForm" validate="yes">
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <input type="hidden" name="selectedValues" id="selectedValues" value="" />
                                <tr><agiletags:AuthTariffCPTSelectionGrid /></tr>
                            </agiletags:ControllerForm>
                            <br/>
                            <tr><agiletags:ButtonOpperationLabelError displayName="Done" elementName="doneButton" type="button" commandName="" javaScript="onClick=\"submitWithAction('MapSelectedTCPTs');\"" valueFromSession="yes" /></tr>
                        </table>
                    </fieldset>
                </td></tr></table>
                        
    </body>
</html>

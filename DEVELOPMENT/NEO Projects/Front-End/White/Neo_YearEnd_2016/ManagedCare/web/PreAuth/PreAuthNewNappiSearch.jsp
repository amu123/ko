<%-- 
    Document   : PreAuthNewNappiSearch
    Created on : Dec 24, 2010, 11:20:48 AM
    Author     : Johan-NB
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" language="JavaScript">

            function submitWithAction(action) {
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Search PreAuth Nappi's</label>
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="searchNewTariff">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <tr><agiletags:LabelTextBoxError displayName="Code" elementName="code" /></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Description" elementName="description" /></tr>
                            <tr><agiletags:ButtonOpperation type="button" align="right" span="3" commandName="" displayname="Search" javaScript="onclick=\"submitWithAction('SearchNewPreAuthNappi');\"" /></tr>
                        </agiletags:ControllerForm>
                    </table>
                    <br/>
                    <HR color="#666666" WIDTH="50%" align="left">
                    <agiletags:AuthNewNappiSearchResult commandName="AllocateAuthNewNappiToSession" javaScript="" />
                    <!-- content ends here -->
        </td></tr></table>
    </body>
</html>
<%-- 
    Document   : AuthSpecificBasketICDs
    Created on : Nov 24, 2010, 7:40:44 PM
    Author     : Johan-NB
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" language="JavaScript">

            function submitWithAction(action) {
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <table>
                        <agiletags:ControllerForm name="basketICDForm" validate="yes">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:AuthBasketICDDisplay />
                        </agiletags:ControllerForm>
                    </table>
        </td></tr></table>
</html>

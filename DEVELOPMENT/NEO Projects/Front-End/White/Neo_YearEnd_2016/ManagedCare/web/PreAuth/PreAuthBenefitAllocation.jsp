<%-- 
    Document   : PreAuthBenefitAllocation
    Created on : Jun 23, 2011, 9:52:50 AM
    Author     : Johan-NB
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="bt" tagdir="/WEB-INF/tags/benefits" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" language="JavaScript">

            function submitWithActionSave(action, benId, benDesc) {
                document.getElementById('opperation').value = action;
                document.getElementById('benId').value = benId;
                document.getElementById('benDesc').value = benDesc;
                document.forms[0].submit();
            } 
            
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }           

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="left"><td align="left">
                    <label class="header">Allocate Benefit To Auth</label>
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="saveLabTariff" validate="yes">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="benId" id="benId" value="" />
                            <input type="hidden" name="benDesc" id="benDesc" value="" />
                            <bt:ViewBenefits items="${preAuthBenefitsList2}" selectCommand="SetBenefitToAuth" showAll="true"/>
                        </agiletags:ControllerForm>
                    </table>
                    <table>
                        <tr>
                            <td colspan="10" align="left"><label class="noteLabel">* Note: Indicates Family Limits</label></td>
                            <td><agiletags:ButtonOpperation align="right" commandName="" displayname="Continue" span="1" type="button" javaScript="onClick=\"submitWithAction('ReturnCommand');\"" /></td>
                        </tr>
                    </table>
                </td></tr></table>
    </body>
</html>

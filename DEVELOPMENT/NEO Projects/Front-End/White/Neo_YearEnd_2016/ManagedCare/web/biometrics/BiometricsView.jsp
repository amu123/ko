<%-- 
    Document   : BiometricsView
    Created on : 11 Feb 2015, 3:00:54 PM
    Author     : gerritr
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script language="JavaScript">

            $(document).ready(function () {
                if (document.getElementById('searchCalled').value === "showTable") {
                    var index = document.getElementById('historyTableFlag').value;
            <c:if test="${empty sessionScope.bioType || sessionScope.bioType eq '100'}">
                    document.getElementById(index).style.display = 'block';
            </c:if>
            <c:if test="${!empty sessionScope.bioType && sessionScope.bioType eq '291'}">
                    document.getElementById('bioOther').style.display = 'block';
            </c:if>
                    if ('${sessionScope.showSummaryTable}' != null || '${sessionScope.showSummaryTable}' !== " " || '${sessionScope.showSummaryTable}' !== "")
                    {
                        if ('${sessionScope.showSummaryTable}' == "true")
                        {
                            document.getElementById('summaryTable').style.display = 'block';
                        } else
                        {
                            document.getElementById('summaryTable').style.display = 'none';
                        }
                    } else
                    {
                        document.getElementById('summaryTable').style.display = 'none';
                    }
                } else {
                    document.getElementById('summaryTable').style.display = 'none';
                }
                window.scrollTo(0, document.body.scrollHeight); //Let it scroll to the bottom
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function showTable(ID, command) {
                document.getElementById('searchCalled').value = "HideTable";
                document.getElementById('historyTableFlag').value = ID;
                var data = {
                    'ID_text': ID,
                    'opperation': command,
                    'id': new Date().getTime()
                };

                var url = "/ManagedCare/AgileController";
                $.get(url, data, function (resultTxt) {
                    if (resultTxt !== null) {
                        var resTxtArr = resultTxt.split("|");
                        var result = resTxtArr[0];
                        if (result === "Done") {
                            submitScreenRefresh("/biometrics/BiometricsView.jsp");
                        } else {
                        }
                    }
                });

                data = null;
            }

            function getCoverByNumber(ID, command) {
                var url = "/ManagedCare/AgileController";
                var data = {
                    //replace this with your command name
                    'opperation': command,
                    'ID_text': ID,
                    'id': new Date().getTime()
                };

                $.get(url, data, function (resultTxt) {
                    if (resultTxt !== null) {
                        var resTxtArr = resultTxt.split("|");
                        var result = resTxtArr[0];
                        if (result === "Done") {
                            submitScreenRefresh("/biometrics/BiometricsView.jsp");
                        } else {
                        }
                    } else {
                    }
                });

                data = null;
            }

            function getDepByNumber(screen) {
                $("#opperation").val("RefreshCommand");
                $("#refreshScreen").val(screen);
                document.forms[0].submit();
            }

            //Used for Capture biometrics
            function setSession(value, elementName) {
                var url = "/ManagedCare/AgileController";
                var data = {
                    //replace this with your command name
                    'opperation': 'CaptureSessionCommand',
                    'value_text': value,
                    'elementName_text': elementName,
                    'id': new Date().getTime()
                };

                $.get(url, data, function () {
                });

                data = null;

                if (elementName === "dateReceived") {
                    $("#dateMeasured").blur();
                }

            }


            function submitScreenRefresh(screen) {
                $("#opperation").val("RefreshCommand");
                $("#refreshScreen").val(screen);
                document.forms[0].submit();
            }

            function showOrHide(index) {
            <c:if test="${empty sessionScope.bioType || sessionScope.bioType eq '100'}">
                document.getElementById(1).style.display = 'none';
                document.getElementById(2).style.display = 'none';
                document.getElementById(3).style.display = 'none';
                document.getElementById(4).style.display = 'none';
                document.getElementById(5).style.display = 'none';
                document.getElementById(6).style.display = 'none';
                document.getElementById(7).style.display = 'none';
                document.getElementById(8).style.display = 'none';
                document.getElementById(9).style.display = 'none';
                document.getElementById(10).style.display = 'none';
                document.getElementById(11).style.display = 'none';
                document.getElementById('cdlDropdown').value = index;
                document.getElementById(index).style.display = 'block';
                lastVisible = index;
            </c:if>
            }

            function getDepHistory(ind, command) {
                showOrHide(document.getElementById('historyTableFlag').value);
                document.getElementById('searchCalled').value = "showTable";

                var url = "/ManagedCare/AgileController";
                var data = {
                    //replace this with your command name
                    'opperation': command,
                    'ID_text': ind,
                    'id': new Date().getTime()
                };

                $.get(url, data, function (resultTxt) {
                    if (resultTxt !== null) {
                        var resTxtArr = resultTxt.split("|");
                        var result = resTxtArr[0];
                        if (result === "Done") {
                            submitScreenRefresh("/biometrics/BiometricsView.jsp");
                        } else {
                        }
                    } else {
                    }
                });

                data = null;
            }

            function viewBiometricsWithAction(biometricId, action, bioName) {
                document.getElementById('opperation').value = action;
                document.getElementById('biometricID').value = biometricId;
                document.getElementById('bioName').value = bioName;
                document.getElementById('cdlDropdown').value = document.getElementById('historyTableFlag').value;
                document.getElementById('onScreen').value = "/biometrics/BiometricsView.jsp";
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <table width=100% height=100% valign="top" align="left">
            <agiletags:ControllerForm name="ViewBiometricsForm" >
                <input type="hidden" name="opperation" id="opperation" value="" />
                <input type="hidden" name="onScreen" id="onScreen" value="" />
                <input type="hidden" name="coverNum" id="coverNum" value="" />
                <input type="hidden" name="depTypeId" id="depTypeId" value="" />
                <input type="hidden" name="entityId" id="entityId" value="" />
                <input type="hidden" name="depNumber" id="depNumber" value="" />
                <input type="hidden" name="optionId" id="optionId" value="" />
                <input type="hidden" name="entityCommon" id="entityCommon" value="" />
                <input type="hidden" name="refreshScreen" id="refreshScreen" value="" />
                <input type="hidden" name="bioName" id="bioName" value="" />
                <input type="hidden" name="biometricID" id="biometricID" value="" />
                <input type="hidden" name="cdlDropdown" id="cdlDropdown" value="" />
                <agiletags:HiddenField elementName="searchCalled" />
                <agiletags:HiddenField elementName="historyTableFlag" />

                <table width=100% height=100%>
                    <tr valign="top">
                        <td width="50px" align="left" colspan="2">
                            <label class="header">Member Detail</label>
                            <br/>
                            <br/>
                            <HR color="#666666" WIDTH="100%" align="left">
                            <agiletags:MemberViewGridTag sessionAttribute="" commandName="" javaScript="" onScreen=""/> 
                        </td>
                    </tr>
                    <tr>
                    <br/><br/>
                    <table>
                        <tr><agiletags:BiometricTypeDropdownTag javascript="onChange=\"showTable(this.value, 'LoadBiometricsDetailsCommand');\""/></tr>
                        <br/>
                        <br/>
                        <tr><agiletags:DropdownBiometricTypeTag displayName="Biometric Type" javascript="onChange=\"showTable(this.value, 'BiometricsTableSelectionCommand');\""/></tr>
                    </table>
                    <br/><br/>
                    <agiletags:BiometricsInfoTag commandName="" javaScript="onClick=\"getDepHistory(this.value, 'BiometricsHistoryCommand');\"" onScreen="" sessionAttribute=""/></tr>

                    <br/><br/>
                    <c:if test="${empty sessionScope.bioType || sessionScope.bioType eq '100'}">
                        <div id="1" style="display:none">
                            <br/>
                            <label class="subheader">Summary of History</label>
                            <br/>
                            <br/>
                            <HR color="#666666" WIDTH="100%" align="left">
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricAsthmaTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'Asthma');\""/>
                            </table>
                        </div>

                        <div id="2" style="display:none">
                            <br/>
                            <label class="subheader">Summary of History</label>
                            <br/><br/>
                            <HR color="#666666" WIDTH="100%" align="left">
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricHypertensionTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'Hypertension');\""/>
                            </table>
                        </div>

                        <div id="3" style="display:none"> 
                            <br/>
                            <label class="subheader">Summary of History</label>
                            <br/><br/>
                            <HR color="#666666" WIDTH="100%" align="left">
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricChronicRenalDiseaseTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'ChronicRenalDisease');\""/>
                            </table>
                        </div>

                        <div id="4" style="display:none">
                            <br/>
                            <label class="subheader">Summary of History</label>
                            <br/><br/>
                            <HR color="#666666" WIDTH="100%" align="left">
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricCardiacFailureAndCardiacMyothy javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'CardiacFailure');\""/>
                            </table>
                        </div>

                        <div id="5" style="display:none"> 
                            <br/>
                            <label class="subheader">Summary of History</label>
                            <br/><br/>
                            <HR color="#666666" WIDTH="100%" align="left">
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricDiabetesTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'Diabetes');\""/>
                            </table>
                        </div>

                        <div id="6" style="display:none"> 
                            <br/>
                            <label class="subheader">Summary of History</label>
                            <br/><br/>
                            <HR color="#666666" WIDTH="100%" align="left">
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricCoronaryArteryDiseaseTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'Coronary');\""/>
                            </table>
                        </div>

                        <div id="7" style="display:none">
                            <br/>
                            <label class="subheader">Summary of History</label>
                            <br/><br/>
                            <HR color="#666666" WIDTH="100%" align="left">
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricCOPDandBronchiectasisTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'Bronchiectasis');\""/>
                            </table>
                        </div>

                        <div id="8" style="display:none">
                            <br/>
                            <label class="subheader">Summary of History</label>
                            <br/><br/>
                            <HR color="#666666" WIDTH="100%" align="left">
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricHyperlipidaemiaTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'Hyperlipidaemia');\""/>
                            </table>
                        </div>

                        <div id="9" style="display:none"> 
                            <br/>
                            <label class="subheader">Summary of History</label>
                            <br/><br/>
                            <HR color="#666666" WIDTH="100%" align="left">
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricMajorDepressionTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'MajorDepression');\""/>
                            </table>
                        </div>
                        <div id="10" style="display:none"> 
                            <br/>
                            <label class="subheader">Summary of History</label>
                            <br/><br/>
                            <HR color="#666666" WIDTH="100%" align="left">
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricsGeneralTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'General');\""/>
                            </table>
                        </div>
                        <div id="11" style="display:none"></div>
                    </c:if>
                    <c:if test="${!empty sessionScope.bioType && sessionScope.bioType eq '291'}">
                        <div id="bioOther" style="display:none">
                            <br/>
                            <label class="subheader">Summary of History</label>
                            <br/><br/>
                            <HR color="#666666" WIDTH="100%" align="left">
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricsOtherTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'Other');\""/>
                            </table>
                        </div>
                    </c:if>
                    <div id="summaryTable" style="display:none">
                        <br><br>
                        <agiletags:BiometricTableTag headerType="subheader" showEditBtn="true" javaScript="onClick=\"getDepByNumber('/biometrics/CaptureBiometrics.jsp');\""/>
                    </div>
                    </tr>
                    <tr>
                        <c:if test="${!empty sessionScope.returnFlag && (sessionScope.returnFlag eq true || sessionScope.returnFlag eq 'true')}">
                        <button name="opperation" type="button" onclick="submitWithAction('ViewSubWorkBenchCommand')" value="ViewSubWorkBenchCommand">Return</button>
                    </c:if>
                    </tr>
                </table>
            </agiletags:ControllerForm>
        </table>
        <br><br><span/>
    </body>
</html>

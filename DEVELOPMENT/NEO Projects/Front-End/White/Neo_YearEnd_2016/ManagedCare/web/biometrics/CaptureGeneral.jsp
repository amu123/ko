<%--
    Document   : CaptureGeneral
    Created on : 2010/05/13, 01:14:39
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
   <%
   String revNo = (String)request.getAttribute("revNo");
   %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
    <link rel="stylesheet" href="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
    <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
    <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
    <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
    <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.tools.min.js"></script>
    <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>

    <script>

        $(document).ready(function() {
             $("#dialog-saved").hide();
             $("#dialog-notsaved").hide();

                 $(".tooltext").tooltip({
                                // place tooltip on the right edge
                                position: "center right",

                                // a little tweaking of the position
                                offset: [-2, 10],

                                // use the built-in fadeIn/fadeOut effect
                                effect: "fade",

                                // custom opacity setting
                                opacity: 0.7

                            });
        /*************************************************************
         *          DIAGNOSIS EVENTS
         *************************************************************/

       $("#icdcode").change(function(){

                        var icdcode = $("#icd10 :selected").val();

                            if(icdcode == 99) {
                            $("#showdescription").hide();
                        }else {
                    $("#showdescription").show();
                  }

                });
        displayDialog();
        toggleSmokingQuestions();
        displayError();
    });


    function toggleSmokingQuestions() {
        var currentSmoker = $("#currentSmoker :selected").val();
        //var exSmoker = $("#exSmoker :selected").val();
        //alert(currentSmoker);
        var valueZero = '0';
        var valueEmpty = '';
        if(currentSmoker == 99) {
            $("#exsmoke").show();
            $("#ciganumber").hide();
            $("#yearsstopped").hide();

            $("#smokingQuestion").val(valueZero);
            $("#stopped").val(valueZero);

        }else if(currentSmoker == 0){
            $("#exsmoke").show();
            $("#ciganumber").hide();
             $("#yearsstopped").show();
            $("#smokingQuestion").val(valueZero);
            //$("#stopped").val(valueEmpty);


        }else {
            $("#smokingQuestion").val(valueEmpty);
            $("#exSmoker").val(0);
            $("#stopped").val(valueZero);

            $("#ciganumber").show();
            $("#exsmoke").hide();
            $("#yearsstopped").hide();


        }
    }


    function toggleExSmokingQuestion() {
            var exSmoker = $("#exSmoker :selected").val();
            //alert(exSmoker);
            var valueZero = '0';
            var valueEmpty = '';

            if(exSmoker == 0 || exSmoker == 99) {
                $("#stopped").val(valueZero);
                $("#yearsstopped").hide();
            }else {
                $("#stopped").val(valueEmpty);
                $("#yearsstopped").show();
            }
        }
        

        function submitWithAction(action) {
            document.getElementById('opperation').value = action;
            document.forms[0].submit();
        }

        function submitWithAction(action, forField, onScreen) {
            document.getElementById('onScreen').value = onScreen;
            document.getElementById('searchCalled').value = forField;
            document.getElementById('opperation').value = action;
            document.forms[0].submit();
        }

        function calculateBMI() {
                var height = document.getElementById('length').value;
                var weight = document.getElementById('weight').value;
                var h2 = height * height
                var bmi = weight / h2;
                var roundedBMI = Math.round(bmi);
                if(height != 0) {
                    document.getElementById('bmi').value = roundedBMI;
                }else {
                    document.getElementById('bmi').value = '';
                }

                return;

            }


        function getTariffForCode(str, element){
            var str = document.getElementById("tariffCode").value;
            xmlhttp=GetXmlHttpObject();
            xmlhttp.overrideMimeType('text/xml');
            if(xmlhttp==null){
                alert ("Your browser does not support XMLHTTP!");
                return;
            }
            var url="/ManagedCare/AgileController";
            url=url+"?opperation=LoadBiometricsTariffCodeCommand&tariffCode="+str;

            xmlhttp.onreadystatechange=function(){stateChanged(element)};
            xmlhttp.open("POST", url, true);
            xmlhttp.send(null);
        }

        function CalculateTarifTestResults(){
            var q = document.getElementById("tariffQuantity").value;
            var p = document.getElementById("testResults").value;
            xmlhttp=GetXmlHttpObject();
            xmlhttp.overrideMimeType('text/xml');
            if(xmlhttp==null){
                alert ("Your browser does not support XMLHTTP!");
                return;
            }

            var url="/ManagedCare/AgileController";
            url=url+"?opperation=CalculateTariffQuantityCommand&tariffQ="+q;
            xmlhttp.onreadystatechange=function(){stateChanged("")};
            xmlhttp.open("POST", url, true);
            xmlhttp.send(null);
        }

        function ModifyTariffFromList(index){
            var x = document.forms[index];
            var tQuan = new String();
            var tCode = new String();
            for (var i=0; i<x.length; i++)
            {
                if (x.elements[i].id == "tTableQuan")
                    tQuan = x.elements[i].value;

                if (x.elements[i].id == "tTableCode")
                    tCode = x.elements[i].value;
            }

            xmlhttp=GetXmlHttpObject();
            xmlhttp.overrideMimeType('text/xml');
            if(xmlhttp==null){
                alert ("Your browser does not support XMLHTTP!");
                return;
            }
            var url="/ManagedCare/AgileController";
            url=url+"?opperation=ModifyAuthTariffCommand&tTableQuan="+tQuan+"&tTableCode="+tCode;
            //alert(url);
            xmlhttp.onreadystatechange=function(){stateChanged("")};
            xmlhttp.open("POST", url, true);
            xmlhttp.send(null);
        }

        function RemoveTariffFromList(index){
            var x = document.forms[index];
            var tQuan = new String();
            var tCode = new String();
            for (var i=0; i<x.length; i++)
            {
                if (x.elements[i].id == "tTableQuan")
                    tQuan = x.elements[i].value;

                if (x.elements[i].id == "tTableCode")
                    tCode = x.elements[i].value;
            }

            xmlhttp=GetXmlHttpObject();
            xmlhttp.overrideMimeType('text/xml');
            if(xmlhttp==null){
                alert ("Your browser does not support XMLHTTP!");
                return;
            }

            var url="/ManagedCare/AgileController";
            url=url+"?opperation=RemoveAuthTariffCommand&tTableQuan="+tQuan+"&tTableCode="+tCode;
            xmlhttp.onreadystatechange=function(){stateChanged("")};
            xmlhttp.open("POST", url, true);
            xmlhttp.send(null);
        }

        function ClearTariffFieldsFromSession(){
            xmlhttp=GetXmlHttpObject();
            xmlhttp.overrideMimeType('text/xml');
            if(xmlhttp==null){
                alert ("Your browser does not support XMLHTTP!");
                return;
            }

            var url="/ManagedCare/AgileController";
            url=url+"?opperation=ClearAuthTariffCommand";
            xmlhttp.onreadystatechange=function(){stateChanged("clearTariffFields")};
            xmlhttp.open("POST", url, true);
            xmlhttp.send(null);
        }

        function loadQuestions(str, element) {
            xmlhttp=GetXmlHttpObject();
            if(xmlhttp==null) {
                alert('Your browser does not support XMLHTTP!')
                return;
            }
            var val = $("#memberNumber_text").val();
            var url="/ManagedCare/AgileController";
            url=url+"?opperation=LoadGeneralBiometricsQuestionsCommand&depListValues="+str+"&memberNumber="+val;
            xmlhttp.onreadystatechange=function(){stateChanged(element)};
            xmlhttp.open("POST",url,true);
            xmlhttp.send(null);
        }

        function GetXmlHttpObject()
        {
            if (window.XMLHttpRequest)
            {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                return new XMLHttpRequest();
            }
            if (window.ActiveXObject)
            {
                // code for IE6, IE5
                return new ActiveXObject("Microsoft.XMLHTTP");
            }
            return null;
        }


        function stateChanged(element){
           // alert('Inside the method');
           // alert(result);
            if (xmlhttp.readyState==4)
            {
               // alert(xmlhttp.responseText);
                var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('$'));
                //alert(result);
                if(result == "LoadBiometricsTariffCodeCommand"){
                    //alert('We are inside the if statement');

                    alert(xmlhttp.responseText);
                    var resText = xmlhttp.responseText;
                    var strArr = new Array();
                    strArr = resText.split('|');
                    var tCode = new String();
                    tCode = strArr[1];
                    var tDesc = new String();
                    tDesc = strArr[2];
                    var tAmount = new String();
                    tAmount = strArr[3];
                    var tQuan = new String();
                    tQuan = strArr[4];

                    document.getElementById("tariffDescription").value = tQuan;
                    document.getElementById("tariffCode").value = tCode;
                    //document.getElementById("tariffAmount").value = tAmount;
                    //document.getElementById("tariffQuantity").value = tQuan;


                }else if(result == "CalculateTariffQuantityCommand"){
                    var resText = xmlhttp.responseText;
                    var strArr = new Array();
                    strArr = resText.split('|');

                    var tQuan = new String(strArr[1]);
                    var tAmount = new String(strArr[2]);

                    document.getElementById("tariffQuantity").value = tQuan;
                    document.getElementById("tariffAmount").value = tAmount;


                }else if(result == "ModifyAuthTariffCommand"){
                    var resText = xmlhttp.responseText;
                    var strArr = new Array();
                    strArr = resText.split('|');
                    var tCode = new String();
                    tCode = strArr[1];
                    var tDesc = new String();
                    tDesc = strArr[2];
                    var tAmount = new String();
                    tAmount = strArr[3];
                    var tQuan = new String();
                    tQuan = strArr[4];

                    document.getElementById("tariffDesc").value = tDesc;
                    document.getElementById("tariffCode").value = tCode;
                    document.getElementById("tariffAmount").value = tAmount;
                    document.getElementById("tariffQuantity").value = tQuan;


                }else if(result == "RemoveAuthTariffCommand"){

                    $.find('#tariffRow').hide().slideUp('slow').fadeOut();
                    $.remove('#tariffRow');

                }else if(result == "ClearTariffFieldsFromSession"){
                    document.getElementById("tariffDesc").value = '';
                    document.getElementById("tariffCode").value = '';
                    document.getElementById("tariffQuantity").value = '';
                    document.getElementById("tariffAmount").value = '';
                }else if(result == "Error"){
                    var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|')+1, xmlhttp.responseText.length);
                    if(forCommand == "GetProviderByNumberCommand"){
                        document.getElementById(element + '_text').value = '';
                        document.getElementById(element).value = '';
                    } else if(forCommand == "GetICD10ByCodeCommand"){
                        document.getElementById(element + '_text').value = '';
                        document.getElementById(element).value = '';
                    }
                }
            }
        }

        function clearElement(element) {
            part = element + '_text';
            document.getElementById(element).value = '';
            document.getElementById(part).value = '';
        }

        function CheckSearchCommands(){
            //reload ajax commands for screen population
            var searchVal = document.getElementById('searchCalled').value;
            if (searchVal == 'memberNumber'){
                //alert(document.getElementById('memberNumber_text').value);
                getCoverByNumber(document.getElementById('memberNumber_text').value, 'memberNumber');

            }else if (searchVal == 'treatingProvider'){
                //alert(document.getElementById('providerNumber_text').value);
                // validateProvider(document.getElementById('treatingProvider_text').value, 'treatingProvider');

            }else if(searchVal == 'code') {
                //alert(document.getElementById('code_text').value);
                getICDDescription(document.getElementById('code_text').value, 'code');
            }
            document.getElementById('searchCalled').value = '';
            var ex = $("#exSmoker :selected").val();
            var curr = $("#currentSmoker :selected").val();
            $("#smoke").change();
        }

        /////////////////////////
        function GetDOMParser(xmlStr){
            var xmlDoc;
            if(window.DOMParser){
                parser=new DOMParser();
                xmlDoc= parser.parseFromString(xmlStr,"text/xml");
            }else{
                xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async="false";
                xmlDoc.loadXML(xmlStr);
            }
            return xmlDoc;
        }


        function getCoverByNumber(str, element){

            xhr = GetXmlHttpObject();//xhr = XMLHttpRequest object
            if(xhr==null){
                alert("ERROR: Browser Incompatability");
                return;
            }
            //url command GetCoverDetailsByNumber location - com.agile.command
            var url = "/ManagedCare/AgileController";
            url+="?opperation=GetCoverDetailsByNumber&number="+ str + "&element="+element;
            xhr.onreadystatechange=function(){
                if(xhr.readyState == 4 && xhr.statusText == "OK"){
                    //loadQuestions(str, element);
                    $("#depListValues").empty();//clear EntityCoverDependantDropDown tag with elementName of depListValues
                    $("#depListValues").append("<option value=\"99\"></option>");//add empty option to force selection
                    var xmlDoc = GetDOMParser(xhr.responseText);//loads xml document of servlets response to the page
                    //print value from servlet
                    $(xmlDoc).find("EAuthCoverDetails").each(function (){//for each <EAuthCoverDetails> in the xml document
                        //set product details
                        var prodName = $(this).find("ProductName").text();//textContent of <ProductName> in the <EAuthCoverDetails>  tag
                        var optName = $(this).find("OptionName").text();//textContent of <OptionName> in the <EAuthCoverDetails>  tag

                        //extract dependent info from tag
                        $(this).find("DependantInfo").each(function(){//for each <DependantInfo> in the <EAuthCoverDetails>  tag
                            var depInfo = $(this).text();
                            var nameSurnameType = depInfo.split("|");
                            //Add option to dependant dropdown
                            var optVal = document.createElement('OPTION');
                            optVal.value = nameSurnameType[0];
                            optVal.text = nameSurnameType[1];
                            document.getElementById("depListValues").options.add(optVal);

                        });
                        //DISPLAY MEMBER DETAIL LIST

                        $(document).find("#schemeName").text(prodName);//set product name to LabelTextDisplay tag with the element name of schemeName
                        $(document).find("#schemeOptionName").text(optName);//set option name to LabelTextDisplay tag with the element name of schemeOptionName
                        $(document).find(element+"_error").text($(this).text());
                    });
                }
            }
            xhr.open("POST",url,true);
            //xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
            xhr.send(null);
        }


        function stateChanged(element){
            if (xmlhttp.readyState==4)
            {
                // alert(xmlhttp.responseText);
                var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                 //alert(result);
                if(result == "LoadBiometricsTariffCodeCommand"){
                    var description = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=')+1, xmlhttp.responseText.lastIndexOf('|'));
                    var testResults = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=')+1, xmlhttp.responseText.lastIndexOf('$'));
                    document.getElementById('tariffDescription').value = description;
                    //document.getElementById('testResults').value = testResults;
                    document.getElementById(element + '_error').innerHTML = '';
                    document.getElementById('searchCalled').value = "";
                }else if(result == "LoadGeneralBiometricsQuestionsCommand"){
                    var weigth = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=')+1, xmlhttp.responseText.lastIndexOf('|'));
                    var exercise = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=')+1, xmlhttp.responseText.lastIndexOf('|'));
                    var alcoholUnits = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=')+1, xmlhttp.responseText.lastIndexOf('$'));
                    //alert(alcoholUnits);

                    var splitResult = weigth.split('|');
                   // alert('The split result is ' + splitResult[0]);
                    var blae = splitResult[1];
                    var b = blae.split('=');
                   // alert('The split result is ' + b[1]);
                    var w = splitResult[0];
                    var e = b[1];
                    var l = splitResult[2].split('=');
                    var le = l[1];
                    var bmiV = splitResult[3].split('=');
                    var bmi = bmiV[1];
                    var bpsV = splitResult[4].split('=');
                    var bps = bpsV[1];
                    var bpdV = splitResult[5].split('=');
                    var bpd = bpdV[1];
                    var smokeV = splitResult[6].split('=');
                    var smoke = smokeV[1];
                    var stoppedV = splitResult[7].split('=');
                    var stopped = stoppedV[1];

                    document.getElementById('weight').value = w;
                    document.getElementById('exerciseQuestion').value = e;
                    document.getElementById('alcoholConsumption').value = alcoholUnits;
                    document.getElementById('length').value = le;
                    document.getElementById('bmi').value = bmi;
                    document.getElementById('bpSystolic').value = bps;
                    document.getElementById('bpDiastolic').value = bpd;
                    document.getElementById('smokingQuestion').value = smoke;
                    document.getElementById('stopped').value = stopped;
                }else if(result == "LoadICD10BiometricsCommand"){
                    var description = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=')+1, xmlhttp.responseText.lastIndexOf('$'));
                    document.getElementById('description').value = description;
                    document.getElementById('searchCalled').value = "";
                    document.getElementById(element + '_error').innerHTML = '';
                }else if(result == "FindProviderByCodeCommand"){
                         document.getElementById(element + '_error').innerHTML = '';
                }else if(result == "Error"){
                    var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|')+1, xmlhttp.responseText.length);
                    if(forCommand == "FindProviderByCodeCommand"){
                    } else if(forCommand == "GetMemberByNumberCommand"){
                        document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                        //document.getElementById(element + '_text').value = '';
                       // document.getElementById('searchCalled').value = "";
                       // submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                    } else if(forCommand == "LoadICD10BiometricsCommand") {
                        document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                        document.getElementById(element + '_text').value = '';
                        document.getElementById('searchCalled').value = "";
                    }
                }
            }
        }

        function validateMember(str, element) {
            xmlhttp=GetXmlHttpObject();
            if(xmlhttp==null) {
                alert("Your browser does not support XMLHTTP!");
                return;
            }
            var url="/ManagedCare/AgileController";
            url=url+"?opperation=MemberDetailsCoverSearchCommand&memberNumber="+str;
            xmlhttp.onreadystatechange=function(){stateChanged(element)};
            xmlhttp.open("POST",url,true);
            xmlhttp.send(null);
        }

        function loadICDDescription(str, element) {
            xmlhttp=GetXmlHttpObject();
            if(xmlhttp==null) {
                alert('Your browser does not support XMLHTTP!')
                return;
            }
            var url="/ManagedCare/AgileController";
            url=url+"?opperation=LoadICD10BiometricsCommand&code="+str;
            xmlhttp.onreadystatechange=function(){stateChanged(element)};
            xmlhttp.open("POST",url,true);
            xmlhttp.send(null);
        }

        function displayDialog() {
            <%  if ((session.getAttribute("updated")) != null && (!session.getAttribute("updated").equals("null")) ) {
            %>

                    $("#dialog-saved").dialog({ modal: true, width:550,
                        buttons: {
                            'Done': function() {
                                $(this).dialog('close');
                            }
                        }
                    });
                    <%
            session.setAttribute("updated", null);
            } %>

            }

             function displayError() {
            <%  if ((session.getAttribute("failed")) != null && (!session.getAttribute("failed").equals("null")) ) {
            %>

                    $("#dialog-notsaved").dialog({ modal: true, width:550,
                        buttons: {
                            'Done': function() {
                                $(this).dialog('close');
                            }
                        }
                    });
                    <%
            session.setAttribute("failed", null);
            } %>

            }

                function validateProvider(str, element){
                    xmlhttp=GetXmlHttpObject();
                    if (xmlhttp==null)
                    {
                        alert ("Your browser does not support XMLHTTP!");
                        return;
                    }
                    var url="/ManagedCare/AgileController";
                    url=url+"?opperation=FindProviderByCodeCommand&providerNumber="+str;
                    xmlhttp.onreadystatechange=function(){stateChanged(element)};
                    xmlhttp.open("POST",url,true);
                    xmlhttp.send(null);
                }

    </script>
    
</head>
<body onload="CheckSearchCommands()">
    <div id="dialog-saved" title="Biometrics Saved"><p>General Biometrics has been saved. Reference No:<%=revNo%></p></div>
    <div id="dialog-notsaved" title="Biometrics Not Saved"><label class="error">General Biometrics save has failed!</label></div>
    <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
            <!-- content goes here -->
            <label class="header">General</label>
            <table>
            <agiletags:ControllerForm name="CaptureGeneral">
                <input type="hidden" name="opperation" id="opperation" value=""/>
                <agiletags:HiddenField elementName="searchCalled"/>
                <input type="hidden" name="onScreen" id="onScreen" value="" />
                <input type="hidden" name="providerDetails" id="providerDetails" value="email" />
                <input type="hidden" name="icd10Description" id="icd10Description" value="icd" />

                <tr><agiletags:LabelTextSearchText displayName="Member Number" elementName="memberNumber" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand"  valueFromRequest="yes" valueFromSession="yes" onScreen="/biometrics/CaptureGeneral.jsp" mandatory="yes" javaScript="onChange=\"getCoverByNumber(this.value, 'memberNumber');\""/></tr>
                <tr><agiletags:EntityCoverDependantDropDown displayName="Cover Dependants" elementName="depListValues" javaScript="onChange=\"loadQuestions(this.value, 'depListValues');\"" mandatory="Yes" valueFromSession="yes" errorValueFromSession="yes"/></tr>
                <tr><agiletags:LabelTextDisplay displayName="Scheme" elementName="schemeName" valueFromSession="yes" javaScript="" /></tr>
                <tr><agiletags:LabelTextDisplay displayName="Scheme Option" elementName="schemeOptionName" valueFromSession="yes" javaScript="" /></tr>
                <!--Generic fields -->

                <tr id="exerciseQuestion"><agiletags:LabelGenericNumberDropDownError displayName="How Many times do you exercise in a week?" elementName="exerciseQuestion" errorValueFromSession="yes" mandatory="no" listSize="8"/></tr>
                <tr id="smoke"><agiletags:LabelNeoLookupValueDropDownError displayName="Are you a current smoker?" elementName="currentSmoker" lookupId="67" mandatory="no" javaScript="onChange=\"toggleSmokingQuestions();\"" errorValueFromSession="yes"/></tr>
                <tr id="ciganumber"><agiletags:LabelTextBoxError displayName="How many cigarettes do you smoke in a day?" elementName="smokingQuestion" valueFromSession="yes" mandatory="no"/></tr>
                <tr id="exsmoke"><agiletags:LabelNeoLookupValueDropDownError displayName="Are you an ex smoker?" elementName="exSmoker"  lookupId="67" mandatory="no" javaScript="onChange=\"toggleExSmokingQuestion();\"" errorValueFromSession="yes"/></tr>
                <tr id="yearsstopped"><agiletags:LabelTextBoxError displayName="How many years since you have stopped" elementName="stopped" valueFromSession="yes" mandatory="no"/></tr>
                <tr id="alcoholConsumption"><agiletags:LabelGenericNumberDropDownError displayName="How many Alcohol units do you consume in a week?" elementName="alcoholConsumption" errorValueFromSession="yes" mandatory="no" listSize="21"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="Weight (KG) e.g. 50.5" elementName="weight" valueFromSession="yes" mandatory="no" javaScript="onBlur=\"calculateBMI();\""/></tr>
                <tr><agiletags:LabelTextBoxError displayName="Length (M) e.g. 1.65" elementName="length" valueFromSession="yes" mandatory="no" javaScript="onBlur=\"calculateBMI();\""/></tr>
                <tr><agiletags:LabelTextBoxError displayName="BMI" elementName="bmi" valueFromSession="yes" mandatory="no" readonly="yes"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="Blood Pressure Systolic(mmHg)" elementName="bpSystolic" valueFromSession="yes" mandatory="no"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="Blood Pressure Diastolic(mmHg)" elementName="bpDiastolic" valueFromSession="yes" mandatory="no"/></tr>
                <tr><td><br/><br/></td></tr>
                
                <tr><agiletags:LabelTextBoxDateTime displayName="Date Measured" elementName="dateMeasured" valueFromSession="yes" mandatory="yes"/></tr>
                <tr><agiletags:LabelTextBoxDate displayname="Date Received" elementName="dateReceived" valueFromSession="yes" mandatory="yes"/></tr>
                <tr><agiletags:LabelTextSearchText displayName="Treating Provider" elementName="providerNumber" valueFromSession="yes" searchFunction="yes" searchOperation="ForwardToSearchProviderCommand" onScreen="/biometrics/CaptureGeneral.jsp" javaScript="onChange=\"validateProvider(this.value, 'providerNumber');\"" mandatory="no"/></tr>
                <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Source" elementName="source" lookupId="104" mandatory="no" errorValueFromSession="yes"/></tr>
                <tr id="icdcode"><agiletags:LabelNeoLookupValueDropDownError displayName="ICD10" elementName="icd10" lookupId="120" javaScript="onBlur=\"loadICDDescription(this.value, 'icd10');\"" mandatory="no" errorValueFromSession="yes"/></tr>
                <tr id="showdescription"><agiletags:LabelTextBoxError displayName="ICD10 Description" elementName="description" valueFromSession="yes" readonly="yes" mandatory="no"/></tr>
                <!-- WILL BE ADDED LATER -->    
                <%--<tr><agiletags:BiometricsTarrifTag commandName="AddBiometricsTariffToSessionCommand"  elementName="tariffCode" javaScript="onClick=\"submitWithAction('AddBiometricsTariffToSessionCommand')\";" /></tr>
                <tr><agiletags:BiometricsTariffListTableTag sessionAttribute="biometricsTariffListArray"  commandName="CaptureGeneralBiometricsCommand"/></tr>
                <tr><agiletags:ButtonOpperation commandName="AddBiometricsTariffToSessionCommand" align="left" displayname="Add" span="1" type="button"  javaScript="onClick=\"submitWithAction('AddBiometricsTariffToSessionCommand')\";"/></tr>
                --%>
                <tr><agiletags:LabelTextAreaError displayName="Detail" elementName="generalDetails" valueFromSession="yes" mandatory="no"/></tr>

            <tr>
                <agiletags:ButtonOpperation commandName="ResetGeneralBiometricsCommand" align="left" displayname="Reset" span="1" type="button"  javaScript="onClick=\"submitWithAction('ResetGeneralBiometricsCommand')\";"/>
                <agiletags:ButtonOpperation commandName="CaptureGeneralBiometricsCommand" align="left" displayname="Save" span="1" type="button"  javaScript="onClick=\"submitWithAction('CaptureGeneralBiometricsCommand')\";"/>
            </tr>

        </agiletags:ControllerForm>
    </table>

    <!-- content ends here -->
    </td></tr></table>
</body>
</html>


<%--
    Document   : CaptureDiabetes
    Created on : 2010/05/13, 01:10:18
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
   <%@ page import="java.text.SimpleDateFormat" %>
   <%@ page import="java.util.Date" %>
   <%
   String revNo = (String)request.getAttribute("revNo");
   %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tooltest/style.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.tools.min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>
    </head>
    <body onload="CheckSearchCommands()">
    <%--    <div id="dialog-saved" title="Biometrics Saved"><p>Diabetes Biometrics has been saved. Reference No:<%=revNo%></p></div>
        <div id="dialog-notsaved" title="Biometrics Not Saved"><label class="error">Diabetes Biometrics save has failed!</label></div>
    --%>
        <table><tr valign="top"><td width="5"></td><td align="left">
                   <!-- content goes here -->
                   <agiletags:BiometricsTabs tabSelectedBIndex="link_DIA"/> 
                   <fieldset id="pageBorder">
                       <div class="" >
                    <!-- content goes here -->
                    
                    <br/><br/>
                    <label class="header">Diabetes Biometrics</label>

                    <table>
                        <tr>
                            <td>
                        <agiletags:ControllerForm name="CaptureDiabetes">
                            <input type="hidden" name="opperation" id="opperation" value=""/>
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="providerDetails" id="providerDetails" value="email" />
                            </td>
                        </tr>
                     <%--
                            <tr><agiletags:LabelTextSearchText displayName="Member Number" elementName="memberNumber" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand"  valueFromRequest="yes" valueFromSession="yes" onScreen="/biometrics/CaptureDiabetes.jsp" mandatory="yes" javaScript="onChange=\"getCoverByNumber(this.value, 'memberNumber');\""/></tr>
                            <tr><agiletags:EntityCoverDependantDropDown displayName="Cover Dependants" elementName="depListValues" javaScript="onChange=\"loadQuestions(this.value, 'depListValues');\"" mandatory="yes" valueFromSession="yes" errorValueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Scheme" elementName="schemeName" valueFromSession="yes" javaScript="" /></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Scheme Option" elementName="schemeOptionName" valueFromSession="yes" javaScript="" /></tr>

                            <tr id="exerciseQuestion"><agiletags:LabelGenericNumberDropDownError displayName="How Many times do you exercise in a week?" elementName="exerciseQuestion" errorValueFromSession="yes" mandatory="no" listSize="8"/></tr>
                            <tr id="smoke"><agiletags:LabelNeoLookupValueDropDownError displayName="Are you a current smoker?" elementName="currentSmoker" lookupId="67" javaScript="onChange=\"toggleSmokingQuestions();\"" mandatory="no" errorValueFromSession="yes"/></tr>
                            <tr id="ciganumber"><agiletags:LabelTextBoxError displayName="How many cigarettes do you smoke in a day?" elementName="smokingQuestion" valueFromSession="yes" mandatory="no"/></tr>
                            <tr id="exsmoke"><agiletags:LabelNeoLookupValueDropDownError displayName="Are you an ex smoker?" elementName="exSmoker" lookupId="67" javaScript="onChange=\"toggleExSmokingQuestion();\"" mandatory="no" errorValueFromSession="yes"/></tr>
                            <tr id="yearsstopped"><agiletags:LabelTextBoxError displayName="How many years since you have stopped" elementName="stopped" valueFromSession="yes"/></tr>
                            <tr id="alcoholConsumption"><agiletags:LabelGenericNumberDropDownError displayName="How many Alcohol units do you consume in a week?" elementName="alcoholConsumption" errorValueFromSession="yes" mandatory="no" listSize="21"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Weight (KG) e.g. 50.5" elementName="weight" valueFromSession="yes" mandatory="no" javaScript="onBlur=\"calculateBMI();\""/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Length (M) e.g. 1.65" elementName="length" valueFromSession="yes" mandatory="no" javaScript="onBlur=\"calculateBMI();\""/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="BMI" elementName="bmi" valueFromSession="yes" readonly="yes" mandatory="no"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Blood Pressure Systolic(mmHg)" elementName="bpSystolic" valueFromSession="yes" mandatory="no"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Blood Pressure Diastolic(mmHg)" elementName="bpDiastolic" valueFromSession="yes" mandatory="no"/></tr>

                            <tr><td><br/><br/></td></tr>

                            <tr><agiletags:LabelTextBoxDateTime displayName="Date Measured" elementName="dateMeasured" valueFromSession="yes" mandatory="yes"/></tr>
                            <tr><agiletags:LabelTextBoxDate displayname="Date Received" elementName="dateReceived" valueFromSession="yes" mandatory="yes"/></tr>
                            <tr><agiletags:LabelTextSearchText displayName="Treating Provider" elementName="providerNumber" valueFromSession="yes" searchFunction="yes" searchOperation="ForwardToSearchProviderCommand" onScreen="/biometrics/CaptureDiabetes.jsp" javaScript="onChange=\"validateProvider(this.value, 'providerNumber');\"" mandatory="no"/></tr>
                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Source" elementName="source" lookupId="104" mandatory="no" errorValueFromSession="yes"/></tr>
                     
                            <tr id="icdcode"><agiletags:LabelNeoLookupValueDropDownError displayName="ICD10" elementName="icd10" lookupId="115" javaScript="onBlur=\"loadICDDescription(this.value, 'icd10');\"" mandatory="yes" errorValueFromSession="yes"/></tr>
                            <tr id="showdescription"><agiletags:LabelTextDisplay displayName="ICD10 Description" elementName="icdDescriptionD" valueFromSession="yes" javaScript=""/></tr>
--%>
                            <tr><agiletags:LabelTextBoxtoolTip displayName="HB AIC(%)" elementName="diabetesHbAIC" valueFromSession="yes" mandatory="no" javaScript="onChange=\"setSession(this.value, 'diabetesHbAIC');\""/></tr>
                            <tr><agiletags:LabelTextBoxtoolTip displayName="Random Glucose (mmol/l)" elementName="diabetesRandomGlucose" valueFromSession="yes" mandatory="no" javaScript="onChange=\"setSession(this.value, 'diabetesRandomGlucose');\""/></tr>
                            <tr><agiletags:LabelTextBoxtoolTip displayName="Fasting Glucose (mmol/l)" elementName="diabetesFastingGlucose" valueFromSession="yes" mandatory="no" javaScript="onChange=\"setSession(this.value, 'diabetesFastingGlucose');\""/></tr>
                            <tr><agiletags:LabelTextBoxtoolTip displayName="Total Cholesterol (mmol/l)" elementName="diabetesTotalCholesterol" valueFromSession="yes" mandatory="no" javaScript="onChange=\"setSession(this.value, 'diabetesTotalCholesterol');\""/></tr>
                            <tr><agiletags:LabelTextBoxtoolTip displayName="LDLC(mmol/l)" elementName="diabetesldlc" valueFromSession="yes" mandatory="no" javaScript="onChange=\"setSession(this.value, 'diabetesldlc');\""/></tr>
                            <tr><agiletags:LabelTextBoxtoolTip displayName="HDLC(mmol/l)" elementName="diabeteshdlc" valueFromSession="yes" mandatory="no" javaScript="onChange=\"setSession(this.value, 'diabeteshdlc');\""/></tr>
                            <tr><agiletags:LabelTextBoxtoolTip displayName="Tryglyceride (mmol/l)" elementName="diabetesTryglyseride" valueFromSession="yes" mandatory="no" javaScript="onChange=\"setSession(this.value, 'diabetesTryglyseride');\""/></tr>
                            <tr><agiletags:LabelTextBoxtoolTip displayName="Proteinuria (Mg/l)" elementName="diabetesProteinuria" valueFromSession="yes" mandatory="no" javaScript="onChange=\"setSession(this.value, 'diabetesProteinuria');\""/></tr>
                            <tr><agiletags:LabelTextBoxtoolTip displayName="TC: HDL ratio" elementName="diabetesTchdlRation" valueFromSession="yes" mandatory="no" javaScript="onChange=\"setSession(this.value, 'diabetesTchdlRation');\""/></tr>
                            <tr><agiletags:LabelTextAreaError displayName="Detail" elementName="diabetesDetail" valueFromSession="yes" javaScript="onChange=\"setSession(this.value, 'diabetesDetail');\""/></tr>

                    <%--        <tr>
                                <agiletags:ButtonOpperation commandName="ResetDiabetesBiometrics" align="left" displayname="Reset" span="1" type="button"  javaScript="onClick=\"submitWithAction('ResetDiabetesBiometrics')\";"/>
                                <agiletags:ButtonOpperation commandName="CaptureDiabetesBiometericsCommand" align="left" displayname="Save" span="1" type="button"  javaScript="onClick=\"submitWithAction('CaptureDiabetesBiometericsCommand')\";"/>
                           </tr>
                    --%> 
                        </agiletags:ControllerForm>
                    </table>
                       </div>
                   </fieldset>
                    <!-- content ends here -->
        </td></tr></table>
    </body>
</html>


<%--
    Document   : CaptureOther
    Created on : 2010/05/13, 01:14:20
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<%
   String revNo = (String)request.getAttribute("revNo");
   System.out.println(session.getAttribute("other"));
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>  
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tooltest/style.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.tools.min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>

    </head>
        <body>

<%--        <div id="dialog-saved" title="Biometrics Saved"><p>Other Biometrics has been saved. Reverence No:<%=revNo%></p></div>
        <div id="dialog-notsaved" title="Biometrics Not Saved"><label class="error">Other Biometrics save has failed!</label></div>
--%>        <table><tr valign="top"><td width="5"></td><td align="left">
                   <!-- content goes here -->
                   <agiletags:BiometricsTabs tabSelectedBIndex="link_CDLOTH"/> 
                   <fieldset id="pageBorder">
                       <div class="" >
                    
                    <br/><br/>
                    <label class="header">Other</label><br/>
                    <label class="header">${sessionScope.other}</label>
                    <table>
                        <tr>
                            <td>
                        <agiletags:ControllerForm name="CaptureOther">
                            <input type="hidden" name="opperation" id="opperation" value=""/>
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="providerDetails" id="providerDetails" value="email" />
                            </td>
                        </tr>
                        
                        <tr><agiletags:LabelTextAreaError displayName="Details" elementName="otherDetails" valueFromSession="yes" javaScript="onChange=\"setSession(this.value, 'otherDetails');\""/></tr>
                        
<%--
                            <tr><agiletags:LabelTextSearchText displayName="Member Number" elementName="memberNumber" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand"  valueFromRequest="yes" valueFromSession="yes" onScreen="/biometrics/CaptureOther.jsp" mandatory="yes" javaScript="onChange=\"getCoverByNumber(this.value, 'memberNumber');\""/></tr>
                            <tr><agiletags:EntityCoverDependantDropDown displayName="Cover Dependants" elementName="depListValues" javaScript="" mandatory="Yes" valueFromSession="yes" errorValueFromSession="yes" /></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Scheme" elementName="schemeName" valueFromSession="yes" javaScript="" /></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Scheme Option" elementName="schemeOptionName" valueFromSession="yes" javaScript="" /></tr>

                            <tr><agiletags:LabelTextBoxError displayName="How Many times do you exercise in a week?" elementName="exerciseQuestion" valueFromSession="yes" mandatory="yes"/></tr>
                            <tr id="smoke"><agiletags:LabelNeoLookupValueDropDownError displayName="Are you a current smoker?" elementName="currentSmoker" lookupId="67" mandatory="yes" javaScript="onChange=\"toggleSmokingQuestions();\"" errorValueFromSession="yes"/></tr>
                            <tr id="ciganumber"><agiletags:LabelTextBoxError displayName="How many cigarettes do you smoke in a day?" elementName="smokingQuestion" valueFromSession="yes" mandatory="yes"/></tr>
                            <tr id="exsmoke"><agiletags:LabelNeoLookupValueDropDownError displayName="Are you an ex smoker?" elementName="exSmoker"  lookupId="67" mandatory="yes" javaScript="onChange=\"toggleExSmokingQuestion();\"" errorValueFromSession="yes"/></tr>
                            <tr id="yearsstopped"><agiletags:LabelTextBoxError displayName="How many years since you have stopped" elementName="stopped" valueFromSession="yes" mandatory="yes"/></tr>
                            <tr><agiletags:LabelTextBoxtoolTip displayName="How many Alcohol units do you consume in a week?" elementName="alcoholConsumption" valueFromSession="yes" mandatory="yes"/></tr>

                            <tr><agiletags:LabelTextBoxDateTime displayName="Date Measured" elementName="dateMeasured" valueFromSession="yes" mandatory="yes"/></tr>
                            <tr><agiletags:LabelTextBoxDate displayname="Date Received" elementName="dateReceived" valueFromSession="yes" mandatory="yes"/></tr>
                            <tr><agiletags:LabelTextSearchText displayName="Treating Provider" elementName="providerNumber" valueFromSession="yes" searchFunction="yes" searchOperation="ForwardToSearchProviderCommand" onScreen="/biometrics/CaptureOther.jsp" mandatory="yes" javaScript="onChange=\"validateProvider(this.value, 'providerNumber');\""/></tr>
                            <!--<tr><agiletags:LabelTextBoxError displayName="Source" elementName="source" valueFromSession="yes" readonly="yes" mandatory="yes"/></tr>-->
                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Source" elementName="source" lookupId="104" mandatory="yes" errorValueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Test Result" elementName="testResults" valueFromSession="yes" mandatory="yes" valueFromRequest="yes"/></tr>
                            <tr id="CDL"><agiletags:LabelNeoLookupValueDropDownError displayName="CDL" elementName="cdl" lookupId="233" javaScript="onBlur=\"toggleICD10(this.value, 'icd10code');\"" mandatory="no" errorValueFromSession="yes"/></tr>
                            <tr id="icdcode"><agiletags:LabelNeoIDCDropDownErrorReq displayName="ICD10"  elementName="icd10code"  mandatory="yes" errorValueFromSession="yes" parentElement="cdl" javaScript="onBlur=\"getICDDescription(this.value, 'icd10code');\""/></tr>
                           <tr id="showdescription"><agiletags:LabelTextDisplay displayName="ICD10 Description" elementName="icdDescription" valueFromSession="yes" javaScript=""/></tr>

    
                            <tr>
                                <td><agiletags:ButtonOpperation commandName="TariffCommand" align="left" displayname="Add New Tariff" span="1" type="button" javaScript="onClick=\"submitWithAction('redirect?')\";"/></td>
                            </tr>
--%>       
                        </agiletags:ControllerForm>
                    </table>
                       </div>
                   </fieldset>
                    <!-- content ends here -->
                </td></tr></table>
    </body>
</html>


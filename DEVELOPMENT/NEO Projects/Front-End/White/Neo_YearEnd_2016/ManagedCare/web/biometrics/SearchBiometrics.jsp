<%-- 
    Document   : SearchBiometrics
    Created on : 2013/10/06, 05:48:39
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Biometrics</title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" language="JavaScript">

            $(document).ready(function () {
                var resultTxt = $("#biometricResultMsg").val();
                if (resultTxt != null && resultTxt != "" && resultTxt != "null") {
                    $("#biometricResultMsgTxt").text(resultTxt);
                } else {
                    $("#biometricResultMsgTxt").text("");
                }
                var searchVal = document.getElementById('searchCalled').value;
                if (searchVal == 'memberNum') {
                    getCoverByNumber(document.getElementById('memberNum_text').value, 'CoverDependantResultCommand');
                } else if ((searchVal == '') || (searchVal = null))
                {
                    document.getElementById('tblTags').style.visibility = 'hidden';
                }
                document.getElementById('searchCalled').value = 'antiReload';
                hideAll();
            });

//            function showOrHide(index) {
//                hideAll();
//                var element = "#rowDiv" + index;
//                $(element).show();
//            }

            function hideAll() {
                for (var i = 1; i < 12; i++) {
                    var element = "#rowDiv" + i;
                    $(element).hide();
                }
            }

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.getElementById('onScreen').value = "MemberCCLogin";
                document.forms[0].submit();
            }

            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function ClearValidation() {
                $("#memberNum_error").text("");
                $("#dateFrom_error").text("");
                $("#dateTo_error").text("");
            }

            function submitWithActionForm(action) {
                var memberNo = $("#memberNum_text").val();
                var depCode = $("#depListValues").val();
                var dateFrom = $("#dateFrom").val();
                var dateTo = $("#dateTo").val();

                var valid = true;
                ClearValidation();

                if (memberNo == null || memberNo == "" || memberNo == "null") {
                    $("#memberNum_error").text("Please enter member number");
                    valid = false;
                } else {
                    if (depCode == null || depCode == ""
                            || depCode == "null" || depCode == "99") {
                        $("#depListValues_error").text("Please select a dependant");
                        valid = false;
                    } else {
                        if (dateFrom != null && dateFrom != "" && dateFrom != "null") {
                            valid = validateDate(dateFrom);
                            if (!valid) {
                                $("#dateFrom_error").text("Incorrect Date Format");
                            }
                            if (dateTo == null || dateTo == "" || dateTo == "null") {
                                valid = false;
                                $("#dateTo_error").text("Please enter to date");
                            }
                        }
                        if (dateTo != null && dateTo != "" && dateTo != "null") {
                            valid = validateDate(dateTo);
                            if (!valid) {
                                $("#dateTo_error").text("Incorrect Date Format");
                            }
                            if (dateFrom == null || dateFrom == "" || dateFrom == "null") {
                                valid = false;
                                $("#dateFrom_error").text("Please enter to date");
                            }
                        }
                    }
                }

                if (valid) {
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }
            }

            function GetXmlHttpObject() {
                if (window.ActiveXObject) {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function GetDOMParser(xmlStr) {
                var xmlDoc;
                if (window.DOMParser) {
                    parser = new DOMParser();
                    xmlDoc = parser.parseFromString(xmlStr, "text/xml");
                } else {
                    xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async = "false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }

            //Grid selected
            function tblSelect(ID, command) {
                var memberNo = $("#memberNum_text").val();
                var data = {
                    'DepCode_text': ID,
                    'memberNo_text': memberNo,
                    'opperation': command,
                    'id': new Date().getTime()

                };

                var url = "/ManagedCare/AgileController";
                $.get(url, data, function (resultTxt) {
                    if (resultTxt !== null) {
                        var resTxtArr = resultTxt.split("|");
                        var result = resTxtArr[0];
                        if (result === "Done") {
                            submitScreenRefresh("/biometrics/BiometricsView.jsp");
                        } else {
                        }
                    }
                });
            }

            //member search
            function getCoverByNumber(ID, command) {
                var dateFrom = $("#dateFrom").val();
                var dateTo = $("#dateTo").val();

                if (ID == null || ID == "" || ID == "null") {
                    $("#policyNumber_error").text("Please enter member number");
                } else
                {
                    if (ID != '')
                    {
                        document.getElementById('tblTags').style.visibility = 'visible';
                        document.getElementById('searchCalled').value = 'policyNumber';
                    } else
                    {
                        document.getElementById('tblTags').style.visibility = 'hidden';
                        document.getElementById('searchCalled').value = '';
                    }
                    var url = "/ManagedCare/AgileController";
                    var data = {
                        'opperation': command,
                        'ID_text': ID,
                        'dateFrom_text': dateFrom,
                        'dateTo_text': dateTo,
                        'id': new Date().getTime()
                    };

                    $.get(url, data, function (resultTxt) {
                        if (resultTxt !== null) {
                            var resTxtArr = resultTxt.split("|");
                            var result = resTxtArr[0];
                            if (result === "Done") {
                                submitScreenRefresh("/biometrics/SearchBiometrics.jsp");
                            } else {
                            }
                        } else {
                        }
                    });

                    data = null;
                }
            }

            function submitScreenRefresh(screen) {
                $("#opperation").val("RefreshCommand");
                $("#refreshScreen").val(screen);
                document.forms[0].submit();
            }

            function validateDate(dateString) {

                //  check for valid numeric strings
                var strValidChars = "0123456789/";
                var strChar;
                var strChar2;

                if (dateString.length < 10 || dateString.length > 10)
                    return false;

                //  test strString consists of valid characters listed above
                for (i = 0; i < dateString.length; i++)
                {
                    strChar = dateString.charAt(i);
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        return false;
                    }
                }

                // test if slash is at correct position (yyyy/mm/dd)
                strChar = dateString.charAt(4);
                strChar2 = dateString.charAt(7);
                if (strChar != '/' || strChar2 != '/')
                    return false;

                return true;
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="left"><td width="5px"></td><td align="left">
                    <!-- content goes here -->
                    <agiletags:ControllerForm name="searchBiometricDetails">
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <agiletags:HiddenField elementName="searchCalled" />
                        <input type="hidden" name="biometricResultMsg" id="biometricResultMsg" value="${requestScope.biometricResultMsg}" />
                        <input type="hidden" name="onScreen" id="onScreen" value="" />
                        <input type="hidden" name="refreshScreen" id="refreshScreen" value="" />
                        <input type="hidden" name="exactCoverNum" id="exactCoverNum" value="1" />
                        <label class="header">Search Member</label>
                        <br/><br/>
                        <table>
                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Member Number" elementName="memberNum" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/biometrics/SearchBiometrics.jsp" mandatory="yes" javaScript="onBlur=\"getCoverByNumber(this.value, 'CoverDependantResultCommand');\""/></tr>
                        </table>
                        <table id="tblTags" width=100% height=100%>
                            <tr valign="top">
                                <td width="50px" align="left">
                                    <br/>  
                                    <!--                        <br/>
                                                            <table>
                                                                <tr><agiletags:DropDownPerPatientLabel displayName="CDL" elementName="cdl" lookupId="100"  sessionValue="bioList" javaScript="onchange=\"showOrHide(this.value);\""/></tr>
                                                            </table>
                                                            <br/>-->
                                    <br/>
                                    <label class="header">Cover Dependants</label>   
                                    <br/>          
                                    <br/>
                                    <HR color="#666666" WIDTH="100%" align="left">
                                    <br/>
                                    <agiletags:CoverDependantResultTag sessionAttribute="" commandName="" alwaysShowButton="yes" javaScript="onClick=\"tblSelect(this.value,'CoverDependendGridSelectionCommand');\"" onScreen="" buttonDisplay="View" onPage="SearchB"/> 
                                    <br/>
                                    <br/>
                                </td>
                            </tr>
                        </table>
                        <label id="biometricResultMsgTxt" class="red"></label></br></br>
                        <div id="rowDiv1">
                            <br/>
                            <label class="header">Asthma</label>
                            <br/><br/>
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricAsthmaTable/>
                            </table>
                        </div>

                        <div id="rowDiv2">
                            <br/>
                            <label class="header">Hypertension</label>
                            <br/><br/>
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricHypertensionTable/>
                            </table>
                        </div>

                        <div id="rowDiv3">
                            <br/>
                            <label class="header">Chronic renal Disease</label>
                            <br/><br/>
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricChronicRenalDiseaseTable/>
                            </table>
                        </div>

                        <div id="rowDiv4">
                            <br/>
                            <label class="header">Cardiac Failure/Cardio Myopathy</label>
                            <br/><br/>
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricCardiacFailureAndCardiacMyothy/>
                            </table>
                        </div>

                        <div id="rowDiv5">
                            <br/>
                            <label class="header">Biometric Diabetes</label>
                            <br/><br/>
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricDiabetesTable/>
                            </table>
                        </div>

                        <div id="rowDiv6">
                            <br/>
                            <label class="header">Coronary Artery Disease</label>
                            <br/><br/>
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricCoronaryArteryDiseaseTable/>
                            </table>
                        </div>

                        <div id="rowDiv7">
                            <br/>
                            <label class="header">COPD and Bronchiectasis</label>
                            <br/><br/>
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricCOPDandBronchiectasisTable/>
                            </table>
                        </div>

                        <div id="rowDiv8">
                            <br/>
                            <label class="header">Hyperlipidaemia</label>
                            <br/><br/>
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricHyperlipidaemiaTable/>
                            </table>
                        </div>

                        <div id="rowDiv9">
                            <br/>
                            <label class="header">Major Depression</label>
                            <br/><br/>
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricMajorDepressionTable/>
                            </table>
                        </div>

                        <%--<div id="10" style="display:none">
                            <br/>
                            <label class="header">Policy Holder - Pathology</label>
                            <br/><br/>
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:PathologyHistoryTable/>
                            </table>
                        </div> --%>

                        <div id="rowDiv10">
                            <br/>
                            <label class="header">Biometric General</label>
                            <br/><br/>
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricsGeneralTable/>
                            </table>
                        </div>

                        <div id="rowDiv11">
                            <br/>
                            <label class="header">Biometric Other</label>
                            <br/><br/>
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:BiometricsOtherTable/>
                            </table>
                        </div>
                    </agiletags:ControllerForm>
                </td></tr></table>
    </body>
</html>
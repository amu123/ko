<%--
    Document   : BiometricsSpecificTariff
    Created on : 2010/06/14, 03:19:31
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script language="JavaScript">


            function getTariffForCode(){
                var str = document.getElementById("tariffCode").value;
                xmlhttp=GetXmlHttpObject();
                xmlhttp.overrideMimeType('text/xml');
                if(xmlhttp==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=GetAuthTariffByCodeCommand&tariffCode="+str;

                xmlhttp.onreadystatechange=function(){stateChanged("")};
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function CalculateTariffQuantity(){
                var q = document.getElementById("tariffQuantity").value;
                var p = document.getElementById("tariffQuantity").value;
                xmlhttp=GetXmlHttpObject();
                xmlhttp.overrideMimeType('text/xml');
                if(xmlhttp==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }

                var url="/ManagedCare/AgileController";
                url=url+"?opperation=CalculateTariffQuantityCommand&tariffQ="+q;
                xmlhttp.onreadystatechange=function(){stateChanged("")};
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function ModifyTariffFromList(index){
                var x = document.forms[index];
                var tQuan = new String();
                var tCode = new String();
                for (var i=0; i<x.length; i++)
                {
                    if (x.elements[i].id == "tTableQuan")
                        tQuan = x.elements[i].value;

                    if (x.elements[i].id == "tTableCode")
                        tCode = x.elements[i].value;
                }

                xmlhttp=GetXmlHttpObject();
                xmlhttp.overrideMimeType('text/xml');
                if(xmlhttp==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=ModifyAuthTariffCommand&tTableQuan="+tQuan+"&tTableCode="+tCode;
                alert(url);
                xmlhttp.onreadystatechange=function(){stateChanged("")};
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }
            function RemoveTariffFromList(index){
                var x = document.forms[index];
                var tQuan = new String();
                var tCode = new String();
                for (var i=0; i<x.length; i++)
                {
                    if (x.elements[i].id == "tTableQuan")
                        tQuan = x.elements[i].value;

                    if (x.elements[i].id == "tTableCode")
                        tCode = x.elements[i].value;
                }

                xmlhttp=GetXmlHttpObject();
                xmlhttp.overrideMimeType('text/xml');
                if(xmlhttp==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }

                var url="/ManagedCare/AgileController";
                url=url+"?opperation=RemoveAuthTariffCommand&tTableQuan="+tQuan+"&tTableCode="+tCode;
                xmlhttp.onreadystatechange=function(){stateChanged("")};
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }
            function ClearTariffFieldsFromSession(){
                xmlhttp=GetXmlHttpObject();
                xmlhttp.overrideMimeType('text/xml');
                if(xmlhttp==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }

                var url="/ManagedCare/AgileController";
                url=url+"?opperation=ClearAuthTariffCommand";
                xmlhttp.onreadystatechange=function(){stateChanged("clearTariffFields")};
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }
            function stateChanged(element){

                if (xmlhttp.readyState==4)
                {
                    //alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    //alert(result);
                    if(result == "GetAuthTariffByCodeCommand"){

                        var resText = xmlhttp.responseText;
                        var strArr = new Array();
                        strArr = resText.split('|');
                        var tCode = new String();
                        tCode = strArr[1];
                        var tDesc = new String();
                        tDesc = strArr[2];
                        var tResult = new String();
                        tAmount = strArr[3];
                        var tQuan = new String();
                        tQuan = strArr[4];

                        document.getElementById("tariffDescription").value = tDesc;
                        document.getElementById("tariffCode").value = tCode;
                        document.getElementById("testResult").value = tResult;
                       
                    }else if(result == "CalculateTariffQuantityCommand"){
                        var resText = xmlhttp.responseText;
                        var strArr = new Array();
                        strArr = resText.split('|');

                        var tQuan = new String(strArr[1]);
                        var tAmount = new String(strArr[2]);

                        document.getElementById("tariffDescription").value = tDesc;
                        document.getElementById("testResult").value = tResult;


                    }else if(result == "ModifyAuthTariffCommand"){
                        var resText = xmlhttp.responseText;
                        var strArr = new Array();
                        strArr = resText.split('|');
                        var tCode = new String();
                        tCode = strArr[1];
                        var tDesc = new String();
                        tDesc = strArr[2];
                        var tAmount = new String();
                        tAmount = strArr[3];
                        var tQuan = new String();
                        tQuan = strArr[4];

                        document.getElementById("tariffDesc").value = tDesc;
                        document.getElementById("tariffCode").value = tCode;
                        document.getElementById("tariffAmount").value = tAmount;
                        document.getElementById("tariffQuantity").value = tQuan;


                    }else if(result == "RemoveAuthTariffCommand"){

                            $.find('#tariffRow').hide().slideUp('slow').fadeOut();
                            $.remove('#tariffRow');

                    }else if(result == "ClearTariffFieldsFromSession"){
                        document.getElementById("tariffDesc").value = '';
                        document.getElementById("tariffCode").value = '';
                        document.getElementById("tariffQuantity").value = '';
                        document.getElementById("tariffAmount").value = '';


                    }else if(result == "Error"){
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|')+1, xmlhttp.responseText.length);
                        if(forCommand == "GetProviderByNumberCommand"){
                            document.getElementById(element + '_text').value = '';
                            document.getElementById(element).value = '';
                        } else if(forCommand == "GetICD10ByCodeCommand"){
                            document.getElementById(element + '_text').value = '';
                            document.getElementById(element).value = '';
                        }
                    }
                }
            }

            function clearElement(element) {
                part = element + '_text';
                document.getElementById(element).value = '';
                document.getElementById(part).value = '';
            }
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <label class="header">Biometrics Specific Tariff Details</label>
                    <br></br><br></br>
                    <table>
                        <agiletags:ControllerForm name="saveAuthTariff" validate="yes">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="searchCalled" id="searchCalled" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <tr><agiletags:BiometricsTarrifTag commandName="AddBiometricsTariffToSessionCommand" javaScript="onClick=\"submitWithAction('AddBiometricsTariffToSessionCommand')\";" /></tr>
                            <br></br>
                            <tr><agiletags:BiometricsTariffListTableTag sessionAttribute="biometricsTariffListArray" /></tr>
                            <tr><agiletags:ButtonOpperation align="right" commandName="" displayname="Done" span="1" type="button" javaScript="onClick=\"submitWithAction('ReturnBiometricsTariffCommand')\";" /></tr>
                        </agiletags:ControllerForm>
                    </table>
        </td></tr></table>
    </body>
</html>

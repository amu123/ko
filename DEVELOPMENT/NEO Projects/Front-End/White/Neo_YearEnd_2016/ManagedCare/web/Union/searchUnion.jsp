<%-- 
    Document   : searchUnion
    Created on : Jun 9, 2016, 2:36:23 PM
    Author     : shimanem
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" language="JavaScript">
            function submitUnionDetails(unionID, unionNumber, unionName, representativeNumber, opperation) {
                //alert("View Union: " + unionNumber + " - " + unionOptionName);
                document.getElementById("unionID").value = unionID;
                document.getElementById("unionNumber").value = unionNumber;
                document.getElementById("unionName").value = unionName;
                document.getElementById("representativeNumber").value = representativeNumber;
                document.getElementById("opperation").value = opperation;
                document.forms[0].submit();
            }

            function submitWithAction(action) {
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%>
            <tr valign="top"><td width="5px"></td><td align="left">

                        <br/><br/>
                        <label class="header">Search Union</label>
                        <br/><br/>
                        <agiletags:ControllerForm name="unionSearchForm">
                            <input type="hidden" name="opperation" id="opperation" value=""/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="unionNumber" id="unionNumber" value="" />
                            <input type="hidden" name="unionName" id="unionName" value="" />
                            <input type="hidden" name="representativeNumber" id="representativeNumber" value="" />
                            <input type="hidden" name="unionID" id="unionID" value="" />
                            <table>
                                <tr><agiletags:LabelTextBoxError displayName="Union Number" elementName="_unionNumber"/><tr>
                                <tr><agiletags:LabelTextBoxError displayName="Union Name" elementName="_unionName"/><tr>      
                                <tr><agiletags:ButtonOpperationLabelError displayName="Search" elementName="unionSearchBtn" type="submit" commandName="" javaScript="onClick=\"submitWithAction('UnionSearchCommand')\";"/>
                            </table>
                            <br/>
                            <agiletags:UnionSearchResultsTag/>
                        </agiletags:ControllerForm>

                </td></tr>
        </table>
    </body>
</html>
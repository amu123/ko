<%-- 
    Document   : UnionNotesDetails
    Created on : Jul 29, 2016, 4:05:02 PM
    Author     : shimanem
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<br>
<label class="subheader">Union Application Notes Details</label>
<hr>
<br>
<%--<c:if test="${persist_user_viewMemApps == false}">--%>
    <c:if test="${!(noteTypeSelectID == '99' || noteTypeSelectID == '5')}">
        <input type="button" name="AddNotesButton" value="Add Note" onclick="document.getElementById('unionNotes_button_pressed').value = this.value;
                getPageContents(this.form, null, 'main_div', 'overlay'); swapDivVisbible('main_div', 'overlay')"/>
        <br><br>    
    </c:if>
<%--</c:if>--%>
<c:choose>
    <c:when test="${empty UnionNotesDetails}">
        <span class="label">No Notes available</span>
    </c:when>
    <c:otherwise>
        <table width="90%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
            <tr>
                <th>Date</th>
                    <c:if test="${noteTypeSelectID == '5'}">
                    <th>Type</th>
                    </c:if>
                <th>User</th>
                <th>Note</th>
                <th></th>
            </tr>
            <c:forEach var="entry" items="${UnionNotesDetails}">
                <tr>
                    <td><label>${agiletags:formatXMLGregorianDate(entry['noteDate'])}</label></td>
                    <c:if test="${noteTypeSelectID == '5'}">
                        <td><label>${entry['noteGroup']}</label></td>
                    </c:if>
                    <td><label>${entry['noteUser']}</label></td>
                    <td><label>${entry['noteDetails']}</label></td>
                    <td><input type="button" value="View" name="ViewButton" onclick="document.getElementById('union_note_id').value =${entry['noteId']};
                            document.getElementById('unionNotes_button_pressed').value = 'ViewButton';
                            getPageContents(this.form, null, 'main_div', 'overlay');"></td>
                </tr>
            </c:forEach>
        </table>
    </c:otherwise>
</c:choose>		
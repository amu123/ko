<%-- 
    Document   : newUnion
    Created on : Jun 9, 2016, 2:36:44 PM
    Author     : shimanem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" language="JavaScript">
            $(document).ready(function () {
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });

            function submitWithAction(action) {
                console.log("Validate Status : " + validate());
                if (validate() === true) {                    
                    document.getElementById("opperation").value = action;
                    document.forms[0].submit();
                }
            }

            function validate() {
                $("#unionName_error").text("");
                $("#repName_error").text("");
                $("#repSurname_error").text("");
                $("#unionRepCapacity_error").text("");
                $("#unionRegion_error").text("");
                $("#unionInceptionDate_error").text("");
                $("#unionTerminationDate_error").text("");
                $("#contactNumber_error").text("");
                $("#emailAddress_error").text("");
                var valid = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if ($("#unionName").val() === "") {
                    $("#unionName_error").text("Please fill in the Union Name");
                    return false;
                }
                if ($("#repName").val() === "") {
                    $("#repName_error").text("Please fill in the Representative Name");
                    return false;
                }
                if ($("#repSurname").val() === "") {
                    $("#repSurname_error").text("Please fill in the Representative Surname");
                    return false;
                }
                if ($("#unionRepCapacity").val() === "") {
                    $("#unionRepCapacity_error").text("Please choose the Union Representative Capacity");
                    return false;
                }
                if ($("#unionRegion").val() === "") {
                    $("#unionRegion_error").text("Please select the Region");
                    return false;
                }
                if ($("#unionInceptionDate").val() === "") {
                    $("#unionInceptionDate_error").text("Please choose the Inception Date");
                    return false;
                }
                if ($("#unionTerminationDate").val() === "") {
                    $("#unionTerminationDate_error").text("Please choose the Termination Date");
                    return false;
                }
                if ($("#contactNumber").val() === "") {
                    $("#contactNumber_error").text("Please fill the Telephone Number");
                    return false;
                }
                if ($("#emailAddress").val() === "" || !valid.test($("#emailAddress").val())) {
                    $("#emailAddress_error").text("Please enter a valid email address");
                    return false;
                }
                
                return true;
            }

            <%
                String PREFIX = "SCB";
                String SUFFIX = "UN";
                int random = (int) (1 + Math.floor(Math.random() * 99999));
                String numericString = PREFIX + random + SUFFIX;
                request.setAttribute("newUnionNumber", numericString);
            %>
        </script>
    </head>
    <body>
        <agiletags:ControllerForm name="newUnionForm">

            <input type="hidden" name="opperation" id="opperation" />
            <input type="hidden" name="onScreen" id="onScreen" value="" />
            <input type="hidden" name="unionNumber" id="unionNumber" value="${requestScope.newUnionNumber}" />
            <input type="hidden" name="altUnionNumber" id="altUnionNumber" value="${requestScope.newUnionNumber}" />
            <div class="main_div">
                <table width=100% height=100%>
                    <tr valign="top">
                        <td width="50px"></td>
                        <td align="left">                          
                            <fieldset id="pageBorder">
                                <!-- content goes here -->
                                <br/><label class="header">Union Details</label><br/>  
                                <HR color="#666666" WIDTH="100%" align="left">
                                <table>
                                    <tr><agiletags:LabelTextBoxErrorReq displayName="Union Name" elementName="unionName" mandatory="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="Yes" displayName="Union Number" elementName="unionNumber" valueFromRequest="newUnionNumber" /></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="Yes" displayName="Alternative Union Number" elementName="altUnionNumber" valueFromRequest="newUnionNumber" /></tr>
                                    <tr><agiletags:LabelTextBoxErrorReq displayName="Representative Name" elementName="repName" mandatory="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxErrorReq displayName="Representative Surname" elementName="repSurname" mandatory="yes"/></tr>
                                    <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Capacity of Representative"  elementName="unionRepCapacity" lookupId="347" mandatory="yes" firstIndexSelected="no" javaScript="" errorValueFromSession="yes" /></tr>
                                    <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Region"  elementName="unionRegion" lookupId="251" mandatory="yes" firstIndexSelected="no" javaScript="" errorValueFromSession="yes" /></tr>
                                    <tr><agiletags:LabelTextBoxDateReq displayname="Inception Date" elementName="unionInceptionDate" mandatory="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxDateReq displayname="Termination Date" elementName="unionTerminationDate" mandatory="yes"/></tr>                               
                                </table>

                                <br/><br/>
                                <label class="header">Contact Details</label><br/>
                                <HR color="#666666" WIDTH="100%" align="left">
                                <table>
                                    <tr><agiletags:LabelTextBoxErrorReq displayName="Telephone Number" elementName="contactNumber" mandatory="yes" numericValidation="yes"/></tr>                         
                                    <tr><agiletags:LabelTextBoxError displayName="Fax Number" elementName="faxNumber" numericValidation="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxErrorReq displayName="Email Address" elementName="emailAddress" mandatory="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Alternative Email Address" elementName="altEmailAddress"/></tr>                              
                                </table>

                                <br/><br/>
                                <label class="header">Postal Address</label><br/>   
                                <HR color="#666666" WIDTH="100%" align="left">
                                <table>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 1" elementName="postLine1"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 2" elementName="postLine2"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 3" elementName="postLine3"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Location" elementName="postLocation"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Code" elementName="postCode" numericValidation="yes"/></tr>
                                </table>

                                <br/><br/>
                                <label class="header">Physical Address</label><br/>
                                <HR color="#666666" WIDTH="100%" align="left">
                                <table>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 1" elementName="physicalLine1"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 2" elementName="physicalLine2"/></tr> 
                                    <tr><agiletags:LabelTextBoxError displayName="Line 3" elementName="physicalLine3"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Location" elementName="physicalLocation"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Code" elementName="physicalCode" numericValidation="yes"/></tr>
                                </table>

                                <br/><HR color="#666666" WIDTH="100%" align="left"><br/>
                                <table>
                                    <tr>
                                        <agiletags:ButtonOpperationLabelError displayName="Reset" elementName="unionContactResetBtn" type="button" commandName="" javaScript=""/>
                                        <agiletags:ButtonOpperationLabelError displayName="Save" elementName="unionContactSaveBtn" type="button" commandName="" javaScript="onClick=\"submitWithAction('UnionCapturerCommand')\";"/>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </div>
        </agiletags:ControllerForm>  
    </body>
</html>

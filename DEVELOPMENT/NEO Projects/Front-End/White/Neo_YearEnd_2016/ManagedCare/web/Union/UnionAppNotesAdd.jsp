<%-- 
    Document   : UnionAppNotesAdd
    Created on : Jul 29, 2016, 4:47:27 PM
    Author     : shimanem
--%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<agiletags:ControllerForm name="UnionAppNotesAddForm">
    <input type="hidden" name="opperation" id="opperation" value="UnionNotesCommand" />
    <input type="hidden" name="unionID" id="union_note_id" value="${sessionScope.unionID}" />
    <input type="hidden" name="repID" id="rep_id" value="${sessionScope.representativeNumber}" />
    <input type="hidden" name="noteTypeSelectID" id="unionAppNoteslistSelect" value="${requestScope.noteTypeSelectID}" />
    <input type="hidden" name="target_div" value="${target_div}" />
    <input type="hidden" name="main_div" value="${main_div}" />
    <input type="hidden" name="buttonPressed" id="unionNotes_button_pressed" value="SaveButton" />

    <label class="header">${empty noteId ? 'Add' : 'View'} Note</label>
    <hr>
    <br>
    <table>
        <tr>
            <td align="left" width="160px"><label>Note Details :</label></td>
            <td align="left">
                <c:choose>
                    <c:when test="${empty noteId}">
                        <textarea class="ckeditor" name="UnionAppNotesAdd_details" id="UnionAppNotesAdd_details" rows="20" cols="50">${noteDetails}</textarea>
                        <script type="text/javascript">
                            jQuery(document).ready(function () {
                                CKEDITOR.replace('UnionAppNotesAdd_details',
                                        {
                                            toolbar: 'Basic'
                                        });
                            });
                        </script>
                    </c:when>
                    <c:otherwise>
                        <label>${noteDetails}</label>
                    </c:otherwise>
                </c:choose>
                <c:if test="${!empty errorMessage}">
                    <label class="red">${errorMessage}</label>
                </c:if>
            </td>
        </tr>
    </table>

    <hr>
    <table id="UnionAppNoteDetailsSaveTable">
        <tr>
            <td><input type="button" value="${empty noteId ? 'Cancel' : 'Close'}" onclick="swapDivVisbible('overlay', 'main_div');"></td>
                <c:if test="${empty noteId}">
                <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'UnionNotesDetails', null, 'main_div', 'overlay');"></td>
                </c:if>
        </tr>
    </table>
</agiletags:ControllerForm>

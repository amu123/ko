<%-- 
    Document   : unionGroupHistory
    Created on : Jun 9, 2016, 2:37:39 PM
    Author     : shimanem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" language="JavaScript">

            $(document).ready(function () {
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>

        <div id="main_div">            

            <table width=100% height=100%><tr valign="top">
                    <td width="5px"></td>
                    <td align="left">

                        <agiletags:UnionTabs tabSelectedIndex="link_union_GH" />
                        <agiletags:ControllerForm name="unionGroupHistoryForm">

                            <input type="hidden" name="opperation" id="opperation" value=""/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="unionNumber" id="unionNumber" value="" />
                            <input type="hidden" name="unionOptionName" id="unionName" value="" />

                            <fieldset id="pageBorder">
                                <div class="header">
                                    <table width="100%" bgcolor="#dddddd"  style=" border: 1px solid #c9c3ba;">
                                        <tr>
                                            <td class="label" width="200"><span><label><b>Union Name :</b></label> <label id="unionName" > ${sessionScope.unionName}</label></span></td>
                                            <td class="label" width="200"><b>Union Number :</b> ${sessionScope.unionNumber}</td>
                                            <td class="label" width="200"><b>Alternative Union Number :</b> ${sessionScope.unionNumber}</td>
                                        </tr>
                                    </table>
                                </div><br/>

                                <!-- content goes here -->
                                <label class="header">Group History</label>  
                                <br/><br/>
                                <HR color="#666666" WIDTH="100%" align="left">
                                <br/>  
                                <label class="subheader">Group History Results</label>
                                <hr>

                                <c:choose>
                                    <c:when test="${empty sessionScope.unionGroupHistory}">
                                        <span class="label">No Employer Group history found</span>
                                    </c:when>
                                    <c:otherwise>
                                        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                            <tr>
                                                <th align="left">Group Number</th>
                                                <th align="left">Group Name</th>
                                          <!--  <th align="left">Union Number (Linked)</th>
                                                <th align="left">Union Name (Linked)</th>
                                                <th align="left">Representative (Linked)</th>-->
                                                <th align="left">Start Date</th>
                                                <th align="left">End Date</th>                             
                                                <th>&nbsp;</th>
                                            </tr>
                                            <c:forEach var="entry" items="${sessionScope.unionGroupHistory}">
                                                <tr>
                                                    <td><label class="label">${entry.groupNumber}</label></td>
                                                    <td><label class="label">${entry.groupName}</label></td>
                                              <%--  <td><label class="label">${entry.unionNumber}</label></td>
                                                    <td><label class="label">${entry.unionName}</label></td>
                                                    <td><label class="label">${entry.name}&nbsp;${entry.surname}</label></td>--%>
                                                    <td><label class="label">${agiletags:formatXMLGregorianDate(entry.inceptionDate)}</label></td>
                                                    <td><label class="label">${agiletags:formatXMLGregorianDate(entry.terminationDate)}</label></td>
                                                </tr>
                                            </c:forEach>
                                        </table>
                                    </c:otherwise>
                                </c:choose>                              
                            </fieldset>
                        </agiletags:ControllerForm>
                    </td>
            </table>
        </div>
    </body>
</html>
<%-- 
    Document   : MemberAddressDetails
    Created on : 2012/05/14, 10:05:53
    Author     : johanl
--%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<div id="tabTableLayout">
    <agiletags:ControllerForm name="MemberAddressDetailsForm">
        <input type="hidden" name="opperation" id="MemberAddress_opperation" value="SaveMemberAddressDetailsCommand" />
        <input type="hidden" name="onScreen" id="MemberAddress_onScreen" value="MemberAddressDetails" />

        <label class="subheader">Member Address Details</label>
        <HR WIDTH="80%" align="left">
        <br/>
        <label class="subheader">Physical Address</label>
        <table id="PhysicalAddressTable">
            <tr id="MemberDetails_physicalAddressLine1Row"><agiletags:LabelTextBoxErrorReq displayName="Line1" elementName="AddressDetails_res_addressLine1" valueFromRequest="AddressDetails_res_addressLine1" mandatory="no"/>        </tr>
            <tr id="MemberDetails_physicalAddressLine2Row"><agiletags:LabelTextBoxErrorReq displayName="Line2" elementName="AddressDetails_res_addressLine2" valueFromRequest="AddressDetails_res_addressLine2" mandatory="no"/>        </tr>
            <tr id="MemberDetails_physicalAddressLine3Row"><agiletags:LabelTextBoxErrorReq displayName="Line3" elementName="AddressDetails_res_addressLine3" valueFromRequest="AddressDetails_res_addressLine3" mandatory="no"/>        </tr>
            <tr id="MemberDetails_physicalCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Physical Code" elementName="AddressDetails_res_postalCode" valueFromRequest="AddressDetails_res_postalCode" mandatory="no"/>        </tr>
        </table>
        <br/>    
        <label class="subheader">Postal Address</label>
        <table id="PostalAddressTable">
            <tr id="MemberDetails_postalAddressLine1Row"><agiletags:LabelTextBoxErrorReq displayName="Line1" elementName="AddressDetails_pos_addressLine1" valueFromRequest="AddressDetails_pos_addressLine1" mandatory="no"/>        </tr>
            <tr id="MemberDetails_postalAddressLine2Row"><agiletags:LabelTextBoxErrorReq displayName="Line2" elementName="AddressDetails_pos_addressLine2" valueFromRequest="AddressDetails_pos_addressLine2" mandatory="no"/>        </tr>
            <tr id="MemberDetails_postalAddressLine3Row"><agiletags:LabelTextBoxErrorReq displayName="Line3" elementName="AddressDetails_pos_addressLine3" valueFromRequest="AddressDetails_pos_addressLine3" mandatory="no"/>        </tr>
            <tr id="MemberDetails_postalCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Postal Code" elementName="AddressDetails_pos_postalCode" valueFromRequest="AddressDetails_pos_postalCode" mandatory="no"/>        </tr>
        </table>
        <br/>
        <table id="MemberPersonalSaveTable">
            <tr>
                <td><input type="reset" value="Reset"></td>
                <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'MemberAddressDetails')"></td>
            </tr>
        </table>

    </agiletags:ControllerForm>
</div>

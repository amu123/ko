<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<agiletags:ControllerForm name="ReInstateMemberForm">
    <input type="hidden" name="opperation" id="opperation" value="SaveMemberCoverDetailsCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="SuspendMember" />
    <input type="hidden" name="buttonPressed" id="disorderValuesButtonPressed" value="" />
    <input type="hidden" name="memberEntityId" id="ReInstateEntityId" value="${memberCoverEntityId}" />
    <input type="hidden" name="brokerEntityId" id="brokerEntityId" value="${requestScope.brokerEntityId}" />
    <br/>
    <br/>
    <label class="header">Re-Instate Member</label>
    <br/>
    <br/>
    <hr>
    <br/>
    <br/>
    <table>                                
        <tr id="suspensionReasonRow">
            <td align="left" width="160px"><label>Dependant</label></td><td align="left" width="200px">
                <select style="width:215px" name="deptList" id="deptList" width="200px">                  
                    <c:forEach var="entry" items="${MemberCoverDependantDetails}">
                        <c:if test="${entry.status == 'Resign' || agiletags:formatXMLGregorianDate(entry.coverEndDate) <= '2999/12/31'}">
                           <option value="${entry.dependentNumber}" selected>${entry.dependentNumber} ${entry.name} ${agiletags:formatXMLGregorianDate(entry.dateOfBirth)}</option>
                        </c:if>
                        </c:forEach> 
                </select>
            </td><td width="200px" align="left"><label id="deptList_error" class="error"></label>
        </tr>
        
    </table>
    <br/>
    <br/>
    <table id="MemberSuspendTable">
        <tr>
            <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
            <td><input type="button" value="Save" onclick="document.getElementById('disorderValuesButtonPressed').value='SaveReinstateMemberButton';submitFormWithAjaxPost(this.form, 'MemberCover', null,'${main_div}', '${target_div}');"></td>
        </tr>
    </table>
</agiletags:ControllerForm>


<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<agiletags:ControllerForm name="MemberIncomeForm">
    <input type="hidden" name="opperation" id="opperation" value="SaveMemberAdditionalInfoCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberIncome" />
    <input type="hidden" name="target_div" value="${target_div}" />
    <input type="hidden" name="main_div" value="${main_div}" />
    <input type="hidden" name="buttonPressed" id="memberIncomeButtonPressed" value="" />
    <input type="hidden" name="incomeStatus" id="incomeStatus" value="">
    <input type="hidden" name="subsidyStatus" id="subsidyStatus" value="">
    <c:if test="${memberView == 'Income'}">
        <label class="header">Change Income</label>
        <br>
        <table>                        
            <tr id="FirmrApp_brokerAccreditationNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Income" elementName="MAI_newIncome" valueFromRequest="MAI_newIncome" mandatory="yes"/>        </tr>
            <tr id="FirmApp_brokerStartDateRow"><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MAI_StartDate" valueFromSession="yes" mandatory="yes"/>        </tr>         
        </table>

        <hr>
        <table>
            <tr>
                <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>           
                <td><input type="button" value="Save" onclick="saveIncome(this);"> </td>
            </tr>
        </table>
    </c:if>
    <c:if test="${memberView == 'Subsidy'}">
        <label class="header">Change Subsidy</label>
        <br>
        <table>                        
            <tr><agiletags:LabelTextBoxErrorReq displayName="Subsidy" elementName="MAI_newSubsidy" valueFromRequest="MAI_newSubsidy" mandatory="yes"/>        </tr>
            <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MAI_StartDate" valueFromSession="yes" mandatory="yes"/>        </tr>         
        </table>

        <hr>
        <table>
            <tr>
                <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>           
                <td><input type="button" value="Save" onclick="saveSubsidy(this);"> </td>
            </tr>
        </table>
    </c:if>
    <c:if test="${memberView == 'Payment'}">
        <label class="header">Change Payment Method</label>
        <br>
        <table>                        
            <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Payment Method" elementName="MAI_newPayment" lookupId="188" mandatory="yes" errorValueFromSession="no"/>       </tr>
            <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MAI_newPaymentStartDate" valueFromSession="yes" mandatory="yes"/>        </tr>         
        </table>

        <hr>
        <table>
            <tr>
                <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>           
                <td><input type="button" value="Save" onclick="savePaymentMethod(this);setElemStyle('debit_date',document.getElementById('MAI_PayMethod').value != 'Electronic Transfer'? 'block' : 'none');"> </td>
            </tr>
        </table>
    </c:if>
    <c:if test="${memberView == 'Debit'}">
        <label class="header">Change Debit Order Date</label>
        <br>
        <table>                        
            <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Debit Date" elementName="MAI_newDebitDate" lookupId="197" mandatory="yes" errorValueFromSession="no"/>        </tr>
            <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MAI_newDebitDateStartDate" valueFromSession="yes" mandatory="yes"/>        </tr>         
        </table>

        <hr>
        <table>
            <tr>
                <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>           
                <td><input type="button" value="Save" onclick="saveDebitDate(this);"> </td>
            </tr>
        </table>
    </c:if>

</agiletags:ControllerForm>

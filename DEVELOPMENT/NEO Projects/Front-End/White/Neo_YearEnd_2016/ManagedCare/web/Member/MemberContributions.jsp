<%-- 
    Document   : MemberContributions
    Created on : 2012/05/14, 10:19:34
    Author     : yuganp
--%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags/contributions" %>
<div id="tabTableLayout">
    <nt:ContributionAgeAnalysis items="${memberAgeAnalysis}" productID="${sessionScope.ageAnalysisProductID}"/>
    <nt:ContributionSavingsLimit items="${savingsLimits}"/>
    <nt:ContributionUnallocatedReceipts items="${Receipts}"/>
    <nt:ContributionDebitOrderDetails items="${debitOrder}" />
    <nt:ReceiptRefunds items="${receiptRefunds}" />
    <label class="subheader">Member Contributions</label>
    <HR WIDTH="100%" align="left">
    <agiletags:ControllerForm name="ContribSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="LoadContribTranCommand" />
        <input type="hidden" name="command" id="ContribSearchCommand" value="ContribSearch" />
        <input type="hidden" name="entityId" id="entityId" value="${memberCoverEntityId}" />
        <table >
            <td><label>Start Date</label></td>
            <td><input name="contrib_start_date" id="contrib_start_date" size="10" value="${contrib_start_date}"></td>
            <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('contrib_start_date', this);"/></td>
            <td>&nbsp;</td>
            <td><label>End Date</label></td>
            <td><input name="contrib_end_date" id="contrib_end_date" size="10" value="${contrib_end_date}"></td>
            <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('contrib_end_date', this);"/></td>
            <td>&nbsp;</td>
            <td><label>Action Date</label></td>
            <td><input name="contrib_ad_date" id="contrib_ad_date" size="10" value="${contrib_ad_date}"></td>
            <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('contrib_ad_date', this);"/></td>
            <td>&nbsp;</td>
            <td><button type="button" onclick="submitFormWithAjaxPost(this.form, 'ContribTranResultsx', this);">Search</button></td>
        </table>
    </agiletags:ControllerForm>

    <HR WIDTH="100%" align="left">
    <br>
    <div id="ContribTranResultsx">
        <jsp:directive.include file="/Member/MemberContributionDetails.jsp"></jsp:directive.include>
    </div>
    <agiletags:ControllerForm name="ExcelExportForm2">
        <input type="hidden" name="opperation" id="opperationExp" value="LoadContribTranCommand" />
        <input type="hidden" name="exp_start_date" id="exp_start_date" value="" />
        <input type="hidden" name="exp_end_date" id="exp_end_date" value="" />
        <input type="hidden" name="buttonPressedExport" id="buttonPressedExport" value="Export" />
        <td><input type="submit" value="Export To Excel" onclick="setFormValuesFromForm('ExcelExportForm2', 'ContribSearchForm', {'exp_start_date':'contrib_start_date','exp_end_date':'contrib_end_date'});"></td>
     </agiletags:ControllerForm>
</div>        

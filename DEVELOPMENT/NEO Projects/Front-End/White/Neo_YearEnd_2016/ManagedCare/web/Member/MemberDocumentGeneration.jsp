<%-- 
    Document   : IndexDocument
    Created on : 02 Jul 2012, 3:34:17 PM
    Author     : Christo
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.koh.serv.PropertiesReader"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%
    System.out.println("-------------MemberDocGen JSP------------------");



    String errorMsg = (String) request.getAttribute("errorMsg");
%>
<div>
    <form name="documentGeneration" id="documentGeneration" action="/ManagedCare/AgileController" method="POST">

        <table width=100% height=100%><tr valign="left"><td width="50px"></td><td align="left">
                    <label class="header">Document Generation</label>
                    <br/><br/><br/>
                    <table>
                        <% if (errorMsg != null) {%>
                        <tr><td><label id="error" class="error"><%=errorMsg%></label></td></tr>
                                <% }%>

                        <input type="hidden" name="entityId" id="entityId" value="" />
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <input type="hidden" name="onScreen" id="onScreen" value="" />
                        <input type="hidden" name="docType" id="docType" value="" />
                        <tr><td>
                                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                    <tr><th>Document Type</th><th>Document Name</th><th></th></tr>
                                            <c:if test="${sessionScope.CustomerCareSide == null || sessionScope.CustomerCareSide eq 'false'}">
                                        <tr><td><label class="label">Card for member</label></td>
                                            <td><label id="DocGen_cardForMember" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onclick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'cardForMember');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        <tr><td><label class="label">Change Of Banking Details</label></td>
                                            <td><label id="DocGen_changeOfBankingDetails" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'changeOfBankingDetails');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        <tr><td><label class="label">Membership Certificate</label></td>
                                            <td><label id="DocGen_membershipCertificate" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'membershipCertificate');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        <tr><td><label class="label">Registration Of Dependant</label></td>
                                            <td><label id="DocGen_registrationOfDependant" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'registrationOfDependant');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        <tr><td><label class="label">Re-Instatement</label></td>
                                            <td><label id="DocGen_reInstatement" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'reInstatement');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        <tr><td><label class="label">Suspension</label></td>
                                            <td><label id="DocGen_suspension" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'suspension');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        <tr><td><label class="label">Update Of Contact Details</label></td>
                                            <td><label id="DocGen_updateToContactDetails" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'updateToContactDetails');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        <tr><td><label class="label">Welcome Letter</label></td>
                                            <td><label id="DocGen_welcomeLetter" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'welcomeLetter');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        <tr><td><label class="label">Option Change</label></td>
                                            <td><label id="DocGen_optionChange" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'optionChange');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        <tr><td><label class="label">Overage Dependant Letter</label></td>
                                            <td><label id="DocGen_overageDependantLetter" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'overageDependantLetter');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>         
                                        
                                        <c:if test="${applicationScope.Client == 'Sechaba' && sessionScope.coverStatus == 'Active'}">
                                        <tr><td><label class="label">New Member - Primary Care With Dependants</label></td>
                                            <td><label id="DocGen_NewMemberPrimaryCareWithDependants" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'NewMemberPrimaryCareWithDependants');
                                           submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                         <tr><td><label class="label">New Member - Affordable Care, Full Benefit Care, Savings Care (Member only) </label></td>
                                            <td><label id="DocGen_NewMemberAffordableFullBenefitSavingsMemberOnly" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'NewMemberAffordableFullBenefitSavingsMemberOnly');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                          <tr><td><label class="label">New Member - Primary Care (Member Only) </label></td>
                                            <td><label id="DocGen_NewMemberPrimaryCareMemberOnly" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'NewMemberPrimaryCareMemberOnly');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        <tr><td><label class="label">New Member - Affordable Care, Full Benefit Care, Savings Care (With Dependants) </label></td>
                                            <td><label id="DocGen_NewMemberAffordableFullBenefitSavingsDependants" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'NewMemberAffordableFullBenefitSavingsDependants');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        <tr><td><label class="label">Amended Membership Card</label></td>
                                            <td><label id="DocGen_AmendedMembershipCard" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'AmendedMembershipCard');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        <tr><td><label class="label">Welcome Letter Gomomo Care</label></td>
                                            <td><label id="DocGen_WelcomeLetterGommoCare" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'WelcomeLetterGommoCare');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        </c:if>
                                       <c:if test="${!applicationScope.Client == 'Sechaba'}">
                                         <tr><td><label class="label">Terms Of Acceptance</label></td>
                                        <td><label id="DocGen_welcomeLetter" class="label"></label></td>
                                        <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand','termsOfAcceptance'); 
                                            submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                       </c:if>
                                      <tr><td><label class="label">Member Outstanding Information</label></td>
                                            <td><label id="DocGen_memberOutstandingInformation" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'memberOutstandingInformation');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                      
                                        <tr><td><label class="label">Dependant Changes Card</label></td>
                                            <td><label id="DocGen_DependantChangesCard" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'DependantChangesCard');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        <tr><td><label class="label">Underwriting Letter</label></td>
                                            <td><label id="DocGen_UnderwritingLetter" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'UnderwritingLetter');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        </c:if> 
                                        <!--<tr><td><label class="label">Contribution Statement</label></td>
                                               <td><label id="DocGen_contributionStatement" class="label"></label></td>
                                        <c:if test="${sessionScope.coverStatus == 'Active' || sessionScope.coverStatus == 'Suspend'}">
                                        <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand','contributionStatement'); submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        </c:if>
                                        <c:if test="${sessionScope.coverStatus == 'Resign'}">
                                        <td><input id="cardButton" type="button" value="Generate" disabled="yes" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand','contributionStatement'); submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        </c:if>
                                    -->
                                    <c:if test="${sessionScope.CustomerCareSide != null && sessionScope.CustomerCareSide eq 'true'}"> <%--Membership Side --%>
                                        <tr><td><label class="label">Membership Certificate</label></td>
                                            <td><label id="DocGen_membershipCertificate" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'membershipCertificate');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                    </c:if> 
                                        
                                    <c:if test="${applicationScope.Client == 'Sechaba'}"> <%--Membership Side --%>
                                        <tr><td><label class="label">Membership terminated, Outstanding Contributions</label></td>
                                            <td><label id="DocGen_familyResignationOutstandingContribution" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'familyResignationOutstandingContribution');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                    </c:if> 
                                        
                                        
                                        <c:if test="${applicationScope.Client == 'Sechaba'}">
                                            <tr><td><label class="label">Suspension Debit Order Returned</label></td>
                                            <td><label id="DocGen_suspensionDebitOrderReturned" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'suspensionDebitOrderReturned');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        <tr><td><label class="label">Additional Dependant Activated</label></td>
                                            <td><label id="DocGen_additionalDependantActivated" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'additionalDependantActivated');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        <tr><td><label class="label">Duplicate Membership Cards</label></td>
                                            <td><label id="DocGen_duplicateMembershipCard" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'duplicateMembershipCard');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                             <tr><td><label class="label">Member Terminated Non-Contribution</label></td>
                                        <td><label id="DocGen_familyResignationNonContribution" class="label"></label></td>
                                            <td><input id="cardButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand', 'familyResignationNonContribution');
                                                submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                               <tr><td><label class="label">Terms Of Acceptance</label></td>
                                        <td><label id="DocGen_TermsOfAcceptanceSizwe" class="label"></label></td>
                                        <td><input id="cardButtontermsOfAcceptanceSizwe" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberDocumentGenerationCommand','TermsOfAcceptanceSizwe'); 
                                            submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                        </c:if>                                       
                                        
                                </table>
                            </td></tr>
                    </table>
                </td></tr></table>
    </form>
</div>

<%-- 
    Document   : MemberContactDetails
    Created on : 2012/05/14, 10:06:16
    Author     : johanl
--%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<div id="tabTableLayout">
    <agiletags:ControllerForm name="MemberContactDetailsForm">
        <input type="hidden" name="opperation" id="MemberContact_opperation" value="SaveMemberContactDetailsCommand" />
        <input type="hidden" name="onScreen" id="MemberContact_onScreen" value="MemberContactDetails" />
        <div id="ContactDetailsPage"><jsp:include page="ContactDetails.jsp"/></div>
        
    </agiletags:ControllerForm>  
    <br/>
    <agiletags:ControllerForm name="MemberContactDetailsForm">
        <input type="hidden" name="opperation" id="MemberContactPref_opperation" value="SaveMemberContactPrefDetailsCommand" />
        <input type="hidden" name="onScreen" id="MemberContactPref_onScreen" value="MemberContactDetails" />
        <div id="ContactPrefPage"><jsp:include page="ContactPreferenceDetails.jsp"/></div>
    </agiletags:ControllerForm>
</div>

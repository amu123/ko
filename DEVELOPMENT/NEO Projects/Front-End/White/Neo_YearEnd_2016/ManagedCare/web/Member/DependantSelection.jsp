<%-- 
    Document   : DependantSelection
    Created on : 2012/05/21, 01:13:48
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" language="JavaScript">
            
            function submitCoverDeps(coverNum, entityId, depTypeId, depNumber, onScreen, optionId, entityCommon){
                //alert("Cover Details = Num - "+coverNum+" Dep - "+depTypeId+" nextScreen - "+onScreen);
                document.getElementById("onScreen").value = onScreen;
                document.getElementById("coverNum").value = coverNum;
                document.getElementById("entityId").value = entityId;
                document.getElementById("depTypeId").value = depTypeId;
                document.getElementById("depNumber").value = depNumber;
                document.getElementById("opperation").value = 'LoadCoverMenuTabs';
                document.getElementById("optionId").value = optionId;
                document.getElementById("entityCommon").value = entityCommon;
                document.forms[0].submit();
            }
            
            function submitGenericReload(action, onScreen){
                document.getElementById("onScreen").value = onScreen;
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }
            
            
        </script>    
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Cover Dependants</label>
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="saveAuth" validate="yes">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="coverNum" id="coverNum" value="" />
                            <input type="hidden" name="depTypeId" id="depTypeId" value="" />
                            <input type="hidden" name="entityId" id="entityId" value="" />
                            <input type="hidden" name="depNumber" id="depNumber" value="" />
                            <input type="hidden" name="optionId" id="optionId" value="" />
                            <input type="hidden" name="entityCommon" id="entityCommon" value="" />
                        </agiletags:ControllerForm>                        
                    </table>
                    <br/>
                    <HR color="#666666" WIDTH="100%" align="left">
                    <br/>
                    <agiletags:CoverDependantSelectionGrid sessionAttribute="MemberCoverDependantDetails" commandName="" javaScript="memberDepSelect" onScreen="/Member/MemberDetails.jsp" /> 
                    <br/>
                    <table>
                        <tr><agiletags:ButtonOpperation align="right" commandName="" displayname="Return" span="5" type="button" javaScript="onClick=\"submitGenericReload('ReloadPreauthGeneric','/Member/SearchMember.jsp')\";" /></tr> 
                    </table>
                </td></tr></table>        
    </body>
</html>

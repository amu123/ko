<%-- 
    Document   : AddMemberUnderwriting
    Created on : 2014/03/12, 11:44:42
    Author     : marcelp
--%>


<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<script type="text/javaScript"> 
</script>
<agiletags:ControllerForm name="AddMemberUnderwritingForm">
    <input type="hidden" name="opperation" id="MemberUnderwriting_opperation" value="MemberUnderwritingCommand" />
    <input type="hidden" name="writingPMB" id="writingPMB" value="${writingPMB}" />
    <input type="hidden" name="writingEntityId" id="writingEntityId" value="${writingEntityId}" />
    <input type="hidden" name="buttonPressed" id="buttonPressedUnderwriting" value="" />    
    <table id="MemberUnderwritingContent">

        <tr><td><label for="GeneralwaitingMonths" name="generalwaitingLabel">Duration:</label></td>
            <td> <select name="GeneralwaitingMonths" id="GeneralwaitingMonths">
                    <option value="1" selected>1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
            </td>
        </tr>
    </table>            
    <table id="MemberUnderwritingTable">
        <tr>
            <td><input type="button" value="Cancel" onclick="document.getElementById('buttonPressedUnderwriting').value = 'forwardToMemberUnderwritingDetails'; submitFormWithAjaxPostAdd(this.form, 'MemberUnderwriting');"></td>
            <td><input id="saveButton" name="saveButton" type="button" value="Save" onclick="document.getElementById('buttonPressedUnderwriting').value = 'SaveMemberUnderwritingDetails'; submitFormWithAjaxPostAdd(this.form, 'MemberUnderwriting');"></td>
        </tr>
    </table>
</agiletags:ControllerForm>




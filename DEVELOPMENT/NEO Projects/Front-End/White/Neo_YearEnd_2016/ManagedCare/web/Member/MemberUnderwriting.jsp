<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions' %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });       
            });
            
            

         
        </script>
    </head>
    <body >

        <div id="tabTableLayout">
            <agiletags:ControllerForm name="MemberUnderwritingForm">
                <input type="hidden" name="opperation" id="MemberUnderwriting_opperation" value="MemberUnderwritingCommand" />
                <input type="hidden" name="buttonPressed" id="buttonPressedUnderwriting" value="" />
                <input type="hidden" name="depCoverId" id="depCoverId" value="" />
                <input type="hidden" name="depLJP" id="depLJP" value="" />
                <input type="hidden" name="writingEntityId" id="writingEntityId" value="" />
                <input type="hidden" name="exclutionType" id="exclutionType" value="" />
                <input type="hidden" name="exclutionId" id="exclutionId" value="" />
                <input type="hidden" name="writingPMB" id="writingPMB" value="" />
                <input type="hidden" name="writingStartDate" id="writingStartDate" value="" />
                <input type="hidden" name="ICDCode" id="ICDCode" value="" />
                <input type="hidden" name="ICDDescription" id="ICDDescription" value="" />

                <c:if test="${warningW == 'claims'}" >
                    <span style="color: red">Warning:Cover has claims</span>
                </c:if>
                <br/>
                <c:if test="${warningW == 'pre'}" >
                    <span style="color: red">Warning:Cover has pre authorizations</span>
                </c:if>
                <br/>
                <c:if test="${warningW == 'both'}" >
                    <span style="color: red">Warning:Cover has claims and pre authorizations</span>
                </c:if>
                <br/>


                <label class="subheader">Late Joiner Penalty</label>
                <agiletags:CoverDependentPenaltyJoin sessionAttribute="MemberCoverDependantDetails" commandName="" javaScript="memberDepSelect" onScreen="/Member/MemberUnderwriting.jsp" /> 
                <br/><br/>

                <label class="subheader">Waiting Periods</label>
                <agiletags:CoverDependentWaitingPeriod sessionAttribute="MemberCoverDependantDetails" commandName="" javaScript="memberDepSelect" onScreen="/Member/MemberUnderwriting.jsp" /> 
                <br/><br/>
                <div>



                    <table>

                        <tr><td><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Concession"  elementName="ConcesType" lookupId="261" mandatory="no" errorValueFromSession="yes"  firstIndexSelected="yes" /></td></tr>  
                        <tr><td ><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="ConcessionStart" valueFromSession="yes" mandatory="no"/></td>
                        <tr><td><agiletags:LabelTextBoxDateReq  displayname="End Date" elementName="ConcessionEnd"  valueFromSession="yes" mandatory="no"/></td></tr> 
                        <center><tr><td>
                                    <c:if test="${validateConceType == '0'}" >
                                        <button type="button" id="addBtn"  onclick="validateAddBtn(this.form, 'MemberUnderwriting');  "  >Add</button>
                                        <button type="button" id="updateBtn" onclick="validateUpdateBtn(this.form, 'MemberUnderwriting');" disabled >Update</button>
                                    </c:if>


                                    <c:if test="${validateConceType ne '0'}" >
                                        <button type="button" id="addBtn"  onclick="validateAddBtn(this.form, 'MemberUnderwriting'); "  disabled>Add</button>
                                        <button type="button" id="updateBtn" onclick="validateUpdateBtn(this.form, 'MemberUnderwriting');">Update</button>
                                    </c:if>
                                </td>

                            </tr></center>


                    </table>

                </div>

                <br/>
                <label class="subheader">Condition Specific</label>
                <HR WIDTH="80%" align="left">

                <button type="button" onclick="document.getElementById('buttonPressedUnderwriting').value = 'AddConSpec'; getPageContents(document.forms['MemberUnderwritingForm'],null,'main_div','overlay');">Add</button>                                                        
                <agiletags:CoverDependentConditionSpecific sessionAttribute="MemberCoverDependantDetails" commandName="" javaScript="memberDepSelect" onScreen="/Member/MemberUnderwriting.jsp" /> 


            </agiletags:ControllerForm>
        </div> 
    </body>
</html>
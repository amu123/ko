<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<agiletags:ControllerForm name="ChangeMemberOptionForm">
    <input type="hidden" name="opperation" id="opperation" value="SaveMemberCoverDetailsCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="ChangeOptionMember" />
    <input type="hidden" name="buttonPressed" id="disorderValuesButtonPressed" value="" />
    <input type="hidden" name="memberEntityId" id="ResignEntityId" value="${memberCoverEntityId}" />
    <input type="hidden" name="brokerEntityId" id="brokerEntityId" value="${requestScope.brokerEntityId}" />
    <label class="header">Change Option</label>
    <hr>
    <br/>
    <table>                                        
        <!--<tr id="OptionChangeApp_optionIdRow"><agiletags:LabelNeoLookupValueDropDownWideErrorReq displayName="New Option"  elementName="OptionChangeApp_optionId" lookupId="option" mandatory="yes" errorValueFromSession="yes"/>        </tr>-->
        <tr id="OptionChangeApp_optionIdRow"><agiletags:LabelProductOptionDropDownRequest productElement="productId" displayName="New Option"  elementName="OptionChangeApp_optionId" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="no" javaScript="onchange=\"toggleNetworkIPA(this.value);\""/></tr>
        <tr id="OptionChangeApp_network_IPARow" style="display: none"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Network IPA" lookupId="351" elementName="optionChangeApp_network_IPA" mandatory="no" errorValueFromSession="yes" firstIndexSelected="no"/></tr>
        <tr id="OptionChangeApp_StartDateRow"><agiletags:LabelTextBoxDateWideReq  displayname="Start Date" elementName="OptionChangeApp_StartDate" valueFromSession="yes" mandatory="yes"/></tr>
        <tr id="OptionChangeApp_ReasonRow"><agiletags:LabelNeoLookupValueDropDownWideErrorReq displayName="Reason"  elementName="OptionChangeApp_Reason" lookupId="252" mandatory="no" errorValueFromSession="yes" sort="yes"/></tr>
        <tr id="OptionChangeApp_IncomeCategoryRow"><agiletags:LabelTextBoxWideErrorReq displayName="Income Category"  elementName="OptionChangeApp_IncomeCategory" mandatory="no"  valueFromRequest="OptionChangeApp_IncomeCategory" /></tr>
    </table>
    <br/>
    <br/>
    <table id="MemberResignTable">
        <tr>
            <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}', '${main_div}');"></td>
            <td><input type="button" value="Save" onclick="document.getElementById('disorderValuesButtonPressed').value = 'SaveChangeOptionButton';submitFormWithAjaxPost(this.form, 'MemberCover', null, '${main_div}', '${target_div}');"></td>
        </tr>
    </table>
</agiletags:ControllerForm>
<script>
    function toggleNetworkIPA(optionID) {
        var networkIPA = document.getElementById("OptionChangeApp_network_IPARow");
        if (networkIPA != null && networkIPA != undefined) {
            if (optionID != null && optionID == '100') {
                networkIPA.style.display = '';
            } else {
                networkIPA.style.display = 'none';
            }
        }
    }
</script>
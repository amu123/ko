<%-- 
    Document   : BrokerSearchResults
    Created on : 2012/06/28, 11:43:08
    Author     : princes
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Membership/MemberMaintenance.js"></script>
    </head>
    <body>
        <br>

        <label class="subheader">Search Results</label>
        <hr>
        <c:choose>
            <c:when test="${empty indexList}">
                <span class="label">No Document found</span>
            </c:when>
            <c:otherwise>
                <label id="DocView_Msg" class="label"></label>
                <agiletags:ControllerForm name="documentIndexing">
                    <input type="hidden" name="folderList" id="folderList" value="" />
                    <input type="hidden" name="fileLocation" id="fileLocation" value="" />
                    <input type="hidden" name="opperation" id="opperation" value="MemberDocumentDisplayViewCommand" />
                    <input type="hidden" name="searchCalled" id="searchCalled" value=""/>
                    <input type="hidden" name="onScreen" id="onScreen" value="" />
                    <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                    <input type="hidden" name="entityType" id="entityType" value="" />
                    <input type="hidden" name="docType" id="docType" value="" />
                    <input type="hidden" name="entityNumber" id="entityNumber" value="">
                    <input type="hidden" name="refNumber" id="refNumber" value="">
                    <input type="hidden" name="indexID" id="indexID" value="">
                    <c:set var="Email" value="${requestScope.ContactDetails_email}"/>
                    <input type="hidden" name="emailAddress" id="emailAddress" value="${Email}">

                    <br/>
                    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                        <tr>
                            <th>Member Number</th>
                            <th>Type</th>
                                <c:if test="${requestScope.docKind == '212' }">
                                <th>Reference Number</th>
                                </c:if>
                            <th>Location</th>
                            <th>Current Date</th>  
                            <th></th>
                            <th></th>
                            <th></th>
                                <c:if test="${sessionScope.withEmail == 'CS'}">  
                                <th>Email</th>
                                <th></th>
                                </c:if>
                        </tr>
                        <c:forEach var="entry" items="${indexList}">
                            <tr class="label">
                                <td>${entry['entityNumber']}</td>
                                <td>${entry['docType']}</td>
                                <c:if test="${requestScope.docKind == '212'}">
                                    <td>${entry['reference']}</td>
                                </c:if>
                                <td>${entry['location']}</td>
                                <td><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.generationDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.generationDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.generationDate.day}"/></td>
                                <td><input id="viewB" type="submit" value="View"  onClick="submitActionAndViewFile(this.form, 'MemberDocumentDisplayViewCommand', '${entry['location']}');" /></td>
                                <td><input id="printB" type="button" value="Print" onClick="submitActionAndFile(this.form, 'AddToPrintPoolCommand', '${entry['indexId']}'); submitFormWithAjaxPost(this.form, 'MemberDocuments');" /></td>
                                    <c:if test="${requestScope.docKind == '212'}">
                                    <td><input id="reindex" type="button" value="Re-Index" onClick="submitActionAndFileReIndex(this.form, 'DocumentReIndexFileCommand', '${entry['entityType']}', '${entry['location']}', '${entry['docType']}', '${entry['entityNumber']}', '${entry['reference']}', '${entry['indexId']}');
                                    getPageContentsForReIndex(document.forms['documentIndexing'], null, 'main_div', 'overlay');" /></td>
                                    </c:if>
                                    <c:if test="${entry['docType'] == 'Member Contribution Statement'}">   
                                    <td><input type="text" name="ContactDetails_email" value="${Email}" /></td>                        
                                    <td><input id="sendB" type="button" name="Email" value="Send" disabled="yes" onClick="document.getElementById('opperationParam').value = 'sendEmail';submitActionAndFile(this.form, 'SubmitDocumentsCommand', '${entry['location']}'); submitFormWithAjaxPost(this.form, 'MemberDocuments');"/></td>
                                    </c:if>
                                    <c:if test="${entry['docType'] == 'Overage Dependant Letter'}">
                                    <td style="text-align: center"><input type="button" id="btnEmail${entry['indexId']}" name="btnEmail${entry['indexId']}" value="Email" onclick="onScreenAction('EmailLatestDocumentCommand', '${entry['indexId']}');"/></td>           
                                    </c:if>
                            </tr>
                        </c:forEach>

                    </table>
                </agiletags:ControllerForm>
            </c:otherwise>
        </c:choose>
    </body>
</html>

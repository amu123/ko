<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<agiletags:ControllerForm name="MemberDependantTypeForm">
    <input type="hidden" name="opperation" id="opperation" value="SaveMemberAdditionalInfoCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberIncome" />
    <input type="hidden" name="target_div" value="${target_div}" />
    <input type="hidden" name="main_div" value="${main_div}" />
    <input type="hidden" name="buttonPressed" id="memberDepTypeButtonPressed" value="" />
    <input type="hidden" name="dependantStatus" id="dependantStatus" value="">


    <c:if test="${depTypeView == 'depType'}">
        <label class="header">Dependant Type</label>
        <br/>
        <c:if test="${depMessage == 'child'}" >
            <span style="color: red">**** Warning : Child will be linked to adult premium ****</span>
        </c:if>

        <c:if test="${depMessage == 'adult'}" >
            <span style="color: red">**** Warning : Adult will be linked to child premium ****</span>
        </c:if>
        <br/>
        <br/>
        <table>
            <tr>
                <td class="label" width="160px"><span style="font-family: Arial, Helvetica, sans-serif;font-size: 1.0em;">Action</span></td>
                <td width="160px"  colspan="3"><select yid="depTypeSelect" name="depTypeSelect" onchange="hideDependentType(this.value)" width="160px">
                        <option value="1" selected>Change Date</option>
                        <option value="2" selected>Change Dependent Type</option>
                    </select></td>
            </tr>
        </table>
        <div  id="MAI_newDependantTypeRow2">
        <table>
            <tr><agiletags:LabelNeoMapListDropDownErrorReq displayName="Dependant Type"  elementName="MAI_newDependantType" mapList="DependantTypeList" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="yes" />        </tr>
            <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MAI_newDependantTypeStartDate2" valueFromSession="yes" mandatory="yes"/>        </tr>         
        </table>
        </div>
        
        <div  id="MAI_newDependantTypeRow1" style="display: none">
        <table>            
            <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MAI_newDependantTypeStartDate" valueFromSession="yes" mandatory="yes"/>        </tr>         
        </table>
        </div>

        <hr>
        <table id="MemberAppDepSaveTable">
            <tr>
                <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>           
                <td><input type="button" value="Save" onclick="saveDepType(this);"> </td>
            </tr>
        </table>
    </c:if>

    <c:if test="${depTypeView == 'finType'}">
        <label class="header">Financially Dependant</label>
        <br/>
        <table>                        
            <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Financially Dependant"  elementName="MAI_newFinanciallyDependant" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
            <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MAI_newFinanciallyDependantStartDate" valueFromSession="yes" mandatory="yes"/>        </tr>         
        </table>

        <hr>
        <table id="MemberAppDepSaveTable">
            <tr>
                <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>           
                <td><input type="button" value="Save" onclick="SaveFinType(this);"> </td>
            </tr>
        </table>
    </c:if>
    
    <c:if test="${depTypeView == 'relType'}">
        <label class="header">Relationship Type</label>
        <br/>
        <table>                        
            <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Relationship Type"  elementName="MAI_newRelType" lookupId="213" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MAI_newRelTypeStartDate" valueFromSession="yes" mandatory="yes"/>        </tr>         
        </table>

        <hr>
        <table id="MemberAppDepSaveTable">
            <tr>
                <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>           
                <td><input type="button" value="Save" onclick="SaveRelType(this);"> </td>
            </tr>
        </table>
    </c:if>

    <c:if test="${depTypeView == 'student'}">
        <label class="header">Student</label>
        <br/>
        <c:if test="${depMessage == 'child'}" >
            <span style="color: red">**** Warning : Child will be linked to adult premium ****</span>
        </c:if>

        <c:if test="${depMessage == 'adult'}" >
            <span style="color: red">**** Warning : Adult will be linked to child premium ****</span>
        </c:if>
        <br/>
        <br/>
        <table>                        
            <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Student"  elementName="MAI_newStudent" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MAI_newStudentStartDate" valueFromSession="yes" mandatory="yes"/>        </tr>         
        </table>

        <hr>
        <table id="MemberAppDepSaveTable">
            <tr>
                <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>           
                <td><input type="button" value="Save" onclick="saveStudent(this);"> </td>
            </tr>
        </table>
    </c:if>

    <c:if test="${depTypeView == 'handicap'}">
        <label class="header">Handicap</label>
        <br/>
        <c:if test="${depMessage == 'child'}" >
            <span style="color: red">**** Warning : Child will be linked to adult premium ****</span>
        </c:if>

        <c:if test="${depMessage == 'adult'}" >
            <span style="color: red">**** Warning : Adult will be linked to child premium ****</span>
        </c:if>
        <br/>
        <br/>
        <table>                        
            <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Handicap"  elementName="MAI_newHandicap" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <tr id="FirmApp_brokerStartDateRow"><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MAI_newHandicapStartDate" valueFromSession="yes" mandatory="yes"/>        </tr>         
        </table>

        <hr>
        <table id="MemberAppDepSaveTable">
            <tr>
                <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>           
                <td><input type="button" value="Save" onclick="saveHandicap(this);"> </td>
            </tr>
        </table>
    </c:if>   


</agiletags:ControllerForm>

<%-- 
    Document   : MemberUnionSearchResults
    Created on : Aug 12, 2016, 10:42:06 AM
    Author     : shimanem
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<br/>
<label class="subheader">Union Search Result</label>
<hr>
<c:if test="${not empty requestScope.MemberUnionResults}">
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th align="left">Union ID</th>
            <th align="left">Union Number</th>
            <th align="left">Union Name</th>
            <th align="left">Inception Date</th>
            <th align="left">Termination Date</th>
            <th>&nbsp;</th>
        </tr>
        <c:forEach items="${MemberUnionResults}" var="union">
            <tr>
                <td><label class="label">${union.unionID}</label></td>
                <td><label class="label">${union.unionNumber}</label></td>
                <td><label class="label">${union.unionName}</label></td>
                <td><label class="label">${agiletags:formatXMLGregorianDate(union.inceptionDate)}</label></td>
                <td><label class="label">${agiletags:formatXMLGregorianDate(union.terminationDate)}</label></td>
                <td><button value="" type="button" name="opperation" width="100px" onclick="setElemValues({'employerGroup_unionID': '${union.unionID}', 'employerGroup_unionNumber': '${agiletags:escapeStr(union.unionNumber)}', 'employerGroup_unionName': '${agiletags:escapeStr(union.unionName)}'});
                        swapDivVisbible('overlay2','overlay');">Select</button></td>
            </tr>
        </c:forEach>
    </table>
</c:if>
<c:if test="${empty requestScope.EmployerUnionResults && requestScope.EmployerUnionResults eq ''}">
    <label class="red">No Results Found</label>
</c:if>
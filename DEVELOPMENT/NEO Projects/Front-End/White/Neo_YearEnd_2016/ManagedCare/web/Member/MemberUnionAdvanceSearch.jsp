<%-- 
    Document   : MemberUnionAdvanceSearch
    Created on : Aug 12, 2016, 10:40:26 AM
    Author     : shimanem
--%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<agiletags:ControllerForm name="unionSearchForm">
    <input type="hidden" name="opperation" id="opperation" value="MemberUnionAdvancedSearchCommand"/>
    <input type="hidden" name="onScreen" id="onScreen" value="" />
    <input type="hidden" name="target_div" value="${target_div}" />
    <input type="hidden" name="main_div" value="${main_div}" />
    <input type="hidden" name="union_Id" id="unionId" value="${union_Id}" />
    <input type="hidden" name="union_Number" id="unionNumber" value="${union_Number}" />
    <input type="hidden" name="union_Name" id="unionName" value="${union_Name}" />
    <br/>
    <br/>
    <label class="subheader">Union Search</label>
    <hr>
    <table>
        <tr><agiletags:LabelTextBoxError displayName="Union Number" elementName="_union_No"/><tr>
        <tr><agiletags:LabelTextBoxError displayName="Union Name" elementName="_union_Name"/><tr>
        <tr>
            <td><input type="button" value="Cancel" onclick="swapDivVisbible('overlay2','overlay');"></td>
            <td><input type="button" value="Search" onclick="submitFormWithAjaxPost(this.form, 'MemberUnionResults');"> </td>
        </tr>
    </table>

    <div style ="display: none"  id="MemberUnionResults"></div>
</agiletags:ControllerForm>


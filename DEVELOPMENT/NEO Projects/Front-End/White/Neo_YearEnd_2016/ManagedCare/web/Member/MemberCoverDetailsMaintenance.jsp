
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions' %>

<div id="tabTableLayout">
    <agiletags:ControllerForm name="MemberCoverDetailsMaintenanceForm">
        <div id="benefitPayoutType">
            <input type="hidden" name="opperation" id="MemberDetails_opperation" value="SaveMemberCoverDetailsCommand" />
            <input type="hidden" name="onScreen" id="MemberPersonal_onScreen" value="MemberEntityDetails" />
            <input type="hidden" name="buttonPressed" id="memberDetailsButtonPressed" value="" />
            <input type="hidden" name="memberEntityId" id="memberCoverDetailsEntityId" value="${sessionScope.memberCoverEntityId}">
            <input type="hidden" name="product" id="product" value="${requestScope.product}">
            <input type="hidden" name="memberCoverDepType" id="memberCoverDepType" value="${sessionScope.memberCoverDepType}">
            <input type="hidden" name="memberCoverNumber" id="memberCoverNumber" value="${sessionScope.memberCoverNumber}">
            <input type="hidden" name="brokerAndConsultant" id="brokerAndConsultant" value="${brokerAndConsultant}">
            
            <c:choose>
                <c:when test="${Client eq 'Sechaba'}">
                    <table align="center" id="depMemberTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%" >
                        <tr>
                            <th>Detail Type</th>
                            <th>Number</th>
                            <th>Name</th>
                            <th>Consultant</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th></th>
                        </tr>
                        <c:if test="${!empty detailsList}">
                            <c:forEach var="entry" items="${detailsList}" varStatus="loop">
                                <c:if test="${entry.detailType == 'Broker' || entry.detailType == 'Consultant'}">
                                    <tr class="label">
                                        <td>${entry.detailType}</td>
                                        <td>${entry.code}</td>
                                        <td>${entry.name}</td>
                                        <c:if test="${entry.detailType == 'Broker'}">
                                            <td>${entry.consultantName}</td>
                                        </c:if>
                                        <c:if test="${entry.detailType == 'Consultant'}">
                                            <td>${entry.consultantName}</td>
                                        </c:if>
                                        <td>${agiletags:formatXMLGregorianDate(entry.effectiveStartDate)}</td>
                                        <td>${agiletags:formatXMLGregorianDate(entry.effectiveEndDate)}</td>
                                        <td>
                                            <c:if test="${agiletags:formatXMLGregorianDate(entry.effectiveEndDate) eq '2999/12/31'}">
                                                <button id="changeButton" type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = ${entry.detailType == "Broker" ? "'LinkBrokerAndConsultantButton'" : entry.detailType == "Group" ? "'LinkGroupButton'" : entry.detailType == "Option" ? "'ChangeOptionButton'" : "'LinkBrokerAndConsultantButton'"}; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Change</button>
                                                <button id="termButton" type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'TerminateButtonPressed'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Terminate</button>
                                            </c:if>
                                            <!--<button id="addBrokerOrBCButton" style="" type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'LinkBrokerAndConsultantButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Add Broker or BC</button>-->
                                        </td>
                                    </tr>
                                </c:if>
                            </c:forEach>
                            <c:if test="${brokerAndConsultant eq '10'}">
                                <tr class="label">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><button id="addBrokerOrBCButton" type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'LinkBrokerAndConsultantButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Add Broker or BC</button></td>
                                </tr>
                            </c:if>
                        </c:if>
                    </table>
                    <br/>
                    <br/>
                </c:when>
                <c:otherwise>
                    <table align="center" id="depMemberTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%" >
                        <tr>
                            <th>Detail Type</th>
                            <th>Number</th>
                            <th>Name</th>
                            <th>Consultant</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th></th>
                        </tr>       

                        <c:choose>
                            <c:when test="${empty detailsList}">                   
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="entry" items="${detailsList}">
                                    <c:if test="${entry.detailType == 'Broker' || entry.detailType == 'Consultant'}">
                                        <tr class="label">
                                            <td>${entry.detailType}</td>
                                            <td>${entry.code}</td>
                                            <td>${entry.name}</td>
                                            <c:if test="${entry.detailType == 'Broker'}">
                                                <td>${entry.consultantName}</td>
                                            </c:if>
                                            <c:if test="${entry.detailType == 'Consultant'}">
                                                <td></td>
                                            </c:if>
                                            <td>${agiletags:formatXMLGregorianDate(entry.effectiveStartDate)}</td>
                                            <td>${agiletags:formatXMLGregorianDate(entry.effectiveEndDate)}</td>
                                            <td>
                                                <c:if test="${agiletags:formatXMLGregorianDate(entry.effectiveEndDate) == '2999/12/31'}">
                                                    <button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = ${entry.detailType == "Broker" ? "'LinkBrokerAndConsultantButton'" : entry.detailType == "Group" ? "'LinkGroupButton'" : entry.detailType == "Option" ? "'ChangeOptionButton'" : "'LinkBrokerAndConsultantButton'"}; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'],null,'main_div','overlay');">Change</button>
                                                </c:if>
                                            </td>                                                        
                                        </tr>
                                    </c:if>
                                </c:forEach> 
                                <c:if test="${brokerAndConsultant == '10'}">
                                    <tr class="label">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'LinkBrokerAndConsultantButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'],null,'main_div','overlay');">Add Broker or BC</button></td>     
                                    </tr>
                                </c:if>
                            </table>                
                        </c:otherwise>
                    </c:choose>
                    <br/>
                    <br/>
                </c:otherwise>
            </c:choose>
            
            <table align="center" id="depMemberTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%" >
                <tr>
                    <th>Detail Type</th>
                    <th>Number</th>
                    <th>Name</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th></th>
                </tr>

                <c:choose>
                    <c:when test="${empty detailsList}">
                        <tr class="label">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>                                                        
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="entry" items="${detailsList}">
                            <c:if test="${entry.detailType == 'Group'}">
                                <tr class="label">
                                    <td>${entry.detailType}</td>
                                    <td>${entry.code}</td>
                                    <td>${entry.name}</td>
                                    <td>${agiletags:formatXMLGregorianDate(entry.effectiveStartDate)}</td>
                                    <td>${agiletags:formatXMLGregorianDate(entry.effectiveEndDate)}</td>
                                    <td>
                                        <c:if test="${agiletags:formatXMLGregorianDate(entry.effectiveEndDate) == '2999/12/31'}">
                                            <button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = ${entry.detailType == "Broker" ? "'LinkBrokerAndConsultantButton'" : entry.detailType == "Group" ? "'LinkGroupButton'" : entry.detailType == "Option" ? "'ChangeOptionButton'" : "'LinkBrokerAndConsultantButton'"}; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Change</button>
                                        </c:if>
                                    </td>                                                        
                                </tr>
                            </c:if>
                        </c:forEach>
                        <c:if test="${group == '0'}">
                            <tr class="label">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td align="center" ><button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'LinkGroupButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Add Group</button></td>                                                        
                            </tr>
                        </c:if>
                    </table>                
                </c:otherwise>
            </c:choose>
            <br/>
            <br/>
            <table align="center" id="depMemberTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%" >
                <tr>
                    <th>Detail Type</th>
                    <th>Number</th>
                    <th>Name</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th></th>
                </tr>

                <c:choose>
                    <c:when test="${empty detailsList}">

                        <tr class="label">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td</td>                                                        
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="entry" items="${detailsList}">
                            <c:if test="${entry.detailType == 'Option'}">
                                <tr class="label">
                                    <td>${entry.detailType}</td>
                                    <td>${entry.code}</td>
                                    <td>${entry.name}</td>
                                    <td>${agiletags:formatXMLGregorianDate(entry.effectiveStartDate)}</td>
                                    <td>${agiletags:formatXMLGregorianDate(entry.effectiveEndDate)}</td>
                                    <td>
                                        <c:if test="${agiletags:formatXMLGregorianDate(entry.effectiveEndDate) == '2999/12/31'}">
                                            <button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = ${entry.detailType == "Broker" ? "'LinkBrokerAndConsultantButton'" : entry.detailType == "Group" ? "'LinkGroupButton'" : entry.detailType == "Option" ? "'ChangeOptionButton'" : "'LinkBrokerAndConsultantButton'"}; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Change</button>
                                        </c:if>
                                    </td>                                                        
                                </tr>
                            </c:if>
                        </c:forEach>
                    </table>                
                </c:otherwise>
            </c:choose>
            <c:if test="${optionId == '11' && productId == '1'}">
                <br/>
                <br/>
                <div>
                    <label class="subheader">Member Benefit Pay Out Type</label>
                    <HR WIDTH="80%" align="left">
                    <table>
                        <tr><label class="red">* Pay Out Type Required For Classic Option Selection</label></tr>
                        <tr id="PayoutType_payoutTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Pay Out Type"  elementName="PayoutType_payoutType" lookupId="201" mandatory="yes" errorValueFromSession="yes"/></tr>
                        <tr id="PayoutType_payoutStartDateRow"><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="PayoutType_payoutStartDate" valueFromSession="yes" mandatory="yes" /></td></tr>
                        <br/>
                        <tr><td><button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'SaveBenefitPayoutType'; submitFormWithAjaxPost(this.form, 'benefitPayoutType');">Save</button></td>
                            <td><label class="red"><b>${PayoutStatus}</b></label></td>
                        </tr>
                    </table>
                </div>
            </c:if>
            <br/>
            <br/>

            <!-- Member Union Details 
                * pls replace 'memberUnionSessionScope' with the correct session variable
            -->
            <c:if test="${applicationScope.Client == 'Sechaba'}">
                <table align="center" id="memeberUnionTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%" >
                    <tr>
                        <th>Detail Type</th>
                        <th>Number</th>
                        <th>Name</th>
                        <th>Representative</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th></th>
                    </tr>
                    <c:choose>
                        <c:when test="${empty sessionScope.memberUnionLinkedDetails}">
                            <tr class="label">
                                <td align="center">Union</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td align="center"><button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'LinkMembertoUnionButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Add Union</button></td> 
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="union" items="${sessionScope.memberUnionLinkedDetails}">

                                <tr class="label">
                                    <td align="center">Union</td>
                                    <td align="center">${union.unionNumber}</td>
                                    <td align="center">${union.unionName}</td>                           
                                    <td align="center">${union.repName}&nbsp;${union.repSurname}</td>
                                    <td align="center">${agiletags:formatXMLGregorianDate(union.startDate)}</td>
                                    <td align="center">${agiletags:formatXMLGregorianDate(union.endDate)}</td>
                                    <td align="center">
                                        <c:if test="${agiletags:formatXMLGregorianDate(union.endDate) == '2999/12/31'}">
                                            <button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'LinkMembertoUnionButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Change</button>
                                        </c:if>
                                    </td>                                                        
                                </tr>
                            </c:forEach>                
                        </table>                
                    </c:otherwise>
                </c:choose>
            </c:if>
            <!-- end of Member Union -->

            <table>
                <c:if test="${coverStatus == 'Active'}">
                    <tr>
                        <td><button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'SuspendMemberButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Suspend Member</button></td>                                                        
                    </tr>
                    <tr>
                        <td><button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'ResignMemberButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Resign Member</button></td>                                                        
                    </tr>
                    <tr>
                        <td><button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'ChangeInceptionButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Change Inception Date</button></td>                                                        
                    </tr>
                </c:if>   
                <c:if test="${coverStatus == 'Suspend'}">
                    <tr>
                        <td><button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'LiftSuspensionButton'; submitFormWithAjaxPost(this.form, 'MemberCover', this);">Lift Suspension</button></td>
                    </tr>
                    <tr>                    
                        <td><button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'ResignMemberButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Resign Member</button></td>   
                    </tr>
                </c:if>
                <c:if test="${coverStatus == 'Resign'}">
                    <tr>
                        <td><button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'ReinstateMemberButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Reinstate Member</button></td>                                                        
                    </tr>
                </c:if>
                <c:if test="${coverStatus == 'both'}">
                    <tr>
                        <td><button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'SuspendMemberButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Suspend Member</button></td>                                                        
                    </tr>
                    <tr>
                        <td><button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'ReinstateMemberButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Reinstate Member</button></td>                                                        
                    </tr>
                    <c:if test="${CanResign}">
                        <tr>
                            <td><button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'ResignMemberButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Resign Member</button></td>                                                        
                        </tr>
                    </c:if>
                    <tr>
                        <td><button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'ChangeInceptionButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Change Inception Date</button></td>                                                        
                    </tr>
                </c:if>
                <c:if test="${fn:length(MemberCoverDependantDetails) > 1 && coverStatus != 'Resign'}">
                    <tr>
                        <td><button type="button" onclick="document.getElementById('memberDetailsButtonPressed').value = 'DepSwapButton'; getPageContents(document.forms['MemberCoverDetailsMaintenanceForm'], null, 'main_div', 'overlay');">Dependant Swap</button></td>                                                        
                    </tr>
                </c:if>
            </table>  
        </div>
    </agiletags:ControllerForm>
</div>        

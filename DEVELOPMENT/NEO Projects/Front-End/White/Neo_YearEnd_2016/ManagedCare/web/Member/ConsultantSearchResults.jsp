<%-- 
    Document   : BrokerSearchResults
    Created on : 2012/06/28, 11:43:08
    Author     : princes
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Consultant Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty BrokerResults}">
            <span>No Broker found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Broker Code</th>
                        <th>Broker Name</th>
                        <th>Broker Alt Code</th>
                        <th>Initials</th>
                      
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${BrokerResults}">
                        <tr>
                            <td>${entry['brokerCode']}</td>
                            <td>${entry['fstName']}</td>
                            <td>${entry['altCode']}</td>
                            <td>${entry['initials']}</td>
                            <td><button type="submit" value="${entry['brokerCode']}" onclick="setMemberAppValues(this.value,'${agiletags:escapeStr(entry['fstName'])} ${agiletags:escapeStr(entry['surname'])}','${entry['brokerEntityId']}','${entry['title']}','${agiletags:escapeStr(entry['contractName'])}','${agiletags:escapeStr(entry['ContractSurname'])}');">View</button></td>
                        </tr>
                    </c:forEach>

                </table>
            </c:otherwise>
        </c:choose>


<%-- 
    Document   : MemberBankingDetails
    Created on : 2012/05/14, 10:07:04
    Author     : johanl
--%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<div id="tabTableLayout">
    <agiletags:ControllerForm name="MemberBankingDetailsForm">
        <input type="hidden" name="opperation" id="MemberBanking_opperation" value="SaveMemberBankingDetailsCommand" />
        <input type="hidden" name="onScreen" id="MemberBanking_onScreen" value="MemberBankingDetails" />

        <label class="subheader">Member Banking Details</label>
        <HR WIDTH="80%" align="left">
        <br/>
        <label class="subheader">Contribution Collection Details</label>
        <table id="ContribTable">
            <!-- <tr id="MemberDetails_paymentMethodRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Payment Method"  elementName="BankingDetails_contri_paymentMethod" lookupId="38" mandatory="yes" errorValueFromSession="yes"/>        </tr> -->
            <tr id="MemberDetails_contribBankNameRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Name of Bank"  elementName="BankingDetails_contri_bankName" lookupId="bank" mandatory="yes" errorValueFromSession="yes" javaScript="onChange=\"toggleBranch(this.value, 'BankingDetails_contri_bankBranchId')\""/>        </tr>

            <tr id="MemberDetails_contribBankBranchIdRow"><agiletags:LabelNeoBranchDropDownErrorReq displayName="Branch"  elementName="BankingDetails_contri_bankBranchId"  mandatory="yes" errorValueFromSession="yes" parentElement="BankingDetails_contri_bankName"/>        </tr>
            <tr id="MemberDetails_contribAccountTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Account Type"  elementName="BankingDetails_contri_accountType" lookupId="37" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <tr id="MemberDetails_contribAccountHolderRow"><agiletags:LabelTextBoxErrorReq displayName="Account Holder" elementName="BankingDetails_contri_accountHolder" valueFromRequest="BankingDetails_contri_accountHolder" mandatory="no"/>        </tr>
            <tr id="MemberDetails_contribAccountNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Account Number" elementName="BankingDetails_contri_accountNumber" valueFromRequest="BankingDetails_contri_accountNumber" mandatory="no"/>        </tr>
        </table>
        <br/>
        <label class="subheader">Reimbursement Details</label>
        <table id="ClaimTable">
            <tr id="MemberDetails_paymentsBankNameRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Name of Bank"  elementName="BankingDetails_pay_bankName" lookupId="bank" mandatory="yes" errorValueFromSession="yes" javaScript="onChange=\"toggleBranch(this.value, 'BankingDetails_pay_bankBranchId')\""/>        </tr>
            <tr id="MemberDetails_paymentsBankBranchIdRow"><agiletags:LabelNeoBranchDropDownErrorReq displayName="Branch"  elementName="BankingDetails_pay_bankBranchId" mandatory="yes" errorValueFromSession="yes" parentElement="BankingDetails_pay_bankName" />        </tr>
            <tr id="MemberDetails_paymentsAccountTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Account Type"  elementName="BankingDetails_pay_accountType" lookupId="37" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <tr id="MemberDetails_paymentsAccountHolderRow"><agiletags:LabelTextBoxErrorReq displayName="Account Holder" elementName="BankingDetails_pay_accountHolder" valueFromRequest="BankingDetails_pay_accountHolder" mandatory="no"/>        </tr>
            <tr id="MemberDetails_paymentsAccountNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Account Number" elementName="BankingDetails_pay_accountNumber" valueFromRequest="BankingDetails_pay_accountNumber" mandatory="no"/>        </tr>
        </table>
        <br/>
        <table id="MemberPersonalSaveTable">
            <tr>
                <td><input type="reset" value="Reset"></td>
                <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'MemberBankingDetails')"></td>
            </tr>
        </table>

    </agiletags:ControllerForm>
</div>
<%-- 
    Document   : MemberEntityDetails
    Created on : 2012/05/14, 10:19:34
    Author     : johanl
--%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<div id="tabTableLayout" >
    <agiletags:ControllerForm name="MemberCoverDetailsForm" >
        <input type="hidden" name="opperation" id="CoverDetails_opperation" value="SaveCoverDetailsCommand" />
        <input type="hidden" name="onScreen" id="CoverDetails_onScreen" value="MemberCoverDetails" />
        <div style="visibility: hidden">
        <agiletags:LabelTextBoxErrorReq enabled="false" readonly="true" displayName="Cover Option" elementName="CoverDetails_option" mandatory="no" valueFromRequest="CoverDetails_option" />
        <agiletags:LabelTextBoxErrorReq enabled="false" readonly="true" displayName="Cover Number" elementName="CoverDetails_coverNumber" mandatory="no" valueFromRequest="CoverDetails_coverNumber" />
        
        <agiletags:LabelTextBoxErrorReq enabled="false" readonly="true" displayName="Employer Entity ID" elementName="CoverDetails_employerEntityId" mandatory="no" valueFromRequest="CoverDetails_employerEntityId" />
        <agiletags:LabelTextBoxErrorReq enabled="false" readonly="true" displayName="Broker Entity ID" elementName="CoverDetails_brokerEntityId" mandatory="no" valueFromRequest="CoverDetails_brokerEntityId" />
        </div>   
       <label class="subheader">Cover Details</label>
        
       <HR WIDTH="80%" align="left">  
       <table id="CoverDetailsTable">  
           <tr><agiletags:LabelDropDown  displayName="Current Option" elementName="optionList"/></tr>
           <tr id="CoverDetails_optionIdStartDate">
               <agiletags:LabelTextBoxDateReq displayname="Start Date"  elementName="newStartDate" valueFromSession="no" mandatory="yes"/>
           </tr>

           <tr><agiletags:LabelDropDown  displayName="Current Group" elementName="employerList"/></tr>
           <tr id="CoverDetails_employerStartDate">
               <agiletags:LabelTextBoxDateReq displayname="Start Date"  elementName="newEmployerStartDate" valueFromSession="no" mandatory="yes"/>
           </tr>
                    
           <tr><agiletags:LabelDropDown  displayName="Current Broker" elementName="brokerList"/></tr>
           <tr id="CoverDetails_brokerStartDate">
               <agiletags:LabelTextBoxDateReq displayname="Start Date"  elementName="newBrokerStartDate" valueFromSession="no" mandatory="yes"/>
           </tr>
           
       </table>
           
       <label class="subheader">Resignations/Reinstatements</label>
       
       <table>
           <tr>
               <td>
                    <input id="radioResignations"  type="radio" name="ReinstatementsResignations" value="Resignations" /> Resignations
               </td>
               
               <td>
                   <input id="radioReinstatements" type="radio" name="ReinstatementsResignations" value="Reinstatements" />Reinstatements
               </td>
           </tr>
           <agiletags:CoverDependantSelection sessionAttribute="MemberCoverDependantDetails"/>
           
           <tr id="CoverDetails_resignationReinstatementStartDate" style="visibility: hidden">
               <agiletags:LabelTextBoxDateReq displayname="Start Date"  elementName="resignationReinstatementStartDate" valueFromSession="no" mandatory="yes"/>
           </tr>
       </table>
       
           
       <!--    <tr></tr>
           <tr>
               <td><label class="subheader">Suspension</label></td>
           </tr>
           <tr><td><input id="suspensionButton" name="suspensionButton" value="Suspend Cover" type="button"></input></tr></td>
           <tr id="CoverDetails_suspensionStartDate">
               <agiletags:LabelTextBoxDateReq displayname="End Date"  elementName="suspensionStartDate" valueFromSession="no" mandatory="yes"/>
           </tr>
           
           <tr>
               <td><label class="subheader">Resignations/Reinstatements</label></td>
               <td>
            </tr>
                
                <c:forEach var="entry" items="${MemberCoverDependantDetails}">
                            <tr>
                                <td>
                                <c:forEach var="item" items="${entry}">
                                    <td>${item.coverNumber}</td>
                                </c:forEach></td>
                            </tr>
                    </c:forEach>

  
           <td></td>
           <td></td>
           <td></td>
       </table>
       
        -->
        
        <table id="MemberPersonalSaveTable">
            <tr>
                <td><input type="reset" id="resetRemoveDate" value="Reset"></td>
                <td><input id="saveButton" type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'MemberCoverDetails')"></td>
            </tr>
        </table>
    </agiletags:ControllerForm>
</div>  




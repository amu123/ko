<%--
    Document   : AddUser
    Created on : 2009/10/20, 12:10:08
    Author     : gerritj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add user</title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="../Validation.js"></script>

        <script language="JavaScript">

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function validatePassword(str, element) {

                var error = "#"+element+"_error";
                if(str.length < 8) {
                    $(error).text("The password must be at least 8 characters!");
                } else {
                    $(error).text("");
                }
            }

            function validateRegister(command) {

                var newP = $("#password").val();
                var confirmP = $("#password2").val();
                var error = "#password2_error";
                var userError = "#username_error";

                if(confirmP == newP && $(userError).text() == "") {
                    $(error).text("");
                    submitWithAction(command);
                } else if(confirmP != newP){
                    $(error).text("The passwords do not match!");
                }else {
                    $(error).text("");
                }
                //validatename(command) 
            }
            
            
            //function validatename(command) {
               // var userError = "#username_error";
               // alert($(userError).text());
           // }
            
    function validateUserName(str, element) {
        
        xmlhttp=GetXmlHttpObject();
        if(xmlhttp==null) {
            alert('Your browser does not support XMLHTTP!')
            return;
        }
        var url="/ManagedCare/AgileController";
        url=url+"?opperation=ValidateUserNameCommand&username="+str;
        xmlhttp.onreadystatechange=function(){stateChanged(element)};
        xmlhttp.open("POST",url,true);
        xmlhttp.send(null);
    }
    
function GetXmlHttpObject()
{
    if (window.ActiveXObject)
    {
        // code for IE6, IE5
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (window.XMLHttpRequest)
    {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        return new XMLHttpRequest();
    }
    
    return null;
}
    

    function GetDOMParser(xmlStr){
        var xmlDoc;
        if(window.DOMParser){
            parser=new DOMParser();
            xmlDoc= parser.parseFromString(xmlStr,"text/xml");
        }else{
            xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async="false";
            xmlDoc.loadXML(xmlStr);
        }
        return xmlDoc;
    }
    
    
    function stateChanged(element){
        
        if (xmlhttp.readyState==4)
        {
            var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
            if(result == "ValidateUserNameCommand") {
                var username = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=')+1, xmlhttp.responseText.lastIndexOf('$'));
                document.getElementById(element + '_error').innerHTML = "";
            }else if(result == "Error"){
                var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|')+1, xmlhttp.responseText.length);
                //if(forCommand == "ValidateUserNameCommand"){
                    document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
               // }
            }
        }
    }
        

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td align="center">
             <table align="center">
                    <!-- content goes here -->
                    <label class="header">Register User</label>
                    <br></br><br></br>
                   
                    <agiletags:ControllerForm name="addUser" validate="yes">
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <tr><agiletags:LabelTextBoxError elementName="name" displayName="Name" mandatory="yes" valueFromSession="yes"/></tr>
                        <tr><agiletags:LabelTextBoxError elementName="surname" displayName="Surname" mandatory="yes" valueFromSession="yes"/></tr>
                        <tr><agiletags:LabelTextBoxError elementName="username" displayName="Username" mandatory="yes" valueFromSession="yes" javaScript="onChange=\"validateUserName(this.value, 'username')\";"/></tr>
                        <tr><agiletags:LabelTextBoxError elementName="email" displayName="Email" mandatory="yes" valueFromSession="yes"/></tr>
                        <tr><agiletags:LabelPasswordError elementName="password" displayName="Password" mandatory="yes"/></tr>
                        <tr><agiletags:LabelPasswordError elementName="password2" displayName="Re-type Password" mandatory="yes"/></tr>
                        <tr><agiletags:LabelTextAreaError elementName="comments" displayName="Comments" mandatory="yes" valueFromSession="yes"/></tr>
                        <tr><agiletags:ButtonOpperation type="button" span="2" align="center" commandName="" displayname="Register" javaScript="onClick=\"validateRegister('AddUserCommand')\";"/></tr>
                     </agiletags:ControllerForm>
                     
                     <agiletags:ValidationScript enabled="yes"/>
                    <!-- content ends here -->
            </table>
        </td></tr></table>
    </body>
</html>

<%-- 
    Document   : UpdateUserProfile
    Created on : 2011/11/15, 04:29:56
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.tools.min.js"></script>       
        <script language="JavaScript">
            
             function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            
             function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('userId').value = index;
                document.forms[0].submit();
            }
            
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
             
                    <agiletags:ControllerForm name="updateUserProfile">
                        <table>
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="userId" id="userId" value="" />
                            
                            <tr><td><label class="header">Update Details</label></td></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Username" elementName="username" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Name" elementName="name" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="surname" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Email" elementName="email"  valueFromSession="yes"/></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td><label class="header">Responsibility Description</label></td></tr>
                            <agiletags:ResponsibilityCheckBoxLabel elementName="resp" numberPerRow="3"/>
                            <tr><td>&nbsp;</td></tr>
                            <agiletags:HiddenField elementName="workitem_id"/>
                            <agiletags:HiddenField elementName="user_id"/>
                            <tr>
                                <agiletags:ButtonOpperation type="button" align="left" commandName="SearchUsersProfilesCommand" displayname="Search" span="1" javaScript="onClick=\"submitWithAction('SearchUsersProfilesCommand')\";"/>
                                <agiletags:ButtonOpperation type="button" align="left" commandName="CancelUserProfileCommand" displayname="Cancel" span="1" javaScript="onClick=\"submitWithAction('CancelUserProfileCommand')\";"/>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <agiletags:NeoUserProfileSearchResultTable commandName="" javaScript=""/>
                        </table>
                    </agiletags:ControllerForm>
                        
                </td></tr></table>
    </body>
</html>

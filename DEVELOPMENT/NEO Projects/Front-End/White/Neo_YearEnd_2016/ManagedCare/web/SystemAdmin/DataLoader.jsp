<%--
    Document   : LoadMember
    Created on : 2010/09/14, 09:15:07
    Author     : KatlegoM
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="com.agile.security.webservice.NeoUser" %>


<script type="text/javascript" src="../Validation.js"></script>

<script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.css"></script>
<script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>

<link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
<script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrator</title>        
    </head>
    <body>        
        <br/>
        <!--agiletags:FileControllerForm name="LoadMember" validate="yes"-->
        <form action="/ManagedCare/AgileController?opperation=LoadDataFileCommand" enctype="multipart/form-data" method="POST">        
            <fieldset>
                <legend class="header">Data Load</legend>
                <br>
                    <tr>                        
                        <agiletags:LabelTextDisplay boldDisplay="no" displayName="File Name" elementName="FileLabel" />
                        <td>
                            <input type="file" name="fileName" size="50%">
                        </td>
                    </tr>
                <br><br>
                <input type="submit" id="submit" value="Upload">
            </fieldset>
        </form>        
    </body>
</html>
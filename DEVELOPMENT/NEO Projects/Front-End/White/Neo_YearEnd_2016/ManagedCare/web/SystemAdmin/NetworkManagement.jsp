<%-- 
    Document   : PracticeCapture
    Created on : Aug 20, 2010, 6:47:14 AM
    Author     : Leslie
--%>
<%@page import="com.koh.serv.PropertiesReader"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        
       <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript">
          
           $(document).ready(function(){
              
              $.post('/ManagedCare/AgileController', 
                            {
                                'id': new Date().getTime(),
                                'opperation': 'ForwardPNMaintenance'
                            }
                    ,function(serverResponse){
                        //alert("serverResponse: " + serverResponse);
                        if (serverResponse.substr(0, 7)=='success'.toString()) {
                            
                            //RETREIVE URL
                            var practiceManagementScreen = '<%= new PropertiesReader().getProperty("networkManagementPageURL")%>';
                            
                            //alert("practiceManagementScreen : " + practiceManagementScreen);
                            window.location.replace(practiceManagementScreen.toString());
                        } else {
                            alert("Unauthorized Access !!!!");
                        }
                        
              });
           });

        </script>
    </head>
    <body>
  </body>
</html>

<%-- 
    Document   : ActivateUserWL
    Created on : 2009/11/23, 09:15:57
    Author     : gerritj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
        <!-- content goes here -->
        <label class="header">Activate User List</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<agiletags:SuccessTag/>
        </br></br>
        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
            <agiletags:WorkListFormTable withAction="yes" workItemType="1" command="ActivateUserWLCommand" commandDisplayName="Activate"/>

        </table>

        <!-- content ends here -->
        </td></tr></table>
    </body>
</html>

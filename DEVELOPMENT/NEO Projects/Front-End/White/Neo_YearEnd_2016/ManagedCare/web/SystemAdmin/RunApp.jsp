<%-- 
    Document   : RunApp
    Created on : 2013/06/07, 01:44:54
    Author     : yuganp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <center><h1>Run Application - ${param.appName}</h1></center>
    <br>
    <agiletags:ControllerForm name="RunAppForm">
        <input type="hidden" name="opperation" value="RunAppCommand" />
        <input type="hidden" name="appName" value="${param.appName}"/>
        <input type="hidden" name="command" value="runApp" />
        <input type="hidden" name="appCommand" value="${param.runApp}"/>
        <input type="hidden" name="paramCount" value="${param.paramCount}"/>
        
        <table>
            <c:if test="${!(empty param.paramCount || param.paramCount == '0') }">
                <c:forEach begin="1" end="${param.paramCount}" var="cnt">
                    <tr>
                        <td width="160">Parameter ${cnt}</td>
                        <td><input name="param${cnt}"></td>
                    </tr>
                </c:forEach>
            </c:if>
                  
            <tr>
                <td width="160"><button type="submit">Execute</button></td>
                <td>&nbsp;</td>
            </tr>
        </table>
        
    </agiletags:ControllerForm>
    </body>
</html>

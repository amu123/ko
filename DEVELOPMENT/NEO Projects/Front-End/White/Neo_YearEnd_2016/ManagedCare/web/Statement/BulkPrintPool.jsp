<%-- 
    Document   : BulkPrintPool
    Created on : 2013/04/16, 05:03:38
    Author     : princes
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%
    String errorMsg = (String)request.getAttribute("errorMsg");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script language="JavaScript">
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }            
            function setParams(a,b,c){
                document.getElementById('opperation').value = a;
                document.getElementById('selectedFile').value = b;
                document.getElementById('selectedId').value = c;
//                document.forms[0].submit();
            }
        </script>
    </head>
    <body>

        <table width=100%><tr valign="left"><td width="50px"></td><td align="left">
                    <label class="header">Bulk Print Pool</label>
                    <br/><br/><br/>
                    <table>
                        <% if(errorMsg != null){ %>
                        <tr><td><label id="error" class="error"><%=errorMsg%></label></td></tr>
                        <% } %>

                                <agiletags:ControllerForm name="bulkPrintPool"> 
                            <input type="hidden" name="opperation" id="opperation" value="" />

                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <tr><td>
                                    <table>
                                        <tr>
                                            <agiletags:BulkPrintPoolList command="BulkPrintPoolCommand" />
                                        </tr>
                                    </table>
                                </td></tr>
                            </agiletags:ControllerForm> 
                    </table>
                </td></tr></table>
    </body>
</html>



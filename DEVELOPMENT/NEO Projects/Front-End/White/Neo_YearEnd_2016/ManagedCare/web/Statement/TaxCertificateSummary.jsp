<%-- 
    Document   : TaxCertificateSummary
    Created on : 12 May 2014, 12:17:19 PM
    Author     : almaries
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="../resources/jQuery/jquery-1.4.2.js"></script>

        <script language="JavaScript">
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            
            function submitWithAction1(action, actionParam) {
                document.getElementById('opperation').value = action;
                document.getElementById('opperationParam').value = actionParam;
                document.forms[0].submit();
            }
            
            function submitWithAction3(action, actionParam) {
                document.getElementById('opperation').value = action;
                document.getElementById('buttonPressedExport').value = actionParam;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <table width=100% height=100%>
            <tr valign="left"><td width="50px"></td><td align="left">
                    <label class="header">Tax Certificate</label>
                    <br/><br/><br/>
                    <table>
                        <agiletags:ControllerForm name="TaxCertificateDetails">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                            <input type="hidden" name="mcPageDirection" id="mcPageDirection" value="" />
                            <input type="hidden" name="depListValues" id="depListValues" value="" />
                            <input type="hidden" name="mcsClaimId" id="mcsClaimId" value="" />
                            <input type="hidden" name="treatmentFrom" id="treatmentFrom" value="" />
                            <input type="hidden" name="treatmentTo" id="treatmentTo" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="buttonPressedExport" id="buttonPressedExport" value="" />
                            <c:choose>
                                <c:when test="${productId == 1 || (productId == 2 && yearRange >= 2015)}">
                                    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <tr>
                                            <th align="left">Type</th>
                                            <th align="left">Amount</th>
                                            <th align="left">Action</th>
                                        </tr>
                                        <tr>
                                            <td><label class="label">Contributions</label></td>
                                            <td><label class="label">R ${sessionScope.ContributionAmount}</label></td>
                                            <td><button name="opperation" type="button" onClick="submitWithAction1('ViewTaxCertificateDetailsCommand', 'Contributions')">Details</button></td>
                                        </tr>
                                        <tr>
                                            <td><label class="label">Claims</label></td>
                                            <td><label class="label">R ${sessionScope.ClaimsAmount}</label></td>
                                            <td><input type="button" value="Export Details" onclick="submitWithAction3('LoadTaxCertClaimsTranCommand','Export');"></td>

                                        </tr>
                                    </table>
                                </c:when>
                                <c:otherwise>
                                    <tr><td><label>No supporting details for Spectramed members</label></td></tr>
                                </c:otherwise>
                            </c:choose>
                        </agiletags:ControllerForm>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>


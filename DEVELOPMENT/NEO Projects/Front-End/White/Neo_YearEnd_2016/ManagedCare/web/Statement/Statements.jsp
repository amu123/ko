<%-- 
    Document   : MemberStatements
    Created on : Jul 29, 2011, 9:26:52 AM
    Author     : Johan-NB
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>.
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>

        <script language="JavaScript">
            $(document).ready(function() {
                toggleStmtType(1);
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction1(action, actionParam) {
                document.getElementById('stmtDatesCorrect').value = $("#stmtDates").val();; 
                document.getElementById('opperation').value = action;
                document.getElementById('opperationParam').value = actionParam;
                document.forms[0].submit();
            }

            function submitWithAction(action, forField, onScreen) {
                document.getElementById('opperation').value = action;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('onScreen').value = onScreen;
                document.forms[0].submit();
            }

            function toggleStmtType(flagForBlur) {
                var str = $("#stmtType").val();
                var check = <%=session.getAttribute("statementScreenFlag")%>
                
                if (str === "99") {
                    $("#memRow").hide();
                    $("#provRow").hide();
                    $("#provRow1").hide();
                    $("#dateRow").hide();
                    $("#statementType").text("Statements");
                    clearFields();
                } else if (str === "1") {
                    $("#memRow").show();
                    $("#provRow").hide();
                    $("#provRow1").hide();
                    $("#dateRow").show();
                    $("#statementType").text("Member Statements");
                    if (flagForBlur === 1 && (check === null || check !== "flag")) {
                        $("#memberNo_text").blur();
                    }                     
                } else if (str === "2") {
                    $("#memRow").hide();
                    $("#provRow").show();
                    $("#provRow1").show();
                    $("#dateRow").show();
                    $("#statementType").text("Provider Statements");
                    if (flagForBlur === 1 && (check === null || check !== "flag")) {
                        $("#providerNo_text").blur();
                    }
                }
                
               <c:remove var="statementScreenFlag" scope="session"/>         
            }

            function toggleStmtDate() {
                $("#dateRow").load();
                $("#dateRow").show();
            }

            function getPrincipalMemberForNumber(str, element, statementScreenFlag) {
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                if (str.length > 0) {
                    var url = "/ManagedCare/AgileController";
                    url = url + "?opperation=GetMemberByNumberAndDirCommand&number=" + str + "&element=" + element + "&flag=" + statementScreenFlag;
                    var errorCheck = <%=session.getAttribute("memberNo_error")%>;

                    if (errorCheck === null || errorCheck === "") {
                        xmlhttp.onreadystatechange = function() {
                            stateChanged(element)
                        };
                        toggleStmtDate();
                        xmlhttp.open("POST", url, true);
                        xmlhttp.send(null);
                    } else {
                        clearFields();
                    }

                } else {
                    clearFields();
                }
            }

            //provider search
            function validateProvider(str, element, flag) {
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }

                if (str.length > 0) {
                    var productId = document.getElementById("schemeOption").value;
                    var url = "/ManagedCare/AgileController";
                    if (productId !== null && productId !== 99 && productId !== 0) {
                        url = url + "?opperation=GetProviderByNumberAndDirCommand&number=" + str + "&element=" + element + "&productId=" + productId + "&flag=" + flag;;
                        var errorCheck = <%=session.getAttribute("providerNo_error")%>;

                        if (errorCheck === null || errorCheck === "") {
                            xmlhttp.onreadystatechange = function() {
                                stateChanged(element);
                            };
                            toggleStmtDate();
                            xmlhttp.open("POST", url, true);
                            xmlhttp.send(null);
                        } else {
                            clearFields();
                        }
                    } else {
                        clearFields();
                    }
                } else {
                    clearFields();
                }
            }

            function clearFields() {
                $("#stmtDates").empty();
                document.getElementById('emailAddress').value = '';
            }


            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function stateChanged(element) {
                if (xmlhttp.readyState == 4)
                {
                    //alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    //alert(result);
                    if (result == "GetMemberByNumberAndDirCommand") {

                        var resultArr = xmlhttp.responseText.split("|");
                        var numberArr = resultArr[1].split("=");
                        var emailArr = resultArr[2].split("=");
                        var productArr = resultArr[3].split("=");

                        var number = numberArr[1];
                        var email = emailArr[1];
                        var product = productArr[1];

                        document.getElementById(element + '_error').innerText = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('emailAddress').value = email;
                        document.getElementById('productId').value = product;
                        var dateList = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('#') + 1, xmlhttp.responseText.lastIndexOf('*'));

                        var selectObj = document.getElementById("stmtDates");
                        var selectParentNode = selectObj.parentNode;
                        var newSelectObj = selectObj.cloneNode(false); // Make a shallow copy
                        selectParentNode.replaceChild(newSelectObj, selectObj);

                        document.getElementById("stmtDates").add(new Option('', '99'));

                        var dateSplit = dateList.split('!');
                        for (index = 0; index < dateSplit.length; index++) {
                            var opt = document.createElement("option");
                            opt.text = dateSplit[index];
                            opt.value = dateSplit[index];
                            document.getElementById("stmtDates").add(opt);
                        }


                        document.getElementById('searchCalled').value = '';

                    } else if (result == "GetProviderByNumberAndDirCommand") {
                        var number = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=') + 1, xmlhttp.responseText.lastIndexOf('|'));
                        var email = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=') + 1, xmlhttp.responseText.lastIndexOf('$'));
                        var dateList = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('#') + 1, xmlhttp.responseText.lastIndexOf('*'));

                        document.getElementById(element + '_error').innerText = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('emailAddress').value = email;
                        document.getElementById('searchCalled').value = '';

                        var selectObj = document.getElementById("stmtDates");
                        var selectParentNode = selectObj.parentNode;
                        var newSelectObj = selectObj.cloneNode(false); // Make a shallow copy
                        selectParentNode.replaceChild(newSelectObj, selectObj);

                        document.getElementById("stmtDates").add(new Option('', '99'));

                        var dateSplit = dateList.split('!');


                        for (index = 0; index < dateSplit.length; index++) {

                            var opt = document.createElement("option");
                            opt.text = dateSplit[index];
                            opt.value = dateSplit[index];
                            document.getElementById("stmtDates").options.add(opt);
                        }
                    } else if (result == "Error") {
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|') + 1, xmlhttp.responseText.length);
                        if (forCommand == "GetMemberByNumberAndDirCommand") {
                            document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            //document.getElementById(element + '_text').value = number;
                            // document.getElementById('emailAddress').value = email;
                            //document.getElementById('searchCalled').value = '';
                        } else if (forCommand == "GetProviderByNumberAndDirCommand") {
                            document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                        }
                    } else if (result == "Access") {
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|') + 1, xmlhttp.responseText.length);
                        if (forCommand == "GetMemberByNumberAndDirCommand") {
                            document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            //document.getElementById(element + '_text').value = number;
                            // document.getElementById('emailAddress').value = email;
                            //document.getElementById('searchCalled').value = '';
                        } else if (forCommand == "GetProviderByNumberAndDirCommand") {
                            document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                        }
                    }
                }
            }

            //screen validtion
            function validateStatementRequest(action, actionParam, validateEmail) {
                var stmtType = $("#stmtType").val();
                var errorCount = 0;
                var errorMsg = "";

                if (stmtType == "99") {
                    errorCount++;
                    errorMsg = "#stmtType_error-Please Select a Statement Type|";

                } else {
                    //statement type number validation
                    if (stmtType == "1") {
                        var memNo = $("#memberNo_text").val();
                        if (memNo == null || memNo == "") {
                            errorCount++;
                            errorMsg = "#memberNo_error-Please Enter a Member Number|";
                        }

                    } else if (stmtType == "2") {
                        var provNo = $("#providerNo_text").val();
                        if (provNo == null || provNo == "") {
                            errorCount++;
                            errorMsg = "#providerNo_error-Please Enter a Provider Number|";
                        }
                    }
                    //satement date validation
                    var stmtDate = $("#stmtDates").val();
                    if (stmtDate == "99" || stmtDate == null) {
                        errorCount++;
                        errorMsg = "#stmtDates_error-Please Select a Statement Date|";
                    }

                    //validate email
                    if (validateEmail == "yes") {
                        var email = $("#emailAddress").val();
                        if (email == null || email == "") {
                            errorCount++;
                            errorMsg = "#emailAddress_error-Please Enter a Email Address|";
                        }
                    }
                }
                //print errors
                if (errorCount > 0) {
                    var errors = errorMsg.split("|");
                    for (i = 0; i < errors.length; i++) {
                        var error = errors[i].split("-");
                        $(error[0]).text(error[1]);
                    }

                } else {
                    submitWithAction1(action, actionParam);
                }
            }

        </script>
    </head>

    <body>

        <table width=100% height=100%><tr valign="left"><td width="50px"></td><td align="left">
                    <label class="header" id="statementType"></label>
                    <br/><br/><br/>
                    <table>
                        <agiletags:ControllerForm name="getMemberStmt">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                            <input type="hidden" name="productId" id="productId" value="" />
                            <input type="hidden" name="exactCoverNum" id="exactCoverNum" value="1" />
                            <input type="hidden" name="stmtDatesCorrect" id="stmtDatesCorrect" value="" />
                            
                            <table> 
                                <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Statement Type" elementName="stmtType" errorValueFromSession="yes" lookupId="169" javaScript="onChange=\"toggleStmtType(1);\"" mandatory="yes" /></tr>
                                <tr id="memRow"><agiletags:LabelTextSearchText displayName="Member Number" elementName="memberNo" valueFromSession="Yes" searchFunction="Yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/Statement/Statements.jsp" javaScript="onBlur=\"getPrincipalMemberForNumber(this.value, 'memberNo','flag');\""/></tr>
                                <tr id="provRow"><agiletags:LabelProductDropDown displayName="Scheme" elementName="schemeOption" valueFromSession="Yes" javaScript="onChange=\"toggleStmtType(1);\""/></tr>
                                <tr id="provRow1"><agiletags:LabelTextSearchText displayName="Provider Number" elementName="providerNo" valueFromSession="Yes" searchFunction="Yes" searchOperation="ForwardToSearchProviderCommand" onScreen="/Statement/Statements.jsp" javaScript="onBlur=\"validateProvider(this.value, 'providerNo', 'flag');\""/></tr>
                                <tr id="dateRow"><agiletags:LabelDropDown displayName="Statement Dates" elementName="stmtDates" errorValueFromSession="yes" mandatory="no" javaScript="" /></tr>
                                <tr id="emailRow"><agiletags:LabelTextBoxError displayName="E-Mail Address" elementName="emailAddress"/></tr>
                            </table>
                            <table>
                                <tr>
                                    <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Cancel" javaScript="onClick=\"submitWithAction('ReloadStatementCommand')\";"/>
                                    <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Preview" javaScript="onClick=\"validateStatementRequest('SubmitStatementCommand', 'Preview', 'no')\";"/>
                                    <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Send" javaScript="onClick=\"validateStatementRequest('SubmitStatementCommand', 'Send', 'yes')\";"/>
                                </tr>
                            </table>
                        </agiletags:ControllerForm>

                    </table>

                </td></tr></table>

    </body>
</html>

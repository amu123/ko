<%-- 
    Document   : BulkStatements
    Created on : 2010/03/15, 04:26:03
    Author     : whauger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script language="JavaScript">

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="left"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Bulk Statement E-Mail</label>
                    <br></br><br></br>
                    <table>
                        <agiletags:ControllerForm name="sendStatement" validate="yes">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="searchCalled" id="searchCalled" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <tr><agiletags:LabelNeoLookupValueDropDown displayName="Type" elementName="statementType" lookupId="107"/></tr>
                            <tr><agiletags:LabelTextBoxDate displayname="Payment run date from" elementName="statementFromDate"/></tr>
                            <tr><agiletags:LabelTextBoxDate displayname="to" elementName="statementToDate"/></tr>
                            <tr><agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Submit" javaScript="onClick=\"submitWithAction('ReloadCallCenterAuth1Command')\";"/>
                            <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Cancel" javaScript="onClick=\"submitWithAction('SaveCallCenterAuth1Command')\";"/></tr>
                        </agiletags:ControllerForm>
                    </table>

                    <!-- content ends here -->
        </td></tr></table>
    </body>
</html>

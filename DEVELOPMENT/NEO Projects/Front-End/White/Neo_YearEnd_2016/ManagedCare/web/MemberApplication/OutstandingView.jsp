<%-- 
    Document   : OutstandingDocuments
    Created on : Jul 5, 2016, 11:51:01 AM
    Author     : sobongad
--%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags"%>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${applicationScope.Client == 'Sechaba'}">
    <agiletags:ControllerForm name="MemberOutstandingDocumentsForm">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>

        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Membership/MemberMaintenance.js"></script>
        <input type="hidden" name="opperation" id="opperation" value="MemberOutstandingUpdateCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="MemberApplication" />
        <input type="hidden" name="memberAppCoverNumber" id="memberAppCoverNumber" value="${memberAppCoverNumber}" />
        <input type="hidden" name="memberAppStatus" id="memberAppStatus" value="${requestScope.memberAppStatus}" />
        <input type="hidden" name="memberAppNumber" id="memberAppNumber" value="${memberAppNumber}" />
        <input type="hidden" name="memberAppType" id="memberAppType" value="${requestScope.memberAppType}" />
        <input type="hidden" name="groupEntityId" id="groupEntityId" value="${groupEntityId}" />
        <input type="hidden" name="target_div" value="${target_div}" />
        <input type="hidden" name="main_div" value="${main_div}" />
        <input type="hidden" name="od" value="${od}"/>    
                               
        <label class="header"> Outstanding Documents View</label>
        <hr>
        <br>
        <table>
            <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;font-family: 'Arial', Halvetica, sans-serif;font-size: 12px;">
                <tr>
                <thead>
                <th>Document Type</th>
                <th>Outstanding</th>
                </thead>
                <tr>
                <tbody>
                    <c:forEach var="docList" items="${docMeaning}">
                        <tr>
                            <td>${docList.value}</td>
                            <td><input type="checkbox" name="docList" value="${docList.id}"/></td>
                        </tr>
                    </c:forEach>

                </tbody>
                </tr>
            </table>
            <label class="error">* TICK Box: Select documents submitted then click Update.</label>
            <hr>
            <table id="MemberAppOustandingDocUpdateTable">
                <tr>
                    <td><input id="MemberAppDocUpdate" type="button" value="Update" onclick="submitFormWithAjaxPost(this.form, '${target_div}', this, '${main_div}', '${target_div}')"></td>
                </tr>
            </table>

        </agiletags:ControllerForm>
    </c:if>


<%-- 
    Document   : MemberAppConditionSpecific
    Created on : 15 Apr 2014, 2:27:51 PM
    Author     : almaries
--%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<agiletags:ControllerForm name="ConditionSpecificForm">
    <input type="hidden" name="opperation" id="opperation" value="MemberApplicationUnderwritingCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberAppConditionSpecific" />
    <input type="hidden" name="buttonPressed" id="conditionSpecificButtonPressed" value="" />
    <input type="hidden" name="memberEntityId" id="conditionSpecificEntityId" value="${memberCoverEntityId}" />
    <!--<input type="hidden" name="disorderId_text" id="conditionSpecificDisorderId" value="" /> -->
    <input type="hidden" name="memberAppNumber" id="MemberAppCondWait_application_number" value="${memberAppNumber}" />
    <input type="hidden" name="memberAppDepNo" id="CondWaitDepNo" value="${memberAppDepNo}" />
    <input type="hidden" name="memberAppICD" id="CondWaitICD" value="" />

    <br/>
    <br/>
    <label class="header">Add Condition Specific</label>
    <hr>
    <br/>
    <br/>
    <table>                                
        <tr><agiletags:LabelTextSearchTextDiv displayName="ICD" elementName="Disorder_diagCode" mandatory="yes" onClick="document.getElementById('conditionSpecificButtonPressed').value = 'SearchICD'; getPageContents(document.forms['ConditionSpecificForm'],null,'overlay','overlay2');"/>        </tr>
        <tr><agiletags:LabelTextDisplay displayName="Description" elementName="Disorder_diagName" valueFromSession="no" javaScript="" /></tr>
        <!--<tr><agiletags:LabelTextDisplay displayName="Id" elementName="disorderId_text" valueFromSession="no" javaScript="" /></tr> -->
        <tr id="conditionSpecificId" style="display: none" ><agiletags:LabelTextBoxError displayName="Id" elementName="disorderId_text" valueFromSession="no"/></tr>
        <tr id="ConditionSpecificStartDateRow"><td align="left" width="160px"><label>Period</label></td><td align="left" width="200px">
                <select style="width:215px" name="conditionSpecificDeptList" id="conditionSpecificDeptList">
                    <option value="1" selected>1 Month</option>
                    <option value="2" selected>2 Months</option>
                    <option value="3" selected>3 Months</option>
                    <option value="4" selected>4 Months</option>
                    <option value="5" selected>5 Months</option>
                    <option value="6" selected>6 Months</option>
                    <option value="7" selected>7 Months</option>
                    <option value="8" selected>8 Months</option>
                    <option value="9" selected>9 Months</option>
                    <option value="10" selected>10 Months</option>
                    <option value="11" selected>11 Months</option>
                    <option value="12" selected>12 Months</option>                                                
                </select>
            </td></tr>  
        <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="startDate" valueFromSession="yes" mandatory="no"/></tr>
        <agiletags:LabelTextBoxDateReq  displayname="End Date" elementName="endDate" valueFromSession="yes" mandatory="no"/></tr>
    </table>

    <br/>
    <table id="ConditionSpecificTable">
        <tr>
            <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
            <td>
                <input type="button" value="Save" onclick="document.getElementById('conditionSpecificButtonPressed').value='SaveButton';submitFormWithAjaxPost(this.form, 'Underwriting', null,'${main_div}', '${target_div}');">
                <label class="error" id="saveButton_error"></label>
            </td>
        </tr>
    </table>
</agiletags:ControllerForm>
        

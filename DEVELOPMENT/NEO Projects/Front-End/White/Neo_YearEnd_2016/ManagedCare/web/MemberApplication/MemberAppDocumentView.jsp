<%-- 
    Document   : MemberAppDocumentView
    Created on : Jul 2, 2013, 10:49:56 AM
    Author     : sphephelot
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<br>
      
<label class="subheader">Search Results</label>
<hr>
<c:choose>
    <c:when test="${empty indexList}">
        <span class="label">No Document found</span>
    </c:when>
    <c:otherwise>
<label id="DocView_Msg" class="label"></label>
        <agiletags:ControllerForm name="documentIndexing">
            <input type="hidden" name="folderList" id="folderList" value="" />
            <input type="hidden" name="fileLocation" id="fileLocation" value="" />
            <input type="hidden" name="opperation" id="opperation" value="MemberDocumentDisplayViewCommand" />
            <input type="hidden" name="searchCalled" id="searchCalled" value=""/>
            <input type="hidden" name="onScreen" id="onScreen" value="" />
            <input type="hidden" name="opperationParam" id="opperationParam" value="" />
            
            <c:set var="Email" value="${requestScope.ContactDetails_email}"/>
           
            <br/>
            <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                <tr>
                    <th>Member Number</th>
                    <th>Type</th>
                    <th>Location</th>
                    <th>Current Date</th>                                              
                    <th></th>
                    <th></th>
                </tr>
                <c:forEach var="entry" items="${indexList}">
                    <tr class="label">
                        <td>${entry['entityNumber']}</td>
                        <td>${entry['docType']}</td>
                        <td>${entry['location']}</td>
                        <td><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.generationDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.generationDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.generationDate.day}"/></td>
                        <td><input id="viewB" type="button" value="View"  onClick="submitActionAndFileView(this.form, 'MemberDocumentDisplayViewCommand','${entry['location']}'); " /></td>
                        <td><input id="printB" type="button" value="Print" onClick="submitActionAndFile(this.form, 'AddToPrintPoolCommand','${entry['indexId']}'); submitFormWithAjaxPost(this.form, 'MemberDocuments');" /></td>                    
                    </tr>
                </c:forEach>

            </table>
        </agiletags:ControllerForm>
    </c:otherwise>
</c:choose>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  
<label class="header">ICD10 Search test</label>
    

     
    <agiletags:ControllerForm name="ICD10SearchForm">
       
         <input type="hidden" name="opperation" id="opperation" value="ICD10SearchCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="DisorderSearch" />
        <input type="hidden" name="target_div" value="${target_div}" />
        <input type="hidden" name="main_div" value="${main_div}" />
        <input type="hidden" name="diagCode" id="icd10DiagCode" value="" />
        <input type="hidden" name="diagName" id="icd10DiagName" value="" />
        <input type="hidden" name="diagCodeId" id="icd10DiagCodeId" value="${diagCodeId}" />
        <input type="hidden" name="diagNameId" id="icd10DiagNameId" value="${diagNameId}" />
        <table>
            <tr><agiletags:LabelTextBoxError displayName="Code" elementName="ICDCode"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Description" elementName="ICDDescription"/></tr>
            <tr>
                <td><input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
                
                <td align="right"><input type="button" value="Search" onclick="submitFormWithAjaxPost(this.form, 'ICD10SearchResults');"></td>
            </tr>
        </table>
      
      <div style ="display: none"  id="ICD10SearchResults"></div>
      </agiletags:ControllerForm>

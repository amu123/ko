<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">ICD10 Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty ICD10SearchResults}">
            <span>No ICD10 found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Code</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${ICD10SearchResults}">
                        <tr>
                            <td><label>${entry.code}</label></td>
                            <td><label>${entry.description}</label></td>
                            
                            <td><input type="button" value="Select" onclick="setElemValues({'${diagCodeId}':'${entry.code}','${diagNameId}':'${agiletags:escapeStr(entry.description)}','${diagCodeId2}':'${entry.id}'});swapDivVisbible('${target_div}','${main_div}');"/></td> 
                      
                        </tr>
                    </c:forEach>

                </table>
            </c:otherwise>
        </c:choose>

   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<br>
<label class="subheader">Member Application Search Results</label>
<hr>
<br>
<c:choose>
    <c:when test="${empty MemAppSearchResults}">            
        <c:if test="${appSearchResultsMessage == 'Yes'}" >
            <span class="label" style="color: red" >User access denied. Please contact your supervisor</span>
        </c:if> 
        <c:if test="${appSearchResultsMessage != 'Yes' && userLength != 'Yes'}" >
            <span class="label">No Member Details found</span>
        </c:if>
            <c:if test="${userLength == 'Yes'}" >
            <span class="label" style="color: red" >User Name must be more than two characters long</span>
        </c:if>
    </c:when>
    <c:otherwise>
        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
            <tr>
                <th>Member Number</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>DOB</th>
                <th>Id No</th>
                <th>Option</th>
                <th>Benefit Start Date</th>
                <th>Status</th>
                <th>Name</th>
                <th></th>
            </tr>
            <c:forEach var="entry" items="${MemAppSearchResults}">
                <tr>
                    <td><label>${entry['coverNumber']}</label></td>
                    <td><label>${entry['name']}</label></td>
                    <td><label>${entry['surname']}</label></td>
                    <td><label>${entry['dob']}</label></td>
                    <td><label>${entry['idNo']}</label></td>
                    <td><label>${entry['optionName']}</label></td>
                    <td><label>${entry['startDate']}</label></td>
                    <td><label>${entry['statusName']}</label></td>
                    <td><label>${entry['employeeName']}</label></td>
                    <td><button type="submit" value="${entry['appNum']}" onclick="setMemberAppValues(this.value,'${agiletags:escapeStr(entry['name'])}', '${entry['status']}');">View</button></td>
                </tr>
            </c:forEach>

        </table>
    </c:otherwise>
</c:choose>




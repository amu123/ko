
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<agiletags:ControllerForm name="MemberAppSpecificQuestionsForm">
    <input type="hidden" name="opperation" id="opperation" value="MemberApplicationSpecificQuestionsCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="" />
    <input type="hidden" name="memberAppNumber" id="MemberAppSpecificQuestions_application_number" value="${memberAppNumber}" />
    <input type="hidden" name="buttonPressed" id="MemberAppSpecificQuestions_button_pressed" value="" />
    <input type="hidden" name="memberAppSpecId" id="memberAppSpecQId" value="" />
    <input type="hidden" name="memberAppType" id="memberAppSpecType" value="${requestScope.memberAppType}" />

    <label class="header">Specific Health Questions</label>
    <hr>
    <br>
    <table id="MemberAppSpecificQuestionsTable">
        <c:forEach var="entry" items="${MemAppSpecificQuestions}">
            <tr id="Member_Specific_${entry.questionNumber}_Row">
                <td width ="20"><label>${entry.questionValue}.</label></td>
                <td width="60%"><label>${entry.description}</label></td>
                <td>
                    <select name="Member_Specific_${entry.questionNumber}_${entry.questionValue}" id="Member_Specific_${entry.questionNumber}">
                        <option value="0" ${empty entry.answerValue || entry.answerValue == "0" ? "selected" : ""}>No</option>
                        <option value="1" ${empty entry.answerValue || entry.answerValue == "0" ? "" : "selected"}>Yes</option>
                    </select>
                </td>
            </tr>
        </c:forEach>
    </table>
    <c:if test="${persist_user_viewMemApps == false}">
        <hr>
        <table id="MemberAppSpecificSaveTable">
            <tr>
                <td><input type="reset" value="Reset"></td>
                <td><input name="SaveButton" type="button" value="Save" onclick="document.getElementById('MemberAppSpecificQuestions_button_pressed').value = this.value;submitFormWithAjaxPost(this.form, 'MemberApplicationSpecific', this)"></td>
            </tr>
        </table>
    </c:if>
    <br>
    <br>
    <label class="subheader">Details</label>
    <hr>
    <c:if test="${persist_user_viewMemApps == false}">
        <input type="button" name="AddSpecificDetailsButton" value="Add Details" onclick="document.getElementById('MemberAppSpecificQuestions_button_pressed').value = this.value; getPageContents(this.form,null,'main_div','overlay');"/>
    </c:if>
    <br>
    <table id="MemberAppSpecificQuestionsDetailsTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%">
        <tr>
            <th>Question</th>
            <th>Dependant</th>
            <th>Date</th>
            <th>Disorder</th>
            <th>Treatment</th>
            <th>Consulting Doctor</th>
            <th>Current Condition</th>
            <th></th>
            <th></th>
        </tr>
        <c:forEach var="entry" items="${MemberAppSpecDetails}">
            <tr class="label">
                <td>${entry.questionNumber}</td>
                <td>${MemberAppGenDepMap[entry.dependentNumber]}</td>
                <td>${entry.specificDateAsString}</td>
                <td>${entry.diagCode}</td>
                <td>${entry.treatment}</td>
                <td>${entry.doctor}</td>
                <td>${entry.conditionValue}</td>
                <td><input type="button" value="Edit" name="EditButton" onclick="document.getElementById('memberAppSpecQId').value=${entry.specificDetailId};document.getElementById('MemberAppSpecificQuestions_button_pressed').value='EditButton';getPageContents(this.form,null,'main_div','overlay');"></td>
                <td><input type="button" value="Remove" name="RemoveButton" onclick="document.getElementById('memberAppSpecQId').value=${entry.specificDetailId};document.getElementById('MemberAppSpecificQuestions_button_pressed').value='RemoveButton';submitFormWithAjaxPost(this.form, 'SpecificQuestions');"></td>
            </tr>
        </c:forEach>
    </table>
</agiletags:ControllerForm>
</body>
</html>
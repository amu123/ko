
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<agiletags:ControllerForm name="MemberDependantAppAddForm">
    <input type="hidden" name="opperation" id="opperation" value="MemberDependantAppCommand"/>
    <input type="hidden" name="onScreen" id="onScreen" value="MemberDependantAppAdd"/>
    <input type="hidden" name="memberAppNumber" id="memberAppNumber" value="${memberAppNumber}" />
    <input type="hidden" name="memberAppDepNumber" id="memberAppDepNumber" value="${memberAppDepNumber}"/>
    <input type="hidden" name="memberAppCoverNumber" value="${memberAppCoverNumber}"/>
    <input type="hidden" name="target_div" value="${target_div}"/>
    <input type="hidden" name="main_div" value="${main_div}"/>
    <input type="hidden" name="buttonPressed" id="memberAppDepAddButtonPressed" value=""/>
    <input type="hidden" name="optID" id="optID" value="${requestScope.optionId}"/>
    
    <label class="header">Add Dependant</label>
    <br>
    <br>
    <label class="subheader">${memberAppDepNumber == 1? "Spouse / Partner / " : ""}Dependant ${memberAppDepNumber}</label>
    <hr>
    <label name="ageWarn" id="ageWarn" style="size: 10; color: red"></label>
    <table>
        <c:if test="${!empty optionId && optionId eq '100'}">
            <tr id="MemberDependantApp_network_IPARow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Network IPA" lookupId="351" elementName="MemberApp_network_IPA" mandatory="no" errorValueFromSession="yes" firstIndexSelected="no"/></tr>
        </c:if>
        <tr><agiletags:LabelNeoMapListDropDownErrorReq displayName="Dependant Type"  elementName="MemberDependantApp_dependant_type" mapList="DependantTypeList" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="yes" javaScript="onChange=\"dateCheck()\"" />        </tr>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Title"  elementName="MemberDependantApp_title" lookupId="24" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr><agiletags:LabelTextBoxErrorReq displayName="Initials" elementName="MemberDependantApp_initials" valueFromRequest="MemberDependantApp_initials" mandatory="yes"/>        </tr>
        <tr><agiletags:LabelTextBoxErrorReq displayName="First Name" elementName="MemberDependantApp_firstName" valueFromRequest="MemberDependantApp_firstName" mandatory="yes"/>        </tr>
        <tr><agiletags:LabelTextBoxErrorReq displayName="Preferred Name" elementName="MemberDependantApp_secondName" valueFromRequest="MemberDependantApp_secondName" mandatory="no"/>        </tr>
        <tr><agiletags:LabelTextBoxErrorReq displayName="Surname" elementName="MemberDependantApp_lastName" valueFromRequest="MemberDependantApp_lastName" mandatory="yes"/>        </tr>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Gender"  elementName="MemberDependantApp_gender" lookupId="23" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr><agiletags:LabelTextBoxDateReq  displayname="Date Of Birth" elementName="MemberDependantApp_DateOfBirth" valueFromSession="yes" mandatory="yes"  javascript="onBlur=\"dateCheck()\""/>        </tr>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Identification Type"  elementName="MemberDependantApp_identificationType" lookupId="16" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr><agiletags:LabelTextBoxErrorReq displayName="Identification Number" elementName="MemberDependantApp_identificationNumber" valueFromRequest="MemberDependantApp_identificationNumber" mandatory="yes"/>        </tr>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Relationship Type"  elementName="MemberDependantApp_relationshipType" lookupId="213" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr><agiletags:LabelTextBoxErrorReq displayName="Relationship Other" elementName="MemberDependantApp_relationshipOther" valueFromRequest="MemberDependantApp_relationshipOther" mandatory="no"/>        </tr>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Married?"  elementName="MemberDependantApp_maritalStatus" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Disabled?"  elementName="MemberDependantApp_disabledInd" lookupId="67" mandatory="no" errorValueFromSession="yes" javaScript="onChange=\"dateCheck()\""/>        </tr>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Student?"  elementName="MemberDependantApp_studentInd" lookupId="67" mandatory="no" errorValueFromSession="yes" javaScript="onChange=\"dateCheck()\""/>        </tr>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Financially Dependant?"  elementName="MemberDependantApp_financiallyDependantInd" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Earn Income?"  elementName="MemberDependantApp_earnIncomeInd" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr><agiletags:LabelTextBoxErrorReq displayName="Monthly Income" elementName="MemberDependantApp_monthlyIncome" valueFromRequest="MemberDependantApp_monthlyIncome" mandatory="no"/>        </tr>
        <tr><agiletags:LabelTextBoxErrorReq displayName="Height (cm)" elementName="MemberDependantApp_height" valueFromRequest="MemberDependantApp_height" mandatory="no"/>        </tr>
        <tr><agiletags:LabelTextBoxErrorReq displayName="Weight (kg)" elementName="MemberDependantApp_weight" valueFromRequest="MemberDependantApp_weight" mandatory="no"/>        </tr>
    </table>
    <br>
    <c:choose>
        <c:when test="${!empty optionId && optionId eq '100'}">
            <br>
            <label class="subheader">Nominated Practitioner Details</label>
            <hr>
            <label class="subheader">Doctor Details:</label>
            <br>
            <table id="DoctorTable">
                <tr id="MemberDependantApp_doctorNameRow"><agiletags:LabelTextSearchWithNoText displayName="Doctor Name" elementName="MemberDependantApp_doctorName" mandatory="no" onClick="document.getElementById('memberAppDepAddButtonPressed').value = 'doctorSearch'; getPageContents(document.forms['MemberDependantAppAddForm'],null,'overlay','overlay2');"/></tr>
                <tr id="MemberDependantApp_doctorPracticeNumberRow"><agiletags:LabelTextBoxErrorReq displayName="BHF Practice Number"  elementName="MemberDependantApp_doctorPracticeNumber" mandatory="no" valueFromRequest="MemberDependantApp_doctorPracticeNumber"/></tr>
                <tr id="MemberDependantApp_doctorNetworkRow"><agiletags:LabelTextBoxErrorReq displayName="Network"  elementName="MemberDependantApp_doctorNetwork" mandatory="no" enabled="false" valueFromRequest="MemberDependantApp_doctorNetwork"/></tr>
            </table>
            <hr>
            <label class="subheader">Dentist Details:</label>
            <br>
            <table id="DentistTable">
                <tr id="MemberDependantApp_dentistNameRow"><agiletags:LabelTextSearchWithNoText displayName="Dentist Name" elementName="MemberDependantApp_dentistName" mandatory="no" onClick="document.getElementById('memberAppDepAddButtonPressed').value = 'dentistSearch'; getPageContents(document.forms['MemberDependantAppAddForm'],null,'overlay','overlay2');"/></tr>
                <tr id="MemberDependantApp_dentistPracticeNumberRow"><agiletags:LabelTextBoxErrorReq displayName="BHF Practice Number"  elementName="MemberDependantApp_dentistPracticeNumber" mandatory="no"  valueFromRequest="MemberDependantApp_dentistPracticeNumber"/></tr>
                <tr id="MemberDependantApp_dentistNetworkRow"><agiletags:LabelTextBoxErrorReq displayName="Network"  elementName="MemberDependantApp_dentistNetwork" mandatory="no" enabled="false" valueFromRequest="MemberDependantApp_dentistNetwork"/></tr>
            </table>
            <hr>
            <label class="subheader">Optometrist Details:</label>
            <br>
            <table id="OptometristTable">
                <tr id="MemberDependantApp_optometristNameRow"><agiletags:LabelTextSearchWithNoText displayName="Optometrist Name" elementName="MemberDependantApp_optometristName" mandatory="no" onClick="document.getElementById('memberAppDepAddButtonPressed').value = 'optometristSearch'; getPageContents(document.forms['MemberDependantAppAddForm'],null,'overlay','overlay2');"/></tr>
                <tr id="MemberDependantApp_optometristPracticeNumberRow"><agiletags:LabelTextBoxErrorReq displayName="BHF Practice Number"  elementName="MemberDependantApp_optometristPracticeNumber" mandatory="no"  valueFromRequest="MemberDependantApp_optometristPracticeNumber"/></tr>
                <tr id="MemberDependantApp_optometristNetworkRow"><agiletags:LabelTextBoxErrorReq displayName="Network"  elementName="MemberDependantApp_optometristNetwork" mandatory="no" enabled="false" valueFromRequest="MemberDependantApp_optometristNetwork"/></tr>
            </table>
        </c:when>
        <c:otherwise>
            <table id="NominatedDependantDoctorTable" style="display:table;">
                <tr><td colspan="5"><label class="subheader">Nominated Practitioner Details</label></td></tr>        
                <tr><agiletags:LabelTextBoxErrorReq displayName="Practice Name" elementName="MemberDependantApp_principalPracticeName" valueFromRequest="MemberDependantApp_principalPracticeName" mandatory="no"/>        </tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="Telephone Number" elementName="MemberDependantApp_principalNonPracticeTel" valueFromRequest="MemberDependantApp_principalNonPracticeTel" mandatory="no"/>        </tr>        
                <tr><agiletags:LabelTextBoxErrorReq displayName="Practice Number" elementName="MemberDependantApp_principalPracticeNumber" valueFromRequest="MemberDependantApp_principalPracticeNumber" mandatory="no"/>        </tr>
                <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Region"  elementName="MemberDependantApp_principalRegion" lookupId="198" mandatory="no" errorValueFromSession="yes"/>        </tr>
            </table>
        </c:otherwise>
    </c:choose>
    <br>
    <table id="MemberAppDepSaveTable">
        <tr>
            <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}', '${main_div}');"></td>
            <td><input type="button" value="Save" onclick="document.getElementById('memberAppDepAddButtonPressed').value = 'SaveButton'; submitFormWithAjaxPost(this.form, 'Dependants', null, '${main_div}', '${target_div}');"></td>
        </tr>
    </table>
</agiletags:ControllerForm>
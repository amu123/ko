<%-- 
    Document   : SearchPracticeResults
    Created on : Jul 8, 2016, 1:04:43 PM
    Author     : charlh
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<br>
<label class="subheader">Practice Search Results</label>
<hr>
<br>
<c:choose>
    <c:when test="${empty PracticeSearchResults}">
        <span class="label">No Practice found</span>
    </c:when>
    <c:otherwise>        
        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
            <tr>
                <th>Practice Number</th>
                <th>Practice Name</th>
                <th>Practice Type</th>
                <th>Practice Network</th>
                <th></th>
            </tr>
            <c:forEach var="entry" items="${PracticeSearchResults}">
                <tr>
                    <td><label>${entry.practiceNumber}</label></td>
                    <td><label>${entry.practiceName}</label></td>
                    <td><label>${entry.practiceType}</label></td>
                    <td><label>${entry.practiceNetwork}</label></td>
                    <td><input type="button" value="Select" onclick="getValues('${entry.practiceNumber}', '${entry.practiceName}', '${entry.practiceNetwork}'); swapDivVisbible('overlay', 'main_div');"/></td>
                </tr>
            </c:forEach>
        </table>
    </c:otherwise>
</c:choose>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

  <agiletags:ControllerForm name="MemberAppActivateForm">
    <input type="hidden" name="opperation" id="opperation" value="MemberApplicationActivateCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberApplicationActivate" />
    <input type="hidden" name="memberAppNumber" id="memberAppStatusNumber" value="${memberAppNumber}" />
    <input type="hidden" name="target_div" value="${target_div}" />
    <input type="hidden" name="main_div" value="${main_div}" />

    <label class="header">Activate Member</label>
    <hr>
    <br>
     <table>
         <!--<tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MemberAppActivate_start_date" /></tr>-->
         <tr>
             <td><p>Start Date:</p></td>
             <td><input type="text" name="MemberAppActivate_start_date" value="${inceptionDate}" disabled="disabled"></td>
             <c:if test="${empty inceptionDate}">
             <td><label class="error">* No inception date found. Provide inception date on capture screen.</label></td>
             </c:if>
             
         </tr>
      </table>
      
    <hr>
    <table id="MemberAppChronicDetailsSaveTable">
        <tr>
            <td><input type="button" value="Close" onclick="if (document.getElementById('MemberAppActiveButton').style.display == 'none') {window.location='/ManagedCare/Security/Welcome.jsp'; } else {swapDivVisbible('${target_div}','${main_div}');}"></td>
            <td><input id="MemberAppActiveButton" type="button" <c:if  test="${empty inceptionDate}">disabled="disabled"</c:if> value="Activate" onclick="this.style.display='none'; submitFormWithAjaxPost(this.form, '${target_div}', this,'${main_div}', '${target_div}');"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>
    <div id="MemberAppActivateResults"></div>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <agiletags:ControllerForm name="MemberAppUnderwritingForm">
    <input type="hidden" name="opperation" id="MemberAppUnderwriting_opperation" value="" />
    <input type="hidden" name="onScreen" id="MemberAppUnderwriting_onScreen" value="" />
    <agiletags:HiddenField elementName="MemberAppUnderwriting_billingId"/>
    <agiletags:HiddenField elementName="MemberAppUnderwriting_employerEntityId"/>
    <label class="header">Member Application Underwriting</label>
    <hr>

      <table id="MemberAppUnderwritingTable">
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Apply Waiting Period"  elementName="MemberAppUnderwriting_waitingPeriod" lookupId="67" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="yes" javaScript="onchange=\"setElemStyle('MemberAppWaitingPeriodDiv',this.value == '1'? 'block' : 'none');\""/>        </tr>
      </table>
      <div id="MemberAppWaitingPeriodDiv" style="display:none;">
          <c:forEach var="entry" items="${DependentList}">
              <br>
              <label class="header">Dependant: ${entry.value}</label>
              <table id="MemberAppUnderWritingTable_${entry.id}">
                <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="General Waiting Period"  elementName="MemberAppUnderwriting_generalWaiting_${entry.id}" lookupId="67" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="yes" javaScript="onchange=\"setElemStyle('MemberAppUnderwriting_start_date_row_${entry.id}',this.value == '1'? 'table-row' : 'none');setElemStyle('MemberAppUnderwriting_end_date_row_${entry.id}',this.value == '1'? 'table-row' : 'none');\""/>        </tr>
                <tr id="MemberAppUnderwriting_start_date_row_${entry.id}" style="display:none;"><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MemberAppUnderwriting_start_date_${entry.id}" valueFromSession="yes" mandatory="yes"/>        </tr>
                <tr id="MemberAppUnderwriting_end_date_row_${entry.id}" style="display:none;"><agiletags:LabelTextBoxDateReq  displayname="End Date" elementName="MemberAppUnderwriting_end_date_${entry.id}" valueFromSession="yes" mandatory="yes"/>        </tr>
                <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Allow PMB"  elementName="MemberAppUnderwriting_allowPMB" lookupId="67" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="yes" />        </tr>
                <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Condition Specific"  elementName="MemberAppUnderwriting_conditionSpecific" lookupId="67" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="yes" javaScript="onchange=\"setElemStyle('MemberAppConditionDiv',this.value == '1'? 'block' : 'none');\""/>        </tr>
              </table>
              <br>
              <div id="MemberAppConditionDiv" style="display:none;">
                <label class="header">Condition Specific Waiting Periods</label>
                <br>
                <input type="button" value="Add Details" >
                
              </div>
          </c:forEach>
          
      </div>
    <br>
    <table id="MemberAppUnderwritingSaveTable">
        <tr>
           <td><input type="reset" value="Reset"></td>
           <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'MemberAppUnderwriting', this)"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>

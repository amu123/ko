
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

  <agiletags:ControllerForm name="MemberAppHospitalDetailForm">
    <input type="hidden" name="opperation" id="opperation" value="MemberApplicationHospitalCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberApplicationHospitalDetails" />
    <input type="hidden" name="memberAppNumber" id="memberAppHospNumber" value="${memberAppNumber}" />
    <input type="hidden" name="memberAppHospId" id="memberAppHospId" value="${memberAppHospId}" />
    <input type="hidden" name="target_div" value="${target_div}" />
    <input type="hidden" name="main_div" value="${main_div}" />
    <input type="hidden" name="buttonPressed" id="MemberAppHospitalDetails_button_pressed" value="" />

    <label class="header">Add Hospital Admission Detail</label>
    <hr>
    <br>
     <table>
                <tr><agiletags:LabelNeoMapListDropDownErrorReq displayName="Dependant"  elementName="Hospital_detail_dependent" mapList="DependentList" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="yes" />        </tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="Details" elementName="Hospital_detail_details" valueFromRequest="Hospital_detail_details" mandatory="yes"/>        </tr>
                <tr><agiletags:LabelTextBoxDateReq  displayname="Date" elementName="Hospital_detail_date" valueFromSession="yes" mandatory="yes"/>        </tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="Reason" elementName="Hospital_detail_reason" valueFromRequest="Hospital_detail_reason" mandatory="yes"/>        </tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="Doctor" elementName="Hospital_detail_doctor" valueFromRequest="Hospital_detail_doctor" mandatory="yes"/>        </tr>
                <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Current Condition"  elementName="Hospital_detail_condition" lookupId="199" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="yes" />        </tr>
      </table>
      
    <hr>
    <table id="MemberAppHospitalDetailsSaveTable">
        <tr>
           <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
           <td><input type="button" value="Save" onclick="document.getElementById('MemberAppHospitalDetails_button_pressed').value='SaveButton';submitFormWithAjaxPost(this.form, 'HospitalAdmission', null,'${main_div}', '${target_div}');"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>

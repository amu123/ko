<%--
    Document   : Member Application Search
    Created on : 2012/03/16, 11:06:14
    Author     : yuganp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>

        <script>
            function setMemberAppValues(id, name, status) {
                document.getElementById("memberNumberRes").value = id;
                document.getElementById("memberNameRes").value = name;
                document.getElementById("memberStatusRes").value = status;
            }
        </script>
    </head>
    <body>

    <label class="header">Member Application Search</label>
    <hr>
    <br>
     <agiletags:ControllerForm name="EmployerSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="MemberApplicationSearchCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="MemberApplicationSearch" />
        <input type="hidden" name="menuResp" id="menuResp" value="${param.menuResp}" />
        <table>
            <tr><agiletags:LabelTextBoxError displayName="Member Number" elementName="memberNumber"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="ID Number" elementName="memberIdNum"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Member Name" elementName="memberName"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Member Surname" elementName="memberSurname"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="User Name" elementName="userName"/></tr>
            <tr><td colspan="2" align="right"><input type="button" value="Search" onclick="submitFormWithAjaxPost(this.form, 'MemberApplicationSearchResults', this);"></td></tr>            
        </table>
      </agiletags:ControllerForm>

     <agiletags:ControllerForm name="MemberApplicationSearchResultsForm">
        <input type="hidden" name="opperation" id="search_opperation" value="MemberApplicationViewCommand" />
        <input type="hidden" name="onScreen" id="search_onScreen" value="MemberApplicationSearchResults" />
        <input type="hidden" name="memberNumber" id="memberNumberRes" value="">
        <input type="hidden" name="memberName" id="memberNameRes" value="">
        <input type="hidden" name="memberStatus" id="memberStatusRes" value="">
        <input type="hidden" name="menuResp" id="menuResp" value="${param.menuResp}" />
      <div style ="display: none"  id="MemberApplicationSearchResults"></div>
      </agiletags:ControllerForm>

    </body>
</html>

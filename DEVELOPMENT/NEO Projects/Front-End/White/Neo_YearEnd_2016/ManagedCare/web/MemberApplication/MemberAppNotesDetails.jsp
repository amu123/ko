<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<br>
<label class="subheader">Member Application Notes Details</label>
<hr>
<br>
<c:if test="${persist_user_viewMemApps == false}">
    <c:if test="${!(notesListSelect == '99' || notesListSelect == '7' || param.menuResp == 'customerCare')}">
        <input type="button" name="AddNotesButton" value="Add Note" onclick="document.getElementById('MemberAppNotes_button_pressed').value = this.value; getPageContents(this.form,null,'main_div','overlay');"/>
        <br><br>    
    </c:if>
</c:if>
<c:choose>
    <c:when test="${empty MemberAppNoteDetails}">
        <span class="label">No Notes available</span>
    </c:when>
    <c:otherwise>
        <table width="90%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
            <tr>
                <th>Date</th>
                <c:if test="${notesListSelect == '7'}">
                    <th>Type</th>
                </c:if>
                <th>User</th>
                <th>Note</th>
                <th></th>
            </tr>
            <c:forEach var="entry" items="${MemberAppNoteDetails}">
                <tr>
                    <td><label>${entry['noteDate']}</label></td>
                    <c:if test="${notesListSelect == '7'}">
                        <td><label>${entry['noteGroup']}</label></td>
                    </c:if>
                    <td><label>${entry['noteUser']}</label></td>
                    <td><label>${entry['noteDetails']}</label></td>
                    <td><input type="button" value="View" name="ViewButton" onclick="document.getElementById('MemberAppNotes_note_id').value=${entry['noteId']};document.getElementById('MemberAppNotes_button_pressed').value='ViewButton';getPageContents(this.form,null,'main_div','overlay');"></td>
                </tr>
            </c:forEach>
        </table>
    </c:otherwise>
</c:choose>
<%-- 
    Document   : MemberAppDocumentFilter
    Created on : Jul 3, 2013, 12:24:41 PM
    Author     : sphephelot
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
      <head>
<label class="header">Member Document</label>
<hr>
<agiletags:ControllerForm name="documentSorting"> 
    <c:if test="${applicationScope.Client == 'Sechaba'}">
        <input type="hidden" name="opperation" id="opperation" value="MemberOutStandingViewCommand" />
        <input type="hidden" name="fileLocation" id="fileLocation" value="" />
        <input type="hidden" name="opperationParam" id="opperationParam" value="" />
        <input type="hidden" name="onScreen" id="onScreen" value="" />
        <input type="hidden" name="MemberApp_cover_number" id="MemberApp_cover_number" value="${memberAppCoverNumber}" />
        <input type="hidden" name="memberAppStatus" id="memberAppStatus" value="${requestScope.memberAppStatus}" />
        <input type="hidden" name="memberAppNumber" id="memberAppNumber" value="${memberAppNumber}" /> 
        <input type="hidden" name="memberAppType" id="memberAppType" value="${requestScope.memberAppType}" />
        <input type="hidden" name="groupEntityId" id="groupEntityId" value="${groupEntityId}" />
        <input type="hidden" name="buttonPressed" id="DocUpdate_button_pressed" value="" />
        <input type="hidden" name="target_div" value="${target_div}" />
        <input type="hidden" name="main_div" value="${main_div}" />
    </c:if>
    <c:if test="${applicationScope.Client == 'Sechaba'|| applicationScope.Client == 'Agility'}">
        <c:if test="${requestScope.docType == 1}">
    <input type="hidden" name="opperation" id="opperation" value="MemberDocumentDisplayViewCommand" />
    <input type="hidden" name="fileLocation" id="fileLocation" value="" />
    <input type="hidden" name="opperationParam" id="opperationParam" value="" />
        </c:if>
        </c:if>
    <script>
        function viewMemberDocumentDetails(action) {
            document.getElementById("opperation").value = action;
//            if (document.getElementById("MemberDocumentDetails").value === '2') {
//                getPageContents(document.forms[0], null, 'main_div', 'overlay');
//            } else {
//                submitFormWithAjaxPost(document.forms[0]);
//            }
            document.forms[0].submit();
        }
        $(document).ready(function () {
            resizeContent();
            //attach on resize event
            $(window).resize(function () {
                resizeContent();
            });
            var currentVal = document.getElementById('MemberDocumentDetails').value;

            if (currentVal === '2') {
                document.getElementById('MemberDocumentDetails').disabled = true;
            }
            ;
        });
    </script>
    <c:if test= "${applicationScope.Client == 'Agility'}">
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Documents"  elementName="docType" lookupId="256" errorValueFromSession="no" javaScript=""/></tr>  
    </c:if>
    <c:if test= "${applicationScope.Client == 'Sechaba'}">
        <td colspan="3" class="label"><span style="border-style:none; border-collapse:collapse; border-width:1px;font-family: 'Arial', Halvetica, sans-serif;font-size: 12px;"> Documents: </span>
            <select id="MemberDocumentDetails" name="MemberDocumentDetails" onchange="document.getElementById('viewMemberDocs').style.visibility = 'visible';" >
                <option value="0" ${requestScope.docType == 0   ? "selected" : ""}> </option>
                <c:if test="${applicationScope.Client == 'Sechaba'}">
                    <c:if test="${persist_user_viewMemApps == false}">
                        <option value="1" ${requestScope.docType == 1  ? "selected" : ""}>Terms Of Acceptance Letter</option>
                    </c:if>
                </c:if>
                <c:if test= "${applicationScope.Client == 'Sechaba'}">
                    <c:if test="${persist_user_viewMemApps == false}">
                        <option value="2" ${requestScope.docType == 2   ? "selected" : ""}>Waiting Documentation</option>
                    </c:if>
                </c:if>
                <c:if test= "${applicationScope.Client == 'Sechaba'}">
                    <c:if test="${persist_user_viewMemApps == false}">
                        <option value="2" ${requestScope.docType == 3   ? "selected" : ""}>Underwriting Letter</option>
                    </c:if>
                </c:if>
            </select>
            <c:choose>
                <c:when test="${requestScope.docType == 1}">    
                    <c:if test= "${applicationScope.Client == 'Sechaba'}">
                        <c:if test="${persist_user_viewMemApps == false}">
                            <button style="visibility: hidden;" id="viewMemberDocs" value="Member Documents" onclick="this.style.visibility = 'hidden';viewMemberDocumentDetails('MemberDocumentDisplayViewCommand');">Member Documents</button></td>
                        </c:if>
                    </c:if>
                </c:when>
                <c:otherwise>
                <button style="visibility: hidden;" id="viewMemberDocs" value="Member Documents" onclick="this.style.visibility = 'hidden';viewMemberDocumentDetails('MemberOutStandingViewCommand');">Member Documents</button></td>
            </c:otherwise>
        </c:choose>
    </table>                              
</c:if>
<label></label>  
    <table>
        <td><label>Start Date:</label></td>
        <td><input name="minDate" id="minDate" size="30" value="${minDate}"></td>
        <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('minDate', this);"/></td>
        <td>&nbsp;</td>
        <td><label>End Date:</label></td>
        <td><input name="maxDate" id="maxDate" size="30" value="${maxDate}"></td>
        <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('maxDate', this);"/></td>
        <td>&nbsp;</td>
    <td><input type="button" name="SearchButton" value="Search" onclick="submitFormWithAjaxPost(this.form, '${target_div}', this, '${main_div}', '${target_div}')"/></td>
    </table>
    <div style ="display: none"  id="MemberDocumentDetails"></div>
</agiletags:ControllerForm>
</html>


<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<agiletags:ControllerForm name="MemberAppHospitalForm">
    <input type="hidden" name="opperation" id="opperation" value="MemberApplicationHospitalCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberApplicationHospital" />
    <input type="hidden" name="memberAppNumber" id="MemberAppHospital_application_number" value="${memberAppNumber}" />
    <input type="hidden" name="memberAppHospId" id="memberAppHospId" value="" />
    <input type="hidden" name="buttonPressed" id="MemberAppHospital_button_pressed" value="" />
    <input type="hidden" name="memberAppType" id="memberAppHospitalType" value="${requestScope.memberAppType}" />

    <label class="header">Surgery And Hospital Admissions</label>
    <hr>
    <c:if test="${persist_user_viewMemApps == false}">
        <br>
        <input type="button" name="AddHospitalButton" value="Add Details" onclick="document.getElementById('MemberAppHospital_button_pressed').value = this.value; getPageContents(this.form,null,'main_div','overlay');"/>
    </c:if>
    <br>    
    <table width="90%" id="MemberAppHospitalTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th>Dependant</th>
            <th>Details</th>
            <th>Date</th>
            <th>Reason</th>
            <th>Doctor</th>
            <th>Current Condition</th>
            <th></th>
            <th></th>
        </tr>

        <c:forEach var="entry" items="${MemberAppHospDetails}">
            <tr>
                <td><label>${DepMap[entry.dependentNumber]}</label></td>
                <td><label>${entry.details}</label></td>
                <td><label>${entry.hospDateAsString}</label></td>
                <td><label>${entry.reason}</label></td>
                <td><label>${entry.doctor}</label></td>
                <td><label>${entry.conditionValue}</label></td>
                <td><input type="button" value="Edit" name="EditButton" onclick="document.getElementById('memberAppHospId').value=${entry.hospDetailId};document.getElementById('MemberAppHospital_button_pressed').value='EditButton';getPageContents(this.form,null,'main_div','overlay');"></td>
                <td><input type="button" value="Remove" name="RemoveButton" onclick="document.getElementById('memberAppHospId').value=${entry.hospDetailId};document.getElementById('MemberAppHospital_button_pressed').value='RemoveButton';submitFormWithAjaxPost(this.form, 'HospitalAdmission');"></td>
            </tr>
        </c:forEach>
    </table>
</agiletags:ControllerForm>
</body>
</html>
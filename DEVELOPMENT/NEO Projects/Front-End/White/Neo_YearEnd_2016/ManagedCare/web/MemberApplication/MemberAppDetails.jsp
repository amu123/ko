<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>


<agiletags:ControllerForm name="MemberApplicationForm">
    <input type="hidden" name="opperation" id="MemberApp_opperation" value="SaveMemberApplicationCommand" />
    <input type="hidden" name="onScreen" id="MemberApp_onScreen" value="Member Application" />
    <input type="hidden" name="MemberApp_app_number" id="MemberApp_app_number" value="${memberAppNumber}" />
    <input type="hidden" name="MemberApp_cover_number" id="MemberApp_cover_number" value="${memberAppCoverNumber}" />
    <input type="hidden" name="MemberApp_application_type" id="MemberApp_application_type" value="${MemberApp_application_type}" />
    <input type="hidden" name="buttonPressed" id="memberValuesButtonPressed" value="" />
    <input type="hidden" name="memberBrokerId" id="memberBrokerIdDetails" value="${memberBrokerId}" />
    <input type="hidden" name="memberBrokerConsultantId" id="memberBrokerConsultantIdDetails" value="${memberBrokerConsultantId}" />
    <input type="hidden" name="groupEntityId" id="groupEntityId" value="${groupEntityId}" />
    <input type="hidden" name="saveBroker" id="saveBroker" value="" />

    <c:if test="${MemberApp_application_type == 1}">
        <label class="subheader">Registration of Additional Dependants</label>
        <br>
        <br>
    </c:if>
    <c:if test="${MemberApp_application_type != 1}">
        <br>
        <label class="subheader">Scheme and Option Selection</label>
        <hr>
        <table id="PlanTable">
            <tr id="MemberApp_productIdRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Scheme"  elementName="MemberApp_productId" lookupId="product" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="no" javaScript="onchange=\"addProductOption(this.value, 'MemberApp_optionId', 'true');\""/></tr>
            <tr id="MemberApp_optionIdRow"><agiletags:LabelProductOptionDropDownRequest productElement="MemberApp_productId" displayName="Option" elementName="MemberApp_optionId" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="no" javaScript="onchange=\"changeMedicalPractice(this.value);\""/></tr>
            <tr id="MemberApp_network_IPARow" style="display: none"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Network IPA" lookupId="351" elementName="MemberApp_network_IPA" mandatory="no" errorValueFromSession="yes" firstIndexSelected="no"/></tr>
            <tr id="MemberApp_coverStartDateRow"><agiletags:LabelTextBoxDateReq  displayname="Inception Date" elementName="MemberApp_CoverStartDate" valueFromSession="yes" mandatory="yes"/>        </tr>
            <tr id="MemberApp_coverReceivedDateRow"><agiletags:LabelTextBoxDateReq  displayname="Received Date" elementName="MemberApp_CoverReceivedDate" valueFromSession="yes" mandatory="no"/>        </tr>
            <tr id="MemberApp_coverSignedDateRow"><agiletags:LabelTextBoxDateReq  displayname="Signed Date" elementName="MemberApp_CoverSignedDate" valueFromSession="yes" mandatory="no"/>        </tr>
        </table>
    </c:if>
    <label class="subheader">Details of Applicant</label>
    <hr>
    <c:if test="${MemberApp_application_type == 1}">
        <table id="ApplicantDetailsTable">
            <tr id="MemberApp_lastNameRow"><agiletags:LabelTextBoxErrorReq displayName="Surname" elementName="MemberApp_lastName" valueFromRequest="MemberApp_lastName" mandatory="no"/>        </tr>
            <tr id="MemberApp_firstNameRow"><agiletags:LabelTextBoxErrorReq displayName="First Name" elementName="MemberApp_firstName" valueFromRequest="MemberApp_firstName" mandatory="no"/>        </tr>        
            <tr id="MemberApp_dateOfBirthRow"><agiletags:LabelTextBoxDateReq  displayname="Date Of Birth" elementName="MemberApp_DateOfBirth" valueFromSession="yes" mandatory="no"/>        </tr>
            <tr id="MemberApp_coverStartDateRow"><agiletags:LabelTextBoxDateReq  displayname="Inception Date" elementName="MemberApp_CoverStartDate" valueFromSession="yes" mandatory="yes"/>        </tr>
    <!--        <tr id="MemberApp_employerEntityIdRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Group Name"  elementName="MemberApp_employerEntityId" lookupId="employer" mandatory="no" errorValueFromSession="yes"/>        </tr> -->
            <tr id="MemberApp_telWorkRow"><agiletags:LabelTextBoxErrorReq displayName="Telephone Work" elementName="MemberApp_telWork" valueFromRequest="MemberApp_telWork" mandatory="no"/>        </tr>
            <tr id="MemberApp_telHomeRow"><agiletags:LabelTextBoxErrorReq displayName="Telephone Home" elementName="MemberApp_telHome" valueFromRequest="MemberApp_telHome" mandatory="no"/>        </tr>
            <tr id="MemberApp_telMobile1Row"><agiletags:LabelTextBoxErrorReq displayName="Cell" elementName="MemberApp_telMobile1" valueFromRequest="MemberApp_telMobile1" mandatory="no"/>        </tr>
            <tr id="MemberApp_correspondanceLanguageRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Language"  elementName="MemberApp_correspondanceLanguage" lookupId="31" mandatory="no" errorValueFromSession="yes"/>        </tr>
            <input type="hidden" name="MemberApp_productId" id="MemberApp_productId" value="${MemberApp_productId}" />
            <input type="hidden" name="MemberApp_optionId" id="MemberApp_optionId" value="${MemberApp_optionId}" />
        </table>
    </c:if>
    <c:if test="${MemberApp_application_type != 1}">
        <table id="ApplicantDetailsTable">
            <tr><td></td></tr>
            <tr id="MemberApp_titleRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Title"  elementName="MemberApp_title" lookupId="24" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <tr id="MemberApp_initialsRow"><agiletags:LabelTextBoxErrorReq displayName="Initials" elementName="MemberApp_initials" valueFromRequest="MemberApp_initials" mandatory="yes"/>        </tr>
            <tr id="MemberApp_firstNameRow"><agiletags:LabelTextBoxErrorReq displayName="First Name" elementName="MemberApp_firstName" valueFromRequest="MemberApp_firstName" mandatory="yes"/>        </tr>        
            <tr id="MemberApp_secondNameRow"><agiletags:LabelTextBoxErrorReq displayName="Preferred Name" elementName="MemberApp_preferredName" valueFromRequest="MemberApp_preferredName" mandatory="no"/>        </tr>
            <tr id="MemberApp_lastNameRow"><agiletags:LabelTextBoxErrorReq displayName="Surname" elementName="MemberApp_lastName" valueFromRequest="MemberApp_lastName" mandatory="yes"/>        </tr>
            <tr id="MemberApp_genderRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Gender"  elementName="MemberApp_gender" lookupId="23" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <tr id="MemberApp_dateOfBirthRow"><agiletags:LabelTextBoxDateReq  displayname="Date Of Birth" elementName="MemberApp_DateOfBirth" valueFromSession="yes" mandatory="yes"/>        </tr>
            <tr id="MemberApp_identificationTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Identification Type"  elementName="MemberApp_identificationType" lookupId="16" mandatory="yes" errorValueFromSession="yes"  />        </tr>
            <tr id="MemberApp_identificationNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Identification Number" elementName="MemberApp_identificationNumber" valueFromRequest="MemberApp_identificationNumber" mandatory="yes"/>        </tr>
            <tr id="MemberApp_taxNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Income Tax Number" elementName="MemberApp_taxNumber" valueFromRequest="MemberApp_taxNumber" mandatory="no"/>        </tr>
            <tr id="MemberApp_brokerCodeRow"><agiletags:LabelTextSearchWithNoText displayName="Group Code" elementName="MemberApp_groupCode" mandatory="yes" onClick="document.getElementById('memberValuesButtonPressed').value = 'SearchEmployerButton'; getPageContents(document.forms['MemberApplicationForm'],null,'main_div','overlay');"/>        </tr>
            <tr id="MemberApp_employeeNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Group Name" elementName="MemberApp_groupName" valueFromRequest="MemberApp_groupName" mandatory="no"/>        </tr>        
            <tr id="MemberApp_employeeNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Employee Number" elementName="MemberApp_employeeNumber" valueFromRequest="MemberApp_employeeNumber" mandatory="no"/>        </tr>        
            <tr id="MemberApp_identificationTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Member Category"  elementName="MemberApp_memberCategoty" lookupId="223" mandatory="yes" errorValueFromSession="yes"  />        </tr>
            <tr id="MemberApp_occupationRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Occupation"  elementName="MemberApp_occupation" lookupId="180" mandatory="no" errorValueFromSession="no"/>        </tr>
            <tr id="MemberApp_dateOfEmploymentRow"><agiletags:LabelTextBoxDateReq  displayname="Date Of Employment" elementName="MemberApp_DateOfEmployment" valueFromSession="yes" mandatory="no"/>        </tr>
            <tr id="MemberApp_correspondanceLanguageRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Language"  elementName="MemberApp_correspondanceLanguage" lookupId="31" mandatory="no" errorValueFromSession="yes"/>        </tr>
            <tr id="MemberAppp_heightRow"><agiletags:LabelTextBoxErrorReq displayName="Height (cm)" elementName="MemberApp_height" valueFromRequest="MemberApp_height" mandatory="no"/>        </tr>
            <tr id="MemberApp_weightRow"><agiletags:LabelTextBoxErrorReq displayName="Weight (kg)" elementName="MemberApp_weight" valueFromRequest="MemberApp_weight" mandatory="no"/>        </tr>
        </table>

        <br>
        <label class="subheader">Contact Details</label>
        <hr>
        <table id="ContactDetailsTable">
            <tr id="MemberApp_telWorkRow"><agiletags:LabelTextBoxErrorReq displayName="Telephone Work" elementName="MemberApp_telWork" valueFromRequest="MemberApp_telWork" mandatory="no"/>        </tr>
            <tr id="MemberApp_telHomeRow"><agiletags:LabelTextBoxErrorReq displayName="Telephone Home" elementName="MemberApp_telHome" valueFromRequest="MemberApp_telHome" mandatory="no"/>        </tr>
            <tr id="MemberApp_telMobile1Row"><agiletags:LabelTextBoxErrorReq displayName="Cell" elementName="MemberApp_telMobile1" valueFromRequest="MemberApp_telMobile1" mandatory="no"/>        </tr>
            <tr id="MemberApp_telMobile2Row"><agiletags:LabelTextBoxErrorReq displayName="Alternate Cell" elementName="MemberApp_telMobile2" valueFromRequest="MemberApp_telMobile2" mandatory="no"/>        </tr>
            <tr id="MemberApp_faxNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Fax Number" elementName="MemberApp_faxNumber" valueFromRequest="MemberApp_faxNumber" mandatory="no"/>        </tr>
            <tr id="MemberApp_emailRow"><agiletags:LabelTextBoxErrorReq displayName="Email Address" elementName="MemberApp_email" valueFromRequest="MemberApp_email" mandatory="no"/>        </tr>
            <tr id="MemberApp_smsRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="SMS Correspondence"  elementName="MemberApp_sms" lookupId="67" mandatory="no" errorValueFromSession="yes" javaScript="onchange=\"validateCellNo(this.value, 'MemberApp_telMobile1')\""/>        </tr>
            <tr id="MemberApp_emailStatementIndRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Email Statement"  elementName="MemberApp_emailStatementInd" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
            <tr id="MemberApp_emailNewsletterIndRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Email Other Correspondence"  elementName="MemberApp_emailNewsletterInd" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        </table>

        <br>
        <label class="subheader">Postal Address</label>
        <hr>
        <table id="PostalAddressTable">
            <tr id="MemberApp_postalAddressLine1Row"><agiletags:LabelTextBoxErrorReq displayName="Line1" elementName="MemberApp_postalAddressLine1" valueFromRequest="MemberApp_postalAddressLine1" mandatory="no"/>        </tr>
            <tr id="MemberApp_postalAddressLine2Row"><agiletags:LabelTextBoxErrorReq displayName="Line2" elementName="MemberApp_postalAddressLine2" valueFromRequest="MemberApp_postalAddressLine2" mandatory="no"/>        </tr>
            <tr id="MemberApp_postalAddressLine3Row"><agiletags:LabelTextBoxErrorReq displayName="Line3" elementName="MemberApp_postalAddressLine3" valueFromRequest="MemberApp_postalAddressLine3" mandatory="no"/>        </tr>
            <tr id="MemberApp_postalCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Postal Code" elementName="MemberApp_postalCode" valueFromRequest="MemberApp_postalCode" mandatory="no"/>        </tr>
        </table>

        <br>
        <label class="subheader">Physical Address</label>
        <hr>
        <table id="PhysicalAddressTable">
            <tr id="MemberApp_physicalAddressLine1Row"><agiletags:LabelTextBoxErrorReq displayName="Line1" elementName="MemberApp_physicalAddressLine1" valueFromRequest="MemberApp_physicalAddressLine1" mandatory="no"/>        </tr>
            <tr id="MemberApp_physicalAddressLine2Row"><agiletags:LabelTextBoxErrorReq displayName="Line2" elementName="MemberApp_physicalAddressLine2" valueFromRequest="MemberApp_physicalAddressLine2" mandatory="no"/>        </tr>
            <tr id="MemberApp_physicalAddressLine3Row"><agiletags:LabelTextBoxErrorReq displayName="Line3" elementName="MemberApp_physicalAddressLine3" valueFromRequest="MemberApp_physicalAddressLine3" mandatory="no"/>        </tr>
            <tr id="MemberApp_physicalCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Physical Code" elementName="MemberApp_physicalCode" valueFromRequest="MemberApp_physicalCode" mandatory="no"/>        </tr>
        </table>

        <br>
        <label class="subheader">Details of Previous Membership:Applicant</label>
        <hr>
        <table id="PreviousSchemeTable">
            <tr id="MemberApp_previousScheme1Row"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Name of Scheme"  elementName="MemberApp_previousSchema1" lookupId="193" mandatory="no" errorValueFromSession="yes"/>        </tr>  
            <tr id="MemberApp_previousOther1Row"><agiletags:LabelTextBoxErrorReq displayName="Other" elementName="MemberApp_previousOther1" valueFromRequest="MemberApp_previousOther1" mandatory="no"/>        </tr>
            <tr id="MemberApp_previousScheme1StartRow"><agiletags:LabelTextBoxDateReq  displayname="From" elementName="MemberApp_PreviousScheme1Start" valueFromSession="yes" mandatory="no"/>        </tr>
            <tr id="MemberApp_previousScheme1EndRow"><agiletags:LabelTextBoxDateReq  displayname="To" elementName="MemberApp_PreviousScheme1End" valueFromSession="yes" mandatory="no"/>        </tr>
            <tr><td colspan="5"><hr></td></tr>
            <tr id="MemberApp_previousScheme2Row"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Name of Scheme"  elementName="MemberApp_previousSchema2" lookupId="193" mandatory="no" errorValueFromSession="yes"/>        </tr>  
            <tr id="MemberApp_previousOther2Row"><agiletags:LabelTextBoxErrorReq displayName="Other" elementName="MemberApp_previousOther2" valueFromRequest="MemberApp_previousOther2" mandatory="no"/>        </tr>
            <tr id="MemberApp_previousScheme2StartRow"><agiletags:LabelTextBoxDateReq  displayname="From" elementName="MemberApp_PreviousScheme2Start" valueFromSession="yes" mandatory="no"/>        </tr>
            <tr id="MemberApp_previousScheme2EndRow"><agiletags:LabelTextBoxDateReq  displayname="to" elementName="MemberApp_PreviousScheme2End" valueFromSession="yes" mandatory="no"/>        </tr>
            <tr><td colspan="5"><hr></td></tr>
            <tr id="MemberApp_previousSchemeProofIndRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Proof Attached"  elementName="MemberApp_previousSchemeProofInd" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>        

        </table>
    </c:if>

    <br>
    <label class="subheader">Details of Previous Membership:Dependant</label>
    <hr>
    <table id="PreviousSchemeDepTable">
        <tr id="MemberApp_previousDepScheme1Row"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Name of Scheme"  elementName="MemberApp_previousDepSchema1" lookupId="193" mandatory="no" errorValueFromSession="yes"/>        </tr>  
        <tr id="MemberApp_previousDepOther1Row"><agiletags:LabelTextBoxErrorReq displayName="Other" elementName="MemberApp_previousDepOther1" valueFromRequest="MemberApp_previousDepOther1" mandatory="no"/>        </tr>
        <tr id="MemberApp_previousDepScheme1StartRow"><agiletags:LabelTextBoxDateReq  displayname="From" elementName="MemberApp_PreviousDepScheme1Start" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="MemberApp_previousDepScheme1EndRow"><agiletags:LabelTextBoxDateReq  displayname="To" elementName="MemberApp_PreviousDepScheme1End" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr><td colspan="5"><hr></td></tr>
        <tr id="MemberApp_previousDepScheme2Row"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Name of Scheme"  elementName="MemberApp_previousDepSchema2" lookupId="193" mandatory="no" errorValueFromSession="yes"/>        </tr>  
        <tr id="MemberApp_previousDepOther2Row"><agiletags:LabelTextBoxErrorReq displayName="Other" elementName="MemberApp_previousDepOther2" valueFromRequest="MemberApp_previousDepOther2" mandatory="no"/>        </tr>
        <tr id="MemberApp_previousDepScheme2StartRow"><agiletags:LabelTextBoxDateReq  displayname="From" elementName="MemberApp_PreviousDepScheme2Start" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="MemberApp_previousDepScheme2EndRow"><agiletags:LabelTextBoxDateReq  displayname="to" elementName="MemberApp_PreviousDepScheme2End" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr><td colspan="5"><hr></td></tr>
        <tr id="MemberApp_previousDepSchemeProofIndRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Proof Attached"  elementName="MemberApp_previousDepSchemeProofInd" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>

    </table>

    <c:if test="${MemberApp_application_type != 1}">
        <br>
        <label class="subheader">Income Details</label>
        <hr>
        <table id="PlanTable">
            <tr id="MemberApp_membershipGuideIndRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Membership Guide"  elementName="MemberApp_membershipGuideInd" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
            <tr id="MemberApp_addressForGuideRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Address For Guide"  elementName="MemberApp_addressForGuideInd" lookupId="33" mandatory="no" errorValueFromSession="yes"/>        </tr>
            <tr id="MemberApp_foundLatestIncomeTaxReturnRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Latest Income Tax Return" elementName="MemberApp_foundLatestIncomeTaxReturn" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
            <tr id="MemberApp_signedAffidavitRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Signed Affidavit" elementName="MemberApp_signedAffidavit" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
            <tr id="MemberApp_commisionStatementRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Commision Statement" elementName="MemberApp_commisionStatement" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
            <tr id="MemberApp_salaryAdviceRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Salary Advice" elementName="MemberApp_salaryAdvice" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
            <tr id="MemberApp_auditorLetterRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Auditor Letter" elementName="MemberApp_auditorLetter" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
            <tr id="MemberApp_nettIncomeRow"><agiletags:LabelTextBoxErrorReq displayName="Income" elementName="MemberApp_nettIncome" valueFromRequest="MemberApp_nettIncome" mandatory="no"/></tr>
            <tr><td width="160px"></td><td width="200px" align=\"left\"><label class="error">* Value may affect contribution amount</label></td></tr>
        </table>
        <!--<table id="PlanOptionTable" style="display:none;"></table>-->

        <div id="currentMedicalPractitioner">
            <br>
            <label class="subheader">Current Medical Practitioner</label>
            <hr>
            <table id="DoctorTable">
                <tr id="MemberApp_principalNameRow"><agiletags:LabelTextBoxErrorReq displayName="Name"  elementName="MemberApp_principalName" mandatory="no"  valueFromRequest="MemberApp_principalName" />        </tr>
                <tr id="MemberApp_principalPracticeTelRow"><agiletags:LabelTextBoxErrorReq displayName="Telephone Number" elementName="MemberApp_principalPracticeTel" valueFromRequest="MemberApp_principalPracticeTel" mandatory="no"/>        </tr>        
                <tr id="MemberApp_principalPracticeFromDateRow"><agiletags:LabelTextBoxDateReq  displayname="Date of First Consult" elementName="MemberApp_principalFirstConsultDate" valueFromSession="yes" mandatory="no"/>        </tr>
            </table>
            <br>
            <hr>
            <table id="NominatedDoctorTable" style="display:${MemberApp_optionId == '5' ? 'table' : 'none'};">
                <tr><td colspan="5"><label class="subheader">Nominated Practitioner Details</label></td></tr>
                <tr id="MemberApp_principalNominatedPractitionerRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Nominated Practitioner" elementName="MemberApp_principalNominatedPractitioner" lookupId="67" mandatory="no" errorValueFromSession="yes"/>    </tr>
                <tr id="MemberApp_principalPracticeNameRow"><agiletags:LabelTextBoxErrorReq displayName="Practice Name" elementName="MemberApp_principalPracticeName" valueFromRequest="MemberApp_principalPracticeName" mandatory="no"/>        </tr>
                <tr id="MemberApp_principalPracticeTelCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Telephone Number" elementName="MemberApp_principalNonPracticeTel" valueFromRequest="MemberApp_principalNonPracticeTel" mandatory="no"/>        </tr>        
                <tr id="MemberApp_principalPracticeNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Practice Number" elementName="MemberApp_principalPracticeNumber" valueFromRequest="MemberApp_principalPracticeNumber" mandatory="no"/>        </tr>
                <tr id="MemberApp_principalPracticeIdRowRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Region"  elementName="MemberApp_principalRegion" lookupId="198" mandatory="no" errorValueFromSession="yes"/>        </tr>
            </table>
        </div>
        <div id="gomomoMedicalPractitioner" style="display: none">
            <br>
            <label class="subheader">Nominated Practitioner Details</label>
            <hr>
            <label class="subheader">Doctor Details:</label>
            <br>
            <table id="DoctorTable">
                <tr id="MemberApp_doctorNameRow"><agiletags:LabelTextSearchWithNoText displayName="Doctor Name" elementName="MemberApp_doctorName" mandatory="no" onClick="document.getElementById('memberValuesButtonPressed').value = 'doctorSearch'; getPageContents(document.forms['MemberApplicationForm'],null,'main_div','overlay');"/></tr>
                <tr id="MemberApp_doctorPracticeNumberRow"><agiletags:LabelTextBoxErrorReq displayName="BHF Practice Number"  elementName="MemberApp_doctorPracticeNumber" mandatory="no"  valueFromRequest="MemberApp_doctorPracticeNumber"/></tr>
                <tr id="MemberApp_doctorNetworkRow"><agiletags:LabelTextBoxErrorReq displayName="Network"  elementName="MemberApp_doctorNetwork" mandatory="no" enabled="false" valueFromRequest="MemberApp_doctorNetwork"/></tr>
            </table>
            <hr>
            <label class="subheader">Dentist Details:</label>
            <br>
            <table id="DentistTable">
                <tr id="MemberApp_dentistNameRow"><agiletags:LabelTextSearchWithNoText displayName="Dentist Name" elementName="MemberApp_dentistName" mandatory="no" onClick="document.getElementById('memberValuesButtonPressed').value = 'dentistSearch'; getPageContents(document.forms['MemberApplicationForm'],null,'main_div','overlay');"/></tr>
                <tr id="MemberApp_dentistPracticeNumberRow"><agiletags:LabelTextBoxErrorReq displayName="BHF Practice Number"  elementName="MemberApp_dentistPracticeNumber" mandatory="no"  valueFromRequest="MemberApp_dentistPracticeNumber"/></tr>
                <tr id="MemberApp_dentistNetworkRow"><agiletags:LabelTextBoxErrorReq displayName="Network"  elementName="MemberApp_dentistNetwork" mandatory="no" enabled="false" valueFromRequest="MemberApp_dentistNetwork"/></tr>
            </table>
            <hr>
            <label class="subheader">Optometrist Details:</label>
            <br>
            <table id="OptometristTable">
                <tr id="MemberApp_optometristNameRow"><agiletags:LabelTextSearchWithNoText displayName="Optometrist Name" elementName="MemberApp_optometristName" mandatory="no" onClick="document.getElementById('memberValuesButtonPressed').value = 'optometristSearch'; getPageContents(document.forms['MemberApplicationForm'],null,'main_div','overlay');"/></tr>
                <tr id="MemberApp_optometristPracticeNumberRow"><agiletags:LabelTextBoxErrorReq displayName="BHF Practice Number"  elementName="MemberApp_optometristPracticeNumber" mandatory="no"  valueFromRequest="MemberApp_optometristPracticeNumber"/></tr>
                <tr id="MemberApp_optometristNetworkRow"><agiletags:LabelTextBoxErrorReq displayName="Network"  elementName="MemberApp_optometristNetwork" mandatory="no" enabled="false" valueFromRequest="MemberApp_optometristNetwork"/></tr>
            </table>
        </div>
        <br>
        
        <label class="subheader">Contribution Collection Details</label>
        <hr>
        <table id="ContribTable">
            <tr id="MemberApp_contirbPaymentMethodRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Payment Method"  elementName="MemberApp_paymentMethod" lookupId="188" mandatory="no" errorValueFromSession="yes"/>   </tr>
            <tr id="MemberApp_contribBankNameRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Name of Bank"  elementName="MemberApp_contribBankName" lookupId="bank" mandatory="no" errorValueFromSession="yes" javaScript="onChange=\"toggleBranch(this.value, 'MemberApp_contribBankBranchId')\""/>       </tr>
            <tr id="MemberApp_contribBankBranchIdRow"><agiletags:LabelNeoBranchDropDownErrorReq displayName="Branch"  elementName="MemberApp_contribBankBranchId"  mandatory="no" errorValueFromSession="yes" parentElement="MemberApp_contribBankName"/>   </tr>
            <tr id="MemberApp_contribAccountTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Account Type"  elementName="MemberApp_contribAccountType" lookupId="37" mandatory="no" errorValueFromSession="yes"/>     </tr>
            <tr id="MemberApp_contribAccountHolderRow"><agiletags:LabelTextBoxErrorReq displayName="Account Holder" elementName="MemberApp_contribAccountHolder" valueFromRequest="MemberApp_contribAccountHolder" mandatory="no"/>        </tr>
            <tr id="MemberApp_contribAccountNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Account Number" elementName="MemberApp_contribAccountNumber" valueFromRequest="MemberApp_contribAccountNumber" mandatory="no"/>        </tr>
            <tr id="MemberApp_conrtibdoDateRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Debit Date" elementName="MemberApp_doDate" lookupId="197" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <tr id="MemberApp_contribCertifiedDateRow"><agiletags:LabelTextBoxDateReq displayname="Certified Date"  elementName="MemberApp_contribCertifiedDate"  mandatory="no"  valueFromSession="no"/>        </tr>
        </table>

        <br>
        <label class="subheader">Reimbursement Details</label>
        <hr>
        <table id="ClaimSameTable">
            <tr id="MemberApp_contibSameBankig"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Same Banking Details"  elementName="MemberApp_contribSameBanking" mandatory="no"  errorValueFromSession="yes" lookupId="67"   javaScript="onchange=\"if(this.value === '1'){mimicBankDetails();}; setElemStyle('ClaimTable',this.value == '1'? 'none' : 'table');\""/>        </tr>
        </table>
        <table id="ClaimTable">
            <tr id="MemberApp_paymentsBankNameRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Name of Bank"  elementName="MemberApp_paymentsBankName" lookupId="bank" mandatory="yes" errorValueFromSession="yes" javaScript="onChange=\"toggleBranch(this.value, 'MemberApp_paymentsBankBranchId')\""/>        </tr>        
            <tr id="MemberApp_paymentsBankBranchIdRow"><agiletags:LabelNeoBranchDropDownErrorReq displayName="Branch"  elementName="MemberApp_paymentsBankBranchId"  mandatory="no" errorValueFromSession="yes" parentElement="MemberApp_paymentsBankName"/>        </tr>
            <tr id="MemberApp_paymentsAccountTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Account Type"  elementName="MemberApp_paymentsAccountType" lookupId="37" mandatory="no" errorValueFromSession="yes"/>        </tr>
            <tr id="MemberApp_paymentsAccountHolderRow"><agiletags:LabelTextBoxErrorReq displayName="Account Holder" elementName="MemberApp_paymentsAccountHolder" valueFromRequest="MemberApp_paymentsAccountHolder" mandatory="no"/>        </tr>
            <tr id="MemberApp_paymentsAccountNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Account Number" elementName="MemberApp_paymentsAccountNumber" valueFromRequest="MemberApp_paymentsAccountNumber" mandatory="no"/>        </tr>
        </table>

        <br>
        <label class="subheader">Intermediary Details</label>
        <hr>
        <table>
            <tr>
                <td colspan="3" class="label" ><span style="font-family: Arial, Helvetica, sans-serif;font-size: 1.0em;">Broker or Broker Consultant: </span>
                <td width="160px"><select id="memAppBroker" name="memAppBroker" onchange="swapBrokerAndConsultant(this.value)" width="160px">
                        <option value="1" ${empty requestScope.brokerConsultant || requestScope.brokerConsultant == 1 ? "selected" : ""}>Broker</option>
                        <option value="2" ${requestScope.brokerConsultant == 2 ? "selected" : ""}>Broker Consultant</option>
                    </select></td>                    
            </tr>
        </table>

        <c:if test="${empty requestScope.brokerConsultant || requestScope.brokerConsultant == 1}">
            <div id="brokerDetails">
                <table id="BrokerTable">
                    <tr id="MemberApp_brokerCodeRow"><agiletags:LabelTextSearchWithNoText displayName="Code" elementName="MemberApp_brokerCode" mandatory="yes" onClick="document.getElementById('memberValuesButtonPressed').value = 'SearchBrokerButton'; getPageContents(document.forms['MemberApplicationForm'],null,'main_div','overlay');"/>        </tr>
                    <tr id="MemberApp_brokerBrokerageNameRow"><agiletags:LabelTextBoxErrorReq displayName="Brokerage Name" elementName="MemberApp_brokerBrokeragaName" valueFromRequest="MemberApp_brokerBrokeragaName" mandatory="no"/>        </tr>
                </table>
            </div>
            <div id="brokerConsultantDetails" style="display: none" >
                <table id="BrokerTable">
                    <tr id="MemberApp_brokerConsultantCodeRow"><agiletags:LabelTextSearchWithNoText displayName="Code" elementName="MemberApp_brokerConsultantCode" mandatory="yes" onClick="document.getElementById('memberValuesButtonPressed').value = 'SearchConsultantButton'; getPageContents(document.forms['MemberApplicationForm'],null,'main_div','overlay');"/>        </tr>
                </table>
            </div>
        </c:if>
        <c:if test="${requestScope.brokerConsultant == 2}">
            <div id="brokerDetails" style="display: none">
                <table id="BrokerTable">
                    <tr id="MemberApp_brokerCodeRow"><agiletags:LabelTextSearchWithNoText displayName="Code" elementName="MemberApp_brokerCode" mandatory="yes" onClick="document.getElementById('memberValuesButtonPressed').value = 'SearchBrokerButton'; getPageContents(document.forms['MemberApplicationForm'],null,'main_div','overlay');"/>        </tr>
                    <tr id="MemberApp_brokerBrokerageNameRow"><agiletags:LabelTextBoxErrorReq displayName="Brokerage Name" elementName="MemberApp_brokerBrokeragaName" valueFromRequest="MemberApp_brokerBrokeragaName" mandatory="no"/>        </tr>
                </table>
            </div>
            <div id="brokerConsultantDetails">
                <table id="BrokerTable">
                    <tr id="MemberApp_brokerConsultantCodeRow"><agiletags:LabelTextSearchWithNoText displayName="Code" elementName="MemberApp_brokerConsultantCode" mandatory="yes" onClick="document.getElementById('memberValuesButtonPressed').value = 'SearchConsultantButton'; getPageContents(document.forms['MemberApplicationForm'],null,'main_div','overlay');"/>        </tr>
                </table>
            </div>
        </c:if>
        <table>
            <tr id="MemberApp_brokerNameRow"><agiletags:LabelTextBoxErrorReq displayName="Name" elementName="MemberApp_brokerName" valueFromRequest="MemberApp_brokerName" mandatory="no"/>        </tr>     
            <tr id="MemberApp_VIPbrokerRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="VIP Broker"  elementName="MemberApp_brokerVIP" lookupId="67" mandatory="no" errorValueFromSession="yes" javaScript="disabled"/>        </tr>    
            <tr id="MemberApp_brokerContactNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Contact Number" elementName="MemberApp_brokerContactNumber" valueFromRequest="MemberApp_brokerContactNumber" mandatory="no"/>        </tr>
            <tr id="MemberApp_brokerEmailRow"><agiletags:LabelTextBoxErrorReq displayName="Email" elementName="MemberApp_brokerEmail" valueFromRequest="MemberApp_brokerEmail" mandatory="no"/>        </tr>        
            <tr id="MemberApp_brokerFaxNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Fax Number" elementName="MemberApp_brokerFaxNumber" valueFromRequest="MemberApp_brokerFaxNumber" mandatory="no"/>        </tr>
            <tr id="MemberApp_brokerCertifiedDateRow"><agiletags:LabelTextBoxDateReq  displayname="Signed Date" elementName="MemberApp_brokerCertifiedDate" valueFromSession="yes" mandatory="yes"/>        </tr>
        </table>
    </c:if>
    <c:if test="${persist_user_viewMemApps == false}">
        <hr>
        <table id="MemberAppSaveTable">
            <tr>
                <td><input type="reset" value="Reset"></td>
                <td><input type="button" value="Save" onclick="document.getElementById('memberValuesButtonPressed').value = ''; submitFormWithAjaxPost(this.form, 'MemberApplicationDetails', this); toggleMemberStatus()"></td>
            </tr>
        </table>
    </c:if>
</agiletags:ControllerForm>
<img src="${pageContext.request.contextPath}/resources/empty.gif" onload="document.getElementById('memberAppNumber').value = '${memberAppNumber}';
        document.getElementById('memberAppType').value = '${MemberApp_application_type}';
        document.getElementById('memberAppCoverNumber').value = '${memberAppCoverNumber}';
        document.getElementById('member_header_applicationNumber').innerHTML = '${memberAppCoverNumber}';
        this.parentNode.removeChild(this);" />

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

  <agiletags:ControllerForm name="MemberAppChronicDetailForm">
    <input type="hidden" name="opperation" id="opperation" value="MemberApplicationChronicCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberApplicationChronicDetails" />
    <input type="hidden" name="memberAppNumber" id="memberAppChronicNumber" value="${memberAppNumber}" />
    <input type="hidden" name="memberAppChronicId" id="memberAppChronicId" value="${memberAppChronicId}" />
    <input type="hidden" name="target_div" value="${target_div}" />
    <input type="hidden" name="main_div" value="${main_div}" />
    <input type="hidden" name="buttonPressed" id="MemberAppChronicDetails_button_pressed" value="" />

    <label class="header">Add Chronic Medication Detail</label>
    <hr>
    <br>
     <table>
                <tr><agiletags:LabelNeoMapListDropDownErrorReq displayName="Dependant"  elementName="Chronic_detail_dependent" mapList="DependentList" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="yes" />        </tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="Medication" elementName="Chronic_detail_medication" valueFromRequest="Chronic_detail_medication" mandatory="yes"/>        </tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="Medical Condition" elementName="Chronic_detail_condition" valueFromRequest="Chronic_detail_condition" mandatory="yes"/>        </tr>
                <tr><agiletags:LabelTextBoxDateReq  displayname="Date" elementName="Chronic_detail_date" valueFromSession="yes" mandatory="yes"/>        </tr>
      </table>
      
    <hr>
    <table id="MemberAppChronicDetailsSaveTable">
        <tr>
           <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
           <td><input type="button" value="Save" onclick="document.getElementById('MemberAppChronicDetails_button_pressed').value='SaveButton';submitFormWithAjaxPost(this.form, 'ChronicMedication', null,'${main_div}', '${target_div}');"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>

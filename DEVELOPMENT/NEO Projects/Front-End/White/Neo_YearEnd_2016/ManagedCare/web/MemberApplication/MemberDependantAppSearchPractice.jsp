<%-- 
    Document   : MemberDependantAppSearchPractice
    Created on : Jul 20, 2016, 9:59:41 AM
    Author     : charlh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<agiletags:ControllerForm name="MemberPracticeSearchForm">
    
    <label class="header">Practice Search</label>
    <input type="hidden" name="opperation" id="opperation" value="MemberDependantAppSearchPracticeCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberDependantApp" />
    
    <input type="hidden" id="prcNr" name="prcNr" value="${practiceNumber}"/>
    <input type="hidden" id="prName" name="prName" value="${practiceName}"/>
    <input type="hidden" id="prNetw" name="prNetw" value="${practiceNetwork}"/>
    <input type="hidden" id="DSPType" name="DSPType" value="${DSPType}"/>
    
    <table>
        <tr><agiletags:LabelTextBoxError displayName="Practice Number" elementName="pracNo"/></tr>
        <tr><agiletags:LabelTextBoxError displayName="Practice Name" elementName="pracName"/></tr>
        <br>
        <tr>
            <td><input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('overlay2', 'overlay');"></td>
            <td align="right"><input type="button" value="Search" onclick="submitFormWithAjaxPost(this.form, 'SearchDependantPracticeResults');"></td>
        </tr>
    </table>
    <div style="display: none" id="SearchDependantPracticeResults"></div>
</agiletags:ControllerForm>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<agiletags:ControllerForm name="MemberAppUnderwritingForm">
    <!--<input type="hidden" name="opperation" id="MemberAppUnderwriting_opperation" value="MemberApplicationUnderwritingCommand" />
    <input type="hidden" name="onScreen" id="MemberAppUnderwriting_onScreen" value="" />
    <input type="hidden" name="memberAppNumber" id="MemberAppUnderwriting_application_number" value="${memberAppNumber}" />
    <input type="hidden" name="memberAppDepNo" id="memberAppUnderwritingDepNo" value="" />
    <input type="hidden" name="buttonPressed" id="MemberAppUnderwriting_button_pressed" value="" />
    <input type="hidden" name="memberAppType" id="memberAppUnderwritingType" value="${requestScope.memberAppType}" />
    <input type="hidden" name="memberAppCondWaitId" id="memberAppCondWaitId" value="" />

    <label class="header">Member Application Underwriting</label>
    <hr>
    <br>
    <label class="subheader">Late Joiner Penalty</label>
    <br>
    <hr>
    <table style="margin-left:10px;" id="MemberAppUnderwritingLJPTable">
    <c:forEach var="entry" items="${DependentList}">
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Dependant ${entry.value}"  elementName="MemberAppUnderwriting_ljp_${entry.id}" lookupId="205" mandatory="no" errorValueFromSession="yes" firstIndexSelected="yes" />        </tr>
    </c:forEach>
</table>
<br>
<label class="subheader">Waiting Periods</label>
<br>
<hr>

<table style="margin-left:10px;" id="MemberAppUnderwritingTable">
    <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Apply Waiting Period"  elementName="MemberAppUnderwriting_waitingPeriod" lookupId="67" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="yes" javaScript="onchange=\"setElemStyle('MemberAppWaitingPeriodDiv',this.value == '1'? 'block' : 'none');\""/>        </tr>
</table>

<div id="MemberAppWaitingPeriodDiv" style="display:${MemberAppUnderwriting_waitingPeriod == '1' ? 'block' : 'none'};margin-left:30px;">
    <c:forEach var="entry" items="${DependentList}">
        <c:set var="genWaitVar" value="MemberAppUnderwriting_generalWaiting_${entry.id}"/>
        <c:set var="condWaitVar" value="MemberAppUnderwriting_conditionSpecific_${entry.id}"/>
        <br>
        <label class="subheader">Dependant: ${entry.value}</label>
        <hr>
        <table style="margin-left:10px;" id="MemberAppUnderWritingTable_${entry.id}">
            <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="General Waiting Period"  elementName="MemberAppUnderwriting_generalWaiting_${entry.id}" lookupId="67" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="yes" javaScript="onchange=\"setElemStyle('MemberAppUnderwriting_start_date_row_${entry.id}',this.value == '1'? 'table-row' : 'none');setElemStyle('MemberAppUnderwriting_end_date_row_${entry.id}',this.value == '1'? 'table-row' : 'none');\""/>        </tr>
            <tr id="MemberAppUnderwriting_start_date_row_${entry.id}" style="display:${requestScope[genWaitVar] == '1' ? 'table-row' : 'none'};"><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MemberAppUnderwriting_start_date_${entry.id}" valueFromSession="yes" mandatory="yes"/>        </tr>
            <tr id="MemberAppUnderwriting_end_date_row_${entry.id}" style="display:${requestScope[genWaitVar] == '1' ? 'table-row' : 'none'};"><agiletags:LabelTextBoxDateReq  displayname="End Date" elementName="MemberAppUnderwriting_end_date_${entry.id}" valueFromSession="yes" mandatory="yes"/>        </tr>
            <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Allow PMB"  elementName="MemberAppUnderwriting_allowPMB_${entry.id}" lookupId="67" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="yes" />        </tr>
            <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Condition Specific"  elementName="MemberAppUnderwriting_conditionSpecific_${entry.id}" lookupId="67" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="yes" javaScript="onchange=\"setElemStyle('MemberAppConditionDiv_${entry.id}',this.value == '1'? 'block' : 'none');\""/>        </tr>
        </table>
        <br>
        <div  style="margin-left:30px;display:${requestScope[condWaitVar] == 1 ? 'block' : 'none'};" id="MemberAppConditionDiv_${entry.id}">
            <label class="subheader">Condition Specific Waiting Periods</label>
            <br>
        <c:if test="${persist_user_viewMemApps == false}">
            <input type="button" value="Add Details" onclick="document.getElementById('memberAppUnderwritingDepNo').value = '${entry.id}';document.getElementById('MemberAppUnderwriting_button_pressed').value = this.value; getPageContents(this.form,null,'main_div','overlay');">
        </c:if>
        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%">
            <tr>
                <th>ICD10 Code</th>
                <th>Description</th>
                <th></th>
            </tr>
        <c:forEach var="condWait" items="${CondWaitList}">
            <c:if test="${condWait.dependentNumber == entry.id}">
                <tr>
                    <td><label>${condWait.diagCode}</label></td>
                    <td><label>${condWait.diagDescription}</label></td>
                    <td><input type="button" value="Remove" name="RemoveButton" onclick="document.getElementById('memberAppCondWaitId').value=${condWait.conditionWaitId};document.getElementById('MemberAppUnderwriting_button_pressed').value='RemoveButton';submitFormWithAjaxPost(this.form, 'Underwriting', this);"></td>
                </tr>
            </c:if>
        </c:forEach>
    </table>

</div>
    </c:forEach>

</div>
<br>
    <c:if test="${persist_user_viewMemApps == false}">
        <hr>
        <table id="MemberAppUnderwritingSaveTable">
            <tr>
                <td><input type="reset" value="Reset"></td>
                <td><input type="button" value="Save" onclick="document.getElementById('MemberAppUnderwriting_button_pressed').value = this.value;submitFormWithAjaxPost(this.form, 'MemberAppUnderwriting', this)"></td>
            </tr>
        </table>
    </c:if>-->



    <input type="hidden" name="opperation" id="MemberAppUnderwriting_opperation" value="MemberApplicationUnderwritingCommand" />
    <input type="hidden" name="onScreen" id="MemberAppUnderwriting_onScreen" value="" />
    <input type="hidden" name="memberAppNumber" id="MemberAppUnderwriting_application_number" value="${memberAppNumber}" />
    <input type="hidden" name="memberAppDepNo" id="memberAppUnderwritingDepNo" value="" />
    <input type="hidden" name="buttonPressed" id="MemberAppUnderwriting_button_pressed" value="" />
    <input type="hidden" name="memberAppType" id="memberAppUnderwritingType" value="${requestScope.memberAppType}" />
    <input type="hidden" name="memberAppCondWaitId" id="memberAppCondWaitId" value="" />

    <br/>
    <br/>
    <c:if test="${warningW == 'groupConcession'}" >
                    <span style="color: red">Warning:Member associated to an existing group concession</span>
                </c:if>
                    <br/>
                     <c:if test="${warningW == 'brokerConcession'}" >
                    <span style="color: red">Warning:Member associated to an existing broker concession</span>
                </c:if>
                    <br/>
                     <c:if test="${warningW == 'brokerGroupConcession'}" >
                    <span style="color: red">Warning:Member associated to an existing group/broker concession</span>
                </c:if>
                    
    <div id="late_penalty">
        <label class="subheader">Late Joiner Penalty</label>
        <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
            <tr>
                <th align="left">Dependant Code</th>
                <th align="left">Dependant Name</th>
                <th align="left">Dependant Surname</th>
                <th align="left">Birth Date</th>
                <th align="left">LJP</th>
                <th align="left"></th>
            </tr>
            <c:forEach var="entry" items="${DependentList}">
                <tr>
                    <td><label class="label">${entry.id}</label></td>
                    <td><label class="label">${entry.name}</label></td>
                    <td><label class="label">${entry.surname}</label></td>
                    <td><label class="label">${entry.birthDate}</label></td>
                    <agiletags:NeoLookupValueDropDown displayName="" elementName="MemberAppUnderwriting_ljp_${entry.id}" lookupId="205" javaScript="onChange=\"$('#button${entry.id}').removeAttr('disabled');\"" />
                    <td><input name="button${entry.id}" id="button${entry.id}" type="submit" value="Update" onclick="document.getElementById('memberAppUnderwritingDepNo').value='${entry.id}';document.getElementById('MemberAppUnderwriting_button_pressed').value = this.value;submitFormWithAjaxPost(this.form, 'MemberAppUnderwriting', this); " disabled /></td>
                </tr>
            </c:forEach>
        </table>
    </div> 
    <br/>
    <br/>
    <div id="waiting_period">
        <label class="subheader">General Waiting Periods</label>
        <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
            <tr>
                <th align="left">Dependant Code</th>
                <th align="left">Dependant Name</th>
                <th align="left">Dependant Surname</th>
                <th align="left">Birth Date</th>                    
                <th align="left">PMB</th>
                <th align="left">Apply General Waiting</th>
                <th align="left"></th>                    
            </tr>
            <c:forEach var="entry" items="${DependentList}">
                <tr>
                    <td><label class="label">${entry.id}</label></td>
                    <td><label class="label">${entry.name}</label></td>
                    <td><label class="label">${entry.surname}</label></td>
                    <td><label class="label">${entry.birthDate}</label></td>                        
                    <agiletags:NeoLookupValueDropDown displayName="" elementName="MemberAppUnderwriting_allowPMB_${entry.id}" lookupId="67" javaScript="onChange=\"$('#buttonUpDate${entry.id}').removeAttr('disabled');\""/>
                    <agiletags:NeoLookupValueDropDown displayName="" elementName="MemberAppUnderwriting_generalWaiting_${entry.id}" lookupId="67" javaScript="onChange=\"$('#buttonUpDate${entry.id}').removeAttr('disabled');\"" />
                    <!--<td><input type="button" value="Delete" onclick="document.getElementById('memberAppUnderwritingDepNo').value='${entry.id}';document.getElementById('MemberAppUnderwriting_button_pressed').value = this.value;submitFormWithAjaxPost(this.form, 'MemberAppUnderwriting', this)"></td>
                    <td><input type="button" value="Add" onclick="document.getElementById('memberAppUnderwritingDepNo').value='${entry.id}';document.getElementById('MemberAppUnderwriting_button_pressed').value = this.value;submitFormWithAjaxPost(this.form, 'MemberAppUnderwriting', this)"></td>-->
                    <td><input name="buttonUpDate${entry.id}" id="buttonUpDate${entry.id}" type="submit" value="Update" onclick="document.getElementById('memberAppUnderwritingDepNo').value='${entry.id}';document.getElementById('MemberAppUnderwriting_button_pressed').value = 'AddGenWait';submitFormWithAjaxPost(this.form, 'MemberAppUnderwriting', this)" disabled /></td>
                </tr>
            </c:forEach>
        </table>
    </div> 
    <br/>
    <br/>
    <div id="condition_specific">
        <c:forEach var="entry" items="${DependentList}">
            <label class="subheader">Condition Specific Waiting Periods for : ${entry.name} ${entry.surname}</label>
            <br>
            <c:if test="${persist_user_viewMemApps == false}">
                <input type="button" value="Add Details" onclick="document.getElementById('memberAppUnderwritingDepNo').value = '${entry.id}';document.getElementById('MemberAppUnderwriting_button_pressed').value = this.value; getPageContents(this.form,null,'main_div','overlay');">
            </c:if>
            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                <th align="left">Dependant Code</th>
                <th align="left">Dependant Name</th>
                <th align="left">Dependant Surname</th>
                <th align="left">Birth Date</th>
                <th align="left">Start Date</th>
                <th align="left">End Date</th>
                <th align="left">ICD10 Code</th>
                <th align="left">Description</th>
                <th align="left"></th>
                <th align="left"></th>                
                <c:forEach var="condWait" items="${CondWaitList}">
                    <c:if test="${condWait.dependentNumber == entry.id}">
                        <tr>
                            <td><label class="label">${entry.id}</label></td>
                            <td><label class="label">${entry.name}</label></td>
                            <td><label class="label">${entry.surname}</label></td>
                            <td><label class="label">${entry.birthDate}</label></td>
                            <td><label class="label">${agiletags:formatXMLGregorianDate(condWait.effectiveStartDate)}</label></td>
                            <td><label class="label">${agiletags:formatXMLGregorianDate(condWait.effectiveEndDate)}</label></td>
                            <td><label class="label">${condWait.diagCode}</label></td>
                            <td><label class="label">${condWait.diagDescription}</label></td>
                            <td><input type="button" value="Remove" name="RemoveButton" onclick="document.getElementById('memberAppCondWaitId').value=${condWait.conditionWaitId};document.getElementById('MemberAppUnderwriting_button_pressed').value='RemoveButton';submitFormWithAjaxPost(this.form, 'Underwriting', this);"></td>
                        </tr>
                    </c:if>
                </c:forEach>                
                </tr>
            </table>
            <br/>
            <br/>
        </c:forEach>

    </div>



</agiletags:ControllerForm>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Broker Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty BrokerSearchResults}">
                <span class="label">No Broker found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Broker Code</th>
                        <th>Broker Name</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${BrokerSearchResults}">
                        <tr>
                            <td><label>${entry.brokerCode}</label></td>
                            <td><label>${entry.firstNames}</label></td>
                            <td><input type="button" value="Select" onclick="setElemValues({'${diagCodeId}':'${entry.brokerCode}','${diagNameId}':'${agiletags:escapeStr(entry.firstNames)}','memberBrokerIdDetails':'${entry.entityId}','${brokerFirm}':'${agiletags:escapeStr(entry.firmName)}','${tel}':'${entry.telnum}','${fax}':'${entry.fanum}','${email}':'${entry.emailAddr}','${VIPBroker}':'${entry.VIPBroker}'});swapDivVisbible('${target_div}','${main_div}');"/></td> 
                        </tr>
                    </c:forEach>

                </table>
            </c:otherwise>
        </c:choose>

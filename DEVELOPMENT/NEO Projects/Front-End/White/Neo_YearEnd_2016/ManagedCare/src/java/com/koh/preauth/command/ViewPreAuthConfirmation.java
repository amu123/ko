/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ContactDetails;
import neo.manager.CoverDetails;
import neo.manager.EntityTimeDetails;
import neo.manager.NeoManagerBean;
import neo.manager.PreAuthDetails;

/**
 *
 * @author johanl
 */
public class ViewPreAuthConfirmation extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        List<PreAuthDetails> paList = (List<PreAuthDetails>) session.getAttribute("PreAuthConfirmList");
        if (paList != null) {
            for (PreAuthDetails pa : paList) {
                System.out.println("pa list size = " + paList.size());
                //get provider details

                //set cover details
                String covNum = pa.getCoverNumber();
                System.out.println("ViewPreAuthConfirmation coverNumber = "+covNum);
                List<CoverDetails> cdList = port.getCoverDetailsByCoverNumber(covNum);

                int depCode = pa.getDependantCode();
                System.out.println("depCode from pa = "+depCode);
                for (CoverDetails cd : cdList) {
                    System.out.println("cd.getDependentNumber() = "+cd.getDependentNumber());
                    if(depCode == cd.getDependentNumber()){

                        session.setAttribute("memberName", cd.getName());
                        System.out.println("cd.getName() = "+cd.getName());
                        System.out.println("session Name = "+ session.getAttribute("memberName").toString());
                        session.setAttribute("memberSurname", cd.getSurname());
                        session.setAttribute("memberDOB", cd.getDateOfBirth());
                        session.setAttribute("memberIdNum", cd.getIdNumber());
                        session.setAttribute("memberGender", "Unknown");
                        session.setAttribute("memberStatus", cd.getStatus());

                        int entityId = cd.getEntityId();
                        List<ContactDetails> conList = port.getAllContactDetails(entityId);
                        for (ContactDetails con : conList) {
                            String cm = con.getCommunicationMethod();
                            if(cm.equals("3") || cm.equals("4") || cm.equals("5")){
                                session.setAttribute("memberContact", con.getMethodDetails());
                                break;
                            }
                        }
                        EntityTimeDetails eTime = port.getAge(cd.getDateOfBirth());
                        session.setAttribute("memberAge", eTime.getYears());

                        break;
                    }
                }
            }
        }

        return null;
    }

    @Override
    public String getName() {
        return "ViewPreAuthConfirmation";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.tags;

import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.EntityPaymentRunDetails;

/**
 *
 * @author josephm
 */
public class ViewProviderPaymentRunDetailsTable extends TagSupport {

    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Payment ID</th><th>Bank Name</th><th>Branch Name</th><th>Branch Code</th>"
                    + "<th>Acc Holder</th><th>Acc Number</th><th>Acc Type</th><th>Payment Type</th>"
                    + "<th>Amount</th><th>Payment Run Date</th><th>Status</th><th>Reject Reason</th><th>Action</th></tr>");

            List<EntityPaymentRunDetails> paymentRunList = (List<EntityPaymentRunDetails>) session.getAttribute("practicePaymentRunDetails");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");

            System.out.println("paymentRunList - size" + paymentRunList.size());
            String showTable = String.valueOf(session.getAttribute("ShowTable"));

            if (paymentRunList != null && paymentRunList.size() > 0) {
                if (showTable != null && showTable.equalsIgnoreCase("false")) {
                    out.println("<tr>");
                    out.println("<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td>&nbsp;</td>");
                    out.println("</tr>");
                    session.removeAttribute("ShowTable");
                } else {
                    for (int i = 0; i < paymentRunList.size(); i++) {

                        EntityPaymentRunDetails payRunDetails = paymentRunList.get(i);
                        int payRunId = payRunDetails.getPaymentRunId();
                        String bankName = "";
                        String brachName = "";
                        String brachCode = "";
                        String accountHolder = "";
                        String accountNumber = "";
                        String accountType = "";
                        String paymentType = "";
                        double paymentRunAmount = 0.0d;
                        String paymentRunDate = "";
                        String paymentStatus = "";
                        String paymentRejectionReason = "";

                        //bankName
                        if (payRunDetails.getBankName() != null && !payRunDetails.getBankName().equalsIgnoreCase("")) {
                            bankName = payRunDetails.getBankName();
                        }
                        //brachName
                        if (payRunDetails.getBranchName() != null && !payRunDetails.getBranchName().equalsIgnoreCase("")) {
                            brachName = payRunDetails.getBranchName();
                        }
                        //brachCode
                        if (payRunDetails.getBranchCode() != null && !payRunDetails.getBranchCode().equalsIgnoreCase("")) {
                            brachCode = payRunDetails.getBranchCode();
                        }
                        //accountHolder
                        if (payRunDetails.getAccountHolder() != null && !payRunDetails.getAccountHolder().equalsIgnoreCase("")) {
                            accountHolder = payRunDetails.getAccountHolder();
                        }
                        //accountNumber
                        if (payRunDetails.getAccountNumber() != null && !payRunDetails.getAccountNumber().equalsIgnoreCase("")) {
                            accountNumber = payRunDetails.getAccountNumber();
                        }
                        //accountType
                        if (payRunDetails.getAccountType() != null && !payRunDetails.getAccountType().equalsIgnoreCase("")) {
                            accountType = payRunDetails.getAccountType();
                        }
                        //paymentType
                        if (payRunDetails.getPaymentType() != null && !payRunDetails.getPaymentType().equalsIgnoreCase("")) {
                            paymentType = payRunDetails.getPaymentType();
                        }
                        //paymentRunAmount
                        if (payRunDetails.getPaymentRunAmount() != 0.0d) {
                            paymentRunAmount = payRunDetails.getPaymentRunAmount();
                        }
                        //paymentRunDate
                        if (payRunDetails.getPaymentRunDate() != null) {
                            paymentRunDate = dateFormat.format(payRunDetails.getPaymentRunDate().toGregorianCalendar().getTime());
                        }
                        //paymentStatus
                        if (payRunDetails.getPaymentStatus() != null && !payRunDetails.getPaymentStatus().equalsIgnoreCase("")) {
                            paymentStatus = payRunDetails.getPaymentStatus();
                        }
                        //paymentRejectionReason
                        if (payRunDetails.getPaymentRejectionReason() != null && !payRunDetails.getPaymentRejectionReason().equalsIgnoreCase("")) {
                            paymentRejectionReason = payRunDetails.getPaymentRejectionReason();
                        }

                        out.println("<tr>");
                        out.println("<td><label class=\"label\">" + payRunId + "</label></td>"
                                + "<td><label class=\"label\">" + bankName + "</label></td>"
                                + "<td><label class=\"label\">" + brachName + "</label></td>"
                                + "<td><label class=\"label\">" + brachCode + "</label></td>"
                                + "<td><label class=\"label\">" + accountHolder + "</label></td>"
                                + "<td><label class=\"label\">" + accountNumber + "</label></td>"
                                + "<td><label class=\"label\">" + accountType + "</label></td>"
                                + "<td><label class=\"label\">" + paymentType + "</label></td>"
                                + "<td><label class=\"label\">" + paymentRunAmount + "</label></td>"
                                + "<td><label class=\"label\">" + paymentRunDate + "</label></td>"
                                + "<td><label class=\"label\">" + paymentStatus + "</label></td>"
                                + "<td><label class=\"label\">" + paymentRejectionReason + "</label></td>");
                        System.out.println("<td><label class=\"label\">" + payRunId + "</label></td>"
                                + "<td><label class=\"label\">" + bankName + "</label></td>"
                                + "<td><label class=\"label\">" + brachName + "</label></td>"
                                + "<td><label class=\"label\">" + brachCode + "</label></td>"
                                + "<td><label class=\"label\">" + accountHolder + "</label></td>"
                                + "<td><label class=\"label\">" + accountNumber + "</label></td>"
                                + "<td><label class=\"label\">" + accountType + "</label></td>"
                                + "<td><label class=\"label\">" + paymentType + "</label></td>"
                                + "<td><label class=\"label\">" + paymentRunAmount + "</label></td>"
                                + "<td><label class=\"label\">" + paymentRunDate + "</label></td>"
                                + "<td><label class=\"label\">" + paymentStatus + "</label></td>"
                                + "<td><label class=\"label\">" + paymentRejectionReason + "</label></td>");

                        if (!paymentRunDate.equalsIgnoreCase("")) {
                            out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitPDDAction('" + commandName + "','" + paymentRunDate + "','practicePayRun')\"; value=\"" + commandName + "\">Select</button></td>");
                        } else {
                            out.println("<td></td>");
                        }

                        out.println("</tr>");
                    }
                }
            } else {
                out.println("<tr>");
                out.println("<td><label class=\"label\">&nbsp;</label></td>"
                        + "<td><label class=\"label\">&nbsp;</label></td>"
                        + "<td><label class=\"label\">&nbsp;</label></td>"
                        + "<td><label class=\"label\">&nbsp;</label></td>"
                        + "<td><label class=\"label\">&nbsp;</label></td>"
                        + "<td><label class=\"label\">&nbsp;</label></td>"
                        + "<td><label class=\"label\">&nbsp;</label></td>"
                        + "<td><label class=\"label\">&nbsp;</label></td>"
                        + "<td><label class=\"label\">&nbsp;</label></td>"
                        + "<td><label class=\"label\">&nbsp;</label></td>"
                        + "<td><label class=\"label\">&nbsp;</label></td>"
                        + "<td><label class=\"label\">&nbsp;</label></td>"
                        + "<td>&nbsp;</td>");
                out.println("</tr>");
            }

            out.println("</table>");


        } catch (java.io.IOException ex) {
            throw new JspException("Error in ViewProviderPaymentRunDetailsTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

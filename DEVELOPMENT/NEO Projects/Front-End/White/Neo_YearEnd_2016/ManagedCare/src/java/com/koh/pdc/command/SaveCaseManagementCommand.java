/*  
 * To change this template, choose Tools | Templates    
 * and open the template in the editor. 
 */
package com.koh.pdc.command;

import com.koh.command.FECommand;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.koh.command.NeoCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import neo.manager.CommunicationMember;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author dewaldo
 */
public class SaveCaseManagementCommand extends FECommand {

    private String contactTypeMessage;
    private String TYPE_COMMUNICATION;
    private String contactTypeFailureMessage = "Communication method failed for";
    private int reasonForPhoneCall;
    private String Status = null;
    private CommunicationMember comMember = new CommunicationMember();
    private String caseMessage = null;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("entered SaveCaseManagementCommand");
        HttpSession session = request.getSession();

        session.setAttribute("statementScreenFlag", request.getParameter("flag"));
        comMember.setMemberName((String) session.getAttribute("patientName"));
        comMember.setMemberSurname((String) session.getAttribute("surname"));
        comMember.setEntityId(Integer.parseInt(String.valueOf(session.getAttribute("entityId"))));
        comMember.setSubject(request.getParameter("subject"));
        comMember.setMemberEmail(request.getParameter("userEmail"));

        comMember.setProductId(Integer.parseInt(String.valueOf(session.getAttribute("productId"))));
        comMember.setMemberMobileNumber(request.getParameter("cellNumber"));
        comMember.setMemberTitle((String) session.getAttribute("title"));
        comMember.setMemberInitials((String) session.getAttribute("initials"));
        reasonForPhoneCall = Integer.parseInt(request.getParameter("reasonForPhoneCall"));
        int contactType = Integer.parseInt(request.getParameter("reasonForAssignment"));

        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

        //gets the details for the user currently logged in
        NeoUser user = (NeoUser) request.getSession().getAttribute("persist_user");

        try {

            System.out.println("The patient " + comMember.getMemberName());

            if (comMember.getMemberName() != null && !comMember.getMemberName().equalsIgnoreCase("")) {

                //Set all appropriate values for the corresponding contact type
                switch (contactType) {
                    case 1:
                        Status = "Success";
                        TYPE_COMMUNICATION = "Phone";
                        contactTypeFailureMessage = "ERROR: Failed to save phone call for";
                        contactTypeMessage = "Phone call saved for";
                        caseMessage = port.getReasonForContactValue(reasonForPhoneCall);
                        break;
                    case 2:
                        TYPE_COMMUNICATION = "SMS";
                        contactTypeFailureMessage = "ERROR: Failed to send SMS to";
                        contactTypeMessage = "SMS successfully sent to";
                        caseMessage = (String) request.getParameter("smsMessage");
                        sendEmailOrSMS(port);
                        break;
                    case 3:
                        TYPE_COMMUNICATION = "Email";
                        contactTypeFailureMessage = "ERROR: Failed to send Email to";
                        contactTypeMessage = "Email successfully sent to";
                        caseMessage = (String) request.getParameter("emailMessage").replace("\n", "<br/>");
                        sendEmailOrSMS(port);
                        break;
                    default:
                        //Failure
                        retMsg(response, "ERROR: Failed to get Communication method for patient!");
                        break;
                }

                //Check if the communication was sent successfully to insert into contact history
                if (Status.equalsIgnoreCase("Success") || (Status.equalsIgnoreCase("Success") && contactType == 1)) {

                    //insert into history
                    int historyInserted = port.insertContactHistory(comMember.getEntityId(), contactType, caseMessage, user.getUserId());

                    if (historyInserted == 1) {
                        //Remove values from session
                        session.removeAttribute("contactHistory");
                        session.removeAttribute("reasonForAssignment_text");
                        session.removeAttribute("contactPatient_text");

                        //Success
                        retMsg(response, contactTypeMessage);
                    } else {
                        //Failure
                        retMsg(response, "ERROR: Failed to insert the contact history BUT, " + contactTypeMessage);
                    }
                } else {
                    //Failure
                    retMsg(response, contactTypeFailureMessage);
                }
            }

        } catch (Exception ex) {
            //Failure
            Logger.getLogger(SaveCaseManagementCommand.class.getName()).log(Level.SEVERE, null, ex);
            retMsg(response, "ERROR: Failed to send communication to ");
        }

        return null;
    }

    public void sendEmailOrSMS(NeoManagerBean port) {
        //Check if neccessary to send email or SMS and ignore Phone
        if (TYPE_COMMUNICATION != null && !TYPE_COMMUNICATION.equalsIgnoreCase("Phone")) {
            comMember.setMessage(caseMessage);

            Status = port.sendEmailOrSmsToPDCWorkBenchPatient(comMember, TYPE_COMMUNICATION);

            if (Status.equalsIgnoreCase("Email sending successful")) {
                Status = "Success";
            }
        }
    }

    public void retMsg(HttpServletResponse response, String notify) {
        PrintWriter out = null;

        try {
            out = response.getWriter();

            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            out.println("<td><label class=\"header\">" + notify + " " + comMember.getMemberName() + "</label></td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");
            response.setHeader("Refresh", "3; URL=/ManagedCare/PDC/ManageEvent.jsp");

        } catch (IOException ex) {
            Logger.getLogger(SaveCaseManagementCommand.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    @Override
    public String getName() {
        return "SaveCaseManagementCommand";
    }
}

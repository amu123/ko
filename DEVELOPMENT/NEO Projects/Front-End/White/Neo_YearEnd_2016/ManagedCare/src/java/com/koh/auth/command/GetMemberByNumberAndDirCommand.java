/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.auth.command;

import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ContactDetails;
import neo.manager.CoverDetails;
import neo.manager.Member;
import neo.manager.NeoManagerBean;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class GetMemberByNumberAndDirCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(GetMemberByNumberAndDirCommand.class);
    private CoverDetails c;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        logger.info("******************************************************");
        HttpSession session = request.getSession();

        session.removeAttribute("emailAddress");

        String number = request.getParameter("number");
        String email = "";
        boolean viewStaff = (Boolean) session.getAttribute("persist_user_viewStaff");
        session.setAttribute("statementScreenFlag", request.getParameter("statementScreenFlag"));

        logger.info("Number : " + number);

        NeoManagerBean port = service.getNeoManagerBeanPort();

        c = port.getEAuthResignedMembers(number);

        try {
            PrintWriter out = response.getWriter();
            if (!viewStaff) {

                if (c != null && c.getEntityId() > 0) {
                    Member mem = port.fetchMemberAddInfo(c.getEntityId());
                    c.setMemberCategory(mem.getMemberCategoryInd());
                    logger.info("EntityId " + c.getEntityId() + " MemberCategoryInd " + mem.getMemberCategoryInd());

                    if (c.getMemberCategory() != 2) {
                        //Get email contact details

                        ContactDetails cd = port.getContactDetails(c.getEntityId(), 1);
                        if (cd != null && cd.getPrimaryIndicationId() == 1) {
                            email = cd.getMethodDetails();
                        }

                        logger.info("Response : " + getName() + "|Number=" + number + "|Email=" + email + "|productId=" + c.getProductId() + "$");
                        out.print(getName() + "|Number=" + number + "|Email=" + email + "|productId=" + c.getProductId() + "$");
                        out.print("#");
                        out.print(getDirectories(number, request));
                        out.print("*");
                        logger.info("Directories: " + getDirectories(number, request));
                        
                    } else {
                        out.print("Access|User access denied. Please contact your supervisor|" + getName());
                    }

                } else {
                    out.print("Error|No such member|" + getName());
                }
            } else {
                if (c != null && c.getEntityId() > 0) {
                    logger.info("EntityId " + c.getEntityId() + " No MemberCategoryInd loaded");
                    //Get email contact details
                    ContactDetails cd = port.getContactDetails(c.getEntityId(), 1);

                    if (cd != null && cd.getPrimaryIndicationId() == 1) {
                        email = cd.getMethodDetails();
                    }

                    logger.info("Response : " + getName() + "|Number=" + number + "|Email=" + email + "|productId=" + c.getProductId() + "$");
                    out.print(getName() + "|Number=" + number + "|Email=" + email + "|productId=" + c.getProductId() + "$");
                    out.print("#");
                    out.print(getDirectories(number, request));
                    out.print("*");
                    logger.info("Directories: " + getDirectories(number, request));

                } else {
                    out.print("Error|No such member|" + getName());
                }
            }
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetMemberByNumberAndDirCommand";
    }

    private String getDirectories(String memberNo, HttpServletRequest request) {
        StringBuffer sb = new StringBuffer();
        HttpSession session = request.getSession();

        try {
            SimpleDateFormat sTOd = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat dTOs = new SimpleDateFormat("yyyy/MM/dd");

            String path = new PropertiesReader().getProperty("statements");

            session.setAttribute("StatementProductID", String.valueOf(c.getProductId()));
            
            if (c.getProductId() == 1) {
                path += "ResolutionHealth\\";
            } else if (c.getProductId() == 2) {
                path += "Spectramed\\";
            }

            //logger.info("Path: " + path);

            File dir = new File(path);
            boolean isAdd = false;

            for (File moreDir : dir.listFiles()) {

                if (moreDir.isDirectory()) {
                    File file = new File(moreDir.getAbsolutePath() + "/member/Statement_" + memberNo + ".pdf");

                    if (file.isFile()) {

                        if (isAdd) {
                            sb.append("!");
                        } else {
                            isAdd = true;
                        }

                        Date d = sTOd.parse(moreDir.getName());
                        String dateStr = dTOs.format(d);
                        sb.append(dateStr);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception getting the folders: " + e.getMessage());
        }
        return sb.toString();
    }
}

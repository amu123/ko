/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthCoverExclusions;
import neo.manager.NeoManagerBean;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class LoadMemberUnderwritingCommand extends NeoCommand {
    
    

    private Logger logger = Logger.getLogger(LoadMemberUnderwritingCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        logger.info("*************************** LoadMemberUnderwritingCommand ********************************");
       
        HttpSession session = request.getSession();
        String coverNum = (String) session.getAttribute("policyHolderNumber");

        PrintWriter out = null;

        try {
            out =  response.getWriter();
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(ViewMemberUnderwritingCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        String dependantSelect = request.getParameter("dependantSelect");
        logger.info("dependantSelect : " + dependantSelect);

        if (dependantSelect==null) dependantSelect = "0";
        logger.info("coverNumber : "  + coverNum);

        List<AuthCoverExclusions> underwriting = port.getCoverExclusionHistoryByCoverDependant(coverNum,dependantSelect);
        if (underwriting != null && underwriting.size() > 0) {
            for (AuthCoverExclusions ce : underwriting) {
                if (ce.getExclusionDateFrom() != null && ce.getExclusionDateTo() != null) {
                    ce.setDuration(String.valueOf(DateTimeUtils.getMonthsDiff(ce.getExclusionDateFrom(), ce.getExclusionDateTo())));
                }
            }
        }

        StringBuilder sb = new StringBuilder();

        sb.append("<br><br>");
        sb.append("<table class='list' id='dependantUnderwritingDetails' style='border-style:none; border-collapse:collapse; border-width:1px'>");

        if (!underwriting.isEmpty()) {
            
            sb.append("<tbody>");
            sb.append("<tr><th>Cover Number</th><th>Diagnosis Code</th><th>Date From</th><th>Date To</th><th>Exclusion Type</th> <th>PMB Allowed</th><th>Duration</th></tr>");

            for (AuthCoverExclusions ace : underwriting) {

                sb.append("<tr>");

                sb.append("<td>");
                sb.append("<label>");
                sb.append(ace.getCoverNumber());
                sb.append("</label>");
                sb.append("</td>");
                

                sb.append("<td>");
                sb.append("<label>");
                if (ace.getDiagnosisCode()!=null)
                sb.append(ace.getDiagnosisCode());
                else 
                    sb.append("");
                sb.append("</label>");
                sb.append("</td>");
                

                sb.append("<td>");
                sb.append("<label>");
                sb.append(DateTimeUtils.convertXMLGregorianCalendarToDate(ace.getExclusionDateFrom()));
                sb.append("</label>");
                sb.append("</td>");
                
                sb.append("<td>");
                sb.append("<label>");
                sb.append(DateTimeUtils.convertXMLGregorianCalendarToDate(ace.getExclusionDateTo()));
                sb.append("</label>");
                sb.append("</td>");
                
                
                sb.append("<td>");
                sb.append("<label>");
                String exclusionType = ace.getExclusionType();
                if ("1".equalsIgnoreCase(exclusionType)) {
                    sb.append("ICD10");
                } else if ("4".equalsIgnoreCase(exclusionType)) {
                    sb.append("General");
                } else {
                    sb.append("");
                }
                sb.append("</label>");
                sb.append("</td>");

                sb.append("<td>");
                sb.append("<label>");
                sb.append(ace.getPmbAllowed());
                sb.append("</label>");
                sb.append("</td>");

                sb.append("<td>");
                sb.append("<label>");
                sb.append(ace.getDuration());
                sb.append("</label>");
                sb.append("</td>");
                
                sb.append("</tr>");
            }
            
            sb.append("</tbody>");
        } else {
            out.println("<span class='label'>No Underwriting details available</span>");
        }

        sb.append("</table>");

        logger.info("Total returned : " + underwriting.size());

        out.println(sb.toString());

        return null;
    }

    @Override
    public String getName() {

        return "LoadMemberUnderwritingCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.FECommand;
import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.FormatUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthHospitalLOC;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.TariffCode;

/**
 *
 * @author johanl
 */
public class AddAuthLOSToSessionCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        List<AuthHospitalLOC> locList = null;
        boolean justAddLoc = false;
        if (session.getAttribute("AuthLocList") != null) {
            locList = (List<AuthHospitalLOC>) session.getAttribute("AuthLocList");
            if (locList == null && locList.isEmpty()) {
                locList = new ArrayList<AuthHospitalLOC>();
                justAddLoc = true;
            } else {
                if (locList.size() == 0) {
                    locList = new ArrayList<AuthHospitalLOC>();
                    justAddLoc = true;
                }
            }
        } else {
            locList = new ArrayList<AuthHospitalLOC>();
            justAddLoc = true;
        }

        //Reset errors
        session.setAttribute("hospLoc_error", null);
        session.setAttribute("locDateFrom_error", null);
        session.setAttribute("hospLos_error", null);

        boolean valid = true;
        //Get values
        String calType = request.getParameter("calType");
        System.out.println("calType for add = " + calType);
        //default values
        String levelOfCare = request.getParameter("hospLoc");
        String locDowngrade = request.getParameter("locDown");
        String levelOfCareDesc = request.getParameter("locDesc");
        String downgradeDesc = request.getParameter("downDesc");
        String estimatedCost = request.getParameter("estimatedCost");
        String savingAchieved = request.getParameter("savingAchieved");
        
        System.out.println("ESTIMATED COST = " + estimatedCost);

        String lengthOfStay = "";
        String approvedLOS = "";
        String dateFrom = "";
        String dateTo = "";
        //error fields
        String dateFromError = "";
        String dateToError = "";
        String losError = "";
        String los2Error = "";

        boolean toggelOn = false;
        if (calType == null || calType.equalsIgnoreCase("null")) {
            System.out.println("calType not checked");
            lengthOfStay = request.getParameter("dcHospLos");
            approvedLOS = request.getParameter("dcAppLos");
            dateFrom = request.getParameter("dcDateFrom");
            dateTo = request.getParameter("dcDateTo");
            //set error fields
            dateFromError = "dcDateFrom_error";
            dateToError = "dcDateTo_error";
            losError = "dcHospLos_error";
            los2Error = "dcAppLos_error";

        } else if (calType.trim().equalsIgnoreCase("on")) {
            toggelOn = true;
            lengthOfStay = request.getParameter("hospLos");
            approvedLOS = request.getParameter("appHospLos");
            dateFrom = request.getParameter("locDateFrom");
            dateTo = request.getParameter("locDateTo");
            //set error fields
            dateFromError = "locDateFrom_error";
            dateToError = "locDateTo_error";
            losError = "hospLos_error";
            los2Error = "appHospLos_error";

        }


        //Test values
        if (levelOfCare == null || levelOfCare.trim().equalsIgnoreCase("99")) {
            session.setAttribute("hospLoc_error", "Level of Care is mandatory");
            valid = false;
        } else {
            session.setAttribute("hospLoc", levelOfCare);
            session.setAttribute("hospDesc", levelOfCareDesc);
        }
        
        boolean addDowngrade = false;
        if (locDowngrade != null && !locDowngrade.trim().equalsIgnoreCase("99")) {
            addDowngrade = true;
            session.setAttribute("locDown", locDowngrade);
            session.setAttribute("downDesc", downgradeDesc);
        }

        if (dateFrom == null || dateFrom.trim().equalsIgnoreCase("")) {
            session.setAttribute(dateFromError, "Date From is mandatory");
            valid = false;
        } else {
            session.setAttribute("locDateFrom", dateFrom);
        }
        if (lengthOfStay == null || lengthOfStay.trim().equalsIgnoreCase("")) {
            session.setAttribute(losError, "Length of Stay is mandatory");
            valid = false;
        } else {
            session.setAttribute("hospLos", lengthOfStay);
        }

        if (approvedLOS == null || approvedLOS.trim().equalsIgnoreCase("")) {
            session.setAttribute(los2Error, "Approved Length of Stay is mandatory");
            valid = false;
        } else {
            if (toggelOn) {
                session.setAttribute("appHospLos", approvedLOS);
            } else {
                session.setAttribute("dcAppLos", approvedLOS);
            }

        }


        if (dateTo == null || dateTo.trim().equalsIgnoreCase("")) {
            session.setAttribute(dateToError, "Date To is mandatory");
            valid = false;
        } else {
            session.setAttribute("locDateTo", dateTo);
        }

        if (estimatedCost != null && !estimatedCost.trim().equalsIgnoreCase("")) {
            session.setAttribute("estimatedCost", estimatedCost);
        }
        //if (savingAchieved != null && !savingAchieved.trim().equalsIgnoreCase("")) {
        //  session.setAttribute("savingAchieved", savingAchieved);
        //}

        if (valid) {
            XMLGregorianCalendar xDateFrom = null;
            XMLGregorianCalendar xDateTo = null;
            try {
                Date dateF = sdf.parse(dateFrom);
                Date dateT = sdf.parse(dateTo);
                xDateFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(dateF);
                xDateTo = DateTimeUtils.convertDateToXMLGregorianCalendar(dateT);
            } catch (ParseException pe) {
                System.out.println("Date format problem : " + pe.getMessage());
            }

            //check if date already exists
            DateTimeUtils.setMapAndReorderLOCByDates(locList);
            boolean dateExist = false;
            HashMap<XMLGregorianCalendar, AuthHospitalLOC> dateMap = DateTimeUtils.locListNew;
            if (dateMap.containsKey(xDateFrom)) {
                AuthHospitalLOC loc = dateMap.get(xDateFrom);

                String facilityDisc = "" + session.getAttribute("facilityProvDiscTypeId");
                int provDesc = Integer.parseInt(facilityDisc);
                int locTypeId = getLOCLookupId(provDesc);

                String error = "";
                String locDesc = NeoCommand.service.getNeoManagerBeanPort().getValueFromCodeTableForTableId(locTypeId, loc.getLevelOfCare());
                error = locDesc + "Already has an Admission Date for "
                        + FormatUtils.dateFormat.format(loc.getDateFrom().toGregorianCalendar().getTime());

                session.setAttribute("locDateMapError", error);
                dateExist = true;
            }

            if (!dateExist) {
                session.removeAttribute("locDateMapError");

                AuthHospitalLOC loc = new AuthHospitalLOC();
                loc.setUserId(user.getUserId());
                loc.setLevelOfCare(levelOfCare);
                loc.setLevelOfCareDesc(levelOfCareDesc);
                if (lengthOfStay != null && !lengthOfStay.trim().equalsIgnoreCase("")) {
                    loc.setLengthOfStay(new Double(lengthOfStay));
                } else {
                    loc.setLengthOfStay(0.0d);
                }
                if (approvedLOS != null && !approvedLOS.trim().equalsIgnoreCase("")) {
                    loc.setApprovedLos(new Double(approvedLOS));
                } else {
                    loc.setApprovedLos(0.0d);
                }
                loc.setDateFrom(xDateFrom);
                loc.setDateTo(xDateTo);
                if (estimatedCost != null && !estimatedCost.trim().equalsIgnoreCase("")) {
                    loc.setEstimatedCost(new Double(estimatedCost));
                } else {
                    loc.setEstimatedCost(0.0d);
                }
                if (savingAchieved != null && !savingAchieved.trim().equalsIgnoreCase("")) {
                    loc.setSavingsAchieved(new Double(savingAchieved));
                } else {
                    loc.setSavingsAchieved(0.0d);
                }
                if (addDowngrade) {
                    loc.setLocDowngrade(locDowngrade);
                    loc.setDowngradeDesc(downgradeDesc);
                }

                if (justAddLoc) {
                    locList.add(loc);
                } else {
                    locList = DateTimeUtils.orderAndRecalculateLOCDetails(locList, loc);
                }

                System.out.println("locList size = " + locList.size());
                session.setAttribute("AuthLocList", locList);
                session.removeAttribute("hospLoc");
                session.removeAttribute("hospDesc");
                session.removeAttribute("locDateFrom");
                session.removeAttribute("hospLos");
                session.removeAttribute("locDateTo");
                session.removeAttribute("estimatedCost");
                session.removeAttribute("locDown");
                session.removeAttribute("downDesc");
                session.removeAttribute("locDateMap");
                session.removeAttribute("appHospLos");
                session.removeAttribute("dcAppLos");
            }
        }

        try {
            String nextJSP = "/PreAuth/AuthSpecificLOC.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "AddAuthLOSToSessionCommand";
    }

    public static TariffCode getWardTariff(XMLGregorianCalendar admission, int prodId,
            int optId, String code, String facilityDisc, String facilityNum, String icd10Str) {

        List<String> icd10 = new ArrayList<String>();
        icd10.add(icd10Str);

        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        TariffCode t = port.getTariffCode(prodId, optId, admission, facilityDisc, facilityNum, facilityNum, icd10, null, code);

        return t;
    }

    public int getLOCLookupId(int providerType) {
        int wardId = 125;
        if (providerType == 79) {
            wardId = 272;

        } else if (providerType == 55) {
            wardId = 273;

        } else if (providerType == 59) {
            wardId = 274;

        } else if (providerType == 49) {
            wardId = 275;

        } else if (providerType == 76) {
            wardId = 276;

        } else if (providerType == 47) {
            wardId = 277;

        } else if (providerType == 56) {
            wardId = 278;

        } else if (providerType == 77) {
            wardId = 279;

        }
        return wardId;
    }
}

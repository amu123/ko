/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.workflow.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.workflow.tags.ViewWorkflowSearchResults;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.WorkflowItem;
import org.eclipse.birt.report.engine.content.impl.PageContent;

/**
 *
 * @author janf
 */
public class WorkflowCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        String command = request.getParameter("command");

        System.out.println("Operation: WorkflowCommand - Command: " + command);
        try {
            if ("memberCRMByCov".equalsIgnoreCase(command)) {
                getMemberCRMByCoverNum(request, response, context, port);
            } else if ("memberCRMByEntity".equalsIgnoreCase(command)) {
                getMemberCRMByEntityId(request, response, context, port);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private void getMemberCRMByCoverNum(HttpServletRequest request, HttpServletResponse response, ServletContext context, NeoManagerBean port) throws ServletException, IOException {
        String coverNumber = request.getParameter("id");
        System.out.println("coverNumber = " + coverNumber);
        WorkflowItem item = new WorkflowItem();
        item.setEntityNumber(coverNumber);

        List<WorkflowItem> itemList = port.getWFSearchResults(item);
        request.setAttribute("viewWorkflowSearchResults", itemList);
        request.setAttribute("workflowSearch", "False");
        context.getRequestDispatcher("/Workflow/WorkflowSearchResults.jsp").forward(request, response);
    }

    private void getMemberCRMByEntityId(HttpServletRequest request, HttpServletResponse response, ServletContext context, NeoManagerBean port) throws ServletException, IOException {
        String entityId = request.getParameter("id");
        System.out.println("entityId = " + entityId);
        WorkflowItem item = new WorkflowItem();
        if (entityId != null && !entityId.equalsIgnoreCase("")) {
            item.setEntityID(Integer.parseInt(entityId));

            List<WorkflowItem> itemList = port.getWFSearchResults(item);
            request.setAttribute("viewWorkflowSearchResults", itemList);
        }
        request.setAttribute("workflowSearch", "False");
        context.getRequestDispatcher("/Workflow/WorkflowSearchResults.jsp").forward(request, response);
    }

    @Override
    public String getName() {
        return "WorkflowCommand";
    }

}

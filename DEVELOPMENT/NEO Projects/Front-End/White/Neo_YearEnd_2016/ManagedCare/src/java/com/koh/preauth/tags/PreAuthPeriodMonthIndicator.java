/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author Johan-NB
 */
public class PreAuthPeriodMonthIndicator extends TagSupport {

    private String elementName;
    private String displayName;
    private String javaScript;
    private String valueFromSession;
    private String mandatory;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        //generate generic dates for period
        Date today = new Date(System.currentTimeMillis());
        String ts = new SimpleDateFormat("yyyy/MM/dd").format(today);
        String currentMonth = ts.substring(5, 7);
        String[] monthsOfYear = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
        String sessionValerror = "";
        if (session.getAttribute(elementName + "_error") != null) {
            sessionValerror = "" + session.getAttribute(elementName + "_error");
        }

        try {
            if (displayName != null && !displayName.equalsIgnoreCase("")) {
                out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td>");
            } else {
                out.println("<td align=\"left\" width=\"160px\"><label></label></td>");
            }

            out.println("<td align=\"left\" width=\"200px\"><select style=\"width:215px\" name=\"" + elementName + "\" id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");

            String sessionVal = "" + session.getAttribute(elementName);
            System.out.println("session val = " + sessionVal);
            boolean isSesVal = false;
            boolean checkSesVal = false;
            if (valueFromSession.trim().equalsIgnoreCase("yes")) {
                if (!sessionVal.equalsIgnoreCase("") && !sessionVal.equalsIgnoreCase("null")) {
                    checkSesVal = true;
                }
            }
            for (String str : monthsOfYear) {
                if (checkSesVal) {
                    if (sessionVal.equalsIgnoreCase(str)) {
                        isSesVal = true;
                    }else{
                        isSesVal = false;
                    }
                }

                if (str.equalsIgnoreCase(currentMonth) && checkSesVal == false) {
                    out.println("<option value=\"" + str + "\" selected>" + str + "</option>");

                } else {
                    if (isSesVal == true) {
                        out.println("<option value=\"" + str + "\" selected>" + str + "</option>");
                    } else {
                        out.println("<option value=\"" + str + "\">" + str + "</option>");
                    }
                }
            }

            out.println("</select></td>");
            if (mandatory.equalsIgnoreCase("yes")) {
                out.println("<td><label class=\"red\">*</label></td>");
            } else {
                out.println("<td></td>");
            }
            //search column
            out.println("<td width=\"30px\"></td>");

            if (sessionValerror != null && !sessionValerror.equalsIgnoreCase("null")) {
                out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
            } else {
                out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
            }


        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelNeoLookupValueDropDown tag", ex);
        }

        return super.doEndTag();
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setValueFromSession(String valueFromSession) {
        this.valueFromSession = valueFromSession;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.tags;

import agility.za.indexdocumenttype.*;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author Christo
 */
public class DocumentIndexingFileList extends TagSupport {

    private String command;
    private String multiple;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        String profileType = ((String) req.getParameter("profiletype"))!=null?((String) req.getParameter("profiletype")):"";
        String profileTypeFromSession = (String)((HttpServletRequest)req).getSession().getAttribute("profile")!=null?(String)((HttpServletRequest)req).getSession().getAttribute("profile"):"";
//        System.out.println("profileTypeFromSession : "+profileTypeFromSession);
//        System.out.println("profileType :"+profileType);
        profileType = (profileTypeFromSession!=null && !profileTypeFromSession.equals(""))?profileTypeFromSession:profileType;
        try {
            if(profileType!=null && !profileType.equals("")){
            String listOrWork = (String) req.getAttribute("listOrWork");
//            System.out.println("listOrWork = " + listOrWork);
            IndexDocumentRequest request = new IndexDocumentRequest();
            String folderList = (String) req.getAttribute("folderList");
            String selectedFile = (String) req.getAttribute("selectedFile");
            String srcDrive = new PropertiesReader().getProperty("DocumentIndexWorkDrive");
            if (folderList == null || listOrWork.equals("List")) {
                folderList = new PropertiesReader().getProperty("DocumentIndexScanFolder");
                srcDrive = new PropertiesReader().getProperty("DocumentIndexScanDrive");
            }
//            System.out.println("folderList : "+folderList);
//            System.out.println("srcDrive : "+srcDrive);
            request.setIndexType("FileList");
            FileListType fileListType = new FileListType();
//            System.out.println("profiletype before fileListType.setFolder  : "+srcDrive);
            fileListType.setFolder(folderList+"/"+profileType);
            request.setFileList(fileListType);
            request.setSrcDrive(srcDrive);
            IndexDocumentResponse response = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(request);
//           System.out.println("sorce location  : "+request.getSrcDrive()+ request.getFileList().getFolder()); 
            List<FileType> fileList = response.getFile();
            if (fileList != null) {
//                System.out.println("File List result size = " + fileList.size());
                out.println("<tr><td> <select style=\"width:215px\" size=\"10\" name=\"selectedFile\" id=\"selectedFile\"");
                if (multiple != null && multiple.equals("true")) {
                    out.println("multiple=\"multiple\">");
                } else {
                    out.println(">");
                }

                for (FileType file : fileList) {
                    if (selectedFile != null && selectedFile.equals(file.getFileName())) {
                        out.println("<option selected=\"selected\" value=\"" + file.getFileName() + "\" >" + file.getFileName() + "</option>");
                    } else {
                        out.println("<option value=\"" + file.getFileName() + "\" >" + file.getFileName() + "</option>");
                    }
                }
                out.println("</select></td></tr>");
                out.println("<tr><td>");
                out.println("<label>Count: " + fileList.size() + "</label>");
                out.println("</td></tr>");
            }
            out.println("<tr><td>");
            //System.out.println("_listOrWork = " + listOrWork);
            if (listOrWork != null && listOrWork.equals("Work")) {
                out.println("<button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexPreviewCommand'; setParams();\">Preview</button>");
                out.println("<button type=\"submit\" name=\"opperation\" id=\"profileIndexButton\" onClick=\"document.getElementById('opperation').value ='DocumentIndexCommand'; setParams();\">Index</button>");
                out.println("<button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexBackCommand'; setParams();\">List</button></td></tr>");
                out.println("<tr><td><button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexMergeCommand'; setParams();\">Merge</button>");
                out.println("<button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexSplitCommand'; setParams();\">Split</button>");
                out.println("<button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexCopyCommand'; setParams();\">Copy</button>");
                out.println("<button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexDeleteCommand'; setParams();\">Delete</button>");
            } else {
                out.println("<button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexFileListCommand'; setParams();\">Select</button>");
                //out.println("<button type=\"button\" name=\"previewDoc\" onClick=\"viewPDF();\">Preview</button>");
            }
            out.println("</td></tr>");
            System.out.println("--End tag--");
                
            }else{
                profileType = "NOFileSelected";
                String listOrWork = (String) req.getAttribute("listOrWork");
//            System.out.println("listOrWork = " + listOrWork);

            IndexDocumentRequest request = new IndexDocumentRequest();
            String folderList = (String) req.getAttribute("folderList");
            String selectedFile = (String) req.getAttribute("selectedFile");
            String srcDrive = new PropertiesReader().getProperty("DocumentIndexWorkDrive");
            if (folderList == null || listOrWork.equals("List")) {
                folderList = new PropertiesReader().getProperty("DocumentIndexScanFolder");
                srcDrive = new PropertiesReader().getProperty("DocumentIndexScanDrive");
            }

//            System.out.println("folderList : "+folderList);
//            System.out.println("srcDrive :"+srcDrive);
            request.setIndexType("FileList");
            FileListType fileListType = new FileListType();

            fileListType.setFolder(folderList+"/"+profileType);
            request.setFileList(fileListType);
            request.setSrcDrive(srcDrive);
            IndexDocumentResponse response = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(request);

            List<FileType> fileList = response.getFile();
            if (fileList != null) {
//                System.out.println("File List result size = " + fileList.size());
                out.println("<tr><td> <select style=\"width:215px\" size=\"10\" name=\"selectedFile\" id=\"selectedFile\" ");
                if (multiple != null && multiple.equals("true")) {
                    out.println("multiple=\"multiple\">");
                } else {
                    out.println(">");
                }

                for (FileType file : fileList) {
                    if (selectedFile != null && selectedFile.equals(file.getFileName())) {
                        out.println("<option selected=\"selected\" value=\"" + file.getFileName() + "\" >" + file.getFileName() + "</option>");
                    } else {
                        out.println("<option value=\"" + file.getFileName() + "\" >" + file.getFileName() + "</option>");
                    }
                }
                out.println("</select></td></tr>");
                out.println("<tr><td>");
                out.println("<label>Count: " + fileList.size() + "</label>");
                out.println("</td></tr>");
            }
            out.println("<tr><td>");
            //System.out.println("_listOrWork = " + listOrWork);
            if (listOrWork != null && listOrWork.equals("Work")) {
                out.println("<button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexPreviewCommand'; setParams();\">Preview</button>");
                out.println("<button type=\"submit\" name=\"opperation\" id=\"profileIndexButton\" onClick=\"document.getElementById('opperation').value ='DocumentIndexCommand'; setParams();\">Index</button>");
                out.println("<button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexBackCommand'; setParams();\">List</button></td></tr>");
                out.println("<tr><td><button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexMergeCommand'; setParams();\">Merge</button>");
                out.println("<button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexSplitCommand'; setParams();\">Split</button>");
                out.println("<button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexCopyCommand'; setParams();\">Copy</button>");
                out.println("<button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexDeleteCommand'; setParams();\">Delete</button>");
            } else {
                out.println("<button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexFileListCommand'; setParams();\">Select</button>");
                //out.println("<button type=\"button\" name=\"previewDoc\" onClick=\"viewPDF();\">Preview</button>");
            }
            out.println("</td></tr>");
            System.out.println("--End tag--");
                
            }
            
        } catch (java.io.IOException ex) {
            throw new JspException("Error in DocumentIndexingFileList tag", ex);
        }
        return super.doEndTag();
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public void setMultiple(String multiple) {
        this.multiple = multiple;
    }
}

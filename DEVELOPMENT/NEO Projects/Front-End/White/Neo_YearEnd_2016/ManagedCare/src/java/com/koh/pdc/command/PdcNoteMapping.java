/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import neo.manager.PdcNotes;

/**
 *
 * @author pavaniv
 */
public class PdcNoteMapping {

    private static final String[][] NOTES_MAPPINGS = {
        {"memberEntityId", "no", "int", "PdcNotes", "memberEntityId"},
        {"notes", "no", "str", "PdcNotes", "noteDetails"}
    };

    public static Map<String, String> validateNotes(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, NOTES_MAPPINGS);
        return errors;
    }

    public static PdcNotes getPdcNotes(HttpServletRequest request, neo.manager.NeoUser neoUser) {

        PdcNotes ma = (PdcNotes) TabUtils.getDTOFromRequest(request, PdcNotes.class, NOTES_MAPPINGS, "PdcNotes");
        Date today = new Date();
        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
        HttpSession session = request.getSession();
        Integer entityId = (Integer) session.getAttribute("entityId");
//        String depenNo = (String) session.getAttribute("dependantNumber").toString();
//        String notedetail = (String) request.getParameter("notes");
        //this.saveScreenToSession(request);
        try {
            ma.setNoteDetails(request.getParameter("notes"));
            ma.setEntityId(entityId);
            ma.setSecurityGroupId(neoUser.getSecurityGroupId());
            ma.setLastUpdatedBy(neoUser.getUserId());
            ma.setCreatedBy(neoUser.getUserId());
            ma.setNoteDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
            ma.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
            ma.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
            ma.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
            ma.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));

            return ma;
        } catch (Exception e) {
            System.out.println("Printing Exception  : " + e);
        }
        return ma;
    }
}

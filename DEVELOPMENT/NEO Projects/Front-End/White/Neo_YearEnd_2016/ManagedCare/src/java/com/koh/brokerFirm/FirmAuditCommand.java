/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.brokerFirm;

import com.koh.command.NeoCommand;
import com.koh.employer.command.EmployerSearchCommand;
import com.koh.utils.MapUtils;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class FirmAuditCommand extends NeoCommand{
       @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        try {
            String name = request.getParameter("auditList");
            String entityId = request.getParameter("brokerFirmEntityId");
            System.out.println("brokerFirmEntityId " + entityId);
            int id = 0;
            try {
                id = Integer.parseInt(entityId);
            } catch (Exception e) {
                
            }
            List<KeyValueArray> kva = port.getAuditTrail(name, id);
            Object col = MapUtils.getMap(kva);
            request.setAttribute("FirmAuditDetails", col);
            context.getRequestDispatcher("/Broker/BrokerFirmAuditDetails.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(EmployerSearchCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "FirmAuditCommand";
    }
 
}

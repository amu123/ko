/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.calltrack.tags;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import neo.manager.LookupValue;
//import neo.manager.CallTrackQueries;

/**
 *
 * @author josephm
 */
public class QueriesCheckBoxTag extends TagSupport {
    private String elementName;
    private String numberPerRow;
    private String displayName;
    private String lookupId;
    private String javascript;
  
    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
         try {
            JspWriter out = pageContext.getOut();
            HttpSession session = pageContext.getSession();
            ServletRequest request = pageContext.getRequest();
            out.println("<td colspan=\"2\"><table width=\"300\" align=\"left\">");
            out.println("<tr><td align=\"left\"><label class=\"header\">"+ displayName +"</label></br></br></td></tr>");
            out.println("<tr align=\"left\" valign=\"top\"><td>");
            String selectedBox;
            
            if(request.getParameter("callQueries") != null){
                selectedBox = "" + request.getParameter("callQueries");
            }else {
                 selectedBox = "" + session.getAttribute("callQueries2");
            }
                
            ArrayList<LookupValue> lookUps = (ArrayList<LookupValue>) NeoCommand.service.getNeoManagerBeanPort().getCodeTable(new Integer(lookupId));
            for (int i = 0; i < lookUps.size(); i++) {
                LookupValue lookupValue = lookUps.get(i);

                StringTokenizer str = new StringTokenizer(selectedBox, ",");
               // System.out.println("The lookup id is " + lookupValue.getId() + " and the lookup id is " + lookupId);
                boolean found = false;
                while (str.hasMoreTokens())
                {
                   if (lookupValue.getId().toString().equalsIgnoreCase(str.nextToken()))
                        found = true;
                }


                if (found) {
                    out.print("<div><input type=\"checkbox\" name=\"" + elementName + "_" + i + "\" value=\"" + lookupValue.getId() + "\" checked><label>" + lookupValue.getValue() + "</label></input></div>");
                }
                else {                   
                    out.print("<div><input type=\"checkbox\" name=\"" + elementName + "_" + i + "\" value=\"" + lookupValue.getId() + "\" ><label>" + lookupValue.getValue() + "</label></div>");                  
                }

            }
            
            out.println("</td><tr></table></td>");
        } catch (IOException ex) {
            Logger.getLogger(QueriesCheckBoxTag.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        return super.doEndTag();
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setNumberPerRow(String numberPerRow) {
        this.numberPerRow = numberPerRow;
        
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

}

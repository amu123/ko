/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthCPTDetails;
import neo.manager.AuthTariffDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class ReturnAuthTariffCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        //get if cpt code must be mapped for tariff code(s)
        boolean check = false;
        if (session.getAttribute("mapCPTForTariff") != null) {
            check = (Boolean) session.getAttribute("mapCPTForTariff"); //authTariffButton
        }
        String searchCalled = "" + session.getAttribute("searchCalled");
        double copay = 0.0d;
        if (check) {
            List<AuthTariffDetails> tList = (List<AuthTariffDetails>) session.getAttribute("tariffListArray");
            if (searchCalled != null && !searchCalled.equalsIgnoreCase("") && searchCalled.equalsIgnoreCase("authTariffButton")) {
                if (tList != null && tList.size() != 0) {
                    String completed = (String) new GetAuthCPTMappingForTariff().execute(request, response, context);
                    System.out.println("ReturnAuthTariffCommand completed = " + completed);
                    if (completed.equalsIgnoreCase("done")) {
                        //set co payment for cpt

                        List<AuthCPTDetails> cptList = (List<AuthCPTDetails>) session.getAttribute("authCPTListDetails");

                        String admissionD = "" + session.getAttribute("admissionDateTime");
                        String authType = "" + session.getAttribute("authType");
                        Date admissionDate = null;
                        XMLGregorianCalendar authStart = null;
                        try {
                            admissionDate = new SimpleDateFormat("yyyy/MM/dd").parse(admissionD);
                            authStart = DateTimeUtils.convertDateToXMLGregorianCalendar(admissionDate);
                        } catch (ParseException ex) {
                            ex.printStackTrace();
                        }
                        //get auth member option id
                        int optionId = Integer.parseInt("" + session.getAttribute("schemeOption"));
                        System.out.println("ReturnAuthTariffCommand member option id for copay mapping = " + optionId);

                        copay = port.getCoPaymentAmountForCPT(cptList, optionId, authStart);
                        System.out.println("co payment amount = " + copay);
                        session.setAttribute("coPay_error", null);
                        if (copay != 99999.0d && copay != 0.0d) {
                            session.setAttribute("coPay_error", null);

                        } else if (copay == 99999.0d) {
                            session.setAttribute("coPay_error",
                                    "*Note: Procedure Excluded Unless PMB");
                        }

                        double dspCopay = 3675.0d;
                        boolean cptEmergency = false;
                        if (session.getAttribute("cptEmergencyCase") != null) {
                            cptEmergency = (Boolean) session.getAttribute("cptEmergencyCase");
                        }

                        String product = "" + session.getAttribute("scheme");
                        String option = "" + session.getAttribute("schemeOption");
                        int prod = Integer.parseInt(product);
                        int opt = Integer.parseInt(option);
                        if ((option.equals("5") || option.equals("7")) && !cptEmergency) {
                            //check provider 
                            String facilityProv = "" + session.getAttribute("facilityProv_text");
                            boolean noProvSet = false;
                            boolean isDSP = false;

                            if (facilityProv == null || facilityProv.equals("null") || facilityProv.equalsIgnoreCase("")) {
                                noProvSet = true;
                            }
                            System.out.println("facilityProv = " + facilityProv);
                            System.out.println("noProvSet = " + noProvSet);
                            
                            String facilityProvNetwork = "" + session.getAttribute("facilityProvNetwork");
                            boolean provincialHosp = false;
                            if(facilityProvNetwork.equalsIgnoreCase("Provincial Hospitals L1") 
                                    || facilityProvNetwork.equalsIgnoreCase("Provincial Hospitals L2") 
                                    || facilityProvNetwork.equalsIgnoreCase("Provincial Hospitals L3")){
                                provincialHosp = true;
                            }

                            if (!noProvSet && !provincialHosp) {
                                isDSP = port.isDSPNetwork(facilityProv, authStart, prod, opt);
                                if (copay != 0.0d && copay != 99999.0d && !isDSP) {
                                    session.setAttribute("coPay_error",
                                            "*Note: Normal Copay of R" + DateTimeUtils.decimalFormat.format(copay) + " "
                                            + "applies with an addtional R3675 for Non-DSP");
                                    copay += dspCopay;

                                } else if (copay == 0.0d && !isDSP) {
                                    session.setAttribute("coPay_error",
                                            "*Note: An addtional R3675 for Non-DSP Applies on Facility " + facilityProv);
                                    copay += dspCopay;

                                } else if (copay == 99999.0d && !isDSP) {
                                    session.setAttribute("coPay_error",
                                            "*Note: Procedure Excluded Unless PMB R" + DateTimeUtils.decimalFormat.format(copay) + " "
                                            + "applies with an addtional R3675 for Non-DSP");
                                    copay += dspCopay;

                                }

                                if (!isDSP) {
                                    session.setAttribute("dspCopayApplies", "yes");
                                }
                            } else {
                                session.setAttribute("dspCopayApplies", "no");
                            }
                        } else {
                            if (copay == 99999.0d) {
                                session.setAttribute("coPay_error",
                                        "*Note: Procedure Excluded Unless PMB R" + DateTimeUtils.decimalFormat.format(copay) + " ");
                            }
                            session.setAttribute("dspCopayApplies", "no");
                        }
                        //TARIFF COPAYMENTS 
                        if (authType.equals("9")) {
                            copay = port.getTariffCoPaymentAmountForTariffs(tList, optionId, authStart);
                            System.out.println("specialized radiology co payment amount = " + copay);
                            session.setAttribute("coPay", copay);
                        }

                        try {
                            String nextJSP = "" + session.getAttribute("onScreen");
                            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                            dispatcher.forward(request, response);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        System.out.println("co payment before return amount = " + copay);
                        session.setAttribute("coPay", copay);
                    }
                } else {
                    session.setAttribute("authCPTListDetails", null);
                }
            }
        }

        return null;
    }

    @Override
    public String getName() {
        return "ReturnAuthTariffCommand";
    }
}

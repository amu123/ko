/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author princes
 */
public class ViewRatePatientCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        System.out.println("entered ViewRatePatientCommand");
        String riskDate;
        String nextJSP;
        Format formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date today = new Date();

        String member = session.getAttribute("activeMember") + "";

        String manualFlag = "" + request.getParameter("manualRatingFlag");
        
        if(manualFlag != null && manualFlag.equalsIgnoreCase("true")){
            String covNo = "" + request.getParameter("coverNumber");
            String depNo = "" + request.getParameter("dependentNumber");
            String patName = "" + request.getParameter("patName");
            String patSurname = "" + request.getParameter("patSurname");

            System.out.println("VALUES FOR MANUAL RISK RATE: " + covNo + "|" + depNo + "|" + patName + "|" + patSurname);
        
            session.setAttribute("policyNumber", covNo);
            session.setAttribute("dependentNumber", depNo);
            session.setAttribute("patientName", patName);
            session.setAttribute("surname", patSurname);
        }else{
            manualFlag = "false";
        }

        riskDate = formatter.format(today);

        session.setAttribute("riskDate", riskDate);
        System.out.println("manual flag ==== " + manualFlag);
        if (member != null && member.trim().equals("") && manualFlag.equalsIgnoreCase("false")) {
            System.out.println("setting it false");
            nextJSP = "/PDC/RiskRatePatient.jsp";
            session.setAttribute("pdcManualFlag",false);
        } else {
            System.out.println("setting it true");
            nextJSP = "/PDC/RiskRatePatient.jsp";
            session.setAttribute("pdcManualFlag",true);
        }

        try {

            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewRatePatientCommand";
    }
}

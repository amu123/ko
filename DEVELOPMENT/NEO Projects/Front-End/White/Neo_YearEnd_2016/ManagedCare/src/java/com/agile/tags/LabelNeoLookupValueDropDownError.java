/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.LookupValue;
import org.apache.log4j.Logger;

/**
 *
 * @author johanl
 */
public class LabelNeoLookupValueDropDownError extends TagSupport {

    private Logger logger = Logger.getLogger(this.getClass());
    private String displayName;
    private String elementName;
    private String lookupId;
    private String javaScript;
    private String mandatory = "no";
    private String errorValueFromSession = "no";
    private String disable = "no";


    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        try {
            if (getDisable().equalsIgnoreCase("yes")) {
                setDisable("disabled");
            } else {
                setDisable("");
            }
            out.println("<td><label class=\"label\">" + displayName + ":</label></td>");
            out.println("<td><select style=\"width:215px\" name=\"" + elementName + "\" id=\"" + elementName + "\" " + getDisable());
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");
            String selectedBox = "" + session.getAttribute(elementName);
            
            //Neo Lookups
            List<neo.manager.LookupValue> lookUps;
            
            //Alphabetically sort AuthowiseWise dropdown
            /********************************************************************************/
            if(lookupId.equalsIgnoreCase("168")){
                List<neo.manager.LookupValue> alphaSort = new ArrayList<neo.manager.LookupValue>();
                alphaSort = NeoCommand.service.getNeoManagerBeanPort().getCodeTable(168);

                Collections.sort(alphaSort, new Comparator<LookupValue>() {
                    public int compare(LookupValue o1, LookupValue o2) {
                        if (o1.getId() == null || o2.getId() == null) {
                            return 0;
                        }
                        return o1.getValue().compareTo(o2.getValue());
                    }
                });

                lookUps = alphaSort;
            }else{
                lookUps = NeoCommand.service.getNeoManagerBeanPort().getCodeTable(new Integer(lookupId));
            }
            /********************************************************************************/
            
            out.println("<option value=\"99\"></option>");
            for (int i = 0; i < lookUps.size(); i++) {
                LookupValue lookupValue = lookUps.get(i);
                if (lookupValue.getId().equalsIgnoreCase(selectedBox)) {
                    out.println("<option value=\"" + lookupValue.getId() + "\" selected >" + lookupValue.getValue() + "</option>");
                } else {
                    out.println("<option value=\"" + lookupValue.getId() + "\" >" + lookupValue.getValue() + "</option>");
                }
            }
            out.println("</select></td>");
            if (mandatory.equalsIgnoreCase("yes")) {
                out.println("<td><label class=\"red\">*</label></td>");
            } else {
                out.println("<td></td>");
            }

            String sessionValerror = "";
            if (errorValueFromSession.equalsIgnoreCase("yes")) {

                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                }

                if (sessionValerror != null && !sessionValerror.equalsIgnoreCase("null")) {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
                } else {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
                }
            }

            /*
             if(errorValueFromSession.equalsIgnoreCase("yes")){
             String errorVal = "" + session.getAttribute(elementName + "_error");
             if(errorVal == null || errorVal.trim().equalsIgnoreCase("null")){
             errorVal = "";
             }
             out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">"+ errorVal +"</label></td>");
             }else{
             out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
             }*/

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelNeoLookupValueDropDown tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setErrorValueFromSession(String errorValueFromSession) {
        this.errorValueFromSession = errorValueFromSession;
    }

    /**
     * @return the disable
     */
    public String getDisable() {
        return disable;
    }

    /**
     * @param disable the disable to set
     */
    public void setDisable(String disable) {
        this.disable = disable;
    }
}

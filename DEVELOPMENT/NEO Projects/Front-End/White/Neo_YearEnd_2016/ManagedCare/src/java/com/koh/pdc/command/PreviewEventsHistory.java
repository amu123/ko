/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.PreAuthDetails;
import neo.manager.PdcSearchCriteria;

/**
 *
 * @author princes
 */
public class PreviewEventsHistory extends NeoCommand {

    private Object _datatypeFactory;
    private int minResult;
    private int maxResult = 100;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        System.out.println("entered PreviewEventsHistory");
        String coverNumber = (String) session.getAttribute("policyNumber");
        String pageDirection = request.getParameter("pageDirection");
        String dateTo = request.getParameter("dateTo");
        String dateFrom = request.getParameter("dateFrom");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Collection<PreAuthDetails> list = (Collection<PreAuthDetails>) session.getAttribute("authDetails");
        String depenNo = (String) session.getAttribute("dependantNumber").toString();

        System.out.println("Date from : " + dateFrom + " Date to : " + dateTo);

        PdcSearchCriteria search = new PdcSearchCriteria();

        if ((dateFrom != null && !dateFrom.equalsIgnoreCase("")) && (dateTo != null && !dateTo.equalsIgnoreCase(""))) {

            Date tFrom = null;
            Date tTo = null;
            XMLGregorianCalendar xTreatFrom = null;
            XMLGregorianCalendar xTreatTo = null;

            try {
                tFrom = dateFormat.parse(dateFrom);
                tTo = dateFormat.parse(dateTo);

                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                xTreatTo = DateTimeUtils.convertDateToXMLGregorianCalendar(tTo);

                search.setDateFrom(xTreatFrom);
                search.setDateTo(xTreatTo);

            } catch (java.text.ParseException ex) {
                Logger.getLogger(PreviewAuthCommand.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        if (pageDirection != null && pageDirection.equalsIgnoreCase("forward")) {

            minResult = minResult + 100;
            maxResult = maxResult + 100;
            search.setMinResult(minResult);
            search.setMaxResult(maxResult);
        }

        if (pageDirection != null && pageDirection.equalsIgnoreCase("backward")) {

            minResult = minResult - 100;
            maxResult = maxResult - 100;
            search.setMaxResult(maxResult);
            search.setMinResult(minResult);

        }

        search.setCoverNumber(coverNumber);


        /*for(PreAuthDetails detail: details){
            System.out.println("-----------------------------------------------");
            System.out.println("getAuthType " + detail.getAuthType());
            System.out.println("getAuthNumber " + detail.getAuthNumber());
            System.out.println("getCoverNumber " + detail.getCoverNumber());
            System.out.println("getFunderDetails " + detail.getFunderDetails());
            System.out.println("getDependantCode " + detail.getDependantCode());

            authType.put(new Integer(detail.getAuthType()), new Integer(detail.getAuthType()));



        session.setAttribute("authList", authType);
        session.setAttribute("authDetails", details);
        }*/
        try {
            String nextJSP = "/PDC/AuthorizationDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private DatatypeFactory getDatatypeFactory() throws DatatypeConfigurationException {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }

        return (DatatypeFactory) _datatypeFactory;
    }

    @Override
    public String getName() {
        return "PreviewEventsHistory";
    }
}

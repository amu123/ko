/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.dto;

import java.util.List;

/**
 *
 * @author marcelp
 */
public class ClaimLineObject {

    private String dependantCode;
    private String treatmentDate;
    private String icdCode;
    private String icdDescription;
    private String tariffCode;
    private String tariffDescription;
    private String tariffAmount;
    private String nappiCode;
    private String nappiDescription;
    private String claimedAmount;
    private String paidAmount;
    private String messageCode;
    private String copayment;
    private String processDate;
    private String operator;
    private String threshold;
    private String benefitDesc;
    private String toothNumber;
    private String overPaidAmount;
    private double units;
    private boolean isPractice = false;
    //rejected
    private List<ClaimLineRuleMessage> lstClaimLineRuleMessages;
    //reversal
    private String reversalCode;
    private String reversalDesc;
    //modifier
    private List<ClaimLineModifier> lstClaimLineModifiers;
    //override
    private List<ClaimLineOverr> lstClaimLineOverride;
    //Pre-authNumber
    private String preAuthNumbers;

    public boolean isIsPractice() {
        return isPractice;
    }

    public void setIsPractice(boolean isPractice) {
        this.isPractice = isPractice;
    }

    public String getOverPaidAmount() {
        return overPaidAmount;
    }

    public void setOverPaidAmount(String overPaidAmount) {
        this.overPaidAmount = overPaidAmount;
    }

    public String getIcdDescription() {
        return icdDescription;
    }

    public void setIcdDescription(String icdDescription) {
        this.icdDescription = icdDescription;
    }

    public String getToothNumber() {
        return toothNumber;
    }

    public void setToothNumber(String toothNumber) {
        this.toothNumber = toothNumber;
    }

    public String getPreAuthNumbers() {
        return preAuthNumbers;
    }

    public String getBenefitDesc() {
        return benefitDesc;
    }

    public void setBenefitDesc(String benefitDesc) {
        this.benefitDesc = benefitDesc;
    }

    public String getThreshold() {
        return threshold;
    }

    public void setThreshold(String threshold) {
        this.threshold = threshold;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public void setPreAuthNumbers(String lstPreauthNumber) {
        this.preAuthNumbers = lstPreauthNumber;
    }

    public List<ClaimLineOverr> getLstClaimLineOverride() {
        return lstClaimLineOverride;
    }

    public void setLstClaimLineOverride(List<ClaimLineOverr> lstClaimLineOverride) {
        this.lstClaimLineOverride = lstClaimLineOverride;
    }

    public String getProcessDate() {
        return processDate;
    }

    public void setProcessDate(String processDate) {
        this.processDate = processDate;
    }

    public String getCopayment() {
        return copayment;
    }

    public void setCopayment(String copayment) {
        this.copayment = copayment;
    }

    public List<ClaimLineModifier> getLstClaimLineModifiers() {
        return lstClaimLineModifiers;
    }

    public void setLstClaimLineModifiers(List<ClaimLineModifier> lstClaimLineModifiers) {
        this.lstClaimLineModifiers = lstClaimLineModifiers;
    }

    public List<ClaimLineRuleMessage> getLstClaimLineRuleMessages() {
        return lstClaimLineRuleMessages;
    }

    public void setLstClaimLineRuleMessages(List<ClaimLineRuleMessage> lstClaimLineRuleMessages) {
        this.lstClaimLineRuleMessages = lstClaimLineRuleMessages;
    }

    public String getReversalCode() {
        return reversalCode;
    }

    public void setReversalCode(String reversalCode) {
        this.reversalCode = reversalCode;
    }

    public String getReversalDesc() {
        return reversalDesc;
    }

    public void setReversalDesc(String reversalDesc) {
        this.reversalDesc = reversalDesc;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getClaimedAmount() {
        return claimedAmount;
    }

    public void setClaimedAmount(String claimedAmount) {
        this.claimedAmount = claimedAmount;
    }

    public String getDependantCode() {
        return dependantCode;
    }

    public void setDependantCode(String dependantCode) {
        this.dependantCode = dependantCode;
    }

    public String getIcdCode() {
        return icdCode;
    }

    public void setIcdCode(String icdCode) {
        this.icdCode = icdCode;
    }

    public String getNappiCode() {
        return nappiCode;
    }

    public void setNappiCode(String nappiCode) {
        this.nappiCode = nappiCode;
    }

    public String getNappiDescription() {
        return nappiDescription;
    }

    public void setNappiDescription(String nappiDescription) {
        this.nappiDescription = nappiDescription;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getTariffAmount() {
        return tariffAmount;
    }

    public void setTariffAmount(String tariffAmount) {
        this.tariffAmount = tariffAmount;
    }

    public String getTariffCode() {
        return tariffCode;
    }

    public void setTariffCode(String tariffCode) {
        this.tariffCode = tariffCode;
    }

    public String getTariffDescription() {
        return tariffDescription;
    }

    public void setTariffDescription(String tariffDescription) {
        this.tariffDescription = tariffDescription;
    }

    public String getTreatmentDate() {
        return treatmentDate;
    }

    public void setTreatmentDate(String treatmentDate) {
        this.treatmentDate = treatmentDate;
    }
    
    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }
}

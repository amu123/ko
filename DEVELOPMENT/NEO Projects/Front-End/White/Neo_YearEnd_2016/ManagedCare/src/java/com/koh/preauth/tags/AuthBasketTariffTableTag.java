/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author johanl
 */
public class AuthBasketTariffTableTag extends TagSupport {
    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            out.println("<td colspan=\"7\"><table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Discipline</th><th>Tariff Code</th><th>Tariff Desc</th><th>Frequency</th><th></th><th></th></tr>");

            ArrayList<AuthTariffDetails> basketList = (ArrayList<AuthTariffDetails>) session.getAttribute(sessionAttribute);
            if (basketList != null) {
                for (int i = 1; i <= basketList.size(); i++) {
                    AuthTariffDetails ab = basketList.get(i - 1);
                    String provDesc = ab.getProviderType();

                    if(ab.getSpecificTariffInd() == 1){
                        provDesc = NeoCommand.service.getNeoManagerBeanPort().getValueFromCodeTableForTableId(59, ab.getProviderType());
                    }

                    String hidden = ab.getTariffCode() + "|" + ab.getTariffDesc()+"|"+ab.getFrequency();

                    out.println("<tr id=\"btRow" + hidden + i + "\"><form>");
                    out.println(
                            "<td>" +
                                "<label class=\"label\">" + provDesc + "</label>" +
                                "<input type=\"hidden\" name=\"btHidden\" id=\"btHidden\" value=\"" + hidden + "\"/>" +
                            "</td>" +
                            "<td>" +
                                "<label class=\"label\">" + ab.getTariffCode() + "</label>" +
                            "</td>" +
                            "<td width=\"200\">" +
                                "<label class=\"label\">" + ab.getTariffDesc() + "</label>" +
                            "</td>" +
                            "<td>" +
                                "<label class=\"label\">" + ab.getFrequency() + "</label>" +
                            "</td>" +
                            "<td>" +
                                "<button type=\"button\" onClick=\"ModifyBTFromList(" + i + ");\" >Modify</button>" +
                            "</td>" +
                            "<td>" +
                                "<button type=\"button\" onClick=\"RemoveBTFromList(" + i + ");\" >Remove</button>" +
                            "</td>" +
                            "</form></tr>");
                }
                out.println("</table></td>");
            }
        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberSearchTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

}

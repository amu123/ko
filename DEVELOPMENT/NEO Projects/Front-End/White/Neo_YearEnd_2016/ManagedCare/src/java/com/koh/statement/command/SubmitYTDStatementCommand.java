/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import com.agile.security.webservice.EmailAttachment;
import com.agile.security.webservice.EmailContent;
import com.koh.command.FECommand;
import static com.koh.command.FECommand.service;
import com.koh.serv.PropertiesReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class SubmitYTDStatementCommand extends FECommand {

    private Logger ytdLogger = Logger.getLogger(SubmitYTDStatementCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        boolean memberDir = false;
        boolean emailSent = false;
        String stmtType = (String) request.getParameter("stmtType");
        String actionType = request.getParameter("opperationParam");

        String reportType = "";
        String entityNumber = "";
        String entityType = "";

        if (stmtType.equals("1")) {
            entityNumber = (String) request.getParameter("memberNo_text");
            entityType = "Member";
            reportType = "Member";
            memberDir = true;
        } else {
            entityNumber = (String) request.getParameter("providerNo_text");
            entityType = "Practice";
            reportType = "Practice";
        }

        String productId = request.getParameter("productId");

        String scheme = "Agility";
        String schemeFileName = "Agility";
        if (productId != null && !productId.equals("")) {
            if (productId.equals("1")) {
                scheme = "Resolution Health";
                schemeFileName = "Resolution";
            } else if (productId.equals("2")) {
                scheme = "Spectramed";
                schemeFileName = "Spectramed";
            }
        }

        String emailTo = request.getParameter("emailAddress");
        String fromDate = (String) request.getParameter("stmtFromDate");
        String toDate = (String) request.getParameter("stmtToDate");

        ytdLogger.info("dateFrom : " + fromDate);
        ytdLogger.info("dateTo : " + toDate);
        ytdLogger.info("reportType : " + reportType);
        ytdLogger.info("entityNumber : " + entityNumber);
        ytdLogger.info("entityType : " + entityType);

        InputStream in = null;
        ByteArrayOutputStream tmpOut = null;

        try {

            String server = new PropertiesReader().getProperty("statementURL");
            System.out.println("Server URL: " + server);

            String ytdURL = server + "?"
                    + "fromDate=" + fromDate
                    + "&toDate=" + toDate
                    + "&entityNumber=" + entityNumber
                    + "&entityType=" + entityType;

            ytdLogger.info("ytdURL : " + ytdURL);

            URL url = new URL(ytdURL);
            URLConnection connection = url.openConnection();
            // Since you get a URLConnection, use it to get the InputStream
            in = connection.getInputStream();
            int connectionTimeout = connection.getConnectTimeout();
            ytdLogger.info("Connect timeout : " + connectionTimeout);
            int readTimeout = connection.getReadTimeout();
            ytdLogger.info("Read timeout : " + readTimeout);
            // Now that the InputStream is open, get the content length
            int contentLength = connection.getContentLength();
            ytdLogger.info("Length : " + contentLength);

            // To avoid having to resize the array over and over and over as
            // bytes are written to the array, provide an accurate estimate of
            // the ultimate size of the byte array
            PrintWriter out = null;
            if (contentLength != 20) { //20 no details found

                if (contentLength != -1) {
                    tmpOut = new ByteArrayOutputStream(contentLength);
                } else {
                    tmpOut = new ByteArrayOutputStream(16384); // Pick some appropriate size
                }

                byte[] buf = new byte[512];
                while (true) {
                    int len = in.read(buf);
                    if (len == -1) {
                        break;
                    }
                    tmpOut.write(buf, 0, len);
                }
                in.close();
                tmpOut.close(); // No effect, but good to do anyway to keep the metaphor alive

                byte[] array = tmpOut.toByteArray();

                OutputStream outStream = null;
                try {

                    if (array.length > 0) {

                        // test to see if the file is corrupt
                        ytdLogger.info("actionType is " + actionType);
                        String fileName = schemeFileName + "_YTD_" + entityType + "_Statement_" + entityNumber + ".pdf";

                        if (actionType != null && actionType.equalsIgnoreCase("Preview")) {
                            printPreview(request, response, array, fileName);

                        } else if (emailTo != null && !emailTo.equalsIgnoreCase("")) {
                            String emailFrom = "Noreply@agilityghs.co.za";
                            String subject = scheme;
                            if (memberDir) {
                                subject += " - YTD Statements For Member " + entityNumber;
                            } else {
                                subject += " - YTD Statements For Provider " + entityNumber;

                            }
                            subject += " For Payment Dates between " + fromDate + " and " + toDate;

                            String emailMsg = null;

                            EmailContent content = new EmailContent();
                            content.setContentType("text/plain");
                            EmailAttachment attachment = new EmailAttachment();
                            attachment.setContentType("application/octet-stream");
                            attachment.setName(fileName);
                            content.setFirstAttachment(attachment);
                            content.setFirstAttachmentData(array);
                            content.setSubject(subject);
                            content.setEmailAddressFrom(emailFrom);
                            content.setEmailAddressTo(emailTo);
                            content.setProductId(1);
                            //content.setProductId(new Integer(productId));
                            //Determine msg for email according to type of document ('claim', 'statement', 'preAuth', 'benefit' 'custom', 'other')
                            content.setType("statement");
                            /*If document type is custom, this message added here will be displayed in the email. The email method will automatically add the 
                             the enquiries and call center aswell as the kind regards at the end of the message*/
                            content.setContent(emailMsg);
                            emailSent = service.getAgileManagerPort().sendEmailWithOrWithoutAttachment(content);
                            ytdLogger.info("Email sent : " + emailSent);
                            printEmailResult(request, response, emailSent);
                        } else {
                            printEmailResult(request, response, emailSent);
                        }

                    } else {
                        //NO DATA RETURNED
                        out = response.getWriter();
                        out.println("<html>");
                        out.println("<head>");
                        out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<center>");
                        out.println("<table cellspacing=\"4\">");
                        out.println("<tr>");
                        out.println("<td><label class=\"header\">No Statement Data Found for Payment Dates.</label></td>");
                        out.println("</tr>");
                        out.println("</table>");
                        out.println("<p> </p>");
                        out.println("</center>");
                        out.println("</body>");
                        out.println("</html>");

                        response.setHeader("Refresh", "2; URL=/ManagedCare/Statement/YearToDateStatements.jsp");
                    }
                } catch (IOException ex) {
                    ytdLogger.error(ex);
                } finally {
                    outStream = null;
                    out = null;
                }
            } else {
                //NO DATA RETURNED
                out = response.getWriter();
                out.println("<html>");
                out.println("<head>");
                out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                out.println("</head>");
                out.println("<body>");
                out.println("<center>");
                out.println("<table cellspacing=\"4\">");
                out.println("<tr>");
                out.println("<td><label class=\"header\">No Statement Data Found for Payment Dates.</label></td>");
                out.println("</tr>");
                out.println("</table>");
                out.println("<p> </p>");
                out.println("</center>");
                out.println("</body>");
                out.println("</html>");

                response.setHeader("Refresh", "2; URL=/ManagedCare/Statement/YearToDateStatements.jsp");
            }
        } catch (Exception e) {
            ytdLogger.error(e);
        } finally {
            in = null;
            tmpOut = null;
        }

        return null;

    }

    @Override
    public String getName() {
        return "SubmitYTDStatementCommand";
    }

    public void printPreview(HttpServletRequest request, HttpServletResponse response, byte[] array, String fileName) throws IOException {
        OutputStream outStream = null;
        try {
            response.setHeader("Pragma", "no-cache"); //HTTP 1.0
            response.setDateHeader("Expires", 0); //prevents caching at the proxy server
            response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-disposition", "attachment; filename=" + fileName);

            response.setContentType("application/pdf");
            response.setContentLength(array.length);
            outStream = response.getOutputStream();
            outStream.write(array);
            outStream.flush();
            outStream.close();

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void printResult(HttpServletRequest request, HttpServletResponse response, boolean result) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (result) {
                out.println("<td><label class=\"header\">Statement emailed.</label></td>");
            } else {
                //out.println("<td><label class=\"header\">No payment(s) found.</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    public void printEmailResult(HttpServletRequest request, HttpServletResponse response, boolean result) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (result) {
                out.println("<td><label class=\"header\">Statement emailed.</label></td>");
            } else {
                out.println("<td><label class=\"header\">Email sending failed, Either email address is missing or incorrect.</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "2; URL=/ManagedCare/Statement/YearToDateStatements.jsp");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }
}
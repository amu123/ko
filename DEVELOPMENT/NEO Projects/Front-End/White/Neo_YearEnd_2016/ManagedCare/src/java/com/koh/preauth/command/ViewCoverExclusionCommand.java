/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthCoverExclusions;

/**
 *
 * @author johanl
 */
public class ViewCoverExclusionCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        int memEId = 0;
        String entityId = "" + session.getAttribute("memberEntityId");
        if (entityId != null && !entityId.trim().equalsIgnoreCase("")) {
            memEId = Integer.parseInt(entityId);
        }
        if (memEId != 0) {
            @SuppressWarnings("static-access")
            List<AuthCoverExclusions> aceList = service.getNeoManagerBeanPort().getCoverExclusionsByEID(memEId);
            if (aceList != null) {
                System.out.println("aceList in ViewCoverExclusionCommand = " + aceList.size());
                request.setAttribute("AuthExclusionList", aceList);
            }

        }
        try {
            String nextJSP = "/Auth/GenericAuthorisation.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return null;
    }

    @Override
    public String getName() {
        return "ViewCoverExclusionCommand";
    }
}

package com.agile.tags;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class SuccessTag extends TagSupport {

     @Override
    public int doEndTag() throws JspException {
        HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
        try{
            String wfe = "" + request.getAttribute("success");
            if(!wfe.equalsIgnoreCase("") && !wfe.equalsIgnoreCase("null")){
                pageContext.getOut().write("<label class=\"success\">"+request.getAttribute("success")+"</label>");
            }
//            if(request.getAttribute("error") != null){
//                if(request.getAttribute("error").equals("null")){

//                }
//                pageContext.getOut().write("<label class=\"error\">"+request.getAttribute("error")+"</label>");
//            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return super.doEndTag();
    }

}

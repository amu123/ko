/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Antaryami
 */
public class DocumentReIndexFileCommand extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
//        throw new UnsupportedOperationException("Not supported yet.");
        RequestDispatcher dispatcher = null;
        try{

        String fileLocation = (String) request.getParameter("fileLocation");
        String docType = (String) request.getParameter("docType")!=null ?(String) request.getParameter("docType") :"";
        String entityNumber = (String) request.getParameter("entityNumber");
        String entityType = (String) request.getParameter("entityType");
        String refNumber = (String) request.getParameter("refNumber");
        String indexID = (String) request.getParameter("indexID");
        String fileName =((fileLocation!=null) && (!fileLocation.equals("")))? fileLocation.substring(fileLocation.lastIndexOf("/")+1, fileLocation.length()):"";
        if(entityType!=null && entityType.equals("5") && docType!=null && docType.contains("Claim")){
            request.setAttribute("indexEntityNumber", entityNumber);
            request.setAttribute("docType", docType);
            request.setAttribute("indexEntityType", entityType);
            request.setAttribute("indexFileLocation", fileLocation);
            request.setAttribute("fileName", fileName);
            request.setAttribute("indexRefNumber",refNumber);
            request.setAttribute("indexID",indexID);
            dispatcher = request.getRequestDispatcher("/Indexing/ReIndexDocumentClaims.jsp");
            dispatcher.forward(request, response);
        }else if(entityType!=null && entityType.equals("5")){
            request.setAttribute("indexEntityNumber", entityNumber);
            request.setAttribute("docType", docType);
            request.setAttribute("indexEntityType", entityType);
            request.setAttribute("indexFileLocation", fileLocation);
            request.setAttribute("fileName", fileName);
            request.setAttribute("indexID",indexID);
            dispatcher = request.getRequestDispatcher("/Indexing/ReIndexDocumentMember.jsp");
            dispatcher.forward(request, response);
        }else if(entityType!=null && !entityType.equals("")){
            dispatcher = request.getRequestDispatcher("/Indexing/ReIndexDocumentGeneric.jsp");
            dispatcher.forward(request, response);
        }  
        }catch(Exception e){
            e.printStackTrace();
        }
        return "DocumentReIndexFileCommand";
    }

    @Override
    public String getName() {
        return "DocumentReIndexFileCommand";
    }
    
}

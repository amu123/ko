/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application;

import com.koh.employer.command.TabUtils;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import neo.manager.MemAppGenDetail;
import neo.manager.MemAppHospDetail;
import neo.manager.MemberApplication;
import neo.manager.MemberDependantApp;
import neo.manager.NeoUser;

/**
 *
 * @author yuganp
 */
public class MemberAppHospitalDetailsMapping {

    private static final String[][] FIELD_MAPPINGS = {
        {"memberAppNumber","Application Number","no", "int", "MemberApp","ApplicationNumber"},
        {"memberAppHospId","Hospital Id","no", "int", "MemberApp","hospDetailId"},
        {"Hospital_detail_date","Date","yes", "date", "MemberApp","hospDate"},
        {"Hospital_detail_dependent","Dependant","yes", "int", "MemberApp","dependentNumber"},
        {"Hospital_detail_details","Details","yes", "str", "MemberApp","details"},
        {"Hospital_detail_reason","Reason","yes", "str", "MemberApp","reason"},
        {"Hospital_detail_doctor","Doctor","yes", "str", "MemberApp","doctor"},
        {"Hospital_detail_condition","Current Condition","no", "int", "MemberApp","conditionInd"}
    };
    
    private static final Map<String,String[]> FIELD_MAP;

    static {
        HashMap hm = new HashMap<String,String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }

    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
        return errors;
    }

    public static MemAppHospDetail getMemAppHospDetail(HttpServletRequest request,NeoUser neoUser) {
        MemAppHospDetail ma = (MemAppHospDetail)TabUtils.getDTOFromRequest(request, MemAppHospDetail.class, FIELD_MAPPINGS, "MemberApp");
        ma.setSecurityGroupId(neoUser.getSecurityGroupId());
        ma.setLastUpdatedBy(neoUser.getUserId());
        ma.setCreatedBy(neoUser.getUserId());
        return ma;
    }

    public static void setMemAppHospDetail(HttpServletRequest request, MemAppHospDetail ma) {
        TabUtils.setRequestFromDTO(request, ma, FIELD_MAPPINGS, "MemberApp");
    }
    
}

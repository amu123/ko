/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.contribution.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.FileData;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author yuganp
 */
public class GroupBillingStatementCommand extends NeoCommand {

    private String product;
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            String s = request.getParameter("command");
            if (s != null && !s.isEmpty()) {
                System.out.println("command : " + s);
                if (s.equalsIgnoreCase("Search")) {
                    search(request, response, context);
                } else if (s.equalsIgnoreCase("SelectEmployer")) {
                    selectEmployer(request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(ContributionBankStatementCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void search(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        product = request.getParameter("scheme");
        String employerNumber = request.getParameter("employerNumber");
        String employerName = request.getParameter("employerName");
        List<KeyValueArray> kva = port.getEmployerList(employerName,employerNumber,Integer.parseInt(product));
        Object col = MapUtils.getMap(kva);
        request.setAttribute("EmployerSearchResults", col);
        context.getRequestDispatcher("/Contribution/GroupBillingSearchResults.jsp").forward(request, response);
    }
    
    private void selectEmployer(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        int entityId = TabUtils.getIntParam(request, "entityId");
        XMLGregorianCalendar date = TabUtils.getXMLDateParam(request, "billingPeriod");
        if (date == null) {
            date = DateTimeUtils.convertDateToXMLGregorianCalendar(new Date());
        }
        FileData employerGroupBillingStatement = port.getEmployerGroupBillingStatement(entityId, date, request.getParameter("fileFormat"), product);
        if (employerGroupBillingStatement == null || employerGroupBillingStatement.getData() == null) {
            
        } else {
            byte[] data = employerGroupBillingStatement.getData();
            ServletOutputStream os = response.getOutputStream();
            String mimetype = context.getMimeType(employerGroupBillingStatement.getFileName());
            response.setContentType( (mimetype != null) ? mimetype : "application/octet-stream" );
            response.setContentLength(data.length);
            response.setHeader( "Content-Disposition", "attachment; filename=\"" + employerGroupBillingStatement.getFileName() + "\"" );
            os.write(data);
            os.flush();
            os.close();
            
        }
        
    }
    
    
    @Override
    public String getName() {
        return "GroupBillingStatementCommand";
    }

    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.validation;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthCPTDetails;
import neo.manager.AuthHospitalLOC;
import neo.manager.AuthTariffCPT;
import neo.manager.AuthTariffDetails;
import neo.manager.Diagnosis;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Johan-NB
 */
public class ValidateHospitalDetails extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        PrintWriter out = null;
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        String errorResponse = "";
        String error = "";
        int errorCount = 0;
        boolean upFlag = false;

        String awType = "";
        boolean newAuthInd = false;
        String authType = "" + session.getAttribute("authType");
        if (!authType.equalsIgnoreCase("11")) {
            newAuthInd = (Boolean) session.getAttribute("newAuthIndicator");
            if (newAuthInd) {
                awType = "" + session.getAttribute("awTypeNew");
            } else {
                awType = "" + session.getAttribute("awType");
            }
        } else {
            newAuthInd = true;
            awType = "BBC";
        }

        String facility = "" + session.getAttribute("facility");
        String rModel = "" + session.getAttribute("rModel");
        String pmb = "" + session.getAttribute("pmb");
        String isPMB = "" + session.getAttribute("isPMB");
        String foundPMB = "" + session.getAttribute("foundPMB");
        String funder = "" + session.getAttribute("funder");
        String fromScreen = "" + session.getAttribute("fromScreen");
        //coPayment amount
        String copay = "";
        String client = context.getAttribute("Client").toString();
        System.out.println("### client " + client);
        if (!client.equalsIgnoreCase("Sechaba")) {
            copay = "" + session.getAttribute("copay");
        } else {
            copay = "";
            session.setAttribute("copay","");       
        }
        
        String selectPmb = "" + session.getAttribute("pmbSelected");
        String pmbFlag = "0";

        //Date Validation
        PreAuthDateValidation pad = new PreAuthDateValidation();

        String authPeriod = "" + session.getAttribute("authPeriod");
        String authMonthPeriod = "" + session.getAttribute("authMonthPeriod");

        String authFrom = "" + session.getAttribute("admissionDateTime");
        String authTo = "" + session.getAttribute("dischargeDateTime");

        if (authFrom == null || authFrom.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|admissionDateTime:Auth From Date is Mandatory";
        } else {
            String vs = pad.DateValidation(authFrom, authTo, "admissionDateTime", "dischargeDateTime", authType, authPeriod, authMonthPeriod, "no");
            String[] returned = vs.split("\\|");
            boolean validDate = Boolean.parseBoolean(returned[0]);
            if (!validDate) {
                String errorsReturned = vs.substring(vs.indexOf("|"), vs.length());
                System.out.println("from date validation error(s) returned = " + errorsReturned);
                errorCount++;
                error = error + errorsReturned;
            }
        }
        if (authTo == null || authTo.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|dischargeDateTime:Auth To Date is Mandatory";
        } else {
            String vs = pad.DateValidation(authFrom, authTo, "admissionDateTime", "dischargeDateTime", authType, authPeriod, authMonthPeriod, "no");
            String[] returned = vs.split("\\|");
            boolean validDate = Boolean.parseBoolean(returned[0]);
            if (!validDate) {
                String errorsReturned = vs.substring(vs.indexOf("|"), vs.length());
                System.out.println("to date validation error(s) returned = " + errorsReturned);
                errorCount++;
                error = error + errorsReturned;
            }
        }

        //copay check
        if (!copay.equalsIgnoreCase("null") && !copay.equalsIgnoreCase("")) {
            boolean coPayError = false;
            //point check
            boolean firstPoint = copay.contains(".");
            if (firstPoint == true) {
                String[] dotSize = copay.split("\\.");
                if (dotSize.length > 2) {
                    coPayError = true;
                }
            }
            //number validation
            if (coPayError != true) {
                char strChar;
                String strValidChars = "0123456789.";

                for (int i = 0; i < copay.length(); i++) {
                    strChar = copay.charAt(i);
                    if (strValidChars.indexOf(strChar) == -1) {
                        errorCount++;
                        error = error + "|coPay:Incorrect Amount = " + copay;
                        break;
                    }
                }

            } else {
                errorCount++;
                error = error + "|coPay:Amount allows only one .";
            }

        }

        //icd checks
        String secIcd = "" + session.getAttribute("secIcd");
        String comIcd = "" + session.getAttribute("comIcd");

        if (!secIcd.equalsIgnoreCase("null")) {
            String[] icds = secIcd.split(",");
            System.out.println("hosp validation sec icd size = " + icds.length);

            for (String code : icds) {

                if (code == null || code.equals("")) {
                    errorCount++;
                    error = error + "|secondaryICD:Null Values Not Allowed";
                    break;

                } else {
                    boolean checkSpaces = false;
                    checkSpaces = code.contains(" ");
                    Character firstChar = secIcd.charAt(0);
                    boolean checkFirst = Character.isLowerCase(firstChar);

                    if (checkSpaces == true) {
                        errorCount++;
                        error = error + "|secondaryICD:Spaces Between Icds Are Not Allowed";
                        break;

                    } else if (checkFirst == true) {
                        errorCount++;
                        error = error + "|secondaryICD:LowerCase ICD is Not Allowed";
                        break;

                    } else {
                        Diagnosis d = port.getDiagnosisForCode(code);
                        if (d == null) {
                            errorCount++;
                            error = error + "|secondaryICD:No such diagnosis";
                            break;
                        }
                    }
                }
            }
        }

        if (!comIcd.equalsIgnoreCase("null")) {
            String[] cicds = comIcd.split(",");
            System.out.println("hosp validation com icd size = " + cicds.length);
            
            for (String cCode : cicds) {

                if (cCode == null || cCode.equals("")) {
                    errorCount++;
                    error = error + "|coMorbidityICD:Null Values Not Allowed";
                    break;

                } else {
                    boolean checkSpaces = false;
                    checkSpaces = cCode.contains(" ");
                    Character firstChar = comIcd.charAt(0);
                    boolean checkFirst = Character.isLowerCase(firstChar);

                    if (checkSpaces == true) {
                        errorCount++;
                        error = error + "|coMorbidityICD:Spaces Between Icds Are Not Allowed";
                        break;

                    } else if (checkFirst == true) {
                        errorCount++;
                        error = error + "|coMorbidityICD:LowerCase ICD is Not Allowed";
                        break;

                    } else {
                        Diagnosis d = port.getDiagnosisForCode(cCode);
                        if (d == null) {
                            errorCount++;
                            error = error + "|coMorbidityICD:No such diagnosis";
                            break;
                        }
                    }
                }
            }
        }

        if (fromScreen.trim().equalsIgnoreCase("update")) {
            upFlag = true;
        }

        List<AuthCPTDetails> cptList = (List<AuthCPTDetails>) session.getAttribute("authCPTListDetails");
        List<AuthHospitalLOC> locList = (List<AuthHospitalLOC>) session.getAttribute("AuthLocList");

        if (awType == null || awType.trim().equals("99")) {
            errorCount++;
            if (newAuthInd) {
                error = error + "|awTypeNew:Authorwise Type is Mandatory";
            } else {
                error = error + "|awType:Authorwise Type is Mandatory";
            }

        } else {

            if (cptList == null || cptList.size() == 0) {
                errorCount++;
                error = error + "|cptButton:Procedure Detail is Mandatory";
            } else {

                String tariff = "";
                boolean failed = false;
                System.out.println("cptList not null, size = " + cptList.size());
                ArrayList<AuthTariffCPT> tcList = (ArrayList<AuthTariffCPT>) session.getAttribute("mappedListOfCptTariffs");
                if (tcList != null) {
                    System.out.println("tcList not null");
                    String cptCodes = "";
                    for (AuthTariffCPT tc : tcList) {

                        String tariffCode = tc.getTariffCode();
                        System.out.println("hosp cpt validate tariffCode = " + tariffCode);

                        int foundTariff = 0;
                        cptCodes = "";

//                        for (AuthCPTDetails cpt : cptList) {
//                                FIELD NOT USED ANYMORE
//                                String[] codeCount = cpt.getCptTariffCount().split("\\|");
//                                String tCode = codeCount[0];
//                                int count = Integer.parseInt(codeCount[1]);
//                                String cptCode = codeCount[2];
//
//                                if (tCode.equalsIgnoreCase(tariffCode) && count == 2) {
//                                    System.out.println("found tCode");
//                                    cptCodes += cptCode + " ";
//                                    foundTariff++;
//                                    if (foundTariff == 2) {
//                                        System.out.println("error: " + cptCodes);
//                                        failed = true;
//                                        break;
//                                    }
//                                }
//                        }
                        if (failed) {
                            System.out.println("failed true");
                            errorCount++;
                            error = error + "|cptButton:Please select only one of the procedures " + cptCodes;
                            break;
                        }
                    }
                }
            }

            if (facility == null || facility.length() == 0) {
                errorCount++;
                error = error + "|facilityProv:Facility is Mandatory";
            }
        }

        if (locList == null || locList.isEmpty() == true) {
            errorCount++;
            error = error + "|authLOCButton:Level of Care Detail is Mandatory";
        }

        if (rModel == null || rModel.trim().equals("99")) {
            errorCount++;
            error = error + "|reimbursementModel:Reimbursement Model is Mandatory";
        }
        try {
            pmbFlag = port.getPMBFlaged(session.getAttribute("updateAuthNumber").toString());
        } catch (Exception e) {
            System.out.println("pmbFlag is empty");
        }

        if (pmb.equalsIgnoreCase("null") || pmb.trim().equals("99")) {
            errorCount++;
            error = error + "|pmb:PMB is Mandatory";
        }
        System.out.println("selectPmb " + selectPmb);
        System.out.println("### pmb: " + pmb);
        if (foundPMB.equalsIgnoreCase("yes")) {
            if (isPMB.equalsIgnoreCase("no")) {
                if (selectPmb.equalsIgnoreCase("true") || selectPmb.equalsIgnoreCase("false")) {

                } else {
                    errorCount++;
                    error = error + "|pmbButton:PMB detail is Mandatory";
                }
            }
        }
        if (funder == null || funder.trim().equals("99")) {
            errorCount++;
            error = error + "|funder:Funder Detail is Mandatory";
        }

        if (upFlag == true) {
            //authStatus hospStatus;
            String authStatus = "" + session.getAttribute("authStatus");
            String hospStatus = "" + session.getAttribute("hospStatus");

            if (authStatus.equalsIgnoreCase("null") || authStatus.trim().equals("99")) {
                errorCount++;
                error = error + "|authStatus:Authorisation Status is Mandatory";
            }

            if (hospStatus.equalsIgnoreCase("null") || hospStatus.trim().equals("99")) {
                errorCount++;
                error = error + "|hospStatus:Hospital Status is Mandatory";
            }
        }

        double hospInterim = 0.0d;
        String hospAmount = "" + session.getAttribute("hospInterim");

        if (hospAmount != null && !hospAmount.equalsIgnoreCase("null") && !hospAmount.equalsIgnoreCase("")) {
            hospInterim = new Double(hospAmount);
            if (hospInterim == 0.0d) {
                errorCount++;
                error = error + "|hospInterim:Hospital Interim is Mandatory";
            }
        } else {
            errorCount++;
            error = error + "|hospInterim:Hospital Interim is Mandatory";
        }

        String providerType = "";
        if (session.getAttribute("provType") != null) {
            providerType = session.getAttribute("provType").toString();
        } else {
            providerType = session.getAttribute("treatingProviderDiscTypeId").toString();
        }
        if (authType.equalsIgnoreCase("4") && awType.equalsIgnoreCase("PSY")) {
            if (!providerType.equalsIgnoreCase("022") && !providerType.equalsIgnoreCase("086") && !providerType.equalsIgnoreCase("090")) {
                errorCount++;
                error = error + "|funder:Invalid Treating Provider for PSY";
            }
        }

        if (errorCount > 0) {
            errorResponse = "Error" + error;
            System.out.println("### error " + error);
        } else if (errorCount == 0) {
            errorResponse = "Done|";
        }
        System.out.println("### errorResponse " + errorResponse);
        try {
            out = response.getWriter();
            out.println(errorResponse);

        } catch (Exception ex) {
            System.out.println("ValidateHospitalDetails error : " + ex.getMessage());
        } finally {
            out.close();
        }

        return null;
    }

    @Override
    public String getName() {
        return "ValidateHospitalDetails";
    }
}

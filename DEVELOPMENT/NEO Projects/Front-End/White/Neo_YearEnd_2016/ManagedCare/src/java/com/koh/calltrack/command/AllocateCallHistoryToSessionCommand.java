/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.calltrack.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CallTrackDetails;

/**
 *
 * @author johanl
 */
public class AllocateCallHistoryToSessionCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        //System.out.println("entered AlllocateCallHistoryToSessionCommand");
        String callId = "" + request.getParameter("trackId");
        int trackId = 0;
        if (callId != null && !callId.trim().equalsIgnoreCase("")) {
            trackId = Integer.parseInt(callId);
            //System.out.println("trackId = " + trackId);
        }
        if (trackId != 0) {
            CallTrackDetails ct = service.getNeoManagerBeanPort().getCallByTrackID(trackId);
            System.out.println("referenceNumber = " + ct.getCallReference());

            //set log call values
            String callTypeId = "";
            if (ct.getCallType().trim().equalsIgnoreCase("member")) {
                callTypeId = "1";
            } else {
                callTypeId = "2";
            }
            request.setAttribute("callType", callTypeId);
            request.setAttribute("callStatus", ct.getCallStatus());
            request.setAttribute("refUser", ct.getReferedUserName());
            request.setAttribute("memNo", ct.getCoverNumber());
            request.setAttribute("providerNumber", ct.getProviderNumber());
            request.setAttribute("provName", ct.getProviderNameSurname());
            request.setAttribute("discpline", ct.getProviderDiscipline());
            request.setAttribute("callerName", ct.getCallerName());
            request.setAttribute("callerContact", ct.getContactDetails());

            session.setAttribute("HistoryCallTrackDetails", ct);

        }
        try {
            String nextJSP = "/Calltrack/LogNewCall.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "AlllocateCallHistoryToSessionCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.tags;

import com.koh.serv.PropertiesReader;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author Christo
 */
public class DocumentIndexLableText extends TagSupport {

    private String elementName = null;
    private String displayName = null;

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        String listOrWork = (String) req.getAttribute("listOrWork");
        
        boolean editable = false;
        if (listOrWork != null && listOrWork.equals("Work")) {
            editable = true;
        }
        String entityNumber = (String)req.getAttribute("indexEntityNumber")!=null?(String)req.getAttribute("indexEntityNumber"):"";
        String referenceNumber = (String) req.getAttribute("indexRefNumber")!=null?(String) req.getAttribute("indexRefNumber"):"";
        try {
           if(entityNumber!=null && !entityNumber.equals("")){
                out.println("<td align=\"left\" width=\"160px\"><label id=\"displayName\">" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\"  id=\"" + elementName + "\" name=\"" + elementName + "\" size=\"30\" value=\""+entityNumber+"\" /></td>");
                req.removeAttribute("indexEntityNumber");
           }else  if(referenceNumber!=null && !referenceNumber.equals("")){
                out.println("<td align=\"left\" width=\"160px\"><label id=\"displayName\">" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\"  id=\"" + elementName + "\" name=\"" + elementName + "\" size=\"30\" value=\""+referenceNumber+"\" /></td>");
                req.removeAttribute("indexRefNumber");
            }else{
            if (editable) {
                out.println("<td align=\"left\" width=\"160px\"><label id=\"displayName\">" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"\" size=\"30\" /></td>");
            } else {
                out.println("<td align=\"left\" width=\"160px\"><label id=\"displayName\">" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"hidden\"  id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"\" size=\"30\"  disabled=\"disabled\"/></td>");
            }
            }
        } catch (Exception ex) {
            throw new JspException("Error in DocumentIndexLableText tag", ex);
        }
        return super.doEndTag();
    }
}

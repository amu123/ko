/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class LineSeparator extends TagSupport {
    private String align;
    private String width;
    private String color;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        try{
            pageContext.getOut().write("<HR color=\"" + color + "\"");
            if(width != null){
                pageContext.getOut().write("WIDTH=\"" + width + "\"");
            }
            if(align != null){
                pageContext.getOut().write("align=\"" + align + "\"");
            }
            pageContext.getOut().write(">");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in LineSeparator tag", ex);
        }
        return super.doEndTag();
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public void setColor(String color) {
        this.color = color;
    }

}

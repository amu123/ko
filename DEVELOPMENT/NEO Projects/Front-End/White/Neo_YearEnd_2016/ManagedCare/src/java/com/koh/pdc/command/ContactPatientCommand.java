/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.ContactDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.PdcAnswer;

/**
 *
 * @author princes
 */
public class ContactPatientCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        System.out.println("entered ContactPatientCommand");

        String nextJSP = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss");
        String coverNumber = (String) session.getAttribute("policyNumber");
        String reason = (String) request.getParameter("reason");
        String questionnaireDate = null;
        int latestPDCQuestionnaire = 0;
        int reason2 = new Integer(reason);
        HashMap<Integer, String> dates = (HashMap<Integer, String>) session.getAttribute("dates");
        List<PdcAnswer> questionnaires = (List<PdcAnswer>) session.getAttribute("dropDownDates");

        NeoManagerBean port = service.getNeoManagerBeanPort();

        List<LookupValue> lookUps = port.getCodeTable(129);

        session.removeAttribute("fromPDC");
        
        if ((reason2) != 9) {

            session.setAttribute("caseReason", lookUps.get(reason2 - 1).getValue());

            Integer entity = (Integer) session.getAttribute("entityId");
            int entityId = entity;

            ArrayList<ContactDetails>contact = (ArrayList<ContactDetails>) port.getAllContactDetails(entityId);

            for(ContactDetails cd: contact){
            if (cd.getCommunicationMethod().equals("E-mail")){
                session.setAttribute("email", cd.getMethodDetails());
            }else if(cd.getCommunicationMethod().equals("Fax")){
                session.setAttribute("faxNumber", cd.getMethodDetails());
            }else if (cd.getCommunicationMethod().equals("Cell")){
                session.setAttribute("cellNumber", cd.getMethodDetails());
            }else if(cd.getCommunicationMethod().equals("Tel (work)")){
                session.setAttribute("workNumber", cd.getMethodDetails());
            }else if(cd.getCommunicationMethod().equals("Tel (home)")){
                session.setAttribute("homeNumber", cd.getMethodDetails());
            }

        }

            if (questionnaires != null) {
                nextJSP = "/PDC/CaseManagement.jsp";
                System.out.println("questionnaires.size() " + questionnaires.size() + " Dates " + dates);
                if (dates != null) {
                    questionnaireDate = request.getParameter("pdcDates");

                    //String date = dates.get(questionnaireDate);

                    System.out.println("Parameter date " + questionnaireDate);
                    Date aDate = new Date();
                    try {
                        aDate = sdf.parse(questionnaireDate);
                    } catch (ParseException ex) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                    }



                    Integer depe = (Integer) session.getAttribute("dependantNumber");
                    int dependentNumber = depe;

                    //Date fromDate = DateTimeUtils.convertFromYYYYMMDD(date);
                    //Date fromDate = sdf.format(date);

                    XMLGregorianCalendar xmlFromDate = DateTimeUtils.convertDateToXMLGregorianCalendar(aDate);
                    //System.out.println("Date sent to the database " + xmlFromDate);
                    PdcAnswer input = new PdcAnswer();
                    input.setCoverNumber(coverNumber);
                    input.setDependentNumber(dependentNumber);
                    input.setAnsweredDate(xmlFromDate);

                    PdcAnswer answer = port.getAnsweredQuestions(input, reason);
                    PdcAnswer newAnwser = port.getTheLatestQuestionnaireForPatient(input, reason2);
                    latestPDCQuestionnaire = port.getLastestQuestionnaireForPDC(reason2);
                    PdcAnswer getAnsered = port.getAnsweredQuestions(newAnwser, reason);




                    //System.out.println("The state of the questionnaire is: " + getAnsered.getState().getId());
                    session.setAttribute("answers", answer);
                    session.setAttribute("newAnswers", getAnsered);
                    session.setAttribute("qDate", questionnaireDate);
                    session.setAttribute("displayed", "current");
                    session.setAttribute("diff", "no");
                    session.setAttribute("reason", reason);
                    if (latestPDCQuestionnaire != newAnwser.getQuestionnaireId()) {
                        session.setAttribute("tapView", "double");
                        session.setAttribute("qaction", "create");
                        session.setAttribute("diff", "yes");
                        session.setAttribute("questionnaireId", latestPDCQuestionnaire);
                    // System.out.println("Questionnaire are not the same");

                    } else {
                        session.setAttribute("questionnaireId", newAnwser.getQuestionnaireId());
                        if (questionnaires.size() == 1) {
                            if (getAnsered.getState().getId().equals("1")) {
                                //System.out.println("SINGLE VIEW and SAve");
                                session.setAttribute("tapView", "single");
                                session.setAttribute("qaction", "save");
                            } else {
                                // System.out.println("Double VIEW and create");
                                session.setAttribute("tapView", "double");
                                session.setAttribute("qaction", "create");
                            }
                        } else if (questionnaires.size() > 1) {
                            session.setAttribute("tapView", "double");
                            if (getAnsered.getState().getId().equals("1")) {
                                session.setAttribute("qaction", "save");
                            } else {
                                session.setAttribute("qaction", "create");
                            }
                        }
                    }
                } else {
                    latestPDCQuestionnaire = port.getLastestQuestionnaireForPDC(reason2);
                    //latestPDCQuestionnaire = port.getLastestQuestionnaireForPDC(5);
                    //System.out.println("latestPDCQuestionnaire :" + latestPDCQuestionnaire);
                    session.setAttribute("reason", reason);
                    //session.setAttribute("reason", "5");
                    session.setAttribute("tapView", "double");
                    session.setAttribute("qaction", "create");
                    session.setAttribute("questionnaireId", latestPDCQuestionnaire);
                    session.setAttribute("diff", "no");
                }

            } else {
                nextJSP = "/PDC/SelectQuestionnaire.jsp";
            }
        } else {
            nextJSP = "/biometrics/CaptureGeneral.jsp";
            session.setAttribute("fromPDC", true);
        }
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ContactPatientCommand";
    }
}

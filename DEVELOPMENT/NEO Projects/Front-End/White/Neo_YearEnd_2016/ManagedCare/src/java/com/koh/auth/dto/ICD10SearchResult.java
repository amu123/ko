/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.dto;

/**
 *
 * @author gerritj
 */
public class ICD10SearchResult {

    private String code;
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.tags;

import com.koh.command.NeoCommand;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.NeoManagerBean;
import neo.manager.ProviderNetworkDetails;

/**
 *
 * @author josephm
 */
public class PracticeDetailsGrid extends TagSupport {
    private String javaScript;
    private String commandName;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest request = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        
        Collection<ProviderNetworkDetails> netList = (Collection<ProviderNetworkDetails>) session.getAttribute("networkHistory");
        
        ArrayList listActiveNetworks = new ArrayList();
        ArrayList listInactiveNetworks = new ArrayList();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        
        try {
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.println("<tr><th colspan=\"6\" align=\"center\">Active Networks</th></tr>"
                            + "<tr height=\"16px\"><td colspan=\"6\" align=\"center\"></td></tr>");
                out.println("<tr>"
                        + "<th align=\"left\">Scheme</th>"
                        + "<th align=\"left\">Network Indicator</th>"
                        + "<th align=\"left\">Effective Start Date</th>"
                        + "<th align=\"left\">Termination Date</th>"
                        + "<th align=\"left\">Process Date</th>"
                        + "<th align=\"left\">Details</th>"
                        + "</tr>");
            int recordCount = 0;  //RecordCount is used to make the Tag IDs unique | and to check if there's records
            for(ProviderNetworkDetails details: netList){   
                 
                String effectiveStartDate = "";
                String terminationDate = "";
                String processDate = "";

                if(details.getEffectiveStartDate() != null) {

                    effectiveStartDate = dateFormat.format(details.getEffectiveStartDate().toGregorianCalendar().getTime());
                }

                if(details.getTerminationDate() != null && dateFormat.format(details.getTerminationDate().toGregorianCalendar().getTime()).equalsIgnoreCase("2999/12/31")) {

                    terminationDate = "";
                }else {

                    terminationDate = dateFormat.format(details.getTerminationDate().toGregorianCalendar().getTime());
                }

                if(details.getProcessDate() != null) {

                    processDate = dateFormat.format(details.getProcessDate().toGregorianCalendar().getTime());
                }
                
                if(details.getEffectiveStartDate() != null && details.getTerminationDate() != null) {
                    //Load Comparison
                    GregorianCalendar today = new GregorianCalendar();
                    Calendar cal = Calendar.getInstance();

                    cal.setTime(today.getTime());
                    int currentYear = cal.get(Calendar.YEAR);

                    cal.setTime(details.getEffectiveStartDate().toGregorianCalendar().getTime());
                    int startYear = cal.get(Calendar.YEAR);

                    cal.setTime(details.getTerminationDate().toGregorianCalendar().getTime());
                    int endYear = cal.get(Calendar.YEAR);

                    if ((startYear <= currentYear && endYear >= currentYear) && endYear == 2999) {
                        if(!listActiveNetworks.contains(details.getProductName() + "-" + details.getNetworkDescription())){
                            recordCount++;
                            out.println("<tr>"
                                        + "<td><label id=\"" + "lbl" + commandName+"_"+ recordCount + "\" class=\"label\">" + details.getProductName()+ "</label></td>"
                                        + "<td><label class=\"label\">" + details.getNetworkDescription() + "</label></td>"
                                        + "<td><label class=\"label\">" + effectiveStartDate + "</label></td>"
                                        + "<td><label class=\"label\">" + terminationDate + "</label></td>"
                                        + "<td><label class=\"label\">" + processDate + "</label></td>"
                                        + "<td align=\"center\"><button name=\"" + "btn" + commandName+"_"+ recordCount + "\" id=\"" + "btn" + commandName+"_"+ recordCount + "\" type=\"button\" " + javaScript + " value=\"" + details.getNetworkDescription() + "\">Details</button></td>"
                                       + "</tr>");
                            listActiveNetworks.add(details.getProductName() + "-" + details.getNetworkDescription());
                        }
                    }
                }
                
             }
            if (recordCount == 0) {
                out.println("<tr>"
                            + "<td><label id=\"" + "lbl" + commandName+"_"+ recordCount + "\" class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                            + "<td><label class=\"label\">&nbsp;</label></td>"
                           + "</tr>");
            }
            
            out.println("</table>");
            
            //------------------INACTIVE NETWORKS------------------//
            String inactiveNetworkFlag = String.valueOf(session.getAttribute("Show_InactiveNetworkIndicatorGrid"));
            if(inactiveNetworkFlag != null && inactiveNetworkFlag.equalsIgnoreCase("true")){
                out.println("<br/><br/>");

                out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                    out.println("<tr><th colspan=\"6\" align=\"center\">Inactive Networks</th></tr>"
                                + "<tr height=\"16px\"><td colspan=\"6\" align=\"center\"></td></tr>");
                    out.println("<tr>"
                                + "<th align=\"left\">Scheme</th>"
                                + "<th align=\"left\">Network Indicator</th>"
                                + "<th align=\"left\">Effective Start Date</th>"
                                + "<th align=\"left\">Termination Date</th>"
                                + "<th align=\"left\">Process Date</th>"
                                + "<th align=\"left\">Details</th>"
                            + "</tr>");
                int compareRecordCount = recordCount;  //See if there's any expired records
                for(ProviderNetworkDetails details: netList){   

                    String effectiveStartDate = "";
                    String terminationDate = "";
                    String processDate = "";

                    if(details.getEffectiveStartDate() != null) {

                        effectiveStartDate = dateFormat.format(details.getEffectiveStartDate().toGregorianCalendar().getTime());
                    }

                    if(details.getTerminationDate() != null && dateFormat.format(details.getTerminationDate().toGregorianCalendar().getTime()).equalsIgnoreCase("2999/12/31")) {

                        terminationDate = "";
                    }else {

                        terminationDate = dateFormat.format(details.getTerminationDate().toGregorianCalendar().getTime());
                    }

                    if(details.getProcessDate() != null) {

                        processDate = dateFormat.format(details.getProcessDate().toGregorianCalendar().getTime());
                    }

                    if(details.getEffectiveStartDate() != null && details.getTerminationDate() != null) {
                        //Load Comparison
                        GregorianCalendar today = new GregorianCalendar();
                        Calendar cal = Calendar.getInstance();

                        cal.setTime(today.getTime());
                        int currentYear = cal.get(Calendar.YEAR);

                        cal.setTime(details.getEffectiveStartDate().toGregorianCalendar().getTime());
                        int startYear = cal.get(Calendar.YEAR);

                        cal.setTime(details.getTerminationDate().toGregorianCalendar().getTime());
                        int endYear = cal.get(Calendar.YEAR);

                        if ((startYear > currentYear || endYear < currentYear) || endYear != 2999) {
                            if(!listActiveNetworks.contains(details.getProductName() + "-" + details.getNetworkDescription()) &&
                            !listInactiveNetworks.contains(details.getProductName() + "-" + details.getNetworkDescription())){
                                recordCount++;
                                out.println("<tr>"
                                            + "<td><label id=\"" + "lbl" + commandName+"_"+ recordCount + "\" class=\"label\">" + details.getProductName()+ "</label></td>"
                                            + "<td><label class=\"label\">" + details.getNetworkDescription() + "</label></td>"
                                            + "<td><label class=\"label\">" + effectiveStartDate + "</label></td>"
                                            + "<td><label class=\"label\">" + terminationDate + "</label></td>"
                                            + "<td><label class=\"label\">" + processDate + "</label></td>"
                                            + "<td align=\"center\"><button name=\"" + "btn" + commandName+"_"+ recordCount + "\" id=\"" + "btn" + commandName+"_"+ recordCount + "\" type=\"button\" " + javaScript + " value=\"" + details.getNetworkDescription() + "\">Details</button></td>"
                                           + "</tr>");
                                listInactiveNetworks.add(details.getProductName() + "-" + details.getNetworkDescription());
                            }
                        }
                    }

                 }
                if (compareRecordCount == recordCount) {
                    out.println("<tr>"
                                + "<td><label id=\"" + "lbl" + commandName+"_"+ recordCount + "\" class=\"label\">&nbsp;</label></td>"
                                + "<td><label class=\"label\">&nbsp;</label></td>"
                                + "<td><label class=\"label\">&nbsp;</label></td>"
                                + "<td><label class=\"label\">&nbsp;</label></td>"
                                + "<td><label class=\"label\">&nbsp;</label></td>"
                                + "<td><label class=\"label\">&nbsp;</label></td>"
                               + "</tr>");
                }

                out.println("</table>");
            }
        } catch (java.io.IOException ex) {
            throw new JspException("Error in PracticeDetailsGrid tag", ex);
        }
        
        return super.doEndTag();
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }
}
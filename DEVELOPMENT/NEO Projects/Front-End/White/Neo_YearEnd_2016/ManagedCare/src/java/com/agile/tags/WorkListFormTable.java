/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

//import com.agile.security.webservice.WorkItemAndComment;
import java.util.ArrayList;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.WorkItemWithDetails;

/**
 *
 * @author gerritj
 */
public class WorkListFormTable extends TagSupport {
    private String withAction;
    private String command;
    private String commandDisplayName;
    private String workItemType;

    public void setWorkItemType(String workItemType) {
        this.workItemType = workItemType;
    }

    

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();

        try {
           ArrayList<WorkItemWithDetails> wic = (ArrayList<WorkItemWithDetails>) TagMethods.getWorkItemsAndComment(new Integer(workItemType));
           if((wic != null) && (wic.size() > 0)){
           out.println("<tr><th>ReqUsername</th><th>Comments</th><th>WorkItemId</th><th>ExternalRef</th><th>Severity</th><th>WorkItemType</th><th>Action</th></tr>");
           }
            for (int i = 0; i < wic.size(); i++) {
                WorkItemWithDetails workItemAndComment = wic.get(i);
                out.print("<tr><form action=\"/ManagedCare/AgileController\" method=\"POST\">");
                out.print("<td width=\"280\"><input id=\"username\" name=\"username\" type=\"hidden\"  value=\""+workItemAndComment.getReqUsername()+"\"/><p>"+workItemAndComment.getReqUsername()+"</p</td>");
                out.print("<td width=\"280\"><input id=\"comment\" name=\"comment\" type=\"hidden\"  value=\""+workItemAndComment.getComment()+"\"/><p>"+workItemAndComment.getComment()+"</p</td>");
                out.print("<td><input id=\"workitem_id\" name=\"workitem_id\" type=\"hidden\"  value=\""+workItemAndComment.getWorkitem().getWorkitemId()+"\"/><label>"+workItemAndComment.getWorkitem().getWorkitemId()+"</label></td>");
                out.print("<td><input id=\"external_reference\" name=\"external_reference\" type=\"hidden\" value=\""+workItemAndComment.getWorkitem().getExternalReference()+"\"/><label>"+workItemAndComment.getWorkitem().getExternalReference()+"</label></td>");
                out.print("<td><input id=\"severity\" name=\"severity\" type=\"hidden\" value=\""+workItemAndComment.getWorkitem().getSeverity()+"\"/><label>"+workItemAndComment.getWorkitem().getSeverity()+"</label></td>");
                if(workItemAndComment.getWorkitem().getWorkitemTypeId() == 1){ //hardcoded. to change to list
                    out.print("<td><input id=\"workitem_type_id\" name=\"workitem_type_id\" type=\"hidden\" value=\"Activate User\"/><label>Activate User</label></td>");
                }
                out.print("<td><button name=\"opperation\" type=\"submit\" value=\""+command+"\">"+commandDisplayName+"</button></td>");
                out.println("</form></tr>");
            }
        } catch (java.io.IOException ex) {
            throw new JspException("Error in WorkListFormTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setWithAction(String withAction) {
        this.withAction = withAction;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public void setCommandDisplayName(String commandDisplayName) {
        this.commandDisplayName = commandDisplayName;
    }

}

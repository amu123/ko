/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.agile.security.webservice.AgileManagerService;
import com.agile.security.webservice.EmailContent;
import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.lang.Exception;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class MemberApplicationActivateCommand extends NeoCommand {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd");

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            NeoManagerBean port = service.getNeoManagerBeanPort();
            NeoUser neoUser = (NeoUser) request.getSession().getAttribute("persist_user");
            String date = request.getParameter("MemberAppActivate_start_date");
            int appNum = getIntParam(request, "memberAppNumber");
            
            HttpSession session = request.getSession();
            
            List<neo.manager.LookupValue> lookUpsClient = service.getNeoManagerBeanPort().findCodeTableForLookupType(341);
            if (lookUpsClient != null) {
                neo.manager.LookupValue val = lookUpsClient.get(0);
                if (val != null) {
                    System.out.println("Client : " + val.getValue());
                    session.setAttribute("Client", val.getValue());
                }
            }
            
            MemberApplication ma = null;
            
            if (request.getSession().getAttribute("Client").equals("Sechaba")) {
                ma = port.getMemberApplicationByApplicationNumberSechaba(appNum);
            } else {
                ma = port.getMemberApplicationByApplicationNumber(appNum);
            }

            //System.err.println("Cover Number " + coverNumber + " App Number " + appNum + " : " + request.getAttribute("memberAppCoverNumber"));

            Date benefitDate = getDate(date);
            String status;
            String additionalData;
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.MONTH, -12);
            Date earlyDate = cal.getTime();

            Calendar cal2 = Calendar.getInstance();;
            cal2.add(Calendar.YEAR, 100);
            Date maxDate = cal2.getTime();


            if (benefitDate == null || benefitDate.after(maxDate)) {
                status = "ERROR";
                additionalData = "\"message\":\"Invalid Date\"";
            } else if (benefitDate == null || benefitDate.before(earlyDate)) {
                status = "ERROR";
                additionalData = "\"message\":\"Invalid Date\"";
            } else {
                //ZERREAL WELCOME LETTER
                System.out.println("Generating Sechaba Welcome Letter");
                String cell = ma.getTelMobile1();
                String email = ma.getEmail();
                int emailNewsLetter = ma.getEmailOtherInd();
                int SMSNewsLetter = ma.getSmsInd();
                System.out.println("DATA:\nCell [" + cell + "]\nEmail ["+ email +"]\nMailNewsLetter [" + emailNewsLetter + "]\nSMSNewsLetter ["+SMSNewsLetter+"]");
                
                if ((emailNewsLetter == 1) && (email != null || !email.isEmpty())) {
                        System.out.println("Sending Email: ");
                        email = email.trim();

                        //MAILING SERVICE
                        boolean emailSent;

                        String emailTo = email;
                        String emailType = "welcomeLetter";

                        if (emailTo != null && emailTo.contains("@")) {
                            String emailFrom = "";
                            String subject = "Explore a world of free Zurreal benefits";

                            EmailContent content = new EmailContent();
                            content.setEmailAddressFrom(emailFrom);
                            content.setEmailAddressTo(emailTo);
                            content.setSubject(subject);
                            content.setProductId(1);
                            content.setType(emailType);
                            emailSent = false;
                            try {
                                String WS_URL_TriggerSoapHttpPort = "http://localhost:9494/ManagedCareBackend/AgileManager?wsdl";
                                String WS_NAMESPACE_TriggerSoapHttpPort = "http://webservice.security.agile.com/";
                                String WS_SERVICE_TriggerSoapHttpPort = "AgileManagerService";
                                URL wsUrl = new URL(WS_URL_TriggerSoapHttpPort);
                                QName wsQName = new QName(WS_NAMESPACE_TriggerSoapHttpPort, WS_SERVICE_TriggerSoapHttpPort);
                                AgileManagerService services = new AgileManagerService(wsUrl, wsQName);

                                //emailSent = services.getAgileManagerPort().sendEmailViaLoader(content);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            System.out.println("Email sent : " + emailSent);

                        //MAILING SERVICE
                    }
                } else if ((SMSNewsLetter == 1) && (cell != null || !cell.isEmpty())) {
                    System.out.println("Sending SMS: ");
                        cell = cell.trim();

                        //SMS SERVICE
                        CommunicationMember comMember = new CommunicationMember();
                        String smsSent = "";
                        comMember.setMemberMobileNumber(cell);
                        comMember.setProductId(ma.getProductId());

                        if (ma.getProductId() == 1) {
                            comMember.setMessage("As a Resolution member, you get a free wellbeing and rewards lifestyle.  Visit www.zurreallifestyle.com to access your exclusive Zurreal world of 7 wonders.");
                        } else if (ma.getProductId() == 2) {
                            comMember.setMessage("As a Spectramed member, you get a free wellbeing and rewards lifestyle.  Visit www.zurreallifestyle.com to access your exclusive Zurreal world of 7 wonders.");
                        }
                        smsSent = port.sendEmailOrSmsToPDCWorkBenchPatient(comMember, "SMS");

                        if (smsSent.equalsIgnoreCase("Email sending successful")) {
                            smsSent = "Success";
                        }
                        System.out.println("SMS sent : " + smsSent);

                        //SMS SERVICE
                }
                //ZERREAL WELCOME LETTER
                boolean result = false;
                
                if (request.getSession().getAttribute("Client").equals("Sechaba")) {
                    result = port.finaliseMemberApplicationSechaba(appNum, getSecurity(request), DateTimeUtils.convertDateToXMLGregorianCalendar(benefitDate));
                } else {
                    result = port.finaliseMemberApplication(appNum, getSecurity(request), DateTimeUtils.convertDateToXMLGregorianCalendar(benefitDate));
                }
                
                String savePayment = (String) request.getSession().getAttribute("savePayment");
                if (ma.getPaymentMethod() != 0) {
                    if (savePayment == null) {
                        port.saveCoverAddInfo(ma.getCoverNumber(), DateTimeUtils.convertDateToXMLGregorianCalendar(benefitDate), ma.getPaymentMethod() + "", 8, getSecurity(request));
                    }
                }
                status = result ? "OK" : "ERROR";
                additionalData = "\"message\":\"" + (result ? "Status has been updated." : "An error occured updating the status") + "\"";
            }

            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, null, null, additionalData));
        } catch (Exception ex) {
            Logger.getLogger(MemberApplicationActivateCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private Date getDate(String dateStr) {
        try {
            return DATE_FORMAT.parse(dateStr);
        } catch (Exception e) {
            return null;
        }
    }

    private int getIntParam(HttpServletRequest request, String paramName) {
        try {
            String maxDepStr = request.getParameter(paramName);
            return Integer.parseInt(maxDepStr);
        } catch (java.lang.Exception e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }

    private Security getSecurity(HttpServletRequest request) {
        Security sec = new Security();
        NeoUser user = (NeoUser) request.getSession().getAttribute("persist_user");
        sec.setCreatedBy(user.getUserId());
        sec.setLastUpdatedBy(user.getUserId());
        sec.setSecurityGroupId(user.getSecurityGroupId());
        return sec;
    }

    @Override
    public String getName() {
        return "MemberApplicationActivateCommand";
    }
}

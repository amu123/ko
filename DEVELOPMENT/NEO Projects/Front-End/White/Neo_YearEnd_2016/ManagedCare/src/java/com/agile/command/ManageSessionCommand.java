/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.command;

import com.koh.command.Command;
import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.BiometricsTabs;
import org.apache.log4j.Logger;

/**
 *
 * @author
 * gerritr
 */
public class ManageSessionCommand extends NeoCommand {
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        org.apache.log4j.Logger.getLogger(this.getClass()).info("in ManageSessionCommand");
        HttpSession session = request.getSession();
        
        String value = request.getParameter("value");
        String sessionName = request.getParameter("sessionName");

        if ((value != null && !value.equals("")) || (sessionName != null  && !sessionName.equals(""))) {
            session.setAttribute(sessionName, value);
        }else if(value == null || value.equals("")){
            session.setAttribute(sessionName, "");
        }

        return null;
    }
    @Override
    public String getName() {
        return "ManageSessionCommand";
    }
}

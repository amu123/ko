/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import agility.za.documentprinting.DocumentPrintingRequest;
import agility.za.documentprinting.DocumentPrintingResponse;
import agility.za.documentprinting.PrintDocType;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class PrintPoolDeleteCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------In PrintPoolDeleteCommand---------------------");

            String selectedFile = request.getParameter("selectedFile");
            String selectedId = request.getParameter("selectedId");
            String displayOption = request.getParameter("displayOption");
            logger.info("selectedFile = " + selectedFile);
            logger.info("selectedId = " + selectedId);
            if (selectedFile != null && !selectedFile.trim().equals("")) {
                DocumentPrintingRequest req = new DocumentPrintingRequest();
                NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
                req.setAgent(loggedInUser.getUsername());
                req.setPrintProcessType("Delete");
                
                req.setSrcPrintDrive(new PropertiesReader().getProperty("DocumentIndexIndexDrive"));
                PrintDocType docType = new PrintDocType(); 
//                docType.setFolder(new PropertiesReader().getProperty("PrintPoolFolder"));
                docType.setFilename(selectedFile);
                docType.setPrintId(new Long(selectedId));
                logger.info("Delete"); 
                
                req.setPrintDoc(docType);
                DocumentPrintingResponse resp = NeoCommand.service.getNeoManagerBeanPort().processPrintRequest(req);
                if(resp.isIsSuccess()){
                    
                   logger.info("Delete succesful");
                }else {
                    logger.error("Error printing .... " + resp.getMessage());
                    errorMessage(request, response, resp.getMessage());
                    return "PrintPoolDeleteCommand";
                }
            }
            request.setAttribute("displayOption", displayOption);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/PrintPool.jsp");
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "PrintPoolDeleteCommand";
    }
    
    private void errorMessage(HttpServletRequest request, HttpServletResponse response, String msg) throws Exception {
        request.setAttribute("errorMsg", msg);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/PrintPool.jsp");
        dispatcher.forward(request, response);
    }   

    @Override
    public String getName() {
        return "PrintPoolDeleteCommand";
    }
}
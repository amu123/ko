/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthCoverExclusions;

/**
 *
 * @author johanl
 */
public class AuthExclusionTableTag extends TagSupport {

    private String commandName;
    private String javaScript;
    
    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        boolean flag = false;

        String removeExc = "" + req.getAttribute("reloadExclusion");
        if (removeExc != null && !removeExc.trim().equalsIgnoreCase("")) {
            if (removeExc.equalsIgnoreCase("true")) {
                flag = true;
            } else {
                flag = false;
            }
        } else {
            String remEx = "" + session.getAttribute("reloadExclusion");
            if (remEx != null && !remEx.equalsIgnoreCase("")) {
                if (remEx.equalsIgnoreCase("true")) {
                    flag = true;
                    session.removeAttribute("reloadExclusion");
                } else {
                    flag = false;
                }
            }else{
                flag = false;
            }
        }

        System.out.println("AuthExclusionTableTag FLAG = "+flag);
        if (flag == false) {
            try {
                int memEId = 0;
                String entityId = "" + session.getAttribute("memberEntityId");
                System.out.println("Member Entity ID = "+entityId);
                if (entityId != null && !entityId.trim().equalsIgnoreCase("") &&
                        !entityId.trim().equalsIgnoreCase("null")) {
                    memEId = Integer.parseInt(entityId);
                }
                if (memEId != 0) {
                    @SuppressWarnings("static-access")
                    List<AuthCoverExclusions> aceList = NeoCommand.service.getNeoManagerBeanPort().getCoverExclusionsByEID(memEId);
                    System.out.println("ace list = "+aceList.size());
                    if (aceList.size() != 0) {
                        out.println("<td colspan=\"5\"><table><tr>");
                        out.println("<td colspan=\"3\"><label class=\"errors\" >* Exclusion(s) exist on this member </label></td>");
                        out.println("<td><button name=\"opperation\" type=\"submit\" " + javaScript + " value=\"" + commandName + "\">View Exclusion Details</button></td>");
                        out.println("</tr></table></td>");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

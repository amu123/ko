/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

import com.koh.calltrack.command.SearchCallCoverByUpdateCommand;
import com.koh.command.Command;
import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.employer.command.TabUtils;
import com.koh.utils.NeoCoverDetailsDependantFilter;
import com.koh.utils.UrlUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CallTrackDetails;
import neo.manager.CoverDetails;
import neo.manager.CoverSearchCriteria;
import neo.manager.EAuthCoverDetails;
import neo.manager.KeyValue;
import neo.manager.KeyValueArray;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.NeoMenu;
import neo.manager.NeoUser;
import neo.manager.ProviderDetails;
import neo.manager.ProviderSearchDetails;
import neo.manager.SecurityResponsibility;
import neo.manager.WorkflowItem;
import neo.manager.WorkflowItemDetails;

/**
 *
 * @author gerritr
 */
public class WorkflowContentCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("in WorkflowContentCommand");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        String location = String.valueOf(request.getParameter("onScreen"));
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        List<WorkflowItem> arrWFItem = null;
        List<WorkflowItem> arrWorkFlowItems = null;
        WorkflowItem wfItem = null;
        List<LookupValue> arrwfSource = null;

        if (user != null) {
            ArrayList<SecurityResponsibility> respList = (ArrayList) user.getSecurityResponsibility();
            if (respList != null) {

                //CALLTRACKING
                //<editor-fold defaultstate="collapsed" desc="Workflow/WorkflowCallTracking.jsp">
                if (location.contains("WorkflowCallTracking.jsp")) {
                    for (int i = 0; i < respList.size(); i++) {
                        SecurityResponsibility resp = respList.get(i);
                        ArrayList<NeoMenu> menuItems = (ArrayList) resp.getMenuItems();
                        if (menuItems != null) {
                            int parent = 35; //CallTracking submenu item
                            ArrayList arrMenu = buildTree(menuItems, parent);
                            if (!arrMenu.isEmpty()) {
                                request.setAttribute("wfCallTrack", arrMenu);

                            }
                        }
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="Workflow/WorkflowLogNewCall.jsp">
                //Directing 
                if (location.contains("WorkflowLogNewCall.jsp")) {
                    System.out.println("Inside WorkflowLogNewCall check");

                    //<editor-fold defaultstate="collapsed" desc="Log a New Call">
                    System.out.println("Setting up clean CallTrack Page");
                    Date today = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                    String date = sdf.format(today);
                    request.setAttribute("uName", user.getUsername());
                    request.setAttribute("uRName", user.getUsername());
                    request.setAttribute("disDate", date);
                    
                    request.setAttribute("memberNumber_text", "");
                    request.setAttribute("memberNumber_text", "");
                    request.setAttribute("notes", "");

                    request.setAttribute("callType", "");
                    request.setAttribute("callTrackNumber", "");
                    request.setAttribute("callStatus", "");
                    request.setAttribute("memberNumber", "");
                    request.setAttribute("provNum", "");
                    request.setAttribute("providerNumber", "");
                    request.setAttribute("memberNum", "");
                    request.setAttribute("provName", "");
                    request.setAttribute("discipline", "");
                    request.setAttribute("callerName", "");
                    request.setAttribute("callerContact", "");
                    request.setAttribute("queryMethod", "");
                    request.setAttribute("callQ", "");
                    
                    session.removeAttribute("callQueries1");
                    request.setAttribute("callQueries1", "");
                    session.removeAttribute("callQueries2");
                    request.setAttribute("callQueries2", "");

                    //<editor-fold defaultstate="collapsed" desc="Catagory Checkboxes">
                    request.setAttribute("claimsFC_0", "");
                    request.setAttribute("claimsFC_1", "");
                    request.setAttribute("claimsSC_0", "");
                    request.setAttribute("claimsSC_1", "");
                    request.setAttribute("claimsSC_2", "");
                    request.setAttribute("claimsSC_3", "");
                    request.setAttribute("claimsSC_4", "");
                    request.setAttribute("claimsSC_5", "");
                    request.setAttribute("claimsSC_6", "");
                    request.setAttribute("claimsTC_0", "");
                    request.setAttribute("claimsTC_1", "");
                    request.setAttribute("claimsTC_2", "");
                    request.setAttribute("claimsTC_3", "");
                    request.setAttribute("claimsTC_4", "");
                    request.setAttribute("claimsTC_5", "");
                    request.setAttribute("claimsTC_6", "");
                    request.setAttribute("claimsTC_7", "");
                    request.setAttribute("claimsTC_8", "");
                    request.setAttribute("claimsTC_9", "");
                    request.setAttribute("claimsTC_10", "");
                    request.setAttribute("claimsTC_11", "");

                    request.setAttribute("backOfficeFC_0", "");
                    request.setAttribute("backOfficeFC_1", "");
                    request.setAttribute("backOfficeSC_0", "");
                    request.setAttribute("backOfficeSC_1", "");
                    request.setAttribute("backOfficeSC_2", "");
                    request.setAttribute("backOfficeSC_3", "");
                    request.setAttribute("backOfficeSC_4", "");

                    request.setAttribute("groupsFC_0", "");
                    request.setAttribute("groupsFC_1", "");
                    request.setAttribute("groupsFC_2", "");
                    request.setAttribute("groupsSC_0", "");
                    request.setAttribute("groupsSC_1", "");
                    request.setAttribute("groupsSC_2", "");
                    request.setAttribute("groupsSC_3", "");
                    request.setAttribute("groupsSC_4", "");

                    request.setAttribute("brokerFC_0", "");
                    request.setAttribute("brokerFC_1", "");
                    request.setAttribute("brokerFC_2", "");
                    request.setAttribute("brokerFC_3", "");
                    request.setAttribute("brokerSC_0", "");
                    request.setAttribute("brokerSC_1", "");
                    request.setAttribute("brokerSC_2", "");
                    request.setAttribute("brokerSC_3", "");
                    request.setAttribute("brokerSC_4", "");

                    request.setAttribute("statementsFC_0", "");
                    request.setAttribute("statementsFC_1", "");
                    request.setAttribute("statementsTC_0", "");
                    request.setAttribute("statementsTC_1", "");
                    request.setAttribute("statementsTC_2", "");
                    request.setAttribute("statementsTC_3", "");
                    request.setAttribute("statementsTC_4", "");

                    request.setAttribute("benefitsFCReso_0", "");
                    request.setAttribute("benefitsFCReso_1", "");
                    request.setAttribute("benefitsFCReso_2", "");
                    request.setAttribute("benefitsFCReso_3", "");
                    request.setAttribute("benefitsFCReso_4", "");
                    request.setAttribute("benefitsFCReso_5", "");
                    request.setAttribute("benefitsFCSpec_0", "");
                    request.setAttribute("benefitsFCSpec_1", "");
                    request.setAttribute("benefitsFCSpec_2", "");
                    request.setAttribute("benefitsFCSpec_3", "");
                    request.setAttribute("benefitsFCSpec_4", "");
                    request.setAttribute("benefitsSC_0", "");
                    request.setAttribute("benefitsSC_1", "");
                    request.setAttribute("benefitsSC_2", "");
                    request.setAttribute("benefitsSC_3", "");
                    request.setAttribute("benefitsSC_4", "");
                    request.setAttribute("benefitsSC_5", "");
                    request.setAttribute("benefitsSC_6", "");
                    request.setAttribute("benefitsTC_0", "");
                    request.setAttribute("benefitsTC_1", "");
                    request.setAttribute("benefitsTC_2", "");
                    request.setAttribute("benefitsTC_3", "");
                    request.setAttribute("benefitsTC_4", "");
                    request.setAttribute("benefitsTC_5", "");
                    request.setAttribute("benefitsTC_6", "");
                    request.setAttribute("benefitsTC_7", "");
                    request.setAttribute("benefitsTC_8", "");
                    request.setAttribute("benefitsTC_9", "");
                    request.setAttribute("benefitsTC_10", "");
                    request.setAttribute("benefitsTC_11", "");
                    request.setAttribute("benefitsTC_12", "");

                    request.setAttribute("prenmiumsFC_0", "");
                    request.setAttribute("prenmiumsFC_1", "");
                    request.setAttribute("premiumsTC_0", "");
                    request.setAttribute("premiumsTC_1", "");
                    request.setAttribute("premiumsTC_2", "");
                    request.setAttribute("premiumsTC_3", "");
                    request.setAttribute("premiumsTC_4", "");
                    request.setAttribute("premiumsTC_5", "");
                    request.setAttribute("premiumsTC_6", "");

                    request.setAttribute("membershipFC_0", "");
                    request.setAttribute("membershipFC_1", "");
                    request.setAttribute("membershipFC_2", "");
                    request.setAttribute("memberShipTC_0", "");
                    request.setAttribute("memberShipTC_1", "");
                    request.setAttribute("memberShipTC_2", "");
                    request.setAttribute("memberShipTC_3", "");
                    request.setAttribute("memberShipTC_4", "");
                    request.setAttribute("memberShipTC_5", "");
                    request.setAttribute("memberShipTC_6", "");
                    request.setAttribute("memberShipTC_7", "");
                    request.setAttribute("memberShipTC_8", "");
                    request.setAttribute("memberShipTC_9", "");
                    request.setAttribute("memberShipTC_10", "");
                    request.setAttribute("memberShipTC_11", "");
                    request.setAttribute("memberShipTC_12", "");
                    request.setAttribute("memberShipTC_13", "");
                    request.setAttribute("memberShipTC_14", "");
                    request.setAttribute("memberShipTC_15", "");

                    request.setAttribute("serviceProviderFC_0", "");
                    request.setAttribute("serviceProviderFC_1", "");
                    request.setAttribute("serviceProviderFC_2", "");
                    request.setAttribute("serviceProviderFC_3", "");
                    request.setAttribute("serviceProviderFC_4", "");
                    request.setAttribute("serviceProviderFC_5", "");
                    request.setAttribute("serviceProviderFC_6", "");
                    request.setAttribute("serviceProviderTC_0", "");
                    request.setAttribute("serviceProviderTC_1", "");
                    request.setAttribute("serviceProviderTC_2", "");

                    request.setAttribute("chronicFC_0", "");
                    request.setAttribute("chronicFC_1", "");
                    request.setAttribute("chronicSC_0", "");
                    request.setAttribute("chronicSC_1", "");

                    request.setAttribute("oncologyFC_0", "");
                    request.setAttribute("oncologyFC_1", "");
                    request.setAttribute("oncologySC_1", "");
                    request.setAttribute("oncologySC_1", "");

                    request.setAttribute("wellcareFC_0", "");
                    request.setAttribute("wellcareFC_1", "");
                    request.setAttribute("wellcareSC_0", "");
                    request.setAttribute("wellcareSC_1", "");

                    request.setAttribute("foundationFC_0", "");
                    request.setAttribute("foundationFC_1", "");
                    request.setAttribute("foundationFC_2", "");
                    request.setAttribute("foundationSC_0", "");
                    request.setAttribute("foundationSC_1", "");
                    request.setAttribute("foundationSC_2", "");
                    request.setAttribute("foundationSC_3", "");
                    request.setAttribute("foundationSC_4", "");
                    request.setAttribute("foundationSC_5", "");
                    request.setAttribute("foundationSC_6", "");
                    //</editor-fold>

                    //</editor-fold>
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="UPDATING CALL TRACKING">
                //Directing 
                if (null != request.getParameter("trackId") && !request.getParameter("trackId").isEmpty()) {
                    System.out.println("Inside WorkflowLogNewCall");
                    location = "Calltrack/WorkflowLogNewCall.jsp";

                    //<editor-fold defaultstate="collapsed" desc="UPDATE EXISTING CALL">
                    System.out.println("Setting up clean CallTrack Page");
                    String in = request.getParameter("trackId");
                    int index = new Integer(in);
                    HttpServletRequest ReturnRequest = request;

                    if (in != null) {
                        CallTrackDetails call = service.getNeoManagerBeanPort().getCallByTrackID(index);                

                        String memberNumber = call.getCoverNumber();
                        CoverDetails c = port.getEAuthResignedMembers(memberNumber);

                        request.setAttribute("productId", c.getProductId());
                        request.setAttribute("callType", call.getCallType());
                        request.setAttribute("callTrackNumber", call.getCallReference());
                        request.setAttribute("callStatus", call.getCallStatusId());
                        request.setAttribute("refUser", call.getReferedUserId());
                        request.setAttribute("callStartDate", call.getCallStartDate());
                        request.setAttribute("callEndDate", call.getCallEndDate());
                        session.setAttribute("callStartDate", call.getCallStartDate());
                        session.setAttribute("callEndDate", call.getCallEndDate());
                        request.setAttribute("memberNumber_text", memberNumber);
                        request.setAttribute("memberNum", memberNumber);
                        request.setAttribute("providerNumber_text", call.getProviderNumber());
                        request.setAttribute("provNum", call.getProviderNumber());
                        request.setAttribute("memNo", call.getProviderNumber());
                        request.setAttribute("provName", call.getProviderNameSurname());
                        request.setAttribute("discipline", call.getProviderDiscipline());
                        request.setAttribute("callerName", call.getCallerName());
                        request.setAttribute("callerContact", call.getContactDetails());
                        request.setAttribute("notes", call.getNotes());
                        session.setAttribute("callTrackingNotes", call.getNotes()); //Session needed to compare with the new notes
                                                
                        Date today = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                        String date = sdf.format(today);
                        request.setAttribute("uName", user.getUsername());
                        request.setAttribute("uRName",  user.getUsername());
                        request.setAttribute("disDate", date);
                        

                        // setting the call queries in the session
                        session.setAttribute("callQueries1", call.getQueries());
                        request.setAttribute("callQueries1", call.getQueries());
                        session.setAttribute("callQueries2", call.getCallQuery());
                        request.setAttribute("callQueries2", call.getCallQuery());

                        request.setAttribute("trackId", call.getCallTrackId());

                        if (call.getCoverNumber() != null && !call.getCoverNumber().trim().equalsIgnoreCase("")
                                && !call.getCoverNumber().trim().equalsIgnoreCase("0")) {

                            List<EAuthCoverDetails> details = new ArrayList<EAuthCoverDetails>();

                            try {
                                CoverSearchCriteria search = new CoverSearchCriteria();
                                search.setCoverNumberExact(memberNumber);
                                details = service.getNeoManagerBeanPort().eAuthCoverSearch(search);
                                if (details != null && details.size() > 0) {
                                    //filter dependants of members to only display one dependant record for each cover dependant
                                    NeoCoverDetailsDependantFilter filter = new NeoCoverDetailsDependantFilter();
                                    details = filter.EAuthCoverDetailsDependantFilter(details);

                                }

                                request.setAttribute("memberCoverDetails", details);
                                request.setAttribute("memberNumber_text", memberNumber);
                            } catch (Exception ex) {

                                ex.printStackTrace();
                            }
                        }
                        
                        //******************REQUEST ITERATOR
                        java.util.Enumeration attributeNames = request.getParameterNames();
                        while (attributeNames.hasMoreElements()) {
                            String name = String.valueOf(attributeNames.nextElement());
                            String value = String.valueOf(request.getParameter(name));
                            String viewClass = String.valueOf(request.getParameter(name).getClass());
                            request.setAttribute(name, value);
                        }
                        //******************REQUEST ITERATOR
                    }
                    //</editor-fold>
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="Workflow/WorkflowMyWorkbench.jsp">
                System.out.println("### location: " + location);
                if (location.contains("WorkflowUpdate.jsp")) {
                    location = "Workflow/WorkflowMyWorkbench.jsp";
                }
                if (location.contains("WorkflowMyWorkbench.jsp")) {

                    //<editor-fold defaultstate="collapsed" desc="Workflow Forward || Reassign">
                    if (request.getParameter("actionDesc") != null && (request.getParameter("actionDesc").equalsIgnoreCase("WorkflowForward") || request.getParameter("actionDesc").equalsIgnoreCase("WorkflowReassign"))) {
                        if (request.getParameter("workflowQueue") != null && !request.getParameter("workflowQueue").isEmpty()) {
                            if (request.getParameter("itemId") != null && !request.getParameter("itemId").isEmpty()) {

                                //Retrieve Panel information
                                wfItem = port.getWorkflowItemByItemID(Integer.parseInt(String.valueOf(request.getParameter("itemId"))));

                                List<WorkflowItemDetails> arrWFItemDetails = port.getWorkflowItemDetailsByItemID(Integer.parseInt(String.valueOf(request.getParameter("itemId"))));
                                WorkflowItemDetails wfID;
                                if (!arrWFItemDetails.isEmpty()) {
                                    wfID = arrWFItemDetails.get(0);
                                } else {
                                    wfID = new WorkflowItemDetails();
                                    wfID.setItemID(Integer.parseInt(String.valueOf(request.getParameter("itemId"))));
                                    wfID.setStatusID(wfItem.getStatusID());
                                }

                                wfID.setNotes("");
                                wfID.setQueueID(Integer.parseInt(String.valueOf(request.getParameter("workflowQueue"))));

                                //If workflowUser is empty = forward command was used, else Reassign command
                                if (request.getParameter("workflowUser") != null && !request.getParameter("workflowUser").isEmpty()) {
                                    //!!REASSIGN!!    
                                    wfID.setUserID(Integer.parseInt(String.valueOf(request.getParameter("workflowUser"))));
                                }

                                if (request.getParameter("wfNote") != null && !request.getParameter("wfNote").isEmpty()) {
                                    wfID.setNotes(String.valueOf(request.getParameter("wfNote")));
                                }

                                System.out.println("Updating Workflow Assignee");
                                int result = port.updateWorkflowItemAssignee(wfID, user.getUserId());
                                if (result > 0) {
                                    System.out.println("Workflow assigned/forwarded successfully");
                                } else {
                                    System.out.println("Workflow assigned/forwarded failed [nothing Altered]");
                                }
                            }

                        }
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="Workflow Status Change">
                    if (request.getParameter("actionDesc") != null && request.getParameter("actionDesc").equalsIgnoreCase("WorkflowStatusChange")) {
                        if (request.getParameter("itemId") != null && !request.getParameter("itemId").isEmpty()) {
                            //Retrieving latest WorkflowItemDetails Object

                            List<WorkflowItemDetails> arrWFItemDetails = port.getWorkflowItemDetailsByItemID(Integer.parseInt(String.valueOf(request.getParameter("itemId"))));
                            WorkflowItemDetails wfID = arrWFItemDetails.get(0);

                            wfID.setNotes("");

                            wfID.setStatusID(Integer.parseInt(String.valueOf(request.getParameter("workflowStatus"))));//Status
                            if(wfID.getStatusID() == 5){
                                String resolutionReason = request.getParameter("workflowResolutionReason");
                                System.out.println("resolutionReason = " + resolutionReason);
                                if(resolutionReason != null && !resolutionReason.equalsIgnoreCase("")){
                                    wfID.setReasonID(Integer.parseInt(String.valueOf(resolutionReason)));
                                }
                            }

                            if (request.getParameter("wfNote") != null && !request.getParameter("wfNote").isEmpty()) {
                                wfID.setNotes(String.valueOf(request.getParameter("wfNote")));
                            }

                            System.out.println("Updating Workflow Assignee");
                            int result = port.updateWorkflowItemAssignee(wfID, user.getUserId());
                            if (result > 0) {
                                System.out.println("Workflow Closed successfully");
                            } else {
                                System.out.println("Workflow assigned/forwarded failed [nothing Altered]");
                            }
                        }
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="Workflow Send Reply Email">
                    if (request.getParameter("actionDesc") != null && request.getParameter("actionDesc").equalsIgnoreCase("WorkflowSendEmail")) {
                        if (request.getParameter("itemId") != null && !request.getParameter("itemId").isEmpty()) {
                            String status = "";
                            String additionalData = "";
                            String notes = "";
                            List<WorkflowItemDetails> arrWFItemDetails = port.getWorkflowItemDetailsByItemID(Integer.parseInt(String.valueOf(request.getParameter("itemId"))));
                            WorkflowItemDetails wfID = arrWFItemDetails.get(0);

                            if (request.getParameter("wfNote") != null && !request.getParameter("wfNote").isEmpty()) {
                                notes = String.valueOf(request.getParameter("wfNote"));
                            }

                            wfID.setNotes(notes);

                            System.out.println("Updating Workflow Note To send");
                            int result = port.updateWorkflowItemAssignee(wfID, user.getUserId());

                            if (result > 0) {
                                System.out.println("New Note Saved Sending Email");
                                WorkflowItem wfi = port.getWorkflowItemByItemID(Integer.parseInt(String.valueOf(request.getParameter("itemId"))));
                                wfi.setNotes(notes);

                                if (port.sendReplyToClient(wfi)) {
                                    System.out.println("Reply Mail Sent successfully");
                                    status = "OK";
                                    additionalData = "\"message\":\"Reply Mail Sent successfully.\"";
                                } else {
                                    System.out.println("Failed to send Reply Mail.");
                                    status = "ERROR";
                                    additionalData = "\"message\":\"Failed to send Reply Mail.\"";
                                }
                            }
                            String jsonResult = TabUtils.buildJsonResult(status, null, null, additionalData);
                            try {
                                PrintWriter out = response.getWriter();
                                out.print(jsonResult);
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                    //</editor-fold>
                    try {
                        //<editor-fold defaultstate="collapsed" desc="Workflow Update">
                        if (request.getParameter("actionDesc") != null && request.getParameter("actionDesc").equalsIgnoreCase("WorkflowUpdate")) {
                            if (request.getParameter("itemId") != null && !request.getParameter("itemId").isEmpty()) {
                                //Retrieving latest WorkflowItemDetails Object
                                List<WorkflowItemDetails> arrWFItemDetails = port.getWorkflowItemDetailsByItemID(Integer.parseInt(String.valueOf(request.getParameter("itemId"))));
                                WorkflowItem updateWFItem = new WorkflowItem();
                                WorkflowItemDetails wfID = new WorkflowItemDetails();

                                if (arrWFItemDetails != null && arrWFItemDetails.size() > 0) {
                                    wfID = arrWFItemDetails.get(0);
                                }

                                updateWFItem = port.getWorkflowItemByItemID(Integer.parseInt(request.getParameter("itemId")));

                                wfID.setNotes("");

                                if (request.getParameter("wfNote") != null && !request.getParameter("wfNote").isEmpty()) {
                                    wfID.setNotes(String.valueOf(request.getParameter("wfNote")));
                                }

                                int subType = Integer.parseInt(request.getParameter("workflowSubType"));
                                String memberNumber = request.getParameter("memberNum");
                                String providerNumber = request.getParameter("provNum");
                                int entityType = Integer.parseInt(request.getParameter("entityType"));
                                String entityNumber = request.getParameter("entityNum");

                                System.out.println("### entityType " + entityType);
                                System.out.println("### entityNumber " + entityNumber);

                                if (entityType == 1) {
                                    System.out.println("### in entity 1");
                                    CoverDetails cd = service.getNeoManagerBeanPort().getEAuthResignedMembers(entityNumber);
                                    System.out.println("### EntityId() " + cd.getEntityId());
                                    updateWFItem.setEntityID(cd.getEntityId());
                                    updateWFItem.setEntityNumber(entityNumber);
                                    wfID.setEntityID(cd.getEntityId());
                                    wfID.setEntityNumber(entityNumber);
                                } else if (entityType == 2) {
                                    System.out.println("### in entity 2");
                                    ProviderDetails pd = service.getNeoManagerBeanPort().getProviderDetailsForEntityByNumber(entityNumber);
                                    System.out.println("### EntityId() " + pd.getEntityId());
                                    updateWFItem.setEntityID(pd.getEntityId());
                                    updateWFItem.setEntityNumber(entityNumber);
                                    wfID.setEntityID(pd.getEntityId());
                                    wfID.setEntityNumber(entityNumber);
                                } else if(entityType == 3){
                                    String code = entityNumber;
                                    String name = "";
                                    List<KeyValueArray> kva = port.getBrokerList(code, name, "");
                                    try {
                                        PrintWriter out = response.getWriter();
                                        if (kva.size() == 1) {
                                            for (KeyValueArray s : kva) {
                                                List<KeyValue> t = s.getKeyValList();
                                                for (KeyValue d : t) {
                                                    System.out.println("d.getKey() " + d.getKey());
//                                                    System.out.println("d.getValue() " + d.getValue());
                                                    if (d.getKey().equalsIgnoreCase("brokerEntityId")) {
                                                        if(d.getValue() != null){
                                                            System.out.println("### d.getValue() " + d.getValue());
                                                            updateWFItem.setEntityID(Integer.parseInt(d.getValue()));
                                                            wfID.setEntityID(Integer.parseInt(d.getValue()));
                                                        }
                                                        updateWFItem.setEntityNumber(entityNumber);
                                                        wfID.setEntityNumber(entityNumber);
                                                    }
                                                }
                                            }
                                        } else {
                                            out.print("Error|No Such Group|" + getName());
                                        }

                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                } else if (entityType == 5) {
                                    System.out.println("### in entity 5");
                                    String groupName = "";
                                    String groupNumber = entityNumber;
                                    int productId = 3;//hardcoded to Sizwe for now
                                    List<KeyValueArray> kva = service.getNeoManagerBeanPort().getEmployerList(groupName, groupNumber, productId);

                                    try {
                                        PrintWriter out = response.getWriter();
                                        if (kva.size() == 1) {
                                            for (KeyValueArray s : kva) {
                                                List<KeyValue> t = s.getKeyValList();
                                                for (KeyValue d : t) {
                                                    if (d.getKey().equalsIgnoreCase("entityId")) {
                                                        if(d.getValue() != null){
                                                            System.out.println("### d.getValue() " + d.getValue());
                                                            updateWFItem.setEntityID(Integer.parseInt(d.getValue()));
                                                            wfID.setEntityID(Integer.parseInt(d.getValue()));
                                                        }
                                                        updateWFItem.setEntityNumber(entityNumber);
                                                        wfID.setEntityNumber(entityNumber);
                                                    }
                                                }
                                            }
                                        } else {
                                            out.print("Error|No Such Group|" + getName());
                                        }

                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                } else {
                                    System.out.println("### in entity else");
                                    updateWFItem.setEntityID(null);
                                    updateWFItem.setEntityNumber(entityNumber);
                                    wfID.setEntityID(null);
                                    wfID.setEntityNumber(entityNumber);
                                }

//                            if (providerNumber != null && !providerNumber.equalsIgnoreCase("")) {
//                                ProviderDetails pd = service.getNeoManagerBeanPort().getProviderDetailsForEntityByNumber(providerNumber);
//
//                                updateWFItem.setEntityID(pd.getEntityId());
//                                updateWFItem.setEntityNumber(providerNumber);
//                                wfID.setEntityID(pd.getEntityId());
//                                wfID.setEntityNumber(providerNumber);
//                            }
//
//                            if (memberNumber != null && !memberNumber.equalsIgnoreCase("")) {
//                                CoverDetails cd = service.getNeoManagerBeanPort().getEAuthResignedMembers(memberNumber);
//                                updateWFItem.setEntityID(cd.getEntityId());
//                                updateWFItem.setEntityNumber(memberNumber);
//                                wfID.setEntityID(cd.getEntityId());
//                                wfID.setEntityNumber(memberNumber);
//                            }
                                updateWFItem.setSubTypeID(subType);
                                wfID.setSubType(subType);

                                int result = port.updateWorkflowItem(updateWFItem, wfID, user.getUserId());
                                if (result > 0) {
                                    System.out.println("Workflow Closed successfully");
                                } else {
                                    System.out.println("Workflow assigned/forwarded failed [nothing Altered]");
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //</editor-fold>

                    //Propulate dropdown options
                    //<editor-fold defaultstate="collapsed" desc="Propulate dropdown options">
                    ArrayList<LookupValue> arrOptionList = new ArrayList();
                    LookupValue lvOptionList = new LookupValue();
                    lvOptionList.setId("Forward");
                    lvOptionList.setValue("Workflow/WorkflowForward.jsp");
                    arrOptionList.add(lvOptionList);

                    boolean canSee = port.getWorkflowSeeItemAuthority(user.getUserId());
                    if (canSee) {
                        lvOptionList = new LookupValue();
                        lvOptionList.setId("Reassign");
                        lvOptionList.setValue("Workflow/WorkflowReassign.jsp");
                        arrOptionList.add(lvOptionList);
                    }

//                    lvOptionList = new LookupValue();
//                    lvOptionList.setId("Audit Trail");
//                    lvOptionList.setValue("Workflow/WorkflowAuditTrail.jsp");
//                    arrOptionList.add(lvOptionList);
//
                    lvOptionList = new LookupValue();
                    lvOptionList.setId("Change Status");
                    lvOptionList.setValue("Workflow/WorkflowChangeStatus.jsp");
                    arrOptionList.add(lvOptionList);
                    request.setAttribute("arrOptionList", arrOptionList);

                    lvOptionList = new LookupValue();
                    lvOptionList.setId("Reply Email");
                    lvOptionList.setValue("Workflow/WorkflowReplyEmail.jsp");
                    arrOptionList.add(lvOptionList);
                    request.setAttribute("arrOptionList", arrOptionList);

                    lvOptionList = new LookupValue();
                    lvOptionList.setId("Update");
                    lvOptionList.setValue("Workflow/WorkflowUpdate.jsp");
                    arrOptionList.add(lvOptionList);
                    request.setAttribute("arrOptionList", arrOptionList);
                    //</editor-fold>

                    //Retrieve Panel information
                    arrWFItem = port.getWorkflowItemByUserID(user.getUserId());
                    //Ignore Closed and cancelled calls
                    arrWorkFlowItems = new ArrayList<WorkflowItem>();

                    arrwfSource = port.getCodeTable(355);

                    for (int i = 0; i < arrWFItem.size(); i++) {
                        if (arrWFItem.get(i).getStatusID() != 5 && arrWFItem.get(i).getStatusID() != 6) {
                            arrWorkFlowItems.add(arrWFItem.get(i));
                            int wfItemSource = arrWFItem.get(i).getSourceID();
                            for (LookupValue wfSource : arrwfSource) {
                                if (wfItemSource == Integer.parseInt(String.valueOf(wfSource.getId()))) {
                                    arrWFItem.get(i).setSourceName(wfSource.getValue());
                                }
                            }
                        }
                    }
                    if (arrWorkFlowItems.size() > 0) {
                        request.setAttribute("category", arrWorkFlowItems.get(arrWorkFlowItems.size() - 1).getQueueDesc());
                    }
                    request.setAttribute("wfPanelList", arrWorkFlowItems);
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="Workflow/WorkflowSearch.jsp">
                if (location.contains("WorkflowSearch.jsp")) {
                    for (int i = 0; i < respList.size(); i++) {
                        SecurityResponsibility resp = respList.get(i);
                        ArrayList<NeoMenu> menuItems = (ArrayList) resp.getMenuItems();
                        if (menuItems != null) {
                            int parent = 35; //CallTracking submenu item
                            ArrayList arrMenu = buildTree(menuItems, parent);
                            if (!arrMenu.isEmpty()) {
                                request.setAttribute("wfCallTrack", arrMenu);

                            }
                        }
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="Workflow/WorkflowMailRetrieval.jsp">
                if (location.contains("WorkflowMailRetrieval.jsp")) {
                    String pullMails = request.getParameter("pullMails");

                    if (pullMails != null && pullMails.equals("Y")) {
                        try {
                            pullExchangeMails(request, response, context);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                //</editor-fold>

            }
        }

        try {
            request.getRequestDispatcher(location).forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(WorkflowContentCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WorkflowContentCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public ArrayList buildTree(ArrayList<NeoMenu> menuItems, int parent) {
        ArrayList arrMenu = new ArrayList();
        LookupValue lvMenu = null;
        for (int i = 0; i < menuItems.size(); i++) {
            NeoMenu item = menuItems.get(i);
            if (item.getParentMenuItem() == parent) {
                lvMenu = new LookupValue();

                if (item.getAttributes().getAction() != null) {
                    String href = UrlUtil.getUrl(item.getAttributes().getAction(), "appName", item.getDescription());
                    lvMenu.setId(item.getDisplayName());
                    lvMenu.setValue(href);

                    arrMenu.add(lvMenu);
                }
            }
        }
        return arrMenu;
    }

    @Override
    public String getName() {
        return "WorkflowContentCommand";
    }

    private void pullExchangeMails(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Integer result = port.pullExchangeMails();
        Map<String, String> fieldsMap = new HashMap<String, String>();
        String additionalData = null;
        if (result != null) {
            if (result > 0) {
                fieldsMap.put("pullMailsResponse", "ok");
                additionalData = "\"message\":\"Exchange mails pulled.\"";
            } else {
                fieldsMap.put("pullMailsResponse", "ok");
                additionalData = "\"message\":\"No new mails where pulled.\"";
            }
        } else {
            fieldsMap.put("pullMailsResponse", "error");
            additionalData = "\"message\":\"There was an error pulling the exchange mails.\"";
        }

        String updateFields = TabUtils.convertMapToJSON(fieldsMap);
        String resultStr = TabUtils.buildJsonResult(null, null, updateFields, additionalData);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println(resultStr);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.practice_management.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.PracticeCaptureDetails;
import neo.manager.Security;

/**
 *
 * @author Johan-NB
 */
public class SavePracticeDetails extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        PrintWriter out = null;
        NeoManagerBean port = service.getNeoManagerBeanPort();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        try {
            out = response.getWriter();

            String pracType = "" + session.getAttribute("pracType");

            String disc = "" + session.getAttribute("disc");
            String subDisc = "" + session.getAttribute("subDisc");
            String pracNo = "" + session.getAttribute("pracNo");
            String provNo = "" + session.getAttribute("provNo");
            String pracName = "" + session.getAttribute("pracName");

            String councilNo = "" + session.getAttribute("councilNo");
            String title = "" + session.getAttribute("title");
            String initial = "" + session.getAttribute("initial");
            String firstName = "" + session.getAttribute("firstName");
            String surname = "" + session.getAttribute("surname");
            String idNo = "" + session.getAttribute("idNo");
            String gpInd = "" + session.getAttribute("gpInd");
            String nop = "" + session.getAttribute("nop");
            String cashPractice = "" + session.getAttribute("cashPractice");

            PracticeCaptureDetails pcd = new PracticeCaptureDetails();
            //set user detail
            Security sec = new Security();
            sec.setSecurityGroupId(user.getSecurityGroupId());
            sec.setCreatedBy(user.getUserId());
            sec.setLastUpdatedBy(user.getUserId());
            pcd.setSecurity(sec);

            //set practice details
            pcd.setPracticeType(pracType);
            pcd.setDiscipline(disc);
            if (subDisc == null || subDisc.trim().equals("")) {
                subDisc = "000";
            }
            pcd.setSubDiscipline(subDisc);
            pcd.setPracticeNo(pracNo);
            pcd.setProviderNo(provNo);
            pcd.setPracticeName(pracName);
            pcd.setGroupInd(new Integer(gpInd));
            pcd.setNumberOfPartners(new Integer(nop));
            pcd.setCashPractice(new Integer(cashPractice));
            
            String toDate = request.getParameter("effectiveDate");
            Date tTo = null;
            
            Pattern pattern = Pattern.compile("\\s");
            Matcher matcher = pattern.matcher(toDate);
            boolean found = matcher.find();

            try {
                if (!found) {
                    tTo = dateFormat.parse(toDate);
                } else {
                    tTo = dateTimeFormat.parse(toDate);
                }
                
                XMLGregorianCalendar xmlFromDate = DateTimeUtils.convertDateToXMLGregorianCalendar(tTo);
                pcd.setEffectiveDate(xmlFromDate);
                //eventTask.setTaskDueDate(xmlFromDate);
                //Date fromDate = DateTimeUtils.convertFromYYYYMMDD(scheduleDate);
            } catch (ParseException ex) {
                Logger.getLogger(SavePracticeDetails.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (pracType.trim().equals("1")) {
                pcd.setCouncilNo(councilNo);
                pcd.setProviderTitle(new Integer(title));
                pcd.setProviderInitials(initial);
                pcd.setProviderFirstName(firstName);
                pcd.setProviderSurname(surname);
                pcd.setProviderIdNo(idNo);
            }

            int practiceSaved = port.savePracticeDetails(pcd);

            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (practiceSaved != 0) {
                request.setAttribute("provNum", provNo);
                request.setAttribute("element", "provNum");

                new FindPracticeDetails().getPSDByNumber(provNo, "provNum", session);
                
                out.println("<td><label class=\"header\">Practice " + pracNo + " saved.</label></td>");
                clearScreenFromSession(request);
                
                response.setHeader("Refresh", "2; URL=/ManagedCare/PracticeManagement/PracticeDetails.jsp");
            } else {
                out.println("<td><label class=\"header\">Failed to save practice.</label></td>");
                
                response.setHeader("Refresh", "2; URL=/ManagedCare/SystemAdmin/PracticeCapture.jsp");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

        } catch (Exception ex) {
            System.out.println("SavePracticeDetails error : " + ex.getMessage());
        } finally {
            out.close();
        }
        
        return null;
    }

    @Override
    public String getName() {
        return "SavePracticeDetails";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import com.koh.command.NeoCommand;
import com.koh.preauth.utils.ERPUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.FormatUtils;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author johanl
 */
public class GetHospitalLengthOfStayDate extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        PrintWriter out = null;
        try {
            out = response.getWriter();

            double los = Double.parseDouble(request.getParameter("hospLos"));
            double appLos = Double.parseDouble(request.getParameter("appHospLos"));
            String locFrom = request.getParameter("dateFrom");
            String locType = request.getParameter("locType");

//            Date locFromDate = null;
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                Pattern pattern = Pattern.compile("\\s");
                Matcher matcher = pattern.matcher(locFrom);
                boolean found = matcher.find();

                Date tFrom = null;
            try {
//                locFromDate = sdf.parse(locFrom);
                if (!found) {
                    tFrom = dateFormat.parse(locFrom);
                } else {
                    tFrom = dateTimeFormat.parse(locFrom);
                }
            } catch (ParseException px) {
                px.printStackTrace();
            }

            Date newDate = DateTimeUtils.calculateLOCEndDateFromLOS(tFrom, los);

            //get ward tariff
            String pICD = "" + session.getAttribute("primaryICD_text");
            String admission = "" + session.getAttribute("admissionDateTime");
            String facility = "" + session.getAttribute("facilityProv_text");
            String facilityDisc = "" + session.getAttribute("facilityProvDiscTypeId");
            String prodId = "" + session.getAttribute("scheme");
            String optId = "" + session.getAttribute("schemeOption");

            int provDesc = Integer.parseInt(facilityDisc);
            int pID = Integer.parseInt(prodId);
            int oID = Integer.parseInt(optId);
            int wardLookup = getWARDLookupID(provDesc);

            //get loc type tariff
            String code = NeoCommand.service.getNeoManagerBeanPort().getValueFromCodeTableForTableId(wardLookup, locType);

            Date aDate = null;
            XMLGregorianCalendar xAdmission = null;

            try {
                aDate = FormatUtils.dateTimeFormat.parse(admission);
                xAdmission = DateTimeUtils.convertDateToXMLGregorianCalendar(aDate);

            } catch (ParseException ex) {
                Logger.getLogger(GetHospitalLengthOfStayDate.class.getName()).log(Level.SEVERE, null, ex);
            }
            double estimatedWardAmount = ERPUtils.getEstimatedCostByLOSForWard(xAdmission, pID, oID, code, facilityDisc, facility, pICD, appLos);
            session.setAttribute("estimatedCost", FormatUtils.decimalFormat.format(estimatedWardAmount));

            String dateStr = dateTimeFormat.format(newDate);
            out.println("Done|" + dateStr + "|" + estimatedWardAmount);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetHospitalLengthOfStayDate";
    }

    public int getWARDLookupID(int providerType) {
        int wardId = 280;
        if (providerType == 79) {
            wardId = 281;

        } else if (providerType == 55) {
            wardId = 265;

        } else if (providerType == 59) {
            wardId = 266;

        } else if (providerType == 49) {
            wardId = 267;

        } else if (providerType == 76) {
            wardId = 268;

        } else if (providerType == 47) {
            wardId = 269;

        } else if (providerType == 56) {
            wardId = 270;

        } else if (providerType == 77) {
            wardId = 271;

        }
        return wardId;
    }
}

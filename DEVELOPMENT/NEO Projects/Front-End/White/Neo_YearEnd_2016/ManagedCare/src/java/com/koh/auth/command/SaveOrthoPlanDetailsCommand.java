/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.OrthodonticPlanDetails;

/**
 *
 * @author whauger
 */

public class SaveOrthoPlanDetailsCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        try {
            HttpSession session = request.getSession();

            //Save orthodontic plan details
            OrthodonticPlanDetails oplan = new OrthodonticPlanDetails();

            String authNumber = "" + session.getAttribute("authNumber");
            oplan.setAuthNumber(authNumber);
            oplan.setAuthDate(this.webserviceConvert("" + session.getAttribute("authDate"), "yyyy/MM/dd"));
            oplan.setCoverNumber("" + session.getAttribute("memNo_text"));
            String memberInfo = "" + session.getAttribute("memNo");
            String[] tokens = memberInfo.split("\\s");
            int num = tokens.length;
            oplan.setDependentNumber(new Integer(tokens[num - 1].substring(1)));
            oplan.setProviderNumber("" + session.getAttribute("treatingProv_text"));
            oplan.setTariffCode("" + session.getAttribute("tariff"));
            oplan.setDuration(new Integer("" + session.getAttribute("numMonths")).intValue());
            oplan.setTotalAmount(new Double("" + session.getAttribute("amountClaimed")).doubleValue());
            oplan.setDeposit(new Double("" + session.getAttribute("depositAmount")).doubleValue());
            oplan.setFirstInstallment(new Double("" + session.getAttribute("firstAmount")).doubleValue());
            oplan.setMonthlyInstallment(new Double("" + session.getAttribute("remainAmount")).doubleValue());
            oplan.setPlanStartDate(this.webserviceConvert("" + session.getAttribute("authFromDate"), "yyyy/MM/dd"));
            oplan.setPlanEstimatedEndDate(this.webserviceConvert("" + session.getAttribute("authToDate"), "yyyy/MM/dd"));

            boolean success = service.getNeoManagerBeanPort().insertOrthodonticPlan(oplan);

            //Create response page
            PrintWriter out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (authNumber != null && !authNumber.equalsIgnoreCase("") && success)
            {
                out.println("<td><label class=\"header\">Authorisation " + authNumber + " saved.</label></td>");
                this.clearAllFromSession(request);
            }
            else
            {
                out.println("<td><label class=\"header\">Authorisation saving failed.</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public String getName() {
        return "SaveOrthoPlanDetailsCommand";
    }

    private XMLGregorianCalendar webserviceConvert(String date, String format)
    {
        XMLGregorianCalendar xmlCal = null;
        try {
            Date objDate = new SimpleDateFormat(format).parse(date);
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(objDate);
            xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return xmlCal;
    }

}

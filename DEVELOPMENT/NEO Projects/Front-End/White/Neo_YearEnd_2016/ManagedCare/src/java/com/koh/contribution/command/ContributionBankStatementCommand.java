/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.contribution.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Exception;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class ContributionBankStatementCommand extends NeoCommand {

    
    static final int PAGE_SIZE = 10;
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            String s = request.getParameter("command");
            if (s != null && !s.isEmpty()) {
                System.out.println("command : " + s);
                if (s.equalsIgnoreCase("Search")) {
                    search(request, response, context);
                } else if (s.equalsIgnoreCase("Select")) {
                    select(request, response, context);
                } else if (s.equalsIgnoreCase("FindEmployer")) {
                    findEmployer(request, response, context);
                } else if (s.equalsIgnoreCase("SearchEmployer")) {
                    searchEmployer(request, response, context);
                } else if (s.equalsIgnoreCase("EmployerSearch")) {
                    employerSearch(request, response, context);
                } else if (s.equalsIgnoreCase("FindMember")) {
                    findMember(request, response, context);
                } else if (s.equalsIgnoreCase("SearchMember")) {
                    searchMember(request, response, context);
                } else if (s.equalsIgnoreCase("MemberSearch")) {
                    memberSearch(request, response, context);
                } else if (s.equalsIgnoreCase("SplitGroup")) {
                    groupSplit(request, response, context);
                } else if (s.equalsIgnoreCase("GroupSplitSearch")) {
                    groupSplitSearch(request, response, context);
                } else if (s.equalsIgnoreCase("AddSplitGroup")) {
                    addGroupSplit(request, response, context);
                } else if (s.equalsIgnoreCase("Save")) {
                    save(request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(ContributionBankStatementCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void save(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Map pMap = request.getParameterMap();
        List<Map<String,String>> aList = new ArrayList<Map<String,String>>();
        String statusPrefix = "BSStatusInd_";
        String typePrefix = "BSEType_";
        String idPrefix = "BSEId_";
        for ( Object p : pMap.keySet()) {
            String key = p.toString();
//            System.out.println(key + " = " +request.getParameter(key).toString());
            if (key != null && key.startsWith(statusPrefix)) {
                String statusInd = request.getParameter(key);
                if (statusInd != null && !statusInd.isEmpty() && (statusInd.equals("A") || statusInd.equals("R")|| statusInd.equals("P"))) {
                    String idParts = key.substring(statusPrefix.length());
                    String[] ids = idParts.split("\\_");
                    if (ids != null && ids.length > 1) {
                        String entityId = request.getParameter(idPrefix+idParts);
                        String entityType = request.getParameter(typePrefix+idParts);
//                        int id = getIntValue(entityId);
                        if ((statusInd.equals("R") || statusInd.equals("P") ) || (entityId  != null && entityType != null && (entityType.equals("M") || entityType.equals("E")  || entityType.equals("ES") ))) {
                            Map<String,String> map = new HashMap<String, String>();
                            map.put("entityId", entityId);
                            map.put("entityType", entityType);
                            map.put("statusInd", statusInd);
                            map.put("statementId", ids[0]);
                            map.put("lineId", ids[1]);
                            map.put("date", request.getParameter("BSDate_"+idParts));
                            map.put("amount", request.getParameter("BSAmount_"+idParts));
                            map.put("coverNo", request.getParameter("BSCover_"+idParts));
                            aList.add(map);
                        } else {
                            System.out.println("Invalid values : part - "  + idParts + " status - " + statusInd + " id - " + entityId + " type - " + entityType);
                        }
                    } else {
                        System.out.println("Invalid parts : " + idParts);
                    }
                } else {
                    System.out.println(key + " is empty or null");
                }
            }
        }
        if (aList.size() > 0) {
            List<neo.manager.KeyValueArray> keyValueArray = MapUtils.getKeyValueArray(aList);
            NeoUser neoUser = (NeoUser)request.getSession().getAttribute("persist_user");

            Security sec = new Security();
            sec.setCreatedBy(neoUser.getUserId());
            sec.setLastUpdatedBy(neoUser.getUserId());
            sec.setSecurityGroupId(2);
            port.allocateBankStatements(keyValueArray, sec);
        }
        System.out.println("Allocation:");
        for (Map m : aList) {
            for (Object s : m.keySet()) {
                System.out.print(s + "\t" + m.get(s));
            }
            System.out.println();
//            System.out.println("id - " + m.get("entityId") + "\t type - " + m.get("entityType") + "\t status - " + m.get("statusInd"));
        }
    }
    
    
    private void search(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String suspParam = request.getParameter("includeSuspense");
        List<KeyValueArray> kva = port.fetchUnassignedContributionBankStatementList(suspParam);
        Object col = MapUtils.getMap(kva);                        
        request.setAttribute("BankStatementSearchResults", col);
        String suspType;
        if (suspParam != null) {
            suspType = "R".equalsIgnoreCase(suspParam) ? "Suspense" : "P".equalsIgnoreCase(suspParam) ? "Persal - Suspense" : "Unallocated";
        } else {
            suspType = "";
        }
        request.setAttribute("suspType", suspType);
        context.getRequestDispatcher("/Contribution/BankStatementListResults.jsp").forward(request, response);
    }
    
    private void select(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String suspParam = request.getParameter("includeSuspense");
        int statementId = getIntParam(request, "statementId");
        int page = getIntParam(request, "pageNum");
        page = page == 0 ? 1 : page;
        int start = (page * PAGE_SIZE)+1 - PAGE_SIZE;
        int end = (page * PAGE_SIZE);
        System.err.println ("Statement Id " + statementId);
        List<ContributionBankStatement> contributionBankStatement = port.fetchContributionBankStatementByStatementId(statementId, start, end,suspParam);
        int max = 0;
        if (contributionBankStatement != null) {
            if (contributionBankStatement.size() > PAGE_SIZE) {
                contributionBankStatement.remove(contributionBankStatement.size()-1);
                request.setAttribute("lastPage", "no");
            } else {
                request.setAttribute("lastPage", "yes");
            }
        } else {
            request.setAttribute("lastPage", "yes");
        }
        request.setAttribute("pageVal", page);
        request.setAttribute("BankStatement", contributionBankStatement);
        request.setAttribute("statementId", statementId);
        if (page == 1) {
            request.setAttribute("includeSuspense",request.getParameter("includeSuspense"));
            String suspType;
            if (suspParam != null) {
                suspType = "R".equalsIgnoreCase(suspParam) ? "Suspense" : "P".equalsIgnoreCase(suspParam) ? "Persal - Suspense" : "";
            } else {
                suspType = "";
            }
            request.setAttribute("suspType", suspType);
            context.getRequestDispatcher("/Contribution/BankStatementAllocation_1.jsp").forward(request, response);
        } else {
            context.getRequestDispatcher("/Contribution/BankStatementAllocationPage.jsp").forward(request, response);
        }
    }

    private void findEmployer(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String number = request.getParameter("number");
        String prod = request.getParameter("productId");
        String id = prod == null? number : number + "_" + prod;
        KeyValueArray group = port.findGroupByGroupNumber(id);
        setBankStatementSearchResults(request, response, context, group, "E", "");
    }
    
    private void findMember(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String number = request.getParameter("number");
        String prod = request.getParameter("productId");
        String id = prod == null? number : number + "_" + prod;
        KeyValueArray member = port.findMemberByCoverNumber(id);
        setBankStatementSearchResults(request, response, context, member, "M", number);
    }
    
    private void searchEmployer(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("bsStatementId", request.getParameter("bsStatementId"));
        request.setAttribute("bsLineId", request.getParameter("bsLineId"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("entityForm", request.getParameter("entityForm"));
        context.getRequestDispatcher("/Contribution/BSEmployerSearch.jsp").forward(request, response);
    }
    
    private void employerSearch(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        request.setAttribute("bsStatementId", request.getParameter("bsStatementId"));
        request.setAttribute("bsLineId", request.getParameter("bsLineId"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("entityForm", request.getParameter("entityForm"));
        String product = request.getParameter("scheme");
        String name = request.getParameter("groupName");
        String number = request.getParameter("groupNumber");
        List<KeyValueArray> kva = port.getEmployerList(name, number, Integer.parseInt(product));
        Object col = MapUtils.getMap(kva);
        request.setAttribute("EmployerSearchResults", col);
        context.getRequestDispatcher("/Contribution/BSEmployerSearchResults.jsp").forward(request, response);
    }
    
    private void searchMember(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("bsStatementId", request.getParameter("bsStatementId"));
        request.setAttribute("bsLineId", request.getParameter("bsLineId"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        request.setAttribute("target_div", request.getParameter("target_div"));
        context.getRequestDispatcher("/Contribution/BSMemberSearch.jsp").forward(request, response);
    }
    
    private void memberSearch(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        request.setAttribute("bsStatementId", request.getParameter("bsStatementId"));
        request.setAttribute("bsLineId", request.getParameter("bsLineId"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        request.setAttribute("target_div", request.getParameter("target_div"));
        String coverNumber = request.getParameter("memNo");
        String idNumber = request.getParameter("idNo");
        String initials = request.getParameter("initials");
        String surname = request.getParameter("surname");
        String dob = request.getParameter("dob");

        CoverSearchCriteria cs = new CoverSearchCriteria();

        //cover number set
        if (coverNumber != null && !coverNumber.trim().equals("")
                && !coverNumber.trim().equalsIgnoreCase("null")) {
            cs.setCoverNumber(coverNumber);
        }
        //id number set
        if (idNumber != null && !idNumber.trim().equals("")
                && !idNumber.trim().equalsIgnoreCase("null")) {
            cs.setCoverIDNumber(idNumber);
        }
        //initials set
        if (initials != null && !initials.trim().equals("")
                && !initials.trim().equalsIgnoreCase("null")) {
            cs.setCoverInitial(initials);
        }
        //surname set
        if (surname != null && !surname.trim().equals("")
                && !surname.trim().equalsIgnoreCase("null")) {
            cs.setCoverSurname(surname);
        }
        //date of birth set
        if (dob != null && !dob.trim().equals("")
                && !dob.trim().equalsIgnoreCase("null")) {

            Date cDOB = null;
            XMLGregorianCalendar xDOB = null;
            try {
                cDOB = DateTimeUtils.dateFormat.parse(dob);
                xDOB = DateTimeUtils.convertDateToXMLGregorianCalendar(cDOB);

            } catch (ParseException px) {
                px.printStackTrace();
            }
            cs.setDateOfBirth(xDOB);
        }
        List<CoverDetails> covList = port.getCoverPrincipleList(cs);
        request.setAttribute("MemberSearchResults", covList);
        context.getRequestDispatcher("/Contribution/BSMemberSearchResults.jsp").forward(request, response);
    }
    
    private void groupSplit(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("bsStatementId", request.getParameter("bsStatementId"));
        request.setAttribute("bsLineId", request.getParameter("bsLineId"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("bsLineAmount", request.getParameter("bsLineAmount"));
        request.setAttribute("statementDate", request.getParameter("statementDate"));
        request.setAttribute("bsLineDescription", request.getParameter("bsLineDescription"));
        context.getRequestDispatcher("/Contribution/BSSplitGroup.jsp").forward(request, response);
    }
    
    private void groupSplitSearch(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        request.setAttribute("bsStatementId", request.getParameter("bsStatementId"));
        request.setAttribute("bsLineId", request.getParameter("bsLineId"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        request.setAttribute("target_div", request.getParameter("target_div"));
        String product = request.getParameter("scheme");
        String name = request.getParameter("groupName");
        String number = request.getParameter("groupNumber");
        List<KeyValueArray> kva = port.getEmployerList(name, number, Integer.parseInt(product));
        Object col = MapUtils.getMap(kva);
        request.setAttribute("EmployerSearchResults", col);
        context.getRequestDispatcher("/Contribution/BSSplitEmployerSearchResult.jsp").forward(request, response);
    }
    
    private void addGroupSplit(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        List<Map<String,Object>> empList = new ArrayList<Map<String,Object>>();
        Map params = request.getParameterMap();
        String addGroupId = request.getParameter("addGroupId");
        for (Object keyObj : params.keySet()) {
            String key = keyObj.toString();
            if (key.startsWith("split_id_")) {
                String entityId = key.substring(key.lastIndexOf("_")+1);
                if (entityId.equals(addGroupId)) {
                    addGroupId = null;
                }
                System.out.println("entityId " + entityId);
                Map<String,Object> map = new HashMap<String,Object>();
                map.put("entityId", entityId);
                map.put("employerNumber", request.getParameter("split_number_" + entityId));
                map.put("employerName", request.getParameter("split_name_" + entityId));
                map.put("amount", request.getParameter("split_id_" + entityId));
                empList.add(map);
            }
        }
        if (addGroupId != null) {
            Map<String,Object> map = new HashMap<String,Object>();
            map.put("entityId", request.getParameter("addGroupId"));
            map.put("employerNumber", request.getParameter("addGroupNumber"));
            map.put("employerName", request.getParameter("addGroupName"));
            empList.add(map);
        }
        request.setAttribute("SplitGroupList", empList);
        context.getRequestDispatcher("/Contribution/BSSplitGroupDetails.jsp").forward(request, response);
    }
    
    private void setBankStatementSearchResults(HttpServletRequest request, HttpServletResponse response, ServletContext context, KeyValueArray kva, String type, String coverNumber) {
        String result = "";
        String sId = request.getParameter("statementId");
        String lId = request.getParameter("lineId");
        String entityId = "";
        String entityName = "";
        if (kva != null) {
            Map<String, String> map = MapUtils.getMap(kva);
            entityId = map.get("entityId") == null ? "" : map.get("entityId");
            entityName = map.get("entityName") == null ? "" : map.get("entityName");
        }
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("BSELbl_" + sId + "_" + lId, entityName);
        fieldsMap.put("BSEId_" + sId + "_" + lId, entityId);
        fieldsMap.put("BSEType_" + sId + "_" + lId, type);
        fieldsMap.put("BSStatusInd_" + sId + "_" + lId, entityId.isEmpty() ? "" : "A");
        fieldsMap.put("BSCover_" + sId + "_" + lId, coverNumber);
        String updateFields = TabUtils.convertMapToJSON(fieldsMap);
        result = TabUtils.buildJsonResult("OK", null, updateFields, null);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println(result);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (out != null) out.close();
        }
    }
    
    private int getIntParam(HttpServletRequest request, String paramName) {
        try {
            String maxDepStr = request.getParameter(paramName);
            return Integer.parseInt(maxDepStr);
        } catch (java.lang.Exception e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }
    
    private int getIntValue(String s) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            return 0;
        }
    }
    

    @Override
    public String getName() {
        return "ContributionBankStatementCommand";
    }
    
}

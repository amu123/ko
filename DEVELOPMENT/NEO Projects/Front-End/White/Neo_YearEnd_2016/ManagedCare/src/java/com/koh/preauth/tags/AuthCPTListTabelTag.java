/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthCPTDetails;

/**
 *
 * @author johanl
 */
public class AuthCPTListTabelTag extends TagSupport {

    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {

            out.println("<td colspan=\"6\"><table width=\"600\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            List<AuthCPTDetails> cptList = (List<AuthCPTDetails>) session.getAttribute(sessionAttribute);
            if (cptList != null) {
                out.println("<tr><th>CPT Code</th><th>CPT Discription</th><th>Theatre Time</th><th>Theatre Date</th><th>Procedure Link</th><th></th><th></th></tr>");
                for (int i =1; i <= cptList.size(); i++){
                    AuthCPTDetails cpt = cptList.get(i-1);

                    String procLink = NeoCommand.service.getNeoManagerBeanPort().getValueFromCodeTableForTableId(126, cpt.getProcedureLink());
                    String theatreDate = DateTimeUtils.convertXMLGregorianCalendarToDate(cpt.getTheatreDate());

                    out.println("<tr id=\"cptRow"+ cpt.getCptCode() + i +"\"><form>");
                    out.println("<td><label class=\"label\">" + cpt.getCptCode() + "</label><input type=\"hidden\" name=\"cptCode\" id=\"cptCode\" value=\"" + cpt.getCptCode() + "\"/></td>");
                    out.println("<td width=\"200\"><label class=\"label\">" + cpt.getCptCodeDesc() + "</label><input type=\"hidden\" name=\"cptDesc\" id=\"cptDesc\" value=\"" + cpt.getCptCodeDesc() + "\"/></td>");
                    out.println("<td><label class=\"label\">" + cpt.getTheatreTime() + "</label></td>");
                    out.println("<td><label class=\"label\">" + theatreDate + "</label></td>");
                    out.println("<td><label class=\"label\">" + procLink + "</label></td>");
                    out.println("<td><button type=\"button\" onClick=\"ModifyCPTList(" + i + ");\" >Modify</button></td>");
                    out.println("<td><button type=\"button\" onClick=\"RemoveCPTFromList(" + i + ");\" >Remove</button></td>");
                    out.println("</form></tr>");
                }
            }
            out.println("</table></td>");


        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberSearchTable tag", ex);
        }

        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

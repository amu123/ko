/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.tags;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author josephm
 */
public class BiometricsTarrifTag extends TagSupport {
    private String elementName;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
     @Override
    public int doEndTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            ServletRequest req = pageContext.getRequest();
            HttpSession session = pageContext.getSession();
            out.println("<td colspan=\"6\">" +
                    "<label class=\"subheader\">Allocate Tariff to Biometrics</label>" +
                    "<table width=\"500\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
          //  if (!elementName.trim().equalsIgnoreCase("")) {
                out.println("<tr><th>Tariff Code</th><th colspan=\"2\">Tariff Description</th>" +
                        "<th>Test Results</th></tr>");
                out.println("<tr><form>" +
                        "<td><input type=\"text\" id=\"tariffCode\" name=\"tariffCode\" size=\"15\" onChange=\"getTariffForCode(this.value, 'tariffCode');\" /></td>" +
                        "<td><img src=\"/ManagedCare/resources/Search.gif\" width=\"28\" height=\"28\" alt=\"Search\" border=\"0\" onClick=\"getTariffForCode(this.value, 'tariffCode');\" /></td>" +
                        "<td><input type=\"text\" id=\"tariffDescription\" name=\"tariffDescription\" size=\"40\" readonly=\"readonly\"/></td>" +
                      //  "<td><input type=\"text\" id=\"testResults\" name=\"testResults\" size=\"15\"/></td>" +
                       // "<td><input type=\"text\" id=\"tariffAmount\" name=\"tariffAmount\" size=\"15\" disabled=\"disabled\"/></td>" +
                        "</tr>");

                out.println("<tr><td colspan=\"6\" align=\"right\"><button type=\"submit\" " + javaScript + " name=\"opperation\" value=\"" + commandName + "\">Add Tariff</button></td></form></tr>");
                System.out.println("The command name is " + commandName);
             

          //  }
            out.println("</table></td>");
        } catch (IOException ex) {
            Logger.getLogger(BiometricsTarrifTag.class.getName()).log(Level.SEVERE, null, ex);
        }
        return super.doEndTag();
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

}

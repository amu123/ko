/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.tags;

import com.koh.command.NeoCommand;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CoverDetails;
import neo.manager.EAuthCoverDetails;
import neo.manager.NeoManagerBean;
import neo.manager.Option;
import neo.manager.PersonDetails;

/**
 *
 * @author charlh
 */
public class MemberNominatedPractitionerHistoryTable extends TagSupport {

    private String javaScript;
    private String commandName;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {

        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        ServletContext sctx = pageContext.getServletContext();
        String client = String.valueOf(sctx.getAttribute("Client"));
        try {

//            int memberEntityCommon = (Integer) session.getAttribute("memberEntityCommon");
            Object memnum = session.getAttribute("memberCoverNumber");
            CoverDetails coverDetails = port.findPrincipalForDepCoverNumber(memnum.toString());
            
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            if (coverDetails != null) {
                //out.println("<label class=\"header\">Dependants</label></br></br>");
                out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.println("<tr>"
                        + "<th align=\"left\">Member Name</th>"
                        + "<th align=\"left\">Member Surname</th>"
                        + "<th align=\"left\">Member Code</th>"
                        + "<th align=\"left\">Doctor Name</th>"
                        + "<th align=\"left\">BHF Practice Number</th>"
                        + "<th align=\"left\">Network</th>"
                        + "<th align=\"left\">Start Date</th>"
                        + "<th align=\"left\">End Date</th>"
                        + "<th align=\"left\">Status</th>"
                        + "</tr>");

//                for (int i = 0; i < members.size(); i++) {
//                    //CoverDetails cd = members.get(i);
//                    EAuthCoverDetails cd = members.get(i);
//                    
//                    if(!members.get(i).getDependantType().equals("Principal Member") && (cd.getCoverEndDate().toGregorianCalendar().before(cd.getCoverStartDate().toGregorianCalendar()))){
//                        continue;
//                    }
//                    //Option opt = port.findOptionWithID(cd.getOptionId());
//                    Option opt = port.findOptionWithID(Integer.parseInt(cd.getOptionLookup().getId()));
//
//                    String dateOfBirth = "";
//                    String joinDate = "";
//                    String benefitDate = "";
//                    String resignDate = "";
//
//                    int entityId = cd.getCoverEntityId();
//                    System.out.println("cover: " + cd.getCoverNumber() + " dep:" + cd.getDependentNumber() + " eId = " + cd.getCoverEntityId());
//                    PersonDetails pd = port.getPersonDetailsByEntityId(entityId);
//
//                    //dob
//                    if (pd.getDateOfBirth() != null) {
//                        dateOfBirth = dateFormat.format(pd.getDateOfBirth().toGregorianCalendar().getTime());
//                    }
//
//                    //benefit start date
//                    if (cd.getCoverStartDate() != null) {
//                        benefitDate = dateFormat.format(cd.getCoverStartDate().toGregorianCalendar().getTime());
//
//                    }
//
//                    //join date
//                    if (cd.getJoinDate() != null) {
//                        joinDate = dateFormat.format(cd.getJoinDate().toGregorianCalendar().getTime());
//                    } else {
//                        joinDate = dateFormat.format(cd.getCoverStartDate().toGregorianCalendar().getTime());
//                    }
//
//                    //resign date
//                    if (cd.getCoverEndDate() != null) {
//                        String dateTo = new SimpleDateFormat("yyyy/MM/dd").format(cd.getCoverEndDate().toGregorianCalendar().getTime());
//                        if (!dateTo.equalsIgnoreCase("2999/12/31")) {
//                            resignDate = dateTo;
//                        }
//
//                    }
//                    String rowColor = "<tr>";
//                    if(cd.getCoverStatus().equalsIgnoreCase("Suspend") && client.equalsIgnoreCase("Sechaba")){
//                        rowColor = "<tr style=\"background-color: #ff4d4d\">";
//                    }
//                    out.println(rowColor
//                            + "<td><label class=\"label\">" + opt.getOptionName() + "</label></td>"
//                            + "<td><label class=\"label\">" + cd.getDependentNumber() + "</label></td>"
//                            + "<td><label class=\"label\">" + pd.getName() + "</label></td>"
//                            + "<td><label class=\"label\">" + pd.getSurname() + "</label></td>"
//                            + "<td><label class=\"label\">" + cd.getDependantType() + "</label></td>"
//                            + "<td><label class=\"label\">" + dateOfBirth + "</label></td>"
//                            + "<td><label class=\"label\">" + pd.getGenderDesc() + "</label></td>"
//                            + "<td><label class=\"label\">" + pd.getIDNumber() + "</label></td>"
//                            + "<td><label class=\"label\">" + cd.getCoverStatus() + "</label></td>"
//                            + "<td><label class=\"label\">" + joinDate + "</label></td>"
//                            + "<td><label class=\"label\">" + benefitDate + "</label></td>"
//                            + "<td><label class=\"label\">" + resignDate + "</label></td>"
//                            + "</tr>");
//                }
            }


            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberNominatedPractitionerHistoryTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }
}
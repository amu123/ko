/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CoverDetails;
import neo.manager.CoverSearchCriteria;
import neo.manager.EAuthCoverDetails;
import neo.manager.NeoUser;
import neo.manager.PreAuthSearchCriteria;
import neo.manager.PreAuthSearchResults;
import neo.manager.SecurityResponsibility;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class PreAuthDetailsCommand extends NeoCommand {

    private Logger log = Logger.getLogger(PreAuthDetailsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);

        log.info("Inside PreAuthDetailsCommand");
        HttpSession session = request.getSession();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        log.info("The user id is " + user.getUsername());
        log.info("The user id is " + user.getUserId());

        String authNumber = request.getParameter("authNo");
        String authType = request.getParameter("authType");
        String authDate = request.getParameter("authDate");
        String coverNumber = request.getParameter("memberNum_text");
        String depNumber = request.getParameter("depListValues");
        String name = request.getParameter("memberAuthFirstName");
        String surname = request.getParameter("memberAuthSurname");
        String product = request.getParameter("scheme");
        String option = request.getParameter("schemeOption");

        log.info("LOgging info");
        log.info("The auth number is " + authNumber);
        log.info("The auth type is " + authType);
        log.info("The auth date " + authDate);
        log.info("The cover number is " + coverNumber);
        log.info("The dependenat number is " + depNumber);
        log.info("The name is " + name);
        log.info("The surname is " + surname);
        log.info("The product is " + product);
        log.info("The option " + option);

        PreAuthSearchCriteria preAuthSearch = new PreAuthSearchCriteria();

        if (authNumber != null && !authNumber.trim().equalsIgnoreCase("")) {

            preAuthSearch.setAuthNumber(authNumber);
        }

        if (authType != null && !authType.trim().equalsIgnoreCase("99")) {

            preAuthSearch.setAuthType(authType);
        }

        if (authDate != null && !authDate.trim().equalsIgnoreCase("")) {
            XMLGregorianCalendar aDate = null;
            try {
                aDate = DateTimeUtils.convertDateToXMLGregorianCalendar(dateFormat.parse(authDate));
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            if (aDate != null) {
                preAuthSearch.setAuthDate(aDate);
            }
        }

        if (coverNumber != null && !coverNumber.trim().equalsIgnoreCase("")) {

            preAuthSearch.setCoverNumber(coverNumber);
        }

        if (depNumber != null && !depNumber.trim().equalsIgnoreCase("")) {

            preAuthSearch.setDependantCode(Integer.parseInt(depNumber));
        }

        if (name != null && !name.trim().equalsIgnoreCase("")) {

            preAuthSearch.setFirstName(name);
        }

        if (surname != null && !surname.trim().equalsIgnoreCase("")) {

            preAuthSearch.setLastName(surname);
        }

        if (product != null && !product.trim().equalsIgnoreCase("99")) {

            preAuthSearch.setScheme(product);
        }

        if (option != null && !option.trim().equalsIgnoreCase("99")) {

            preAuthSearch.setSchemeOption(option);
        }
        log.info("The auth number from the object " + preAuthSearch.getAuthNumber());
        log.info("The auth type from the object" + preAuthSearch.getAuthType());
        log.info("The auth date from the object" + preAuthSearch.getAuthDate());
        log.info("The cover number from the object" + preAuthSearch.getCoverNumber());
        log.info("The dependenta number from the object is " + preAuthSearch.getDependantCode());
        log.info("The name from the object is " + preAuthSearch.getFirstName());
        log.info("The surname from the object is " + preAuthSearch.getLastName());
        log.info("The product from the object is " + preAuthSearch.getScheme());
        log.info("The option from the object is " + preAuthSearch.getSchemeOption());

        List<PreAuthSearchResults> paResult = service.getNeoManagerBeanPort().findPreAuthDetailsByCriteria(user, preAuthSearch);

        if (paResult.size() == 0) {
            request.setAttribute("authSearchResultsMessage", "No results found.");
        } else if (paResult.size() >= 100) {
            request.setAttribute("authSearchResultsMessage", "Only first 100 results shown. Please refine search.");
        } else {
            request.setAttribute("authSearchResultsMessage", null);
        }
        List<CoverDetails> memberCoverDetailsLst = null;
        Iterator<PreAuthSearchResults> preAuthIter = paResult.iterator();
        while (preAuthIter.hasNext()) {
            memberCoverDetailsLst = service.getNeoManagerBeanPort().getCoverDetailsByCoverNumber(preAuthIter.next().getCoverNumber());
            System.out.println("coverList size:" + memberCoverDetailsLst.size());
            for (CoverDetails coverDetail : memberCoverDetailsLst) {
                if (service.getNeoManagerBeanPort().getMemberCDLStatusDetails(coverDetail.getEntityId(), 1).getRegisteredInd() == 1) {
                    for (SecurityResponsibility userRole : user.getSecurityResponsibility()) {
                        if (userRole.getResponsibilityId() != 2) {
                            session.setAttribute("userRestriction", true);
                            preAuthIter.remove();
                        }
                    }
                }
            }
        }

        session.setAttribute("authSearchResults", paResult);
        log.info("The size fo the preauth list is " + paResult.size());

        try {

            //String nextJSP = "/Claims/MemberDetailsTabbedPage.jsp";
            String nextJSP = "/Claims/PracticePreAuthorisation.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "PreAuthDetailsCommand";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.union.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.Union;
import neo.manager.UnionAddressDetails;

/**
 *
 * @author shimanem
 */
public class UnionViewCommand extends NeoCommand {

    private static final Logger logger = Logger.getLogger(UnionViewCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        String unionNumber = request.getParameter("unionNumber");
        String unionName = request.getParameter("unionName");
        String representativeNumber = String.valueOf(request.getParameter("representativeNumber"));
        String unionID = String.valueOf(request.getParameter("unionID"));

        try {
            logger.info(">> **** Inside UnionViewCommand **** <<<");
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

            logger.log(Level.INFO, ">> *** Union Number : {0}", unionNumber);
            logger.log(Level.INFO, ">> *** Union Name : {0}", unionName);
            logger.log(Level.INFO, ">> *** Union ID : {0}", unionID);
            logger.log(Level.INFO, ">> *** Union Representative Number : {0}", representativeNumber);

            session.setAttribute("unionID", unionID);
            session.setAttribute("unionNumber", unionNumber);
            session.setAttribute("unionName", unionName);
            session.setAttribute("representativeNumber", representativeNumber);

            int repNo = Integer.parseInt(representativeNumber);
            Union union = port.retrieveUnion(repNo);
            UnionAddressDetails addressDetails = port.retrieveUnionAddressDetails(repNo);

            if (union != null) {

                session.setAttribute("unionDetails", union);
                session.setAttribute("altUnionNumber", union.getUnionAlternativeNumber());
                session.setAttribute("repName", union.getRepresentativeName());
                session.setAttribute("repSurname", union.getRepresentativeSurname());
                session.setAttribute("unionRepCapacity", union.getRepresentativeID());
                session.setAttribute("unionRegion", union.getRepresentativeRegionID());
                session.setAttribute("unionInceptionDate", DateTimeUtils.convertToYYYYMMDD(union.getInceptionDate().toGregorianCalendar().getTime()));
                session.setAttribute("unionTerminationDate", DateTimeUtils.convertToYYYYMMDD(union.getTerminationDate().toGregorianCalendar().getTime()));
                // Contacts    
                session.setAttribute("contactNumber", union.getRepresentativeContact());
                session.setAttribute("faxNumber", union.getRepresentativeFaxNumber());
                session.setAttribute("emailAddress", union.getRepresentativeEmail());
                session.setAttribute("altEmailAddress", union.getRepresentativeAlternativeEmail());

                if (addressDetails != null) {
                    session.setAttribute("postLine1", addressDetails.getPostAddressLine1());
                    session.setAttribute("postLine2", addressDetails.getPostAddressLine2());
                    session.setAttribute("postLine3", addressDetails.getPostAddressLine3());
                    session.setAttribute("postLocation", addressDetails.getPostAddressLocation());
                    session.setAttribute("postCode", addressDetails.getPostAddressCode());

                    session.setAttribute("physicalLine1", addressDetails.getPhysicalAddressLine1());
                    session.setAttribute("physicalLine2", addressDetails.getPhysicalAddressLine2());
                    session.setAttribute("physicalLine3", addressDetails.getPhysicalAddressLine3());
                    session.setAttribute("physicalLocation", addressDetails.getPhysicalAddressLocation());
                    session.setAttribute("physicalCode", addressDetails.getPhysicalAddressCode());

                } else {
                    session.setAttribute("postLine1", null);
                    session.setAttribute("postLine2", null);
                    session.setAttribute("postLine3", null);
                    session.setAttribute("postLocation", null);
                    session.setAttribute("postCode", null);

                    session.setAttribute("physicalLine1", null);
                    session.setAttribute("physicalLine2", null);
                    session.setAttribute("physicalLine3", null);
                    session.setAttribute("physicalLocation", null);
                    session.setAttribute("physicalCode", null);
                }

            } else {
                session.setAttribute("unionDetails", null);
                session.setAttribute("altUnionNumber", null);
                session.setAttribute("repName", null);
                session.setAttribute("repSurname", null);
                session.setAttribute("unionRepCapacity", null);
                session.setAttribute("unionRegion", null);
                session.setAttribute("unionInceptionDate", null);
                session.setAttribute("unionTerminationDate", null);
                // Contacts    
                session.setAttribute("contactNumber", null);
                session.setAttribute("faxNumber", null);
                session.setAttribute("emailAddress", null);
                session.setAttribute("altEmailAddress", null);
            }

            context.getRequestDispatcher("/Union/unionDetails.jsp").forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(UnionViewCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UnionViewCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "UnionViewCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.pdc.command.CoverDependantResultCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.BiometricMemDetail;
import neo.manager.CoverDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;

/**
 *
 * @author gerritr
 */
public class LoadBiometricsDetailsCommand extends NeoCommand {
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        NeoManagerBean port = service.getNeoManagerBeanPort();
        // ----------------Lookup ID Dropdown list-------------------
        ArrayList<CoverDetails> covList = (ArrayList<CoverDetails>) session.getAttribute("MemberCoverDetails");
        String coverNumber = covList.get(0).getCoverNumber();
        String depCode = String.valueOf(covList.get(0).getDependentNumber());
        String optionValue = request.getParameter("ID_text");
        
        System.out.println("CoverNumber = " + coverNumber);
        System.out.println("DepNumber = " + depCode);
        
        Collection<LookupValue> mList = new ArrayList<LookupValue>();
        Collection<LookupValue> mListNew = new ArrayList<LookupValue>();
        
        String bioType = "null";
        if(optionValue!= null && optionValue.equals("100") && !coverNumber.isEmpty() && !depCode.isEmpty())
        {
            bioType = "ICD10";
            
            mList = port.getBiometricsCoverNumber(coverNumber, Integer.parseInt(depCode), 100);
            session.setAttribute("bioType", "100");
        }
        else if(optionValue!= null && optionValue.equals("291") && !coverNumber.isEmpty() && !depCode.isEmpty())
        {
            bioType = "Other";
            
            mList = port.getBiometricsCoverNumber(coverNumber, Integer.parseInt(depCode), 291);
            session.setAttribute("bioType", "291");
        }
        
        session.setAttribute("BioTypeSelected", bioType);
        session.removeAttribute("ViewBiometricsInfo");
        
        LookupValue arrIcdList = new LookupValue();
        arrIcdList.setId("0");
        arrIcdList.setValue(" ");
        boolean firstRunFlag = true;
        for (LookupValue icd : mList) {//Insert blank space for the dropdown
            if(firstRunFlag == true){
                mListNew.add(arrIcdList);
                firstRunFlag = false;
            }
            
            
            mListNew.add(icd);
        }
        session.setAttribute("IcdList", mListNew);

        //Creating the tabels for the Biometric Types
        for (LookupValue arrBioTypes : mList) { //Iterate through LookuoValue Dropdown
            Collection<BiometricMemDetail> bioView = new ArrayList<BiometricMemDetail>();
            bioView = port.getBiometricMemDetail(coverNumber, depCode, arrBioTypes.getValue());

            Collection<BiometricMemDetail> arrBmd = new ArrayList<BiometricMemDetail>();
            for (BiometricMemDetail bmd : bioView) //Iterate through Diagnosis Codes
            {
                arrBmd.add(bmd);
            }
            if (arrBmd.size() != 0) {
                session.setAttribute(arrBioTypes.getValue(), arrBmd);
                System.out.println("Session [" + arrBioTypes.getValue() + "] Stored");
            }

        }
        
        PrintWriter out = null;
        try {
            out = response.getWriter();

            if (depCode != null) {
                out.print("Done|");
            } else {
                out.print("Failed|");
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(CoverDependantResultCommand.class
                    .getName()).log(Level.SEVERE, null, ex);
            System.out.print(
                    "Exception PRINTWRITER: " + ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "LoadBiometricsDetailsCommand";
    }
}

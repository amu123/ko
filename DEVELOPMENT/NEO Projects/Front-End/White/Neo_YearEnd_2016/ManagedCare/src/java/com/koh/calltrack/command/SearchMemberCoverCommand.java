/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.calltrack.command;

import com.koh.auth.dto.MemberSearchResult;
import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverDetails;
import neo.manager.CoverSearchCriteria;


/**
 *
 * @author josephm
 */
public class SearchMemberCoverCommand extends NeoCommand {

   @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        //System.out.println("inside SearchMemberCoverCommand");
        this.saveScreenToSession(request);

        String memNo = request.getParameter("memberNumber_text");
        //System.out.println("COVER_NUMBER ="+memNo);

        CoverSearchCriteria search = new CoverSearchCriteria();
        if (memNo != null && !memNo.equals("") && memNo.length() >= 5){
            search.setCoverNumber(memNo);
            this.clearAllFromSession(request);

           // System.out.println("The value of the memberNo is " + memNo);
            ArrayList<MemberSearchResult> members = new ArrayList<MemberSearchResult>();

            List<CoverDetails> pdList = new ArrayList<CoverDetails>();
            try {
               // System.out.println("Did the method reach here");
               pdList = service.getNeoManagerBeanPort().getAllDependentsForCoverToday(memNo);
              // System.out.println("What is the value of of the list " + pdList);




            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        else {

            session.setAttribute("providerNumber_error", "Please enter at least a 5 digit number");
        }

        try {
            String nextJSP = "/Calltrack/LogNewCall.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }


    @Override
    public String getName() {
        return "SearchMemberCoverCommand";
    }
}

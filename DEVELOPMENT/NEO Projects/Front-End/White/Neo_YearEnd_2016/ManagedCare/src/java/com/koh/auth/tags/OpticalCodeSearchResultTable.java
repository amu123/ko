/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.tags;

import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.OpticalCode;

/**
 *
 * @author whauger
 */
public class OpticalCodeSearchResultTable extends TagSupport {
    private String javascript;
    private String commandName;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Discipline</th><th>Code</th><th>Description</th><th>Rules</th></tr>");

            ArrayList<OpticalCode> opticalProtocols = (ArrayList<OpticalCode>) req.getAttribute("searchOpticalCodeResult");
            if (opticalProtocols != null) {
                for (int i = 0; i < opticalProtocols.size(); i++) {
                    OpticalCode proto = opticalProtocols.get(i);
                    out.println("<tr><form><td><label class=\"label\">" + proto.getDisciplineType() + "</label><input type=\"hidden\" name=\"code\" value=\"" + proto.getDisciplineType() + "\"/></td>" +
                            "<td><label class=\"label\">" + proto.getCode() + "</label><input type=\"hidden\" name=\"description\" value=\"" + proto.getCode() + "\"/></td>" +
                            "<td><label class=\"label\">" + proto.getDescription() + "</label><input type=\"hidden\" name=\"description\" value=\"" + proto.getDescription() + "\"/></td>" +
                            //"<td><label class=\"label\">" + proto.getProgessiveTariff() + "</label><input type=\"hidden\" name=\"description\" value=\"" + proto.getProgessiveTariff() + "\"/></td>" +
                            //"<td><label class=\"label\">" + proto.getPrestigeTariff() + "</label><input type=\"hidden\" name=\"description\" value=\"" + proto.getPrestigeTariff() + "\"/></td>" +
                            "<td><label class=\"label\">" + proto.getRules() + "</label><input type=\"hidden\" name=\"description\" value=\"" + proto.getRules() + "\"/></td></form></tr>");
                            //"<td><button name=\"opperation\" type=\"submit name=\"opperation\" type=\"submit\" " + javascript + " value=\"" + commandName + "\">Select</button></td></form></tr>");

                }
            }
            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in OpticalCodeSearchResultTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

}

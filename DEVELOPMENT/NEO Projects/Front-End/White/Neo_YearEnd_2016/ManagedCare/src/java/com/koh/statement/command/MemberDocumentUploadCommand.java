/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import agility.za.indexdocumenttype.DeleteDocType;
import agility.za.indexdocumenttype.IndexDocType;
import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoUser;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author janf
 */
public class MemberDocumentUploadCommand extends NeoCommand {

    static String uploadedFileName = "";
    static String docType = "";

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            HttpSession session = request.getSession();
            String srcDrive = new PropertiesReader().getProperty("DocumentIndexWorkDrive");
            String indexDrive = new PropertiesReader().getProperty("DocumentIndexIndexDrive");
            System.out.println("inMemberDocumentUploadCommand");
            String memberNumber = request.getParameter("memberNumber");
            String entityType = request.getParameter("entityType");
            System.out.println("entityType = " + entityType);
            String folder = "Index/";
            String finalPath = srcDrive + folder;

            boolean isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("isMultipart : " + isMultipart);

            File uploadedFile = null;
            if (isMultipart) {

                // Create a factory for disk-based file items
                // FileItemFactory factory = new DiskFileItemFactory();
                DiskFileItemFactory itemFactory = new DiskFileItemFactory();
                itemFactory.setRepository(uploadedFile);
                itemFactory.setSizeThreshold(2000);

                ServletFileUpload upload = new ServletFileUpload(itemFactory);
//                Map<String, String> paramsMap = new HashMap<String, String>();
//                String fileDetails = "";
//                String fileNames = "";
//
//                FileItemIterator iter = upload.getItemIterator(request);
//                while (iter.hasNext()) {
//                    FileItemStream item = iter.next();
//                    String name = item.getFieldName();
//                    InputStream stream = item.openStream();
//                    if (item.isFormField()) {
//                        paramsMap.put(name, Streams.asString(stream));
//                        System.out.println("Form field " + name + " with value "
//                                + paramsMap.get(name) + " detected.");
//                    } else {
//                        fileDetails = Streams.asString(stream);
//                        fileNames = item.getName();
//                        System.out.println("File field " + name + " with file name "
//                                + item.getName() + " detected.");
//                        System.out.println(fileDetails);
//                    }
//                }
//                docType = paramsMap.get("docuType");
                System.out.println("DocType = " + docType);

                // Parse the request
                List fileItems = upload.parseRequest(request);

                Map<String, String> paramsMap = new HashMap<String, String>();
                String fileDetails = "";
                String fileNames = "";

                FileItemIterator iter = upload.getItemIterator(request);
                while (iter.hasNext()) {
                    FileItemStream item = iter.next();
                    String name = item.getFieldName();
                    InputStream stream = item.openStream();
                    if (item.isFormField()) {
                        paramsMap.put(name, Streams.asString(stream));
                        System.out.println("Form field " + name + " with value "
                                + paramsMap.get(name) + " detected.");
                    } else {
                        fileDetails = Streams.asString(stream);
                        fileNames = item.getName();
                        System.out.println("File field " + name + " with file name "
                                + item.getName() + " detected.");
                        System.out.println(fileDetails);
                    }
                }
                docType = paramsMap.get("docuType");

                if (fileItems != null && !fileItems.isEmpty()) {
                    String fileName = memberNumber + " - Incomming";
                    upload(finalPath, fileItems, uploadedFile, fileName);
                    System.out.println("Starting Indexing");
                    System.out.println("[fr]Filename before = " + fileName);
                    fileName += uploadedFileName.substring(uploadedFileName.lastIndexOf("."));
                    System.out.println("[fr]Filename after = " + fileName);
                    IndexDocument(request, memberNumber, srcDrive, indexDrive, fileName, folder, entityType);
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(MemberDocumentUploadCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    private void upload(String path, List fileItems, File uploadedFile, String fileName) throws Exception {
        Iterator iter = fileItems.iterator();
        while (iter.hasNext()) {

            FileItem item = (FileItem) iter.next();

            if (item.getName() != null && !item.getName().equalsIgnoreCase("")) {
                String itemName = item.getName();
                //String uploadedFileName = itemName.subSequence(itemName.lastIndexOf("/")+1, itemName.length);
                uploadedFileName = FilenameUtils.getName(itemName);

                boolean check = getFileExtention(new File(uploadedFileName));
                System.out.println("uploadedFileName = " + uploadedFileName);
                String ext = uploadedFileName.substring(uploadedFileName.lastIndexOf("."));
                fileName += ext;
                System.out.println("fileName = " + fileName);

                if (check) {
                    String fileUploadLoc = path;
                    System.out.println("uploadedFileName = " + uploadedFileName);
                    System.out.println("uploadedFileLocation = " + fileUploadLoc);
                    System.out.println("fileName = " + fileName);

                    File direcory = new File(fileUploadLoc);
                    if (!direcory.exists()) {
                        System.out.println("Directory not found. Creating....");
                        direcory.mkdirs();
                        System.out.println("Done");
                    } else {
                        System.out.println("Directory exist. not creating.");
                    }

                    //uploadedFile = new File(fileUploadLoc + uploadedFileName);
                    uploadedFile = new File(fileUploadLoc + fileName);

                    System.out.println("Content Type is : " + item.getContentType());

                    // uploadedFile = new File(destination, item.getName());
                    item.write(uploadedFile);
                    System.out.println("Upload completed successfully !!!!!!!!!!");
                } else {
                    uploadedFile = null;
                }
            } else if (item.isFormField()) {
                InputStream stream = item.getInputStream();
                String name = item.getFieldName();
                Map<String, String> paramsMap = new HashMap<String, String>();
                paramsMap.put(name, Streams.asString(stream));
                System.out.println("Form field " + name + " with value "
                        + paramsMap.get(name) + " detected.");
                docType = paramsMap.get("docuType");
                System.out.println("docType = " + docType);
            }
        }
    }

    private void IndexDocument(HttpServletRequest request, String memberNumber, String srcDrive, String indexDrive, String selectedFile, String folder, String entityType) {
        //String entityType = "";
        String folderList = "Index";
        String profileType = "Claims";
        IndexDocumentRequest req = new IndexDocumentRequest();
        NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
        req.setAgent(loggedInUser);
        req.setIndexType("Index");
        req.setSrcDrive(srcDrive);
        req.setEntityNumber(memberNumber);
        if (entityType != null && !entityType.equals("")) {
            req.setEntityType(Integer.parseInt(entityType));
        } else {
            req.setEntityType(5);
        }
        IndexDocType indexDocType = new IndexDocType();
        indexDocType.setDestDrive(indexDrive);
        indexDocType.setFolder(folder);
        indexDocType.setSrcFile(selectedFile);
        indexDocType.setEntityNumber(memberNumber);

        indexDocType.setDocType(docType);

        if (entityType != null && !entityType.equals("")) {
            indexDocType.setEntityType(Integer.parseInt(entityType));
        } else {
            indexDocType.setEntityType(5);
        }
        req.setIndexDoc(indexDocType);
        System.out.println("Before Webservice");
        IndexDocumentResponse res = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
        System.out.println("res = " + res.isIsSuccess());
        System.out.println("res = " + res.getMessage());
        if (res.isIsSuccess()) {
            req.setIndexDoc(null);
            req.setIndexType("Delete");
            DeleteDocType deleteDocType = new DeleteDocType();
            deleteDocType.setFolder(folderList + "/" + profileType);
            deleteDocType.setFilename(selectedFile);
            req.setDelete(deleteDocType);
            res = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
        }
    }

    private static boolean getFileExtention(File file) {
        String fileName = file.getName();
        boolean check = false;
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            String ext = fileName.substring(fileName.lastIndexOf(".") + 1);
            System.out.println("Uploaded file type is :" + ext);

            check = true;

        }
        return check;
    }

    @Override
    public String getName() {
        return "MemberDocumentUploadCommand";
    }

}

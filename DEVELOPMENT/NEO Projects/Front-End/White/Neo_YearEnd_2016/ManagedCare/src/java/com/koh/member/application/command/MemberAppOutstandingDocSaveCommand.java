/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.cover.MemberMainMapping;
import com.koh.employer.command.TabUtils;
import static com.koh.employer.command.TabUtils.getIntParam;
import com.koh.member.application.MemberAppMapping;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.LookupValue;
import neo.manager.MemberApplication;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author josephk
 */
public class MemberAppOutstandingDocSaveCommand extends NeoCommand {

    private static final String JSP_FOLDER = "/MemberApplication/";

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        System.out.println("In the member app command :)");

        ArrayList<LookupValue> lookUps = (ArrayList<LookupValue>) NeoCommand.service.getNeoManagerBeanPort().getCodeTable(new Integer(349));
        System.out.println("These are the values from the table" + lookUps.size());
        NeoUser user = getNeoUser(request);
        
        MemberApplication memApp = new MemberApplication();
        memApp.setSecurityGroupId(user.getSecurityGroupId());
        memApp.setEffectiveEndDate(null);
        memApp.setEffectiveStartDate(null);
        memApp.setCreatedBy(user.getUserId());
        memApp.setCreationDate(null);
        memApp.setLastUpdateDate(null);
        memApp.setLastUpdatedBy(user.getUserId());
        memApp.setCoverNumber(request.getParameter("memberAppCoverNumber"));
        memApp.setApplicationNumber(Integer.parseInt(request.getParameter("memberAppNumber")));
        
        String[] documentList = request.getParameterValues("docList");

        List checkedDocs = new ArrayList<String>();

        for (int i = 0; i < documentList.length; i++) {
            if (documentList[i].contains("true")) {
                checkedDocs.add(lookUps.get(i).getId());
            }
            System.out.println("Value " + i + " of list is " + documentList[i]);
        }

        for (int y = 0; y < checkedDocs.size(); y++) {
            System.out.println("Value checked " + checkedDocs.get(y));
        }
        
        PrintWriter out = null;
        try {
            int success = service.getNeoManagerBeanPort().saveMemberOutstandingDocuments(memApp, checkedDocs);
            
            if (success >= 0) {
                String appNumStr = request.getParameter("memberAppNumber");
                if (appNumStr != null && !appNumStr.isEmpty()) {
                    int appNum = Integer.parseInt(appNumStr);
                    MemberApplication ma = port.getMemberApplicationByApplicationNumber(appNum);

                    MemberAppMapping.setMemberApplication(request, ma);

                    request.getAttribute("memberAppCoverNumber ");  //, ma.getCoverNumber());
                    request.getAttribute("memberAppNumber");
                    request.getAttribute("memberAppStatus");
                    request.setAttribute("MemberName", ma.getFirstName() + " " + ma.getLastName());
                    request.setAttribute("memberAppNumber", ma.getApplicationNumber());
                    request.setAttribute("memberAppCoverNumber", ma.getCoverNumber());

                    System.out.println("Saved Successfully");

                    updateAppStatus(port, request);
                    out = response.getWriter();
                    out.println(TabUtils.buildJsonResult("OK", null, null, "\"message\":\"Documents Saved.\""));
                }
            } else {
                System.out.println("Saved Unsuccessfully");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
        return null;

    }
        private void updateAppStatus(NeoManagerBean port, HttpServletRequest request) {
        int appNum = getIntParam(request, "memberAppNumber");
        int status = getIntParam(request, "memberAppStatus");
        Security sec = new Security();
        NeoUser user = getNeoUser(request);
        sec.setCreatedBy(user.getUserId());
        sec.setLastUpdatedBy(user.getUserId());
        sec.setSecurityGroupId(user.getSecurityGroupId());
        if(status != 2 ){
             int result = port.updateMemberApplicationStatus(appNum, 2, sec);
        }
    }


    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser) request.getSession().getAttribute("persist_user");
    }

    @Override
    public String getName() {
        return "MemberAppOutstandingDocSaveCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author johanl
 */
public class LeftMenuClearAllFromSession extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.clearAllFromSession(request);
        PrintWriter out = null;
        try{
            out = response.getWriter();
            System.out.println("All Cleared");
            out.println("OK");
        }catch(Exception ex){
            ex.printStackTrace();

        }
        return null;
    }

    @Override
    public String getName() {
        return "LeftMenuClearAllFromSession";
    }
}

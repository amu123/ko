/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.dto;

import java.util.Date;

/**
 *
 * @author marcelp
 */
public class ClaimLineOverr {

    private int claimLineId;
    private int overrideId;
    private String dateOverriden;
    private int userId;
    private String description;
    private String userName;
    private double units;
    protected int multiplier;

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    public int getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getClaimLineId() {
        return claimLineId;
    }

    public void setClaimLineId(int claimLineId) {
        this.claimLineId = claimLineId;
    }

    public String getDateOverriden() {
        return dateOverriden;
    }

    public void setDateOverriden(String dateOverriden) {
        this.dateOverriden = dateOverriden;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getOverrideId() {
        return overrideId;
    }

    public void setOverrideId(int overrideId) {
        this.overrideId = overrideId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}

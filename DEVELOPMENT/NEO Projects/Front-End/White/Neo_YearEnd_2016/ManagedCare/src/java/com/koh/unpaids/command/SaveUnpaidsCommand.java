/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.unpaids.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.AcceptedList;
import neo.manager.SpmUnpaidsList;
import neo.manager.UnpaidsList;

/**
 *
 * @author dewaldo
 */
public class SaveUnpaidsCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        int scheme = new Integer(String.valueOf(session.getAttribute("schemeOption")));

        switch (scheme) {
            case 1: {
                List<UnpaidsList> unpaids = (ArrayList) session.getAttribute("unpaids");
                List<UnpaidsList> warnings = (ArrayList) session.getAttribute("warnings");
                List<AcceptedList> accepted = (ArrayList) session.getAttribute("accepted");
                List<AcceptedList> rejected = (ArrayList) session.getAttribute("rejected");
                NeoManagerBean port = service.getNeoManagerBeanPort();
                String result = port.saveResoClaimsPaymentRunDetails(rejected, unpaids);

                if (result != null && !result.equalsIgnoreCase("")) {
                    request.setAttribute("Message", result);
                } else {
                    request.setAttribute("Message", "<label class=\"error\">Something went wrong during the save process and no result was found!</label>");
                }

                dispatch(request, response, "Unpaids/UnpaidClaims.jsp");
                break;
            }
            case 2: {
                List<SpmUnpaidsList> Normal = (ArrayList) session.getAttribute("Normal");
                List<SpmUnpaidsList> vet = (ArrayList) session.getAttribute("VetReport");
                NeoManagerBean port = service.getNeoManagerBeanPort();
                String result = "";

                if (!Normal.isEmpty()) {
                    result = port.saveSpectramedClaimsPaymentRunDetails(Normal);
                } else if (!vet.isEmpty()) {
                    result = port.saveSpectramedClaimsPaymentRunDetails(vet);
                } else {
                    request.setAttribute("Message", "<label class=\"error\">Could not find any records to save!</label>");
                }

                if (result != null && !result.equalsIgnoreCase("")) {
                    request.setAttribute("Message", result);
                } else {
                    request.setAttribute("Message", "<label class=\"error\">Something went wrong during the save process and no result was found!</label>");
                }

                dispatch(request, response, "Unpaids/UnpaidClaims.jsp");
                break;
            }
            default:
                request.setAttribute("Message", "<label class=\"error\">ERROR: No scheme found!</label>");
                dispatch(request, response, "Unpaids/UnpaidClaims.jsp");
                break;
        }
        return null;
    }

    public void dispatch(HttpServletRequest request, HttpServletResponse response, String path) {
        try {
            request.getRequestDispatcher(path).forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getName() {
        return "SaveUnpaidsCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.dto;

/**
 *
 * @author nick
 */
public class AuthLabTariffListDisplay {
    
    private String tariff;
    private String labCodeCSV;
    private double tariffTotal;

    public String getLabCodeCSV() {
        return labCodeCSV;
    }

    public void setLabCodeCSV(String labCodeCSV) {
        this.labCodeCSV = labCodeCSV;
    }

    public String getTariff() {
        return tariff;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    public double getTariffTotal() {
        return tariffTotal;
    }

    public void setTariffTotal(double tariffTotal) {
        this.tariffTotal = tariffTotal;
    }
    
}

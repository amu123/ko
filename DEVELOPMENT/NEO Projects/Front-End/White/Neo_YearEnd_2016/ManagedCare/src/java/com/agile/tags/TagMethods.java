/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import neo.manager.LookupValue;
import neo.manager.SecurityResponsibility;
import neo.manager.WorkItemWithDetails;

/**
 *
 * @author gerritj
 */
public class TagMethods extends CoreTagMethods {

    public static Collection<WorkItemWithDetails> getWorkItemsAndComment(int type){
        return NeoCommand.service.getNeoManagerBeanPort().getAllWorklistItemsForType(type);
    }

    public static Collection<LookupValue> getAllResponsibilities(){
        
        ArrayList<LookupValue> lv = new ArrayList<LookupValue>();
        Collection<SecurityResponsibility> respList =  NeoCommand.service.getNeoManagerBeanPort().fetchAllSecurityResponsibilities();

        for (SecurityResponsibility resp : respList)
        {
            LookupValue l = new LookupValue();
            l.setId("" + resp.getResponsibilityId());
            l.setValue(resp.getDescription());
            lv.add(l);
        }

        return lv;
    }

    public static Collection<com.agile.security.webservice.LookupValue> getLookupValue(int type){
        return port.findCodeTableForLookupType(type);
    }

    public static HashMap<Integer, String> getNeoResoUsers(){
        HashMap<Integer, String> userMap = new HashMap<Integer, String>();
        Collection<String> userList = NeoCommand.service.getNeoManagerBeanPort().getUserListByGroupId(2);
        for (String s : userList) {
            String[] values = s.split("\\|");
            int id = Integer.parseInt(values[0]);
            String name = values[1];
            //add values to map
            userMap.put(id, name);
        }
        return userMap;
    }

    public static Collection<String> getUsersByRespId( int respId){
        //HashMap<Integer, String> userMap = new HashMap<Integer, String>();
        Collection<String> userList = NeoCommand.service.getNeoManagerBeanPort().getUserListByRespId(respId);
        for (String s : userList) {
            String[] values = s.split("\\|");
            int id = Integer.parseInt(values[0]);
            String name = values[1];
            //add values to map
            //userMap.put(id, name);
        }
        return userList;
    }

    public static HashMap<Integer, String> getMultipleUsersRespForPDC(){
        HashMap<Integer, String> userMap = new HashMap<Integer, String>();
        Collection<String> superUser = NeoCommand.service.getNeoManagerBeanPort().getUserListByRespId(2);
        for (String s : superUser) {
            String[] values = s.split("\\|");
            int id = Integer.parseInt(values[0]);
            String name = values[1];
            //add values to map
            userMap.put(id, name);
        }
        Collection<String> phc = NeoCommand.service.getNeoManagerBeanPort().getUserListByRespId(3);
        for (String s : phc) {
            String[] values = s.split("\\|");
            int id = Integer.parseInt(values[0]);
            String name = values[1];
            //add values to map
            userMap.put(id, name);
        }

        Collection<String> clinical = NeoCommand.service.getNeoManagerBeanPort().getUserListByRespId(7);
        for (String s : clinical) {
            String[] values = s.split("\\|");
            int id = Integer.parseInt(values[0]);
            String name = values[1];
            //add values to map
            userMap.put(id, name);
        }
        return userMap;
    }

}

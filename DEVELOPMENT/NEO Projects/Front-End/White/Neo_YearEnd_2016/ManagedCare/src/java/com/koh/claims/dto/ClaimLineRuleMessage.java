/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.dto;

/**
 *
 * @author marcelp
 */
public class ClaimLineRuleMessage {

    public ClaimLineRuleMessage(){}
    
    public ClaimLineRuleMessage(String msgCode, String sev, String longDesc)
    {
        this.longMsgDescription= longDesc;
        this.severity = sev;
        this.messageCode = msgCode;
    }
         
    
    private String messageCode;
    private String severity;
    private String longMsgDescription;

    public String getLongMsgDescription() {
        return longMsgDescription;
    }

    public void setLongMsgDescription(String LongMsgDescription) {
        this.longMsgDescription = LongMsgDescription;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String MessageCode) {
        this.messageCode = MessageCode;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String Severity) {
        this.severity = Severity;
    }
}

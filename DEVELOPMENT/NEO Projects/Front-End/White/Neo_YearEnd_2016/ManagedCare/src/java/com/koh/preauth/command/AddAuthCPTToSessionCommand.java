/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthCPTDetails;
import neo.manager.NeoUser;
import org.eclipse.birt.core.data.DateUtil;

/**
 *
 * @author johanl
 */
public class AddAuthCPTToSessionCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        //PrintWriter out = null;
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        List<AuthCPTDetails> cptList = null;
        if (session.getAttribute("authCPTListDetails") != null) {
            cptList = (List<AuthCPTDetails>) session.getAttribute("authCPTListDetails");
            if (cptList == null && cptList.isEmpty()) {
                cptList = new ArrayList<AuthCPTDetails>();
            } else {
                if (cptList.size() == 0) {
                    cptList = new ArrayList<AuthCPTDetails>();
                }
            }
        } else {
            cptList = new ArrayList<AuthCPTDetails>();
            session.setAttribute("cptEmergencyCase", false);
        }

        String cptCode = request.getParameter("cptCode_text");
        String tTime = request.getParameter("theatreTime");
        String procedureLink = request.getParameter("procLink");

        String sTheatreDate = request.getParameter("theatreDateTime");
        String sAdmissionDate = session.getAttribute("admissionDateTime").toString();
        Date theatreDate = null;
        XMLGregorianCalendar xTheatreDates = null;
        if (sTheatreDate != null && !sTheatreDate.equalsIgnoreCase("")) {
            try {
                theatreDate = new SimpleDateFormat("yyyy/MM/dd").parse(sTheatreDate);
                xTheatreDates = DateTimeUtils.convertDateToXMLGregorianCalendar(theatreDate);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        } else {
            if (sAdmissionDate != null) {
                try {
                    theatreDate = new SimpleDateFormat("yyyy/MM/dd").parse(sAdmissionDate);
                    xTheatreDates = DateTimeUtils.convertDateToXMLGregorianCalendar(theatreDate);
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
            }
        }

        if (procedureLink != null && !procedureLink.equalsIgnoreCase("")
                && !procedureLink.equalsIgnoreCase("null") && procedureLink.equals("2")) {
            session.setAttribute("cptEmergencyCase", true);
        }

        if (cptCode != null && !cptCode.trim().equalsIgnoreCase("")) {
            session.setAttribute("cptCode_error", null);

            AuthCPTDetails cpt = new AuthCPTDetails();
            cpt.setUserId(user.getUserId());
            cpt.setCptCode(request.getParameter("cptCode_text"));
            cpt.setCptCodeDesc("" + session.getAttribute("cptCodeDesc"));
            if (tTime != null && !tTime.trim().equalsIgnoreCase("")) {
                cpt.setTheatreTime(new Integer(tTime));
            } else {
                cpt.setTheatreTime(0);
            }
            cpt.setProcedureLink(procedureLink);
            
            if (theatreDate != null) {
                cpt.setTheatreDate(xTheatreDates);
            }
            cptList.add(cpt);
            System.out.println("cptList size = " + cptList.size());
            session.setAttribute("authCPTListDetails", cptList);
            session.setAttribute("cptCode_text", null);
            session.setAttribute("cptCodeDesc", null);

            /*
             try {
             out = response.getWriter();
             out.println("OK|");
             } catch (Exception ex) {
             ex.printStackTrace();
             }*/
            //check for emergency
            for (AuthCPTDetails cpts : cptList) {
                if (cpts.getProcedureLink() == null || cpts.getProcedureLink().equalsIgnoreCase("")
                        || cpts.getProcedureLink().equalsIgnoreCase("null") || cpts.getProcedureLink().equals("1")) {
                    session.setAttribute("cptEmergencyCase", false);
                }
            }

        } else {
            session.setAttribute("cptCode_error", "CPT Code is mandatory");
        }
        try {
            String nextJSP = "/PreAuth/AuthSpecificCPT.jsp";
            //System.out.println(nextJSP);
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "AddAuthCPTToSessionCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.practice_management.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.cover.MemberMainMapping;
import com.koh.cover.commands.SaveMemberAddressDetailsCommand;
import com.koh.employer.command.TabUtils;
import com.koh.fe.command.LoginCommand;
import com.koh.practice.PracticeManagementMainMapping;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.AddressDetails;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.ProviderDetails;
import neo.manager.Security;
import org.apache.log4j.Logger;

/**
 *
 * @author almaries
 */
public class SavePracticeAddressDetailsCommand extends NeoCommand{

    private Logger logger = Logger.getLogger(SavePracticeAddressDetailsCommand.class);
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        logger.info("^^^^^^^^^^^^^^^^^^^ SavePracticeAddressDetailsCommand execute ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        String result = validateAndSave(request);
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
        } catch (IOException ex) {
            logger.error(ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "SavePracticeAddressDetailsCommand";
    }
    
    private String validateAndSave(HttpServletRequest request) {
        Map<String, String> errors = PracticeManagementMainMapping.validateAddress(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (save(request)) {
                additionalData = "\"message\":\"Practice Address Details Updated\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error updating the practice address details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
    }

    private boolean save(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);
        
        int entityId = PracticeManagementMainMapping.getEntityId(request);
        
        try {
            
            List<AddressDetails> adList = PracticeManagementMainMapping.getAddressDetails(request, neoUser, entityId);
            port.saveAddressListDetails(adList, sec, entityId);
            
            String practiceNumber = ""+request.getSession().getAttribute("provNum");
            System.out.println("Practice Number: "+practiceNumber);
            ProviderDetails list = port.getProviderDetailsForEntityByNumber(practiceNumber);

            if (list != null) {
                int provEntityId = list.getEntityId();
                port.saveAddressListDetails(adList, sec, provEntityId);
            }
            
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}

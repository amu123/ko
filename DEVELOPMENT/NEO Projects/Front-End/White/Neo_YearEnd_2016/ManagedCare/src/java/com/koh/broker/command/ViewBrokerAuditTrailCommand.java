/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.EmployerSearchCommand;
import com.koh.utils.MapUtils;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class ViewBrokerAuditTrailCommand extends NeoCommand{
       @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        try {
            String name = request.getParameter("auditList");
            String entityId = request.getParameter("brokerEntityId");
            int id = 0;
            try {
                id = Integer.parseInt(entityId);
            } catch (Exception e) {
                
            }
            List<KeyValueArray> kva = port.getAuditTrail(name, id);
            Object col = MapUtils.getMap(kva);
            request.setAttribute("BrokerAuditDetails", col);
            context.getRequestDispatcher("/Broker/BrokerAuditDetails.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ViewBrokerAuditTrailCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewBrokerAuditTrailCommand";
    }
 
}

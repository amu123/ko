/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Johan-NB
 */
public class ReturnMemberPayRunStatement extends NeoCommand{

    private static final Logger logger = Logger.getLogger(ReturnPracticeClaimDetails.class.getName());
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        HttpSession session = request.getSession();
        
        String returnScreen = "" + session.getAttribute("returnScreen");

        if (returnScreen != null && !returnScreen.equalsIgnoreCase("")) {
            
            try {
                RequestDispatcher dispatcher = context.getRequestDispatcher(returnScreen);
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "ReturnMemberPayRunStatement";
    }
    
    
}

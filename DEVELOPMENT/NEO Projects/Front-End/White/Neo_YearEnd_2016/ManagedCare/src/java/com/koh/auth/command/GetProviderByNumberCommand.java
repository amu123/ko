/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.Collection;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ContactDetails;
import neo.manager.NeoManagerBean;
import neo.manager.ProviderDetails;

/**
 *
 * @author whauger
 */

public class GetProviderByNumberCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        String detailsRequired = request.getParameter("element");
        String number = request.getParameter("number");
        String email = "";
        NeoManagerBean port = service.getNeoManagerBeanPort();
        ProviderDetails p = port.getProviderDetailsForEntityByNumber(number);
        try{
            PrintWriter out = response.getWriter();
            if (p != null && p.getEntityId() > 0) {
                session.setAttribute("treatProvDiscType", p.getDisciplineType());
                if (detailsRequired != null && detailsRequired.equalsIgnoreCase("providerNo")) {
                    //Get email contact details
                    ContactDetails cd = port.getContactDetails(p.getEntityId(), 1);
                    if (cd != null && cd.getPrimaryIndicationId() == 1) {
                        email = cd.getMethodDetails();
                    }
                    
                    out.print(getName() + "|Number=" + number + "|Email=" + email + "$");
                } else
                {
                    out.print(getName() + "|Number=" + p.getProviderNumber() + "|Name=" + p.getSurname() + "$");
                    
                    String forField = request.getParameter("element");

                    session.setAttribute(forField + "_text", p.getProviderNumber());
                    session.setAttribute(forField, p.getSurname());
                }
            }else{
                out.print("Error|No such provider|" + getName());
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetProviderByNumberCommand";
    }

}

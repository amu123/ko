/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Johan-NB
 */
public class ReturnMemberClaimDetails extends NeoCommand {

    private static final Logger logger = Logger.getLogger(ReturnMemberClaimDetails.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();

        String onScreen = request.getParameter("onScreen");
        String previousJsp = "";
     
        if (onScreen != null && !onScreen.equalsIgnoreCase("")) {

            if (onScreen.equalsIgnoreCase("MemberDetailedClaimLine.jsp")) {
                if (session.getAttribute("onBenefitScreen") != null) {
                    previousJsp = "/Claims/MemberBenefitClaimResult.jsp";
                } else {
                    previousJsp = "/Claims/MemberHistoryClaims.jsp";
                }

            } else if (onScreen.equalsIgnoreCase("MemberHistoryClaims.jsp")) {
                previousJsp = "/Claims/MemberClaimSearch.jsp";

            } else if (onScreen.equalsIgnoreCase("MemberBenefitClaimResult.jsp")) {
                previousJsp = "/Claims/MemberBenefitDetails.jsp";

            }
            try {
                RequestDispatcher dispatcher = context.getRequestDispatcher(previousJsp);
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "ReturnMemberClaimDetails";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.Exception;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.MemberBroker;
import neo.manager.NeoManagerBean;

/**
 *
 * @author sobongad
 */
public class LoadBrokerCoverDetailsCommand extends NeoCommand {

    private void exportToCSV(HttpServletRequest request, HttpServletResponse response, NeoManagerBean port) throws IOException {
        HttpSession session = request.getSession();
        try {
            PrintWriter out = null;
            File file = new File("C:/temp/ExpoerBrokerCoverDetails.csv");
            if (!file.exists()) {
                file.createNewFile();
            }
            Writer writer = new BufferedWriter(new FileWriter(file));
            System.out.println(session.getAttribute("entityId"));
            String entityId = session.getAttribute("entityId") + "";
            if (entityId != null) {
                List<MemberBroker> ba = port.findMemberBrokerByBrokerEntityId(Integer.parseInt(entityId));
                String exportString = "";
//            Object MemberBroker = "";
                String totalLines = "";
                totalLines = "" + ba.size();

                exportString = "Member Number,Name &,Surname,Initials,ID Number, Status,Start Date, End Date\n";
                for (MemberBroker brokerDetails : ba) {
                    exportString += brokerDetails.getCoverNumber() + ',' + brokerDetails.getMemberName() + ',' + brokerDetails.getMemberSurname() + ',' + brokerDetails.getMemberInitial() + ',' + brokerDetails.getMemberIdNumber() + ',' + brokerDetails.getStatus() + ',' + brokerDetails.getEffectiveStartDate() + ',' + brokerDetails.getEffectiveEndDate() + '\n';
                }
                writer.write(exportString);
                writer.close();

                InputStream in = new FileInputStream(file);

                int contentLength = (int) file.length();
                System.out.println("The content length is " + contentLength);
//            out = response.getWriter();
//            out.print(file.toURI().toURL());
//            response.reset();

                ByteArrayOutputStream temporaryOutput;
                if (contentLength != -1) {
                    temporaryOutput = new ByteArrayOutputStream(contentLength);
                } else {
                    temporaryOutput = new ByteArrayOutputStream(20480);
                }

                byte[] bufer = new byte[512];
                while (true) {

                    int len = in.read(bufer);
                    if (len == -1) {

                        break;
                    }
                    temporaryOutput.write(bufer, 0, len);
                }
                in.close();
                byte[] bytes = temporaryOutput.toByteArray();
                printPreview(request, response, bytes, "ExportBrokerCoverDetails.csv");
                //openCSVFile(file);
            }
        } catch (Exception e) {
            //Unable to create writer
            System.err.println("Unable to create a writer");
            e.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return "LoadBrokerCoverDetailsCommand";
    }

    private void printPreview(HttpServletRequest request, HttpServletResponse response, byte[] bytes, String ExpoerBrokerCoverDetailscsv) {
        OutputStream outStream = null;
        try {
            System.out.println("fileName = " + ExpoerBrokerCoverDetailscsv);
            response.setHeader("Pragma", "no-cache"); //HTTP 1.0
            response.setDateHeader("Expires", 0); //prevents caching at the proxy server
            response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-Disposition", "attachment; filename=" + ExpoerBrokerCoverDetailscsv);
            response.setHeader("Content-Type", "text/csv");
            response.setHeader("Content-Transfer-Encoding", "binary");

            //response.setContentType("text/csv");
            response.setContentLength(bytes.length);
            outStream = response.getOutputStream();
            outStream.write(bytes);
            outStream.flush();
            outStream.close();

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

    }

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        try {
            exportToCSV(request, response, port);
        } catch (IOException ex) {
            Logger.getLogger(LoadBrokerCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

}

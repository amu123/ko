/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class LabelPasswordError extends TagSupport {

    private String elementName;
    private String displayName;
    private String valueFromSession = "no";
    private String mandatory = "no";
    private String javaScript = "";

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setValueFromSession(String valueFromSession) {
        this.valueFromSession = valueFromSession;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        try {
            String sessionVal = "";
            String sessionValerror = "";
            if (valueFromSession.equalsIgnoreCase("yes")) {
                if (session.getAttribute(elementName) != null) {
                    sessionVal = "" + session.getAttribute(elementName);
                }
                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                }
                out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"password\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"" + sessionVal + "\" size=\"31\" " + javaScript + "/></td>");
            } else {
                out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"password\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"\" size=\"31\" " + javaScript + "/></td>");
            }

            if(mandatory.equalsIgnoreCase("yes")){
                out.print("<td><label class=\"red\">*</label></td>");
            }

            if (valueFromSession.equalsIgnoreCase("Yes") && sessionValerror != null && !sessionValerror.equalsIgnoreCase("null")) {
                out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
            } else {
                out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
            }
            

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelPasswordError tag", ex);
        }
        return super.doEndTag();
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }
}

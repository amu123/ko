/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.command.NeoCommand;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Antaryami
 */
public class ConsultantForBrokerSearchCommand extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
//        throw new UnsupportedOperationException("Not supported yet.");
        try{
            searchConsultant(request, response, context);
        }catch(Exception e){
            
        }
        return null;
    }
    
    private void searchConsultant(HttpServletRequest request , HttpServletResponse response , ServletContext context) throws ServletException, IOException{
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
        }
        String consultantCode = request.getParameter("consultantCode");
        String consultantName = request.getParameter("consultantName");
//        System.out.println("Consultant code : " + consultantCode);
//        System.out.println("Consultant Name : " + consultantName);
        List<KeyValueArray> kva = port.getBrokerConsultantList(consultantCode, consultantName);
        Object data = MapUtils.getMap(kva);
//        System.out.println("Size of list back from ws : " + kva.size());
        request.setAttribute("brokerConsultantResults", data);
        context.getRequestDispatcher("/Broker/BrokerConsultantSearchResult.jsp").forward(request, response);
    }
    
    @Override
    public String getName() {
//        throw new UnsupportedOperationException("Not supported yet.");
        return "ConsultantForBrokerSearchCommand";
    }
    
}

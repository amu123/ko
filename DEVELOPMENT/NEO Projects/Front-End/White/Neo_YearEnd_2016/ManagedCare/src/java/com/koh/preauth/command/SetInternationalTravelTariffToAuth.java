/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthTariffDetails;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.TariffCode;

/**
 *
 * @author Johan-NB
 */
public class SetInternationalTravelTariffToAuth extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        PrintWriter out = null;

        String provTypeId = "" + session.getAttribute("treatingProviderDiscTypeId");
        String provNum = "" + session.getAttribute("prov");
        System.out.println("SetInternationalTravelTariffToAuth prov = " + provNum);
        String tariffCode = "TRV99";

        int pId = Integer.parseInt("" + session.getAttribute("scheme"));
        int oId = Integer.parseInt("" + session.getAttribute("schemeOption"));
        String optName = "" + session.getAttribute("schemeOptionName");
        XMLGregorianCalendar authDate = null;
        try {
            authDate = DateTimeUtils.convertDateToXMLGregorianCalendar(new SimpleDateFormat("yyyy/MM/dd").parse("" + session.getAttribute("authDate")));
        } catch (ParseException ex) {
            Logger.getLogger(SetInternationalTravelTariffToAuth.class.getName()).log(Level.SEVERE, null, ex);
        }

        String errorResponse = "";
        if (optName != null && !optName.equals("") && optName.equalsIgnoreCase("Foundation")) {
            errorResponse = "Error|Member Option not applicable on this Auth";
            
        } else {

            String icd = "" + session.getAttribute("primaryICD_text");
            List<String> icd10 = new ArrayList<String>();
            icd10.add(icd);
            TariffCode t = port.getTariffCode(pId, oId, authDate, provTypeId, provNum, provNum, icd10, null, tariffCode);
            if (t != null) {

                ArrayList<AuthTariffDetails> atList = null;
                ArrayList<AuthTariffDetails> sessionList = (ArrayList<AuthTariffDetails>) session.getAttribute("tariffListArray");
                if (sessionList != null) {
                    atList = sessionList;
                } else {
                    atList = new ArrayList<AuthTariffDetails>();
                }
                AuthTariffDetails at = new AuthTariffDetails();

                at.setTariffDesc(t.getDescription());
                at.setTariffCode(t.getCode());

                double quantity = 0.0d;
                String quanStr = "";
                if (t.getUnits() == 0.0d) {
                    quantity = 1.0d;
                } else {
                    quantity = t.getUnits();
                }
                quanStr = String.valueOf(quantity);
                at.setQuantity(quanStr);
                at.setAmount(t.getPrice());
                at.setUserId(user.getUserId());
                at.setTariffType("2");
                at.setProviderType(provTypeId);
                atList.add(at);
                //add array to session
                session.setAttribute("tariffListArray", atList);

                errorResponse = "Done|";
            } else {
                errorResponse = "Error|Member Option not applicable on this Auth";

            }
        }
        try {
            out = response.getWriter();
            out.println(errorResponse);
        } catch (IOException ex) {
            Logger.getLogger(SetInternationalTravelTariffToAuth.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "SetInternationalTravelTariffToAuth";
    }
}

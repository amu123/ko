/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.dto;

import java.util.List;
import neo.manager.PreAuthConfirmationDetails;
import neo.manager.PreAuthDetails;

/**
 *
 * @author johanl
 */
public class AuthConfirmationDetails {

    private int authId;
    private String authNumber;
    private List<PreAuthDetails> authDetails;
    private PreAuthConfirmationDetails authConfirmation;

    public AuthConfirmationDetails(){}

    public PreAuthConfirmationDetails getAuthConfirmation() {
        return authConfirmation;
    }

    public void setAuthConfirmation(PreAuthConfirmationDetails authConfirmation) {
        this.authConfirmation = authConfirmation;
    }

    public List<PreAuthDetails> getAuthDetails() {
        return authDetails;
    }

    public void setAuthDetails(List<PreAuthDetails> authDetails) {
        this.authDetails = authDetails;
    }

    public int getAuthId() {
        return authId;
    }

    public void setAuthId(int authId) {
        this.authId = authId;
    }

    public String getAuthNumber() {
        return authNumber;
    }

    public void setAuthNumber(String authNumber) {
        this.authNumber = authNumber;
    }

}

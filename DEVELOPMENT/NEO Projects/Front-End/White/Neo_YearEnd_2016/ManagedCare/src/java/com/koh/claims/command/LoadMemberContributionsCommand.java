/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ContributionTransactionHistoryView;
import neo.manager.NeoManagerBean;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class LoadMemberContributionsCommand extends NeoCommand {
    
    
    private Logger logger = Logger.getLogger(LoadMemberContributionsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        logger.info("*************************** LoadMemberContributionsCommand ********************************");
       
        HttpSession session = request.getSession();
        String coverNum = (String) session.getAttribute("policyHolderNumber");
        logger.info("LoadMemberContributionsCommand");
        logger.info("coverNum : " + coverNum);
        Date endDate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.MONTH, -12);
        Date startDate = cal.getTime();
        DateTimeUtils.convertDateToXMLGregorianCalendar(endDate);
        List<ContributionTransactionHistoryView> memberContributionsHistory = port.fetchMemberContributionsHistory(coverNum, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), DateTimeUtils.convertDateToXMLGregorianCalendar(endDate), DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        logger.info("memberContributionsHistory total  : " + memberContributionsHistory.size());
        
        session.setAttribute("Contributions", memberContributionsHistory);
        
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher("/Claims/MemberContributionsDetails.jsp");
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {

        return "LoadMemberContributionsCommand";
    }

}

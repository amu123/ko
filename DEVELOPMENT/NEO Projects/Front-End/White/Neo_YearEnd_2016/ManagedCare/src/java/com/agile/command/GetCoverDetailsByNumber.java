/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.XmlUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthHospitalLOC;
import neo.manager.CoverDetails;
import neo.manager.CoverProductDetails;
import neo.manager.CoverSearchCriteria;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author johanl
 */
public class GetCoverDetailsByNumber extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter out = null;
        HttpSession session = request.getSession();
        String xmlStr = "";
        String memberNo = request.getParameter("number");
        //System.out.println("memberNo = " + memberNo);
        
        //String exactFlag = request.getParameter("exactCoverNum");
        //System.out.println(">>>> Exact Cover Number : " + exactFlag);
        
        CoverSearchCriteria csc = new CoverSearchCriteria();
        
        if (memberNo != null && !memberNo.trim().equalsIgnoreCase("")) {
//                if(exactFlag != null && exactFlag.equals("1")){
//                    csc.setCoverNumberExact(memberNo);
//                    System.out.println("CoverNumber set to exact");
//                } else {
//                    csc.setCoverNumber(memberNo);
//                    System.out.println("CoverNumber set to default");
//                }
            List<CoverDetails> cdList = service.getNeoManagerBeanPort().getResignedMemberByCoverNumber(memberNo);
            if (cdList != null && cdList.isEmpty() == false) {

                List<CoverDetails> newCdList = new ArrayList<CoverDetails>();
                XMLGregorianCalendar endDateBig = null;
                HashMap<Integer, String> dependantMap = new HashMap<Integer, String>();
                for (CoverDetails cd : cdList) {
                    //greater end date check
                    XMLGregorianCalendar endDate = cd.getCoverEndDate();
                    System.out.println("cd end date = " + new SimpleDateFormat("yyyy/MM/dd").format(endDate.toGregorianCalendar().getTime()));
                    if (endDateBig == null) {
                        endDateBig = endDate;

                    } else {
                        if (endDate.toGregorianCalendar().after(endDateBig.toGregorianCalendar())) {
                            endDateBig = endDate;
                        }
                    }

                    //remove duplicates
                    if (dependantMap.isEmpty()) {
                        dependantMap.put(cd.getDependentNumber(), cd.getDependentType());

                        newCdList.add(cd);

                    } else if (!dependantMap.containsKey(cd.getDependentNumber())) {
                        dependantMap.put(cd.getDependentNumber(), cd.getDependentType());
                        newCdList.add(cd);
                    }

                }
                session.setAttribute("covMemListDetails", newCdList);
                //System.out.println("endDateBig = " + new SimpleDateFormat("yyyy/MM/dd").format(endDateBig.toGregorianCalendar().getTime()));
                //option date validation
                XMLGregorianCalendar optionDate = null;
                SimpleDateFormat optFormat = new SimpleDateFormat("yyyy/MM/dd");
                Date methodDate = null;

                if (optFormat.format(endDateBig.toGregorianCalendar().getTime()).equalsIgnoreCase("2999/12/31")) {
                    methodDate = new Date();
                } else {
                    methodDate = endDateBig.toGregorianCalendar().getTime();
                }
                optionDate = DateTimeUtils.convertDateToXMLGregorianCalendar(methodDate);
                //set option
                CoverProductDetails prodDetails = service.getNeoManagerBeanPort().getCoverProductDetailsByCoverNumberByDate(memberNo, optionDate);

                response.setHeader("Cache-Control", "no-cache");
                response.setContentType("text/xml");
                //get new document
                @SuppressWarnings("static-access")
                Document doc = new XmlUtils().getDocument();
                //create xml elements
                Element root = doc.createElement("EAuthCoverDetails");
                //add root to document
                doc.appendChild(root);
                try {
                    out = response.getWriter();
                    if (newCdList.size() != 0) {
                        //response.setStatus(200);
                        session.setAttribute("eAuthCoverList", newCdList);

                        //set product & option names
                        Element prodDetail = doc.createElement("ProductDetails");
                        Element prodName = doc.createElement("ProductName");
                        Element optName = doc.createElement("OptionName");

                        //set product details content
                        prodName.setTextContent(prodDetails.getProductName());
                        optName.setTextContent(prodDetails.getOptionName());
                        //set product detail ids to session
                        session.setAttribute("scheme", prodDetails.getProductId());
                        session.setAttribute("schemeOption", prodDetails.getOptionId());
                        session.setAttribute("schemeName", prodDetails.getProductName());
                        session.setAttribute("schemeOptionName", prodDetails.getOptionName());
                        //add elements to product details
                        prodDetail.appendChild(prodName);
                        prodDetail.appendChild(optName);
                        //add prodDetails element to root
                        root.appendChild(prodDetail);

                        Element member = doc.createElement("MemberDetails");
                        for (CoverDetails cd : newCdList) {
                            //create member detail string
                            String dob = new SimpleDateFormat("yyyy/MM/dd").format(cd.getDateOfBirth().toGregorianCalendar().getTime());
                            String depType = cd.getDependentType().substring(0, 1);
                            //String ecDepInfo = cd.getDependentNumber() + "|" + cd.getName() + "-" + cd.getSurname() + "-" + cd.getDependentType() + "-" + dob;
                            String ecDepInfo = cd.getDependentNumber() + "|" + depType + "-" + cd.getName() + "-" + dob;
                            ecDepInfo = ecDepInfo.replace(" Member", "");
                            //create info element
                            Element depInfo = doc.createElement("DependantInfo");
                            depInfo.setTextContent(ecDepInfo);
                            //add element to root
                            member.appendChild(depInfo);
                        }
                        root.appendChild(member);
                    } else {
                        root.setTextContent("ERROR|Member not found");
                    }
                    //return xml object
                    try {
                        xmlStr = XmlUtils.getXMLString(doc);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                        xmlStr = "";

                    }
                    out.println(xmlStr);
                } catch (IOException io) {
                    io.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetCoverDetailsByNumber";
    }

    public static List<CoverDetails> setCoverForUpdate(String memberNo, int depCode) {

        System.out.println("setCoverForUpdate memberNo = " + memberNo);
        CoverSearchCriteria csc = new CoverSearchCriteria();
        List<CoverDetails> newCdList = null;

        if (memberNo != null && !memberNo.trim().equalsIgnoreCase("")) {
            csc.setCoverNumber(memberNo);
            List<CoverDetails> cdList = service.getNeoManagerBeanPort().getResignedMemberByCoverNumber(memberNo);
            if (cdList != null && cdList.isEmpty() == false) {

                Collections.sort(cdList, new Comparator<CoverDetails>() {
                    public int compare(CoverDetails o1, CoverDetails o2) {
                        if (o1.getCoverEndDate() == null || o2.getCoverEndDate() == null) {
                            return 0;
                        }
                        return o2.getCoverEndDate().toGregorianCalendar().getTime().compareTo(o1.getCoverEndDate().toGregorianCalendar().getTime());
                    }
                });

                newCdList = new ArrayList<CoverDetails>();

                XMLGregorianCalendar endDateBig = null;
                HashMap<Integer, String> dependantMap = new HashMap<Integer, String>();
                for (CoverDetails cd : cdList) {
                    //greater end date check
                    XMLGregorianCalendar endDate = cd.getCoverEndDate();
                    System.out.println("cd end date = " + new SimpleDateFormat("yyyy/MM/dd").format(endDate.toGregorianCalendar().getTime()));
                    if (endDateBig == null) {
                        endDateBig = endDate;

                    } else {
                        if (endDate.toGregorianCalendar().after(endDateBig.toGregorianCalendar())) {
                            endDateBig = endDate;
                        }
                    }

                    //remove duplicates
                    if (dependantMap.isEmpty()) {
                        dependantMap.put(cd.getDependentNumber(), cd.getDependentType());

                        newCdList.add(cd);

                    } else if (!dependantMap.containsKey(cd.getDependentNumber())) {
                        dependantMap.put(cd.getDependentNumber(), cd.getDependentType());
                        newCdList.add(cd);
                    }

                }
            }
        }
        return newCdList;
    }
}

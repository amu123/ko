/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoUser;


/**
 *
 * @author josephm
 */
public class ForwardGenericAuthorizationPortal extends NeoCommand {

    private String memberNumber;
    private String coverNumber;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        System.out.println("Inside ForwardGenericAuthorizationPortal");
        HttpSession session = request.getSession();
        String nextJSP = null;

        try {

            String providerNum = request.getParameter("provNum");
            String memberNum =  request.getParameter("memberNum");
            String beneficiaryNum = request.getParameter("beneficiaryNum");
           // String beneficiaryNum = request.getParameter("depListValues_text");



            if (providerNum != null && !providerNum.trim().equalsIgnoreCase("")) {
                NeoUser user = service.getNeoManagerBeanPort().authenticateUser("provider", "redivorp");
                session.setAttribute("persist_user", user);
                nextJSP = "/PreAuth/GenericAuthorizationProviderPortal.jsp";
                session.setAttribute("treatingProvider", providerNum);
                session.setAttribute("memberNum",  memberNum);
                session.setAttribute("depListValues_text", beneficiaryNum);
                session.setAttribute("authType", 4);
                session.setAttribute("searchCalled", "memberNum");
            } else if (memberNum != null && !memberNum.trim().equalsIgnoreCase("")) {
               NeoUser user = service.getNeoManagerBeanPort().authenticateUser("member", "rebmem");
               session.setAttribute("persist_user", user);
               nextJSP = "/PreAuth/GenericAuthorisationPortal.jsp";
               session.setAttribute("memberNum",  memberNum);
               session.setAttribute("depListValues_text", beneficiaryNum);
               session.setAttribute("authType", 4);
               session.setAttribute("searchCalled", "memberNum");
            } else {
               nextJSP = "/Login.jsp";
            }

            System.out.println("The name portal is  " + memberNum);
            System.out.println("The dependent number is " + beneficiaryNum);
            System.out.println("the provider nyumber is " + providerNum);

        }catch(Exception ex) {
            nextJSP = "/Login.jsp";
            ex.printStackTrace();
        }

        try {
            //String nextJSP = "/PreAuth/GenericAuthorisationPortal.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        }catch(Exception ex) {

            ex.printStackTrace();
        }

        return null;

    }
 
    public String getCoverNumber() {
        return coverNumber;
    }

    public void setCoverNumber(String coverNumber) {
        this.coverNumber = coverNumber;
    }

    public String getMemberNumber() {
        return memberNumber;
    }

    public void setMemberNumber(String memberNumber) {
        this.memberNumber = memberNumber;
    }

    

    @Override
    public String getName() {

        return "ForwardGenericAuthorizationPortal";
    }

}

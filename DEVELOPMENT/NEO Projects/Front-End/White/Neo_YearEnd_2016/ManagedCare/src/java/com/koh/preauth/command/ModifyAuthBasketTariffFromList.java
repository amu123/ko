/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author johanl
 */
public class ModifyAuthBasketTariffFromList extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession ses = request.getSession();
        PrintWriter out = null;

        String btHidden = request.getParameter("btHidden");
        String btValues[] = btHidden.split("\\|");
        String code = btValues[0];
        String desc = btValues[1];
        int freq = new Integer(btValues[2]);
        List<AuthTariffDetails> basketList = (List<AuthTariffDetails>) ses.getAttribute("AuthBasketTariffs");
        try {
            out = response.getWriter();
            for (AuthTariffDetails bt : basketList) {
                if (bt.getTariffCode().equals(code)) {
                    if (bt.getTariffDesc().equalsIgnoreCase(desc)) {
                        if (bt.getFrequency() == freq) {

                            String provDesc = bt.getProviderType();
                            System.out.println("modified prov cat = " + provDesc);

                            ses.setAttribute("providerDesc", bt.getProviderType());
                            ses.setAttribute("btCode", code);
                            ses.setAttribute("btDesc", desc);
                            ses.setAttribute("tariffFreq", freq);
                            ses.setAttribute("tariffOption", bt.getTariffOption());

                            out.print("Done|" + provDesc + "|" + bt.getTariffCode() + "|" + bt.getTariffDesc() +
                                    "|" + bt.getFrequency());
                            basketList.remove(bt);
                            break;

                        }
                    }
                }
            }
            
            ses.setAttribute("AuthBasketTariffs", basketList);
            
            for(AuthTariffDetails bt : basketList){
                System.out.println("tariff in basket when modifying = " + bt.getTariffCode() + " - " + bt.getProviderType());
            }

        } catch (Exception ex) {
            System.out.println("ModifyAuthBasketTariffFromList error : " + ex.getMessage());
            out.print("Error|");
        } finally {
            out.close();
        }

        return null;
    }

    @Override
    public String getName() {
        return "ModifyAuthBasketTariffFromList";
    }
}

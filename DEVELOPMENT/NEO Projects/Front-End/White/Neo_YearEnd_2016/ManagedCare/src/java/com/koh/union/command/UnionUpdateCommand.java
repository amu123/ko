/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.union.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Union;
import neo.manager.UnionAddressDetails;
import neo.manager.UnionRepresentative;

/**
 *
 * @author shimanem
 */
public class UnionUpdateCommand extends NeoCommand {

    private static final Logger logger = Logger.getLogger(UnionUpdateCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
        logger.info("*** Updating Union [Frontend] ***");
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String repID = String.valueOf(session.getAttribute("representativeNumber"));
        String unionID = String.valueOf(session.getAttribute("unionID"));

        logger.log(Level.INFO, "Union ID : {0}", unionID);
        logger.log(Level.INFO, "Rep ID : {0}", repID);

        int union_id = Integer.parseInt(unionID);
        int rep_id = Integer.parseInt(repID);

        updateUnion(port, session, union_id, rep_id, request, response, context);

        return null;
    }

    public void updateUnion(NeoManagerBean port, HttpSession session, int unionID, int repID, HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        try {

            NeoUser neoUser = getNeoUser(request);

            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            String message = "";

            String unionName = request.getParameter("unionName");
            String unionNumber = request.getParameter("unionNumber");
            String unionAltNumber = request.getParameter("altUnionNumber");
            String repName = request.getParameter("repName");
            String repSurname = request.getParameter("repSurname");
            String unionRepCapacity = request.getParameter("unionRepCapacity");
            String unionRegion = request.getParameter("unionRegion");
            String inceptionDate = request.getParameter("unionInceptionDate");
            String terminationDate = request.getParameter("unionTerminationDate");
            // Contact Components
            String contactNumber = request.getParameter("contactNumber");
            String faxNumber = request.getParameter("faxNumber");
            String email = request.getParameter("emailAddress");
            String alternateEmail = request.getParameter("altEmailAddress");
            // Postal Address Components
            String postLine1 = request.getParameter("postLine1");
            String postLine2 = request.getParameter("postLine2");
            String postLine3 = request.getParameter("postLine3");
            String postLocation = request.getParameter("postLocation");
            String postCode = request.getParameter("postCode");
            // Physical Address Components
            String physicalLine1 = request.getParameter("physicalLine1");
            String physicalLine2 = request.getParameter("physicalLine2");
            String physicalLine3 = request.getParameter("physicalLine3");
            String physicalLocation = request.getParameter("physicalLocation");
            String physicalCode = request.getParameter("physicalCode");

            Union union = new Union();
            UnionRepresentative unionRepresentative = new UnionRepresentative();
            UnionAddressDetails addressDetails = new UnionAddressDetails();

            //Union Details
            union.setUnionNumber(unionNumber);
            union.setUnionAlternativeNumber(unionAltNumber);
            union.setUnionName(unionName);
            union.setInceptionDate(DateTimeUtils.convertDateToXMLGregorianCalendar(format.parse(inceptionDate)));
            union.setTerminationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(format.parse(terminationDate)));
            //Union rep Details
            //unionRepresentative.setUnionID(unionNumber);
            unionRepresentative.setRepresentativeCapacityID(Integer.parseInt(unionRepCapacity));
            unionRepresentative.setName(repName);
            unionRepresentative.setSurname(repSurname);
            unionRepresentative.setContactNumber(contactNumber);
            unionRepresentative.setFaxNumber(faxNumber);
            unionRepresentative.setEmail(email);
            unionRepresentative.setAltEmail(alternateEmail);
            unionRepresentative.setRegionID(Integer.parseInt(unionRegion));
            //union Address Details
            addressDetails.setPostAddressLine1(postLine1);
            addressDetails.setPostAddressLine2(postLine2);
            addressDetails.setPostAddressLine3(postLine3);
            addressDetails.setPostAddressLocation(postLocation);
            addressDetails.setPostAddressCode(postCode);
            addressDetails.setPhysicalAddressLine1(physicalLine1);
            addressDetails.setPhysicalAddressLine2(physicalLine2);
            addressDetails.setPhysicalAddressLine3(physicalLine3);
            addressDetails.setPhysicalAddressLocation(physicalLocation);
            addressDetails.setPhysicalAddressCode(physicalCode);

            boolean updateStatus = port.updateUnionDetails(union, unionRepresentative, addressDetails, neoUser, unionID, repID);
            logger.log(Level.INFO, "Update Status : {0}", updateStatus);
            if (updateStatus == true) {
                //message = "Union Updated Successfully";
                //responseMsg(message, "/ManagedCare/Union/unionDetails.jsp", response);
                reloadSessionValues(port, session, response, repID);
            } else {
                message = "Unable to Update Union, Please contact Administrator";
                responseMsg(message, "/ManagedCare/Union/unionDetails.jsp", response);
            }

        } catch (ParseException ex) {
            Logger.getLogger(UnionUpdateCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void reloadSessionValues(NeoManagerBean port, HttpSession session, HttpServletResponse response, int repNo) {

        Union union = port.retrieveUnion(repNo);
        UnionAddressDetails addressDetails = port.retrieveUnionAddressDetails(repNo);
        String message = "";
        logger.info("Reloading Session Value on Union Update");
        if (union != null) {
            logger.info("Reloading Session Value (Union Details)");
            session.setAttribute("unionDetails", union);
            session.setAttribute("altUnionNumber", union.getUnionAlternativeNumber());
            session.setAttribute("repName", union.getRepresentativeName());
            session.setAttribute("repSurname", union.getRepresentativeSurname());
            session.setAttribute("unionRepCapacity", union.getRepresentativeID());
            session.setAttribute("unionRegion", union.getRepresentativeRegionID());
            session.setAttribute("unionInceptionDate", DateTimeUtils.convertToYYYYMMDD(union.getInceptionDate().toGregorianCalendar().getTime()));
            session.setAttribute("unionTerminationDate", DateTimeUtils.convertToYYYYMMDD(union.getTerminationDate().toGregorianCalendar().getTime()));
            // Contacts    
            session.setAttribute("contactNumber", union.getRepresentativeContact());
            session.setAttribute("faxNumber", union.getRepresentativeFaxNumber());
            session.setAttribute("emailAddress", union.getRepresentativeEmail());
            session.setAttribute("altEmailAddress", union.getRepresentativeAlternativeEmail());

            if (addressDetails != null) {
                logger.info("Reloading Session Value (Address Details)");
                session.setAttribute("postLine1", addressDetails.getPostAddressLine1());
                session.setAttribute("postLine2", addressDetails.getPostAddressLine2());
                session.setAttribute("postLine3", addressDetails.getPostAddressLine3());
                session.setAttribute("postLocation", addressDetails.getPostAddressLocation());
                session.setAttribute("postCode", addressDetails.getPostAddressCode());

                session.setAttribute("physicalLine1", addressDetails.getPhysicalAddressLine1());
                session.setAttribute("physicalLine2", addressDetails.getPhysicalAddressLine2());
                session.setAttribute("physicalLine3", addressDetails.getPhysicalAddressLine3());
                session.setAttribute("physicalLocation", addressDetails.getPhysicalAddressLocation());
                session.setAttribute("physicalCode", addressDetails.getPhysicalAddressCode());

            } else {
                session.setAttribute("postLine1", null);
                session.setAttribute("postLine2", null);
                session.setAttribute("postLine3", null);
                session.setAttribute("postLocation", null);
                session.setAttribute("postCode", null);

                session.setAttribute("physicalLine1", null);
                session.setAttribute("physicalLine2", null);
                session.setAttribute("physicalLine3", null);
                session.setAttribute("physicalLocation", null);
                session.setAttribute("physicalCode", null);
            }

        } else {
            session.setAttribute("unionDetails", null);
            session.setAttribute("altUnionNumber", null);
            session.setAttribute("repName", null);
            session.setAttribute("repSurname", null);
            session.setAttribute("unionRepCapacity", null);
            session.setAttribute("unionRegion", null);
            session.setAttribute("unionInceptionDate", null);
            session.setAttribute("unionTerminationDate", null);
            // Contacts    
            session.setAttribute("contactNumber", null);
            session.setAttribute("faxNumber", null);
            session.setAttribute("emailAddress", null);
            session.setAttribute("altEmailAddress", null);
        }
        logger.info("Reloading Session Value : Refreshing page....");
        message = "Union Updated Successfully";
        responseMsg(message, "/ManagedCare/Union/unionDetails.jsp", response);
    }

    public void responseMsg(String message, String url, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();

            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            out.println("<td><label class=\"header\"><h2>" + message + "</h2></label></td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "3; URL=" + url); // /ManagedCare/Union/WorkbenchDetails.jsp

        } catch (IOException ex) {
            Logger.getLogger(UnionCapturerCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser) request.getSession().getAttribute("persist_user");
    }

    @Override
    public String getName() {
        return "UnionUpdateCommand";
    }

}

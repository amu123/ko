/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author johanl
 */
public class AllocateBasketTariffToSession extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        String provType = request.getParameter("providerDesc");
        String btCode = request.getParameter("btCode");
        String btDesc = request.getParameter("btDesc");
        int tFeq = Integer.parseInt(request.getParameter("tariffFreq"));
        int tOpt = Integer.parseInt(request.getParameter("tariffOption"));


        session.setAttribute("providerDesc", provType);
        session.setAttribute("btCode", btCode);
        session.setAttribute("btDesc", btDesc);
        session.setAttribute("tariffFreq", tFeq);
        session.setAttribute("tariffOption", tOpt);

        try {
            String nextJSP = "/PreAuth/AuthSpecificBasketTariff.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "AllocateBasketTariffToSession";
    }
}

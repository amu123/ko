/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import com.koh.contribution.command.ContributionBankStatementCommand;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class NewDepAppSearchCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            NeoManagerBean port = service.getNeoManagerBeanPort();
            String s = request.getParameter("command");
            if (s != null && !s.isEmpty()) {
                System.out.println("command : " + s);
                if (s.equalsIgnoreCase("Search")) {
                    search(port, request, response, context);
                } else if (s.equalsIgnoreCase("Select")) {
                    select(port, request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(ContributionBankStatementCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void search(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        String coverNumber = request.getParameter("memNo");
        String idNumber = request.getParameter("idNo");
        String initials = request.getParameter("initials");
        String surname = request.getParameter("surname");
        String dob = request.getParameter("dob");

        CoverSearchCriteria cs = new CoverSearchCriteria();
        HttpSession session = request.getSession();
        //cover number set
        if (coverNumber != null && !coverNumber.trim().equals("")
                && !coverNumber.trim().equalsIgnoreCase("null")) {
            cs.setCoverNumber(coverNumber);
        }
        //id number set
        if (idNumber != null && !idNumber.trim().equals("")
                && !idNumber.trim().equalsIgnoreCase("null")) {
            cs.setCoverIDNumber(idNumber);
        }
        //initials set
        if (initials != null && !initials.trim().equals("")
                && !initials.trim().equalsIgnoreCase("null")) {
            cs.setCoverInitial(initials);
        }
        //surname set
        if (surname != null && !surname.trim().equals("")
                && !surname.trim().equalsIgnoreCase("null")) {
            cs.setCoverSurname(surname);
        }
        //date of birth set
        if (dob != null && !dob.trim().equals("")
                && !dob.trim().equalsIgnoreCase("null")) {

            Date cDOB = null;
            XMLGregorianCalendar xDOB = null;
            try {
                cDOB = DateTimeUtils.dateFormat.parse(dob);
                xDOB = DateTimeUtils.convertDateToXMLGregorianCalendar(cDOB);

            } catch (ParseException px) {
                px.printStackTrace();
            }
            cs.setDateOfBirth(xDOB);
        }
        List<CoverDetails> covList2 = new ArrayList<CoverDetails>();
        List<CoverDetails> covList = port.getCoverPrincipleList(cs);
        boolean viewStaff = (Boolean) session.getAttribute("persist_user_viewStaff");
        for (CoverDetails cov : covList) {
            if (!viewStaff) {
                if (cov.getMemberCategory() != 2) {
                    covList2.add(cov);
                }
            } else {
                covList2.add(cov);
            }
        }
        if (!viewStaff) {
            if (covList != null && covList2 != null) {
                if (covList.size() > 0) {
                    if (covList2.size() == 0) {
                        request.setAttribute("depSearchResultsMessage", "Yes");
                    }
                }
            }
        }
        request.getSession().setAttribute("savePayment", "no");
        request.setAttribute("MemAppSearchResults", covList2);
        context.getRequestDispatcher("/MemberApplication/DepApplicationResults.jsp").forward(request, response);
    }

    private void select(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
//memberCoverNumber
        String coverNo = request.getParameter("memberCoverNumber");
        request.setAttribute("memberCoverNumber", coverNo);
        request.setAttribute("memberAppType", "1");
        context.getRequestDispatcher("/MemberApplication/MemberApplication.jsp").forward(request, response);
    }

    private int getIntParam(HttpServletRequest request, String paramName) {
        try {
            String maxDepStr = request.getParameter(paramName);
            return Integer.parseInt(maxDepStr);
        } catch (java.lang.Exception e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }

    @Override
    public String getName() {
        return "NewDepAppSearchCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

/**
 *
 * @author Princes
 *
 *
 */
import com.koh.claims.dto.MemberClaimSearchBean;
import com.koh.claims.tags.ViewMemberClaimsTable;
import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author Johan-NB
 */
public class GetMemberClaimDetailsPDCCommand extends NeoCommand {

//    @Override
//    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
//        this.saveScreenToSession(request);
//
//        HttpSession session = request.getSession();
//        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
//
//        boolean doSixMonths = false;
//        boolean claimIdFound = false;
//        String claimSearchResultMsg = "";
//
//        PrintWriter out = null;
//        CcClaimSearchResult batch = new CcClaimSearchResult();
//        ClaimCCSearchCriteria search = new ClaimCCSearchCriteria();
//        //set default search values
//        search.setDependentCode(-1);
//        search.setResultCountMin(0);
//        search.setResultCountMax(100);
//
//        NeoUser user = (NeoUser) session.getAttribute("persist_user");
//
//        neo.manager.Security _secure = new neo.manager.Security();
//        _secure.setCreatedBy(user.getUserId());
//        _secure.setLastUpdatedBy(user.getUserId());
//        _secure.setSecurityGroupId(2);
//
//        String pageDirection = "" + session.getAttribute("mcPageDirection");
//        if (pageDirection != null && !pageDirection.equalsIgnoreCase("")) {
//            search = (ClaimCCSearchCriteria) session.getAttribute("memberClaimSearch");
//
//            int currentMin = search.getResultCountMin();
//            int currentMax = search.getResultCountMax();
//
//            if (pageDirection.equalsIgnoreCase("forward")) {
//                search.setResultCountMin(currentMin + 100);
//                search.setResultCountMax(currentMax + 100);
//
//            } else if (pageDirection.equalsIgnoreCase("backward")) {
//                search.setResultCountMin(currentMin - 100);
//                search.setResultCountMax(currentMax - 100);
//            }
//            batch = port.findCCClaimBySearchCriteria(search, _secure);
//
//
//        } else {
//
//            int dependantCode = -1;
//
//            System.out.println("entered ViewMemberClaimsTable");
//            String coverNumber = (String) session.getAttribute("policyHolderNumber");
//            System.out.println("coverNumber " + coverNumber);
//
//
//
//            //set cover number
//            search.setCoverNumber(coverNumber);
//
//            dependantCode = Integer.parseInt("" + session.getAttribute("depCode"));
//            System.out.println("BCDD: dependantCode = " + dependantCode);
//            search.setDependentCode(dependantCode);
//
//
//
//            String treatFrom = "";
//            String treatTo = "";
//
//            treatFrom = "" + session.getAttribute("treatmentFrom");
//            treatTo = "" + session.getAttribute("treatmentTo");
//
//            if ((treatFrom != null && !treatFrom.equalsIgnoreCase("")) && (treatTo != null && !treatTo.equalsIgnoreCase(""))) {
//
//                Date tFrom = null;
//                Date tTo = null;
//                XMLGregorianCalendar xTreatFrom = null;
//                XMLGregorianCalendar xTreatTo = null;
//
//                try {
//                    tFrom = dateFormat.parse(treatFrom);
//                    tTo = dateFormat.parse(treatTo);
//
//                    xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
//                    xTreatTo = DateTimeUtils.convertDateToXMLGregorianCalendar(tTo);
//
//                    search.setServiceDateFrom(xTreatFrom);
//                    search.setServiceDateTo(xTreatTo);
//
//                } catch (java.text.ParseException ex) {
//                    Logger.getLogger(ViewMemberClaimsTable.class.getName()).log(Level.SEVERE, null, ex);
//                }
//
//            }
//
//
//
//            if (doSixMonths) {
//                //set service dates
//                Calendar c1 = Calendar.getInstance();
//                c1.add(Calendar.MONTH, -6); // substract 6 month
//
//                Calendar calendar = Calendar.getInstance();
//
//                try {
//                    String today = dateFormat.format(calendar.getTime());
//                    Date now = dateFormat.parse(today);
//                    XMLGregorianCalendar xmlNow = DateTimeUtils.convertDateToXMLGregorianCalendar(now);
//                    calendar.add(Calendar.MONTH, -6);
//                    String sixLater = dateFormat.format(calendar.getTime());
//                    Date sixMonths = dateFormat.parse(sixLater);
//                    XMLGregorianCalendar xmlSixMonthsLater = DateTimeUtils.convertDateToXMLGregorianCalendar(sixMonths);
//                    search.setServiceDateFrom(xmlSixMonthsLater);
//                    search.setServiceDateTo(xmlNow);
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            //set rusult message
//
//            }
//
//
//
//            batch = port.findCCClaimBySearchCriteria(search, _secure);
//
//        }
//
//        if (batch == null && batch.getBatchs().isEmpty() == true) {
//            claimSearchResultMsg += " No results found.";
//
//        } else if (batch.getResultCounter() == 100) {
//            claimSearchResultMsg += " Please Redefine Claim Search.";
//
//        } else {
//            List<ClaimsBatch> claimList = batch.getBatchs();
//
//            System.out.println("batch not null : size = " + claimList.size());
//
//            session.setAttribute("memCCClaimSearchResult", claimList);
//            session.setAttribute("memCCClaimSearchResultCount", batch.getResultCounter());
//            session.setAttribute("memCCClaimSearch", batch.getCcSearch());
//
//        }
//
//        request.setAttribute("memberClaimSearchResultsMessage", claimSearchResultMsg);
//
//        try {
//            String nextJSP = "/PDC/ClaimsHistory.jsp";
//            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
//            dispatcher.forward(request, response);
//
//        } catch (Exception ex) {
//            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//        }
//        return null;
//    }
//
//    @Override
//    public String getName() {
//        return "GetMemberClaimDetailsPDCCommand";
//    }
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);

        HttpSession session = request.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        boolean doSixMonths = false;
        boolean claimIdFound = false;
        String claimSearchResultMsg = "";

        PrintWriter out = null;
        CcClaimSearchResult batch = new CcClaimSearchResult();
        ClaimCCSearchCriteria search = new ClaimCCSearchCriteria();
        //set default search values
        search.setDependentCode(-1);
        search.setResultCountMin(0);
        search.setResultCountMax(100);

        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        neo.manager.Security _secure = new neo.manager.Security();
        _secure.setCreatedBy(user.getUserId());
        _secure.setLastUpdatedBy(user.getUserId());
        _secure.setSecurityGroupId(2);

        String pageDirection = "" + session.getAttribute("mcPageDirection");
        if (pageDirection != null && !pageDirection.equalsIgnoreCase("")) {
            search = (ClaimCCSearchCriteria) session.getAttribute("memberClaimSearch");

            int currentMin = search.getResultCountMin();
            int currentMax = search.getResultCountMax();

            if (pageDirection.equalsIgnoreCase("forward")) {
                search.setResultCountMin(currentMin + 100);
                search.setResultCountMax(currentMax + 100);

            } else if (pageDirection.equalsIgnoreCase("backward")) {
                search.setResultCountMin(currentMin - 100);
                search.setResultCountMax(currentMax - 100);
            }
            batch = port.findCCClaimBySearchCriteria(search, _secure);

        } else {

            int dependantCode = -1;

            System.out.println("entered ViewMemberClaimsTable");
            String coverNumber = (String) session.getAttribute("policyNumber");
            System.out.println("coverNumber " + coverNumber);

            //set cover number
            search.setCoverNumber(coverNumber);

            dependantCode = Integer.parseInt("" + session.getAttribute("dependantNumber"));
            System.out.println("BCDD: dependantCode = " + dependantCode);
            search.setDependentCode(dependantCode);

            String treatFrom = "";
            String treatTo = "";

            treatFrom = "" + session.getAttribute("treatmentFrom");
            treatTo = "" + session.getAttribute("treatmentTo");

            if ((treatFrom != null && !treatFrom.equalsIgnoreCase("")) && (treatTo != null && !treatTo.equalsIgnoreCase(""))) {

                Date tFrom = null;
                Date tTo = null;
                XMLGregorianCalendar xTreatFrom = null;
                XMLGregorianCalendar xTreatTo = null;

                try {
                    tFrom = dateFormat.parse(treatFrom);
                    tTo = dateFormat.parse(treatTo);

                    xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                    xTreatTo = DateTimeUtils.convertDateToXMLGregorianCalendar(tTo);

                    search.setServiceDateFrom(xTreatFrom);
                    search.setServiceDateTo(xTreatTo);

                } catch (java.text.ParseException ex) {
                    Logger.getLogger(ViewMemberClaimsTable.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

            if (doSixMonths) {
                //set service dates
                Calendar c1 = Calendar.getInstance();
                c1.add(Calendar.MONTH, -6); // substract 6 month

                Calendar calendar = Calendar.getInstance();

                try {
                    String today = dateFormat.format(calendar.getTime());
                    Date now = dateFormat.parse(today);
                    XMLGregorianCalendar xmlNow = DateTimeUtils.convertDateToXMLGregorianCalendar(now);
                    calendar.add(Calendar.MONTH, -6);
                    String sixLater = dateFormat.format(calendar.getTime());
                    Date sixMonths = dateFormat.parse(sixLater);
                    XMLGregorianCalendar xmlSixMonthsLater = DateTimeUtils.convertDateToXMLGregorianCalendar(sixMonths);
                    search.setServiceDateFrom(xmlSixMonthsLater);
                    search.setServiceDateTo(xmlNow);

                } catch (java.lang.Exception e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                }
                //set rusult message

            }

            batch = port.findCCClaimBySearchCriteria(search, _secure);

        }

        if (batch == null && batch.getBatchs().isEmpty() == true) {
            claimSearchResultMsg += " No results found.";

        } else if (batch.getResultCounter() == 100) {
            claimSearchResultMsg += " Please Redefine Claim Search.";

        } else {
            List<ClaimsBatch> claimList = batch.getBatchs();

            //buil the JSTL tag bean to replace old cold
            List<MemberClaimSearchBean> meberClaimSearchBeanList = new ArrayList<MemberClaimSearchBean>();
            for (ClaimsBatch claim : claimList) {
                String claimType = "";
                if (claim.getEdiId() == null || claim.getEdiId().equalsIgnoreCase("")) {
                    claimType = "Paper";
                } else {
                    claimType = "EDI";
                }
                for (Claim list_claim : claim.getClaims()) {
                    MemberClaimSearchBean bean = new MemberClaimSearchBean();
                    ProviderDetails prac = port.getPracticeDetailsForEntityByNumberAndDate(list_claim.getPracticeNo(), list_claim.getServiceDateFrom());
                    String catagory = port.getValueForId(59, list_claim.getClaimCategoryTypeId() + "");
                    ProviderDetails practice = port.getPracticeDetailsForEntityByNumber(list_claim.getPracticeNo());
                    ProviderDetails practice2 = port.getPracticeDetailsForEntityByNumber(list_claim.getServiceProviderNo());

                    bean.setClaimType(claimType);
                    bean.setClaimNumber(String.valueOf(list_claim.getClaimId()));
                    bean.setAccountNumber(list_claim.getPatientRefNo());
                    bean.setPracticeNumber(list_claim.getPracticeNo());
                    bean.setPracticeName(practice.getPracticeName());
                    bean.setServiceProvider(list_claim.getServiceProviderNo());
                    bean.setServiceProviderName(practice2.getPracticeName());
                    bean.setDisciplineType(catagory);
                    bean.setMemberNumber(list_claim.getCoverNumber());
                    bean.setTreatmentFrom(dateFormat.format(list_claim.getServiceDateFrom().toGregorianCalendar().getTime()));
                    bean.setTreatmentTo(dateFormat.format(list_claim.getServiceDateTo().toGregorianCalendar().getTime()));
                    String totalAmountClaimed = "";
                    String totalAmountPaid = "";
                    if (list_claim.getClaimCalculatedTotal() != 0.0d) {
                        totalAmountClaimed = new DecimalFormat("#####0.00").format(list_claim.getClaimCalculatedTotal());
                    } else {
                        totalAmountClaimed = "0.00";
                    }

                    if (list_claim.getClaimCalculatedPaidTotal() != 0.0d) {
                        totalAmountPaid = new DecimalFormat("#####0.00").format(list_claim.getClaimCalculatedPaidTotal());
                    } else {
                        totalAmountPaid = "0.00";
                    }
                    bean.setTotalClaimed(totalAmountClaimed);
                    bean.setTotalPaid(totalAmountPaid);
                    bean.setClaimID("" + list_claim.getClaimId());
                    bean.setServiceProviderNo(list_claim.getServiceProviderNo());
                    String recipient = port.getValueFromCodeTableForTableId(15, list_claim.getRecipient());
                    bean.setRecipient(recipient);
                    String coopay = "";
                    if (claim.getClaims().get(0).getClaimCopayAmount() != 0.0d) {
                        coopay = new DecimalFormat("#####0.00").format(claim.getClaims().get(0).getClaimCopayAmount());
                    } else {
                        coopay = "0.00";
                    }
                    bean.setCo_payment(coopay);
                    bean.setNetwork(prac.getNetworkName());
                    meberClaimSearchBeanList.add(bean);
                }

            }//end here
            session.setAttribute("meberClaimSearchBeanList", meberClaimSearchBeanList);

            System.out.println("batch not null : size = " + claimList.size());

            session.setAttribute("memCCClaimSearchResult", claimList);
            session.setAttribute("memCCClaimSearchResultCount", batch.getResultCounter());
            session.setAttribute("memCCClaimSearch", batch.getCcSearch());

        }

        session.setAttribute("memberClaimSearchResultsMessage", claimSearchResultMsg);

        try {
            String nextJSP = "/PDC/ClaimsHistory.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetMemberClaimDetailsPDCCommand";
    }
}

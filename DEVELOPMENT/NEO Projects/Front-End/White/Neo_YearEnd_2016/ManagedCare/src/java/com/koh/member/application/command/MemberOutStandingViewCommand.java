/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import static com.koh.fe.command.LoginCommand.getNeoUser;
import com.koh.member.application.MemberAppMapping;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.LookupValue;
import neo.manager.MemberApplication;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.OutstandingDocuments;

/**
 *
 * @author sobongad
 */
public class MemberOutStandingViewCommand extends NeoCommand {

    private static final String JSP_FOLDER = "/MemberApplication/";

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        NeoUser user = getNeoUser(request);
        HttpSession session = request.getSession();

        MemberApplication memApp = new MemberApplication();
        memApp.setSecurityGroupId(user.getSecurityGroupId());
        memApp.setEffectiveEndDate(null);
        memApp.setEffectiveStartDate(null);
        memApp.setCreatedBy(user.getUserId());
        memApp.setCreationDate(null);
        memApp.setLastUpdateDate(null);
        memApp.setLastUpdatedBy(user.getUserId());
        memApp.setCoverNumber(request.getParameter("memberAppCoverNumber"));
        String appNumber = (String) session.getAttribute("memberAppNumber");

        try {

            NeoManagerBean port = service.getNeoManagerBeanPort();
            System.out.println("In the member app command :)");

            search(port, request, response, context);
            String appNumStr = request.getParameter("memberAppNumber");

        } catch (Exception ex) {
            Logger.getLogger(MemberAppOutstandingDocSaveCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void search(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
 
        List<OutstandingDocuments> od = port.getOutstandingDocuments(Integer.parseInt(request.getParameter("memberAppNumber")));
        ArrayList<LookupValue> lookUps = (ArrayList<LookupValue>) NeoCommand.service.getNeoManagerBeanPort().getCodeTable(new Integer(349));
        ArrayList<LookupValue> docMeaning = new ArrayList<LookupValue>();
        NeoUser user = getNeoUser(request);
        HttpSession session = request.getSession();
        String appNumStr = request.getParameter("memberAppNumber");
        String memberNumber = (String) session.getAttribute("memberAppCoverNumber");


        System.out.println("Size of outstanding docs missing " + od.size());

        for (int x = 0; x < od.size(); x++) {
            for (int y = 0; y < lookUps.size(); y++) {
                if (od.get(x).getOutstandingDocumentsCode().equals(lookUps.get(y).getId())) {
                    LookupValue look = new LookupValue();

                    look.setId(lookUps.get(y).getId());
                    look.setValue(lookUps.get(y).getValue());
                    docMeaning.add(look);
                }
            }
        }
        request.setAttribute("docMeaning", docMeaning);
        request.setAttribute("memberAppCoverNumber", memberNumber);
        request.setAttribute("memberAppNumber", appNumStr);

        RequestDispatcher dispatcher = context.getRequestDispatcher(JSP_FOLDER + "OutstandingView.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    public String getName() {
        return "MemberOutStandingViewCommand";
    }
}

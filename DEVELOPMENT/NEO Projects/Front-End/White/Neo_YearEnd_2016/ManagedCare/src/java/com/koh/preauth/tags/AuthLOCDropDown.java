/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.agile.tags.TagMethods;
import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthHospitalLOC;
import neo.manager.LookupValue;

/**
 *
 * @author nick
 */
public class AuthLOCDropDown extends TagSupport {

    private String displayName;
    private String elementName;
    private String javaScript;
    private String mandatory = "no";
    private String errorValueFromSession = "no";
    private String firstIndexSelected = "no";

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        try {
            out.println("<td style=\"width:160px\"><label class=\"label\">" + displayName + ":</label></td>");
            //out.println("<td><select style=\"width:215px\" name=\"" + elementName + "\" id=\"" + elementName + "\"");
            out.println("<td><select name=\"" + elementName + "\" id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");
            String selectedBox = "" + session.getAttribute(elementName);
            //Neo Lookups
            String facilityDisc = "" + session.getAttribute("facilityProvDiscTypeId");
            int provDesc = Integer.parseInt(facilityDisc);
            int lookupId = getLookupID(provDesc);
            List<neo.manager.LookupValue> lookUps = NeoCommand.service.getNeoManagerBeanPort().getCodeTable(lookupId);

            //order by type_id
            Collections.sort(lookUps, new Comparator<LookupValue>() {
                public int compare(LookupValue o1, LookupValue o2) {
                    if (o1.getId() == null || o2.getId() == null) {
                        return 0;
                    }
                    return o1.getId().compareTo(o2.getId());
                }
            });

            //check if firstIndex is selected
            boolean isFirstIndex = false;
            if (firstIndexSelected.equalsIgnoreCase("yes")) {
                isFirstIndex = true;
            }

            if (isFirstIndex) {
                out.println("<option value=\"\"></option>");
            } else {
                out.println("<option value=\"99\"></option>");
            }

            boolean firstIndexSet = pageContext.getSession().getAttribute(elementName) == null || selectedBox.isEmpty() ? false : true;
            for (int i = 0; i < lookUps.size(); i++) {
                LookupValue lookupValue = lookUps.get(i);
                if (lookupValue.getId().equalsIgnoreCase(selectedBox)) {
                    out.println("<option value=\"" + lookupValue.getId() + "\" selected >" + lookupValue.getValue() + "</option>");
                } else {
                    if (isFirstIndex) {
                        if (firstIndexSet) {
                            out.println("<option value=\"" + lookupValue.getId() + "\" >" + lookupValue.getValue() + "</option>");
                        } else {
                            out.println("<option value=\"" + lookupValue.getId() + "\" selected >" + lookupValue.getValue() + "</option>");
                            firstIndexSet = true;
                        }
                    } else {
                        out.println("<option value=\"" + lookupValue.getId() + "\" >" + lookupValue.getValue() + "</option>");
                    }
                }
            }

            out.println("</select></td>");
            if (mandatory.equalsIgnoreCase("yes")) {
                out.println("<td><label class=\"red\">*</label></td>");
            } else {
                out.println("<td></td>");
            }

            String sessionValerror = "";
            if (errorValueFromSession.equalsIgnoreCase("yes")) {

                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                }

                if (sessionValerror != null && !sessionValerror.equalsIgnoreCase("null")) {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
                } else {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
                }
            }

            /*
             * if(errorValueFromSession.equalsIgnoreCase("yes")){ String
             * errorVal = "" + session.getAttribute(elementName + "_error");
             * if(errorVal == null || errorVal.trim().equalsIgnoreCase("null")){
             * errorVal = ""; } out.println("<td width=\"200px\"
             * align=\"left\"><label id=\"" + elementName + "_error\"
             * class=\"error\">"+ errorVal +"</label></td>"); }else{
             * out.println("<td width=\"200px\" align=\"left\"><label id=\"" +
             * elementName + "_error\" class=\"error\"></label></td>"); }
             */

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelNeoLookupValueDropDown tag", ex);
        }
        return super.doEndTag();

    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setErrorValueFromSession(String errorValueFromSession) {
        this.errorValueFromSession = errorValueFromSession;
    }

    public void setFirstIndexSelected(String firstIndexSelected) {
        this.firstIndexSelected = firstIndexSelected;
    }

    public int getLookupID(int providerType) {
        int locTypeID = 125;
        if (providerType == 79) {
            locTypeID = 272;

        } else if (providerType == 55) {
            locTypeID = 273;

        } else if (providerType == 59) {
            locTypeID = 274;

        } else if (providerType == 49) {
            locTypeID = 275;

        } else if (providerType == 76) {
            locTypeID = 276;

        } else if (providerType == 47) {
            locTypeID = 277;

        } else if (providerType == 56) {
            locTypeID = 278;

        } else if (providerType == 77) {
            locTypeID = 279;

        }
        return locTypeID;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
import neo.manager.ClaimLine;
import neo.manager.ClaimReversals;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author johanl
 */
public class ReverseClaimLine extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Date today = new Date(System.currentTimeMillis());
        PrintWriter out = null;

        int claimLineId = Integer.parseInt("" + session.getAttribute("memberDetailedClaimLineId"));
        ClaimLine claimLine = (ClaimLine) session.getAttribute("memCLCLDetails");

        String reversalCode = request.getParameter("reversalReason");
        String reversalCodeText = port.getValueFromCodeTableForId("Claim Reversal Codes", reversalCode);

        System.out.println("reversalCode = " + reversalCode);
        System.out.println("reversalCodeText = " + reversalCodeText);
        System.out.println("claimLineId = " + claimLineId);

        Security _secure = new Security();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        _secure.setCreatedBy(user.getUserId());
        _secure.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        _secure.setLastUpdatedBy(user.getUserId());
        _secure.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        _secure.setSecurityGroupId(user.getSecurityGroupId());

        ClaimReversals cr = new ClaimReversals();
        cr.setReversalCode(reversalCode);
        cr.setReversalDescription(reversalCodeText);

        String reversed = port.reverseClaimLine(claimLine, cr, _secure);
        try {
            out = response.getWriter();
            String returnMsg = "";
            if (reversed.equals("Sucsessful")) {
                returnMsg = "Done|ClaimLine " + claimLineId + " reversed";
            } else if (reversed.equals("Duplicate")) {
                returnMsg = "Error|ClaimLine " + claimLineId + " already reversed";
            } else if (reversed.equals("Reversal")) {
                returnMsg = "Error|ClaimLine " + claimLineId + " is a reversal line!";
            }
            out.println(returnMsg);
        } catch (IOException ex) {
            Logger.getLogger(ReverseClaimLine.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ReverseClaimLine";
    }
}

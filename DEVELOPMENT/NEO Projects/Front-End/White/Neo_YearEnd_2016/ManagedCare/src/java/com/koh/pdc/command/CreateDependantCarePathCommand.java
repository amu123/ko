/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.CalendarUtils;
import com.koh.utils.MapUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CarePathDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author nick
 */
public class CreateDependantCarePathCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("entered CreateDependantCarePathCommand");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        String cdlString = "" + request.getParameter("cpList");
        List<Integer> cdlList = new ArrayList<Integer>();

        int workListId = 0;
        try {
            workListId = Integer.parseInt("" + session.getAttribute("workListId"));
        } catch (NumberFormatException e) {
            System.out.println("Could not format workListId in session: " + e.getMessage());
        }
        System.out.println("Dependant work list id = " + workListId);
        
        if (cdlString != null && workListId != 0) {
            for (String i : cdlString.split(",")) {
                cdlList.add(Integer.parseInt(i));
            }

            System.out.println("Number of carepaths to be loaded = " + cdlList.size());
            
            NeoUser u = (NeoUser) session.getAttribute("persist_user");
            //load care path template default tasks for each disease and set them in session
            List<CarePathDetails> cpList = port.createDepCarePath(workListId, cdlList, u);
            Map<LookupValue, List<CarePathDetails>> cpUnassignedMap = MapUtils.buildMapForPDCCarePath(cpList, workListId, 0);
            session.setAttribute("depCarePathTaskUnassignedMap", cpUnassignedMap);

            Map<LookupValue, List<CarePathDetails>> cpAssignedMap = MapUtils.buildMapForPDCCarePath(cpList, workListId, 1);
            session.setAttribute("depCarePathTaskAssignedMap", cpAssignedMap);

            CalendarUtils cu = new CalendarUtils();

            List<String> years = cu.getCurrentPreviousAndNextYear();
            session.setAttribute("pdcUsableYears", years);
            session.setAttribute("pdcCurrentSelectedYear", years.get(1));
            session.setAttribute("pdcCurrentSelectedYearID", 1);
            session.setAttribute("pdcCarePathScrollTrue", "false");

            List<neo.manager.LookupValue> allMonths = NeoCommand.service.getNeoManagerBeanPort().getCodeTable(338);

            Collections.sort(allMonths, new Comparator<LookupValue>() {
                public int compare(LookupValue o1, LookupValue o2) {
                    if (o1.getId() == null || o2.getId() == null) {
                        return 0;
                    }
                    
                    return new Integer(o1.getId()).compareTo(new Integer(o2.getId()));
                }
            });

            session.setAttribute("pdcAllMonths", allMonths);

            List<Integer> weeks = new ArrayList<Integer>();
            weeks.add(1);
            weeks.add(2);
            weeks.add(3);
            weeks.add(4);
            weeks.add(5);

            session.setAttribute("pdcUsableWeeks", weeks);

            Calendar c = Calendar.getInstance();
            int currentMonth = c.get(Calendar.MONTH) + 1;

            List<LookupValue> sixMonths = new ArrayList<LookupValue>();
            if (currentMonth <= 6) {
                //get first 6 months for initial calendar display
                for (int i = 0; i < 6; i++) {
                    sixMonths.add(allMonths.get(i));
                }
            } else {
                //get last 6 months for initial calendar display
                for (int i = 6; i < 12; i++) {
                    sixMonths.add(allMonths.get(i));
                }
            }

            session.setAttribute("pdc6Months", sixMonths);

            List<neo.manager.LookupValue> pdcTaskTypes = NeoCommand.service.getNeoManagerBeanPort().getCodeTable(337);
            session.setAttribute("pdcTaskTypeLookup", pdcTaskTypes);

            try {
                String nextJSP = "/PDC/ViewDependantCarepathDetails.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            try {
                String nextJSP = "/PDC/SelectNewChronicCondition.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);
                session.setAttribute("pdcCarePathError", "Please select at least one care path...");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return null;
    }

    @Override
    public String getName() {
        return "CreateDependantCarePathCommand";
    }

}

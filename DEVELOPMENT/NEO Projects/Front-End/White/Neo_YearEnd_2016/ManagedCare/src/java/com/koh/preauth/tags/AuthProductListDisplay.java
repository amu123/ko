/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import java.math.BigDecimal;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthNappiProduct;

/**
 *
 * @author martins
 */
public class AuthProductListDisplay extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        List<AuthNappiProduct> dbproductList = null;

        try {
            out.println("<td colspan=\"7\"><table width=\"800\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px; cellpadding:5;\">");
            try {
                dbproductList = NeoCommand.service.getNeoManagerBeanPort().getNappiUpdate(session.getAttribute("updateAuthNumber").toString().trim());
            } catch (Exception e) {
                System.out.println("No nappi products found");
            }

            List<AuthNappiProduct> productList = (List<AuthNappiProduct>) session.getAttribute("productNappiList");
            out.println("<tr><th align=\"left\">Product Code</th><th align=\"left\">Product Description</th><th align=\"left\">Quantity</th><th align=\"left\">Price</th><th align=\"left\">Status</th><th align=\"left\">Date From</th><th align=\"left\">Date To</th></tr>");
            if (productList != null) {
                for (int i = 1; i <= productList.size(); i++) {
                    AuthNappiProduct anp = productList.get(i - 1);

                    out.println("<tr id=\"anpRow" + anp.getNappiCode() + i + "\"><form>");
                    out.println("<td><label class=\"label\">" + anp.getNappiCode() + "</label></td>");
                    out.println("<td><label class=\"label\">" + anp.getProductDesc() + "</label></td>");
                    out.println("<td><label class=\"label\">" + anp.getQuantity() + "</label></td>");

                    out.println("<td><label class=\"label\">" + "R" + anp.getRand() + "</label></td>");
//                    out.println("<td><label class=\"label\">" + anp.getICD10()+ "</label></td>");
                    out.println("<td><label class=\"label\">" + anp.getStatus() + "</label></td>");
                    out.println("<td><label class=\"label\">" + anp.getDateFrom() + "</label></td>");
                    out.println("<td><label class=\"label\">" + anp.getDateTo() + "</label></td>");
                }
            }

            if (session.getAttribute("updateAuthNumber") != null) {
                System.out.println("### updateAuthNumber " + session.getAttribute("updateAuthNumber"));
                if (dbproductList != null) {
                    for (int i = 1; i <= dbproductList.size(); i++) {
                        AuthNappiProduct anp = dbproductList.get(i - 1);

                        out.println("<tr id=\"anpRow" + anp.getNappiCode() + i + "\"><form>");
                        out.println("<td><label class=\"label\">" + anp.getNappiCode() + "</label></td>");
                        out.println("<td><label class=\"label\">" + anp.getProductDesc() + "</label></td>");
                        out.println("<td><label class=\"label\">" + anp.getQuantity() + "</label></td>");
                        out.println("<td><label class=\"label\">" + "R" + anp.getRand() + "</label></td>");

                        out.println("<td><label class=\"label\">" + anp.getStatus() + "</label></td>");
                        out.println("<td><label class=\"label\">" + anp.getDateFrom() + "</label></td>");
                        out.println("<td><label class=\"label\">" + anp.getDateTo() + "</label></td>");
                        out.println("</form></tr>");
                    }
                }
            }
            out.println("</table></td>");

        } catch (java.io.IOException ex) {
            ex.printStackTrace();
        }

        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

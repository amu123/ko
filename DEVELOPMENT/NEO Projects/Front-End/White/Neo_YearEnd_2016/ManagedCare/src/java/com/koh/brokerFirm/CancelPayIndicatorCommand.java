/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.brokerFirm;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.BrokerFirm;
import neo.manager.BrokerFirmConcessionDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author chrisdp
 */
public class CancelPayIndicatorCommand extends NeoCommand {

    private static final String JSP_FOLDER = "/Broker/";
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
         int firmEntityId = Integer.parseInt(request.getParameter("brokerFirmEntityId"));   
         
         String page = "";
         page = getFirmDetails(request, firmEntityId);

         try {
            context.getRequestDispatcher(page).forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(CancelPayIndicatorCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CancelPayIndicatorCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        return null;
    }

    @Override
    public String getName() {
        return "CancelPayIndicatorCommand";
    }
    
    public String getFirmDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        if (entityId != 0) {
            String tab = "FirmAppDetails";
            BrokerFirm bf = port.fetchBrokerFirmByEntityId(entityId);
            BrokerFirmMainMapping.setFirmApplication(request, bf);
            BrokerFirmConcessionDetails ba = port.fetchBrokerConcessionByBrokerEntity(entityId);
            BrokerFirmMainMapping.setConcessionFirmApplication(request, ba);
            String commInd = "";

            if (bf.getCommisionInd() == 0) {
                commInd = "No";
            } else if (bf.getCommisionInd() == 1) {
                commInd = "Yes";
            }

            request.setAttribute("firmAppName", bf.getBrokerFirmName());
            request.setAttribute("firmAppCode", bf.getBrokerFirmCode());
            request.setAttribute("brokerFirmEntityId", bf.getEntityId());
            request.setAttribute("brokerFirmTile", bf.getContactTitle());
            request.setAttribute("brokerFirmContactPerson", bf.getContactFirstName());
            request.setAttribute("brokerFirmContactSurname", bf.getContactLastName());
            request.setAttribute("firmRegion", bf.getRegion());
            request.setAttribute("VIPBroker", bf.getVipBrokerInd());
            request.setAttribute("commissionInd", commInd);
            session.setAttribute("currentCommissionInd", bf.getCommisionInd());
            if (ba != null) {
                request.setAttribute("brokerFirmEntityId", ba.getFirmEntityId());
                request.setAttribute("concessionFlag", ba.getFirmEntityId());
                request.setAttribute("FirmApp_ConcessionStart", DateTimeUtils.convertXMLGregorianCalendarToDate(ba.getConcessionStartDate()));
                request.setAttribute("FirmApp_ConcessionEnd", DateTimeUtils.convertXMLGregorianCalendarToDate(ba.getConcessionEndDate()));
            }
        }
        return JSP_FOLDER + "BrokerFirm.jsp";
    }   
}

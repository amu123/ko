/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

import com.koh.command.NeoCommand;
import com.koh.utils.UrlUtil;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.NeoMenu;
import neo.manager.NeoUser;
import neo.manager.SecurityResponsibility;
import neo.manager.WorkflowItem;
import neo.manager.WorkflowItemDetails;

/**
 *
 * @author gerritr
 */
public class WorkflowPopulateOptionPanelCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("in WorkflowPopulateOptionPanelCommand");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        String location = "";
        ArrayList<WorkflowItemDetails> arrWfItemDetails;

        if (null != request.getParameter("action") && String.valueOf(request.getParameter("action")).contains(".jsp")) {
            location = String.valueOf(request.getParameter("action"));
        } else if (null != request.getParameter("onScreen") && String.valueOf(request.getParameter("onScreen")).contains(".jsp")) {
            location = String.valueOf(request.getParameter("onScreen"));
        } else if (null != request.getParameter("command") && String.valueOf(request.getParameter("command")).contains(".jsp")) {//NEOPANEL OnScreen command
            location = String.valueOf(request.getParameter("command"));
        }

        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        if (user != null) {
            ArrayList<SecurityResponsibility> respList = (ArrayList) user.getSecurityResponsibility();
            if (respList != null) {
                if (!location.isEmpty()) {

                    //<editor-fold defaultstate="collapsed" desc="WorkflowOptPnlContent.jsp">
                    if (location.contains("WorkflowOptPnlContent.jsp")) {
                        int itemId;
                        
                        if (null != request.getParameter("id")) {//on panel click
                            itemId = Integer.valueOf(request.getParameter("id"));
                            List<WorkflowItem> arrWFItem = port.getWorkflowItemByUserID(user.getUserId());
                            for (int i = 0; i < arrWFItem.size(); i++) {
                                if (arrWFItem.get(i).getItemID().equals(itemId)) {
                                    //<editor-fold defaultstate="collapsed" desc="Adding WorkFlow Status">
                                    List<LookupValue> arrWFStatus = port.getCodeTable(350);

                                    int wfItemStatus = arrWFItem.get(i).getStatusID();
                                    for (LookupValue wfStatus : arrWFStatus) {
                                        if (wfItemStatus == Integer.parseInt(String.valueOf(wfStatus.getId()))) {
                                            arrWFItem.get(i).setStatusName(wfStatus.getValue());
                                        }
                                    }
                                    List<LookupValue> arrWFMembershipSubType = null;
                                    
                                    if(arrWFItem.get(i).getQueueDesc().equalsIgnoreCase("Membership") ){
                                        arrWFMembershipSubType = port.getCodeTable(366);
                                    }
                                    if(arrWFItem.get(i).getQueueDesc().equalsIgnoreCase("Call Centre Admin") || arrWFItem.get(i).getQueueDesc().equalsIgnoreCase("Queries")){
                                        arrWFMembershipSubType = port.getCodeTable(367);
                                    }
                                    
                                    if(arrWFItem.get(i).getSubTypeID() != null && arrWFItem.get(i).getSubTypeID() != 0){
                                        int wfMembershipSubTypeID = arrWFItem.get(i).getSubTypeID();
                                        for (LookupValue wfMembershipSubType : arrWFMembershipSubType) {
                                            if (wfMembershipSubTypeID == Integer.parseInt(String.valueOf(wfMembershipSubType.getId()))) {
                                                arrWFItem.get(i).setSubTypeValue(wfMembershipSubType.getValue());
                                            }
                                        }
                                    }
                                   
                                    //</editor-fold>

                                    //<editor-fold defaultstate="collapsed" desc="Adding WorkFlow Complaint Flag">
                                    //Rather convert the char here than to do it in the JSP side
                                    if (null != arrWFItem.get(i).getIsComplaint()) {
                                        int intCharValueComplaint = arrWFItem.get(i).getIsComplaint();
                                        char charValueComplaint = (char) (intCharValueComplaint);
                                        String stringValueComplaint = Character.toString(charValueComplaint);

                                        request.setAttribute("isComplaint", stringValueComplaint);
                                    }
                                    //</editor-fold>
                                            
                                    request.setAttribute("wfPanel", arrWFItem.get(i));
                                    arrWFItem.get(i).getUserID();
                                    request.setAttribute("queueDesc", arrWFItem.get(i).getQueueDesc());
                                    NeoUser neoUsr = port.getUserSafeDetailsById(Integer.parseInt(String.valueOf(arrWFItem.get(i).getUserID())));
                                    request.setAttribute("userName", neoUsr.getUsername());
                                    request.setAttribute("entityNumber", arrWFItem.get(i).getEntityNumber());//JSP Page requires this [valueFromRequest="entityNumber"]
                                    break;
                                }
                            }

                            arrWfItemDetails = (ArrayList<WorkflowItemDetails>) port.getWorkflowItemDetailsByItemID(itemId);

                            for (int k = 0; k < arrWfItemDetails.size(); k++) {
                                String wfUserName = port.getNeoUserNameByID(arrWfItemDetails.get(k).getUserID());
                                arrWfItemDetails.get(k).setUserName(wfUserName);
                            }

                            request.setAttribute("arrWfItemDetails", arrWfItemDetails);

                            String emlLocation = port.getWorkflowMailItemLocationByItemID(itemId);
                            
                            emlLocation = emlLocation.replace("//", "\\\\\\\\");
                            emlLocation = emlLocation.replace("/", "\\\\");
                            System.out.println("email location : " + emlLocation);
                            if (null != emlLocation && emlLocation.contains(".eml")) {
                                File file = new File(emlLocation);
                                if (file.exists()) {
                                    request.setAttribute("emlLocation", emlLocation);
                                }
                            }
                        }
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="WorkflowForward.jsp">
                    if (location.contains("WorkflowForward.jsp")) {
                        int itemId;
                        if (null != request.getParameter("id")) {//on panel click
                            itemId = Integer.valueOf(request.getParameter("id"));
                            List<WorkflowItem> arrWFItem = port.getWorkflowItemByUserID(user.getUserId());

                            for (int i = 0; i < arrWFItem.size(); i++) {
                                if (arrWFItem.get(i).getItemID().equals(itemId)) {
                                    request.setAttribute("wfPanel", arrWFItem.get(i));
                                    break;
                                }
                            }
                        }
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="WorkflowReassign.jsp">
                    if (location.contains("WorkflowReassign.jsp")) {
                        int itemId;
                        if (null != request.getParameter("id")) {//on panel click
                            itemId = Integer.valueOf(request.getParameter("id"));
                            List<WorkflowItem> arrWFItem = port.getWorkflowItemByUserID(user.getUserId());

                            for (int i = 0; i < arrWFItem.size(); i++) {
                                if (arrWFItem.get(i).getItemID().equals(itemId)) {
                                    request.setAttribute("wfPanel", arrWFItem.get(i));

                                    break;
                                }
                            }
                        }
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="WorkflowChangeStatus.jsp">
                    if (location.contains("WorkflowChangeStatus.jsp")) {
                        int itemId;
                        if (null != request.getParameter("id")) {//on panel click
                            itemId = Integer.valueOf(request.getParameter("id"));
                            List<WorkflowItem> arrWFItem = port.getWorkflowItemByUserID(user.getUserId());

                            for (int i = 0; i < arrWFItem.size(); i++) {
                                if (arrWFItem.get(i).getItemID().equals(itemId)) {
                                    request.setAttribute("wfPanel", arrWFItem.get(i));

                                    break;
                                }
                            }
                        }
                    }
                    //</editor-fold>
                    
                    //<editor-fold defaultstate="collapsed" desc="WorkflowReplyEmail.jsp">
                    if (location.contains("WorkflowReplyEmail.jsp")) {
                        int itemId;
                        if (null != request.getParameter("id")) {//on panel click
                            itemId = Integer.valueOf(request.getParameter("id"));
                            List<WorkflowItem> arrWFItem = port.getWorkflowItemByUserID(user.getUserId());

                            for (int i = 0; i < arrWFItem.size(); i++) {
                                if (arrWFItem.get(i).getItemID().equals(itemId)) {
                                    request.setAttribute("wfPanel", arrWFItem.get(i));

                                    break;
                                }
                            }
                        }
                    }
                    //</editor-fold>
                    
                    //<editor-fold defaultstate="collapsed" desc="WorkflowUpdate.jsp">
                    if (location.contains("WorkflowUpdate.jsp")) {
                        int itemId;
                        if (null != request.getParameter("id")) {//on panel click
                            itemId = Integer.valueOf(request.getParameter("id"));
                            List<WorkflowItem> arrWFItem = port.getWorkflowItemByUserID(user.getUserId());

                            for (int i = 0; i < arrWFItem.size(); i++) {
                                if (arrWFItem.get(i).getItemID().equals(itemId)) {
                                    request.setAttribute("wfPanel", arrWFItem.get(i));
                                    request.setAttribute("catG", arrWFItem.get(i).getQueueDesc());
                                    break;
                                }
                            }
                        }
                    }
                    //</editor-fold>
                }
            }
        }
        try {
            request.getRequestDispatcher(location).forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(WorkflowContentCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WorkflowContentCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public ArrayList buildTree(ArrayList<NeoMenu> menuItems, int parent) {
        ArrayList arrMenu = new ArrayList();
        LookupValue lvMenu = null;
        for (int i = 0; i < menuItems.size(); i++) {
            NeoMenu item = menuItems.get(i);
            if (item.getParentMenuItem() == parent) {
                lvMenu = new LookupValue();

                if (item.getAttributes().getAction() != null) {
                    String href = UrlUtil.getUrl(item.getAttributes().getAction(), "appName", item.getDescription());
                    lvMenu.setId(item.getDisplayName());
                    lvMenu.setValue(href);

                    arrMenu.add(lvMenu);
                }
            }
        }
        return arrMenu;
    }

    @Override
    public String getName() {
        return "WorkflowPopulateOptionPanelCommand";
    }
}

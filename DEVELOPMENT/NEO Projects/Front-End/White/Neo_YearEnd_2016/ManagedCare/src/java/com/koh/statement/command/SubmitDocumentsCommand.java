/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import com.agile.security.webservice.EmailAttachment;
import com.agile.security.webservice.EmailContent;
import com.koh.command.FECommand;
import static com.koh.command.FECommand.service;
import com.koh.cover.commands.DocumentFilterCommand;
import com.koh.serv.PropertiesReader;
import java.io.*;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author sphephelot
 */
public class SubmitDocumentsCommand extends FECommand {

    private Logger logger = Logger.getLogger(SubmitDocumentsCommand.class);
    private static String errorReturnPage = "";
    private boolean emailSent;
    private byte[] array;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        logger.info("Inside SubmitDocumentsCommand");
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        SimpleDateFormat dTOs = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat dTOYear = new SimpleDateFormat("yyyy");
        SimpleDateFormat sTOd = new SimpleDateFormat("yyyy/MM/dd");

        String memberNo = "";
        String fileName = request.getParameter("fileLocation");
        String emailTo = (String) request.getParameter("ContactDetails_email");
        String docType = request.getParameter("value");

        try {
            String indexDrive = new PropertiesReader().getProperty("DocumentIndexIndexDrive");
            String fileLocation = request.getParameter("fileLocation");
            logger.info("fileLocation = " + fileLocation);
            if (fileLocation == null || fileLocation.equals("")) {
                printErrorResult(request, response);
                return null;
            }

            logger.info("Path: " + indexDrive + fileLocation);
            File file = new File(indexDrive + fileLocation);
            InputStream in = new FileInputStream(file);
            int contentLength = (int) file.length();
            logger.info("The content length is " + contentLength);
            ByteArrayOutputStream temporaryOutput;
            if (contentLength != -1) {
                temporaryOutput = new ByteArrayOutputStream(contentLength);
            } else {
                temporaryOutput = new ByteArrayOutputStream(20480);
            }

            byte[] bufer = new byte[512];
            while (true) {

                int len = in.read(bufer);
                if (len == -1) {

                    break;
                }
                temporaryOutput.write(bufer, 0, len);
            }
            in.close();
            temporaryOutput.close();

            array = temporaryOutput.toByteArray();

            if (emailTo != null && !emailTo.equalsIgnoreCase("")) {
                String emailFrom = "";
                String subject = "";
                emailFrom = "memberstatement@agilityghs.co.za";
                subject = "Member statement";

                String emailMsg = "Dear Member, <br/>"
                        + "Please find attached your monthly billing statement of contributions.";

                EmailContent content = new EmailContent();
                content.setContentType("text/plain");
                EmailAttachment attachment = new EmailAttachment();
                attachment.setContentType("application/octet-stream");
                attachment.setName(fileName);
                content.setFirstAttachment(attachment);
                content.setFirstAttachmentData(array);
                content.setSubject(subject);
                content.setEmailAddressFrom(emailFrom);
                content.setEmailAddressTo(emailTo);
                content.setProductId(0);
                //Determine msg for email according to type of document ('claim', 'statement', 'preAuth', 'benefit' 'custom', 'other')
                content.setType("custom");
                /*If document type is custom, this message added here will be displayed in the email. The email method will automatically add the 
                 the enquiries and call center aswell as the kind regards at the end of the message*/
                content.setContent(emailMsg);
                emailSent = service.getAgileManagerPort().sendEmailWithOrWithoutAttachment(content);
                logger.info("Email sent : " + emailSent);
                printEmailResult(request, response, emailSent);
            } else {
                printEmailResult(request, response, emailSent);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return emailSent;
    }

    @Override
    public String getName() {
        return "SubmitDocumentsCommand";
    }

    public void printEmailResult(HttpServletRequest request, HttpServletResponse response, boolean result) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (result) {
                out.println("<td><label class=\"header\">Statement emailed.</label></td>");
            } else {
                out.println("<td><label class=\"header\">Email sending failed, Either email address is missing or incorrect.</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "2; URL=" + errorReturnPage + "");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    public void printErrorResult(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");

            out.println("<td><label class=\"header\">No records found.</label></td>");

            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "2; URL=" + errorReturnPage + "");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }
}

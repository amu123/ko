/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.agile.security.webservice.AuthDetails;
import com.koh.command.FECommand;
import com.koh.command.NeoCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.NeoUser;
//import neo.manager.AuthHistoryDetails;

/**
 *
 * @author whauger
 */
public class SaveOrthoPreAuthDetailsCommand extends FECommand {

    private final NeoCommand neo = new NeoCommand() {

        @Override
        public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public String getName() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    };

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        try {
            HttpSession session = request.getSession();
            AuthDetails adSaved = (AuthDetails) session.getAttribute("FullAuthSearchResult");
            String authNumberRecieved = "" + session.getAttribute("memAuthNo");
            String authUpdate = "" + session.getAttribute("AuthUpdate");
            //AuthHistoryDetails ah = new AuthHistoryDetails();
            //Save generic auth details
            AuthDetails ad = new AuthDetails();

            //User detail
            NeoUser user = (NeoUser) session.getAttribute("persist_user");
            ad.setModifiedBy(user.getName() + " " + user.getSurname());
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTimeInMillis(System.currentTimeMillis());
            XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            ad.setModifiedDate(xmlCal);
            //set update history
            //ah.setCreatedBy(user.getName() + " " + user.getSurname());
           // ah.setAuthType("Dental Orthodontic");

            //Auth detail page
            ad.setScheme("" + session.getAttribute("scheme"));
            ad.setSchemeOption("" + session.getAttribute("schemeOption"));
            ad.setAuthType("" + session.getAttribute("authType"));
            ad.setAuthorisationDate("" + session.getAttribute("authDate"));
            ad.setMemberNo("" + session.getAttribute("memNo_text"));
            String memberInfo = "" + session.getAttribute("memNo");
            String[] tokens = memberInfo.split("\\s");
            int num = tokens.length;
            String surname = "";
            ad.setFullnames(tokens[0]);
            for (int i = 1; i < num - 1; i++) {
                if (surname.equalsIgnoreCase("")) {
                    surname = tokens[i];
                } else {
                    surname = surname + " " + tokens[i];
                }
            }
            ad.setSurname(surname);
            ad.setDependantCode(tokens[num - 1]);
            ad.setCallerName("" + session.getAttribute("callerName"));
            ad.setCallerRelationship("" + session.getAttribute("callerRelationship"));
            ad.setCallerReason("" + session.getAttribute("callerReason"));
            ad.setCallerContact("" + session.getAttribute("callerContact"));

            //Orthodontic detail page
            ad.setTreatingProviderNo(this.getValue(session.getAttribute("treatingProv_text")));
            ad.setTreatingProvider(this.getValue(session.getAttribute("treatingProv")));
            ad.setReferringProviderNo(this.getValue(session.getAttribute("referringProv_text")));
            ad.setReferringProvider(this.getValue(session.getAttribute("referringProv")));
            String icd = (String) session.getAttribute("picd10");
            int pos = icd.indexOf(" ");
            if (pos > 0) {
                String icdCode = icd.substring(0, pos);
                ad.setPrimaryIcd(icdCode);
            } else {
                ad.setPrimaryIcd(icd);
            }

            ad.setTarrifList("" + session.getAttribute("tariff"));
            ad.setPlannedDuration("" + session.getAttribute("numMonths"));
            ad.setEstimatedCost("" + session.getAttribute("amountClaimed"));
            ad.setDepositAmount("" + session.getAttribute("depositAmount"));
            ad.setFirstInstallmentAmount("" + session.getAttribute("firstAmount"));
            ad.setRemainingInstallmentAmount("" + session.getAttribute("remainAmount"));
            ad.setDateValidFrom("" + session.getAttribute("authFromDate"));
            ad.setDateValidTo("" + session.getAttribute("authToDate"));
            ad.setNotes("" + session.getAttribute("notes"));
            ad.setAuthStatus("" + session.getAttribute("status"));

            if ((authUpdate != null) && (!authUpdate.trim().equalsIgnoreCase("")) &&
                    (!authUpdate.trim().equalsIgnoreCase("null")) &&
                    (authUpdate.trim().equalsIgnoreCase("true"))) {

                //create populated member info
                String memInfo = "" + session.getAttribute("memNo");
                String[] t = memInfo.split("\\s");
                int nums = t.length;
                String memSurname = "";
                adSaved.setFullnames(t[0]);
                for (int i = 1; i < num - 1; i++) {
                    if (memSurname.equalsIgnoreCase("")) {
                        memSurname = t[i];
                    } else {
                        memSurname = memSurname + " " + t[i];
                    }
                }
                adSaved.setSurname(memSurname);
                adSaved.setDependantCode(t[nums - 1]);
                ad.setAuthCode(authNumberRecieved);

                String xml = "";//port.xmlForAuth(adSaved, ad, ah.getAuthType());
                //ah.setXmlInput(xml);
//                System.out.println("========================");
//                System.out.println("xml recieved from backend = "+xml);
//                System.out.println("========================");
//                System.out.println("xmlInput set = "+ah.getXmlInput());
//                System.out.println("========================");
                @SuppressWarnings("static-access")
                int historyId = 0;//neo.port.saveAuthUpdateHistory(ah);
                System.out.println("ortho auth history data saved for auth_update_history_id " + historyId);
                if (historyId != 0) {
                    AuthDetails aResult = null;//port.updateDentalAuth(adSaved, ad, historyId);
                    session.setAttribute("authNumber", aResult.getAuthCode());
                    SavePreAuthOrthoPlanDetails command = new SavePreAuthOrthoPlanDetails();
                    command.execute(request, response, context);
                    System.out.println("Returned caller from update saved = " + aResult.getCallerName());
                    if (aResult != null) {
//                        String aId = String.valueOf(aResult.getAuthId());
//                        File f = new File("C:/Projects/ManagedCare/web/resources/OrthodonticReport.jrxml");
//                        System.out.println(f.exists());
//                        Map<String, Object> params = new HashMap<String, Object>();
//                        params.put("auth_id", aId);
//                        JasperDesign jasperDesign = JRXmlLoader.load(f);
//                        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
//                        Connection jdbcConnection = new DatabaseConnection().connectDB();
//                        JasperPrint printer = JasperFillManager.fillReport(jasperReport, params, jdbcConnection);

                        //For html
//                        PrintWriter out = response.getWriter();
//
//                        //JRHtmlExporter exporter = new JRHtmlExporter();
//                        //exporter.setParameter(JRExporterParameter.JASPER_PRINT, printer);
//                        //exporter.setParameter(JRExporterParameter.OUTPUT_WRITER, out);
//
//                        JRHtmlExporter exporter = new JRHtmlExporter();
//
//                        request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, printer);
//                        exporter.setParameter(JRHtmlExporterParameter.JASPER_PRINT, printer);
//                        exporter.setParameter(JRHtmlExporterParameter.OUTPUT_WRITER, out);
//
//                        exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, "ManagedCare/resources/reso_logo.jpg");
//                        Map imagesMap = new HashMap();
//                        request.getSession().setAttribute("IMAGES_MAP", imagesMap);
//                        exporter.setParameter(JRHtmlExporterParameter.IMAGES_MAP, imagesMap);
//
//                        exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
//                        exporter.setParameter(JRHtmlExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.FALSE);
//                        exporter.setParameter(JRHtmlExporterParameter.SIZE_UNIT, "px");
//
//                        exporter.exportReport();

//                        //for PDF
//                        response.setContentType("application/pdf");
//                        response.setContentLength(0);
//                        byte[] pdf = JasperExportManager.exportReportToPdf(printer);
//
//                        response.reset();
//                        ServletOutputStream outt = response.getOutputStream();
//
//                        outt.write(pdf, 0, pdf.length);
//                        outt.flush();
//                        outt.close();


                        //exporter.exportReport();

                        //out.close();

                    } else {
                        printFailedResult(request, response);
                    //this.clearAllFromSession(request);
                    }

                } else {
                    printFailedResult(request, response);
                //this.clearAllFromSession(request);
                }

            } else {

                //Save auth object
                String authNumber = service.getAgileManagerPort().insertAuth(ad);

                if (authNumber != null && !authNumber.equalsIgnoreCase("")) {
                    session.setAttribute("authNumber", authNumber);
                    SavePreAuthOrthoPlanDetails command = new SavePreAuthOrthoPlanDetails();
                    command.execute(request, response, context);
                } else {
                    //Create response page
                    printFailedResult(request, response);
                }
            }

        //Save orthodontic plan details
            /*
        OrthodonticPlanDetails oplan = new OrthodonticPlanDetails();

        oplan.setAuthNumber(authNumber);
        oplan.setAuthDate(this.webserviceConvert("" + session.getAttribute("authDate"), "yyyy/MM/dd"));
        oplan.setCoverNumber("" + session.getAttribute("memNo_text"));
        oplan.setDependentNumber(new Integer(tokens[2].substring(1)));
        oplan.setProviderNumber("" + session.getAttribute("treatingProv_text"));
        oplan.setTariffCode("" + session.getAttribute("tariff"));
        oplan.setDuration((Integer)session.getAttribute("numMonths"));
        oplan.setTotalAmount((Double)session.getAttribute("amountClaim"));
        oplan.setDeposit((Double)session.getAttribute("depositAmount"));
        oplan.setFirstInstallment((Double)session.getAttribute("firstAmount"));
        oplan.setMonthlyInstallment((Double)session.getAttribute("remainAmount"));
        oplan.setPlanStartDate(this.webserviceConvert("" + session.getAttribute("authFromDate"), "yyyy/MM/dd"));
        oplan.setPlanEstimatedEndDate(this.webserviceConvert("" + session.getAttribute("authToDate"), "yyyy/MM/dd"));

        port.insertOrthodonticPlan(oplan);
         */
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void printFailedResult(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            out.println("<td><label class=\"header\">Authorisation saving failed.</label></td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

        } catch (IOException ex) {
            Logger.getLogger(SavePreAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    @Override
    public String getName() {
        return "SaveOrthoPreAuthDetailsCommand";
    }

    private String getValue(Object obj) {
        String value = "";
        if (obj != null) {
            value = value + obj;
        }
        return value;
    }
}

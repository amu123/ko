/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.tags;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author Johan-NB
 */
public class AuthNewNappiSearchResult extends TagSupport {
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        ServletRequest req = pageContext.getRequest();

        List<AuthTariffDetails> ntList = (List<AuthTariffDetails>) session.getAttribute("authNNSearchResults");
        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");

            String message = (String) req.getAttribute("authNNSearchResultsMessage");

            if (message != null && !message.equalsIgnoreCase("")) {
                out.println("<label class=\"red\">" + message + "</label></br></br>");
            }

            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Nappi Code</th><th>Nappi Desc</th><th></th></tr>");

            if (ntList != null) {
                for (AuthTariffDetails at : ntList) {

                    out.println("<tr><form>"
                            + "<td>"
                            + "<label class=\"label\">" + at.getTariffCode() + "</label>"
                            + "<input type=\"hidden\" id=\"nc\" name=\"nc\" value=\"" + at.getTariffCode() + "\"/>"
                            + "</td>"
                            + "<td width=\"200\">"
                            + "<label class=\"label\">" + at.getTariffDesc() + "</label>"
                            + "<input type=\"hidden\" id=\"nd\" name=\"nd\" value=\"" + at.getTariffDesc() + "\"/>"
                            + "</td>"
                            + "<td>"
                            + "<button name=\"opperation\" type=\"submit\" " + javaScript + " value=\"" + commandName + "\">Select</button>"
                            + "</td>"
                            + "</form></tr>");
                }
            }
            out.println("</table>");

        } catch (IOException io) {
            io.printStackTrace();
        }

        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

}

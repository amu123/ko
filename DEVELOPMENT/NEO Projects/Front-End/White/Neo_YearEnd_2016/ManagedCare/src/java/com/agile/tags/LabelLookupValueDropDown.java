/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import com.agile.security.webservice.LookupValue;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class LabelLookupValueDropDown extends TagSupport {

    private String displayName;
    private String elementName;
    private String lookupId;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            out.println("<td><label class=\"label\">" + displayName + ":</label></td>");
            out.println("<td><select name=\"" + elementName + "\" id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");
            String selectedBox = "" + session.getAttribute(elementName);
            ArrayList<LookupValue> lookUps = (ArrayList<LookupValue>) TagMethods.getLookupValue(new Integer(lookupId));
            for (int i = 0; i < lookUps.size(); i++) {
                LookupValue lookupValue = lookUps.get(i);
                if (lookupValue.getId().toString().equalsIgnoreCase(selectedBox)) {
                    out.println("<option value=\"" + lookupValue.getId() + "\" selected >" + lookupValue.getValue() + "</option>");
                } else {
                    out.println("<option value=\"" + lookupValue.getId() + "\" >" + lookupValue.getValue() + "</option>");
                }

            }
            out.println("</select></td>");

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelLookupValueDropDown tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.koh.command.NeoCommand;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverDetails;
import neo.manager.Member;
import neo.manager.MemberEmployerBrokerInfo;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class ViewDependantsForCover extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        String onScreen = request.getParameter("onScreen");
        String coverNumber = request.getParameter("coverNum");
        session.setAttribute("coverNum", coverNumber);

//        System.out.println("onScreen = "+onScreen);
//        System.out.println("coverNumber = "+coverNumber);

        List<CoverDetails> covList = port.getDependantsForPrincipleMemberByDate(coverNumber);
        MemberEmployerBrokerInfo memberEmployerBrokerInfo = port.getMemberEmployerBrokerInfo(coverNumber);
        //System.out.println("member dependant size = " + covList.size());
        session.setAttribute("MemberCoverDependantDetails", covList);
        if (covList != null && covList.size() > 0) {
            session.setAttribute("schemeName", covList.get(0).getProductName());
        } else {
            session.setAttribute("schemeName", "N/A");
        }
        session.setAttribute("memberEmployerBrokerInfo", memberEmployerBrokerInfo);
        
        int entityId = 0;
        for (CoverDetails cv : covList) {
            if(cv.getDependentTypeId() == 17){
                entityId = cv.getEntityId();
                break;
            }
        }
        
        Member mem = port.fetchMemberAddInfo(entityId);
        
        session.setAttribute("showHandedOverFlag", "false");
        if (mem == null || mem.getHasDebt() == null){
            session.setAttribute("hasDebt", '0');
            session.setAttribute("debtHandedOver", null);
            session.setAttribute("debtHandedOverDate", null);
        } else {            
            session.setAttribute("hasDebt", mem.getHasDebt());
            session.setAttribute("debtHandedOver", mem.getDebtHandedOver());
            session.setAttribute("debtHandedOverDate", mem.getDebtHandedOverDate());
            
            GregorianCalendar today = new GregorianCalendar();
            if(mem.getDebtHandedOverDate() != null && mem.getDebtHandedOver().equalsIgnoreCase("1")){
                if(mem.getDebtHandedOverDate().toGregorianCalendar().before(today) || mem.getDebtHandedOverDate().toGregorianCalendar().equals(today)){
                    session.setAttribute("showHandedOverFlag", "true");
                }
            }
        }
        
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(onScreen);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewDependantsForCover";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Diagnosis;
import neo.manager.NeoManagerBean;

/**
 *
 * @author josephm
 */
public class LoadICDLongDescriptionsCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
        String icd10 = request.getParameter("icd10");

        NeoManagerBean port = service.getNeoManagerBeanPort();

        //String provCat = port.getValueFromCodeTableForTableId(TABLE ID(59),LOOKUP_ID);
        String icd10Code = port.getValueFromCodeTableForTableId(120, icd10);

        Diagnosis diagnosis = port.getDiagnosisForCode(icd10Code);

        //System.out.println("The code is " + icd10);
        //System.out.println("The coverted code is " + icd10Code);

        try {
            PrintWriter out = response.getWriter();
            session.setAttribute("icdDescription", diagnosis.getDescription());

            if(icd10 != null && !icd10.equalsIgnoreCase("")) {

                out.print(getName() + "|Description=" + diagnosis.getDescription() + "$");
            }else {

                out.print("|Error|No such Such Diagnosis " + getName());
            }
            System.out.println("The icd description is " + diagnosis.getDescription() + " and the code is " + icd10);

        }catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "LoadICDLongDescriptionsCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Diagnosis;

/**
 *
 * @author johanl
 */
public class GetMultipleICD10DetailsByCode extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;

        String codes = request.getParameter("code");
        String element = request.getParameter("element");
        String[] icds = null;
        String errors = "";
        int errorCount = 0;
        try {
            out = response.getWriter();
            if (codes != null && !codes.trim().equalsIgnoreCase("")) {
                icds = codes.split(",");
                System.out.println("icd size = " + icds.length);
                String icdDesc = "";
                for (String code : icds) {
                    Diagnosis d = service.getNeoManagerBeanPort().getDiagnosisForCode(code.trim());
                    if (d == null) {
                        errors = errors + code + " ";
                        errorCount++;
                    }else{
                      icdDesc += d.getDescription() +", ";
                    }
                }
                System.out.println("multiple icd desc = "+icdDesc);
                if(!icdDesc.equalsIgnoreCase("")){
                    icdDesc = icdDesc.substring(0, icdDesc.lastIndexOf(",") -1 );
                }
                System.out.println("new multiple icd desc = "+icdDesc);
                session.setAttribute(element + "Code", codes);
                System.out.println("errors = "+errors);
                if(errorCount != 0){
                    errors = "No such Diagnosis : " + errors;
                    out.println("Error|"+errors);
                }else{
                    out.println("Done|"+icdDesc);
                }
            }else{
                out.println("Error|ICD field not set");
            }
        } catch (Exception e) {
            System.out.println("multiple icd error : "+e.getMessage());
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetMultipleICD10DetailsByCode";
    }
}

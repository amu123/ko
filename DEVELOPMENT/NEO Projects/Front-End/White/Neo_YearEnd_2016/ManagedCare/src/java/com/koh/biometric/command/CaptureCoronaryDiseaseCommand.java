/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import java.lang.Exception;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author josephm
 */
public class CaptureCoronaryDiseaseCommand extends NeoCommand {
    private String nextJSP;
    private Object _datatypeFactory;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        Enumeration<String> e = request.getParameterNames();
        while (e.hasMoreElements()) {
            String param = e.nextElement();
        }
        
        HttpSession session = request.getSession();
        boolean checkError = false;
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        
        //clearAllFromSession(request);
        CoronaryBiometrics coronary = new CoronaryBiometrics();
        coronary.setBiometricTypeId("6");
        
        NeoManagerBean port = service.getNeoManagerBeanPort();

        //String exerciseInWeek = request.getParameter("exerciseQuestion");
        String exerciseInWeek = (String) session.getAttribute("exerciseQuestion_text");
        if (exerciseInWeek != null && !exerciseInWeek.equalsIgnoreCase("")) {
            if(exerciseInWeek.matches("[0-9]+")){
           int exerciseInWeekValue = Integer.parseInt(exerciseInWeek);
            coronary.setExercisePerWeek(exerciseInWeekValue);
            }else {
            
                session.setAttribute("exerciseQuestion_error", "The exercise field should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("exerciseQuestion_error", "The exercise field should be filled in");
            checkError = true;
        }*/
        
        //String currentSmoker = request.getParameter("currentSmoker");
        String currentSmoker = (String) session.getAttribute("currentSmoker_text");
        if (currentSmoker != null && !currentSmoker.equalsIgnoreCase("99")) {
            coronary.setCurrentSmoker(currentSmoker);
            LookupValue smoker = new LookupValue();
            smoker.setId(currentSmoker);
            smoker.setValue(port.getValueFromCodeTableForId("YesNo Type", currentSmoker));
            coronary.setCurrentSmokerLookup(smoker);
        } /*else {

            session.setAttribute("currentSmoker_error", "The current smoker field should be filled in");
            checkError = true;
        }*/
        
        //String numberOfCigaretes = request.getParameter("smokingQuestion");
        String numberOfCigaretes = (String) session.getAttribute("smokingQuestion_text");
        if (numberOfCigaretes != null && !numberOfCigaretes.equalsIgnoreCase("")) {
            if(numberOfCigaretes.matches("[0-9]+")){
            int numberOfCigaretesValue = Integer.parseInt(numberOfCigaretes);
            coronary.setCigarettesPerDay(numberOfCigaretesValue);
            }else {
            
                session.setAttribute("smokingQuestion_error", "The cigarettes field should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("smokingQuestion_error", "The cigarettes field should be filled in");
            checkError = true;
        }*/
        
        //String exSmoker = request.getParameter("exSmoker");
        String exSmoker = (String) session.getAttribute("exSmoker_text");
        if (exSmoker != null && !exSmoker.equalsIgnoreCase("99")) {
            coronary.setExSmoker(exSmoker);
        } /*else {

            session.setAttribute("exSmoker_error", "The ex smoker field should be filled in");
            checkError = true;
        }*/
        
        //String yearsSinceSmoking = request.getParameter("stopped");
        String yearsSinceSmoking = (String) session.getAttribute("stopped_text");
        if (yearsSinceSmoking != null && !yearsSinceSmoking.equalsIgnoreCase("")) {
            if (yearsSinceSmoking.matches("[0-9]+")) {
                int yearsSinceSmokingValue = Integer.parseInt(yearsSinceSmoking);
                coronary.setYearsSinceStopped(yearsSinceSmokingValue);
                 session.setAttribute("stopped_error", "");
            } else {
                session.setAttribute("stopped_error", "The years stopped field should be a numeric value");
                checkError = true;
            }
        }

        //String alcoholUnits = request.getParameter("alcoholConsumption");
        String alcoholUnits = (String) session.getAttribute("alcoholConsumption_text");
        if (alcoholUnits != null && !alcoholUnits.equalsIgnoreCase("")) {
            if(alcoholUnits.matches("[0-9]+")){
            int alcoholUnitsValue = Integer.parseInt(alcoholUnits);
            coronary.setAlcoholUnitsPerWeek(alcoholUnitsValue);
            }else {
            
                session.setAttribute("alcoholConsumption_error", "The alcohol units field should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("alcoholConsumption_error", "The alcohol units field should be filled in");
            checkError = true;
        }*/
        
        //String weight = request.getParameter("weight");
        //String height = request.getParameter("length");
        String weight = (String) session.getAttribute("weight_text");
        String height = (String) session.getAttribute("length_text");
        if (weight != null && !weight.equalsIgnoreCase("")) {
            if (weight.matches("([0-9]+(\\.[0-9]+)?)+")) {
                if (height != null && !height.equalsIgnoreCase("")) {
                    if (height.matches("([0-9]+(\\.[0-9]+)?)+")) {
                        double heightValue = Double.parseDouble(height);
                        double weightValue = Double.parseDouble(weight);
                        coronary.setHeight(heightValue);
                        coronary.setWeight(weightValue);
                    } else {
                        session.setAttribute("length_error", "The length should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("length_error", "Require length to calculate BMI");
                    checkError = true;
                }
            } else {

                session.setAttribute("weight_error", "The weight should be a numeric value");
                checkError = true;
            }
        }/*else {

         session.setAttribute("weight_error", "The weight should be filled in");
         checkError = true;
         }*/
        
        if (height != null && !height.equalsIgnoreCase("")) {
            if (height.matches("([0-9]+(\\.[0-9]+)?)+")) {
                if (weight != null && !weight.equalsIgnoreCase("")) {
                    if (weight.matches("([0-9]+(\\.[0-9]+)?)+")) {
                        double heightValue = Double.parseDouble(height);
                        double weightValue = Double.parseDouble(weight);
                        coronary.setHeight(heightValue);
                        coronary.setWeight(weightValue);
                    } else {
                        session.setAttribute("weight_error", "The weight should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("weight_error", "Require weight to calculate BMI");
                    checkError = true;
                }
            } else {

                session.setAttribute("length_error", "The length should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("length_error", "The length should be filled in");
         checkError = true;
         }*/
        
        //String bmi = request.getParameter("bmi");
        String bmi = (String) session.getAttribute("bmi_text");
        if (bmi != null && !bmi.equalsIgnoreCase("")) {
            if(bmi.matches("([0-9]+(\\.[0-9]+)?)+")){
            double bmiValue = Double.parseDouble(bmi);
            coronary.setBmi(bmiValue);
            coronary.setBmiCoronary(bmiValue);
            }else {
            
                session.setAttribute("bmi_error", "The BMI should be a numeric value");
                checkError = true;
            }
        }/*else {

            session.setAttribute("bmi_error", "The BMI should be filled in");
            checkError = true;
        }*/
        
        //String bloodPresureSystolic = request.getParameter("bpSystolic");
        //String bloodPressureDiastolic = request.getParameter("bpDiastolic");
        String bloodPresureSystolic = (String) session.getAttribute("bpSystolic_text");
        String bloodPressureDiastolic = (String) session.getAttribute("bpDiastolic_text");
        if (bloodPresureSystolic != null && !bloodPresureSystolic.equalsIgnoreCase("")) {
            if (bloodPresureSystolic.matches("[0-9]+")) {
                if (bloodPressureDiastolic != null && !bloodPressureDiastolic.equalsIgnoreCase("")) {
                    if (bloodPressureDiastolic.matches("[0-9]+")) {
                        int bloodvalueSys = Integer.parseInt(bloodPresureSystolic);
                        int bloodValueDia = Integer.parseInt(bloodPressureDiastolic);
                        coronary.setBloodPressureSystolic(bloodvalueSys);
                        coronary.setBloodPressureDiastolic(bloodValueDia);
                    } else {

                        session.setAttribute("bpDiastolic_error", "The blood pressure should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("bpDiastolic_error", "The blood pressure should be filled in");
                    checkError = true;
                }
            } else {

                session.setAttribute("bpSystolic_error", "The blood pressure should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bpSystolic_error", "The blood pressure should be filled in");
         checkError = true;
         }*/

        if (bloodPressureDiastolic != null && !bloodPressureDiastolic.equalsIgnoreCase("")) {
            if (bloodPressureDiastolic.matches("[0-9]+")) {
                if (bloodPresureSystolic != null && !bloodPresureSystolic.equalsIgnoreCase("")) {
                    if (bloodPresureSystolic.matches("[0-9]+")) {
                        int bloodvalueSys = Integer.parseInt(bloodPresureSystolic);
                        int bloodValueDia = Integer.parseInt(bloodPressureDiastolic);
                        coronary.setBloodPressureSystolic(bloodvalueSys);
                        coronary.setBloodPressureDiastolic(bloodValueDia);
                    } else {

                        session.setAttribute("bpSystolic_error", "The blood pressure should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("bpSystolic_error", "The blood pressure should be filled in");
                    checkError = true;
                }
            } else {

                session.setAttribute("bpDiastolic_error", "The blood pressure should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bpDiastolic_error", "The blood pressure should be filled in");
         checkError = true;
         }*/
        
        //String memberNumber = request.getParameter("memberNumber_text");
        String memberNumber = (String) session.getAttribute("memberNumber_text");
        if(memberNumber != null && !memberNumber.equalsIgnoreCase("")) {

            coronary.setMemberNumber(memberNumber);
        }else {

         session.setAttribute("memberNumber_error", "The member number should be filled in");
         checkError = true;
        }

        //String dependantCode = request.getParameter("depListValues");
        String dependantCode = (String) session.getAttribute("depListValues_text");
        if (dependantCode != null && !dependantCode.equalsIgnoreCase("99") && !dependantCode.equalsIgnoreCase("")
                && !dependantCode.equalsIgnoreCase("null")) {

            int code = Integer.parseInt(dependantCode);
            coronary.setDependantCode(code);
        } else {

            session.setAttribute("depListValues_error", "The cover dependants should be filled in");
            checkError = true;
        }  
           
        // Coronary specific biometrics
       // String hbAIC = request.getParameter("hbAIC");
        String hbAIC = (String) session.getAttribute("CoronaryHbAIC_text");
        if(hbAIC != null && !hbAIC.equalsIgnoreCase("")) {
            if(hbAIC.matches("([0-9]+(\\.[0-9]+)?)+")){
            double hbAICValue = Double.parseDouble(hbAIC);
            coronary.setHbAIC(hbAICValue);
            session.removeAttribute("CoronaryHbAIC_text");
            }else {
            
                session.setAttribute("CoronaryHbAIC_error", "The HB AIC should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("hbAIC_error", "The HB AIC should be filled in");
            checkError = true;
        }*/
        
        //String totalCholesterol = request.getParameter("totalCholesterol");
        String totalCholesterol = (String) session.getAttribute("CoronaryTotalCholesterol_text");
        if(totalCholesterol != null && !totalCholesterol.equalsIgnoreCase("")) {
            if(totalCholesterol.matches("([0-9]+(\\.[0-9]+)?)+")){
            double totalCholesterolValue = Double.parseDouble(totalCholesterol);
            coronary.setTotalCholesterol(totalCholesterolValue);
            session.removeAttribute("CoronaryTotalCholesterol_text");
            }else {
            
                session.setAttribute("CoronaryTotalCholesterol_error", "The cholesterol should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("totalCholesterol_error", "The cholesterol should be filled in");
            checkError = true;
        }*/
        
        //String ldlcMol = request.getParameter("ldlc");
        String ldlcMol = (String) session.getAttribute("CoronaryLdlc_text");
        if(ldlcMol != null && !ldlcMol.equalsIgnoreCase("")) {
            if(ldlcMol.matches("([0-9]+(\\.[0-9]+)?)+")){
            double ldlcMolValue = Double.parseDouble(ldlcMol);
            coronary.setLdlcMol(ldlcMolValue);
            session.removeAttribute("CoronaryLdlc_text");
            }else {
            
                session.setAttribute("CoronaryLdlc_error", "The LDLC should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("ldlc_error", "The LDLC should be filled in");
            checkError = true;
        }*/
        
        //String hdlcMol = request.getParameter("hdlc");
        String hdlcMol = (String) session.getAttribute("CoronaryHdlc_text");
        if(hdlcMol != null && !hdlcMol.equalsIgnoreCase("")) {
            if(hdlcMol.matches("([0-9]+(\\.[0-9]+)?)+")){
            double hdlcMolValue = Double.parseDouble(hdlcMol);
            coronary.setHdlcMol(hdlcMolValue);
            session.removeAttribute("CoronaryHdlc_text");
            }else {
            
                session.setAttribute("CoronaryHdlc_error", "The HDLC should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("hdlc_error", "The HDLC should be filled in");
            checkError = true;
        }*/
        
        //String tryglyseride = request.getParameter("tryglyseride");
        String tryglyseride = (String) session.getAttribute("CoronaryTryglyseride_text");
        if(tryglyseride != null && !tryglyseride.equalsIgnoreCase("")) {
            if(tryglyseride.matches("([0-9]+(\\.[0-9]+)?)+")){
            double tryglyserideValue = Double.parseDouble(tryglyseride);
            coronary.setTryglyserideMol(tryglyserideValue);
            session.removeAttribute("CoronaryTryglyseride_text");
            }else {
            
                session.setAttribute("CoronaryTryglyseride_error", "The tryglyseride should be a numeric value");
                checkError = true;
            }
        }/*else {

            session.setAttribute("tryglyseride_error", "The tryglyseride should be filled in");
            checkError = true;
        }*/
        
        //String scoreRisks = request.getParameter("scoreRisks");//Score Risks
/*        String scoreRisks = (String) session.getAttribute("CoronaryScoreRisks_text");
        if(scoreRisks != null && !scoreRisks.equalsIgnoreCase("")) {
            if(scoreRisks.matches("([0-9]+(\\.[0-9]+)?)+")){
            double risk = Double.parseDouble(scoreRisks);
            coronary.setScoreRisks(risk);
            }else {
            
                session.setAttribute("CoronaryScoreRisks_error", "The score risks should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("scoreRisks_error", "The score risks should be filled in");
            checkError = true;
        }*/
        
        //String tcHDlRation = request.getParameter("tchdlRatio");
        String tcHDlRation = (String) session.getAttribute("CoronaryTchdlRatio_text");
        if(tcHDlRation != null && !tcHDlRation.equalsIgnoreCase("")) {
            if(tcHDlRation.matches("([0-9]+(\\.[0-9]+)?)+")){
            double tcHDlRationValue = Double.parseDouble(tcHDlRation);
            coronary.setTcHdlRatio(tcHDlRationValue);
            session.removeAttribute("CoronaryTchdlRatio_text");
            }else {
            
                session.setAttribute("CoronaryTchdlRatio_error", "The HDL ratio should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("tchdlRatio_error", "The HDL ratio should be filled in");
            checkError = true;
        }*/
        
        //String detail = request.getParameter("detail");
        String detail = (String) session.getAttribute("coronaryDetail_text");
        if(detail != null && !detail.equalsIgnoreCase("")) {

            coronary.setDetail(detail);
            session.removeAttribute("coronaryDetail_text");
        } /*else {

            session.setAttribute("detail_error", "The detail should be filled in");
            checkError = true;
        }*/
        
        //String treatingProvider = request.getParameter("providerNumber_text");
        String treatingProvider = (String) session.getAttribute("providerNumber_text");
        if(treatingProvider != null && !treatingProvider.equalsIgnoreCase("")) {

            coronary.setTreatingProvider(treatingProvider);
        } /*else {

             session.setAttribute("providerNumber_error", "The treating provider should be filled in");
             checkError = true;
             
        }*/
        
        //String icd10 = request.getParameter("icd10");
        //String icd10 = (String) session.getAttribute("icd10_text");
        String icd10 = (String) session.getAttribute("coronaryLookupValue");
        if(icd10 != null && !icd10.equalsIgnoreCase("99")) {

            coronary.setIcd10(icd10);
             LookupValue icd = new LookupValue();
            icd.setId(icd10);
            icd.setValue(port.getValueFromCodeTableForId("Coronary Biometric ICD", icd10));
            coronary.setIcd10Lookup(icd);
        }else {

            session.setAttribute("icd10_error", "The ICD10 should be filled in");
            checkError = true;
        }
        
        //String source = request.getParameter("source");
        String source = (String) session.getAttribute("source_text");
        if(source != null && !source.equalsIgnoreCase("99")) {

            coronary.setSource(source);
        } /*else {

            session.setAttribute("source_error", "The source should be filled in");
            checkError = true;
        }*/

         SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
         String UpdateBiometrics = (String) session.getAttribute("UpdateBiometrics");
        if (UpdateBiometrics != null && UpdateBiometrics.equals("true")) {
            dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        }
       try {
            //String measuredDate = request.getParameter("dateMeasured");
            String measuredDate =  (String) session.getAttribute("dateMeasured_text");
            if(measuredDate != null && !measuredDate.equalsIgnoreCase("")){
            Date dateMeasured = dateFormat.parse(measuredDate);
            coronary.setDateMeasured(convertDateXML(dateMeasured));
            }else {

                session.setAttribute("dateMeasured_error", "The date measured should be filled in");
                checkError = true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");

        try {

           //String receivedDate = request.getParameter("dateReceived");
            String receivedDate = (String) session.getAttribute("dateReceived_text");
           if(receivedDate != null && !receivedDate.equalsIgnoreCase("")) {
           
               Date dateReceived = sdf2.parse(receivedDate);
               coronary.setDateReceived(convertDateXML(dateReceived));
           }else {
           
               session.setAttribute("dateReceived_error", "The date received should be filled in");
               checkError = true;
           }
        } catch (Exception ex) {

            ex.printStackTrace();
        }



        try {
            if(checkError) {

                saveScreenToSession(request);
                nextJSP = "/biometrics/CaptureBiometrics.jsp";         //CaptureCoronaryDisease.jsp";

            }else {
                //Check if the user is UPDATING or CREATING
                if (UpdateBiometrics != null && UpdateBiometrics.equals("true")) {
                    String oldBioID = (String) session.getAttribute("BiometricID");
                    System.out.println("OLDBIOID = " + oldBioID);
                    
                    int userId = user.getUserId();
                    //clearAllFromSession(request);
                    String hadValueInLip = String.valueOf(session.getAttribute("HadInformationBeforeSwitch"));
                    System.out.println("hadValueInLip = " + hadValueInLip);
                    BiometricsId bId = null;
                    if(hadValueInLip != null && hadValueInLip.equalsIgnoreCase("true"))
                    {  
                        bId = port.saveCoronaryBiometrics(coronary, userId, 1, oldBioID+"|"+hadValueInLip);
                    }
                    else{
                        bId = port.saveCoronaryBiometrics(coronary, userId, 1, oldBioID);
                    }
                    request.setAttribute("revNo",bId.getReferenceNo());
                    int coronaryId = bId.getId();
                    nextJSP = "/biometrics/CaptureBiometrics.jsp";         //CaptureCoronaryDisease.jsp";

                    if(coronaryId > 0){
                        coronary.setBiometricsId(coronaryId);
    //                    port.createTrigger(coronary);
                        session.setAttribute("updated", "Your biometrics data has been captured");
                    }else{
                        session.setAttribute("failed", "Sorry! Coronary Biometrics save has failed, Please try again!");
                        nextJSP = "/biometrics/CaptureBiometrics.jsp";         //CaptureCoronaryDisease.jsp";
                   }
                } else {
                    int userId = user.getUserId();
                    //clearAllFromSession(request);
                    BiometricsId bId = port.saveCoronaryBiometrics(coronary, userId, 1, null);
                    request.setAttribute("revNo",bId.getReferenceNo());
                    int coronaryId = bId.getId();
                    nextJSP = "/biometrics/CaptureBiometrics.jsp";         //CaptureCoronaryDisease.jsp";

                    if(coronaryId > 0){
                        coronary.setBiometricsId(coronaryId);
    //                    port.createTrigger(coronary);
                        session.setAttribute("updated", "Your biometrics data has been captured");
                    }else{
                        session.setAttribute("failed", "Sorry! Coronary Biometrics save has failed, Please try again!");
                        nextJSP = "/biometrics/CaptureBiometrics.jsp";         //CaptureCoronaryDisease.jsp";
                   }
                }
            }
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
           

        }catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    public XMLGregorianCalendar convertDateXML(Date date) {

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getDatatypeFactory().newXMLGregorianCalendar(calendar);

    }

    private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                ex.printStackTrace();
            }
        }

        return (DatatypeFactory) _datatypeFactory;
    }

    @Override
    public String getName() {
        return "CaptureCoronaryDiseaseCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.auth.dto.ICD10SearchResult;
import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Diagnosis;

/**
 *
 * @author josephm
 */
public class LoadICD10BiometricsCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

         System.out.println("Inside LoadICD10BiometricsCommand");

        HttpSession session = request.getSession();
        String icd10 = request.getParameter("code");

        //GenericTariff general = port.fetchTariffs(icd10);
        ArrayList<ICD10SearchResult> icd10s = new ArrayList<ICD10SearchResult>();
        List<Diagnosis> diagList = new ArrayList<Diagnosis>();
        ICD10SearchResult icd = null;//new ICD10SearchResult();

         diagList = service.getNeoManagerBeanPort().findAllDiagnosisForCode(icd10);
         //Need to do this in an ajax command for the ICD description.
         //Diagnosis d = port.getDiagnosisForCode(icd10);


       try {
           PrintWriter out = response.getWriter();


           for(Diagnosis diag: diagList) {
              icd = new ICD10SearchResult();
              icd.setCode(diag.getCode());
              icd.setDescription(diag.getDescription());
              icd10s.add(icd);
           }

           if(icd != null) {

               out.print(getName() + "|ICD10Description=" + icd.getDescription() + "$");

           }else {

               out.print("Error|No such ICD code|" + getName());
           }

       }catch(Exception ex) {
       
           ex.printStackTrace();
       }
        return null;
    }



        /*System.out.println("the tariff description is " + general.getTariffDescription());

    try {

    PrintWriter out = response.getWriter();

    if(general != null) {

    out.print(getName() + "ICDdescription" + general.getTariffDescription());
    }else {

    out.print("Error|No such ICD code" + getName());
    }
    }catch(Exception ex) {

    ex.printStackTrace();
    }
    return null;
    }*/

    @Override
    public String getName() {
        return "LoadICD10BiometricsCommand";
    }
}

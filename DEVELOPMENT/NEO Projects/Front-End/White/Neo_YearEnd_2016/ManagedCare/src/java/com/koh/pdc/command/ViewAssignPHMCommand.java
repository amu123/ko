/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.HistoryList;

/**
 *
 * @author shimanem
 */
public class ViewAssignPHMCommand extends NeoCommand {

    Logger logger = Logger.getLogger(ViewAssignPHMCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
        logger.info(">> Inside ViewAssignPHMCommand <<");

        String workList_ID = "" + request.getParameter("listIndex"); // loads from the actual form

        if (workList_ID != null && !workList_ID.equals("")) {
            logger.log(Level.INFO, ">> PHM WorkListID [param] : {0}", workList_ID);
            session.setAttribute("listIndex", workList_ID);
            int intWorkListId = Integer.parseInt(workList_ID);
            List<HistoryList> retObject = service.getNeoManagerBeanPort().getAssignmentHistory(intWorkListId);
            session.setAttribute("assignHistory", retObject); //session name from tag file
        }

        try {
            String nextJSP = "/PDC/AssignPHM.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }

    @Override
    public String getName() {
        return "ViewAssignPHMCommand";
    }

}

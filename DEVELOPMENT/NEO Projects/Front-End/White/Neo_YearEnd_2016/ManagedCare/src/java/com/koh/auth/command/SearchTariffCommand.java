/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.command;

import com.koh.auth.dto.TariffSearchResult;
import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.Treatment;
import neo.manager.TreatmentSearchCriteria;

/**
 *
 * @author gerritj
 */
public class SearchTariffCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        String code = request.getParameter("code");
        String description = request.getParameter("description");
        TreatmentSearchCriteria search = new TreatmentSearchCriteria();
        if (code != null && !code.equals(""))
            search.setCode(code);
        if (description != null && !description.equals(""))
            search.setDescription(description);

        ArrayList<TariffSearchResult> tariffs = new ArrayList<TariffSearchResult>();
        List<Treatment> treatList = new ArrayList<Treatment>();
        try {
           treatList = service.getNeoManagerBeanPort().findAllTreatmentsByCriteria(search);

           for (Treatment treat : treatList)
           {
             TariffSearchResult tariff = new TariffSearchResult();
             tariff.setCode(treat.getCode());
             tariff.setDescription(treat.getDescription());
             tariff.setDiscipline("" + treat.getPracticeType());
             tariffs.add(tariff);
           }

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
        TariffSearchResult r = new TariffSearchResult();
        r.setCode("10011");
        r.setDescription("Expensive");

        TariffSearchResult r2 = new TariffSearchResult();
        r2.setCode("20022");
        r2.setDescription("Very Expensive");

        tariffs.add(r);
        tariffs.add(r2);
        */

        request.setAttribute("searchTariffResult", tariffs);
        try {
            String nextJSP = "/Auth/TariffSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SearchTariffCommand";
    }
}

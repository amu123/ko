/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.contribution.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author yuganp
 */
public class TransferReceiptsCommand extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            String s = request.getParameter("command");
            System.out.println(this.getName() + " : " + s);
            if (s != null && !s.isEmpty()) {
                System.out.println("command : " + s);
                if (s.equalsIgnoreCase("Search")) {
                    search(request, response, context);
                } else if (s.equalsIgnoreCase("FindMember")) {
                    findMember(request, response, context);
                } else if (s.equalsIgnoreCase("FindEmployer")) {
                    findEmployer(request, response, context);
                } else if (s.equalsIgnoreCase("Save")) {
                    save(request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(ContributionBankStatementCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void save(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Map pMap = request.getParameterMap();
        List<Map<String,String>> aList = new ArrayList<Map<String,String>>();
        String statusPrefix = "URStatusInd_";
        String typePrefix = "UREType_";
        String idPrefix = "UREId_";
        for ( Object p : pMap.keySet()) {
            String key = p.toString();
            if (key != null && key.startsWith(statusPrefix)) {
                String statusInd = request.getParameter(key);
                if (statusInd != null && !statusInd.isEmpty() && (statusInd.equals("A") || statusInd.equals("R"))) {
                    String idParts = key.substring(statusPrefix.length());
                    if (idParts != null && idParts.length() > 0) {
                        String entityId = request.getParameter(idPrefix+idParts);
                        String entityType = request.getParameter(typePrefix+idParts);
                        if (statusInd.equals("R") || (entityId  != null && entityType != null && (entityType.equals("M") || entityType.equals("E")  || entityType.equals("ES") ))) {
                            Map<String,String> map = new HashMap<String, String>();
                            map.put("entityId", entityId);
                            map.put("entityType", entityType);
                            map.put("statusInd", statusInd);
                            map.put("contribId", idParts);
                            map.put("coverNo", request.getParameter("URCover_"+idParts));
                            aList.add(map);
                        } else {
                            System.out.println("Invalid values : part - "  + idParts + " status - " + statusInd + " id - " + entityId + " type - " + entityType);
                        }
                    } else {
                        System.out.println("Invalid parts : " + idParts);
                    }
                } else {
                    System.out.println(key + " is empty or null");
                }
            }
        }
        if (aList.size() > 0) {
            List<neo.manager.KeyValueArray> keyValueArray = MapUtils.getKeyValueArray(aList);
            NeoUser neoUser = (NeoUser)request.getSession().getAttribute("persist_user");

            Security sec = new Security();
            sec.setCreatedBy(neoUser.getUserId());
            sec.setLastUpdatedBy(neoUser.getUserId());
            sec.setSecurityGroupId(2);
            port.transferUnallocatedContributionReceipts(keyValueArray, sec);
            String   status = "OK";
            String   additionalData = "\"message\":\"Receipts have been transferred" +  "\"";
            String result = TabUtils.buildJsonResult(status, null, null, additionalData);
            PrintWriter out = null;
            try {
                out = response.getWriter();
                out.println(result);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            } finally {
                if (out != null) out.close();
            }
        }
        System.out.println("Allocation:");
        for (Map m : aList) {
            for (Object s : m.keySet()) {
                System.out.print(s + "\t" + m.get(s));
            }
            System.out.println();
//            System.out.println("id - " + m.get("entityId") + "\t type - " + m.get("entityType") + "\t status - " + m.get("statusInd"));
        }
    }

    private void search(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String rType = request.getParameter("ReceiptsType");
        if ("M".equalsIgnoreCase(rType)) {
            String memNo = request.getParameter("memNo");
            String idNo = request.getParameter("idNo");
            String initials = request.getParameter("initials");
            String surname = request.getParameter("surname");
            Date dt = TabUtils.getDateParam(request, "dob");
            XMLGregorianCalendar dob = null;
            if (dt != null )
                dob = DateTimeUtils.convertDateToXMLGregorianCalendar(dt);
            List<KeyValueArray> list = port.fetchUnallocatedReceiptsForMember(memNo, idNo, initials, surname, dob);
            List<Map<String, String>> map = MapUtils.getMap(list);
            request.setAttribute("SearchResults", map);
        }
        if ("E".equalsIgnoreCase(rType)) {
            String employerNumber = request.getParameter("employerNumber");
            String employerName = request.getParameter("employerName");
            List<KeyValueArray> list = port.fetchUnallocatedReceiptsForEmployer(employerNumber, employerName);
            List<Map<String, String>> map = MapUtils.getMap(list);
            request.setAttribute("SearchResults", map);
        }
        context.getRequestDispatcher("/Contribution/TransferUnallocatedReceiptsResults.jsp").forward(request, response);
    }
    
    private void findEmployer(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String number = request.getParameter("number");
        KeyValueArray group = port.findGroupByGroupNumber(number);
        setUnallocatedSearchResults(request, response, context, group, "E", "");
    }
    
    private void findMember(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String number = request.getParameter("number");
        KeyValueArray member = port.findMemberByCoverNumber(number);
        setUnallocatedSearchResults(request, response, context, member, "M", number);
    }
    
    private void setUnallocatedSearchResults(HttpServletRequest request, HttpServletResponse response, ServletContext context, KeyValueArray kva, String type, String coverNumber) {
        String result = "";
        String sId = request.getParameter("contribId");
        String entityId = "";
        String entityName = "";
        if (kva != null) {
            Map<String, String> map = MapUtils.getMap(kva);
            entityId = map.get("entityId") == null ? "" : map.get("entityId");
            entityName = map.get("entityName") == null ? "" : map.get("entityName");
        }
        Map<String, String> fieldsMap = new HashMap<String, String>();
        fieldsMap.put("URELbl_" + sId, entityName);
        fieldsMap.put("UREId_" + sId, entityId);
        fieldsMap.put("UREType_" + sId, type);
        fieldsMap.put("URStatusInd_" + sId, entityId.isEmpty() ? "" : "A");
        fieldsMap.put("URCover_" + sId, coverNumber);
        String updateFields = TabUtils.convertMapToJSON(fieldsMap);
        result = TabUtils.buildJsonResult("OK", null, updateFields, null);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println(result);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (out != null) out.close();
        }
    }
    
    @Override
    public String getName() {
        return "TransferReceiptsCommand";
    }
    
}

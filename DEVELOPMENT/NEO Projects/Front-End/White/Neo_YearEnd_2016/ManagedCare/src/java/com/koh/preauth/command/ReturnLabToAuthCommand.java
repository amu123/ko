/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.LabTariffDetails;

/**
 *
 * @author johanl
 */
public class ReturnLabToAuthCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession ses = request.getSession();
        boolean errorCheck = false;
        String selectedLabs = request.getParameter("selectedLabValues");
        ArrayList<LabTariffDetails> newLabList = new ArrayList<LabTariffDetails>();

        selectedLabs = selectedLabs.replace("|undefined%", "");
        System.out.println("SELECTED LABS = " + selectedLabs);
        if (selectedLabs != null && !selectedLabs.equalsIgnoreCase("")) {

            String[] selectionCount = selectedLabs.split("%");



            for (int x = 0; x < selectionCount.length; x++) {

                String[] labDetails = selectionCount[x].split("\\|");

                //System.out.println("Lab0 =" + x + " " + labDetails[0]);
                //System.out.println("Lab1 =" + x + " " + labDetails[1]);
                //System.out.println("Lab2 =" + x + " " + labDetails[2]);
                //System.out.println("Lab3 =" + x + " " + labDetails[3]);
//            for(int i = 0; i < labDetails.length; i++){
                LabTariffDetails lab = new LabTariffDetails();

                lab.setClinicalTariff(labDetails[0]);
                lab.setLabCode(Integer.parseInt(labDetails[1]));
                //lab.setTCode(labDetails[2]);
                if (labDetails.length == 2) {
                    errorCheck = true;
                    ses.setAttribute("doubError_error", "Error: Zero amounts are not allowed, please enter appropriate amount");                   
                } else {
                    if (labDetails[2].equalsIgnoreCase("9999999.99")) {
                        errorCheck = true;
                        ses.setAttribute("doubError_error", "Error: 9999999.99 amounts are not allowed, please enter appropriate amount");                      
                    } else {                       
                        lab.setLabCodeAmount(Double.parseDouble(labDetails[2]));
                    }
                }

                /*
                 * if(labDetails[2].contains(".")){ String quan = labDetails[2];
                 * quan = quan.substring(0, quan.indexOf(".")); labDetails[2] =
                 * quan; }
                 */

                //lab.setLabCodeQuantity(Integer.parseInt(labDetails[2]));



                lab.setProviderType("054");

                lab.setLabCodeInd("1");

                newLabList.add(lab);
                //}
            }
        }
        ses.setAttribute("SavedDentalLabCodeList", newLabList);

        String selectedNoLabs = request.getParameter("selectedNoLabValues");
        ArrayList<LabTariffDetails> newNoLabList = new ArrayList<LabTariffDetails>();

        System.out.println("SELECTED NOLABS = " + selectedNoLabs);
        if (selectedNoLabs != null && !selectedNoLabs.equalsIgnoreCase("")) {


            String[] noLabSelectionCount = selectedNoLabs.split("%");

            for (int x = 0; x < noLabSelectionCount.length; x++) {
                String[] noLabDetails = noLabSelectionCount[x].split("\\|");

                LabTariffDetails noLab = new LabTariffDetails();

                noLab.setClinicalTariff(noLabDetails[0]);
                
                if(noLabDetails.length == 2){
                   noLab.setToothNumber(noLabDetails[1]); 
                }

                noLab.setLabCodeAmount(0.0d);

                noLab.setProviderType("054");

                noLab.setLabCodeInd("0");

                newNoLabList.add(noLab);
            }
        }
        ses.setAttribute("SavedDentalNoLabCodeList", newNoLabList);
        try {
            String nextJSP = "" + ses.getAttribute("onScreen");
            if (errorCheck == true) {
                nextJSP = "/PreAuth/LabSpecificTariff.jsp";
            }else{
                ses.setAttribute("doubError_error", "");
            }
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ReturnLabToAuthCommand";
    }
}

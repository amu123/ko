/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Johan-NB
 */
public class RemoveAllocatedBenefit extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;
        
        String errorResponse = "";
        if(session.getAttribute("benAllocated") != null){
            session.removeAttribute("benAllocated");
            errorResponse = "Done|";
        }else{
            errorResponse = "No|";
        }
        
        try{
            out = response.getWriter();
            out.println(errorResponse);
        }catch(IOException io){
            io.printStackTrace();
        }
        
        return null;
    }

    @Override
    public String getName() {
        return "RemoveAllocatedBenefit";
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.Diagnosis;

/**
 *
 * @author whauger
 */

public class GetICD10ByCodeCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String diagnosis = request.getParameter("code");
        Diagnosis d = service.getNeoManagerBeanPort().getDiagnosisForCode(diagnosis);
        try{
            PrintWriter out = response.getWriter();
            if(d != null){
                out.print(getName() + "|Code="+d.getCode()+"|Description="+d.getDescription()+"$");
            }else{
                out.print("Error|No such diagnosis|" + getName());
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetICD10ByCodeCommand";
    }

}

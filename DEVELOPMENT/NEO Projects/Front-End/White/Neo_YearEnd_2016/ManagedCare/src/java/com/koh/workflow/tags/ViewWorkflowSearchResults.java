/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.workflow.tags;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.NeoManagerBean;
import neo.manager.WorkflowItem;

/**
 *
 * @author janf
 */
public class ViewWorkflowSearchResults extends TagSupport {

    private String sessionAttribute;

    public String getSessionAttribute() {
        return sessionAttribute;
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    //PHASING THIS PAGE OUT PLEASE CHECK WorkflowSearchResults.jsp for updates
    @Override
    public int doEndTag() throws JspException {
        HttpSession session = pageContext.getSession();
        ServletRequest request = pageContext.getRequest();
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String workflowSearch = request.getParameter("workflowSearch");
        System.out.println("workflowSearch = " + workflowSearch);
        List<WorkflowItem> list = null;
        try {
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.print("<tr>");
            out.print("<th align=\"left\">Reference Number</th>");
            out.print("<th align=\"left\">Entity Number</th>");
            out.print("<th align=\"left\">Entity Name</th>");
            out.print("<th align=\"left\">Workflow Queue</th>");
            out.print("<th align=\"left\">Assigned Queue</th>");
            out.print("<th align=\"left\">Workflow Status</th>");
            out.print("<th align=\"left\">Subject</th>");
            out.print("<th align=\"left\">Start Date</th>");
            out.print("<th align=\"left\">Last Updated Date</th>");
            out.print("<th align=\"left\">Action</th>");
            out.print("</tr>");

            //get the object from the session and cast it to the object made in the backend
            list = (List<WorkflowItem>) request.getAttribute("viewWorkflowSearchResults");
            
            if (list != null) {
                for (WorkflowItem l : list) {
                    out.print("<tr>");
                    out.print("<td><label class=\"label\">" + l.getRefNum() + "</label></td>");
                    out.print("<td><label class=\"label\">" + l.getEntityNumber() + "</label></td>");
                    out.print("<td><label class=\"label\">" + l.getEntityName() + "</label></td>");
                    out.print("<td><label class=\"label\">" + l.getQueueDesc() + "</label></td>");
                    out.print("<td><label class=\"label\">" + l.getUserID() + "</label></td>");
                    out.print("<td><label class=\"label\">" + l.getStatusName() + "</label></td>");
                    out.print("<td><label class=\"label\">" + l.getSubject() + "</label></td>");
                    out.print("<td><label class=\"label\">" + DateTimeUtils.convertXMLGregorianCalendarToDate(l.getCreationDate()) + "</label></td>");
                    out.print("<td><label class=\"label\">" + DateTimeUtils.convertXMLGregorianCalendarToDate(l.getLastUpdateDate())   + "</label></td>");
//                    out.print("<td><button name=\"opperation\" type=\"button\" onClick=\"document.getElementById('onScreen').value = 'Workflow/WorkflowOptPnlContent.jsp'; viewSelectedWorkflowItem('WorkflowPopulateOptionPanelCommand','" + l.getItemID() + "')\"; value=\"\">Details</button></td>");
                    if(workflowSearch == null){
                        out.print("<td><button name=\"opperation\" type=\"button\" onClick=\"viewSelectedWorkflowItem('ViewWorkbenchDetails','" + l.getItemID() + "')\"; value=\"\">Details</button></td>");
                    } else {
                        out.print("<td><button name=\"opperation\" type=\"button\" onClick=\"\"; value=\"\">TEST</button></td>");
                    }
                    out.print("</tr>");
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ViewWorkflowSearchResults.class.getName()).log(Level.SEVERE, null, ex);
        }

        return super.doEndTag();
    }
}

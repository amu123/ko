/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.lang.Exception;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class ClaimLineCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        String command = request.getParameter("command");
        System.out.println("Operation: ClaimLineCommand - Command: " + command);
        if ("NewClaimLine".equalsIgnoreCase(command)) {
            try {
                CoverDetails cdUnkown = new CoverDetails();
                cdUnkown.setDependentNumber(99);
                cdUnkown.setEntityId(0);
                cdUnkown.setName("Unknown");
                List<CoverDetails> covList = port.getDependantsForPrincipleMemberByDate(request.getParameter("coverNumber"));
//                covList.add(cdUnkown);
                List<ClaimLine> cls = port.fetchClaimLineForClaim(TabUtils.getIntParam(request, "claimId"));
                request.setAttribute("ClaimLines", cls);
                request.setAttribute("claimId", request.getParameter("claimId"));
                request.setAttribute("ClaimLine_Category", request.getParameter("category"));
                request.setAttribute("coverNumber", request.getParameter("coverNumber"));
                request.setAttribute("covList", covList);
                request.setAttribute("ClaimLine_Provider_No", request.getParameter("providerNumber"));
                request.setAttribute("ClaimLine_Practice_No", request.getParameter("practiceNumber"));
                String update = request.getParameter("update");
                System.out.println("Update = " + update);
                if(update != null && update.equalsIgnoreCase("true")) {
                    RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/EditClaims_ClaimLine.jsp");
                    dispatcher.forward(request, response);
                } else {
                    RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/CaptureClaims_ClaimLine.jsp");
                    dispatcher.forward(request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if ("SaveClaimLine".equalsIgnoreCase(command)) {
            try {
                ClaimLine cl = saveClaimLine(request, port, session);
                System.out.println("Claim Line: " + cl.getClaimLineId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("fetchClaimLines".equalsIgnoreCase(command)) {
            List<ClaimLine> cls = port.fetchClaimLineForClaim(TabUtils.getIntParam(request, "claimId"));

            request.setAttribute("ClaimLines", cls);
            request.setAttribute("claimId", request.getParameter("claimId"));
            try {
                String update = request.getParameter("update");
                System.out.println("Update = " + update);
                if(update != null && update.equalsIgnoreCase("true")){
                    RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/EditClaims_ClaimLineSummary.jsp");
                    dispatcher.forward(request, response);
                } else {
                    RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/CaptureClaims_ClaimLineSummary.jsp");
                    dispatcher.forward(request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("findICD".equalsIgnoreCase(command)) {
            try {
                String icd = request.getParameter("icd");
                System.out.println("ICD = " + icd);
                String result = "";
//                if(icd.contains(",")){
//                    icd = icd.replace(",", " ");
//                }
//                String[] icdArr = icd.split(" ");
//                String result = "";
//                for (String s : icdArr) {
//                    if (!result.isEmpty()) {
//                        result = result.concat("<br>");
//                    }
//                    Diagnosis diagnosis = port.getDiagnosisForCode(s);
//                    if (diagnosis == null) {
//                        result = result.concat("<label class='error'>" + s + " - Invalid ICD</label>");
//                    } else {
//                        result = result.concat("<label>" + diagnosis.getCode() + " - " + diagnosis.getDescription() + "</label>");
//                        result = diagnosis.getCode() + " - " + diagnosis.getDescription();
//                    }
//                }
                Diagnosis diagnosis = port.getDiagnosisForCode(icd);
                if(diagnosis != null){
                    result = diagnosis.getCode() + " - " + diagnosis.getDescription();
                } else {
                    //result = "<label class='error'>" + icd + " - Invalid ICD</label>";
                    result = icd + " - Invalid ICD";
                }
                
                System.out.println("result = " + result);
                PrintWriter out = response.getWriter();
                out.print(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("findTariff".equalsIgnoreCase(command)) {
            try {
                String tariff = request.getParameter("tariff");
                String practiceTypeId = request.getParameter("practiceTypeId");
                String practiceNo = request.getParameter("practiceNum");
                String providerNo = request.getParameter("providerNum");
                Date serviceDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("serviceDate"));
                XMLGregorianCalendar xserviceD = DateTimeUtils.convertDateToXMLGregorianCalendar(serviceDate);
                String coverNum = request.getParameter("coverNum");
                System.out.println("coverNum = " + coverNum);
                System.out.println("xserviceD = " + xserviceD);
                System.out.println("serviceDate = " + serviceDate);
                String result = "";
                System.out.println("[fr]tarrif = " + tariff + "\nPracticeTypeId = " + practiceTypeId);
                System.out.println("practiceNo = " + practiceNo + "\nproviderNo = " + providerNo);
                
                CoverProductDetails cdp = port.getCoverProductDetailsByCoverNumber(coverNum);
                System.out.println("[fr]cdp = " + cdp.getCoverNumber() + " - " + cdp.getProductName() + cdp.getProductId() + " - " + cdp.getOptionName() + cdp.getOptionId());
                if(cdp != null){
                    if(tariff.equalsIgnoreCase("GMED")) {
                        result = "MEDICINE";
                    } else {
                        /*if(tariff.contains(practiceTypeId.substring(1))){
                            System.out.println("true");
                            tariff = tariff.substring(2);
                            System.out.println("[fr]tarif = " + tariff);
                        }*/
                        //List<TariffCode> tariffCodeList = port.getTariffCodeByCriteria(cdp.getProductId(), cdp.getOptionId(), practiceTypeId, tariff, null,xserviceD /*DateTimeUtils.convertDateToXMLGregorianCalendar(new Date())*/);
                        TariffCode tariffs = port.getTariffCode(cdp.getProductId(),cdp.getOptionId(),xserviceD,practiceTypeId,practiceNo,providerNo,null,null,tariff);
                        if(tariffs != null){
                            System.out.println("[fr]tariffs = " + tariffs.getCode() + tariffs.getDescription());
                            result = tariffs.getDescription();
                        }
                        /*for (TariffCode tc : tariffCodeList) {
                            result = tc.getDescription();
                            break;
                        }*/
                    }
                }
                
                PrintWriter out = response.getWriter();
                out.print(result);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if ("findCPT".equalsIgnoreCase(command)) {
            try {
                String cpt = request.getParameter("cpt");
                //String practiveTyoeId = request.getParameter("practiceTyoeId");
                String result = "";
                System.out.println("cpt = " + cpt);

                Procedure cptCode = port.getProcedureForCode(cpt);
                if (cptCode != null) {
                    result = cptCode.getCode() +  " - " + cptCode.getDescription();
                } else {
                    //result = "<label class='error'>" + cpt + " - Invalid CPT</label>";
                    result = cpt + " - Invalid CPT";
                }
                System.out.println("result = " + result);
                PrintWriter out = response.getWriter();
                out.print(result);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if ("ValidateTime".equalsIgnoreCase(command)) {
            try {
                Calendar cTime = Calendar.getInstance();
                String time = request.getParameter("time");
                System.out.println("Time " + time);
                boolean valid = false;
                boolean isPM = false;
                int min = 0;
                boolean is24Hour = true;
                System.out.println("Time Lenght = " + time.length());
                
                if(time.contains(".")){
                    time = time.replace(".", ":");
                }
                
                if(time.toUpperCase().contains("H")){
                    time = time.toUpperCase().replace("H", ":");
                }
                
                if(time.length() == 4){
                    if(!time.contains(":")){
                        time = time.substring(0,2) + ":" + time.substring(2,4);
                    }
                }
                
                if(time.length() == 3){
                    if(!time.contains(":")){
                        time = "0" + time.substring(0,1) + ":" + time.substring(1,3);
                    }
                }
                System.out.println("Time = " + time);

                String result = "";
                if (time.contains(":")) {
                    String[] timeArr = time.split(":");
                    String[] AMPM = null;
                    if(timeArr.length >= 2){
                    //check for AMPM
                        if (timeArr[1].toUpperCase().contains("AM") || timeArr[1].toUpperCase().contains("PM")) {
                            if (!timeArr[1].contains(" ")) {
                                timeArr[1] = timeArr[1].substring(0, 2) + " " + timeArr[1].substring(timeArr[1].toUpperCase().indexOf("M") - 1, timeArr[1].toUpperCase().indexOf("M") + 1);
                            }

                            AMPM = timeArr[1].split(" ");

                            if (AMPM[0].length() == 2) {
                                valid = true;
                                if (AMPM[0].matches("-?\\d+(\\.\\d+)?")) {
                                    min = Integer.parseInt(AMPM[0]);
                                } else {
                                    valid = false;
                                }
                            } else {
                                valid = false;
                            }

                            is24Hour = !AMPM[1].equalsIgnoreCase("PM") && !AMPM[1].equalsIgnoreCase("AM");
                            //sSystem.out.println("is24hour = " + is24Hour + "\nAMPM[1] = " + AMPM[1]);
                        } else {
                            valid = true;
                            if (timeArr[1].matches("-?\\d+(\\.\\d+)?")) {
                                min = Integer.parseInt(timeArr[1]);
                            } else {
                                valid = false;
                            }
                        }
                        //test hour validity
                        if (timeArr[0].length() == 2 && timeArr[0].matches("-?\\d+(\\.\\d+)?") && valid) {
                            DateFormat formatter = new SimpleDateFormat("hh:mm a");
                            //try {
                            int hours = Integer.parseInt(timeArr[0]);

                            valid = false;
                            if (hours >= 0 && hours < 24) {
                                if (hours > 12) {
                                    hours = hours - 12;
                                    isPM = true;
                                }
                                if (hours == 12) {
                                    isPM = true;
                                }
                                if (hours == 0) {
                                    hours = 12;
                                    isPM = false;
                                }

                                valid = true;

                            } else {
                                result = "error|Hours Out of bounds";
                                valid = false;
                            }
                            //test Min validity
                            if (min >= 0 && min < 60 && valid) {
                                valid = true;
                            } else {
                                result = "error|Minuts Out of bounds";
                                valid = false;
                            }

                            if (valid) {
                                String newTime = null;
                                String sHours = "" + hours;
                                String sMin = "" + min;

                                if (sHours.length() == 1) {
                                    sHours = "0" + hours;
                                }

                                if (sMin.length() == 1) {
                                    sMin = "0" + sMin;
                                }

                                System.out.println("isPM = " + isPM);
                                System.out.println("is24Hour = " + is24Hour);
                                if (is24Hour) {
                                    if (isPM) {
                                        newTime = sHours + ":" + sMin + " PM";
                                    } else {
                                        newTime = sHours + ":" + sMin + " AM";
                                    }
                                } else {
                                    newTime = sHours + ":" + sMin + " " + AMPM[1].toUpperCase();
                                }

                                Date datetimeTime = (Date) formatter.parse(newTime);
                                result = "done|" + newTime;
                            } else {
                                result = "error|Incorrect Time";
                            }
                            //} catch (Exception e) {
                            //    e.printStackTrace();
                            //    result = "error|Invalide Time format";
                            //}
                        } else {
                            result = "error|Invalide Time Format";
                        }
                    }else{
                        result = "error|Invalide Time Format";
                    }
                } else {
                    result = "error|Invalide Time Format";
                }
                //try {
                PrintWriter out = response.getWriter();
                System.out.println("Result = " + result);
                out.print(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if("clearSessionLists".equalsIgnoreCase(command)) {
            System.out.println("capOrEdit = " + request.getParameter("capOrEdit"));
            System.out.println("capOrEditStartLoad = " + request.getParameter("capOrEditStartLoad"));
            if(request.getParameter("capOrEditStartLoad").equalsIgnoreCase("true")){
                System.out.println("ModList = " + session.getAttribute("modList"));
                System.out.println("NdcLine = " + session.getAttribute("NdcLine"));
                System.out.println("claimLineNdcList = " + session.getAttribute("claimLineNdcList"));
            } else {
                System.out.println("Session Cleared");
                session.setAttribute("claimLineNdcList",null);
                session.setAttribute("NdcLine",null);
                session.setAttribute("modList",null);
            }
        }
        return null;
    }

    private ClaimLine saveClaimLine(HttpServletRequest request, NeoManagerBean port, HttpSession session) {
        ClaimLine cl = new ClaimLine();
        cl.setServiceDateFrom(TabUtils.getXMLDateParam(request, "ClaimLine_DateStart"));
        cl.setServiceDateTo(TabUtils.getXMLDateParam(request, "ClaimLine_DateEnd"));
        String dep = request.getParameter("ClaimLine_Dependant");
        String[] deps = dep.split("_");
        cl.setInsuredPersonEntityId(new Integer(deps[0])); //todo
        cl.setInsuredPersonDependentCode(new Integer(deps[1]));  //todo
        cl.setTimeIn(request.getParameter("ClaimLine_Time_In"));
        cl.setTimeOut(request.getParameter("ClaimLine_Time_Out"));
        cl.setTarrifCodeNo(request.getParameter("ClaimLine_Tariff").toUpperCase());
        cl.setMultiplier(TabUtils.getIntParam(request, "ClaimLine_Multiplier"));
        cl.setUnits(TabUtils.getBDParam(request, "ClaimLine_Units").doubleValue());
        cl.setQuantity(TabUtils.getIntParam(request, "ClaimLine_Quantity"));
        cl.setLabInvoiceNumber(request.getParameter("ClaimLine_Invoice_Number"));
        cl.setLabOrderNumber(request.getParameter("ClaimLine_Order_Number"));
        cl.setClaimedAmount(TabUtils.getBDParam(request, "ClaimLine_Claimed_Amount").doubleValue());
        cl.setClaimLineStatusId(1);
        cl.setChequeRunID(0);
        cl.setChequeRunDate(null);
        cl.setProsessDate(null);
        Collection<String> authNumbers = cl.getAuthNumber();
        String authNumber = request.getParameter("ClaimLine_Authorization");
        if (authNumber != null && !"".equalsIgnoreCase(authNumber)) {
            authNumbers.add(authNumber);
            //cl.getAuthNumber().add(authNumber);
        }
        Collection<Integer> toothNumbers = cl.getToothNumber();
        //String toothNumber = request.getParameter("ClaimLine_Tooth_Numbers");
        String toothList = request.getParameter("toothList");
        System.out.println("ToothList = " + toothList);
        if (toothList != null && !"".equals(toothList)) {
            if(toothList.contains(",")){
                toothList = toothList.replace(",", " ");
            }
            String[] sArr = toothList.split(" ");
            for (String s : sArr) {
                toothNumbers.add(new Integer(s));
                //cl.getToothNumber().add(new Integer(s));
            }
        }
        Collection<ClaimCptCodes> cptCodes = cl.getCptCodes();
        //String cptCode = request.getParameter("ClaimLine_CPT");
        String cptList = request.getParameter("cptList");
        System.out.println("cptList = " + cptList);
        if (cptList != null && !"".equals(cptList)) {
            if(cptList.contains(",")){
                    cptList = cptList.replace(",", " ");
                }
            String[] sArr = cptList.split(" ");
            for (String s : sArr) {
                ClaimCptCodes ccptc = new ClaimCptCodes();
                ccptc.setCptCode(s);
                cptCodes.add(ccptc);
                //cl.getCptCodes().add(ccptc);
            }
        }
        Collection<ClaimIcd10> icd10 = cl.getIcd10();
        //String icdCode = request.getParameter("ClaimLine_Diagnosis");
        String icdList = request.getParameter("icdList");
        System.out.println("icdList = " + icdList);
        if (icdList != null && !"".equals(icdList)) {
            if(icdList.contains(",")){
                    icdList = icdList.replace(",", " ");
                }
            String[] sArr = icdList.split(" ");
            for (String s : sArr) {
                ClaimIcd10 cicd = new ClaimIcd10();
                cicd.setIcd10Code(s.toUpperCase());
                cicd.setReferingIcd10(0);
                icd10.add(cicd);
                //cl.getIcd10().add(cicd);
            }
        }
//        icdCode = request.getParameter("ClaimLine_Ref_Diagnosis");
        icdList = request.getParameter("icdRefList");
        System.out.println("icdRefList = " + icdList);
        if (icdList != null && !"".equals(icdList)) {
            if(icdList.contains(",")){
                    icdList = icdList.replace(",", " ");
                }
            String[] sArr = icdList.split(" ");
            for (String s : sArr) {
                ClaimIcd10 cicd = new ClaimIcd10();
                cicd.setIcd10Code(s.toUpperCase());
                cicd.setReferingIcd10(1);
                icd10.add(cicd);
                //cl.getIcd10().add(cicd);
            }
        }
        Collection<Eye> eyes = cl.getEyes();
        String lensLeft = request.getParameter("ClaimLine_Lens_Left");
        if (lensLeft != null && !"".equalsIgnoreCase(lensLeft)) {
            Eye eye = new Eye();
            eye.setEyeLR("L");
            eye.setLensesRX(lensLeft);
            eyes.add(eye);
            //cl.getEyes().add(eye);
        }
        String lensRight = request.getParameter("ClaimLine_Lens_Left");
        if (lensRight != null && !"".equalsIgnoreCase(lensRight)) {
            Eye eye = new Eye();
            eye.setEyeLR("R");
            eye.setLensesRX(lensRight);
            eyes.add(eye);
            //cl.getEyes().add(eye);
        }
        
        Collection<Ndc> ndcs = cl.getNdc();
        List<Ndc> clNdcList = (List<Ndc>) session.getAttribute("claimLineNdcList");

        if(clNdcList != null && !clNdcList.isEmpty()){
            System.out.println("clNdcList size = "  + clNdcList.size());
            for(Ndc nc : clNdcList){
                ndcs.add(nc);
                //cl.getNdc().add(nc);
            }
        }
        Collection<Modifier> modifier = cl.getModifiers();
        List<Modifier> modList = (List<Modifier>) session.getAttribute("modList");
        
        if(modList != null && !modList.isEmpty()){
            System.out.println("modList size = " + modList.size());
            for(Modifier mod : modList){
                modifier.add(mod);
            }
        }
        
        session.setAttribute("claimLineNdcList",null);
        session.setAttribute("NdcLine",null);
        session.setAttribute("modList",null);

        Security security = TabUtils.getSecurity(request);
        int claimID = TabUtils.getIntParam(request, "ClaimLine_claimId");
        System.out.println("ClaimID = " + claimID);
        cl.setClaimLineId(port.saveClaimLine(cl, claimID, security, true));
        return cl;
    }

    @Override
    public String getName() {
        return "ClaimLineCommand";
    }

}

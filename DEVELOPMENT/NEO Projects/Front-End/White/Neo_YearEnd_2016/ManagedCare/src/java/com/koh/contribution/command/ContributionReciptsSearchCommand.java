/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.contribution.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.ContributionReceipts;
import neo.manager.CoverDetails;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author yuganp
 */
public class ContributionReciptsSearchCommand extends NeoCommand{


    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            String s = request.getParameter("command");
            if (s != null && !s.isEmpty()) {
                System.out.println("command : " + s);
                if (s.equalsIgnoreCase("Search")) {
                    search(request, response, context);
                } else if (s.equalsIgnoreCase("SelectMember")) {
                    selectMember(request, response, context);
                } else if (s.equalsIgnoreCase("SelectEmployer")) {
                    selectEmployer(request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(ContributionBankStatementCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void search(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String rType = request.getParameter("ReceiptsType");
        if ("M".equalsIgnoreCase(rType)) {
            String memNo = request.getParameter("memNo");
            String idNo = request.getParameter("idNo");
            String initials = request.getParameter("initials");
            String surname = request.getParameter("surname");
            Date dt = TabUtils.getDateParam(request, "dob");
            XMLGregorianCalendar dob = null;
            if (dt != null )
                dob = DateTimeUtils.convertDateToXMLGregorianCalendar(dt);
            List<KeyValueArray> list = port.fetchUnallocatedReceiptsSummaryForMember(memNo, idNo, initials, surname, dob);
            List<Map<String, String>> map = MapUtils.getMap(list);
            request.setAttribute("MemberSearchResults", map);
        }
        if ("E".equalsIgnoreCase(rType)) {
            String employerNumber = request.getParameter("employerNumber");
            String employerName = request.getParameter("employerName");
            List<KeyValueArray> list = port.fetchUnallocatedReceiptsSummaryForEmployer(employerNumber, employerName);
            List<Map<String, String>> map = MapUtils.getMap(list);
            request.setAttribute("EmployerSearchResults", map);
        }
        context.getRequestDispatcher("/Contribution/UnallocatedReceiptsListResults.jsp").forward(request, response);
    }
    
    private void selectMember(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String coverNo = request.getParameter("coverNo");
        List<ContributionReceipts> unallocatedReceiptsByEntityId = port.fetchUnallocatedReceiptsByEntityId(TabUtils.getIntParam(request, "entityId"), null);
        List<KeyValueArray> unallocatedContributionsForMember = port.fetchUnallocatedContributionsForMember(coverNo);
        List<Map<String, String>> map = MapUtils.getMap(unallocatedContributionsForMember);
        request.setAttribute("memberContribs", map);
        request.setAttribute("receipts", unallocatedReceiptsByEntityId);
        request.setAttribute("amount", request.getParameter("amount"));
        request.setAttribute("coverNo", request.getParameter("coverNo"));
        request.setAttribute("entityId", request.getParameter("entityId"));
        request.setAttribute("memberName", request.getParameter("memberName"));
        request.setAttribute("memberSurname", request.getParameter("memberSurname"));
        List<CoverDetails> cd= port.getCoverDetailsByCoverNumber(coverNo);
        String status = "";
        for(CoverDetails cd1 : cd){
            if(cd1.getDependentTypeId() == 17){
              status = cd1.getStatus();  
            }
        }
        request.setAttribute("status", status);        
        context.getRequestDispatcher("/Contribution/MemberPaymentAllocation.jsp").forward(request, response);
    }

    private void selectEmployer(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        System.out.println("Getting employer receipt allocation");
        int intParam = TabUtils.getIntParam(request, "entityId");
        long start = System.currentTimeMillis();
        List<ContributionReceipts> unallocatedReceiptsByEntityId = port.fetchUnallocatedReceiptsByEntityId(intParam, null);
        long end = System.currentTimeMillis();
        System.out.println("Receipts took : " + (end - start));
        XMLGregorianCalendar xmlDateParam = TabUtils.getXMLDateParam(request, "contribPeriod");
        List<KeyValueArray> unallocatedContributionsForEmployer = port.fetchUnallocatedContributionsForEmployer(intParam, xmlDateParam);
        start = System.currentTimeMillis();
        System.out.println("Contribs took : " + (start - end));
        List<Map<String, String>> map = MapUtils.getMap(unallocatedContributionsForEmployer);
        end = System.currentTimeMillis();
        System.out.println("Mapping took : " + (end - start));
        request.setAttribute("employerContribs", map);
        request.setAttribute("receipts", unallocatedReceiptsByEntityId);
        request.setAttribute("amount", request.getParameter("amount"));
        request.setAttribute("groupName", request.getParameter("groupName"));
        request.setAttribute("groupNumber", request.getParameter("groupNumber"));
        request.setAttribute("entityId", request.getParameter("entityId"));
        context.getRequestDispatcher("/Contribution/EmployerPaymentAllocation.jsp").forward(request, response);
    }

    @Override
    public String getName() {
        return "ContributionReciptsSearchCommand";
    }
    
}

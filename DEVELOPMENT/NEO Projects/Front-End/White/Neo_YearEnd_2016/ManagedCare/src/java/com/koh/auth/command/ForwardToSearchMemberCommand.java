/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.auth.command;

import com.koh.command.Command;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author gerritj
 */
public class ForwardToSearchMemberCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        session.setAttribute("refreshed", true);
        try {
            String nextJSP = "/Auth/MemberSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ForwardToSearchMemberCommand";
    }

    /*
    private void saveScreenToSession(HttpServletRequest request) {
       HttpSession session = request.getSession();
        Enumeration<String> e = request.getParameterNames();
        while (e.hasMoreElements()) {
            String param = e.nextElement();
            if (param.length() > 4) {
                String name = param.substring(param.length() - 4, param.length());
                if (!name.equalsIgnoreCase("list")) {
                    session.setAttribute(param, request.getParameter(param));
                }
            } else {
                session.setAttribute(param, request.getParameter(param));
            }

        }
    }*/
}

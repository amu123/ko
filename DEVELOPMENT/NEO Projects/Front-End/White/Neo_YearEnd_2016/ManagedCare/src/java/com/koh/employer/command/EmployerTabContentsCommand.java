/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.employer.command;

import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import agility.za.indexdocumenttype.IndexForMember;
import com.koh.command.NeoCommand;
import com.koh.employer.*;
import com.koh.serv.PropertiesReader;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import com.koh.utils.Parameters;
import java.io.PrintWriter;
import java.lang.Exception;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;
import org.apache.log4j.Logger;

/**
 *
 * @author yuganp
 */
public class EmployerTabContentsCommand extends NeoCommand {

    private static final String JSP_FOLDER = "/Employer/";
    private Logger logger = Logger.getLogger(EmployerTabContentsCommand.class);
    static final int PAGE_SIZE = 10;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        String tab = request.getParameter("tab");
        System.out.println("Button pressed " + tab);
        String entityId = request.getParameter("employerEntityId");
        request.setAttribute("EmployerGroup_entityId", entityId);
        String page = "";
        if ("EmployerDetails".equalsIgnoreCase(tab)) {
            page = getEmployerDetails(port, request);
        } else if ("EmployerEligibility".equalsIgnoreCase(tab)) {
            page = getEmployerEligibility(port, request, context);
        } else if ("EmployerBilling".equalsIgnoreCase(tab)) {
            page = getEmployerBilling(port, request);
        } else if ("EmployerCommunication".equalsIgnoreCase(tab)) {
            page = getEmployerCommunication(port, request);
        } else if ("EmployerIntermediary".equalsIgnoreCase(tab)) {
            page = getEmployerIntermediary(port, request);
        } else if ("EmployerUnderwriting".equalsIgnoreCase(tab)) {
            page = getEmployerUnderwriting(port, request);
        } else if ("EmployerBranches".equalsIgnoreCase(tab)) {
            page = getEmployerBranches(port, request);
        } else if ("EmployerAudit".equalsIgnoreCase(tab)) {
            page = getEmployerAudit(port, request);
        } else if ("Notes".equalsIgnoreCase(tab)) {
            page = getEmployeeAppNotes(port, request);
        } else if ("Members".equalsIgnoreCase(tab)) {
            page = getMembers(port, request);
        } else if ("Documents".equalsIgnoreCase(tab)) {
            page = getDocuments(port, request);
        } else if ("EmployerStatements".equalsIgnoreCase(tab)) {
            page = getEmployerStatements(port, request);
        } else if ("EmployerTransactions".equalsIgnoreCase(tab)) {
            page = getEmployerTransactions(port, request);
        } else if ("EmployerGroupUnion".equalsIgnoreCase(tab)) {
            page = getEmployerGroupUnion(port, session, response, request);
        } else if ("EmployerCRMCommunication".equalsIgnoreCase(tab)){
            page = getCRMCommunications(request);
        }
        PrintWriter out = null;
        try {
            if (page.isEmpty()) {
                int status = Parameters.getIntParam(request, "employerStatus");
                if (status == 9) {
                    request.setAttribute("employerEntityId", request.getParameter("employerEntityId"));
                    request.setAttribute("target_div", request.getParameter("target_div"));
                    request.setAttribute("main_div", request.getParameter("main_div"));
                    RequestDispatcher dispatcher = context.getRequestDispatcher(JSP_FOLDER + "EmployerTermination.jsp");
                    dispatcher.forward(request, response);
                } else {
                    boolean result = updateAppStatus(port, request);
                    out = response.getWriter();
                    if (result) {
                        out.println(TabUtils.buildJsonResult("OK", null, null, "\"message\":\"Status has been changed.\""));
                    } else {
                        out.println(TabUtils.buildJsonResult("ERROR", null, null, "\"message\":\"Status cannot be changed until details have been saved.\""));
                    }
                }
            } else {
                RequestDispatcher dispatcher = context.getRequestDispatcher(page);
                dispatcher.forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "EmployerTabContentsCommand";
    }

    private String getEmployerDetails(NeoManagerBean port, HttpServletRequest request) {
        request.setAttribute("emp_map", EmployerGroupMapping.getMapping());
        String employerId = request.getParameter("employerEntityId");
        String parentEmployerId = request.getParameter("employerParentEntityId");
        int entityId;
        if (employerId != null && !employerId.isEmpty()) {
            entityId = Integer.parseInt(employerId);

            EmployerGroup eg = port.getEmployerByEntityId(entityId);
            Collection<ContactDetails> cd = port.getContactDetailsByEntityId(entityId);
            Collection<AddressDetails> ad = port.getAddressDetailsByEntityId(entityId);
            EmployerGroupMapping.setEmployerGroup(request, eg);
            EmployerGroupMapping.setAddressDetails(request, ad);
            EmployerGroupMapping.setContactDetails(request, cd);
            request.setAttribute("employerEntityId", employerId);
            request.setAttribute("EmployerName", eg.getEmployerName());
            request.setAttribute("EmployerNumber", eg.getEmployerNumber());
        } else if (parentEmployerId != null && !parentEmployerId.isEmpty()) {
            int parentId = Integer.parseInt(parentEmployerId);
            request.setAttribute("EmployerGroup_parentEntityId", parentEmployerId);
            EmployerGroup eg = new EmployerGroup();
//            eg.setParentEntityId(parentId);
            Collection<ContactDetails> cd = port.getContactDetailsByEntityId(parentId);
            Collection<AddressDetails> ad = port.getAddressDetailsByEntityId(parentId);
            EmployerGroupMapping.setEmployerGroup(request, eg);
            EmployerGroupMapping.setAddressDetails(request, ad);
            EmployerGroupMapping.setContactDetails(request, cd);
            request.setAttribute(employerId, ad);
        }
        return JSP_FOLDER + "EmployerDetails.jsp";
    }

    private String getEmployerBilling(NeoManagerBean port, HttpServletRequest request) {
        int entityId = getEntityId(request);
        EmployerBilling eb = port.getEmployerBillingByEntityId(entityId);
        EmployerBillingMap.setEmployerBilling(request, eb);
        List<BankingDetails> bd = port.getAllBankingDetails(entityId);
        System.out.println("Banking list size = " + bd.size());
        EmployerBillingMap.setBankingDetails(request, bd);
        return JSP_FOLDER + "EmployerBilling.jsp";
    }

    private String getEmployerCommunication(NeoManagerBean port, HttpServletRequest request) {
        int entityId = getEntityId(request);
        EmployerComm eb = port.getEmployerCommByEntityId(entityId);
        EmployerCommunicationMapping.setEmployerComm(request, eb);
        return JSP_FOLDER + "EmployerCommunication.jsp";
    }

    private String getEmployerIntermediary(NeoManagerBean port, HttpServletRequest request) {
        int entityId = getEntityId(request);
        EmployerBroker eb = port.getEmployerBrokerByEntityId(entityId);
        EmployerIntermediaryMapping.setEmployerBroker(request, eb);
        return JSP_FOLDER + "EmployerIntermediary.jsp";
    }

    private String getEmployerUnderwriting(NeoManagerBean port, HttpServletRequest request) {
        int entityId = getEntityId(request);
        EmployerUnderwriting eb = port.getEmployerUnderwritingByEntityId(entityId);
        EmployerUnderwritingMapping.setEmployerUnderwriting(request, eb);
        return JSP_FOLDER + "EmployerUnderwriting.jsp";
    }

    private String getEmployerEligibility(NeoManagerBean port, HttpServletRequest request, ServletContext context) {
        int entityId = getEntityId(request);
        EmployerEligibility eb = port.getEmployerEligibilityByEntityId(entityId);
        EmployerEligibilityMapping.setEmployerEligibility(request, eb);
        //Object obj = new LookupService().getList("option");
        String client = String.valueOf(context.getAttribute("Client"));
        List<Option> opt = null;
        if (client != null && client.equals("Sechaba")) {
            opt = port.findOptionsForProduct(3);

            request.setAttribute("ProductOptions", opt);
        } else {
            opt = port.findOptionsForProduct(1);

            request.setAttribute("ProductOptions", opt);
        }

        request.setAttribute("ProductOptions", opt);
        return JSP_FOLDER + "EmployerEligibility.jsp";
    }

    private String getEmployerBranches(NeoManagerBean port, HttpServletRequest request) {
        String employerId = request.getParameter("employerEntityId");
        request.setAttribute("EmployerBranch_entityId", employerId);
        int entityId;
        if (employerId != null && !employerId.isEmpty()) {
            entityId = Integer.parseInt(employerId);
            List<EmployerGroup> eg = port.getEmployerBranchList(entityId);
            request.setAttribute("EmployerBranchList", eg);
        }
        return JSP_FOLDER + "EmployerBranch.jsp";
    }

    private String getEmployeeAppNotes(NeoManagerBean port, HttpServletRequest request) {
        request.setAttribute("employerEntityId", request.getParameter("employerEntityId"));
        System.out.println("employerEntityId " + request.getParameter("employerEntityId"));
        System.out.println("memResp = " + request.getParameter("menuResp"));
        request.setAttribute("menuResp", request.getParameter("menuResp"));
        return JSP_FOLDER + "EmployeeAppNotes.jsp";
    }

    private String getEmployerTransactions(NeoManagerBean port, HttpServletRequest request) {
        request.setAttribute("employerEntityId", request.getParameter("employerEntityId"));
        System.out.println("employerEntityId " + request.getParameter("employerEntityId"));
        return JSP_FOLDER + "EmployerTransactions.jsp";
    }

    private String getMembers(NeoManagerBean port, HttpServletRequest request) {
        request.setAttribute("employerEntityId", request.getParameter("employerEntityId"));
        System.out.println("employerEntityId " + request.getParameter("employerEntityId"));
        int entityId = new Integer(request.getParameter("employerEntityId"));

        int page = Parameters.getIntParam(request, "pageNum");
        page = page == 0 ? 1 : page;
        int start = (page * PAGE_SIZE) + 1 - PAGE_SIZE;
        int end = (page * PAGE_SIZE);
        List<KeyValueArray> kva = port.getMembersLinkedToGroup(entityId, start, end);
        Object col = MapUtils.getMap(kva);

        int max = 0;
        if (kva != null) {
            if (kva.size() > PAGE_SIZE) {
                kva.remove(kva.size() - 1);
                request.setAttribute("lastPage", "no");
            } else {
                request.setAttribute("lastPage", "yes");
            }
        } else {
            request.setAttribute("lastPage", "yes");
        }
        request.setAttribute("pageVal", page);

        request.setAttribute("accreList", col);

        return JSP_FOLDER + (page == 1 ? "Members.jsp" : "Members_1.jsp");
    }

    private String getDocuments(NeoManagerBean port, HttpServletRequest request) {
        HttpSession session = request.getSession();
        try {
            request.setAttribute("folderList", new PropertiesReader().getProperty("DocumentIndexWorkFolder"));
            request.setAttribute("listOrWork", "Work");
            //String memberNumber = request.getParameter("memberNumber");
            String memberNumber = (String) session.getAttribute("memberNumber");
            logger.info("Memeber no = " + memberNumber);
            if (memberNumber != null && !memberNumber.trim().equals("")) {
                IndexDocumentRequest req = new IndexDocumentRequest();
                NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
                req.setAgent(loggedInUser);
                req.setIndexType("IndexForMember");
                req.setEntityNumber(memberNumber);
                req.setSrcDrive(new PropertiesReader().getProperty("DocumentIndexIndexDrive"));
                IndexDocumentResponse resp = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
                if (resp.getIndexForMember() != null) {
                    logger.info("Index size = " + resp.getIndexForMember().size());
                }
                for (IndexForMember index : resp.getIndexForMember()) {
                    String typeName = index.getDocType();
                    Pattern p = Pattern.compile("([0-9]*)");
                    Matcher m = p.matcher(typeName);
                    if (m.matches()) {
                        typeName = port.getValueForId(212, typeName);
                        index.setDocType(typeName);
                    }
                }
                request.setAttribute("indexList", resp.getIndexForMember());
            }
            int entityId = new Integer(request.getParameter("employerEntityId"));
            request.setAttribute("memberNumber", memberNumber);
            request.setAttribute("entityId", entityId);
            request.getSession().setAttribute("memberCoverEntityId", entityId);
            //RequestDispatcher dispatcher = request.getRequestDispatcher("/Claims/CallCenterViewDocuments.jsp");
            //dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return JSP_FOLDER + "EmployeeDocuments.jsp";
    }

    private static String getEmployerStatements(NeoManagerBean port, HttpServletRequest request) {
        String employerId = request.getParameter("employerEntityId");
        request.setAttribute("EmployerStatements_employerEntityId", employerId);
        //        List<Map<String, String>> map = MapUtils.getMap(port.getEmployerStatementList(Integer.parseInt(employerId)));
        List<CommunicationLog> commLogForEntityId = port.getCommLogForEntityId(Integer.parseInt(employerId), null, null, null);
        request.setAttribute("communicationsLog", commLogForEntityId);
        return JSP_FOLDER + "EmployerStatements.jsp";
    }

    private String getEmployerAudit(NeoManagerBean port, HttpServletRequest request) {
        String employerId = request.getParameter("employerEntityId");
        request.setAttribute("EmployerAudit_employerEntityId", employerId);
        return JSP_FOLDER + "EmployerAudit.jsp";
    }

    private int getEntityId(HttpServletRequest request) {
        try {
            String maxDepStr = request.getParameter("employerEntityId");
            return Integer.parseInt(maxDepStr);
        } catch (java.lang.Exception e) {
            System.err.println(e.getMessage());
        }
        return 0;

    }

    public String getEmployerGroupUnion(NeoManagerBean port, HttpSession session, HttpServletResponse response, HttpServletRequest request) {
        String employerId = request.getParameter("employerEntityId");

        int entityId;
        if (employerId != null && !employerId.isEmpty()) {
            entityId = Integer.parseInt(employerId);
            EmployerGroup eg = port.getEmployerByEntityId(entityId);
            request.setAttribute("employerEntityId", employerId);
            request.setAttribute("EmployerName", eg.getEmployerName());
            request.setAttribute("EmployerNumber", eg.getEmployerNumber());
            
            logger.info("Entity ID : " + employerId);
            logger.info("Employer Name : " + eg.getEmployerName());
            logger.info("Employer Number : " + eg.getEmployerNumber());
          
            loadSessionValues(port, session, request, response, employerId);
        }
        request.setAttribute("employerEntityId", employerId);
        
        request.setAttribute("EmployerGroupUnion_employerEntityId", employerId);
        return JSP_FOLDER + "EmployerGroupUnion.jsp";
    }

    private boolean updateAppStatus(NeoManagerBean port, HttpServletRequest request) {
        int entityId = Parameters.getIntParam(request, "employerEntityId");
        if (entityId > 0) {
            int status = Parameters.getIntParam(request, "employerStatus");
            Security sec = new Security();
            NeoUser user = (NeoUser) request.getSession().getAttribute("persist_user");
            sec.setCreatedBy(user.getUserId());
            sec.setLastUpdatedBy(user.getUserId());
            int result = port.updateEmployerStatus(entityId, status, null, sec);
            System.out.println("Entity : " + entityId + " Status" + status + " result " + result);
            return true;
        } else {
            return false;
        }
    }

    public void loadSessionValues(NeoManagerBean port, HttpSession session, HttpServletRequest request, HttpServletResponse response, String employerEntityId) {
        UnionRepresentative unionEmployerGroup = port.retriveUnionGroupByEntityId(Integer.parseInt(employerEntityId));
        String message = "";
        if (unionEmployerGroup.getUnionID() != 0) {
            logger.info("loading Session Value (Union Group)");

            // Employer Unino Group Details
            session.setAttribute("employerName", unionEmployerGroup.getGroupName());
            session.setAttribute("employerNumber", unionEmployerGroup.getGroupNumber());
            request.setAttribute("employerGroup_unionName", unionEmployerGroup.getUnionName());
            session.setAttribute("employerGroup_unionID", unionEmployerGroup.getUnionID());
            session.setAttribute("employerGroup_unionNumber", unionEmployerGroup.getUnionNumber());
            // Rep Details
            session.setAttribute("repNumber", unionEmployerGroup.getRepresentativeNumber());
            session.setAttribute("repName", unionEmployerGroup.getName());
            session.setAttribute("repSurname", unionEmployerGroup.getSurname());
            session.setAttribute("unionRepCapacity", unionEmployerGroup.getRepresentativeCapacityID());
            session.setAttribute("unionRegion", unionEmployerGroup.getRegionID());
            session.setAttribute("unionInceptionDate", DateTimeUtils.convertToYYYYMMDD(unionEmployerGroup.getInceptionDate().toGregorianCalendar().getTime()));
            session.setAttribute("unionTerminationDate", DateTimeUtils.convertToYYYYMMDD(unionEmployerGroup.getTerminationDate().toGregorianCalendar().getTime()));
            // Contacts Contact 
            session.setAttribute("contactNumber", unionEmployerGroup.getContactNumber());
            session.setAttribute("faxNumber", unionEmployerGroup.getFaxNumber());
            session.setAttribute("emailAddress", unionEmployerGroup.getEmail());
            session.setAttribute("altEmailAddress", unionEmployerGroup.getAltEmail());
            
        } else {
            logger.info("*** No Employer Union Group Found! ***");
            // Employer Unino Group Details
            session.setAttribute("employerName", null);
            session.setAttribute("employerNumber", null);
            session.setAttribute("employerGroup_unionName", null);
            session.setAttribute("employerGroup_unionID", null);
            session.setAttribute("employerGroup_unionNumber", null);
            // Rep Details
            session.setAttribute("repName", null);
            session.setAttribute("repSurname", null);
            session.setAttribute("unionRepCapacity", null);
            session.setAttribute("unionRegion", null);
            session.setAttribute("unionInceptionDate", null);
            session.setAttribute("unionTerminationDate", null);
            // Contacts Contact 
            session.setAttribute("contactNumber", null);
            session.setAttribute("faxNumber", null);
            session.setAttribute("emailAddress", null);
            session.setAttribute("altEmailAddress", null);
        }
    }
        private String getCRMCommunications(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String employerEntityId = request.getParameter("employerEntityId");
        request.setAttribute("entityId", employerEntityId);
        return JSP_FOLDER + "EmployerCRMCommunication.jsp";   
        }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthCoverExclusions;

/**
 *
 * @author johanl
 */
public class GetDependentCoverExclusions extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();

        String coverNumber = "" + session.getAttribute("memberNum_text");
        String depNo = "" + session.getAttribute("depListValues");

        System.out.println("coverNumber = "+coverNumber);
        System.out.println("depNo = "+depNo);

        if(coverNumber != null && !coverNumber.trim().equalsIgnoreCase("null")){
            if(depNo != null && !depNo.trim().equalsIgnoreCase("null")
                    && !depNo.trim().equalsIgnoreCase("99")){
                List<AuthCoverExclusions> aceList = service.getNeoManagerBeanPort().getCoverExclusionByCoverDependant(coverNumber, depNo);
                if(aceList.size() > 0){
                    session.setAttribute("AuthExclusionList", aceList);
                    String ced = coverNumber+"|"+depNo;
                    session.setAttribute("CoverExclusionDetail", ced);
                }else{
                    session.setAttribute("ViewExclusions_error", "No Exclusions Found");
                }
            }
        }
        try {
            String nextJSP = "" + session.getAttribute("onScreen");
            request.getRequestDispatcher(nextJSP).forward(request, response);
        } catch (ServletException ex) {
            ex.printStackTrace();
        } catch (IOException io) {
            io.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return  "GetDependentCoverExclusions";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.calltrack.command;

import com.koh.command.NeoCommand;
import com.koh.utils.NeoCoverDetailsDependantFilter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverSearchCriteria;
import neo.manager.EAuthCoverDetails;

/**
 *
 * @author josephm
 */
public class SearchCallCoverByUpdateCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {


        //System.out.println("Inside MemberDetailsCoverSearchCommand");
        HttpSession session = request.getSession();
        String memberNumber = "" + session.getAttribute("memberNumber_text");

        //System.out.println("The member number is " + memberNumber);

        //Collection<CoverDetails> details = new ArrayList<CoverDetails>();
        List<EAuthCoverDetails> details = new ArrayList<EAuthCoverDetails>();

        try {
            CoverSearchCriteria search = new CoverSearchCriteria();
            search.setCoverNumberExact(memberNumber);
            details = service.getNeoManagerBeanPort().eAuthCoverSearch(search);
            //details = service.getNeoManagerBeanPort().getCoveredMembersForCall(memberNumber);

            /*for(CoverDetails cover: details) {
            
            System.out.println("The cover name is " + cover.getName());
            System.out.println("The cover surname is " + cover.getSurname());
            System.out.println("The dependent is " + cover.getDependentType());
            System.out.println("The cover number is " + cover.getCoverNumber());
            
            if(details == null) {
            
            System.out.println("The list is null ");
            }
            else {
            
            System.out.println("At least to show that we are arriving here");
            }
            
            }*/
            if (details != null && details.size() > 0) {
                //filter dependants of members to only display one dependant record for each cover dependant
                NeoCoverDetailsDependantFilter filter = new NeoCoverDetailsDependantFilter();
                details = filter.EAuthCoverDetailsDependantFilter(details);

            }

            request.setAttribute("memberCoverDetails", details);
            session.setAttribute("memberNumber_text", memberNumber);
        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return request;
    }

    @Override
    public String getName() {
        return "SearchCallCoverByUpdateCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.employer.command;

/**
 *
 * @author Administrator
 */
public class EmployerLookup {
    
    String name;
    int employerEntityId;
    boolean selected;
    
    public EmployerLookup(int employerEntityId,String name) {
        this.name = name;
        this.employerEntityId = employerEntityId;
    }
    
    public EmployerLookup(int employerEntityId,String name,boolean selected) {
        this.name = name;
        this.employerEntityId = employerEntityId;
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getEmployerEntityId() {
        return employerEntityId;
    }
    public void setEmployerEntityId(int employerEntityId) {
        this.employerEntityId = employerEntityId;
    }
    
    
    
}

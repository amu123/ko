/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.command;

import com.agile.security.webservice.AuthDetails;
import com.koh.command.FECommand;
import java.io.PrintWriter;
import java.util.GregorianCalendar;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.NeoUser;

/**
 *
 * @author whauger
 */

public class SaveOrthoAuthDetailsCommand extends FECommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        try {
            HttpSession session = request.getSession();

            //Save generic auth details
            AuthDetails ad = new AuthDetails();

            //User detail
            NeoUser user = (NeoUser) session.getAttribute("persist_user");
            ad.setModifiedBy(user.getName() + " " + user.getSurname());
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTimeInMillis(System.currentTimeMillis());
            XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            ad.setModifiedDate(xmlCal);

            //Auth detail page
            ad.setScheme("" + session.getAttribute("scheme"));
            ad.setSchemeOption("" + session.getAttribute("schemeOption"));
            ad.setAuthType("" + session.getAttribute("authType"));
            ad.setAuthorisationDate("" + session.getAttribute("authDate"));
            ad.setMemberNo("" + session.getAttribute("memNo_text"));
            String memberInfo = "" + session.getAttribute("memNo");
            String[] tokens = memberInfo.split("\\s");
            int num = tokens.length;
            String surname = "";
            ad.setFullnames(tokens[0]);
            for (int i = 1; i < num - 1; i++)
            {
                if (surname.equalsIgnoreCase(""))
                {
                    surname = tokens[i];
                }
                else
                {
                    surname = surname + " "  + tokens[i];
                }
            }
            ad.setSurname(surname);
            ad.setDependantCode(tokens[num - 1]);
            ad.setCallerName("" + session.getAttribute("callerName"));
            ad.setCallerRelationship("" + session.getAttribute("callerRelationship"));
            ad.setCallerReason("" + session.getAttribute("callerReason"));
            ad.setCallerContact("" + session.getAttribute("callerContact"));

            //Orthodontic detail page
            ad.setTreatingProviderNo(this.getValue(session.getAttribute("treatingProv_text")));
            ad.setTreatingProvider(this.getValue(session.getAttribute("treatingProv")));
            ad.setReferringProviderNo(this.getValue(session.getAttribute("referringProv_text")));
            ad.setReferringProvider(this.getValue(session.getAttribute("referringProv")));
            String icd = (String) session.getAttribute("picd10");
            int pos = icd.indexOf(" ");
            if (pos > 0) {
                String icdCode = icd.substring(0, pos);
                ad.setPrimaryIcd(icdCode);
            } else
            {
               ad.setPrimaryIcd(icd);
            }

            ad.setTarrifList("" + session.getAttribute("tariff"));
            ad.setPlannedDuration("" + session.getAttribute("numMonths"));
            ad.setEstimatedCost(""+session.getAttribute("amountClaimed"));
            ad.setDepositAmount("" + session.getAttribute("depositAmount"));
            ad.setFirstInstallmentAmount("" + session.getAttribute("firstAmount"));
            ad.setRemainingInstallmentAmount("" + session.getAttribute("remainAmount"));
            ad.setDateValidFrom(""+session.getAttribute("authFromDate"));
            ad.setDateValidTo(""+session.getAttribute("authToDate"));
            ad.setNotes("" + session.getAttribute("notes"));
            ad.setAuthStatus("" + session.getAttribute("status"));

            //Save auth object
            String authNumber = service.getAgileManagerPort().insertAuth(ad);

            if (authNumber != null && !authNumber.equalsIgnoreCase(""))
            {
                session.setAttribute("authNumber", authNumber);
                SaveOrthoPlanDetailsCommand command = new SaveOrthoPlanDetailsCommand();
                command.execute(request, response, context);
            }
            else
            {
                //Create response page
                PrintWriter out = response.getWriter();
                out.println("<html>");
                out.println("<head>");
                out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                out.println("</head>");
                out.println("<body>");
                out.println("<center>");
                out.println("<table cellspacing=\"4\">");
                out.println("<tr>");
                out.println("<td><label class=\"header\">Authorisation saving failed.</label></td>");
                out.println("</tr>");
                out.println("</table>");
                out.println("<p> </p>");
                out.println("</center>");
                out.println("</body>");
                out.println("</html>");
            }

            //Save orthodontic plan details
            /*
            OrthodonticPlanDetails oplan = new OrthodonticPlanDetails();

            oplan.setAuthNumber(authNumber);
            oplan.setAuthDate(this.webserviceConvert("" + session.getAttribute("authDate"), "yyyy/MM/dd"));
            oplan.setCoverNumber("" + session.getAttribute("memNo_text"));
            oplan.setDependentNumber(new Integer(tokens[2].substring(1)));
            oplan.setProviderNumber("" + session.getAttribute("treatingProv_text"));
            oplan.setTariffCode("" + session.getAttribute("tariff"));
            oplan.setDuration((Integer)session.getAttribute("numMonths"));
            oplan.setTotalAmount((Double)session.getAttribute("amountClaim"));
            oplan.setDeposit((Double)session.getAttribute("depositAmount"));
            oplan.setFirstInstallment((Double)session.getAttribute("firstAmount"));
            oplan.setMonthlyInstallment((Double)session.getAttribute("remainAmount"));
            oplan.setPlanStartDate(this.webserviceConvert("" + session.getAttribute("authFromDate"), "yyyy/MM/dd"));
            oplan.setPlanEstimatedEndDate(this.webserviceConvert("" + session.getAttribute("authToDate"), "yyyy/MM/dd"));

            port.insertOrthodonticPlan(oplan);
            */
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SaveOrthoAuthDetailsCommand";
    }

    private String getValue(Object obj)
    {
        String value = "";
        if (obj != null)
        {
            value = value + obj;
        }
        return value;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import com.koh.serv.PropertiesReader;
import com.koh.statement.command.SubmitStatementCommand;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.log4j.Logger;

/**
 *
 * @author Johan-NB
 */
public class LabelStatementDateDropDown extends TagSupport {

    private Logger logger = Logger.getLogger(this.getClass());
    private String elementName;
    private String displayName;
    private String javaScript;
    private String mandatory;
    private String errorValueFromSession;
    private int productId = 0;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        SimpleDateFormat sTOd = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat dTOs = new SimpleDateFormat("yyyy/MM/dd");
        String type = (String) session.getAttribute("stmtTypeVal");
        String netw = (String) session.getAttribute("networkIndicator");
        productId = new Integer(String.valueOf(session.getAttribute("productId")));


        try {
            out.println("<td><label class=\"label\">" + displayName + ":</label></td>");
            out.println("<td><select style=\"width:215px\" name=\"" + elementName + "\" id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }

            out.println(">");
            String selectedBox = "" + session.getAttribute(elementName);

            //Get statement dates from managecare.properties statement directory
            String path = new PropertiesReader().getProperty("statements");
            //System.out.println("statement directory: " + path);
            File stmt_dir = new File(path);

            boolean isClearingUser = (Boolean) session.getAttribute("persist_user_ccClearing");

            String[] stmtDated;

//            String[] stmtDateDir = stmt_dir.list();
            List<String> stmtDates = new ArrayList<String>();

//if(stmtDateDir != null){
//            System.out.println("stmt date dir size = " + stmtDateDir.length);
//}

            String a = (String) session.getAttribute("practiceNumber");
            String b = (String) session.getAttribute("policyHolderNumber");
            stmtDates = getDirectories(a, b, type, netw);
//            for (String s : stmtDateDir) {
//                
//                String patchVar = "";
//                if (isClearingUser) {
//                    //set path per date
//                    patchVar = path + s;
//                    //System.out.println("patchValue = "+patchVar);
//                    File clearingProvStmt = new File(patchVar);
//                    //System.out.println("clearingProvStmt size = "+clearingProvStmt.list().length);
//                    
//                    //check date folder for emcprovider
//                    String[] stmtFolders = clearingProvStmt.list();
//                    if (stmtFolders.length > 2) {
//                        stmtDates.add(s);
//                    }
//
//                } else {
//                    System.out.println("s "+s);
//                    //Remove the dates till a member is selected
////                    stmtDates.add(s);
//                }
//
//            }
            System.out.println("list date size = " + stmtDates.size());
            int size = stmtDates.size();
            stmtDated = new String[stmtDates.size()];
            for (int x = 0; x < size; x++) {
                stmtDated[x] = stmtDates.get(x);
            }

            out.println("<option value=\"99\"></option>");
            for (String s : stmtDated) {
                //format date
                String dateStr = "";
                try {
                    Date d = sTOd.parse(s);
                    dateStr = dTOs.format(d);
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }

                if (dateStr.trim().equalsIgnoreCase(selectedBox)) {
                    out.println("<option value=\"" + dateStr + "\" selected >" + dateStr + "</option>");
                } else {
                    out.println("<option value=\"" + dateStr + "\" >" + dateStr + "</option>");
                }
            }
            out.println("</select></td>");
            if (mandatory.equalsIgnoreCase("yes")) {
                out.println("<td><label class=\"red\">*</label></td>");
            } else {
                out.println("<td></td>");
            }

            String sessionValerror = "";
            if (errorValueFromSession.equalsIgnoreCase("yes")) {

                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                }

                if (sessionValerror != null && !sessionValerror.equalsIgnoreCase("null")) {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
                } else {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
                }
            }
        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelStatementDateDropDown tag", ex);
        }
        return super.doEndTag();
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setErrorValueFromSession(String errorValueFromSession) {
        this.errorValueFromSession = errorValueFromSession;
    }

    private List getDirectories(String practiceNumber, String policyHolderNumber, String type, String netw) {
        List<String> list = new ArrayList<String>();

        try {
            SimpleDateFormat sTOd = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat dTOs = new SimpleDateFormat("yyyy/MM/dd");

            String path = new PropertiesReader().getProperty("statements");


            if (productId == 1) {
                path += "ResolutionHealth\\";
            } else if (productId == 2) {
                path += "Spectramed\\";
            }

            //logger.info("Path: " + path);

            File dir = new File(path);
            boolean isAdd = false;

            for (File moreDir : dir.listFiles()) {

                Date d = sTOd.parse(moreDir.getName());
                String dateStr = dTOs.format(d);
                File file = null;

                if (moreDir.isDirectory()) {
                    if (type.equalsIgnoreCase("member")) {
                         file = new File(moreDir.getAbsolutePath() + "/" + dateStr + "/member/Statement_" + policyHolderNumber + ".pdf");
                    } else if (type.equalsIgnoreCase("provider")) {
                        file = new File(moreDir.getAbsolutePath() + "/" + dateStr + "/provider/Statement_" + practiceNumber + ".pdf");
                    }
                   
                    if (file.isFile()) {
                        list.add(moreDir.getName());
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception getting the folders: " + e.getMessage());
        }
        return list;
    }
}

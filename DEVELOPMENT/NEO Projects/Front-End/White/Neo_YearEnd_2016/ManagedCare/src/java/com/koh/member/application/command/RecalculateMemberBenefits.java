/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;

/**
 *
 * @author dewaldvdb
 */
public class RecalculateMemberBenefits extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        String coverNumber = request.getParameter("policyNumber_text");
        String flag = "1";
        session.setAttribute("policyNumber_text", coverNumber);
        
        boolean b = port.recalculateMemberbenefits(coverNumber);
        
        if(!b)session.setAttribute("successfulRecal", flag);
        
        try {
            String nextJSP = "/Member/BenefitRecalculate.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "RecalculateMemberBenefits";
    }
}
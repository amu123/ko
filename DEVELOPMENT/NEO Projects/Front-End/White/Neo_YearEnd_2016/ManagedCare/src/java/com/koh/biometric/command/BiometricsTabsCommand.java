/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.BiometricsTabs;

/**
 *
 * @author martins
 */
public class BiometricsTabsCommand extends NeoCommand {

    //@Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        session.removeAttribute("Atabs");
        session.setAttribute("other", "");
        session.removeAttribute("bioObject");

        String append = "";
        String icdcode = request.getParameter("Ticd10_text").toUpperCase();
        String oldIcdCode = (String) session.getAttribute("oldICD");

        List<BiometricsTabs> retObject = port.getBiometricTabs(icdcode);
        String UpdateBiometrics = (String) session.getAttribute("UpdateBiometrics");
        if((UpdateBiometrics == null || !UpdateBiometrics.equalsIgnoreCase("true")) || ((!oldIcdCode.toUpperCase().equals(icdcode))))
        {
            //remove asthma values from session
            session.removeAttribute("asthmaDaySym_text");
            session.removeAttribute("asthmaNightSym_text");
            session.removeAttribute("sabaUse_text");
            session.removeAttribute("astmanDetail_text");
            session.removeAttribute("bpef_text");
            //remove hypertension values from session
            session.removeAttribute("hypertensionDetails_text");
            //remove hypperlipidaemia values from session 
            String TotalCholesterol = String.valueOf(session.getAttribute("fev1Predicted_text"));
            String Ldlc = String.valueOf(session.getAttribute("ldlc_text"));
            String totalCholesterol = String.valueOf(session.getAttribute("totalCholesterol_text"));
            String hdlc = String.valueOf(session.getAttribute("hdlc_text"));
            String framingham = String.valueOf(session.getAttribute("framingham_text"));
            String score = String.valueOf(session.getAttribute("score_text"));
            String Triglyceride = String.valueOf(session.getAttribute("triglyceride_text"));
            String TcHdlRatio = String.valueOf(session.getAttribute("tcHdlRatio_text"));
            String HyperlipaemiaDetail = String.valueOf(session.getAttribute("hyperlipaemiaDetail_text"));
            if((score == null || score.equals("") || score.equals("null") || score.equals("0.0")) 
                    && (framingham == null || framingham.equals("") || framingham.equals("null") || framingham.equals("0.0")) 
                    && (hdlc == null || hdlc.equals("") || hdlc.equals("null") || hdlc.equals("0.0")) 
                    && (totalCholesterol == null || totalCholesterol.equals("") || totalCholesterol.equals("null") || totalCholesterol.equals("0.0")) 
                    && (TotalCholesterol == null || TotalCholesterol.equals("") || TotalCholesterol.equals("null") || TotalCholesterol.equals("0.0")) 
                    && (Ldlc == null || Ldlc.equals("") || Ldlc.equals("null") || Ldlc.equals("0.0"))
                    && (Triglyceride == null || Triglyceride.equals("") || Triglyceride.equals("null") || Triglyceride.equals("0.0"))
                    && (TcHdlRatio == null || TcHdlRatio.equals("") || TcHdlRatio.equals("null") || TcHdlRatio.equals("0.0"))
                    && (HyperlipaemiaDetail == null || HyperlipaemiaDetail.equals("") || HyperlipaemiaDetail.equals("null") || HyperlipaemiaDetail.equals("0.0"))){
                
                session.setAttribute("HadInformationBeforeSwitch", "false");
                //System.out.println("HadInformationBeforeSwitch = FALSE");
            }
            else
            {
                session.setAttribute("HadInformationBeforeSwitch", "true");
                //System.out.println("HadInformationBeforeSwitch = TRUE");
            }
            session.removeAttribute("totalCholesterol_text");
            session.removeAttribute("ldlc_text");
            session.removeAttribute("hdlc_text");
            session.removeAttribute("framingham_text");
            session.removeAttribute("score_text");
            session.removeAttribute("triglyceride_text");
            session.removeAttribute("tcHdlRatio_text");
            session.removeAttribute("hyperlipaemiaDetail_text");
            //remove coronary values from session
            session.removeAttribute("CoronaryHbAIC_text");
            session.removeAttribute("CoronaryTotalCholesterol_text");
            session.removeAttribute("CoronaryLdlc_text");
            session.removeAttribute("CoronaryHdlc_text");
            session.removeAttribute("CoronaryTryglyseride_text");
            session.removeAttribute("CoronaryTchdlRatio_text");
            session.removeAttribute("coronaryDetail_text");
            //remove COPD values from session
            session.removeAttribute("fev1Predicted_text");
            session.removeAttribute("fev1FVC_text");
            session.removeAttribute("dyspnea_text");
            session.removeAttribute("COPDdetail_text");
            //remove Diabetes values from session
            session.removeAttribute("diabetesHbAIC_text");
            session.removeAttribute("diabetesRandomGlucose_text");
            session.removeAttribute("diabetesFastingGlucose_text");
            session.removeAttribute("diabetesTotalCholesterol_text");
            session.removeAttribute("diabetesldlc_text");
            session.removeAttribute("diabeteshdlc_text");
            session.removeAttribute("diabetesTryglyseride_text");
            session.removeAttribute("diabetesProteinuria_text");
            session.removeAttribute("diabetesTchdlRation_text");
            session.removeAttribute("diabetesDetail_text");
            //remove Chronic values from session
            session.removeAttribute("sCreatinin_text");
            session.removeAttribute("proteinuria_text");
            session.removeAttribute("renalDetail_text");
            //remove Major Depression values from session
            session.removeAttribute("hamdScore_text");
            session.removeAttribute("depressionDetail_text");
            //remove Cardiac values from session
            session.removeAttribute("nyhaClass_text");
            session.removeAttribute("CardiacDetail_text");
            session.removeAttribute("lvef_text");
            //remove Other values from session
            session.removeAttribute("otherDetails_text");            
        }
        else
        {
            //System.out.println("OldICD = " + oldIcdCode.toUpperCase() + " - NewICD = " + icdcode);
            if(!oldIcdCode.toUpperCase().equals(icdcode))
            {
                //remove asthma values from session
                session.removeAttribute("asthmaDaySym_text");
                session.removeAttribute("asthmaNightSym_text");
                session.removeAttribute("sabaUse_text");
                session.removeAttribute("astmanDetail_text");
                session.removeAttribute("bpef_text");
                //remove hypertension values from session
                session.removeAttribute("hypertensionDetails_text");
                //remove hypperlipidaemia values from session 
                session.removeAttribute("totalCholesterol_text");
                session.removeAttribute("ldlc_text");
                session.removeAttribute("hdlc_text");
                session.removeAttribute("framingham_text");
                session.removeAttribute("score_text");
                session.removeAttribute("triglyceride_text");
                session.removeAttribute("tcHdlRatio_text");
                session.removeAttribute("hyperlipaemiaDetail_text");
                //remove coronary values from session
                session.removeAttribute("CoronaryHbAIC_text");
                session.removeAttribute("CoronaryTotalCholesterol_text");
                session.removeAttribute("CoronaryLdlc_text");
                session.removeAttribute("CoronaryHdlc_text");
                session.removeAttribute("CoronaryTryglyseride_text");
                session.removeAttribute("CoronaryTchdlRatio_text");
                session.removeAttribute("coronaryDetail_text");
                //remove COPD values from session
                session.removeAttribute("fev1Predicted_text");
                session.removeAttribute("fev1FVC_text");
                session.removeAttribute("dyspnea_text");
                session.removeAttribute("COPDdetail_text");
                //remove Diabetes values from session
                session.removeAttribute("diabetesHbAIC_text");
                session.removeAttribute("diabetesRandomGlucose_text");
                session.removeAttribute("diabetesFastingGlucose_text");
                session.removeAttribute("diabetesTotalCholesterol_text");
                session.removeAttribute("diabetesldlc_text");
                session.removeAttribute("diabeteshdlc_text");
                session.removeAttribute("diabetesTryglyseride_text");
                session.removeAttribute("diabetesProteinuria_text");
                session.removeAttribute("diabetesTchdlRation_text");
                session.removeAttribute("diabetesDetail_text");
                //remove Chronic values from session
                session.removeAttribute("sCreatinin_text");
                session.removeAttribute("proteinuria_text");
                session.removeAttribute("renalDetail_text");
                //remove Major Depression values from session
                session.removeAttribute("hamdScore_text");
                session.removeAttribute("depressionDetail_text");
                //remove Cardiac values from session
                session.removeAttribute("nyhaClass_text");
                session.removeAttribute("CardiacDetail_text");
                session.removeAttribute("lvef_text");
                //remove Other values from session
                session.removeAttribute("otherDetails_text");
                
                
                session.removeAttribute("asthmaDaySym");
                session.removeAttribute("asthmaNightSym");
                session.removeAttribute("sabaUse");
                session.removeAttribute("astmanDetail");
                session.removeAttribute("bpef");
                //remove hypertension values from session
                session.removeAttribute("hypertensionDetails");
                //remove hypperlipidaemia values from session 
                session.removeAttribute("totalCholesterol");
                session.removeAttribute("ldlc");
                session.removeAttribute("hdlc");
                session.removeAttribute("framingham");
                session.removeAttribute("score");
                session.removeAttribute("triglyceride");
                session.removeAttribute("tcHdlRatio");
                session.removeAttribute("hyperlipaemiaDetail");
                //remove coronary values from session
                session.removeAttribute("CoronaryHbAIC");
                session.removeAttribute("CoronaryTotalCholesterol");
                session.removeAttribute("CoronaryLdlc");
                session.removeAttribute("CoronaryHdlc");
                session.removeAttribute("CoronaryTryglyseride");
                session.removeAttribute("CoronaryTchdlRatio");
                session.removeAttribute("coronaryDetail");
                //remove COPD values from session
                session.removeAttribute("fev1Predicted");
                session.removeAttribute("fev1FVC");
                session.removeAttribute("dyspnea");
                session.removeAttribute("COPDdetail");
                //remove Diabetes values from session
                session.removeAttribute("diabetesHbAIC");
                session.removeAttribute("diabetesRandomGlucose");
                session.removeAttribute("diabetesFastingGlucose");
                session.removeAttribute("diabetesTotalCholesterol");
                session.removeAttribute("diabetesldlc");
                session.removeAttribute("diabeteshdlc");
                session.removeAttribute("diabetesTryglyseride");
                session.removeAttribute("diabetesProteinuria");
                session.removeAttribute("diabetesTchdlRation");
                session.removeAttribute("diabetesDetail");
                //remove Chronic values from session
                session.removeAttribute("sCreatinin");
                session.removeAttribute("proteinuria");
                session.removeAttribute("renalDetail");
                //remove Major Depression values from session
                session.removeAttribute("hamdScore");
                session.removeAttribute("depressionDetail");
                //remove Cardiac values from session
                session.removeAttribute("nyhaClass");
                session.removeAttribute("CardiacDetail");
                session.removeAttribute("lvef");
                //remove Other values from session
                session.removeAttribute("otherDetails");
            }
            
        }
        
        session.setAttribute("bioObject", retObject);

        session.removeAttribute("tabSelected");
        
        int count = 0;
        
        for (BiometricsTabs bio : retObject) {
               // System.out.println("@retObject: " + bio.getCD());
            if (bio.getCD().equals("Asthma Biometric ICD")) {
                append = append + " <li><a id=\"link_AST\" onClick=\"load_selectedTabs(this.id);\" href=\"#Asthma\" >Asthma</a></li>";
                count++;
                if (count == 1) {
                    session.setAttribute("tabSelected", "link_AST");
                }
           }
            if (bio.getCD().equals("Hypertension Biometric ICD")) {
                append = append + " <li><a id=\"link_HTENS\" onClick=\"load_selectedTabs(this.id);\" href=\"#Hypertension\">Hypertension</a></li>";
                count++;
                if (count == 1) {
                    session.setAttribute("tabSelected", "link_HTENS");
                }
            }
            if(bio.getCD().equals("Renal Biometric ICD")){
                append = append + " <li><a id=\"link_CRD\" onClick=\"load_selectedTabs(this.id);\" href=\"#ChronicalRenalDisease\">Chronical Renal Disease</a></li>";
                count++;
                if (count == 1) {
                    session.setAttribute("tabSelected", "link_CRD");
                }
            }
            if(bio.getCD().equals("Cardiac Biometric ICD")){
                append = append + " <li><a id=\"link_CF\" onClick=\"load_selectedTabs(this.id);\" href=\"#CardiacFailure\">Cardiac Failure / Cardio Myopathy</a></li>";
                count++;
                if (count == 1) {
                    session.setAttribute("tabSelected", "link_CF");
                }
            }
            if(bio.getCD().equals("Diabetes Biometric ICD") || bio.getCD().equals("Diabetes Insipidus") || 
                    bio.getCD().equals("Diabetes Mellitus Type 1") || bio.getCD().equals("Diabetes Mellitus Type 2")){
                append = append + " <li><a id=\"link_DIA\" onClick=\"load_selectedTabs(this.id);\" href=\"#Diabetes\">Diabetes</a></li>";
                count++;
                //session.setAttribute("cdlType", bio.getBlCDLvalue());
                if (count == 1) {
                    session.setAttribute("tabSelected", "link_DIA");
                }
            }
            if(bio.getCD().equals("Coronary Biometric ICD")){
                append = append + " <li><a id=\"link_CAD\" onClick=\"load_selectedTabs(this.id);\" href=\"#CoronaryArteryDisease\">Coronary Artery Disease</a></li>";
                count++;
                if (count == 1) {
                    session.setAttribute("tabSelected", "link_CAD");
                }
            }
            if(bio.getCD().equals("COPD Biometric ICD")){
                append = append + " <li><a id=\"link_CB\" onClick=\"load_selectedTabs(this.id);\" href=\"#COPD\">COPD and Bronchiectasis</a></li>";
                count++;
                if (count == 1) {
                    session.setAttribute("tabSelected", "link_CB");
                }
            }
            if(bio.getCD().equals("Hyperlipidaemia Biometric ICD")){
                append = append + " <li><a id=\"link_HLIP\" onClick=\"load_selectedTabs(this.id);\" href=\"#Hyperlipidaemia\">Hyperlipidaemia</a></li>";
                count++;
                if (count == 1) {
                    session.setAttribute("tabSelected", "link_HLIP");
                }
            }
            if(bio.getCD().equals("Depression Biometric ICD")){
                append = append + " <li><a id=\"link_MD\" onClick=\"load_selectedTabs(this.id);\" href=\"#Major Depression\">Major Depression</a></li>";
                count++;
                if (count == 1) {
                    session.setAttribute("tabSelected", "link_MD");
                }
            }


//            if(bio.getCD().equals("HIV Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "HIV");
//            }
//            if(bio.getCD().equals("GORD Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Gastro-oesophageal reflux disease (GORD)");
//            }
//            if(bio.getCD().equals("Hypertrophy Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Benign Prostatic Hypertrophy");
//            }
//            if(bio.getCD().equals("Gout Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Gout");
//            }
//            if(bio.getCD().equals("Cushing Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Cushing Syndrome");
//            }
//            if(bio.getCD().equals("Ankylosing Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Ankylosing Spondylitis");
//            }
//            if(bio.getCD().equals("Osteoarthritis Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Osteoarthritis");
//            }
//            if(bio.getCD().equals("ADHD Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "ADHD");
//            }
//            if(bio.getCD().equals("Delusional Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Delusional Disorder");
//            }
//            if(bio.getCD().equals("Menopause Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Female Menopause");
//            }
//            if(bio.getCD().equals("Hyperthyroidism Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Hyperthyroidism");
//            }
//            if(bio.getCD().equals("ITP Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Idiopathic Thrombocytopenic Purpura (ITP)");
//            }
//            if(bio.getCD().equals("Fibrosis Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Interstitial Fibrosis of the Lung");
//            }
//            if(bio.getCD().equals("Meniere's Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Meniere's Syndrome");
//            }
//            if(bio.getCD().equals("Motor Neurone Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Motor Neurone Disease");
//            }
//            if(bio.getCD().equals("Myasthenia Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Myasthenia Gravis");
//            }
//            if(bio.getCD().equals("Osteoporosis Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Osteoporosis");
//            }
//            if(bio.getCD().equals("Peripheral Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Peripheral Vascular Disease (Arterial)");
//            }
//            if(bio.getCD().equals("Adenoma Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Pituitary Adenoma");
//            }
//            if(bio.getCD().equals("Psoriasis Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Psoriasis");
//            }
//            if(bio.getCD().equals("Scleroderma Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Scleroderma");
//            }
//            if(bio.getCD().equals("Urinary Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Urinary Incontinence");
//            }
//            if(bio.getCD().equals("Paget Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Paget Disease (Bone)");
//            }
//            if(bio.getCD().equals("Stroke Biometric ICD")){
//                append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
//                count++;
//                if (count == 1) {
//                    session.setAttribute("tabSelected", "link_CDLOTH");
//                }
//                session.setAttribute("other", "Stroke");
//            }
        if((bio.getCD()!= null && !bio.getCD().equals("")) && append.length() < 1){
           //System.out.println("retObject: " + bio.getCD());
            
            session.removeAttribute("other");
            session.setAttribute("other", bio.getCD());
            session.setAttribute("icdTypeID", bio.getLookupTypeId());
            //session.getAttribute("other");
            //System.out.println("session: " + session.getAttribute("other"));
            append = append + " <li><a id=\"link_CDLOTH\" onClick=\"load_selectedTabs(this.id);\" href=\"#Other\">Other</a></li>";
            count++;
            if (count == 1) {
                session.setAttribute("tabSelected", "link_CDLOTH");
            }
        }
      }

        PrintWriter out = null;

        try {
            out = response.getWriter();            
            if(append.isEmpty()) {
                out.print("Failed|");
            } else {
                out.print("Done|");
            }
            session.setAttribute("Atabs", append);
            

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        session.setAttribute("getTrue", "true");
        return null;
    }

    //@Override
    public String getName() {
        return "BiometricsTabsCommand";
    }
}

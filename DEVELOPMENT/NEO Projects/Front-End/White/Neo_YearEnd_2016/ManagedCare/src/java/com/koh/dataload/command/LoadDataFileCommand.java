/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.dataload.command;

import neo.manager.NeoUser;
import com.koh.command.NeoCommand;
import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.DataLoaderStagingStatus;
import neo.manager.ErrorLog;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author KatlegoM
 */
public class LoadDataFileCommand extends NeoCommand {

    Logger logger = Logger.getLogger(LoadDataFileCommand.class);
    private final static int LOAD_DATA_FILE = 1;
    private String uploadedFileName = "";
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sysFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    SimpleDateFormat sysFormat2 = new SimpleDateFormat("ddMMyyyy_hhmmss");
    NeoUser neoUser = null;
    Date todayDate = null;
    int innerCommand = -1;
    String strToday = "";
    String dateFileName = "";

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
       // String nextJSP = "/SystemAdmin/DataLoaderComplete.jsp";
        System.out.println("Inside LoadDataFileCommand ");

        neoUser = (NeoUser) session.getAttribute("persist_user");
        PrintWriter out = null;

        try {

            out = response.getWriter();

            strToday = sysFormat.format(new Date());
            dateFileName = sysFormat2.format(new Date());
            todayDate = sysFormat.parse(strToday);

            System.out.println("Session user = " + neoUser.getUsername());
            File uploadedFile = fileUpload(session, request);

            //NeoManagerBean port = service.getNeoManagerBeanPort();
            DataLoaderStagingStatus dlss =  service.getNeoManagerBeanPort().loadDataFileToStaging(uploadedFile.getPath());


            if (dlss != null) {

                session.setAttribute("stagingFileName", dlss.getStagingFileName());
                session.setAttribute("actualTotalRecords", dlss.getActualTotalRecords());
                session.setAttribute("totalRecordsInserted", dlss.getTotalRecordsInserted());
                if (dlss.getStagingStatus() == 1) {
                   // session.setAttribute("stagingStatusSuccess", "Success");
                } else {
                   // session.setAttribute("stagingStatusError", "Error");
                }

                System.out.println("********************Doing some debugging*********************");
                System.out.println("The staging file name is " + dlss.getStagingFileName());
                System.out.println("The actual total records " + dlss.getActualTotalRecords());
                System.out.println("The total records inserted " + dlss.getTotalRecordsInserted());
                System.out.println("The staging status is " + dlss.getStagingStatus());

                for(ErrorLog error: dlss.getErrorLogMessages()) {
                    System.out.println("************Debuging***********");
                    System.out.println("The error key is " + error.getErrorKey());
                    System.out.println("The file key is " + error.getFileKey());
                    System.out.println("The staging key is " + error.getStagingKey());
                    System.out.println("The error message is " + error.getErrorMessage());
                }
            }
           // String nextJSP = "/dataloader/GenericFileLoader.jsp";

            //RequestDispatcher dispatcher = request.getRequestDispatcher(nextJSP);
            //dispatcher.forward(request, response);

            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (dlss.getErrorLogMessages() != null && dlss.getErrorLogMessages().size() > 0) {
                out.println("<td><label class=\"header\">File upload for <i>" + uploadedFileName + "</i> was not successful, Please check the erorrs below!</label></td>");
                out.print("<tr><td><label class=\"header\">The following is the list of errors: <label></td></tr>");

                for (ErrorLog error : dlss.getErrorLogMessages()) {
                    out.print("<tr><td><label class=\"subheader\">The error message is " + error.getErrorMessage() + "</label></td></tr>");
                }

            } else {

                out.println("<td><label class=\"header\">File  <i>" + uploadedFileName + "</i>  uploaded successfully </label></td>");
                out.print("<tr><td><label class=\"header\">The upload report </label></td></tr>");
                out.print("<tr><td><label class=\"subheader\">The actual record in the file are " + dlss.getActualTotalRecords() + " </label></td></tr>");
                out.print("<tr><td><label class=\"subheader\">The actual record inserted in the database are " + dlss.getTotalRecordsInserted() + " </label></td></tr>");
                clearScreenFromSession(request);

            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "10; URL=/ManagedCare/dataloader/GenericFileLoader.jsp");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "LoadDataFileCommand";
    }

    private File fileUpload(HttpSession session, HttpServletRequest request) {

        File uploadedFile = null;
        try {
            // Check that we have a file upload request
            boolean isMultipart = ServletFileUpload.isMultipartContent(request);

            System.out.println("isMultipart : " + isMultipart);

            if (isMultipart) {

                // Create a factory for disk-based file items
               // FileItemFactory factory = new DiskFileItemFactory();
                
                DiskFileItemFactory itemFactory = new DiskFileItemFactory();
                itemFactory.setRepository(uploadedFile);
                itemFactory.setSizeThreshold(2000);

                // Create a new file upload handler
               // ServletFileUpload upload = new ServletFileUpload(factory);
                ServletFileUpload upload = new ServletFileUpload(itemFactory);
                // Parse the request
                List fileItems = upload.parseRequest(request);

                Iterator iter = fileItems.iterator();
                while (iter.hasNext()) {

                    FileItem item = (FileItem) iter.next();

                    if (item.getName() != null && !item.getName().equalsIgnoreCase("")) {
                        String itemName = item.getName();
                        //String uploadedFileName = itemName.subSequence(itemName.lastIndexOf("/")+1, itemName.length);
                        uploadedFileName = FilenameUtils.getName(itemName);

                        System.out.println("uploadedFileName = " + uploadedFileName);

                        File direcory = new File("c:/CimasUpload");
                        if (!direcory.exists()) {
                            logger.info("CimasUpload directory not found. Creating....");
                            new File("c:/CimasUpload").mkdir();
                            logger.info("Done");
                        } else {
                            logger.info("Directory exist. not creating.");
                        }

                        //File destination = new File("C:/CimasUpload/" + dateFileName + "/" + uploadedFileName);
                        File destination = new File("C:/CimasUpload/" + dateFileName);
                        if (!destination.exists()) {
                            destination.mkdir();
                            logger.info("Done");
                        } else {
                            logger.info("Directory exist. not creating.");
                        }

                        uploadedFile = new File("C:/CimasUpload/" + dateFileName + "/" + uploadedFileName);

                        System.out.println("Content Type is : " + item.getContentType());

                        // uploadedFile = new File(destination, item.getName());
                        item.write(uploadedFile);
                        System.out.println("Upload completed successfully !!!!!!!!!!");


                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return uploadedFile;
    }

    @Override
    public String getName() {
        return "LoadDataFileCommand";
    }
}

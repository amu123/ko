/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ForwardToMemberStatementsCommand extends NeoCommand {

    private Logger log = Logger.getLogger(ForwardToMemberStatementsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
        log.info("Inside ForwardToMemberStatementsCommand");
        this.saveScreenToSession(request);
        
        session.setAttribute("stmtTypeVal", "Member");

        try {
            String nextJSP = "/Claims/MemberStatements.jsp";
           // String nextJSP = "/Claims/MemberAddressDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        }catch(Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {

        return "ForwardToMemberStatementsCommand";
    }
}

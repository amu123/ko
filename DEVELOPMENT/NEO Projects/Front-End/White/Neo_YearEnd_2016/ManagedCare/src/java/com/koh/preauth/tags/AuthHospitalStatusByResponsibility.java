/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.tags;

import com.agile.tags.LabelNeoLookupValueDropDownError;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author Johan-NB
 */
public class AuthHospitalStatusByResponsibility extends TagSupport {

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        HttpSession session = pageContext.getSession();
        String as = "" + session.getAttribute("authStatus");

        if (as.equals("1")) {
            LabelNeoLookupValueDropDownError hsDrop1 = new LabelNeoLookupValueDropDownError();
            hsDrop1.setDisplayName("Hospital Status");
            hsDrop1.setElementName("hospStatus");
            hsDrop1.setLookupId("135");
            hsDrop1.setMandatory("yes");
            hsDrop1.setErrorValueFromSession("yes");
            hsDrop1.setJavaScript("");
            //required fields
            hsDrop1.setPageContext(pageContext);
            hsDrop1.doEndTag();

        } else {
            //get status desc
            LabelNeoLookupValueDropDownError hsDrop2 = new LabelNeoLookupValueDropDownError();
            hsDrop2.setDisplayName("Hospital Status");
            hsDrop2.setElementName("hospStatus");
            hsDrop2.setLookupId("136");
            hsDrop2.setMandatory("yes");
            hsDrop2.setErrorValueFromSession("yes");
            hsDrop2.setJavaScript("");
            //required fields
            hsDrop2.setPageContext(pageContext);
            hsDrop2.doEndTag();

        }


        return super.doEndTag();
    }

}

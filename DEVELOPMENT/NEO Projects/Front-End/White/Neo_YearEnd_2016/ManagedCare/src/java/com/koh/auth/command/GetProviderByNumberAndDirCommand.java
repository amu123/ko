/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.auth.command;

import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ContactDetails;
import neo.manager.NeoManagerBean;
import neo.manager.ProviderDetails;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class GetProviderByNumberAndDirCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());
    private ProviderDetails p;
    private int productId;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        session.removeAttribute("emailAddress");
        String element = request.getParameter("element");
        productId = new Integer(request.getParameter("productId"));
        String number = request.getParameter("number");
        String email = "";
        NeoManagerBean port = service.getNeoManagerBeanPort();
        p = port.getProviderDetailsForEntityByNumber(number);

        try {
            PrintWriter out = response.getWriter();
            if (p != null && p.getEntityId() > 0) {
                session.setAttribute("treatProvDiscType", p.getDisciplineType());
                if (element != null) {
                    //Get email contact details
                    ContactDetails cd = port.getContactDetails(p.getEntityId(), 1);
                    if (cd != null && cd.getPrimaryIndicationId() == 1) {
                        email = cd.getMethodDetails();
                        session.setAttribute("StatementProductID", String.valueOf(productId));
                        session.setAttribute("schemeOption", String.valueOf(productId));
                        session.setAttribute(element + "_text", p.getProviderNumber());
                        session.setAttribute(element, p.getSurname());

                    }
                } else {
                    out.print(getName() + "|Number=" + p.getProviderNumber() + "|Name=" + p.getSurname() + "$");
                    session.setAttribute(element + "_error", "Could not find element!");
                }

            } else {
                out.print("Error|No such provider|" + getName());
                session.setAttribute(element + "_error", "No such provider!");
            }

            out.print(getName() + "|Number=" + number + "|Email=" + email + "$");
            out.print("#");
            out.print(getDirectories(number));
            out.print("*");
            logger.info("Directories: " + getDirectories(number));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getDirectories(String number) {
        StringBuffer sb = new StringBuffer();

        try {
            SimpleDateFormat sTOd = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat dTOs = new SimpleDateFormat("yyyy/MM/dd");

            String path = new PropertiesReader().getProperty("statements");

            if (productId == 1) {
                path += "ResolutionHealth\\";
            } else if (productId == 2) {
                path += "Spectramed\\";
            }

            logger.info("Path: " + path);
            File dir = new File(path);

            boolean isAdd = false;

            for (File moreDir : dir.listFiles()) {
                if (moreDir.isDirectory()) {
                    File file = new File(moreDir.getAbsolutePath() + "/provider/Statement_" + number + ".pdf");
                    if (file.isFile()) {
                        if (isAdd) {
                            sb.append("!");
                        } else {
                            isAdd = true;
                        }
                        Date d = sTOd.parse(moreDir.getName());
                        String dateStr = dTOs.format(d);
                        sb.append(dateStr);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception getting the folders: " + e.getMessage());
        }
        return sb.toString();
    }

    @Override
    public String getName() {
        return "GetProviderByNumberAndDirCommand";
    }
}

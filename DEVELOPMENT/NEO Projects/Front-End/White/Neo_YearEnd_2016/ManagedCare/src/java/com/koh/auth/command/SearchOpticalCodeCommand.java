/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.auth.command;

import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoManagerBean;
import neo.manager.OpticalCode;

/**
 *
 * @author whauger
 */
public class SearchOpticalCodeCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("in SearchOpticalCodeCommand");
        String discipline = request.getParameter("disci");
        String opticalCode = request.getParameter("optical");
        NeoManagerBean port = service.getNeoManagerBeanPort();
        List<OpticalCode> opticalProtoList = new ArrayList<OpticalCode>();
        try {
            opticalProtoList = port.findOpticalTreatmentForCodeOrDiscipline(discipline, opticalCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*
         TariffSearchResult r = new TariffSearchResult();
         r.setCode("10011");
         r.setDescription("Expensive");

         TariffSearchResult r2 = new TariffSearchResult();
         r2.setCode("20022");
         r2.setDescription("Very Expensive");

         tariffs.add(r);
         tariffs.add(r2);
         */

        request.setAttribute("searchOpticalCodeResult", opticalProtoList);
        try {
            String nextJSP = "/Auth/OpticalCodeSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SearchOpticalCodeCommand";
    }
}

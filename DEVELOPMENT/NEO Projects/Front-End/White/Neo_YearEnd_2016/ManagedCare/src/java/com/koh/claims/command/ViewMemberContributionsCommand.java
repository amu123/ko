
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.lang.Exception;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ContributionAge;
import neo.manager.ContributionReceipts;
import neo.manager.ContributionTransactionHistoryView;
import neo.manager.CoverDetails;
import neo.manager.EAuthCoverDetails;
import neo.manager.NeoManagerBean;
import neo.manager.KeyValueArray;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ViewMemberContributionsCommand extends NeoCommand {
    
    

    private Logger logger = Logger.getLogger(ViewMemberContributionsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        logger.info("*************************** ViewMemberContributionsCommand ********************************");
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        session.setAttribute("ageAnalysisProductID", 0);
        String coverNum = (String) session.getAttribute("policyHolderNumber");
        logger.info("LoadMemberContributionsCommand");
        logger.info("coverNum : " + coverNum);
        
        CoverDetails c = port.getEAuthResignedMembers(coverNum);
        session.setAttribute("ageAnalysisProductID", c.getProductId());
        int entityId = c.getEntityId();
        
        Date st = TabUtils.getDateParam(request, "contrib_start_date");
        Date en = TabUtils.getDateParam(request, "contrib_end_date");
        Date ad = TabUtils.getDateParam(request, "contrib_ad_date");
        Date endDate = new Date();
        Calendar cal = Calendar.getInstance();
        Date startDate;
        if (st == null) {
            cal.setTime(endDate);
            cal.add(Calendar.MONTH, -6);
            startDate = cal.getTime();
        } else {
            startDate = st;
        }
        if (en == null) {
            cal.setTime(endDate);
            cal.add(Calendar.MONTH, 1);
            endDate = cal.getTime();
        } else {
            endDate = en;
        }
        if (ad == null) {
            ad = new Date();
        }
        DateTimeUtils.convertDateToXMLGregorianCalendar(endDate);
        List<ContributionTransactionHistoryView> memberContributionsHistory = port.fetchMemberContributionsHistory(coverNum, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), DateTimeUtils.convertDateToXMLGregorianCalendar(endDate), DateTimeUtils.convertDateToXMLGregorianCalendar(ad));
        logger.info("memberContributionsHistory total  : " + memberContributionsHistory.size());
        
        List<ContributionReceipts> unallocatedReceipts = port.fetchUnallocatedReceiptsByEntityId(entityId, null);
        
        int productId = 1;
        try
        {
            productId = Integer.valueOf(session.getAttribute("productId").toString());
        }
        catch(Exception ex)
        {
            System.out.println("Couldn't conver prodID: " + ex.getMessage());
        }
        if (productId > 0) {
        } else {
            productId = 1;
            System.out.println("Member Product name = NULL!");
        }

        List<ContributionAge> memberAgeAnalysis = port.getAgeAnalysis(coverNum, null);
        
        request.setAttribute("Contributions", memberContributionsHistory);
        request.setAttribute("Receipts", unallocatedReceipts);
        request.setAttribute("memberAgeAnalysis", memberAgeAnalysis);
        request.setAttribute("contrib_end_date", DateTimeUtils.convertToYYYYMMDD(endDate));
        request.setAttribute("contrib_start_date", DateTimeUtils.convertToYYYYMMDD(startDate));
        request.setAttribute("contrib_ad_date", DateTimeUtils.convertToYYYYMMDD(ad));
        
        List<KeyValueArray> debitOrderHistory = port.fetchDebitOrderHistory(coverNum, null, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), DateTimeUtils.convertDateToXMLGregorianCalendar(endDate), productId);
        request.setAttribute("debitOrder", MapUtils.getMap(debitOrderHistory));
        List<Map<String, String>> map = MapUtils.getMap(port.getCoverSavingsDetails(coverNum));
        request.setAttribute("savingsLimits", map);
        if (map != null && map.size() > 0) {
            boolean interest = false;
            boolean jnl = false;
            boolean claims = false;
            boolean savings = false;
            boolean contrib = false;
            for (Map<String, String> m : map) {
                if (!"0".equals(m.get("interest"))) {
                    interest = true;
                }
                if (!"0".equals(m.get("int"))) {
                    jnl = true;
                }
                if (!"0".equals(m.get("claimsRecovered"))) {
                    claims = true;
                }
                if (!"0".equals(m.get("savingsPaid"))) {
                    savings = true;
                }
                if (!"0".equals(m.get("contribRecovered"))) {
                    contrib = true;
                }
            }
            request.setAttribute("hasInt", interest ? "yes" : "");
            request.setAttribute("hasJnl", jnl ? "yes" : "");
            request.setAttribute("hasClaim", claims ? "yes" : "");
            request.setAttribute("hasSav", savings ? "yes" : "");
            request.setAttribute("hasContrib", contrib ? "yes" : "");
        }
 
        try {

            String nextJSP = "/Claims/MemberContributionsDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        }catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "ViewMemberContributionsCommand";
    }

}

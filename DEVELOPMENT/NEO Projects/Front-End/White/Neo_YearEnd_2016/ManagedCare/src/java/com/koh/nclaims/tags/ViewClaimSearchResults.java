/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.tags;

import com.koh.command.NeoCommand;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.Claim;
import neo.manager.ClaimsBatch;
import neo.manager.CoverDetails;
import neo.manager.CoverProductDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.ProviderDetails;

/**
 *
 * @author janf
 */
public class ViewClaimSearchResults extends TagSupport {
    
    private String sessionAttribute;
    
    @Override
    public int doEndTag() throws JspException {
        HttpSession session = pageContext.getSession();
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        
        try {
            //out.println("<label class=\"header\">Search Results</label></br></br>");

            String message = (String) req.getAttribute("ViewClaimSearchResultsMessage");
            //String message = (String) session.getAttribute("ViewClaimSearchResultsMessage");
            if (message != null && !message.equalsIgnoreCase("")) {
                out.println("<label class=\"red\">" + message + "</label></br></br>");
            }
            
            List<ClaimsBatch> claimList = (List<ClaimsBatch>) session.getAttribute(sessionAttribute);
            if (claimList == null) {
                System.out.println("claimList is null");
            } else {
                System.out.println("claimList is not null : size = " + claimList.size());
            }
            
            if (claimList != null && !claimList.isEmpty()) {
                
                out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.print("<tr>");
                out.print("<th align=\"left\">Batch Number</th>");
                out.print("<th align=\"left\">Claim Number</th>");
                out.print("<th align=\"left\">Cover Number</th>");
                out.print("<th align=\"left\">Cover Option</th>");
                out.print("<th align=\"left\">Practice Number</th>");
                out.print("<th align=\"left\">Received Date</th>");
                out.print("<th align=\"left\">Service Date</th>");
                //out.print("<th align=\"left\">Payment Date</th>");
                out.print("<th align=\"left\">Claimed Amount</th>");
                out.print("<th align=\"left\">Paid Amount</th>");
                out.print("<th align=\"left\">Network Indicator</th>");
                //out.print("<th align=\"left\">Status</th>");
                out.print("<th align=\"left\">Action</th>");
                out.print("</tr>");
                
                
                for(ClaimsBatch claim : claimList){
                    for(Claim list_claim : claim.getClaims()){
                        out.print("<tr>");
                        out.print("<td><label class=\"label\">" + list_claim.getBatchId() + "</label></td>");
                        out.print("<td><label class=\"label\">" + list_claim.getClaimId()+ "</label></td>");
                        out.print("<td><label class=\"label\">" + list_claim.getCoverNumber()+ "</label></td>");
                        CoverProductDetails cpd = port.getCoverProductDetailsByCoverNumber(list_claim.getCoverNumber());
                        out.print("<td><label class=\"label\">" + cpd.getOptionName() + "</label></td>");
                        out.print("<td><label class=\"label\">" + list_claim.getPracticeNo()+ "</label></td>");
                        if(list_claim.getReceiveDate() != null){
                        out.print("<td><label class=\"label\">" + dateFormat.format(list_claim.getReceiveDate().toGregorianCalendar().getTime())+ "</label></td>");
                        } else {
                            out.print("<td><label class=\"label\">No Received Date</label></td>");
                        }
                        if(list_claim.getServiceDateFrom() != null) {
                        out.print("<td><label class=\"label\">" + dateFormat.format(list_claim.getServiceDateFrom().toGregorianCalendar().getTime())+ "</label></td>");
                        } else {
                            out.print("<td><label class=\"label\">No Service Date</label></td>");
                        }
                        //out.print("<td><label class=\"label\">" + list_claim.getChequeRunId() + "</label></td>");
                        //set totals
                        String totalAmountClaimed = "";
                        String totalAmountPaid = "";
                        
                        if (list_claim.getClaimCalculatedTotal() != 0.0d) {
                            totalAmountClaimed = new DecimalFormat("#.##").format(list_claim.getClaimCalculatedTotal());
                        }
                        if (list_claim.getClaimCalculatedPaidTotal() != 0.0d) {
                            totalAmountPaid = new DecimalFormat("#.##").format(list_claim.getClaimCalculatedPaidTotal());
                        }

                        out.print("<td><label class=\"label\">" + totalAmountClaimed + "</label></td>");
                        out.print("<td><label class=\"label\">" + totalAmountPaid + "</label></td>");
                        ProviderDetails prac = port.getPracticeDetailsForEntityByNumberAndDate(list_claim.getPracticeNo(), list_claim.getServiceDateFrom());
                        String networkName ="";
                        if(prac != null){
                            networkName = prac.getNetworkName();
                            if(networkName == null){
                                networkName = "";
                            }
                        }
                        out.print("<td><label class=\"label\">" + networkName + "</label></td>");
                        
                        //out.print("<td><label class=\"label\">" + list_claim.getClaimStatusId() + "</label></td>");
                        out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('ViewClaimLineDetails', '" + list_claim.getClaimId() + "','"+ list_claim.getServiceProviderNo() +"')\"; value=\"\">Details</button></td>");
                        out.print("</tr>");
                    }
                }
            }
            
        } catch (Exception e){
            e.printStackTrace();
        }
        
        return super.doEndTag();
    }
    
    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }
    
}

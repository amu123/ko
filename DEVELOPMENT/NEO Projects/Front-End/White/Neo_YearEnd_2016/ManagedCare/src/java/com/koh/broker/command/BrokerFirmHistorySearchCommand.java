/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.command.NeoCommand;
import com.koh.member.application.command.MemberApplicationSpecificQuestionsCommand;
import java.io.IOException;
import java.lang.Exception;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.*;

/**
 *
 * @author princes
 */
public class BrokerFirmHistorySearchCommand extends NeoCommand{
 
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            search(request, response, context);
        } catch (Exception ex) {
            Logger.getLogger(MemberApplicationSpecificQuestionsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void search(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        String code = request.getParameter("firmCode");
        String name = request.getParameter("firmName");
        List<BrokerFirm> firm = port.fetchAllBrokers(code, name);
        request.setAttribute("FirmSearchResults", firm);
        context.getRequestDispatcher("/Broker/FirmHistoryResults.jsp").forward(request, response);
    }
    
    @Override
    public String getName() {
        return "BrokerFirmHistorySearchCommand";
    }
}
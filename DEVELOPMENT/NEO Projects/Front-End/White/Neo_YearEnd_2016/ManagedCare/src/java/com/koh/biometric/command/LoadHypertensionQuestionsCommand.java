/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Biometrics;
import neo.manager.HypertensionBiometrics;

/**
 *
 * @author josephm
 */
public class LoadHypertensionQuestionsCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

         System.out.println("Inside LoadHypertensionQuestionsCommand");
        HttpSession session = request.getSession();
        String memberNumber = request.getParameter("memberNumber");
        int  code = Integer.parseInt(request.getParameter("depListValues"));
        HypertensionBiometrics hypertension = new HypertensionBiometrics();


        Biometrics biometrics =  service.getNeoManagerBeanPort().fetchBiometricsTypeId(hypertension, memberNumber, code);


        try {
            PrintWriter out = response.getWriter();

            if(biometrics != null && biometrics.getWeight() != 0.0) {

               out.print(getName() + "|Weight=" + biometrics.getWeight());
               out.print("|Exercise=" + biometrics.getExercisePerWeek());
               out.print("|Length=" + biometrics.getHeight());
               out.print("|BMI=" + biometrics.getBmi());
               out.print("|BloodPressureSystolic=" + biometrics.getBloodPressureSystolic());
               out.print("|BloodPressuredDiastolic=" + biometrics.getBloodPressureDiastolic());
               out.print("|SmokingQuestion=" + biometrics.getCigarettesPerDay());
               out.print("|YearsSinceStopped=" + biometrics.getYearsSinceStopped());
               out.print("|AlcoholConsumption=" + biometrics.getAlcoholUnitsPerWeek() + "$");

              

                      // "|BMI=" + biometrics.getBmi() + "|BloodPressureSystolic=" + biometrics.getBloodPressureSystolic() + "|BloodPressuredDiastolic=" +
                     //  biometrics.getBloodPressureDiastolic() + "|DateMeasured=" + biometrics.getDateMeasured() + "|DateReceived=" + biometrics.getDateReceived() +
                     //  "|TreatingProvider=" + biometrics.getTreatingProvider() + "|Source=" + biometrics.getSource() + "|ICD10=" + biometrics.getIcd10() + "$");

                System.out.println("the exercise " + biometrics.getExercisePerWeek());
                System.out.println("The weight is " + biometrics.getWeight());
                System.out.println("The current smoker is " + biometrics.getCurrentSmoker());
                System.out.println("The biometrics class is " + biometrics.getClass());
                System.out.println("The type if biometrics is " + biometrics.getClass().getName());
                System.out.println("the biometrics type is is " + biometrics.getBiometricTypeId());





            }else {

                out.print("Error|No Such member|" + getName());
            }
        }catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "LoadHypertensionQuestionsCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.MapUtils;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CarePathDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author nick
 */
public class EditCarePathTaskCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("entered EditCarePathTaskCommand");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        String taskYear = "" + request.getParameter("pdcTaskYearEdit");
        int taskMonth = 0;
        int taskWeek = 0;
        int taskType = 0;
        int taskId = 0;
        int workListId = 0;
        
        try{
            taskMonth = Integer.parseInt("" + request.getParameter("pdcTaskMonthEdit"));        
            taskWeek = Integer.parseInt("" + request.getParameter("pdcTaskWeekEdit"));
            taskType = Integer.parseInt("" + request.getParameter("pdcTaskTypeEdit"));
            taskId = Integer.parseInt("" + request.getParameter("selectedTaskId"));
            workListId = Integer.parseInt("" + session.getAttribute("workListId"));
        }catch(NumberFormatException e){
            System.out.println("Could not format number for carepath task: " + e.getMessage());
        }
        
        NeoUser u = (NeoUser) session.getAttribute("persist_user");
        
        System.out.println("TASK DETAILS = " + taskId + "," + taskYear + "," + taskMonth + "," + taskWeek + "," + taskType + "," + u.getUserId());
        
        //Assign carepath task to calendar and change assign status
        int success = port.assignDepCarePathTaskDetails(taskId, taskYear, taskMonth, taskWeek, taskType, u);
        
        if(success == 0){
            session.setAttribute("pdcTaskAssignmentError", "Error assigning task! Please try again...");
        }else{
            List<CarePathDetails> cpList = port.getAllCarePathTasksForDependant(workListId);
            Map<LookupValue, List<CarePathDetails>> cpUnassignedMap = MapUtils.buildMapForPDCCarePath(cpList, workListId, 0);
            session.setAttribute("depCarePathTaskUnassignedMap", cpUnassignedMap);
            
            Map<LookupValue, List<CarePathDetails>> cpAssignedMap = MapUtils.buildMapForPDCCarePath(cpList, workListId, 1);
            session.setAttribute("depCarePathTaskAssignedMap", cpAssignedMap);
        }
        
        session.setAttribute("pdcCarePathScrollTrue", "true");
        
        try {
            String nextJSP = "/PDC/ViewDependantCarepathDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return null;
    }

    @Override
    public String getName() {
        return "EditCarePathTaskCommand";
    }
}

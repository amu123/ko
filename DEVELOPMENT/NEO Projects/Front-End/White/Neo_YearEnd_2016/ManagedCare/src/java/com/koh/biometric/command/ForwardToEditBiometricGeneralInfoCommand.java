/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author martins
 */
public class ForwardToEditBiometricGeneralInfoCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        try{
            String nextJSP = "/biometrics/CaptureBiometrics.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        }catch(Exception e){
            e.printStackTrace();
        }


        return null;
    }

    @Override
    public String getName() {
        return "ForwardToEditBiometricGeneralInfoCommand";
    }
}

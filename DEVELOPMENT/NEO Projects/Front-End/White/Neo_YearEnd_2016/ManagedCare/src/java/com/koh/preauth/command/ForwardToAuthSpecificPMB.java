package com.koh.preauth.command;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author johanl
 */
public class ForwardToAuthSpecificPMB extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        
        GetPMBByICDCommand getPMB = new GetPMBByICDCommand();
        getPMB.execute(request, response, context);
        
        try {
            String nextJSP = "/PreAuth/AuthSpesificPMB.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ForwardToAuthSpecificPMB";
    }
    
}

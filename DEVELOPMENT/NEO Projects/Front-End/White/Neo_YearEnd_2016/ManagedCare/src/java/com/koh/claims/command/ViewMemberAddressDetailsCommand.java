/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.cover.MemberMainMapping;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AddressDetails;
import neo.manager.ContactDetails;
import neo.manager.ContactPreference;
import neo.manager.CoverDetails;
import neo.manager.CoverPDCDetails;
import neo.manager.NeoManagerBean;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ViewMemberAddressDetailsCommand extends NeoCommand {

    private Logger log = Logger.getLogger(ViewMemberAddressDetailsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        log.info("Inside ViewMemberAddressDetailsCommand");
        HttpSession session = request.getSession();
        ArrayList<ContactDetails> contactDetailsList = (ArrayList<ContactDetails>) session.getAttribute("pMemContactDetails");
        ArrayList<AddressDetails> detailsAddressList = (ArrayList<AddressDetails>) session.getAttribute("pMemAddressDetails"); 

        for (AddressDetails details : detailsAddressList) {

            if (details.getAddressType().equals("Physical")) {
                //Physical
                session.setAttribute("memberPhysicalAddressLine1", details.getAddressLine1());
                session.setAttribute("memberPhysicalAddressLine2", details.getAddressLine2());
                session.setAttribute("memberPhysicalAddressLine3", details.getAddressLine3());
                session.setAttribute("memberPhysicalCityAddrees", details.getRegion());
                session.setAttribute("memberPhysicalCodeAddress", details.getPostalCode());
            } else if (details.getAddressType().equals("Postal")) {
                //Postal
                session.setAttribute("memberPostalAddressLine1", details.getAddressLine1());
                session.setAttribute("memberPostalAddressLine2", details.getAddressLine2());
                session.setAttribute("memberPostalAddressLine3", details.getAddressLine3());
                session.setAttribute("memberPostalCityAddress", details.getRegion());
                session.setAttribute("memberPostalCodeAddress", details.getPostalCode());
            }
        }

        for (ContactDetails cd : contactDetailsList) {
            if (cd.getCommunicationMethodId() == 1) {
                session.setAttribute("memberEmailAddress", cd.getMethodDetails());
            } else if (cd.getCommunicationMethodId() == 2) {
                session.setAttribute("memberFaxNo", cd.getMethodDetails());
            } else if (cd.getCommunicationMethodId() == 3) {
                session.setAttribute("memberCellNo", cd.getMethodDetails());
            } else if (cd.getCommunicationMethodId() == 4) {
                session.setAttribute("memberWorkNo", cd.getMethodDetails());
            } else if (cd.getCommunicationMethodId() == 5) {
                session.setAttribute("memberHomeTelNumber", cd.getMethodDetails());
            }
            session.setAttribute("communicationMethod", cd.getCommunicationMethod());
            log.info("The preferred method is (communication method id )" + cd.getCommunicationMethodId());

        }
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String coverNumber = "" + session.getAttribute("policyHolderNumber_text");
        CoverDetails c = port.getEAuthResignedMembers(coverNumber);
        int entityId = c.getEntityId();
        session.setAttribute("coverEntityId", entityId);
        session.setAttribute("memberCoverEntityId", entityId);
        
        //set contact preferences
        List<ContactPreference> conPref = port.getAllContactPreferences(entityId);
        MemberMainMapping.setContactPrefDetails(request, conPref);
        
        ArrayList<ContactPreference> conPrefDetailsList = new ArrayList<ContactPreference>();
        conPrefDetailsList = (ArrayList<ContactPreference>) port.getAllContactPreferences(entityId);
            
        session.setAttribute("memberContactPrefListDetail", conPrefDetailsList);

        try {

            String nextJSP = "/Claims/MemberAddressDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch(Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "ViewMemberAddressDetailsCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.NeoManagerBean;
import neo.manager.PracticeNetworkDetails;
import neo.manager.ProviderSearchDetails;

/**
 *
 * @author johanl
 */
public class FindProviderDetailsByCode extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;
        NeoManagerBean port = service.getNeoManagerBeanPort();

        String providerNumber = request.getParameter("provNum");
        String element = request.getParameter("element");
        try {
            out = response.getWriter();
            String authType = "" + session.getAttribute("authType");
            String provTypeId = "";

            provTypeId = "" + session.getAttribute("providerDiscTypeId");
            if (provTypeId == null || provTypeId.equalsIgnoreCase("null")) {
                provTypeId = "" + session.getAttribute("treatingProviderDiscTypeId");
                System.out.println("t provTypeId " + provTypeId);
            }

            if (providerNumber != null && !providerNumber.trim().equalsIgnoreCase("null")) {
                ProviderSearchDetails pd = service.getNeoManagerBeanPort().findProviderAjaxDetailsByNumber(providerNumber);

                if (pd.getProviderNo() != null && !pd.getProviderNo().trim().equals("")) {
                    String provDescId = pd.getDisciplineType().getId();
                    String provDesc = pd.getDisciplineType().getValue();
                    String providerDesc = provDescId + " - " + provDesc;
                    String providerName = pd.getProviderName();

                    //get network
                    PracticeNetworkDetails pnd = service.getNeoManagerBeanPort().getPracticeNetworkByNumberForToday(pd.getProviderNo());
                    String networkName = "None";
                    if (pnd != null) {
                        networkName = pnd.getNetworkDescription();
                    }

                    //String[] provNameSur = providerName.split("\\-");
                    //String provName = provNameSur[0];
                    //String provSurname = provNameSur[1];
                    
                    boolean valid = true;
                    System.out.println("authType: " + authType);
                    System.out.println("provDescId: " + provDescId);
                    if (authType.trim().equals("11") && element.equalsIgnoreCase("facilityProv")) {//Hospice Care
                        valid = validateHospiceFacilityProvider(provDescId);
                    }
                    
                    System.out.println("valid: " + valid);
                    if (valid) {
                        session.setAttribute(element + "Name", providerName);
                        session.setAttribute(element + "Surname", providerName);
                        //session.setAttribute(element + "NameSur", provName + " " + provSurname);
                        session.setAttribute(element + "DiscType", provDesc);
                        session.setAttribute(element + "DiscTypeId", provDescId);
                        session.setAttribute(element + "Network", networkName);
                    } else {
                        out.println("Error|Invalid provider for Hospice Care|" + getName());
                        session.setAttribute(element + "Name", "");
                        session.setAttribute(element + "Surname", "");
                        //session.setAttribute(element + "NameSur", provName + " " + provSurname);
                        session.setAttribute(element + "DiscType", "");
                        session.setAttribute(element + "DiscTypeId", "");
                        session.setAttribute(element + "Network", "");
                    }

                    if (element.equalsIgnoreCase("facilityProv")) {
                        int prodID = Integer.parseInt("" + session.getAttribute("scheme"));
                        int optID = Integer.parseInt("" + session.getAttribute("schemeOption"));

                        String admissionD = "" + session.getAttribute("admissionDateTime");
                        Date admissionDate = null;
                        XMLGregorianCalendar authStart = null;
                        try {
                            admissionDate = new SimpleDateFormat("yyyy/MM/dd").parse(admissionD);
                            authStart = DateTimeUtils.convertDateToXMLGregorianCalendar(admissionDate);
                        } catch (ParseException ex) {
                            ex.printStackTrace();
                        }

                        boolean isDSP = port.isDSPNetwork(providerNumber, authStart, prodID, optID);
                        if (!isDSP) {
                            session.setAttribute("isDSPPenalty", "no");
                        } else {
                            session.setAttribute("isDSPPenalty", "yes");
                        }
                    }

                    String responseText = "Done|ProviderName=" + providerName + "|ProviderDiscipline=" + providerDesc + "|ProviderNetwork=" + networkName + "$";
                    out.println(responseText);
                } else {
                    out.println("Error|No such provider|" + getName());
                }

            } else {
                out.println("Error|No such provider|" + getName());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public boolean validateHospiceFacilityProvider(String providerType) {
        boolean valid = false;

        if (providerType.equalsIgnoreCase("047")
                || providerType.equalsIgnoreCase("049")
                || providerType.equalsIgnoreCase("049")
                || providerType.equalsIgnoreCase("059")
                || providerType.equalsIgnoreCase("079")) {
            valid = true;
        }

        return valid;
    }

    @Override
    public String getName() {
        return "FindProviderDetailsByCode";
    }
}

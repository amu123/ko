/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.brokerFirm.BrokerMapping;
import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Broker;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author princes
 */
public class SaveBrokerApplicationCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String result = validateAndSave(request);
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SaveBrokerApplicationCommand";
    }

    private String validateAndSave(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Map<String, String> errors = BrokerMapping.validate(request);
        NeoUser neoUser = getNeoUser(request);

        String additionalData;
        String updateFields = null;
        List<Broker> list = null;
        if (request.getParameter("BrokerApp_brokerCode") != null && !request.getParameter("BrokerApp_brokerCode").isEmpty()) {
            list = port.chechExistingBrokerByCode(request.getParameter("BrokerApp_brokerCode"));
            if (list.size() >= 1) {
                if (request.getParameter("brokerEntityId") == null || request.getParameter("brokerEntityId").isEmpty()) {
                    errors.put("BrokerApp_brokerCode_error", "Broker code is already used");
                }
            }
        }
        
        if (neoUser == null) {
            neoUser = new NeoUser();
            neoUser.setSecurityGroupId(1);
            neoUser.setUserId(1);
        }
        
        Broker broker = BrokerMapping.getBrokerApplication(request, neoUser);
        String DOB = request.getParameter("BrokerApp_brokerDateOfBirth");
        System.out.println("Broker Entity Id " + broker.getEntityId());
        
        if(broker.getEntityId() == 0 && DOB.isEmpty()){
            errors.put("BrokerApp_brokerDateOfBirth_error", "Date Of Birth is required.");
        }

        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        if ("OK".equalsIgnoreCase(status)) {
            if (save(request, broker, neoUser)) {
                additionalData = "\"enable_tabs\":\"true\"";
                Broker br = (Broker) request.getAttribute("BrokerApplication");
                Map<String, String> fieldsMap = new HashMap<String, String>();
                fieldsMap.put("brokerEntityId", br.getEntityId() + "");
                fieldsMap.put("brokerAppCode", br.getBrokerCode());
                fieldsMap.put("brokerTitle", br.getContactTitle() + "");
                fieldsMap.put("brokerCPerson", br.getContactFirstName());
                fieldsMap.put("brokerCSurname", br.getContactLastName());
                fieldsMap.put("brokerCRegion", br.getRegion()+"");
                fieldsMap.put("broker_header_memberName", br.getFirstNames() + " " + br.getSurname());
                fieldsMap.put("broker_header_applicationNumber", br.getBrokerCode());


                updateFields = TabUtils.convertMapToJSON(fieldsMap);
                additionalData = additionalData + ",\"message\":\"Broker Application Saved\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving the broker application.\"";
            }
        } else {
            additionalData = "\"enable_tabs\":\"false\"";
        }
        return TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
    }

    private boolean save(HttpServletRequest request, Broker br, NeoUser neoUser) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        //List<MemberDependantApp>  depeList = new ArrayList<MemberDependantApp>();
        try {
            br = port.saveBrokerApplication(br);
            System.out.println("New Broker Entity Common Id " + br.getEntityCommonId());
            request.setAttribute("BrokerApplication", br);
            session.setAttribute("BrokerApplication", br);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser) request.getSession().getAttribute("persist_user");
    }
}

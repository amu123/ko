/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.CoverDetails;
import neo.manager.MemberApplication;
import neo.manager.NeoManagerBean;

/**
 *
 * @author yuganp
 */
public class MemberApplicationViewCommand extends NeoCommand  {
 
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String applicationNumber = request.getParameter("memberNumber");
        System.out.println("APPLICATION NUMBER " + applicationNumber);
        String appName = request.getParameter("memberName");
        String appStatus = request.getParameter("memberStatus");
        String memberAppNumber = request.getParameter("memberAppNumber");
        String memberAppCoverNumber = request.getParameter("memberAppCoverNumber");
        String memberAppStatus = request.getParameter("memberAppStatus");
        request.setAttribute("memberAppNumber", memberAppNumber);
        request.setAttribute("memberAppCoverNumber", memberAppCoverNumber);
        request.setAttribute("memberAppStatus", memberAppStatus);
        request.setAttribute("memberAppNumber", applicationNumber);
        request.setAttribute("memberName", appName);
        request.setAttribute("memberStatus", appStatus);
        System.out.println("menuResp = " + request.getParameter("menuResp"));
        request.setAttribute("menuResp", request.getParameter("menuResp"));
        
        NeoManagerBean port = service.getNeoManagerBeanPort();
        MemberApplication memApp = port.getMemberApplicationByApplicationNumber(Integer.parseInt(applicationNumber));
        System.out.println("APPLICATION NUMBER " + applicationNumber);
        request.getSession().setAttribute("MemberApp_productId", memApp.getProductId());
        System.out.println("MemberApp_productId: " + memApp.getProductId());
        
        try {
            context.getRequestDispatcher("/MemberApplication/MemberApplication.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(MemberApplicationViewCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MemberApplicationViewCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "MemberApplicationViewCommand";
    }

}

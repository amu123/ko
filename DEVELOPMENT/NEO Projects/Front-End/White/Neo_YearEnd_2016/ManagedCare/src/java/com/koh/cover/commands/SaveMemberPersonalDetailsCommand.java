/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.koh.command.NeoCommand;
import com.koh.cover.MemberMainMapping;
import com.koh.employer.command.TabUtils;
import com.koh.fe.command.LoginCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Exception;
import java.util.Date;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.*;

/**
 *
 * @author johanl
 */
public class SaveMemberPersonalDetailsCommand extends NeoCommand {


    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String result = validateAndSave(request);
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SaveMemberPersonalDetailsCommand";
    }

    private String validateAndSave(HttpServletRequest request) {
        Map<String, String> errors = MemberMainMapping.validatePerson(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (save(request)) {
                additionalData = "\"message\":\"Member Personal Details Updated\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error updating the member personal details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
    }

    private boolean save(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);

        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);

        int entityId = MemberMainMapping.getEntityId(request);
        Integer entityCommID = (Integer) request.getSession().getAttribute("memberEntityCommon");

        try {
            PersonDetails pd = MemberMainMapping.getPersonDetail(request, entityId);
            port.updatePersonDetails(pd, sec, entityId);
            
            if (request.getParameter("PersonDetails_network_IPA") != null) {
                CoverAdditionalInfo memDoct = MemberMainMapping.getNetworkIPA(request);
                memDoct.setEntityCommonId(entityCommID);
                memDoct.setCreatedBy(sec.getCreatedBy());
                memDoct.setLastUpdatedBy(sec.getLastUpdatedBy());
                memDoct.setSecurityGroupId(sec.getSecurityGroupId());
                port.saveCoverAdd(memDoct);
            }            
            
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}

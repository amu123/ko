/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Pathology;

/**
 *
 * @author josephm
 */
public class CapturePathologyCommand extends NeoCommand {
    private Object  _datatypeFactory;
    private String nextJSP;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
       HttpSession session = request.getSession();
       boolean checkError = false;

       NeoUser user = (NeoUser) session.getAttribute("persist_user");

       NeoManagerBean port = service.getNeoManagerBeanPort();

       Pathology pathology = new Pathology();

       String coverNumber = request.getParameter("memberNumber_text");
       if(coverNumber != null && !coverNumber.equalsIgnoreCase("")) {
       
           pathology.setMemberNumber(coverNumber);
           session.setAttribute("memberNumber_text", coverNumber);
       }
       else {

           session.setAttribute("memberNumber_error", "The member field should be filled in");
           checkError = true;
           
       }
       String treatingProvider = request.getParameter("providerNumber_text");
       if(treatingProvider != null && !treatingProvider.equalsIgnoreCase("")) {

           pathology.setTreatingProvider(treatingProvider);
           session.setAttribute("providerNumber_text", treatingProvider);
       }

       else {

           session.setAttribute("testingProvider_error", "The provider field should be filled in");
           checkError = true;
           
       }
       String tariffCode = request.getParameter("tariffCode_text");
       if(tariffCode != null && !tariffCode.equalsIgnoreCase("")) {

           pathology.setTariffCode(tariffCode);
           session.setAttribute("tariffCode_text", tariffCode);
       }
       else {

           session.setAttribute("tariffCode_error", "The tariff code  field should be filled in");
           checkError = true;
           
       }
       String tarrifDescription = request.getParameter("tariffDescription");
       if(tarrifDescription != null && !tarrifDescription.equalsIgnoreCase("")) {

           pathology.setTariffDescription(tarrifDescription);
       }
       String source = request.getParameter("source");
       if(source != null && !source.equalsIgnoreCase("99")) {

           pathology.setSource(source);
       }
       else {

           session.setAttribute("source_error", "The source should be filled in");
           checkError = true;
       }
       String testResult = request.getParameter("testResults");
       if(testResult != null && !testResult.equalsIgnoreCase("")) {
           double test = Double.parseDouble(testResult);
           pathology.setTestResult(test);
       }
       else {

           session.setAttribute("testResults_error", "At least one field should be filled in");
           checkError = true;
       }
       String icd10 = request.getParameter("code_text");
       if(icd10 != null && !icd10.equalsIgnoreCase("")) {

           pathology.setIcd10(icd10);
       }else {
       
           session.setAttribute("code_error", "The icd code should be filled in");
           checkError = true;
       }

       String dependantCode = request.getParameter("depListValues");
       if(dependantCode != null && !dependantCode.equalsIgnoreCase("")) {

           pathology.setDependantCode(dependantCode);
       }else {

           session.setAttribute("depListValues_error", "dependants should be filled in");
           checkError = true;
       }
       String detail = request.getParameter("detail");
       if(detail != null && !detail.equalsIgnoreCase("")) {

           pathology.setDetail(detail);
       }else {

          checkError = true;
           session.setAttribute("detail_error", "the value should be filled in");
       }


//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        try {
            String receivedDate = request.getParameter("dateReceived");
            if (receivedDate != null && !receivedDate.equalsIgnoreCase("")) {
//            Date dateReceived = dateFormat.parse(receivedDate);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                Pattern pattern = Pattern.compile("\\s");
                Matcher matcher = pattern.matcher(receivedDate);
                boolean found = matcher.find();

                Date tFrom = null;
                XMLGregorianCalendar xTreatFrom = null;

                if (!found) {
                    tFrom = dateFormat.parse(receivedDate);
                } else {
                    tFrom = dateTimeFormat.parse(receivedDate);
                }

                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                pathology.setDateReceived(xTreatFrom);
            } else {

                session.setAttribute("dateReceived_error", "The date should be filled in");
                checkError = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        try {
            String serviceDate = request.getParameter("serviceDate");
            if (serviceDate != null && !serviceDate.equalsIgnoreCase("")) {
//            Date dateService = dateTimeFormat.parse(serviceDate);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                Pattern pattern = Pattern.compile("\\s");
                Matcher matcher = pattern.matcher(serviceDate);
                boolean found = matcher.find();

                Date tFrom = null;
                XMLGregorianCalendar xTreatFrom = null;

                if (!found) {
                    tFrom = dateFormat.parse(serviceDate);
                } else {
                    tFrom = dateTimeFormat.parse(serviceDate);
                }

                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                pathology.setServiceDate(xTreatFrom);
                session.setAttribute("serviceDate", xTreatFrom);
            } else {
                session.setAttribute("serviceDate_error", "The date should be filled in");
                checkError = true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

       try {
           if(checkError) {
                saveScreenToSession(request);
                nextJSP = "/biometrics/CapturePathology.jsp";
           }else {
                clearAllFromSession(request);
                int useId  = user.getUserId();
                int pathologyId = port.savePathology(pathology, useId);
                nextJSP = "/biometrics/CapturePathology.jsp";
                if(pathologyId > 0){
                session.setAttribute("updated", "Your pathology details have been captured");
                }else {

                    session.setAttribute("failed", "Sorry! Pathology save has failed, Please try again!");
                    nextJSP = "/biometrics/CapturePathology.jsp";
                }
           }
           RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
           dispatcher.forward(request, response);

       }catch(Exception ex) {

           ex.printStackTrace();
       }
       return null;
    }

//     public XMLGregorianCalendar convertDateXML(Date date) {
//
//        GregorianCalendar calendar = new GregorianCalendar();
//        calendar.setTime(date);
//        return getDatatypeFactory().newXMLGregorianCalendar(calendar);
//
//    }
//
//    private DatatypeFactory getDatatypeFactory() {
//        if (_datatypeFactory == null) {
//
//            try {
//                _datatypeFactory = DatatypeFactory.newInstance();
//            } catch (DatatypeConfigurationException ex) {
//                ex.printStackTrace();
//            }
//        }
//
//        return (DatatypeFactory) _datatypeFactory;
//    }

    @Override
    public String getName() {
        return "CapturePathologyCommand";
    }
}

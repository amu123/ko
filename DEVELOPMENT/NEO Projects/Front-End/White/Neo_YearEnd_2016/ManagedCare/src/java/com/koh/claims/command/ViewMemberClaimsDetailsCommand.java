/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Claim;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ViewMemberClaimsDetailsCommand extends NeoCommand {
    
    private Logger logger = Logger.getLogger(ViewMemberClaimsDetailsCommand.class);


    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        this.saveScreenToSession(request);
        logger.info("Inside ViewMemberClaimsDetailsCommand");
        HttpSession session = request.getSession();

        String index = request.getParameter("listIndex");
        session.setAttribute("listIndex", index);

        logger.info("The claim index is " + index);

        List<Claim> listClaims = (List<Claim>) session.getAttribute("listOfClaims");
        if (index != null) {
            int in = new Integer(index);

            //System.out.println("Size : In Command" + listClaims.size());
            Claim selectedClaim = listClaims.get(in);
            session.setAttribute("selected", selectedClaim);

            logger.info("the selected claim is " + selectedClaim.getClaimId());
        }

        try {
            String nextJSP = "/Claims/MemberHistoryClaims.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {

        return "ViewMemberClaimsDetailsCommand";
    }
}

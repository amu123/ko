/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Johan-NB
 */
public class GetCcEntityDetailsByNumber extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter out = null;
        NeoManagerBean port = service.getNeoManagerBeanPort();

        String entityNumber = request.getParameter("number");
        String entityType = request.getParameter("type");
        Date today = new Date(System.currentTimeMillis());
        XMLGregorianCalendar xToday = DateTimeUtils.convertDateToXMLGregorianCalendar(today);

        try {
            out = response.getWriter();

            if (entityType.equalsIgnoreCase("1")) {
                CoverDetails cd = port.getPrincipalMemberDetailsByDate(entityNumber, xToday);
                if (cd.getEntityId() != 0) {
                    System.out.println("cd entity is not null");
                    System.out.println("cd entity = " + cd.getEntityId());
                    out.println("Done|");

                } else {
                    out.println("Error|");
                }
            } else {
                System.out.println("provider type");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "GetCcEntityDetailsByNumber";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.BenefitLimitsDisplay;
import neo.manager.CoverClaimBenefitLimitStructure;
import neo.manager.NeoManagerBean;
import neo.manager.SavingsContribution;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ViewMemberBenefitsCommand extends NeoCommand {

    private Logger log = Logger.getLogger(ViewMemberBenefitsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        this.saveScreenToSession(request);
        log.info("Inside ViewMemberBenefitsCommand");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        String memberNumber = request.getParameter("policyHolderNumber");
        int depNumber = Integer.parseInt(request.getParameter("depListValues"));
        String benDateStr = request.getParameter("depBenDate");

        log.info("The values sent to the method are coverNumber" + memberNumber);


        Collection<CoverClaimBenefitLimitStructure> benLimStruct = new ArrayList<CoverClaimBenefitLimitStructure>();
        List<BenefitLimitsDisplay> benefitLimitsForDisplay = null;
        //convert date
        Date benDate = null;
        XMLGregorianCalendar xBenDate = null;
        try {
            benDate = new SimpleDateFormat("yyyy/MM/dd").parse(benDateStr);
            xBenDate = DateTimeUtils.convertDateToXMLGregorianCalendar(benDate);

        } catch (ParseException px) {
            px.printStackTrace();
        }

        if (xBenDate != null) {
//            benLimStruct = port.getBenefitLimitsForCover(memberNumber, depNumber, xBenDate);
            benefitLimitsForDisplay = port.getBenefitLimitsForDisplay(memberNumber, depNumber, xBenDate);
            System.out.println("Ben size :" + benefitLimitsForDisplay.size());
            //check for Saver Benefit
            boolean getSaverContribution = false;
            for (BenefitLimitsDisplay bens : benefitLimitsForDisplay) {
                if(bens.getBenefitType() == 2){
                    getSaverContribution = true;
                    break;
                }                    
            }
            if(getSaverContribution){
                SavingsContribution sc = port.getSaverBenefitContributionDetails(memberNumber, xBenDate);
                session.setAttribute("memberBenSavingsContribution", sc);
            }
            
            session.setAttribute("memberBenefitsList", benLimStruct);
            session.setAttribute("memberBenefitsList2", benefitLimitsForDisplay);
            session.setAttribute("depBenDate_error", null);
        } else {
            session.setAttribute("depBenDate_error", "Incorrect Date: " + benDateStr);
        }


        log.info("The size of the list is " + benLimStruct.size());
//        request.setAttribute("memberBenefitsList", benLimStruct);
        session.setAttribute("memBenDepCode", depNumber);
//        request.setAttribute("memberBenefitsList2", benefitLimitsForDisplay);

        try {
            String nextJSP = "/Claims/MemberBenefitDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "ViewMemberBenefitsCommand";
    }
}

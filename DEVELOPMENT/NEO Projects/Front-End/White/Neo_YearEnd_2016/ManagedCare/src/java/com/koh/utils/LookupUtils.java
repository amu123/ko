/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.utils;

import com.agile.command.GetBankBranchByBankIdCommand;
import com.koh.command.NeoCommand;
import com.koh.cover.MemberMainMapping;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import neo.manager.*;
import java.util.HashMap;
/**
 *
 * @author johanl
 */
public class LookupUtils {

    final static NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
    public static Map<String, String> bankBranchMap = null;
    public static Map<String, Integer> bankBranchIdMap = null;
    public static Map<String, Integer> bankMap = new HashMap<String, Integer>();

    public static List<LookupValue> getList(String name, NeoUser user) {
        List<LookupValue> lookupList = new ArrayList<LookupValue>();

        if (name.equalsIgnoreCase("product")) {
            List<Product> products = port.fetchAllProducts();
            for (Product prod : products) {
                LookupValue lv = new LookupValue();
                lv.setId(String.valueOf(prod.getProductId()));
                lv.setValue(prod.getProductName());
                lookupList.add(lv);
            }

        } else if (name.equalsIgnoreCase("option")) {
            List<Option> optlist = port.findOptionsForProduct(1);
            List<OptionVersion> optionVersions = port.findOptionVersionsForProduct(1);
            List<OptionVersion> optionVersions2 = port.findOptionVersionsForProduct(2);
            optionVersions.addAll(optionVersions2);
            for (OptionVersion opt : optionVersions) {
                LookupValue lv = new LookupValue();
                lv.setId(String.valueOf(opt.getOptionId()));
                lv.setValue(opt.getDescription());
                for (Option o : optlist) {
                    if (o.getOptionId() == opt.getOptionId()) {
                        lv.setValue(o.getOptionName());
                        break;
                    }
                }
                lookupList.add(lv);
            }
//            for (Option opt : optlist) {
//                opt.getInternalOptionVersions().get(0).
//                LookupValue lv = new LookupValue();
//                lv.setId(String.valueOf(opt.getOptionId()));
//                lv.setValue(opt.getOptionName());
//                lookupList.add(lv);
//            }

        } else if (name.equalsIgnoreCase("bank")) {
            lookupList = searchAllBanks(user);

        } else if (name.equalsIgnoreCase("branch")) {
            //List<Branch> branchList = port.getBranchTable(user, bankID);
        } else if (name.equalsIgnoreCase("employer")) {
            lookupList = port.getEmployerLookup();

        } else {
            return new ArrayList<LookupValue>();
        }

        //covenver to lookup

        return lookupList;
    }

    public static List<LookupValue> searchBranch(int bankID, String selectedBranchID, NeoUser user) {
        List<LookupValue> arrLookupList = new ArrayList<LookupValue>();
        
        List<Branch> branchList = port.getFilteredBankBranches(bankID, selectedBranchID);
        String branchName = null;
        for (Branch branch : branchList) {
            LookupValue lv = new LookupValue();
            branchName = branch.getCode() + " - " + branch.getName();
            lv.setValue(branchName);
            lv.setId(String.valueOf(branch.getId()));
            arrLookupList.add(lv);       
        }
        
        
        //<editor-fold defaultstate="collapsed" desc="Redundant">
        /*
        ArrayList<String[]> returnList = new ArrayList<String[]>();
//            List<Branch> branchList = port.getBranchTable(user, bankID);  //Commented out: getBranchTable() doesn't check for Effective End Date [2999/12/31]
        int i = 0;

        bankBranchMap = new HashMap<String, String>();
        bankBranchIdMap = new HashMap<String, Integer>();

        String[] branchCodes = new String[branchList.size()];

        for (Branch branch : branchList) {
            branchCodes[i] = branch.getName();
            bankBranchMap.put(branch.getName(), branch.getCode());
            bankBranchIdMap.put(branch.getName(), branch.getId());
            i++;
        }
        returnList.add(branchCodes);
        
        branchNames = returnList.get(0);
        //if branch list is not null, sort
        //COMMENTED OUT: using A specific search in the backend [getFilteredBankBranches]
//        if (branchNames.length != 0) {
//            Arrays.sort(branchNames, String.CASE_INSENSITIVE_ORDER);
//        }
        //add sorted values to LookupValue object
        for (String s : branchNames) {
            int branchId = bankBranchIdMap.get(s);
            String branchCode = bankBranchMap.get(s);
            s = branchCode + " - " + s;

            LookupValue lv = new LookupValue();
            lv.setValue(s);
            lv.setId(String.valueOf(branchId));
            arrLookupList.add(lv);
        }
*/
        
        //</editor-fold>

        return arrLookupList;

    }

    public static List<LookupValue> searchAllBanks(NeoUser user) {
        String[] conArrList = null;
        List<LookupValue> sortedLookupList = new ArrayList<LookupValue>();
        try {
            List<Bank> bankList = port.getBankTable(user);
            int i = 0;
            conArrList = new String[bankList.size()];
            for (Bank bank : bankList) {
                conArrList[i] = bank.getName();
                bankMap.put(conArrList[i], bank.getId());
                i++;
            }
            //sort banks
            Arrays.sort(conArrList, String.CASE_INSENSITIVE_ORDER);
            //add sorted values to LookupValue object
            for (String s : conArrList) {
                LookupValue lv = new LookupValue();
                lv.setValue(s);
                lv.setId(String.valueOf(bankMap.get(s)));
                sortedLookupList.add(lv);
            }

            return sortedLookupList;
        } catch (NeoSecurityException_Exception ex) {
            System.out.println("Exception Caught..." + ex.getMessage());
            ex.printStackTrace();

        }
        return sortedLookupList;

    }
    
    public static List<LookupValue> seachContactPrefDropdown(int entityId) {
       
        List<LookupValue> lvList = new ArrayList<LookupValue>();
        
       
        //get address details
        List<AddressDetails> adList = port.getAddressDetailsByEntityId(entityId);
        for (AddressDetails ad : adList) {
            String id = "A"+ad.getAddressTypeId();
            String value = port.getValueFromCodeTableForTableId(33, String.valueOf(ad.getAddressTypeId()));
            
            LookupValue lv = new LookupValue();
            lv.setId(id);
            lv.setValue(value);
            
            lvList.add(lv);
        }
        
        //get contact details
        List<ContactDetails> conList = port.getContactDetailsByEntityId(entityId);
        for (ContactDetails con : conList) {
            
            String id = "C"+con.getCommunicationMethodId();
            String value = port.getValueFromCodeTableForTableId(35, String.valueOf(con.getCommunicationMethodId()));
            
            LookupValue lv = new LookupValue();
            lv.setId(id);
            lv.setValue(value);
            
            lvList.add(lv);            
        }        
        
        return lvList;

    }    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.tags;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.TagSupport;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.Claim;
import neo.manager.ClaimIcd10;
import neo.manager.ClaimLine;
import neo.manager.ClaimLineBenefit;
import neo.manager.ClaimRuleMessage;
import neo.manager.CoverProductDetails;
import neo.manager.Drug;
import neo.manager.Ndc;
import neo.manager.NeoManagerBean;
import neo.manager.ProviderDetails;
import neo.manager.TariffCode;
import neo.manager.Treatment;

/**
 *
 * @author josephm
 */
public class PracticeClaimsHistoryDetailsTable extends TagSupport {

    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {

        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        System.out.println("entered Practice Claims History Detail Tabel");

        String coverNumber = "";
        int depenNo = -1;
        coverNumber = (String) session.getAttribute("policyNumber");
        depenNo = (Integer) session.getAttribute("dependantNumber");

        Claim selectedClaim = (Claim) session.getAttribute("selected");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        String serviceDateFrom = "";
        String paymentDaye = "";
        String paymentDate = "";
        try {
            out.println("<label class=\"header\">Claims Details</label></br></br>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.print("<tr>");
            out.print("<th scope=\"col\">ClaimLine Number</th>");
            out.print("<th scope=\"col\">Dependant Code</th>");
            out.print("<th scope=\"col\">ICD10 Code</th>");
            out.print("<th scope=\"col\">Nappi Code</th>");
            out.print("<th scope=\"col\">Nappi Description</th>");
            out.print("<th scope=\"col\">Tariff Code</th>");
            out.print("<th scope=\"col\">Tariff Description</th>");
            out.print("<th scope=\"col\">Treatment Date</th>");
            out.print("<th scope=\"col\">Pay Status</th>");
            out.print("<th scope=\"col\">Amount Claimed</th>");
            out.print("<th scope=\"col\">Tariff Amount</th>");
            out.print("<th scope=\"col\">Payment Date</th>");
            out.print("<th scope=\"col\">Rejection Code</th>");
            out.print("<th scope=\"col\">Rejection Description</th>");
            out.print("</tr>");
            if (selectedClaim != null) {
                List<ClaimLine> batchsub = port.fetchClaimLinesDetailsById(selectedClaim.getClaimLineIds());

                for (ClaimLine claimLine : batchsub) {

                    Collection<ClaimLineBenefit> clb = port.fetchClaimLineBenefit(claimLine.getClaimLineId());
                    double benifitTotalPaid = 0.00;
                    for (ClaimLineBenefit claimLineBenefit : clb) {
                        benifitTotalPaid = benifitTotalPaid + claimLineBenefit.getAmmountPaid();
                    }

                    String icds = "";
                    List<String> icd10 = new ArrayList<String>();
                    
                    List<ClaimIcd10> icdList = claimLine.getIcd10();
                    if (icdList != null && !icdList.isEmpty()) {
                        for (ClaimIcd10 icd : icdList) {
                            icds = icd.getIcd10Code();
                        }
                        icd10.add(icds);
                    }

                    ProviderDetails proDet = port.getProviderDetailsForEntityByNumber(selectedClaim.getServiceProviderNo());

                    CoverProductDetails cpd = port.getCoverProductDetailsByCoverNumber(selectedClaim.getCoverNumber());

                    Treatment t = null;//port.getTreatmentForResoCodeForCover(proDet.getDisciplineType().getId(), new Integer(claimLine.getTarrifCodeNo()), cpd.getOptionName(), cpd.getCoverNumber(), claimLine.getInsuredPersonDependentCode(), claimLine.getServiceDateFrom());
                    // (int productId, int optionId, Date  serviceDate, String discipline, String  practiceNo, String code)

                    TariffCode tariff = port.getTariffCode(cpd.getProductId(), cpd.getOptionId(), claimLine.getServiceDateFrom(), proDet.getDisciplineType().getId(), proDet.getPracticeNumber(), proDet.getPracticeNumber(), icd10, null, claimLine.getTarrifCodeNo());

                    Integer status = claimLine.getClaimLineStatusId();

                    out.print("<tr>");
                    out.print("<td><label class=\"label\">" + claimLine.getClaimLineId() + "</label></td>");
                    out.print("<td><label class=\"label\">" + depenNo + "</label></td>");

                    if (icds != null) {
                        out.print("<td><label class=\"label\">" + icds + "</label></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    Ndc ndc = null;
                    System.out.println("The NDC is " + claimLine.getNdc());
                    System.out.println("The NDC size is " + claimLine.getNdc().size());

                    if (claimLine.getNdc() != null && claimLine.getNdc().size() != 0) {
                        ArrayList<Ndc> ndcs = (ArrayList<Ndc>) claimLine.getNdc();
                        for (Ndc ndc1 : ndcs) {
                            if (ndc1.getPrimaryNdcIndicator() == 1) {
                                ndc = ndc1;
                                break;
                            }
                            ndc = ndc1;
                            System.out.println("The NDC primary indicator " + ndc1.getPrimaryNdcIndicator());
                        }
                    }
                    if (ndc != null) {
                        Drug d = port.getDrugForCode(ndc.getNdcCode(), proDet.getDisciplineType().getId());
                        out.print("<td><label class=\"label\">" + d.getCode() + "</label></td>");
                        out.print("<td><label class=\"label\">" + d.getName() + "</label></td>");
                    } else {
                        out.print("<td></td>");
                        out.print("<td></td>");
                    }
                    if (tariff != null) {
                        out.print("<td><label class=\"label\">" + tariff.getCode() + "</label></td>");
                        out.print("<td><label class=\"label\">" + tariff.getDescription() + "</label></td>");
                    } else {
                        out.print("<td></td>");
                        out.print("<td></td>");
                    }

                    Date serviceDate = claimLine.getServiceDateFrom().toGregorianCalendar().getTime();
                    serviceDateFrom = formatter.format(serviceDate);
                    out.print("<td><label class=\"label\">" + serviceDateFrom + "</label></td>");

                    //paystatus validation
                    String payStatus = port.getValueForId(83, status.toString());
                    if (payStatus.equalsIgnoreCase("rejected")) {
                        out.println("<td onmouseover=\"popRulesMsg('" + claimLine.getClaimLineId() + "');\" onmouseout=\"hideRulesMsg('" + claimLine.getClaimLineId() + "');\"><label class=\"label\">" + payStatus + "</label>"
                                + "<br/><div id=\"ruleTT" + claimLine.getClaimLineId() + "\" onmouseover=\"popRulesMsg(" + claimLine.getClaimLineId() + ");\" style=\"display:none\" >"
                                + "<table>"
                                + "<tr>");

                        List<ClaimRuleMessage> crmList = claimLine.getRuleMessages();
                        if (crmList != null && crmList.isEmpty() == false) {
                            out.println("<td><table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">"
                                    + "<tr>"
                                    + "<th>Rule Code</th>"
                                    + "<th>Rule Msg</th>"
                                    + "</tr>");

                            for (ClaimRuleMessage crm : crmList) {
                                out.println("<tr>"
                                        + "<td>"
                                        + "<label class=\"label\">" + crm.getMessageCode() + "</label>"
                                        + "</td>"
                                        + "<td>"
                                        + "<label class=\"label\">" + crm.getShortMsgDescription() + "</label>"
                                        + "</td>"
                                        + "</tr>");
                            }
                            out.println("</table></td>");
                        } else {
                            out.println("<td><label class=\"label\">No Rejection Reason Found</label></td>");
                        }


                        out.println("</tr>"
                                + "</table>"
                                + "</div>"
                                + "</td>");
                    } else {
                        out.print("<td><label class=\"label\">" + payStatus + "</label></td>");
                    }



                    out.print("<td><label class=\"label\">" + claimLine.getClaimedAmount() + "</label></td>");
                    out.print("<td><label class=\"label\">" + benifitTotalPaid + "</label></td>");
                    if (claimLine.getChequeRunDate() != null) {
                        paymentDate = formatter.format(claimLine.getChequeRunDate().toGregorianCalendar().getTime());
                        out.print("<td><label class=\"label\">" + paymentDate + "</label></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("<td><label class=\"label\">" + serviceDateFrom + "</label></td>");
                    out.print("<td><label class=\"label\">" + serviceDateFrom + "</label></td>");
                    out.print("</tr>");

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

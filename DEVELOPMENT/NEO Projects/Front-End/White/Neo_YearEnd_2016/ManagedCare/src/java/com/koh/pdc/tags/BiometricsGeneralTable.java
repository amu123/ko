/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import com.koh.command.NeoCommand;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.NeoManagerBean;
import neo.manager.GeneralBiometrics;

/**
 *
 * @author nick
 */
public class BiometricsGeneralTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String javascript;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

        List<GeneralBiometrics> getGeneral = (List<GeneralBiometrics>) session.getAttribute("getGeneral");
        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        String dateMeasured;

        try {
            out.print("<tr>");
            out.print("<th scope=\"col\">Date Measured</th>");
            out.print("<th scope=\"col\">ICD10</th>");
            out.print("<th scope=\"col\">Exercise</th>");
            out.print("<th scope=\"col\">Current Smoker</th>");
            out.print("<th scope=\"col\">Ex Smoker Stopped</th>");
            out.print("<th scope=\"col\">Alcohol Units</th>");
            out.print("<th scope=\"col\">Notes</th>");
            out.print("<th scope=\"col\">Select</th>");
            out.print("</tr>");

            if (getGeneral != null && getGeneral.size() > 0) {
                for (GeneralBiometrics a : getGeneral) {

                    out.print("<tr>");
                    if (a.getDateMeasured() != null) {
                        Date mDate = a.getDateMeasured().toGregorianCalendar().getTime();
                        dateMeasured = formatter.format(mDate);
                        out.print("<td><center><label class=\"label\">" + dateMeasured + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (a.getIcd10() != null) {
                        out.print("<td><center><label class=\"label\">" + a.getIcd10() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (a.getExercisePerWeek() != 0) {
                        out.print("<td><center><label class=\"label\">" + a.getExercisePerWeek() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (a.getCurrentSmoker() != null && a.getCurrentSmoker().equalsIgnoreCase("1")) {
                        out.print("<td><center><label class=\"label\">" + "Yes" + "</label></center></td>");
                    } else if (a.getCurrentSmoker() != null && a.getCurrentSmoker().equalsIgnoreCase("2")) {
                        out.print("<td><center><label class=\"label\">" + "No" + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (a.getYearsSinceStopped() > 0) {
                        out.print("<td><center><label class=\"label\">" + a.getYearsSinceStopped() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (a.getAlcoholUnitsPerWeek() != 0) {
                        out.print("<td><center><label class=\"label\">" + a.getAlcoholUnitsPerWeek() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (a.getDetail() != null) {
                        out.print("<td><center><label class=\"label\">" + a.getDetail() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("</tr>");
                    out.print("<td><center><button name=\"opperation\" type=\"button\" " + javascript + " value=\"" + a.getBiometricsId() + "\">Select</button></center></td>");
                }
            }

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.workflow.command;

import com.koh.command.NeoCommand;
import com.koh.utils.LookupUtils;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.WorkflowItem;

/**
 *
 * @author janf
 */
public class SearchWorkflowCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("in SearchWorkflowCommand...");
        HttpSession session = request.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        request.setAttribute("viewWorkflowSearchResults", null);
        WorkflowItem item = new WorkflowItem();
        List<WorkflowItem> itemList;
        String ref = "";
        String ent = "";
        String queue = "";
        String status = "";
                
        if (null != request.getParameter("refNum") && !request.getParameter("refNum").isEmpty()) {
            ref = request.getParameter("refNum");
        }

        if (null != request.getParameter("entNum") && !request.getParameter("entNum").isEmpty()) {
            ent = request.getParameter("entNum");
        }

        if (null != request.getParameter("workQueue") && !request.getParameter("workQueue").isEmpty()) {
            queue = request.getParameter("workQueue");
        }

        if (null != request.getParameter("workStatus") && !request.getParameter("workStatus").isEmpty()) {
            status = request.getParameter("workStatus");
        }
        //place parameters into search class to send to backend

        item.setRefNum(ref);
        item.setEntityNumber(ent);
        item.setQueueID(Integer.parseInt(queue));
        item.setStatusID(Integer.parseInt(status));
        
        //call search method in backend using search class
        itemList = port.getWFSearchResults(item);
        String user = "";
        for(WorkflowItem wf : itemList){
            user = port.getNeoUserNameByID(wf.getUserID());
            request.setAttribute("userAssigned", user + wf.getRefNum());
        }

        //retrieve and set search results into request to display in search results grid
        request.setAttribute("viewWorkflowSearchResults", itemList);

        try {
            String nextJSP = "/Workflow/WorkflowSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "SearchWorkflowCommand";
    }

}

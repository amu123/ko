package com.koh.statement.command;

import agility.za.documentprinting.AddToPrintPoolType;
import agility.za.documentprinting.DocumentPrintingRequest;
import agility.za.documentprinting.DocumentPrintingResponse;
import agility.za.documentprinting.PrintDocType;
import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.serv.PropertiesReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class AddToPrintPoolCommand  extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------In AddToPrintPoolCommand---------------------");

            String indexId = request.getParameter("fileLocation");
            
            String test = request.getParameter("indexId");
            
            logger.info("printId = " + indexId);
            Map<String, String> fieldsMap = new HashMap<String, String>();
            Pattern p = Pattern.compile( "([0-9]*)" );
            Matcher m = p.matcher(indexId);
			
            if (indexId != null && !indexId.trim().equals("") && m.matches()) {
                
                DocumentPrintingRequest req = new DocumentPrintingRequest();
                NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
                req.setAgent(loggedInUser.getUsername());
                req.setPrintProcessType("AddGeneratedDocToPrintPool");
                
                AddToPrintPoolType type = new AddToPrintPoolType();
		type.setIndexId(new Integer(indexId));
		type.setUsers(loggedInUser.getUsername());
		req.setAddToPrintPool(type);
                
                DocumentPrintingResponse resp = NeoCommand.service.getNeoManagerBeanPort().processPrintRequest(req);
                if(resp.isIsSuccess()){
                   logger.info("Print successful");
                   fieldsMap.put("DocView_Msg", "Print successful");
                } else {
                    logger.error("Error printing .... " + resp.getMessage());
                    fieldsMap.put("DocView_Msg", "Error printing");
                }
            }
            String updateFields = TabUtils.convertMapToJSON(fieldsMap);
            String result = TabUtils.buildJsonResult(null, null, updateFields, null);
            PrintWriter out = response.getWriter();
            out.println(result);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "AddToPrintPoolCommand";
    }
    

    
    @Override
    public String getName() {
        return "AddToPrintPoolCommand";
    }
}
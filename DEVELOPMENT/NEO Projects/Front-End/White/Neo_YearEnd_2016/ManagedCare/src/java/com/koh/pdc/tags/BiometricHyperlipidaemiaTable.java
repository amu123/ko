/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.HyperlipidaemiaBiometrics;

;

/**
 *
 * @author princes
 */
public class BiometricHyperlipidaemiaTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String javascript;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        List<HyperlipidaemiaBiometrics> getHyperlipidaemia = (List<HyperlipidaemiaBiometrics>) session.getAttribute("getHyperlipidaemia");
        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        String dateMeasured;
        try {

            out.print("<tr>");
            out.print("<th scope=\"col\">Date Measured</th>");
            out.print("<th scope=\"col\">Height</th>");
            out.print("<th scope=\"col\">Weight</th>");
            out.print("<th scope=\"col\">Blood Pressure Systolic</th>");
            out.print("<th scope=\"col\">Blood Pressure Diastolic</th>");
            out.print("<th scope=\"col\">Total Cholesterol</th>");
            out.print("<th scope=\"col\">LDLC mmol/l</th>");
            out.print("<th scope=\"col\">Tryglyceride mmol/l</th>");
            out.print("<th scope=\"col\">TC: HDL ratio</th>");
            out.print("<th scope=\"col\">Select</th>");

//            out.print("<th scope=\"col\">Date Measured</th>");
//            out.print("<th scope=\"col\">ICD10</th>");
//            out.print("<th scope=\"col\">Excercise</th>");            
//            out.print("<th scope=\"col\">Ex Smoker Stopped</th>");
//            out.print("<th scope=\"col\">Alchohol Units</th>");
//            out.print("<th scope=\"col\">Total Cholesterol</th>");
//            out.print("<th scope=\"col\">LDLC mmol/l</th>");
//            out.print("<th scope=\"col\">Tryglyceride mmol/l</th>");
//            out.print("<th scope=\"col\">TC:HDL RaRatio</th>");
//            out.print("<th scope=\"col\">Source</th>");
//            out.print("<th scope=\"col\">Notes</th>");
            out.print("</tr>");

            if (getHyperlipidaemia != null && getHyperlipidaemia.size() > 0) {
                for (HyperlipidaemiaBiometrics h : getHyperlipidaemia) {

                    out.print("<tr>");

                    if (h.getDateMeasured() != null) {
                        Date mDate = h.getDateMeasured().toGregorianCalendar().getTime();
                        dateMeasured = formatter.format(mDate);
                        out.print("<td><center><label class=\"label\">" + dateMeasured + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (h.getHeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + h.getHeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (h.getWeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + h.getWeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (h.getBloodPressureSystolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + h.getBloodPressureSystolic() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (h.getBloodPressureDiastolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + h.getBloodPressureDiastolic() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (h.getTotalCholesterol() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + h.getTotalCholesterol() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (h.getLdlcMol() != 0) {
                        out.print("<td><center><label class=\"label\">" + h.getLdlcMol() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (h.getTriglycerideMol() != 0) {
                        out.print("<td><center><label class=\"label\">" + h.getTriglycerideMol() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (h.getTcHdlRatio() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + h.getTcHdlRatio() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("<td><center><button name=\"opperation\" type=\"button\" " + javascript + " value=\"" + h.getBiometricsId() + "\">Select</button></center></td>");
//                    if (h.getDateMeasured() != null) {
//                        Date mDate = h.getDateMeasured().toGregorianCalendar().getTime();
//                        dateMeasured = formatter.format(mDate);
//                        out.print("<td>" + dateMeasured + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (h.getIcd10Lookup() != null) {
//                        out.print("<td>" + h.getIcd10Lookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (h.getExercisePerWeek() != 0) {
//                        out.print("<td>" + h.getExercisePerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }                    
//
//                    if (h.getYearsSinceStopped() > 0) {
//                        out.print("<td>" + h.getYearsSinceStopped() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (h.getAlcoholUnitsPerWeek() != 0) {
//                        out.print("<td>" + h.getAlcoholUnitsPerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (h.getTotalCholesterol() != 0) {
//                        out.print("<td>" + h.getTotalCholesterol() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (h.getLdlcMol() != 0) {
//                        out.print("<td>" + h.getLdlcMol() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (h.getTriglycerideMol() != 0) {
//                        out.print("<td>" + h.getTriglycerideMol() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (h.getTcHdlRatio() != 0) {
//                        out.print("<td>" + h.getTcHdlRatio() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (h.getSourceLookup() != null) {
//                        out.print("<td>" + h.getSourceLookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (h.getDetail() != null) {
//                        out.print("<td>" + h.getDetail() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
                    out.print("</tr>");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

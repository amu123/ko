/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker_firm.application.commands;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author princes
 */
public class BrokerFirmViewCommand extends NeoCommand {
 @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String firmAppCode = request.getParameter("firmCode");
        String firmAppName = request.getParameter("firmName");
        String brokerFirmEntityId = request.getParameter("brokerFirmEntityId");
        String firmTitle = request.getParameter("brokerFirmTile");
        String contactName = request.getParameter("brokerFirmContactPerson");
        String contactSurname = request.getParameter("brokerFirmContactSurname");
        String firmRegion = request.getParameter("firmRegion");
        request.setAttribute("firmAppCode", firmAppCode);
        request.setAttribute("firmAppName", firmAppName);
        request.setAttribute("brokerFirmEntityId", brokerFirmEntityId);
        request.setAttribute("brokerFirmTile", firmTitle);
        request.setAttribute("brokerFirmContactPerson", contactName);
        request.setAttribute("brokerFirmContactSurname", contactSurname);
        request.setAttribute("firmRegion", firmRegion);
        
        try {
            context.getRequestDispatcher("/Broker/BrokerFirm.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(BrokerFirmViewCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BrokerFirmViewCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "BrokerFirmViewCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthCPTDetails;

/**
 *
 * @author johanl
 */
public class AuthCPTListDisplay extends TagSupport {

    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            out.println("<td colspan=\"3\"><table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            List<AuthCPTDetails> cptList = (List<AuthCPTDetails>) session.getAttribute(sessionAttribute);
            out.println("<tr><th align=\"left\">CPT Code</th><th align=\"left\">Theatre Time</th><th align=\"left\">Theatre Date</th><th align=\"left\">Procedure Link</th></tr>");
            if (cptList != null) {
                for (AuthCPTDetails cpt : cptList) {

                    String procLink = NeoCommand.service.getNeoManagerBeanPort().getValueFromCodeTableForTableId(126, cpt.getProcedureLink());
                    String theatreDate = DateTimeUtils.convertXMLGregorianCalendarToDate(cpt.getTheatreDate());
                    System.out.println("cpt.getTheatreDate() " + cpt.getTheatreDate());

                    out.println("<tr>");
                    out.println("<td><label class=\"label\">" + cpt.getCptCode() + "</label></td>");
                    out.println("<td><label class=\"label\">" + cpt.getTheatreTime() + "</label></td>");
                    out.println("<td><label class=\"label\">" + theatreDate + "</label></td>");
                    out.println("<td><label class=\"label\">" + procLink + "</label></td>");
                    out.println("</tr>");
                }
            }
            out.println("</table></td>");

        } catch (java.io.IOException ex) {
            ex.printStackTrace();
        }

        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

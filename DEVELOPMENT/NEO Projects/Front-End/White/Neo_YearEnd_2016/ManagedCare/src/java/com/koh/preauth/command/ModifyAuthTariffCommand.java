/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Formatter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author johanl
 */
public class ModifyAuthTariffCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        ArrayList<AuthTariffDetails> atList = (ArrayList<AuthTariffDetails>) session.getAttribute("tariffListArray");
        try {
            PrintWriter out = response.getWriter();
            if (atList != null && atList.size() != 0) {
                String tCode = request.getParameter("tTableCode");
                String tQuan = request.getParameter("tTableQuan");
                for (AuthTariffDetails at : atList) {
                    if (tCode.equals(at.getTariffCode())) {
                        if (tQuan.equalsIgnoreCase(at.getQuantity())) {
                            Formatter bs = new Formatter();
                            Formatter format = bs.format("%10.2f",at.getAmount() );
                            String tAmountF = format.toString();
                            tAmountF = tAmountF.trim();

                            session.setAttribute("tariffDesc", at.getTariffDesc());
                            session.setAttribute("tariffCode_text", at.getTariffCode());
                            session.setAttribute("tariffQuantity", at.getQuantity());
                            session.setAttribute("tariffAmount", tAmountF);
                            System.out.println(tAmountF+ ":" +tAmountF);
                            out.print(getName() + "|" + at.getTariffCode() + "|" + at.getTariffDesc() +
                                    "|" + tAmountF + "|" + at.getQuantity());
                            atList.remove(at);
                            break;
                        }
                    }
                }
                session.setAttribute("tariffListArray", atList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ModifyAuthTariffCommand";
    }
}

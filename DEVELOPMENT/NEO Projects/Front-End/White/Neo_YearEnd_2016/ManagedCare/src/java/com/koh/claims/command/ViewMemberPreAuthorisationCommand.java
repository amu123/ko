/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverSearchCriteria;
import neo.manager.EAuthCoverDetails;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ViewMemberPreAuthorisationCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(ViewMemberPreAuthorisationCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        logger.info("Inside ViewMemberPreAuthorisationCommand");
         NeoManagerBean port = service.getNeoManagerBeanPort();
         HttpSession session = request.getSession();
        try {
             //validate user for CDL view restriction
            NeoUser user = (NeoUser) session.getAttribute("persist_user");
            String coverNumber = (String) session.getAttribute("policyHolderNumber");
            List<EAuthCoverDetails> coverList;
            CoverSearchCriteria csc = new CoverSearchCriteria();            
            csc.setCoverNumberExact(coverNumber); // Always exact
            coverList = port.eAuthCoverSearch(csc);
            for (neo.manager.SecurityResponsibility resp : user.getSecurityResponsibility()) {
                if (resp.getResponsibilityId() != 2 && resp.getResponsibilityId() != 3) {
                    for (EAuthCoverDetails eAuth : coverList) {
                        if (eAuth.getRegisteredInd() == 1) {
                            session.setAttribute("userRestriction", true);
                        }
                    }
                }
            }
            String nextJSP = "/Claims/MemberPreAuthorisationDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        }catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "ViewMemberPreAuthorisationCommand";
    }

}

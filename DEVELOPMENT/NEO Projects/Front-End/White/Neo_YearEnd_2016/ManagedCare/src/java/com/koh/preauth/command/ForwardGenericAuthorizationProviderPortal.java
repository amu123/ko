/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author josephm
 */
public class ForwardGenericAuthorizationProviderPortal extends NeoCommand {


    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
        System.out.println("Inside ForwardGenericAuthorizationProviderPortal command");

        String providerNum = request.getParameter("treatingProvider");

        String nameportal =  request.getParameter("memberNum");

        String beneficiaryNum = request.getParameter("depListValues_text");

        System.out.println("the provider nyumber is " + providerNum);


            session.setAttribute("treatingProvider", providerNum);

            session.setAttribute("memberNum", nameportal);

            session.setAttribute("depListValues_text", beneficiaryNum);

            System.out.println("The provider from session is " + session.getAttribute("treatingProvider"));

            System.out.println("The dependent number is " + beneficiaryNum);
            
      

        try {

            String nextJSP = "/PreAuth/GenericAuthorizationProviderPortal.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        }catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "ForwardGenericAuthorizationProviderPortal";
    }
}

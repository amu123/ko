/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;


//import com.agile.security.webservice.HashMap;
import java.util.Collection;
import java.util.HashMap;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author johanl
 */
public class LabelNeoUserDropDown extends TagSupport {
    private String displayName;
    private String elementName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        try {
            out.println("<td><label class=\"label\">" + displayName + "</label></td>");
            out.println("<td><select name=\"" + elementName + "\" id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");
            String selectedBox = "" + session.getAttribute(elementName);
            HashMap<Integer, String> users = (HashMap<Integer, String>)TagMethods.getNeoResoUsers();
            session.setAttribute("NeoUserMap", users);
            Collection<Integer> uName = users.keySet();
            for (Integer i : uName) {
                if (i.toString().equals(selectedBox)) {
                    out.println("<option value=\"" + i + "\" selected >" + users.get(i) + "</option>");
                } else {
                    out.println("<option value=\"" + i + "\">" + users.get(i) + "</option>");
                }
            }
            out.println("</select></td>");

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LableNeoUserDropDown tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

}

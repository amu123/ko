/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

/**
 *
 * @author sobongad
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.servlet.http.HttpSession;
import neo.manager.OutstandingDocuments;
import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.employer.command.TabUtils;
import com.koh.member.application.MemberAppMapping;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.MemberApplication;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

public class MemberOutstandingUpdateCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        System.out.println("In the member update command :)");
        update(request, response, context);
        return null;
    }

    public Object update(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        
      
        ArrayList<OutstandingDocuments> oo = (ArrayList<OutstandingDocuments>) session.getAttribute("od");
        NeoUser user = getNeoUser(request);

        MemberApplication memApp = new MemberApplication();
        memApp.setSecurityGroupId(user.getSecurityGroupId());
        memApp.setEffectiveEndDate(null);
        memApp.setEffectiveStartDate(null);
        memApp.setCreatedBy(user.getUserId());
        memApp.setCreationDate(null);
        memApp.setLastUpdateDate(null);
        memApp.setLastUpdatedBy(user.getUserId());
        memApp.setCoverNumber(request.getParameter("memberAppCoverNumber"));
        memApp.setApplicationNumber(Integer.parseInt(request.getParameter("memberAppNumber")));        

        String[] documentList = request.getParameterValues("docList");

        List checkedDocs = new ArrayList<String>();

        List<OutstandingDocuments> od = port.getOutstandingDocuments(Integer.parseInt(request.getParameter("memberAppNumber")));

        for (int i = 0; i < documentList.length; i++) {
            if (documentList[i].contains("true")) {
                checkedDocs.add(od.get(i).getOutstandingDocumentsCode());
            }
            System.out.println("Value " + i + " of list is " + documentList[i]);
        }

        for (int y = 0; y < checkedDocs.size(); y++) {
            System.out.println("Value checked " + checkedDocs.get(y));
        }
        PrintWriter out = null;
        int appNum;
        try {
            
            System.out.println("COVER NUMBER " + request.getParameter("memberAppCoverNumber"));
            System.out.println(" APP NUMBER " + request.getParameter("memberAppNumber"));
            
            int success = service.getNeoManagerBeanPort().updateOutstandingDocuments(memApp, checkedDocs);
            String appNumStr = request.getParameter("memberAppNumber");

            System.out.println("Str1 " + appNumStr);
            if (success >= 0) {
                if (appNumStr != null && !appNumStr.isEmpty()) {
                    appNum = Integer.parseInt(appNumStr);
                    MemberApplication ma = port.getMemberApplicationByApplicationNumber(appNum);

                    MemberAppMapping.setMemberApplication(request, ma);

                    request.setAttribute("MemberName", ma.getFirstName() + " " + ma.getLastName());
                    request.setAttribute("memberAppNumber", ma.getApplicationNumber());
                    request.setAttribute("memberAppCoverNumber", ma.getCoverNumber());

                    System.out.println("Saved Successfully");

                    out = response.getWriter();
                    out.println(TabUtils.buildJsonResult("OK", null, null, "\"message\":\"Documents Updated.\""));

                }
            } else {
                System.out.println("Saved Unsuccessfully");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }

        return null;
    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser) request.getSession().getAttribute("persist_user");
    }

    @Override
    public String getName() {
        return "MemberOutstandingUpdateCommand";
    }

}

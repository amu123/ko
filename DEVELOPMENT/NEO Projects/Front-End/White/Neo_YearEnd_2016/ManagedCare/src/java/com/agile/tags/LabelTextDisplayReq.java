/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author yuganp
 */
public class LabelTextDisplayReq extends TagSupport {

    private String elementName;
    private String displayName;
    private String javaScript;
    private String boldDisplay = "no";

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            if(boldDisplay.equalsIgnoreCase("yes")){
                out.println("<td align=\"left\" width=\"160px\"><label class=\"labelTextDisplay\">" + displayName + ":</label></td>");
            }else{
                out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td>");
            }

            out.println("<td align=\"left\" width=\"200px\"><label id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");

            String sessionVal = "" + req.getAttribute(elementName);
            if (sessionVal != null && !sessionVal.trim().equalsIgnoreCase("null")) {
                out.println(sessionVal);
            }
            out.println("</label></td>");



        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelTextDisplayReq tag", ex);
        }

        return super.doEndTag();
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setBoldDisplay(String boldDisplay) {
        this.boldDisplay = boldDisplay;
    }

}

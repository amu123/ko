/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Johan-NB
 */
public class RemoveCompleteBasketFromList extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter out = null;
        HttpSession session = request.getSession();
        try {
            out = response.getWriter();

            session.setAttribute("AuthBasketTariffs", null);
            session.setAttribute("AuthNappiTariffs", null);

            out.println("Done|");

        } catch (IOException io) {
            io.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "RemoveCompleteBasketFromList";
    }
}

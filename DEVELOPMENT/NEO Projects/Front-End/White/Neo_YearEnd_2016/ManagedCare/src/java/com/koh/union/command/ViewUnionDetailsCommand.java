/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.union.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.Union;

/**
 *
 * @author shimanem
 */
public class ViewUnionDetailsCommand extends NeoCommand {

    private static final Logger LOG = Logger.getLogger(ViewUnionDetailsCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        LOG.info(">> ***** Union Details Command ***** <<");
        try {
            NeoManagerBean port = service.getNeoManagerBeanPort();
            HttpSession session = request.getSession();
            String unionNumber = (String) session.getAttribute("unionNumber");
            String unionName = (String) session.getAttribute("unionName");
            String representativeNumber = (String) session.getAttribute("representativeNumber");

            LOG.log(Level.INFO, ">> *** Union Number : {0}", unionNumber);
            LOG.log(Level.INFO, ">> *** Union Name : {0}", unionName);

            Union union = port.retrieveUnion(Integer.parseInt(representativeNumber));
            if (union != null) {
                LOG.log(Level.INFO, ">> UNION : {0}", union.toString());
                session.setAttribute("unionDetails", union);
            }

            String nextJSP = "/Union/unionDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(ViewUnionDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ViewUnionDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewUnionDetailsCommand";
    }

}

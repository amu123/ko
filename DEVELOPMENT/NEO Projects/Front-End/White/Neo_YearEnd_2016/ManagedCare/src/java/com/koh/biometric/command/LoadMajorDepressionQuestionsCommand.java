/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.Biometrics;
import neo.manager.MajorDepressionBiometrics;

/**
 *
 * @author josephm
 */
public class LoadMajorDepressionQuestionsCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        String coverNumber = request.getParameter("memberNumber");
        int  code = Integer.parseInt(request.getParameter("depListValues"));

        MajorDepressionBiometrics depression = new MajorDepressionBiometrics();

        Biometrics biometrics = service.getNeoManagerBeanPort().fetchBiometricsTypeId(depression, coverNumber, code);

        try {
            PrintWriter out = response.getWriter();

            if(biometrics != null && biometrics.getWeight() != 0.0) {

               out.print(getName() + "|Weight=" + biometrics.getWeight());
               out.print("|Exercise=" + biometrics.getExercisePerWeek());
               out.print("|Length=" + biometrics.getHeight());
               out.print("|BMI=" + biometrics.getBmi());
               out.print("|BloodPressureSystolic=" + biometrics.getBloodPressureSystolic());
               out.print("|BloodPressuredDiastolic=" + biometrics.getBloodPressureDiastolic());
               out.print("|AlcoholConsumption=" + biometrics.getAlcoholUnitsPerWeek() + "$");

            }else {

                out.print("Error|No such biometrics|");
            }


        }catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "LoadMajorDepressionQuestionsCommand";
    }
}

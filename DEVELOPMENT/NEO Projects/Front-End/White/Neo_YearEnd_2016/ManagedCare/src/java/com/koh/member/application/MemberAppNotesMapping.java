/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application;

import com.koh.employer.command.TabUtils;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class MemberAppNotesMapping {

    private static final String[][] FIELD_MAPPINGS = {
        {"memberAppNumber","Application Number","no", "int", "MemberApp","ApplicationNumber"},
        {"notesListSelect","Note Type","no", "int", "MemberApp","noteType"},
        {"MemberAppNotesAdd_details","Details","yes", "str", "MemberApp","noteDetails"}
    };
    
    private static final Map<String,String[]> FIELD_MAP;

    static {
        HashMap hm = new HashMap<String,String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }

    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
        return errors;
    }

    public static MemberAppNotes getMemberAppNotes(HttpServletRequest request,NeoUser neoUser) {
        MemberAppNotes ma = (MemberAppNotes)TabUtils.getDTOFromRequest(request, MemberAppNotes.class, FIELD_MAPPINGS, "MemberApp");
        ma.setSecurityGroupId(neoUser.getSecurityGroupId());
        ma.setLastUpdatedBy(neoUser.getUserId());
        ma.setCreatedBy(neoUser.getUserId());
        return ma;
    }

    public static void setMemberAppNotes(HttpServletRequest request, MemberAppNotes ma) {
        TabUtils.setRequestFromDTO(request, ma, FIELD_MAPPINGS, "MemberApp");
    }
    
}

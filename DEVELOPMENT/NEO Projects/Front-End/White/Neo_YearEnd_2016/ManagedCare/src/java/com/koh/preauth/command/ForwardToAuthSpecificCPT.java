/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author johanl
 */
public class ForwardToAuthSpecificCPT extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();

        String onScreen = "" + session.getAttribute("onScreen");
        String nextJSP = "/PreAuth/AuthSpecificCPT.jsp";
        String authType = "" + session.getAttribute("authType");
        String provTypeId = "";

        if (authType.trim().equals("11") || authType.trim().equals("4")) {
            provTypeId = "" + session.getAttribute("facilityProvDiscTypeId");
            if (provTypeId != null && !provTypeId.equalsIgnoreCase("null") && !provTypeId.equalsIgnoreCase("")) {
                session.setAttribute("facilityProv_error", null);
            } else {
                session.setAttribute("facilityProv_error", "Facility Provider Required");
                nextJSP = onScreen;
            }
        }

        try {            
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ForwardToAuthSpecificCPT";
    }
}

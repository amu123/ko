/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.brokerFirm;

import com.koh.cover.commands.MemberUnderwritingCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import neo.manager.*;

/**
 *
 * @author princes
 */
public class BrokerFirmMainMapping {

    private static final String[][] FIELD_MAPPINGS = {
        {"brokerFirmEntityId", "", "no", "int", "FirmApp", "entityId"},
        {"FirmApp_firmCode", "Broker Firm Code", "no", "str", "FirmApp", "brokerCode"},
        {"FirmApp_alternativeCode", "Alternative Code", "no", "str", "FirmApp", "brokerAltCode", "0", "15"},
        {"FirmApp_tradingName", "Trading Name", "no", "str", "FirmApp", "tradingName", "0", "50"},
        {"FirmApp_vipBroker", "VIP Broker", "no", "int", "FirmApp", "vipBrokerInd"},
        {"ConcesType", "Concession", "no", "str", "FirmApp", "concessionFlag"},
        {"FirmApp_ConcessionStart", "Concession Start", "no", "date", "FirmApp", "startConcession"},
        {"FirmApp_ConcessionEnd", "Concession End", "no", "date", "FirmApp", "endConcession"},
        {"FirmApp_registeredVat", "Registered For Vat", "no", "int", "FirmApp", "registeredForVatInd"},
        {"FirmApp_registeredCompanyNumber", "Company Registration Number", "no", "str", "FirmApp", "companyRegistrationNo", "0", "30"},
        {"FirmApp_registeredVatNumber", "Vat Registration Number", "no", "str", "FirmApp", "vatRegistrationNo", "0", "30"},
        {"FirmApp_CommisionInd", "Commission", "yes", "int", "FirmApp", "commisionInd"},
        {"FirmApp_contractStatus", "Contract Status", "yes", "int", "FirmApp", "contractStatus"},
        {"FirmApp_contractStartDate", "Contract Start Date", "no", "date", "FirmApp", "contractStartDate"},
        {"FirmApp_contractEndDate", "Contract End Date", "no", "date", "FirmApp", "contractEndDate"},
        {"FirmApp_title", "Title", "yes", "int", "FirmApp", "contactTitle"},
        {"FirmApp_firstName", "First Name", "no", "str", "FirmApp", "contactFirstName", "0", "50"},
        {"FirmApp_surname", "Surname", "no", "str", "FirmApp", "contactLastName", "0", "50"},
        {"FirmApp_firmName", "Broker Firm Name", "yes", "str", "FirmApp", "brokerFirmName", "0", "50"},
        {"FirmApp_firmCode", "Broker Firm Code", "yes", "str", "FirmApp", "brokerFirmCode", "0", "15"},
        {"FirmApp_ContactDetails_title", "Title", "no", "int", "ContactDetails", "ContactPersonTitle"},
        {"FirmApp_region", "Region", "no", "int", "FirmApp", "region"},
        {"FirmApp_ContactDetails_contactPerson", "Contact Person", "no", "str", "ContactDetails", "ContactPerson", "0", "50"}
    };
    private static final String[][] CONCESSION_FIELD_MAPPINGS = {
        {"ConcesType", "Concession", "yes", "str", "ConcessionFirmApp", "concessionFlag"},
        {"ConcessionStart", "Concession Start", "yes", "date", "ConcessionFirmApp", "concessionStartDate"},
        {"ConcessionEnd", "Concession End", "yes", "date", "ConcessionFirmApp", "concessionEndDate"}
    };
    private static final String[][] BANK_MAPPINGS = {
        {"FirmBankApp_bank", "Name of Bank", "yes", "int", "FirmBank", ""},
        {"FirmBankApp_branch", "Branch", "yes", "int", "FirmBank", ""},
        {"FirmBankApp_accountType", "Account Type", "yes", "int", "FirmBank", ""},
        {"FirmBankApp_accountHolder", "Account Holder", "yes", "str", "FirmBank", ""},
        {"FirmBankApp_accountNo", "Account no.", "yes", "numberField", "FirmBank", ""}
    };
    private static final String[][] ACCREDITATION_MAPPINGS = {
        {"FrimApp_brokerAccreditationType", "Accreditation Type", "yes", "int", "Accreditation", "accreditationType"},
        {"FirmrApp_brokerAccreditationNumber", "Accreditation Number", "yes", "str", "Accreditation", "accreditationNumber"},
        {"FirmApp_brokerApplicationDate", "Application Date", "no", "date", "Accreditation", "applicationDate"},
        {"FirmApp_brokerAuthorisationDate", "Authorisation Date", "no", "date", "Accreditation", "authorisationDate"},
        {"FirmApp_brokerStartDate", "Start Date", "yes", "date", "Accreditation", "effectiveStartDate"},
        {"FirmApp_brokerEndDate", "End Date", "yes", "date", "Accreditation", "effectiveEndDate"}
    };
    private static final String[][] CONTACT_MAPPINGS = {
        {"FirmApp_ContactDetails_telNo", "Telephone Number", "no", "str", "ContactDetails", ""},
        {"FirmApp_ContactDetails_faxNo", "Fax No", "no", "str", "ContactDetails", ""},
        {"FirmApp_ContactDetails_email", "Email Address", "no", "str", "ContactDetails", ""},
        {"FirmApp_ContactDetails_altEmail", "Alternative Email Address", "no", "str", "ContactDetails", ""},
        {"FirmApp_PostalAddress_addressLine1", "Line 1", "no", "str", "AddressDetails", ""},
        {"FirmApp_PostalAddress_addressLine2", "Line 2", "no", "str", "AddressDetails", ""},
        {"FirmApp_PostalAddress_addressLine3", "Line 3", "no", "str", "AddressDetails", ""},
        {"FirmApp_PostalAddress_postalCode", "Postal Code", "no", "str", "AddressDetails", ""},
        {"FirmApp_ResAddress_addressLine1", "Line 1", "no", "str", "AddressDetails", ""},
        {"FirmApp_ResAddress_addressLine2", "Line 2", "no", "str", "AddressDetails", ""},
        {"FirmApp_ResAddress_addressLine3", "Line 3", "no", "str", "AddressDetails", ""},
        {"FirmApp_ResAddress_postalCode", "Physical Code", "no", "str", "AddressDetails", ""}
    };
    private static final Map<String, String[]> FIELD_MAP;
    private static final String[][] NOTES_MAPPINGS = {
        {"brokerFirmEntityId", "", "no", "int", "FirmApp", "entityId"},
        {"notesListSelect", "Note Type", "no", "int", "FirmApp", "noteType"},
        {"BrokerFirmAppNotesAdd_details", "Details", "no", "str", "FirmApp", "noteDetails"}
    };

    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }

    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
        if (request.getParameter("ConcesType").equalsIgnoreCase("1")) {

            if (request.getParameter("FirmApp_ConcessionStart").equalsIgnoreCase("")
                    || request.getParameter("FirmApp_ConcessionStart") == null
                    || request.getParameter("FirmApp_ConcessionStart").equalsIgnoreCase("null")) {

                errors.put("FirmApp_ConcessionStart_error", "Please enter the Start Date.");

            }
            
            
              if ((!request.getParameter("FirmApp_ConcessionStart").equalsIgnoreCase(""))
                    || (request.getParameter("FirmApp_ConcessionStart") != null)
                    || (!request.getParameter("FirmApp_ConcessionStart").equalsIgnoreCase("null"))) {


               

   

 try {
                    String firmAppConcessionStartDate = request.getParameter("FirmApp_ConcessionStart").toString();
                    if(firmAppConcessionStartDate!=null && !firmAppConcessionStartDate.isEmpty()){
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                        formatter.setLenient(false);
                        Date startdate = formatter.parse(firmAppConcessionStartDate);
                    }
                    //Date enddate = formatter.parse(endingDate);

                    


       } catch (ParseException e) {

                    errors.put("FirmApp_ConcessionStart_Error", "Incorrect Format");
                }

              

            }

        
         
//               if ((!request.getParameter("FirmApp_ConcessionEnd").toString().equalsIgnoreCase(""))
//                    || (request.getParameter("FirmApp_ConcessionEnd").toString() != null)
//                    || (!request.getParameter("FirmApp_ConcessionEnd").toString().equalsIgnoreCase("null"))) {

if(request.getParameter("FirmApp_ConcessionEnd").toString() !=null && request.getParameter("FirmApp_ConcessionEnd").toString().length() > 0){
               

    System.out.println("testttt" + request.getParameter("FirmApp_ConcessionEnd").toString());
     System.out.println("Classs" + request.getParameter("FirmApp_ConcessionEnd").getClass());

 try {
                    String firmAppConcessionStartDate = request.getParameter("FirmApp_ConcessionStart").toString();
                    String firmAppConcessionEndDate = request.getParameter("FirmApp_ConcessionEnd").toString();
                    if(firmAppConcessionStartDate!=null && !firmAppConcessionStartDate.isEmpty() && firmAppConcessionEndDate!=null && !firmAppConcessionEndDate.isEmpty()){
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                        formatter.setLenient(false);
                        Date startdate = formatter.parse(firmAppConcessionStartDate);
                        Date endingDate = formatter.parse(firmAppConcessionEndDate);
                        //Date enddate = formatter.parse(endingDate);

                        if (startdate.after(endingDate)) {

                            errors.put("FirmApp_ConcessionEnd_error", "End Date Must Be Greater Than Start Date");


                        }
                        
                    }


       } catch (ParseException e) {

                    errors.put("FirmApp_ConcessionEnd_Error", "Incorrect Format");
                }

              

            }

        

        }
        else {
            
            
            
            
              if ((!request.getParameter("FirmApp_ConcessionStart").equalsIgnoreCase(""))
                    || (request.getParameter("FirmApp_ConcessionStart") != null)
                    || (!request.getParameter("FirmApp_ConcessionStart").equalsIgnoreCase("null"))) {


               

   

 try {
                    String firmAppConcessionStartDate = request.getParameter("FirmApp_ConcessionStart").toString();
                    if(firmAppConcessionStartDate!=null  && !firmAppConcessionStartDate.isEmpty()){
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                        formatter.setLenient(false);
                        Date startdate = formatter.parse(firmAppConcessionStartDate);
                    }                  
                    //Date enddate = formatter.parse(endingDate);

                    


       } catch (ParseException e) {

                    errors.put("FirmApp_ConcessionStart_Error", "Incorrect Format");
                }

              

            }

        
         
//               if ((!request.getParameter("FirmApp_ConcessionEnd").toString().equalsIgnoreCase(""))
//                    || (request.getParameter("FirmApp_ConcessionEnd").toString() != null)
//                    || (!request.getParameter("FirmApp_ConcessionEnd").toString().equalsIgnoreCase("null"))) {

if(request.getParameter("FirmApp_ConcessionEnd").toString() !=null && request.getParameter("FirmApp_ConcessionEnd").toString().length() > 0){
               

    System.out.println("testttt" + request.getParameter("FirmApp_ConcessionEnd").toString());
     System.out.println("Classs" + request.getParameter("FirmApp_ConcessionEnd").getClass());

 try {              
                    String firmAppConcessionStartDate = request.getParameter("FirmApp_ConcessionStart").toString();
                    String firmAppConcessionEndDate = request.getParameter("FirmApp_ConcessionEnd").toString();
                    if(firmAppConcessionStartDate!=null && firmAppConcessionEndDate!=null && !firmAppConcessionStartDate.isEmpty() && !firmAppConcessionEndDate.isEmpty()){
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                        formatter.setLenient(false);
                        Date startdate = formatter.parse(firmAppConcessionStartDate);
                        Date endingDate = formatter.parse(firmAppConcessionEndDate);
                        //Date enddate = formatter.parse(endingDate);

                        if (startdate.after(endingDate)) {

                            errors.put("FirmApp_ConcessionEnd_error", "End Date Must Be Greater Than Start Date");


                        }
                    }


       } catch (ParseException e) {

                    errors.put("FirmApp_ConcessionEnd_Error", "Incorrect Format");
                }

              

            }

        

         
            
        }
        
        
        return errors;
    }

    public static Map<String, String> validateNotes(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, NOTES_MAPPINGS);
        return errors;
    }

    public static Map<String, String> validateBank(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, BANK_MAPPINGS);
        return errors;
    }

    public static Map<String, String> validateAccreditation(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, ACCREDITATION_MAPPINGS);
        return errors;
    }

    public static Map<String, String> validateContact(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, CONTACT_MAPPINGS);
        return errors;
    }

    public static BrokerFirm getFirmApplication(HttpServletRequest request, NeoUser neoUser) {
        BrokerFirm fm = (BrokerFirm) TabUtils.getDTOFromRequest(request, BrokerFirm.class, FIELD_MAPPINGS, "FirmApp");
        fm.setSecurityGroupId(neoUser.getSecurityGroupId());
        fm.setLastUpdatedBy(neoUser.getUserId());
        return fm;
    }

    public static BrokerFirmConcessionDetails getConcessionFirmApplication(HttpServletRequest request, NeoUser neoUser) {
        BrokerFirmConcessionDetails ba = (BrokerFirmConcessionDetails) TabUtils.getDTOFromRequest(request, BrokerFirmConcessionDetails.class, FIELD_MAPPINGS, "FirmApp");
        neoUser = (NeoUser) request.getSession().getAttribute("persist_user");

        String strStartDate = request.getParameter("FirmApp_ConcessionStart");
        String strEndDate = request.getParameter("FirmApp_ConcessionEnd");
     
        Integer concessionTypeValue = new Integer(request.getParameter("ConcesType"));

        String brokerCode = request.getParameter("FirmApp_firmCode");

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date startDate = null;
        Date endDates = null;
        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");

        try {

            if (concessionTypeValue == 1) {

                startDate = !strStartDate.isEmpty() ? dateFormat.parse(strStartDate) : null;
                endDates = !strEndDate.isEmpty() ? dateFormat.parse(strEndDate) : endDate;


            } else {

                startDate = !strStartDate.isEmpty() ? dateFormat.parse(strStartDate) : null;
                endDates = !strEndDate.isEmpty() ? dateFormat.parse(strEndDate) : null;


            }
          
        } catch (ParseException ex) {
            Logger.getLogger(BrokerFirmMainMapping.class.getName()).log(Level.SEVERE, null, ex);
        }



        try {

            ba.setBrokerFirmCode(brokerCode);
            ba.setConcessionFlag(String.valueOf(concessionTypeValue));
            ba.setConcessionStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
            ba.setConcessionEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDates));
            // ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));


            return ba;



        } catch (java.lang.Exception e) {
            System.out.println("Printing Exception  : " + e);
        }




        return ba;

    }

    public static void setFirmApplication(HttpServletRequest request, BrokerFirm firmApplication) {
        TabUtils.setRequestFromDTO(request, firmApplication, FIELD_MAPPINGS, "FirmApp");
    }

    public static void setConcessionFirmApplication(HttpServletRequest request, BrokerFirmConcessionDetails firmApplication) {
        TabUtils.setRequestFromDTO(request, firmApplication, FIELD_MAPPINGS, "FirmApp");
    }

    public neo.manager.NeoUser getNeoUser(HttpServletRequest request) {
        return (neo.manager.NeoUser) request.getSession().getAttribute("persist_user");
    }

    public static List<AddressDetails> getAddressDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<AddressDetails> cdl = new ArrayList<AddressDetails>();
        cdl.add(getAddressDetail(request, "FirmApp_PostalAddress_", neoUser, entityId, 2));
        cdl.add(getAddressDetail(request, "FirmApp_ResAddress_", neoUser, entityId, 1));
        return cdl;
    }

    private static AddressDetails getAddressDetail(HttpServletRequest request, String param, NeoUser neoUser, int entityId, int addrType) {
        AddressDetails ad = new AddressDetails();
        ad.setAddressTypeId(addrType);
        ad.setPrimaryInd("Y");
        ad.setAddressUse(1);
        ad.setAddressLine1(request.getParameter(param + "addressLine1"));
        ad.setAddressLine2(request.getParameter(param + "addressLine2"));
        ad.setAddressLine3(request.getParameter(param + "addressLine3"));
        ad.setPostalCode(request.getParameter(param + "postalCode"));
        //ad.setEntityId(entityId);
        //ad.setSecurityGroupId(neoUser.getSecurityGroupId());
        //ad.setLastUpdatedBy(neoUser.getUserId());
        return ad;
    }

    public static void setBrokerFirm(HttpServletRequest request, BrokerFirm brokerFirm) {
        TabUtils.setRequestFromDTO(request, brokerFirm, FIELD_MAPPINGS, "FirmApp");
    }

    public static void setAddressDetails(HttpServletRequest request, Collection<AddressDetails> addressDetails) {
        if (addressDetails == null) {
            return;
        }
        for (AddressDetails ad : addressDetails) {
            if (ad == null) {
                continue;
            }
            String fieldStart = ad.getAddressTypeId() == 1 ? "FirmApp_ResAddress_" : "FirmApp_PostalAddress_";
            request.setAttribute(fieldStart + "addressLine1", ad.getAddressLine1());
            request.setAttribute(fieldStart + "addressLine2", ad.getAddressLine2());
            request.setAttribute(fieldStart + "addressLine3", ad.getAddressLine3());
            request.setAttribute(fieldStart + "postalCode", ad.getPostalCode());

        }
    }

    public static List<ContactDetails> getContactDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<ContactDetails> cdl = new ArrayList<ContactDetails>();
        cdl.add(getContactDetail(request, "FirmApp_ContactDetails_telNo", neoUser, entityId, 4));
        cdl.add(getContactDetail(request, "FirmApp_ContactDetails_faxNo", neoUser, entityId, 2));
        cdl.add(getContactDetail(request, "FirmApp_ContactDetails_cell", neoUser, entityId, 3));
        cdl.add(getContactDetail(request, "FirmApp_ContactDetails_email", neoUser, entityId, 1));
        cdl.add(getContactDetail(request, "FirmApp_ContactDetails_altEmail", neoUser, entityId, 8));
        return cdl;
    }

    private static ContactDetails getContactDetail(HttpServletRequest request, String param, NeoUser neoUser, int entityId, int commType) {
        ContactDetails cd = new ContactDetails();
        cd.setCommunicationMethodId(commType);
        cd.setMethodDetails(request.getParameter(param));
        cd.setPrimaryIndicationId(1);
        //cd.setEntityId(entityId);
        //cd.setSecurityGroupId(neoUser.getSecurityGroupId());
        //cd.setLastUpdatedBy(neoUser.getUserId());
        return cd;
    }

    public static void setContactDetails(HttpServletRequest request, Collection<ContactDetails> contactDetails) {
        if (contactDetails == null) {
            return;
        }
        request.setAttribute("FirmBankApp_ContantRegion", request.getAttribute("firmRegion"));
        request.setAttribute("FirmApp_ContactDetails_title", request.getAttribute("brokerFirmTile"));
        request.setAttribute("FirmApp_ContactDetails_contactPerson", request.getAttribute("brokerFirmContactPerson") + " " + request.getAttribute("brokerFirmContactSurname"));
        System.out.println("Contact Name and Surname " + request.getAttribute("brokerFirmContactPerson") + " " + request.getAttribute("brokerFirmContactSurname"));
        for (ContactDetails cd : contactDetails) {
            if (cd == null) {
                continue;
            }
            String fieldStart = "";
            fieldStart = cd.getCommunicationMethodId() == 4 ? "FirmApp_ContactDetails_telNo" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 2 ? "FirmApp_ContactDetails_faxNo" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 3 ? "FirmApp_ContactDetails_cell" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 1 ? "FirmApp_ContactDetails_email" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 8 ? "FirmApp_ContactDetails_altEmail" : fieldStart;
            request.setAttribute(fieldStart, cd.getMethodDetails() == null ? "" : cd.getMethodDetails());
        }
    }

    public static EmployerBilling getEmployerBilling(HttpServletRequest request, NeoUser neoUser) {
        EmployerBilling eg = (EmployerBilling) TabUtils.getDTOFromRequest(request, EmployerBilling.class, FIELD_MAPPINGS, "FirmBank");
        eg.setSecurityGroupId(neoUser.getSecurityGroupId());
        eg.setLastUpdatedBy(neoUser.getUserId());
        return eg;
    }

    public static void setEmployerBilling(HttpServletRequest request, EmployerBilling employerBilling) {
        TabUtils.setRequestFromDTO(request, employerBilling, FIELD_MAPPINGS, "FirmBank");
    }

    public static List<BankingDetails> getBankingDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<BankingDetails> cdl = new ArrayList<BankingDetails>();
        cdl.add(getBankingDetails(request, "FirmBankApp_", 1));
        //cdl.add(getBankingDetails(request, "BankingDetails_pay_", 2));
        return cdl;
    }

    private static BankingDetails getBankingDetails(HttpServletRequest request, String param, int accUse) {
        BankingDetails bd = new BankingDetails();
        String paymentMethod = accUse == 1 ? request.getParameter(param + "method") : "B";
        bd.setPaymentMethodId(paymentMethod);
        bd.setAccountUseId(accUse);
        bd.setBankId(Integer.parseInt(request.getParameter(param + "bank")));
        bd.setBranchId(Integer.parseInt(request.getParameter(param + "branch")));
        bd.setAccountTypeId(request.getParameter(param + "accountType"));
        bd.setAccountName(request.getParameter(param + "accountHolder"));
        bd.setAccountNo(request.getParameter(param + "accountNo"));

        return bd;
    }

    public static void setBankingDetails(HttpServletRequest request, Collection<BankingDetails> bankingDetails) {
        if (bankingDetails == null) {
            return;
        }
        for (BankingDetails bd : bankingDetails) {
            if (bd == null) {
                continue;
            }
            String fieldStart = "FirmBankApp_";

            request.setAttribute(fieldStart + "bank", bd.getBankId());
            request.setAttribute(fieldStart + "branch", bd.getBranchId());
            request.setAttribute(fieldStart + "accountType", bd.getAccountTypeId());
            request.setAttribute(fieldStart + "accountHolder", bd.getAccountName());
            request.setAttribute(fieldStart + "accountNo", bd.getAccountNo());
        }
    }

    public static int getEntityId(HttpServletRequest request) {
        return Integer.parseInt(request.getParameter("brokerFirmEntityId"));
    }

    /*
     * public static List<BrokerAccred>
     * getAccreditationDetails(HttpServletRequest request, NeoUser neoUser, int
     * entityId) { List<BrokerAccred> cdl = new ArrayList<BrokerAccred>();
     * cdl.add(getAccreditationDetails(request, entityId, neoUser));
     * //cdl.add(getBankingDetails(request, "BankingDetails_pay_", 2)); return
     * cdl; }
     */
    public static BrokerAccred getAccreditationDetails(HttpServletRequest request, int entityId, NeoUser neoUser, int brokerAccredID) {
        BrokerAccred ba = new BrokerAccred();
        Date today = new Date();
        ba.setBrokerAccredId(brokerAccredID);
        ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setEntityId(entityId);
        ba.setCreatedBy(neoUser.getUserId());
        ba.setLastUpdatedBy(neoUser.getUserId());
        ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setSecurityGroupId(2);
        ba.setAccreditationType(Integer.parseInt(request.getParameter("FrimApp_brokerAccreditationType")));
        ba.setAccreditationNumber(request.getParameter("FirmrApp_brokerAccreditationNumber"));

        if (request.getParameter("FirmApp_brokerApplicationDate") != null && !request.getParameter("FirmApp_brokerApplicationDate").isEmpty()) {
            Date appDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("FirmApp_brokerApplicationDate"));
            ba.setApplicationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(appDate));
        }

        if (request.getParameter("FirmApp_brokerAuthorisationDate") != null && !request.getParameter("FirmApp_brokerAuthorisationDate").isEmpty()) {
            Date authDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("FirmApp_brokerAuthorisationDate"));
            ba.setAuthorisationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(authDate));
        }

        if (request.getParameter("FirmApp_brokerStartDate") != null) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("FirmApp_brokerStartDate"));
            ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        }
        if (request.getParameter("FirmApp_brokerEndDate") != null) {
            Date endDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("FirmApp_brokerEndDate"));
            ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        }

        return ba;
    }
    
    public static BrokerAccred getAccreditationDetailsForRemove(HttpServletRequest request, int entityId, NeoUser neoUser, int brokerAccredID) {
        BrokerAccred ba = new BrokerAccred();
        Date today = new Date();
        ba.setBrokerAccredId(brokerAccredID);
        ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setEntityId(entityId);
        ba.setCreatedBy(neoUser.getUserId());
        ba.setLastUpdatedBy(neoUser.getUserId());
        ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setSecurityGroupId(2);
        
        int brokerAccreditationType = 0;
        if(request.getAttribute("FrimApp_brokerAccreditationType_").equals("1")){
            brokerAccreditationType = 1;
        }else if(request.getAttribute("FrimApp_brokerAccreditationType_").equals("2")){
            brokerAccreditationType = 2;
        }
        ba.setAccreditationType(brokerAccreditationType);
        ba.setAccreditationNumber(request.getAttribute("FirmrApp_brokerAccreditationNumber_").toString());

        if (request.getAttribute("FirmApp_brokerApplicationDate_") != null && !request.getAttribute("FirmApp_brokerApplicationDate_").toString().isEmpty()) {
            Date appDate = DateTimeUtils.convertFromYYYYMMDD(request.getAttribute("FirmApp_brokerApplicationDate_").toString());
            ba.setApplicationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(appDate));
        }

        if (request.getAttribute("FirmApp_brokerAuthorisationDate_") != null && !request.getAttribute("FirmApp_brokerAuthorisationDate_").toString().isEmpty()) {
            Date authDate = DateTimeUtils.convertFromYYYYMMDD(request.getAttribute("FirmApp_brokerAuthorisationDate_").toString());
            ba.setAuthorisationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(authDate));
        }

        if (request.getAttribute("FirmApp_brokerStartDate_") != null) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getAttribute("FirmApp_brokerStartDate_").toString());
            ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        }
        if (request.getAttribute("FirmApp_brokerEndDate_") != null) {
            Date endDate = DateTimeUtils.convertFromYYYYMMDD(request.getAttribute("FirmApp_brokerEndDate_").toString());
            ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        }

        return ba;
    }

    public static EntityNotes getBrokerFirmAppNotes(HttpServletRequest request, NeoUser neoUser) {
        EntityNotes ma = (EntityNotes) TabUtils.getDTOFromRequest(request, EntityNotes.class, NOTES_MAPPINGS, "FirmApp");
        Date today = new Date();
        ma.setNoteDetails(request.getParameter("BrokerFirmAppNotesAdd_details"));
        ma.setEntityId(new Integer(request.getParameter("brokerFirmEntityId")));
        ma.setNoteType(new Integer(request.getParameter("notesListSelect")));
        ma.setSecurityGroupId(neoUser.getSecurityGroupId());
        ma.setLastUpdatedBy(neoUser.getUserId());
        ma.setCreatedBy(neoUser.getUserId());
        ma.setNoteDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ma.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ma.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
        ma.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        ma.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        return ma;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.statement.command;

import com.koh.command.FECommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author josephm
 */
public class ReloadTaxCertificateCommand extends FECommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        System.out.println("Inside ReloadTaxCertificateCommand");
        this.clearScreenFromSession(request);

        try {

            String nextJSP = "/Statement/TaxCertificate.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        }catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "ReloadTaxCertificateCommand";
    }

}

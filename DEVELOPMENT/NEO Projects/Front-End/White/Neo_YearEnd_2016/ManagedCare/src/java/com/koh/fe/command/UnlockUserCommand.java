/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoUser;
import com.koh.command.NeoCommand;

/**
 *
 * @author johanl
 */
public class UnlockUserCommand extends NeoCommand {
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoUser user = (NeoUser)session.getAttribute("persist_user");
        Integer userId = (Integer) session.getAttribute("userId");
        int success = service.getNeoManagerBeanPort().unlockUser(user, userId);
        
        System.out.println("successfully unlocked user("+userId+") = "+success);
        
        try {
        
            PrintWriter out = response.getWriter();

            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            out.println("<td><label class=\"header\"><h2>User account has been successfully unlocked.</h2></label></td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "2; URL=/ManagedCare/SystemAdmin/UpdateUserProfile.jsp");
        }catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public String getName() {
        return "UnlockUserCommand";
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.employer.command;

import com.koh.command.NeoCommand;
import com.koh.utils.MapUtils;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.EmployerGroup;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author yuganp
 */
public class EmployerSearchCommand extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        
        try {
            String product = request.getParameter("scheme");
            String name = request.getParameter("employerName");
            String number = request.getParameter("employerNumber"); 
            List<KeyValueArray> kva = port.getEmployerList(name, number, Integer.parseInt(product.trim()));
            Object col = MapUtils.getMap(kva);
            request.setAttribute("EmployerSearchResults", col);
            context.getRequestDispatcher("/Employer/EmployerSearchResults.jsp").forward(request, response);
            session.setAttribute("productID", Integer.parseInt(product.trim()));
        } catch (Exception ex) {
            Logger.getLogger(EmployerSearchCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "EmployerSearchCommand";
    }

}

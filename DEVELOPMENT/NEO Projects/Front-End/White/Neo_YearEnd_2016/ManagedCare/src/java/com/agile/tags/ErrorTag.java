/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class ErrorTag extends TagSupport {

    @Override
    public int doEndTag() throws JspException {
        HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
        try{
            String wfe = "" + request.getAttribute("error");
            if(!wfe.equalsIgnoreCase("") && !wfe.equalsIgnoreCase("null")){
                pageContext.getOut().write("<label class=\"error\">"+request.getAttribute("error")+"</label>");
            }
//            if(request.getAttribute("error") != null){
//                if(request.getAttribute("error").equals("null")){
                    
//                }
//                pageContext.getOut().write("<label class=\"error\">"+request.getAttribute("error")+"</label>");
//            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return super.doEndTag();
    }



    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
//    @Override
//    public void doTag() throws JspException {
//        JspWriter out = getJspContext().getOut();
//
//        try {
//            HttpServletRequest request =
//            out.println("<label class=\"error\">"+getJspContext().getAttribute("error")+"</label>");
//             JspFragment f = getJspBody();
//            if (f != null) {
//                f.invoke(out);
//            }
//
//
//        } catch (java.io.IOException ex) {
//            throw new JspException("Error in ErrorTag tag", ex);
//        }
//    }

}

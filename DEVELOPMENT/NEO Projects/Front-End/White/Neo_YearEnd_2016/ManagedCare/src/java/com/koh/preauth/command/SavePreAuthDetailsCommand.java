/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.util.logging.Level;
import java.util.logging.Logger;
import neo.manager.LabTariffDetails;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthCPTDetails;
import neo.manager.AuthHospitalLOC;
import neo.manager.AuthNappiProduct;
import neo.manager.AuthNoteDetail;
import neo.manager.AuthPMBDetails;
import neo.manager.AuthSpecificBasket;
import neo.manager.AuthSpecificBasketTariffs;
import neo.manager.AuthTariffDetails;
import neo.manager.CoverDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.OrthodonticPlanDetails;
import neo.manager.PreAuthDetails;
import neo.manager.WorkflowItem;
import org.apache.commons.lang.time.DateFormatUtils;
import com.agile.command.GetCoverDetailsByNumber;

//import neo.manager.AuthHistoryDetails;
/**
 *
 * @author gerritj
 */
public class SavePreAuthDetailsCommand extends NeoCommand {

    private String commandName;
    private String javascript;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        PreAuthDetails pa = new PreAuthDetails();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        pa.setUserId(user.getUserId());
        //Generic Fields
        String schemeName = "" + session.getAttribute("schemeName");
        String schemeOptionName = "" + session.getAttribute("schemeOptionName");

        String authType = "" + session.getAttribute("authType");
        System.out.println("Auth Type = " + authType);

        XMLGregorianCalendar authDate = null;
        XMLGregorianCalendar authFromDate = null;
        XMLGregorianCalendar authToDate = null;
        try {
            authDate = DateTimeUtils.convertDateToXMLGregorianCalendar(sdfTime.parse("" + session.getAttribute("authDate")));
            if (!authType.equalsIgnoreCase("4") && !authType.equalsIgnoreCase("11")) {
                authFromDate = DateTimeUtils.convertDateToXMLGregorianCalendar(sdf.parse("" + session.getAttribute("authFromDate")));
                authToDate = DateTimeUtils.convertDateToXMLGregorianCalendar(sdf.parse("" + session.getAttribute("authToDate")));
            }

        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        String memberNo = "" + session.getAttribute("memberNum_text");
        int depCode = Integer.parseInt("" + session.getAttribute("depListValues"));
        String treatProv = "" + session.getAttribute("treatingProvider_text");
        String referProv = "" + session.getAttribute("referringProvider_text");

        String pICD = "" + session.getAttribute("primaryICD_text");
        pICD = UppercaseICDs(pICD);

        String reqName = "" + session.getAttribute("requestorName");
        String reqRelationship = "" + session.getAttribute("requestorRelationship");
        String reqReason = "" + session.getAttribute("requestorReason");
        String reqContact = "" + session.getAttribute("requestorContact");

        //String notes = "" + session.getAttribute("notes"); old notes
        //new notes
        List<AuthNoteDetail> noteList = (List<AuthNoteDetail>) session.getAttribute("authNoteList");
        if (noteList == null) {
            noteList = new ArrayList<AuthNoteDetail>();
        }

        String authStatus = "" + session.getAttribute("authStatus");
        if (authStatus == null || authStatus.trim().equalsIgnoreCase("")
                || authStatus.trim().equalsIgnoreCase("99")) {
            authStatus = "3";
        }

        //check for auth type not Hospice and not Hospital AND auth status 2
        if (!authType.equalsIgnoreCase("4") && !authType.equalsIgnoreCase("11")) {
            if (authStatus.equalsIgnoreCase("2")) {
                String rejectReason = "" + request.getParameter("authRejReason");
                pa.setHospitalStatus(rejectReason);
            }
        }

        //benefit allocated
        if (session.getAttribute("benAllocated") != null) {
            LookupValue benAllocated = (LookupValue) session.getAttribute("benAllocated");
            String benCode = benAllocated.getId();
            //int benefitId = Integer.parseInt(benId);
            pa.setBenefitCode(benCode);
        }

        //set generic fields
        pa.setAuthType(authType);
        pa.setAuthorisationDate(authDate);
        pa.setCoverNumber(memberNo);
        pa.setDependantCode(depCode);
        pa.setTreatingProviderNo(treatProv);
        pa.setReferringProviderNo(referProv);
        pa.setAdmissionICD(pICD);
        pa.setPrimaryICD(pICD);
        pa.setRequestorName(reqName);
        pa.setRequestorRelationship(reqRelationship);
        pa.setRequestorReason(reqReason);
        pa.setRequestorContact(reqContact);
        pa.setAuthStartDate(authFromDate);
        pa.setAuthEndDate(authToDate);
        //pa.setNotes(notes);
        pa.getAuthNotes().addAll(noteList);
        pa.setAuthStatus(authStatus);

        List<AuthTariffDetails> tariffList = null;
        if (!authType.trim().equalsIgnoreCase("4") && !authType.trim().equalsIgnoreCase("5") && !authType.trim().equalsIgnoreCase("11")) {
            if (authType.trim().equalsIgnoreCase("6")) {
                tariffList = (List<AuthTariffDetails>) session.getAttribute("newNappiListArray");
            } else {
                tariffList = (List<AuthTariffDetails>) session.getAttribute("tariffListArray");
            }
            pa.getAuthTariffs().addAll(tariffList);
        }

        if (authType.trim().equalsIgnoreCase("4") || authType.trim().equalsIgnoreCase("7")) {
            List<AuthNappiProduct> productList = (List<AuthNappiProduct>) session.getAttribute("productNappiList");
            if (productList != null) {
                pa.getProductList().addAll(productList);
            }
        }

        //set neo awType for non hospital auths
        String awType = "";
        if (!authType.trim().equals("4") && !authType.trim().equals("9") && !authType.trim().equals("11")) {
            pa.setAuthorwiseType("NEO");

        } else if (authType.trim().equals("9")) {
            pa.setAuthorwiseType("BBX");

        } else if (authType.trim().equals("11")) {
            pa.setAuthorwiseType("BBC");
            awType = "BBC";

        } else if (authType.trim().equals("4")) {
            awType = "" + session.getAttribute("awTypeNew");
            pa.setAuthorwiseType(awType);
        }

        //Set Auth Specific Fields
        List<LabTariffDetails> labList = null;
        List<LabTariffDetails> noLabList = null;
        List<AuthCPTDetails> cptList = null;
        List<AuthHospitalLOC> locList = null;
        String client = context.getAttribute("Client").toString();
        //set copayment
        if (authType.trim().equalsIgnoreCase("4") || authType.trim().equalsIgnoreCase("11") || authType.trim().equalsIgnoreCase("9") && !client.equalsIgnoreCase("Sechaba")) {
            //set copayment amount
            double coPayAmount = 0.0d;
            String sesCpVal = "" + session.getAttribute("coPay");
            if (sesCpVal != null && !sesCpVal.trim().equals("")) {
                try {
                    coPayAmount = new Double(sesCpVal);
                } catch (Exception ex) {
                    System.out.println("copay amount exception : " + ex.getMessage());
                    coPayAmount = 0.0d;
                }
            }
            pa.setCoPaymentAmount(coPayAmount);
            if (coPayAmount > 0.0d) {
                pa.setCoPayment("1");
            } else {
                pa.setCoPayment("0");
            }
        }

        if (authType.trim().equalsIgnoreCase("5") && !client.equalsIgnoreCase("Sechaba")) {
            //set copayment amount
            double coPayAmount = 0.0d;
            String sesCpVal = "" + session.getAttribute("spmOHCopayValue");
            if (sesCpVal != null && !sesCpVal.trim().equals("")) {
                try {
                    coPayAmount = new Double(sesCpVal);
                } catch (Exception ex) {
                    System.out.println("spmOHCopayValue amount exception : " + ex.getMessage());
                    coPayAmount = 0.0d;
                }
            }
            pa.setCoPaymentAmount(coPayAmount);
            if (coPayAmount > 0.0d) {
                pa.setCoPayment("1");
            } else {
                pa.setCoPayment("0");
            }
        }

        if (authType.trim().equalsIgnoreCase("1")) {
            //Optical Details
            double numberOfDays = 0.0d;
            String numDays = "" + session.getAttribute("numDays");
            if (numDays != null && !numDays.trim().equalsIgnoreCase("")) {
                numberOfDays = new Double(numDays);
            }

            pa.setFacilityProviderNo("" + session.getAttribute("facilityProv_text"));
            pa.setSecondaryICD("" + session.getAttribute("secondaryICD_text"));
            pa.setCoMorbidityICD("" + session.getAttribute("coMorbidityICD_text"));
            pa.setNumberOfDays(numberOfDays);
            pa.setPreviousLensRx("" + session.getAttribute("prevLens"));
            pa.setCurrentLensRx("" + session.getAttribute("curLens"));

        } else if (authType.trim().equalsIgnoreCase("2") || authType.trim().equalsIgnoreCase("13")) {
            //Dental Details
            pa.setAuthSubType("" + session.getAttribute("DentalSubType"));
            pa.setLabProviderNo("" + session.getAttribute("labProvider_text"));
            labList = (List<LabTariffDetails>) session.getAttribute("SavedDentalLabCodeList");
            noLabList = (List<LabTariffDetails>) session.getAttribute("SavedDentalNoLabCodeList");
            if (labList == null) {
                labList = new ArrayList<LabTariffDetails>();
            }
            if (noLabList != null) {
                labList.addAll(noLabList);
            }
            double ac = 0.0d;
            String amount = "" + session.getAttribute("amountClaimed");
            if (amount != null && !amount.trim().equalsIgnoreCase("")
                    && !amount.trim().equalsIgnoreCase("null")) {
                ac = new Double(ac);
            }
            pa.setAmountClaimed(ac);
            pa.getAuthLabTariffs().addAll(labList);

        } else if (authType.trim().equalsIgnoreCase("3")) {
            //Save orthodontic plan details
            OrthodonticPlanDetails oplan = new OrthodonticPlanDetails();
            oplan.setAuthDate(authDate);
            oplan.setCoverNumber(memberNo);
            oplan.setDependentNumber(depCode);
            oplan.setProviderNumber(treatProv);
            //get tariff code
            String authTariff = tariffList.get(0).getTariffCode();
            oplan.setTariffCode(authTariff);
            oplan.setDuration(new Integer("" + session.getAttribute("planDuration")).intValue());
            oplan.setTotalAmount(new Double("" + session.getAttribute("amountClaimed")).doubleValue());
            oplan.setDeposit(new Double("" + session.getAttribute("depositAmount")).doubleValue());
            oplan.setFirstInstallment(new Double("" + session.getAttribute("firstAmount")).doubleValue());
            oplan.setMonthlyInstallment(new Double("" + session.getAttribute("remainAmount")).doubleValue());
            oplan.setPlanStartDate(authFromDate);
            oplan.setPlanEstimatedEndDate(authToDate);

            pa.setOrthoPlanSave(oplan);

        } else if (authType.trim().equalsIgnoreCase("4") || authType.trim().equalsIgnoreCase("11")) {
            //Hospital Details
            locList = (List<AuthHospitalLOC>) session.getAttribute("AuthLocList");
            XMLGregorianCalendar authFromCPTDate = null;

            try {
                Date dateAdd = sdfTime.parse("" + session.getAttribute("admissionDateTime"));
                Date dateCptFrom = sdf.parse("" + session.getAttribute("admissionDateTime"));
                Date dateDis = sdfTime.parse("" + session.getAttribute("dischargeDateTime"));
                authFromDate = DateTimeUtils.convertDateToXMLGregorianCalendar(dateAdd);
                authToDate = DateTimeUtils.convertDateToXMLGregorianCalendar(dateDis);
                authFromCPTDate = DateTimeUtils.convertDateToXMLGregorianCalendar(dateCptFrom);
            } catch (ParseException ex) {
                Logger.getLogger(SavePreAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            }
            pa.setAuthStartDate(authFromDate);
            pa.setAuthEndDate(authToDate);

            //Provider
            pa.setFacilityProviderNo("" + session.getAttribute("facilityProv_text"));
            pa.setAnaesthetistProviderNo("" + session.getAttribute("anaesthetistProv_text"));
            pa.setReinbursementModel("" + session.getAttribute("reimbursementModel"));

            pa.setSecondaryICD("" + session.getAttribute("secondaryICD_text"));
            pa.setCoMorbidityICD("" + session.getAttribute("coMorbidityICD_text"));

            List<AuthPMBDetails> savedPMBList = (List<AuthPMBDetails>) session.getAttribute("savedAuthPMBs");

            if (savedPMBList != null && !savedPMBList.isEmpty()) {
                pa.getAuthPMBDetails().addAll(savedPMBList);
                pa.setPmb("1");
            } else {
                pa.setPmb("0");
            }

            //set copayment amount
            if (!client.equalsIgnoreCase("Sechaba")) {
                double coPayAmount = 0.0d;
                String sesCpVal = "" + session.getAttribute("coPay");
                if (sesCpVal != null && !sesCpVal.trim().equals("")) {
                    try {
                        coPayAmount = new Double(sesCpVal);
                    } catch (Exception ex) {
                        System.out.println("copay amount exception : " + ex.getMessage());
                        coPayAmount = 0.0d;
                    }
                }
                pa.setCoPaymentAmount(coPayAmount);
                if (coPayAmount > 0.0d) {
                    pa.setCoPayment("1");
                } else {
                    pa.setCoPayment("0");
                }
            }

            if (!schemeName.equalsIgnoreCase("Resolution Health") && !client.equalsIgnoreCase("Sechaba")) {
                //penalty 
                if (session.getAttribute("penaltyPerc") != null) {

                    String pp = "" + session.getAttribute("penaltyPerc");
                    String pr = "" + session.getAttribute("penaltyReason");

                    if (!pp.equalsIgnoreCase("99") && !pr.equalsIgnoreCase("99")) {

                        pa.setPenaltyPercentage(pp);
                        pa.setPenaltyOverReason(pr);

                    }

                }

                //dsp copay
                if (session.getAttribute("dspPenaltyPerc") != null) {

                    String dspp = "" + session.getAttribute("dspPenaltyPerc");
                    String dspr = "" + session.getAttribute("dspPenaltyReason");

                    if (!dspp.equalsIgnoreCase("99") && !dspr.equalsIgnoreCase("99")) {

                        pa.setDspCopayPercentage(dspp);
                        pa.setDspCopayOverReason(dspr);

                    }

                }
            }

            pa.setNumberOfDays(new Double("" + session.getAttribute("los")));
            pa.setFunderDetails("" + session.getAttribute("funder"));

            double hospInterim = 0.0d;
            double estimatedCost = 0.0d;
            double amountClaimed = 0.0d;

            String hospAmount = "" + session.getAttribute("hospInterim");
            String cAmount = "" + session.getAttribute("amountClaimed");

            if (hospAmount != null && !hospAmount.equalsIgnoreCase("null") && !hospAmount.equalsIgnoreCase("")) {
                hospInterim = new Double(hospAmount);
            }
            if (cAmount != null && !cAmount.equalsIgnoreCase("null") && !cAmount.equalsIgnoreCase("")) {
                estimatedCost = new Double(cAmount);
            }

            if (hospInterim != 0.0d) {
                System.out.println("save hosp interim = " + hospInterim);
                System.out.println("save estimated = " + estimatedCost);
                amountClaimed = hospInterim;

            } else if (hospInterim > estimatedCost) {
                amountClaimed = hospInterim;

            } else {
                amountClaimed = estimatedCost;
            }
            pa.setAmountClaimed(amountClaimed);

            //set collection
            pa.getHospitalLOCDetails().addAll(locList);
            //ADD TARIFFS
            tariffList = (List<AuthTariffDetails>) session.getAttribute("tariffListArray");
            if (tariffList != null && tariffList.isEmpty() == false) {
                pa.getAuthTariffs().addAll(tariffList);
            }
            //ADD CTP's
            //get auth member option id
            //TODO - get option id of member by using auth start date
            int optionId = Integer.parseInt("" + session.getAttribute("schemeOption"));
            System.out.println("SavePreAuthDetailsCommand member option id for copay mapping = " + optionId);

            cptList = (List<AuthCPTDetails>) session.getAttribute("authCPTListDetails");
            if (!client.equalsIgnoreCase("Sechaba")) {
                cptList = port.addCptCoPaymentAmount(cptList, optionId, authFromCPTDate);
            }
            pa.getAuthCPTDetails().addAll(cptList);

        } else if (authType.trim().equalsIgnoreCase("5")) {

            pa.setAuthSubType("" + session.getAttribute("authSub"));

            //Condition Specific Details
            AuthSpecificBasket specificBasket = new AuthSpecificBasket();
            Collection<AuthSpecificBasketTariffs> nappiBTList = new ArrayList<AuthSpecificBasketTariffs>();
            Collection<AuthSpecificBasketTariffs> nhrplBTList = new ArrayList<AuthSpecificBasketTariffs>();

            //get modiefied basket
            int authSub = Integer.parseInt("" + session.getAttribute("authSub"));
            int productId = Integer.parseInt("" + session.getAttribute("scheme"));
            int optionId = Integer.parseInt("" + session.getAttribute("schemeOption"));
            int basketId = port.getBasketIdForCDL(productId, optionId, authSub);

            specificBasket.setAuthDate(authDate);
            specificBasket.setBasketStartDate(authFromDate);
            specificBasket.setBasketEndDate(authToDate);
            specificBasket.setCoverNumber(memberNo);
            specificBasket.setDependantNumber(depCode);
            specificBasket.setBasketYear("" + session.getAttribute("planYear"));
            specificBasket.setModifiedBasket(String.valueOf(basketId));

            List<AuthTariffDetails> tariffs = (List<AuthTariffDetails>) session.getAttribute("AuthBasketTariffs");
            List<AuthTariffDetails> specTariffs = (List<AuthTariffDetails>) session.getAttribute("AuthSpecificBasketTariffs");
            List<AuthTariffDetails> nappies = (List<AuthTariffDetails>) session.getAttribute("AuthNappiTariffs");

            if (tariffs != null && tariffs.isEmpty() == false) {
                if (specTariffs != null && specTariffs.isEmpty() == false) {
                    for (AuthTariffDetails at : specTariffs) {
                        at.setSpecificTariffInd(1);
                    }
                    tariffs.addAll(specTariffs);
                }
                for (AuthTariffDetails at : tariffs) {
                    AuthSpecificBasketTariffs nhrpl = new AuthSpecificBasketTariffs();

                    nhrpl.setCode(at.getTariffCode());
                    nhrpl.setCodeType(at.getTariffType());
                    nhrpl.setCodeOption(at.getTariffOption());
                    nhrpl.setCodeAmount(at.getAmount());
                    //nhrpl.setDosage(at.getDosage());
                    nhrpl.setFrequency(at.getFrequency());
                    nhrpl.setProviderCategory(at.getProviderType());
                    //nhrpl.setCoPay(at.getCoPayment());
                    //nhrpl.setQuantity(new Integer(at.getQuantity()));
                    if (at.getSpecificTariffInd() != 0) {
                        nhrpl.setSpecificInd(at.getSpecificTariffInd());
                    }

                    nhrplBTList.add(nhrpl);
                }
            }
            if (nappies != null && nappies.size() > 0) {
                for (AuthTariffDetails an : nappies) {
                    AuthSpecificBasketTariffs nappi = new AuthSpecificBasketTariffs();

                    nappi.setCode(an.getTariffCode());
                    nappi.setCodeType(an.getTariffType());
                    nappi.setCodeAmount(an.getAmount());
                    nappi.setDosage(an.getDosage());
                    nappi.setFrequency(an.getFrequency());
                    nappi.setProviderCategory(an.getProviderType());
                    //nappi.setCoPay(an.getCoPayment());
                    Double dQuan = new Double(an.getQuantity());
                    int intQuan = dQuan.intValue();
                    nappi.setQuantity(intQuan);

                    nappiBTList.add(nappi);
                }
            }
            if (null != specificBasket.getBasketTariffs()) {
                System.out.println("specificBasket.getBasketTariffs() not null");
                System.out.println("nhrplBTList = " + nhrplBTList);
                System.out.println("nhrplBTList size = " + nhrplBTList.size());
                specificBasket.getBasketTariffs().addAll(nhrplBTList);
                specificBasket.getBasketTariffs().addAll(nappiBTList);
            }

            pa.setAuthBasketSave(specificBasket);

        } else if (authType.trim().equals("7")) {
            String overrideInd = "" + session.getAttribute("overrideInd");
            if (overrideInd == null || overrideInd.equalsIgnoreCase("99")
                    || overrideInd.equalsIgnoreCase("null")) {
                pa.setOverrideInd("0");
            } else {
                pa.setOverrideInd(overrideInd);
            }

            String benefitSubType = "" + session.getAttribute("benSubType");
            pa.setAuthSubType(benefitSubType);

        } else if (authType.trim().equals("9")) {
            List<AuthPMBDetails> savedPMBList = (List<AuthPMBDetails>) session.getAttribute("savedAuthPMBs");
            String pmb = "0";
            if (savedPMBList != null && !savedPMBList.isEmpty()) {
                pa.getAuthPMBDetails().addAll(savedPMBList);
                pmb = "1";
            }
            pa.setPmb(pmb);

        }

        //upfront rejection of resigned dependant
        System.out.println("member no = " + memberNo);
        System.out.println("dependant = " + depCode);
        System.out.println("resigned auth pa.getAuthType() = " + pa.getAuthType());

        CoverDetails cd = new CoverDetails();
        
        List<CoverDetails> coverList = new GetCoverDetailsByNumber().setCoverForUpdate(memberNo, depCode);
        
        CoverDetails cdalternativeNum = null;
        if (coverList != null && !coverList.isEmpty()) {
//            session.setAttribute("covMemListDetails", coverList);
//            session.setAttribute("depListValues", pa.getDependantCode());
            for (CoverDetails cover : coverList) {
                if (cover.getDependentNumber() == depCode) {
                    cdalternativeNum = cover;
                    break;
                }
            }
        }
        
        if (cdalternativeNum != null) {
            if (cdalternativeNum.getCoverNumber() != null) {
                memberNo = cdalternativeNum.getCoverNumber();
                depCode = cdalternativeNum.getDependentNumber();
            }
        }
        
        System.out.println("### memberNo " + memberNo);
        System.out.println("### depCode " + depCode);
        
        cd = port.getDependentForCoverByDate(memberNo, authFromDate, depCode);
        pa.setCoverNumber(memberNo);
        pa.setDependantCode(depCode);
        boolean reject = false;
        if (cd.getEntityId() == 0) {
            System.out.println("rejected entityId = " + cd.getEntityId());
            reject = true;
        } else {
            if (cd.getStatus().trim().equalsIgnoreCase("") || cd.getStatus().trim().equalsIgnoreCase("resign")) {
                reject = true;
            }
        }
        //if (reject) {
        //pa.setAuthStatus("2");
        //System.out.println("member " + memberNo + " dependant " + depCode + " auth rejected because member resigned");
        //}

        PrintWriter out = null;
        boolean saveFailed = false;
        boolean save = true;
        String saveFailedMsg = "";
        try {
            out = response.getWriter();

            /*//DUPLICATE AUTH VALIDATION
             PreAuthSearchCriteria pasc = new PreAuthSearchCriteria();
             pasc.setAuthDate(authFromDate);
             pasc.setCoverNumber(memberNo);
             pasc.setDependantCode(depCode);
             pasc.setAuthStatus("1");
            
             List<PreAuthDetails> pdList = port.findAllPreAuthDetailsByCriteria(pasc);
             int errorCount = 0;
             if (pdList != null && pdList.isEmpty() == false) {
             for (PreAuthDetails pd : pdList) {
             System.out.println("dup validation pd.getAuthType() - "+pd.getAuthType());
             System.out.println("dup validation authType - "+authType);
             if (pd.getAuthType().equals("4") && authType.equals("9")) {
             break;
             }else{
             errorCount++;
             }
             }
             if(errorCount > 0){
             save = false;
             saveFailed = true;
             saveFailedMsg = "Error: Overlaps with existing Authorisation’s Admission Date";
             }
             }*/
            String authNumber = "";
            if (save) {
                if(client.equalsIgnoreCase("Sechaba")){
                    pa.setCoPayment(null);
                    pa.setCoPaymentAmount(0);
                    pa.setPenaltyOverReason(null);
                    pa.setPenaltyPercentage(null);
                }
                
                String authReturn = port.savePreAuthDetails(user, pa);
                //pa.getAuthCPTDetails().addAll(cptList);
                //pa.setOrthoPlan();

                if (reject) {
                    //rejection screen
                    out.println("<html>");
                    out.println("<head>");
                    out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<center>");
                    out.println("<table cellspacing=\"4\">");
                    out.println("<tr>");
                    out.println("<td><label class=\"header\">Authorisation saving failed.</label></td><br/>");
                    out.println("<td><label class=\"label\">Member: " + memberNo + "</label></td>");
                    out.println("<td><label class=\"label\">Dependant: " + depCode + "</label></td>");
                    out.println("<td><label class=\"label\">is not active for the auth period provided. </label></td>");
                    out.println("</tr>");
                    out.println("</table>");
                    out.println("<p> </p>");
                    out.println("</center>");
                    out.println("</body>");
                    out.println("</html>");

                    response.setHeader("Refresh", "5; URL=/ManagedCare/PreAuth/GenericAuthorisation.jsp");

                } else {
                    
                    if (authReturn != null && !authReturn.trim().equalsIgnoreCase("")) {

                        String[] authReturnValues = authReturn.split("\\|");
                        authNumber = authReturnValues[0];
                        int authCode = Integer.parseInt(authReturnValues[1]);

                        if (authNumber != null && !authNumber.trim().equalsIgnoreCase("")) {

                            session.setAttribute("authCode", authCode);
                            session.setAttribute("authNumber", authNumber);

                            out.println("<html>");
                            out.println("<head>");
                            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                            out.println("</head>");
                            out.println("<body>");
                            out.println("<center>");
                            out.println("<table cellspacing=\"4\">");
                            out.println("<tr>");
                            if (authNumber != null && !authNumber.equalsIgnoreCase("")) {
                                out.println("<td><label class=\"header\">Authorisation " + authNumber + " saved. under auth code " + authCode + "</label></td>");
                                System.out.println("Generated Auth Number : " + authNumber);
                                if ((pa.getAuthType().trim().equals("4") || pa.getAuthType().trim().equals("9") || pa.getAuthType().trim().equals("10") || pa.getAuthType().trim().equals("11")) && !pa.getAuthStatus().equalsIgnoreCase("2")) {
                                    port.processAuthorisation(authCode);
                                }

                            } else {
                                out.println("<td><label class=\"header\">Authorisation saving failed.</label></td>");

                            }
                            out.println("</tr>");
                            out.println("</table>");
                            out.println("<p> </p>");
                            out.println("</center>");
                            out.println("</body>");
                            out.println("</html>");

                            GetAuthConfirmationDetails getConfirmed = new GetAuthConfirmationDetails();
                            getConfirmed.execute(request, response, context);

                        } else {
                            System.out.println("authReturn null");
                            saveFailed = true;
                            saveFailedMsg = "Authorisation Failed to Save.";
                        }
                    } else {
                        System.out.println("authReturn null");
                        saveFailed = true;
                        saveFailedMsg = "Authorisation Failed to Save.";
                    }
                }
            }

            if (saveFailed) {
                out.println("<html>");
                out.println("<head>");
                out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                out.println("</head>");
                out.println("<body>");
                out.println("<center>");
                out.println("<table cellspacing=\"4\">");
                out.println("<tr>");
                out.println("<td><label class=\"header\">" + saveFailedMsg + "</label></td>");
                out.println("</tr>");
                out.println("</table>");
                out.println("<p> </p>");
                out.println("</center>");
                out.println("</body>");
                out.println("</html>");

                response.setHeader("Refresh", "3; URL=/ManagedCare/PreAuth/GenericAuthorisation.jsp");
            } else {
                if (authType.trim().equals("5") && client.equalsIgnoreCase("Sechaba")) {
                    String condSubType = pa.getAuthSubType();
                    if(condSubType.equalsIgnoreCase("27") || condSubType.equalsIgnoreCase("28")){
                        System.out.println("Condition Specific workflow trigger");
                        int entityID = cd.getEntityId();
                        String refNum = null;   //autogenerated
                        int callCat = 1;   //Authorisation
                        int status = 1;       //Open
                        int userID = user.getUserId();
                        System.out.println("entityID " + entityID);
                        System.out.println("userID " + userID);
                        String notes = "Condition specific authorisation created " + DateTimeUtils.convertXMLGregorianCalendarToDate(pa.getAuthorisationDate()) + ", please load supporting medicine pre-authorisation. Auth Number: " + authNumber + ", Member Number: " + memberNo;
                        WorkflowItem wi = setPreAuthWorkflowItem(entityID, refNum, memberNo, callCat, status, notes, userID, condSubType);
                        service.getNeoManagerBeanPort().captureGenericWorkflowItem(wi);
                    } 
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SavePreAuthDetailsCommand";
    }

    private String getValue(Object obj) {
        String value = "";
        if (obj != null) {
            value = value + obj;
        }

        return value;
    }

    private String UppercaseICDs(String icd) {
        char tc = Character.toUpperCase(icd.charAt(0));
        String sub = Character.toString(tc) + icd.substring(1, icd.length());
        return sub;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
    
    private WorkflowItem setPreAuthWorkflowItem(Integer entityID, String refNum, String entityNum, Integer category, Integer statusID, String notes, Integer userID, String condSubType) {
        WorkflowItem wi = new WorkflowItem();
        Integer queueID = category;
        
        wi.setSourceID(6); // Source is Neo
	wi.setProductID(3); // Sizwe
	wi.setQueueID(queueID); // Set Correct Queue ID
	wi.setStatusID(statusID);
        
	wi.setSubject("Condtion Specific Medicine Required");
        wi.setRefNum(refNum);
        
        ///////////////////
        wi.setEntityID(entityID);
        wi.setEntityNumber(entityNum);
        wi.setNotes(notes);
        ///////////////////
        
	wi.setUserID(null); // User assigned to
	wi.setCreatedBy(userID);
	wi.setLastUpdatedBy(userID);
	wi.setDocumentID(null);

        return wi;
    }
}

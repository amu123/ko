/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.validation;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.preauth.command.ForwardToBenefitAllocation;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthBenefitLimits;
import neo.manager.AuthTariffDetails;
import neo.manager.CoverDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.TariffCode;

/**
 *
 * @author Johan-NB
 */
public class ValidateHospiceCare extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        PrintWriter out = null;

        String client = String.valueOf(context.getAttribute("Client"));

        String errorResponse = "";
        String error = "";
        int errorCount = 0;
        List<AuthBenefitLimits> allBen = new ArrayList<AuthBenefitLimits>();
        LookupValue benAllocated = new LookupValue();
        //validation
        String authTypeStr = "" + session.getAttribute("authType");
        String authPeriod = "" + session.getAttribute("authPeriod");
        String provDesc = "" + session.getAttribute("facilityProvDiscTypeId");
        String tProvDesc = "" + session.getAttribute("treatingProviderDiscTypeId");

        //String optIdStr = "";// + session.getAttribute("schemeOption");
        int optionId = 0;//Integer.parseInt(optIdStr);
        String memNo = "" + session.getAttribute("memberNum_text");
        String depCodeStr = "" + session.getAttribute("depListValues");
        String fromDateStr = "" + session.getAttribute("fromDate");

        //System.out.println("ValidateHospiceCare authType = " + authTypeStr);
        //System.out.println("ValidateHospiceCare authPeriod = " + authPeriod);
        //System.out.println("ValidateHospiceCare provider discipline = " + provDesc);
        //System.out.println("ValidateHospiceCare optIdStr = " + optIdStr);
        //System.out.println("ValidateHospiceCare memNo = " + memNo);
        //System.out.println("ValidateHospiceCare depCodeStr = " + depCodeStr);
        //System.out.println("ValidateHospiceCare fromDateStr = " + fromDateStr);
        //auth type
        if (authTypeStr == null || authTypeStr.trim().equalsIgnoreCase("99")) {
            errorCount++;
            error = error + "|authType:Authorisation Type is Mandatory";
        }

        //date
        if (Integer.parseInt(authPeriod) < 2012) {
            errorCount++;
            error = error + "|authLOCButton:Incorrect Authorisation Period";
        } else {
            authPeriod = authPeriod + "/01/01";
        }

        //option
        if (fromDateStr == null || fromDateStr.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|authLOCButton:Auth Start Date Required";
        } else {
            Date fromDate = null;
            XMLGregorianCalendar xDate = null;
            try {
                fromDate = new SimpleDateFormat("yyyy/MM/dd").parse(fromDateStr);
            } catch (ParseException px) {
                px.printStackTrace();
                errorCount++;
                error = error + "|authLOCButton:Incorrect Date Format (" + fromDateStr + ")";
            }
            if (errorCount == 0) {
                xDate = DateTimeUtils.convertDateToXMLGregorianCalendar(fromDate);
                int depCode = Integer.parseInt(depCodeStr);

                CoverDetails cd = service.getNeoManagerBeanPort().getDependentForCoverByDate(memNo, xDate, depCode);
                optionId = cd.getOptionId();
            }
        }

        //provider
        if (provDesc == null || provDesc.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|facilityProv:Facility Provider is Mandatory";
        } else {
            if (!provDesc.equals("047") && !provDesc.equals("049")
                    && !provDesc.equals("059") && !provDesc.equals("079")) {
                errorCount++;
                error = error + "|facilityProv:Hospice Care only allows 047, 049, 059, 079 disciplines";
            }
        }

        //tariffs
        if (errorCount == 0) {
            ArrayList<AuthTariffDetails> atList = (ArrayList<AuthTariffDetails>) session.getAttribute("tariffListArray");
            if (atList != null && atList.isEmpty() == false) {
//                String validStr = validateDesciplineTariffs(request, provDesc, atList);
//                String[] validStrSplit = validStr.split("\\|");
//                boolean valid = Boolean.valueOf(validStrSplit[0]);
//                if (valid == false) {
//                    errorCount++;
//                    error = error + "|authTariffButton:The Following Tariffs are not allowed - " + validStrSplit[1];
//                }

            } else {
                errorCount++;
                error = error + "|authTariffButton:Clinical Tariff is Mandatory";

            }

        }

        if (errorCount > 0) {
            errorResponse = "ERROR" + error;
        } else if (errorCount == 0) {
            //getBenefit
            int authType = Integer.parseInt(authTypeStr);
            XMLGregorianCalendar authDate = null;
            try {
                authDate = DateTimeUtils.convertDateToXMLGregorianCalendar(new SimpleDateFormat("yyyy/MM/dd").parse(authPeriod));
            } catch (ParseException ex) {
                Logger.getLogger(ForwardToBenefitAllocation.class.getName()).log(Level.SEVERE, null, ex);
            }

//            allBen = port.getAuthProductBenefits(authType, authDate, memNo, depCodeStr);
//            if (allBen == null || allBen.isEmpty() == true) {
//                errorResponse = "ERROR|authNoteButton:Member Option not Applicable on this Auth";
//            } else {
//                for (AuthBenefitLimits lv : allBen) {
//                    benAllocated.setId(lv.getMainBenefitCode());
//                    benAllocated.setValue(lv.getMainDesc());
//                }
//                session.setAttribute("benAllocated", benAllocated);
            errorResponse = "Done|";
//            }
        }
        try {
            out = response.getWriter();
            out.println(errorResponse);

        } catch (Exception ex) {
            System.out.println("ValidateHospiceCare error : " + ex.getMessage());
        } finally {
            out.close();
        }

        return null;
    }

    @Override
    public String getName() {
        return "ValidateHospiceCare";
    }

    public String validateDesciplineTariffs(HttpServletRequest request, String provDesc, List<AuthTariffDetails> atList) {
        boolean valid = false;
        String[] allowedTariffs = null;

        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Collection<TariffCode> tList = new ArrayList<TariffCode>();
//        Collection<AuthTariffDetails> atList = new ArrayList<AuthTariffDetails>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        //get values from request
//        String tariffCode = request.getParameter("code");
//        String tariffDesc = request.getParameter("description");

//        if (tariffCode == null || tariffCode.equalsIgnoreCase("null")) {
//            tariffCode = "";
//        }
//        if (tariffDesc == null || tariffDesc.equalsIgnoreCase("null")) {
//            tariffDesc = "";
//        }
//        String provTypeId = "";
//        if (session.getAttribute("providerDiscTypeId") != null) {
//            provTypeId = session.getAttribute("providerDiscTypeId").toString();
//        } else {
//            provTypeId = "" + session.getAttribute("treatingProviderDiscTypeId");
//        }
        int pId = Integer.parseInt("" + session.getAttribute("scheme"));
        int oId = Integer.parseInt("" + session.getAttribute("schemeOption"));
        XMLGregorianCalendar authDate = null;
        try {
            authDate = DateTimeUtils.convertDateToXMLGregorianCalendar(sdf.parse("" + session.getAttribute("authDate")));
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        System.out.println("product = " + pId);
        System.out.println("option = " + oId);
        System.out.println("service date = " + authDate);
        System.out.println("prov disc = " + provDesc);

//        if (provDesc.equalsIgnoreCase("047")) {
//            allowedTariffs = new String[1];
//            allowedTariffs[0] = "001";
//
//        } else if (provDesc.equalsIgnoreCase("049")) {
//            allowedTariffs = new String[19];
//            allowedTariffs[0] = "001";
//            allowedTariffs[1] = "003";
//            allowedTariffs[2] = "005";
//            allowedTariffs[3] = "007";
//            allowedTariffs[4] = "101";
//            allowedTariffs[5] = "105";
//            allowedTariffs[6] = "284";
//            allowedTariffs[7] = "419";
//            allowedTariffs[8] = "49301";
//            allowedTariffs[9] = "49302";
//            allowedTariffs[10] = "49303";
//            allowedTariffs[11] = "49304";
//            allowedTariffs[12] = "49305";
//            allowedTariffs[13] = "49306";
//            allowedTariffs[14] = "710";
//            allowedTariffs[15] = "711";
//            allowedTariffs[16] = "712";
//            allowedTariffs[17] = "713";
//            allowedTariffs[18] = "714";
//
//        } else if (provDesc.equalsIgnoreCase("059")) {
//            allowedTariffs = new String[30];
//            allowedTariffs[0] = "006";
//            allowedTariffs[1] = "009";
//            allowedTariffs[2] = "012";
//            allowedTariffs[3] = "100";
//            allowedTariffs[4] = "101";
//            allowedTariffs[5] = "105";
//            allowedTariffs[6] = "107";
//            allowedTariffs[7] = "109";
//            allowedTariffs[8] = "110";
//            allowedTariffs[9] = "112";
//            allowedTariffs[10] = "113";
//            allowedTariffs[11] = "272";
//            allowedTariffs[12] = "278";
//            allowedTariffs[13] = "283";
//            allowedTariffs[14] = "284";
//            allowedTariffs[15] = "301";
//            allowedTariffs[16] = "302";
//            allowedTariffs[17] = "415";
//            allowedTariffs[18] = "574";
//            allowedTariffs[19] = "614";
//            allowedTariffs[20] = "701";
//            allowedTariffs[21] = "702";
//            allowedTariffs[22] = "703";
//            allowedTariffs[23] = "704";
//            allowedTariffs[24] = "705";
//            allowedTariffs[25] = "710";
//            allowedTariffs[26] = "711";
//            allowedTariffs[27] = "712";
//            allowedTariffs[28] = "713";
//            allowedTariffs[29] = "714";
//
//        } else if (provDesc.equalsIgnoreCase("079")) {
//            allowedTariffs = new String[3];
//            allowedTariffs[0] = "950";
//            allowedTariffs[1] = "955";
//            allowedTariffs[2] = "960";
//
//        }

        //validate descipline allowed tariffs against tariff list
//        System.out.println("allowed tariff size = " + allowedTariffs.length);
        String errorTariffs = "";
        for (AuthTariffDetails at : atList) {
            String tariff = at.getTariffCode();
            String et = "";
            if(provDesc.equalsIgnoreCase("047") || provDesc.equalsIgnoreCase("049") || provDesc.equalsIgnoreCase("059") || provDesc.equalsIgnoreCase("079"))
                tList = port.getTariffCodeByCriteria(pId, oId, provDesc, at.getTariffCode(), at.getTariffDesc(), authDate);

            //System.out.println("atList tariff = " + tariff);
            for (TariffCode s : tList) {
                //System.out.println("allowed tariff = " + s);
                if (tariff.trim().equalsIgnoreCase(s.getCode())) {
                    et = "found";
                    break;
                }
            }
            if (et.equalsIgnoreCase("")) {
                errorTariffs += tariff + ",";
            }
        }

        if (errorTariffs.equalsIgnoreCase("")) {
            valid = true;
        } else {
            errorTariffs = errorTariffs.substring(0, errorTariffs.lastIndexOf(","));

        }
        System.out.println("errorTariffs = " + errorTariffs);

        String returnStr = valid + "|" + errorTariffs;
        return returnStr;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.practice_management.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoUser;
import neo.manager.PersonDetails;
import neo.manager.Security;

/**
 *
 * @author chrisdp
 */
public class UpdatePracticeNameCommand extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        String practiceNo = (String) session.getAttribute("provNum");
        String practiceName = (String) session.getAttribute("provNumName");
        String restriction = request.getParameter("oldRestriction");
        String restriction1 = request.getParameter("oldRestriction1");
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        String newName = request.getParameter("newPracticeName");
        String newRestriction = request.getParameter("newRestriction");
        int commEntityId = Integer.parseInt(request.getParameter("commonEntityId1"));
        String practiceType = request.getParameter("practiceTypeId1");
        session.removeAttribute("PracticeDetails_practiceName");
        
        System.out.println("User logged in : " + user.getUserId());
        System.out.println("Practice number from session : " + practiceNo);
        System.out.println("Practice name from session : " + practiceName);
        System.out.println("Practice name from agiletag : " + newName);
        System.out.println("Common entity ID : " + commEntityId);
        System.out.println("Practice type ID : " + practiceType);

        if (newName != null && !newName.equals("") && !practiceName.equals(newName)) {
            boolean status = service.getNeoManagerBeanPort().updatePracticeNameByCommEntityId(commEntityId, newName);
            if (status) {
                System.out.println("Practice name record(s) updated.");
                
                PersonDetails pd = new PersonDetails();
                Security sec = new Security();
                pd.setEntityCommonId(new Integer(commEntityId));                
                pd.setEntityTypeId(new Integer(practiceType));
                pd.setName(practiceName);
                sec.setCreatedBy(user.getUserId());
                int rows = service.getNeoManagerBeanPort().insertPracticeNameHistory(pd, sec);
                if(rows > 0){
                    System.out.println("Practice name record inserted into NEO_ENTITY_DETAILS_AUDIT");
                    session.setAttribute("PracticeDetails_practiceName", newName);  // Populate text field with new value/name
                    session.setAttribute("provNumName", newName);                   // Populate header with new name
                } else {
                    System.out.println("No records inserted into NEO_ENTITY_DETAILS_AUDIT");
                }
            } else {
                System.out.println("Practice name record(s) not updated.");
            }
        } else {
            System.out.println("Practice name record(s) not updated.");
        }
        
        System.out.println("restriction = " + restriction);
        System.out.println("restriction1 = " + restriction1);
        System.out.println("new restriction = " + newRestriction);
        if (newRestriction != null && !newRestriction.equals("") && !restriction.equals(newRestriction)) {
            System.out.println("Setting new Restriction");
            int restrict = Integer.parseInt(newRestriction);
            if(service.getNeoManagerBeanPort().updatePracticeRestrictionsByCommEntityId(commEntityId, restrict)){
                System.out.println("Practice Restrictions updated");
            } else {
                System.out.println("Practice Restrictions Not updated");
            }
        } else {
            System.out.println("Practice Restrictions Not updated");
        }
      
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher("/PracticeManagement/PracticeDetails.jsp");
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "UpdatePracticeNameCommand"; 
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author whauger
 */
public class LabelTextSearchTextDiv extends TagSupport {
    private String displayName;
    private String elementName;
    private String mandatory = "no";
    private String onChange = "";
    private String onClick = "";

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        try {

                    out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "_text\" name=\"" + elementName + "_text\" value=\"" +( req.getAttribute(elementName+ "_text") == null ? "" : req.getAttribute(elementName+ "_text")) + "\" size=\"30\" onChange='" + onChange + "'/></td>");
                    if (mandatory.equalsIgnoreCase("yes")) {
                        out.println("<td><label class=\"red\">*</label></td>");
                    } else {
                        out.println("<td></td>");
                    }
                    out.println("<td align=\"left\"><img src=\"/ManagedCare/resources/Search.gif\" width=\"28\" height=\"28\" alt=\"Search\" border=\"0\" onClick=\"" + onClick + "\";/></td>");
                    out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelTextSearchTextDiv tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setOnChange(String onChange) {
        this.onChange = onChange;
    }

    public void setOnClick(String onClick) {
        this.onClick = onClick;
    }

}

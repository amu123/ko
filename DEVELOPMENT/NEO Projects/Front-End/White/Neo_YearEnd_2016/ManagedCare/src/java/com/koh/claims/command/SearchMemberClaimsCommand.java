/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.util.Collection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ClaimSearchCriteria;
import neo.manager.ClaimsBatch;
import neo.manager.NeoManagerBean;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class SearchMemberClaimsCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(SearchMemberClaimsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        logger.info("Inside SearchClaimsCommand");

//        HttpSession session = request.getSession();
//
//        NeoManagerBean port = service.getNeoManagerBeanPort();
//
//        ClaimSearchCriteria claimSearch = new ClaimSearchCriteria();
//
//        String coverNumber = ""+session.getAttribute("policyHolderNumber");
//
//        String practiceNumber =  request.getParameter("practiceNumber_text");
//
//        claimSearch.setPracticeNumber(practiceNumber);
//
//        claimSearch.setCoverNumber(coverNumber);
//
//        Collection<ClaimsBatch> batchList =  port.findClaimBySearchCriteria(claimSearch);
//
//        session.setAttribute("batchListResults", batchList);

        try {

            String nextJSP = "/Claims/MemberCCClaimsDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {

        return "SearchMemberClaimsCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.dto;

/**
 *
 * @author marcelp
 */
public class ClaimLineModifier {

    private String modifier;
    private double modifierAmount;
    
    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public double getModifierAmount() {
        return modifierAmount;
    }

    public void setModifierAmount(double modifierAmount) {
        this.modifierAmount = modifierAmount;
    }
    
    
}

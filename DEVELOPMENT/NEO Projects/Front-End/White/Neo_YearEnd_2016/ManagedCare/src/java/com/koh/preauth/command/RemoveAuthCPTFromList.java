/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthCPTDetails;

/**
 *
 * @author johanl
 */
public class RemoveAuthCPTFromList extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        ArrayList<AuthCPTDetails> cptList = (ArrayList<AuthCPTDetails>) session.getAttribute("authCPTListDetails");
        try {
            PrintWriter out = response.getWriter();
            if (cptList != null) {
                String cptCode = request.getParameter("cptCode");
                for (AuthCPTDetails cpt : cptList) {
                    if (cptCode.equalsIgnoreCase(cpt.getCptCode())) {
                        out.print("OK|" + cpt.getCptCode());
                        cptList.remove(cpt);
                        break;
                    }
                }
                session.setAttribute("authCPTListDetails", cptList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "RemoveAuthCPTFromList";
    }
}

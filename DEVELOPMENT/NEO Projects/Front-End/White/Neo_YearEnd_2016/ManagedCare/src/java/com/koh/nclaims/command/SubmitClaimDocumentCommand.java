/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.command;

import agility.za.indexdocumenttype.DeleteDocType;
import agility.za.indexdocumenttype.IndexDocType;
import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import com.koh.command.NeoCommand;
import com.koh.dataload.command.LoadDataFileCommand;
import com.koh.serv.PropertiesReader;
import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoUser;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.servlet.ServletRequestContext;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author janf
 */
public class SubmitClaimDocumentCommand extends NeoCommand {

    static String uploadedFileName = "";

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            String command = request.getParameter("command");
            HttpSession session = request.getSession();
            String srcDrive = new PropertiesReader().getProperty("DocumentIndexWorkDrive");
            String indexDrive = new PropertiesReader().getProperty("DocumentIndexIndexDrive");
            System.out.println("[frs]SubmitClaimDocumentCommand - " + command);

            if ("save".equalsIgnoreCase(command)) {

                String selectedFile = request.getParameter("fileName");
                String claimNumber = request.getParameter("claimId") != null ? request.getParameter("claimId") : "";
                String memberNumber = request.getParameter("memberNumber");
                String providerNumber = request.getParameter("providerNumber");
                String batchNumber = request.getParameter("batchId");
                Date today = new Date(System.currentTimeMillis());
                String day = "" + today.getDate();
                String month = "" + (today.getMonth() + 1);
                String year = "" + (today.getYear() + 1900);
                if(day.length() < 2){
                    day = "0" + day;
                }
                if(month.length() < 2){
                    month = "0" + month;
                }
                
                System.out.println("[fr]today = " + today + "\ngetTime = " + today.getTime() + "\nYear = " + (today.getYear()+1900) + "\nMonth = " + (today.getMonth() + 1) + "\nDay = " + today.getDate());
                //String finalPath = indexDrive + "Output/" + year + "/" + month + "/" + day + "/";
                String finalPath = srcDrive + "Index/Claims/";
                
                System.out.println("srcDrive = " + srcDrive);
                System.out.println("indexDrive = " + indexDrive);
                System.out.println("selectedFile = " + selectedFile);
                System.out.println("claimNumber = " + claimNumber);
                System.out.println("memberNumber = " + memberNumber);
                System.out.println("providerNumber = " + providerNumber);
                System.out.println("batchNumber = " + batchNumber);
                System.out.println("Final Path = " + finalPath);

                File uploadedFile = null;

                List fileItems = (List) session.getAttribute("file");
                session.setAttribute("file", null);
                if (fileItems != null && !fileItems.isEmpty()) {
                    String fileName = memberNumber + " - " + claimNumber +  " - CLM";
                    upload(finalPath, fileItems, uploadedFile, fileName);
                    System.out.println("Starting Indexing");
                    System.out.println("[fr]Filename before = " + fileName);
                    fileName += uploadedFileName.substring(uploadedFileName.lastIndexOf("."));
                    System.out.println("[fr]Filename after = " + fileName);
                    IndexDocument(request, memberNumber, claimNumber, srcDrive, indexDrive, fileName);
                }
            } else if ("upload".equalsIgnoreCase(command)) {
                boolean isMultipart = ServletFileUpload.isMultipartContent(request);
                System.out.println("isMultipart : " + isMultipart);

                File uploadedFile = null;
                if (isMultipart) {

                    // Create a factory for disk-based file items
                    // FileItemFactory factory = new DiskFileItemFactory();
                    DiskFileItemFactory itemFactory = new DiskFileItemFactory();
                    itemFactory.setRepository(uploadedFile);
                    itemFactory.setSizeThreshold(2000);

                    ServletFileUpload upload = new ServletFileUpload(itemFactory);
                    // Parse the request
                    List fileItems = upload.parseRequest(request);
                    session.setAttribute("file", fileItems);
                    //upload(srcDrive, fileItems, uploadedFile, "tempFileName");
                }
//                    Iterator iter = fileItems.iterator();
//                    while (iter.hasNext()) {
//
//                        FileItem item = (FileItem) iter.next();
//
//                        if (item.getName() != null && !item.getName().equalsIgnoreCase("")) {
//                            String itemName = item.getName();
//                            //String uploadedFileName = itemName.subSequence(itemName.lastIndexOf("/")+1, itemName.length);
//                            uploadedFileName = FilenameUtils.getName(itemName);
//
//                            boolean check = getFileExtention(new File(uploadedFileName), 2);
//
//                            if (check) {
//                                String fileUploadLoc = srcDrive + "Test/";
//                                System.out.println("uploadedFileName = " + uploadedFileName);
//                                System.out.println("uploadedFileLocation = " + fileUploadLoc);
//
//                                File direcory = new File(fileUploadLoc);
//                                if (!direcory.exists()) {
//                                    System.out.println("Directory not found. Creating....");
//                                    direcory.mkdirs();
//                                    System.out.println("Done");
//                                } else {
//                                    System.out.println("Directory exist. not creating.");
//                                }
//
//                                uploadedFile = new File(fileUploadLoc + uploadedFileName);
//
//                                System.out.println("Content Type is : " + item.getContentType());
//
//                                // uploadedFile = new File(destination, item.getName());
//                                item.write(uploadedFile);
//                                System.out.println("Upload completed successfully !!!!!!!!!!");
//                            } else {
//                                uploadedFile = null;
//                            }
//                        }
//                    }
                //}
            }

        } catch (Exception e) {
            System.out.println("Exeption : " + e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    private void upload(String path, List fileItems, File uploadedFile, String fileName) throws Exception {
        Iterator iter = fileItems.iterator();
        while (iter.hasNext()) {

            FileItem item = (FileItem) iter.next();

            if (item.getName() != null && !item.getName().equalsIgnoreCase("")) {
                String itemName = item.getName();
                //String uploadedFileName = itemName.subSequence(itemName.lastIndexOf("/")+1, itemName.length);
                uploadedFileName = FilenameUtils.getName(itemName);

                boolean check = getFileExtention(new File(uploadedFileName), 2);
                System.out.println("uploadedFileName = " + uploadedFileName);
                String ext = uploadedFileName.substring(uploadedFileName.lastIndexOf("."));
                fileName += ext;
                System.out.println("fileName = " + fileName);

                if (check) {
                    String fileUploadLoc = path;
                    System.out.println("uploadedFileName = " + uploadedFileName);
                    System.out.println("uploadedFileLocation = " + fileUploadLoc);
                    System.out.println("fileName = " + fileName);

                    File direcory = new File(fileUploadLoc);
                    if (!direcory.exists()) {
                        System.out.println("Directory not found. Creating....");
                        direcory.mkdirs();
                        System.out.println("Done");
                    } else {
                        System.out.println("Directory exist. not creating.");
                    }

                    //uploadedFile = new File(fileUploadLoc + uploadedFileName);
                    uploadedFile = new File(fileUploadLoc + fileName);

                    System.out.println("Content Type is : " + item.getContentType());

                    // uploadedFile = new File(destination, item.getName());
                    item.write(uploadedFile);
                    System.out.println("Upload completed successfully !!!!!!!!!!");
                } else {
                    uploadedFile = null;
                }
            }
        }
    }

    private static boolean getFileExtention(File file, int scheme) {
        String fileName = file.getName();
        boolean check = false;
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            String ext = fileName.substring(fileName.lastIndexOf(".") + 1);
            System.out.println("Uploaded file type is :" + ext);
            if (ext.equalsIgnoreCase("pdf")) {
                check = true;
            }
        }
        return check;
    }
    
    private void IndexDocument(HttpServletRequest request, String memberNumber, String claimNumber, String srcDrive, String indexDrive, String selectedFile) {
        String entityType = "";
        String folderList = "Index";
        String docType = null;
        String profileType = "Claims";
        IndexDocumentRequest req = new IndexDocumentRequest();
        NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
        req.setAgent(loggedInUser);
        req.setIndexType("Index");
        req.setSrcDrive(srcDrive);
        req.setEntityNumber(memberNumber);
        if (entityType != null && !entityType.equals("")) {
            req.setEntityType(Integer.parseInt(entityType));
        } else {
            req.setEntityType(5);
        }
        IndexDocType indexDocType = new IndexDocType();
        indexDocType.setDestDrive(indexDrive);
        indexDocType.setFolder(folderList + "/" + profileType);
        indexDocType.setSrcFile(selectedFile);
        indexDocType.setEntityNumber(memberNumber);
        if (claimNumber != null && !claimNumber.equals("")) {
            req.setRef(claimNumber);
            indexDocType.setDocType("10");
        } else {
            indexDocType.setDocType(docType);
        }
        if (entityType != null && !entityType.equals("")) {
            indexDocType.setEntityType(Integer.parseInt(entityType));
        } else {
            indexDocType.setEntityType(5);
        }
        req.setIndexDoc(indexDocType);
        System.out.println("Before Webservice");
        IndexDocumentResponse res = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
        System.out.println("res = " + res.isIsSuccess());
        System.out.println("res = " + res.getMessage());
        if (res.isIsSuccess()) {
            req.setIndexDoc(null);
            req.setIndexType("Delete");
            DeleteDocType deleteDocType = new DeleteDocType();
            deleteDocType.setFolder(folderList + "/" + profileType);
            deleteDocType.setFilename(selectedFile);
            req.setDelete(deleteDocType);
            res = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
        } 
    }

    @Override
    public String getName() {
        return "SubmitClaimDocumentCommand";
    }

}

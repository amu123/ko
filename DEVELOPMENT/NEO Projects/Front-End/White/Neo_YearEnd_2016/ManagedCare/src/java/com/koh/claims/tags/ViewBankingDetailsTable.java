/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.tags;

import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author josephm
 */
public class ViewBankingDetailsTable extends TagSupport {
    private String javaScript;
    private String commandName;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest request = pageContext.getRequest();


        try {

            out.println("<label class=\"header\">Banking Details</label></br></br>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Payment Method</th><th>Bank Name</th><th>Branch Name</th><th>Branch Code</th>" +
                    "<th>Account Type</th><th>Account Number</th><th>Account Name</th><th>Date From</th>" +
                    "<th>Date To</th></tr>");


        } catch (java.io.IOException ex) {
            throw new JspException("Error in ViewBankingDetailsTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

}

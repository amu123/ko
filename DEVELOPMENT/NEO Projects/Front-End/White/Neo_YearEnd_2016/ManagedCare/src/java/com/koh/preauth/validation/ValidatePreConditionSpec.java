/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.validation;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Johan-NB
 */
public class ValidatePreConditionSpec extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter out = null;
        HttpSession session = request.getSession();

        String errorResponse = "";
        String error = "";
        int errorCount = 0;
        NeoManagerBean port = service.getNeoManagerBeanPort();

        String authSubType = request.getParameter("authSub");
        String authFromDate = request.getParameter("authFromDate");
        String picd = "" + session.getAttribute("primaryICD_text");

        if (authSubType.equalsIgnoreCase("") || authSubType.equalsIgnoreCase("99")) {
            errorCount++;
            error = error + "|authSub:Auth Sub Type is Mandatory";

        }

        if (authFromDate.equalsIgnoreCase("") || authFromDate.equalsIgnoreCase("null")) {
            errorCount++;
            error = error + "|authFromDate:Date From is mandatory";

        } else {
            Date aFrom = null;
            try {
                aFrom = new SimpleDateFormat("yyyy/MM/dd").parse(authFromDate);
            } catch (ParseException px) {
                errorCount++;
                error = error + "|authFromDate:Date From is incorrect";
            }
        }

        if (errorCount > 0) {
            errorResponse = "Error" + error;
        } else if (errorCount == 0) {

            int basketType = Integer.parseInt(authSubType);
            int prodId = Integer.parseInt(session.getAttribute("scheme").toString());
            Collection<String> basketICDs = port.getBasketSpecificICDs(basketType, prodId);
            boolean icdFound = false;
            for (String basketIcd : basketICDs) {
                String[] icdValues = basketIcd.split("\\|");
                String icd = icdValues[0];
                if (icd.equals(picd)) {
                    icdFound = true;
                    break;
                }
            }
            if (icdFound == false) {
                errorResponse = "Error|picd:basketICDs";
                session.setAttribute("basketICDs", basketICDs);
            }else{
                session.setAttribute("basketICDs", null);
                errorResponse = "Done|";
            }
        }

        try {
            out = response.getWriter();
            out.println(errorResponse);

        } catch (Exception ex) {
            System.out.println("ValidatePreConditionSpec error : " + ex.getMessage());
        } finally {
            out.close();
        }

        return null;
    }

    @Override
    public String getName() {
        return "ValidatePreConditionSpec";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.calltrack.command;

import com.koh.command.Command;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoUser;
/**
 *
 * @author gerritr
 */
public class WorkflowReloadCallLogCommand extends Command{
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        this.clearScreenFromSession(request);
        clearAllFromSession(request);
        try {
            String nextJSP = "/Calltrack/WorkflowLogNewCall.jsp";
            
            
                    //<editor-fold defaultstate="collapsed" desc="Log a New Call">
                    System.out.println("Setting up clean CallTrack Page");
                    
                    HttpSession session = request.getSession();
                    NeoUser user = (NeoUser) session.getAttribute("persist_user");
                    Date today = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                    String date = sdf.format(today);
                    request.setAttribute("uName", user.getUsername());
                    request.setAttribute("disDate", date);

                    request.setAttribute("callType", "");
                    request.setAttribute("callTrackNumber", "");
                    request.setAttribute("callStatus", "");
                    request.setAttribute("memberNumber", "");
                    request.setAttribute("provNum", "");
                    request.setAttribute("providerNumber", "");
                    request.setAttribute("memberNum", "");
                    request.setAttribute("provName", "");
                    request.setAttribute("discipline", "");
                    request.setAttribute("callerName", "");
                    request.setAttribute("callerContact", "");
                    request.setAttribute("queryMethod", "");
                    request.setAttribute("callQ", "");

                    //<editor-fold defaultstate="collapsed" desc="Catagory Checkboxes">
                    request.setAttribute("claimsFC_0", "");
                    request.setAttribute("claimsFC_1", "");
                    request.setAttribute("claimsSC_0", "");
                    request.setAttribute("claimsSC_1", "");
                    request.setAttribute("claimsSC_2", "");
                    request.setAttribute("claimsSC_3", "");
                    request.setAttribute("claimsSC_4", "");
                    request.setAttribute("claimsSC_5", "");
                    request.setAttribute("claimsSC_6", "");
                    request.setAttribute("claimsTC_0", "");
                    request.setAttribute("claimsTC_1", "");
                    request.setAttribute("claimsTC_2", "");
                    request.setAttribute("claimsTC_3", "");
                    request.setAttribute("claimsTC_4", "");
                    request.setAttribute("claimsTC_5", "");
                    request.setAttribute("claimsTC_6", "");
                    request.setAttribute("claimsTC_7", "");
                    request.setAttribute("claimsTC_8", "");
                    request.setAttribute("claimsTC_9", "");
                    request.setAttribute("claimsTC_10", "");
                    request.setAttribute("claimsTC_11", "");

                    request.setAttribute("backOfficeFC_0", "");
                    request.setAttribute("backOfficeFC_1", "");
                    request.setAttribute("backOfficeSC_0", "");
                    request.setAttribute("backOfficeSC_1", "");
                    request.setAttribute("backOfficeSC_2", "");
                    request.setAttribute("backOfficeSC_3", "");
                    request.setAttribute("backOfficeSC_4", "");

                    request.setAttribute("groupsFC_0", "");
                    request.setAttribute("groupsFC_1", "");
                    request.setAttribute("groupsFC_2", "");
                    request.setAttribute("groupsSC_0", "");
                    request.setAttribute("groupsSC_1", "");
                    request.setAttribute("groupsSC_2", "");
                    request.setAttribute("groupsSC_3", "");
                    request.setAttribute("groupsSC_4", "");

                    request.setAttribute("brokerFC_0", "");
                    request.setAttribute("brokerFC_1", "");
                    request.setAttribute("brokerFC_2", "");
                    request.setAttribute("brokerFC_3", "");
                    request.setAttribute("brokerSC_0", "");
                    request.setAttribute("brokerSC_1", "");
                    request.setAttribute("brokerSC_2", "");
                    request.setAttribute("brokerSC_3", "");
                    request.setAttribute("brokerSC_4", "");

                    request.setAttribute("statementsFC_0", "");
                    request.setAttribute("statementsFC_1", "");
                    request.setAttribute("statementsTC_0", "");
                    request.setAttribute("statementsTC_1", "");
                    request.setAttribute("statementsTC_2", "");
                    request.setAttribute("statementsTC_3", "");
                    request.setAttribute("statementsTC_4", "");

                    request.setAttribute("benefitsFCReso_0", "");
                    request.setAttribute("benefitsFCReso_1", "");
                    request.setAttribute("benefitsFCReso_2", "");
                    request.setAttribute("benefitsFCReso_3", "");
                    request.setAttribute("benefitsFCReso_4", "");
                    request.setAttribute("benefitsFCReso_5", "");
                    request.setAttribute("benefitsFCSpec_0", "");
                    request.setAttribute("benefitsFCSpec_1", "");
                    request.setAttribute("benefitsFCSpec_2", "");
                    request.setAttribute("benefitsFCSpec_3", "");
                    request.setAttribute("benefitsFCSpec_4", "");
                    request.setAttribute("benefitsSC_0", "");
                    request.setAttribute("benefitsSC_1", "");
                    request.setAttribute("benefitsSC_2", "");
                    request.setAttribute("benefitsSC_3", "");
                    request.setAttribute("benefitsSC_4", "");
                    request.setAttribute("benefitsSC_5", "");
                    request.setAttribute("benefitsSC_6", "");
                    request.setAttribute("benefitsTC_0", "");
                    request.setAttribute("benefitsTC_1", "");
                    request.setAttribute("benefitsTC_2", "");
                    request.setAttribute("benefitsTC_3", "");
                    request.setAttribute("benefitsTC_4", "");
                    request.setAttribute("benefitsTC_5", "");
                    request.setAttribute("benefitsTC_6", "");
                    request.setAttribute("benefitsTC_7", "");
                    request.setAttribute("benefitsTC_8", "");
                    request.setAttribute("benefitsTC_9", "");
                    request.setAttribute("benefitsTC_10", "");
                    request.setAttribute("benefitsTC_11", "");
                    request.setAttribute("benefitsTC_12", "");

                    request.setAttribute("prenmiumsFC_0", "");
                    request.setAttribute("prenmiumsFC_1", "");
                    request.setAttribute("premiumsTC_0", "");
                    request.setAttribute("premiumsTC_1", "");
                    request.setAttribute("premiumsTC_2", "");
                    request.setAttribute("premiumsTC_3", "");
                    request.setAttribute("premiumsTC_4", "");
                    request.setAttribute("premiumsTC_5", "");
                    request.setAttribute("premiumsTC_6", "");

                    request.setAttribute("membershipFC_0", "");
                    request.setAttribute("membershipFC_1", "");
                    request.setAttribute("membershipFC_2", "");
                    request.setAttribute("memberShipTC_0", "");
                    request.setAttribute("memberShipTC_1", "");
                    request.setAttribute("memberShipTC_2", "");
                    request.setAttribute("memberShipTC_3", "");
                    request.setAttribute("memberShipTC_4", "");
                    request.setAttribute("memberShipTC_5", "");
                    request.setAttribute("memberShipTC_6", "");
                    request.setAttribute("memberShipTC_7", "");
                    request.setAttribute("memberShipTC_8", "");
                    request.setAttribute("memberShipTC_9", "");
                    request.setAttribute("memberShipTC_10", "");
                    request.setAttribute("memberShipTC_11", "");
                    request.setAttribute("memberShipTC_12", "");
                    request.setAttribute("memberShipTC_13", "");
                    request.setAttribute("memberShipTC_14", "");
                    request.setAttribute("memberShipTC_15", "");

                    request.setAttribute("serviceProviderFC_0", "");
                    request.setAttribute("serviceProviderFC_1", "");
                    request.setAttribute("serviceProviderFC_2", "");
                    request.setAttribute("serviceProviderFC_3", "");
                    request.setAttribute("serviceProviderFC_4", "");
                    request.setAttribute("serviceProviderFC_5", "");
                    request.setAttribute("serviceProviderFC_6", "");
                    request.setAttribute("serviceProviderTC_0", "");
                    request.setAttribute("serviceProviderTC_1", "");
                    request.setAttribute("serviceProviderTC_2", "");

                    request.setAttribute("chronicFC_0", "");
                    request.setAttribute("chronicFC_1", "");
                    request.setAttribute("chronicSC_0", "");
                    request.setAttribute("chronicSC_1", "");

                    request.setAttribute("oncologyFC_0", "");
                    request.setAttribute("oncologyFC_1", "");
                    request.setAttribute("oncologySC_1", "");
                    request.setAttribute("oncologySC_1", "");

                    request.setAttribute("wellcareFC_0", "");
                    request.setAttribute("wellcareFC_1", "");
                    request.setAttribute("wellcareSC_0", "");
                    request.setAttribute("wellcareSC_1", "");

                    request.setAttribute("foundationFC_0", "");
                    request.setAttribute("foundationFC_1", "");
                    request.setAttribute("foundationFC_2", "");
                    request.setAttribute("foundationSC_0", "");
                    request.setAttribute("foundationSC_1", "");
                    request.setAttribute("foundationSC_2", "");
                    request.setAttribute("foundationSC_3", "");
                    request.setAttribute("foundationSC_4", "");
                    request.setAttribute("foundationSC_5", "");
                    request.setAttribute("foundationSC_6", "");
                    //</editor-fold>

                    //</editor-fold>
            
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "WorkflowReloadCallLogCommand";
    }

}

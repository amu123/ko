/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.calltrack.command;

import com.koh.command.Command;
import com.koh.command.NeoCommand;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CallTrackPagingSearch;
import neo.manager.CallTrackSearchCriteria;
import neo.manager.CallWorkbenchDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author josephm
 */
public class CallWorkbenchCommand extends Command {
    private Object _datatypeFactory;
    private int  minResult;
    private int maxResult = 100;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

       // System.out.println("Inside the CallWorkbenchCommand");
        CallTrackSearchCriteria search = new CallTrackSearchCriteria();
        HttpSession session = request.getSession();
        SimpleDateFormat dateTime = new SimpleDateFormat("yyyy/MM/dd");
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        Collection<CallWorkbenchDetails> details = (Collection<CallWorkbenchDetails>) session.getAttribute("HistoryCallTrackDetails");
        String client = String.valueOf(context.getAttribute("Client"));

        String pageDirection = request.getParameter("pageDirection");
        String dateFrom = request.getParameter("dateFrom");
        String dateTo = request.getParameter("dateTo");
        CallTrackPagingSearch page = new CallTrackPagingSearch();
        if(page.getMaxResult() == 0){
            page.setMaxResult(maxResult);
        }
        
         try {
            if(dateFrom != null && !dateFrom.equalsIgnoreCase("")){
            Date callDateFrom = dateTime.parse(dateFrom);
            
            page.setDateFrom(convertDateXML(callDateFrom));
            request.setAttribute("dateFrom", page.getDateFrom());
            }else if(request.getAttribute("dateFrom") != null) {
                
                XMLGregorianCalendar xmlDateFrom = (XMLGregorianCalendar) request.getAttribute("dateFrom");
            
                page.setDateFrom(xmlDateFrom);
            }
            if(dateTo != null && !dateTo.equalsIgnoreCase("")) {
            
               Date callDateTo = dateTime.parse(dateTo); 
               page.setDateTo(convertDateXML(callDateTo));
               request.setAttribute("dateTo", page.getDateTo());
            } else if(request.getAttribute("dateTo") != null) {
            
                XMLGregorianCalendar xmlDateTo = (XMLGregorianCalendar) request.getAttribute("dateTo");
                
                page.setDateTo(xmlDateTo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(pageDirection != null && pageDirection.equalsIgnoreCase("forward")) {
        
            minResult = minResult + 100;
            maxResult = maxResult + 100;
            page.setMinResult(minResult);
            page.setMaxResult(maxResult);
        }
        
        if(pageDirection != null && pageDirection.equalsIgnoreCase("backward")) {
        
            minResult = minResult - 100;
            maxResult = maxResult - 100;
            page.setMaxResult(maxResult);
            page.setMinResult(minResult);

        }
        
        List<CallWorkbenchDetails> cwList = port.getCallWorkBenchDetails(page);
        session.setAttribute("WorkbenchDetails", cwList);
              
        try {
            String nextJSP = "/Calltrack/CallTrackWorkbench.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        }
        catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }
    
    
    public XMLGregorianCalendar convertDateXML(Date date) {

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getDatatypeFactory().newXMLGregorianCalendar(calendar);

    }

    private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                ex.printStackTrace();
            }
        }

        return (DatatypeFactory) _datatypeFactory;
    }

    @Override
    public String getName() {
        return "CallWorkbenchCommand";
    }
}

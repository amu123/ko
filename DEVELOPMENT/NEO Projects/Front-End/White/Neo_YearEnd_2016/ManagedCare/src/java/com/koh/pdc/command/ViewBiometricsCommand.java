/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class ViewBiometricsCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        session.setAttribute("BioTypeSelected", "null");
        String coverNumber = (String) session.getAttribute("policyNumber");

        NeoManagerBean port = service.getNeoManagerBeanPort();

        List<CoverDetails> depList = service.getNeoManagerBeanPort().getCoverDetailsByCoverNumber(coverNumber);
        session.setAttribute("eAuthCoverList", depList);

        try {
            String nextJSP = "/PDC/BiometricDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewBiometricsCommand";
    }
}

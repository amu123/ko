/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author johanl
 */
public class AuthBasketTariffDisplay extends TagSupport {

    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        ServletContext sctx = pageContext.getServletContext();
        String client = String.valueOf(sctx.getAttribute("Client"));
        String updateAuthNum = (String) session.getAttribute("updateAuthNumber");
        
        try {
            if (javaScript != null) {
                out.println("<td colspan=\"5\"><table class=\"" + javaScript + "\" style=\"cellpadding=\"5\"; cellspacing=\"0\";\">");
            } else {
                out.println("<td colspan=\"5\"><table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            }
            out.println("<tr>"
                    + "<th align=\"left\">Discipline</th>"
                    + "<th align=\"left\">Tariff Code</th>"
                    + "<th align=\"left\">Frequency</th>"
                    //+ "<th align=\"left\">Amount</th>"
                    + "<th align=\"left\">Tariff Desc</th>");
            
            if(client.equalsIgnoreCase("Sechaba")){
               out.println("<th align=\"left\">Available</th>");
            }
            
            out.println("</tr>");

            ArrayList<AuthTariffDetails> atList = (ArrayList<AuthTariffDetails>) session.getAttribute(sessionAttribute);
            double totalTAmount = 0.0d;
            String totalTariffAmount = "";
            if (atList != null && !atList.isEmpty()) {
                for (int i = 1; i <= atList.size(); i++) {
                    AuthTariffDetails ab = atList.get(i - 1);

                    //totalTAmount += ab.getAmount();

                    String provDesc = ab.getProviderType();

                    if (ab.getSpecificTariffInd() == 1) {
                        provDesc = NeoCommand.service.getNeoManagerBeanPort().getValueFromCodeTableForTableId(59, ab.getProviderType());
                    }

                    out.println("<tr id=\"tariffDisplay1" + i + i + "\">"
                            + "<td><label class=\"label\">" + provDesc + "</label></td> "
                            + "<td><label class=\"label\">" + ab.getTariffCode() + "</label></td> "
                            + "<td><label class=\"label\">" + ab.getFrequency() + "</label></td> "
                            //+ "<td><label class=\"label\">" + ab.getAmount() + "</label></td> "
                            + "<td width=\"350\"><label class=\"label\">" + ab.getTariffDesc() + "</label></td> ");
                    
                    if(client.equalsIgnoreCase("Sechaba")){
                        if(updateAuthNum != null && !updateAuthNum.equals("") && !updateAuthNum.equalsIgnoreCase("null")){
                            out.println("<td><label class=\"label\">" + ab.getFreqAvailable() + "</label></td> ");   
                        }else{
                            out.println("<td><label class=\"label\">" + ab.getFrequency() + "</label></td> ");   
                        }
                    }        
                    out.println("</tr>");


                }
                /*totalTariffAmount = new DecimalFormat("#.##").format(totalTAmount);
                out.println("<tr><td colspan=\"5\"></td></tr>");
                out.println("<tr>"
                        + "<th colspan=\"2\" align=\"left\">Total tariff amount :</td>"
                        + "<th colspan=\"2\" align=\"left\">" + totalTariffAmount + "</td>"
                        + "</tr>");*/
            }
            out.println("</table></td>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberSearchTable tag", ex);
        }

        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

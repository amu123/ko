/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import agility.za.documentprinting.*;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import neo.manager.NeoUser;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class PrintBulkCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    @SuppressWarnings("UnusedAssignment")
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------In PrintBulkCommand---------------------");
            String displayScheme = request.getParameter("displayScheme");
            String displayOption = request.getParameter("displayOption");
            String displayDay = request.getParameter("displayDay");
            String displayGroup = request.getParameter("displayGroup");
            String displayType = request.getParameter("displayType");

            logger.info("displayScheme = " + displayScheme);
            logger.info("displayOption = " + displayOption);
            logger.info("displayDay = " + displayDay);
            logger.info("displayGroup = " + displayGroup);
            logger.info("displayType = " + displayType);

            DocumentPrintingRequest req = new DocumentPrintingRequest();
            PrintFilter filter = new PrintFilter();

            if (displayOption == null || displayOption.equals("") || displayOption.equalsIgnoreCase("ACTIVE")) {
                displayOption = "active";
                filter.setStatus("ACTIVE");
                req.setFilter(filter);
            } else if (displayOption.equalsIgnoreCase("FAILED")) {
                filter.setStatus("FAILED");
                req.setFilter(filter);
            } else if (displayOption.equalsIgnoreCase("SUCCESS")) {
                filter.setStatus("SUCCESS");
                req.setFilter(filter);
            }

            if (displayDay == null || displayDay.equals("")) {
                displayDay = "today";
                filter.setFromDate(addDay(0));
                req.setFilter(filter);
            } else if (displayDay.equals("all")) {
                //Do nothing
            } else if (displayDay.equals("today")) {
                filter.setFromDate(addDay(0));
                req.setFilter(filter);
            } else if (displayDay.equals("yesterday")) {
                filter.setFromDate(addDay(-1));
                req.setFilter(filter);
            } else if (displayDay.equals("7days")) {
                filter.setFromDate(addDay(-7));
                req.setFilter(filter);
            } else if (displayDay.equals("30days")) {
                filter.setFromDate(addDay(-30));
                req.setFilter(filter);
            }

            if (displayGroup != null && displayGroup.equals("groupType")) {
                filter.setGroup(true);
                req.setFilter(filter);
            }
            
            if (displayType != null && !displayType.equals("")) {
                filter.setType(displayType);
                req.setFilter(filter);
            }

            if (displayScheme == null || displayScheme.equals("") || displayScheme.equals("All")) {
                displayScheme = "All";
            } else if (displayScheme.equals("1")) {
                filter.setSchemeId(1);
                req.setFilter(filter);
            } else if (displayScheme.equals("2")) {
                filter.setSchemeId(2);
                req.setFilter(filter);
            }
            
            req.setPrintProcessType("ViewPrintJobs");
            DocumentPrintingResponse res = NeoCommand.service.getNeoManagerBeanPort().processPrintRequest(req);

            List<PrintingType> printqueueList = res.getPrintEntity();
            logger.info("Size of printqueueList : " + (printqueueList!=null ?printqueueList.size():"empty"));

            for (PrintingType printingType : printqueueList) {
                System.out.println("FileName: " + printingType.getFileName());

                if (printingType.getFileName() != null && !printingType.getFileName().trim().equals("")) {
                    logger.info("Build file to print");
                    req = new DocumentPrintingRequest();
                    NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
                    req.setAgent(loggedInUser.getUsername());
                    req.setPrintProcessType("PrintDoc");

                    req.setSrcPrintDrive(new PropertiesReader().getProperty("DocumentIndexIndexDrive"));
                    PrintDocType docType = new PrintDocType();
//                  docType.setFolder(new PropertiesReader().getProperty("PrintPoolFolder"));//removed after changes
                    docType.setFilename(printingType.getFileName());
                    docType.setPrintId(printingType.getPrintId());
                    logger.info("Print");
                    if (printingType.getFileName().contains("CFM")) {
                        docType.setPrinterName(new PropertiesReader().getProperty("CardPrinter"));
                    } else {
                        docType.setPrinterName(new PropertiesReader().getProperty("BulkPrinter"));
                    }
                    req.setPrintDoc(docType);
                    DocumentPrintingResponse resp = NeoCommand.service.getNeoManagerBeanPort().processPrintRequest(req);
                    if (resp.isIsSuccess()) {
                        logger.info("Print successful"); 
                    } else {
                        logger.error("Error printing .... " + resp.getMessage());
                        errorMessage(request, response, resp.getMessage());
                        return "PrintPoolCommand";
                    }
                } else {
                    logger.info("No filename");
                }
            }
            request.setAttribute("displayOption", displayOption);
            request.setAttribute("displayDay", displayDay);
            request.setAttribute("displayGroup", displayGroup);
            request.setAttribute("displayType", displayType);
            request.setAttribute("displayOption", displayOption);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/PrintPool.jsp");
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            ex.printStackTrace();
        }
        return "PrintBulkCommand";
    }

    private void errorMessage(HttpServletRequest request, HttpServletResponse response, String msg) throws Exception {
        request.setAttribute("errorMsg", msg);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/PrintPool.jsp");
        dispatcher.forward(request, response);
    }

    public XMLGregorianCalendar addDay(int add) {
        try {
            Calendar fromDate = Calendar.getInstance();
            fromDate.add(Calendar.DAY_OF_MONTH, add);
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(fromDate.getTime());
            DatatypeFactory fact = DatatypeFactory.newInstance();
            XMLGregorianCalendar cal = fact.newXMLGregorianCalendar(calendar);
            return cal;
        } catch (Exception e) {
            System.err.println(e);
        }
        return null;
    }

    @Override
    public String getName() {
        return "PrintBulkCommand";
    }
}
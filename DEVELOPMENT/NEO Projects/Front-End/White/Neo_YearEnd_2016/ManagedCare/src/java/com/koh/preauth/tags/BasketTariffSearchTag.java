/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import java.io.IOException;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author johanl
 */
public class BasketTariffSearchTag extends TagSupport {

    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        String provDesc = "" + session.getAttribute("providerDesc");
        String btCode = "" + session.getAttribute("btCode");
        String btDesc = "" + session.getAttribute("btDesc");
        String btFreq = "" + session.getAttribute("tariffFreq");
        String btOption = "" + session.getAttribute("tariffOption");

        if (provDesc == null || provDesc.equals("") || provDesc.equalsIgnoreCase("null")){
            provDesc = "";
        }

        if (btCode == null || btCode.equals("") || btCode.equalsIgnoreCase("null")){
            btCode = "";
        }
        if (btDesc == null || btDesc.equals("") || btDesc.equalsIgnoreCase("null")){
            btDesc = "";
        }
        if (btFreq == null || btFreq.equals("") || btFreq.equalsIgnoreCase("null")){
            btFreq = "";
        }

        try {
            out.println("<td colspan=\"5\">" +
                    "<table width=\"500\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");

            out.println("<tr><th align=\"left\">Discipline</th><th align=\"left\">Tariff Code</th><th align=\"left\">Tariff Desc</th><th align=\"left\">Frequency</th></tr>");
            out.println("<tr><form>" +
                    "<td>" + 
                        "<label id=\"disc\" class=\"label\">" + provDesc + "</label>" +
                        "<input type=\"hidden\" name=\"pcType\" value=\"" + provDesc + "\" />" +
                        "<input type=\"hidden\" name=\"btOption\" value=\"" + btOption + "\" />" +
                    "</td>" +

                    "<td>" +
                        "<label id=\"code\" class=\"label\">" + btCode + "</label>" +
                        "<input type=\"hidden\" name=\"basketCode\" value=\"" + btCode + "\" />" +
                    "</td>" +

                    "<td width=\"300\">" +
                        "<label id=\"desc\" class=\"label\">" + btDesc + "</label>" +
                        "<input type=\"hidden\" name=\"basketDesc\" value=\"" + btDesc + "\" />" +
                    "</td>" +
                    
                    "<td>" +
                        "<input type=\"text\" id=\"tariffFreq\" name=\"tariffFreq\" size=\"15\" value=\""+ btFreq +"\" onChange=\"validateFreq(this.value);\" /></td>" +

                    "</tr>");

            //ERROR MSG FIELD
            String error = "" + session.getAttribute("basketAddError");
            if(error.equalsIgnoreCase("") || error.equalsIgnoreCase("null")){
                error = "";
            }

            out.println("<tr>" +
                    "<td colspan=\"3\" align=\"left\"><label id=\"btGrid_error\" class=\"error\">"+error+"</label></td>" +
                    "<td align=\"right\"><button type=\"button\" " + javaScript + " name=\"opperation\" value=\"" + commandName + "\">Add Tariff</button></td></form></tr>");

            out.println("</table></td>");

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

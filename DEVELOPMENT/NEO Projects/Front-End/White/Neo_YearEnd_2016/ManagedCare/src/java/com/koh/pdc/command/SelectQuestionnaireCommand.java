/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.Command;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author princes
 */
public class SelectQuestionnaireCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        System.out.println("entered SelectQuestionnaireCommand");

        /*String coverNumber = (String) session.getAttribute("policyNumber");
        Integer depenNo = (Integer) session.getAttribute("dependantNumber");
        int dependentNumber = depenNo;
        //List<PdcAnswer> questionnaires = port.getQuestionnairesFilledByPatient(coverNumber, dependentNumber);*/
        session.removeAttribute("dates");
        session.removeAttribute("dropDownDates");
        session.removeAttribute("sessionHash");
        session.removeAttribute("catched");

        //session.setAttribute("dropDownDates", questionnaires);
        try {
            String nextJSP = "/PDC/SelectQuestionnaire.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "SelectQuestionnaireCommand";
    }
}

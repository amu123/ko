/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.Command;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.PreAuthDetails;

/**
 *
 * @author princes
 */
public class ViewAuthTariffDetailsCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        //System.out.println("entered AlllocateCallHistoryToSessionCommand");
        String in = request.getParameter("listIndex");
        session.setAttribute("listIndex", in);
        List<PreAuthDetails> list = (List<PreAuthDetails>) session.getAttribute("authDetails");
        if (in != null) {
            int index = new Integer(in);
            System.out.println("intdex " + index);

            //System.out.println("Size : In Command" + listClaims.size());
            PreAuthDetails selectedClaim = list.get(index);
            session.setAttribute("tariffListArray", selectedClaim.getAuthTariffs());
        }
        try {
            String nextJSP = "/PDC/ClaimsHistory.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "ViewAuthTariffDetailsCommand";
    }
}

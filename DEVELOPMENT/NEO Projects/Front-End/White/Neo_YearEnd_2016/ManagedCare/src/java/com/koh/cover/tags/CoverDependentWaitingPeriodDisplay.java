/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.tags;

import javax.servlet.jsp.tagext.TagSupport;
import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthCoverExclusions;
import neo.manager.CoverDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;

/**
 *
 * @author kirsteno
 */
public class CoverDependentWaitingPeriodDisplay extends TagSupport {

    private String commandName;
    private String javaScript;
    private String sessionAttribute;
    private String onScreen;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

        try {
            List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute(sessionAttribute);

            if (cdList != null && cdList.isEmpty() == false) {

                out.println("<table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.println("<tr>"
                        + "<th align=\"left\">Dependant Code</th>"
                        + "<th align=\"left\">Dependant Name</th>"
                        + "<th align=\"left\">Dependant Surname</th>"
                        + "<th align=\"left\">Birth Date</th>"
                        + "<th align=\"left\">Start Date</th>"
                        + "<th align=\"left\">End Date</th>"
                        + "<th align=\"left\">PMB</th>"
                        + "</tr>");

                int counter = 0;
                List<LookupValue> values = port.getCodeTable(67);
                LookupValue lookupValue = new neo.manager.LookupValue();
                for (CoverDetails cd : cdList) {
                    //set cover details 
                    String dateOfBirth = "";
                    String wStartDate = "";
                    String wEndDate = "";
                    
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

                    String depCode = "" + cd.getDependentNumber();

                    //dob
                    if (cd.getDateOfBirth() != null) {
                        dateOfBirth = dateFormat.format(cd.getDateOfBirth().toGregorianCalendar().getTime());
                    }
                    if (cd.getPenaltyLateJoin().equals("0")) {
                        cd.setPenaltyLateJoin("None");
                    }

                    List<AuthCoverExclusions> underwriting = port.getActiveCoverExclusionsByCoverDependant(cd.getCoverNumber(), depCode);
                    if (underwriting != null && underwriting.size() > 0) {
                        for (AuthCoverExclusions ce : underwriting) {
                            if (ce.getExclusionType().equals("4")) {
                                if (ce.getExclusionDateFrom() != null) {
                                    wStartDate = dateFormat.format(ce.getExclusionDateFrom().toGregorianCalendar().getTime());
                                }

                                if (ce.getExclusionDateFrom() != null) {
                                    wEndDate = dateFormat.format(ce.getExclusionDateTo().toGregorianCalendar().getTime());
                                }

                                String wPMB = ce.getPmbAllowed();
                                System.out.println("wPMB = " + wPMB);
                                String pmb = "No";
                                if (!wPMB.isEmpty()) {
                                    pmb = wPMB;
                                }
                                System.out.println("pmb = "+pmb);
                                out.println("<tr>"
                                        + "<td><label class=\"label\">" + cd.getDependentNumber() + "</label></td>"
                                        + "<td><label class=\"label\">" + cd.getName() + "</label></td>"
                                        + "<td><label class=\"label\">" + cd.getSurname() + "</label></td>"
                                        + "<td><label class=\"label\">" + dateOfBirth + "</label></td>"
                                        + "<td><label class=\"label\">" + wStartDate + "</label></td>"
                                        + "<td><label class=\"label\">" + wEndDate + "</label></td>"
                                        + "<td><label class=\"label\">" + pmb + "</label></td>");
                                out.println("</tr>");
                            }
                        }


                    } else {
                        
                        out.println("<tr>"
                                + "<td><label class=\"label\">" + cd.getDependentNumber() + "</label></td>"
                                + "<td><label class=\"label\">" + cd.getName() + "</label></td>"
                                + "<td><label class=\"label\">" + cd.getSurname() + "</label></td>"
                                + "<td><label class=\"label\">" + dateOfBirth + "</label></td>"
                                + "<td><label class=\"label\">" + wStartDate + "</label></td>"
                                + "<td><label class=\"label\">" + wEndDate + "</label></td>"
                                + "<td><label class=\"label\">No</label></td>");
                    }
                    out.println("</tr>");
                    counter++;
                }
                out.println("</table>");

            }
        } catch (java.io.IOException ex) {
            throw new JspException("Error in CoverDependentWaitingPeriodDisplay tag", ex);
        }

        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setOnScreen(String onScreen) {
        this.onScreen = onScreen;
    }
}

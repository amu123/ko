/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application;

import com.koh.employer.command.TabUtils;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class MemberAppSpecificDetailsMapping {

    private static final String[][] FIELD_MAPPINGS = {
        {"memberAppNumber","Application Number","no", "int", "MemberApp","ApplicationNumber"},
        {"memberAppSpecId","Question","no", "int", "MemberApp","specificDetailId"},
        {"Disorder_question","Question","yes", "int", "MemberApp","questionNumber"},
        {"Disorder_dependent","Dependant","yes", "int", "MemberApp","dependentNumber"},
        {"Disorder_date","Date","yes", "date", "MemberApp","specificDate"},
        {"Disorder_treatment","Treatment","yes", "str", "MemberApp","treatment"},
        {"Disorder_doctor","Consulting Doctor","yes", "str", "MemberApp","doctor"},
        {"Disorder_condition","Current Condition","yes", "int", "MemberApp","conditionInd"},
        {"Disorder_diagCode_text","ICD 10 Code","yes", "str", "MemberApp","diagCode"}
    };
    
    private static final Map<String,String[]> FIELD_MAP;

    static {
        HashMap hm = new HashMap<String,String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }

    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
        return errors;
    }

    public static MemAppSpecificDetail getMemAppSpecificDetail(HttpServletRequest request,NeoUser neoUser) {
        MemAppSpecificDetail ma = (MemAppSpecificDetail)TabUtils.getDTOFromRequest(request, MemAppSpecificDetail.class, FIELD_MAPPINGS, "MemberApp");
        ma.setSecurityGroupId(neoUser.getSecurityGroupId());
        ma.setLastUpdatedBy(neoUser.getUserId());
        ma.setCreatedBy(neoUser.getUserId());
        return ma;
    }

    public static void setMemAppSpecificDetail(HttpServletRequest request, MemAppSpecificDetail memAppSpecificDetail) {
        TabUtils.setRequestFromDTO(request, memAppSpecificDetail, FIELD_MAPPINGS, "MemberApp");
    }
    
}

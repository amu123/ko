package com.koh.workflow.command;

import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import java.io.*;
import java.net.MalformedURLException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author gerritr
 */
public class WorkflowEmailViewCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        logger.info("Inside WorkflowEmailViewCommand");
        
        try {
                String fileLocation = request.getParameter("fileLocation");
                logger.info("fileLocation = "+fileLocation);
                if(fileLocation == null || fileLocation.equals("")){
                    printErrorResult(request, response);
                    return null;
                }
                
                File file = new File(fileLocation);
                InputStream in = new FileInputStream(file);
                int contentLength = (int) file.length();
                logger.info("The content length is " + contentLength);
                ByteArrayOutputStream temporaryOutput;
                if (contentLength != -1) {
                    temporaryOutput = new ByteArrayOutputStream(contentLength);
                } else {
                    temporaryOutput = new ByteArrayOutputStream(20480);
                }

                byte[] bufer = new byte[512];
                while (true) {

                    int len = in.read(bufer);
                    if (len == -1) {

                        break;
                    }
                    temporaryOutput.write(bufer, 0, len);
                }
                in.close();
                temporaryOutput.close();

                byte[] array = temporaryOutput.toByteArray();
                printPreview(request, response, array, fileLocation);
            
           

        } catch (MalformedURLException ex) {

            ex.printStackTrace();
        } catch (IOException ioex) {
            // FileNotFound exception
            printErrorResult(request, response);
            logger.info(ioex.getMessage());
            System.out.println("The ioex.getMessage() " + ioex.getMessage());
            ioex.printStackTrace();
        }

        return null;
    }

    public void printPreview(HttpServletRequest request, HttpServletResponse response, byte[] array, String fileName) throws IOException {
        OutputStream outStream = null;
        try {

            response.setHeader("Pragma", "no-cache"); //HTTP 1.0
            response.setDateHeader("Expires", 0); //prevents caching at the proxy server
            response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-disposition", "attachment; filename=" + fileName);

            response.setContentType("application/pdf");
            response.setContentLength(array.length);
            outStream = response.getOutputStream();
            outStream.write(array);
            outStream.flush();
            outStream.close();

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void printResult(HttpServletRequest request, HttpServletResponse response, boolean result) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (result) {
                out.println("<td><label class=\"header\">...</label></td>");
            } else {
                out.println("<td><label class=\"header\">Document Generation failed.</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    public void printErrorResult(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");

            out.println("<td><label class=\"header\">No records found.</label></td>");

            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "2; URL=/ManagedCare/Statement/MemberDocumentsView.jsp");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    @Override
    public String getName() {

        return "WorkflowEmailViewCommand";
    }
}

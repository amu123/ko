/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.OtherBiometrics;

/**
 *
 * @author nick
 */
public class BiometricsOtherTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String javascript;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
//        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

        List<OtherBiometrics> getOther = (List<OtherBiometrics>) session.getAttribute("getOther");
        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        String dateMeasured;

        try {
            out.print("<tr>");
            out.print("<th scope=\"col\">Date Measured</th>");
            out.print("<th scope=\"col\">Height</th>");
            out.print("<th scope=\"col\">Weight</th>");
            out.print("<th scope=\"col\">Notes</th>");
            out.print("<th scope=\"col\">Select</th>");
            out.print("</tr>");

            if (getOther != null && getOther.size() > 0) {
                for (OtherBiometrics a : getOther) {
                    out.print("<tr>");
                    if (a.getDateMeasured() != null) {
                        Date mDate = a.getDateMeasured().toGregorianCalendar().getTime();
                        dateMeasured = formatter.format(mDate);
                        out.print("<td><center><label class=\"label\">" + dateMeasured + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (a.getHeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + a.getHeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (a.getWeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + a.getWeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (a.getDetail() != null) {
                        out.print("<td><center><label class=\"label\">" + a.getDetail() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("<td><center><button name=\"opperation\" type=\"button\" " + javascript + " value=\"" + a.getBiometricsId() + "\">Select</button></center></td>");
                    out.print("</tr>");
                }
            }

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }

//    COMMENTED OUT TO PROVIDE NEW LOOK
//    @Override
//    public int doEndTag() throws JspException {
//        JspWriter out = pageContext.getOut();
//        HttpSession session = pageContext.getSession();
//        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
//        
//        List<OtherBiometrics> getOther = (List<OtherBiometrics>) session.getAttribute("getOther");
//        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
//        String dateMeasured = "";
//        
//        
//        if(getOther != null && !getOther.isEmpty()){
//            for (OtherBiometrics ob : getOther) {
//                String cdlName = port.getValueFromCodeTableForTableId(233, ob.getCdl());
//                String icdName = port.getValueFromCodeTableForTableId(Integer.parseInt(ob.getCdl()), ob.getIcd10());
//            }
//        }
//        
//        try {
//            out.print("<tr>");
//            out.print("<th scope=\"col\">Date Measured</th>");
//            out.print("<th scope=\"col\">CDL</th>");
//            out.print("<th scope=\"col\">ICD10</th>");
//            out.print("<th scope=\"col\">Exercise</th>");
//            out.print("<th scope=\"col\">Current Smoker</th>");
//            out.print("<th scope=\"col\">Ex Smoker Stopped</th>");
//            out.print("<th scope=\"col\">Alcohol Units</th>");
//            out.print("<th scope=\"col\">Test Results</th>");
//            out.print("<th scope=\"col\">Notes</th>");
//            out.print("<th scope=\"col\">Select</th>");
//            out.print("</tr>");
//            
//             if (getOther != null && getOther.size() > 0) {
//                for (OtherBiometrics a : getOther) {
//
//                    String cdlName = port.getValueFromCodeTableForTableId(233, a.getCdl());
//                    String icdName = port.getValueFromCodeTableForTableId(Integer.parseInt(a.getCdl()), a.getIcd10());
//                    
//                    out.print("<tr>");
//                    if (a.getDateMeasured() != null) {
//                        Date mDate = a.getDateMeasured().toGregorianCalendar().getTime();
//                        dateMeasured = formatter.format(mDate);
//                        out.print("<td><center><label class=\"label\">" + dateMeasured + "</label></center></td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (cdlName != null) {                        
//                        out.print("<td><center><label class=\"label\">" + cdlName + "</label></center></td>");
//                    } else {
//                        out.print("<td></td>");
//                    } 
//                    if (icdName != null) {
//                        out.print("<td><center><label class=\"label\">" + icdName + "</label></center></td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (a.getExercisePerWeek() != 0) {
//                        out.print("<td><center><label class=\"label\">" + a.getExercisePerWeek() + "</label></center></td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (a.getCurrentSmoker() != null && a.getCurrentSmoker().equalsIgnoreCase("1")) {
//                        out.print("<td><center><label class=\"label\">" + "Yes" + "</label></center></td>");
//                    } else if(a.getCurrentSmoker() != null && a.getCurrentSmoker().equalsIgnoreCase("2")) {
//                        out.print("<td><center><label class=\"label\">" + "No" + "</label></center></td>");              
//                    } else{
//                        out.print("<td></td>");
//                    }
//                    if (a.getYearsSinceStopped() > 0) {
//                        out.print("<td><center><label class=\"label\">" + a.getYearsSinceStopped() + "</label></center></td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (a.getAlcoholUnitsPerWeek() != 0) {
//                        out.print("<td><center><label class=\"label\">" + a.getAlcoholUnitsPerWeek() + "</label></center></td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (a.getTestResult() != 0.0) {
//                        out.print("<td><center><label class=\"label\">" + a.getTestResult() + "</label></center></td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (a.getDetail() != null) {
//                        out.print("<td><center><label class=\"label\">" + a.getDetail() + "</label></center></td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    out.print("</tr>");
//                    out.print("<td><center><button name=\"opperation\" type=\"button\" " + javascript + " value=\"" + a.getBiometricsId() + "\">Select</button></center></td>");
//                }
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return super.doEndTag();
//    }
    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

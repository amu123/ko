/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthTariffDetails;
import neo.manager.Basket;
import neo.manager.BasketItem;
import neo.manager.LookupType;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.TariffCode;

/**
 *
 * @author johanl
 */
public class GetAuthBasketDetails extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        PrintWriter out = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        NeoManagerBean port = service.getNeoManagerBeanPort();

        try {
            out = response.getWriter();

            LookupValue authType = new LookupValue();
            String authTypeValue = port.getValueFromCodeTableForId("Auth Type", "5");
            authType.setId("5");
            authType.setValue(authTypeValue);

            String authSub = request.getParameter("authSub");
            String authSubValue = port.getValueFromCodeTableForId("Condition Sub Type", authSub);
            LookupType authSubType = new LookupType();
            authSubType.setId(Integer.parseInt(authSub));
            authSubType.setValue(authSubValue);

            String year = request.getParameter("planYear");
            String planYearValue = port.getValueFromCodeTableForId("Enrolment Year", year);
            LookupValue planYear = new LookupValue();
            planYear.setId(year);
            planYear.setValue(planYearValue);

            int prodId = Integer.parseInt(session.getAttribute("scheme").toString());
            int optId = Integer.parseInt(session.getAttribute("schemeOption").toString());
            String optName = "" + session.getAttribute("schemeOptionName");

            Basket authBasket = port.getBasketForTypeAndProduct(authType, authSubType, planYear, prodId, optId);
            if (authBasket != null) {

                session.setAttribute("ModifiedBasketID", authBasket.getBasketId());
                session.setAttribute("ModiifiedBasketYear", authBasket.getBasketYear().getId());
                List<BasketItem> basketItems = authBasket.getBasketItems();
                List<AuthTariffDetails> basketTariffList = new ArrayList<AuthTariffDetails>();
                List<AuthTariffDetails> basketNappiList = new ArrayList<AuthTariffDetails>();

                for (BasketItem b : basketItems) {
                    AuthTariffDetails bTariff = new AuthTariffDetails();
                    //AuthTariffDetails nTariff = new AuthTariffDetails();
                    String dos = "";

                    //get bsket tariff desc
                    String provCat = b.getDiscipline();
                    String code = b.getCode();
                    String basketType = b.getCodeType().getId();
                    //System.out.println("basket type for code " + code + " = " + basketType);

                    if (basketType.equals("2")) {

                        //get neo rpl
                        String provTypeId = "" + session.getAttribute("treatingProviderDiscTypeId");
                        int pId = Integer.parseInt("" + session.getAttribute("scheme"));
                        int oId = Integer.parseInt("" + session.getAttribute("schemeOption"));
                        XMLGregorianCalendar authDate = DateTimeUtils.convertDateToXMLGregorianCalendar(sdf.parse("" + session.getAttribute("authDate")));
                        String provNum = "" + session.getAttribute("treatingProvider_text");
                        String icd = "" + session.getAttribute("primaryICD_text");
                        List<String> icd10 = new ArrayList<String>();
                        icd10.add(icd);

                        TariffCode t = port.getTariffCode(pId, oId, authDate, b.getDiscipline(), provNum, provNum, icd10, null, code);
                        String desc = "";
                        double amount = 0.0d;
                        if (t != null) {
                            desc = t.getDescription();
                            amount = t.getPrice();
                        }

                        bTariff.setProviderType(b.getDiscipline());
                        bTariff.setTariffCode(b.getCode());
                        bTariff.setTariffDesc(desc);
                        bTariff.setTariffType(basketType);
                        bTariff.setFrequency(b.getFrequency());
                        bTariff.setAmount(amount);

                        basketTariffList.add(bTariff);

                    } /*else if (b.getCodeType().getId().equals("3")) {
                     XMLGregorianCalendar authDate = null;
                     try {
                     authDate = DateTimeUtils.convertDateToXMLGregorianCalendar(sdf.parse("" + session.getAttribute("authDate")));
                     } catch (ParseException ex) {
                     ex.printStackTrace();
                     }
                     List<AuthTariffDetails> atList = port.getNappiDetailsByCriteria(code, "", authDate);
                     String napDesc = atList.get(0).getTariffDesc();

                     nTariff.setProviderType(b.getProviderCategory().getId());
                     nTariff.setTariffCode(b.getCode());
                     nTariff.setTariffDesc(napDesc);
                     nTariff.setFrequency(b.getFrequency());
                     nTariff.setAmount(b.getCodeAmount());
                     nTariff.setDosage(dos);
                     nTariff.setQuantity("");

                     basketNappiList.add(nTariff);

                     }*/
                }
                session.setAttribute("AuthBasketTariffs", basketTariffList);
                if(!basketTariffList.isEmpty()){
                    session.setAttribute("AuthBasketTariffsSize", basketTariffList.size());
                }else{
                    session.setAttribute("AuthBasketTariffsSize", "");
                }
                
                //session.setAttribute("AuthNappiTariffs", basketNappiList);
                System.out.println("basket tariff list size = " + basketTariffList.size());
                out.println("Done|");

            } else {
                out.println("Error|");
            }
        } catch (Exception ex) {
            System.out.println("basket load error : " + ex.getMessage());
            ex.printStackTrace();
            out.println("Error|");

        } finally {
            out.close();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetAuthBasketDetails";
    }
}

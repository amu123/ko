/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthTariffDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class SearchNewPreAuthNappi extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        this.saveScreenToSession(request);
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Collection<AuthTariffDetails> medList = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");


        String code = (String) session.getAttribute("code");
        String desc = (String) session.getAttribute("description");

        if (code.trim().equals("") && code.trim().equals("null")) {
            code = "";
        }
        if (desc.trim().equalsIgnoreCase("") && desc.trim().equalsIgnoreCase("null")) {
            desc = "";
        }

        XMLGregorianCalendar authDate = null;
        try {
            authDate = DateTimeUtils.convertDateToXMLGregorianCalendar(sdf.parse("" + session.getAttribute("authDate")));
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        medList = port.getNappiDetailsByCriteria(code, desc, authDate);

        if (medList != null) {
            System.out.println("medList size = " + medList.size());
            session.setAttribute("PreAuthNappiSearchList", medList);
            if (medList.size() == 0) {
                request.setAttribute("authNNSearchResultsMessage", "No results found.");
            } else if (medList.size() >= 100) {
                request.setAttribute("authNNSearchResultsMessage", "Only first 100 results shown. Please refine search.");
            } else {
                request.setAttribute("authNNSearchResultsMessage", null);
            }
            session.setAttribute("authNNSearchResults",medList);
        }else{
            request.setAttribute("authNNSearchResultsMessage", "No results found.");
        }



        try {
            String nextJSP = "/PreAuth/PreAuthNewNappiSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SearchNewPreAuthNappi";

    }
}

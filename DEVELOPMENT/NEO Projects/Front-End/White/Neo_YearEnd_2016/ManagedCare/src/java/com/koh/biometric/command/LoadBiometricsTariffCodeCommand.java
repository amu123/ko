/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.GenericTariff;



/**
 *
 * @author josephm
 */
public class LoadBiometricsTariffCodeCommand extends NeoCommand {
    private Object _datatypeFactory;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("Inside the LoadBiometricsTariffCodeCommand");
        HttpSession session = request.getSession();

        String tarrifCode = request.getParameter("tariffCode");

        GenericTariff general = service.getNeoManagerBeanPort().fetchTariffs(tarrifCode);
        try {
            PrintWriter out = response.getWriter();

            if (general != null) {

                out.print(getName() + "|TariffDescription=" +general.getTariffDescription() + "$");

            } else {

                out.print("Error|No such tariff|" + getName());
            }
           
        } catch (Exception ex) {

            System.out.println("Error in the command in LoadBiometricsTariffCodeCommand");
            ex.printStackTrace();
        }
        return null;
    }

     public XMLGregorianCalendar convertDateXML(Date date) {

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getDatatypeFactory().newXMLGregorianCalendar(calendar);

    }

    private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                ex.printStackTrace();
            }
        }

        return (DatatypeFactory) _datatypeFactory;
    }

    @Override
    public String getName() {
        return "LoadBiometricsTariffCodeCommand";
    }
}

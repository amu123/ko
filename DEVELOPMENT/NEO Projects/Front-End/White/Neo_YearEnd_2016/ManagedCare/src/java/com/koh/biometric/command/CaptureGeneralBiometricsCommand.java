/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.lang.Exception;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author josephm
 */
public class CaptureGeneralBiometricsCommand extends NeoCommand {
    private Object _datatypeFactory;
    private String nextJSP;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        HttpSession session = request.getSession();
        boolean checkError = false;
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        
        clearAllFromSession(request);

        NeoManagerBean port = service.getNeoManagerBeanPort();

        List<GenericTariff> atList = (ArrayList<GenericTariff>) session.getAttribute("biometricsTariffListArray");

        GeneralBiometrics general = new GeneralBiometrics();
         
        String exerciseInWeek = request.getParameter("exerciseQuestion");
        if (exerciseInWeek != null && !exerciseInWeek.equalsIgnoreCase("")) {
            if(exerciseInWeek.matches("[0-9]+")){
            int exerciseInWeekValue = Integer.parseInt(exerciseInWeek);
            general.setExercisePerWeek(exerciseInWeekValue);
            }else {
            
                session.setAttribute("exerciseQuestion_error", "The exercise field should be a numeric value");
                checkError = true;
            }
        } /*else {

             session.setAttribute("exerciseQuestion_error", "The exercise field should be filled in");
            checkError = true;
        }*/

        String currentSmoker = request.getParameter("currentSmoker");
        if (currentSmoker != null && !currentSmoker.equalsIgnoreCase("99")) {
            general.setCurrentSmoker(currentSmoker);
        } /*else {

             session.setAttribute("currentSmoker_error", "The current smoker field should be filled in");
            checkError = true;
        }*/

        String numberOfCigaretes = request.getParameter("smokingQuestion");
        if (numberOfCigaretes != null && !numberOfCigaretes.equalsIgnoreCase("")) {
            if(numberOfCigaretes.matches("[0-9]+")){
            int numberOfCigaretesValue = Integer.parseInt(numberOfCigaretes);
            general.setCigarettesPerDay(numberOfCigaretesValue);
            }else {
            
                session.setAttribute("smokingQuestion_error", "The cigarettes field should be a numeric value");
                checkError = true;
            }
        } /*else {

             session.setAttribute("smokingQuestion_error", "The cigarettes field should be filled in");
            checkError = true;
        }*/

        String exSmoker = request.getParameter("exSmoker");
        if (exSmoker != null && !exSmoker.equalsIgnoreCase("99")) {
            general.setExSmoker(exSmoker);
        } /*else {

             session.setAttribute("exSmoker_error", "The ex smoker field should be filled in");
            checkError = true;
        }*/

        String yearsSinceSmoking = request.getParameter("stopped");
        if (yearsSinceSmoking != null && !yearsSinceSmoking.equalsIgnoreCase("")) {
            if (yearsSinceSmoking.matches("[0-9]+")) {
                int yearsSinceSmokingValue = Integer.parseInt(yearsSinceSmoking);
                general.setYearsSinceStopped(yearsSinceSmokingValue);
                 session.setAttribute("stopped_error", "");
            } else {
                session.setAttribute("stopped_error", "The years stopped field should be a numeric value");
                checkError = true;
            }
        }

        String alcoholUnits = request.getParameter("alcoholConsumption");
        if (alcoholUnits != null && !alcoholUnits.equalsIgnoreCase("")) {
            if(alcoholUnits.matches("[0-9]+")){
            int alcoholUnitsValue = Integer.parseInt(alcoholUnits);
            general.setAlcoholUnitsPerWeek(alcoholUnitsValue);
            }else {
            
                session.setAttribute("alcoholConsumption_error", "The alcohol units field should be a numeric value");
                checkError = true;
            }
        } /*else {

             session.setAttribute("alcoholConsumption_error", "The alcohol units field should be filled in");
            checkError = true;
        }*/

        String weight = request.getParameter("weight");
        String height = request.getParameter("length");
        if (weight != null && !weight.equalsIgnoreCase("")) {
            if (weight.matches("([0-9]+(\\.[0-9]+)?)+")) {
                if (height != null && !height.equalsIgnoreCase("")) {
                    if (height.matches("([0-9]+(\\.[0-9]+)?)+")) {
                        double heightValue = Double.parseDouble(height);
                        double weightValue = Double.parseDouble(weight);
                        general.setHeight(heightValue);
                        general.setWeight(weightValue);
                    } else {
                        session.setAttribute("length_error", "The length should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("length_error", "Require length to calculate BMI");
                    checkError = true;
                }
            } else {

                session.setAttribute("weight_error", "The weight should be a numeric value");
                checkError = true;
            }
        }/*else {

         session.setAttribute("weight_error", "The weight should be filled in");
         checkError = true;
         }*/
        
        if (height != null && !height.equalsIgnoreCase("")) {
            if (height.matches("([0-9]+(\\.[0-9]+)?)+")) {
                if (weight != null && !weight.equalsIgnoreCase("")) {
                    if (weight.matches("([0-9]+(\\.[0-9]+)?)+")) {
                        double heightValue = Double.parseDouble(height);
                        double weightValue = Double.parseDouble(weight);
                        general.setHeight(heightValue);
                        general.setWeight(weightValue);
                    } else {
                        session.setAttribute("weight_error", "The weight should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("weight_error", "Require weight to calculate BMI");
                    checkError = true;
                }
            } else {

                session.setAttribute("length_error", "The length should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("length_error", "The length should be filled in");
         checkError = true;
         }*/

        String bmi = request.getParameter("bmi");
        if (bmi != null && !bmi.equalsIgnoreCase("")) {
            if(bmi.matches("([0-9]+(\\.[0-9]+)?)+")){
            double bmiValue = Double.parseDouble(bmi);
            general.setBmi(bmiValue);
            }else {
            
               session.setAttribute("bmi_error", "The BMI should be a numeric value");
               checkError = true; 
            }
        } /*else {

            session.setAttribute("bmi_error", "The BMI should be filled in");
            checkError = true;
        }*/
        
        String bloodPresureSystolic = request.getParameter("bpSystolic");
        String bloodPressureDiastolic = request.getParameter("bpDiastolic");
        if (bloodPresureSystolic != null && !bloodPresureSystolic.equalsIgnoreCase("")) {
            if (bloodPresureSystolic.matches("[0-9]+")) {
                if (bloodPressureDiastolic != null && !bloodPressureDiastolic.equalsIgnoreCase("")) {
                    if (bloodPressureDiastolic.matches("[0-9]+")) {
                        int bloodvalueSys = Integer.parseInt(bloodPresureSystolic);
                        int bloodValueDia = Integer.parseInt(bloodPressureDiastolic);
                        general.setBloodPressureSystolic(bloodvalueSys);
                        general.setBloodPressureDiastolic(bloodValueDia);
                    } else {

                        session.setAttribute("bpDiastolic_error", "The blood pressure should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("bpDiastolic_error", "The blood pressure should be filled in");
                    checkError = true;
                }
            } else {

                session.setAttribute("bpSystolic_error", "The blood pressure should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bpSystolic_error", "The blood pressure should be filled in");
         checkError = true;
         }*/
        
        if (bloodPressureDiastolic != null && !bloodPressureDiastolic.equalsIgnoreCase("")) {
            if (bloodPressureDiastolic.matches("[0-9]+")) {
                if (bloodPresureSystolic != null && !bloodPresureSystolic.equalsIgnoreCase("")) {
                    if (bloodPresureSystolic.matches("[0-9]+")) {
                        int bloodvalueSys = Integer.parseInt(bloodPresureSystolic);
                        int bloodValueDia = Integer.parseInt(bloodPressureDiastolic);
                        general.setBloodPressureSystolic(bloodvalueSys);
                        general.setBloodPressureDiastolic(bloodValueDia);
                    } else {

                        session.setAttribute("bpSystolic_error", "The blood pressure should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("bpSystolic_error", "The blood pressure should be filled in");
                    checkError = true;
                }
            } else {

                session.setAttribute("bpDiastolic_error", "The blood pressure should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bpDiastolic_error", "The blood pressure should be filled in");
         checkError = true;
         }*/

        String memberNumber = request.getParameter("memberNumber_text");

        if (memberNumber != null && !memberNumber.equalsIgnoreCase("")) {

            general.setMemberNumber(memberNumber);
        }else {

             session.setAttribute("memberNumber_error", "The member number should be filled in");
            checkError = true;
        }


        String details = request.getParameter("generalDetails");
        if(details != null && !details.equalsIgnoreCase("")) {

            general.setDetail(details);
        } /*else {

             session.setAttribute("generalDetails_error", "The detail should be filled in");
             checkError = true;
        }*/

        String dependantCode = request.getParameter("depListValues");
        if (dependantCode != null && !dependantCode.equalsIgnoreCase("99") && !dependantCode.equalsIgnoreCase("")
                && !dependantCode.equalsIgnoreCase("null")) {

            int code = Integer.parseInt(dependantCode);
            general.setDependantCode(code);
        } else {

            session.setAttribute("depListValues_error", "The cover dependants should be filled in");
            checkError = true;
        }

//         SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");

        try {
            String measuredDate = request.getParameter("dateMeasured");
            if (measuredDate != null && !measuredDate.equalsIgnoreCase("")) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                Pattern pattern = Pattern.compile("\\s");
                Matcher matcher = pattern.matcher(measuredDate);
                boolean found = matcher.find();

                Date tFrom = null;
                XMLGregorianCalendar xTreatFrom = null;

                if (!found) {
                    tFrom = dateFormat.parse(measuredDate);
                } else {
                    tFrom = dateTimeFormat.parse(measuredDate);
                }

                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);

//            Date dateMeasured = dateTimeFormat.parse(measuredDate);
                general.setDateMeasured(xTreatFrom);
            } else {
                session.setAttribute("dateMeasured_error", "The date measured should be filled in");
                checkError = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//         SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        try {
            String received = request.getParameter("dateReceived");
            if (received != null && !received.equalsIgnoreCase("")) {
//                 Date dateReceived = dateFormat.parse(request.getParameter("dateReceived"));
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                Pattern pattern = Pattern.compile("\\s");
                Matcher matcher = pattern.matcher(received);
                boolean found = matcher.find();

                Date tFrom = null;
                XMLGregorianCalendar xTreatFrom = null;

                if (!found) {
                    tFrom = dateFormat.parse(received);
                } else {
                    tFrom = dateTimeFormat.parse(received);
                }

                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);

                general.setDateReceived(xTreatFrom);
            } else {
                session.setAttribute("dateReceived_error", "The date received should be filled in");
                checkError = true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        /*String tariffCode = request.getParameter("tariffCode");
        if (tariffCode != null && !tariffCode.equalsIgnoreCase("")) {

            general.setTariffCode(tariffCode);
        }else {

             session.setAttribute("tariffCode_error", "The tarrif should be filled in");
             //checkError = true;
        }*/

        /*String testResults = request.getParameter("testResults");
        if(testResults != null && !testResults.equalsIgnoreCase("")) {
            double test = Double.parseDouble(testResults);
            general.setTestResult(test);
        }else {

             session.setAttribute("testResults_error", "The test should be filled in");
             //checkError = true;
        }*/
        
         String treatingProvider = request.getParameter("providerNumber_text");
        if (treatingProvider != null && !treatingProvider.equalsIgnoreCase("")) {

            general.setTreatingProvider(treatingProvider);
        } /*else {

              session.setAttribute("providerNumber_error", "The treating provider should be filled in");
            checkError = true;
        }*/
        
        String source = request.getParameter("source");
        if (source != null && !source.equalsIgnoreCase("99")) {
            general.setSource(source);
        } /*else {

             session.setAttribute("source_error", "The source should be filled in");
            checkError = true;
        }*/
        
        String icd10 = request.getParameter("code_text");
        if (icd10 != null && !icd10.equalsIgnoreCase("99")) {

            general.setIcd10(icd10);
        } /*else {

            session.setAttribute("code_error", "The ICD10 should be filled in");
            checkError = true;
        }*/
        
        try {
            if(checkError){
                saveScreenToSession(request);
                nextJSP = "/biometrics/CaptureGeneral.jsp";
            }else if(session.getAttribute("fromPDC") != null) {
                nextJSP = "/PDC/PolicyHolderDetails.jsp";
            }
            else{
                nextJSP = "/biometrics/CaptureGeneral.jsp";
                int userId = user.getUserId();
                BiometricsId bId = port.saveGeneralBiometrics(general, atList, userId, 1, null);
                request.setAttribute("revNo",bId.getReferenceNo());
                int generalId = bId.getId();
                clearAllFromSession(request);
                if(generalId > 0) {
                    session.setAttribute("updated", "Your biometrics data has been captured");
                }else{
                    session.setAttribute("failed", "Sorry! General Biometrics save has failed, Please try again!");
                    nextJSP = "/biometrics/CaptureGeneral.jsp";
                }
            }
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

//    public XMLGregorianCalendar convertDateXML(Date date) {
//
//        GregorianCalendar calendar = new GregorianCalendar();
//        calendar.setTime(date);
//        return getDatatypeFactory().newXMLGregorianCalendar(calendar);
//
//    }
//
//    private DatatypeFactory getDatatypeFactory() {
//        if (_datatypeFactory == null) {
//
//            try {
//                _datatypeFactory = DatatypeFactory.newInstance();
//            } catch (DatatypeConfigurationException ex) {
//                ex.printStackTrace();
//            }
//        }
//
//        return (DatatypeFactory) _datatypeFactory;
//    }

    @Override
    public String getName() {
        return "CaptureGeneralBiometricsCommand";
    }
}

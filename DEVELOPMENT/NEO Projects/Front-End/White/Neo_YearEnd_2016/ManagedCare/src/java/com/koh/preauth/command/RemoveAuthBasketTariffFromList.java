/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author johanl
 */
public class RemoveAuthBasketTariffFromList extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession ses = request.getSession();
        PrintWriter out = null;

        String btHidden = request.getParameter("btHidden");
        String btValues[] = btHidden.split("\\|");
        String code = btValues[0];
        String desc = btValues[1];
        int freq = new Integer(btValues[2]);
        List<AuthTariffDetails> basketList = (List<AuthTariffDetails>) ses.getAttribute("AuthBasketTariffs");
        try {
            out = response.getWriter();

            for (AuthTariffDetails bt : basketList) {
                if (bt.getTariffCode().equals(code)) {
                    if (bt.getTariffDesc().equalsIgnoreCase(desc)) {
                        if (bt.getFrequency() == freq) {
                            String provCat = service.getNeoManagerBeanPort().getValueFromCodeTableForTableId(59, bt.getProviderType());
                            System.out.println("modified prov cat = "+provCat);
                            basketList.remove(bt);
                            out.println("Done|");

                            break;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("RemoveAuthBasketTariffFromList error : "+ex.getMessage());
            out.println("Error|");
        }finally{
            out.close();
        }
        return null;
    }

    @Override
    public String getName() {
        return "RemoveAuthBasketTariffFromList";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.util.Collection;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.AuthNappiProduct;

/**
 *
 * @author martins
 */
public class AuthGetNappiList extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String prodID = session.getAttribute("productCode").toString();
        String prodName = session.getAttribute("productName").toString();
        String code = "%" + prodID + "%";
        String name = "%" + prodName + "%";

        List<AuthNappiProduct> list = (List<AuthNappiProduct>) port.getNappiList(code.toString(), name.toString());
        session.setAttribute("nappiList", list);

        String onScreen = "" + session.getAttribute("onScreen");
        String nextJSP = "/PreAuth/AuthNappiProductSearch.jsp";
        String authType = "" + session.getAttribute("authType");
        String provTypeId = "";
//        if (authType.trim().equals("11") || authType.trim().equals("4")) {
//            provTypeId = "" + session.getAttribute("facilityProvDiscTypeId");
//            if (provTypeId != null && !provTypeId.equalsIgnoreCase("null") && !provTypeId.equalsIgnoreCase("")) {
//                session.setAttribute("facilityProv_error", null);
//            } else {
//                session.setAttribute("facilityProv_error", "Facility Provider Required");
//                nextJSP = onScreen;
//            }
//        }

        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    @Override
    public String getName() {
        return "AuthGetNappiList";
    }
}

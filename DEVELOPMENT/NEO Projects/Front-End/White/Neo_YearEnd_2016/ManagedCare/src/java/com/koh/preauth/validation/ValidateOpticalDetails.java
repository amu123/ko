/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.validation;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Diagnosis;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Johan-NB
 */
public class ValidateOpticalDetails extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);

        PrintWriter out = null;
        HttpSession session = request.getSession();

        String errorResponse = "";
        String error = "";
        int errorCount = 0;

        String afd = "" + session.getAttribute("afd");
        String atd = "" + session.getAttribute("atd");
        String as = "" + session.getAttribute("as");
        String secIcd = "" + session.getAttribute("secIcd");
        String comIcd = "" + session.getAttribute("comIcd");
        String numDays = "" + session.getAttribute("numDays");
        String prevLens = "" + session.getAttribute("prevLens");
        String curLens = "" + session.getAttribute("curLens");

        System.out.println("secICD = " + secIcd);

        if (secIcd != null && !secIcd.trim().equals("")
                && !secIcd.trim().equals("null")) {
            boolean check = ValidateMultiICD(secIcd);
            if (check == false) {
                errorCount++;
                error = error + "|secondaryICD:Invalid ICD";
            }
        }
        if (comIcd != null && !comIcd.trim().equals("")
                && !comIcd.trim().equals("null")) {
            boolean check = ValidateSingleICD(comIcd);
            if (check == false) {
                errorCount++;
                error = error + "|coMorbidityICD:Invalid ICD";
            }
        }

        //Date Validation
        PreAuthDateValidation pad = new PreAuthDateValidation();

        String authType = "" + session.getAttribute("authType");
        String authPeriod = "" + session.getAttribute("authPeriod");
        String authMonthPeriod = "" + session.getAttribute("authMonthPeriod");

        if (afd == null || afd.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|authFromDate:Auth From Date is Mandatory";
        } else {
            String vs = pad.DateValidation(afd, atd, "authFromDate", "authToDate", authType, authPeriod, authMonthPeriod, "no");
            String[] returned = vs.split("\\|");
            boolean validDate = Boolean.parseBoolean(returned[0]);
            if (!validDate) {
                String errorsReturned = vs.substring(vs.indexOf("|"), vs.length());
                System.out.println("from date validation error(s) returned = " + errorsReturned);
                errorCount++;
                error = error + errorsReturned;
            }
        }

        if (atd == null || atd.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|authToDate:Auth From Date is Mandatory";
        } else {
            String vs = pad.DateValidation(afd, atd, "authFromDate", "authToDate", authType, authPeriod, authMonthPeriod, "no");
            String[] returned = vs.split("\\|");
            boolean validDate = Boolean.parseBoolean(returned[0]);
            if (!validDate) {
                String errorsReturned = vs.substring(vs.indexOf("|"), vs.length());
                System.out.println("to date validation error(s) returned = " + errorsReturned);
                errorCount++;
                error = error + errorsReturned;
            }
        }

        if (as == null || as.trim().equals("99")) {
            errorCount++;
            error = error + "|authStatus:Auth Status is Mandatory";
        }

        if (numDays == null || numDays.trim().equals("")) {
            errorCount++;
            error = error + "|numDays:Number of Days is Mandatory";
        }
        if (prevLens == null || prevLens.trim().equals("")) {
            errorCount++;
            error = error + "|prevLens:Previous Lens RX is Mandatory";
        }
        if (curLens == null || curLens.trim().equals("")) {
            errorCount++;
            error = error + "|curLens:Current Lens RX is Mandatory";
        }

        if (errorCount > 0) {
            errorResponse = "Error" + error;
        } else if (errorCount == 0) {
            errorResponse = "Done|";
        }

        try {
            out = response.getWriter();
            out.println(errorResponse);

        } catch (Exception ex) {
            System.out.println("ValidateOpticalDetails error : " + ex.getMessage());
        } finally {
            out.close();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ValidateOpticalDetails";
    }

    private boolean ValidateDate(String date) {
        boolean valid = true;
        String validChars = "0123456789/";
        char strChar;
        char strChar2;

        if (date.length() < 10 || date.length() > 10) {
            valid = false;
        }
        //  test strString consists of valid characters listed above
        for (int i = 0; i < date.length(); i++) {
            strChar = date.charAt(i);
            if (validChars.indexOf(strChar) == -1) {
                valid = false;
            }
        }
        // test if slash is at correct position (yyyy/mm/dd)
        strChar = date.charAt(4);
        strChar2 = date.charAt(7);
        if (strChar != '/' || strChar2 != '/') {
            valid = false;
        }
        return valid;
    }

    private boolean ValidateMultiICD(String icd) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        boolean valid = true;
        String[] icds = null;
        icds = icd.split(",");

        if (icds != null || icds.length != 0) {
            for (String code : icds) {
                Diagnosis d = port.getDiagnosisForCode(code.trim());
                if (d == null) {
                    valid = false;
                }
            }
        } else {
            valid = ValidateSingleICD(icd);
        }
        return valid;
    }

    private boolean ValidateSingleICD(String icd) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        boolean valid = true;
        Diagnosis d = port.getDiagnosisForCode(icd);
        if (d == null) {
            valid = false;
        }
        return valid;
    }
}

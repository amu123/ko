/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.employer;

import com.koh.employer.command.TabUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import neo.manager.AppAnswers;
import neo.manager.EmployerEligibility;
import neo.manager.EmployerGroup;
import neo.manager.NeoUser;

/**
 *
 * @author yuganp
 */
public class EmployerEligibilityMapping {
    private static final String[][] FIELD_MAPPINGS = {
        {"EmployerGroup_entityId","","no", "int", "EmployerEligibility","EmployerEntityId"},
        {"EmployerEligibility_availableToAllInd","Available To All","no", "int", "EmployerEligibility","AvailableToAllInd"},
        {"EmployerEligibility_totalStaffCount","Total Staff Count","no", "int", "EmployerEligibility","TotalStaffCount"},
        {"EmployerEligibility_totalStaffEligible","Total Staff Eligible","no", "int", "EmployerEligibility","TotalStaffEligible"},
        {"EmployerEligibility_totalStaffParticipating","Total Staff Participating","no", "int", "EmployerEligibility","TotalStaffParticipating"},
        {"EmployerEligibility_totalPensioners","Total Pensioners","no", "int", "EmployerEligibility","TotalPensioners"},
        {"EmployerEligibility_totalPensionersEligible","Total Pensioners Eligible","no", "int", "EmployerEligibility","TotalPensionersEligible"},
        {"EmployerEligibility_totalPensionersParticipating","Total Pensioners Participating","no", "int", "EmployerEligibility","TotalPensionersParticipating"},
        {"EmployerEligibility_ageLimit","Average Age Limit","no", "int", "EmployerEligibility","AgeLimit"},

        {"EmployerEligibility_Scheme_id1","Name of Scheme","no", "int", "EmployerEligibility","SchemeId1"},
        {"EmployerEligibility_Scheme_schemeName1","Other Scheme","no", "str", "EmployerEligibility","SchemeName1"},
        {"EmployerEligibility_Scheme_startDate1","From","no", "date", "EmployerEligibility","SchemeStartDate1"},
        {"EmployerEligibility_Scheme_endDate1","To","no", "date", "EmployerEligibility","SchemeEndDate1"},
        {"EmployerEligibility_Scheme_id2","Name of Scheme","no", "int", "EmployerEligibility","SchemeId2"},
        {"EmployerEligibility_Scheme_schemeName2","Other Scheme","no", "str", "EmployerEligibility","SchemeName2"},
        {"EmployerEligibility_Scheme_startDate2","From","no", "date", "EmployerEligibility","SchemeStartDate2"},
        {"EmployerEligibility_Scheme_endDate2","To","no", "date", "EmployerEligibility","SchemeEndDate2"},

        {"EmployerEligibility_Scheme_declined","Declined?","no", "int", "EmployerEligibility","DeclinedInd"},
        {"EmployerEligibility_Scheme_declinedDetails","Details","no", "str", "EmployerEligibility","DeclinedReason"},
        {"EmployerEligibility_Scheme_loaded","Loaded?","no", "int", "EmployerEligibility","LoadedInd"},
        {"EmployerEligibility_Scheme_loadedDetails","Details","no", "str", "EmployerEligibility","LoadedReason"},
        {"EmployerEligibility_Scheme_exclusion","Exclusion?","no", "int", "EmployerEligibility","ExclusionInd"},
        {"EmployerEligibility_Scheme_exclusionDetails","Details","no", "str", "EmployerEligibility","ExclusionReason"}
        
    };
    private static final Map<String,String[]> FIELD_MAP;

    static {
        HashMap hm = new HashMap<String,String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }
    
    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
        validateOthers(request, errors);
        for (String error : errors.keySet()) {
            System.out.println(error + " = " + errors.get(error));
        }
        return errors;
    }
    
    private static void validateOthers(HttpServletRequest request, Map<String, String> errors) {
        Map map = request.getParameterMap();
        String check = "EmployerEligibility_option_";
        for (Object s : map.keySet()) {
            String key = s.toString();
            String value = request.getParameter(key);
//            System.out.println(key + " = " + value);
            if (key != null && value!= null) {
                if (key.length() > check.length() && key.substring(0, check.length()).equalsIgnoreCase(check)) {
                    String data = key.substring(check.length());
                    String[] sarr = data.split("_");
                    if (sarr != null && sarr.length > 1) {
                        if (!value.isEmpty() &&  !isIntValue(value)) {
                            errors.put(key+"_error", sarr[0] + " must be a number.");
                        } else {
                            errors.put(key+"_error","");
                        }
                    }
                }
            }
        }
    }
    
    private static boolean isIntValue(String s) {
        try {
            Integer.parseInt(s);
            System.out.println(s + " is an int");
            return true;
        } catch (Exception e) {
            System.out.println(s + " is not an int");
            return false;
        }
    }
    
    public static EmployerEligibility getEmployerEligibility(HttpServletRequest request,NeoUser neoUser) {
        EmployerEligibility eg = (EmployerEligibility)TabUtils.getDTOFromRequest(request, EmployerEligibility.class, FIELD_MAPPINGS, "EmployerEligibility");
        eg.setSecurityGroupId(neoUser.getSecurityGroupId());
        eg.setLastUpdatedBy(neoUser.getUserId());
        return eg;
    }

    public static void setEmployerEligibility(HttpServletRequest request, EmployerEligibility employerEligibility) {
        TabUtils.setRequestFromDTO(request, employerEligibility, FIELD_MAPPINGS, "EmployerEligibility");
    }

    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author princes
 */
public class ConsultantViewCommand extends NeoCommand {
 @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String consultantCode = request.getParameter("consultantCode");
        String consultantName = request.getParameter("consultantName");
        String consultantEntityId = request.getParameter("consultantEntityId");
        String consultantRegion = request.getParameter("consultantRegion");

        
        request.setAttribute("consultantAppCode", consultantCode);
        request.setAttribute("consultantAppName", consultantName);
        request.setAttribute("consultantEntityId", consultantEntityId);
        request.setAttribute("consultantRegion", consultantRegion);

        try {
            context.getRequestDispatcher("/Broker/BrokerConsultant.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(BrokerViewCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BrokerViewCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ConsultantViewCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 *
 * @author pavaniv
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;

import com.koh.utils.MapUtils;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;
import org.apache.log4j.Logger;
import neo.manager.KeyValue;
public class CustomerCareAuditCommand extends NeoCommand{
    
  private static final Logger logger = Logger.getLogger(CustomerCareAuditCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("entered CustomerCareAuditCommand");
        NeoManagerBean port = service.getNeoManagerBeanPort();
        // HttpSession session = request.getSession();
        
        try {
           
            String name = request.getParameter("AuditList");
            String entityId = request.getParameter("memberEntityId");
            request.setAttribute("AuditLists", name);
            //request.getRequestDispatcher("/Claims/CallCenterAuditTrail.jsp").forward(request, response);
            
            logger.info("name : " + name);
            logger.info("entityId: " + entityId);
            
            System.out.println("entityid value"+ entityId);
            
             System.out.println("id value"+ name);
            int id = 0;
            try {
                id = Integer.parseInt(entityId);
                
             System.out.println("id "+ id);
                logger.info("id: " + id);
            } catch (Exception e) {
                logger.error(e);
                
            }
            
            List<KeyValueArray> kva = port.getAuditTrail(name, id);
            logger.info("total kva returned : " +  kva.size());
           System.out.println("kva value "+ kva);
            /*for (KeyValueArray kVa : kva) {
                List<KeyValue> kVl =  kVa.getKeyValList();
                for (KeyValue kv : kVl) {
                    logger.info("key : " +  kv.getKey());
                    logger.info("value : " +  kv.getValue());
                }
            } */
            
            Object col = MapUtils.getMap(kva);
            request.setAttribute("CallCenterAuditTrail", col);
            context.getRequestDispatcher("/Claims/CallCenterAuditTrail.jsp").forward(request, response);
        } catch (Exception ex) {
            logger.error(ex);
        }        
        
        return null;
    }

    @Override
    public String getName() {
        return "CustomerCareAuditCommand";
    }
    
}

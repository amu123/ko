/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;
import neo.manager.Option;
import neo.manager.PersonDetails;

/**
 *
 * @author Johan-NB
 */
public class PreAuthMemberSearchGrid extends TagSupport {

    private String commandName;
    private String javaScript;
    private String sessionAttribute;
    private String onScreen;
    private String specificDependant = "no";

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        boolean updateMode = false;

        if (specificDependant != null && !specificDependant.equalsIgnoreCase("")
                && specificDependant.equalsIgnoreCase("yes")) {
            updateMode = true;
        }


        try {
            /*if (updateMode) {

                CoverDetails cd = (CoverDetails) session.getAttribute(sessionAttribute);

                if (!cd.getCoverNumber().equalsIgnoreCase("")) {

                    out.println("<td colspan=\"12\"><table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                    out.println("<tr>"
                            + "<th align=\"left\">Option</th>"
                            + "<th align=\"left\">Code</th>"
                            + "<th align=\"left\">First Name</th>"
                            + "<th align=\"left\">Surname</th>"
                            + "<th align=\"left\">Dependant Type</th>"
                            + "<th align=\"left\">Date Of Birth</th>"
                            + "<th align=\"left\">Gender</th>"
                            + "<th align=\"left\">Id Number</th>"
                            + "<th align=\"left\">Status</th>"
                            + "<th align=\"left\">Join Date</th>"
                            + "<th align=\"left\">Option Date</th>"
                            + "<th align=\"left\">Resigned Date</th>"
                            + "</tr>");

                    //set product
                    Option opt = port.findOptionWithID(cd.getOptionId());

                    //set cover details 
                    String dateOfBirth = "";
                    String joinDate = "";
                    String benefitDate = "";
                    String resignDate = "";
                    PersonDetails pd = null;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

                    int entityId = cd.getEntityId();
                    pd = port.getPersonDetailsByEntityId(entityId);

                    System.out.println("cover: " + cd.getCoverNumber() + " dep:" + cd.getDependentNumber() + " eId = " + cd.getEntityId());

                    //dob
                    if (pd.getDateOfBirth() != null) {
                        dateOfBirth = dateFormat.format(pd.getDateOfBirth().toGregorianCalendar().getTime());
                    }

                    //benefit start date
                    if (cd.getCoverStartDate() != null) {
                        benefitDate = dateFormat.format(cd.getCoverStartDate().toGregorianCalendar().getTime());
                    }

                    //join date
                    if (cd.getSchemeJoinDate() != null) {
                        joinDate = dateFormat.format(cd.getSchemeJoinDate().toGregorianCalendar().getTime());
                    } else {
                        joinDate = dateFormat.format(cd.getCoverStartDate().toGregorianCalendar().getTime());
                    }

                    //resign date
                    if (cd.getCoverEndDate() != null) {
                        String dateTo = new SimpleDateFormat("yyyy/MM/dd").format(cd.getCoverEndDate().toGregorianCalendar().getTime());
                        if (!dateTo.equalsIgnoreCase("2999/12/31")) {
                            resignDate = dateTo;
                        }

                    }


                    out.println("<tr>"
                            + "<td><label class=\"label\">" + opt.getOptionName() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getDependentNumber() + "</label></td>"
                            + "<td><label class=\"label\">" + pd.getName() + "</label></td>"
                            + "<td><label class=\"label\">" + pd.getSurname() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getDependentType() + "</label></td>"
                            + "<td><label class=\"label\">" + dateOfBirth + "</label></td>"
                            + "<td><label class=\"label\">" + pd.getGenderDesc() + "</label></td>"
                            + "<td><label class=\"label\">" + pd.getIDNumber() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getStatus() + "</label></td>"
                            + "<td><label class=\"label\">" + joinDate + "</label></td>"
                            + "<td><label class=\"label\">" + benefitDate + "</label></td>"
                            + "<td><label class=\"label\">" + resignDate + "</label></td>"
                            + "</tr>");

                    out.println("</table>");

                } else {
                    out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                    out.println("<tr colspan=\"12\">"
                            + "<th align=\"left\">Member is not active for the Auth Period</th>"
                            + "</tr>");
                    out.println("</td></table>");
                }



            } else {*/

                List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute(sessionAttribute);

                if (cdList != null && cdList.isEmpty() == false) {
                    //sort list
                    Collections.sort(cdList, new Comparator<CoverDetails>() {
                        public int compare(CoverDetails o1, CoverDetails o2) {
                            return new Integer(o1.getDependentNumber()).compareTo(o2.getDependentNumber());
                        }
                    });
                    
                    
                    //get dependant 
                    int depCode = -1;
                    if (session.getAttribute("depListValues") != null) {
                        String depCodeStr = "" + session.getAttribute("depListValues");
                        System.out.println("Member search grid depCodeStr = "+depCodeStr);
                        if(depCodeStr != null && !depCodeStr.equalsIgnoreCase("") && !depCodeStr.equalsIgnoreCase("null")){
                            depCode = Integer.parseInt(depCodeStr);
                        }
                    }

                    out.println("<td colspan=\"12\"><table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                    out.println("<tr>"
                            + "<th align=\"left\">Option</th>"
                            + "<th align=\"left\">Code</th>"
                            + "<th align=\"left\">First Name</th>"
                            + "<th align=\"left\">Surname</th>"
                            + "<th align=\"left\">Dependant Type</th>"
                            + "<th align=\"left\">Date Of Birth</th>"
                            + "<th align=\"left\">Gender</th>"
                            + "<th align=\"left\">Id Number</th>"
                            + "<th align=\"left\">Status</th>"
                            + "<th align=\"left\">Join Date</th>"
                            + "<th align=\"left\">Option Date</th>"
                            + "<th align=\"left\">Resigned Date</th>"
                            + "<th align=\"left\"></th>"
                            + "</tr>");

                    //set product
                    Option opt = (Option) session.getAttribute("schemeOptionDetails");

                    //set cover details 
                    String dateOfBirth = "";
                    String joinDate = "";
                    String benefitDate = "";
                    String resignDate = "";
                    PersonDetails pd = null;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                    String cssClass = "label";

                    for (CoverDetails cd : cdList) {
                        int entityId = cd.getEntityId();
                        pd = port.getPersonDetailsByEntityId(entityId);

                        System.out.println("cover: " + cd.getCoverNumber() + " dep:" + cd.getDependentNumber() + " eId = " + cd.getEntityId());

                        //dob
                        if (pd.getDateOfBirth() != null) {
                            dateOfBirth = dateFormat.format(pd.getDateOfBirth().toGregorianCalendar().getTime());
                        }

                        //benefit start date
                        if (cd.getCoverStartDate() != null) {
                            benefitDate = dateFormat.format(cd.getCoverStartDate().toGregorianCalendar().getTime());
                        }

                        //join date
                        if (cd.getSchemeJoinDate() != null) {
                            joinDate = dateFormat.format(cd.getSchemeJoinDate().toGregorianCalendar().getTime());
                        } else {
                            joinDate = dateFormat.format(cd.getCoverStartDate().toGregorianCalendar().getTime());
                        }

                        //resign date
                        if (cd.getCoverEndDate() != null) {
                            String dateTo = new SimpleDateFormat("yyyy/MM/dd").format(cd.getCoverEndDate().toGregorianCalendar().getTime());
                            if (!dateTo.equalsIgnoreCase("2999/12/31")) {
                                resignDate = dateTo;
                            }

                        }

                        if (depCode != -1 && depCode == cd.getDependentNumber()) {
                           out.println("<tr>"
                                    + "<td><label class=\"noteLabel\">" + opt.getOptionName() + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + cd.getDependentNumber() + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + pd.getName() + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + pd.getSurname() + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + cd.getDependentType() + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + dateOfBirth + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + pd.getGenderDesc() + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + pd.getIDNumber() + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + cd.getStatus() + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + joinDate + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + benefitDate + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + resignDate + "</label></td>"
                                    + "<td><button name=\"opperation\" type=\"button\" onClick=\"submitMemberDepWithAction('SetSelectedCoverDep','" + cd.getDependentNumber() + "','" + onScreen + "')\";>Select</button></td>"
                                    + "</tr>");
                        } else {
                            out.println("<tr>"
                                    + "<td><label class=\"label\">" + opt.getOptionName() + "</label></td>"
                                    + "<td><label class=\"label\">" + cd.getDependentNumber() + "</label></td>"
                                    + "<td><label class=\"label\">" + pd.getName() + "</label></td>"
                                    + "<td><label class=\"label\">" + pd.getSurname() + "</label></td>"
                                    + "<td><label class=\"label\">" + cd.getDependentType() + "</label></td>"
                                    + "<td><label class=\"label\">" + dateOfBirth + "</label></td>"
                                    + "<td><label class=\"label\">" + pd.getGenderDesc() + "</label></td>"
                                    + "<td><label class=\"label\">" + pd.getIDNumber() + "</label></td>"
                                    + "<td><label class=\"label\">" + cd.getStatus() + "</label></td>"
                                    + "<td><label class=\"label\">" + joinDate + "</label></td>"
                                    + "<td><label class=\"label\">" + benefitDate + "</label></td>"
                                    + "<td><label class=\"label\">" + resignDate + "</label></td>"
                                    + "<td><button name=\"opperation\" type=\"button\" onClick=\"submitMemberDepWithAction('SetSelectedCoverDep','" + cd.getDependentNumber() + "','" + onScreen + "')\";>Select</button></td>"
                                    + "</tr>");

                        }
                    }
                    out.println("</td></table>");

                }
            //}

        } catch (java.io.IOException ex) {
            throw new JspException("Error in PreAuthMemberSearchGrid tag", ex);
        }




        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setOnScreen(String onScreen) {
        this.onScreen = onScreen;
    }

    public void setSpecificDependant(String specificDependant) {
        this.specificDependant = specificDependant;
    }
}

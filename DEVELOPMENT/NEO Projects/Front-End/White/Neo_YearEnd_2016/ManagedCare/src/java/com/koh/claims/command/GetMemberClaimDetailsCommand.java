/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.claims.dto.MemberClaimSearchBean;
import com.koh.claims.tags.ViewMemberClaimsTable;
import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.lang.Exception;
import java.security.Security;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author Johan-NB
 */
public class GetMemberClaimDetailsCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);

        HttpSession session = request.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        boolean doSixMonths = false;
        boolean claimIdFound = false;
        String claimSearchResultMsg = "";

        PrintWriter out = null;
        List<ClaimsBatch> claimList = null;
        ClaimSearchCriteria search = new ClaimSearchCriteria();
        //set default search values
        search.setDependentCode(-1);
        
        String disciplineType = request.getParameter("disciplineType");
        if (disciplineType != null && !disciplineType.equalsIgnoreCase("")) {
            search.setDisciplineId(disciplineType);
        }

        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        neo.manager.Security _secure = new neo.manager.Security();
        _secure.setCreatedBy(user.getUserId());
        _secure.setLastUpdatedBy(user.getUserId());
        _secure.setSecurityGroupId(2);
        
        String pageDirection = "" + session.getAttribute("mcPageDirection");
        
        int currentMin = 0;
        int currentMax = 100;
        
        if (pageDirection != null && !pageDirection.equalsIgnoreCase("")) {
            search = (ClaimSearchCriteria) session.getAttribute("memberClaimSearch");
            if(session.getAttribute("resultCountMin") != null || session.getAttribute("resultCountMax") != null) {
                currentMin = Integer.parseInt(String.valueOf(session.getAttribute("resultCountMin")));
                currentMax = Integer.parseInt(String.valueOf(session.getAttribute("resultCountMax")));
            }

            if (pageDirection.equalsIgnoreCase("forward")) {
                currentMin = currentMin + 100;
                currentMax = currentMax + 100;

            } else if (pageDirection.equalsIgnoreCase("backward")) {
                currentMin = currentMin - 100;
                currentMax = currentMax - 100;
            }
            
            session.setAttribute("resultCountMin", currentMin);
            session.setAttribute("resultCountMax", currentMax);
            CcClaimSearchResult searchResult = port.findDetailedClaimBySearchCriteria(search, currentMin, currentMax, _secure);
            if (searchResult.getBatchs() != null) {
                claimList = searchResult.getBatchs();
            }
        } else {
            String onScreen = "";
            int dependantCode = -1;

            //System.out.println("entered ViewMemberClaimsTable");
            String coverNumber = (String) session.getAttribute("policyHolderNumber");
            //System.out.println("coverNumber " + coverNumber);
            String idNumber = "" + session.getAttribute("idNumber");
            //System.out.println("idNumber " + idNumber);
            String practiceNumber = "";

            CoverDetails details = port.getCoverDetailsByCoverIdNumber(idNumber);

            //set cover number
            search.setCoverNumber(coverNumber);

            //Benefit to claims drilldown
            if (session.getAttribute("onBenefitScreen") != null) {
                onScreen = "" + session.getAttribute("onBenefitScreen");
                dependantCode = Integer.parseInt("" + session.getAttribute("depCode"));
                //System.out.println("BCDD: onScreen = " + onScreen);
                //System.out.println("BCDD: benefitId = " + benefitId);
                //System.out.println("BCDD: dependantCode = " + dependantCode);
                search.setDependentCode(dependantCode);


            } else {
                //cover dependant 
                String covDep = "" + session.getAttribute("depListValues");
                if (covDep != null && !covDep.equals("") && !covDep.equals("99")) {
                    search.setDependentCode(Integer.parseInt(covDep));
                }

                //claim id
                String memClaimSearchClaimId = "" + session.getAttribute("mcsClaimId");
                if (memClaimSearchClaimId != null && !memClaimSearchClaimId.equals("")) {
                    search.setClaimId(Integer.parseInt(memClaimSearchClaimId));
                    claimIdFound = true;
                }
                
                String treatFrom = "";
                String treatTo = "";

                treatFrom = "" + session.getAttribute("treatmentFrom");
                treatTo = "" + session.getAttribute("treatmentTo");

                if ((treatFrom != null && !treatFrom.equalsIgnoreCase(""))
                        && (treatTo != null && !treatTo.equalsIgnoreCase(""))) {

                    Date tFrom = null;
                    Date tTo = null;
                    XMLGregorianCalendar xTreatFrom = null;
                    XMLGregorianCalendar xTreatTo = null;

                    try {
                        tFrom = dateFormat.parse(treatFrom);
                        tTo = dateFormat.parse(treatTo);

                        xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                        xTreatTo = DateTimeUtils.convertDateToXMLGregorianCalendar(tTo);

                        search.setServiceDateFrom(xTreatFrom);
                        search.setServiceDateTo(xTreatTo);

                    } catch (java.text.ParseException ex) {
                        Logger.getLogger(ViewMemberClaimsTable.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } else {
                    if(!claimIdFound){
                        doSixMonths = true;
                        claimSearchResultMsg = "Please note only the past 6 months claims data was retrieved.";
                    }                                        
                }
                
                practiceNumber = request.getParameter("practiceNumber_text");
                if (practiceNumber != null && !practiceNumber.equalsIgnoreCase("")) {
                    search.setPracticeNumber(practiceNumber);
                }
                
                
                if (request.getParameter("claimStatus") != null && !request.getParameter("claimStatus").equalsIgnoreCase("") && !request.getParameter("claimStatus").equalsIgnoreCase("99")) {
                    int claimStatus = Integer.parseInt(request.getParameter("claimStatus"));
                    List<LookupValue> claimStatusLV = port.getCodeTable(83); //claimStatus = 83
                    if (!claimStatusLV.isEmpty()) {
                        LookupValue claimStatusLookup = claimStatusLV.get(claimStatus-1);
                        search.setClaimStatus(claimStatusLookup);
                    }
                }
                
                String tariffCode = request.getParameter("tariffCode");
                if (tariffCode != null && !tariffCode.equals("") && !tariffCode.equals(" ")) {
                    search.setTariffCodeNo(tariffCode);
                }
                
                if (request.getParameter("toothNum") != null && !request.getParameter("toothNum").equalsIgnoreCase("") && !request.getParameter("toothNum").equalsIgnoreCase("0")) {
                    int toothNumber = Integer.parseInt(request.getParameter("toothNum"));
                    search.setToothnumber(toothNumber);
                }
                
                if (request.getParameter("batchNum") != null && !request.getParameter("batchNum").equalsIgnoreCase("") && !request.getParameter("batchNum").equalsIgnoreCase("0")) {
                    int batchNumber = Integer.parseInt(request.getParameter("batchNum"));
                    search.setBatchId(batchNumber);
                }
                
                if (doSixMonths) {
                    //set service dates
                    Calendar c1 = Calendar.getInstance();
                    c1.add(Calendar.MONTH, -6); // substract 6 month

                    Calendar calendar = Calendar.getInstance();

                    try {
                        String today = dateFormat.format(calendar.getTime());
                        Date now = dateFormat.parse(today);
                        XMLGregorianCalendar xmlNow = DateTimeUtils.convertDateToXMLGregorianCalendar(now);
                        calendar.add(Calendar.MONTH, -6);
                        String sixLater = dateFormat.format(calendar.getTime());
                        Date sixMonths = dateFormat.parse(sixLater);
                        XMLGregorianCalendar xmlSixMonthsLater = DateTimeUtils.convertDateToXMLGregorianCalendar(sixMonths);
                        search.setServiceDateFrom(xmlSixMonthsLater);
                        search.setServiceDateTo(xmlNow);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //set rusult message

                }

            }

            session.setAttribute("resultCountMin", currentMin);
            session.setAttribute("resultCountMax", currentMax);
            CcClaimSearchResult searchResult = port.findDetailedClaimBySearchCriteria(search, 0, 100, _secure);
            if (searchResult.getBatchs() != null) {
                claimList = searchResult.getBatchs();
            }

        }

        if (claimList == null && claimList.size() < 1) {
            claimSearchResultMsg += " No results found.";

        } else if (claimList.size() >= 100) {
            claimSearchResultMsg += " Please Redefine Claim Search.";

        } else {                        
            //buil the JSTL tag bean to replace old cold
            List<MemberClaimSearchBean> meberClaimSearchBeanList = new ArrayList<MemberClaimSearchBean>();
            for (ClaimsBatch claim : claimList){
                String claimType = "";
                    if (claim.getEdiId() == null || claim.getEdiId().equalsIgnoreCase("")) {
                        claimType = "Paper";
                    } else {
                        claimType = "EDI";
                    }
                for(Claim list_claim : claim.getClaims()){
                    MemberClaimSearchBean bean = new MemberClaimSearchBean();
                    ProviderDetails prac = port.getPracticeDetailsForEntityByNumberAndDate(list_claim.getPracticeNo(), list_claim.getServiceDateFrom() );
                    String catagory = port.getValueForId(59, list_claim.getClaimCategoryTypeId() + "");
                    ProviderDetails practice = port.getPracticeDetailsForEntityByNumber(list_claim.getPracticeNo());
                    ProviderDetails practice2 = port.getPracticeDetailsForEntityByNumber(list_claim.getServiceProviderNo());
                   
                    bean.setClaimType(claimType);
                    bean.setClaimNumber(""+list_claim.getClaimId());
                    bean.setAccountNumber(list_claim.getPatientRefNo());                    
                    bean.setPracticeNumber(list_claim.getPracticeNo());
                    if(practice != null){
                        bean.setPracticeName(practice.getPracticeName());
                    } else {
                        bean.setPracticeName("INVALID PROVIDER");
                    }
                    //bean.setPracticeName(practice.getPracticeName());
                    bean.setServiceProvider(list_claim.getServiceProviderNo());
                    if(practice2 != null){
                        bean.setServiceProviderName(practice2.getPracticeName());
                    } else {
                        bean.setServiceProviderName("INVALID PROVIDER");
                    }
                    //bean.setServiceProviderName(practice2.getPracticeName());
                    bean.setDisciplineType(catagory);
                    bean.setMemberNumber(list_claim.getCoverNumber());
                    bean.setTreatmentFrom(dateFormat.format(list_claim.getServiceDateFrom().toGregorianCalendar().getTime()));
                    bean.setTreatmentTo(dateFormat.format(list_claim.getServiceDateTo().toGregorianCalendar().getTime()));
                    String totalAmountClaimed = "";
                    String totalAmountPaid = "";
                    if (list_claim.getClaimCalculatedTotal() != 0.0d) {
                          totalAmountClaimed = new DecimalFormat("#####0.00").format(list_claim.getClaimCalculatedTotal());
                        }else{totalAmountClaimed = "0.00";} 
                        
                        if (list_claim.getClaimCalculatedPaidTotal() != 0.0d) {
                          totalAmountPaid = new DecimalFormat("#####0.00").format(list_claim.getClaimCalculatedPaidTotal());
                        }else{totalAmountPaid = "0.00";}
                    bean.setTotalClaimed(totalAmountClaimed);
                    bean.setTotalPaid(totalAmountPaid);
                    bean.setClaimID(""+list_claim.getClaimId());
                    bean.setServiceProviderNo(list_claim.getServiceProviderNo());                    
                    String recipient = port.getValueFromCodeTableForTableId(15, list_claim.getRecipient());
                    bean.setRecipient(recipient);
                    String coopay="";
                    if (claim.getClaims().get(0).getClaimCopayAmount() != 0.0d) {
                          coopay = new DecimalFormat("#####0.00").format(claim.getClaims().get(0).getClaimCopayAmount());
                    }else{
                    coopay = "0.00";
                    }
                    bean.setCo_payment(coopay);
                    if(prac != null){
                       bean.setNetwork(prac.getNetworkName()); 
                    } else {
                        bean.setNetwork("INVALID PROVIDER");
                    }
                    
                    meberClaimSearchBeanList.add(bean);
                }
                
            }//end here
            session.removeAttribute("meberClaimSearchBeanList");
            session.setAttribute("meberClaimSearchBeanList", meberClaimSearchBeanList);

            System.out.println("batch not null : size = " + claimList.size());

            session.setAttribute("memCCClaimSearchResult", claimList);
            session.setAttribute("memCCClaimSearchResultCount", claimList.size());
//            session.setAttribute("memCCClaimSearch", batch.getCcSearch());

        }
        session.removeAttribute("memberClaimSearchResultsMessage");
        session.setAttribute("memberClaimSearchResultsMessage", claimSearchResultMsg);

        try {
            String nextJSP = "/Claims/MemberClaimSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetMemberClaimDetailsCommand";
    }
}

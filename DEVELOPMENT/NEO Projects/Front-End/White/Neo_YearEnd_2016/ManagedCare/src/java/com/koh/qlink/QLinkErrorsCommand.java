/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.qlink;

import com.koh.command.NeoCommand;
import com.koh.contribution.command.PaymentAllocationCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverDetails;
import neo.manager.KeyValueArray;
import neo.manager.MemberEmployerBrokerInfo;
import neo.manager.NeoManagerBean;

/**
 *
 * @author yuganp
 */
public class QLinkErrorsCommand  extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            String s = request.getParameter("command");
            if (s != null && !s.isEmpty()) {
                System.out.println("command : " + s);
                if (s.equalsIgnoreCase("selectPayroll")) {
                    getPayrollBatch(request, response, context);
                } else if (s.equalsIgnoreCase("selectBatch")) {
                    getPayrollDetails(request, response, context);
                } else if (s.equalsIgnoreCase("selectDetails")) {
                    selectPayrollDetail(request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(QLinkErrorsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void getPayrollBatch(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        try {
            String payroll = request.getParameter("payroll");
            if (payroll != null && !payroll.isEmpty()) {
                Date endDate = new Date();
                Calendar cal = Calendar.getInstance();
                cal.setTime(endDate);
                cal.add(Calendar.MONTH, -6);
                Date startDate = cal.getTime();
                List<KeyValueArray> qLinkBatchList = port.getQLinkBatchList("QERR", payroll,DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
                List<Map<String, String>> mapList = MapUtils.getMap(qLinkBatchList);
                request.setAttribute("BatchResults", mapList);
            }
            context.getRequestDispatcher("/QLink/_QLinkErrorBatches.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(QLinkErrorsCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(QLinkErrorsCommand.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        }
    }

    private void getPayrollDetails(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        try {
            String batch = request.getParameter("batch");
            if (batch != null && !batch.isEmpty()) {
                List<KeyValueArray> qLinkBatchList = port.getQLinkBatchDetails(Integer.parseInt(batch));
                List<Map<String, String>> mapList = MapUtils.getMap(qLinkBatchList);
                request.setAttribute("BatchResults", mapList);
            }
            context.getRequestDispatcher("/QLink/_QLinkErrorDetails.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(QLinkErrorsCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(QLinkErrorsCommand.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        }
    }

    private void selectPayrollDetail(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        String onScreen = "/Member/DependantSelection.jsp";
        String coverNumber = request.getParameter("coverNumber");
        String detailsId = request.getParameter("detailsId");
        String detailLineStatus = request.getParameter("detailLineStatus");
        session.setAttribute("coverNum", coverNumber);
        if ("1".equalsIgnoreCase(detailLineStatus) || "2".equalsIgnoreCase(detailLineStatus)) {
            port.updateQLinkDetailStatus(Integer.parseInt(detailsId), 2, TabUtils.getSecurity(request));
        }
        
        List<CoverDetails> covList = port.getDependantsForPrincipleMemberByDate(coverNumber);
        MemberEmployerBrokerInfo memberEmployerBrokerInfo = port.getMemberEmployerBrokerInfo(coverNumber);
        //System.out.println("member dependant size = " + covList.size());
        session.setAttribute("MemberCoverDependantDetails", covList);
        session.setAttribute("memberEmployerBrokerInfo", memberEmployerBrokerInfo);
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(onScreen);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public String getName() {
        return "QLinkErrorsCommand";
    }
    
}

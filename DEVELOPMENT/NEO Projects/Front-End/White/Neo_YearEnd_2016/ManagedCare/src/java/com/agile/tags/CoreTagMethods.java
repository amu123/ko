/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

import com.agile.security.webservice.AgileManager;
import com.agile.security.webservice.AgileManagerService;
import java.net.URL;
import javax.xml.namespace.QName;

/**
 *
 * @author gerritj
 */
public class CoreTagMethods {

    public static AgileManager port = null;

    static{
        getWebServicePort();
    }

    /**
     * Build the static webservice port
     */
    private static void getWebServicePort() {
        try {
            String WS_URL_TriggerSoapHttpPort = "http://localhost:9494/ManagedCareBackend/AgileManager?wsdl";
            String WS_NAMESPACE_TriggerSoapHttpPort = "http://webservice.security.agile.com/";
            String WS_SERVICE_TriggerSoapHttpPort = "AgileManagerService";
            String WS_PORT_TriggerSoapHttpPort = "AgileManagerPort";
            URL wsUrl = new URL(WS_URL_TriggerSoapHttpPort);
            QName wsQName = new QName(WS_NAMESPACE_TriggerSoapHttpPort, WS_SERVICE_TriggerSoapHttpPort);
            AgileManagerService service = new AgileManagerService(wsUrl, wsQName);
            AgileManager portt = service.getPort(new QName(WS_NAMESPACE_TriggerSoapHttpPort, WS_PORT_TriggerSoapHttpPort), AgileManager.class);
            port = portt;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

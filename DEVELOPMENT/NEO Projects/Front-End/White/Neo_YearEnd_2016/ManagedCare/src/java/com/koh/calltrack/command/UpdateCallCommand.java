/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.calltrack.command;

import com.koh.command.NeoCommand;
import java.util.Collection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CallTrackDetails;
import neo.manager.NeoUser;

/**
 *
 * @author josephm
 */
public class UpdateCallCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoUser user = (NeoUser)session.getAttribute("persisit_user");

        String callTrackId = request.getParameter("trackID");
        int callId = new Integer(callTrackId);

        if(callTrackId != null) {

          Collection<CallTrackDetails> details = service.getNeoManagerBeanPort().getCallTrackDetailsById(callId);
          for(CallTrackDetails call: details) {

            //  System.out.println("The  call reference is " + call.getCallReference());
          }
        }

        try {
            String nextJSP = "/Calltrack/UpdateCall.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        }
        catch(Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "UpdateCallCommand";
    }
}

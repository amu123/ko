/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import com.koh.command.NeoCommand;
import com.koh.utils.LookupUtils;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.LookupValue;
import neo.manager.NeoUser;

/**
 *
 * @author johanl
 */
public class LabelNeoLookupValueDropDownErrorReq extends TagSupport {

    private String displayName;
    private String elementName;
    private String lookupId;
    private String javaScript;
    private String mandatory = "no";
    private String errorValueFromSession = "no";
    private String firstIndexSelected = "no";
    private String sort = null;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag 
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        try {
            out.println("<td align=\"left\" width=\"160px\"><label class=\"label\" for=\"" + elementName + "\">" + displayName + ":</label></td>");
            out.println("<td><select style=\"width:200px\" name=\"" + elementName + "\" id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");
            String selectedBox = "" + pageContext.getRequest().getAttribute(elementName);
            //Neo Lookups
            List<neo.manager.LookupValue> lookUps = null;
            String lookupStr = "lookup_cache_" + lookupId;
            Object lookupObj = session.getAttribute(lookupStr);
            if (lookupObj != null) {
                lookUps = (List<neo.manager.LookupValue>) lookupObj;
            } else {
                try {
                    int lid = Integer.parseInt(lookupId);
                    lookUps = NeoCommand.service.getNeoManagerBeanPort().getCodeTable(new Integer(lookupId));
                    //order by type_id
                    if (sort != null) {
                        Collections.sort(lookUps, new Comparator<LookupValue>() {

                            /*
                             * public int compare(LookupValue o1, LookupValue
                             * o2) { if (o1.getId() == null || o2.getId() ==
                             * null) { return 0; } return
                             * Integer.parseInt(o1.getId()) -
                             * Integer.parseInt(o2.getId());
                        }
                             */
                            public int compare(LookupValue lok1, LookupValue lok2) {

                                String nValue1 = lok1.getValue().toUpperCase();
                                String nValue2 = lok2.getValue().toUpperCase();

                                //ascending order
                                return nValue1.compareTo(nValue2);

                                //descending order
                                //return fruitName2.compareTo(fruitName1);
                            }
                        });
                    }

                } catch (Exception e) {
                    lookUps = (List<neo.manager.LookupValue>) LookupUtils.getList(lookupId, user);
                }
//                session.setAttribute(lookupStr, lookUps);
            }
            out.println("<option value=\"\"></option>");
            boolean firstIndexSet = pageContext.getRequest().getAttribute(elementName) == null || selectedBox.isEmpty() ? false : true;
            for (int i = 0; i < lookUps.size(); i++) {
                LookupValue lookupValue = lookUps.get(i);
                if (lookupValue.getId().equalsIgnoreCase(selectedBox)) {
                    out.println("<option value=\"" + lookupValue.getId() + "\" selected >" + lookupValue.getValue() + "</option>");
                } else {
                    if (firstIndexSelected.equalsIgnoreCase("yes")) {
                        if (firstIndexSet) {
                            out.println("<option value=\"" + lookupValue.getId() + "\" >" + lookupValue.getValue() + "</option>");
                        } else {
                            out.println("<option value=\"" + lookupValue.getId() + "\" selected >" + lookupValue.getValue() + "</option>");
                            firstIndexSet = true;
                            System.out.println("found first index");
                        }
                    } else {
                        out.println("<option value=\"" + lookupValue.getId() + "\" >" + lookupValue.getValue() + "</option>");
                    }

                }
            }
            out.println("</select></td>");
            if (mandatory.equalsIgnoreCase("yes")) {
                out.println("<td><label class=\"red\">*</label></td>");
            } else {
                out.println("<td></td>");
            }

            String sessionValerror = "";
            if (errorValueFromSession.equalsIgnoreCase("yes")) {

                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                }

                if (sessionValerror != null && !sessionValerror.equalsIgnoreCase("null")) {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
                } else {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
                }
            }

            /*
             * if(errorValueFromSession.equalsIgnoreCase("yes")){ String
             * errorVal = "" + session.getAttribute(elementName + "_error");
             * if(errorVal == null || errorVal.trim().equalsIgnoreCase("null")){
             * errorVal = ""; } out.println("<td width=\"200px\"
             * align=\"left\"><label id=\"" + elementName + "_error\"
             * class=\"error\">"+ errorVal +"</label></td>"); }else{
             * out.println("<td width=\"200px\" align=\"left\"><label id=\"" +
             * elementName + "_error\" class=\"error\"></label></td>"); }
             */

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelNeoLookupValueDropDown tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setErrorValueFromSession(String errorValueFromSession) {
        this.errorValueFromSession = errorValueFromSession;
    }

    public void setFirstIndexSelected(String firstIndexSelected) {
        this.firstIndexSelected = firstIndexSelected;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.validation;

import com.koh.command.NeoCommand;
import com.koh.preauth.command.ForwardToBenefitAllocation;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthBenefitLimits;
import neo.manager.CoverDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Johan-NB
 */
public class ValidateTraumaCounselling extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        PrintWriter out = null;

        String errorResponse = "";
        String error = "";
        int errorCount = 0;
        List<AuthBenefitLimits> allBen = new ArrayList<AuthBenefitLimits>();
        LookupValue benAllocated = new LookupValue();

        //validation
        String authTypeStr = "" + session.getAttribute("authType");
        String authPeriod = "" + session.getAttribute("authPeriod");
        String provDesc = "" + session.getAttribute("treatingProviderDiscTypeId");
        
        //String optIdStr = "";//+ session.getAttribute("schemeOption");
        int optionId = 0;//Integer.parseInt(optIdStr);
        String memNo = "" + session.getAttribute("memNo");
        String depCodeStr = "" + session.getAttribute("depCode");
        String fromDateStr = "" + session.getAttribute("fromDate");

        //System.out.println("ValidateTraumaCounselling authType = " + authTypeStr);
        //System.out.println("ValidateTraumaCounselling authPeriod = " + authPeriod);
        //System.out.println("ValidateTraumaCounselling provider discipline = " + provDesc);
        //System.out.println("ValidateTraumaCounselling optIdStr = " + optIdStr);
        //System.out.println("ValidateTraumaCounselling memNo = " + memNo);
        //System.out.println("ValidateTraumaCounselling depCodeStr = " + depCodeStr);
        //System.out.println("ValidateTraumaCounselling fromDateStr = " + fromDateStr);

        //auth type
        if (authTypeStr == null || authTypeStr.trim().equalsIgnoreCase("99")) {
            errorCount++;
            error = error + "|authType:Authorisation Type is Mandatory";
        }

        //date
        if (authPeriod == null || authPeriod.trim().equalsIgnoreCase("99")) {
            errorCount++;
            error = error + "|authPeriod:Authorisation Period is Mandatory";
        } else {
            if (Integer.parseInt(authPeriod) < 2012) {
                errorCount++;
                error = error + "|authPeriod:Incorrect Authorisation Period";
            } else {
                authPeriod = authPeriod + "/01/01";
            }
        }

        //option
        if (memNo == null || memNo.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|memberNum:Member Number Required";
        } else {
            if (depCodeStr == null || depCodeStr.trim().equalsIgnoreCase("") || depCodeStr.trim().equalsIgnoreCase("99")) {
                errorCount++;
                error = error + "|depListValues:Member Dependant Required";
            } else {
                if (fromDateStr == null || fromDateStr.trim().equalsIgnoreCase("")) {
                    errorCount++;
                    error = error + "|authFromDate:Auth Start Date Required";
                } else {
                    Date fromDate = null;
                    XMLGregorianCalendar xDate = null;
                    try {
                        fromDate = new SimpleDateFormat("yyyy/MM/dd").parse(fromDateStr);
                    } catch (ParseException px) {
                        px.printStackTrace();
                        errorCount++;
                        error = error + "|authFromDate:Incorrect Date Format ("+fromDateStr+")";
                    }
                    if(errorCount == 0){
                        xDate = DateTimeUtils.convertDateToXMLGregorianCalendar(fromDate);
                        int depCode = Integer.parseInt(depCodeStr);
                        
                        CoverDetails cd = service.getNeoManagerBeanPort().getDependentForCoverByDate(memNo, xDate, depCode);
                        optionId = cd.getOptionId();
                    }
                }
            }
        }

        //provider
        if (provDesc == null || provDesc.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|treatingProvider:Treating Provider is Mandatory";
        } else {
            if (!provDesc.equals("014") && !provDesc.equals("015")
                    && !provDesc.equals("022") && !provDesc.equals("086")
                    && !provDesc.equals("088") && !provDesc.equals("089")) {
                errorCount++;
                error = error + "|treatingProvider:Trauma Counselling only allows 014, 015, 022, 086, 088, 089 disciplines";
            }
        }

        if (errorCount > 0) {
            errorResponse = "ERROR" + error;
        } else if (errorCount == 0) {
            //getBenefit
            int authType = Integer.parseInt(authTypeStr);
            XMLGregorianCalendar authDate = null;
            try {
                authDate = DateTimeUtils.convertDateToXMLGregorianCalendar(new SimpleDateFormat("yyyy/MM/dd").parse(authPeriod));
            } catch (ParseException ex) {
                Logger.getLogger(ForwardToBenefitAllocation.class.getName()).log(Level.SEVERE, null, ex);
            }

//            allBen = port.getAuthProductBenefits(authType, authDate, memNo, depCodeStr);
//            if (allBen != null && allBen.isEmpty() == false) {
//                for (AuthBenefitLimits lv : allBen) {
//                    benAllocated.setId(lv.getMidBenefitCode());
//                    benAllocated.setValue(lv.getMidDesc());
//                }
//                session.setAttribute("benAllocated", benAllocated);
                errorResponse = "Done|";
//
//            } else {
//                errorResponse = "ERROR|memberNum:Member Option not Applicable on this Auth";
//            }
        }

        try {
            out = response.getWriter();
            out.println(errorResponse);

        } catch (Exception ex) {
            System.out.println("ValidateTraumaCounselling error : " + ex.getMessage());
        } finally {
            out.close();
        }

        return null;
    }

    @Override
    public String getName() {
        return "ValidateTraumaCounselling";
    }
}

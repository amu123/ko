/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.tags;

import com.koh.utils.DateTimeUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CoverDetails;

/**
 *
 * @author johanl
 */
public class CoverSearchGrid extends TagSupport {

    private String sessionAttribute;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        String error = "";
        try {
            ArrayList<CoverDetails> members = (ArrayList<CoverDetails>) session.getAttribute(sessionAttribute);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            
            out.println("<label class=\"header\">Member Search Result</label></br></br>");
            
            String message = (String) req.getAttribute("coverSearchResultsMessage");
            if (message != null && !message.equalsIgnoreCase("")) {
                out.println("<label class=\"red\">" + message + "</label></br></br>");
            }

            if (members != null && !members.isEmpty()) {
                              
                out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.println("<tr>"
                        + "<th align=\"left\">Cover Number</th>"
                        + "<th align=\"left\">Option</th>"
                        + "<th align=\"left\">Status</th>"
                        + "<th align=\"left\">Join Date</th>"
                        + "<th align=\"left\">Option Date</th>"
                        + "<th align=\"left\">Resigned Date</th>"
                        + "<th></th>"
                        + "</tr>");

                for (int i = 0; i < members.size(); i++) {
                    CoverDetails cd = members.get(i);

                    String joinDate = "";
                    String benefitDate = "";
                    String resignDate = "";

                    //benefit start date
                    if (cd.getCoverStartDate() != null) {
                        benefitDate = dateFormat.format(cd.getCoverStartDate().toGregorianCalendar().getTime());
                    }

                    //join date
                    if (cd.getSchemeJoinDate() != null) {
                        joinDate = dateFormat.format(cd.getSchemeJoinDate().toGregorianCalendar().getTime());
                    } else {
                        joinDate = dateFormat.format(cd.getCoverStartDate().toGregorianCalendar().getTime());
                    }

                    //resign date
                    String dateTo = "";
                    if (cd.getCoverEndDate() != null) {
                        dateTo = DateTimeUtils.dateFormat.format(cd.getCoverEndDate().toGregorianCalendar().getTime());
                        if (!dateTo.equalsIgnoreCase("2999/12/31")) {
                            resignDate = dateTo;
                        }

                    }
                    
                    String startDate = DateTimeUtils.dateFormat.format(cd.getCoverStartDate().toGregorianCalendar().getTime());
                    String endDate = DateTimeUtils.dateFormat.format(cd.getCoverEndDate().toGregorianCalendar().getTime());

                    out.println("<tr>"
                            + "<td><label class=\"label\">" + cd.getCoverNumber() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getOptionName() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getStatus() + "</label></td>"
                            + "<td><label class=\"label\">" + joinDate + "</label></td>"
                            + "<td><label class=\"label\">" + benefitDate + "</label></td>"
                            + "<td><label class=\"label\">" + resignDate + "</label></td>");

                    if (javaScript != null && !javaScript.equalsIgnoreCase("")) {

                        if (javaScript.equalsIgnoreCase("memberMainSearch")) {
                            out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"reloadCoverDeps('" + cd.getCoverNumber() + "')\" >Details</button></td>");
                        } else {
                            out.println("<td><button name=\"opperation\" type=\"button\" " + javaScript + " >Details</button></td>");
                        }

                    }

                    out.println("</tr>");
                }
                out.println("</table>");
            }


        } catch (java.io.IOException ex) {
            throw new JspException("Error in PricipleMemberHistoryGrid tag", ex);
        }

        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

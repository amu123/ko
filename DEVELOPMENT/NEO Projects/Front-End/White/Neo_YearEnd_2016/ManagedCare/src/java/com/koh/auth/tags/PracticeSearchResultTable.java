/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.tags;

import com.koh.auth.dto.PracticeSearchResult;
import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author whauger
 */
public class PracticeSearchResultTable extends TagSupport {
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();

        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");

            String message = (String) req.getAttribute("searchPracticeResultMessage");

            if (message != null && !message.equalsIgnoreCase(""))
            {
                out.println("<label class=\"red\">" + message + "</label></br></br>");
            }

            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Practice Number</th><th>Practice Name</th><th>Practice Type</th><th>Network</th><th>Select</th>" +
                    "</tr>");

            ArrayList<PracticeSearchResult> providers = (ArrayList<PracticeSearchResult>) req.getAttribute("searchPracticeResult");
            if (providers != null) {
                for (int i = 0; i < providers.size(); i++) {
                    PracticeSearchResult practiceSearchResult = providers.get(i);
                    if (practiceSearchResult.getPracticeNetwork() == null){
                        practiceSearchResult.setPracticeNetwork("");
                    }
                    out.println("<tr><form method=\"post\"><td><label class=\"label\">" + practiceSearchResult.getPracticeNumber() + "</label><input type=\"hidden\" name=\"practiceNumber\" value=\"" + practiceSearchResult.getPracticeNumber() + "\"/></td>" +
                            "<td><label class=\"label\">" + practiceSearchResult.getPracticeName() + "</label><input type=\"hidden\" name=\"practiceName\" value=\"" + practiceSearchResult.getPracticeName() + "\"/></td>" +
                            "<td><label class=\"label\">" + practiceSearchResult.getPracticeType() + "</label><input type=\"hidden\" name=\"practiceType\" value=\"" + practiceSearchResult.getPracticeType() + "\"/></td>" +
                            "<td><label class=\"label\">" + practiceSearchResult.getPracticeNetwork() + "</label><input type=\"hidden\" name=\"practiceType\" value=\"" + practiceSearchResult.getPracticeNetwork() + "\"/></td>" +
                            "<td><button name=\"opperation\" name=\"opperation\" type=\"submit\" " + javaScript + " value=\"" + commandName + "\">Select</button> </td></form></tr>");
                }
            }
            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in PracticeSearchResultTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

}

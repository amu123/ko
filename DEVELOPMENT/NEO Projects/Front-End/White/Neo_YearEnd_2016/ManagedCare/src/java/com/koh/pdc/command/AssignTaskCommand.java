/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverPDCDetails;
import neo.manager.EventDetails;
import neo.manager.EventTask;
import neo.manager.MainList;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class AssignTaskCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        //System.out.println("entered AlllocateCallHistoryToSessionCommand");
        String in = request.getParameter("listIndex");

        if (in != null) {
            int index = new Integer(in);
            ArrayList<MainList> workList = (ArrayList<MainList>) session.getAttribute("mainList");
            MainList mainList = workList.get(index);
            session.setAttribute("selectedTask", mainList);

            NeoManagerBean port = service.getNeoManagerBeanPort();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
            String s = "";
            EventDetails ed = port.getEventDetailsByEventId(mainList.getEventId());
            CoverPDCDetails list = port.getCoverProductDetailsByCoverNumberForPDC(ed.getCoverNumber(), ed.getDependentNumber());
            EventTask et = port.getEventTaskByEventId(ed.getEventId());

            session.setAttribute("task", et.getTaskType().getValue());
            Date update = et.getTaskDueDate().toGregorianCalendar().getTime();
            s = formatter.format(update);
            session.setAttribute("dueDate", s);
            session.setAttribute("policyHolder", list.getPatientName());
            session.setAttribute("policyNumber", list.getCoverNumber());

            try {
                String nextJSP = "/PDC/ViewTask.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);

            } catch (ServletException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "AssignTaskCommand";
    }
}

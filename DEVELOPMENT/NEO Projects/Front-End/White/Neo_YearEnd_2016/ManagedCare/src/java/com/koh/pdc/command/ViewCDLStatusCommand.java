/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.PdcCDLStatus;

/**
 *
 * @author marcelp
 */
public class ViewCDLStatusCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("in ViewHIVStatusCommand");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        int entityId;
        PdcCDLStatus memberHIVStatusObj;
        int memberHIVStatusInd = 0;
        try {
            entityId = Integer.parseInt(session.getAttribute("entityId").toString());
            memberHIVStatusObj = port.getMemberCDLStatusDetails(entityId, 1);
            if (memberHIVStatusObj != null) {
                memberHIVStatusInd = memberHIVStatusObj.getRegisteredInd();
            }
            session.setAttribute("memberHIVStatusInd", memberHIVStatusInd);
            String nextJSP = "/PDC/MemberHIVStatus.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (NumberFormatException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewCDLStatusCommand";
    }
}

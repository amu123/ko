/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.SecurityResponsibility;
import neo.manager.UserProductRestriction;

/**
 * For user to update his own details
 *
 * @author josephm
 */
public class UpdateUserProfileCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        System.out.println("Inside UpdateUserProfileCommand");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        boolean administrator = false;

        NeoUser superUser = (NeoUser) session.getAttribute("persist_user");
        NeoUser user = new NeoUser();

        Integer userId = (Integer) session.getAttribute("userId");
        int superUserId = superUser.getUserId();

        String username = request.getParameter("username");
        user.setUsername(username);
        String name = request.getParameter("name");
        user.setName(name);
        String surname = request.getParameter("surname");
        user.setSurname(surname);
        String email = request.getParameter("email");
        user.setEmailAddress(email);
        user.setUserId(userId);
        String newPassword = request.getParameter("password");

        //get product restriction details
        Integer productRestr = null;
        if(request.getParameter("productRest") != null){
            productRestr = new Integer(request.getParameter("productRest"));
        }else{
            productRestr = 3;
        }

        UserProductRestriction userP = new UserProductRestriction();
        userP.setUserID(userId);
        userP.setProductID(productRestr);
        userP.setCreatedBy(superUser.getUserId());
        userP.setLastUpdatedBy(superUser.getUserId());
        port.updateUserProductRestriction(userP);

        ArrayList<Integer> resp = new ArrayList<Integer>();
        Enumeration e = request.getParameterNames();
        while (e.hasMoreElements()) {
            String pName = "" + e.nextElement();
            if (pName.contains("resp_")) {
                resp.add(new Integer(request.getParameter(pName)));
            }
        }
        for (SecurityResponsibility security : superUser.getSecurityResponsibility()) {

            if (security.getResponsibilityId() == 1 || security.getResponsibilityId() == 13) {

                administrator = true;
            }
        }

        boolean passUsed = port.validatePasswordHistory(newPassword, username);
        if (!passUsed) {
            session.setAttribute("password_error", "");
            user.setPassword(newPassword);
            port.updateUserDetailsAndPassword(user, resp, superUserId);

//        if (administrator) {
//            user.setCreatePswd(request.getParameter("password"));
//            port.updateUserDetailsAndPassword(user, resp, superUserId);
//        } else {
//            port.updateUserDetailsBySuperUser(superUserId, user, resp);
//        }

            try {

                PrintWriter out = response.getWriter();

                out.println("<html>");
                out.println("<head>");
                out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                out.println("</head>");
                out.println("<body>");
                out.println("<center>");
                out.println("<table cellspacing=\"4\">");
                out.println("<tr>");
                out.println("<td><label class=\"header\"><h2>User details have been successfully updated</h2></label></td>");
                out.println("</tr>");
                out.println("</table>");
                out.println("<p> </p>");
                out.println("</center>");
                out.println("</body>");
                out.println("</html>");

                response.setHeader("Refresh", "2; URL=/ManagedCare/SystemAdmin/UpdateUserProfile.jsp");

                //String nextJSP = "/SystemAdmin/UpdateUserProfile.jsp";
                //RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                //dispatcher.forward(request, response);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return null;
    }

    @Override
    public String getName() {

        return "UpdateUserProfileCommand";
    }
}

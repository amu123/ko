/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.tags;

import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.GenericTariff;

/**
 *
 * @author josephm
 */
public class BiometricsTariffListTableTag extends TagSupport {
    private String sessionAttribute;
    private String commandName;
    private String javaScript;
    private String tariffType;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();

        //List<AuthTariffDetails> atList =
        try {

            out.println("<td colspan=\"6\"><table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th colspan=\"6\">Biometrics Tariff(s) Allocated</tr>");
            out.println("<tr><th>Tariff Description</th><th>Tariff Code</th><th>Test Result</th><th></th><th></th></tr>");

            ArrayList<GenericTariff> atList = (ArrayList<GenericTariff>) session.getAttribute("biometricsTariffListArray");
            //ArrayList<GenericTariff> atList = (ArrayList<GenericTariff>) session.getAttribute("searchTariffResult");

            if (atList != null && atList.size() != 0) {
                for (int i =1; i <= atList.size(); i++) {
                    GenericTariff at = atList.get(i-1);
                    out.println("<tr id=\"tariffRow\"><form>" +
                                    "<td><label class=\"label\">" + at.getTariffDescription() + "</label></td> " +
                                    "<td><label class=\"label\">" + at.getTariffCode() + "</label><input type=\"hidden\" name=\"tariffCode\" id=\"tariffCode\" value=\"" + at.getTariffCode() + "\"/></td> " +
                                    "<td><label class=\"label\">" + at.getTestResult() + "</label><input type=\"hidden\" name=\"testResults\" id=\"testResults\" value=\"" + at.getTestResult() + "\"/></td> " +
                                    "<td><button type=\"button\" onClick=\"ModifyTariffFromList(" + i + ");\" >Modify</button></td>" +
                                    "<td><button type=\"button\" onClick=\"RemoveTariffFromList(" + i + ");\" >Remove</button></td>" +
                               "</form></tr>");
                }
            }
            out.println("</table></td>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberSearchTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setTariffType(String tariffType) {
        this.tariffType = tariffType;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import com.agile.security.webservice.EmailAttachment;
import com.agile.security.webservice.EmailContent;
import com.koh.command.FECommand;
import com.koh.serv.PropertiesReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Johan-NB
 */
public class SubmitStatementCommand extends FECommand {

    private Logger logger = Logger.getLogger(SubmitStatementCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();

        this.saveScreenToSession(request);
        SimpleDateFormat dTOs = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sTOd = new SimpleDateFormat("yyyy/MM/dd");

        String actionType = request.getParameter("opperationParam");
        String stmtType = request.getParameter("stmtType");

        boolean memberDir = false;
        String directoryType = "";
        String memberNo = "";
        String providerNo = "";
        String fileName = "";
        String emailFrom = "";

        //date manipulation
        String stmtDate = request.getParameter("stmtDatesCorrect");
        String dateStr = "";

        String productId = null;
        if (stmtType.trim().equalsIgnoreCase("1")) {
            productId = String.valueOf(session.getAttribute("StatementProductID"));
        } else if (stmtType.trim().equalsIgnoreCase("2")) {
            productId = request.getParameter("schemeOption");
        }

        //System.out.println("ProductID :: " + productId + " :: ");

        String scheme = "Agility";

        try {

            String path = new PropertiesReader().getProperty("statements");

            if (productId != null && !productId.equalsIgnoreCase("")) {
                if (productId.trim().equalsIgnoreCase("1")) {
                    path += "ResolutionHealth\\";
                    scheme = "Resolution Health";
                    emailFrom = "Noreply@agilityghs.co.za";
                } else if (productId.trim().equalsIgnoreCase("2")) {
                    path += "Spectramed\\";
                    scheme = "Spectramed";
                    emailFrom = "Statements@spectramed.co.za";
                } else if (productId.trim().equalsIgnoreCase("3")){
                    path += "Sizwe\\";
                    scheme = "Sizwe";
                    emailFrom = "do-not-reply@sizwe.co.za";
                }
            }

            Date date = sTOd.parse(stmtDate.trim());
            dateStr = dTOs.format(date);

            if (stmtType.trim().equals("1")) {
                directoryType = "Member";
                memberNo = request.getParameter("memberNo_text");
                fileName = "Statement_" + memberNo + ".pdf";
                memberDir = true;
            } else if (stmtType.trim().equals("2")) {
                directoryType = "Provider";
                providerNo = request.getParameter("providerNo_text");
                fileName = "Statement_" + providerNo + ".pdf";
            }
            String emailTo = request.getParameter("emailAddress");

            boolean emailSent = false;


            System.out.println("statements directory: " + path);

            String patchVar = "";
            if (memberDir) {
                patchVar = path + dateStr + "\\" + directoryType + "\\Statement_" + memberNo + ".pdf";
            } else {
                patchVar = path + dateStr + "\\" + directoryType + "\\Statement_" + providerNo + ".pdf";
            }
            logger.info("Path: " + patchVar);

            File file = new File(patchVar);

            InputStream in = new FileInputStream(file);
            int contentLength = (int) file.length();
            logger.info("The content length is " + contentLength);

            ByteArrayOutputStream temporaryOutput;
            if (contentLength != -1) {
                temporaryOutput = new ByteArrayOutputStream(contentLength);
            } else {
                temporaryOutput = new ByteArrayOutputStream(20480);
            }

            byte[] bufer = new byte[512];
            while (true) {
                int len = in.read(bufer);
                if (len == -1) {
                    break;
                }
                temporaryOutput.write(bufer, 0, len);
            }
            in.close();
            temporaryOutput.close();

            byte[] array = temporaryOutput.toByteArray();

            // test to see if the file is corrupt
            logger.info("actionType is " + actionType);

            if (actionType != null && actionType.equalsIgnoreCase("Preview")) {
                printPreview(request, response, array, fileName);

            } else if (emailTo != null && !emailTo.equalsIgnoreCase("")) {
                String subject = scheme;
                String emailMsg = null;
                if (memberDir) {
                    subject += " - Statements For Member " + memberNo;
                } else {
                    subject += " - Statements For Provider " + providerNo + " For Payment Process Date " + stmtDate;
                }

                EmailContent content = new EmailContent();
                content.setContentType("text/plain");
                //1
                EmailAttachment attachment1 = new EmailAttachment();
                attachment1.setContentType("application/octet-stream");
                attachment1.setName(fileName);
                content.setFirstAttachment(attachment1);
                content.setFirstAttachmentData(array);
                
                content.setSubject(subject);
                content.setEmailAddressFrom(emailFrom);
                content.setEmailAddressTo(emailTo);
                content.setProductId(new Integer(productId));
                //Determine msg for email according to type of document
                content.setType("claim");
                /*If document type is custom, this message added here will be displayed in the email. The email method will automatically add the 
                 the enquiries and call center aswell as the kind regards at the end of the message*/
                content.setContent(emailMsg);
                emailSent = service.getAgileManagerPort().sendEmailWithOrWithoutAttachment(content);
                logger.info("Email sent : " + emailSent);
                printEmailResult(request, response, emailSent);
            } else {
                printEmailResult(request, response, emailSent);
            }

        } catch (MalformedURLException ex) {
            ex.printStackTrace();

        } catch (IOException ioex) {
            // FileNotFound exception
            printErrorResult(request, response);
            logger.info(ioex.getMessage());
            //System.out.println("The ioex.getMessage() " + ioex.getMessage());
            ioex.printStackTrace();
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(SubmitStatementCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "SubmitStatementCommand";
    }

    public void printPreview(HttpServletRequest request, HttpServletResponse response, byte[] array, String fileName) throws IOException {
        OutputStream outStream = null;
        try {
            response.setHeader("Pragma", "no-cache"); //HTTP 1.0
            response.setDateHeader("Expires", 0); //prevents caching at the proxy server
            response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-disposition", "attachment; filename=" + fileName);

            response.setContentType("application/pdf");
            response.setContentLength(array.length);
            outStream = response.getOutputStream();
            outStream.write(array);
            outStream.flush();
            outStream.close();

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void printResult(HttpServletRequest request, HttpServletResponse response, boolean result) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (result) {
                out.println("<td><label class=\"header\">Statement emailed.</label></td>");
            } else {
                //out.println("<td><label class=\"header\">No payment(s) found.</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    public void printEmailResult(HttpServletRequest request, HttpServletResponse response, boolean result) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (result) {
                out.println("<td><label class=\"header\">Statement emailed.</label></td>");
            } else {
                out.println("<td><label class=\"header\">Email sending failed, Either email address is missing or incorrect.</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "3; URL=/ManagedCare/Statement/Statements.jsp");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    public void printErrorResult(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");

            out.println("<td><label class=\"header\">No records found.</label></td>");

            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "3; URL=/ManagedCare/Statement/Statements.jsp");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }
}

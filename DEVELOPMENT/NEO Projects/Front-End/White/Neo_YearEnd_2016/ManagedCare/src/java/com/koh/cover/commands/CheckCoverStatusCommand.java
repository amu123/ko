/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.koh.command.NeoCommand;
import com.koh.cover.MemberMainMapping;
import com.koh.employer.command.TabUtils;
import com.koh.fe.command.LoginCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;
import org.apache.log4j.Logger;

/**
 *
 * @author johanl
 */
public class CheckCoverStatusCommand extends NeoCommand {
    
    private Logger logger = Logger.getLogger(CheckCoverStatusCommand.class);

    NeoManagerBean port = service.getNeoManagerBeanPort();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    private static XMLGregorianCalendar endCal = null;
    
    public CheckCoverStatusCommand() {
        try {
            endCal = DateTimeUtils.convertDateToXMLGregorianCalendar(sdf.parse("2999/12/31"));
        } catch (ParseException ex) {
            logger.error(ex);
        }
    }
    
    

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        logger.info("^^^^^^^^^^^^^^^^^^^ SaveCoverDetailsCommand execute ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        String result = validateCoverStatus(request);
        
        logger.info("result : " + result);
                
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
        } catch (IOException ex) {
            logger.error(ex);
        }
        logger.info("^^^^^^^^^^^^^^^^^^^ SaveCoverDetailsCommand execute end ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        return null;
    }


    private String validateCoverStatus(HttpServletRequest request) {
   
        Map<String, String> errors = MemberMainMapping.validateCover(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (save(request)) {
                additionalData = "\"message\":\"Cover Details Updated\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error updating the Cover details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
    }

    private boolean save(HttpServletRequest request) {
        boolean result = true;
        
        NeoUser neoUser = LoginCommand.getNeoUser(request);

        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);

        int memberEntityId = MemberMainMapping.getEntityId(request);
        
        logger.info("memberEntityId : " + memberEntityId);
        
        String optionChangeStartDate = request.getParameter("newStartDate");
        logger.info("New Start Date : " + optionChangeStartDate);
        
        String employerChangeStartDate = request.getParameter("newEmployerStartDate");
        logger.info("New Employer Change Date : " + employerChangeStartDate);
        
        String brokerChangeStartDate = request.getParameter("newBrokerStartDate");
        logger.info("New Broker Change Date : " + brokerChangeStartDate);
        
        String suspensionStartDate = request.getParameter("suspensionStartDate");
        logger.info("New suspension start date : " + suspensionStartDate);
      
        try {
           
            if (!optionChangeStartDate.isEmpty()) {
                saveOptionChange(request, memberEntityId, sdf.parse(optionChangeStartDate));
            }
            
            if (!employerChangeStartDate.isEmpty()) {
                saveGroupChange(request, memberEntityId, sdf.parse(employerChangeStartDate));
            }
            
            if (!brokerChangeStartDate.isEmpty()) {
               saveBrokerChange(request, memberEntityId, sdf.parse(brokerChangeStartDate));
            }
            
            if (!suspensionStartDate.isEmpty()) {
               saveSuspension(request, memberEntityId, sdf.parse(suspensionStartDate));
            }
            
        } catch (java.lang.Exception e) {
            logger.error(e);
            result = false;
        }
        
        return result;
    }
    
    /* UPDATES NEO_COVER_DETAILS */
    private void saveOptionChange(HttpServletRequest request,int memberEntityId,Date newStartDate) {
        
        CoverDetails cd = MemberMainMapping.getCoverDetails(request, memberEntityId);
           
        String newOptionIdS = request.getParameter("optionList");
        logger.info("New newOptionId : " + newOptionIdS);

        
        List<CoverDetails> coverDetails = port.getCoverDetailsByCoverNumberByDate(cd.getCoverNumber(),endCal);

        logger.info("Total covDet returned : " + coverDetails.size());
        
        for (CoverDetails covDet : coverDetails) {
            
            logger.info("CovDet endDate : " + DateTimeUtils.convertXMLGregorianCalendarToDate(covDet.getEffectiveEndDate()));
        
            //FIND LATEST RECORD
            if (DateTimeUtils.convertXMLGregorianCalendarToDate(covDet.getEffectiveEndDate()).equalsIgnoreCase("2999/12/31")) {
                
                logger.info("Latest CoverDetail found : " + covDet.getCoverNumber() 
                        + " "  + DateTimeUtils.convertXMLGregorianCalendarToDate(covDet.getEffectiveEndDate()));
                
                logger.info("Cover Number  : " + cd.getCoverNumber());
        
                //CALL NEW BACKEND WEBSERICE TO UPDATE DETAILS
                port.updateCoverDetailsOption(cd.getCoverNumber(),cd.getDependentNumber(),
                        covDet.getOptionId(), Integer.valueOf(newOptionIdS),
                    DateTimeUtils.convertDateToXMLGregorianCalendar(newStartDate));

            }
        }
    }
    
    /* UPDATES NEO_COVER_DETAILS */
    private void saveSuspension(HttpServletRequest request,int memberEntityId,Date newStartDate) {
        
        CoverDetails cd = MemberMainMapping.getCoverDetails(request, memberEntityId);

        XMLGregorianCalendar endDate = DateTimeUtils.convertDateToXMLGregorianCalendar(newStartDate);
        
        //CALL NEW BACKEND WEBSERICE TO UPDATE DETAILS
        port.updateCoverDetailsSuspend(cd.getCoverNumber(), endDate); 
        
    }
    
    /* UPDATES NEO_MEMBER_EMPLOYER */
    private void saveGroupChange(HttpServletRequest request,int memberEntityId,Date newStartDate) {
        
        //CALL NEW BACKEND WEBSERICE TO UPDATE DETAILS
        String newEmployerEntityIdS =  request.getParameter("employerList");
        int newEmployerEntityIdI = Integer.parseInt(newEmployerEntityIdS);
      
        logger.info("New Employer EntityId : " + newEmployerEntityIdS);
        logger.info("Member EntityID : " + memberEntityId);

        //FIND ALL MEMBER_EMPLOYER RECORD FOR MEMBER
        List<MemberEmployer> memEmployers = port.findMemberEmployerByMemberEntityId(memberEntityId);
        
        logger.info("Total MemberEmployer returned for member EntityID : " + memEmployers.size());

        //IF NO HISTORY FOUND
        if (memEmployers.size()==1) {
            
            port.updateMemberEmployer(
                        memEmployers.get(0),DateTimeUtils.convertDateToXMLGregorianCalendar(
                        newStartDate), newEmployerEntityIdI); 

        } else {
            
            //THERE SHOULD BE ONLY 1 RECORD WITH END DATE 2999(CAN BE FUTURE DATED)
            MemberEmployer currentEmployer = null;

            //FIND LATEST RECORD
            for (MemberEmployer memEmp : memEmployers) {
                if (DateTimeUtils.convertXMLGregorianCalendarToDate(memEmp.getEffectiveEndDate()).equalsIgnoreCase("2999/12/31")) {
                    currentEmployer = memEmp;
                    break;
                }
            }
        
            port.updateMemberEmployer(
                            currentEmployer,DateTimeUtils.convertDateToXMLGregorianCalendar(
                            newStartDate), newEmployerEntityIdI); 

        }
        
    }
    
    /* UPDATES NEO_MEMBER_BROKER */
    private void saveBrokerChange(HttpServletRequest request,int memberEntityId,Date newStartDate) {
        
        String newBrokerEntityIdS = request.getParameter("brokerList");
        int newBrokerEntityIdI = Integer.parseInt(newBrokerEntityIdS);

        logger.info("New Broker EntityId : " + newBrokerEntityIdI);
        logger.info("Member EntityID : " + memberEntityId);

        List<MemberBroker> memBrokers = port.findMemberBrokerByMemberEntityId(memberEntityId);
        logger.info("Total MemberBroker returned for member EntityID : " + memBrokers.size());

        if (memBrokers.size()==1) {
            
            port.updateMemberBroker(memBrokers.get(0),DateTimeUtils.convertDateToXMLGregorianCalendar(
                        newStartDate), newBrokerEntityIdI); 
            
        } else {
            
            //THERE SHOULD BE ONLY 1 RECORD WITH END DATE 2999(CAN BE FUTURE DATED)
            MemberBroker currentBroker = null;
            
            //FIND LATEST RECORD
            for (MemberBroker memBroker : memBrokers) {
                if (DateTimeUtils.convertXMLGregorianCalendarToDate(memBroker.getEffectiveEndDate()).equalsIgnoreCase("2999/12/31")) {
                    currentBroker = memBroker;
                    break;
                }
            }
            
            port.updateMemberBroker(currentBroker,DateTimeUtils.convertDateToXMLGregorianCalendar(
                        newStartDate), newBrokerEntityIdI); 
            
        }
    }
    
    @Override
    public String getName() {
        return "CheckCoverStatusCommand";
    }
}

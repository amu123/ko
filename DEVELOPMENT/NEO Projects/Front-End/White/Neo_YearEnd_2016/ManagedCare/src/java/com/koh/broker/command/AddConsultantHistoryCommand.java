/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.brokerFirm.ConsultantMapping;
import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.member.application.command.MemberApplicationSpecificQuestionsCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.BrokerBrokerConsult;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author princes
 */
public class AddConsultantHistoryCommand extends NeoCommand{
    
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        request.setAttribute("consultantEntityId", request.getParameter("consultantEntityId"));
        try {
            String s = request.getParameter("buttonPressed");
            System.out.println("buttonPressed " + s);
            if (s != null && !s.isEmpty()) {
                if (s.equalsIgnoreCase("Add History")) {
                    addDetails(request, response, context);
                } else if (s.equalsIgnoreCase("SaveButton")) {
                    save(request, response, context);
                } else if (s.equalsIgnoreCase("SearchBrokerButton".trim())) {
                    System.out.println("broker");
                    searchFirm(request, response, context);
                } else if(s.equalsIgnoreCase("End")){
                    endBrokerLinktoBrokerConsultant(request, response, context);
                }else if(s.equalsIgnoreCase("EndBroker")){
                    endBrokerWithReason(request, response, context);
                }
            }

//            request.setAttribute("MemAppSearchResults", col);
//            context.getRequestDispatcher("/MemberApplication/MemberAppSearchResults.jsp").forward(request, response);
        } catch (java.lang.Exception ex) {
            Logger.getLogger(MemberApplicationSpecificQuestionsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void addDetails(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));

        context.getRequestDispatcher("/Broker/LinkConsultantToBroker.jsp").forward(request, response);
    }
    
    private void searchFirm(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
//            request.setAttribute("MemAppSearchResults", col);
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        request.setAttribute("diagCodeId", "Disorder_diagCode_text");
        request.setAttribute("diagNameId", "Disorder_diagName");
        context.getRequestDispatcher("/Broker/ConsultantBorkerSearch.jsp").forward(request, response);

    }
    
    private int getIntParam(HttpServletRequest request, String paramName) {
        try {
            String maxDepStr = request.getParameter(paramName);
            return Integer.parseInt(maxDepStr);
        } catch (java.lang.Exception e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }
        
    private void save(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        try{
        int entityId = Integer.parseInt(request.getParameter("consultantEntityId"));
        int brokerEntityId = Integer.parseInt(request.getParameter("brokerEntityId"));
        NeoManagerBean port = service.getNeoManagerBeanPort();
        List<BrokerBrokerConsult> brokerConsultantList = port.fetchBrokerConsultantByBrokerEntityID(brokerEntityId);
        boolean isOverlapping = false;
        for(BrokerBrokerConsult consultObj:brokerConsultantList){
            if(brokerEntityId==consultObj.getBrokerEntityId()){
//                System.out.println("Broker found : "+brokerEntityId);
                try{
                   Date date1 = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("ConsApp_historyStartDate"));
                   Date date2 = DateTimeUtils.convertFromYYYYMMDD(DateTimeUtils.convertXMLGregorianCalendarToDate(consultObj.getEffectiveEndDate()));
                   if( date2.getTime() >= date1.getTime()){
                        isOverlapping = true;
                    }
                }catch(Exception e){
                    System.out.println("Failed to compare overlapping dates : " + e.getMessage());
                }
            }
       }
        if(!isOverlapping){
            Map<String, String> errors = ConsultantMapping.validateHistory(request);
            String validationErros = TabUtils.convertMapToJSON(errors);
            String status = TabUtils.getStatus(errors);
            String additionalData = null;
            String updateFields = null;

            if ("OK".equalsIgnoreCase(status)) {
                if (save(request, entityId)) {
                    
                } else {
                    status = "ERROR";
                    additionalData = "\"message\":\"There was an error saving history.\"";
                }
            } else {
                additionalData = null;
            }

            if ("OK".equalsIgnoreCase(status)) {                        
                context.getRequestDispatcher(new ConsultantAppTabContentsCommand().getConsultantHistory(request, entityId)).forward(request, response);

            } else {
                PrintWriter out = response.getWriter();
                out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
            }
        }else{
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult("ERROR", null, null, "\"message\":\"Warning, the dates entered overlaps with the previous broker data\""));
        }
        }catch(NumberFormatException ne){
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult("ERROR", null, null,"\"message\":\"There was an error saving history.\""));
        }
    }
    
    private boolean save(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = getNeoUser(request);
        
        try {
            BrokerBrokerConsult ba = ConsultantMapping.getConsultantHistoryDetails(request, entityId, neoUser);
            port.saveBrokerConsultanLink(ba);
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
        
    }
    
    private void endBrokerLinktoBrokerConsultant(HttpServletRequest request , HttpServletResponse response , ServletContext context) throws ServletException, IOException{
        request.setAttribute("endbrokerBrokerConsultantId", request.getParameter("brokerBrokerConsultantID"));
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        context.getRequestDispatcher("/Broker/EndBrokerLinkToConsultant.jsp").forward(request, response);
    }
    
    private void endBrokerWithReason (HttpServletRequest request , HttpServletResponse response , ServletContext context) throws ServletException, IOException{
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String reason = request.getParameter("reasonForEndingBroker")!= null?request.getParameter("reasonForEndingBroker"):"";
        String endDate = request.getParameter("Cons_historyEndDate")!= null?request.getParameter("Cons_historyEndDate"):"";
        String brokerBrokerConsultantId = request.getParameter("endbrokerBrokerConsultantId");
        String erroMessage = null;
        int entityId = new Integer(request.getParameter("consultantEntityId"));
        boolean success = false;
        if(!reason.isEmpty() && !endDate.isEmpty() && !reason.equals("") && !endDate.equals("") && !reason.equals("0")){
            try{
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat format= new SimpleDateFormat("yyyy/MM/dd");
                Date date = format.parse(endDate);
                cal.add(Calendar.MONTH, -5);
                if(cal.getTimeInMillis() <= date.getTime()){
//                      success = true;
                    success = port.endBrokerForConsultant(Integer.parseInt(brokerBrokerConsultantId),Integer.parseInt(reason), getNeoUser(request).getUserId(),DateTimeUtils.convertDateToXMLGregorianCalendar(DateTimeUtils.convertFromYYYYMMDD(endDate))); 
                    erroMessage = "\"message\":\"There was error while saving data.\"";
                }else{
                    success = false;
                    erroMessage = "\"message\":\"Please enter a date from last five month\"";
                }
            }catch(ParseException pe){
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,"Failed to parse end date",pe);
                success = false;
                erroMessage = "\"message\":\"Please enter correct date.\"";
            }
        }else{
            success = false;
            erroMessage = "\"message\":\"Please fill all the mandatory fields.\"";
        }
        if(!success){
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult("ERROR", null, null, erroMessage ));
        }else{
            context.getRequestDispatcher(new ConsultantAppTabContentsCommand().getConsultantHistory(request, entityId)).forward(request, response);
        }
    }
    
    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser)request.getSession().getAttribute("persist_user");
    }
    
    private String getDateStr(XMLGregorianCalendar xgc) {
        try {
            Date dt = xgc.toGregorianCalendar().getTime();
            return DateTimeUtils.convertToYYYYMMDD(dt);
        } catch (java.lang.Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
    }
    


    @Override
    public String getName() {
        return "AddConsultantHistoryCommand";
    }

   
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthTariffDetails;
import neo.manager.OrthodonticPlan;

/**
 *
 * @author johanl
 */
public class GetAuthOrthoPlanDetails extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;

        //get values from request
        String provTypeId = "" + session.getAttribute("treatingProviderDiscTypeId");
        String authPeriod = "" + session.getAttribute("authPeriod");
        //generate date from period
        String cDate = authPeriod.trim() + "/01/01";
        Date fromDate = null;
        XMLGregorianCalendar xFromDate = null;
        try{
            fromDate = new SimpleDateFormat("yyyy/MM/dd").parse(cDate);
            xFromDate = DateTimeUtils.convertDateToXMLGregorianCalendar(fromDate);
        }catch(ParseException px){
            px.printStackTrace();
        }

        //get dentalTCode for clinical code
        String tariffSesVal = request.getParameter("sesVal");
        List<AuthTariffDetails> ctList = (List<AuthTariffDetails>) session.getAttribute(tariffSesVal);
        String tariff = ctList.get(0).getTariffCode();
        System.out.println("ortho tariff = " + tariff);
        System.out.println("provider typ id = "+provTypeId);
        OrthodonticPlan op = service.getNeoManagerBeanPort().findOrthoPlanForCode(provTypeId, tariff, xFromDate);
        try {
            out = response.getWriter();
            if (op != null) {
                String authDate = "" + session.getAttribute("authDate");

                session.setAttribute("OrthoPlanId", op.getPlanId());
                String planDuration = String.valueOf(op.getDuration());
                String totAmount = String.valueOf(op.getTotalAmount());
                String depAmount = String.valueOf(op.getDeposit());
                String firstInstall = String.valueOf(op.getFirstInstallment());
                String remainInstall = String.valueOf(op.getMonthlyInstallment());

                System.out.println("planDuration = " + planDuration);
                System.out.println("totAmount = " + totAmount);
                System.out.println("depAmount = " + depAmount);
                System.out.println("firstInstall = " + firstInstall);
                System.out.println("remainInstall = " + remainInstall);

                session.setAttribute("planDuration", planDuration);
                session.setAttribute("amountClaimed", totAmount);
                session.setAttribute("depositAmount", depAmount);
                session.setAttribute("firstAmount", firstInstall);
                session.setAttribute("remainAmount", remainInstall);
                session.setAttribute("authFromDate", authDate);
                

                out.println("Done|");

            } else {
                out.println("Error|No Orthodontic Plan for Tariff");

            }
        } catch (Exception ex) {
            System.out.println("GetAuthOrthoPlanDetails error : "+ex.getMessage());
            out.println("Error|");
        }finally{
            out.close();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetAuthOrthoPlanDetails";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;

import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import java.io.*;
import java.lang.Exception;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;

/**
 *
 * @author Antaryami
 */
public class DocumentReIndexCommand extends NeoCommand {

    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String entityNumber = request.getParameter("indexEntityNumber")!=null ? request.getParameter("indexEntityNumber") : "" ;
        String entityType = request.getParameter("indexEntityType");
        String entityrefNumber = request.getParameter("indexRefNumber")!=null? request.getParameter("indexRefNumber") : "" ;
        String entityFileLocation = request.getParameter("indexFileLocation");
        String indexID = request.getParameter("indexID");
        String docType = request.getParameter("indexDocType");
        PrintWriter out;
//        System.out.println("DocumentReIndexCommand called entityNumber : " + entityNumber + " entityType : " + entityType + " entityrefNumber : " + entityrefNumber + " entityFileLocation : " + entityFileLocation + " indexID : " + indexID + " docType : " + docType);
        try {
            IndexDocumentRequest req = new IndexDocumentRequest();
            NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
            req.setAgent(loggedInUser);
            req.setIndexType("ReIndex");
            req.setIndexId(Long.parseLong(indexID));
            if(!entityNumber.equals("")){
                String result = validateMember(entityNumber, request.getSession());
                if(!result.contains("Error")){
                    req.setEntityNumber(entityNumber);
                }else{
                    out = response.getWriter();
                    out.write("Please enter valid member number");
                    return null ;
                }
            }else{
                out = response.getWriter();
                out.write("Please enter valid member number");
                return null ;
            }
            req.setEntityType(Integer.parseInt(entityType));
            if(entityrefNumber.equals("")){
                out = response.getWriter();
                out.write("Please enter valid claim number");
                return null;
            }else if(!entityrefNumber.equals("None")){
                if(!numericValidation(entityrefNumber,entityNumber)){
                    out = response.getWriter();
                    out.write("Please enter valid claim number");
                    return null;
                }else{
                    req.setRef(entityrefNumber);
                }
            }
            req.setDocumentType(docType);
            String indexDrive = new PropertiesReader().getProperty("DocumentIndexIndexDrive");
            byte[] byteStream = getByteStream(indexDrive + entityFileLocation);
            req.setDocByte(byteStream);
//            System.out.println("Agent : "+req.getAgent());
//            System.out.println("Index Type : " + req.getIndexType());
//            System.out.println("Index ID : "+req.getIndexId());
//            System.out.println("Entity Number : "+ req.getEntityNumber());
//            System.out.println("Entity Type : "+ req.getEntityType());
//            System.out.println("Reference number : "+ req.getRef());
//            System.out.println("Doc Type : "+ req.getDocumentType());
//            System.out.println("Byte Stream : "+ req.getDocByte());
            IndexDocumentResponse res = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
//            System.out.println(res.getMessage());
            out = response.getWriter();
            out.print(res.getMessage());
        } catch (Exception ie) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Failed to Reindex the file",ie);
        }
        return getName();
    }

    public byte[] getByteStream(String fileLocation)throws FileNotFoundException,IOException {
            byte[] bytes = null;
            File file = new File(fileLocation);
            InputStream in = new FileInputStream(file);
            int contentLength = (int) file.length();
            ByteArrayOutputStream temporaryOutput;
            if (contentLength != -1) {
                temporaryOutput = new ByteArrayOutputStream(contentLength);
            } else {
                temporaryOutput = new ByteArrayOutputStream(20480);
            }

            byte[] bufer = new byte[512];
            while (true) {

                int len = in.read(bufer);
                if (len == -1) {

                    break;
                }
                temporaryOutput.write(bufer, 0, len);
            }
            in.close();
            temporaryOutput.close();

            bytes = temporaryOutput.toByteArray();
        
        return bytes;
    }
    
    private String validateMember(String number , HttpSession session) throws Exception{
        boolean viewStaff = (Boolean) session.getAttribute("persist_user_viewStaff");
        String email = "";
        String message="";
            NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
            CoverDetails c = port.getEAuthResignedMembers(number);
            Logger.getLogger(this.getClass().getName()).info("viewStaff " + viewStaff);
            if (!viewStaff) {
                if (c != null && c.getEntityId() > 0) {
                    Member mem = port.fetchMemberAddInfo(c.getEntityId());
                    c.setMemberCategory(mem.getMemberCategoryInd());
                    Logger.getLogger(this.getClass().getName()).info("EntityId " + c.getEntityId() + " MemberCategoryInd " +mem.getMemberCategoryInd() );
                    if (c.getMemberCategory() != 2) {
                        //Get email contact details
                        ContactDetails cd = port.getContactDetails(c.getEntityId(), 1);
                        if (cd != null && cd.getPrimaryIndicationId() == 1) {
                            email = cd.getMethodDetails();
                        }
                        Logger.getLogger(this.getClass().getName()).info("Response : " + getName() + "|Number=" + number + "|Email=" + email + "$");
                        message = getName() + "|Number=" + number + "|Email=" + email + "|entityId=" + c.getEntityId() + "$";
                    } else {
                        message = "Access|User access denied. Please contact your supervisor|" + getName();
                    }
                } else {
                    message = "Error|No such member|" + getName();
                }
            } else {
                if (c != null && c.getEntityId() > 0) {
                    Logger.getLogger(this.getClass().getName()).info("EntityId " + c.getEntityId() + " No MemberCategoryInd loaded");
                    //Get email contact details
                    ContactDetails cd = port.getContactDetails(c.getEntityId(), 1);
                    if (cd != null && cd.getPrimaryIndicationId() == 1) {
                        email = cd.getMethodDetails();
                    }
                    Logger.getLogger(this.getClass().getName()).info("Response : " + getName() + "|Number=" + number + "|Email=" + email + "$");
                    message = getName() + "|Number=" + number + "|Email=" + email + "|entityId=" + c.getEntityId() + "$";

                } else {
                    message = "Error|No such member|" + getName();
                }
            }
        
        return message;
    }
    
    private boolean numericValidation(String number,String coverNumber){
        boolean valid = false;
        try{
            long claimNo = Long.parseLong(number);
            NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
            valid = port.validateClaimForMember(claimNo, coverNumber);
        }catch(NumberFormatException ne){
            valid = false;
        }catch(Exception e){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Failed to validate claim number", e);
            valid = false;
        }
        return valid;
    }
    @Override
    public String getName() {
        return "DocumentReIndexCommand";
    }
}

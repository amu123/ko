/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.tags;


import com.koh.command.NeoCommand;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CoverDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class CoverDependentPenaltyJoin extends TagSupport {

    private String commandName;
    private String javaScript;
    private String sessionAttribute;
    private String onScreen;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

        try {
            List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute(sessionAttribute);

            if (cdList != null && cdList.isEmpty() == false) {
                //get dependant 
                int depCode = -1;
                if (session.getAttribute("depListValues") != null) {
                    String depCodeStr = "" + session.getAttribute("depListValues");
                    System.out.println("Member search grid depCodeStr = " + depCodeStr);
                    if (depCodeStr != null && !depCodeStr.equalsIgnoreCase("") && !depCodeStr.equalsIgnoreCase("null")) {
                        depCode = Integer.parseInt(depCodeStr);
                    }
                }

                out.println("<table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.println("<tr>"
                        + "<th align=\"left\">Dependant Code</th>"
                        + "<th align=\"left\">Dependant Name</th>"
                        + "<th align=\"left\">Dependant Surname</th>"
                        + "<th align=\"left\">Birth Date</th>"
                        + "<th align=\"left\">LJP</th>"
                        + "<th align=\"left\"></th>"
                        + "</tr>");

                List<LookupValue> values = port.getCodeTable(205);
                System.out.println("Values size = " + values.size());
                LookupValue lookupValue = new neo.manager.LookupValue();
                int counter = 0;
                for (CoverDetails cd : cdList) {
                    //set cover details 
                    String dateOfBirth = "";
                    String penaltyJoin = "";
                    String ljp = null;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

                    //dob
                    if (cd.getDateOfBirth() != null) {
                        dateOfBirth = dateFormat.format(cd.getDateOfBirth().toGregorianCalendar().getTime());
                    }

                    if (cd.getPenaltyLateJoin().equals("0")) {
                        cd.setPenaltyLateJoin("None");
                    }
                    out.println("<tr>"
                            + "<td><label class=\"label\">" + cd.getDependentNumber() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getName() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getSurname() + "</label></td>"
                            + "<td><label class=\"label\">" + dateOfBirth + "</label></td>" //+ "<td><label class=\"label\">" + cd.getPenaltyLateJoin() + "</label></td>");

                            + "<td><select style=\"width:100px\" size=\"1\" name=\"depSelectLJP" + counter + "\" id=\"depSelectLJP" + counter + "\" value=\"" + cd.getPenaltyLateJoin() + "\" onChange=\"$('#button" + counter + "').removeAttr('disabled');\";>");
                    
                    for (LookupValue val : values) {
                        ljp = val.getValue();
                        int id = Integer.parseInt(val.getId());
                        if (cd.getPenaltyLateJoinId() == id) {
                            out.println("<option selected=\"selected\" value=\"" + val.getId() + "\" >" + val.getValue() + "</option>");
                        } else {
                            out.println("<option value=\"" + val.getId() + "\" >" + val.getValue() + "</option>");
                        }
                        
                        /*if (cd.getPenaltyLateJoin().equalsIgnoreCase(val.getValue())) {
                            out.println("<option selected=\"selected\" value=\"" + val.getId() + "\" >" + val.getValue() + "</option>");
                        } else {
                            out.println("<option value=\"" + val.getId() + "\" >" + val.getValue() + "</option>");
                        }*/
                    }

                    out.println("</select> </td>");

                    if (javaScript != null && !javaScript.equalsIgnoreCase("")) {

                        if (javaScript.equalsIgnoreCase("memberDepSelect")) {                            
                            out.println("<td><input type=\"button\" name=\"button" + counter + "\" id=\"button" + counter + "\" value=\"Update\" disabled onclick=\"document.getElementById('buttonPressedUnderwriting').value = 'Update';document.getElementById('depCoverId').value = '" + 
                                    cd.getCoverDetailsId() + "';var node = document.getElementById('depSelectLJP" + counter + "');document.getElementById('depLJP').value = node.options[node.selectedIndex].value; submitFormWithAjaxPost(this.form, 'MemberUnderwriting')\"></td>");
                        } else {
                            out.println("<td><button name=\"opperation\" type=\"button\" " + javaScript + " >Details</button></td>");
                        }
                    }
                    
                    out.println("</tr>");
                  counter++;  
                }
                out.println("</table>");

            }
        } catch (java.io.IOException ex) {
            throw new JspException("Error in CoverDependentPenaltyJoin tag", ex);
        }

        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setOnScreen(String onScreen) {
        this.onScreen = onScreen;
    }
}

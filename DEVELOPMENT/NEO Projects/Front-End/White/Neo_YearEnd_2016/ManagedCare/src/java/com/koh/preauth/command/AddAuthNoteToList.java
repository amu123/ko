/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthNoteDetail;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author Johan-NB
 */
public class AddAuthNoteToList extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        //noteType
        String notes = request.getParameter("notes");
        String noteType = request.getParameter("noteType");

        System.out.println("AddAuthNoteToList notes = "+notes);
        System.out.println("AddAuthNoteToList noteType = "+noteType);

        XMLGregorianCalendar xmlCreationDate = null;
        Date today = new Date(System.currentTimeMillis());

        if (notes != null && !notes.trim().equalsIgnoreCase("")) {
            System.out.println("note is not null");
            if (noteType != null && !noteType.trim().equalsIgnoreCase("99")) {
                System.out.println("noteType is not null or 99");
                NeoUser user = (NeoUser) session.getAttribute("persist_user");
                xmlCreationDate = DateTimeUtils.convertDateToXMLGregorianCalendar(today);

                //get list from session
                List<AuthNoteDetail> noteList = (List<AuthNoteDetail>) session.getAttribute("authNoteList");
                if (noteList == null) {
                    noteList = new ArrayList<AuthNoteDetail>();
                }
                AuthNoteDetail an = new AuthNoteDetail();
                //create note type lookup
                LookupValue nType = new LookupValue();
                String nTypeVal = port.getValueFromCodeTableForId("Auth Note Types",noteType);
                nType.setId(noteType);
                nType.setValue(nTypeVal);

                //generate user info for display
                String uName = user.getName();
                String uSurname = user.getSurname();
                String desc = user.getDescription();
                String createdBy = "";

                if(uName.trim().equalsIgnoreCase("") && uSurname.trim().equalsIgnoreCase("")){
                    createdBy = desc;
                }else{
                    createdBy = uName + " " + uSurname;
                }

                an.setCreationDate(xmlCreationDate);
                an.setCreatedBy(createdBy);

                an.setNotes(notes);
                an.setNoteType(nType);

                noteList.add(an);
                session.setAttribute("authNoteList", noteList);

            }
        }

        try {
            String nextJSP = "/PreAuth/AuthSpesificNotes.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "AddAuthNoteToList";


    }
}

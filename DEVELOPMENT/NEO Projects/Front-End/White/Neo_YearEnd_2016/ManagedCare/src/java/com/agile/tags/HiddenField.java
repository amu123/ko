/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class HiddenField extends TagSupport {
    private String elementName;
    private String hidden = "";

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
        HttpSession session = pageContext.getSession();

        String val = "";
        if (request.getAttribute(elementName) != null) {
            val = (String) request.getAttribute(elementName);
        } else if (session.getAttribute(elementName) != null) {
            val = (String) session.getAttribute(elementName);
        }

        //IF HIDDEN FIELD POPULATED
        if (!hidden.equalsIgnoreCase("")) {
             try {
                out.println("<input type='text' name='" + elementName + "' id='" + elementName + "' value='" + val + "'/>");
            } catch (java.io.IOException ex) {
                throw new JspException("Error in HiddenField tag", ex);
            }
            
        //DEFAULT BEHAVIOR
        } else {
         
            try {
                out.println("<input type=\"hidden\" name=\"" + elementName + "\" id=\"" + elementName + "\" value=\"" + val + "\"/>");
            } catch (java.io.IOException ex) {
                throw new JspException("Error in HiddenField tag", ex);
            }
           
        }
        
        
        return super.doEndTag();
    }

    public String getHidden() {
        return hidden;
    }

    public void setHidden(String hidden) {
        this.hidden = hidden;
    }
    
    

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CoverDetails;
import neo.manager.EventDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.WorkList;

/**
 *
 * @author bonganim
 */
public class CaptureManuallyRiskRateCommand extends NeoCommand{
  private Object _datatypeFactory;
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
      System.out.println("Inside the CaptureManuallyRiskRateCommand");
      Date date=new Date();
       XMLGregorianCalendar xml=convertDateXML(date);
       
       
        HttpSession session = request.getSession();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
      
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
       
        EventDetails event = new EventDetails();
        WorkList work=new WorkList();
        CoverDetails coverlist=(CoverDetails) session.getAttribute("CoverListObject");
          
        System.out.println("Before setting variables to worklist..");
     
        work.setAssignedBy(user.getUserId());
        work.setAssignedTo(244);
        work.setCreatedDate(xml);
        work.setAssignmentDate(xml);
        work.setCoverNumber(coverlist.getCoverNumber());
        work.setDependentNumber(coverlist.getDependentNumber());
        
        
        String riskFactor = request.getParameter("riskFactor");
        String riskReason = request.getParameter("riskReason");
        String riskRatingNotes = request.getParameter("riskRatingNotes");
      
       System.out.println("riskFactor " + riskFactor);
       System.out.println("riskReason " + riskReason);
       System.out.println("riskRatingNotes " + riskRatingNotes);
       System.out.println("coverlis " + coverlist.getCoverNumber());
        
    
        
        String desc = "Manual Risk Rating..";
        
        event.setEventDescription(desc);

        event.setEventRiskRating(Integer.parseInt(riskFactor));
        event.setEventReason(riskReason);
        event.setRiskRatingNotes(riskRatingNotes);

        event.setCoverNumber(coverlist.getCoverNumber());
        event.setDependentNumber(coverlist.getDependentNumber());

        System.out.println("risk.getCoverNumber() " + event.getCoverNumber());
        System.out.println("risk.getDependentNumber() " + event.getDependentNumber());

        event.setUserActioned(user.getUserId());
        event.setUser(4);
        event.setIndicator(10);

        LookupValue source = new LookupValue();
        source.setId(1+"");
        event.setEventSource(source);

        LookupValue type = new LookupValue();
        type.setId("4");
        event.setEventType(type);

        String msg = null;
        
        int  workListID= port.addWorklistItem(work);
        
        event.setWorkListID(workListID);
        int eventID = port.addEventDetails(event,0);
      
        
        if (eventID >= 1) {
               System.out.println("Manual patient risk rating was saved..");
               
                msg = "Manual patient risk rating was saved";
            } else {
                msg = "Save was unsuccessful";
            }

            PrintWriter out;
            try {
                out = response.getWriter();

                out.println("<html>");
                out.println("<head>");
                out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                out.println("</head>");
                out.println("<body>");
                out.println("<center>");
                out.println("<table cellspacing=\"4\">");
                out.println("<tr>");
                out.println("<td><label class=\"header\"><h2>" + msg + "</h2></label></td>");
                out.println("</tr>");
                out.println("</table>");
                out.println("<p> </p>");
                out.println("</center>");
                out.println("</body>");
                out.println("</html>");

                response.setHeader("Refresh", "3; URL=/ManagedCare/PDC/PolicyHolderDetails.jsp");
            } catch (IOException ex) {
                Logger.getLogger(SaveRiskRateCommand.class.getName()).log(Level.SEVERE, null, ex);
            }


        return null;
       
    }
    
public XMLGregorianCalendar convertDateXML(Date date) {

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getDatatypeFactory().newXMLGregorianCalendar(calendar);

    }

 private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
               Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }

        return (DatatypeFactory) _datatypeFactory;
    }
    @Override
    public String getName() {
      return"CaptureManuallyRiskRateCommand";
    }
    
}

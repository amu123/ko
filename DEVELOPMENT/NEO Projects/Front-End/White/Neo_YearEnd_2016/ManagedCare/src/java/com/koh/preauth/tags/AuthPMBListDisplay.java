/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthPMBDetails;

/**
 *
 * @author johanl
 */
public class AuthPMBListDisplay extends TagSupport {

    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();

        try {

            out.println("<td colspan=\"5\"><table width=\"700px\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr>"
                    + "<th align=\"left\">PMB Code</th>"
                    + "<th align=\"left\">Diagnosis</th>"
                    + "<th align=\"left\">Treatment</th>"
                    + "<th align=\"left\">Primary ICD</th>"
                    + "<th align=\"left\">Secondary ICD</th>"
                    + "<th align=\"left\">Co-Mobidity ICD</th>"
                    + "</tr>");
            
            ArrayList<AuthPMBDetails> pmbList = (ArrayList<AuthPMBDetails>) session.getAttribute("savedAuthPMBs");
            
//            if(pmbList == null || pmbList.isEmpty()){
//                pmbList = (ArrayList<AuthPMBDetails>) session.getAttribute("authPMBDetails");
//                session.setAttribute("savedAuthPMBs", pmbList);
////                String selectedValues = req.getParameter("selectedValues");
////                System.out.println("### selectedValues " + selectedValues);
////                if(session.getAttribute("pmbSelected") == null){
////                    if (selectedValues != null && !selectedValues.equalsIgnoreCase("")
////                            && !selectedValues.equalsIgnoreCase("null")) {
////                        session.setAttribute("pmbSelected", "true");
////                    }else{
////                        session.setAttribute("pmbSelected", "false");
////                    }
////                }
//            }

            if (pmbList != null && !pmbList.isEmpty()) {
                for (AuthPMBDetails pmb : pmbList) {

                    String pICD = pmb.getPrimaryICD();
                    String sICD = pmb.getSecondaryICD();
                    String cICD = pmb.getComorbidityICD();

                    if (pICD == null || pICD.equalsIgnoreCase("null")) {
                        pICD = "";
                    }
                    if (sICD == null || sICD.equalsIgnoreCase("null")) {
                        sICD = "";
                    }
                    if (cICD == null || cICD.equalsIgnoreCase("null")) {
                        cICD = "";
                    }

                    out.println("<tr>"
                            + "<td><label class=\"label\">" + pmb.getPmbCode() + "</label></td> "
                            + "<td width=\"300\"><label class=\"label\">" + pmb.getPmbDiagnosis() + "</label></td> "
                            + "<td width=\"300\"><label class=\"label\">" + pmb.getPmbTreatment() + "</label></td> "
                            + "<td><label class=\"label\">" + pICD + "</label></td> "
                            + "<td><label class=\"label\">" + sICD + "</label></td> "
                            + "<td><label class=\"label\">" + cICD + "</label></td> "
                            + "</tr>");
                }
            }
            out.println("</table></td>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in AuthPMBListTag tag", ex);
        }

        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

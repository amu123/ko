/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import agility.za.indexdocumenttype.IndexDocType;
import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.serv.PropertiesReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author johanl
 */
public class MemberCertificateGenerationCC extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        
        int entityId = Integer.parseInt(request.getParameter("entityId"));
        String memberNo = request.getParameter("memberNum_text");
        String process = request.getParameter("process");
        String docType = "membershipCertificate";

        if (process.equalsIgnoreCase("generate")) {
            generateMemberCertificate(entityId, docType, memberNo, request, response, context);
            
        } else if (process.equalsIgnoreCase("search")) {
            searchGeneratedDocs(memberNo, request, response, context);

        } else if (process.equalsIgnoreCase("view")) {
            viewGeneratedCertificate(request, response, context);
        }
        return null;
    }

    @Override
    public String getName() {
        return "MemberCertificateGenerationCC";
    }

    private void searchGeneratedDocs(String memberNumber, HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            request.setAttribute("folderList", new PropertiesReader().getProperty("DocumentIndexWorkFolder"));
            request.setAttribute("listOrWork", "Work");

            if (memberNumber != null && !memberNumber.trim().equals("")) {
                IndexDocumentRequest req = new IndexDocumentRequest();
                //IndexDocType indexDoc = new IndexDocType();
                //indexDoc.setDocType("MembershipCertificate");
                NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
                req.setAgent(loggedInUser);
                req.setIndexType("IndexForMemberByType");
                req.setIndexDoc(null);
                //req.setIndexType("Index");
                req.setEntityNumber(memberNumber);
                req.setSrcDrive(new PropertiesReader().getProperty("DocumentIndexIndexDrive"));
                req.setDocumentType("MembershipCertificate");
                IndexDocumentResponse resp = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
                if (resp.getIndexForMember() != null) {
                    logger.info("Index size = " + resp.getIndexForMember().size());
                }
                if (!resp.isIsSuccess()) {
                    logger.error("Error getting indexes .... " + resp.getMessage());
                    errorMessage(request, response, resp.getMessage());
                }
                request.setAttribute("indexList", resp.getIndexForMember());
            }
            request.setAttribute("memberNumber", memberNumber);

            RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/GenerateMemberCert.jsp");
            dispatcher.forward(request, response);
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void generateMemberCertificate(int entityId, String docType, String memberNumber, HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        int i = new Integer(entityId);
        String filePath = NeoCommand.service.getNeoManagerBeanPort().processDocumentGeneration(i, docType, false);

        if (filePath != null && !filePath.equals("")) {
            logger.info("CC Succesful Generation for entity - " + entityId);
            searchGeneratedDocs(memberNumber, request, response, context);

        } else {
            try {
                errorMessage(request, response, "Error Generating Member Certificate");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    private void viewGeneratedCertificate(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            String indexDrive = new PropertiesReader().getProperty("DocumentIndexIndexDrive");
            String fileLocation = request.getParameter("fileLocation");
            logger.info("CC viewGeneratedCertificate fileLocation = " + fileLocation);
            if (fileLocation == null || fileLocation.equals("")) {
                printErrorResult(request, response);
            }

            File file = new File(indexDrive + fileLocation);
            InputStream in = new FileInputStream(file);
            int contentLength = (int) file.length();
            ByteArrayOutputStream temporaryOutput;
            if (contentLength != -1) {
                temporaryOutput = new ByteArrayOutputStream(contentLength);
            } else {
                temporaryOutput = new ByteArrayOutputStream(20480);
            }

            byte[] bufer = new byte[512];
            while (true) {

                int len = in.read(bufer);
                if (len == -1) {

                    break;
                }
                temporaryOutput.write(bufer, 0, len);
            }
            in.close();
            temporaryOutput.close();

            byte[] array = temporaryOutput.toByteArray();
            printPreview(request, response, array, fileLocation);
        } catch (MalformedURLException ex) {

            ex.printStackTrace();
        } catch (IOException ioex) {
            // FileNotFound exception
            printErrorResult(request, response);
            logger.info(ioex.getMessage());
            System.out.println("The ioex.getMessage() " + ioex.getMessage());
            ioex.printStackTrace();
        }
    }

    public void printPreview(HttpServletRequest request, HttpServletResponse response, byte[] array, String fileName) throws IOException {
        OutputStream outStream = null;
        try {

            response.setHeader("Pragma", "no-cache"); //HTTP 1.0
            response.setDateHeader("Expires", 0); //prevents caching at the proxy server
            response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-disposition", "attachment; filename=" + fileName);

            response.setContentType("application/pdf");
            response.setContentLength(array.length);
            outStream = response.getOutputStream();
            outStream.write(array);
            outStream.flush();
            outStream.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void printErrorResult(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            out.println("<td><label class=\"header\">No records found.</label></td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "2; URL=/ManagedCare/Statement/GenerateMemberCert.jsp");

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }    

    private void errorMessage(HttpServletRequest request, HttpServletResponse response, String msg) throws Exception {
        request.setAttribute("errorMsg", msg);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/GenerateMemberCert.jsp");
        dispatcher.forward(request, response);
    }
}

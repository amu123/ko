/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.command;

import com.koh.command.Command;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthTariffDetails;
import neo.manager.NeoUser;

/**
 *
 * @author Johan-NB
 */
public class AddNewAuthNappiToSessionCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        String napDesc = "" + session.getAttribute("napDesc");
        String napCode = "" + request.getParameter("napCode_text");
        String provTypeId = "" + session.getAttribute("treatingProviderDiscTypeId");

        System.out.println("napCode = "+napCode);
        System.out.println("napDesc = "+napDesc);
        System.out.println("provType = "+provTypeId);

        ArrayList<AuthTariffDetails> atList = null;
        ArrayList<AuthTariffDetails> sessionList = (ArrayList<AuthTariffDetails>) session.getAttribute("newNappiListArray");
        if (sessionList != null) {
            atList = sessionList;
        } else {
            atList = new ArrayList<AuthTariffDetails>();
        }

        AuthTariffDetails at = new AuthTariffDetails();
        //set user
        at.setUserId(user.getUserId());
        at.setTariffType("3");

        boolean nullFlag = false;
        if (!napDesc.trim().equalsIgnoreCase("") && !napDesc.trim().equalsIgnoreCase("null")) {
            at.setTariffDesc(napDesc);
        } else {
            nullFlag = true;
        }
        if (!napCode.trim().equalsIgnoreCase("") && !napCode.trim().equalsIgnoreCase("null")) {
            at.setTariffCode(napCode);
        } else {
            nullFlag = true;
        }

        if(provTypeId.equalsIgnoreCase("null") || provTypeId.equalsIgnoreCase("")){
            provTypeId = "060";
        }

        if (nullFlag != true) {
            at.setProviderType(provTypeId);
            atList.add(at);
            //add array to session
            session.setAttribute("newNappiListArray", atList);
            //session.removeAttribute("provType");
            session.removeAttribute("napDesc");
            session.removeAttribute("napCode_text");

        }
        try {
            String nextJSP = "/PreAuth/NappiAuthSpecific.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "AddNewAuthNappiToSessionCommand";
    }
    
}

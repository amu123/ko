/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author josephm
 */
public class ResetTarrifComamnd extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        clearAllFromSession(request);

        try {

            String nextJSP = "/biometrics/AddGeneralTarrif.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        }
        catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ResetTarrifComamnd";
    }
}

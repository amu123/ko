/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;

import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import agility.za.indexdocumenttype.PreviewDocType;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import java.util.ArrayList;
import java.util.Collection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class DocumentIndexFileListCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        logger.info("------------In File list command---------------------");
        try {
            RequestDispatcher dispatcher = null;
            String[] selectedFiles = request.getParameterValues("selectedFile");
            String folderList = (String) request.getParameter("folderList");
            String listOrWork = (String) request.getParameter("listOrWork");
            String profileType = (!(((String) request.getParameter("profiletype")).equals("select")) &&(((String) request.getParameter("profiletype"))!=null))?((String) request.getParameter("profiletype")):"";
            String selctedProfileType = ((String)request.getParameter("retrievalType"))!=null?((String)request.getParameter("retrievalType")):"";
//            System.out.println("ProfileType : " +profileType);
//            System.out.println("retrievalType  :"+ selctedProfileType);
            request.getSession().removeAttribute("profile");
            if(selctedProfileType.equals("") && !profileType.equals("")){
            if (folderList == null) {
                folderList = new PropertiesReader().getProperty("DocumentIndexScanFolder");
            }
//            System.out.println("folderList === " + folderList);
            
            String scanDrive = new PropertiesReader().getProperty("DocumentIndexScanDrive");
            String destDrive = new PropertiesReader().getProperty("DocumentIndexWorkDrive");
            String workFolder = new PropertiesReader().getProperty("DocumentIndexWorkFolder");
//            System.out.println("scanDrive : "+scanDrive);
//            System.out.println("destDrive : "+destDrive);
//            System.out.println("workFolder :"+workFolder);
            logger.info("workFolder ==== " + workFolder);
//            System.out.println("listOrWork == " + listOrWork);
            Collection selectedFilesList = new ArrayList();
            if (selectedFiles != null && selectedFiles.length > 0) {
                for (String selectedFile : selectedFiles) {
                    selectedFilesList.add(selectedFile);
                    logger.info("Filename ==== " + selectedFile);
                }

                IndexDocumentRequest req = new IndexDocumentRequest();
                NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
//                String profileType = (String ) request.getSession().getAttribute("profiletype");
                req.setAgent(loggedInUser);
//                req.setIndexType("PreviewByProfile");
                req.setIndexType("Preview");
                req.setSrcDrive(scanDrive);
                PreviewDocType previewDocType = new PreviewDocType();
                previewDocType.getFilename().addAll(selectedFilesList);
                previewDocType.setSrcFolder(folderList+"/"+profileType);
                previewDocType.setDestDrive(destDrive);
                previewDocType.setDestFolder(workFolder+"/"+profileType);
                previewDocType.setDeleteSource(Boolean.TRUE);
//                previewDocType.setMemberProfile(profileType);
                request.setAttribute("folderList", workFolder);
                req.setPreview(previewDocType);
                System.out.println("Before Webservice");
                IndexDocumentResponse res = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
                
                if (res.isIsSuccess()) {
                    request.setAttribute("folderList", new PropertiesReader().getProperty("DocumentIndexWorkFolder"));
                    request.setAttribute("listOrWork", "Work");
                    request.getSession().setAttribute("profile", profileType);
                    if(profileType.toLowerCase().contains("claim")){
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentClaims.jsp");
                    }else if(profileType.toLowerCase().contains("member")){
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentMember.jsp");
                    }else{
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentGeneric.jsp");
                    }
                } else {
                    request.setAttribute("folderList", new PropertiesReader().getProperty("DocumentIndexScanFolder"));
                    request.setAttribute("listOrWork", "List");
                    request.setAttribute("errorMsg", res.getMessage());
                    dispatcher = request.getRequestDispatcher("/Indexing/IndexTifSelect.jsp");
                }
            } else {
                    request.setAttribute("folderList", new PropertiesReader().getProperty("DocumentIndexWorkFolder"));
                    request.setAttribute("listOrWork", "Work");
                    request.getSession().setAttribute("profile", profileType);
                    if(profileType.toLowerCase().contains("claim")){
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentClaims.jsp");
                    }else if(profileType.toLowerCase().contains("member")){
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentMember.jsp");
                    }else{
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentGeneric.jsp");
                    }
                    
                }
            }else{
                //Redirect it to the same page
                dispatcher = request.getRequestDispatcher("/Indexing/IndexTifSelect.jsp");
            }
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "DocumentIndexFileListCommand";
    }

    @Override
    public String getName() {
        return "DocumentIndexFileListCommand";
    }
}
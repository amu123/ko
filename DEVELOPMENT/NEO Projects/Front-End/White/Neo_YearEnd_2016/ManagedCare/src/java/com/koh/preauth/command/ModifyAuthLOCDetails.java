/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;


import com.koh.command.Command;
import com.koh.utils.FormatUtils;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthHospitalLOC;

/**
 *
 * @author johanl
 */
public class ModifyAuthLOCDetails extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;

        List<AuthHospitalLOC> locList = (List<AuthHospitalLOC>) session.getAttribute("AuthLocList");
        String ti = request.getParameter("locIndex");
        int index = Integer.parseInt(ti);

        try {
            out = response.getWriter();
            if (locList != null) {

                AuthHospitalLOC modifyLoc = locList.get(index);
                
                String dateFrom = FormatUtils.dateTimeFormat.format(modifyLoc.getDateFrom().toGregorianCalendar().getTime());
                String dateTo = FormatUtils.dateTimeFormat.format(modifyLoc.getDateTo().toGregorianCalendar().getTime());

                out.println("Done|"
                        + modifyLoc.getLevelOfCare() + "|"
                        //+loc.getLocDowngrade()+"|"
                        + dateFrom + "|"
                        + modifyLoc.getEstimatedCost() + "|"
                        + dateTo);
                //+"|"+loc.getSavingsAchieved());

                locList.remove(modifyLoc);
                
                if(locList.size() == 0){
                    session.removeAttribute("AuthLocList");
                }else{
                    session.setAttribute("AuthLocList", locList);
                }

                
            } else {
                out.println("Error|");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ModifyAuthLOCDetails";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.SecurityResponsibility;

/**
 *
 * @author josephm
 */
public class ValidateUserNameCommand extends NeoCommand {
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
    
        System.out.println("Inside ValidateUserNameCommand");
        
        String username = request.getParameter("username");
        
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        NeoUser user = port.validateUsername(username);
        
        List<SecurityResponsibility> respList = port.getUserResponsibilities(user.getUserId());

        try {
        
            PrintWriter out = response.getWriter();
            
            if(user != null && user.getUsername() != null) {
            
                out.println("Error|Username already exists|" + getName());

            }else {
            
                out.print(getName() + "|Username=" + user.getUsername() + "$");
            }

            if(respList.size() > 0)
                user.setDescription(respList.get(0).getDescription());

            
        }catch(Exception ex) {
            ex.printStackTrace();
        }
        
        return null;
    }
    
    @Override
    public String getName() {
    
        return "ValidateUserNameCommand";
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.pdc.command.CoverDependantResultCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author gerritr
 */
public class CoverDependendGridSelectionCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        session.removeAttribute("MemberCoverDetails");

        String depCode = request.getParameter("DepCode_text");
        String coverNumber = request.getParameter("memberNo_text");
        session.setAttribute("coverNumber", coverNumber);
        session.setAttribute("depNumber", depCode);
        String returnFlag = request.getParameter("returnFlag");
        session.setAttribute("returnFlag", returnFlag);

        NeoManagerBean port = service.getNeoManagerBeanPort();

        List<CoverDetails> covList = port.getDependantsForPrincipleMemberByDate(coverNumber);

        List<CoverDetails> newResult = new ArrayList<CoverDetails>();

        for (CoverDetails cd : covList) {
            if (cd.getDependentNumber() == Integer.parseInt(depCode)) {
                newResult.add(cd);
            }
        }
        session.setAttribute("MemberCoverDetails", newResult);

        //Load values for the BiometricTypeDropdownTag
        String bioType = "null";
        session.setAttribute("BioTypeSelected", bioType);

        if (depCode == null) { //Sothat it only loads this for redirecting to this page via another jsp
            try {
                String nextJSP = "/biometrics/BiometricsView.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }

        PrintWriter out = null;
        try {
            out = response.getWriter();

            if (covList != null) {
                out.print("Done|");
            } else {
                out.print("Failed|");
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(CoverDependantResultCommand.class
                    .getName()).log(Level.SEVERE, null, ex);
            System.out.print(
                    "Exception PRINTWRITER: " + ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "CoverDependendGridSelectionCommand";
    }
}

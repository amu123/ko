/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.BenefitLimitsDisplay;
import neo.manager.CoverClaimBenefitLimitStructure;
import neo.manager.NeoManagerBean;
import neo.manager.SavingsContribution;
import org.apache.log4j.Logger;

/**
 *
 * @author nick
 */
public class ViewPreAuthBenefitsCommand extends NeoCommand{
    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */

    private Logger log = Logger.getLogger(com.koh.preauth.command.ViewPreAuthBenefitsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        this.saveScreenToSession(request);
        log.info("Inside ViewPreAuthBenefitsCommand");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        String memberNumber = "" + session.getAttribute("memberNum_text");
        String dependantNumber = "" + session.getAttribute("depListValues");

        int depNumber = 99;
        if(dependantNumber != null && !dependantNumber.equalsIgnoreCase("null") && !dependantNumber.equalsIgnoreCase("")){
            depNumber = Integer.parseInt(dependantNumber);
        }

        String benDateStr = "" + session.getAttribute("authFromDate");

        log.info("The values sent to the method are coverNumber - " + memberNumber + " ; dependant id - " + depNumber + " ; auth date - " + benDateStr);


        Collection<CoverClaimBenefitLimitStructure> benLimStruct = new ArrayList<CoverClaimBenefitLimitStructure>();
        List<BenefitLimitsDisplay> benefitLimitsForDisplay = null;
        //convert date
        Date benDate = null;
        XMLGregorianCalendar xBenDate = null;
        try {
            benDate = new SimpleDateFormat("yyyy/MM/dd").parse(benDateStr);
            xBenDate = DateTimeUtils.convertDateToXMLGregorianCalendar(benDate);

        } catch (ParseException px) {
            px.printStackTrace();
        }

        if (xBenDate != null) {
//            benLimStruct = port.getBenefitLimitsForCover(memberNumber, depNumber, xBenDate);
            benefitLimitsForDisplay = port.getBenefitLimitsForDisplay(memberNumber, depNumber, xBenDate);
            System.out.println("Ben size :" + benefitLimitsForDisplay.size());
            //check for Saver Benefit
            boolean getSaverContribution = false;
            for (BenefitLimitsDisplay bens : benefitLimitsForDisplay) {
                if (bens.getBenefitType() == 2) {
                    getSaverContribution = true;
                    break;
                }
            }
            if (getSaverContribution) {
                SavingsContribution sc = port.getSaverBenefitContributionDetails(memberNumber, xBenDate);
                session.setAttribute("memberBenSavingsContribution", sc);
            }

            session.setAttribute("preAuthBenefitsList", benLimStruct);
            session.setAttribute("preAuthBenefitsList2", benefitLimitsForDisplay);
            session.setAttribute("authDate_error", null);
        } else {
            session.setAttribute("authDate_error", "Incorrect Date: " + benDateStr);
        }


        log.info("The size of the list is " + benLimStruct.size());
//        request.setAttribute("memberBenefitsList", benLimStruct);
        session.setAttribute("authBenDepCode", depNumber);
//        request.setAttribute("memberBenefitsList2", benefitLimitsForDisplay);

        try {
            String nextJSP = "/PreAuth/PreAuthBenefitAllocation.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "ViewPreAuthBenefitsCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.agile.security.webservice.AuthDetails;
import com.koh.command.FECommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author johanl
 */
public class AllocateAuthToSessionCommand extends FECommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
        session.setAttribute("AuthUpdate", "true");
        boolean orthoFlag = false;
        AuthDetails auths = new AuthDetails();
        AuthDetails ad = new AuthDetails();
        String authNo = request.getParameter("memAuthorisationNo");
        session.setAttribute("memAuthNo", authNo);
        auths = null;//port.getAllAuthByCode(authNo);

        if (auths.getAuthType() != null && !auths.getAuthType().trim().equalsIgnoreCase("")) {
            //set auth details
            System.out.println("setting full auth values");
            ad.setAuthType(auths.getAuthType());
            ad.setScheme(auths.getScheme());
            ad.setSchemeOption(auths.getSchemeOption());
            ad.setAuthorisationDate(auths.getAuthorisationDate());
            ad.setMemberNo(auths.getMemberNo());
            ad.setCallerName(auths.getCallerName());
            ad.setCallerRelationship(auths.getCallerRelationship());
            ad.setCallerReason(auths.getCallerReason());
            ad.setCallerContact(auths.getCallerContact());

            //set session values for auth caller page
            session.setAttribute("scheme", ad.getScheme());
            session.setAttribute("schemeOption", ad.getSchemeOption());
            session.setAttribute("authType", ad.getAuthType());
            session.setAttribute("authDate", ad.getAuthorisationDate());
            session.setAttribute("memNo_text", ad.getMemberNo());
            //set member number for search screen
            session.setAttribute("memberNumber", ad.getMemberNo());
            session.setAttribute("callerName", ad.getCallerName());
            session.setAttribute("callerRelationship", ad.getCallerRelationship());
            session.setAttribute("callerReason", ad.getCallerReason());
            session.setAttribute("callerContact", ad.getCallerContact());

            //set common auth details
            ad.setTreatingProviderNo(auths.getTreatingProviderNo());
            ad.setTreatingProvider(auths.getTreatingProvider());
            ad.setReferringProviderNo(auths.getReferringProviderNo());
            ad.setReferringProvider(auths.getReferringProvider());
            ad.setPrimaryIcd(auths.getPrimaryIcd());
            ad.setTarrifList(auths.getTarrifList());
            ad.setNotes(auths.getNotes());
            //set auth type specific details
            if (auths.getAuthType().trim().equalsIgnoreCase("2")) {
                System.out.println("Dental Auth Found in alloAuthToSession");
                session.setAttribute("AuthorisationType", "dAuth");
                ad.setDateValidFrom(auths.getDateValidFrom());
                ad.setDateValidTo(auths.getDateValidTo());
                ad.setEstimatedCost(auths.getEstimatedCost());
                ad.setAuthStatus(auths.getAuthStatus());

                if ((auths.getPlannedDuration() != null) && (!auths.getPlannedDuration().trim().equalsIgnoreCase("null"))) {
                    session.setAttribute("DentalAuthType", "ortho");
                    orthoFlag = true;
                    //set orthodontic specific details
                    ad.setPlannedDuration(auths.getPlannedDuration());
                    ad.setDepositAmount(auths.getDepositAmount());
                    ad.setFirstInstallmentAmount(auths.getFirstInstallmentAmount());
                    ad.setRemainingInstallmentAmount(auths.getRemainingInstallmentAmount());

                } else {
                    //set normal dental details
                    session.setAttribute("DentalAuthType", "normal");
                    orthoFlag = false;
                    ad.setLabProviderNo(auths.getLabProviderNo());
                    ad.setLabProvider(auths.getLabProvider());
                    ad.setLabCodeList(auths.getLabCodeList());
                    ad.setQuantity(auths.getQuantity());
                    ad.setToothNumber(auths.getToothNumber());

                    //set hospital details if indicated
                    String hospitalInd = auths.getHospitalIndicator();
                    if (!hospitalInd.trim().equalsIgnoreCase("NULL") && (!hospitalInd.trim().equals("0"))) {
                        session.setAttribute("DentalHospitalInd", "true");
                        ad.setHospitalProviderNo(auths.getHospitalProviderNo());
                        ad.setHospitalProvider(auths.getHospitalProvider());
                        ad.setAnaesthetistCode(auths.getAnaesthetistCode());
                        ad.setAnaesthetistName(auths.getAnaesthetistName());
                        ad.setSecondaryIcd(auths.getSecondaryIcd());
                        ad.setTertiaryIcd(auths.getTertiaryIcd());
                        ad.setPMB(auths.getPMB());
                        ad.setCoPayment(auths.getCoPayment());
                        ad.setTheatreTime(auths.getTheatreTime());
                        ad.setFunder(auths.getFunder());


                    } else {
                        session.setAttribute("DentalHospitalInd", "false");
                    }
                }
                System.out.println("ORTHOdontic FLAG = "+orthoFlag);
                if(orthoFlag == true){
                    session.setAttribute("dental", "3");

                }else{
                    session.setAttribute("dental", "1");
                }
                System.out.println("Value set for dental type radion = "+session.getAttribute("dental"));

            } else {
                System.out.println("Optical Auth Found in alloAuthToSession");
                session.setAttribute("AuthorisationType", "oAuth");

            }
            //request.setAttribute("FullAuthSearchResult", ad);
            session.setAttribute("FullAuthSearchResult", ad);
        }
        try {
            String nextJSP = "/Auth/AuthCaller.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "AllocateAuthToSessionCommand";
    }
}

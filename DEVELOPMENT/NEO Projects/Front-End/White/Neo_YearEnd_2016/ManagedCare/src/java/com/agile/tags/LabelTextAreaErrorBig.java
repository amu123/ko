/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author josephm
 */
public class LabelTextAreaErrorBig extends TagSupport {
    private String displayName;
    private String elementName;
    private String mandatory = "no";
    private String valueFromSession = "no";

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            String sessionValerror = "";
            if (valueFromSession.equalsIgnoreCase("Yes")) {
                String sessionVal = "";

                if(session.getAttribute(elementName) != null) {
                    sessionVal = "" + session.getAttribute(elementName);
                }
                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                }
                out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\"><textarea style=\"font-family:arial;\" id=\""+elementName+"\" name=\""+elementName+"\" rows=\"20\" cols=\"50\">" + sessionVal + "</textarea></td>");
            }
            else
            {
                out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\"><textarea style=\"font-family:arial;\" id=\""+elementName+"\" name=\""+elementName+"\" rows=\"20\" cols=\"50\"></textarea></td>");
            }
            if(mandatory.equalsIgnoreCase("yes")){
                out.print("<td><label class=\"red\">*</label></td>");
            }
            else {
                out.print("<td></td>");
            }

            if (valueFromSession.equalsIgnoreCase("Yes") && sessionValerror != null && !sessionValerror.equalsIgnoreCase("null")) {
                out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
            } else {
                out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
            }

            out.println("</td>");
            
        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelTextAreaError tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setValueFromSession(String valueFromSession) {
        this.valueFromSession = valueFromSession;
    }
}

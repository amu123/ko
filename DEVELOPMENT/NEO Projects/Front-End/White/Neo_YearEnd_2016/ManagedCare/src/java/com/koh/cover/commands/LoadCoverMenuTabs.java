/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author johanl
 */
public class LoadCoverMenuTabs extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        
        String onScreen = request.getParameter("onScreen");
        
        session.setAttribute("memberCoverNumber", request.getParameter("coverNum"));
        session.setAttribute("memberNumber", request.getParameter("coverNum"));
        session.setAttribute("memberCoverDepType", Integer.parseInt(request.getParameter("depTypeId")));
        session.setAttribute("memberCoverDepNum", Integer.parseInt(request.getParameter("depNumber")));
        session.setAttribute("memberCoverEntityId", Integer.parseInt(request.getParameter("entityId")));
        session.setAttribute("memberCoverOptionId", Integer.parseInt(request.getParameter("optionId")));
        session.setAttribute("memberEntityCommon", Integer.parseInt(request.getParameter("entityCommon")));
        
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(onScreen);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "LoadCoverMenuTabs";
    }
}

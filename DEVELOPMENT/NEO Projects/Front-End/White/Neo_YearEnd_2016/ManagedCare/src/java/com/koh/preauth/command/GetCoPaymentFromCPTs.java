/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthCPTDetails;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.SecurityResponsibility;

/**
 *
 * @author Johan-NB
 */
public class GetCoPaymentFromCPTs extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter out = null;
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        double copay = 0.0d;

        //copayment override validation
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        int userRespId = 0;
        for (SecurityResponsibility security : user.getSecurityResponsibility()) {
            userRespId = security.getResponsibilityId();
        }

        String admissionD = "" + session.getAttribute("admissionDateTime");
        Date admissionDate = null;
        XMLGregorianCalendar authStart = null;
        try {
            admissionDate = new SimpleDateFormat("yyyy/MM/dd").parse(admissionD);
            authStart = DateTimeUtils.convertDateToXMLGregorianCalendar(admissionDate);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        try {
            out = response.getWriter();
            List<AuthCPTDetails> cptList = (List<AuthCPTDetails>) session.getAttribute("authCPTListDetails");
            //set copay readonly remove
            String allowRORemove = "";
            if (userRespId == 2 || userRespId == 3) {
                allowRORemove = "yes";
            } else {
                allowRORemove = "no";
            }

            if (cptList != null && cptList.isEmpty() == false && authStart != null) {
                //get auth member option id
                int optionId = Integer.parseInt("" + session.getAttribute("schemeOption"));
                System.out.println("GetCoPaymentFromCPTs member option id for copay mapping = " + optionId);

                copay = service.getNeoManagerBeanPort().getCoPaymentAmountForCPT(cptList, optionId, authStart);

                String errorMsg = "";
                System.out.println("co payment amount = " + copay);
                session.setAttribute("coPay_error", null);
                if (copay == 99999.0d) {
                    errorMsg = "*Note: Procedure Excluded Unless PMB";
                    session.setAttribute("coPay_error",
                            "*Note: Procedure Excluded Unless PMB");
                }


                double dspCopay = 3675.0d;

                String product = "" + session.getAttribute("scheme");
                String option = "" + session.getAttribute("schemeOption");
                int prod = Integer.parseInt(product);
                int opt = Integer.parseInt(option);
                boolean cptEmergency = false;
                if (session.getAttribute("cptEmergencyCase") != null) {
                    cptEmergency = (Boolean) session.getAttribute("cptEmergencyCase");
                }

                if ((option.equals("5") || option.equals("7")) && !cptEmergency) {

                    //check provider 
                    String manFac = "" + session.getAttribute("facilityProv_text");
                    String optFac = "" + session.getAttribute("optFacilityProv_text");
                    String facilityProv = "";
                    boolean noProvSet = false;
                    boolean isDSP = false;

                    if (manFac != null && !manFac.equals("null") && !manFac.equalsIgnoreCase("")) {
                        facilityProv = manFac;
                    } else if (optFac != null && !optFac.equals("null") && !optFac.equalsIgnoreCase("")) {
                        facilityProv = optFac;
                    } else {
                        noProvSet = true;
                    }
                    System.out.println("facilityProv = " + facilityProv);
                    System.out.println("noProvSet = " + noProvSet);
                    
                    String facilityProvNetwork = "" + session.getAttribute("facilityProvNetwork");
                    boolean provincialHosp = false;
                    if(facilityProvNetwork.equalsIgnoreCase("Provincial Hospitals L1") 
                            || facilityProvNetwork.equalsIgnoreCase("Provincial Hospitals L2") 
                            || facilityProvNetwork.equalsIgnoreCase("Provincial Hospitals L3")){
                        provincialHosp = true;
                    }
                    
                    if (!noProvSet && !provincialHosp) {
                        isDSP = port.isDSPNetwork(facilityProv, authStart, prod, opt);

                        if (copay != 0.0d && copay != 99999.0d && !isDSP) {
                            errorMsg = "*Note: Normal Copay of R" + DateTimeUtils.decimalFormat.format(copay) + " applies with an addtional R3000 for Non-DSP";
                            session.setAttribute("coPay_error",
                                    "*Note: Normal Copay of R" + DateTimeUtils.decimalFormat.format(copay) + " "
                                    + "applies with an addtional R3675 for Non-DSP");
                            copay += dspCopay;

                        } else if (copay == 0.0d && !isDSP) {
                            errorMsg = "*Note: An addtional R3675 for Non-DSP Applies on Facility " + facilityProv;
                            session.setAttribute("coPay_error",
                                    "*Note: An addtional R3675 for Non-DSP Applies on Facility " + facilityProv);
                            copay += dspCopay;

                        } else if (copay == 99999.0d && !isDSP) {
                            errorMsg = "*Note: Procedure Excluded Unless PMB R" + DateTimeUtils.decimalFormat.format(copay) + " applies with an addtional R3000 for Non-DSP";
                            session.setAttribute("coPay_error",
                                    "*Note: Procedure Excluded Unless PMB R" + DateTimeUtils.decimalFormat.format(copay) + " "
                                    + "applies with an addtional R3675 for Non-DSP");
                            copay += dspCopay;
                        }
                        if (!isDSP) {
                            session.setAttribute("dspCopayApplies", "yes");
                        }
                    } else {
                        session.setAttribute("dspCopayApplies", "no");
                    }
                } else {
                    if (copay == 99999.0d) {
                        errorMsg = "*Note: Procedure Excluded Unless PMB R" + DateTimeUtils.decimalFormat.format(copay) + " ";
                        session.setAttribute("coPay_error",
                                "*Note: Procedure Excluded Unless PMB R" + DateTimeUtils.decimalFormat.format(copay) + " ");
                    }
                    session.setAttribute("dspCopayApplies", "no");
                }

                System.out.println("co payment before return amount = " + copay);

                out.println("Done|" + allowRORemove + "|" + copay + "|" + errorMsg);

            } else {

                out.println("Error|" + allowRORemove);
            }

        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            out.close();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetCoPaymentFromCPTs";
    }
}

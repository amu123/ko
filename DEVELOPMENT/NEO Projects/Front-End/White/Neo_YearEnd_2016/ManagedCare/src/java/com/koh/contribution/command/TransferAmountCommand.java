/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.contribution.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.KeyObjectArray;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author yuganp
 */
public class TransferAmountCommand extends NeoCommand{
    
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMdd");

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            String s = request.getParameter("command");
            System.out.println(this.getName() + " : " + s);
            if (s != null && !s.isEmpty()) {
                System.out.println("command : " + s);
                if (s.equalsIgnoreCase("Search")) {
                    search(request, response, context);
                } else if (s.equalsIgnoreCase("Select")) {
                    select(request, response, context);
                } else if (s.equalsIgnoreCase("Save")) {
                    save(request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(ContributionBankStatementCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void save(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Map pMap = request.getParameterMap();
        String fromPrefix = "contribFrom_";
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        
        for (Object s : pMap.keySet()) {
            String key = s.toString();
            if (key.startsWith(fromPrefix)) {
                String idString = key.substring(12);
                String fromValue = request.getParameter(key);
                String fromAmount = request.getParameter("contribAmt_" + idString);
                if (!fromAmount.isEmpty()) {
                    String[] fromArr = idString.split("_");
                    String[] toArr = fromValue.split("_");
                    XMLGregorianCalendar fromDate = getDate(fromArr[0]);
                    Integer fromEntity = getInt(fromArr[1]);
                    Integer fromEntityType = getInt(fromArr[2]);
                    Integer fromOption = getInt(fromArr[3]);
                    XMLGregorianCalendar toDate = getDate(toArr[0]);
                    Integer toEntity = getInt(toArr[1]);
                    Integer toEntityType = getInt(toArr[2]);
                    Integer toOption = getInt(toArr[3]);
                    BigDecimal amt = getBD(fromAmount);
                    Map<String, Object> hm = new LinkedHashMap<String, Object>();
                    hm.put("fromDate", fromDate);
                    hm.put("fromEntity", fromEntity);
                    hm.put("fromEntityType", fromEntityType);
                    hm.put("fromOption", fromOption);
                    hm.put("toDate", toDate);
                    hm.put("toEntity", toEntity);
                    hm.put("toEntityType", toEntityType);
                    hm.put("toOption", toOption);
                    hm.put("amt", amt);
                    hm.put("coverNo", request.getParameter("coverNo"));
                    list.add(hm);
                }
            }
        }
        boolean resultStatus = false;
        if (list.size() > 0) {
            resultStatus = port.saveTransferAmount(MapUtils.getKeyObjectArray(list), TabUtils.getSecurity(request));
        }
        Map<String, String> fieldsMap = new HashMap<String, String>();
        if (resultStatus) {
            fieldsMap.put("transferStatus", "ok");
        } else {
            fieldsMap.put("transferStatus", "error");
        }
        String updateFields = TabUtils.convertMapToJSON(fieldsMap);
        String result = TabUtils.buildJsonResult(resultStatus ? "OK" : "ERROR", null, updateFields, null);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println(result);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (out != null) out.close();
        }
    }
    
    private void search(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Map<String, String> crit = new HashMap<String,String>();
        Date dt = TabUtils.getDateParam(request, "dob");
        String dob = null;
        if (dt != null) {
            dob = TabUtils.getStringFromDate(dt);
        }
        crit.put("coverNo", request.getParameter("memNo"));
        crit.put("idNumber", request.getParameter("idNo"));
        crit.put("initials", request.getParameter("initials"));
        crit.put("surname", request.getParameter("surname"));
        crit.put("dob", dob);
        for (String s : crit.keySet()) {
            System.out.println(s + " = " + crit.get(s));
        }
        List<KeyValueArray> list = port.findMemberByMapSearchCriteria(MapUtils.getKeyValueArray(crit));
        List<Map<String, String>> map = MapUtils.getMap(list);
        request.setAttribute("MemberSearchResults", map);
        context.getRequestDispatcher("/Contribution/TransferAmountResults.jsp").forward(request, response);
    }
    
    private void select(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        List<KeyValueArray> list = port.fetchContributionPeriodBalancePerEntity(request.getParameter("coverNo"));
        List<Map<String, String>> map = MapUtils.getMap(list);
        request.setAttribute("MemberContribResults", map);
        request.setAttribute("coverNo", request.getParameter("coverNo"));
        request.setAttribute("memberName", request.getParameter("memberName"));
        request.setAttribute("memberSurname", request.getParameter("memberSurname"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        request.setAttribute("target_div", request.getParameter("target_div"));
        
        context.getRequestDispatcher("/Contribution/TransferAmountDetails.jsp").forward(request, response);
    }
    
    private XMLGregorianCalendar getDate(String date) {
        try {
            return DateTimeUtils.convertDateToXMLGregorianCalendar(SDF.parse(date));
        } catch (Exception e) {
            return null;
        }
    }
    
    private Integer getInt(String val) {
        try {
            return new Integer(val);
        } catch (Exception e) {
            return null;
        }
    }
    
    private BigDecimal getBD(String val) {
        try {
            return new BigDecimal(val);
        } catch (Exception e) {
            return null;
        }
    }
    
    
    @Override
    public String getName() {
        return "TransferAmountCommand";
    }
    
}

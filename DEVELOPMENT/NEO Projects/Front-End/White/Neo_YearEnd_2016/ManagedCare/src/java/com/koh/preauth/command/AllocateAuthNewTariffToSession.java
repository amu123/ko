/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Johan-NB
 */
public class AllocateAuthNewTariffToSession extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        String code = request.getParameter("tc");
        String desc = request.getParameter("td");
        String quan = request.getParameter("tq");
        String amount = request.getParameter("ta");
        String provDisc = request.getParameter("pd");

        session.setAttribute("clinicOriginalTA", amount);
        session.setAttribute("clinicOriginalTQ", quan);

        session.setAttribute("provType", provDisc);
        session.setAttribute("tariffDesc", desc);
        session.setAttribute("tariffCode_text", code);
        session.setAttribute("tariffQuantity", quan);
        session.setAttribute("tariffAmount", amount);

        String noTariff = "" + session.getAttribute("noTariffFound");
        System.out.println("allocate noTariff = " + noTariff);
        if (!noTariff.equalsIgnoreCase("null") || !noTariff.equalsIgnoreCase("")) {
            session.removeAttribute("noTariffFound");
        }

        try {
            String nextJSP = "/PreAuth/AuthSpecificTariff.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "AllocateAuthNewTariffToSession";
    }
}

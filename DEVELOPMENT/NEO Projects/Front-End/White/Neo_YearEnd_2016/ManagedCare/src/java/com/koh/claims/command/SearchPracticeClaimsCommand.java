/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.auth.dto.MemberSearchResult;
import com.koh.claims.dto.MemberClaimSearchBean;
import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.lang.Exception;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author josephm
 */
public class SearchPracticeClaimsCommand extends NeoCommand {

    private static final Logger logger = Logger.getLogger(SearchPracticeClaimsCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        logger.info("Inside SearchPracticeClaimsCommand");

//        HttpSession session = request.getSession();
//        NeoManagerBean port = service.getNeoManagerBeanPort();
//        ClaimSearchCriteria claimSearch = new ClaimSearchCriteria();
//        String coverNumber = request.getParameter("memberNum_text");
//       // String coverNumber = ""+session.getAttribute("policyHolderNumber");
//        claimSearch.setCoverNumber(coverNumber);
//       // claimSearch.setServiceDateFrom(arg0);
//        //String practiceNumber = request.getParameter("practiceNumber_text");
//       // claimSearch.setPracticeNumber(practiceNumber);
//       // logger.info("The practice number is " + practiceNumber);
//        logger.info("The cover number number is " + session.getAttribute("policyHolderNumber"));
//        Collection<ClaimsBatch> batchList = port.findClaimBySearchCriteria(claimSearch);
//        logger.info("The size of the batch list from the SearchPracticeClaimsCommand is " + batchList.size());
//        request.setAttribute("batchListResults", batchList);
//        session.setAttribute("batchListResults", batchList);
        /*        try {
         String nextJSP = "/Claims/PracticeClaimsDetails.jsp";
         RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
         dispatcher.forward(request, response);
         }catch(Exception ex) {
         ex.printStackTrace();
         }*/
        this.saveScreenToSession(request);

        HttpSession session = request.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        boolean doSixMonths = false;
        boolean claimIdFound = false;
        String claimSearchResultMsg = "";

        PrintWriter out = null;
        //neo.manager.CcClaimSearchResult batch = new neo.manager.CcClaimSearchResult();
        ClaimSearchCriteria search = new ClaimSearchCriteria();
        //set default search values
        search.setDependentCode(-1);

        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        neo.manager.Security _secure = new neo.manager.Security();
        _secure.setCreatedBy(user.getUserId());
        _secure.setLastUpdatedBy(user.getUserId());
        _secure.setSecurityGroupId(2);


        int currentMin = 0;
        int currentMax = 100;
        
        String onScreen = "";
        int benefitId = 0;
        int dependantCode = -1;

        //System.out.println("entered SearchPracticeClaimsCommand");
        String practiceNumber = "" + session.getAttribute("practiceNumber");

        //TODO set cleariing indicator on claim level
        //search.setClearingClaim

        System.out.println("SearchPracticeClaimsCommand practice no = " + practiceNumber);
        if (practiceNumber != null && !practiceNumber.equalsIgnoreCase("")) {
            //clearing claim validation

            boolean isClearingUser = (Boolean) session.getAttribute("persist_user_ccClearing");
            boolean isClearProviderClaim = (Boolean) session.getAttribute("persist_isClearingClaims");

            if (!isClearingUser) {
                search.setPracticeNumber(practiceNumber);

            } else if (isClearingUser && isClearProviderClaim) {
                search.setPracticeNumber("9999997");
                search.setProviderNumber(practiceNumber);


            } else if (isClearingUser && !isClearProviderClaim) {
                search.setPracticeNumber(practiceNumber);
                search.setProviderNumber(practiceNumber);
            }

        }

        //set cover number        
        String coverNumber = (String) session.getAttribute("memberNum_text");
        if (coverNumber != null && !coverNumber.equals("")) {
            //System.out.println("coverNumber " + coverNumber);
            search.setCoverNumber(coverNumber);
        }

        //cover dependant 
        String covDep = "" + session.getAttribute("depListValues");
        if (covDep != null && !covDep.equals("") && !covDep.equals("99")) {
            //System.out.println("cover dependant " + covDep);
            search.setDependentCode(Integer.parseInt(covDep));
        }

        //claim id
        String memClaimSearchClaimId = "" + session.getAttribute("pcsClaimId");
        if (memClaimSearchClaimId != null && !memClaimSearchClaimId.equals("")) {
            search.setClaimId(Integer.parseInt(memClaimSearchClaimId));
            claimIdFound = true;
        }
        if (request.getParameter("claimStatus") != null && !request.getParameter("claimStatus").equalsIgnoreCase("") && !request.getParameter("claimStatus").equalsIgnoreCase("99")) {
                int claimStatus = Integer.parseInt(request.getParameter("claimStatus"));
                List<LookupValue> claimStatusLV = port.getCodeTable(83); //claimStatus = 83
                if (!claimStatusLV.isEmpty()) {
                    LookupValue claimStatusLookup = claimStatusLV.get(claimStatus-1);
                    search.setClaimStatus(claimStatusLookup);
                }
            }

            String tariffCode = request.getParameter("tariffCode");
            if (tariffCode != null && !tariffCode.equals("") && !tariffCode.equals(" ")) {
                search.setTariffCodeNo(tariffCode);
            }

            if (request.getParameter("toothNum") != null && !request.getParameter("toothNum").equalsIgnoreCase("") && !request.getParameter("toothNum").equalsIgnoreCase("0")) {
                int toothNumber = Integer.parseInt(request.getParameter("toothNum"));
                search.setToothnumber(toothNumber);
            }

            if (request.getParameter("batchNum") != null && !request.getParameter("batchNum").equalsIgnoreCase("") && !request.getParameter("batchNum").equalsIgnoreCase("0")) {
                int batchNumber = Integer.parseInt(request.getParameter("batchNum"));
                search.setBatchId(batchNumber);
            }


        String treatFrom = "";
        String treatTo = "";

        treatFrom = "" + session.getAttribute("treatmentFrom");
        treatTo = "" + session.getAttribute("treatmentTo");

        if ((treatFrom != null && !treatFrom.equalsIgnoreCase(""))
                && (treatTo != null && !treatTo.equalsIgnoreCase(""))) {

            Date tFrom = null;
            Date tTo = null;
            XMLGregorianCalendar xTreatFrom = null;
            XMLGregorianCalendar xTreatTo = null;

            try {
                tFrom = dateFormat.parse(treatFrom);
                tTo = dateFormat.parse(treatTo);

                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                xTreatTo = DateTimeUtils.convertDateToXMLGregorianCalendar(tTo);

                search.setServiceDateFrom(xTreatFrom);
                search.setServiceDateTo(xTreatTo);

            } catch (java.text.ParseException ex) {
                logger.log(Level.SEVERE, null, ex);
            }

        } else {
            if (!claimIdFound) {
                doSixMonths = true;
                claimSearchResultMsg = "Please note only the past 6 months claims data was retrieved.";
            }
        }

        if (doSixMonths) {
            //set service dates
            Calendar c1 = Calendar.getInstance();
            c1.add(Calendar.MONTH, -6); // substract 6 month

            Calendar calendar = Calendar.getInstance();

            try {
                String today = dateFormat.format(calendar.getTime());
                Date now = dateFormat.parse(today);
                XMLGregorianCalendar xmlNow = DateTimeUtils.convertDateToXMLGregorianCalendar(now);
                calendar.add(Calendar.MONTH, -6);
                String sixLater = dateFormat.format(calendar.getTime());
                Date sixMonths = dateFormat.parse(sixLater);
                XMLGregorianCalendar xmlSixMonthsLater = DateTimeUtils.convertDateToXMLGregorianCalendar(sixMonths);
                search.setServiceDateFrom(xmlSixMonthsLater);
                search.setServiceDateTo(xmlNow);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        CcClaimSearchResult searchResult = port.findDetailedClaimBySearchCriteria(search, 0, 100, _secure);

        if (searchResult == null && searchResult.getBatchs().isEmpty() == true) {
            claimSearchResultMsg += " No results found.";

        } else if (searchResult.getResultCounter() == 100) {
            claimSearchResultMsg += " Please Redefine Claim Search.";

        } else {
            List<ClaimsBatch> claimList = searchResult.getBatchs();
            //Build practice bean for each row and add in an arraylist
            List<MemberClaimSearchBean> meberClaimSearchBeanList = new ArrayList<MemberClaimSearchBean>();
            int pdcCdlStatus;
            boolean isFlaggedForCdl;
            for (ClaimsBatch claim : claimList) {
                String claimType = "";
                if (claim.getEdiId() == null || claim.getEdiId().equalsIgnoreCase("")) {
                    claimType = "Paper";
                } else {
                    claimType = "EDI";
                }
                for (Claim list_claim : claim.getClaims()) {
                    pdcCdlStatus = 0;
                    isFlaggedForCdl = false;
                    MemberClaimSearchBean bean = new MemberClaimSearchBean();
                    ProviderDetails prac = port.getPracticeDetailsForEntityByNumberAndDate(list_claim.getPracticeNo(), list_claim.getServiceDateFrom());
                    String catagory = port.getValueForId(59, list_claim.getClaimCategoryTypeId() + "");
                    ProviderDetails practice = port.getPracticeDetailsForEntityByNumber(list_claim.getPracticeNo());
                    ProviderDetails practice2 = port.getPracticeDetailsForEntityByNumber(list_claim.getServiceProviderNo());
                    bean.setClaimType(claimType);
                    bean.setClaimNumber("" + list_claim.getClaimId());
                    bean.setAccountNumber(list_claim.getPatientRefNo());
                    bean.setPracticeNumber(list_claim.getPracticeNo());
                    bean.setPracticeName(practice.getPracticeName());
                    bean.setServiceProvider(list_claim.getServiceProviderNo());
                    bean.setServiceProviderName(practice2.getPracticeName());
                    bean.setDisciplineType(catagory);
                    bean.setMemberNumber(list_claim.getCoverNumber());
                    bean.setTreatmentFrom(dateFormat.format(list_claim.getServiceDateFrom().toGregorianCalendar().getTime()));
                    bean.setTreatmentTo(dateFormat.format(list_claim.getServiceDateTo().toGregorianCalendar().getTime()));
                    String totalAmountClaimed = "";
                    String totalAmountPaid = "";
                    if (list_claim.getClaimCalculatedTotal() != 0.0d) {
                        totalAmountClaimed = new DecimalFormat("#####0.00").format(list_claim.getClaimCalculatedTotal());
                    } else {
                        totalAmountClaimed = "0.00";
                    }

                    if (list_claim.getClaimCalculatedPaidTotal() != 0.0d) {
                        totalAmountPaid = new DecimalFormat("#####0.00").format(list_claim.getClaimCalculatedPaidTotal());
                    } else {
                        totalAmountPaid = "0.00";
                    }
                    bean.setTotalClaimed(totalAmountClaimed);
                    bean.setTotalPaid(totalAmountPaid);
                    String recipient = port.getValueFromCodeTableForTableId(15, list_claim.getRecipient());
                    bean.setRecipient(recipient);
                    bean.setClaimID("" + list_claim.getClaimId());
                    bean.setServiceProviderNo(list_claim.getServiceProviderNo());
                    CoverDetails cover = port.getPrincipalMemberDetailsByDate(list_claim.getCoverNumber(), list_claim.getServiceDateFrom());
                    PersonDetails member = port.getPersonDetailsByEntityId(cover.getEntityId());
                    bean.setMemberName(member.getName() + " " + member.getSurname());
                    List<CoverDetails> memberCoverDetailsLst = port.getCoverDetailsByCoverNumberByDate(list_claim.getCoverNumber(), list_claim.getServiceDateFrom());
                    for (CoverDetails coverDetail : memberCoverDetailsLst) {
                        if (port.getMemberCDLStatusDetails(coverDetail.getEntityId(), 1).getRegisteredInd() == 1) {
                            for (SecurityResponsibility userRole : user.getSecurityResponsibility()) {
                                if (userRole.getResponsibilityId() != 2) {
                                    isFlaggedForCdl = true;
                                }
                            }
                        }
                    }
                    if (isFlaggedForCdl) {
                        continue;
                    }
                    meberClaimSearchBeanList.add(bean);
                }
            }
            //validate user for CDL view restriction
//            String memberNumber;
//            List<EAuthCoverDetails> coverList = new ArrayList<EAuthCoverDetails>();
//            CoverSearchCriteria csc = new CoverSearchCriteria();
//            System.out.println("Entering user cdl validation...");
//            if (!meberClaimSearchBeanList.isEmpty() && meberClaimSearchBeanList != null) {
//                System.out.println("meberClaimSearchBeanList not empty...");
//                memberNumber = meberClaimSearchBeanList.get(0).getMemberNumber();
//                csc.setCoverNumberExact(memberNumber);
//                coverList = port.eAuthCoverSearch(csc);
//                if (coverList != null && !coverList.isEmpty()) {
//                    System.out.println("coverList not empty...");
//                    for (neo.manager.SecurityResponsibility resp : user.getSecurityResponsibility()) {
//                        System.out.println("userId:" + resp.getResponsibilityId());
//                        if (resp.getResponsibilityId() != 2) {
//                            for (EAuthCoverDetails eAuth : coverList) {
//                                if (eAuth.getRegisteredInd() == 1) {
//                                    System.out.println("user is restricted to view member claims...");
//                                    session.setAttribute("userRestriction", true);
//                                    meberClaimSearchBeanList.clear();
//                                    claimSearchResultMsg = "Cannot view member claims, please contact administrator.";
//                                }
//                            }
//                        }
//                    }
//                }
//            }
            //add list to seesion
            session.removeAttribute("meberClaimSearchBeanList");
            session.setAttribute("meberClaimSearchBeanList", meberClaimSearchBeanList);

            System.out.println("batch not null : size = " + claimList.size());

            session.setAttribute("pracCCClaimSearchResult", claimList);
            session.setAttribute("pracCCClaimSearchResultCount", searchResult.getResultCounter());
            session.setAttribute("pracCCClaimSearch", searchResult.getCcSearch());

        }
        //add result message to session
        session.removeAttribute("practiceClaimSearchResultsMessage");
        session.setAttribute("practiceClaimSearchResultsMessage", claimSearchResultMsg);

        try {
            String nextJSP = "/Claims/PracticeClaimSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return null;
    }

    @Override
    public String getName() {

        return "SearchPracticeClaimsCommand";
    }
}

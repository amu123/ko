/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.LookupValue;

/**
 *
 * @author Johan-NB
 */
public class AuthBenefitListDisplay extends TagSupport {

    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            out.println("<td colspan=\"6\"><table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th align=\"left\">Benefit Id</th><th align=\"left\">Benefit Description</th></tr>");

            LookupValue ben = (LookupValue) session.getAttribute(sessionAttribute);
            if (ben != null) {
                out.println("<tr>"
                        + "<td><label class=\"label\">" + ben.getId() + "</label></td> "
                        + "<td width=\"300\"><label class=\"label\">" + ben.getValue() + "</label></td> "
                        + "</tr>");
            }

            out.println("</table></td>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in AuthBenefitListDisplay tag", ex);
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

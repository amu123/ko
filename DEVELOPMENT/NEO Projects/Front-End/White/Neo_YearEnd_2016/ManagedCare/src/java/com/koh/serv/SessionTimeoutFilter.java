/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.serv;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author gerritj
 */
public class SessionTimeoutFilter implements Filter {

    private String timeoutPage = "index.jsp";

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException,
            ServletException {
//        System.out.println("in do filter");
//        if ((request instanceof HttpServletRequest) && (response instanceof HttpServletResponse)) {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
//        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        // is session expire control required for this request?
        if (isSessionControlRequiredForThisResource(httpServletRequest)) {

            // is session invalid?
            if (isSessionInvalid(httpServletRequest)) {
//                String timeoutUrl = httpServletRequest.getContextPath() + "/" + getTimeoutPage();
//                    httpServletResponse.sendRedirect(timeoutUrl);
                PrintWriter out = response.getWriter();
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Your Page Title</title>");
                out.println("<link rel='stylesheet' href='/ManagedCare/resources/styles.css'>");
                
                out.println("<script type=\"text/javascript\">");
                
          
                
                out.println("function doReload() {");
                out.println("parent.location=\'/ManagedCare/index.jsp\';");
                out.println(" }");
                out.println(" </script>");
                out.println("<BODY onload=\"doReload()\">");
                out.println("</BODY>");
                out.println("</HTML>");
                return;
            }
        }
//        }
        filterChain.doFilter(request, response);
    }

    /**
     *
     * session shouldn't be checked for some pages. For example: for timeout page..
     * Since we're redirecting to timeout page from this filter,
     * if we don't disable session control for it, filter will again redirect to it
     * and this will be result with an infinite loop...
     */
    private boolean isSessionControlRequiredForThisResource(HttpServletRequest httpServletRequest) {
        String requestPath = httpServletRequest.getRequestURI();
        boolean controlRequired = !requestPath.contains("index");

        return controlRequired;
    }

    private boolean isSessionInvalid(HttpServletRequest httpServletRequest) {
        //maybe add user object check
        boolean sessionInValid = (httpServletRequest.getRequestedSessionId() != null) && !httpServletRequest.isRequestedSessionIdValid();
        return sessionInValid;
    }

    public void destroy() {
    }

    public String getTimeoutPage() {
        return timeoutPage;
    }

    public void setTimeoutPage(String timeoutPage) {
        this.timeoutPage = timeoutPage;
    }
}
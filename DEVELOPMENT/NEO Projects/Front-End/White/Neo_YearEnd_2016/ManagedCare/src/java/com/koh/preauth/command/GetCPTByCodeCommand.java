/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Procedure;

/**
 *
 * @author johanl
 */
public class GetCPTByCodeCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;

        String returnStr = "";
        String code = request.getParameter("code");
                    
        Procedure proc = service.getNeoManagerBeanPort().getProcedureForCode(code);
        if(proc != null){
            session.setAttribute("cptCode_text", proc.getCode());
            session.setAttribute("cptCodeDesc", proc.getDescription());

            returnStr = "OK|"+proc.getCode()+"|"+proc.getDescription();

        }else{
            returnStr = "ERROR|CPT Code not found";
        }
                
        try{
            out = response.getWriter();
            out.println(returnStr);

        }catch(Exception ex){
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "GetCPTByCodeCommand";
    }
}

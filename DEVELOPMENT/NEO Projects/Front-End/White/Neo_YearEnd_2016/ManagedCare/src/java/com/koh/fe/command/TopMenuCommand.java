/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.fe.command;

import com.koh.command.Command;
import com.koh.serv.PropertiesReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoMenu;
import neo.manager.NeoUser;
import neo.manager.SecurityResponsibility;

/**
 *
 * @author gerritj
 */
public class TopMenuCommand extends Command {



    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
       
        String fontColor = null;
        String menuNameColor = null;
        try {
            fontColor = new PropertiesReader().getProperty("fontColor");
            menuNameColor = new PropertiesReader().getProperty("menuColor"); 
        } catch (IOException ex) {
            Logger.getLogger(TopMenuCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        String screenWidth = "<script type=\"text/javascript\">function GetReso(){return screen.window.avail;}</script>";
        String client = String.valueOf(context.getAttribute("Client"));
        System.out.println("[fr]Client (TOPMENUCOMMAND)= " +client);
        
        if(fontColor == null)
            fontColor = "#666666";
        
        if(menuNameColor == null)
            menuNameColor = "#FFFFFF";
        
       
                try {
            PrintWriter out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"resources/fisheye/fisheye-menu.css\" />");
            out.println("<script src=\"resources/fisheye/fisheye.js\" type='text/javascript'></script>");
            out.println("<script type=\"text/javascript\">");
            out.println("function doMenuClick(p, url) {");
            out.println("parent.leftMenu.document.forms[0].application.value = p;");
            out.println("parent.leftMenu.document.forms[0].submit();");
            out.println("parent.worker.location = url;");
            out.println(" }");
            out.println(" </script>");
            out.println("</head>");
            out.println("<body style=\"margin:8px\">");
            out.println("<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr>");
            out.println("<td width=\"100%\" >");
            out.println("<table width=\"100%\">");
            out.println("<tr height=\"90\">");
            out.println("<td width=\"230\">"); 
            out.println("<form name=\"logout\" action=\"/ManagedCare/AgileController\" method=\"POST\" style=\"margin:0px\">");
            out.println("<input type=\"hidden\" name=\"opperation\" value=\"LogoutCommand\"/>");
            if(client.equalsIgnoreCase("Agility")){
            out.println("<a href=\"javascript:document.logout.submit()\"><img src=\"resources/Agility GHS logo.png\" alt=\"Logout\" border=\"0\"/></a>");
            } else if(client.equalsIgnoreCase("Sechaba")){
                out.println("<a href=\"javascript:document.logout.submit()\"><img src=\"resources/Sechaba logo.png\" alt=\"Logout\" border=\"0\"/></a>");
            }
            out.println("</form>");
            out.println("</td>");
            out.println("<td width=\"100%\" style=\"text-align:center\" bgcolor=\""+menuNameColor+"\">");
            ArrayList<NeoMenu> topMenus = new ArrayList<NeoMenu>();
            NeoUser user = (NeoUser) session.getAttribute("persist_user");
            if (user != null) {
                ArrayList<SecurityResponsibility> respList = (ArrayList) user.getSecurityResponsibility();
                if (respList != null) {
                    for (int i = 0; i < respList.size(); i++) {
                        SecurityResponsibility resp = respList.get(i);
                        ArrayList<NeoMenu> menuItems = (ArrayList) resp.getMenuItems();
                        if (menuItems != null) {
                            for (int j = 0; j < menuItems.size(); j++) {
                                NeoMenu menuItem = menuItems.get(j);
                                if (menuItem.getParentMenuItem() == 0) {
                                    if (client.equalsIgnoreCase("Sechaba") && menuItem.getMenuItemId() == 34) {
                                        continue;
                                    }
                                    topMenus.add(menuItem);
                                }
                            }
                        }
                    }
                }
            }
            if (topMenus.size() > 0) {
                out.println("<table width=\"100%\">");
                out.println("<tr><td width=\"20px\">");
                out.println("</td><td valign=\"bottom\">");
                out.println("<div>");
                out.println("<center>");
                out.println("<ul id=\"fisheye_menu\">");
                if(client.equalsIgnoreCase("Sechaba")){
                    for (int i = 0; i < topMenus.size(); i++) {
                        NeoMenu menuItem = topMenus.get(i);
                        //System.out.println("Action " + menuItem.getAttributes().getAction());
                        //System.out.println("Big Image " + menuItem.getAttributes().getBigImageName());
                        //System.out.println("Small Image " + menuItem.getAttributes().getSmallImageName());
                       // out.println("<li style=\"border-style: solid;border-width: 1px;border-color: red;\"><a href=\"# " + (i + 1) + "\"><img style=\"border-style: solid;border-width: 1px;border-color: red;\"  onclick=\"doMenuClick(\'" + menuItem.getMenuItemId() + "\', \'" + menuItem.getAttributes().getAction() + "\')\" src=\"resources/" + menuItem.getAttributes().getBigImageName() + "\" alt=\"" + menuItem.getDisplayName() + "\"/><label style=\"color:"+fontColor+";border-style: solid;border-width: 1px;border-color: green;\">" + menuItem.getDisplayName() + "</label></a></li>");
                        if(menuItem.getMenuItemId() == 143 || menuItem.getMenuItemId() == 145){
                            out.println("<li><a target=\"_blank\" href=\"" + menuItem.getAttributes().getAction() + "\"><img src=\"resources/" + menuItem.getAttributes().getBigImageName() + "\" alt=\"" + menuItem.getDisplayName() + "\"/><span style=\"color:"+fontColor+";\">" + menuItem.getDisplayName() + "</span></a></li>");
                        } else {
                       out.println("<li><a href=\"# " + (i + 1) + "\"><img onclick=\"doMenuClick(\'" + menuItem.getMenuItemId() + "\', \'" + menuItem.getAttributes().getAction() + "\')\" src=\"resources/" + menuItem.getAttributes().getBigImageName() + "\" alt=\"" + menuItem.getDisplayName() + "\"/><span style=\"color:"+fontColor+";\">" + menuItem.getDisplayName() + "</span></a></li>");
                        }
                    }
                } else if(client.equalsIgnoreCase("Agility")){
                    for (int i = 0; i < topMenus.size(); i++) {
                        NeoMenu menuItem = topMenus.get(i);
                        out.println("<li><a href=\"# " + (i + 1) + "\"><img onclick=\"doMenuClick(\'" + menuItem.getMenuItemId() + "\', \'" + menuItem.getAttributes().getAction() + "\')\" src=\"resources/" + menuItem.getAttributes().getBigImageName() + "\" alt=\"" + menuItem.getDisplayName() + "\"/><span style=\"color:"+fontColor+";\">" + menuItem.getDisplayName() + "</span></a></li>");
                    }
                }
                out.println("</ul>");
                out.println("</center>");
                out.println("</div>");
                out.println("</td></tr>");
                out.println("</table>");
            }
            out.println("</td>");
            out.println("</tr>");
            out.println("</table>");
            out.println(" </td>");
            out.println("</tr>");
            if(client.equalsIgnoreCase("Agility")){
            out.println("<tr bgcolor=\"#660000\">");/*#a50d12*/
            } else if(client.equalsIgnoreCase("Sechaba")){
                out.println("<tr bgcolor=\"#6cc24a\">");
            }
            out.println(" <td width=\"100%\" height=\"8\">&nbsp;</td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("<form action=\"/ManagedCare/AgileController\" name=\"leftMenu\">");
            out.println("<input type=\"hidden\" name=\"opperation\" value=\"LeftMenuCommand\" size=\"30\" />");
            out.println("<input type=\"hidden\" name=\"application\" value=\"0\" size=\"30\" />");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "TopMenuCommand";
    }




}

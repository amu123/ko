/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.employer.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.EmployerGroup;
import neo.manager.EmployerGroupStatus;
import neo.manager.NeoManagerBean;

/**
 *
 * @author yuganp
 */
public class EmployerViewCommand extends NeoCommand {

   
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String employerId = request.getParameter("employerId");
        request.setAttribute("employerEntityId", employerId);
        String empCode = request.getParameter("employerNumber");
        System.out.println("menuResp = " + request.getParameter("menuResp"));
        request.setAttribute("menuResp", request.getParameter("menuResp"));
        if (empCode == null || empCode.isEmpty()) {
            EmployerGroup employer = port.getEmployerByEntityId(getIntParam(request, "employerId"));
            if (employer != null) {
                request.setAttribute("employerName", employer.getEmployerName());
                request.setAttribute("employerNumber", employer.getEmployerNumber());
            }
        } else {
            request.setAttribute("employerName", request.getParameter("employerName"));
            request.setAttribute("employerNumber", request.getParameter("employerNumber"));
        }
        EmployerGroupStatus employerGroupStatusByEntityId = port.fetchEmployerGroupStatusByEntityId(getIntParam(request, "employerId"));
        if (employerGroupStatusByEntityId != null) {
            request.setAttribute("empStatus", employerGroupStatusByEntityId.getEmpGroupStatus());
        }
        try {
            context.getRequestDispatcher("/Employer/Employer.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(EmployerViewCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EmployerViewCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private int getIntParam(HttpServletRequest request, String paramName) {
        try {
            String maxDepStr = request.getParameter(paramName);
            return Integer.parseInt(maxDepStr);
        } catch (java.lang.Exception e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }

    @Override
    public String getName() {
        return "EmployerViewCommand";
    }
}

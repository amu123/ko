/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.cover.MemberMainMapping;
import com.koh.employer.command.TabUtils;
import com.koh.fe.command.LoginCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverAdditionalInfo;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author charlh
 */
public class SaveNominatedProviderInfoCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String s = request.getParameter("buttonPressed");
        System.out.println("buttonPressed " + s);
        
        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("doctorSearch".trim())) {
                try {
                    searchDoctor(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveNominatedProviderInfoCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveNominatedProviderInfoCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }
        
        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("dentistSearch".trim())) {
                try {
                    searchDentist(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveNominatedProviderInfoCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveNominatedProviderInfoCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }
        
        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("optometristSearch".trim())) {
                try {
                    searchOptometrist(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveNominatedProviderInfoCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveNominatedProviderInfoCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }
        
        String result = validateAndSave(request);
        
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    private void searchDoctor(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        
        request.setAttribute("practiceName", "MAI_doctorPracticeName");
        request.setAttribute("practiceNumber", "MAI_doctorPracticeNumber");
        request.setAttribute("practiceNetwork", "MAI_doctorPracticeNetwork");
        request.setAttribute("DSPType", "doctor");
        
        context.getRequestDispatcher("/Member/MemberSearchPractice.jsp").forward(request, response);
    }
    
    private void searchDentist(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        
        request.setAttribute("practiceName", "MAI_dentistPracticeName");
        request.setAttribute("practiceNumber", "MAI_dentistPracticeNumber");
        request.setAttribute("practiceNetwork", "MAI_dentistPracticeNetwork");
        request.setAttribute("DSPType", "dentist");
        
        context.getRequestDispatcher("/Member/MemberSearchPractice.jsp").forward(request, response);
    }
    
    private void searchOptometrist(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        
        request.setAttribute("practiceName", "MAI_optometristPracticeName");
        request.setAttribute("practiceNumber", "MAI_optometristPracticeNumber");
        request.setAttribute("practiceNetwork", "MAI_optometristPracticeNetwork");
        request.setAttribute("DSPType", "optometrist");
        
        context.getRequestDispatcher("/Member/MemberSearchPractice.jsp").forward(request, response);
    }
    
    private String validateAndSave(HttpServletRequest request) {
        String additionalData = null;
        String status = "";
        String validationErros = "";
        
        if (save(request) >= 1) {
            status = "OK";
            additionalData = "\"message\":\"Member Additional Details Updated\"";
        } else {
            status = "ERROR";
            additionalData = "\"message\":\"There was an error updating the member additional details.\"";
        }
        return TabUtils.buildJsonResult(status, validationErros, "", additionalData);
    }
    
    private int save(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        HttpSession session = request.getSession();
        int success = 0;
        
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);
        
        try {
            // Doctor
            Integer entityID = (Integer) session.getAttribute("memberEntityCommon");
            
            if (request.getParameter("MAI_doctorPracticeNumber") != null && DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_doctorPracticeStartDate")) != null) {
                CoverAdditionalInfo memDoct = MemberMainMapping.getNominatedProviderDoctorDetails(request);
                memDoct.setEntityCommonId(entityID);
                memDoct.setCreatedBy(sec.getCreatedBy());
                memDoct.setLastUpdatedBy(sec.getLastUpdatedBy());
                memDoct.setSecurityGroupId(sec.getSecurityGroupId());
                success = port.saveCoverAdd(memDoct);
            }
            
            // Dentist
            
            
            if (request.getParameter("MAI_dentistPracticeNumber") != null && DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_dentistPracticeStartDate")) != null) {
                CoverAdditionalInfo memDent = MemberMainMapping.getNominatedProviderDentistDetails(request);
                memDent.setEntityCommonId(entityID);
                memDent.setCreatedBy(sec.getCreatedBy());
                memDent.setLastUpdatedBy(sec.getLastUpdatedBy());
                memDent.setSecurityGroupId(sec.getSecurityGroupId());
                success = port.saveCoverAdd(memDent);
            }
            
            //Optometrist
            if (request.getParameter("MAI_optometristPracticeNumber") != null && DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_optometristPracticeStartDate")) != null) {
                CoverAdditionalInfo memOpt = MemberMainMapping.getNominatedProviderOptometristDetails(request);
                memOpt.setEntityCommonId(entityID);
                memOpt.setCreatedBy(sec.getCreatedBy());
                memOpt.setLastUpdatedBy(sec.getLastUpdatedBy());
                memOpt.setSecurityGroupId(sec.getSecurityGroupId());
                success = port.saveCoverAdd(memOpt);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return success;
        }
        return success;
    }
    
    @Override
    public String getName() {
        return "SaveNominatedProviderInfoCommand";
    }
}
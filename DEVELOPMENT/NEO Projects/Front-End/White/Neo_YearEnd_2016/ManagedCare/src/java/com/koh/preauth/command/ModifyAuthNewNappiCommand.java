/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author johanl
 */
public class ModifyAuthNewNappiCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        ArrayList<AuthTariffDetails> atList = (ArrayList<AuthTariffDetails>) session.getAttribute("newNappiListArray");
        try {
            PrintWriter out = response.getWriter();
            if (atList != null && atList.size() != 0) {
                String nCode = request.getParameter("nTableCode");
                String nDesc = request.getParameter("nTableDesc");
                for (AuthTariffDetails at : atList) {
                    if (nCode.equals(at.getTariffCode())) {
                        if (nDesc.equalsIgnoreCase(at.getTariffDesc())) {

                            session.setAttribute("napDesc", at.getTariffDesc());
                            session.setAttribute("napCode_text", at.getTariffCode());

                            out.print(getName() + "|" + at.getTariffCode() + "|" + at.getTariffDesc());
                            atList.remove(at);
                            break;
                        }
                    }
                }
                session.setAttribute("newNappiListArray", atList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ModifyAuthNewNappiCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.calltrack.command;

import com.agile.security.webservice.EmailContent;
import com.koh.command.FECommand;
import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import javax.mail.internet.InternetAddress;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CallTrackDetails;
import neo.manager.ContactDetails;
import neo.manager.CoverDetails;
import neo.manager.NeoUser;
import neo.manager.PersonDetails;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class UpdateLoadedCommand extends NeoCommand {

    private Logger log = Logger.getLogger(UpdateLoadedCommand.class);
    private String nextJSP;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        String emailFrom = "";
        String scheme = "";
        String emailTo = "";
        CoverDetails coverDetails;
        
        boolean checkError = false;
        HttpSession session = request.getSession();
        CallTrackDetails details = new CallTrackDetails();
        String number = request.getParameter("memNo");
        coverDetails = service.getNeoManagerBeanPort().getEAuthResignedMembers(number);
        ArrayList<ContactDetails> contact = (ArrayList<ContactDetails>) service.getNeoManagerBeanPort().getAllContactDetails(coverDetails.getEntityId());
        PersonDetails pd = service.getNeoManagerBeanPort().getPersonDetailsByEntityId(coverDetails.getEntityId());

        // TODO
        //Integer callId = new Integer((String)session.getAttribute("trackId"));
        String callTrackId = "" + session.getAttribute("trackId");
        int callId;
        if (callTrackId != null && !callTrackId.equalsIgnoreCase("")) {
            // System.out.println("The call id is null " + callTrackId);
            callId = Integer.parseInt(callTrackId);
            details.setCallTrackId(callId);
        } else {
            // System.out.println("The thing is null " + callTrackId);
        }
        String callRef = (String) session.getAttribute("callTrackNumber");

        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        //details.setCallTrackId(callId);
        details.setCallReference(callRef);

        if (request.getParameter("callType") != null && !request.getParameter("callType").equals("")) {
            details.setCallType(request.getParameter("callType"));
        } else {
            // System.out.println("Should be mandatory");
        }

        if (request.getParameter("callStatus") != null && !request.getParameter("callStatus").equals("")) {
            details.setCallStatus(request.getParameter("callStatus"));
            details.setCallStatusId(Integer.parseInt(request.getParameter("callStatus")));
        } else {
            // System.out.println("Should be mandatory");
        }

        if (request.getParameter("refUser") != null && !request.getParameter("refUser").equals("") && request.getParameter("callStatus").equals("2")) {
            int referedUserId = Integer.parseInt(request.getParameter("refUser"));
            details.setReferedUserId(referedUserId);
        } else {
            //  System.out.println("Should be mandatory");
        }


        //Cannot be changed, so get from session
        if (session.getAttribute("callStartDate") != null && !session.getAttribute("callStartDate").equals("")) {
            details.setCallStartDate((XMLGregorianCalendar) session.getAttribute("callStartDate"));
        }

        if (session.getAttribute("callEndDate") != null && !session.getAttribute("callEndDate").equals("")) {
            details.setCallEndDate((XMLGregorianCalendar) session.getAttribute("callEndDate"));
        }

        if (session.getAttribute("memberNumber_text") != null && !((String) session.getAttribute("memberNumber_text")).equals("") && details.getCallType().equals("1")) {
            details.setCoverNumber((String) session.getAttribute("memberNumber_text"));
        }

        if (session.getAttribute("providerNumber_text") != null && !((String) session.getAttribute("providerNumber_text")).equals("")) {
            details.setProviderNumber((String) session.getAttribute("providerNumber_text"));
        }

        if (session.getAttribute("provName") != null && !((String) session.getAttribute("provName")).equals("")) {
            details.setProviderNameSurname((String) session.getAttribute("provName"));
        }

        if (session.getAttribute("discipline") != null && !((String) session.getAttribute("discipline")).equals("")) {
            details.setProviderDiscipline((String) session.getAttribute("discipline"));
        }

        //Can be changed, so get again from request
        if (request.getParameter("callerName") != null && !request.getParameter("callerName").equals("")) {
            details.setCallerName(request.getParameter("callerName"));
        } else {
            session.setAttribute("callerName_error", "mandatory field");
            checkError = true;
        }

        if (request.getParameter("callerContact") != null && !request.getParameter("callerContact").equals("")) {
            details.setContactDetails(request.getParameter("callerContact"));
        } else {
            session.setAttribute("callerContact_error", "mandatory field");
            checkError = true;
        }

        if (request.getParameter("notes") != null && !request.getParameter("notes").equals("")) {
            if (!request.getParameter("notes").equalsIgnoreCase("" + session.getAttribute("callTrackingNotes"))) {
                details.setNotes(session.getAttribute("callTrackingNotes") + " " + request.getParameter("notes"));
            } else {
                details.setNotes(request.getParameter("notes"));
            }
            log.info("The notes are " + session.getAttribute("callTrackingNotes"));
            log.info("The notes are " + request.getAttribute("callTrackingNotes"));
        }

        // clearAllFromSession(request);

        Enumeration e = request.getParameterNames();
        Collection<CallTrackDetails> resps = new ArrayList<CallTrackDetails>();
        String callQ = "";
        String query = "";
        boolean init = true;
        while (e.hasMoreElements()) {
            String pName = "" + e.nextElement();

            //if ((pName.contains("callQ_") | pName.contains("disciplineCategory_") | pName.contains("benefitSub_") | pName.contains("claimSub_") | pName.contains("membershipSub_") | pName.contains("underwritingSub_") | pName.contains("queryMethod_")) && (!pName.contains("_identifier"))) {
            if ((pName.contains("callQ") | (pName.contains("queryMethod")) | pName.contains("claimsFC_") | pName.contains("claimsSC_") | pName.contains("claimsTC_") | pName.contains("statementsFC_") | pName.contains("statementsTC_") | pName.contains("benefitsFC_") | pName.contains("benefitsSC_") | pName.contains("benefitsTC_") | pName.contains("prenmiumsFC_") | pName.contains("premiumsTC_") | pName.contains("membershipFC_") | pName.contains("memberShipTC_") | pName.contains("serviceProviderFC_") | pName.contains("serviceProviderTC_") | pName.contains("foundationFC_") | pName.contains("foundationSC_")) && (!pName.contains("_identifier"))) {
                String identifier = null;
                String respId = "";
                respId = request.getParameter(pName);
                identifier = request.getParameter(pName);

                if (identifier != null) {
                    if (init) {

                        callQ += "," + identifier;
                    } else {
                        callQ += "," + identifier;
                    }
                }
            }
        }

        if (callQ.startsWith(",")) {

            query = callQ.substring(1);
        }
        log.info("The queries session varables " + session.getAttribute("callQueries2"));
        details.setCallQuery("" + session.getAttribute("callQueries2"));

        try {
            details.setResoUserName(user.getUsername());
            details.setResoUserId(user.getUserId());
            int userId = user.getUserId();
            //clearAllFromSession(request);
            nextJSP = "/Calltrack/CallTrackworkbenchForUser.jsp";
            
            boolean isValid = false;

            for (ContactDetails cd : contact) {
                if (cd.getCommunicationMethod().equals("E-mail")) {
                    emailTo = cd.getMethodDetails();
                }
            }

            //Validate member email address
            try {
                new InternetAddress(emailTo).validate();
                isValid = true;
            } catch (Exception ex) {
                //Member Mail failed now trying contact details for call tracking
                try {
                    emailTo = details.getContactDetails();
                    new InternetAddress(emailTo).validate();
                    isValid = true;
                } catch (Exception ex2) {
                    System.out.println("MAIL| Member does not have a valid email address");
                }
            }
            
            boolean sendSms = false;
            
            System.out.println("CALL TYPE = " + request.getParameter("callType"));

            if (request.getParameter("callType") != null && !request.getParameter("callType").equals("") && request.getParameter("callType").equals("1")) {
                Date today = new Date();
                if (today.after(DateTimeUtils.getDateFromXMLGregorianCalendar(coverDetails.getCoverEndDate()))) {
                    sendSms = false;
                    System.out.println("Cover ended. Not sending SMS");
                } else {
                    sendSms = true;
                }               
            } else {
                sendSms = false;
            }
            
            System.out.println("Sending SMS = " + sendSms);
                
            //validate email first, so that the backend that can determine when not to send the sms
            int success = service.getNeoManagerBeanPort().updateCallTrackDetails(userId, details, isValid, sendSms);

            //Only send mail when call is closed
            if (isValid && success > 0 && details.getCallStatus().equals("3") && details.getCallType().equals("1") && coverDetails.getProductId() > 0 && sendSms) {                
                // Check to ensure product id is not null                    
                if (coverDetails.getProductId() == 1) {
                    scheme = "Resolution Health";
                    emailFrom = "Noreply@agilityghs.co.za";
                } else if (coverDetails.getProductId() == 2) {
                    scheme = "Spectramed";
                    emailFrom = "noreply@spectramed.co.za";
                } else if (coverDetails.getProductId() == 3){
                    scheme = "Sizwe";
                    emailFrom = "do-not-reply@sizwe.co.za";
                }

                String subject = scheme;
                String emailMsg = null;
                if (details.getCallType().equals("1")) {
                    subject += " - Member Survey";
                }

                EmailContent content = new EmailContent();
                content.setName(pd.getName());
                content.setSurname(pd.getSurname());
                content.setMemberCoverNumber(details.getCoverNumber());
                content.setContentType("text/plain");
                content.setSubject(subject.trim());
                content.setEmailAddressFrom(emailFrom.trim());
                content.setEmailAddressTo(emailTo.trim());
                content.setProductId(coverDetails.getProductId());

                //Determine msg for email according to type of document
                content.setType("survey");
                /*If document type is custom, this message added here will be displayed in the email. The email method will automatically add the 
                 the enquiries and call center aswell as the kind regards at the end of the message*/
                content.setContent(emailMsg);
                boolean emailSent = false;

                emailSent = FECommand.service.getAgileManagerPort().sendEmailWithOrWithoutAttachment(content);

                if (emailSent) {
                    System.out.println("MAIL| Survey email sent successfully!");
                }
            }

            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "UpdateLoadedCommand";
    }
}

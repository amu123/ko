/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author johanl
 */
public class CalculateNappiQuantityCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;
        try {
            out = response.getWriter();
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-cache");

            double tariffAmount = new Double("" + session.getAttribute("defaultNappiAmount"));
            double tQuan = 0.0d;
            try {
                tQuan = new Double("" + session.getAttribute("defaultNappiQuan"));
            } catch (Exception ex) {
                System.out.println("exception : " + ex.getMessage());
            }

            double tariffQuantity = new Double(request.getParameter("napQuan"));
            double tAmount = 0.0d;
            double newTAmount = 0.0d;

            if (tQuan == 0.0d) {
                tQuan = 1.0d;
            }
            if (tariffAmount != 0.0d) {
                tAmount = tariffAmount / tQuan;
                if (tariffQuantity != 0.0d) {
                    newTAmount = tAmount * tariffQuantity;
                }
            }
            out.print("Done|" + tariffQuantity + "|" + newTAmount);

        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            out.close();
        }
        return null;
    }

    @Override
    public String getName() {
        return "CalculateNappiQuantityCommand";
    }
}

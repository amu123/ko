/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.AddressDetails;
import org.apache.log4j.Logger;


/**
 *
 * @author josephm
 */
public class UpdateMemberAddressDetailsCommand extends NeoCommand {
    
    private Logger log = Logger.getLogger(UpdateMemberAddressDetailsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse resposne, ServletContext context) {
        this.saveScreenToSession(request);

        log.info("Inside UpdateMemberAddressDetailsCommand");
        AddressDetails details = new AddressDetails();

        //details.set

        //Physical address
        String addressLine1 = request.getParameter("memberPhysicalAddressLine1");
        String addressLine2 = request.getParameter("memberPhysicalAddressLine2");
        String addressLine3 = request.getParameter("memberPhysicalAddressLine3");
        String physicalCity = request.getParameter("memberPhysicalCityAddrees");
        String physicalCode = request.getParameter("memberPhysicalCodeAddress");

        //Postal details
        String memnerPostalLine1 = request.getParameter("memberPostalAddressLine1");
        String memberPostalLine2 = request.getParameter("memberPostalAddressLine2");
        String memberPostalLine3 = request.getParameter("memberPostalAddressLine3");
        String memberPostalCity = request.getParameter("memberPostalCityAddress");
        String memberPostalCode = request.getParameter("memberPostalCodeAddress");

        //Contaact details
        String memberHomePhone = request.getParameter("memberHomeTel");
        String memberFax = request.getParameter("memberFaxNo");
        String workNumber = request.getParameter("memberWorkNo");
        String nextOfKin = request.getParameter("memberNextOfKinDetails");
        String memberContactNumber = request.getParameter("memberContactPersonTelNo");
        String memberCellNumber = request.getParameter("memberCellNo");
        String memberEmailAddress = request.getParameter("memberEmailAddress");

        log.info("The address line 1 is " + addressLine1);
        log.info("The address line 2 is " + addressLine2);
        log.info("The address line 3 is " + addressLine3);
        log.info("The Physical city is " + physicalCity);
        log.info("The Physical code is " + physicalCode);

        log.info("The member postal line 1" + memnerPostalLine1);
        log.info("The member postal line 2" + memberPostalLine2);
        log.info("The member postal line 3" + memberPostalLine3);
        log.info("The postal city " + memberPostalCity);
        log.info("The postal code " + memberPostalCode);

        service.getNeoManagerBeanPort();


        try {

           // String nextJSP = "/Claims/MemberDetailsTabbedPage.jsp";
            String nextJSP = "/PDC/ClaimsHistory.jsp";
            RequestDispatcher dispacther = context.getRequestDispatcher(nextJSP);
            dispacther.forward(request, resposne);

        }catch(Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {

        return "UpdateMemberAddressDetailsCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.Collection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AddressDetails;
import neo.manager.ContactDetails;
import neo.manager.CoverDetails;
import neo.manager.CoverPDCDetails;
import neo.manager.NeoManagerBean;
import neo.manager.ProviderDetails;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ViewPracticeAddressDetailsCommand extends NeoCommand {

        private Logger log = Logger.getLogger(ViewPracticeAddressDetailsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        log.info("Inside ViewPracticeAddressDetailsCommand");

        int entityId = 0;

        HttpSession session = request.getSession();

        NeoManagerBean port = service.getNeoManagerBeanPort();

        String practiceNumber =  (String) session.getAttribute("practiceNumber");
        String practiceType =  (String) session.getAttribute("PracticeType");
        
        ProviderDetails list;
        if(practiceType != null && !practiceType.isEmpty() && practiceType.equalsIgnoreCase("Practice"))
        {
            list = port.getPracticeDetailsForEntityByNumber(practiceNumber);
            session.removeAttribute("PracticeType");
        }
        else
        {
            list = port.getProviderDetailsForEntityByNumber(practiceNumber);
        }
        if (list != null) {
            entityId = list.getEntityId();
        }

        ArrayList<ContactDetails> contactDetailsList = (ArrayList<ContactDetails>) port.getAllContactDetails(entityId);
        
        ArrayList<AddressDetails> detailsAddressList = (ArrayList<AddressDetails>) port.getAllAddressDetails(entityId);


        for (AddressDetails details : detailsAddressList) {

            if (details.getAddressType().equals("Physical")) {
                //Physical
                session.setAttribute("practicePhysicalAddressLine1", details.getAddressLine1());
                session.setAttribute("practicePhysicalAddressLine2", details.getAddressLine2());
                session.setAttribute("practicePhysicalAddressLine3", details.getAddressLine3());
                session.setAttribute("practicePhysicalCityAddrees", details.getRegion());
                session.setAttribute("practicePhysicalCodeAddress", details.getPostalCode());
            } else if (details.getAddressType().equals("Postal")) {
                //Postal
                session.setAttribute("practicePostalAddressLine1", details.getAddressLine1());
                session.setAttribute("practicePostalAddressLine2", details.getAddressLine2());
                session.setAttribute("practicePostalAddressLine3", details.getAddressLine3());
                session.setAttribute("practicePostalCityAddress", details.getRegion());
                session.setAttribute("practicePostalCodeAddress", details.getPostalCode());
            }
        }

        for (ContactDetails cd : contactDetailsList) {
            if (cd.getCommunicationMethod().equals("E-mail")) {
                session.setAttribute("practiceEmailAddress", cd.getMethodDetails());
            } else if (cd.getCommunicationMethod().equals("Fax")) {
                session.setAttribute("practiceFaxNo", cd.getMethodDetails());
            } else if (cd.getCommunicationMethod().equals("Cell")) {
                session.setAttribute("practiceCellNo", cd.getMethodDetails());
            } else if (cd.getCommunicationMethod().equals("Tel (work)")) {
                session.setAttribute("practiceWorkNo", cd.getMethodDetails());
            } else if (cd.getCommunicationMethod().equals("Tel (home)")) {
                session.setAttribute("practiceHomeTelNumber", cd.getMethodDetails());
            }
            log.info("The communication method is " + cd.getCommunicationMethod());
            log.info("The method is " + cd.getMethodDetails());
            session.setAttribute("preferredMethod", cd.getCommunicationMethod());
            log.info("The preferred method is " + cd.getCommunicationMethod());

        }

        try {
            
            String nextJSP = "/Claims/PracticeAddressDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch(Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "ViewPracticeAddressDetailsCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.utils;

import java.net.URLEncoder;

/**
 *
 * @author yuganp
 */
public class UrlUtil {
    public static String getUrl(String action, String paramName, String paramValue) {
        String result = action;
        try {
            result += (action.contains("?") ? "&" : "?") + paramName + "=" +  URLEncoder.encode(paramValue, "UTF-8"); 
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }
        return result;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.auth.command;

import com.koh.auth.dto.MemberSearchResult;
import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.NeoCoverDetailsDependantFilter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CoverSearchCriteria;
import neo.manager.EAuthCoverDetails;

/**
 *
 * @author gerritj
 */
public class SearchMemberCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        //to get from service
        String memberNumber = request.getParameter("memNo");
        String firstName = request.getParameter("name");
        String surname = request.getParameter("surname");
        String product = request.getParameter("scheme");
        String option = request.getParameter("schemeOption");
        String idNumber = request.getParameter("idNumber");
        String dateOfBirth = request.getParameter("dateOfBirth");
        String errorMsg = "";
        
        CoverSearchCriteria search = new CoverSearchCriteria();
        boolean flag = false;
        if (firstName != null && !firstName.equals("")) {
            flag = true;
            search.setCoverName(firstName);
        }
        if (surname != null && !surname.equals("")) {
            flag = true;
            search.setCoverSurname(surname);
        }
        if (product != null && !product.equals("") && !product.equals("99")) {
            search.setProductId(Integer.parseInt(product));
        }
        if (option != null && !option.equals("") && !option.equals("99")) {
            search.setOptionId(Integer.parseInt(option));
        }
        if (idNumber != null && !idNumber.equals("")) {

            flag = true;
            search.setCoverIDNumber(idNumber);
        }
        if (dateOfBirth != null && !dateOfBirth.equals("")) {

            flag = true;
            XMLGregorianCalendar dob = null;
            try {
                dob = DateTimeUtils.convertDateToXMLGregorianCalendar(dateFormat.parse(dateOfBirth));
            } catch (ParseException ex) {
                Logger.getLogger(SearchMemberCommand.class.getName()).log(Level.SEVERE, null, ex);
            }
            search.setDateOfBirth(dob);
        }

        if (memberNumber != null && !memberNumber.equals("")) {
            if (!flag) {
                if (memberNumber.length() < 4) {
                    //errorMsg = ": Cover Number Must be at least 4 characters";
                    flag = true;
                    search.setCoverNumber(memberNumber);
                } else {
                    flag = true;
                    search.setCoverNumber(memberNumber);
                }

            } else {
                flag = true;
                search.setCoverNumber(memberNumber);
            }

        }

        ArrayList<MemberSearchResult> members = new ArrayList<MemberSearchResult>();
        List<EAuthCoverDetails> pdList = new ArrayList<EAuthCoverDetails>();
        boolean viewStaff = (Boolean) session.getAttribute("persist_user_viewStaff");
        try {
            if (flag) {
                pdList = service.getNeoManagerBeanPort().eAuthCoverSearch(search);

                //filter dependants of pdList to only display one dependant record for each cover dependant
                NeoCoverDetailsDependantFilter filter = new NeoCoverDetailsDependantFilter();
                pdList = filter.EAuthCoverDetailsDependantFilter(pdList);

                for (EAuthCoverDetails cd : pdList) {
                    if (!viewStaff) {
                        if (cd.getMemberCategory() == 2) {
                            continue;
                        }
                    }

                    MemberSearchResult member = new MemberSearchResult();
                    member.setMemberNumber(cd.getCoverNumber());
                    member.setDependant("0" + cd.getDependentNumber());
                    member.setName(cd.getCoverName());
                    member.setSurname(cd.getCoverSurname());
                    member.setScheme(cd.getProduct());
                    member.setPlan(cd.getOption());
                    member.setStatus(cd.getCoverStatus());
                    member.setBenefitFromDate(cd.getCoverStartDate().toGregorianCalendar().getTime());
                    if (cd.getCoverEndDate() != null) {
                        member.setBenefitToDate(cd.getCoverEndDate().toGregorianCalendar().getTime());
                    }
                    if (cd.getJoinDate() != null) {
                        member.setJoinDate(cd.getJoinDate().toGregorianCalendar().getTime());
                    }
                    members.add(member);
                }
                request.setAttribute("searchResult", members);
                session.setAttribute("searchMem_error", null);
            } else {
                session.setAttribute("searchMem_error", "Please Refine Search " + errorMsg);
            }
            if (!viewStaff) {
                if (pdList != null && members != null) {
                    if (pdList.size() > 0) {
                        if (members.size() == 0) {
                            //session.setAttribute("searchMem_error", "User access denied. Please contact your supervisor " + errorMsg);
                            request.setAttribute("error", "User access denied. Please contact your supervisor");
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            String nextJSP = "/Auth/MemberSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.include(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SearchMemberCommand";
    }
}

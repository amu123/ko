/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.Command;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverDetails;

/**
 *
 * @author bonganim
 */
public class RiskRateCommand extends Command{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        HttpSession session=request.getSession();
        
        
       String depNum=request.getParameter("risRatekButton");
       
       System.out.println("depNum-: "+depNum);
       
       int dep= Integer.parseInt(depNum);
     
       List<CoverDetails> covList=(List<CoverDetails>)session.getAttribute("MemberCoverDetails");
      
       
       for(CoverDetails d: covList){
     
      if (d.getDependentNumber()==dep)
       {
       
         session.setAttribute("policyNumber", d.getCoverNumber());
         session.setAttribute("patientName", d.getName());
         session.setAttribute("surname", d.getSurname());
         session.setAttribute("riskDate", d.getStatusStartDate());
         
         session.setAttribute("depNum", d.getDependentNumber());
         
            session.setAttribute("CoverListObject", d);
         
          System.out.println(" policyNum: "+d.getCoverNumber());
          System.out.println(" depNum: "+d.getDependentNumber());
           System.out.println(" surname: "+d.getSurname());
            System.out.println(" riskDate: "+d.getStatusStartDate());
       }
           
       
       }
     
      
            
          try {
       System.out.println("Navigating to risk rate JSP..");
       
           String nextJSP = "/PDC/RiskRate.jsp";
           RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
           dispatcher.forward(request, response);
  

            
        } catch (Exception ex) {
            System.out.println("Catch in RiskRateCommand.");
            System.out.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public String getName() {
        return"RiskRateCommand";
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author josephm
 */
public class RouteWelcomeCommand extends Command {

   @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
         String nextJSP = "/Security/Welcome.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            try {
                dispatcher.forward(request, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
    }

    @Override
    public String getName() {
        return "RouteWelcomeCommand";
    }
}

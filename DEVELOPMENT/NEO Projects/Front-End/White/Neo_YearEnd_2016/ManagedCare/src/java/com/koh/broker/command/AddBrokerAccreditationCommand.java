/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.brokerFirm.BrokerMapping;
import com.koh.broker_firm.application.commands.BrokerAppTabContentsCommand;
import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.employer.command.TabUtils;
import com.koh.member.application.command.MemberApplicationSpecificQuestionsCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.BrokerAccred;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author princes
 */
public class AddBrokerAccreditationCommand extends NeoCommand{
    private int brokerAccredID = 0;
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        request.setAttribute("brokerEntityId", request.getParameter("brokerEntityId"));
        try {
            String s = request.getParameter("buttonPressed");
            if (s != null && !s.isEmpty()) {
                if (s.equalsIgnoreCase("Add Accreditation")) {
                    addDetails(request, response, context);
                } else if (s.equalsIgnoreCase("SaveButton")) {
                    save(request, response, context);
                }else if (s.equalsIgnoreCase("Edit")) {
                    editDetails(request, response, context);
                }else if (s.equalsIgnoreCase("Remove")) {
                    remove(request, response, context);
                }
            }
            
        } catch (java.lang.Exception ex) {
            Logger.getLogger(MemberApplicationSpecificQuestionsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void addDetails(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));

        context.getRequestDispatcher("/Broker/AddBrokerAccreditation.jsp").forward(request, response);
    }
    
    private void editDetails(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        brokerAccredID = Integer.parseInt(request.getParameter("selectedBrokerAccredIDFlag"));
        String accType = request.getParameter("accreditationTypeFlag");
        String accNum = request.getParameter("accreditationNumberFlag");
        String appDate = request.getParameter("applicationDateFlag");
        String authDate = request.getParameter("authorizationDateFlag");
        String startDate = request.getParameter("startDateFlag");
        String endDate = request.getParameter("endDateFlag");
        request.setAttribute("BrokerApp_brokerAccreditationType", accType);
        request.setAttribute("BrokerApp_brokerAccreditationNumber", accNum);
        request.setAttribute("BrokerApp_brokerApplicationDate", appDate);
        request.setAttribute("BrokerApp_brokerAuthorisationDate", authDate);
        request.setAttribute("BrokerApp_brokerStartDate", startDate);
        request.setAttribute("BrokerApp_brokerEndDate", endDate);
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        context.getRequestDispatcher("/Broker/BrokerAccreditationEdit.jsp").forward(request, response);
    }
    
    private void remove(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        brokerAccredID = Integer.parseInt(request.getParameter("selectedBrokerAccredIDFlag"));
        int entityId = Integer.parseInt(request.getParameter("brokerEntityId"));
        String accType = request.getParameter("accreditationTypeFlag");
        String accNum = request.getParameter("accreditationNumberFlag");
        String appDate = request.getParameter("applicationDateFlag");
        String authDate = request.getParameter("authorizationDateFlag");
        String startDate = request.getParameter("startDateFlag");
        String endDate = request.getParameter("endDateFlag");
        request.setAttribute("BrokerApp_brokerAccreditationType_", accType);
        request.setAttribute("BrokerApp_brokerAccreditationNumber_", accNum);
        request.setAttribute("BrokerApp_brokerApplicationDate_", appDate);
        request.setAttribute("BrokerApp_brokerAuthorisationDate_", authDate);
        request.setAttribute("BrokerApp_brokerStartDate_", startDate);
        request.setAttribute("BrokerApp_brokerEndDate_", endDate);
        
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = getNeoUser(request);
        BrokerAccred ba = BrokerMapping.getAccreditationDetailsForRemove(request, entityId, neoUser, brokerAccredID);
        
        port.removeBrokerFirmAccreditation(ba);
        context.getRequestDispatcher(new BrokerAppTabContentsCommand().getBrokerAccreditation(request, entityId)).forward(request, response);
    }
    
    private int getIntParam(HttpServletRequest request, String paramName) {
        try {
            String maxDepStr = request.getParameter(paramName);
            return Integer.parseInt(maxDepStr);
        } catch (java.lang.Exception e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }
    
    private void save(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = BrokerMapping.validateAccredittation(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        int entityId = new Integer(request.getParameter("brokerEntityId"));
        
        if ("OK".equalsIgnoreCase(status)) {
            if (save(request, entityId, brokerAccredID)) {
                
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving accreditation.\"";
            }
        } else {
            additionalData = null;
        }

        if ("OK".equalsIgnoreCase(status)) {                        
            context.getRequestDispatcher(new BrokerAppTabContentsCommand().getBrokerAccreditation(request, entityId)).forward(request, response);
            
        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }
        
    }
    
    private boolean save(HttpServletRequest request, int entityId, int brokerAccredID) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = getNeoUser(request);
        
        try {
            BrokerAccred ba = BrokerMapping.getAccreditationDetails(request, entityId, neoUser, brokerAccredID);
            String updateOrSave = request.getParameter("updateOrSave");
            port.saveBrokerFirmAccreditation(ba, updateOrSave);
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
        
    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser)request.getSession().getAttribute("persist_user");
    }
    
    private String getDateStr(XMLGregorianCalendar xgc) {
        try {
            Date dt = xgc.toGregorianCalendar().getTime();
            return DateTimeUtils.convertToYYYYMMDD(dt);
        } catch (java.lang.Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
    }
    


    @Override
    public String getName() {
        return "AddBrokerAccreditationCommand";
    }

   
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthTariffDetails;
import neo.manager.NeoUser;
import java.util.Formatter;

/**
 *
 * @author johanl
 */
public class AddAuthTariffToSessionCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest req, HttpServletResponse response, ServletContext context) {

        HttpSession session = req.getSession();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        String tariffDesc = "" + session.getAttribute("tariffDesc");
        //get tariff
        String tariffCode = "";
        String authPeriod = "" + session.getAttribute("authPeriod");

        double totalTariffAmount = 0.0d;

        if (authPeriod.equalsIgnoreCase("2010")) {
            tariffCode = req.getParameter("tariffCode");
        } else if (Integer.parseInt(authPeriod) >= 2011) {
            tariffCode = req.getParameter("tariffCode_text");
        }

        String tariffQuantity = req.getParameter("tariffQuantity");
        String tariffAmount = req.getParameter("tariffAmount");
        String tariffProvider = req.getParameter("provider_text");
        
        System.out.println("tariffProvider " + tariffProvider);
        String provType = "" + session.getAttribute("provType");
        String noTariff = "" + session.getAttribute("noTariffFound");

        int errorCount = 0;

        ArrayList<AuthTariffDetails> atList = null;
        ArrayList<AuthTariffDetails> sessionList = null;
        if (session.getAttribute("tariffListArray") != null) {
            sessionList = (ArrayList<AuthTariffDetails>) session.getAttribute("tariffListArray");
            if (sessionList != null && !sessionList.isEmpty()) {
                atList = sessionList;
            } else {
                atList = new ArrayList<AuthTariffDetails>();
            }
        } else {
            atList = new ArrayList<AuthTariffDetails>();
        }

        if (!noTariff.equalsIgnoreCase("null") && !noTariff.equalsIgnoreCase("null")) {
            if (noTariff.equalsIgnoreCase("true")) {
                errorCount++;
                System.out.println("noTariff found");
            }
        }

        AuthTariffDetails at = new AuthTariffDetails();

        //boolean nullFlag = false;
        if (!tariffDesc.trim().equalsIgnoreCase("") && !tariffDesc.trim().equalsIgnoreCase("null")) {
            at.setTariffDesc(tariffDesc);
        } else {
            errorCount++;
        }

        if (!tariffCode.trim().equalsIgnoreCase("") && !tariffCode.trim().equalsIgnoreCase("null")) {
            at.setTariffCode(tariffCode);
            session.removeAttribute("tariffCode_error");
        } else {
            errorCount++;
            session.setAttribute("tariffCode_error", "Tariff Code is Mandatory");
        }

        if (!tariffQuantity.trim().equalsIgnoreCase("") && !tariffQuantity.trim().equalsIgnoreCase("null")) {
            if (tariffQuantity.equalsIgnoreCase("0") || tariffQuantity.equalsIgnoreCase("0.0")
                    || tariffQuantity.equalsIgnoreCase("0.00")) {
                errorCount++;
                session.setAttribute("tariffQuantity_error", "Zero Tariff Quantity not Allowed");
            } else {
                boolean valid = checkValidity(tariffQuantity);
                if (valid == true) {
                    at.setQuantity(tariffQuantity);
                    session.removeAttribute("tariffQuantity_error");
                } else {
                    errorCount++;
                    session.setAttribute("tariffQuantity_error", "Invalid Tariff Quantity " + tariffQuantity);
                }
            }
        } else {
            errorCount++;
            session.setAttribute("tariffQuantity_error", "Empty Tariff Quantity not Allowed");
        }

        if (!tariffProvider.trim().equalsIgnoreCase("") && !tariffProvider.trim().equalsIgnoreCase("null")) {
            at.setProviderNum(tariffProvider);
            session.removeAttribute("treatingProvider_error");
        } else {
            errorCount++;
            session.setAttribute("treatingProvider_error", "Please enter a provider");
        }

        if (!tariffAmount.trim().equalsIgnoreCase("") && !tariffAmount.trim().equalsIgnoreCase("null")) {
            if (tariffAmount.equalsIgnoreCase("0") || tariffAmount.equalsIgnoreCase("0.0")
                    || tariffAmount.equalsIgnoreCase("0.00")) {
                errorCount++;
                session.setAttribute("tariffAmount_error", "Zero Tariff Amount not Allowed");
            } else {
                boolean valid = checkValidity(tariffAmount);
                if (valid == true) {
                    at.setAmount(Double.parseDouble(tariffAmount));
                    session.removeAttribute("tariffAmount_error");
                } else {
                    errorCount++;
                    session.setAttribute("tariffAmount_error", "Invalid Tariff Amount " + tariffAmount);
                }
            }

        } else {
            errorCount++;
            session.setAttribute("tariffAmount_error", "Empty Tariff Amount not Allowed");
        }

        System.out.println("errorCount for addTariff = " + errorCount);

        if (errorCount == 0) {
            //set user
            at.setUserId(user.getUserId());
            at.setTariffType("2");

            at.setProviderType(provType);
            atList.add(at);

            if (atList != null) {
                for (AuthTariffDetails tar : atList) {
                    totalTariffAmount += tar.getAmount();
                }
            }

            session.setAttribute("totTariffCost", totalTariffAmount);

            //add array to session
            session.setAttribute("tariffListArray", atList);
            //session.removeAttribute("provType");
            session.removeAttribute("tariffDesc");
            session.removeAttribute("tariffCode_text");
            session.removeAttribute("tariffCode");
            session.removeAttribute("tariffCode_error");

            session.removeAttribute("tariffQuantity");
            session.removeAttribute("tariffQuantity_error");

            session.removeAttribute("tariffAmount");
            session.removeAttribute("tariffAmount_error");

            session.removeAttribute("treatingProvider");
            session.removeAttribute("treatingProvider_error");
        }
        try {
            String nextJSP = "/PreAuth/AuthSpecificTariff.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(req, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "AddAuthTariffToSessionCommand";
    }

    private boolean checkValidity(String value) {
        boolean valid = true;
        Pattern p = Pattern.compile("[0-9]{1,10}([.][0-9]{1,2})?$");
        Matcher m = p.matcher(value);
        valid = m.matches();

        return valid;
    }

}

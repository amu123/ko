/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author johanl
 */
public class CalculateTariffQuantityCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;

        String tariffType = request.getParameter("tariffType");
        boolean intFlag = false;


        double tariffAmount = new Double("" + session.getAttribute(tariffType + "OriginalTA"));
        System.out.println("tariffType = " + tariffType + " tariffAmount = " + tariffAmount);
        String tq = "" + session.getAttribute(tariffType + "OriginalTQ");

        double tQuan = 0.0d;
        int tQuanInt = 0;
        try {
            tQuan = Double.parseDouble(tq);
        } catch (Exception ex) {
            intFlag = true;
            tQuanInt = Integer.parseInt(tq);
        }

        if (intFlag == false) {
            double tariffQuantity = new Double(request.getParameter("quantity"));
            double tAmount = 0.0d;
            double newTAmount = 0.0d;

            if (tQuan == 0.0d) {
                tQuan = 1.0d;
            }
            if (tariffAmount != 0.0d) {
                tAmount = tariffAmount / tQuan;
                if (tariffQuantity != 0.0d) {
                    newTAmount = tAmount * tariffQuantity;
                }
            }
            session.setAttribute("tariffQuantity", tariffQuantity);
            session.setAttribute("tariffAmount", newTAmount);

            try {
                out = response.getWriter();
                out.print(getName() + "|" + tariffQuantity + "|" + newTAmount);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            int tariffQuantity = new Integer(request.getParameter("quantity"));
            double tAmount = 0.0d;
            double newTAmount = 0.0d;

            if (tQuanInt == 0) {
                tQuanInt = 1;
            }
            if (tariffAmount != 0.0d) {
                tAmount = tariffAmount / tQuanInt;
                if (tariffQuantity != 0) {
                    newTAmount = tAmount * tariffQuantity;
                }
            }
            System.out.println("tariffQuantity = " + tariffQuantity);
            System.out.println("tariffAmount = " + tariffAmount);
            session.setAttribute("tariffQuantity" + tariffType, tariffQuantity);
            session.setAttribute("tariffAmount" + tariffType, newTAmount);

            try {
                out = response.getWriter();
                out.print(getName() + "|" + tariffQuantity + "|" + newTAmount);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        return null;
    }

    @Override
    public String getName() {
        return "CalculateTariffQuantityCommand";
    }
}

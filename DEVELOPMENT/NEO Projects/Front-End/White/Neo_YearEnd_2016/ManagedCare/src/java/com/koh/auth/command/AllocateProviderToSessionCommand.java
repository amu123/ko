/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.command;

import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author gerritj
 */
public class AllocateProviderToSessionCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        String forField = ""+ session.getAttribute("searchCalled");
        String onScreen = ""+ session.getAttribute("onScreen");

        session.setAttribute(forField + "_text", request.getParameter("providerNumber"));
        session.setAttribute(forField, request.getParameter("providerName"));
        try {
            String nextJSP = onScreen;
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "AllocateProviderToSessionCommand";
    }
}

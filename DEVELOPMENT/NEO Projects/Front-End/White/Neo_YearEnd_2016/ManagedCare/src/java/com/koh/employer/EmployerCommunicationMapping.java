/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.employer;

import com.koh.employer.command.TabUtils;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import neo.manager.EmployerComm;
import neo.manager.NeoUser;

/**
 *
 * @author yuganp
 */
public class EmployerCommunicationMapping {
    private static final String[][] FIELD_MAPPINGS = {
        {"EmployerGroup_entityId","","no", "int", "EmployerCommunication","employerEntityId"},
        {"EmployerCommunication_memberCards","Membership Cards","no", "int", "EmployerCommunication","membershipCardsInd"},
        {"EmployerCommunication_communicateDirectly","Communicate Directly with Members","no", "int", "EmployerCommunication","CommunicateWithMembersInd"},
        {"EmployerCommunication_communicationMethod","Communication Method","no", "int", "EmployerCommunication","communicationMethod"},
        {"EmployerCommunication_communicationOther","Other Details","no", "str", "EmployerCommunication","OtherCommunication"},
        {"EmployerCommunication_contactPerson","Contact Person","no", "str", "EmployerCommunication","ContactPerson"},
        {"EmployerCommunication_contactNo","Contact no.","no", "str", "EmployerCommunication","ContactNo"},
        {"EmployerCommunication_contactEmail","Contact Email","no", "str", "EmployerCommunication","ContactEmail"}
    };
    
    private static final Map<String,String[]> FIELD_MAP;

    static {
        HashMap hm = new HashMap<String,String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }
    
    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
        return errors;
    }
    
    public static EmployerComm getEmployerComm(HttpServletRequest request,NeoUser neoUser) {
        EmployerComm eg = (EmployerComm)TabUtils.getDTOFromRequest(request, EmployerComm.class, FIELD_MAPPINGS, "EmployerCommunication");
        eg.setSecurityGroupId(neoUser.getSecurityGroupId());
        eg.setLastUpdatedBy(neoUser.getUserId());
        return eg;
    }

    public static void setEmployerComm(HttpServletRequest request, EmployerComm employerComm) {
        TabUtils.setRequestFromDTO(request, employerComm, FIELD_MAPPINGS, "EmployerCommunication");
    }

    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.tags;

import com.koh.command.NeoCommand;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CcClaimLine;
import neo.manager.CcClaimLineSearchResult;
import neo.manager.ClaimCCSearchCriteria;
import neo.manager.NeoManagerBean;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Johan-NB
 */
public class MemberBenefitClaimLineDetailsTable extends TagSupport {

    private static final Logger logger = Logger.getLogger(MemberClaimLineDetailsTable.class);

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

        System.out.println("entered MemberBenefitClaimLineDetailsTable");

        //Claim selectedClaim = (Claim) session.getAttribute("selected");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        String serviceDateFrom = "";
        String paymentDaye = "";
        String paymentDate = "";

        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");

            List<CcClaimLine> claimLineList = (List<CcClaimLine>) session.getAttribute("memBenClaimClaimLines");

            if (claimLineList != null && claimLineList.isEmpty() == false) {
                boolean addForwardNav = false;
                boolean addBackwardNav = false;

                ClaimCCSearchCriteria claimLineResults = (ClaimCCSearchCriteria) session.getAttribute("memBenClaimSearch");
                int claimLineCount = claimLineList.size();
                int totalBenClaims = Integer.parseInt("" + session.getAttribute("totalMemBenClaims"));

                int resultMin = claimLineResults.getResultCountMin();
                int resultMax = claimLineResults.getResultCountMax();

                logger.info("MemberBenefitClaimLineDetailsTable claimLineCount = " + claimLineCount);
                logger.info("MemberBenefitClaimLineDetailsTable resultMin = " + resultMin);
                logger.info("MemberBenefitClaimLineDetailsTable resultMax = " + resultMax);

                if (claimLineCount == 100 && resultMin == 0) {
                    addForwardNav = true;

                } else if (claimLineCount == 100 && resultMin > 0) {
                    addForwardNav = true;
                    addBackwardNav = true;

                } else if (claimLineCount < 100 && resultMin > 0) {
                    addBackwardNav = true;
                }

                out.println("<table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.print("<tr>");
                out.print("<th align=\"left\">Claim Number</th>");
                out.print("<th align=\"left\">ClaimLine Number</th>");
                out.print("<th align=\"left\">Dependant Number</th>");
                out.print("<th align=\"left\">Dependant Name</th>");
                out.print("<th align=\"left\">Practice Number</th>");
                out.print("<th align=\"left\">Practice Name</th>");
                out.print("<th align=\"left\">Pay Status</th>");
                out.print("<th align=\"left\">Treatment From</th>");
                out.print("<th align=\"left\">Payment Date</th>");
                out.print("<th align=\"left\">Amount Claimed</th>");
                out.print("<th align=\"left\">Amount Paid</th>");
                out.print("<th align=\"left\"></th>");
                out.print("</tr>");

                for (CcClaimLine claimLine : claimLineList) {

                    out.print("<tr valign=\"top\">");

                    out.print("<td><label class=\"label\">" + claimLine.getClaimId() + "</label></td>");
                    out.print("<td><label class=\"label\">" + claimLine.getClaimLineId() + "</label></td>");

                    //dependant
                    out.print("<td><label class=\"label\">" + claimLine.getDependantCode() + "</label></td>");
                    out.print("<td width=\"100\"><label class=\"label\">" + claimLine.getDependantName() + "</label></td>");                    
                    
                    //service provider 
                    //out.print("<td><label class=\"label\">" + claimLine.get + "</label></td>");
                    out.print("<td><label class=\"label\">" + claimLine.getPracticeNo() + "</label></td>");
                    out.print("<td width=\"100\"><label class=\"label\">" + claimLine.getPracticeName() + "</label></td>");

                    //paystatus validation
                    Integer payStatusId = claimLine.getClaimLineStatusId();
                    if (payStatusId != 0) {
                        String payStatus = port.getValueForId(83, payStatusId.toString());
                        out.print("<td><label class=\"label\">" + payStatus + "</label></td>");
                    } else {
                        out.print("<td></td>");
                    }

                    if (claimLine.getTreatmentFromDate() != null) {
                        out.print("<td><label class=\"label\">" + formatter.format(claimLine.getTreatmentFromDate().toGregorianCalendar().getTime()) + "</label></td>");
                    } else {
                        out.print("<td></td>");
                    }


                    if (claimLine.getChequeRunDate() != null) {
                        paymentDate = formatter.format(claimLine.getChequeRunDate().toGregorianCalendar().getTime());
                        out.print("<td><label class=\"label\">" + paymentDate + "</label></td>");
                    } else {
                        out.print("<td></td>");
                    }

                    out.print("<td><label class=\"label\">" + new DecimalFormat("#####0.00").format(claimLine.getClaimedAmount()) + "</label></td>");
                    out.print("<td><label class=\"label\">" + new DecimalFormat("#####0.00").format(claimLine.getPaidAmount()) + "</label></td>");

                    out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('ForwardToMemberClaimLineClaimLine', '" + claimLine.getClaimLineId() + "', '"+ claimLine.getPaidAmount() +"', '"+ claimLine.getDependantCode() +"')\"; >Details</button></td>");

                    out.print("</tr>");


                }
                //set navigation buttons
                if (addBackwardNav == true || addForwardNav == true) {
                    out.println("<tr><th colspan=\"12\" align=\"right\" >");

                    if (addBackwardNav) {
                        out.println("<button name=\"opperation\" type=\"button\" onClick=\"submitPageAction('ViewMemberClaimsBenefitsCommand', 'backward')\"; value=\"\">Previous</button>");
                    }
                    if (addForwardNav) {
                        out.println("<button name=\"opperation\" type=\"button\" onClick=\"submitPageAction('ViewMemberClaimsBenefitsCommand', 'forward')\"; value=\"\">Next</button>");
                    }


                    out.println("</th></tr></table>");
                } else {
                    out.println("</table>");
                }


            } else {
                System.out.println("claim line error");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return super.doEndTag();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.auth.command;

import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.DentalCode;
import neo.manager.NeoManagerBean;

/**
 *
 * @author whauger
 */
public class SearchDentalCodeCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("in SearchDentalCodeCommand");
        String discipline = request.getParameter("disci");
        String dentalCode = request.getParameter("dental");

        List<DentalCode> dentalProtoList = new ArrayList<DentalCode>();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        try {
            dentalProtoList = port.findDentalTreatmentForCodeOrDiscipline(discipline, dentalCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.setAttribute("searchDentalCodeResult", dentalProtoList);
        try {
            String nextJSP = "/Auth/DentalCodeSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "SearchDentalCodeCommand";
    }
}

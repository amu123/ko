/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import com.koh.cover.MemberMainMapping;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;

/**
 *
 * @author princes
 */
public class MemberNotesCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            NeoManagerBean port = service.getNeoManagerBeanPort();
            String s = request.getParameter("buttonPressed");
            if (s != null && !s.isEmpty()) {
                System.out.println("buttonPressed : " + s);
                if (s.equalsIgnoreCase("Add Note")) {
                    addDetails(port, request, response, context);
                } else if (s.equalsIgnoreCase("SaveButton")) {
                    saveDetails(port, request, response, context);
                } else if (s.equalsIgnoreCase("SelectButton")) {
                    showSearch(port, request, response, context);
                } else if (s.equalsIgnoreCase("ViewButton")) {
                    viewNote(port, request, response, context);
                } else if (s.equalsIgnoreCase("ViewButton2")) {
                    viewNoteCC(port, request, response, context);
                } else if (s.equalsIgnoreCase("SearchNotesButton")) {
                    getNotesList(port, request, response, context);
                } else if (s.equalsIgnoreCase("SearchNotesButton2")) {
                    getNotesListCC(port, request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(MemberApplicationHospitalCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void addDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        request.setAttribute("noteId", null);
        context.getRequestDispatcher("/Member/MemberAppNotesAdd.jsp").forward(request, response);
    }

    private void viewNote(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }

        int noteId = getStrToInt(request.getParameter("noteId"));
        String noteType = request.getParameter("noteType");
        //        String s = port.fetchMember

        if (noteType.equals("1")){
        EntityNotes memberAppNoteByNoteId = port.fetchEmployerAppNoteByNoteId(noteId);
        request.setAttribute("noteDetails", memberAppNoteByNoteId.getNoteDetails());
//        MemberAppNotesMapping.setMemberAppNotes(request, memberAppNoteByNoteId);
        }else{
            CallTrackDetails call = port.getCallByTrackID(noteId);
            request.setAttribute("noteDetails", call.getNotes());
        }
        context.getRequestDispatcher("/Member/MemberAppNotesAdd.jsp").forward(request, response);
    }
    
    private void viewNoteCC(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
        }

        int noteId = getStrToInt(request.getParameter("noteId"));

        EntityNotes memberAppNoteByNoteId = port.fetchEmployerAppNoteByNoteId(noteId);
        request.setAttribute("noteDetails", memberAppNoteByNoteId.getNoteDetails());

        context.getRequestDispatcher("/Claims/MemberNoteDetails.jsp").forward(request, response);
    }

    private void saveDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = MemberMainMapping.validateNotes(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        System.out.println("[fr]Errors = " + errors);
        System.out.println("[fr]Status = " + status);
        if ("OK".equalsIgnoreCase(status)) {
            if (saveDetails(port, request)) {
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving.\"";
            }
        } else {
            additionalData = null;
        }
        if ("OK".equalsIgnoreCase(status)) {
            getNotesList(port, request, response, context);
        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }


    }

    private boolean saveDetails(NeoManagerBean port, HttpServletRequest request) {
        NeoUser neoUser = (NeoUser) request.getSession().getAttribute("persist_user");
        try {
            EntityNotes app = MemberMainMapping.getBrokerAppNotes(request, neoUser);
            app.setNoteDetails(request.getParameter("noteDetails"));
            port.saveEmployerAppNotes(app);
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
    }

    private void showSearch(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        context.getRequestDispatcher("/Member/MemberNotesDetails.jsp").forward(request, response);
    }

    private void getNotesList(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        int memberEntityId = getIntParam(request, "memberEntityId");
        int noteNum = getIntParam(request, "notesListSelect");
        String coverNumber = (String) request.getSession().getAttribute("coverNumber");
        request.setAttribute("notesListSelect", request.getParameter("notesListSelect"));
        request.setAttribute("note_start_date", request.getParameter("note_start_date"));
        request.setAttribute("note_end_date", request.getParameter("note_end_date"));
        HttpSession session = request.getSession();

        Date startDate = null;
        Date endDate = null;
        if (request.getParameter("note_start_date") != null && !request.getParameter("note_start_date").isEmpty()) {
            startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("note_start_date"));
            session.setAttribute("note_start_date", request.getParameter("note_start_date"));
        } else if (session.getAttribute("note_start_date") != null) {
            String sDate = (String) session.getAttribute("note_start_date");
            startDate = DateTimeUtils.convertFromYYYYMMDD(sDate);
        }
        if (request.getParameter("note_end_date") != null && !request.getParameter("note_end_date").isEmpty()) {
            endDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("note_end_date"));
            session.setAttribute("note_end_date", request.getParameter("note_end_date"));
        } else if (session.getAttribute("note_end_date") != null) {
            String eDate = (String) session.getAttribute("note_end_date");
            endDate = DateTimeUtils.convertFromYYYYMMDD(eDate);
        }
        
        GregorianCalendar sDate = new GregorianCalendar();        
    	sDate.setTime(new Date());
    	sDate.add(Calendar.MONTH, -6);
    	Date dateFrom = sDate.getTime();
        
        GregorianCalendar eDate = new GregorianCalendar();
    	eDate.setTime(new Date());
    	Date dateTo = eDate.getTime();

        /*System.err.println("memberEntityId " + memberEntityId);
        System.err.println("noteNum " + noteNum);
        System.err.println("contrib_start_date " + startDate + " from request " + request.getParameter("note_start_date"));
        System.err.println("contrib_end_date " + endDate + " from request " + request.getParameter("note_end_date"));
        System.err.println("notesListSelect " + request.getParameter("notesListSelect"));*/
        
        if (startDate == null){ 
            startDate = dateFrom;
        }
    
        if (endDate == null){
            endDate = dateTo;
        }
        
        if (noteNum == 99){                                               
        } else {
            //List<KeyValueArray> kva = port.fetchNotesList(200,memberEntityId, noteNum,7);
            //List<KeyValueArray> kva = port.fetchNotesListByDate(200, memberEntityId, noteNum, 7, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
            List<KeyValueArray> kva = port.fetchNotesListByDateForMember(257, memberEntityId,coverNumber, noteNum, 8, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
            Object col = MapUtils.getMap(kva);            
            request.setAttribute("MemberNoteDetails", col);

        }
        context.getRequestDispatcher("/Member/MemberNotesDetails.jsp").forward(request, response);
    }
    
    private void getNotesListCC(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        int memberEntityId = getIntParam(request, "memberEntityId");
        int noteNum = getIntParam(request, "notesListSelect");
        request.setAttribute("notesListSelect", request.getParameter("notesListSelect"));
        request.setAttribute("note_start_date", request.getParameter("note_start_date"));
        request.setAttribute("note_end_date", request.getParameter("note_end_date"));
        HttpSession session = request.getSession();

        Date startDate = null;
        Date endDate = null;
        if (request.getParameter("note_start_date") != null && !request.getParameter("note_start_date").isEmpty()) {
            startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("note_start_date"));
            session.setAttribute("note_start_date", request.getParameter("note_start_date"));
        } else if (session.getAttribute("note_start_date") != null) {
            String sDate = (String) session.getAttribute("note_start_date");
            startDate = DateTimeUtils.convertFromYYYYMMDD(sDate);
        }
        if (request.getParameter("note_end_date") != null && !request.getParameter("note_end_date").isEmpty()) {
            endDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("note_end_date"));
            session.setAttribute("note_end_date", request.getParameter("note_end_date"));
        } else if (session.getAttribute("note_end_date") != null) {
            String eDate = (String) session.getAttribute("note_end_date");
            endDate = DateTimeUtils.convertFromYYYYMMDD(eDate);
        }
        
        GregorianCalendar sDate = new GregorianCalendar();        
    	sDate.setTime(new Date());
    	sDate.add(Calendar.MONTH, -6);
    	Date dateFrom = sDate.getTime();
        
        GregorianCalendar eDate = new GregorianCalendar();
    	eDate.setTime(new Date());
    	Date dateTo = eDate.getTime();
        
        if (startDate == null){ 
            startDate = dateFrom;
        }
    
        if (endDate == null){
            endDate = dateTo;
        }

        if (noteNum == 99 ){
        } else {
            List<KeyValueArray> kva = port.fetchNotesListByDate(200, memberEntityId, noteNum, 7, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
            Object col = MapUtils.getMap(kva);
            session.setAttribute("MemberNoteDetails", col);

        }
        context.getRequestDispatcher("/Claims/MemberNoteDetails.jsp").forward(request, response);
    }

    private int getIntParam(HttpServletRequest request, String paramName) {
        try {
            String maxDepStr = request.getParameter(paramName);
            return Integer.parseInt(maxDepStr);
        } catch (java.lang.Exception e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser) request.getSession().getAttribute("persist_user");
    }

    private int getStrToInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (java.lang.Exception e) {
            System.out.println(s + " = " + e.getMessage());
        }
        return 0;
    }

    @Override
    public String getName() {
        return "MemberNotesCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.employer.command;

import com.koh.command.NeoCommand;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoManagerBean;

/**
 *
 * @author yuganp
 */
public class EmployerBranchCommand extends NeoCommand{
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        try {
            String entityId = request.getParameter("entityId");
            String branchId = request.getParameter("branchId"); 
            String branchName = request.getParameter("branchName"); 
            String branchNumber = request.getParameter("branchNumber"); 
            request.setAttribute("parentEntityId", entityId);
            request.setAttribute("employerEntityId", branchId);
            request.setAttribute("employer_header_employerName", branchName);
            request.setAttribute("employer_header_employerNumber", branchNumber);
            request.setAttribute("Action", "BranchEdit");
            context.getRequestDispatcher("/Employer/Employer.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(EmployerSearchCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "EmployerBranchCommand";
    }
    
}

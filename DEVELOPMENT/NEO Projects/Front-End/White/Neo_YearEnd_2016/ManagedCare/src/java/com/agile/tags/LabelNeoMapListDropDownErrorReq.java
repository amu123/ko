/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import com.koh.command.NeoCommand;
import com.koh.utils.LookupUtils;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.LookupValue;
import neo.manager.NeoUser;

/**
 *
 * @author johanl
 */
public class LabelNeoMapListDropDownErrorReq extends TagSupport {

    private String displayName;
    private String elementName;
    private String mapList;
    private String javaScript;
    private String mandatory = "no";
    private String errorValueFromSession = "no";
    private String firstIndexSelected = "yes";

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        
        try {
            out.println("<td><label class=\"label\" for=\"" + elementName + "\">" + displayName + ":</label></td>");
            out.println("<td><select style=\"width:215px\" name=\"" + elementName + "\" id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");
            String selectedBox = "" + pageContext.getRequest().getAttribute(elementName);
            //Neo Lookups
            List<Map> lookUps = (List<Map>)pageContext.getRequest().getAttribute(mapList);
            out.println("<option value=\"\"></option>");
            boolean firstIndexSet = false;
            if (lookUps == null || lookUps.isEmpty()) {
                 
            } else {
            for (int i = 0; i < lookUps.size(); i++) {
                Map map = lookUps.get(i);
                String id = (String) map.get("id");
                String value = (String) map.get("value");
                if (id != null && id.equalsIgnoreCase(selectedBox)) {
                    out.println("<option value=\"" + id + "\" selected >" + value + "</option>");
                } else {
                    if(firstIndexSelected.equalsIgnoreCase("yes") && selectedBox.isEmpty()){
                        if(firstIndexSet){
                            out.println("<option value=\"" + id + "\" >" + value + "</option>");
                        }else{
                            out.println("<option value=\"" + id + "\" selected >" + value + "</option>");
                            firstIndexSet = true;
                            System.out.println("found first index");
                        }
                    }else{
                        out.println("<option value=\"" + id + "\" >" + value + "</option>");
                    }
                }
            }
            }
            out.println("</select></td>");
            if (mandatory.equalsIgnoreCase("yes")) {
                out.println("<td><label class=\"red\">*</label></td>");
            } else {
                out.println("<td></td>");
            }
                       
            String sessionValerror = "";
            if (errorValueFromSession.equalsIgnoreCase("yes")) {

                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                }

                if (sessionValerror != null && !sessionValerror.equalsIgnoreCase("null")) {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
                } else {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
                }
            }

            /*
            if(errorValueFromSession.equalsIgnoreCase("yes")){
                String errorVal = "" + session.getAttribute(elementName + "_error");
                if(errorVal == null || errorVal.trim().equalsIgnoreCase("null")){
                    errorVal = "";
                }
                out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">"+ errorVal +"</label></td>");
            }else{
                out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
            }*/

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelMapListValueDropDownErrorReq tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setErrorValueFromSession(String errorValueFromSession) {
        this.errorValueFromSession = errorValueFromSession;
    }

    public void setFirstIndexSelected(String firstIndexSelected) {
        this.firstIndexSelected = firstIndexSelected;
    }

    public void setMapList(String mapList) {
        this.mapList = mapList;
    }

}

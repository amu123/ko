/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Princes
 */
public class ReturnToClaimHistoryCommand extends NeoCommand {

    private static final Logger logger = Logger.getLogger(ReturnToClaimHistoryCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();

        String onScreen = request.getParameter("onScreen");
        String previousJsp = null;

        System.out.println("On Screen : " + onScreen);

        if (onScreen != null && !onScreen.equalsIgnoreCase("")) {

            if (onScreen.equalsIgnoreCase("MemberHistoryClaims.jsp")) {

                previousJsp = "/PDC/ClaimsHistory.jsp";

            } else if (onScreen.equalsIgnoreCase("MemberDetailedClaimLine.jsp")) {
                previousJsp = "/PDC/MemberClaimsLineHistory.jsp";

            }

            try {
                RequestDispatcher dispatcher = context.getRequestDispatcher(previousJsp);
                dispatcher.forward(request, response);

            } catch (ServletException ex) {
                logger.log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "ReturnToClaimHistoryCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.BiometricsId;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.OtherBiometrics;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class CaptureOtherCommand extends NeoCommand {

    private Object _datatypeFactory;
    private String nextJSP;
    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();

        boolean checkError = false;
        logger.info("------------In CaptureOtherCommand---------------------");
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        NeoManagerBean port = service.getNeoManagerBeanPort();

        OtherBiometrics other = new OtherBiometrics();

        //String coverNumber = request.getParameter("memberNumber_text");
        String coverNumber = (String) session.getAttribute("memberNumber_text");
        //System.out.println("@other memberNumber_text: " + coverNumber);
        if (coverNumber != null && !coverNumber.equalsIgnoreCase("")) {

            other.setMemberNumber(coverNumber);
            session.setAttribute("memberNumber_text", coverNumber);
        } else {

            session.setAttribute("memberNumber_error", "The member field should be filled in");
            checkError = true;

        }
        
        //String treatingProvider = request.getParameter("providerNumber_text");
        String treatingProvider = (String) session.getAttribute("providerNumber_text");
        //System.out.println("@other providerNumber_text: " + treatingProvider);
        if (treatingProvider != null && !treatingProvider.equalsIgnoreCase("")) {

            other.setTreatingProvider(treatingProvider);
            session.setAttribute("providerNumber_text", treatingProvider);
        } 
//        else {
//
//            session.setAttribute("testingProvider_error", "The provider field should be filled in");
//            checkError = true;
//
//        }

        //////////////////////////NEW//////////////////////////////////////
        //String exerciseInWeek = request.getParameter("exerciseQuestion");
        String exerciseInWeek = (String) session.getAttribute("exerciseQuestion_text");
        //System.out.println("@other exerciseQuestion_text: " + exerciseInWeek);
        if (exerciseInWeek != null && !exerciseInWeek.equalsIgnoreCase("")) {
            if (exerciseInWeek.matches("[0-9]+")) {
                int exerciseInWeekValue = Integer.parseInt(exerciseInWeek);
                other.setExercisePerWeek(exerciseInWeekValue);
            } else {

                session.setAttribute("exerciseQuestion_error", "The exercise question should be a numeric value");
                checkError = true;
            }
        } 
//        else {
//
//            session.setAttribute("exerciseQuestion_error", "The exercise question should be filled in");
//            checkError = true;
//        }

        //String currentSmoker = request.getParameter("currentSmoker");
        String currentSmoker = (String) session.getAttribute("currentSmoker_text");
        //System.out.println("@other currentSmoker_text: " + currentSmoker);
        if (currentSmoker != null && !currentSmoker.equalsIgnoreCase("99")) {
            other.setCurrentSmoker(currentSmoker);
        } 
//        else {
//
//            session.setAttribute("currentSmoker_error", "The smoker value should be filled in");
//            checkError = true;
//        }

        //String numberOfCigaretes = request.getParameter("smokingQuestion");
        String numberOfCigaretes = (String) session.getAttribute("smokingQuestion_text");
        //System.out.println("@other smokingQuestion_text: " + numberOfCigaretes);
        if (numberOfCigaretes != null && !numberOfCigaretes.equalsIgnoreCase("")) {
            if (numberOfCigaretes.matches("[0-9]+")) {
                int numberOfCigaretesValue = Integer.parseInt(numberOfCigaretes);
                other.setCigarettesPerDay(numberOfCigaretesValue);
            } else {

                session.setAttribute("smokingQuestion_error", "The smoking question should be a numeric value");
                checkError = true;
            }
        } 
//        else {
//
//            session.setAttribute("smokingQuestion_error", "The smoking question should be filled in");
//            checkError = true;
//        }

        //String exSmoker = request.getParameter("exSmoker");
        String exSmoker = (String) session.getAttribute("exSmoker_text");
        //System.out.println("@other exSmoker_text: " + exSmoker);
        if (exSmoker != null && !exSmoker.equalsIgnoreCase("99")) {
            other.setExSmoker(exSmoker);
        } 
//        else {
//
//            session.setAttribute("exSmoker_error", "The ex smoker value should be filled in");
//            checkError = true;
//        }

        //String yearsSinceSmoking = request.getParameter("stopped");
        String yearsSinceSmoking = (String) session.getAttribute("stopped_text");
        //System.out.println("@other stopped_text: " + yearsSinceSmoking);
        if (yearsSinceSmoking != null && !yearsSinceSmoking.equalsIgnoreCase("")) {
            int yearsSinceSmokingValue = Integer.parseInt(yearsSinceSmoking);
            other.setYearsSinceStopped(yearsSinceSmokingValue);
        }

        //String alcoholUnits = request.getParameter("alcoholConsumption");
        String alcoholUnits = (String) session.getAttribute("alcoholConsumption_text");
        //System.out.println("@other alcoholConsumption_text: " + alcoholUnits);
        if (alcoholUnits != null && !alcoholUnits.equalsIgnoreCase("")) {
            if (alcoholUnits.matches("[0-9]+")) {
                int alcoholUnitsValue = Integer.parseInt(alcoholUnits);
                other.setAlcoholUnitsPerWeek(alcoholUnitsValue);
            } else {

                session.setAttribute("alcoholConsumption_error", "The alcohol consumption should be a number");
                checkError = true;
            }
        } 
//        else {
//
//            session.setAttribute("alcoholConsumption_error", "The alcohol consumption should be filled in");
//            checkError = true;
//        }

        //String weight = request.getParameter("weight");
        String weight = (String) session.getAttribute("weight_text");
        //String height = request.getParameter("length");
        String height = (String) session.getAttribute("length_text");
        if (weight != null && !weight.equalsIgnoreCase("")) {
            if (weight.matches("([0-9]+(\\.[0-9]+)?)+")) {
                if (height != null && !height.equalsIgnoreCase("")) {
                    if (height.matches("([0-9]+(\\.[0-9]+)?)+")) {
                        double heightValue = Double.parseDouble(height);
                        double weightValue = Double.parseDouble(weight);
                        other.setHeight(heightValue);
                        other.setWeight(weightValue);
                    } else {
                        session.setAttribute("length_error", "The length should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("length_error", "Require length to calculate BMI");
                    checkError = true;
                }
            } else {

                session.setAttribute("weight_error", "The weight should be a numeric value");
                checkError = true;
            }
        }/*else {

         session.setAttribute("weight_error", "The weight should be filled in");
         checkError = true;
         }*/

        if (height != null && !height.equalsIgnoreCase("")) {
            if (height.matches("([0-9]+(\\.[0-9]+)?)+")) {
                if (weight != null && !weight.equalsIgnoreCase("")) {
                    if (weight.matches("([0-9]+(\\.[0-9]+)?)+")) {
                        double heightValue = Double.parseDouble(height);
                        double weightValue = Double.parseDouble(weight);
                        other.setHeight(heightValue);
                        other.setWeight(weightValue);
                    } else {
                        session.setAttribute("weight_error", "The weight should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("weight_error", "Require weight to calculate BMI");
                    checkError = true;
                }
            } else {

                session.setAttribute("length_error", "The length should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("length_error", "The length should be filled in");
         checkError = true;
         }*/
        
        //String bmi = request.getParameter("bmi");
        String bmi = (String) session.getAttribute("bmi_text");
        if (bmi != null && !bmi.equalsIgnoreCase("")) {
            if (bmi.matches("([0-9]+(\\.[0-9]+)?)+")) {
                double bmiValue = Double.parseDouble(bmi);
                other.setBmi(bmiValue);
            } else {

                session.setAttribute("bmi_error", "The BMI should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bmi_error", "The BMI should be filled in");
         checkError = true;
         }*/


        //String bloodPresureSystolic = request.getParameter("bpSystolic");
        String bloodPresureSystolic = (String) session.getAttribute("bpSystolic_text");
        //String bloodPressureDiastolic = request.getParameter("bpDiastolic");
        String bloodPressureDiastolic = (String) session.getAttribute("bpDiastolic_text");
        if (bloodPresureSystolic != null && !bloodPresureSystolic.equalsIgnoreCase("")) {
            if (bloodPresureSystolic.matches("[0-9]+")) {
                if (bloodPressureDiastolic != null && !bloodPressureDiastolic.equalsIgnoreCase("")) {
                    if (bloodPressureDiastolic.matches("[0-9]+")) {
                        int bloodvalueSys = Integer.parseInt(bloodPresureSystolic);
                        int bloodValueDia = Integer.parseInt(bloodPressureDiastolic);
                        other.setBloodPressureSystolic(bloodvalueSys);
                        other.setBloodPressureDiastolic(bloodValueDia);
                    } else {

                        session.setAttribute("bpDiastolic_error", "The blood pressure should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("bpDiastolic_error", "The blood pressure should be filled in");
                    checkError = true;
                }
            } else {

                session.setAttribute("bpSystolic_error", "The blood pressure should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bpSystolic_error", "The blood pressure should be filled in");
         checkError = true;
         }*/

        if (bloodPressureDiastolic != null && !bloodPressureDiastolic.equalsIgnoreCase("")) {
            if (bloodPressureDiastolic.matches("[0-9]+")) {
                if (bloodPresureSystolic != null && !bloodPresureSystolic.equalsIgnoreCase("")) {
                    if (bloodPresureSystolic.matches("[0-9]+")) {
                        int bloodvalueSys = Integer.parseInt(bloodPresureSystolic);
                        int bloodValueDia = Integer.parseInt(bloodPressureDiastolic);
                        other.setBloodPressureSystolic(bloodvalueSys);
                        other.setBloodPressureDiastolic(bloodValueDia);
                    } else {

                        session.setAttribute("bpSystolic_error", "The blood pressure should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("bpSystolic_error", "The blood pressure should be filled in");
                    checkError = true;
                }
            } else {

                session.setAttribute("bpDiastolic_error", "The blood pressure should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bpDiastolic_error", "The blood pressure should be filled in");
         checkError = true;
         }*/
        
        /////////////////////////////NEW////////////////////////////////////

        //String cdl = request.getParameter("cdl");
        String cdl = (String) session.getAttribute("otherlookupValue");
        //System.out.println("@other cdl: " + cdl);
        if (cdl != null && !cdl.equalsIgnoreCase("")) {

            other.setCdl(cdl);
            session.setAttribute("cdl", cdl);
        } 
//        else {
//
//            session.setAttribute("cdl_error", "The CDL field should be filled in");
//            checkError = true;
//
//        }

        //String source = request.getParameter("source");
        String source = (String) session.getAttribute("source_text");
        //System.out.println("@other source_text: " + source);
        if (source != null && !source.equalsIgnoreCase("99")) {

            other.setSource(source);
        } 
//        else {
//
//            session.setAttribute("source_error", "The source should be filled in");
//            checkError = true;
//
//        }
        
//        String testResult = request.getParameter("testResults");
//        if (testResult != null && !testResult.equalsIgnoreCase("")) {
//            double test = Double.parseDouble(testResult);
//            other.setTestResult(test);
//        } else {
//
//            session.setAttribute("testResults_error", "At least one field should be filled in");
//            checkError = true;
//
//        }
        
        //String icd10 = request.getParameter("icd10code");
        String icd10 = (String) session.getAttribute("othericd");
        //System.out.println("@other otherlookupValue: " + icd10);
        if (icd10 != null && !icd10.equalsIgnoreCase("")) {
            other.setIcd10(icd10);
        } else {
            session.setAttribute("icd10_error", "The icd code should be filled in");
            checkError = true;
            logger.info("CHECKERROR SET 6");
        }
        
        String icd10TypeId = String.valueOf(session.getAttribute("icdTypeID"));
        //System.out.println("@other otherlookupValue: " + icd10);
        if (icd10TypeId != null && !icd10TypeId.equalsIgnoreCase("")) {
            other.setIcd10TypeID(icd10TypeId);
        }

        //String dependantCode = request.getParameter("depListValues");
        String dependantCode = (String) session.getAttribute("depListValues_text");
        //System.out.println("@other depListValues_text: " + dependantCode);
        if (dependantCode != null && !dependantCode.equalsIgnoreCase("") && !dependantCode.equalsIgnoreCase("99") && !dependantCode.equalsIgnoreCase("null")) {

            other.setDependantCode(new Integer(dependantCode));
        } else {

            session.setAttribute("depListValues_error", "dependants should be filled in");
            checkError = true;

        }
        
        //String detail = request.getParameter("detail");
        String detail = (String) session.getAttribute("otherDetails_text");
        //System.out.println("@other otherDetails_text: " + detail);
        if (detail != null && !detail.equalsIgnoreCase("")) {

            other.setDetail(detail);
            session.removeAttribute("otherDetails_text");
        } 
//        else {
//
//            session.setAttribute("detail_error", "the value should be filled in");
//            checkError = true;
//
//        }

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        try {
            //String receivedDate = request.getParameter("dateReceived");
            String receivedDate = (String) session.getAttribute("dateReceived_text");
            //System.out.println("@other dateReceived_text: " + receivedDate);
            if (receivedDate != null && !receivedDate.equalsIgnoreCase("")) {
//                Date dateReceived = dateFormat.parse(receivedDate);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                Pattern pattern = Pattern.compile("\\s");
                Matcher matcher = pattern.matcher(receivedDate);
                boolean found = matcher.find();

                Date tFrom = null;
                XMLGregorianCalendar xTreatFrom = null;

                if (!found) {
                    tFrom = dateFormat.parse(receivedDate);
                } else {
                    tFrom = dateTimeFormat.parse(receivedDate);
                }

                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                other.setDateReceived(xTreatFrom);
            } else {
                session.setAttribute("dateReceived_error", "The date should be filled in");
                checkError = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String UpdateBiometrics = (String) session.getAttribute("UpdateBiometrics");
//        if (UpdateBiometrics != null && UpdateBiometrics.equals("true")) {
//            dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd");
//        }
        try {
            //String dateMeasured = request.getParameter("dateMeasured");
            String dateMeasured = (String) session.getAttribute("dateMeasured_text");
            //System.out.println("@other dateMeasured_text: " + dateMeasured);
            if (dateMeasured != null && !dateMeasured.equalsIgnoreCase("")) {
//                Date dateService = dateTimeFormat.parse(dateMeasured);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                Pattern pattern = Pattern.compile("\\s");
                Matcher matcher = pattern.matcher(dateMeasured);
                boolean found = matcher.find();

                Date tFrom = null;
                XMLGregorianCalendar xTreatFrom = null;

                if (dateMeasured.length() == 10) {
                    tFrom = dateFormat.parse(dateMeasured);
                } else {
                    if(dateMeasured.contains("T")){
                        dateMeasured = dateMeasured.replace("T", " ");
                    }
                    if(dateMeasured.contains(".")){
                        int i = dateMeasured.indexOf(".");
                        dateMeasured = dateMeasured.substring(0, i);
                    }
                    if(dateMeasured.contains("-")){
                        dateMeasured = dateMeasured.replaceAll("-", "/");
                    }
                    System.out.println("DATE MEASURED FINAL = " + dateMeasured);
                    tFrom = dateTimeFormat.parse(dateMeasured);
                }

                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                other.setDateMeasured(xTreatFrom);
                session.setAttribute("dateMeasured", xTreatFrom);
            } else {
                session.setAttribute("dateMeasured_error", "The date should be filled in");
                checkError = true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            logger.info("Doing Error Check");
            if (checkError) {
                saveScreenToSession(request);
                nextJSP = "/biometrics/CaptureBiometrics.jsp";
            } else {
                if (UpdateBiometrics != null && UpdateBiometrics.equals("true")) {
                    String oldBioID = (String) session.getAttribute("BiometricID");
                    System.out.println("OLDBIOID = " + oldBioID);
                    //clearAllFromSession(request);
                    int useId = user.getUserId();

                    logger.info("Before calling the WS");
                    BiometricsId bId = port.saveOtherBiometrics(other, useId, 1, oldBioID);
                    int otherId = bId.getId();
                    logger.info("From WS, ID = " + otherId);
                    nextJSP = "/biometrics/CaptureBiometrics.jsp";


                    request.setAttribute("revNo", bId.getReferenceNo());

                    if (otherId > 0) {
                        session.setAttribute("updated", "Your other details have been captured");
                    } else {

                        session.setAttribute("failed", "Sorry! other save has failed, Please try again!");
                        nextJSP = "/biometrics/CaptureBiometrics.jsp";
                    }
                } else {
                    //clearAllFromSession(request);
                    int useId = user.getUserId();

                    logger.info("Before calling the WS");
                    BiometricsId bId = port.saveOtherBiometrics(other, useId, 1, null);
                    int otherId = bId.getId();
                    logger.info("From WS, ID = " + otherId);
                    nextJSP = "/biometrics/CaptureBiometrics.jsp";


                    request.setAttribute("revNo", bId.getReferenceNo());

                    if (otherId > 0) {
                        session.setAttribute("updated", "Your other details have been captured");
                    } else {

                        session.setAttribute("failed", "Sorry! other save has failed, Please try again!");
                        nextJSP = "/biometrics/CaptureBiometrics.jsp";
                    }
                }
            }
            session.setAttribute("refreshed", true);
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

//    public XMLGregorianCalendar convertDateXML(Date date) {
//
//        GregorianCalendar calendar = new GregorianCalendar();
//        calendar.setTime(date);
//        return getDatatypeFactory().newXMLGregorianCalendar(calendar);
//
//    }
//
//    private DatatypeFactory getDatatypeFactory() {
//        if (_datatypeFactory == null) {
//
//            try {
//                _datatypeFactory = DatatypeFactory.newInstance();
//            } catch (DatatypeConfigurationException ex) {
//                ex.printStackTrace();
//            }
//        }
//
//        return (DatatypeFactory) _datatypeFactory;
//    }

    @Override
    public String getName() {
        return "CaptureOtherCommand";
    }
}

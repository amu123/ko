package com.koh.fe.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoUser;

/**
 *
 * @author gerritj
 */
public class AddUserCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoUser user = new NeoUser();
        boolean checkError = true;
        boolean checkMissing = false;
        HttpSession session = request.getSession();
        String nextJSP = "";
        
        user.setEmailAddress(request.getParameter("email"));
        user.setUsername(request.getParameter("username"));
        user.setPassword(request.getParameter("password"));
        user.setName(request.getParameter("name"));
        user.setSurname(request.getParameter("surname"));
        String comment = request.getParameter("comments");
        String passwordRetype = request.getParameter("password2");
        //service.getNeoManagerBeanPort().createInactiveUser(user, comment);
        
//        System.out.println("The name " + user.getName());
//        System.out.println("The surname " + user.getSurname());
//        System.out.println("The email address " + user.getEmailAddress());
//        System.out.println("The password " + user.getPassword());
//        System.out.println("The username " + user.getUsername());
//        System.out.println("The comment " + comment);
//        System.out.println("the retype password " + passwordRetype);

        
         //Check for missing fields
            
        if (user.getName() == null || user.getName().trim().equalsIgnoreCase("")) {
            checkMissing = true;
            //System.out.println("1");
        } else if (user.getSurname() == null || user.getSurname().trim().equalsIgnoreCase("")) {

            checkMissing = true;
            //System.out.println("2");
        } else if (user.getEmailAddress() == null || user.getEmailAddress().trim().equalsIgnoreCase("")) {

            checkMissing = true;
            //System.out.println("3");
        } else if (user.getPassword() == null || user.getPassword().trim().equalsIgnoreCase("")) {

            checkMissing = true;
            //System.out.println("4");
        
        } else if (passwordRetype == null || passwordRetype.trim().equalsIgnoreCase("")) {

            checkMissing = true;
            //System.out.println("5");
        } else if (comment == null || comment.trim().equalsIgnoreCase("")) {

            checkMissing = true;
            //System.out.println("6");
        }
        
        if (checkMissing) {
            nextJSP = "/SystemAdmin/AddUser.jsp";
            session.setAttribute("name_error", "All the fields are required");
            this.saveScreenToSession(request);

        } else {
            checkError = service.getNeoManagerBeanPort().validateUserDetailsByCriteria(user);
            
            if (checkError) {
                nextJSP = "/SystemAdmin/AddUser.jsp";
                session.setAttribute("name_error", "The user with the following details already exists");
                this.saveScreenToSession(request);
            } else {
                nextJSP = "/Login.jsp";
                service.getNeoManagerBeanPort().createInactiveUser(user, comment);
                this.clearAllFromSession(request);
                System.out.println("Success");
            }
    
        }
               
        RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
        try {

            dispatcher.forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "AddUserCommand";
    }
}

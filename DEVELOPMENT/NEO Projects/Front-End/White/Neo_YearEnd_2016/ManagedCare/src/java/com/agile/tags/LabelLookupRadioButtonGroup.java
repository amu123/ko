/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import com.agile.security.webservice.LookupValue;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;


/**
 *
 * @author gerritj
 */
public class LabelLookupRadioButtonGroup extends TagSupport {

    private String elementName;
    private String javaScript;
    private String lookupId;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            ArrayList<LookupValue> lookUps = (ArrayList<LookupValue>) TagMethods.getLookupValue(new Integer(lookupId));
            out.println("<table>");
            String selectedRadio = "" + session.getAttribute(elementName);
            for (int i = 0; i < lookUps.size(); i++) {
                LookupValue lookupValue = lookUps.get(i);
                if(!lookupValue.getId().toString().equalsIgnoreCase(selectedRadio)){
                    out.println("<tr><td><input type=\"radio\" name=\"" + elementName + "\" value=\"" + lookupValue.getId() + "\"><label class=\"label\">" + lookupValue.getValue() + "</label></input></td></tr>");
                }else{
                    out.println("<tr><td><input type=\"radio\" name=\"" + elementName + "\" value=\"" + lookupValue.getId() + "\" checked><label class=\"label\">" + lookupValue.getValue() + "</label></input></td></tr>");
                }
                
            }
            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabeLookuplRadioButtonGroup tag", ex);
        }
        return super.doEndTag();

    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author princes
 */
public class BrokerViewCommand extends NeoCommand {
 @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String brokerAppCode = request.getParameter("brokerCode");
        String brokerAppName = request.getParameter("brokerName");
        String brokerEntityId = request.getParameter("brokerEntityId");
        String brokerTitle = request.getParameter("brokerTitle");
        String brokerContactPerson = request.getParameter("brokerContactPerson");
        String brokerContactSurname = request.getParameter("brokerContactSurname");
        String brokerRegion = request.getParameter("brokerRegion");
        String vipBrokerInd = request.getParameter("vipBrokerInd");
        
        request.setAttribute("brokerAppCode", brokerAppCode);
        request.setAttribute("brokerAppName", brokerAppName);
        request.setAttribute("brokerEntityId", brokerEntityId);
        request.setAttribute("brokerTitle", brokerTitle);
        request.setAttribute("brokerContactPerson", brokerContactPerson);
        request.setAttribute("brokerContactSurname", brokerContactSurname);
        request.setAttribute("brokerRegion", brokerRegion);
        request.setAttribute("vipBrokerInd", vipBrokerInd);
        try {
            context.getRequestDispatcher("/Broker/Broker.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(BrokerViewCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BrokerViewCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "BrokerViewCommand";
    }

}

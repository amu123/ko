/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;


import com.koh.command.Command;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthPMBDetails;

/**
 *
 * @author johanl
 */
public class SetSelectedPMBCode extends Command{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        
        ArrayList<AuthPMBDetails> pmbList = (ArrayList<AuthPMBDetails>) session.getAttribute("authPMBDetails");
        ArrayList<AuthPMBDetails> pmbSelected = new ArrayList<AuthPMBDetails>();
        
        boolean doSomething = false;
        
        String selectedValues = request.getParameter("selectedValues");
        String checked = request.getParameter("checked");
        System.out.println("checked " + checked);

        if (checked != null) {
            if (checked.equalsIgnoreCase("true")) {
                session.setAttribute("pmbSelected", "true");
                doSomething = true;
            } else if (checked.equalsIgnoreCase("false")) {
                session.setAttribute("pmbSelected", "false");
                session.setAttribute("pmb", "0");
            }
        }
        
        if (doSomething) {
            String[] selectedValuesArr = selectedValues.split("\\|");
            for (String s : selectedValuesArr) {
                if (s != null && !s.equals("")) {
                    int index = Integer.parseInt(s);
                    AuthPMBDetails pmb = pmbList.get(index);
                    pmbSelected.add(pmb);
                }
            }
            session.setAttribute("savedAuthPMBs", pmbSelected);
            session.setAttribute("pmb", "1");
            session.setAttribute("authBenLimMap", null);
            session.setAttribute("coPay", "0.0");
            session.setAttribute("coPay_error", "");
            
        } else {
            session.setAttribute("savedAuthPMBs", null);
        }   
        
        try {
            String nextJSP = "" + session.getAttribute("onScreen");
            System.out.println("SetSelectedPMBCode onScreen = " + nextJSP);
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }        
        return null;
    }

    @Override
    public String getName() {
        return "SetSelectedPMBCode";
    }
    
}

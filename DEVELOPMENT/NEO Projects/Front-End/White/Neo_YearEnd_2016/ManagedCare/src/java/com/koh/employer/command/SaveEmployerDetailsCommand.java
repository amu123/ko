/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.employer.command;

import com.koh.command.NeoCommand;
import com.koh.employer.EmployerGroupMapping;
import com.koh.member.application.command.SaveMemberCoverDetailsCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Exception;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class SaveEmployerDetailsCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String s = request.getParameter("buttonPressed");
        System.out.println("buttonPressed " + s);
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("changeAdmin")) {
                try {
                    linkAdminToEmployer(port, request, response, context);
                    request.setAttribute("buttonPressed", null);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveEmployerDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveEmployerDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if(s.equals("update_button")){
                String result = updateChanges(port, request);
                request.setAttribute("buttonPressed", null);
                try {
                PrintWriter out = response.getWriter();
                out.println(result);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            }
        } else {
            String result = validateAndSave(port, request);

            try {
                PrintWriter out = response.getWriter();
                out.println(result);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "SaveEmployerDetailsCommand";
    }
    
    private String updateChanges(NeoManagerBean port , HttpServletRequest request){
        Map<String, String> errors = EmployerGroupMapping.validate(request);
        List<KeyValueArray> list = new ArrayList<KeyValueArray>();
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        System.out.println("Status : " + status);
        String additionalData = null;
        String updateFields = null;   
        if ("OK".equalsIgnoreCase(status)) {
        System.out.println("Employer Group Entity ID : " +request.getParameter("EmployerGroup_entityId"));
        if (request.getParameter("EmployerGroup_entityId") != null && !request.getParameter("EmployerGroup_entityId").isEmpty()) {
            int employerGroupEntityID = Integer.parseInt(request.getParameter("EmployerGroup_entityId"));           
            if(employerGroupEntityID == 0) {
                employerGroupEntityID = Integer.parseInt(request.getParameter("EmployerGroup_parentEntityId"));
            }
            
            XMLGregorianCalendar cal = port.getFirstMemberLinkedDateToGroup(employerGroupEntityID);
            if(cal!=null){
                String inceptionDate = request.getParameter("EmployerGroup_startDate");
                Date date1 = DateTimeUtils.getDateFromXMLGregorianCalendar(cal);
                Date date2 = DateTimeUtils.convertFromYYYYMMDD(inceptionDate);
                if(date1.getTime()<date2.getTime()){
                    errors.put("EmployerGroup_startDate_error", "Please enter correct start date. Proposed inception date " + DateTimeUtils.convertToYYYYMMDD(date1));
                }
            }
        }
        validationErros = TabUtils.convertMapToJSON(errors);
        status = TabUtils.getStatus(errors);
        //System.out.println("Status : " + status);
        if ("OK".equalsIgnoreCase(status)) {
            if (updateEmployer(port, request)) {
                additionalData = "\"enable_tabs\":\"true\"";
                EmployerGroup eg = (EmployerGroup) request.getAttribute("EmployerGroup");
                Map<String, String> fieldsMap = new HashMap<String, String>();
                fieldsMap.put("employer_header_employerName", eg.getEmployerName());
                fieldsMap.put("employer_header_employerNumber", eg.getEmployerNumber());
                fieldsMap.put("EmployerGroup_entityId", Integer.toString(eg.getEntityId()));
                fieldsMap.put("employerEntityId", Integer.toString(eg.getEntityId()));
                updateFields = TabUtils.convertMapToJSON(fieldsMap);
                additionalData = additionalData + ",\"message\":\"Employer Saved\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving the employer.\"";
            }
        } else {
            additionalData = "\"message\":\"There were validation errors.\"";
        }
        }else {
            additionalData = "\"message\":\"There were validation errors.\"";
        }
        String jsonResult = TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
        return jsonResult;
    }
    
    private boolean updateEmployer(NeoManagerBean port , HttpServletRequest request){
        boolean result;
        NeoUser neoUser = getNeoUser(request);
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);
        
        try{
            EmployerGroup eg = EmployerGroupMapping.getEmployerGroup(request, neoUser);
            
            List<AddressDetails> ad = EmployerGroupMapping.getAddressDetails(request, neoUser, eg.getEntityId());
            List<ContactDetails> cd = EmployerGroupMapping.getContactDetails(request, neoUser, eg.getEntityId());
                       
            port.saveAddressListDetails(ad, sec, eg.getEntityId());
            port.saveContactListDetails(cd, sec, eg.getEntityId());
            result = port.updateEmployerGroup(eg, neoUser);
            request.setAttribute("EmployerGroup", eg);
            
        }catch(Exception e){
            result=false;
            System.out.println("Failed to update Employer record : " + e.getMessage());
        }
        return result;
    }

    private String validateAndSave(NeoManagerBean port, HttpServletRequest request) {
        Map<String, String> errors = EmployerGroupMapping.validate(request);
        List<KeyValueArray> list = new ArrayList<KeyValueArray>();
        if (request.getParameter("EmployerGroup_employerNumber") != null && !request.getParameter("EmployerGroup_employerNumber").isEmpty()) {
            list = port.fetchEmployerListByNumber(request.getParameter("EmployerGroup_employerNumber"));
        }
        if (list.size() >= 1) {
            if (request.getParameter("EmployerGroup_entityId") == null || request.getParameter("EmployerGroup_entityId").isEmpty()) {
                errors.put("EmployerGroup_employerNumber_error", "Group code is already used");
            }
        }
        
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (save(port, request)) {
                additionalData = "\"enable_tabs\":\"true\"";
                EmployerGroup eg = (EmployerGroup) request.getAttribute("EmployerGroup");
                Map<String, String> fieldsMap = new HashMap<String, String>();
                fieldsMap.put("employer_header_employerName", eg.getEmployerName());
                fieldsMap.put("employer_header_employerNumber", eg.getEmployerNumber());
                fieldsMap.put("EmployerGroup_entityId", Integer.toString(eg.getEntityId()));
                fieldsMap.put("employerEntityId", Integer.toString(eg.getEntityId()));
                updateFields = TabUtils.convertMapToJSON(fieldsMap);
                additionalData = additionalData + ",\"message\":\"Employer Saved\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving the employer.\"";
            }
        } else {
            additionalData = "\"enable_tabs\":\"false\"";
        }
        String jsonResult = TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
//        System.out.println("Final System result : " + jsonResult);
        return jsonResult;
    }

    private boolean save(NeoManagerBean port, HttpServletRequest request) {
        NeoUser neoUser = getNeoUser(request);

        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);

        /**
         * Need to remove *
         */
        if (neoUser == null) {
            neoUser = new NeoUser();
            neoUser.setSecurityGroupId(1);
            neoUser.setUserId(1);
        }
        boolean newEmployer = false;
        try {
            EmployerGroup eg = EmployerGroupMapping.getEmployerGroup(request, neoUser);
            newEmployer = eg.getEntityId() == 0;
            String s = request.getParameter("EmployerGroup_parentEntityId");
            System.out.println("Inception Date : " + eg.getInceptionDate());
            if (s != null && !s.isEmpty() && newEmployer) {
                try {
                    int i = Integer.parseInt(s);
                    System.out.println("Saving Employer Branch");
                    eg = port.saveEmployerBranch(eg, i);
                } catch (Exception e) {
                    eg = port.saveEmployer(eg);
                }
            } else {
                eg = port.saveEmployer(eg);
            }
            List<AddressDetails> ad = EmployerGroupMapping.getAddressDetails(request, neoUser, eg.getEntityId());
            List<ContactDetails> cd = EmployerGroupMapping.getContactDetails(request, neoUser, eg.getEntityId());
                       
            port.saveAddressListDetails(ad, sec, eg.getEntityId());
            port.saveContactListDetails(cd, sec, eg.getEntityId());

            request.setAttribute("EmployerGroup", eg);
            if (newEmployer) {
                request.getSession().setAttribute("lookup_cache_employer", null);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void linkAdminToEmployer(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        System.out.println("we are here");
        request.setAttribute("diagCodeId", "EmployerGroup_administrator");
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        context.getRequestDispatcher("/Employer/LinkGroupToAdministrator.jsp").forward(request, response);

    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser) request.getSession().getAttribute("persist_user");
    }
}

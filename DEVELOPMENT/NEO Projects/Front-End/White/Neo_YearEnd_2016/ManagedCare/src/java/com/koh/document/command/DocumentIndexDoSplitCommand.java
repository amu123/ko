/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;

import agility.za.indexdocumenttype.IndexDocType;
import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import agility.za.indexdocumenttype.SplitDocType;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class DocumentIndexDoSplitCommand extends NeoCommand {
    private Logger logger = Logger.getLogger(this.getClass());
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------In DocumentIndexDoSplitCommand---------------------");
            String profileType = (String)request.getSession().getAttribute("profile")!=null?(String)request.getSession().getAttribute("profile"):"";
            String workDrive = new PropertiesReader().getProperty("DocumentIndexWorkDrive");
            String selectedFile = request.getParameter("selectedFile");
            String splitFilter = request.getParameter("SplitFilter");

            String folderList = (String) request.getParameter("folderList");
            String listOrWork = (String) request.getParameter("listOrWork");
            request.setAttribute("folderList", folderList);
            request.setAttribute("listOrWork", listOrWork);
            if (selectedFile == null || selectedFile.equals("")) {
                errorMessage(request, response, "No file selected");
                return "DocumentIndexDoSplitCommand";
            }else if (splitFilter == null || splitFilter.equals("")) {
                errorMessage(request, response, "No filter entered");
                return "DocumentIndexDoSplitCommand";
            }

            IndexDocumentRequest req = new IndexDocumentRequest();
            req.setIndexType("Split");
            req.setSrcDrive(workDrive);
            NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
            req.setAgent(loggedInUser);
           
            req.setEntityNumber("n/a");
            SplitDocType splitDocType = new SplitDocType();
            splitDocType.setFilename(selectedFile);
            splitDocType.setFolder(folderList+"/"+profileType);
            splitDocType.setPages(splitFilter);
            req.setSplit(splitDocType);
            System.out.println("Before Webservice");
            IndexDocumentResponse res = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
            if(!res.isIsSuccess()){
                errorMessage(request, response, res.getMessage());
                return "DocumentIndexDoSplitCommand";                              
            }
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Indexing/SplitDocument.jsp");
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "DocumentIndexDoSplitCommand";
    }
    @Override
    public String getName() {
        return "DocumentIndexDoSplitCommand";
    }
    private void errorMessage(HttpServletRequest request, HttpServletResponse response, String msg) throws Exception {
        request.setAttribute("errorMsg", msg);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/Indexing/SplitDocument.jsp");
        dispatcher.forward(request, response);
    }
}
package com.koh.preauth.command;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import com.koh.command.NeoCommand;
import com.koh.preauth.utils.ERPUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.FormatUtils;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;


/**
 *
 * @author Johan-NB
 */
public class GetAuthCPTMappingForTariff extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        List<AuthTariffDetails> tariffList = (List<AuthTariffDetails>) session.getAttribute("tariffListArray");
        System.out.println("tarifList size = " + tariffList.size());
        List<AuthCPTDetails> cptList = null;
        if (session.getAttribute("authCPTListDetails") != null) {
            cptList = (List<AuthCPTDetails>) session.getAttribute("authCPTListDetails");
            if (cptList == null && cptList.isEmpty()) {
                cptList = new ArrayList<AuthCPTDetails>();
            }
        } else {
            cptList = new ArrayList<AuthCPTDetails>();
        }

        HashMap<String, AuthCPTDetails> cptListMap = new HashMap<String, AuthCPTDetails>();
        for (AuthCPTDetails cpt : cptList) {
            cptListMap.put(cpt.getCptCode(), cpt);
        }


        List<AuthTariffCPT> tcListAll = new ArrayList<AuthTariffCPT>();

        double totEstimatedWard = 0.0d;
        double totEstimatedRPL = 0.0d;
        double copay = 0.0d;

        int singleMapCount = 0;

        boolean doLOC = false;
        if (tariffList.isEmpty()) {
            doLOC = true;
        }

        //admission dates
        Date aDate = null;
        Date admissionDate72 = null;
        XMLGregorianCalendar xAdmission = null;
        XMLGregorianCalendar previousEnd = null;
        String admission = "" + session.getAttribute("admissionDateTime");
        try {
            if (admission.length()>10){
                aDate = FormatUtils.dateTimeFormat.parse(admission);
            } else {
                aDate = FormatUtils.dateFormat.parse(admission);
            }
            admissionDate72 = FormatUtils.dateTimeNoSecFormat.parse(admission);
            xAdmission = DateTimeUtils.convertDateToXMLGregorianCalendar(aDate);

        } catch (ParseException ex) {
            Logger.getLogger(GetAuthCPTMappingForTariff.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (AuthTariffDetails at : tariffList) {
            doLOC = false;
            totEstimatedRPL += at.getAmount();

            List<AuthTariffCPT> tcList = port.getMappedCPTForTreatment(at.getTariffCode());
            System.out.println("tcList size " + tcList.size());
            tcListAll.addAll(tcList);

            if (!tcList.isEmpty()) {

                if (tariffList.size() == 1 && tcList.size() == 1) {
                    doLOC = true;
                }

                if (tcList.size() == 1) {
                    singleMapCount++;

                    if (singleMapCount == tariffList.size()) {
                        doLOC = true;
                    }

                    session.setAttribute("authCPTListDetails", cptList);
                }

            } else {
                doLOC = true;
            }

            if (doLOC) {

                //skip LOC mapping on update
                String nextJSP = "" + session.getAttribute("onScreen");
                System.out.println("skip LOC onScreen = " + nextJSP);
                if (!nextJSP.equalsIgnoreCase("/PreAuth/HospitalAuthDetails_Update.jsp")) {
                    List<AuthLocLosDetails> locLosList = new ArrayList<AuthLocLosDetails>();

                    String facility = "" + session.getAttribute("facilityProvider_text");
                    String facilityDisc = "" + session.getAttribute("facilityProvDiscTypeId");
                    String prodId = "" + session.getAttribute("scheme");
                    String optId = "" + session.getAttribute("schemeOption");
                    String pICD = "" + session.getAttribute("primaryICD_text");

                    int provDesc = Integer.parseInt(facilityDisc);
                    int pID = Integer.parseInt(prodId);
                    int oID = Integer.parseInt(optId);
                    
                    int wardLookup = getWARDLookupID(provDesc);
                    
                    boolean skipLocLosCalc = false;
                    skipLocLosCalc = validateLocLocProvDesc(provDesc);

                    if (skipLocLosCalc) {
                        if (cptList != null && !cptList.isEmpty()) {

                            locLosList = port.calculateLOSByCPT(cptList, facilityDisc);
                            locLosList = DateTimeUtils.orderLocLosByType(locLosList);

                            if (locLosList != null && !locLosList.isEmpty()) {

                                System.out.println("locLosList size = " + locLosList.size());

                                List<AuthHospitalLOC> locList = null;//TODO
                                if (session.getAttribute("AuthLocList") != null) {
                                    locList = (List<AuthHospitalLOC>) session.getAttribute("AuthLocList");
                                    if (locList == null && locList.isEmpty()) {
                                        locList = new ArrayList<AuthHospitalLOC>();
                                    }
                                } else {
                                    locList = new ArrayList<AuthHospitalLOC>();
                                }

                                int totLos = 0;
                                int x = 0;
                                for (AuthLocLosDetails ll : locLosList) {

                                    if (previousEnd != null) {
                                        xAdmission = previousEnd;
                                        aDate = previousEnd.toGregorianCalendar().getTime();
                                    }

                                    String code = NeoCommand.service.getNeoManagerBeanPort().getValueFromCodeTableForTableId(wardLookup, ll.getLocType());

                                    int los = ll.getLos();
                                    totLos += los;

                                    double estimatedWardAmount = 0.0d;
                                    //load ward tariff if exist
                                    if (code != null && !code.equals("")) {
                                        estimatedWardAmount = ERPUtils.getEstimatedCostByLOSForWard(xAdmission, pID, oID, code, facilityDisc, facility, pICD, los);
                                        totEstimatedWard += estimatedWardAmount;
                                    }

                                    //get end date form los 
                                    Date endDate = DateTimeUtils.calculateLOCEndDateFromLOS(aDate, los);
                                    previousEnd = DateTimeUtils.convertDateToXMLGregorianCalendar(endDate);


                                    //set level of care
                                    AuthHospitalLOC loc = new AuthHospitalLOC();
                                    loc.setUserId(user.getUserId());

                                    loc.setLevelOfCare(ll.getLocType());
                                    loc.setLengthOfStay(los);
                                    loc.setApprovedLos(los);
                                    loc.setDateFrom(xAdmission);
                                    loc.setDateTo(previousEnd);
                                    loc.setEstimatedCost(estimatedWardAmount);

                                    locList.add(loc);

                                    x++;
                                }
                                //set total los
                                session.setAttribute("los", totLos);

                                session.setAttribute("AuthLocList", locList);
                                session.setAttribute("dischargeDateTime", FormatUtils.dateFormat.format(previousEnd.toGregorianCalendar().getTime()));
                            } else {
                                System.out.println("loc list is null");
                            }

                            //to copayment calculation 
                            //copay = CalculateCoPaymentAmounts.getMaxCoPayFromCPTList(cptList);
                            System.out.println("co payment amount = " + copay);
                            session.setAttribute("coPay", copay);
                        }
                    }
                }
            }

        }
        
        //always check 72 hours
        //validate penalty
        Calendar admissionCal = Calendar.getInstance();
        Calendar authDateCal = Calendar.getInstance();

        Date authorisationDate = null;
        String authDate = "" + session.getAttribute("authDate");
        try {
            authorisationDate = FormatUtils.dateFormat.parse(authDate);
        } catch (ParseException ex) {
             Logger.getLogger(GetAuthCPTMappingForTariff.class.getName()).log(Level.SEVERE, null, ex);
        }

        admissionCal.setTime(admissionDate72);
        authDateCal.setTime(authorisationDate);
        authDateCal.add(Calendar.HOUR_OF_DAY, -72);

        session.setAttribute("isPenalty", null);
        if (authDateCal.equals(admissionCal) || authDateCal.after(admissionCal)) {
            session.setAttribute("isPenalty", "yes");
        } else {
            String dspCoPay = "" + session.getAttribute("isDSPPenalty");
            if (dspCoPay.equalsIgnoreCase("no")) {
                session.setAttribute("isPenalty", null);
            }
            session.setAttribute("authPenFound", null);
        }

        //set amounts and los for mappings
        double hospInterim = 0.0d;
        hospInterim = totEstimatedRPL + totEstimatedWard;
        String hospInt = FormatUtils.decimalFormat.format(hospInterim);

        session.setAttribute("hospInterim", hospInt);


        System.out.println("tcListAll size " + tcListAll.size());
        session.setAttribute("mappedListOfCptTariffs", tcListAll);
        if (!cptList.isEmpty()) {
            session.setAttribute("authCPTListDetails", cptList);
        }

        //map screen amount
        session.setAttribute("mapTariffTotal", totEstimatedRPL);

        System.out.println("doLoc before return = " + doLOC);
        return "Done|" + doLOC;
    }
    
    public int getWARDLookupID(int providerType) {
        int wardId = 280;
        if (providerType == 79) {
            wardId = 281;

        } else if (providerType == 55) {
            wardId = 265;

        } else if (providerType == 59) {
            wardId = 266;

        } else if (providerType == 49) {
            wardId = 267;

        } else if (providerType == 76) {
            wardId = 268;

        } else if (providerType == 47) {
            wardId = 269;

        } else if (providerType == 56) {
            wardId = 270;

        } else if (providerType == 77) {
            wardId = 271;

        }
        return wardId;
    }

    @Override
    public String getName() {
        return "GetAuthCPTMappingForTariff";
    }

    public boolean validateLocLocProvDesc(int provDesc) {
        if (provDesc != 57 && provDesc != 58) {
            return false;
        } else {
            return true;
        }
    }
}

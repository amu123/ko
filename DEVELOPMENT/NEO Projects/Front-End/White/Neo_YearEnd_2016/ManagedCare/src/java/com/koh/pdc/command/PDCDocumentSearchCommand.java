/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author shimanem
 */
public class PDCDocumentSearchCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println(" --->>> PDC DOCUMENT SEARCH <<<---");
        try {
            HttpSession session = request.getSession();

            int index = Integer.parseInt(String.valueOf(request.getParameter("selectIndex")));
            System.out.println(">> SELECTED INDEX >> " + index);
            if (index != 0) {
                session.setAttribute("optIndex", index);
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("/PDC/PDCDocumentView.jsp");
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(PDCDocumentSearchCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PDCDocumentSearchCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "PDCDocumentSearchCommand";
    }
}

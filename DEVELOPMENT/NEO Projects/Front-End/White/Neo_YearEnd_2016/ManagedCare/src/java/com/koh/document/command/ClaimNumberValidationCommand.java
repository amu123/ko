/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoManagerBean;
import neo.manager.NeoManagerBeanService;

/**
 *
 * @author Antaryami
 */
public class ClaimNumberValidationCommand extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String result = "failed";
        try{
        String claimNumber = request.getParameter("tempClaimNumber");
        String entityNumber = request.getParameter("tempentityNumber");
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        result = port.validateClaimForMember(Long.parseLong(claimNumber), entityNumber)?"success":"failed";
        PrintWriter out = response.getWriter();
        out.write(result);
        }catch(Exception e){
            result = "failed";
            Logger.getLogger(this.getClass().getName()).severe("Claim number validation fialed ");
        }
        return null;
    }

    @Override
    public String getName() {
        return "ClaimNumberValidationCommand";
    }
    
}

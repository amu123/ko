/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.NeoProcess;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author yuganp
 */
public class RunAppCommand extends NeoCommand{

//    private static final List<Process>  APP_PROCESSES = new ArrayList<Process>();
    static Hashtable processTable = new Hashtable();
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String command = request.getParameter("command");
        System.out.println(command);
        if ("runApp".equalsIgnoreCase(command)) {
            runApplication(request, response, context);
        } else if ("terminateApp".equalsIgnoreCase(command)) {
            terminateApp(request, response, context);
        } else if ("viewApp".equalsIgnoreCase(command)) {
            viewApp(request, response, context);
        }
        return null;
    }
    
    private void runApplication(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = (NeoUser) request.getSession().getAttribute("persist_user");
        PrintWriter out = null;
        Process p = null;
        String index="";
        int logId = 0;
        try {
            long startTime = System.currentTimeMillis();
            out = response.getWriter();
            String app = request.getParameter("appName");
            String command = request.getParameter("appCommand");
        boolean alreadyRunning = false;
            sendHeader(out, app, context.getContextPath());
            for (Object o : processTable.values()) {
                NeoProcess np = (NeoProcess) o;
                if (command.equalsIgnoreCase(np.getCommand())) {
                    alreadyRunning = true;
                    break;
                }
            }
            index = command;
            if (alreadyRunning) {
                sendLine(out, "Application is currently running");
                sendFooter(out, app, -1, "");
                port.saveApplicationLog(app, command, 2, neoUser.getUserId());
                return;
            }
            List<String[]> commandList= getCommandList(request);
            File f = new File(command);
            String absolutePath =f.getAbsolutePath(); 
            System.out.println("Process Started");
            logId = port.saveApplicationLog(app, command, 1, neoUser.getUserId());
            for (String[] sArr : commandList) {
                ProcessBuilder pb = new ProcessBuilder(sArr);
                pb.directory(new File(absolutePath.substring(0,absolutePath.lastIndexOf(File.separator))));
                p = pb.start();
                NeoProcess np = new NeoProcess();
                np.setApplicationName(app);
                np.setStartTime(new Date());
                np.setProcess(p);
                np.setCommand(command);
                np.setLogId(logId);
                processTable.put(command, np);
                BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String resultLine = in.readLine();
                while (resultLine != null) {
                    sendLine(out, resultLine);
                    resultLine = in.readLine();
                }
                in.close();
                if (p.exitValue() != 0) {
                    break;
                }
                
            }
            long endTime = System.currentTimeMillis();
            long timeTaken = endTime - startTime;
            if (processTable.get(index) != null) {
                port.updateApplicationLog(logId, 3, neoUser.getUserId());
            }
            sendFooter(out, app, p.exitValue(), getTimeTaken(timeTaken));
        } catch (Exception e) {
            e.printStackTrace();
            try {
                port.updateApplicationLog(logId, 4, neoUser.getUserId());
                sendLine(out, e.getMessage());
                sendFooter(out, "", -1, "");
            } catch (Exception ee) {
                
            }
        } finally {
            if (out != null) {
                out.close();
            }
            if (p != null) {
                try {
                    p.destroy();
                } catch (Exception e) {
                    
                }
                processTable.remove(index);
            }
        }
        
    }
    
    private void terminateApp(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = (NeoUser) request.getSession().getAttribute("persist_user");
        PrintWriter out = null;
        try {
            System.out.println("Terminating Applications");
            String index = request.getParameter("processId");
             NeoProcess np = (NeoProcess) processTable.get(index);
            if (np != null && np.getProcess() != null) {
                port.updateApplicationLog(np.getLogId(), 5, neoUser.getUserId());
                np.getProcess().destroy();
                processTable.remove(index);
            } else {
                System.out.println("Process is null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void viewApp(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        request.setAttribute("processes", processTable);
        try {
            context.getRequestDispatcher("/SystemAdmin/ViewApp.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(RunAppCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RunAppCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private List<String> getCommand(HttpServletRequest request) {
        List<String> result = new ArrayList<String>();
        result.add(request.getParameter("appCommand"));
        int paramCount = TabUtils.getIntParam(request, "paramCount");
        if (paramCount > 0) {
            for (int i = 1; i < paramCount + 1; i++) {
                result.add(request.getParameter("param"+i));
            }
        }
        System.out.print("Run Application:");
        for (String s : result) {
            System.out.print(" " + s);
        }
        System.out.println();
                
        return result;
    }
    
    private List<String[]> getCommandList(HttpServletRequest request) {
        List<String[]> result = new ArrayList<String[]>();
        String command = request.getParameter("appCommand");
        String lowerCommand = command.toLowerCase();
        if (lowerCommand.endsWith(".bat") || lowerCommand.endsWith(".cmd")) {
            try {
                FileReader fr = new FileReader(command);
                LineNumberReader l = new LineNumberReader(fr);
                String s = l.readLine();
                while (s != null) {
                    if (s == null || s.isEmpty()) {
                        
                    } else {
                        List<String> sList = splitString(s);
                        for (int i = 0; i < sList.size(); i++) {
                            String ss = sList.get(i);
                            if (ss != null && ss.length() == 2 && ss.startsWith("%") ) {
                                String index = ss.substring(1);
                                String paramValue = request.getParameter("param"+index);
                                if (paramValue == null || paramValue.isEmpty()) {
                                    sList.set(i, "");
                                } else {
                                    sList.set(i, paramValue);
                                }
                            }
                        }
                        result.add(sList.toArray(new String[0]));
                    }
                    s = l.readLine();
                }
                l.close();
                fr.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            
        } else {
            List<String> sList = new ArrayList<String>();
            sList.add(command);
            int paramCount = TabUtils.getIntParam(request, "paramCount");
            if (paramCount > 0) {
                for (int i = 1; i < paramCount + 1; i++) {
                    sList.add(request.getParameter("param"+i));
                }
            }
            result.add(sList.toArray(new String[0] ));
        }
        return result;
    }
    
    private List<String> splitString(String s) {
        List<String> list = new ArrayList<String>();
        Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(s);
        while (m.find()) {
            list.add(m.group(1));
        }     
        return list;
    }
    
    private String getTimeTaken(long time) {
        String second =""+ (time / 1000) % 60;
        String minute =""+ (time / (1000 * 60)) % 60;
        String hour = ""+(time / (1000 * 60 * 60)) % 24;
        String result=hour+":"+minute+":"+second;
        return result;
    }
    

    private void sendHeader(PrintWriter out, String app, String contextPath) {
        out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"><html>");
        out.println("<head>");
        out.println("<link rel=\"stylesheet\" href=\"" + contextPath + "/resources/styles.css\"/>");
        out.println("<script type='text/javascript' src=\"" + contextPath + "/resources/jQuery/jquery-1.4.2.js\"></script>");
        out.println("<script type='text/javascript' src=\"" + contextPath + "/resources/jQuery/jquery.loadmask.js\"></script>");
        out.println("<script type='text/javascript' src=\"" + contextPath + "/resources/AgileTabs.js\"></script>");
  //      out.println("<script type='text/javascript' src=\"" + contextPath + "/resources/jQuery/jquery.scrollTo-1.4.2-min.js\"></script>");
        out.println("<script>");
        out.println("function addLine(line) {");
//        out.println("document.getElementById('detailsDiv').innerHTML = document.getElementById('detailsDiv').innerHTML + line + '<br>';");
        out.println("$('#detailsDiv').append(line+'<br>');");
        out.println("$('#detailsDiv').scrollTop($('#detailsDiv')[0].scrollHeight);");
//        out.println("$('#detailsDiv').animate({scrollTop: $('#detailsDiv').scrollHeight}, 500);");
        out.println("}");
        out.println("function terminateApp(processId) {");
        out.println("$.post('" + contextPath + "/AgileController?opperation=RunAppCommand&command=terminateApp&processId=' + processId);");
        out.println("}");
        out.println("</script>");
        out.println("</head><body>");
        out.println("<label class=\"header\">Running " + app + "</label><center><label class=\"subheader\">Application Output</label></center>");
        out.println("        <div style='height: 400px; border: .2em solid #900; overflow: auto; ' id='detailsDiv'>");
        out.println(app + " Started<br>");
        out.println("</div>");
        out.flush();
    }
    
    private void sendTerminateButton(PrintWriter out, String index) {
//        out.println("<agiletags:ControllerForm name='RunAppForm1'>");
//        out.println("<input type=\"hidden\" name=\"opperation\" value=\"RunAppCommand\" />");
//        out.println("<input type=\"hidden\" name=\"command\" value=\"terminateApp\" />");
//        out.println("<input type=\"hidden\" name=\"processId\" value=\"" + index + "\" />");
        out.println("<button type=\"button\" onclick=\"terminateApp('" + index + "');\">Terminate Application</button>");
//        out.println("</agiletags:ControllerForm>");
    }
    
    private void sendFooter(PrintWriter out, String app, int exitValue, String time) {
//        out.println("</div><p>");
        out.println("<p>");
        if (exitValue == 0) {
             out.print(app + " has finished in " + time);
        } else {
             out.print("An error occured running " + app + ".");
        }
        out.println("</p><button type='button' onclick='window.location=\"/ManagedCare/Security/Welcome.jsp\"; '>Close</button>");
        out.println("<button type='button' onclick='window.history.back();'>Return</button>");
        out.println("</html> ");
        out.flush();
    }
    
    private void sendLine(PrintWriter out, String line) {
//        out.println("<label>"+line + "</label><br>");
        out.println("<script>addLine('" + line + "');</script>");
        out.flush();
    }

    @Override
    public String getName() {
        return "RunAppCommand";
    }
    
}

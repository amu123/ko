/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverDetails;

/**
 *
 * @author Johan-NB
 */
public class DisplayCoverDepJoinDate extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        PrintWriter out = null;

        String depCode = request.getParameter("depCode");
        session.setAttribute("dependantNumber", depCode);
        System.out.println("dependant selected = "+depCode);
        int dependantCode = -1;
        boolean error = false;
        String joinDate = "";

        if(depCode != null && !depCode.equalsIgnoreCase("") && !depCode.equalsIgnoreCase("99")){
            dependantCode = Integer.parseInt(depCode);
        }else{
            error = true;
        }
        System.out.println("error = "+error);
        System.out.println("dependantCode = "+dependantCode);
        if(!error && dependantCode != -1){
            List<CoverDetails> coverList = (List<CoverDetails>) session.getAttribute("covMemListDetails");
            if(coverList != null){
                for (CoverDetails cd : coverList) {
                    if(cd.getDependentNumber() == dependantCode){
                        session.setAttribute("entityId", cd.getEntityId());
                        session.setAttribute("policyNumber", cd.getCoverNumber());
                        session.setAttribute("policyHolderNumber_text", cd.getCoverNumber());
                        session.setAttribute("policyHolderNumber", cd.getCoverNumber());
                        if(cd.getSchemeJoinDate() != null){
                            joinDate = new SimpleDateFormat("yyyy/MM/dd").format(cd.getSchemeJoinDate().toGregorianCalendar().getTime());
                        }else{
                            System.out.println("join date is null, using cover start date");
                            joinDate = new SimpleDateFormat("yyyy/MM/dd").format(cd.getCoverStartDate().toGregorianCalendar().getTime());
                        }
                        
                        break;
                    }
                }

            }else{
                error = true;
            }
        }
        System.out.println("joinDate = "+joinDate);
        try{
            out = response.getWriter();
            if(error){
                out.println("Error|Please Select Dependant");
            }else{
                out.println("Done|"+joinDate);
            }

        }catch(IOException io){
            io.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "DisplayCoverDepJoinDate";
    }

}

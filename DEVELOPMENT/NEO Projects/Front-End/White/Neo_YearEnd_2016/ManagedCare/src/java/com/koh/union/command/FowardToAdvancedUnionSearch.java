/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.union.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.EmployerTabContentsCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.UnionGroup;
import neo.manager.UnionRepresentative;

/**
 *
 * @author shimanem
 */
public class FowardToAdvancedUnionSearch extends NeoCommand {

    private static final Logger LOG = Logger.getLogger(FowardToAdvancedUnionSearch.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        

        try {
            PrintWriter out = response.getWriter();

            String button = request.getParameter("buttonPressed");
            LOG.log(Level.INFO, "Getting Union Details : {0}", button);
            if (button != null && !button.isEmpty()) {
                if (button.equalsIgnoreCase("advancedUnionSearch".trim())) {
                    viewAdvancedSearch(port, request, response, context);
                    request.setAttribute("buttonPressed", null);
                } else if (button.equalsIgnoreCase("saveEmployerUnionGroup".trim())) {
                    String result = linkEmployerGroupToUnion(port, request, response, session, context);
                    request.setAttribute("buttonPressed", null);
                    
                    out.println(result);
                }
            }
            
           

        } catch (ServletException ex) {
            Logger.getLogger(ViewUnionDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ViewUnionDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(FowardToAdvancedUnionSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void viewAdvancedSearch(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        HttpSession session = request.getSession();
        LOG.info("Union Employer Group [Searching Union to Link Group...]");
        request.setAttribute("union_Id", "employerGroup_unionID");
        request.setAttribute("union_Number", "employerGroup_unionNumber");
        request.setAttribute("union_Name", "employerGroup_unionName");
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));

        context.getRequestDispatcher("/Employer/EmployerUnionSearchAdvanced.jsp").forward(request, response);
    }

    private String linkEmployerGroupToUnion(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, HttpSession session, ServletContext context) throws ParseException, ServletException, IOException {
        LOG.info("** Linking Employer Group to Union **");
        Map<String, String> errors = EmployerUnionGroupMapping.validate(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        NeoUser neoUser = getNeoUser(request);

        LOG.log(Level.INFO, "** Link Employer Group to Union : [STATUS] : {0}", status);

        if ("OK".equalsIgnoreCase(status)) {
            if (saveEmployerUnionGroup(request, neoUser)) {
                additionalData = "\"enable_tabs\":\"true\"";  
                additionalData = additionalData + ",\"message\":\"Union for Employer Group Saved\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error linking Employer Group to a Union.\"";
            }
        }
        
        return TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);

    }

    public boolean saveEmployerUnionGroup(HttpServletRequest request, NeoUser neoUser) throws ParseException, ServletException, IOException {
        LOG.info("*** Saving Employer Union Group ***");
        NeoManagerBean port = service.getNeoManagerBeanPort();
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

        //Employer Group Details
        String employerEntityId = String.valueOf(request.getParameter("employerEntityId"));
        String employerName = request.getParameter("employerName");
        String employerNumber = request.getParameter("employerNumber");
        // Employer Union Group Details 
        String unionName = request.getParameter("employerGroup_unionName");
        String unionId = String.valueOf(request.getParameter("employerGroup_unionID"));
        String unionNumber = request.getParameter("employerGroup_unionNumber");
        String repName = request.getParameter("repName");
        String repSurname = request.getParameter("repSurname");
        String unionRepCapacity = String.valueOf(request.getParameter("unionRepCapacity"));
        String unionRegion = String.valueOf(request.getParameter("unionRegion"));
        String inceptionDate = request.getParameter("unionInceptionDate");
        String terminationDate = request.getParameter("unionTerminationDate");
        // Employer Union Group Contact
        String contactNumber = request.getParameter("contactNumber");
        String faxNumber = request.getParameter("faxNumber");
        String email = request.getParameter("emailAddress");
        String alternateEmail = request.getParameter("altEmailAddress");

        UnionGroup unionGroup = new UnionGroup();
        UnionRepresentative unionRep = new UnionRepresentative();

        // Setting Employer "Union" Group Details
        unionGroup.setGroupNumber(employerNumber);
        unionGroup.setGroupName(employerName);
        unionGroup.setGroupUnionId(Integer.parseInt(unionId));
        unionGroup.setGroupUnionNumber(unionNumber);
        unionGroup.setGroupEntityId(Integer.parseInt(employerEntityId));
        // Setting Employer Union Group Rep Details
        unionRep.setUnionID(Integer.parseInt(unionId));
        unionRep.setUnionNumber(unionNumber);
        unionRep.setGroupEntityId(Integer.parseInt(employerEntityId));
        unionRep.setRepresentativeCapacityID(Integer.parseInt(unionRepCapacity));
        unionRep.setName(repName);
        unionRep.setSurname(repSurname);
        unionRep.setContactNumber(contactNumber);
        unionRep.setFaxNumber(faxNumber);
        unionRep.setEmail(email);
        unionRep.setAltEmail(alternateEmail);
        unionRep.setRegionID(Integer.parseInt(unionRegion));
        unionRep.setInceptionDate(DateTimeUtils.convertDateToXMLGregorianCalendar(format.parse(inceptionDate)));
        unionRep.setTerminationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(format.parse(terminationDate)));

        boolean blnValid = port.linkUnionGroup(unionGroup, unionRep, neoUser);
        LOG.log(Level.INFO, "Linking Union Group Status : {0}", blnValid);
        return blnValid;
    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser) request.getSession().getAttribute("persist_user");
    }

    @Override
    public String getName() {
        return "FowardToAdvancedUnionSearch";
    }

}

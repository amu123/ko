/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.tags;

import agility.za.indexdocumenttype.IndexForMember;
import com.koh.command.NeoCommand;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Christo
 */
public class MemberDocumentList extends TagSupport {

    private String command;
    private String confirmCommand;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            List<IndexForMember> list = (List<IndexForMember>) req.getAttribute("indexList");

            if (list == null) {
                System.out.println("List in tag == null");
            } else {
                System.out.println("List in tag = " + list.size());
            }
            if (list != null && list.size() > 0) {

                if (confirmCommand == null) {
                    out.println("<label class=\"header\">Search Results</label><br/><br/>");
                }
                out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
//                out.println("<tr><th>Id</th><th>Member Number</th><th>Type</th><th>Location</th>"
//                        + "<th>Reference</th><th>Current Date</th></tr>");
                out.println("<tr><th>Member Number</th><th>Type</th><th>Location</th>"
                        + "<th>Current Date</th><th colspan='2'></th></tr>");

                out.println("<tr>");//<form name=\"viewDoc\" action=\"/ManagedCare/AgileController\" method=\"POST\">");
//                out.println("<input type=\"hidden\" name=\"opperation\" id=\"opperation\" value=\"MemberDocumentDisplayViewCommand\" />");
//                out.println("<input type=\"hidden\" name=\"fileLocation\" id=\"fileLocation\" value=\"\" /> ");
                NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
                if (confirmCommand == null) {
                    confirmCommand = "MemberDocumentViewCommand";
                }

                for (IndexForMember index : list) {
                    String typeName = index.getDocType();
                    Pattern p = Pattern.compile("([0-9]*)");
                    Matcher m = p.matcher(typeName);
                    if (m.matches()) {
                        typeName = port.getValueForId(212, typeName);
                    }
                    
                    out.println("<tr>" //<td><label class=\"label\">" + index.getIndexId() + "</label></td>"
                            + "<td><label class=\"label\">" + index.getEntityNumber() + "</label></td>"
                            + "<td><label class=\"label\">" + typeName + "</label></td>"
                            + "<td><label class=\"label\">" + index.getLocation() + "</label></td>");
//                            + "<td><label class=\"label\">" + index.getReference() + "</label></td>");
                    if (command.equals(confirmCommand)) {
                        if (index.getLocation().contains("MCT")) {            //Member Certificates 
                        out.println("<td><label class=\"label\">" + convertDate(index.getGenerationDate()) + "</label></td>"
                                + "<td><button type=\"submit\" name=\"com" + index.getIndexId() + "\"  onClick=\"submitWithAction('MemberDocumentDisplayViewCommand','" + index.getLocation() + "')\">View</button></td>"
                                + "<td style=\"text-align: center\"><input type=\"button\" id=\"btnEmail"+index.getIndexId()+"\" name=\"btnEmail"+index.getIndexId()+"\" value=\"Email\" onclick=\"onScreenAction('EmailLatestDocumentCommand', '"+ index.getIndexId() +"');\"/></td>");            
                        }
                        else
                        {
                            out.println("<td><label class=\"label\">" + convertDate(index.getGenerationDate()) + "</label></td>"
                                //                                + "<td><button type=\"submit\" name=\"com"+index.getIndexId()+"\"  onClick=\"document.getElementById('fileLocation').value ='"+ index.getLocation() +"'\">View</button></td></tr>");
                                + "<td><button type=\"submit\" name=\"com" + index.getIndexId() + "\"  onClick=\"submitWithAction('MemberDocumentDisplayViewCommand','" + index.getLocation() + "')\">View</button></td>");
                        }
                    } else {
                        out.println("<td><label class=\"label\">" + convertDate(index.getGenerationDate()) + "</label></td></tr>");
                    }

                    if (typeName.equals("Member Contribution Statement")) {
                        out.println(""
                                + " <td><input type=\"text\" name=\"ContactDetails_email\" value=\"" + req.getAttribute("ContactDetails_email") + "\"/></td>"
                                //+" <td><td><button type=\"submit\" name=\"send\"  onClick=\"submitWithAction('SubmitDocumentsCommand','" + index.getLocation() + "');\">Send</button></td></tr>");
                                + " <td><button type=\"button\" name=\"send\" disabled=\"yes\"  onClick=\"document.getElementById('opperationParam').value='sendEmail';submitActionAndFile(this.form, 'SubmitDocumentsCommand','" + index.getLocation() + "');\">Send</button></td></tr>");
                    }
                }
//                out.println("</form></tr></table>");
                out.println("</tr></table>");
            }
            System.out.println("--End tag--");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberDocumentList tag", ex);
        }
        return super.doEndTag();
    }

    private String convertDate(XMLGregorianCalendar cal) {
        if (cal == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm");
        return sdf.format(cal.toGregorianCalendar().getTimeInMillis());

    }

    public void setCommand(String command) {
        this.command = command;
    }

    public void setConfirmCommand(String confirmCommand) {
        this.confirmCommand = confirmCommand;
    }
}

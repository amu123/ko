/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ForwardToProviderStatementCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(ForwardToProviderStatementCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        HttpSession session = request.getSession();
        logger.info("Inside ForwardToProviderStatementCommand");

        session.setAttribute("stmtTypeVal", "Provider");
        try {

            String nextJSP = "/Claims/ProviderStatements.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        }catch(Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {

        return "ForwardToProviderStatementCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.brokerFirm.ConsultantMapping;
import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.BrokerConsultant;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author princes
 */
public class SaveConsultantApplicationCommand  extends NeoCommand{
    
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String result = validateAndSave(request);
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SaveConsultantApplicationCommand";
    }

    private String validateAndSave(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Map<String, String> errors = ConsultantMapping.validate(request);
        List<KeyValueArray> list = null;
        if (request.getParameter("ConsApp_brokerCode") != null && !request.getParameter("ConsApp_brokerCode").isEmpty()) {
            list = port.getBrokerConsultantList(request.getParameter("ConsApp_brokerCode"), "");
            if (list.size() >= 1) {
                if (request.getParameter("consultantEntityId") == null || request.getParameter("consultantEntityId").isEmpty()) {
                    errors.put("ConsApp_brokerCode_error", "Consultant code is already used");
                }
            }
        }
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData;
        String updateFields = null;
        //Integer nomPrac = new Integer (request.getParameter("MemberApp_principalNominatedPractitioner"));
        if ("OK".equalsIgnoreCase(status)) {
            if (save(request)) {
                additionalData = "\"enable_tabs\":\"true\"";
                BrokerConsultant br = (BrokerConsultant) request.getAttribute("ConsultantApplication");
                Map<String, String> fieldsMap = new HashMap<String, String>();
                fieldsMap.put("consultantEntityId", br.getEntityId()+"");
                //fieldsMap.put("brokerCSurname", br.getContactLastName());
                fieldsMap.put("consultant_header_memberName", br.getFirstNames() + " " + br.getSurname());
                fieldsMap.put("consultant_header_applicationNumber", br.getConsultantCode());
                fieldsMap.put("consultantDetailsEntityId", br.getEntityId()+"");
                fieldsMap.put("consultantAppRegion", br.getRegion()+"");
                               
                updateFields = TabUtils.convertMapToJSON(fieldsMap);
                additionalData = additionalData + ",\"message\":\"Broker Consultant Application Saved\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving the broker consultant application.\"";
            }
        } else {
            additionalData = "\"enable_tabs\":\"false\"";
        }
        return TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
    }

    private boolean save(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = getNeoUser(request);
        HttpSession session = request.getSession();
        /**  Need to remove **/
        if (neoUser == null) {
            neoUser = new NeoUser();
            neoUser.setSecurityGroupId(1);
            neoUser.setUserId(1);
        }
        //List<MemberDependantApp>  depeList = new ArrayList<MemberDependantApp>();
        try {
            BrokerConsultant br = ConsultantMapping.ConsultantMapping(request, neoUser);
            System.out.println("Broker Consultant Entity Id " + br.getEntityId());
            br = port.saveConsultantApplication(br);
            System.out.println("New Broker Consultant Entity Common Id " + br.getEntityCommonId());
            request.setAttribute("ConsultantApplication", br);
            session.setAttribute("ConsultantApplication", br);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser)request.getSession().getAttribute("persist_user");
    }

}

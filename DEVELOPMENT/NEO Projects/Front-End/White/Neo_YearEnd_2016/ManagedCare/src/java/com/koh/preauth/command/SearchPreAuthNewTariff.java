/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthTariffDetails;
import neo.manager.NeoManagerBean;
import neo.manager.TariffCode;

/**
 *
 * @author Johan-NB
 */
public class SearchPreAuthNewTariff extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Collection<TariffCode> tList = new ArrayList<TariffCode>();
        Collection<AuthTariffDetails> atList = new ArrayList<AuthTariffDetails>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        //get values from request
        String tariffCode = request.getParameter("code");
        String tariffDesc = request.getParameter("description");

        if (tariffCode == null || tariffCode.equalsIgnoreCase("null")) {
            tariffCode = "";
        }
        if (tariffDesc == null || tariffDesc.equalsIgnoreCase("null")) {
            tariffDesc = "";
        }
        
        String provTypeId = "";
        if (session.getAttribute("providerDiscTypeId") != null) {
            provTypeId = session.getAttribute("providerDiscTypeId").toString();
        } else {
            provTypeId = "" + session.getAttribute("treatingProviderDiscTypeId");
        }

        int pId = Integer.parseInt("" + session.getAttribute("scheme"));
        int oId = Integer.parseInt("" + session.getAttribute("schemeOption"));
        XMLGregorianCalendar authDate = null;
        try {
            authDate = DateTimeUtils.convertDateToXMLGregorianCalendar(sdf.parse("" + session.getAttribute("authDate")));
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        System.out.println("product = " + pId);
        System.out.println("option = " + oId);
        System.out.println("service date = " + authDate);
        System.out.println("prov disc = " + provTypeId);

        tList = port.getTariffCodeByCriteria(pId, oId, provTypeId, tariffCode, tariffDesc, authDate);

        if (tList != null) {
            if (tList.size() == 0) {
                request.setAttribute("authNTSearchResultsMessage", "No results found.");
            } else if (tList.size() >= 100) {
                request.setAttribute("authNTSearchResultsMessage", "Only first 100 results shown. Please refine search.");
            } else {
                request.setAttribute("authNTSearchResultsMessage", null);
            }

            for (TariffCode t : tList) {
                AuthTariffDetails at = new AuthTariffDetails();

                double quantity = 0.0d;
                if (t.getUnits() == 0.0d) {
                    quantity = 1.0d;
                } else {
                    quantity = t.getUnits();
                }
                at.setTariffDesc(t.getDescription());
                at.setTariffCode(t.getCode());
                at.setQuantity(String.valueOf(quantity));
                at.setAmount(t.getPrice());

                at.setProviderType(provTypeId);

                atList.add(at);
            }
            session.setAttribute("authNTSearchResults", atList);
        } else {
            request.setAttribute("authNTSearchResultsMessage", "No results found.");
        }
        try {
            String nextJSP = "/PreAuth/PreAuthNewTariffSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SearchPreAuthNewTariff";
    }
}

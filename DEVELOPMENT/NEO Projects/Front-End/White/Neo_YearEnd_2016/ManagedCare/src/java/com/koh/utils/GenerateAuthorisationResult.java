/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.utils;

import com.agile.security.webservice.AuthDetails;
import com.koh.command.FECommand;
import com.koh.command.NeoCommand;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author johanl
 */
public class GenerateAuthorisationResult extends FECommand {

    private String commandName;
    private String javascript;
    private final NeoCommand neo = new NeoCommand() {

        @Override
        public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public String getName() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    };

    public String redirectAuthType(HttpServletRequest request, HttpServletResponse response, AuthDetails ad) {
        String generatedHtml = "";
        if(ad.getAuthType().equalsIgnoreCase("Dental Authorisation")){
            generatedHtml = generateDentalAuthResult(request, response, ad);
        
        }else if(ad.getAuthType().equalsIgnoreCase("Dental Orthodontic")){
             generatedHtml = generateOrthoAuthResult(request, response, ad);
        }
        return generatedHtml;

    }

    public String generateDentalAuthResult(HttpServletRequest request, HttpServletResponse response, AuthDetails ad) {
        String generatedHtml = "";

        generatedHtml = "<html><head><link rel=\"stylesheet\" href=\"resources/styles.css\"/></head><body>" +
                "<div align=\"center\">" +
                "<img align=\"middle\" width=\"600\" height=\"180\" src=\"resources/reso_logo.gif\" alt=\"ResoLogo\" />" +
                "</br>"+
                "<form>" +
                "<table class=\"reso\" width=\"600\" style=\"border-style:none; border-collapse:collapse; border-width:1px;>" +
                "<tr><th><label class=\"reso\">Authorisation Results</label></th></tr>" +
                
                "<tr>" +
                "<td><label class=\"reso\">Provider Number:</label></td><td>" + ad.getTreatingProviderNo() + "</td>" +
                "<td><label class=\"reso\">Provider Name:</label></td><td>" + ad.getTreatingProvider() + "</td>" +
                "</tr> "+

                "<tr> "+
                "<td><label class=\"reso\">Member Number:</label></td><td>" + ad.getMemberNo() + "</td>" +
                "<td><label class=\"reso\">Dependant Number:</label></td><td>" + ad.getDependantCode() + "</td>" +
                "</tr> "+

                "<tr>" +
                "<td><label class=\"reso\">Name:</label></td><td>" + ad.getFullnames() + "</td>" +
                "<td><label class=\"reso\">Surname:</label></td><td>" + ad.getSurname() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Scheme:</label></td><td>" + ad.getScheme() + "</td>" +
                "<td><label class=\"reso\">Plan:</label></td><td>" + ad.getSchemeOption() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Caller contact:</label></td><td align=\"left\" colspan=\"3\">" + ad.getCallerContact() + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td><label class=\"reso\">Authorisation Number:</label></td><td>" + ad.getAuthCode() + "</td>" +
                "<td><label class=\"reso\">Authorisation Type:</label></td><td>" + ad.getAuthType() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Authorisation Status:</label></td><td align=\"left\" colspan=\"3\">" + ad.getAuthStatus() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Admission Date:</label></td><td>" + ad.getAdmissionDate() + "</td>" +
                "<td><label class=\"reso\">Discharge Date:</label></td><td>" + ad.getDischargeDate() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Authorisation Date:</label></td><td align=\"left\" colspan=\"3\">" + ad.getAuthorisationDate() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Primary IDC10 Code:</label></td><td>" + ad.getPrimaryIcd() + "</td>" +
                "<td><label class=\"reso\">Treatment Code(s):</label></td><td>" + ad.getTarrifList() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Procedure Code(s):</label></td><td>" + ad.getLabCodeList() + "</td>" +
                "<td><label class=\"reso\">Secondary ICD10:</label></td><td>" + ad.getSecondaryIcd() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Co-Morbidity ICD10:</label></td><td align=\"left\" colspan=\"3\">" + ad.getTertiaryIcd() + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td colspan=\"4\" align=\"right\"><button type=\"submit\" name=\"operation\" > Export </button></td>" +
                "</tr>"+
                "</table>" +
                "</form>" +
                "</br>" +
                "<label class=\"subheader\">Disclaimer</label><br/><p>Resolution health is....</p>" +
                "</div>" +
                "</body>" +
                "</html> ";

        return generatedHtml;
    }

    public String generateOrthoAuthResult(HttpServletRequest request, HttpServletResponse response, AuthDetails ad) {
        String generatedHtml = "";
        generatedHtml = "<html><head><link rel=\"stylesheet\" href=\"resources/styles.css\"/></head><body>" +
                "<div align=\"center\">" +
                "<img align=\"middle\" width=\"600\" height=\"180\" src=\"resources/reso_logo.gif\" alt=\"ResoLogo\" />" +
                "</br>"+
                "<form>" +
                "<table class=\"reso\" width=\"600\" style=\"border-style:none; border-collapse:collapse; border-width:1px;>" +
                "<tr><th><label class=\"reso\">Authorisation Results</label></th></tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Provider Number:</label></td><td>" + ad.getTreatingProviderNo() + "</td>" +
                "<td><label class=\"reso\">Provider Name:</label></td><td>" + ad.getTreatingProvider() + "</td>" +
                "</tr> "+

                "<tr> "+
                "<td><label class=\"reso\">Member Number:</label></td><td>" + ad.getMemberNo() + "</td>" +
                "<td><label class=\"reso\">Dependant Number:</label></td><td>" + ad.getDependantCode() + "</td>" +
                "</tr> "+

                "<tr>" +
                "<td><label class=\"reso\">Name:</label></td><td>" + ad.getFullnames() + "</td>" +
                "<td><label class=\"reso\">Surname:</label></td><td>" + ad.getSurname() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Scheme:</label></td><td>" + ad.getScheme() + "</td>" +
                "<td><label class=\"reso\">Plan:</label></td><td>" + ad.getSchemeOption() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Caller contact:</label></td><td align=\"left\" colspan=\"3\">" + ad.getCallerContact() + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td><label class=\"reso\">Authorisation Number:</label></td><td>" + ad.getAuthCode() + "</td>" +
                "<td><label class=\"reso\">Authorisation Type:</label></td><td>" + ad.getAuthType() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Authorisation Status:</label></td><td align=\"left\" colspan=\"3\">" + ad.getAuthStatus() + "</td>" +
                "</tr>" +


                "<tr>" +
                "<td><label class=\"reso\">Tariff Code(s):</label></td><td>" + ad.getTarrifList() + "</td>" +
                "<td><label class=\"reso\">Plan Duration:</label></td><td>" + ad.getPlannedDuration() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Total Amount Claimed:</label></td><td>" + ad.getEstimatedCost() + "</td>" +
                "<td><label class=\"reso\">Deposit Amount:</label></td><td>" + ad.getDepositAmount() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">First Installment Amount:</label></td><td>" + ad.getFirstInstallmentAmount() + "</td>" +
                "<td><label class=\"reso\">Remaining Installment Amount:</label></td><td>" + ad.getRemainingInstallmentAmount() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Date Valid From:</label></td><td>" + ad.getDateValidFrom() + "</td>" +
                "<td><label class=\"reso\">Date Valid To:</label></td><td>" + ad.getDateValidTo() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Authorisation Date:</label></td><td align=\"left\" colspan=\"3\">" + ad.getAuthorisationDate() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Primary IDC10 Code:</label></td><td>" + ad.getPrimaryIcd() + "</td>" +
                "<td><label class=\"reso\">Secondary ICD10:</label></td><td>" + ad.getSecondaryIcd() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td><label class=\"reso\">Co-Morbidity ICD10:</label></td><td>" + ad.getTertiaryIcd() + "</td>" +
                "</tr>" +

                "<tr>" +
                "<td colspan=\"4\" align=\"right\"><button type=\"submit\" name=\"operation\" > Export </button></td>" +
                "</tr>"+
                "</table>" +
                "</form>" +
                "</br>" +
                "<div align=\"left\"><label class=\"subheader\">Disclaimer</label><br/><p>Resolution health is....</p></div>" +
                "</div>" +
                "</body>" +
                "</html>";

        return generatedHtml;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

import com.koh.command.Command;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author gerritj
 */
public class LogoutCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            request.getSession().invalidate();
            String redirecttourl = "/ManagedCare/Welcome.jsp";
            StringBuffer sb = new StringBuffer();
            sb.append("<head>\n<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; " + redirecttourl + "\">\n");
            sb.append("<SCRIPT>\n<!-- \n var version = parseInt(navigator.appVersion);\n");
            sb.append("if (version>=4 || window.top.location.replace)\n");
            sb.append("window.top.location.replace(\" " + redirecttourl + "\");\n");
            sb.append("else window.top.location.href = \"" + redirecttourl + "\";\n//-->\n</SCRIPT>\n</head>");
            response.getWriter().println(sb.toString());
            response.getWriter().flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "LogoutCommand";
    }
}

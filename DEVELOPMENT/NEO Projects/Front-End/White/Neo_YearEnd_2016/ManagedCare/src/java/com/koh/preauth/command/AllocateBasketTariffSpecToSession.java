/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.command;

import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Johan-NB
 */
public class AllocateBasketTariffSpecToSession extends Command{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        String provType = request.getParameter("providerCategory");
        String btCode = request.getParameter("btCode");
        String btDesc = request.getParameter("btDesc");
        double tAmount = new Double(request.getParameter("tariffAmount"));
        System.out.println("amount = "+tAmount);
        int tFeq = 1;

        session.setAttribute("providerCategory", provType);
        session.setAttribute("btCode", btCode);
        session.setAttribute("btDesc", btDesc);
        session.setAttribute("tariffAmount", tAmount);
        session.setAttribute("tariffFreq", tFeq);

        try {
            String nextJSP = "/PreAuth/AuthSpecificBasketTariff.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "AllocateBasketTariffSpecToSession";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.dataload.command;

import com.koh.command.FECommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author KatlegoM
 */
public class AcceptStagingStatusCommand extends FECommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {


        try {
            
            RequestDispatcher dispatcher = request.getRequestDispatcher("/SystemAdmin/DataLoader.jsp");
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return "AcceptStagingStatusCommand";
    }

    @Override
    public String getName() {
        return "AcceptStagingStatusCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.dataload.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author josephm
 */
public class CancelUploadCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.clearAllFromSession(request);

       System.out.println("Inside CancelUploadCommand");

       try {

           String nextJSP = "/dataloader/GenericFileLoader.jsp";
           RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
           dispatcher.forward(request, response);

       }catch(Exception ex) {

           ex.printStackTrace();
       }
       return null;
    }

    @Override
    public String getName() {

        return "CancelUploadCommand";
    }

}

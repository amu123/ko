/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author johanl
 */
public class AddAuthNappiToSession extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        try {
            //

            String provType = request.getParameter("provCatType");
            System.out.println("provider type = " + provType);
            String napCode = request.getParameter("nCode");
            String napDesc = request.getParameter("nDesc");
            String napAmount = request.getParameter("nAmount");
            System.out.println("nappi amount = " + napAmount);
            String napDosage = request.getParameter("nDosage");
            String napQuan = request.getParameter("nQuan");
            String napFreq = request.getParameter("nFreq");

            double nAmount = Double.valueOf(napAmount);
            int freq = 0;
            if(napFreq != null && !napFreq.trim().equalsIgnoreCase("")){
                freq = new Integer(napFreq);
            }


            List<AuthTariffDetails> nappiList = (List<AuthTariffDetails>) session.getAttribute("AuthNappiTariffs");
            if (nappiList == null) {
                nappiList = new ArrayList<AuthTariffDetails>();
            }
            //set values to list
            AuthTariffDetails ab = new AuthTariffDetails();
            ab.setProviderType(provType);
            ab.setTariffCode(napCode);
            ab.setTariffDesc(napDesc);
            ab.setAmount(nAmount);
            ab.setFrequency(freq);
            ab.setDosage(napDosage);
            ab.setQuantity(napQuan);
            ab.setTariffType("3");

            nappiList.add(ab);

            session.setAttribute("AuthNappiTariffs", nappiList);

            session.removeAttribute("nappiCode");
            session.removeAttribute("nappiDesc");
            session.removeAttribute("nappiAmount");
            session.removeAttribute("nappiDosage");
            session.removeAttribute("nappiQuan");

            System.out.println("Done|");


        } catch (Exception ex) {
            System.out.println("AddAuthNappiToSession error : " + ex.getMessage());
            System.out.println("Error|");
        }

        try {
            String nextJSP = "/PreAuth/AuthSpecificNappi.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "AddAuthNappiToSession";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.tags;

import com.agile.security.webservice.DentalTCode;
import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author whauger
 */
public class DentalTCodeSearchResultTable extends TagSupport {
    private String javascript;
    private String commandName;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            out.println("<label class=\"header\">Grid Search Results</label></br></br>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Dental Code</th><th>TCode</th><th>Select</th></tr>");

            ArrayList<DentalTCode> tCodes = (ArrayList<DentalTCode>) req.getAttribute("searchDentalTCodeResult");
            if (tCodes != null) {
                for (int i = 0; i < tCodes.size(); i++) {
                    DentalTCode tcode = tCodes.get(i);
                    out.println("<tr><form><td><label class=\"label\">" + tcode.getDentalCode() + "</label><input type=\"hidden\" name=\"code\" value=\"" + tcode.getDentalCode() + "\"/></td>" +
                            "<td><label class=\"label\">" + tcode.getTCode() + "</label><input type=\"hidden\" name=\"description\" value=\"" + tcode.getTCode() + "\"/></td>" +
                            "<td><button name=\"opperation\" name=\"opperation\" type=\"submit\" " + javascript + " value=\"" + commandName + "\">Select</button></td></form></tr>");

                }
            }
            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in DentalTCodeSearchResultTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

}

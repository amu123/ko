/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author princes
 */
public class BulkPrintPoolFilterCommand  extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------BulkPrintPoolFilterCommand---------------------");


            String displayOption = request.getParameter("displayOption");
            String displayDay =  request.getParameter("displayDay");
            String displayGroup =  request.getParameter("displayGroup");
            String displayType =  request.getParameter("displayType");
            
            logger.info("displayOption = " + displayOption);
            logger.info("displayDay = " + displayDay);
            logger.info("displayGroup = " + displayGroup);
            logger.info("displayType = " + displayType);
            
            request.setAttribute("displayOption", displayOption);
            request.setAttribute("displayDay", displayDay);
            request.setAttribute("displayGroup", displayGroup);
            request.setAttribute("displayType", displayType);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/BulkPrintPool.jsp");
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "BulkPrintPoolFilterCommand";
    }

    @Override
    public String getName() {
        return "BulkPrintPoolFilterCommand";
    }
}
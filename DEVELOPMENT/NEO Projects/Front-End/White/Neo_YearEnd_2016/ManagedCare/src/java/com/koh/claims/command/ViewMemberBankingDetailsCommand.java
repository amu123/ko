/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.BankingDetails;
import neo.manager.NeoManagerBean;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ViewMemberBankingDetailsCommand extends NeoCommand {

    private Logger log = Logger.getLogger(ViewProviderDetailsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        log.info("Inside ViewMemberBankingDetailsCommand");

        HttpSession session = request.getSession();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        ArrayList<BankingDetails> bankDetails = (ArrayList<BankingDetails>) session.getAttribute("pMemBankDetails");

        for (BankingDetails bank : bankDetails) {

            if (bank.getAccountUseId() == 3) {
                session.setAttribute("bankName", bank.getBankName());
                session.setAttribute("branchCode", bank.getBranchCode());
                session.setAttribute("branchName", bank.getBranchName());
                session.setAttribute("accountNumber", bank.getAccountNo());
                session.setAttribute("accountName", bank.getAccountName());
                session.setAttribute("accountType", bank.getAccountType());
                session.setAttribute("effectiveBankDate",
                        dateFormat.format(bank.getEffectiveStartDate().toGregorianCalendar().getTime()));

                session.setAttribute("bankNameCon", bank.getBankName());
                session.setAttribute("branchCodeCon", bank.getBranchCode());
                session.setAttribute("branchNameCon", bank.getBranchName());
                session.setAttribute("accountNumberCon", bank.getAccountNo());
                session.setAttribute("accountNameCon", bank.getAccountName());
                session.setAttribute("accountTypeCon", bank.getAccountType());
                session.setAttribute("effectiveBankDateCon",
                        dateFormat.format(bank.getEffectiveStartDate().toGregorianCalendar().getTime()));

            } else {

                if (bank.getAccountUseId() == 1) {
                    session.setAttribute("bankName", bank.getBankName());
                    session.setAttribute("branchCode", bank.getBranchCode());
                    session.setAttribute("branchName", bank.getBranchName());
                    session.setAttribute("accountNumber", bank.getAccountNo());
                    session.setAttribute("accountName", bank.getAccountName());
                    session.setAttribute("accountType", bank.getAccountType());
                    session.setAttribute("effectiveBankDate", dateFormat.format(bank.getEffectiveStartDate().toGregorianCalendar().getTime()));
                }

                if (bank.getAccountUseId() == 2) {
                    session.setAttribute("bankNameCon", bank.getBankName());
                    session.setAttribute("branchCodeCon", bank.getBranchCode());
                    session.setAttribute("branchNameCon", bank.getBranchName());
                    session.setAttribute("accountNumberCon", bank.getAccountNo());
                    session.setAttribute("accountNameCon", bank.getAccountName());
                    session.setAttribute("accountTypeCon", bank.getAccountType());
                    session.setAttribute("effectiveBankDateCon", dateFormat.format(bank.getEffectiveStartDate().toGregorianCalendar().getTime()));
                }
            }

        }

        try {
            //String nextJSP = "/Claims/ProviderDetailsTabbedPage.jsp";
            String nextJSP = "/Claims/MemberBankingDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "ViewMemberBankingDetailsCommand";
    }
}

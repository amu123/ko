/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author johanl
 */
public class AllocateAuthCPTToSession extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        System.out.println("inside AllocateAuthCPTToSession");

        String forField = "" + session.getAttribute("searchCalled");
        String onScreen = "" + session.getAttribute("onScreen");
        System.out.println("forField = "+forField);
        System.out.println("onScreen = "+onScreen);

        String name = forField.substring(forField.length() - 4, forField.length());
        if (name.equalsIgnoreCase("list")) {
            if (session.getAttribute(forField) != null) {
                ArrayList<String> list = (ArrayList<String>) session.getAttribute(forField);
                list.add(request.getParameter("code") + " - " + request.getParameter("description"));
                session.setAttribute(forField, list);
            } else {
                ArrayList<String> list = new ArrayList<String>();
                list.add(request.getParameter("code") + " - " + request.getParameter("description"));
                session.setAttribute(forField, list);
            }

        } else {
            session.setAttribute(forField + "_text", request.getParameter("code"));
            session.setAttribute(forField + "Desc", request.getParameter("description"));

        }
        try {
            String nextJSP = "/PreAuth/AuthSpecificCPT.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "AllocateAuthCPTToSession";
    }

}

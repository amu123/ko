/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthNappiBasketSearch;
import neo.manager.AuthTariffDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class SearchPreAuthNappi extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        this.saveScreenToSession(request);
        NeoManagerBean port = service.getNeoManagerBeanPort();
        List<AuthTariffDetails> medList = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        AuthNappiBasketSearch np = new AuthNappiBasketSearch();

        String provCat = (String) session.getAttribute("provCat");
        String planYear = (String) session.getAttribute("planYear");
        String code = (String) session.getAttribute("code");
        String desc = (String) session.getAttribute("description");
        String basket = (String) session.getAttribute("basketSearch");

        boolean nappiSearchFlag = false;
        if (code != null && !code.trim().equals("")) {
            nappiSearchFlag = true;
            np.setCode(new Integer(code));
        }
        if (desc != null && !desc.trim().equalsIgnoreCase("")) {
            nappiSearchFlag = true;
            np.setName(desc);
        }

        if (provCat != null && !provCat.trim().equals("")) {
            nappiSearchFlag = true;
            np.setCategory(new Integer(provCat));
        }

        if (planYear != null && !planYear.trim().equals("")) {
            nappiSearchFlag = true;
            np.setBasketYear(new Integer(planYear));
        }

        np.setBasketId(1);

        if (nappiSearchFlag != false) {

            if (basket != null && !basket.trim().equalsIgnoreCase("")) {
                medList = port.getNappiBasketDetailsByCriteria(np);
                System.out.println("getNappiBasketDetailsByCriteria");
            } else {
                XMLGregorianCalendar authDate = null;
                try {
                    authDate = DateTimeUtils.convertDateToXMLGregorianCalendar(sdf.parse("" + session.getAttribute("authDate")));
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }

                medList = port.getNappiDetailsByCriteria(code, desc, authDate);
                System.out.println("getNappiDetailsByCriteria");
            }

            //List<AuthTariffDetails> basketList = new ArrayList<AuthTariffDetails>();
            if (medList != null) {
                System.out.println("medList size = " + medList.size());
                session.setAttribute("PreAuthNappiSearchList", medList);
            }
        }
        try {

            String nextJSP = null;

            if (basket != null && !basket.trim().equalsIgnoreCase("")) {
                nextJSP = "/PreAuth/PreAuthBasketNappiSearch.jsp";
            } else {
                nextJSP = "/PreAuth/PreAuthNappiSearch.jsp";
            }

            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SearchPreAuthNappi";
    }
}

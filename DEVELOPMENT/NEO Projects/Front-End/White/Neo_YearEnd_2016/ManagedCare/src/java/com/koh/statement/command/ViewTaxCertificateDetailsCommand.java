/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.FormatUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.ContributionTransactionHistoryView;
import neo.manager.CoverProductDetails;
import neo.manager.NeoManagerBean;
import org.apache.log4j.Logger;

/**
 *
 * @author almaries
 */
public class ViewTaxCertificateDetailsCommand extends NeoCommand{

    private Logger logger = Logger.getLogger(ViewTaxCertificateDetailsCommand.class);
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        logger.info("Inside ViewTaxCertificateDetailsCommand");

        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        
        String actionType = request.getParameter("opperationParam");
        String nextJSP = "";

        logger.info("The context path is " + context.getContextPath());
        
        if(actionType != null && actionType.equalsIgnoreCase("Details")){
            String contribAmount = "";
            String claimsAmount = "";
            double amount;
            
            String memberNo = request.getParameter("memberNo_text");
            session.setAttribute("policyHolderNumber", memberNo);
            String yearRange = request.getParameter("yearRange");
            session.setAttribute("yearRange", yearRange);
            int taxYear = Integer.parseInt(yearRange);
                        
            amount = port.fetchMemberContribTaxCertDetails(memberNo, taxYear);           
            logger.info("The contribution summary amount is " + amount);
            if(amount <= 0){
                amount = 0;
                contribAmount = "0"+FormatUtils.decimalFormat.format(amount);
            }
            else if(amount > 0){
                contribAmount = FormatUtils.decimalFormat.format(amount);
            }
            
            amount = port.fetchMemberClaimsTaxCertDetails(memberNo, taxYear);
            logger.info("The claims summary amount is " + amount);
            if(amount <= 0){
                amount = 0;
                claimsAmount = "0"+FormatUtils.decimalFormat.format(amount);
            }
            else if(amount > 0){
                claimsAmount = FormatUtils.decimalFormat.format(amount);
            }
            
            CoverProductDetails coverDetails = port.getProductDetailsDespiteCoverStatus(memberNo);
            
            session.setAttribute("ContributionAmount", contribAmount);
            session.setAttribute("ClaimsAmount", claimsAmount);
            session.setAttribute("productId", coverDetails.getProductId());
            
            logger.info("Details_MemberNumber: " + memberNo);
            logger.info("Details_YearRange: " + yearRange);
            logger.info("Details_ContributionAmount: " + contribAmount);
            logger.info("Details_ClaimsAmount: " + claimsAmount);
            
            nextJSP = "/Statement/TaxCertificateSummary.jsp";
            
            try {
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        else if(actionType != null && actionType.equalsIgnoreCase("Contributions")){
            String memberNum = (String)session.getAttribute("policyHolderNumber");
            String yearRange = (String)session.getAttribute("yearRange");
            session.setAttribute("coverNum", memberNum);//this is for the export to excel button
            
            int year = Integer.parseInt(yearRange);
            String startDateStr = Integer.toString(year-1)+"/03/01";
            Date startDate = parseDate(startDateStr);
            session.setAttribute("startDate", startDateStr);
            
            String endDateStr = yearRange+"/03/01";
            Date endDate = parseEndDate(endDateStr);
            session.setAttribute("endDate", endDateStr);
            
            Date creationDate = new Date();
            
            logger.info("Contributions_MemberNumber: " + memberNum);
            logger.info("Contributions_YearRange: " + yearRange);
            logger.info("Contributions_StartDate: " + startDate);
            logger.info("Contributions_EndDate: " + endDate);
            logger.info("Contributions_CreationDate: " + creationDate);
            //fetchMemberContributionsHistory
            List<ContributionTransactionHistoryView> memberContributionsHistory = port.fetchTaxCertContributionsHistory(memberNum, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), DateTimeUtils.convertDateToXMLGregorianCalendar(endDate), DateTimeUtils.convertDateToXMLGregorianCalendar(creationDate));
            logger.info("memberContributionsHistory total  : " + memberContributionsHistory.size());
            request.setAttribute("Contributions", memberContributionsHistory);
            
            nextJSP = "/Statement/TaxCertificateContributions.jsp";
            try {
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        else if(actionType != null && actionType.equalsIgnoreCase("Return")){
            
            logger.info("Return_MemberNumber: " + session.getAttribute("policyHolderNumber"));
            logger.info("Return_YearRange: " + session.getAttribute("yearRange"));
            logger.info("Return_ContributionAmount: " + session.getAttribute("ContributionAmount"));
            logger.info("Return_ClaimsAmount: " + session.getAttribute("ClaimsAmount"));
            
            nextJSP = "/Statement/TaxCertificateSummary.jsp";

            try {
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        
        return null;
    }

    @Override
    public String getName() {
        return "ViewTaxCertificateDetailsCommand";
    }
    
    public Date parseDate(String tmpDate){
        Date date = null;
        
        try {
            date = FormatUtils.dateFormat.parse(tmpDate);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return date;
    }
    
    public Date parseEndDate(String endDate){
        Date enDate = null;
        
        try {
            SimpleDateFormat date = new SimpleDateFormat("yyyy/MM/dd");
            Calendar c = Calendar.getInstance();
            c.setTime(date.parse(endDate));
            c.add(Calendar.DATE, -1);
            endDate = date.format(c.getTime());
            
            enDate = FormatUtils.dateFormat.parse(endDate);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return enDate;
    }
}

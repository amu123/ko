/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import com.koh.cover.MemberMainMapping;
import com.koh.cover.commands.SaveNominatedProviderInfoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.fe.command.LoginCommand;
import com.koh.member.application.command.SaveMemberApplicationCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverAdditionalInfo;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author janf
 */
public class SaveMemberNominatedProviderCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String s = request.getParameter("buttonPressed");
        System.out.println("in SaveMemberNominatedProviderCommand");
        System.out.println("buttonPressed " + s);

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("optometristSearch".trim())) {
                try {
                    searchOptometrist(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }

            if (s.equalsIgnoreCase("doctorSearch".trim())) {
                try {
                    searchDoctor(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }

            if (s.equalsIgnoreCase("dentistSearch".trim())) {
                try {
                    searchDentist(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }

//            if (s.equalsIgnoreCase("SaveDSP".trim())) {
//                try {
//                    saveDSP(port, request, response, context);
//                } catch (ServletException ex) {
//                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
//                } catch (IOException ex) {
//                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                return null;
//            }
        }

        String result = validateAndSave(port, request);

        try {
            PrintWriter out = response.getWriter();
            out.println(result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    private String validateAndSave(NeoManagerBean port, HttpServletRequest request) {
        String additionalData = null;
        String status = "";
        String validationErros = "";

        try {
            if (saveDSP(port, request)) {
                status = "OK";
                additionalData = "\"message\":\"Member Additional Details Updated\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error updating the member additional details.\"";
            }
        } catch (ServletException ex) {
            Logger.getLogger(SaveMemberNominatedProviderCommand.class.getName()).log(Level.SEVERE, null, ex);
            status = "ERROR";
            additionalData = "\"message\":\"There was an error updating the member additional details.\"";
        } catch (IOException ex) {
            Logger.getLogger(SaveMemberNominatedProviderCommand.class.getName()).log(Level.SEVERE, null, ex);
            status = "ERROR";
            additionalData = "\"message\":\"There was an error updating the member additional details.\"";
        }

        return TabUtils.buildJsonResult(status, validationErros, "", additionalData);

    }

    private boolean saveDSP(NeoManagerBean port, HttpServletRequest request) throws ServletException, IOException {
        System.out.println("SAVEDPS CALLED");
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        HttpSession session = request.getSession();
        boolean success = true;
        int res = 0;
        Integer entityID = (Integer) session.getAttribute("memberEntityCommon");

        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);

        //GP
        CoverAdditionalInfo memDoct = new CoverAdditionalInfo();
        String practiceNimber = request.getParameter("MAI_doctorPracticeNumber");
        Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_doctorPracticeStartDate"));

        memDoct.setInfoTypeId(21);
        memDoct.setInfoValue(practiceNimber);
        memDoct.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        memDoct.setEntityCommonId(entityID);
        memDoct.setCreatedBy(sec.getCreatedBy());
        memDoct.setLastUpdatedBy(sec.getLastUpdatedBy());
        memDoct.setSecurityGroupId(sec.getSecurityGroupId());
        res = port.saveCoverAdd(memDoct);
        if (res == 0) {
            success = false;
        }

        // Dentist
        CoverAdditionalInfo memDent = new CoverAdditionalInfo();
        practiceNimber = request.getParameter("MAI_dentistPracticeNumber");
        startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_dentistPracticeStartDate"));

        memDent.setInfoTypeId(22);
        memDent.setInfoValue(practiceNimber);
        memDent.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        memDent.setEntityCommonId(entityID);
        memDent.setCreatedBy(sec.getCreatedBy());
        memDent.setLastUpdatedBy(sec.getLastUpdatedBy());
        memDent.setSecurityGroupId(sec.getSecurityGroupId());
        res = port.saveCoverAdd(memDent);
        if (res == 0) {
            success = false;
        }

        //Optometrist
        CoverAdditionalInfo memOpt = new CoverAdditionalInfo();
        practiceNimber = request.getParameter("MAI_dentistPracticeNumber");
        startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_dentistPracticeStartDate"));

        memOpt.setInfoTypeId(23);
        memOpt.setInfoValue(practiceNimber);
        memOpt.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        memOpt.setEntityCommonId(entityID);
        memOpt.setCreatedBy(sec.getCreatedBy());
        memOpt.setLastUpdatedBy(sec.getLastUpdatedBy());
        memOpt.setSecurityGroupId(sec.getSecurityGroupId());
        res = port.saveCoverAdd(memOpt);
        if (res == 0) {
            success = false;
        }

        return success;
    }

    private void searchDoctor(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }

        request.setAttribute("practiceName", "DSP_doctorPracticeName");
        request.setAttribute("practicePracticeNumber", "DSP_doctorPracticeNumber");
        request.setAttribute("practiceNetwork", "DSP_doctorPracticeNetwork");

        context.getRequestDispatcher("/MemberApplication/MemberAppSearchPractice.jsp").forward(request, response);
    }

    private void searchDentist(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }

        request.setAttribute("practiceName", "DSP_dentistPracticeName");
        request.setAttribute("practicePracticeNumber", "DSP_dentistPracticeNumber");
        request.setAttribute("practiceNetwork", "DSP_dentistPracticeNetwork");

        context.getRequestDispatcher("/MemberApplication/MemberAppSearchPractice.jsp").forward(request, response);
    }

    private void searchOptometrist(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        System.out.println("GETTING PARAMETER MAP");
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }

        request.setAttribute("practiceName", "DSP_optometristPracticeName");
        request.setAttribute("practicePracticeNumber", "DSP_optometristPracticeNumber");
        request.setAttribute("practiceNetwork", "DSP_optometristPracticeNetwork");

        context.getRequestDispatcher("/MemberApplication/MemberAppSearchPractice.jsp").forward(request, response);
    }

    @Override
    public String getName() {
        return "SaveMemberNominatedProviderCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class PrintPoolPauseCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------In PrintPoolPauseCommand---------------------");
            String displayOption = request.getParameter("selectedDisplayOption");
            String displayDay = request.getParameter("selectedDisplayDay");
            String displayGroup = request.getParameter("selectedDisplayGroup");
            String displayType = request.getParameter("selectedDisplayType");
            logger.info("displayOption = " + displayOption);
            logger.info("displayDay = " + displayDay);
            logger.info("displayGroup = " + displayGroup);
            logger.info("displayType = " + displayType);
            request.setAttribute("displayOption", displayOption);
            request.setAttribute("displayDay", displayDay);
            request.setAttribute("displayGroup", displayGroup);
            request.setAttribute("displayType", displayType);
            
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/PrintBulk.jsp");
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            ex.printStackTrace();
        }
        return "PrintPoolPauseCommand";
    }

    private void errorMessage(HttpServletRequest request, HttpServletResponse response, String msg) throws Exception {
        request.setAttribute("errorMsg", msg);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/PrintBulk.jsp");
        dispatcher.forward(request, response);
    }



    @Override
    public String getName() {
        return "PrintPoolPauseCommand";
    }
}
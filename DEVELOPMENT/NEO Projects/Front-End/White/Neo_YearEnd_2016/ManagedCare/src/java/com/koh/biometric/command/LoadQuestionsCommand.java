/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AsthmaBiometrics;
import neo.manager.Biometrics;

/**
 *
 * @author josephm
 */
public class LoadQuestionsCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        String memberNumber = request.getParameter("memberNumber");
       int  code = 0;// Integer.parseInt(request.getParameter("depListValues"));
        //System.out.println("the drop down is " + code);
        //System.out.println("the member number is " + memberNumber);
        //System.out.println("The dependant code is " + request.getParameter("depListValues"));

        AsthmaBiometrics asthma = new AsthmaBiometrics();

        Biometrics biometrics =  service.getNeoManagerBeanPort().fetchBiometricsTypeId(asthma, memberNumber, code);

        try {
            PrintWriter out = response.getWriter();

            if(biometrics != null && biometrics.getWeight() != 0.0) {

               out.print(getName() + "|Weight=" + biometrics.getWeight());
               out.print("|Exercise=" + biometrics.getExercisePerWeek());
               out.print("|Length=" + biometrics.getHeight());
               out.print("|BMI=" + biometrics.getBmi());
               out.print("|BloodPressureSystolic=" + biometrics.getBloodPressureSystolic());
               out.print("|BloodPressuredDiastolic=" + biometrics.getBloodPressureDiastolic());
               out.print("|AlcoholConsumption=" + biometrics.getAlcoholUnitsPerWeek() + "$");

            }else {

                out.print("Error|No Such member|" + getName());
            }

           //System.out.println("The weith is " + biometrics.getWeight());

        }catch(Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "LoadQuestionsCommand";
    }
}

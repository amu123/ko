/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.tags;

import com.koh.command.NeoCommand;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthCoverExclusions;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class CoverDependentConditionSpecific extends TagSupport {

    private String commandName;
    private String javaScript;
    private String sessionAttribute;
    private String onScreen;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

        try {
            List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute(sessionAttribute);

            if (cdList != null && cdList.isEmpty() == false) {
                //get dependant 
                int depCode = -1;
                if (session.getAttribute("depListValues") != null) {
                    String depCodeStr = "" + session.getAttribute("depListValues");
                    System.out.println("Member search grid depCodeStr = " + depCodeStr);
                    if (depCodeStr != null && !depCodeStr.equalsIgnoreCase("") && !depCodeStr.equalsIgnoreCase("null")) {
                        depCode = Integer.parseInt(depCodeStr);
                    }
                }

                out.println("<table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.println("<tr>"
                        + "<th align=\"left\">Dependant Code</th>"
                        + "<th align=\"left\">Dependant Name</th>"
                        + "<th align=\"left\">Dependant Surname</th>"
                        + "<th align=\"left\">Birth Date</th>"
                        + "<th align=\"left\">Start Date</th>"
                        + "<th align=\"left\">End Date</th>"
                        + "<th align=\"left\">ICD10 Code</th>"
                        + "<th align=\"left\">Description</th>"
                        + "<th align=\"left\"></th>"
                        + "</tr>");


                for (CoverDetails cd : cdList) {
                    //set cover details 
                    String dateOfBirth = "";
                    String cCode = "";
                    String cDescription = "";

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

                    //dob
                    if (cd.getDateOfBirth() != null) {
                        dateOfBirth = dateFormat.format(cd.getDateOfBirth().toGregorianCalendar().getTime());
                    }

                    //dob
                    List<AuthCoverExclusions> underwriting = port.getActiveCoverExclusionsByCoverDependant(cd.getCoverNumber(), cd.getDependentNumber() + "");
                    System.out.println("Waiting Periods size " + underwriting.size() + " depNumber " + cd.getDependentNumber() + " cover Number " + cd.getCoverNumber());
                    int counter = 0;
                    if (underwriting != null && underwriting.size() > 0) {
                        for (AuthCoverExclusions ce : underwriting) {
                            String startDate = "";
                            String endDate = "";
                            if (ce.getExclusionType().equals("1")) {
                                if (ce.getDiagnosisCode() != null) {
                                    cCode = ce.getDiagnosisCode();
                                }

                                if (ce.getDescription() != null) {
                                    cDescription = ce.getDescription();
                                }

                                //start date
                                if (ce.getExclusionDateFrom() != null) {
                                    startDate = dateFormat.format(ce.getExclusionDateFrom().toGregorianCalendar().getTime());
                                }

                                //end date
                                if (ce.getExclusionDateTo() != null) {
                                    endDate = dateFormat.format(ce.getExclusionDateTo().toGregorianCalendar().getTime());
                                }
                                out.println("<tr>"
                                        + "<td><label class=\"label\">" + cd.getDependentNumber() + "</label></td>"
                                        + "<td><label class=\"label\">" + cd.getName() + "</label></td>"
                                        + "<td><label class=\"label\">" + cd.getSurname() + "</label></td>"
                                        + "<td><label class=\"label\">" + dateOfBirth + "</label></td>"
                                        + "<td><label class=\"label\">" + startDate + "</label></td>"
                                        + "<td><label class=\"label\">" + endDate + "</label></td>"
                                        + "<td><label class=\"label\">" + cCode + "</label></td>"
                                        + "<td><label class=\"label\">" + cDescription + "</label></td>");
                                if (javaScript != null && !javaScript.equalsIgnoreCase("")) {
                                    if (javaScript.equalsIgnoreCase("memberDepSelect")) {
                                        out.println("<td><input type=\"button\" name=\"buttonDel" + counter + "\" id=\"buttonDel" + counter + "\" value=\"Delete\" onclick=\"document.getElementById('buttonPressedUnderwriting').value = 'delConSpecific';document.getElementById('writingEntityId').value = '"
                                                + cd.getEntityId() + "';document.getElementById('exclutionType').value = '" + ce.getExclusionType() + "';document.getElementById('ICDCode').value = '" + cCode + "';document.getElementById('ICDDescription').value = '" + cDescription + "';document.getElementById('exclutionId').value = '" + ce.getExclusionDetailId() + "';$('#buttonDel" + counter + "').attr('disabled',true);submitFormWithAjaxPost(this.form, 'MemberUnderwriting')\"></td>");
                                    } else {
                                        out.println("<td><button name=\"opperation\" type=\"button\" " + javaScript + " >Delete</button></td>");
                                    }
                                }
                                out.println("</tr>");
                                counter++;
                            }

                        }
                    }/*
                     * else {
                     *
                     * if (cd.getPenaltyLateJoin().equals("0")) {
                     * cd.setPenaltyLateJoin("None"); } out.println("<tr>" +
                     * "<td><label class=\"label\">" + cd.getDependentNumber() +
                     * "</label></td>" + "<td><label class=\"label\">" +
                     * cd.getName() + "</label></td>" + "<td><label
                     * class=\"label\">" + cd.getSurname() + "</label></td>" +
                     * "<td><label class=\"label\">" + dateOfBirth +
                     * "</label></td>" + "<td><label class=\"label\">" + cCode +
                     * "</label></td>" + "<td><label class=\"label\">" +
                     * cDescription + "</label></td>"); }
                     */


                }
                out.println("</table>");

            }
        } catch (java.io.IOException ex) {
            throw new JspException("Error in CoverDependentPenaltyJoin tag", ex);
        }

        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setOnScreen(String onScreen) {
        this.onScreen = onScreen;
    }
}

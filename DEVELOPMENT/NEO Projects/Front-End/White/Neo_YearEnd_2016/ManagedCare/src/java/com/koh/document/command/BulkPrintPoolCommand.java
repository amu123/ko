/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;

import agility.za.documentprinting.DocumentPrintingRequest;
import agility.za.documentprinting.DocumentPrintingResponse;
import agility.za.documentprinting.PrintDocType;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author princes
 */
public class BulkPrintPoolCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------In BulkPrintPoolCommand---------------------");

            String selectedFile = request.getParameter("selectedFile");
            String selectedId = request.getParameter("selectedId");
            String displayOption = request.getParameter("displayOption");
            logger.info("selectedFile = " + selectedFile);
            logger.info("BatchId = " + selectedId);
            logger.info("displayOption = " + displayOption);
            if (selectedFile != null && !selectedFile.trim().equals("")) {
                DocumentPrintingRequest req = new DocumentPrintingRequest();
                NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
                req.setAgent(loggedInUser.getUsername());
                req.setPrintProcessType("PrintDoc");
                
                req.setSrcPrintDrive(new PropertiesReader().getProperty("DocumentIndexIndexDrive"));
                PrintDocType docType = new PrintDocType(); 
//                docType.setFolder(new PropertiesReader().getProperty("PrintPoolFolder"));//removed after changes
                docType.setFilename(selectedFile);
                docType.setPrintId(new Long(selectedId));
                logger.info("Print");
                req.setPrintDoc(docType);
                NeoCommand.service.getNeoManagerBeanPort().setBulkStatusToPrinting(new Integer(selectedId), 5);
                DocumentPrintingResponse resp = NeoCommand.service.getNeoManagerBeanPort().processBulkPrintRequest(req);
                if(resp.isIsSuccess()){
                   logger.info("Print successful");
                } else {
                    logger.error("Error printing .... " + resp.getMessage());
                    errorMessage(request, response, resp.getMessage());
                    return "BulkPrintPoolCommand";
                }
            }
            request.setAttribute("displayOption", displayOption);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/BulkPrintPool.jsp");
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "BulkPrintPoolCommand";
    }
    
    private void errorMessage(HttpServletRequest request, HttpServletResponse response, String msg) throws Exception {
        request.setAttribute("errorMsg", msg);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/BulkPrintPool.jsp");
        dispatcher.forward(request, response);
    }   
    
    @Override
    public String getName() {
        return "BulkPrintPoolCommand";
    }
}
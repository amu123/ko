/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.employer;

import com.koh.employer.command.TabUtils;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import neo.manager.BankingDetails;
import neo.manager.EmployerBilling;
import neo.manager.NeoUser;

/**
 *
 * @author yuganp
 */
public class EmployerBillingMap {

    private static final String[][] FIELD_MAPPINGS = {
        {"EmployerGroup_entityId", "", "no", "int", "EmployerBilling", "EmployerEntityId"},
        {"EmployerBilling_method", "Billing Method", "no", "int", "EmployerBilling", "BillingMethod"},
        {"EmployerBilling_scheduleDate", "Schedule Day", "no", "int", "EmployerBilling", "ScheduleDay"},
        {"EmployerBilling_fileFormat", "File Format", "no", "int", "EmployerBilling", "fileFormat"},
        {"EmployerBilling_sortInd", "Sort Method", "no", "int", "EmployerBilling", "sortInd"},
        {"EmployerBilling_contact", "Contact Person", "no", "str", "EmployerBilling", ""},
        {"EmployerBilling_name", "Name", "no", "str", "EmployerBilling", "BillingName"},
        {"EmployerBilling_designation", "Designation", "no", "str", "EmployerBilling", "BillingDesignation"},
        {"EmployerBilling_tel", "Telephone No.", "no", "str", "EmployerBilling", "BillingTelNo"},
        {"EmployerBilling_email", "Email", "no", "str", "EmployerBilling", "BillingEmail"},
        //        {"EmployerBilling_optionInd","Prefered Option","no", "int", "EmployerBilling","PreferredOptionInd"},
        //        {"EmployerBilling_option","Option","no", "int", "EmployerBilling","PreferredOptionId"},
        {"EmployerBilling_activeInd", "Active Members", "no", "int", "EmployerBilling", "BillGroup"},
        //        {"EmployerBilling_pensionerInd","Pensioner Members","no", "int", "EmployerBilling","PensionerMembersInd"},
        //        {"EmployerBilling_pensionerDetails","Pensioner Details","no", "str", "EmployerBilling","PensionerMembersOther"},
        {"EmployerBilling_paidBy", "Paid By", "yes", "int", "EmployerBilling", "paidBy"},
        {"EmployerBilling_splitValue", "Split Value", "no", "bd", "EmployerBilling", "splitValue"},
        {"EmployerBilling_splitType", "Split Type", "no", "int", "EmployerBilling", "splitType"},
        {"EmployerBilling_splitBy", "Split Refers To", "no", "int", "EmployerBilling", "splitBy"},
        {"EmployerBilling_Payment_paymentMethod", "Payment Method", "yes", "int", "EmployerBilling", "paymentMethod"},
        {"EmployerBilling_Payment_date", "Debit Order Day", "no", "int", "EmployerBilling", "debitOrderDay"},
        {"EmployerBilling_Payment_name1", "Full Name", "no", "str", "EmployerBilling", "AuthByName1"},
        {"EmployerBilling_Payment_surname1", "Surname", "no", "str", "EmployerBilling", "AuthBySurname1"},
        {"EmployerBilling_Payment_designation1", "Designation", "no", "str", "EmployerBilling", "AuthByDesig1"},
        {"EmployerBilling_Payment_name2", "Full Name", "no", "str", "EmployerBilling", "AuthByName2"},
        {"EmployerBilling_Payment_surname2", "Surname", "no", "str", "EmployerBilling", "AuthBySurname2"},
        {"EmployerBilling_Payment_designation2", "Designation", "no", "str", "EmployerBilling", "AuthByDesig2"},
        {"EmployerBilling_maxAmount", "Maximum Amount", "no", "bd", "EmployerBilling", "maxAmount"},
        
        {"EmployerBilling_Payment_bankName", "Name of Bank", "no", "int", "EmployerPayment", ""},
        {"EmployerBilling_Payment_bankBranchId", "Branch", "no", "int", "EmployerPayment", ""},
        //{"EmployerBilling_Payment_branchCode", "Branch Code", "no", "str", "EmployerPayment", ""},
        {"EmployerBilling_Payment_accountType", "Account Type", "no", "int", "EmployerPayment", ""},
        {"EmployerBilling_Payment_accountHolder", "Account Holder", "no", "str", "EmployerPayment", ""},
        {"EmployerBilling_Payment_accountNumber", "Account no.", "no", "numberField", "EmployerPayment", ""}
    };
    private static final Map<String, String[]> FIELD_MAP;

    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }

    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
        return errors;
    }

    public static EmployerBilling getEmployerBilling(HttpServletRequest request, NeoUser neoUser) {
        EmployerBilling eg = (EmployerBilling) TabUtils.getDTOFromRequest(request, EmployerBilling.class, FIELD_MAPPINGS, "EmployerBilling");         
        if(request.getParameter("EmployerBilling_maxAmount") == null || request.getParameter("EmployerBilling_maxAmount").equals("")){
            eg.setMaxAmount(null);
        }
        
        // If percentage not selected reset max amount
        if(request.getParameter("EmployerBilling_splitType") != null && !request.getParameter("EmployerBilling_splitType").equals("2")) {
            eg.setMaxAmount(null);
        }       
        
        eg.setSecurityGroupId(neoUser.getSecurityGroupId());
        eg.setLastUpdatedBy(neoUser.getUserId());
        return eg;
    }

    public static void setEmployerBilling(HttpServletRequest request, EmployerBilling employerBilling) {
        TabUtils.setRequestFromDTO(request, employerBilling, FIELD_MAPPINGS, "EmployerBilling");
    }

    public static List<BankingDetails> getBankingDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<BankingDetails> cdl = new ArrayList<BankingDetails>();
        /*
         * if (request.getParameter("BankingDetails_contri_bankName") != null &&
         * !request.getParameter("BankingDetails_contri_bankName").isEmpty()) {
         * cdl.add(getBankingDetails(request, "BankingDetails_contri_", 2)); }
         * if (request.getParameter("BankingDetails_pay_bankName") != null &&
         * !request.getParameter("BankingDetails_pay_bankName").isEmpty()) {
         * cdl.add(getBankingDetails(request, "BankingDetails_pay_", 1)); }
         */


        //cdl.add(getBankingDetails(request, "BankingDetails_contri_", 2));


        cdl.add(getBankingDetails(request, "EmployerBilling_Payment_", 1));

        return cdl;
    }

    private static BankingDetails getBankingDetails(HttpServletRequest request, String param, int accUse) {
        BankingDetails bd = new BankingDetails();
        String paymentMethod = accUse == 1 ? request.getParameter(param + "paymentMethod") : "B";

        bd.setPaymentMethodId(paymentMethod);
        bd.setAccountUseId(accUse);
        if (request.getParameter(param + "bankName") != null && !request.getParameter(param + "bankName").isEmpty()) {
            bd.setBankId(Integer.parseInt(request.getParameter(param + "bankName")));
        }
        if (request.getParameter(param + "bankBranchId") != null && !request.getParameter(param + "bankBranchId").isEmpty()) {
            bd.setBranchId(Integer.parseInt(request.getParameter(param + "bankBranchId")));
        }
        bd.setAccountTypeId(request.getParameter(param + "accountType"));
        bd.setAccountName(request.getParameter(param + "accountHolder"));
        bd.setAccountNo(request.getParameter(param + "accountNumber"));

        return bd;
    }

    public static void setBankingDetails(HttpServletRequest request, Collection<BankingDetails> bankingDetails) {
        if (bankingDetails == null) {
            return;
        }
        for (BankingDetails bd : bankingDetails) {
            if (bd == null || bd.getAccountUseId() == 3) {
                continue;
            }
            //String fieldStart = bd.getAccountUseId() == 2 ? "BankingDetails_contri_" : "BankingDetails_pay_";
            String fieldStart = "EmployerBilling_Payment_";
            System.out.println("Payment from database " + bd.getPaymentMethodId() + " : " + fieldStart + "paymentMethod");
            if (bd.getAccountUseId() == 2) {
                request.setAttribute(fieldStart + "paymentMethod", bd.getPaymentMethodId());
            }

            request.setAttribute(fieldStart + "bankName", bd.getBankId());
            request.setAttribute(fieldStart + "bankBranchId", bd.getBranchId());
            request.setAttribute(fieldStart + "accountType", bd.getAccountTypeId());
            request.setAttribute(fieldStart + "accountHolder", bd.getAccountName());
            request.setAttribute(fieldStart + "accountNumber", bd.getAccountNo());
        }
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.validation;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthTariffDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Johan-NB
 */
public class ValidateConditionSpecific extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter out = null;
        HttpSession session = request.getSession();

        String errorResponse = "";
        String error = "";
        int errorCount = 0;
        NeoManagerBean port = service.getNeoManagerBeanPort();

        String authSubType = request.getParameter("authSub");
        String authFromDate = request.getParameter("authFromDate");
        String authToDate = request.getParameter("authToDate");
        String authStatus = request.getParameter("authStatus");
        String picd = "" + session.getAttribute("primaryICD_text");

        if (authSubType.equalsIgnoreCase("") || authSubType.equalsIgnoreCase("99")) {
            errorCount++;
            error = error + "|authSub:Auth Sub Type is Mandatory";

        }

        //Date Validation
        PreAuthDateValidation pad = new PreAuthDateValidation();

        String authType = "" + session.getAttribute("authType");
        String authPeriod = "" + session.getAttribute("authPeriod");
        String authMonthPeriod = "" + session.getAttribute("authMonthPeriod");

        if (authFromDate.equalsIgnoreCase("") || authFromDate.equalsIgnoreCase("null")) {
            errorCount++;
            error = error + "|authFromDate:Date From is mandatory";

        } else {
            String vs = pad.DateValidation(authFromDate, authToDate, "authFromDate", "authToDate", authType, authPeriod, authMonthPeriod, "no");
            String[] returned = vs.split("\\|");
            boolean validDate = Boolean.parseBoolean(returned[0]);
            if (!validDate) {
                String errorsReturned = vs.substring(vs.indexOf("|"), vs.length());
                System.out.println("from date validation error(s) returned = " + errorsReturned);
                errorCount++;
                error = error + errorsReturned;
            }
        }

        if (authToDate.equalsIgnoreCase("") || authToDate.equalsIgnoreCase("null")) {
            errorCount++;
            error = error + "|authToDate:Date From is mandatory";

        } else {
            String vs = pad.DateValidation(authFromDate, authToDate, "authFromDate", "authToDate", authType, authPeriod, authMonthPeriod, "no");
            String[] returned = vs.split("\\|");
            boolean validDate = Boolean.parseBoolean(returned[0]);
            if (!validDate) {
                String errorsReturned = vs.substring(vs.indexOf("|"), vs.length());
                System.out.println("to date validation error(s) returned = " + errorsReturned);
                errorCount++;
                error = error + errorsReturned;
            }
        }

        if (authStatus.equalsIgnoreCase("null") || authStatus.equalsIgnoreCase("99")) {
            errorCount++;
            error = error + "|authStatus:Auth Status is mandatory";
        }

        //check list sizes
        boolean tSizeValid = true;
        //boolean nSizeValid = true;
        List<AuthTariffDetails> tariffs = (List<AuthTariffDetails>) session.getAttribute("AuthBasketTariffs");
        //List<AuthTariffDetails> nappies = (List<AuthTariffDetails>) session.getAttribute("AuthNappiTariffs");

        if (tariffs == null || tariffs.isEmpty() == true) {
            tSizeValid = false;
        }
        /*if (nappies == null || nappies.isEmpty() == true) {
            nSizeValid = false;
        }*/

        if (tSizeValid == false){// && nSizeValid == false) {
            errorCount++;
            error = error + "|basketButton:Empty Basket is not allowed";
        }


        if (errorCount > 0) {
            errorResponse = "Error" + error;
        } else if (errorCount == 0) {

            int basketType = Integer.parseInt(authSubType);
            int prodId = Integer.parseInt(session.getAttribute("scheme").toString());
            Collection<String> basketICDs = port.getBasketSpecificICDs(basketType, prodId);
            boolean icdFound = false;
            for (String basketIcd : basketICDs) {
                String[] icdValues = basketIcd.split("\\|");
                String icd = icdValues[0];
                if (icd.equals(picd)) {
                    icdFound = true;
                    break;
                }
            }
            if (icdFound == false) {
                errorResponse = "Error|picd:basketICDs";
                session.setAttribute("basketICDs", basketICDs);
            } else {
                session.setAttribute("basketICDs", null);
                errorResponse = "Done|";
            }
        }

        try {
            out = response.getWriter();
            out.println(errorResponse);

        } catch (Exception ex) {
            System.out.println("ValidatePreConditionSpec error : " + ex.getMessage());
        } finally {
            out.close();
        }

        return null;
    }

    @Override
    public String getName() {
        return "ValidateConditionSpecific";
    }
}

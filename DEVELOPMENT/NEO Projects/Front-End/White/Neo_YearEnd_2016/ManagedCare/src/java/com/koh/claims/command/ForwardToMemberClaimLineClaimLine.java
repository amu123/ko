/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ClaimLine;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Johan-NB
 */
public class ForwardToMemberClaimLineClaimLine extends NeoCommand {

    private static final Logger logger = Logger.getLogger(ForwardToMemberClaimLineClaimLine.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);

        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        int selectedClaimLineId = Integer.parseInt(request.getParameter("memberDetailedClaimLineId"));
        String returnFlag = request.getParameter("returnFlag");
        session.setAttribute("returnFlag", returnFlag);

        logger.log(Level.INFO, "The selected claimLine id {0}", selectedClaimLineId);
        ClaimLine claimLine = port.fetchClaimLineById(selectedClaimLineId);
        session.setAttribute("memCLCLDetails", claimLine);
        String onScreen = request.getParameter("onScreen");
        try {
            new MemberClaimLineClaimLineCommand().execute(request, response, context);
            String nextJSP;
            nextJSP = "/Claims/MemberDetailedClaimLine.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "ForwardToMemberClaimLineClaimLine";
    }
}

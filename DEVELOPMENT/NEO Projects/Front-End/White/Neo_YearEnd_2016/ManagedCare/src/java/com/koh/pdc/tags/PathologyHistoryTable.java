/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author princes
 */
public class PathologyHistoryTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
//        Collection<Biometrics> details = (Collection<Biometrics>) session.getAttribute("bioDetails");

        try {

            out.print("<tr>");
            out.print("<th scope=\"col\">Recieved Date</th>");
            out.print("<th scope=\"col\">Service Date</th>");
            out.print("<th scope=\"col\">Tarrif Code</th>");
            out.print("<th scope=\"col\">Test Results</th>");
            out.print("<th scope=\"col\">Test Comments</th>");
            out.print("</tr>");
            /*for (Biometrics bio : details) {
            if (bio.getBiometricTypeId().equals("10")) {
            for (GeneralBiometrics g : bio.getGeneral()) {
            out.print("<tr>");
            out.print("<td>" + g.getDateReceived() + "</td>");
            out.print("<td>" + g.getDateMeasured() + "</td>");
            out.print("<td>" + g.getTariffCode() + "</td>");
            out.print("<td>" + g.getTestResult() + "</td>");
            out.print("<td>" + g.getDetail() + "</td>");
            out.print("</tr>");
            }
            }
            }*/

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.tags;

import com.koh.command.NeoCommand;
import com.koh.utils.LookupUtils;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.LookupValue;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class LabelNeoIDCDropDownErrorReq extends TagSupport {
        private Logger logger = Logger.getLogger(this.getClass());
    private String displayName;
    private String elementName;
    private String javaScript;
    private String mandatory = "no";
    private String errorValueFromSession = "no";
    private String firstIndexSelected = "no";
    private String parentElement;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
logger.info("----------------------LabelNeoIDCDropDownErrorReq-----------------");
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        try {
            out.println("<td><label class=\"label\" for=\"" + elementName + "\">" + displayName + ":</label></td>");
            out.println("<td><select style=\"width:215px\" name=\"" + elementName + "\" id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");
            String selectedBox = "" + pageContext.getRequest().getAttribute(elementName);
logger.info("selectedBox:" + selectedBox);
            //get parent 
            String parentValue = "" + pageContext.getRequest().getAttribute(parentElement);
logger.info("parentValue:" + parentValue);
            //System.out.println("LabelNeoBranchDropDownErrorReq parentValue = " + parentValue);
            List<LookupValue> lvList = new ArrayList<LookupValue>();
            if (parentValue != null && !parentValue.equalsIgnoreCase("") && !parentValue.equalsIgnoreCase("99") && !parentValue.equalsIgnoreCase("null")) {
                int cdl = Integer.parseInt(parentValue);
logger.info("cdl:" + cdl);
                lvList = NeoCommand.service.getNeoManagerBeanPort().getCodeTable( cdl);

            }
            out.println("<option value=\"\"></option>");
            boolean firstIndexSet = false;
            
            for (LookupValue lv : lvList) {
                
                //Branch lookupValue = branchList.get(i);
                if (lv.getId().trim().equalsIgnoreCase(selectedBox)) {
                    out.println("<option value=\"" + lv.getId() + "\" selected >" + lv.getValue() + "</option>");
                } else {
                    if (firstIndexSelected.equalsIgnoreCase("yes")) {
                        if (firstIndexSet) {
                            out.println("<option value=\"" + lv.getId() + "\" >" + lv.getValue() + "</option>");
                        } else {
                            out.println("<option value=\"" + lv.getId() + "\" selected >" + lv.getValue() + "</option>");
                            firstIndexSet = true;
                            System.out.println("found first index");
                        }
                    } else {
                        out.println("<option value=\"" + lv.getId() + "\" >" + lv.getValue() + "</option>");
                    }

                }
            }
            out.println("</select></td>");
            if (mandatory.equalsIgnoreCase("yes")) {
                out.println("<td><label class=\"red\">*</label></td>");
            } else {
                out.println("<td></td>");
            }

            String sessionValerror = "";
            if (errorValueFromSession.equalsIgnoreCase("yes")) {

                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                }

                if (sessionValerror != null && !sessionValerror.equalsIgnoreCase("null")) {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
                } else {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
                }
            }

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelNeoLookupValueDropDown tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setErrorValueFromSession(String errorValueFromSession) {
        this.errorValueFromSession = errorValueFromSession;
    }

    public void setFirstIndexSelected(String firstIndexSelected) {
        this.firstIndexSelected = firstIndexSelected;
    }

    public void setParentElement(String parentElement) {
        this.parentElement = parentElement;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.brokerFirm.BrokerMapping;
import com.koh.brokerFirm.ConsultantMapping;
import com.koh.broker_firm.application.commands.BrokerAppTabContentsCommand;
import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.member.application.command.MemberApplicationSpecificQuestionsCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.BrokerBrokerConsult;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author Antaryami
 */
public class AddBrokerConsultantHistoryCommand extends NeoCommand {
    
    private static final String JSP_FOLDER = "/Broker/";
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        request.setAttribute("brokerEntityId", request.getParameter("brokerEntityId"));
        try {
            String s = request.getParameter("buttonPressed");
            System.out.println("buttonPressed " + s);
            if (s != null && !s.isEmpty()) {
                if (s.equalsIgnoreCase("Add History")) {
                    getAddConsultantPage(request, response, context);
                } else if (s.equalsIgnoreCase("SaveButton")) {
                    //Save the consultant record here
                    saveConsultant(request, response, context);
                } else if (s.equalsIgnoreCase("SearchConsultantButton".trim())) {
                    searchConsultant(request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(MemberApplicationSpecificQuestionsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void getAddConsultantPage(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        context.getRequestDispatcher("/Broker/LinkBrokerToConsultant.jsp").forward(request, response);
    }
    
    private void searchConsultant(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        request.setAttribute("diagCodeId", "Disorder_diagCode_text");
        request.setAttribute("diagNameId", "Disorder_diagName");
        context.getRequestDispatcher("/Broker/BrokerConsultantForBrokerSearch.jsp").forward(request, response);
    }
    
    private void saveConsultant(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
//        System.out.println("Broker entityID " + request.getParameter("brokerEntityId"));
//        System.out.println("Consultant enitytID " + request.getParameter("consultantEntityId"));
//        System.out.println("Save consultant called");
        String status = "ERROR";
        String additionalData = null;
        String updateFields = null;
        Map<String, String> errors = null;
        String validationErros = null;
        String startDate = request.getParameter("BrokerApp_ConsultantHistoryStartDate") != null ? request.getParameter("BrokerApp_ConsultantHistoryStartDate") : "";
        int entityId = new Integer(request.getParameter("brokerEntityId"));
        if (!startDate.isEmpty() && !startDate.equals("")) {
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
                Date date = format.parse(startDate);
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MONTH, -5);
                if (cal.getTimeInMillis() <= date.getTime()) {
                    errors = ConsultantMapping.validateBrokerConsultantHistory(request);
                    validationErros = TabUtils.convertMapToJSON(errors);
                    status = TabUtils.getStatus(errors);
                    if ("OK".equalsIgnoreCase(status)) {
                        if (save(request, entityId)) {
                            
                        } else {
                            status = "ERROR";
                            additionalData = "\"message\":\"There was an error saving history.\"";
                        }
                    } else {
                        additionalData = null;
                    }
                } else {
                    status = "ERROR";
                    additionalData = "\"message\":\"Please enter a date from last five months.\"";
                }
            } catch (ParseException pe) {
                status = "ERROR";
                additionalData = "\"message\":\"Please eneter a valid date.\"";
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Failed to parse the start date", pe);
            }
        } else {
            status = "ERROR";
            additionalData = "\"message\":\"Please fill all the fields.\"";
        }
        if ("OK".equalsIgnoreCase(status)) {            
            context.getRequestDispatcher(new BrokerAppTabContentsCommand().getBrokerConsultantdetail(request, entityId)).forward(request, response);
        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }
    }
    
    private boolean save(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = getNeoUser(request);
        try {
            BrokerBrokerConsult ba = ConsultantMapping.getBrokerConsultantHistoryDetails(request, entityId, neoUser);
            port.saveBrokerConsultanForBroker(ba);
            return true;
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Failed to get brokerConsultant history details ", e.getMessage());
            return false;
        }
    }
    
    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser) request.getSession().getAttribute("persist_user");
    }
    
    @Override
    public String getName() {
        return "AddBrokerConsultantHistoryCommand";
    }
}

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;

/**
*
* @author dewaldo
*/
public class SaveManageMyPatientCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        System.out.println("entered SaveManageMyPatientCommand");
        String msg = null;

        try {
            NeoManagerBean port = service.getNeoManagerBeanPort();

            String closeEvent = request.getParameter("closeEvent");
            String eventID = String.valueOf(session.getAttribute("eventId"));

            int i = 0;
            String action = null;
            System.out.println("closeEvent: " + closeEvent);
            System.out.println("eventID: " + eventID);
            if (closeEvent != null && eventID != null && !eventID.trim().equalsIgnoreCase("") && !eventID.equalsIgnoreCase("null")) {
                if (!closeEvent.equals("0")) {
                    System.out.println("Closing event...");
                    action = "close";
                    i = port.closeOpenEvent(new Integer(eventID.trim()), false);
                    msg = "Event " + action + "d";
                } else if (!closeEvent.equals("1")) {
                    System.out.println("Opening event...");
                    action = "open";
                    i = port.closeOpenEvent(new Integer(eventID.trim()), false);
                    msg = "Event " + action + "ed";
                }
            } else {
                System.out.println("No action...");
            }

            if (i != 1) {
                msg = "Failed to " + action + " event!";
            }

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            msg = "Failed to close event!";
        }

        PrintWriter out;
        try {
            out = response.getWriter();

            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            out.println("<td><label class=\"header\"><h2>" + msg + "</h2></label></td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "3; URL=/ManagedCare/PDC/PolicyHolderDetails.jsp");
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "SaveManageMyPatientCommand";
    }
}
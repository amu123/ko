/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.command;

import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author gerritj
 */
public class SaveCallCenterAuth1Command extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        try {
            String nextJSP = "";
            String authType = request.getParameter("authType");
            String dentalType = request.getParameter("dental");
            if(authType.equalsIgnoreCase("1")){ //optical
                nextJSP = "/Auth/OpticalDetails.jsp";
            }else if(authType.equalsIgnoreCase("2")){ //dental
                if(dentalType.equalsIgnoreCase("3")) {
                    nextJSP = "/Auth/OrthodonticDetails.jsp";
                } else {
                    nextJSP = "/Auth/DentalDetails.jsp";
                }
            }

            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SaveCallCenterAuth1Command";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.fe.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoUser;

/**
 *
 * @author whauger
 */
public class ChangePasswordCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        PrintWriter out = null;

        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        try {
            out = response.getWriter();

            String oldPassword = request.getParameter("oldPassword");
            String newPassword = request.getParameter("newPassword");

            System.out.println("The old password is " + oldPassword);
            System.out.println("The new password is " + newPassword);
            boolean passUsed = service.getNeoManagerBeanPort().validatePasswordHistory(newPassword, user.getUsername());

            if (!passUsed) {
                session.setAttribute("newPassword_error","");
                boolean result = service.getNeoManagerBeanPort().changePassword(user, oldPassword, newPassword);

                out.println("<html>");
                out.println("<head>");
                out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                out.println("</head>");
                out.println("<body>");
                out.println("<center>");
                out.println("<table cellspacing=\"4\">");
                out.println("<tr>");
                if (result) {
                    out.println("<td><label class=\"header\">Password successfully changed for " + user.getUsername() + "</label></td>");
                    clearScreenFromSession(request);
					response.setHeader("Refresh", "1; URL=/ManagedCare/Security/ChangePassword.jsp");
                }
            } else {
                if(passUsed){
                  session.setAttribute("newPassword_error", "Password previously used, please use a different password.");                  
                  response.setHeader("Refresh", "0; URL=/ManagedCare/Security/ChangePassword.jsp");
                }else{
                  session.setAttribute("newPassword_error", "");
                  out.println("<td><label class=\"header\">Failed to change password. Please check password.</label></td>");  
                  response.setHeader("Refresh", "1; URL=/ManagedCare/Security/ChangePassword.jsp");
                }
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

        }catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }

        return null;
    }

    @Override
    public String getName() {
        return "ChangePasswordCommand";
    }

}

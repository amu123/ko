/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CardiacBiometrics;

/**
 *
 * @author princes
 */
public class BiometricCardiacFailureAndCardiacMyothy extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String javascript;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        List<CardiacBiometrics> getCardiac = (List<CardiacBiometrics>) session.getAttribute("getCardiac");
        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        String dateMeasured;
        try {

            out.print("<tr>");
            out.print("<th scope=\"col\">Date Measured</th>");
            out.print("<th scope=\"col\">Height</th>");
            out.print("<th scope=\"col\">Weight</th>");
            out.print("<th scope=\"col\">BMI</th>");
            out.print("<th scope=\"col\">Blood Pressure Systolic</th>");
            out.print("<th scope=\"col\">Blood Pressure Diastolic</th>");
            out.print("<th scope=\"col\">NYHA Stage</th>");
            out.print("<th scope=\"col\">LVEF(%)</th>");
            out.print("<th scope=\"col\">Select</th>");

//            out.print("<th scope=\"col\">ICD10</th>");
//            out.print("<th scope=\"col\">Excercise</th>");            
//            out.print("<th scope=\"col\">Ex Smoker Years Stopped</th>");
//            out.print("<th scope=\"col\">Alchohol Units</th>");
//            out.print("<th scope=\"col\">NYHA Class</th>");
//            out.print("<th scope=\"col\">BMI</th>");
//            out.print("<th scope=\"col\">LVEF%</th>");
//            out.print("<th scope=\"col\">Source</th>");
//            out.print("<th scope=\"col\">Notes</th>");
            out.print("</tr>");
            if (getCardiac != null && getCardiac.size() > 0) {
                for (CardiacBiometrics c : getCardiac) {

                    out.print("<tr>");
                    if (c.getDateMeasured() != null) {
                        Date mDate = c.getDateMeasured().toGregorianCalendar().getTime();
                        dateMeasured = formatter.format(mDate);
                        out.print("<td><center><label class=\"label\">" + dateMeasured + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getHeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + c.getHeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getWeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + c.getWeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getBmi() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getBmi() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getBloodPressureSystolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getBloodPressureSystolic() + "</label></center></td>");
                        System.out.println("getBloodPressureSystolic: " + c.getBloodPressureSystolic());
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getBloodPressureDiastolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getBloodPressureDiastolic() + "</label></center></td>");
                        System.out.println("getBloodPressureDiastolic: " + c.getBloodPressureDiastolic());
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getNyhaClass() != null) {
                        out.print("<td><center><label class=\"label\">" + c.getNyhaClass() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getLvef() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getLvef() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("<td><center><button name=\"opperation\" type=\"button\" " + javascript + " value=\"" + c.getBiometricsId() + "\">Select</button></center></td>");
//                    if (c.getDateMeasured() != null) {
//                        Date mDate = c.getDateMeasured().toGregorianCalendar().getTime();
//                        dateMeasured = formatter.format(mDate);
//                        out.print("<td>" + dateMeasured + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (c.getIcd10Lookup() != null) {
//                        out.print("<td>" + c.getIcd10Lookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getExercisePerWeek() != 0) {
//                        out.print("<td>" + c.getExercisePerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getYearsSinceStopped() > 0) {
//                        out.print("<td>" + c.getYearsSinceStopped() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getAlcoholUnitsPerWeek() != 0) {
//                        out.print("<td>" + c.getAlcoholUnitsPerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getNhyClassLookup() != null) {
//                        out.print("<td>" + c.getNhyClassLookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getBmi() != 0) {
//                        out.print("<td>" + c.getBmi() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getLvef() != 0.0) {
//                        out.print("<td>" + c.getLvef() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getSourceLookup() != null) {
//                        out.print("<td>" + c.getSourceLookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getDetail() != null) {
//                        out.print("<td>" + c.getDetail() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
                    out.print("</tr>");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

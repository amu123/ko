/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthCPTDetails;
import neo.manager.AuthTariffDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class OverridePMB extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        session.removeAttribute("authPMBDetails");
        session.removeAttribute("savedAuthPMBs");
        session.removeAttribute("pmb");

        String authType = "" + session.getAttribute("authType");
        if (!authType.equals("12")) {

            //RECALCULATE COPAY
            double copay = 0.0d;
            List<AuthCPTDetails> cptList = (List<AuthCPTDetails>) session.getAttribute("authCPTListDetails");

            String admissionD = "";
            if (authType.equals("9")) {
                admissionD = "" + session.getAttribute("authFromDate");
            } else {
                admissionD = "" + session.getAttribute("admissionDateTime");
            }
            Date admissionDate = null;
            XMLGregorianCalendar authStart = null;
            try {
                admissionDate = new SimpleDateFormat("yyyy/MM/dd").parse(admissionD);
                authStart = DateTimeUtils.convertDateToXMLGregorianCalendar(admissionDate);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }

            //get auth member option id
            String optionId = "" + session.getAttribute("schemeOption");
            System.out.println("CalculateCoPaymentAmounts member option id for copay mapping = " + optionId);

            if (authType.equals("9")) {
                int option = 0;
                if (optionId != null && !optionId.equalsIgnoreCase("null") && !optionId.equals("")) {
                    option = Integer.parseInt(optionId);
                    List<AuthTariffDetails> tList = (List<AuthTariffDetails>) session.getAttribute("tariffListArray");
                    copay = port.getTariffCoPaymentAmountForTariffs(tList, option, authStart);

                    session.setAttribute("coPay_error", null);
                    if (copay != 99999.0d && copay != 0.0d) {
                        session.setAttribute("coPay_error", null);
                    } else if (copay == 99999.0d) {
                        session.setAttribute("coPay_error",
                                "*Note: Procedure Excluded Unless PMB");
                    }
                }
            } else {
                copay = CalculateCoPaymentAmounts.setCopayment(session);
            }

            System.out.println("co payment amount after setCopayment = " + copay);
            session.setAttribute("coPay", copay);
        }
        try {
            String nextJSP = "" + session.getAttribute("onScreen");
            System.out.println("OverridePMB onscreen = " + nextJSP);
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "OverridePMB";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.ClaimLine;
import neo.manager.ClaimLineRejection;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author janf
 */
public class RejectClaimLine extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Date today = new Date(System.currentTimeMillis());
        XMLGregorianCalendar xToday = DateTimeUtils.convertDateToXMLGregorianCalendar(today);
        PrintWriter out = null;

        int claimLineId = Integer.parseInt("" + session.getAttribute("memberDetailedClaimLineId"));
        //ClaimLine claimLine = (ClaimLine) session.getAttribute("memCLCLDetails");
        //int claimId = claimLine.getClaimId();
        
        String rejectionCode = request.getParameter("rejectionReason");
        String rejectionCodeText = port.getValueFromCodeTableForTableId(84, rejectionCode);

        System.out.println("rejectionCode = " + rejectionCode);
        System.out.println("rejectionCodeText = " + rejectionCodeText);
        System.out.println("claimLineId = " + claimLineId);
        
        Security _secure = new Security();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        _secure.setCreatedBy(user.getUserId());
        _secure.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        _secure.setLastUpdatedBy(user.getUserId());
        _secure.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        _secure.setSecurityGroupId(user.getSecurityGroupId());
        
        
        try {
            String returnMsg = "";
            ClaimLineRejection clr = new ClaimLineRejection();
            clr.setClaimLineId(claimLineId);
            clr.setDescription(rejectionCodeText);
            boolean success = port.rejectClaimLine(claimLineId, clr, user);
            
            out = response.getWriter();
            System.out.println("success = " + success);
            if (!success) {
                returnMsg = "Error|Can't Reject claim line - " + claimLineId;
            } else {
                returnMsg = "Done|Claim line " + claimLineId + " Rejected";
            }
            out.println(returnMsg);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "RejectClaimLine";
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.tags;

import java.text.ParseException;
import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CoverClaimBenefitLimitStructure;
import neo.manager.SavingsContribution;

/**
 *
 * @author josephm
 */
public class ViewBenefitsTable extends TagSupport {

    private String javaScript;
    private String commandName;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest request = pageContext.getRequest();
        HttpSession session = pageContext.getSession();

        int depCode = -1;
        if (session.getAttribute("memBenDepCode") != null) {
            depCode = Integer.parseInt("" + session.getAttribute("memBenDepCode"));
        }

        try {

            out.println("<tr><td><label class=\"subheader\">Benefits Details</label></td></tr></br></br>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr>"
                    + "<th align=\"left\">Limit</th>"
                    + "<th align=\"left\">Sub-Limit</th>"
                    + "<th align=\"left\">Sub-Sub-Limit</th>"
                    + "<th align=\"left\">Limit Amount</th>"
                    + "<th align=\"left\">Amount Used</th>"
                    + "<th align=\"left\">Amount Available</th>"
                    + "<th align=\"left\">Limit Count</th>"
                    + "<th align=\"left\">Count Used</th>"
                    + "<th align=\"left\">Count Available</th>"
                    + "<th align=\"left\">Action</th></tr>");

            //ArrayList<CoverClaimBenefitLimitStructure> benLimStruct = (ArrayList<CoverClaimBenefitLimitStructure>) request.getAttribute("memberBenefitsList");
            ArrayList<CoverClaimBenefitLimitStructure> benLimStruct = (ArrayList<CoverClaimBenefitLimitStructure>) session.getAttribute("memberBenefitsList");
            //System.out.println("The size of the list from the tag is " + benefitsList.size());

            //String benefitsLimit = "";
            boolean displaySavingsContribution = false;
            boolean displayThresholdBenefit = false;
            boolean famBenIndNote = false;
            java.text.DecimalFormat desFormat = new java.text.DecimalFormat("#.##");
            if (benLimStruct != null && benLimStruct.size() > 0) {

                String memberNumber = "";
                String mainBenefit = "";
                String midBenefit = "";
                String lowBenefit = "";

                for (CoverClaimBenefitLimitStructure ccb : benLimStruct) {

                    if (ccb.getMainBenefitType() == 2) {
                        displaySavingsContribution = true;
                    }

                    if (ccb.getMainBenefitType() == 3) {
                        displayThresholdBenefit = true;
                    } else {
                        displayThresholdBenefit = false;
                    }
                    
                    memberNumber = ccb.getCoverNumber();

                    String mainB = ccb.getMainDesc();
                    boolean addMainClaimsButton = false;
                    boolean addMidClaimsButton = false;
                    boolean addLowClaimsButton = false;

                    double mainAmount = 0.0d;
                    double mainAmountUsed = 0.0d;
                    double mainAmountAvail = 0.0d;
                    int mainCount = 0;
                    int mainCountUsed = 0;
                    int mainCountAvail = 0;
                    double midAmount = 0.0d;
                    double midAmountUsed = 0.0d;
                    double midAmountAvail = 0.0d;
                    int midCount = 0;
                    int midCountUsed = 0;
                    int midCountAvail = 0;
                    double lowAmount = 0.0d;
                    double lowAmountUsed = 0.0d;
                    double lowAmountAvail = 0.0d;
                    int lowCount = 0;
                    int lowCountUsed = 0;
                    int lowCountAvail = 0;


                    if (mainB != null && mainB.equals(mainBenefit)) {
                        mainB = "";

                    } else {
                        int mainBenId = ccb.getMainBenefitId();
                        mainBenefit = mainB;
                        mainB = "";

                        mainAmount = ccb.getMainLimitAmount();
                        mainAmountUsed = ccb.getMainLimitUsedAmount();
                        mainAmountAvail = ccb.getMainLimitAvailAmount();
                        mainCount = ccb.getMainLimitCount();
                        mainCountUsed = ccb.getMainLimitUsedCount();
                        mainCountAvail = ccb.getMainLimitAvailCount();

                        if (mainAmount != 0.0d) {
                            try {
                                mainAmount = desFormat.parse(desFormat.format(mainAmount)).doubleValue();

                            } catch (ParseException ex) {
                                System.out.println("exception" + ex);
                            }
                        }
                        if (mainAmountUsed != 0.0d) {
                            addMainClaimsButton = true;
                            try {
                                mainAmountUsed = desFormat.parse(desFormat.format(mainAmountUsed)).doubleValue();

                            } catch (ParseException ex) {
                                System.out.println("exception" + ex);
                            }
                        }

                        if (mainAmountAvail != 0.0d) {
                            try {
                                mainAmountAvail = desFormat.parse(desFormat.format(mainAmountAvail)).doubleValue();
                            } catch (ParseException ex) {
                                System.out.println("exception" + ex);
                            }
                        }

                        if (mainCountUsed != 0) {
                            addMainClaimsButton = true;
                        }

                        if (ccb.getFamilyBenefitInd() != null && ccb.getFamilyBenefitInd().equalsIgnoreCase("main")) {
                            famBenIndNote = true;
                            
                            if(displaySavingsContribution){
                                out.println("<tr id=\"saver\"><td><a onClick=\"openContributionGrid();\"><label class=\"noteLabel\">" + mainBenefit + "</label></a></td>");
                            }else{
                                out.println("<tr><td><label class=\"noteLabel\">" + mainBenefit + "</label></td>");
                            }
                            out.println("<td><label class=\"noteLabel\"></label></td>"
                                    + "<td><label class=\"noteLabel\"></label></td>"
                                    + "<td><label class=\"noteLabel\">" + mainAmount + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + mainAmountUsed + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + mainAmountAvail + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + mainCount + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + mainCountUsed + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + mainCountAvail + "</label></td>");

                        } else {
                            out.println("<tr>"
                                    + "<td><label class=\"label\">" + mainBenefit + "</label></td>"
                                    + "<td><label class=\"label\"></label></td>"
                                    + "<td><label class=\"label\"></label></td>"
                                    + "<td><label class=\"label\">" + mainAmount + "</label></td>"
                                    + "<td><label class=\"label\">" + mainAmountUsed + "</label></td>"
                                    + "<td><label class=\"label\">" + mainAmountAvail + "</label></td>"
                                    + "<td><label class=\"label\">" + mainCount + "</label></td>"
                                    + "<td><label class=\"label\">" + mainCountUsed + "</label></td>"
                                    + "<td><label class=\"label\">" + mainCountAvail + "</label></td>");
                        }



                        if (addMainClaimsButton) {
                            if (displayThresholdBenefit) {
                                out.println("<td><button name=\"opperation\" type=\"submit\" onClick=\"submitBDDAction('ViewMemberClaimsBenefitsCommand','" + mainBenId + "','memberBenefits','" + depCode + "','true')\"; value=\"" + commandName + "\">Select</button></td>");
                            } else {
                                out.println("<td><button name=\"opperation\" type=\"submit\" onClick=\"submitBDDAction('ViewMemberClaimsBenefitsCommand','" + mainBenId + "','memberBenefits','" + depCode + "','false')\"; value=\"" + commandName + "\">Select</button></td>");
                            }
                        } else {
                            out.println("<td></td>");
                        }

                        out.println("</tr>");

                    }

                    String midB = ccb.getMidDesc();
                    boolean midAdded = false;

                    if (midB != null) {
                        if (midB.equals(midBenefit)) {
                            midB = "";

                        } else {
                            int midBenId = ccb.getMidBenefitId();
                            midAdded = true;
                            midBenefit = midB;
                            midB = "";

                            midAmount = ccb.getMidLimitAmount();
                            midAmountUsed = ccb.getMidLimitUsedAmount();
                            midAmountAvail = ccb.getMidLimitAvailAmount();
                            midCount = ccb.getMidLimitCount();
                            midCountUsed = ccb.getMidLimitUsedCount();
                            midCountAvail = ccb.getMidLimitAvailCount();

                            if (midAmount != 0.0d) {
                                try {
                                    midAmount = desFormat.parse(desFormat.format(midAmount)).doubleValue();
                                } catch (ParseException ex) {
                                    System.out.println("exception" + ex);
                                }
                            }
                            if (midAmountUsed != 0.0d) {
                                addMidClaimsButton = true;
                                try {
                                    midAmountUsed = desFormat.parse(desFormat.format(midAmountUsed)).doubleValue();
                                } catch (ParseException ex) {
                                    System.out.println("exception" + ex);
                                }
                            }
                            if (midAmountAvail != 0.0d) {
                                try {
                                    midAmountAvail = desFormat.parse(desFormat.format(midAmountAvail)).doubleValue();
                                } catch (ParseException ex) {
                                    System.out.println("exception" + ex);
                                }
                            }
                            if (midCountUsed != 0) {
                                addMidClaimsButton = true;
                            }

                            if (ccb.getFamilyBenefitInd() != null && ccb.getFamilyBenefitInd().equalsIgnoreCase("mid")) {
                                famBenIndNote = true;
                                out.println("<tr>"
                                        + "<td><label class=\"noteLabel\">" + mainB + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midBenefit + "</label></td>"
                                        + "<td><label class=\"noteLabel\"></label></td>"
                                        + "<td><label class=\"noteLabel\">" + midAmount + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midAmountUsed + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midAmountAvail + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midCount + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midCountUsed + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midCountAvail + "</label></td>");

                            } else {
                                out.println("<tr>"
                                        + "<td><label class=\"label\">" + mainB + "</label></td>"
                                        + "<td><label class=\"label\">" + midBenefit + "</label></td>"
                                        + "<td><label class=\"label\"></label></td>"
                                        + "<td><label class=\"label\">" + midAmount + "</label></td>"
                                        + "<td><label class=\"label\">" + midAmountUsed + "</label></td>"
                                        + "<td><label class=\"label\">" + midAmountAvail + "</label></td>"
                                        + "<td><label class=\"label\">" + midCount + "</label></td>"
                                        + "<td><label class=\"label\">" + midCountUsed + "</label></td>"
                                        + "<td><label class=\"label\">" + midCountAvail + "</label></td>");

                            }



                            if (addMidClaimsButton) {
                                if (displayThresholdBenefit) {
                                    out.println("<td><button name=\"opperation\" type=\"submit\" onClick=\"submitBDDAction('ViewMemberClaimsBenefitsCommand','" + midBenId + "','memberBenefits','" + depCode + "','true')\"; value=\"" + commandName + "\">Select</button></td>");
                                } else {
                                    out.println("<td><button name=\"opperation\" type=\"submit\" onClick=\"submitBDDAction('ViewMemberClaimsBenefitsCommand','" + midBenId + "','memberBenefits','" + depCode + "','false')\"; value=\"" + commandName + "\">Select</button></td>");
                                }
                            } else {
                                out.println("<td></td>");
                            }
                            out.println("</tr>");

                        }
                    }

                    //add low
                    lowBenefit = ccb.getLowDesc();

                    if (lowBenefit != null && !lowBenefit.equalsIgnoreCase("")) {
                        int lowBenId = ccb.getLowBenefitId();

                        lowAmount = ccb.getLowLimitAmount();
                        lowAmountUsed = ccb.getLowLimitUsedAmount();
                        lowAmountAvail = ccb.getLowLimitAvailAmount();
                        lowCount = ccb.getLowLimitCount();
                        lowCountUsed = ccb.getLowLimitUsedCount();
                        lowCountAvail = ccb.getLowLimitAvailCount();

                        if (lowAmount != 0.0d) {
                            try {
                                lowAmount = desFormat.parse(desFormat.format(lowAmount)).doubleValue();

                            } catch (ParseException ex) {
                                System.out.println("exception" + ex);
                            }
                        }
                        if (lowAmountUsed != 0.0d) {
                            addLowClaimsButton = true;
                            try {
                                lowAmountUsed = desFormat.parse(desFormat.format(lowAmountUsed)).doubleValue();

                            } catch (ParseException ex) {
                                System.out.println("exception" + ex);
                            }
                        }
                        if (lowAmountAvail != 0.0d) {
                            try {
                                lowAmountAvail = desFormat.parse(desFormat.format(lowAmountAvail)).doubleValue();

                            } catch (ParseException ex) {
                                System.out.println("exception" + ex);
                            }
                        }

                        if (lowCountUsed != 0) {
                            addLowClaimsButton = true;
                        }

                        if (ccb.getFamilyBenefitInd() != null && ccb.getFamilyBenefitInd().equalsIgnoreCase("low")) {
                            famBenIndNote = true;
                            out.println("<tr>"
                                    + "<td><label class=\"noteLabel\">" + mainB + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + midB + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + lowBenefit + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + lowAmount + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + lowAmountUsed + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + lowAmountAvail + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + lowCount + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + lowCountUsed + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + lowCountAvail + "</label></td>");

                        } else {
                            out.println("<tr>"
                                    + "<td><label class=\"label\">" + mainB + "</label></td>"
                                    + "<td><label class=\"label\">" + midB + "</label></td>"
                                    + "<td><label class=\"label\">" + lowBenefit + "</label></td>"
                                    + "<td><label class=\"label\">" + lowAmount + "</label></td>"
                                    + "<td><label class=\"label\">" + lowAmountUsed + "</label></td>"
                                    + "<td><label class=\"label\">" + lowAmountAvail + "</label></td>"
                                    + "<td><label class=\"label\">" + lowCount + "</label></td>"
                                    + "<td><label class=\"label\">" + lowCountUsed + "</label></td>"
                                    + "<td><label class=\"label\">" + lowCountAvail + "</label></td>");

                        }



                        if (addLowClaimsButton) {
                            out.println("<td><button name=\"opperation\" type=\"submit\" onClick=\"submitBDDAction('ViewMemberClaimsBenefitsCommand','" + lowBenId + "','memberBenefits','" + depCode + "')\"; value=\"" + commandName + "\">Select</button></td>");
                        } else {
                            out.println("<td></td>");
                        }
                        out.println("</tr>");

                    } else {

                        if (midB != null && midAdded == false) {
                            addMidClaimsButton = false;
                            int midBenId = ccb.getMidBenefitId();
                            lowBenefit = "";
                            midBenefit = midB;
                            midB = "";

                            midAmount = ccb.getMidLimitAmount();
                            midAmountUsed = ccb.getMidLimitUsedAmount();
                            midAmountAvail = ccb.getMidLimitAvailAmount();
                            midCount = ccb.getMidLimitCount();
                            midCountUsed = ccb.getMidLimitUsedCount();
                            midCountAvail = ccb.getMidLimitAvailCount();

                            if (midAmount != 0.0d) {
                                try {
                                    midAmount = desFormat.parse(desFormat.format(midAmount)).doubleValue();
                                } catch (ParseException ex) {
                                    System.out.println("exception" + ex);
                                }
                            }
                            if (midAmountUsed != 0.0d) {
                                addMidClaimsButton = true;
                                try {
                                    midAmountUsed = desFormat.parse(desFormat.format(midAmountUsed)).doubleValue();
                                } catch (ParseException ex) {
                                    System.out.println("exception" + ex);
                                }
                            }
                            if (midAmountAvail != 0.0d) {
                                try {
                                    midAmountAvail = desFormat.parse(desFormat.format(midAmountAvail)).doubleValue();
                                } catch (ParseException ex) {
                                    System.out.println("exception" + ex);
                                }
                            }
                            if (midCountUsed != 0) {
                                addMidClaimsButton = true;
                            }

                            if (ccb.getFamilyBenefitInd() != null && ccb.getFamilyBenefitInd().equalsIgnoreCase("mid")) {
                                famBenIndNote = true;
                                out.println("<tr>"
                                        + "<td><label class=\"noteLabel\">" + mainB + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midBenefit + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + lowBenefit + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midAmount + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midAmountUsed + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midAmountAvail + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midCount + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midCountUsed + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midCountAvail + "</label></td>");

                            } else {
                                out.println("<tr>"
                                        + "<td><label class=\"label\">" + mainB + "</label></td>"
                                        + "<td><label class=\"label\">" + midBenefit + "</label></td>"
                                        + "<td><label class=\"label\">" + lowBenefit + "</label></td>"
                                        + "<td><label class=\"label\">" + midAmount + "</label></td>"
                                        + "<td><label class=\"label\">" + midAmountUsed + "</label></td>"
                                        + "<td><label class=\"label\">" + midAmountAvail + "</label></td>"
                                        + "<td><label class=\"label\">" + midCount + "</label></td>"
                                        + "<td><label class=\"label\">" + midCountUsed + "</label></td>"
                                        + "<td><label class=\"label\">" + midCountAvail + "</label></td>");

                            }



                            if (addMidClaimsButton) {
                                out.println("<td><button name=\"opperation\" type=\"submit\" onClick=\"submitBDDAction('ViewMemberClaimsBenefitsCommand','" + midBenId + "','memberBenefits','" + depCode + "')\"; value=\"" + commandName + "\">Select</button></td>");
                            } else {
                                out.println("<td></td>");
                            }

                            out.println("</tr>");

                        }
                    }
                }
            }
           
            out.println("</table>");
            //savings contribution grid
            if (displaySavingsContribution) {

                SavingsContribution sc = (SavingsContribution) session.getAttribute("memberBenSavingsContribution");
                if (sc != null) {
                    //set values
                    double advSavLimitAmount = 0.0d;
                    double advSavLimitAvail = 0.0d;
                    double advSavLimitUsed = 0.0d;

                    double riskSavLimitAmount = 0.0d;
                    double riskSavLimitAvail = 0.0d;
                    double riskSavLimitUsed = 0.0d;

                    double totContributionAmount = 0.0d;
                    double contributionAvail = 0.0d;
                    double contributionUsed = 0.0d;

                    try {
                        advSavLimitAmount = desFormat.parse(desFormat.format(sc.getAdvanceSavLimitAmount())).doubleValue();
                        advSavLimitAvail = desFormat.parse(desFormat.format(sc.getAdvanceSavLimitAvail())).doubleValue();
                        advSavLimitUsed = desFormat.parse(desFormat.format(sc.getAdvanceSavLimitUsed())).doubleValue();
                        riskSavLimitAmount = desFormat.parse(desFormat.format(sc.getRiskSavLimitAmount())).doubleValue();
                        riskSavLimitAvail = desFormat.parse(desFormat.format(sc.getRiskSavLimitAvail())).doubleValue();
                        riskSavLimitUsed = desFormat.parse(desFormat.format(sc.getRiskSavLimitUsed())).doubleValue();
                        totContributionAmount = desFormat.parse(desFormat.format(sc.getContributionAmount())).doubleValue();
                        contributionAvail = desFormat.parse(desFormat.format(sc.getContributionAvail())).doubleValue();
                        contributionUsed = desFormat.parse(desFormat.format(sc.getContributionUsed())).doubleValue();

                    } catch (ParseException ex) {
                        System.out.println("exception" + ex);
                    }

                    out.println("<br/><div id=\"contributionTable\" style=\"display:none\" >"
                            + "<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                    out.println("<tr>"
                            + "<th align=\"left\">Benefit Description</th>"
                            + "<th align=\"left\">Limit Amount</th>"
                            + "<th align=\"left\">Available Amount</th>"
                            + "<th align=\"left\">Used Amount</th>"
                            + "</tr>");
                    
                    //Advance Savings
                    out.println("<tr>"
                            + "<td><label class=\"label\">Advance Savings</label></td>"
                            + "<td><label class=\"label\">" + advSavLimitAmount + "</label></td>"
                            + "<td><label class=\"label\">" + advSavLimitAvail + "</label></td>"
                            + "<td><label class=\"label\">" + advSavLimitUsed + "</label></td>"
                            + "</tr>");
                    
                    //Risk Savings
                    out.println("<tr>"
                            + "<td><label class=\"label\">Risk Savings</label></td>"
                            + "<td><label class=\"label\">" + riskSavLimitAmount + "</label></td>"
                            + "<td><label class=\"label\">" + riskSavLimitAvail + "</label></td>"
                            + "<td><label class=\"label\">" + riskSavLimitUsed + "</label></td>"
                            + "</tr>");
                    
                    //Contributed Savings
                    out.println("<tr>"
                            + "<td><label class=\"label\">Contributed Savings</label></td>"
                            + "<td><label class=\"label\">" + totContributionAmount + "</label></td>"
                            + "<td><label class=\"label\">" + contributionAvail + "</label></td>"
                            + "<td><label class=\"label\">" + contributionUsed + "</label></td>"
                            + "</tr>");

                    out.println("<tr><td align=\"right\" colspan=\"4\" ><button name=\"closeContri\" type=\"button\" onClick=\"closeContributionGrid();\">Close</button></td></tr>");
                    
                    out.println("</table></div>");
                }
            }            
            
            
            //set family limit notification
            if (famBenIndNote) {
                out.println("<br/><table><tr align=\"left\"><td colspan=\"10\" ><label class=\"noteLabel\">* Note: Indicates Family Limits</label></td></tr></table>");
            }


        } catch (java.io.IOException ex) {
            throw new JspException("Error in ViewBenefitsTable tag", ex);
        }


        return super.doEndTag();
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }
}

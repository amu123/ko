/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.command;

import com.koh.command.NeoCommand;
import com.koh.utils.LookupUtils;
import com.koh.utils.XmlUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.LookupValue;
import neo.manager.NeoUser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author johanl
 */
public class GetBankBranchByBankIdCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter out = null;
        String xmlStr = "";

        NeoUser user = (NeoUser) request.getSession().getAttribute("persist_user");
        if (request.getParameter("bankId") != null && !request.getParameter("bankId").isEmpty()) {
            int bankId = Integer.parseInt(request.getParameter("bankId"));
            List<LookupValue> lvList = LookupUtils.searchBranch(bankId, null, user);

            response.setHeader("Cache-Control", "no-cache");
            response.setContentType("text/xml");
            response.setStatus(200);

            @SuppressWarnings("static-access")
            Document doc = new XmlUtils().getDocument();

            Element root = doc.createElement("BankBranches");
            doc.appendChild(root);


            for (LookupValue lv : lvList) {
                Element option = doc.createElement("Branch");

                String branchIdName = lv.getId() + "|" + lv.getValue();
                option.setTextContent(branchIdName);

                root.appendChild(option);

            }


            try {
                out = response.getWriter();

                try {
                    xmlStr = XmlUtils.getXMLString(doc);
                } catch (IOException ex) {
                    ex.printStackTrace();
                    xmlStr = "";

                }
                //System.out.println("product option xml = "+xmlStr);
                out.println(xmlStr);


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetBankBranchByBankIdCommand";
    }
}

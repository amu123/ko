/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.tags;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthNoteDetail;

/**
 *
 * @author Johan-NB
 */
public class AuthNoteConfirmationTag extends TagSupport {
    private String sessionAttribute;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            if(javaScript != null){
                out.println("<td colspan=\"4\"><table class=\""+ javaScript +"\" style=\"cellpadding=\"5\"; cellspacing=\"0\";\">");
            }else{
                out.println("<td colspan=\"4\"><table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            }

            out.println("<tr><th align=\"left\">Creation Date</th><th align=\"left\">Created By</th><th align=\"left\">Note</th></tr>");

            List<AuthNoteDetail> noteList = (List<AuthNoteDetail>) session.getAttribute(sessionAttribute);
            if (noteList != null) {
                for (int i = 1; i <= noteList.size(); i++) {
                    AuthNoteDetail notes = noteList.get(i - 1);

                    String creationDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(notes.getCreationDate().toGregorianCalendar().getTime());

                    out.println("<tr><td><label class=\"label\">" + creationDate + "</label></td>");
                    out.println("<td width=\"150\"><label class=\"label\">" + notes.getCreatedBy() + "</label></td>");
                    out.println("<td width=\"400\"><label class=\"label\">" + notes.getNotes() + "</label></td>");

                }
            }
            out.println("</table></td>");
        } catch (IOException ex) {
            throw new JspException("Error in AuthNoteListDisplay tag", ex);
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.command;

import com.koh.command.FECommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author Princes
 */
public class SendForgotPasswordCommand extends FECommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;

        NeoUser pUser = new NeoUser();
        pUser.setUsername(request.getParameter("userName"));

        try {
            out = response.getWriter();
            NeoManagerBean neoPort = NeoCommand.service.getNeoManagerBeanPort();
            NeoUser user = neoPort.validateUserPassword(pUser);

            /*System.out.println("Name " + user.getName());
            System.out.println("Surname " + user.getEmailAddress());
            System.out.println("Password " + user.getPassword());
            System.out.println("Username " + user.getUsername());*/
            System.out.println("The pUser " + pUser.getUsername());
            System.out.println("The user " + user.getUsername());

            if (user.getUsername() != null && user.getUsername().equalsIgnoreCase(pUser.getUsername())) {
                service.getAgileManagerPort().sendPasswordEmail(user.getEmailAddress(), user.getName(), user.getPassword());


                out.println("<html>");
                out.println("<head>");
                out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                out.println("</head>");
                out.println("<body>");
                out.println("<center>");
                out.println("<table cellspacing=\"4\">");
                out.println("<tr>");
                out.println("<td><label class=\"header\">Email successfully sent for " + user.getUsername() + "</label></td>");
                clearScreenFromSession(request);
                out.println("</tr>");
                out.println("</table>");
                out.println("<p> </p>");
                out.println("</center>");
                out.println("</body>");
                out.println("</html>");
            } else {

                out.println("<html>");
                out.println("<head>");
                out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                out.println("</head>");
                out.println("<body>");
                out.println("<center>");
                out.println("<table cellspacing=\"4\">");
                out.println("<tr>");
                out.println("<td><label class=\"error\"><h2>Invalid username captured!</h2></label></td>");
                clearScreenFromSession(request);
                out.println("</tr>");
                out.println("</table>");
                out.println("<p> </p>");
                out.println("</center>");
                out.println("</body>");
                out.println("</html>");
            }
            response.setHeader("Refresh", "1; URL=/ManagedCare/Login.jsp");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }

        /*try {
        String nextJSP = "/Login.jsp";
        RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

        dispatcher.forward(request, response);
        } catch (Exception ex) {
        ex.printStackTrace();
        }*/
        return null;
    }

    @Override
    public String getName() {
        return "SendForgotPasswordCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CcClaimLineSearchResult;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Johan-NB
 */
public class ViewPracticeClaimLineDetails extends NeoCommand {

    private static final Logger logger = Logger.getLogger(ViewPracticeClaimLineDetails.class.getName());
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        boolean isClearProviderClaim = (Boolean) session.getAttribute("persist_isClearingClaims");

        //logger.info("Inside ViewPracticeClaimLineDetails");
        int selectedClaimId = 0;
        String fromCLScreen = request.getParameter("onClaimLineScreen");
        if (fromCLScreen == null || fromCLScreen.equalsIgnoreCase("")) {
            selectedClaimId = Integer.parseInt(request.getParameter("pracClaimId"));
            //logger.log(Level.INFO, "The selected claim id ", selectedClaimId);
            System.out.println("ViewPracticeClaimLineDetails selected claim id = " + selectedClaimId);
        }

        String pracClaimMemberNumber = request.getParameter("pracClaimMemNum");
        session.setAttribute("policyNumber", pracClaimMemberNumber);
        CcClaimLineSearchResult claimLineResults = new CcClaimLineSearchResult();
        claimLineResults = (CcClaimLineSearchResult) session.getAttribute("pracCCClaimLineSearchResult");

        if (claimLineResults != null && claimLineResults.getClaimLines().isEmpty() == false) {

            int claimLineCount = claimLineResults.getClaimLineCount();
            int claimId = claimLineResults.getClaimId();
            int resultMin = claimLineResults.getResultMin();
            int resultMax = claimLineResults.getResultMax();
            String pageDirection = request.getParameter("pcPageDirection");
            if (pageDirection != null && !pageDirection.equalsIgnoreCase("")) {

                //logger.log(Level.INFO, "pageDirection triggered ");
                logger.log(Level.INFO, "pageDirection claimLineCount = ", claimLineCount);
                logger.log(Level.INFO, "pageDirection claimId = ", claimId);
                logger.log(Level.INFO, "pageDirection resultMin = ", resultMin);
                logger.log(Level.INFO, "pageDirection resultMax = ", resultMax);

                if (pageDirection.equalsIgnoreCase("forward")) {
                    resultMin = resultMin + 100;
                    resultMax = resultMax + 100;

                } else if (pageDirection.equalsIgnoreCase("backward")) {
                    resultMin = resultMin - 100;
                    resultMax = resultMax - 100;
                }
                logger.log(Level.INFO, "pageDirection new resultMin = ", resultMin);
                logger.log(Level.INFO, "pageDirection new resultMax = ", resultMax);

                claimLineResults = port.fetchCCClaimLineIdsByClaimId(isClearProviderClaim, claimId, resultMin, resultMax);

            } else {
                if (selectedClaimId != 0) {
                    claimLineResults = port.fetchCCClaimLineIdsByClaimId(isClearProviderClaim, selectedClaimId, 0, 100);
                }
                session.setAttribute("pracClaimResultMemNum", pracClaimMemberNumber);
            }
        } else {

            if (selectedClaimId != 0) {
                claimLineResults = port.fetchCCClaimLineIdsByClaimId(isClearProviderClaim, selectedClaimId, 0, 100);
            }
            session.setAttribute("pracClaimResultMemNum", pracClaimMemberNumber);

        }
        session.setAttribute("pracCCClaimLineSearchResult", claimLineResults);
        session.setAttribute("pracCCClaimLineDetails", claimLineResults.getClaimLines());

        try {
            String nextJSP = "/Claims/PracticeClaimsHistoryDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public String getName() {
        return "ViewPracticeClaimLineDetails";
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.command;

import com.agile.security.webservice.DentalCode;
import com.koh.command.FECommand;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author whauger
 */

public class SearchPreAuthDentalCodeCommand extends FECommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        String discipline = request.getParameter("disci");
        String dentalCode = request.getParameter("dental");

        List<DentalCode> dentalProtoList = new ArrayList<DentalCode>();
        try {
            if (discipline != null && !discipline.equals(""))
            {
                dentalProtoList = service.getAgileManagerPort().findDentalProtocolsByDiscipline(discipline);
            }
            else if (dentalCode != null && !dentalCode.equals(""))
            {
                dentalProtoList = service.getAgileManagerPort().findDentalProtocolsByCode(new Integer(dentalCode).intValue());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*
        TariffSearchResult r = new TariffSearchResult();
        r.setCode("10011");
        r.setDescription("Expensive");

        TariffSearchResult r2 = new TariffSearchResult();
        r2.setCode("20022");
        r2.setDescription("Very Expensive");

        tariffs.add(r);
        tariffs.add(r2);
        */

        request.setAttribute("searchDentalCodeResult", dentalProtoList);
        try {
            String nextJSP = "/Auth/DentalCodeSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SearchPreAuthDentalCodeCommand";
    }

}

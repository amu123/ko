
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.serv;
//General

import com.agile.command.*;
import com.koh.auth.command.*;
import com.koh.automation.command.*;
import com.koh.biometric.command.*;
import com.koh.command.Command;
import com.koh.fe.command.ActivateUserCommand;
import com.koh.fe.command.ActivateUserWLCommand;
import com.koh.fe.command.AddUserCommand;
import com.koh.fe.command.ChangePasswordCommand;
import com.koh.fe.command.LeftMenuCommand;
import com.koh.fe.command.LoginCommand;
import com.koh.fe.command.LogoutCommand;
import com.koh.fe.command.RejectUserCommand;
import com.koh.fe.command.TopMenuCommand;
import com.koh.fe.command.WorkflowContentCommand;
//Statements
//Call Tracking
import com.koh.calltrack.command.AllocateCallHistoryToSessionCommand;
import com.koh.calltrack.command.AllocateCallTrackWorkbench;
import com.koh.calltrack.command.CallCoverSearchCommand;
import com.koh.calltrack.command.CallWorkbenchCommand;
import com.koh.calltrack.command.FindProviderByCodeCommand;
import com.koh.calltrack.command.GenerateCallCoverCommand;
import com.koh.calltrack.command.LoadCallWorkbenchCommand;
import com.koh.calltrack.command.MemberCallTrackHistoryCommand;
import com.koh.calltrack.command.MemberDetailsCoverSearchCommand;
import com.koh.calltrack.command.MemberSearchAjaxCommand;
import com.koh.calltrack.command.ProviderCallTrackHistoryCommand;
import com.koh.calltrack.command.ReloadCallLogCommand;
import com.koh.calltrack.command.ReloadSearchHistoryCommand;
import com.koh.calltrack.command.RouteToUpdateCommand;
import com.koh.calltrack.command.SaveCallTrackDetailsCommand;
import com.koh.calltrack.command.SeachCallTrackHistoryCommand;
import com.koh.calltrack.command.SearchCallCoverByUpdateCommand;
import com.koh.calltrack.command.SearchMemberCoverCommand;
import com.koh.calltrack.command.UpdateCallCommand;
import com.koh.calltrack.command.UpdateCallTrackingCommand;
import com.koh.calltrack.command.UpdateLoadedCommand;
import com.koh.calltrack.command.WorkflowSaveCallTrackDetailsCommand;
import com.koh.calltrack.command.WorkflowFindPracticeByCodeCommand;
import com.koh.calltrack.command.WorkflowFindGroupsByNumberCommand;
//Auth
//PDC
import com.koh.pdc.command.AssignPHMCommand;
import com.koh.pdc.command.AssignTaskCommand;
import com.koh.pdc.command.BackToWorkbenchCommand;
import com.koh.pdc.command.BackToWorkbenchDetailsCommand;
import com.koh.pdc.command.ContactPatientCommand;
import com.koh.pdc.command.NextQuestionCommand;
import com.koh.pdc.command.SaveCaseManagementCommand;
import com.koh.pdc.command.SaveQuestionSetCommand;
import com.koh.pdc.command.SaveScheduledTaskCommand;
import com.koh.pdc.command.ScheduleTaskCommand;
import com.koh.pdc.command.SelectQuestionnaireCommand;
import com.koh.pdc.command.ShowCurrentQuestionnaire;
import com.koh.pdc.command.ShowOldQuestionnaire;
import com.koh.pdc.command.SubmitQuestionSetCommand;
import com.koh.pdc.command.UpdatePolicyHolderAddress;
import com.koh.pdc.command.ViewAddressCommand;
import com.koh.pdc.command.ViewAuthorizationCommand;
import com.koh.pdc.command.ViewBiometricsCommand;
import com.koh.pdc.command.ViewClaimDetailsCommand;
import com.koh.pdc.command.ViewClaimsCommand;
import com.koh.pdc.command.ViewClosedBenchmarkCommand;
import com.koh.pdc.command.ViewEventsCommand;
import com.koh.pdc.command.ViewMainBenchmarkCommand;
import com.koh.pdc.command.ViewOpenBenchmarkCommand;
import com.koh.pdc.command.ViewPathologyCommand;
import com.koh.pdc.command.ViewPolicyHolderDetailsCommand;
import com.koh.pdc.command.ViewReAssignPHMCommand;
import com.koh.pdc.command.ViewWorkBenchCommand;
import com.koh.pdc.command.GetAuthConfirmationDetailsForPDC;
import com.koh.pdc.command.BackToAuthorizationCommand;
import com.koh.pdc.command.CloseTaskCommand;
import com.koh.pdc.command.BackTaskViewCommand;
import com.koh.pdc.command.GetQuestionnaireDatesForPDC;
import com.koh.pdc.command.ForwardToMemberClaimLineClaimLinePDC;
import com.koh.pdc.command.ReturnMemberClaimDetailsPDC;
import com.koh.pdc.command.ViewPDCTasksByUserCommand;
// Biometrics
import com.koh.broker.command.*;
import com.koh.brokerFirm.AddBrokerFirmAccreditationCommand;
import com.koh.brokerFirm.CancelPayIndicatorCommand;
import com.koh.brokerFirm.ChangePayIndicatorCommand;
import com.koh.brokerFirm.FirmAuditCommand;
import com.koh.brokerFirm.SaveBrokerFirmBankingDetails;
import com.koh.brokerFirm.SavePayIndicatorCommand;
import com.koh.broker_firm.application.commands.*;
//Pre Auth
import com.koh.calltrack.command.CancelCallWorkbenchCommand;
import com.koh.calltrack.command.FindPracticeByCodeCommand;
import com.koh.calltrack.command.ForwardToSubCategroriesCommand;
import com.koh.calltrack.command.WorkflowMemberDetailsCoverSearchCommand;
import com.koh.calltrack.command.WorkflowReloadCallLogCommand;
import com.koh.calltrack.command.WorkflowUpdateLoadedCommand;
import com.koh.claims.command.*;
import com.koh.commission.command.CommissionCommand;
import com.koh.practice_management.command.ClearPracticeScreen;
import com.koh.practice_management.command.SavePracticeDetails;
import com.koh.preauth.command.AddAuthBasketSpecTariffToSession;
import com.koh.preauth.command.AddAuthBasketTariffToSession;
import com.koh.preauth.command.AddAuthCPTToSessionCommand;
import com.koh.preauth.command.AddAuthLOSToSessionCommand;
import com.koh.preauth.command.AddNewAuthNappiToSessionCommand;
import com.koh.preauth.command.AddAuthNappiToSession;
import com.koh.preauth.command.AddAuthNoteToList;
import com.koh.preauth.command.AddAuthTariffToSessionCommand;
import com.koh.preauth.command.AllocateAuthCPTToSession;
import com.koh.preauth.command.AllocateAuthHistoryToSession;
import com.koh.preauth.command.AllocateAuthHistoryXMLCommand;
import com.koh.preauth.command.AllocateAuthNewNappiToSession;
import com.koh.preauth.command.AllocateAuthNewTariffToSession;
import com.koh.preauth.command.AllocateAuthToSessionCommand;
import com.koh.preauth.command.AllocateBasketTariffSpecToSession;
import com.koh.preauth.command.AllocateBasketTariffToSession;
import com.koh.preauth.command.AllocateNappiToSession;
import com.koh.preauth.command.AllocatePreAuthICD10ToSession;
import com.koh.preauth.command.AllocatePreAuthMemberToSession;
import com.koh.preauth.command.AllocatePreAuthMultiICD10ToSession;
import com.koh.preauth.command.CalculateCoPaymentAmounts;
import com.koh.preauth.command.CalculateLOSFromDates;
import com.koh.preauth.command.CalculateNappiQuantityCommand;
import com.koh.preauth.command.CalculateOrthoPlanDate;
import com.koh.preauth.command.CalculateTariffQuantityCommand;
import com.koh.preauth.command.ClearAuthTariffsFromList;
import com.koh.preauth.command.ForwardGenericAuthorizationPortal;
import com.koh.preauth.command.ForwardGenericAuthorizationProviderPortal;
import com.koh.preauth.command.ForwardToAuthBasketTariffSearch;
import com.koh.preauth.command.ForwardToAuthLOC;
import com.koh.preauth.command.ForwardToAuthNappiBasketSearchCommand;
import com.koh.preauth.command.ForwardToAuthNappiSearch;
import com.koh.preauth.command.ForwardToAuthNewNappi;
import com.koh.preauth.command.ForwardToAuthSearchMember;
import com.koh.preauth.command.ForwardToAuthSpecificBasket;
import com.koh.preauth.command.ForwardToAuthSpecificCPT;
import com.koh.preauth.command.ForwardToAuthSpecificNappi;
import com.koh.preauth.command.ForwardToAuthSpecificNote;
import com.koh.preauth.command.ForwardToAuthTariffCommand;
import com.koh.preauth.command.ForwardToAuthTariffSearchBasketCommand;
import com.koh.preauth.command.ForwardToBasketICD;
import com.koh.preauth.command.ForwardToCPTSearch;
import com.koh.preauth.command.ForwardToLabSpecificTariff;
import com.koh.preauth.command.ForwardToNewAuthNappiSearch;
import com.koh.preauth.command.ForwardToNewAuthTariffSearch;
import com.koh.preauth.command.ForwardToPreAuthConfirmation;
import com.koh.preauth.command.ForwardToSearchPreAuthICD10;
import com.koh.preauth.command.ForwardToSearchPreAuthMultiICD10;
import com.koh.preauth.command.GetAuthBasketDetails;
import com.koh.preauth.command.GetAuthConfirmationDetails;
import com.koh.preauth.command.GetAuthLabCodeByTariffCommand;
import com.koh.preauth.command.GetAuthNappiByCodeCommand;
import com.koh.preauth.command.GetAuthOrthoPlanDetails;
import com.koh.preauth.command.GetAuthTariffByCodeCommand;
import com.koh.preauth.command.GetCPTByCodeCommand;
import com.koh.preauth.command.GetCoPaymentFromCPTs;
import com.koh.preauth.command.GetDependentCoverExclusions;
import com.koh.preauth.command.GetHospitalLengthOfStayDate;
import com.koh.preauth.command.GetHospitalStatusCommand;
import com.koh.preauth.command.LoadDefaultHospitalLOC;
import com.koh.preauth.command.ModifyAuthBasketTariffFromList;
import com.koh.preauth.command.ModifyAuthCPTCommand;
import com.koh.preauth.command.ModifyAuthLOCDetails;
import com.koh.preauth.command.ModifyAuthLabTariffList;
import com.koh.preauth.command.ModifyAuthNappiFromList;
import com.koh.preauth.command.ModifyAuthNewNappiCommand;
import com.koh.preauth.command.ModifyAuthNoLabTariffList;
import com.koh.preauth.command.ModifyAuthTariffCommand;
import com.koh.preauth.command.ReloadCoverExclusionCommand;
import com.koh.preauth.command.ReloadGenericPreAuthCommand;
import com.koh.preauth.command.ReloadPreAuthDentalDetailsCommand;
import com.koh.preauth.command.ReloadPreAuthOrthoDetailsCommand;
import com.koh.preauth.command.RemoveAuthBasketTariffFromList;
import com.koh.preauth.command.RemoveAuthCPTFromList;
import com.koh.preauth.command.RemoveAuthLOCFromList;
import com.koh.preauth.command.RemoveAuthNewNappiCommand;
import com.koh.preauth.command.RemoveAuthTariffCommand;
import com.koh.preauth.command.RemoveCompleteBasketFromList;
import com.koh.preauth.command.ResetAuthDetails;
import com.koh.preauth.command.ReturnAuthBasketTariffCommand;
import com.koh.preauth.command.ReturnAuthTariffCommand;
import com.koh.preauth.command.ReturnFromBasketICD;
import com.koh.preauth.command.ReturnLabToAuthCommand;
import com.koh.preauth.command.ReturnToAuthSearchCommand;
import com.koh.preauth.command.ReturnToGenericAuth;
import com.koh.preauth.command.ReturnToHospitalAuth;
import com.koh.preauth.command.SaveCallCenterPreAuth1Command;
import com.koh.preauth.command.SaveOrthoPreAuthDetailsCommand;
import com.koh.preauth.command.SavePreAuthDetailsCommand;
import com.koh.preauth.command.SavePreAuthOrthoPlanDetails;
import com.koh.preauth.command.SearchAuthHistoryNotes;
import com.koh.preauth.command.SearchNewPreAuthNappi;
import com.koh.preauth.command.SearchPreAuthBasketAllTariffs;
import com.koh.preauth.command.SearchPreAuthCommand;
import com.koh.preauth.command.SearchPreAuthDentalCodeCommand;
import com.koh.preauth.command.SearchPreAuthICD10;
import com.koh.preauth.command.SearchPreAuthNappi;
import com.koh.preauth.command.SearchPreAuthNewTariff;
import com.koh.preauth.command.SearchPreAuthTariff;
import com.koh.preauth.command.SetHospitalLevelOfCare;
import com.koh.preauth.command.ViewCoverExclusionCommand;
import com.koh.preauth.command.ViewPreAuthConfirmation;
import com.koh.preauth.update.command.PreAuthUpdateCommand;
import com.koh.preauth.update.command.SaveCallCenterPreAuthUpdate;
import com.koh.preauth.update.command.SaveUpdatedPreAuthDetailsCommand;
import com.koh.preauth.validation.ValidateConditionSpecific;
import com.koh.preauth.validation.ValidateGenericAuth;
import com.koh.preauth.validation.ValidateHospitalDetails;
import com.koh.preauth.validation.ValidateDentalDetails;

// Data Loader
import com.koh.dataload.command.AcceptStagingStatusCommand;
import com.koh.dataload.command.CancelUploadCommand;
import com.koh.dataload.command.PreAuthDocumentUploadCommand;
//import com.koh.dataload.command.LoadDataFileCommand;

//Claims
import com.koh.contribution.command.*;
import com.koh.cover.commands.*;
import com.koh.document.command.*;
import com.koh.employer.command.*;
import com.koh.fe.command.*;
import com.koh.member.application.MemberConsultantSearchCommand;
import com.koh.member.application.command.*;
import com.koh.nclaims.command.*;
//import com.koh.nclaims.command.ClaimHeaderCommand;
//import com.koh.nclaims.command.ClaimLineClaimLineCommand;
//import com.koh.nclaims.command.ClaimLineCommand;
//import com.koh.nclaims.command.ClaimLineNdcCommand;
//import com.koh.nclaims.command.SearchClaimCommand;
//import com.koh.nclaims.command.ClaimLineModifierCommand;
//import com.koh.nclaims.command.EditClaimCommand;
//import com.koh.nclaims.command.ForwardToClaimLineClaimLine;
//import com.koh.nclaims.command.OverrideClaim;
//import com.koh.nclaims.command.ReturnClaimDetails;
//import com.koh.nclaims.command.ReverseAndCorrectClaim;
//import com.koh.nclaims.command.ReverseClaim;
//import com.koh.nclaims.command.ReverseClaimLine;
//import com.koh.nclaims.command.ViewClaimLineDetails;
import com.koh.network_management.command.ForwardPNMaintenance;
import com.koh.pdc.command.*;
import com.koh.pdc.command.CoverDependantResultCommand;
import com.koh.pdc.command.CoverDependantResultFilterCommand;
import com.koh.practice_management.command.FindPracticeDetails;
import com.koh.practice_management.command.LoadPracticeMenuTabs;
import com.koh.practice_management.command.PracticeAuditTrailCommand;
import com.koh.practice_management.command.PracticeManagementNotesCommand;
import com.koh.practice_management.command.PracticeManagementTabContentCommand;
import com.koh.practice_management.command.SavePracticeAddressDetailsCommand;
import com.koh.practice_management.command.SavePracticeBankingDetailsCommand;
import com.koh.practice_management.command.SavePracticeContactDetailsCommand;
import com.koh.practice_management.command.UpdatePracticeNameCommand;
import com.koh.statement.command.ReloadStatementCommand;
import com.koh.preauth.command.AddLOCDowngrades;
import com.koh.preauth.command.AllocatePreAuthICD10ToSessionCommand;
import com.koh.preauth.command.AuthGetNappiList;
import com.koh.preauth.command.CheckIfMemberHasOHAuth;
import com.koh.preauth.command.DisplayCoverDepJoinDate;
import com.koh.preauth.command.ForwardToAuthHospDetailsCommand;
import com.koh.preauth.command.ForwardToAuthNappiProduct;
import com.koh.preauth.command.ForwardToAuthSpecificPMB;
import com.koh.preauth.command.ForwardToBenefitAllocation;
import com.koh.preauth.command.ForwardToICD10Search;
import com.koh.preauth.command.ForwardToPreAuthDocumentCommand;
import com.koh.preauth.command.ForwardToPreAuthRejectedConfirmation;
import com.koh.preauth.command.ForwardToSearchProduct;
import com.koh.preauth.command.GeneratePDFForPrint;
import com.koh.preauth.command.GetAuthCPTMappingForTariff;
import com.koh.preauth.command.GetNappiProductFromCode;
import com.koh.preauth.command.GetPMBByICDCommand;
import com.koh.preauth.command.GetRejectedAuthConfirmationDetails;
import com.koh.preauth.command.GetTariffCoPayDetails;
import com.koh.preauth.command.MapSelectedTCPTs;
import com.koh.preauth.command.ModNappiProdListCommand;
import com.koh.preauth.command.OverridePMB;
import com.koh.preauth.command.RefreshCommand;
import com.koh.preauth.command.ReloadPreauthGeneric;
import com.koh.preauth.command.RemoveAllocatedBenefit;
import com.koh.preauth.command.RemoveNappiProdFromListCommand;
import com.koh.preauth.command.ReturnAuthCommand;
import com.koh.preauth.command.ReturnCommand;
import com.koh.preauth.command.SaveProductCommand;
import com.koh.preauth.command.SearchPreAthICD10Command;
import com.koh.preauth.command.SendAuthConfirmation;
import com.koh.preauth.command.SetBenefitToAuth;
import com.koh.preauth.command.SetInternationalTravelTariffToAuth;
import com.koh.preauth.command.SetSelectedCoverDep;
import com.koh.preauth.command.SetSelectedPMBCode;
import com.koh.preauth.command.ViewPreAuthBenefitsCommand;
import com.koh.preauth.validation.PreAuthDateValidation;
import com.koh.preauth.validation.ValidateGenericAuthUpdate;
import com.koh.preauth.validation.ValidateHospiceCare;
import com.koh.preauth.validation.ValidateOpticalDetails;
import com.koh.preauth.validation.ValidateOrthoDetails;
import com.koh.preauth.validation.ValidatePreConditionSpec;
import com.koh.preauth.validation.ValidateTraumaCounselling;
import com.koh.qlink.QLinkErrorsCommand;
import com.koh.statement.command.*;
import com.koh.union.command.FowardToAdvancedUnionSearch;
import com.koh.union.command.LinkMembershipToUnionCommand;
import com.koh.union.command.MemberUnionAdvancedSearchCommand;
import com.koh.union.command.UnionAdvancedSearchCommand;
import com.koh.union.command.UnionCapturerCommand;
import com.koh.union.command.UnionNotesCommand;
import com.koh.union.command.UnionSearchCommand;
import com.koh.union.command.UnionUpdateCommand;
import com.koh.union.command.UnionViewCommand;
import com.koh.union.command.ViewGroupHistoryCommand;
import com.koh.union.command.ViewUnionAuditTrail;
import com.koh.union.command.ViewUnionDetailsCommand;
import com.koh.union.command.ViewUnionDocuments;
import com.koh.union.command.ViewUnionMemberHistoryCommand;
import com.koh.union.command.ViewUnionNotes;
import com.koh.unpaids.command.SaveUnpaidsCommand;
import com.koh.unpaids.command.SubmitUnpaidsDocumentCommand;
import com.koh.workflow.command.*;
import java.io.IOException;
//import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
//import javax.servlet.http.HttpSession;

//Amukelani Import
import com.koh.preauth.command.SearchAuthAmukelanim;
import com.koh.preauth.command.ForwardToAdvanceSearchAmu;
import com.koh.preauth.command.AdvanceSearchAuthAmu;

/**
 * Controller servlet. All processing should happen in the servlet
 *
 * @author gerritj
 */
public class AgileController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    HttpServletRequest request = null;
    HttpServletResponse response = null;
    HttpSession session = null;
    HashMap<String, Command> commands = new HashMap<String, Command>();
    private static final Logger logger = Logger.getLogger(AgileController.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

//        logger.info("Agile Controller reached");
        response.setContentType("text/html;charset=UTF-8");

        String command = null;

        try {
            Enumeration e = request.getParameterNames();

            while (e.hasMoreElements()) {
                String pName = "" + e.nextElement();

                if (pName.equalsIgnoreCase("opperation")) {
                    command = request.getParameter(pName);
                    break;
                }
            }
            //executes command link to opperation attribute in the form
            Command c = commands.get(command);
            if (c != null) {
                c.execute(request, response, getServletContext());
            }
        } catch (Exception e) {
            logger.log(Priority.ERROR, e);
            //out.close();
        } finally {
            //out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    /**
     * Builds a list of all accessible command
     */
    public void init() throws ServletException {
        ServletContext context = this.getServletContext();
        try {
            String client = new PropertiesReader().getProperty("Client");
            System.out.println("Client: " + client);
            context.setAttribute("Client", client);
        } catch (IOException ex) {
            System.out.println("Error capturing the Client: " + ex.getMessage());
        }
        try {
            String environment = new PropertiesReader().getProperty("Environment");
            System.out.println("Environment: " + environment);
            context.setAttribute("Environment", environment);
        } catch (IOException ex) {
            System.out.println("Error capturing the Environment: " + ex.getMessage());
        }
        /**
         * ******* General *******
         */

        CaptureManuallyRiskRateCommand cmrc = new CaptureManuallyRiskRateCommand();
        commands.put(cmrc.getName(), cmrc);

        RiskRateCommand rrc = new RiskRateCommand();
        commands.put(rrc.getName(), rrc);

        ViewBiometricsAsthmaCommand viewBiometricsAsthmaCommand = new ViewBiometricsAsthmaCommand();
        commands.put(viewBiometricsAsthmaCommand.getName(), viewBiometricsAsthmaCommand);

        ActivateUserCommand auc = new ActivateUserCommand();
        commands.put(auc.getName(), auc);
        ActivateUserWLCommand auqlc = new ActivateUserWLCommand();
        commands.put(auqlc.getName(), auqlc);
        AddUserCommand addUser = new AddUserCommand();
        commands.put(addUser.getName(), addUser);
        ChangePasswordCommand changePass = new ChangePasswordCommand();
        commands.put(changePass.getName(), changePass);
        LeftMenuCommand lmc = new LeftMenuCommand();
        commands.put(lmc.getName(), lmc);
        LoginCommand login = new LoginCommand();
        commands.put(login.getName(), login);
        LogoutCommand locom = new LogoutCommand();
        commands.put(locom.getName(), locom);
        RejectUserCommand ruc = new RejectUserCommand();
        commands.put(ruc.getName(), ruc);
        TopMenuCommand tmc = new TopMenuCommand();
        commands.put(tmc.getName(), tmc);
        WorkflowContentCommand wfcc = new WorkflowContentCommand();
        commands.put(wfcc.getName(), wfcc);
        WorkflowPopulateOptionPanelCommand wpopc = new WorkflowPopulateOptionPanelCommand();
        commands.put(wpopc.getName(), wpopc);
        SearchWorkflowCommand swfc = new SearchWorkflowCommand();
        commands.put(swfc.getName(), swfc);
        WorkflowCommand wfc = new WorkflowCommand();
        commands.put(wfc.getName(), wfc);
        getUserListByWorkflowQueueID ulbwq = new getUserListByWorkflowQueueID();
        commands.put(ulbwq.getName(), ulbwq);
        WorkflowEmailViewCommand wevc = new WorkflowEmailViewCommand();
        commands.put(wevc.getName(), wevc);
        ViewWorkbenchDetails vwd = new ViewWorkbenchDetails();
        commands.put(vwd.getName(), vwd);
        ValidateUserNameCommand validateUser = new ValidateUserNameCommand();
        commands.put(validateUser.getName(), validateUser);
        UpdateMyProfileCommand updateMyProfile = new UpdateMyProfileCommand();
        commands.put(updateMyProfile.getName(), updateMyProfile);
        CancelUserProfileCommand cancelUpdateProfile = new CancelUserProfileCommand();
        commands.put(cancelUpdateProfile.getName(), cancelUpdateProfile);
        SearchUsersProfilesCommand searchProfile = new SearchUsersProfilesCommand();
        commands.put(searchProfile.getName(), searchProfile);
        AllocateUserProfileToSessionCommand allocateProfile = new AllocateUserProfileToSessionCommand();
        commands.put(allocateProfile.getName(), allocateProfile);
        UpdateUserProfileCommand updateUserProfile = new UpdateUserProfileCommand();
        commands.put(updateUserProfile.getName(), updateUserProfile);
        LockUserCommand lockUser = new LockUserCommand();
        commands.put(lockUser.getName(), lockUser);
        //new commands
        GetCoverDetailsByNumber getEACov = new GetCoverDetailsByNumber();
        commands.put(getEACov.getName(), getEACov);
        GetActiveCoverDetailsByNumber getActiveEACov = new GetActiveCoverDetailsByNumber();
        commands.put(getActiveEACov.getName(), getActiveEACov);
        GetCoverDetailsByNumberWithDate getEACovByDate = new GetCoverDetailsByNumberWithDate();
        commands.put(getEACovByDate.getName(), getEACovByDate);
        FindProviderDetailsByCode findPD = new FindProviderDetailsByCode();
        commands.put(findPD.getName(), findPD);
        FindPracticeDetailsByCode findPrD = new FindPracticeDetailsByCode();
        commands.put(findPrD.getName(), findPrD);
        FindPracticeDetails findPD2 = new FindPracticeDetails();
        commands.put(findPD2.getName(), findPD2);
        GetICD10DetailsByCode icdSeacrh = new GetICD10DetailsByCode();
        commands.put(icdSeacrh.getName(), icdSeacrh);
        GetProductOptionCommand getProdOpt = new GetProductOptionCommand();
        commands.put(getProdOpt.getName(), getProdOpt);
        LeftMenuClearAllFromSession leftClearAll = new LeftMenuClearAllFromSession();
        commands.put(leftClearAll.getName(), leftClearAll);
        GetMultipleICD10DetailsByCode getMultiICD = new GetMultipleICD10DetailsByCode();
        commands.put(getMultiICD.getName(), getMultiICD);
        ManageSessionCommand manageSessionCommand = new ManageSessionCommand();
        commands.put(manageSessionCommand.getName(), manageSessionCommand);

        /**
         * *********** STATEMENTS ***********
         */
        GetPaymentRunDatesCommand payment = new GetPaymentRunDatesCommand();
        commands.put(payment.getName(), payment);
        SubmitIndividualStatementCommand submitis = new SubmitIndividualStatementCommand();
        commands.put(submitis.getName(), submitis);
        ReloadIndividualStatementCommand reloadis = new ReloadIndividualStatementCommand();
        commands.put(reloadis.getName(), reloadis);
        SubmitIndividualStatementForResoPortal submitResoSt = new SubmitIndividualStatementForResoPortal();
        commands.put(submitResoSt.getName(), submitResoSt);
        SubmitTaxCertificateCommand submitTax = new SubmitTaxCertificateCommand();
        commands.put(submitTax.getName(), submitTax);
        ViewTaxCertificateDetailsCommand taxDetails = new ViewTaxCertificateDetailsCommand();
        commands.put(taxDetails.getName(), taxDetails);
        ReloadTaxCertificateCommand reloadTax = new ReloadTaxCertificateCommand();
        commands.put(reloadTax.getName(), reloadTax);
        SubmitStatementCommand subStmt = new SubmitStatementCommand();
        commands.put(subStmt.getName(), subStmt);
        ReloadStatementCommand reloadStmt = new ReloadStatementCommand();
        commands.put(reloadStmt.getName(), reloadStmt);
        //StatementAvailabilityCommand sac = new StatementAvailabilityCommand();
        //commands.put(sac.getName(), sac);
        SubmitYTDStatementCommand ytdSTMT = new SubmitYTDStatementCommand();
        commands.put(ytdSTMT.getName(), ytdSTMT);
        ReloadYTDStatementCommand reloadYTD = new ReloadYTDStatementCommand();
        commands.put(reloadYTD.getName(), reloadYTD);

        /**
         * *********** Auth ***********
         */
        AllocateICD10ToSessionCommand aicd10sc = new AllocateICD10ToSessionCommand();
        commands.put(aicd10sc.getName(), aicd10sc);
        AllocateMemberToSessionCommand amtsc = new AllocateMemberToSessionCommand();
        commands.put(amtsc.getName(), amtsc);
        AllocateOrthoPlanToSessionCommand aopsc = new AllocateOrthoPlanToSessionCommand();
        commands.put(aopsc.getName(), aopsc);
        AllocateProcedureToSessionCommand aprocsc = new AllocateProcedureToSessionCommand();
        commands.put(aprocsc.getName(), aprocsc);
        AllocateProviderToSessionCommand aptsc = new AllocateProviderToSessionCommand();
        commands.put(aptsc.getName(), aptsc);
        AllocatePracticeToSessionCommand aprtsc = new AllocatePracticeToSessionCommand();
        commands.put(aprtsc.getName(), aprtsc);
        AllocateTariffToSessionCommand attsc = new AllocateTariffToSessionCommand();
        commands.put(attsc.getName(), attsc);
        ForwardToSearchICD10Command fsicd10 = new ForwardToSearchICD10Command();
        commands.put(fsicd10.getName(), fsicd10);
        ForwardToSearchMemberCommand ftsmc = new ForwardToSearchMemberCommand();
        commands.put(ftsmc.getName(), ftsmc);
        ForwardToSearchOrthoPlanCommand ftsopc = new ForwardToSearchOrthoPlanCommand();
        commands.put(ftsopc.getName(), ftsopc);
        ForwardToSearchProcedureCommand fsproc = new ForwardToSearchProcedureCommand();
        commands.put(fsproc.getName(), fsproc);
        ForwardToSearchProviderCommand ftspc = new ForwardToSearchProviderCommand();
        commands.put(ftspc.getName(), ftspc);
        ForwardToSearchPracticeCommand ftsprc = new ForwardToSearchPracticeCommand();
        commands.put(ftsprc.getName(), ftsprc);
        ForwardToSearchTariffCommand ftstc = new ForwardToSearchTariffCommand();
        commands.put(ftstc.getName(), ftstc);
        GetICD10ByCodeCommand icdCode = new GetICD10ByCodeCommand();
        commands.put(icdCode.getName(), icdCode);
        GetMemberByNumberCommand member = new GetMemberByNumberCommand();
        commands.put(member.getName(), member);
        MemberAppSearchPracticeCommand searchPrac = new MemberAppSearchPracticeCommand();
        commands.put(searchPrac.getName(), searchPrac);
        MemberDependantAppSearchPracticeCommand searchDepPrac = new MemberDependantAppSearchPracticeCommand();
        commands.put(searchDepPrac.getName(), searchDepPrac);
        MemberSearchPracticeCommand searchMemPrac = new MemberSearchPracticeCommand();
        commands.put(searchMemPrac.getName(), searchMemPrac);

        /**
         * *********** commands ***********
         */
        DocumentReIndexCommand docReIndexCommand = new DocumentReIndexCommand();
        commands.put(docReIndexCommand.getName(), docReIndexCommand);

        GetMemberByNumberAndDirCommand getMemberByNumberAndDirCommand = new GetMemberByNumberAndDirCommand();
        commands.put(getMemberByNumberAndDirCommand.getName(), getMemberByNumberAndDirCommand);
        GetActiveMemberByNumberCommand activeMember = new GetActiveMemberByNumberCommand();
        commands.put(activeMember.getName(), activeMember);
        GetProviderByNumberCommand provider = new GetProviderByNumberCommand();
        commands.put(provider.getName(), provider);
        GetProviderByNumberAndDirCommand getProviderByNumberAndDirCommand = new GetProviderByNumberAndDirCommand();
        commands.put(getProviderByNumberAndDirCommand.getName(), getProviderByNumberAndDirCommand);
        ReloadCallCenterAuth1Command reloadAuthCall1 = new ReloadCallCenterAuth1Command();
        commands.put(reloadAuthCall1.getName(), reloadAuthCall1);
        ReloadDentalDetailsCommand reloadDental = new ReloadDentalDetailsCommand();
        commands.put(reloadDental.getName(), reloadDental);
        ReloadOrthodonticDetailsCommand reloadOrtho = new ReloadOrthodonticDetailsCommand();
        commands.put(reloadOrtho.getName(), reloadOrtho);
        SaveAuthDetailsCommand sad = new SaveAuthDetailsCommand();
        commands.put(sad.getName(), sad);
        SaveCallCenterAuth1Command saveAuthCall1 = new SaveCallCenterAuth1Command();
        commands.put(saveAuthCall1.getName(), saveAuthCall1);
        SaveOrthoAuthDetailsCommand soad = new SaveOrthoAuthDetailsCommand();
        commands.put(soad.getName(), soad);
        SaveOrthoPlanDetailsCommand sopd = new SaveOrthoPlanDetailsCommand();
        commands.put(sopd.getName(), sopd);
        SearchAuthCommand saCom = new SearchAuthCommand();
        commands.put(saCom.getName(), saCom);
        SearchDentalCodeCommand sdcc = new SearchDentalCodeCommand();
        commands.put(sdcc.getName(), sdcc);
        SearchICD10Command sicdc = new SearchICD10Command();
        commands.put(sicdc.getName(), sicdc);
        SearchMemberCommand smc = new SearchMemberCommand();
        commands.put(smc.getName(), smc);
        SearchOpticalCodeCommand socc = new SearchOpticalCodeCommand();
        commands.put(socc.getName(), socc);
        SearchOrthoPlanCommand sopc = new SearchOrthoPlanCommand();
        commands.put(sopc.getName(), sopc);
        SearchProcedureCommand sproc = new SearchProcedureCommand();
        commands.put(sproc.getName(), sproc);
        SearchProviderCommand spc = new SearchProviderCommand();
        commands.put(spc.getName(), spc);
        SearchPracticeCommand sprc = new SearchPracticeCommand();
        commands.put(sprc.getName(), sprc);
        SearchTCodeCommand stcc = new SearchTCodeCommand();
        commands.put(stcc.getName(), stcc);
        SearchTariffCommand stc = new SearchTariffCommand();
        commands.put(stc.getName(), stc);

        //YE12
        SendAuthConfirmation sendAConf = new SendAuthConfirmation();
        commands.put(sendAConf.getName(), sendAConf);

        /**
         * *********** Call Tracking ***********
         */
        AllocateCallHistoryToSessionCommand alloCH = new AllocateCallHistoryToSessionCommand();
        commands.put(alloCH.getName(), alloCH);
        AllocateCallTrackWorkbench allCallWork = new AllocateCallTrackWorkbench();
        commands.put(allCallWork.getName(), allCallWork);
        CallCoverSearchCommand callSearch = new CallCoverSearchCommand();
        commands.put(callSearch.getName(), callSearch);
        CallWorkbenchCommand callWork = new CallWorkbenchCommand();
        commands.put(callWork.getName(), callWork);
        FindProviderByCodeCommand providerPractice = new FindProviderByCodeCommand();
        commands.put(providerPractice.getName(), providerPractice);
        GenerateCallCoverCommand g3c = new GenerateCallCoverCommand();
        commands.put(g3c.getName(), g3c);
        LoadCallWorkbenchCommand loadCall = new LoadCallWorkbenchCommand();
        commands.put(loadCall.getName(), loadCall);
        MemberCallTrackHistoryCommand memberSearch = new MemberCallTrackHistoryCommand();
        commands.put(memberSearch.getName(), memberSearch);
        MemberDetailsCoverSearchCommand coverDetails = new MemberDetailsCoverSearchCommand();
        commands.put(coverDetails.getName(), coverDetails);
        MemberSearchAjaxCommand searchAjax = new MemberSearchAjaxCommand();
        commands.put(searchAjax.getName(), searchAjax);
        ProviderCallTrackHistoryCommand providerSearch = new ProviderCallTrackHistoryCommand();
        commands.put(providerSearch.getName(), providerSearch);
        ReloadCallLogCommand reCallLog = new ReloadCallLogCommand();
        commands.put(reCallLog.getName(), reCallLog);
        ReloadSearchHistoryCommand history = new ReloadSearchHistoryCommand();
        commands.put(history.getName(), history);
        RouteToUpdateCommand route = new RouteToUpdateCommand();
        commands.put(route.getName(), route);
        SaveCallTrackDetailsCommand saveCall = new SaveCallTrackDetailsCommand();
        commands.put(saveCall.getName(), saveCall);
        SeachCallTrackHistoryCommand searchCallHistory = new SeachCallTrackHistoryCommand();
        commands.put(searchCallHistory.getName(), searchCallHistory);
        SearchCallCoverByUpdateCommand searchCoverUp = new SearchCallCoverByUpdateCommand();
        commands.put(searchCoverUp.getName(), searchCoverUp);
        SearchMemberCoverCommand searchCover = new SearchMemberCoverCommand();
        commands.put(searchCover.getName(), searchCover);
        UpdateCallCommand updateCall = new UpdateCallCommand();
        commands.put(updateCall.getName(), updateCall);
        UpdateCallTrackingCommand updateCallTrack = new UpdateCallTrackingCommand();
        commands.put(updateCallTrack.getName(), updateCallTrack);
        UpdateLoadedCommand updateloaded = new UpdateLoadedCommand();
        commands.put(updateloaded.getName(), updateloaded);
        WorkflowSaveCallTrackDetailsCommand wfSaveCT = new WorkflowSaveCallTrackDetailsCommand();
        commands.put(wfSaveCT.getName(), wfSaveCT);
        WorkflowUpdateLoadedCommand wulc = new WorkflowUpdateLoadedCommand();
        commands.put(wulc.getName(), wulc);
        WorkflowMemberDetailsCoverSearchCommand wfMemDetCSC = new WorkflowMemberDetailsCoverSearchCommand();
        commands.put(wfMemDetCSC.getName(), wfMemDetCSC);
        WorkflowReloadCallLogCommand WRCLC = new WorkflowReloadCallLogCommand();
        commands.put(WRCLC.getName(), WRCLC);
        WorkflowFindPracticeByCodeCommand WFPBCC = new WorkflowFindPracticeByCodeCommand();
        commands.put(WFPBCC.getName(), WFPBCC);
        ForwardToSubCategroriesCommand forwadToCategory = new ForwardToSubCategroriesCommand();
        commands.put(forwadToCategory.getName(), forwadToCategory);
        FindPracticeByCodeCommand findPractice = new FindPracticeByCodeCommand();
        commands.put(findPractice.getName(), findPractice);
        CancelCallWorkbenchCommand callBench = new CancelCallWorkbenchCommand();
        commands.put(callBench.getName(), callBench);
        WorkflowFindGroupsByNumberCommand WFGBN = new WorkflowFindGroupsByNumberCommand();
        commands.put(WFGBN.getName(), WFGBN);

        /**
         * *********** PDC ***********
         */
        AssignPHMCommand assPHM = new AssignPHMCommand();
        commands.put(assPHM.getName(), assPHM);
        AssignTaskCommand assTsk = new AssignTaskCommand();
        commands.put(assTsk.getName(), assTsk);
        BackToWorkbenchCommand bkWorkbe = new BackToWorkbenchCommand();
        commands.put(bkWorkbe.getName(), bkWorkbe);
        BackToWorkbenchDetailsCommand bkPoHoDeC = new BackToWorkbenchDetailsCommand();
        commands.put(bkPoHoDeC.getName(), bkPoHoDeC);
        ContactPatientCommand coPatC = new ContactPatientCommand();
        commands.put(coPatC.getName(), coPatC);
        SaveCaseManagementCommand svCaseM = new SaveCaseManagementCommand();
        commands.put(svCaseM.getName(), svCaseM);
        UpdatePolicyHolderAddress upAddress = new UpdatePolicyHolderAddress();
        commands.put(upAddress.getName(), upAddress);
        ViewAddressCommand vwAddrC = new ViewAddressCommand();
        commands.put(vwAddrC.getName(), vwAddrC);
        ViewAuthorizationCommand vwAthC = new ViewAuthorizationCommand();
        commands.put(vwAthC.getName(), vwAthC);
        ViewBiometricsCommand vwBioC = new ViewBiometricsCommand();
        commands.put(vwBioC.getName(), vwBioC);
        ViewClaimsCommand vwClaimsC = new ViewClaimsCommand();
        commands.put(vwClaimsC.getName(), vwClaimsC);
        ViewEventsCommand vwEveC = new ViewEventsCommand();
        commands.put(vwEveC.getName(), vwEveC);
        ViewMainBenchmarkCommand vwDetails = new ViewMainBenchmarkCommand();
        commands.put(vwDetails.getName(), vwDetails);
        ViewPathologyCommand vwPatC = new ViewPathologyCommand();
        commands.put(vwPatC.getName(), vwPatC);
        ViewPolicyHolderDetailsCommand vwPoHoDe = new ViewPolicyHolderDetailsCommand();
        commands.put(vwPoHoDe.getName(), vwPoHoDe);
        ViewReAssignPHMCommand vwReASPHM = new ViewReAssignPHMCommand();
        commands.put(vwReASPHM.getName(), vwReASPHM);
        ViewWorkBenchCommand vwWrkbch = new ViewWorkBenchCommand();
        commands.put(vwWrkbch.getName(), vwWrkbch);
        ViewClosedBenchmarkCommand svOpenM = new ViewClosedBenchmarkCommand();
        commands.put(svOpenM.getName(), svOpenM);
        ViewOpenBenchmarkCommand svClosedM = new ViewOpenBenchmarkCommand();
        commands.put(svClosedM.getName(), svClosedM);
        NextQuestionCommand nxtQuesComm = new NextQuestionCommand();
        commands.put(nxtQuesComm.getName(), nxtQuesComm);
        SelectQuestionnaireCommand selectQ = new SelectQuestionnaireCommand();
        commands.put(selectQ.getName(), selectQ);
        ScheduleTaskCommand scheduleT = new ScheduleTaskCommand();
        commands.put(scheduleT.getName(), scheduleT);
        SaveQuestionSetCommand svQC = new SaveQuestionSetCommand();
        commands.put(svQC.getName(), svQC);
        SubmitQuestionSetCommand submitQC = new SubmitQuestionSetCommand();
        commands.put(submitQC.getName(), submitQC);
        ShowCurrentQuestionnaire currentViewQ = new ShowCurrentQuestionnaire();
        commands.put(currentViewQ.getName(), currentViewQ);
        ShowOldQuestionnaire oldViewQ = new ShowOldQuestionnaire();
        commands.put(oldViewQ.getName(), oldViewQ);
        SaveScheduledTaskCommand svSchTask = new SaveScheduledTaskCommand();
        commands.put(svSchTask.getName(), svSchTask);
        ViewClaimDetailsCommand vwClaimsDetails = new ViewClaimDetailsCommand();
        commands.put(vwClaimsDetails.getName(), vwClaimsDetails);
        GetAuthConfirmationDetailsForPDC gtAuthDetailsForPDC = new GetAuthConfirmationDetailsForPDC();
        commands.put(gtAuthDetailsForPDC.getName(), gtAuthDetailsForPDC);
        BackToAuthorizationCommand bkAuthCommand = new BackToAuthorizationCommand();
        commands.put(bkAuthCommand.getName(), bkAuthCommand);
        CloseTaskCommand clTaskC = new CloseTaskCommand();
        commands.put(clTaskC.getName(), clTaskC);
        BackTaskViewCommand bkTaskView = new BackTaskViewCommand();
        commands.put(bkTaskView.getName(), bkTaskView);
        GetQuestionnaireDatesForPDC getDatesPDC = new GetQuestionnaireDatesForPDC();
        commands.put(getDatesPDC.getName(), getDatesPDC);
        ViewRatePatientCommand vwRiskP = new ViewRatePatientCommand();
        commands.put(vwRiskP.getName(), vwRiskP);
        ViewRiskRateHistoryCommand vwRiskH = new ViewRiskRateHistoryCommand();
        commands.put(vwRiskH.getName(), vwRiskH);
        GetMemberClaimDetailsPDCCommand getClaimD = new GetMemberClaimDetailsPDCCommand();
        commands.put(getClaimD.getName(), getClaimD);
        ViewMemberClaimLineDetails vwMemberClaimLine = new ViewMemberClaimLineDetails();
        commands.put(vwMemberClaimLine.getName(), vwMemberClaimLine);
        ViewMemberClaimLineDetailsPDC vwMemberClaim = new ViewMemberClaimLineDetailsPDC();
        commands.put(vwMemberClaim.getName(), vwMemberClaim);
        ViewMemberClaimLineClaimLinePDC vwMemberLine = new ViewMemberClaimLineClaimLinePDC();
        commands.put(vwMemberLine.getName(), vwMemberLine);
        ReturnToClaimHistoryCommand returnHistory = new ReturnToClaimHistoryCommand();
        commands.put(returnHistory.getName(), returnHistory);
        ViewPolicyDetailsCommand vwPolicy = new ViewPolicyDetailsCommand();
        commands.put(vwPolicy.getName(), vwPolicy);
        PreviewAuthCommand prvAuth = new PreviewAuthCommand();
        commands.put(prvAuth.getName(), prvAuth);
        PreviewBiometricsCommand prvBio = new PreviewBiometricsCommand();
        commands.put(prvBio.getName(), prvBio);
        NextworkbenchListCommand nextList = new NextworkbenchListCommand();
        commands.put(nextList.getName(), nextList);
        SaveRiskRateCommand saveRisk = new SaveRiskRateCommand();
        commands.put(saveRisk.getName(), saveRisk);
        PreviewClaimHistory previewCliamHistory = new PreviewClaimHistory();
        commands.put(previewCliamHistory.getName(), previewCliamHistory);
        NextEventHistoryListCommand nextHistory = new NextEventHistoryListCommand();
        commands.put(nextHistory.getName(), nextHistory);
        ViewManageEventCommand vwManage = new ViewManageEventCommand();
        commands.put(vwManage.getName(), vwManage);
        ViewCDLStatusCommand viewCDL = new ViewCDLStatusCommand();
        commands.put(viewCDL.getName(), viewCDL);
        SaveMemberCDLStatusCommand saveCDLStatus = new SaveMemberCDLStatusCommand();
        commands.put(saveCDLStatus.getName(), saveCDLStatus);
        SavePdcContactDetails pdcContactDetails = new SavePdcContactDetails();
        commands.put(pdcContactDetails.getName(), pdcContactDetails);
        ViewWorkbenchFromTabIntex viewWorkbenchFromTabIntex = new ViewWorkbenchFromTabIntex();
        commands.put(viewWorkbenchFromTabIntex.getName(), viewWorkbenchFromTabIntex);
        GetContactHistoryCommand getContactHistoryCommand = new GetContactHistoryCommand();
        commands.put(getContactHistoryCommand.getName(), getContactHistoryCommand);
        ViewSubWorkBenchCommand getViewSubWorkBenchCommand = new ViewSubWorkBenchCommand();
        commands.put(getViewSubWorkBenchCommand.getName(), getViewSubWorkBenchCommand);
        ManageMyPatientCommand getManageMyPatientCommand = new ManageMyPatientCommand();
        commands.put(getManageMyPatientCommand.getName(), getManageMyPatientCommand);
        SaveManageMyPatientCommand getSaveManageMyPatientCommand = new SaveManageMyPatientCommand();
        commands.put(getSaveManageMyPatientCommand.getName(), getSaveManageMyPatientCommand);
        ViewChronicConditionSelectionCommand viewChronicConditionSelectionCommand = new ViewChronicConditionSelectionCommand();
        commands.put(viewChronicConditionSelectionCommand.getName(), viewChronicConditionSelectionCommand);
        CreateDependantCarePathCommand createDependantCarePathCommand = new CreateDependantCarePathCommand();
        commands.put(createDependantCarePathCommand.getName(), createDependantCarePathCommand);
        AssignCarePathTaskCommand assignCarePathTaskCommand = new AssignCarePathTaskCommand();
        commands.put(assignCarePathTaskCommand.getName(), assignCarePathTaskCommand);
        RemoveCarePathTaskCommand removeCarePathTaskCommand = new RemoveCarePathTaskCommand();
        commands.put(removeCarePathTaskCommand.getName(), removeCarePathTaskCommand);
        CreateCarePathTaskCommand createCarePathTaskCommand = new CreateCarePathTaskCommand();
        commands.put(createCarePathTaskCommand.getName(), createCarePathTaskCommand);
        ViewDepCarePathCommand viewDepCarePathCommand = new ViewDepCarePathCommand();
        commands.put(viewDepCarePathCommand.getName(), viewDepCarePathCommand);
        SwitchCarePathYearCommand switchCarePathYearCommand = new SwitchCarePathYearCommand();
        commands.put(switchCarePathYearCommand.getName(), switchCarePathYearCommand);
        ViewAssignPHMCommand viewAssignPHMCommand = new ViewAssignPHMCommand();
        commands.put(viewAssignPHMCommand.getName(), viewAssignPHMCommand);
        /**
         * *********** PDC NOTES ***********
         */
        ViewNoteDetailsCommand viewNote = new ViewNoteDetailsCommand();
        commands.put(viewNote.getName(), viewNote);
        AddPdcNoteToList pdcNote = new AddPdcNoteToList();
        commands.put(pdcNote.getName(), pdcNote);

        ForwardToMemberClaimLineClaimLinePDC viewMemCLCLPDC = new ForwardToMemberClaimLineClaimLinePDC();
        commands.put(viewMemCLCLPDC.getName(), viewMemCLCLPDC);
        ReturnMemberClaimDetailsPDC returnMemClaimsPDC = new ReturnMemberClaimDetailsPDC();
        commands.put(returnMemClaimsPDC.getName(), returnMemClaimsPDC);

        /**
         * *********** Biometrics Commands ***********
         */
        CaptureAsthmaBiometricsCommand captureAsthma = new CaptureAsthmaBiometricsCommand();
        commands.put(captureAsthma.getName(), captureAsthma);
        CaptureCOPDBiometricsCommand captureCOPD = new CaptureCOPDBiometricsCommand();
        commands.put(captureCOPD.getName(), captureCOPD);
        CaptureCardiacBiometrics captureCardiac = new CaptureCardiacBiometrics();
        commands.put(captureCardiac.getName(), captureCardiac);
        CaptureChronicBiometricsCommand captureChronic = new CaptureChronicBiometricsCommand();
        commands.put(captureChronic.getName(), captureChronic);
        CaptureCoronaryDiseaseCommand captureCoronary = new CaptureCoronaryDiseaseCommand();
        commands.put(captureCoronary.getName(), captureCoronary);
        CaptureDiabetesBiometericsCommand captureDiabetes = new CaptureDiabetesBiometericsCommand();
        commands.put(captureDiabetes.getName(), captureDiabetes);
        CaptureGeneralBiometricsCommand captureGeneral = new CaptureGeneralBiometricsCommand();
        commands.put(captureGeneral.getName(), captureGeneral);
        CaptureHyperlipidaemiaBiometricsCommand captureHyperlipidaemia = new CaptureHyperlipidaemiaBiometricsCommand();
        commands.put(captureHyperlipidaemia.getName(), captureHyperlipidaemia);
        CaptureHypertensionBiometricsCommand captureHypertension = new CaptureHypertensionBiometricsCommand();
        commands.put(captureHypertension.getName(), captureHypertension);
        CapturePathologyCommand capturePathology = new CapturePathologyCommand();
        commands.put(capturePathology.getName(), capturePathology);
        CaptureOtherCommand captureOtherCommand = new CaptureOtherCommand();
        commands.put(captureOtherCommand.getName(), captureOtherCommand);
        CaptureMajorDepressionCommand depression = new CaptureMajorDepressionCommand();
        commands.put(depression.getName(), depression);
        ResetAsthmaBiometricsCommand resetAsthma = new ResetAsthmaBiometricsCommand();
        commands.put(resetAsthma.getName(), resetAsthma);
        ResetCOPDBiometricsCommand resetCOPD = new ResetCOPDBiometricsCommand();
        commands.put(resetCOPD.getName(), resetCOPD);
        ResetCardiacBiometricCommand resetCardiac = new ResetCardiacBiometricCommand();
        commands.put(resetCardiac.getName(), resetCardiac);
        ResetChronicBiometricCommand resetChronic = new ResetChronicBiometricCommand();
        commands.put(resetChronic.getName(), resetChronic);
        ResetCoronaryBiometricsCommand resetCoronary = new ResetCoronaryBiometricsCommand();
        commands.put(resetCoronary.getName(), resetCoronary);
        ResetDiabetesBiometrics resetDiabetes = new ResetDiabetesBiometrics();
        commands.put(resetDiabetes.getName(), resetDiabetes);
        ResetGeneralBiometricsCommand resetGeneral = new ResetGeneralBiometricsCommand();
        commands.put(resetGeneral.getName(), resetGeneral);
        ResetHyperlipidaemiaBiometricsCommand resetHyperlipidaemia = new ResetHyperlipidaemiaBiometricsCommand();
        commands.put(resetHyperlipidaemia.getName(), resetHyperlipidaemia);
        ResetHypertensionCommand resetHypertension = new ResetHypertensionCommand();
        commands.put(resetHypertension.getName(), resetHypertension);
        ResetMajorDepressionBiometricsCommand resetMajordepression = new ResetMajorDepressionBiometricsCommand();
        commands.put(resetMajordepression.getName(), resetMajordepression);
        ResetOtherCommand resetOtherCommand = new ResetOtherCommand();
        commands.put(resetOtherCommand.getName(), resetOtherCommand);
        ResetPathologyCommand resetPathology = new ResetPathologyCommand();
        commands.put(resetPathology.getName(), resetPathology);
        ResetTarrifComamnd resetTariff = new ResetTarrifComamnd();
        commands.put(resetTariff.getName(), resetTariff);
        LoadQuestionsCommand questions = new LoadQuestionsCommand();
        commands.put(questions.getName(), questions);
        ForwardToSearchICDCommand icd10 = new ForwardToSearchICDCommand();
        commands.put(icd10.getName(), icd10);
        GetICD10ListCommand icD10ListCommand = new GetICD10ListCommand();
        commands.put(icD10ListCommand.getName(), icD10ListCommand);
        AddBiometricsTariffToSessionCommand biometricstariff = new AddBiometricsTariffToSessionCommand();
        commands.put(biometricstariff.getName(), biometricstariff);
        ReturnBiometricsTariffCommand tariffreturn = new ReturnBiometricsTariffCommand();
        commands.put(tariffreturn.getName(), tariffreturn);
        LoadBiometricsTariffCodeCommand loadTariff = new LoadBiometricsTariffCodeCommand();
        commands.put(loadTariff.getName(), loadTariff);
        LoadHypertensionQuestionsCommand loadHypertension = new LoadHypertensionQuestionsCommand();
        commands.put(loadHypertension.getName(), loadHypertension);
        LoadChronicQuestionsCommand loadChronic = new LoadChronicQuestionsCommand();
        commands.put(loadChronic.getName(), loadChronic);
        LoadCardiacFailureQuestionsCommand loadCardiac = new LoadCardiacFailureQuestionsCommand();
        commands.put(loadCardiac.getName(), loadCardiac);
        LoadDiabetesQuestionsCommand loadDiabetes = new LoadDiabetesQuestionsCommand();
        commands.put(loadDiabetes.getName(), loadDiabetes);
        LoadCoronaryQuestionsCommand loadCoronary = new LoadCoronaryQuestionsCommand();
        commands.put(loadCoronary.getName(), loadCoronary);
        LoadCOPDQuestionsCommand loadCOPD = new LoadCOPDQuestionsCommand();
        commands.put(loadCOPD.getName(), loadCOPD);
        LoadHyperlipidaemiaQuestionsCommand loadHyperlipidaemia = new LoadHyperlipidaemiaQuestionsCommand();
        commands.put(loadHyperlipidaemia.getName(), loadHyperlipidaemia);
        LoadMajorDepressionQuestionsCommand loadMajorDepression = new LoadMajorDepressionQuestionsCommand();
        commands.put(loadMajorDepression.getName(), loadMajorDepression);
        LoadGeneralBiometricsQuestionsCommand loadGeneral = new LoadGeneralBiometricsQuestionsCommand();
        commands.put(loadGeneral.getName(), loadGeneral);
        ForwardToTarrifSearchCommand forwardTariff = new ForwardToTarrifSearchCommand();
        commands.put(forwardTariff.getName(), forwardTariff);
        RemoveAllFromSession clearSession = new RemoveAllFromSession();
        commands.put(clearSession.getName(), clearSession);
        PutSourceCommand putSource = new PutSourceCommand();
        commands.put(putSource.getName(), putSource);
        AllocateBiometricsICD10ToSessionCommand allocateICD10 = new AllocateBiometricsICD10ToSessionCommand();
        commands.put(allocateICD10.getName(), allocateICD10);
        AllocateBiometricsTariffToSessionCommand allocateTariff = new AllocateBiometricsTariffToSessionCommand();
        commands.put(allocateTariff.getName(), allocateTariff);
        SearchBiometricsICD10Command searchBiometricsICD = new SearchBiometricsICD10Command();
        commands.put(searchBiometricsICD.getName(), searchBiometricsICD);
        SearchBiometricsTariffCommand searchBiometricsTariff = new SearchBiometricsTariffCommand();
        commands.put(searchBiometricsTariff.getName(), searchBiometricsTariff);
        LoadICD10BiometricsCommand loadICD10 = new LoadICD10BiometricsCommand();
        commands.put(loadICD10.getName(), loadICD10);
        LoadICDLongDescriptionsCommand longIcd = new LoadICDLongDescriptionsCommand();
        commands.put(longIcd.getName(), longIcd);
        LoadLongICD10DescriptionForHypertensionCommand longHypertension = new LoadLongICD10DescriptionForHypertensionCommand();
        commands.put(longHypertension.getName(), longHypertension);
        LoadICD10DescriptionChronicCommand chronicDesc = new LoadICD10DescriptionChronicCommand();
        commands.put(chronicDesc.getName(), chronicDesc);
        LongICD10CardiacCommand longCardiac = new LongICD10CardiacCommand();
        commands.put(longCardiac.getName(), longCardiac);
        LongDescriptionHyperlipidaemiaCommand longHyper = new LongDescriptionHyperlipidaemiaCommand();
        commands.put(longHyper.getName(), longHyper);
        LongICD10CoronaryCommand longCoronary = new LongICD10CoronaryCommand();
        commands.put(longCoronary.getName(), longCoronary);
        LongICDDepressionCommand longDepression = new LongICDDepressionCommand();
        commands.put(longDepression.getName(), longDepression);
        LongICDDiabetesComamnd longDiabetes = new LongICDDiabetesComamnd();
        commands.put(longDiabetes.getName(), longDiabetes);
        LongICDCOPDCommand longCopd = new LongICDCOPDCommand();
        commands.put(longCopd.getName(), longCopd);
        SearchBiometrics searchBiometrics = new SearchBiometrics();
        commands.put(searchBiometrics.getName(), searchBiometrics);

        BiometricsTabsCommand biometricsTabsCommand = new BiometricsTabsCommand();
        commands.put(biometricsTabsCommand.getName(), biometricsTabsCommand);

        ResetBiometricsCommand resetBiometricsCommand = new ResetBiometricsCommand();
        commands.put(resetBiometricsCommand.getName(), resetBiometricsCommand);

        CaptureBiometricsCommand captureBiometricsCommand = new CaptureBiometricsCommand();
        commands.put(captureBiometricsCommand.getName(), captureBiometricsCommand);

        CaptureSessionCommand captureSessionCommand = new CaptureSessionCommand();
        commands.put(captureSessionCommand.getName(), captureSessionCommand);

        /**
         * *********** PRE AUTH ***********
         */
        AddAuthTariffToSessionCommand addAT = new AddAuthTariffToSessionCommand();
        commands.put(addAT.getName(), addAT);
        AllocateAuthHistoryToSession aHis = new AllocateAuthHistoryToSession();
        commands.put(aHis.getName(), aHis);
        AllocateAuthHistoryXMLCommand authXml = new AllocateAuthHistoryXMLCommand();
        commands.put(authXml.getName(), authXml);
        AllocateAuthToSessionCommand allAuth = new AllocateAuthToSessionCommand();
        commands.put(allAuth.getName(), allAuth);
        AllocatePreAuthMemberToSession alloAM = new AllocatePreAuthMemberToSession();
        commands.put(alloAM.getName(), alloAM);
        CalculateTariffQuantityCommand calTAmount = new CalculateTariffQuantityCommand();
        commands.put(calTAmount.getName(), calTAmount);
        ForwardToAuthSearchMember ftAM = new ForwardToAuthSearchMember();
        commands.put(ftAM.getName(), ftAM);
        ForwardToAuthTariffCommand frAuthTariff = new ForwardToAuthTariffCommand();
        commands.put(frAuthTariff.getName(), frAuthTariff);
        GetAuthTariffByCodeCommand getAT = new GetAuthTariffByCodeCommand();
        commands.put(getAT.getName(), getAT);
        ModifyAuthTariffCommand modifyAT = new ModifyAuthTariffCommand();
        commands.put(modifyAT.getName(), modifyAT);
        ReloadCoverExclusionCommand reloadEx = new ReloadCoverExclusionCommand();
        commands.put(reloadEx.getName(), reloadEx);
        ReloadPreAuthDentalDetailsCommand reloadPreDental = new ReloadPreAuthDentalDetailsCommand();
        commands.put(reloadPreDental.getName(), reloadPreDental);
        ReloadPreAuthOrthoDetailsCommand reloadPreOrtho = new ReloadPreAuthOrthoDetailsCommand();
        commands.put(reloadPreOrtho.getName(), reloadPreOrtho);
        RemoveAuthTariffCommand removeAT = new RemoveAuthTariffCommand();
        commands.put(removeAT.getName(), removeAT);
        ReturnAuthTariffCommand returnAT = new ReturnAuthTariffCommand();
        commands.put(returnAT.getName(), returnAT);
        SaveCallCenterPreAuth1Command saveCCPre = new SaveCallCenterPreAuth1Command();
        commands.put(saveCCPre.getName(), saveCCPre);
        SaveOrthoPreAuthDetailsCommand savePreOrtho = new SaveOrthoPreAuthDetailsCommand();
        commands.put(savePreOrtho.getName(), savePreOrtho);
        SavePreAuthDetailsCommand savePAD = new SavePreAuthDetailsCommand();
        commands.put(savePAD.getName(), savePAD);
        SearchPreAuthCommand searchPA = new SearchPreAuthCommand();
        commands.put(searchPA.getName(), searchPA);
        SearchPreAuthDentalCodeCommand searchPreDental = new SearchPreAuthDentalCodeCommand();
        commands.put(searchPreDental.getName(), searchPreDental);
        ViewCoverExclusionCommand viewEx = new ViewCoverExclusionCommand();
        commands.put(viewEx.getName(), viewEx);
        //new commands
        GetDependentCoverExclusions getDepEx = new GetDependentCoverExclusions();
        commands.put(getDepEx.getName(), getDepEx);
        ReloadGenericPreAuthCommand reloadGen = new ReloadGenericPreAuthCommand();
        commands.put(reloadGen.getName(), reloadGen);
        GetAuthLabCodeByTariffCommand getLabForTariff = new GetAuthLabCodeByTariffCommand();
        commands.put(getLabForTariff.getName(), getLabForTariff);
        ValidateGenericAuth validatePreAuth = new ValidateGenericAuth();
        commands.put(validatePreAuth.getName(), validatePreAuth);
        ForwardToLabSpecificTariff forwardLabT = new ForwardToLabSpecificTariff();
        commands.put(forwardLabT.getName(), forwardLabT);
        GetAuthOrthoPlanDetails getAuthOrtho = new GetAuthOrthoPlanDetails();
        commands.put(getAuthOrtho.getName(), getAuthOrtho);
        AllocatePreAuthICD10ToSession alloPreAuthICD = new AllocatePreAuthICD10ToSession();
        commands.put(alloPreAuthICD.getName(), alloPreAuthICD);
        ForwardToSearchPreAuthICD10 forwardPreAuthICD = new ForwardToSearchPreAuthICD10();
        commands.put(forwardPreAuthICD.getName(), forwardPreAuthICD);
        SearchPreAuthICD10 preAuthICDSearch = new SearchPreAuthICD10();
        commands.put(preAuthICDSearch.getName(), preAuthICDSearch);
        SavePreAuthOrthoPlanDetails savePreOrthoPlan = new SavePreAuthOrthoPlanDetails();
        commands.put(savePreOrthoPlan.getName(), savePreOrthoPlan);
        ModifyAuthLabTariffList modifyAuthLab = new ModifyAuthLabTariffList();
        commands.put(modifyAuthLab.getName(), modifyAuthLab);
        ForwardToCPTSearch forwardCPT = new ForwardToCPTSearch();
        commands.put(forwardCPT.getName(), forwardCPT);
        AddAuthCPTToSessionCommand addCPTToSession = new AddAuthCPTToSessionCommand();
        commands.put(addCPTToSession.getName(), addCPTToSession);
        ReturnLabToAuthCommand returnLabT = new ReturnLabToAuthCommand();
        commands.put(returnLabT.getName(), returnLabT);
        ReturnToHospitalAuth returnToHosp = new ReturnToHospitalAuth();
        commands.put(returnToHosp.getName(), returnToHosp);
        GetCPTByCodeCommand getPreAuthCpt = new GetCPTByCodeCommand();
        commands.put(getPreAuthCpt.getName(), getPreAuthCpt);
        ForwardToAuthSpecificCPT forwardSpecCPT = new ForwardToAuthSpecificCPT();
        commands.put(forwardSpecCPT.getName(), forwardSpecCPT);
        AllocateAuthCPTToSession alloPreAuthCpt = new AllocateAuthCPTToSession();
        commands.put(alloPreAuthCpt.getName(), alloPreAuthCpt);
        SearchCPTCommand searchCPT = new SearchCPTCommand();
        commands.put(searchCPT.getName(), searchCPT);
        ResetAuthDetails resetAuth = new ResetAuthDetails();
        commands.put(resetAuth.getName(), resetAuth);
        ViewPreAuthConfirmation viewConfirmedAuth = new ViewPreAuthConfirmation();
        commands.put(viewConfirmedAuth.getName(), viewConfirmedAuth);
        ForwardToPreAuthConfirmation forwardConfirmedAuth = new ForwardToPreAuthConfirmation();
        commands.put(forwardConfirmedAuth.getName(), forwardConfirmedAuth);
        ModifyAuthCPTCommand modifyCPT = new ModifyAuthCPTCommand();
        commands.put(modifyCPT.getName(), modifyCPT);
        RemoveAuthCPTFromList removeCPT = new RemoveAuthCPTFromList();
        commands.put(removeCPT.getName(), removeCPT);
        CalculateOrthoPlanDate calOrthoPlan = new CalculateOrthoPlanDate();
        commands.put(calOrthoPlan.getName(), calOrthoPlan);
        GetAuthConfirmationDetails getAuthCon = new GetAuthConfirmationDetails();
        commands.put(getAuthCon.getName(), getAuthCon);
        ReturnToAuthSearchCommand returnAuthSearch = new ReturnToAuthSearchCommand();
        commands.put(returnAuthSearch.getName(), returnAuthSearch);
        GetAuthBasketDetails getAuthBasket = new GetAuthBasketDetails();
        commands.put(getAuthBasket.getName(), getAuthBasket);
        ForwardToAuthSpecificBasket forwardBasketT = new ForwardToAuthSpecificBasket();
        commands.put(forwardBasketT.getName(), forwardBasketT);
        ForwardToAuthLOC forwardLOC = new ForwardToAuthLOC();
        commands.put(forwardLOC.getName(), forwardLOC);
        GetHospitalLengthOfStayDate getHospLos = new GetHospitalLengthOfStayDate();
        commands.put(getHospLos.getName(), getHospLos);
        AddAuthLOSToSessionCommand addHospLoc = new AddAuthLOSToSessionCommand();
        commands.put(addHospLoc.getName(), addHospLoc);
        ModifyAuthLOCDetails modifyAuthLoc = new ModifyAuthLOCDetails();
        commands.put(modifyAuthLoc.getName(), modifyAuthLoc);
        RemoveAuthLOCFromList removeAuthLoc = new RemoveAuthLOCFromList();
        commands.put(removeAuthLoc.getName(), removeAuthLoc);
        SetHospitalLevelOfCare setHospLOCToSes = new SetHospitalLevelOfCare();
        commands.put(setHospLOCToSes.getName(), setHospLOCToSes);
        SearchPreAuthTariff searchPAT = new SearchPreAuthTariff();
        commands.put(searchPAT.getName(), searchPAT);
        ForwardToAuthSpecificBasket forwardToSpecBasket = new ForwardToAuthSpecificBasket();
        commands.put(forwardToSpecBasket.getName(), forwardToSpecBasket);
        AllocateBasketTariffToSession alloSpecBasket = new AllocateBasketTariffToSession();
        commands.put(alloSpecBasket.getName(), alloSpecBasket);
        ForwardToAuthBasketTariffSearch forwardToSearchBasketT = new ForwardToAuthBasketTariffSearch();
        commands.put(forwardToSearchBasketT.getName(), forwardToSearchBasketT);
        RemoveAuthBasketTariffFromList removeAuthBT = new RemoveAuthBasketTariffFromList();
        commands.put(removeAuthBT.getName(), removeAuthBT);
        ModifyAuthBasketTariffFromList modifyAuthBT = new ModifyAuthBasketTariffFromList();
        commands.put(modifyAuthBT.getName(), modifyAuthBT);
        AddAuthBasketTariffToSession addBTToSession = new AddAuthBasketTariffToSession();
        commands.put(addBTToSession.getName(), addBTToSession);
        ClearAuthTariffsFromList clearAt = new ClearAuthTariffsFromList();
        commands.put(clearAt.getName(), clearAt);
        LoadDefaultHospitalLOC loadHospLoc = new LoadDefaultHospitalLOC();
        commands.put(loadHospLoc.getName(), loadHospLoc);
        ReturnToGenericAuth returnAuth = new ReturnToGenericAuth();
        commands.put(returnAuth.getName(), returnAuth);
        ModifyAuthNoLabTariffList modifyNoLabT = new ModifyAuthNoLabTariffList();
        commands.put(modifyNoLabT.getName(), modifyNoLabT);
        CalculateCoPaymentAmounts calCoPay = new CalculateCoPaymentAmounts();
        commands.put(calCoPay.getName(), calCoPay);
        AllocatePreAuthMultiICD10ToSession alloMultiICD = new AllocatePreAuthMultiICD10ToSession();
        commands.put(alloMultiICD.getName(), alloMultiICD);
        ForwardToSearchPreAuthMultiICD10 forwardMultiICD = new ForwardToSearchPreAuthMultiICD10();
        commands.put(forwardMultiICD.getName(), forwardMultiICD);
        ReturnAuthBasketTariffCommand returnABT = new ReturnAuthBasketTariffCommand();
        commands.put(returnABT.getName(), returnABT);
        ForwardToAuthSpecificNappi forwardNappi = new ForwardToAuthSpecificNappi();
        commands.put(forwardNappi.getName(), forwardNappi);
        ForwardToAuthNappiSearch forwardToNapSearch = new ForwardToAuthNappiSearch();
        commands.put(forwardToNapSearch.getName(), forwardToNapSearch);
        SearchPreAuthNappi searchAuthNappi = new SearchPreAuthNappi();
        commands.put(searchAuthNappi.getName(), searchAuthNappi);
        AddAuthNappiToSession addNappiToSes = new AddAuthNappiToSession();
        commands.put(addNappiToSes.getName(), addNappiToSes);
        AllocateNappiToSession alloNappi = new AllocateNappiToSession();
        commands.put(alloNappi.getName(), alloNappi);
        CalculateNappiQuantityCommand calNappiQuan = new CalculateNappiQuantityCommand();
        commands.put(calNappiQuan.getName(), calNappiQuan);
        ModifyAuthNappiFromList modifyNappi = new ModifyAuthNappiFromList();
        commands.put(modifyNappi.getName(), modifyNappi);
        ForwardToAuthTariffSearchBasketCommand fwBas = new ForwardToAuthTariffSearchBasketCommand();
        commands.put(fwBas.getName(), fwBas);
        ForwardToAuthNappiBasketSearchCommand fwNapBas = new ForwardToAuthNappiBasketSearchCommand();
        commands.put(fwNapBas.getName(), fwNapBas);
        CalculateLOSFromDates calLosFromDates = new CalculateLOSFromDates();
        commands.put(calLosFromDates.getName(), calLosFromDates);
        GetCoPaymentFromCPTs getCoPayFromCpt = new GetCoPaymentFromCPTs();
        commands.put(getCoPayFromCpt.getName(), getCoPayFromCpt);
        GetTariffCoPayDetails getTCOP = new GetTariffCoPayDetails();
        commands.put(getTCOP.getName(), getTCOP);
        ValidateTraumaCounselling valTrauma = new ValidateTraumaCounselling();
        commands.put(valTrauma.getName(), valTrauma);
        ValidateHospiceCare valHospice = new ValidateHospiceCare();
        commands.put(valHospice.getName(), valHospice);
        SetInternationalTravelTariffToAuth setInterTravTariff = new SetInternationalTravelTariffToAuth();
        commands.put(setInterTravTariff.getName(), setInterTravTariff);
        RemoveAllocatedBenefit remAuthBen = new RemoveAllocatedBenefit();
        commands.put(remAuthBen.getName(), remAuthBen);
        AddLOCDowngrades addLocDown = new AddLOCDowngrades();
        commands.put(addLocDown.getName(), addLocDown);
        //new baskets
        ValidateConditionSpecific valConSpec = new ValidateConditionSpecific();
        commands.put(valConSpec.getName(), valConSpec);
        ValidatePreConditionSpec preConSpec = new ValidatePreConditionSpec();
        commands.put(preConSpec.getName(), preConSpec);
        ForwardToBasketICD ftbICD = new ForwardToBasketICD();
        commands.put(ftbICD.getName(), ftbICD);
        ReturnFromBasketICD rfbICD = new ReturnFromBasketICD();
        commands.put(rfbICD.getName(), rfbICD);
        RemoveCompleteBasketFromList remCB = new RemoveCompleteBasketFromList();
        commands.put(remCB.getName(), remCB);
        AddAuthBasketSpecTariffToSession addABST = new AddAuthBasketSpecTariffToSession();
        commands.put(addABST.getName(), addABST);
        AllocateBasketTariffSpecToSession alloABST = new AllocateBasketTariffSpecToSession();
        commands.put(alloABST.getName(), alloABST);
        SearchPreAuthBasketAllTariffs searchBAT = new SearchPreAuthBasketAllTariffs();
        commands.put(searchBAT.getName(), searchBAT);
        
        //Amukelani pre Auth command ForwardToAdvanceSearchAmu
        SearchAuthAmukelanim amuAuthCom = new SearchAuthAmukelanim();
        commands.put(amuAuthCom.getName(), amuAuthCom);
        
        ForwardToAdvanceSearchAmu amuFowCom = new ForwardToAdvanceSearchAmu();
        commands.put(amuFowCom.getName(), amuFowCom);
        
        AdvanceSearchAuthAmu advanceAmu = new AdvanceSearchAuthAmu();
        commands.put(advanceAmu.getName(), advanceAmu);

        //new auth notes
        ForwardToAuthSpecificNote forwSpecNote = new ForwardToAuthSpecificNote();
        commands.put(forwSpecNote.getName(), forwSpecNote);
        AddAuthNoteToList addNote = new AddAuthNoteToList();
        commands.put(addNote.getName(), addNote);
        SearchAuthHistoryNotes searchHisNotes = new SearchAuthHistoryNotes();
        commands.put(searchHisNotes.getName(), searchHisNotes);
        ForwardGenericAuthorizationPortal genericPortal = new ForwardGenericAuthorizationPortal();
        commands.put(genericPortal.getName(), genericPortal);
        ForwardGenericAuthorizationProviderPortal providerPortal = new ForwardGenericAuthorizationProviderPortal();
        commands.put(providerPortal.getName(), providerPortal);
        GetCoverDetailsByNumberPortal getCoverPortal = new GetCoverDetailsByNumberPortal();
        commands.put(getCoverPortal.getName(), getCoverPortal);
        /**
         * ********* customer care audit trail commands **********
         */
        ForwardToMemberAuditTrail custAudit = new ForwardToMemberAuditTrail();
        commands.put(custAudit.getName(), custAudit);
        CustomerCareAuditCommand ccaudit = new CustomerCareAuditCommand();
        commands.put(ccaudit.getName(), ccaudit);
        //update commands
        PreAuthUpdateCommand updatePreAuth = new PreAuthUpdateCommand();
        commands.put(updatePreAuth.getName(), updatePreAuth);
        SaveCallCenterPreAuthUpdate saveAUCall = new SaveCallCenterPreAuthUpdate();
        commands.put(saveAUCall.getName(), saveAUCall);
        SaveUpdatedPreAuthDetailsCommand saveAU = new SaveUpdatedPreAuthDetailsCommand();
        commands.put(saveAU.getName(), saveAU);
        ValidateGenericAuthUpdate validateAU = new ValidateGenericAuthUpdate();
        commands.put(validateAU.getName(), validateAU);
        ValidateHospitalDetails validateHosp = new ValidateHospitalDetails();
        commands.put(validateHosp.getName(), validateHosp);
        ValidateDentalDetails validateDental = new ValidateDentalDetails();
        commands.put(validateDental.getName(), validateDental);
        ValidateOpticalDetails validateOptical = new ValidateOpticalDetails();
        commands.put(validateOptical.getName(), validateOptical);
        ValidateOrthoDetails validateOrtho = new ValidateOrthoDetails();
        commands.put(validateOrtho.getName(), validateOrtho);
        GetHospitalStatusCommand getHospStatus = new GetHospitalStatusCommand();
        commands.put(getHospStatus.getName(), getHospStatus);

        //new commands
        ReloadPreauthGeneric reloadPreGeneric = new ReloadPreauthGeneric();
        commands.put(reloadPreGeneric.getName(), reloadPreGeneric);
        //tariffs
        ForwardToNewAuthTariffSearch forwNT = new ForwardToNewAuthTariffSearch();
        commands.put(forwNT.getName(), forwNT);
        SearchPreAuthNewTariff searchNTar = new SearchPreAuthNewTariff();
        commands.put(searchNTar.getName(), searchNTar);
        AllocateAuthNewTariffToSession alloNT = new AllocateAuthNewTariffToSession();
        commands.put(alloNT.getName(), alloNT);
        //benefits
        SetBenefitToAuth setBen = new SetBenefitToAuth();
        commands.put(setBen.getName(), setBen);
        ForwardToBenefitAllocation forwToABen = new ForwardToBenefitAllocation();
        commands.put(forwToABen.getName(), forwToABen);
        SetSelectedCoverDep setMemDep = new SetSelectedCoverDep();
        commands.put(setMemDep.getName(), setMemDep);
        GetPreAuthCoverByNumberAndDate preMemByDate = new GetPreAuthCoverByNumberAndDate();
        commands.put(preMemByDate.getName(), preMemByDate);

        //nappi's
        ForwardToAuthNewNappi forwANN = new ForwardToAuthNewNappi();
        commands.put(forwANN.getName(), forwANN);
        ForwardToNewAuthNappiSearch forwNNS = new ForwardToNewAuthNappiSearch();
        commands.put(forwNNS.getName(), forwNNS);
        GetAuthNappiByCodeCommand getANN = new GetAuthNappiByCodeCommand();
        commands.put(getANN.getName(), getANN);
        AllocateAuthNewNappiToSession alloANN = new AllocateAuthNewNappiToSession();
        commands.put(alloANN.getName(), alloANN);
        AddNewAuthNappiToSessionCommand addNewANToSess = new AddNewAuthNappiToSessionCommand();
        commands.put(addNewANToSess.getName(), addNewANToSess);
        ModifyAuthNewNappiCommand modANN = new ModifyAuthNewNappiCommand();
        commands.put(modANN.getName(), modANN);
        RemoveAuthNewNappiCommand remANN = new RemoveAuthNewNappiCommand();
        commands.put(remANN.getName(), remANN);
        SearchNewPreAuthNappi searchMANN = new SearchNewPreAuthNappi();
        commands.put(searchMANN.getName(), searchMANN);

        //confirmation
        GeneratePDFForPrint genPDFPrint = new GeneratePDFForPrint();
        commands.put(genPDFPrint.getName(), genPDFPrint);
        ForwardToPreAuthRejectedConfirmation forReAuthCon = new ForwardToPreAuthRejectedConfirmation();
        commands.put(forReAuthCon.getName(), forReAuthCon);
        GetRejectedAuthConfirmationDetails getReCon = new GetRejectedAuthConfirmationDetails();
        commands.put(getReCon.getName(), getReCon);

        //new validation commands
        PreAuthDateValidation preAuthDateValidation = new PreAuthDateValidation();
        commands.put(preAuthDateValidation.getName(), preAuthDateValidation);

        /**
         * *********** Practice Management ***********
         */
        SavePracticeDetails savePrac = new SavePracticeDetails();
        commands.put(savePrac.getName(), savePrac);
        ClearPracticeScreen clearPrac = new ClearPracticeScreen();
        commands.put(clearPrac.getName(), clearPrac);
        PracticeManagementTabContentCommand pracManagementTCC = new PracticeManagementTabContentCommand();
        commands.put(pracManagementTCC.getName(), pracManagementTCC);
        PracticeManagementNotesCommand practiceManagementNC = new PracticeManagementNotesCommand();
        commands.put(practiceManagementNC.getName(), practiceManagementNC);

        DisplayCoverDepJoinDate covJoinDisp = new DisplayCoverDepJoinDate();
        commands.put(covJoinDisp.getName(), covJoinDisp);

        //auth tariff cpt mapping
        GetAuthCPTMappingForTariff getCFTMap = new GetAuthCPTMappingForTariff();
        commands.put(getCFTMap.getName(), getCFTMap);

        //auth icd tariff mapping
        MapSelectedTCPTs mapTCPT = new MapSelectedTCPTs();
        commands.put(mapTCPT.getName(), mapTCPT);

        //PMB
        GetPMBByICDCommand getPMB = new GetPMBByICDCommand();
        commands.put(getPMB.getName(), getPMB);
        ForwardToAuthSpecificPMB forwardPMB = new ForwardToAuthSpecificPMB();
        commands.put(forwardPMB.getName(), forwardPMB);
        SetSelectedPMBCode setPMB = new SetSelectedPMBCode();
        commands.put(setPMB.getName(), setPMB);
        /**
         * **PAGE FORWARD COMMANDS***
         */
        ReturnCommand rc = new ReturnCommand();
        commands.put(rc.getName(), rc);
        RefreshCommand rfc = new RefreshCommand();
        commands.put(rfc.getName(), rfc);
        OverridePMB rPMB = new OverridePMB();
        commands.put(rPMB.getName(), rPMB);

        //TCPT
        ReturnAuthCommand returnAuthC = new ReturnAuthCommand();
        commands.put(returnAuthC.getName(), returnAuthC);

        /**
         * *********** Data Loader ***********
         */
        //LoadFileCommand lfc = new LoadFileCommand();
        //commands.put(lfc.getName(), lfc);
//        LoadDataFileCommand ldfc = new LoadDataFileCommand();
//        commands.put(ldfc.getName(), ldfc);
        AcceptStagingStatusCommand assc = new AcceptStagingStatusCommand();
        commands.put(assc.getName(), assc);
        CancelUploadCommand cancelUpload = new CancelUploadCommand();
        commands.put(cancelUpload.getName(), cancelUpload);

        /**
         * *********** Emails ***********
         */
        SendForgotPasswordCommand sndPass = new SendForgotPasswordCommand();
        commands.put(sndPass.getName(), sndPass);
        SubmitDocumentsCommand sendDocuments = new SubmitDocumentsCommand();
        commands.put(sendDocuments.getName(), sendDocuments);

        /**
         * *********** Claims ***********
         */
        ViewCoverClaimsCommand veiwCoverClaims = new ViewCoverClaimsCommand();
        commands.put(veiwCoverClaims.getName(), veiwCoverClaims);
        PolicyHolderDetailsCommand policyHolder = new PolicyHolderDetailsCommand();
        commands.put(policyHolder.getName(), policyHolder);
        //DebugSessionCommand debug = new DebugSessionCommand();
        //commands.put(debug.getName(), debug);
        ForwardToPreAuthCommand forwardToAuth = new ForwardToPreAuthCommand();
        commands.put(forwardToAuth.getName(), forwardToAuth);
        UpdateMemberAddressDetailsCommand updateMemberDetails = new UpdateMemberAddressDetailsCommand();
        commands.put(updateMemberDetails.getName(), updateMemberDetails);
        PreAuthDetailsCommand preAuthDetails = new PreAuthDetailsCommand();
        commands.put(preAuthDetails.getName(), preAuthDetails);
        ViewProviderDetailsCommand providerDetails = new ViewProviderDetailsCommand();
        commands.put(providerDetails.getName(), providerDetails);
        ViewNetworkIndicatorHistoryCommand providerNetworkHistDetails = new ViewNetworkIndicatorHistoryCommand();
        commands.put(providerNetworkHistDetails.getName(), providerNetworkHistDetails);
        ForwardToCallCenterPreAuthCommand forwardCallCenterAuth = new ForwardToCallCenterPreAuthCommand();
        commands.put(forwardCallCenterAuth.getName(), forwardCallCenterAuth);
        GetCallCenterPreAuthConfirmationDetails getCallCenterConfirmation = new GetCallCenterPreAuthConfirmationDetails();
        commands.put(getCallCenterConfirmation.getName(), getCallCenterConfirmation);
        ViewMemberBenefitsCommand viewBenefits = new ViewMemberBenefitsCommand();
        commands.put(viewBenefits.getName(), viewBenefits);
        SearchMemberClaimsCommand searchClaims = new SearchMemberClaimsCommand();
        commands.put(searchClaims.getName(), searchClaims);
        ViewMemberClaimsDetailsCommand viewMemberClaimDetails = new ViewMemberClaimsDetailsCommand();
        commands.put(viewMemberClaimDetails.getName(), viewMemberClaimDetails);
        ViewPracticeAuthorisationCommand practiceAuth = new ViewPracticeAuthorisationCommand();
        commands.put(practiceAuth.getName(), practiceAuth);
        ForwardToPracticeDetailsCommand practice = new ForwardToPracticeDetailsCommand();
        commands.put(practice.getName(), practice);
        ViewPracticeBankingDetailsCommand practiceBanking = new ViewPracticeBankingDetailsCommand();
        commands.put(practiceBanking.getName(), practiceBanking);
        ViewPracticePaymentDetailsCommand practicePayment = new ViewPracticePaymentDetailsCommand();
        commands.put(practicePayment.getName(), practicePayment);
        ViewProviderClaimsCommand providerClaims = new ViewProviderClaimsCommand();
        commands.put(providerClaims.getName(), providerClaims);
        SearchPracticeClaimsCommand searchPraticeClaims = new SearchPracticeClaimsCommand();
        commands.put(searchPraticeClaims.getName(), searchPraticeClaims);
        ForwardToSearchPracticeClaimsCommand forwardToSearch = new ForwardToSearchPracticeClaimsCommand();
        commands.put(forwardToSearch.getName(), forwardToSearch);
        ViewMemberAddressDetailsCommand memberAddress = new ViewMemberAddressDetailsCommand();
        commands.put(memberAddress.getName(), memberAddress);
        ForwardToMemberDetailsCommand forwardToMember = new ForwardToMemberDetailsCommand();
        commands.put(forwardToMember.getName(), forwardToMember);
        ForwardToMemberPreAuthorisationCommand forwardMemberPraAuth = new ForwardToMemberPreAuthorisationCommand();
        commands.put(forwardMemberPraAuth.getName(), forwardMemberPraAuth);
        GetMemberPreAuthConfirmationDetails getMemberPreAuth = new GetMemberPreAuthConfirmationDetails();
        commands.put(getMemberPreAuth.getName(), getMemberPreAuth);
        ViewMemberPreAuthorisationCommand viewMemberPreAuth = new ViewMemberPreAuthorisationCommand();
        commands.put(viewMemberPreAuth.getName(), viewMemberPreAuth);
        ViewMemberInfoCommand viewMemberInfo = new ViewMemberInfoCommand();
        commands.put(viewMemberInfo.getName(), viewMemberInfo);
        ViewMemberNominatedProviderDetailsCommand viewNominatedProv = new ViewMemberNominatedProviderDetailsCommand();
        commands.put(viewNominatedProv.getName(), viewNominatedProv);
        ForwardToMemberNominatedProviderCommand forwardNominatedProv = new ForwardToMemberNominatedProviderCommand();
        commands.put(forwardNominatedProv.getName(), forwardNominatedProv);
        SaveMemberNominatedProviderCommand saveNominatedProv = new SaveMemberNominatedProviderCommand();
        commands.put(saveNominatedProv.getName(), saveNominatedProv);
        PreAuthDocumentUploadCommand preAuthDocCommand = new PreAuthDocumentUploadCommand();
        commands.put(preAuthDocCommand.getName(), preAuthDocCommand);
        ForwardToPreAuthDocumentCommand forwardToPreAuthDocs = new ForwardToPreAuthDocumentCommand();
        commands.put(forwardToPreAuthDocs.getName(), forwardToPreAuthDocs);

        SaveCallCenterContactDetails saveCallCenterContactDetails = new SaveCallCenterContactDetails();
        commands.put(saveCallCenterContactDetails.getName(), saveCallCenterContactDetails);

        ViewMemberUnderwritingCommand viewMemberUndewriting = new ViewMemberUnderwritingCommand();
        commands.put(viewMemberUndewriting.getName(), viewMemberUndewriting);

        ViewMemberContributionsCommand viewMemberContributionsCommand = new ViewMemberContributionsCommand();
        commands.put(viewMemberContributionsCommand.getName(), viewMemberContributionsCommand);

        LoadMemberUnderwritingCommand loadMemberUnderwritingCommand = new LoadMemberUnderwritingCommand();
        commands.put(loadMemberUnderwritingCommand.getName(), loadMemberUnderwritingCommand);

        LoadMemberContributionsCommand loadMemberContributionsCommand = new LoadMemberContributionsCommand();
        commands.put(loadMemberContributionsCommand.getName(), loadMemberContributionsCommand);

        MemberPreAuthDetailsCommand memberPreAuth = new MemberPreAuthDetailsCommand();
        commands.put(memberPreAuth.getName(), memberPreAuth);
        ViewMemberPaymentDetailsCommand viewMemberPay = new ViewMemberPaymentDetailsCommand();
        commands.put(viewMemberPay.getName(), viewMemberPay);
        ViewMemberBankingDetailsCommand viewMemberBanking = new ViewMemberBankingDetailsCommand();
        commands.put(viewMemberBanking.getName(), viewMemberBanking);
        ForwardToMemberBenefitsCommand forwardBenefits = new ForwardToMemberBenefitsCommand();
        commands.put(forwardBenefits.getName(), forwardBenefits);
        ForwardToMemberClaimsCommand forwardMemberClaims = new ForwardToMemberClaimsCommand();
        commands.put(forwardMemberClaims.getName(), forwardMemberClaims);
        ViewPracticeAddressDetailsCommand viewPracticeAddress = new ViewPracticeAddressDetailsCommand();
        commands.put(viewPracticeAddress.getName(), viewPracticeAddress);
        ViewMemberClaimsBenefitsCommand claimsBenefits = new ViewMemberClaimsBenefitsCommand();
        commands.put(claimsBenefits.getName(), claimsBenefits);
        ForwardToMemberStatementsCommand forwardToMemberShip = new ForwardToMemberStatementsCommand();
        commands.put(forwardToMemberShip.getName(), forwardToMemberShip);
        SubmitClaimCallCenterStatementCommand submitCallCenterClaims = new SubmitClaimCallCenterStatementCommand();
        commands.put(submitCallCenterClaims.getName(), submitCallCenterClaims);
        ForwardToProviderStatementCommand providerStatements = new ForwardToProviderStatementCommand();
        commands.put(providerStatements.getName(), providerStatements);
        ReturnMemberCCPreAuth returnCCAuth = new ReturnMemberCCPreAuth();
        commands.put(returnCCAuth.getName(), returnCCAuth);
        GetMemberClaimDetailsCommand getMemClaimDet = new GetMemberClaimDetailsCommand();
        commands.put(getMemClaimDet.getName(), getMemClaimDet);
        SubmitPracticeClaimCallCenterStatementCommand submitPracticeStatements = new SubmitPracticeClaimCallCenterStatementCommand();
        commands.put(submitPracticeStatements.getName(), submitPracticeStatements);
        ForwardToMemberClaimLineClaimLine viewMemCLCL = new ForwardToMemberClaimLineClaimLine();
        commands.put(viewMemCLCL.getName(), viewMemCLCL);
        ReturnMemberClaimDetails returnMemClaims = new ReturnMemberClaimDetails();
        commands.put(returnMemClaims.getName(), returnMemClaims);
        ViewPracticeClaimLineDetails viewPracCL = new ViewPracticeClaimLineDetails();
        commands.put(viewPracCL.getName(), viewPracCL);
        ViewPracticeClaimLineClaimLine viewPracCLCL = new ViewPracticeClaimLineClaimLine();
        commands.put(viewPracCLCL.getName(), viewPracCLCL);
        ReturnPracticeClaimDetails returnPracClaims = new ReturnPracticeClaimDetails();
        commands.put(returnPracClaims.getName(), returnPracClaims);
        ReturnMemberPayRunStatement returnPayStmt = new ReturnMemberPayRunStatement();
        commands.put(returnPayStmt.getName(), returnPayStmt);
        ViewPayRunStatements viewPRS = new ViewPayRunStatements();
        commands.put(viewPRS.getName(), viewPRS);
        GetCcEntityDetailsByNumber ccEntityVal = new GetCcEntityDetailsByNumber();
        commands.put(ccEntityVal.getName(), ccEntityVal);
        ReloadCoverHistoryByDate reloadCovHist = new ReloadCoverHistoryByDate();
        commands.put(reloadCovHist.getName(), reloadCovHist);
        ForwardPNMaintenance forwardPNMCommand = new ForwardPNMaintenance();
        commands.put(forwardPNMCommand.getName(), forwardPNMCommand);
        FindClearingPracticeDetailsByCode findCP = new FindClearingPracticeDetailsByCode();
        commands.put(findCP.getName(), findCP);
        CallDocumentViewCommand callDocumentViewCommand = new CallDocumentViewCommand();
        commands.put(callDocumentViewCommand.getName(), callDocumentViewCommand);
        SaveMemberCoverDetailsCommand saveMemberCoverDetailsCommand = new SaveMemberCoverDetailsCommand();
        commands.put(saveMemberCoverDetailsCommand.getName(), saveMemberCoverDetailsCommand);
        BrokerMemberSearchCommand brokerMemberSearchCommand = new BrokerMemberSearchCommand();
        commands.put(brokerMemberSearchCommand.getName(), brokerMemberSearchCommand);
        ConsultantMemberSearchCommand consultantMemberSearchCommand = new ConsultantMemberSearchCommand();
        commands.put(consultantMemberSearchCommand.getName(), consultantMemberSearchCommand);
        GroupMemberSearchCommand groupMemberSearchCommand = new GroupMemberSearchCommand();
        commands.put(groupMemberSearchCommand.getName(), groupMemberSearchCommand);
        SaveMemberAdditionalInfoCommand saveMemberAdditionalInfoCommand = new SaveMemberAdditionalInfoCommand();
        commands.put(saveMemberAdditionalInfoCommand.getName(), saveMemberAdditionalInfoCommand);
        ForwardToMemberNotes forwardToNote = new ForwardToMemberNotes();
        commands.put(forwardToNote.getName(), forwardToNote);
        MemberCertificateGenerationCC memCertCC = new MemberCertificateGenerationCC();
        commands.put(memCertCC.getName(), memCertCC);
        ForwardToMemberCommunicationCommand fwdMemberComm = new ForwardToMemberCommunicationCommand();
        commands.put(fwdMemberComm.getName(), fwdMemberComm);

        //new commands
        addEmployerCommands();
        addMemberApplicationCommands();
        addContributionCommands();
        addBrokerFirmApplicationCommands();
        addBrokerApplicationCommands();
        brokerConsultantCommands();
        ClaimCommands();
        automationCommands();

        //new membership entity command
        MemberSearchByCriteria msc = new MemberSearchByCriteria();
        commands.put(msc.getName(), msc);
        ViewDependantsForCover viewCovDep = new ViewDependantsForCover();
        commands.put(viewCovDep.getName(), viewCovDep);
        LoadCoverMenuTabs loadCMenu = new LoadCoverMenuTabs();
        commands.put(loadCMenu.getName(), loadCMenu);
        LoadPracticeMenuTabs loadPracMenu = new LoadPracticeMenuTabs();
        commands.put(loadPracMenu.getName(), loadPracMenu);
        MemberMaintenanceTabContentCommand mmTab = new MemberMaintenanceTabContentCommand();
        commands.put(mmTab.getName(), mmTab);
        //save commands
        SaveMemberAddressDetailsCommand saveMemAddress = new SaveMemberAddressDetailsCommand();
        commands.put(saveMemAddress.getName(), saveMemAddress);
        SaveMemberBankingDetailsCommand saveMemBanking = new SaveMemberBankingDetailsCommand();
        commands.put(saveMemBanking.getName(), saveMemBanking);
        SaveMemberContactDetailsCommand saveMemContact = new SaveMemberContactDetailsCommand();
        commands.put(saveMemContact.getName(), saveMemContact);
        SaveMemberPersonalDetailsCommand saveMemPersonal = new SaveMemberPersonalDetailsCommand();
        commands.put(saveMemPersonal.getName(), saveMemPersonal);
        SaveNominatedProviderInfoCommand saveNomProv = new SaveNominatedProviderInfoCommand();
        commands.put(saveNomProv.getName(), saveNomProv);
        SaveMemberContactPrefDetailsCommand saveMemConPref = new SaveMemberContactPrefDetailsCommand();
        commands.put(saveMemConPref.getName(), saveMemConPref);
        SaveCoverDetailsCommand saveCovDetailsCmd = new SaveCoverDetailsCommand();
        commands.put(saveCovDetailsCmd.getName(), saveCovDetailsCmd);
        //practice Management save commands
        SavePracticeAddressDetailsCommand savePracticeAddress = new SavePracticeAddressDetailsCommand();
        commands.put(savePracticeAddress.getName(), savePracticeAddress);
        SavePracticeBankingDetailsCommand savePracticeBanking = new SavePracticeBankingDetailsCommand();
        commands.put(savePracticeBanking.getName(), savePracticeBanking);
        SavePracticeContactDetailsCommand savePracticeContact = new SavePracticeContactDetailsCommand();
        commands.put(savePracticeContact.getName(), savePracticeContact);

        //tag commands
        GetBankBranchByBankIdCommand getBranchByBankId = new GetBankBranchByBankIdCommand();
        commands.put(getBranchByBankId.getName(), getBranchByBankId);
        //audit command
        MemberCoverAuditCommand memCovAudit = new MemberCoverAuditCommand();
        commands.put(memCovAudit.getName(), memCovAudit);
        //practice audit command
        PracticeAuditTrailCommand practiceAuditTrail = new PracticeAuditTrailCommand();
        commands.put(practiceAuditTrail.getName(), practiceAuditTrail);
        // Member Application Audit command
        MemberAppAuditCommand memberAppAuditCommand = new MemberAppAuditCommand();
        commands.put(memberAppAuditCommand.getName(), memberAppAuditCommand);
        //Document Generation
        DocumentGenerationCommand docGeneration = new DocumentGenerationCommand();
        commands.put(docGeneration.getName(), docGeneration);
        DocumentIndexPreviewCommand previewCommand = new DocumentIndexPreviewCommand();
        commands.put(previewCommand.getName(), previewCommand);
        DocumentIndexFileListCommand fileListCommand = new DocumentIndexFileListCommand();
        commands.put(fileListCommand.getName(), fileListCommand);
        DocumentIndexCommand documentIndexCommand = new DocumentIndexCommand();
        commands.put(documentIndexCommand.getName(), documentIndexCommand);
        DocumentIndexMergeCommand documentIndexMergeCommand = new DocumentIndexMergeCommand();
        commands.put(documentIndexMergeCommand.getName(), documentIndexMergeCommand);
        DocumentIndexSplitCommand documentIndexSplitCommand = new DocumentIndexSplitCommand();
        commands.put(documentIndexSplitCommand.getName(), documentIndexSplitCommand);
        DocumentIndexCopyCommand documentIndexCopyCommand = new DocumentIndexCopyCommand();
        commands.put(documentIndexCopyCommand.getName(), documentIndexCopyCommand);
        DocumentIndexDeleteCommand documentIndexDeleteCommand = new DocumentIndexDeleteCommand();
        commands.put(documentIndexDeleteCommand.getName(), documentIndexDeleteCommand);
        DocumentIndexBackCommand documentIndexBackCommand = new DocumentIndexBackCommand();
        commands.put(documentIndexBackCommand.getName(), documentIndexBackCommand);
        DocumentIndexDoSplitCommand documentIndexDoSplitCommand = new DocumentIndexDoSplitCommand();
        commands.put(documentIndexDoSplitCommand.getName(), documentIndexDoSplitCommand);
        DocumentIndexDoMergeCommand documentIndexDoMergeCommand = new DocumentIndexDoMergeCommand();
        commands.put(documentIndexDoMergeCommand.getName(), documentIndexDoMergeCommand);
        MemberDocumentSearchCommand memberDocumentSearchCommand = new MemberDocumentSearchCommand();
        commands.put(memberDocumentSearchCommand.getName(), memberDocumentSearchCommand);
        MemberDocumentViewCommand memberDocumentViewCommand = new MemberDocumentViewCommand();
        commands.put(memberDocumentViewCommand.getName(), memberDocumentViewCommand);
        MemberDocumentDisplayViewCommand memberDocumentDisplayViewCommand = new MemberDocumentDisplayViewCommand();
        commands.put(memberDocumentDisplayViewCommand.getName(), memberDocumentDisplayViewCommand);
        MemberDocumentUploadCommand mduc = new MemberDocumentUploadCommand();
        commands.put(mduc.getName(), mduc);

        /**
         * *********** Commented ***********
         */
        DocumentReIndexFileCommand documentReIndexFileCommand = new DocumentReIndexFileCommand();
        commands.put(documentReIndexFileCommand.getName(), documentReIndexFileCommand);

        PrintBulkCommand printBulkCommand = new PrintBulkCommand();
        commands.put(printBulkCommand.getName(), printBulkCommand);
        PrintPoolPauseCommand printPoolPauseCommand = new PrintPoolPauseCommand();
        commands.put(printPoolPauseCommand.getName(), printPoolPauseCommand);
        PrintPoolCommand printPoolCommand = new PrintPoolCommand();
        commands.put(printPoolCommand.getName(), printPoolCommand);
        PrintPoolFilterCommand printPoolFilterCommand = new PrintPoolFilterCommand();
        commands.put(printPoolFilterCommand.getName(), printPoolFilterCommand);
        BulkPrintPoolFilterCommand bulkPrintPoolFilterCommand = new BulkPrintPoolFilterCommand();
        commands.put(bulkPrintPoolFilterCommand.getName(), bulkPrintPoolFilterCommand);
        BulkPrintPoolCommand bulkPrintPoolCommand = new BulkPrintPoolCommand();
        commands.put(bulkPrintPoolCommand.getName(), bulkPrintPoolCommand);
        PrintPoolDeleteCommand printPoolDeleteCommand = new PrintPoolDeleteCommand();
        commands.put(printPoolDeleteCommand.getName(), printPoolDeleteCommand);
        BulkPrintPoolDeleteCommand bulkPrintPoolDeleteCommand = new BulkPrintPoolDeleteCommand();
        commands.put(bulkPrintPoolDeleteCommand.getName(), bulkPrintPoolDeleteCommand);
        MemberDocumentGenerationCommand documentGenerationCommand = new MemberDocumentGenerationCommand();
        commands.put(documentGenerationCommand.getName(), documentGenerationCommand);
        MemberAppDocumentGenerationCommand appDocumentGenerationCommand = new MemberAppDocumentGenerationCommand();
        commands.put(appDocumentGenerationCommand.getName(), appDocumentGenerationCommand);
        AddToPrintPoolCommand addToPrintPoolCommand = new AddToPrintPoolCommand();
        commands.put(addToPrintPoolCommand.getName(), addToPrintPoolCommand);
        MemberCommunicationLogCommand memberCommunicationLogCommand = new MemberCommunicationLogCommand();
        commands.put(memberCommunicationLogCommand.getName(), memberCommunicationLogCommand);
        LoadContribTranCommand loadContribTranCommand = new LoadContribTranCommand();
        commands.put(loadContribTranCommand.getName(), loadContribTranCommand);
        LoadTaxCertContribTranCommand loadTaxCertContribTranCommand = new LoadTaxCertContribTranCommand();
        commands.put(loadTaxCertContribTranCommand.getName(), loadTaxCertContribTranCommand);
        LoadTaxCertClaimsTranCommand loadtaxCertClaims = new LoadTaxCertClaimsTranCommand();
        commands.put(loadtaxCertClaims.getName(), loadtaxCertClaims);
        MemberUnderwritingCommand memberUnderwritingCommand = new MemberUnderwritingCommand();
        commands.put(memberUnderwritingCommand.getName(), memberUnderwritingCommand);
        DocumentFilterCommand documentSortingCommand = new DocumentFilterCommand();
        commands.put(documentSortingCommand.getName(), documentSortingCommand);
        CallCenterDocumentListCommand callCenterDocumentListCommand = new CallCenterDocumentListCommand();
        commands.put(callCenterDocumentListCommand.getName(), callCenterDocumentListCommand);
        LoadDocumentDropDownListCommand documentDropDownListCommand = new LoadDocumentDropDownListCommand();
        commands.put(documentDropDownListCommand.getName(), documentDropDownListCommand);
        MemberAppDocumentFilterCommand appDocumentFilterCommand = new MemberAppDocumentFilterCommand();
        commands.put(appDocumentFilterCommand.getName(), appDocumentFilterCommand);
        ClaimNumberValidationCommand claimNumberValidationCommand = new ClaimNumberValidationCommand();
        commands.put(claimNumberValidationCommand.getName(), claimNumberValidationCommand);
        RefreshPdcBiometricsCommand refreshPdcBiometricsCommand = new RefreshPdcBiometricsCommand();
        commands.put(refreshPdcBiometricsCommand.getName(), refreshPdcBiometricsCommand);
        RecalculateMemberBenefits recalculateMemberBenefits = new RecalculateMemberBenefits();
        commands.put(recalculateMemberBenefits.getName(), recalculateMemberBenefits);
        //Unpaids
        SubmitUnpaidsDocumentCommand submitUnpaidsDocumentCommand = new SubmitUnpaidsDocumentCommand();
        commands.put(submitUnpaidsDocumentCommand.getName(), submitUnpaidsDocumentCommand);
        SaveUnpaidsCommand saveUnpaidsCommand = new SaveUnpaidsCommand();
        commands.put(saveUnpaidsCommand.getName(), saveUnpaidsCommand);

        ForwardToAuthNappiProduct forwardToAuthNappiProduct = new ForwardToAuthNappiProduct();
        commands.put(forwardToAuthNappiProduct.getName(), forwardToAuthNappiProduct);
        ForwardToSearchProduct forwardToSearchProduct = new ForwardToSearchProduct();
        commands.put(forwardToSearchProduct.getName(), forwardToSearchProduct);
        AuthGetNappiList authGetNappiList = new AuthGetNappiList();
        commands.put(authGetNappiList.getName(), authGetNappiList);
        ForwardToICD10Search forwardToICD10Search = new ForwardToICD10Search();
        commands.put(forwardToICD10Search.getName(), forwardToICD10Search);
        SearchPreAthICD10Command searchPreAthICD10Command = new SearchPreAthICD10Command();
        commands.put(searchPreAthICD10Command.getName(), searchPreAthICD10Command);
        AllocatePreAuthICD10ToSessionCommand allocatePreAuthICD10ToSessionCommand = new AllocatePreAuthICD10ToSessionCommand();
        commands.put(allocatePreAuthICD10ToSessionCommand.getName(), allocatePreAuthICD10ToSessionCommand);
        SaveProductCommand saveProductCommand = new SaveProductCommand();
        commands.put(saveProductCommand.getName(), saveProductCommand);
        ForwardToAuthHospDetailsCommand forwardToAuthHospDetailsCommand = new ForwardToAuthHospDetailsCommand();
        commands.put(forwardToAuthHospDetailsCommand.getName(), forwardToAuthHospDetailsCommand);
        RemoveNappiProdFromListCommand removeNappiProdFromListCommand = new RemoveNappiProdFromListCommand();
        commands.put(removeNappiProdFromListCommand.getName(), removeNappiProdFromListCommand);
        ModNappiProdListCommand modNappiProdListCommand = new ModNappiProdListCommand();
        commands.put(modNappiProdListCommand.getName(), modNappiProdListCommand);
        GetNappiProductFromCode getNappiProductFromCode = new GetNappiProductFromCode();
        commands.put(getNappiProductFromCode.getName(), getNappiProductFromCode);

        /**
         * *********** Claims ***********
         */
        ViewClaimLineDetails vcld = new ViewClaimLineDetails();
        commands.put(vcld.getName(), vcld);
        ReturnClaimDetails retCD = new ReturnClaimDetails();
        commands.put(retCD.getName(), retCD);
        ClaimLineClaimLineCommand clcl = new ClaimLineClaimLineCommand();
        commands.put(clcl.getName(), clcl);
        ForwardToClaimLineClaimLine forClcl = new ForwardToClaimLineClaimLine();
        commands.put(forClcl.getName(), forClcl);
        ReverseClaim rclaim = new ReverseClaim();
        commands.put(rclaim.getName(), rclaim);
        ReverseAndCorrectClaim rcclaim = new ReverseAndCorrectClaim();
        commands.put(rcclaim.getName(), rcclaim);
        ReverseClaimLine rcl = new ReverseClaimLine();
        commands.put(rcl.getName(), rcl);

    }

    private void addEmployerCommands() {
        EmployerTabContentsCommand employerTabContentsCommand = new EmployerTabContentsCommand();
        commands.put(employerTabContentsCommand.getName(), employerTabContentsCommand);
        SaveEmployerDetailsCommand saveEmployerDetailsCommand = new SaveEmployerDetailsCommand();
        commands.put(saveEmployerDetailsCommand.getName(), saveEmployerDetailsCommand);
        SaveEmployerBillingCommand saveEmployerBillingCommand = new SaveEmployerBillingCommand();
        commands.put(saveEmployerBillingCommand.getName(), saveEmployerBillingCommand);
        SaveEmployerCommunicationCommand saveEmployerCommunicationCommand = new SaveEmployerCommunicationCommand();
        commands.put(saveEmployerCommunicationCommand.getName(), saveEmployerCommunicationCommand);
        SaveEmployerEligibilityCommand saveEmployerEligibilityCommand = new SaveEmployerEligibilityCommand();
        commands.put(saveEmployerEligibilityCommand.getName(), saveEmployerEligibilityCommand);
        SaveEmployerIntermediaryCommand saveEmployerIntermediaryCommand = new SaveEmployerIntermediaryCommand();
        commands.put(saveEmployerIntermediaryCommand.getName(), saveEmployerIntermediaryCommand);
        SaveEmployerPaymentCommand saveEmployerPaymentCommand = new SaveEmployerPaymentCommand();
        commands.put(saveEmployerPaymentCommand.getName(), saveEmployerPaymentCommand);
        SaveEmployerSchemeCommand saveEmployerSchemeCommand = new SaveEmployerSchemeCommand();
        commands.put(saveEmployerSchemeCommand.getName(), saveEmployerSchemeCommand);
        SaveEmployerUnderwritingCommand saveEmployerUnderwritingCommand = new SaveEmployerUnderwritingCommand();
        commands.put(saveEmployerUnderwritingCommand.getName(), saveEmployerUnderwritingCommand);
        EmployerSearchCommand employerSearchCommand = new EmployerSearchCommand();
        commands.put(employerSearchCommand.getName(), employerSearchCommand);
        EmployerViewCommand employerViewCommand = new EmployerViewCommand();
        commands.put(employerViewCommand.getName(), employerViewCommand);
        EmployerAuditCommand employerAuditCommand = new EmployerAuditCommand();
        commands.put(employerAuditCommand.getName(), employerAuditCommand);
        EmployerBranchCommand employerBranchCommand = new EmployerBranchCommand();
        commands.put(employerBranchCommand.getName(), employerBranchCommand);
        EmployerListCommand employerListCommand = new EmployerListCommand();
        commands.put(employerListCommand.getName(), employerListCommand);
        AdministratorSearchCommand administratorSearchCommand = new AdministratorSearchCommand();
        commands.put(administratorSearchCommand.getName(), administratorSearchCommand);
        RunAppCommand runAppCommand = new RunAppCommand();
        commands.put(runAppCommand.getName(), runAppCommand);
        EmployerTransactionCommand employerTransactionCommand = new EmployerTransactionCommand();
        commands.put(employerTransactionCommand.getName(), employerTransactionCommand);
        EmployerTerminationCommand employerTerminationCommand = new EmployerTerminationCommand();
        commands.put(employerTerminationCommand.getName(), employerTerminationCommand);
        EmployerExportCommand employExport = new EmployerExportCommand();
        commands.put(employExport.getName(), employExport);

    }

    private void addMemberApplicationCommands() {

        MemberAppTabContentsCommand memberAppTabContentsCommand = new MemberAppTabContentsCommand();
        commands.put(memberAppTabContentsCommand.getName(), memberAppTabContentsCommand);
        SaveMemberApplicationCommand saveMemberApplicationCommand = new SaveMemberApplicationCommand();
        commands.put(saveMemberApplicationCommand.getName(), saveMemberApplicationCommand);
        MemberApplicationSearchCommand memberApplicationSearchCommand = new MemberApplicationSearchCommand();
        commands.put(memberApplicationSearchCommand.getName(), memberApplicationSearchCommand);
        MemberApplicationViewCommand memberApplicationViewCommand = new MemberApplicationViewCommand();
        commands.put(memberApplicationViewCommand.getName(), memberApplicationViewCommand);
        MemberApplicationSpecificQuestionsCommand memberApplicationSpecificQuestionsCommand = new MemberApplicationSpecificQuestionsCommand();
        commands.put(memberApplicationSpecificQuestionsCommand.getName(), memberApplicationSpecificQuestionsCommand);
        ICD10SearchCommand disorderSearchCommand = new ICD10SearchCommand();
        commands.put(disorderSearchCommand.getName(), disorderSearchCommand);
        BrokerFirmViewCommand brokerFirmViewCommand = new BrokerFirmViewCommand();
        commands.put(brokerFirmViewCommand.getName(), brokerFirmViewCommand);
        MemberApplicationActivateCommand memberApplicationActivateCommand = new MemberApplicationActivateCommand();
        commands.put(memberApplicationActivateCommand.getName(), memberApplicationActivateCommand);
        NewDepAppSearchCommand newDepAppSearchCommand = new NewDepAppSearchCommand();
        commands.put(newDepAppSearchCommand.getName(), newDepAppSearchCommand);
        MemberBrokerSearchCommand memberBrokerSearchCommand = new MemberBrokerSearchCommand();
        commands.put(memberBrokerSearchCommand.getName(), memberBrokerSearchCommand);
        MemberNotesCommand memberNotesCommand = new MemberNotesCommand();
        commands.put(memberNotesCommand.getName(), memberNotesCommand);
        MemberConsultantSearchCommand memberConsultantSearchCommand = new MemberConsultantSearchCommand();
        commands.put(memberConsultantSearchCommand.getName(), memberConsultantSearchCommand);
        MemberEmployerSearchCommand memberEmployerSearchCommand = new MemberEmployerSearchCommand();
        commands.put(memberEmployerSearchCommand.getName(), memberEmployerSearchCommand);
        MemberAppOutstandingDocSaveCommand memOutstandingDoc = new MemberAppOutstandingDocSaveCommand();
        commands.put(memOutstandingDoc.getName(), memOutstandingDoc);
        MemberOutStandingViewCommand memOutStandingView = new MemberOutStandingViewCommand();
        commands.put(memOutStandingView.getName(), memOutStandingView);
        MemberOutstandingUpdateCommand memOutstandingViewUpdate = new MemberOutstandingUpdateCommand();
        commands.put(memOutstandingViewUpdate.getName(), memOutstandingViewUpdate);
    }

    private void addBrokerFirmApplicationCommands() {

        BrokerFirmAppTabContentsCommand brokerFirmrAppTabContentsCommand = new BrokerFirmAppTabContentsCommand();
        commands.put(brokerFirmrAppTabContentsCommand.getName(), brokerFirmrAppTabContentsCommand);
        SaveBrokerFirmApplicationCommand saveBrokerFirmCommand = new SaveBrokerFirmApplicationCommand();
        commands.put(saveBrokerFirmCommand.getName(), saveBrokerFirmCommand);
        BrokerFirmSearchCommand brokerFirmSearchCommand = new BrokerFirmSearchCommand();
        commands.put(brokerFirmSearchCommand.getName(), brokerFirmSearchCommand);
        SaveBrokerFirmContactDetailsCommand SaveBrokerFirmContactDetailsCommand = new SaveBrokerFirmContactDetailsCommand();
        commands.put(SaveBrokerFirmContactDetailsCommand.getName(), SaveBrokerFirmContactDetailsCommand);
        BrokerFirmNotesCommand brokerFirmNotesCommand = new BrokerFirmNotesCommand();
        commands.put(brokerFirmNotesCommand.getName(), brokerFirmNotesCommand);
        BrokerFirmCommissionCommand brokerFirmCommissionCommand = new BrokerFirmCommissionCommand();
        commands.put(brokerFirmCommissionCommand.getName(), brokerFirmCommissionCommand);
    }

    private void addBrokerApplicationCommands() {

        BrokerAppTabContentsCommand brokerAppTabContentsCommand = new BrokerAppTabContentsCommand();
        commands.put(brokerAppTabContentsCommand.getName(), brokerAppTabContentsCommand);

        MemberDependantAppCommand memberDependantAppCommand = new MemberDependantAppCommand();
        commands.put(memberDependantAppCommand.getName(), memberDependantAppCommand);
        MemberApplicationGeneralQuestionsCommand memberApplicationGeneralQuestionsCommand = new MemberApplicationGeneralQuestionsCommand();
        commands.put(memberApplicationGeneralQuestionsCommand.getName(), memberApplicationGeneralQuestionsCommand);

        MemberApplicationHospitalCommand memberApplicationHospitalCommand = new MemberApplicationHospitalCommand();
        commands.put(memberApplicationHospitalCommand.getName(), memberApplicationHospitalCommand);
        MemberApplicationChronicCommand memberApplicationChronicCommand = new MemberApplicationChronicCommand();
        commands.put(memberApplicationChronicCommand.getName(), memberApplicationChronicCommand);
        MemberAppNotesCommand memberAppNotesCommand = new MemberAppNotesCommand();
        commands.put(memberAppNotesCommand.getName(), memberAppNotesCommand);

        SaveBrokerApplicationCommand saveBrokerApplicationCommand = new SaveBrokerApplicationCommand();
        commands.put(saveBrokerApplicationCommand.getName(), saveBrokerApplicationCommand);
        BrokerSearchCommand brokerSearchCommand = new BrokerSearchCommand();
        commands.put(brokerSearchCommand.getName(), brokerSearchCommand);
        BrokerViewCommand brokerViewCommand = new BrokerViewCommand();
        commands.put(brokerViewCommand.getName(), brokerViewCommand);
        SaveBrokerContactDetailsCommand saveBrokerContactDetailsCommand = new SaveBrokerContactDetailsCommand();
        commands.put(saveBrokerContactDetailsCommand.getName(), saveBrokerContactDetailsCommand);
        SaveBrokerFirmBankingDetails saveBrokerFirmBankingDetails = new SaveBrokerFirmBankingDetails();
        commands.put(saveBrokerFirmBankingDetails.getName(), saveBrokerFirmBankingDetails);
        FirmAuditCommand firmAuditCommand = new FirmAuditCommand();
        commands.put(firmAuditCommand.getName(), firmAuditCommand);
        AddBrokerFirmAccreditationCommand addBrokerFirmAccreditationCommand = new AddBrokerFirmAccreditationCommand();
        commands.put(addBrokerFirmAccreditationCommand.getName(), addBrokerFirmAccreditationCommand);

        ViewBrokerAuditTrailCommand viewBrokerAuditTrailCommand = new ViewBrokerAuditTrailCommand();
        commands.put(viewBrokerAuditTrailCommand.getName(), viewBrokerAuditTrailCommand);
        AddBrokerAccreditationCommand addBrokerAccreditationCommand = new AddBrokerAccreditationCommand();
        commands.put(addBrokerAccreditationCommand.getName(), addBrokerAccreditationCommand);
        AddBrokerHistoryCommand addBrokerHistoryCommand = new AddBrokerHistoryCommand();
        commands.put(addBrokerHistoryCommand.getName(), addBrokerHistoryCommand);

        MemberApplicationUnderwritingCommand memberApplicationUnderwritingCommand = new MemberApplicationUnderwritingCommand();
        commands.put(memberApplicationUnderwritingCommand.getName(), memberApplicationUnderwritingCommand);

        BrokerFirmHistorySearchCommand brokerFirmHistorySearchCommand = new BrokerFirmHistorySearchCommand();
        commands.put(brokerFirmHistorySearchCommand.getName(), brokerFirmHistorySearchCommand);

        BrokerListCommand brokerListCommand = new BrokerListCommand();
        commands.put(brokerListCommand.getName(), brokerListCommand);

        BrokerNotesCommand brokerNotesCommand = new BrokerNotesCommand();
        commands.put(brokerNotesCommand.getName(), brokerNotesCommand);

    }

    private void addContributionCommands() {
        GenerateContributionCommand generateContribtionCommand = new GenerateContributionCommand();
        commands.put(generateContribtionCommand.getName(), generateContribtionCommand);
        ContributionBankStatementCommand contributionBankStatementCommand = new ContributionBankStatementCommand();
        commands.put(contributionBankStatementCommand.getName(), contributionBankStatementCommand);
        ContributionReciptsSearchCommand contributionReciptsSearchCommand = new ContributionReciptsSearchCommand();
        commands.put(contributionReciptsSearchCommand.getName(), contributionReciptsSearchCommand);
        PaymentAllocationCommand paymentAllocationCommand = new PaymentAllocationCommand();
        commands.put(paymentAllocationCommand.getName(), paymentAllocationCommand);
        GroupBillingStatementCommand gbsc = new GroupBillingStatementCommand();
        commands.put(gbsc.getName(), gbsc);
        CommissionCommand commCommand = new CommissionCommand();
        commands.put(commCommand.getName(), commCommand);
        TransferReceiptsCommand transferReceiptsCommand = new TransferReceiptsCommand();
        commands.put(transferReceiptsCommand.getName(), transferReceiptsCommand);
        TransferReversePayAllocCommand transferReversePayAllocCommand = new TransferReversePayAllocCommand();
        commands.put(transferReversePayAllocCommand.getName(), transferReversePayAllocCommand);
        RefundContribCommand refundContribCommand = new RefundContribCommand();
        commands.put(refundContribCommand.getName(), refundContribCommand);
        ContributionBulkAllocSearchCommand contributionBulkAllocSearchCommand = new ContributionBulkAllocSearchCommand();
        commands.put(contributionBulkAllocSearchCommand.getName(), contributionBulkAllocSearchCommand);
        BulkAllocFileUploadCommand bulkAllocFileUploadCommand = new BulkAllocFileUploadCommand();
        commands.put(bulkAllocFileUploadCommand.getName(), bulkAllocFileUploadCommand);
        QLinkErrorsCommand qLinkErrorsCommand = new QLinkErrorsCommand();
        commands.put(qLinkErrorsCommand.getName(), qLinkErrorsCommand);
        TransferBalFromResignedMemberCommand transferBalFromResignedMemberCommand = new TransferBalFromResignedMemberCommand();
        commands.put(transferBalFromResignedMemberCommand.getName(), transferBalFromResignedMemberCommand);
        RefundUnallocatedReceiptsCommand refundUnallocatedReceiptsCommand = new RefundUnallocatedReceiptsCommand();
        commands.put(refundUnallocatedReceiptsCommand.getName(), refundUnallocatedReceiptsCommand);
        TransferAmountCommand transferAmountCommand = new TransferAmountCommand();
        commands.put(transferAmountCommand.getName(), transferAmountCommand);

    }

    private void brokerConsultantCommands() {
        BrokerConsultantSearchCommand brokerConsultantSearchCommand = new BrokerConsultantSearchCommand();
        commands.put(brokerConsultantSearchCommand.getName(), brokerConsultantSearchCommand);
        SaveConsultantApplicationCommand saveConsultantApplicationCommand = new SaveConsultantApplicationCommand();
        commands.put(saveConsultantApplicationCommand.getName(), saveConsultantApplicationCommand);
        ConsultantAppTabContentsCommand consultantAppTabContentsCommand = new ConsultantAppTabContentsCommand();
        commands.put(consultantAppTabContentsCommand.getName(), consultantAppTabContentsCommand);
        ConsultantSearchCommand consultantSearchCommand = new ConsultantSearchCommand();
        commands.put(consultantSearchCommand.getName(), consultantSearchCommand);
        ConsultantViewCommand consultantViewCommand = new ConsultantViewCommand();
        commands.put(consultantViewCommand.getName(), consultantViewCommand);
        AddConsultantAccreditationCommand addConsultantAccreditationCommand = new AddConsultantAccreditationCommand();
        commands.put(addConsultantAccreditationCommand.getName(), addConsultantAccreditationCommand);
        SaveConsultantContactDetailsCommand saveConsultantContactDetailsCommand = new SaveConsultantContactDetailsCommand();
        commands.put(saveConsultantContactDetailsCommand.getName(), saveConsultantContactDetailsCommand);
        ViewConsultantAuditTrailCommand viewConsultantAuditTrailCommand = new ViewConsultantAuditTrailCommand();
        commands.put(viewConsultantAuditTrailCommand.getName(), viewConsultantAuditTrailCommand);
        AddConsultantHistoryCommand addConsultantHistoryCommand = new AddConsultantHistoryCommand();
        commands.put(addConsultantHistoryCommand.getName(), addConsultantHistoryCommand);
        BrokerConsultantHistorySearchCommand brokerConsultantHistorySearchCommand = new BrokerConsultantHistorySearchCommand();
        commands.put(brokerConsultantHistorySearchCommand.getName(), brokerConsultantHistorySearchCommand);
        EmployeeAppNotesCommands employeeAppNotesCommands = new EmployeeAppNotesCommands();
        commands.put(employeeAppNotesCommands.getName(), employeeAppNotesCommands);
        BrokerConsultantNotesCommand brokerConsultantNotesCommand = new BrokerConsultantNotesCommand();
        commands.put(brokerConsultantNotesCommand.getName(), brokerConsultantNotesCommand);
        AddBrokerConsultantHistoryCommand addBrokerConsultantHistoryCommand = new AddBrokerConsultantHistoryCommand();
        commands.put(addBrokerConsultantHistoryCommand.getName(), addBrokerConsultantHistoryCommand);
        ConsultantForBrokerSearchCommand consultantForBrokerSearchCommand = new ConsultantForBrokerSearchCommand();
        commands.put(consultantForBrokerSearchCommand.getName(), consultantForBrokerSearchCommand);
        ViewPreAuthBenefitsCommand viewPreAuthBenefitsCommand = new ViewPreAuthBenefitsCommand();
        commands.put(viewPreAuthBenefitsCommand.getName(), viewPreAuthBenefitsCommand);

        PdcLoadAssignedUsers pdcLoadAssignedUsers = new PdcLoadAssignedUsers();
        commands.put(pdcLoadAssignedUsers.getName(), pdcLoadAssignedUsers);
        ViewPDCTasksByUserCommand viewPDCTasksByUserCommand = new ViewPDCTasksByUserCommand();
        commands.put(viewPDCTasksByUserCommand.getName(), viewPDCTasksByUserCommand);
        AssignTaskForTaskTableCommand assignTaskForTaskTableCommand = new AssignTaskForTaskTableCommand();
        commands.put(assignTaskForTaskTableCommand.getName(), assignTaskForTaskTableCommand);
        //PDC Member dependency
        CoverDependantResultCommand coverDependantResultCmd = new CoverDependantResultCommand();
        commands.put(coverDependantResultCmd.getName(), coverDependantResultCmd);
        CoverDependantResultFilterCommand coverDependantResultFltrCmd = new CoverDependantResultFilterCommand();
        commands.put(coverDependantResultFltrCmd.getName(), coverDependantResultFltrCmd);
        CoverDependendGridSelectionCommand cdGridSelectionCommand = new CoverDependendGridSelectionCommand();
        commands.put(cdGridSelectionCommand.getName(), cdGridSelectionCommand);
        NavigateBiometricsCommand navigateBiometricsCommand = new NavigateBiometricsCommand();
        commands.put(navigateBiometricsCommand.getName(), navigateBiometricsCommand);
        BiometricsTableSelectionCommand bioTableSelectionCommand = new BiometricsTableSelectionCommand();
        commands.put(bioTableSelectionCommand.getName(), bioTableSelectionCommand);
        UnlockUserCommand unlockUser = new UnlockUserCommand();
        commands.put(unlockUser.getName(), unlockUser);
        ViewBiometricCommand viewBiometricAsthmaCommand = new ViewBiometricCommand();
        commands.put(viewBiometricAsthmaCommand.getName(), viewBiometricAsthmaCommand);
        BiometricsHistoryCommand bioHistoryCommand = new BiometricsHistoryCommand();
        commands.put(bioHistoryCommand.getName(), bioHistoryCommand);
        ForwardToEditBiometricGeneralInfoCommand forwardToEditBiometricGeneralInfoCommand = new ForwardToEditBiometricGeneralInfoCommand();
        commands.put(forwardToEditBiometricGeneralInfoCommand.getName(), forwardToEditBiometricGeneralInfoCommand);
        UpdatePracticeNameCommand updatePracticeNameCommand = new UpdatePracticeNameCommand();
        commands.put(updatePracticeNameCommand.getName(), updatePracticeNameCommand);
        //PDC Benefits Letters
        ViewDocumentGenerationCommand documentGenerationCommand = new ViewDocumentGenerationCommand();
        commands.put(documentGenerationCommand.getName(), documentGenerationCommand);
        ViewDocumentsCommand viewDocumentsCommand = new ViewDocumentsCommand();
        commands.put(viewDocumentsCommand.getName(), viewDocumentsCommand);

        PDCDocumentGenerationCommand pdcDocGenerationCommand = new PDCDocumentGenerationCommand();
        commands.put(pdcDocGenerationCommand.getName(), pdcDocGenerationCommand);
        LoadBiometricsDetailsCommand loadBioDetails = new LoadBiometricsDetailsCommand();
        commands.put(loadBioDetails.getName(), loadBioDetails);
        ProductIDSelectionCommand prodIDSelection = new ProductIDSelectionCommand();
        commands.put(prodIDSelection.getName(), prodIDSelection);

        PDCDocumentSearchViewCommand pDCDocumentSearchViewCommand = new PDCDocumentSearchViewCommand();
        commands.put(pDCDocumentSearchViewCommand.getName(), pDCDocumentSearchViewCommand);
        PDCDocumentDisplayViewCommand documentDisplayViewCommand = new PDCDocumentDisplayViewCommand();
        commands.put(documentDisplayViewCommand.getName(), documentDisplayViewCommand);

        ChangePayIndicatorCommand changePayIndicatorCommand = new ChangePayIndicatorCommand();
        commands.put(changePayIndicatorCommand.getName(), changePayIndicatorCommand);

        SavePayIndicatorCommand savePayIndicatorCommand = new SavePayIndicatorCommand();
        commands.put(savePayIndicatorCommand.getName(), savePayIndicatorCommand);

        CancelPayIndicatorCommand cancelPayIndicatorCommand = new CancelPayIndicatorCommand();
        commands.put(cancelPayIndicatorCommand.getName(), cancelPayIndicatorCommand);
        EmailLatestDocumentCommand emailLatestDocument = new EmailLatestDocumentCommand();
        commands.put(emailLatestDocument.getName(), emailLatestDocument);

        ViewProviderCommunicationCommand viewProviderCommunicationCommand = new ViewProviderCommunicationCommand();
        commands.put(viewProviderCommunicationCommand.getName(), viewProviderCommunicationCommand);
        ViewProviderDocumentsCommand vpdc = new ViewProviderDocumentsCommand();
        commands.put(vpdc.getName(),vpdc);

        CheckIfMemberHasOHAuth checkIfMemberHasOHAuth = new CheckIfMemberHasOHAuth();
        commands.put(checkIfMemberHasOHAuth.getName(), checkIfMemberHasOHAuth);

        // Sizwe Comands [Union Module]
        UnionSearchCommand unionSearchCommand = new UnionSearchCommand();
        commands.put(unionSearchCommand.getName(), unionSearchCommand);

        UnionViewCommand unionViewCommand = new UnionViewCommand();
        commands.put(unionViewCommand.getName(), unionViewCommand);

        ViewUnionDetailsCommand viewUnionDetailsCommand = new ViewUnionDetailsCommand();
        commands.put(viewUnionDetailsCommand.getName(), viewUnionDetailsCommand);

        ViewGroupHistoryCommand viewGroupHistoryCommand = new ViewGroupHistoryCommand();
        commands.put(viewGroupHistoryCommand.getName(), viewGroupHistoryCommand);

        ViewUnionMemberHistoryCommand viewUnionMemberHistoryCommand = new ViewUnionMemberHistoryCommand();
        commands.put(viewUnionMemberHistoryCommand.getName(), viewUnionMemberHistoryCommand);

        ViewUnionDocuments viewUnionDocuments = new ViewUnionDocuments();
        commands.put(viewUnionDocuments.getName(), viewUnionDocuments);

        ViewUnionNotes viewUnionNotes = new ViewUnionNotes();
        commands.put(viewUnionNotes.getName(), viewUnionNotes);

        ViewUnionAuditTrail viewUnionAuditTrail = new ViewUnionAuditTrail();
        commands.put(viewUnionAuditTrail.getName(), viewUnionAuditTrail);

        UnionCapturerCommand unionCapturerCommand = new UnionCapturerCommand();
        commands.put(unionCapturerCommand.getName(), unionCapturerCommand);

        UnionUpdateCommand unionUpdateCommand = new UnionUpdateCommand();
        commands.put(unionUpdateCommand.getName(), unionUpdateCommand);

        UnionAdvancedSearchCommand unionAdvancedSearchCommand = new UnionAdvancedSearchCommand();
        commands.put(unionAdvancedSearchCommand.getName(), unionAdvancedSearchCommand);  
        
        FowardToAdvancedUnionSearch fowardToAdvancedUnionSearch = new FowardToAdvancedUnionSearch();
        commands.put(fowardToAdvancedUnionSearch.getName(), fowardToAdvancedUnionSearch);
        
        UnionNotesCommand unionNotesCommand = new UnionNotesCommand();
        commands.put(unionNotesCommand.getName(), unionNotesCommand);
        
        LinkMembershipToUnionCommand linkMembershipToUnionCommand = new LinkMembershipToUnionCommand();
        commands.put(linkMembershipToUnionCommand.getName(), linkMembershipToUnionCommand);
        
        MemberUnionAdvancedSearchCommand advancedSearchCommand = new MemberUnionAdvancedSearchCommand();
        commands.put(advancedSearchCommand.getName(), advancedSearchCommand);
    }

    private void ClaimCommands() {
        ClaimHeaderCommand claimHeaderCommand = new ClaimHeaderCommand();
        commands.put(claimHeaderCommand.getName(), claimHeaderCommand);
        ClaimLineCommand claimLineCommand = new ClaimLineCommand();
        commands.put(claimLineCommand.getName(), claimLineCommand);
        ClaimLineNdcCommand claimLineNdcCommand = new ClaimLineNdcCommand();
        commands.put(claimLineNdcCommand.getName(), claimLineNdcCommand);
        ClaimLineModifierCommand claimLineModifierCommand = new ClaimLineModifierCommand();
        commands.put(claimLineModifierCommand.getName(), claimLineModifierCommand);
        SearchClaimCommand searchClaimCommand = new SearchClaimCommand();
        commands.put(searchClaimCommand.getName(), searchClaimCommand);
        EditClaimCommand editClaimCommand = new EditClaimCommand();
        commands.put(editClaimCommand.getName(), editClaimCommand);
        OverrideClaim overrideClaim = new OverrideClaim();
        commands.put(overrideClaim.getName(), overrideClaim);
        RejectClaimLine rejectClaimLine = new RejectClaimLine();
        commands.put(rejectClaimLine.getName(), rejectClaimLine);
        ReverseAndRejectClaim reverseAndRejectClaim = new ReverseAndRejectClaim();
        commands.put(reverseAndRejectClaim.getName(), reverseAndRejectClaim);
        ForwardToClaimNotes forwardToClaimNotes = new ForwardToClaimNotes();
        commands.put(forwardToClaimNotes.getName(), forwardToClaimNotes);
        ClaimNotesCommand claimNotesCommand = new ClaimNotesCommand();
        commands.put(claimNotesCommand.getName(), claimNotesCommand);
        SubmitClaimDocumentCommand submitClaimDocumentCommand = new SubmitClaimDocumentCommand();
        commands.put(submitClaimDocumentCommand.getName(), submitClaimDocumentCommand);
        CloseCarePathTaskCommand closeCarePathTaskCommand = new CloseCarePathTaskCommand();
        commands.put(closeCarePathTaskCommand.getName(), closeCarePathTaskCommand);
        EditCarePathTaskCommand editCarePathTaskCommand = new EditCarePathTaskCommand();
        commands.put(editCarePathTaskCommand.getName(), editCarePathTaskCommand);
        ForwardToConditionSpecificAuth forwardToConditionSpecificAuth = new ForwardToConditionSpecificAuth();
        commands.put(forwardToConditionSpecificAuth.getName(), forwardToConditionSpecificAuth);
    }

    private void automationCommands() {
        PaymentRunCommand paymentRunCommand = new PaymentRunCommand();
        commands.put(paymentRunCommand.getName(), paymentRunCommand);
        LoadBrokerCoverDetailsCommand loadBrokerCoverDetailsCommand = new LoadBrokerCoverDetailsCommand();
        commands.put(loadBrokerCoverDetailsCommand.getName(), loadBrokerCoverDetailsCommand);
    }

}

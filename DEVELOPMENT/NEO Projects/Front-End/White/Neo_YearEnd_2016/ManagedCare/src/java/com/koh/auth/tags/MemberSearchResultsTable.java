/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.auth.tags;

import com.koh.auth.dto.MemberSearchResult;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class MemberSearchResultsTable extends TagSupport {

    private String javaScript;
    private String commandName;

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Expand</th><th>Member Number</th><th>Dependant</th><th>Name</th>" +
                    "<th>Surname</th><th>Scheme</th><th>Plan</th><th>Status</th><th>Join Date</th>" +
                    "<th>Option From</th><th>Option To</th><th>Select</th></tr>");
//
            ArrayList<MemberSearchResult> members = (ArrayList<MemberSearchResult>) req.getAttribute("searchResult");
            String lastMember = "";
            boolean mustClose = false;
            if (members != null && members.size() > 0) {
                for (int i = 0; i < members.size(); i++) {
                    MemberSearchResult memberSearchResult = members.get(i);

                    if (!memberSearchResult.getMemberNumber().equalsIgnoreCase(lastMember)) {
                        if (i != 0) {
                            out.println("</tbody><tr id=\"" + memberSearchResult.getMemberNumber() + "main\"><form method=\"post\"><td align=\"center\">" +
                                    "<a id=\""+memberSearchResult.getMemberNumber()+"plus\" href=\"#\" onClick=\"showRows('" + memberSearchResult.getMemberNumber() + "')\"><img src=\"resources/plus.gif\" width=\"18\" height=\"18\" alt=\"Logout\" border=\"0\"/></a>" +
                                    "<a id=\""+memberSearchResult.getMemberNumber()+"minus\" style=\"display:none\" href=\"#\" onClick=\"hideRows('" + memberSearchResult.getMemberNumber() + "')\"><img src=\"resources/minus.gif\" width=\"18\" height=\"18\" alt=\"Logout\" border=\"0\"/></a>" +
                                    "</td>");
                            mustClose = false;
                        } else {
                            out.println("<tr id=\"" + memberSearchResult.getMemberNumber() + "main\"><form method=\"post\"><td align=\"center\">" +
                                    "<a id=\""+memberSearchResult.getMemberNumber()+"plus\" href=\"#\" onClick=\"showRows('" + memberSearchResult.getMemberNumber() + "')\"><img src=\"resources/plus.gif\" width=\"18\" height=\"18\" alt=\"Logout\" border=\"0\"/></a>" +
                                    "<a id=\""+memberSearchResult.getMemberNumber()+"minus\" style=\"display:none\" href=\"#\" onClick=\"hideRows('" + memberSearchResult.getMemberNumber() + "')\"><img src=\"resources/minus.gif\" width=\"18\" height=\"18\" alt=\"Logout\" border=\"0\"/></a>" +
                                    "</td>");
                            mustClose = false;
                        }

                    } else {
                        if (!mustClose) {
                            out.println("<tbody id=\"" + memberSearchResult.getMemberNumber() + "\" style=\"display:none\">");
                            
                            mustClose = true;
                        }
                        out.println("<tr><form method=\"post\"><td></td>");
                    }

                    out.println("<td><label class=\"label\">" + memberSearchResult.getMemberNumber() + "</label><input type=\"hidden\" name=\"memNo\" value=\""+memberSearchResult.getMemberNumber()+"\"/></td>" +
                            "<td><label class=\"label\">" + memberSearchResult.getDependant() + "</label><input type=\"hidden\" name=\"dependant\" value=\""+memberSearchResult.getDependant()+"\"/></td>" +
                            "<td><label class=\"label\">" + memberSearchResult.getName() + "</label><input type=\"hidden\" name=\"name\" value=\""+memberSearchResult.getName()+"\"/></td>" +
                            "<td><label class=\"label\">" + memberSearchResult.getSurname() + "</label><input type=\"hidden\" name=\"surname\" value=\""+memberSearchResult.getSurname()+"\"/></td>" +
                            "<td><label class=\"label\">" + memberSearchResult.getScheme() + "</label></td>" +
                            "<td><label class=\"label\">" + memberSearchResult.getPlan() + "</label></td>" +
                            "<td><label class=\"label\">" + memberSearchResult.getStatus() + "</label></td>");
                    
                    //join date
                    if (memberSearchResult.getJoinDate() != null) {
                        String dateFrom = new SimpleDateFormat("yyyyMMdd").format(memberSearchResult.getJoinDate());
                        out.println("<td><label class=\"label\">" + dateFrom + "</label></td>");
                    } else {
                        out.println("<td></td>");
                    }
                    //benefit start
                    if (memberSearchResult.getBenefitFromDate() != null) {
                        String dateFrom = new SimpleDateFormat("yyyyMMdd").format(memberSearchResult.getBenefitFromDate());
                        out.println("<td><label class=\"label\">" + dateFrom + "</label></td>");
                    } else {
                        out.println("<td></td>");
                    }
                    //benefit end
                    if (memberSearchResult.getBenefitFromDate() != null) {
                        String dateTo = new SimpleDateFormat("yyyyMMdd").format(memberSearchResult.getBenefitToDate());
                        out.println("<td><label class=\"label\">" + dateTo + "</label></td>");
                    } else {
                        out.println("<td></td>");
                    }
                    out.println("<td><button name=\"opperation\" type=\"submit\" " + javaScript + " value=\"" + commandName + "\">Select</button></td></form>");
                    lastMember = memberSearchResult.getMemberNumber();
                    if (i == members.size() - 1 && mustClose) {
                        out.println("<tbody></tr>");
                    } else {
                        out.println("</tr>");
                    }
                }
            }


            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberSearchTable tag", ex);
        }
        return super.doEndTag();
    }
}

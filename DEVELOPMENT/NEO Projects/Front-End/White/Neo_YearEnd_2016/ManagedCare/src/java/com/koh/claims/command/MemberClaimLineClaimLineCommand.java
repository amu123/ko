/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.claims.dto.ClaimLineModifier;
import com.koh.command.NeoCommand;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import neo.manager.Claim;
import neo.manager.ClaimIcd10;
import neo.manager.ClaimLine;
import neo.manager.ClaimReversals;
import neo.manager.ClaimRuleMessage;
import neo.manager.CoverProductDetails;
import neo.manager.Drug;
import neo.manager.Modifier;
import neo.manager.Ndc;
import neo.manager.NeoManagerBean;
import neo.manager.ProviderDetails;
import neo.manager.TariffCode;
import com.koh.claims.dto.ClaimLineObject;
import com.koh.claims.dto.ClaimLineOverr;
import com.koh.claims.dto.ClaimLineRuleMessage;
import com.koh.utils.DateTimeUtils;
import java.lang.Exception;
import java.util.Collection;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;

/**
 *
 * @author Johan-NB
 */
public class MemberClaimLineClaimLineCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        String serviceProviderNumber = "";
        session.setAttribute("ClaimLineObject", "null");
        String coverNumber = "";
        String depNoStr = "";
        double paidAmount = 0.0d;
        double benefitTotalPaid = 0.00;
        String amount1 = String.valueOf(session.getAttribute("pracDetailedClaimPaidAmount"));
        String amount2 = String.valueOf(session.getAttribute("memberDetailedClaimPaidAmount"));
        if (amount1 != null && !amount1.equals("") && !amount1.equals("null")) {
            paidAmount = Double.parseDouble("" + amount1);
        } else if (amount2 != null && !amount2.equals("") && !amount2.equals("null")) {
            paidAmount = Double.parseDouble("" + amount2);
        }

        ClaimLine claimLine = (ClaimLine) session.getAttribute("memCLCLDetails");
        //set claim details
        ClaimLineObject claimObject = new ClaimLineObject();
        boolean isClearProviderClaim = false;
        if (session.getAttribute("persist_isClearingClaims") != null) {
            isClearProviderClaim = (Boolean) session.getAttribute("persist_isClearingClaims");
        }
        claimObject.setIsPractice(isClearProviderClaim);

        try {

            if (session.getAttribute("onBenefitScreen") != null) {
                coverNumber = (String) session.getAttribute("policyHolderNumber");
                if (session.getAttribute("memberDetailedClaimBenDepCode") != null) {
                    claimObject.setDependantCode(session.getAttribute("memberDetailedClaimBenDepCode").toString());
                    depNoStr = String.valueOf(claimObject.getDependantCode());
                }
                Claim memClaim = port.fetchClaimForClaimLine(claimLine.getClaimId());

                serviceProviderNumber = memClaim.getServiceProviderNo();
            } else {
                coverNumber = (String) session.getAttribute("policyNumber");
                if (coverNumber == null) {
                    coverNumber = "" + session.getAttribute("policyHolderNumber");
                    if(coverNumber.equalsIgnoreCase("")){
                        coverNumber = "" + session.getAttribute("memNum");
                        if(coverNumber.equalsIgnoreCase("")){
                            coverNumber = "" + session.getAttribute("memberNumber");
                        }
                    }
                }
                if (session.getAttribute("memberDetailedClaimDepCode") != null) {
                    claimObject.setDependantCode(session.getAttribute("memberDetailedClaimDepCode").toString());
                    depNoStr = String.valueOf(claimObject.getDependantCode());
                }
                serviceProviderNumber = "" + session.getAttribute("memClaimResultProvNum");

            }
            if (claimObject.getDependantCode() == null) {
                depNoStr = "" + claimLine.getInsuredPersonDependentCode();
            }

            if (session.getAttribute("memberDetailedClaimPaidAmount") != null) {
                claimObject.setPaidAmount(session.getAttribute("memberDetailedClaimPaidAmount").toString());
            }

            //delimit icd codes and descriptions
            String icds = "";
            StringBuilder icdCSV = new StringBuilder();
            StringBuilder icdDesc = new StringBuilder();
            List<String> icd10 = new ArrayList<String>();
            Diagnosis diag;
            List<ClaimIcd10> icdList = claimLine.getIcd10();
            if (icdList != null && !icdList.isEmpty()) {
                for (ClaimIcd10 icd : icdList) {
                    icdCSV.append(icd.getIcd10Code()).append("/ ");
                    diag = port.getDiagnosisForCode(icd.getIcd10Code());
                    if (diag != null && diag.getDescription() != null) {
                        icdDesc.append(diag.getDescription()).append("/ ");
                    } else {
                        icdDesc.append("ICD Description not found on ICD List  ");
                    }
                }

                icdDesc.replace(0, icdDesc.length(), icdDesc.substring(0, icdDesc.length() - 2));
                icdCSV.replace(0, icdCSV.length(), icdCSV.substring(0, icdCSV.length() - 2));
                claimObject.setIcdDescription(icdDesc.toString());
                claimObject.setIcdCode(icdCSV.toString());
            }

            //get service provider number
            ProviderDetails proDet = port.getProviderDetailsForEntityByNumber(serviceProviderNumber);
            CoverProductDetails cpd = port.getCoverProductDetailsByCoverNumber(coverNumber);
            TariffCode tariff = null;
            if (cpd.getProductId() != 0) {
                tariff = port.getTariffCode(cpd.getProductId(), cpd.getOptionId(), claimLine.getServiceDateFrom(), proDet.getDisciplineType().getId(), proDet.getPracticeNumber(), proDet.getPracticeNumber(), icd10, null, claimLine.getTarrifCodeNo());

            }
            String test = "" + claimLine.getTarrifAmount();
            String test2 = "" + claimLine.getTarrifCodeNo();
            //claim line benefits
            List<ClaimLineBenefit> lstBenefitsClaim = port.fetchClaimLineBenefit(claimLine.getClaimLineId());
            Benefit topLvlBenefit;
            String paymentAllocation = "";
            if (lstBenefitsClaim.isEmpty() == false && lstBenefitsClaim != null) {
                for (ClaimLineBenefit benefit : lstBenefitsClaim) {
                    paymentAllocation = (benefit.getPaymentAllocation() == null) ? "" : benefit.getPaymentAllocation();
                    topLvlBenefit = port.findBenefitWithId(benefit.getBenefitId());
                    claimObject.setBenefitDesc(topLvlBenefit.getDescription());
                    benefitTotalPaid = benefitTotalPaid + benefit.getAmmountPaid();
                }
            } else {
                claimObject.setBenefitDesc("-");
            }

            //claim line threshold 
            Collection<ClaimLineThreshold> lstThreshold = port.fetchClaimLineThreshold(claimLine.getClaimLineId());
            double thresholdTotalPaid = 0.00;
            if (lstThreshold.isEmpty() == false && lstThreshold != null) {
                for (ClaimLineThreshold claimLineTheshold : lstThreshold) {
                    thresholdTotalPaid = thresholdTotalPaid + claimLineTheshold.getThresholdAmount();
                }

            }
            claimObject.setThreshold(new DecimalFormat("#####0.00").format(thresholdTotalPaid));

            //depCode
            claimObject.setDependantCode(depNoStr);

            //treatment date
            claimObject.setTreatmentDate(DateTimeUtils.dateFormat.format(claimLine.getServiceDateFrom().toGregorianCalendar().getTime()));

            //tariffs
            if (tariff != null) {
                claimObject.setTariffDescription(tariff.getDescription());
                claimObject.setTariffCode(tariff.getCode());
            } else {
                claimObject.setTariffCode("Tariff Code not found on Tariff List");
            }

            //nappi's
            Ndc ndc = null;
            if (claimLine.getNdc() != null && !claimLine.getNdc().isEmpty()) {
                ArrayList<Ndc> ndcs = (ArrayList<Ndc>) claimLine.getNdc();
                for (Ndc ndc1 : ndcs) {
                    if (ndc1.getPrimaryNdcIndicator() == 1) {
                        ndc = ndc1;
                        break;
                    }
                    ndc = ndc1;
                    System.out.println("The NDC primary indicator " + ndc1.getPrimaryNdcIndicator());
                }
            }
            if (ndc != null) {
                Drug d = port.getDrugForCode(ndc.getNdcCode(), proDet.getDisciplineType().getId());
                if (d != null) {
                    claimObject.setNappiCode(d.getCode());
                    claimObject.setNappiDescription(d.getName());
                } else {
                    claimObject.setNappiCode("Nappi Code not found on Drug List");
                }
            } else {
                claimObject.setNappiCode("-");
                claimObject.setNappiDescription("-");
            }
            //Amounts
            double claimedAmount = 0.0d;
            double tariffAmount = 0.0d;
            double overpaidAmount = 0.0d;
            String overPaid = "0.0";
            if (isClearProviderClaim) {
                //do clearing calculation
                //claimed amount (clearing_original_claimed_amount)
                claimedAmount = claimLine.getClaimLineClearing().getClaimedAmount();
                //tariff amount (cl tariff amount - clc admin fee)
                double adminAmount = claimLine.getClaimLineClearing().getAdminAmount();
                if (adminAmount < 0) {
                    tariffAmount = claimLine.getTarrifAmount() - Math.abs(adminAmount);
                } else {
                    tariffAmount = claimLine.getTarrifAmount() - adminAmount;
                }
                //overpaid amount (claimed amount - tariff amount)
                double overPaidCalc = 0.0d;
                overPaidCalc = claimedAmount - tariffAmount;
                if (overPaidCalc > 0.0d) {
                    overpaidAmount = overPaidCalc;
                    overPaid = new DecimalFormat("#####0.00").format(overpaidAmount);
                }

            } else {
                claimedAmount = claimLine.getClaimedAmount();
                tariffAmount = claimLine.getTarrifAmount();
            }
            //overpaid amount -> if practice 
            if (isClearProviderClaim) {
                claimObject.setOverPaidAmount(overPaid);
            }
            //claimed amount
            claimObject.setClaimedAmount(new DecimalFormat("#####0.00").format(claimedAmount));
            //tariff amount
            claimObject.setTariffAmount(new DecimalFormat("#####0.00").format(tariffAmount));
            //paid amount
            claimObject.setPaidAmount(new DecimalFormat("#####0.00").format(paidAmount));
            //copayment
            claimObject.setCopayment(new DecimalFormat("#####0.00").format(claimLine.getCopayAmount()));
            //process date
            if (claimLine.getProsessDate() != null) {
                claimObject.setProcessDate(DateTimeUtils.dateFormat.format(claimLine.getProsessDate().toGregorianCalendar().getTime()));
            } else {
                claimObject.setProcessDate("-");
            }
            //Pre-Auth Number
            List<String> lstAuthNumbers = claimLine.getAuthNumber();
            StringBuilder strAuthNumbers = new StringBuilder();

            if (lstAuthNumbers.isEmpty() == false && lstAuthNumbers != null) {
                for (String str : lstAuthNumbers) {
                    strAuthNumbers.append(str).append(", ");
                }
                strAuthNumbers.replace(0, strAuthNumbers.length(), strAuthNumbers.substring(0, strAuthNumbers.length() - 2));
            } else {
                strAuthNumbers.append("-");
            }
            claimObject.setPreAuthNumbers(strAuthNumbers.toString());

            //Neo User Name/Operator
            NeoUser user = claimLine.getNeoUser();
            if (user != null) {
                NeoUser userOperator = port.getUserSafeDetailsById(claimLine.getNeoUser().getUserId());
                claimObject.setOperator(userOperator.getName() + " " + ((userOperator.getSurname() == null) ? "" : userOperator.getSurname()));
            } else {
                claimObject.setOperator("-");
            }
            //tooth number 
            StringBuilder strToothNumber = new StringBuilder();
            if (claimLine.getToothNumber() != null && claimLine.getToothNumber().isEmpty() == false) {
                for (int tooth : claimLine.getToothNumber()) {
                    strToothNumber.append(tooth).append(", ");
                }
                strToothNumber.replace(0, strToothNumber.length(), strToothNumber.substring(0, strToothNumber.length() - 2));
            } else {
                strToothNumber.append("-");
            }
            claimObject.setToothNumber(strToothNumber.toString());

            //override 
            List<ClaimLineOverride> lstOverride = claimLine.getClaimLineOverride();
            List<ClaimLineOverr> lstClaimOverride = new ArrayList<ClaimLineOverr>();
            ClaimLineOverr claimOver = null;
            if (lstOverride.isEmpty() == false && lstOverride != null) {
                for (ClaimLineOverride override : lstOverride) {
                    claimOver = new ClaimLineOverr();
                    if (claimLine.getClaimLineId() == override.getClaimLineId()) {
                        claimOver.setUnits(claimLine.getUnits());
                        claimOver.setMultiplier(claimLine.getMultiplier());
                    }
                    claimOver.setClaimLineId(override.getClaimLineId());
                    claimOver.setDateOverriden(override.getDateOverriden().toString());
                    claimOver.setDescription(override.getDescription());
                    claimOver.setOverrideId(override.getOverrideId());
                    claimOver.setUserId(override.getUserId());
                    NeoUser neoUser = port.getUserSafeDetailsById(override.getUserId());
                    claimOver.setUserName(neoUser.getName());
                    lstClaimOverride.add(claimOver);
                }
                claimObject.setLstClaimLineOverride(lstClaimOverride);
            }

            //rejection reason table
            Integer status = claimLine.getClaimLineStatusId();
            String payStatus = port.getValueForId(83, status.toString());
            List<ClaimLineRuleMessage> lstClaimRuleMsg = new ArrayList<ClaimLineRuleMessage>();
            ClaimLineRuleMessage claimMsg = null;
            if (payStatus.equalsIgnoreCase("rejected") || payStatus.equalsIgnoreCase("accepted") || payStatus.equalsIgnoreCase("Pended")) {
                List<ClaimRuleMessage> crmList = claimLine.getRuleMessages();
                if (crmList != null && crmList.isEmpty() == false) {
                    for (ClaimRuleMessage crm : crmList) {
                        claimMsg = new ClaimLineRuleMessage();
                        claimMsg.setMessageCode(crm.getMessageCode());
                        claimMsg.setSeverity(crm.getSeverity());
                        claimMsg.setLongMsgDescription(crm.getLongMsgDescription());
                        lstClaimRuleMsg.add(claimMsg);
                    }
                    claimObject.setLstClaimLineRuleMessages(lstClaimRuleMsg);
                }
            } else if (payStatus.equalsIgnoreCase("reversed")) {
                ClaimReversals cr = claimLine.getClaimReversals();
                if (cr != null) {
                    claimObject.setReversalCode(cr.getReversalCode());
                    claimObject.setReversalDesc(cr.getReversalDescription());
                }
            }

            //modifiers
            List<Modifier> modList = claimLine.getModifiers();
            List<ClaimLineModifier> lstMod = new ArrayList<ClaimLineModifier>();
            ClaimLineModifier modObj = null;
            if (modList != null && modList.isEmpty() == false) {
                for (Modifier mod : modList) {
                    modObj = new ClaimLineModifier();
                    modObj.setModifier(mod.getModifier());
                    modObj.setModifierAmount(mod.getModifierTariffAmount());
                    lstMod.add(modObj);
                }
                claimObject.setLstClaimLineModifiers(lstMod);
            }

            //unit
            if (claimLine.getUnits() != 0) {
                claimObject.setUnits(claimLine.getUnits());
            }
            double memberliability = 0.00;
            memberliability = (claimLine.getClaimedAmount() - benefitTotalPaid);

            session.setAttribute("MemberLiability", new DecimalFormat("#####0.00").format(memberliability));
            session.setAttribute("ClaimLineObject", claimObject);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "MemberClaimLineClaimLineCommand";
    }
}

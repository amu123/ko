/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.utils;

import com.koh.preauth.command.AddAuthLOSToSessionCommand;
import com.koh.utils.FormatUtils;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.TariffCode;

/**
 *
 * @author johanl
 */
public class ERPUtils {

    public ERPUtils() {
    }

    public static double getEstimatedCostByLOSForWard(XMLGregorianCalendar xAdmission, int prodId, int optId, String code,
            String facilityDisc, String facility, String pICD, double los) {

        System.out.println("admission = " + xAdmission); 
        System.out.println("prod id = " + prodId);
        System.out.println("opt id = " + optId);
        System.out.println("code = " + code);
        System.out.println("facility disc = " + facilityDisc);
        System.out.println("facility = " + facility);
        System.out.println("pICD = " + pICD);
        System.out.println("LOS = " + los);
        
        TariffCode t = null;
        double estimatedWardAmount = 0.0d;

        t = AddAuthLOSToSessionCommand.getWardTariff(xAdmission, prodId, optId, code, facilityDisc, facility, pICD);

        System.out.println("TariffCode Object = " + t);
        
        if (t != null) {
            String bdAmount = FormatUtils.decimalFormat.format(t.getPrice());
            System.out.println("getEstimatedCostByLOSForWard bdAmount = " + bdAmount);
            estimatedWardAmount = Double.valueOf(bdAmount);

            //multiply ward amount with los
            double newTAmount = estimatedWardAmount * los;
            newTAmount = Double.valueOf(FormatUtils.decimalFormat.format(newTAmount));
            estimatedWardAmount = newTAmount;

            System.out.println("estimatedWardAmount amount after erp = " + estimatedWardAmount);
        }
        return estimatedWardAmount;
    }
}

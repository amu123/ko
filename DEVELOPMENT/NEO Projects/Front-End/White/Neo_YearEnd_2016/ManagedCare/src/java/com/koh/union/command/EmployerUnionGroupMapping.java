/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.union.command;

import com.koh.employer.command.TabUtils;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author shimanem
 */
public class EmployerUnionGroupMapping {

    private static final String[][] FIELD_MAPPINGS = {
        {"employerEntityId", "", "no", "int", "EmployerUnionGroup", "employerEntityId"},
        {"employerName", "", "no", "str", "EmployerUnionGroup", "employerName"},
        {"employerNumber", "", "yes", "str", "EmployerUnionGroup", "employerNumber"},
        {"employerGroup_unionName", "Union Name", "yes", "str", "EmployerUnionGroup", "employerGroupUnionName"},
        {"employerGroup_unionID", "Union ID", "yes", "int", "EmployerUnionGroup", "employeremployerGroupUnionIDName"},
        {"employerGroup_unionNumber", "Union Number", "yes", "str", "EmployerUnionGroup", "employerGroupUnionNumber"},
        {"repName", "Representative Name", "yes", "str", "EmployerUnionGroup", "repName"},
        {"repSurname", "Representative Surname", "yes", "str", "EmployerUnionGroup", "repSurname"},
        {"unionRepCapacity", "Capacity of Representative", "yes", "int", "EmployerUnionGroup", "unionRepCapacity"},
        {"unionRegion", "Region", "yes", "int", "EmployerUnionGroup", "unionRegion"},
        {"unionInceptionDate", "Inception Date", "yes", "date", "EmployerUnionGroup", "unionInceptionDate"},
        {"unionTerminationDate", "Termination Date", "yes", "date", "EmployerUnionGroup", "unionTerminationDate"},
        {"contactNumber", "Contact Number", "yes", "str", "EmployerUnionGroup", "contactNumber"},
        {"faxNumber", "Fax Number", "no", "str", "EmployerUnionGroup", "faxNumber"},
        {"emailAddress", "Email Address", "yes", "str", "EmployerUnionGroup", "emailAddress"},
        {"altEmailAddress", "Alternative Email Address", "no", "str", "EmployerUnionGroup", "altEmailAddress"}

    };

    private static final Map<String, String[]> FIELD_MAP;

    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }

    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
        return errors;
    }
}

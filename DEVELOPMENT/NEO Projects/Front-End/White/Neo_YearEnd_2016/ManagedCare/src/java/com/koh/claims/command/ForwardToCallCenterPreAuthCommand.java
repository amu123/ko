/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ForwardToCallCenterPreAuthCommand extends NeoCommand {

    private Logger log = Logger.getLogger(ForwardToCallCenterPreAuthCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        log.info("Inside ForwardToCallCenterPreAuthCommand");

        HttpSession session = request.getSession();

        String authNumber = request.getParameter("authNumber");
        String aId = request.getParameter("authId");
        int authId = 0;
        if (aId != null && !aId.trim().equalsIgnoreCase("")) {
            try {
                authId = new Integer(aId);
            } catch (NumberFormatException nf) {
                System.out.println("no auth id found");
            }
        }
        session.setAttribute("authNumber", authNumber);
        session.setAttribute("authCode", authId);

        try {

            GetCallCenterPreAuthConfirmationDetails callCenterConfirm = new GetCallCenterPreAuthConfirmationDetails();
            callCenterConfirm.execute(request, response, context);

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "ForwardToCallCenterPreAuthCommand";
    }

}

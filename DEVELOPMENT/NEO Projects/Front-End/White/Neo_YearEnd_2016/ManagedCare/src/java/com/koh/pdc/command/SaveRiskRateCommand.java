/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.Command;
import com.koh.command.NeoCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CoverDetails;
import neo.manager.EventDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.PdcRuleMessage;
import neo.manager.PersonDetails;
import neo.manager.Security;

/**
 *
 * @author josephm
 */
public class SaveRiskRateCommand extends Command {

    private Object _datatypeFactory;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        System.out.println("Inside the SaveRiskRateCommand");

        HttpSession session = request.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        //build pdc firing object
        PdcRuleMessage rm = new PdcRuleMessage();
        
        rm.setPCGDESC("Manual Entry");
        rm.setMessageCode("P999");
        rm.setRULENO("9991");
        rm.setRULERUNDATETIME(sdf.format(new Date()));
        
        String riskFactor = request.getParameter("riskFactor");
        String riskReason = request.getParameter("riskReason");
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        String riskRatingNotes = request.getParameter("riskRatingNotes");
        
        rm.setShortMsgDescription("Manual Entry - " + riskReason);
        
        if(riskRatingNotes != null && !riskRatingNotes.equals("") && !riskRatingNotes.equalsIgnoreCase("null")){
            rm.setLongMsgDescription("Manual Entry - " + riskReason + " (" + riskRatingNotes + ")");
        }else{
            rm.setLongMsgDescription("Manual Entry - " + riskReason);
        }
        
        rm.setSeverity(riskFactor);
        
        Security sec = new Security();
        sec.setSecurityGroupId(user.getSecurityGroupId());
        sec.setCreatedBy(user.getUserId());
        sec.setLastUpdatedBy(user.getUserId());
        int firingId = port.insertPDCFiring(rm, sec);
        
        String msg = "";
        
        if(firingId != 0){
            rm.setFiringID(firingId);

            String covNum = (String) session.getAttribute("policyNumber");
            int depCode = new Integer(session.getAttribute("dependentNumber").toString());

            List<CoverDetails> covList = port.getCoverDetailsByCoverNumber(covNum);
            CoverDetails selectedDep = new CoverDetails();
            
            for(CoverDetails c : covList){
                if(c.getDependentNumber() == depCode){
                    selectedDep = c;
                }
            }

            System.out.println("selected dependant entity id = " + selectedDep.getEntityId());
            
            boolean saved = port.insertManualWorklistEvent(rm, selectedDep.getEntityId());
            
            if(saved){
                msg = "Manual patient risk rating was saved";
            }else{
                msg = "Save was unsuccessful";
            }
        }else{
            msg = "Save was unsuccessful";
        }
        
        PrintWriter out;
        try {
            out = response.getWriter();

            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            out.println("<td><label class=\"header\"><h2>" + msg + "</h2></label></td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            String manualRatingFlag = "" + session.getAttribute("pdcManualFlag");
            if(manualRatingFlag != null && manualRatingFlag.equalsIgnoreCase("true")){
                response.setHeader("Refresh", "3; URL=/ManagedCare/PDC/ViewWorkBench.jsp");
            }else{
                response.setHeader("Refresh", "3; URL=/ManagedCare/PDC/PolicyHolderDetails.jsp");
            }
            
        } catch (IOException ex) {
            Logger.getLogger(SaveRiskRateCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public XMLGregorianCalendar convertDateXML(Date date) {

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getDatatypeFactory().newXMLGregorianCalendar(calendar);

    }

    private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }

        return (DatatypeFactory) _datatypeFactory;
    }

    @Override
    public String getName() {
        return "SaveRiskRateCommand";
    }
}

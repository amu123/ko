/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.member.application.MemberAppChronicDetailsMapping;
import com.koh.member.application.MemberAppCommon;
import com.koh.member.application.MemberAppDepMapping;
import com.koh.member.application.MemberAppGeneralDetailsMapping;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class MemberApplicationChronicCommand extends NeoCommand{
 
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
            Enumeration e = request.getParameterNames();
            while (e.hasMoreElements()) {
                String pName = "" + e.nextElement();
                System.out.println(pName + " " + request.getParameter(pName));
            }
        try {
            String s = request.getParameter("buttonPressed");
            if (s != null && !s.isEmpty()) {
                System.out.println("buttonPressed : " + s);
                if (s.equalsIgnoreCase("Add Details")) {
                    addDetails(port,request, response, context);
                } else if (s.equalsIgnoreCase("SaveButton")) {
                    saveDetails(port,request, response, context);
                } else if (s.equalsIgnoreCase("EditButton")) {
                    editDetails(port,request, response, context);
                } else if (s.equalsIgnoreCase("RemoveButton")) {
                    removeDetails(port,request, response, context);
                }
            }

//            request.setAttribute("MemAppSearchResults", col);
//            context.getRequestDispatcher("/MemberApplication/MemberAppSearchResults.jsp").forward(request, response);
        } catch (java.lang.Exception ex) {
            Logger.getLogger(MemberApplicationChronicCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void addDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        request.setAttribute("memberAppChronicId", null);
        int appNum = getStrToInt(request.getParameter("memberAppNumber"));
        request.setAttribute("DependentList", MemberAppCommon.getDependantListMap(service, appNum, request.getParameter("memberAppType")));
        context.getRequestDispatcher("/MemberApplication/MemberAppChronicDetails.jsp").forward(request, response);
    }
    
    private void saveDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = MemberAppChronicDetailsMapping.validate(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (saveDetails(port,request)) {
                
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving.\"";
            }
        } else {
            additionalData = null;
        }
        if ("OK".equalsIgnoreCase(status)) {
            context.getRequestDispatcher(new MemberAppTabContentsCommand().getMemberAppChronic(port,request)).forward(request, response);
        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }
         
        
    }
    
    private boolean saveDetails(NeoManagerBean port, HttpServletRequest request) {
        NeoUser neoUser = (NeoUser)request.getSession().getAttribute("persist_user");
        try {
            MemAppChronicDetail app = MemberAppChronicDetailsMapping.getMemAppChronicDetail(request, neoUser);
            port.saveMemAppChronicDetail(app);
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
    }
    
    private void editDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        int appNum = getStrToInt(request.getParameter("memberAppNumber"));
        int genId = getStrToInt(request.getParameter("memberAppChronicId"));
        List<MemAppChronicDetail> memAppChronicDetail = port.fetchMemAppChronicDetailByAppNum(appNum);
        MemAppChronicDetail chronicDetail = null;
        for (MemAppChronicDetail cd : memAppChronicDetail) {
            if (cd.getChronicDetailId() == genId) {
                chronicDetail = cd;
            }
        }
        request.setAttribute("DependentList", MemberAppCommon.getDependantListMap(service, appNum, request.getParameter("memberAppType")));
        MemberAppChronicDetailsMapping.setMemAppChronicDetail(request, chronicDetail);
        context.getRequestDispatcher("/MemberApplication/MemberAppChronicDetails.jsp").forward(request, response);
    }
    
    private void removeDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        int appNum = getStrToInt(request.getParameter("memberAppNumber"));
        int chronicId = getStrToInt(request.getParameter("memberAppChronicId"));
        port.removeMemberApplicationChronicDetail(chronicId);
        context.getRequestDispatcher(new MemberAppTabContentsCommand().getMemberAppChronic(port,request)).forward(request, response);
    }
     
    private int getStrToInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (java.lang.Exception e) {
            System.out.println(s + " = " + e.getMessage());
        }
        return 0;
    }
    
    @Override
    public String getName() {
        return "MemberApplicationChronicCommand";
    }

   
}

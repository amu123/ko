/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.PdcNotes;
import neo.manager.PersonDetails;

/**
 *
 * @author princes
 */
public class ViewPolicyHolderDetailsCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        //System.out.println("entered ViewPolicyHolderDetailsCommand");
        Integer entityId = (Integer) session.getAttribute("entityId");
        String searchView = (String) session.getAttribute("searchView");
        PersonDetails person = service.getNeoManagerBeanPort().getPersonDetailsByEntityId(entityId);

        session.setAttribute("title", person.getTitle());
        session.setAttribute("initials", person.getInitials());
        session.setAttribute("idNumber", person.getIDNumber());
        session.setAttribute("matitalStatus", person.getMaritalStatus());
        session.setAttribute("homeLanguage", person.getHomeLanguage());
        session.setAttribute("employerName", person.getEmployer());
        session.setAttribute("incomeCategoty", person.getIncomeCategory());
        session.setAttribute("incomeCategoty", person.getJobTitle());

        //Remove for Address infor from session
        session.removeAttribute("PhysicalAddress");
        session.removeAttribute("PhysicalAddress1");
        session.removeAttribute("patientPhysicalAddress2");
        session.removeAttribute("PhysicalCity");
        session.removeAttribute("PostalAddress");
        session.removeAttribute("PostalAddress1");
        session.removeAttribute("PostalAddress2");
        session.removeAttribute("PhysicalCity");
        session.removeAttribute("email");
        session.removeAttribute("faxNumber");
        session.removeAttribute("cellNumber");
        session.removeAttribute("workNumber");
        session.removeAttribute("homeNumber");

        session.removeAttribute("dates");
        session.removeAttribute("dropDownDates");
        session.removeAttribute("sessionHash");
        session.removeAttribute("catched");

        String nextJSP;

        if (searchView != null) {
            nextJSP = "/PDC/PolicyHolderDetailsMemberSearchView.jsp";
        } else {
            nextJSP = "/PDC/PolicyHolderDetails.jsp";
        }

        //set pdc notes to session
        List<PdcNotes> noteList = port.retrievePdcNotes(entityId);//session.getAttribute("pdcNoteList");
        if (noteList != null && !noteList.isEmpty()) {
            session.setAttribute("pdcNoteList", noteList);
        }

        try {

            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewPolicyHolderDetailsCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.employer.command;

/**
 *
 * @author Administrator
 */
public class BrokerLookup {
    
    String name;
    int brokerEntityId;
    boolean selected;
    
    public BrokerLookup(int brokerEntityId,String name) {
        this.name = name;
        this.brokerEntityId = brokerEntityId;
    }
    
    public BrokerLookup(int brokerEntityId,String name,boolean selected) {
        this.name = name;
        this.brokerEntityId = brokerEntityId;
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getBrokerEntityId() {
        return brokerEntityId;
    }
    public void setBrokerEntityId(int brokerEntityId) {
        this.brokerEntityId = brokerEntityId;
    }
    
    
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.statement.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author whauger
 */

public class GetPaymentRunDatesCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String from = request.getParameter("fromDate");
        String to = request.getParameter("toDate");
        Date fromDate = DateTimeUtils.convertFromYYYYMMDD(from);
        Date toDate = DateTimeUtils.convertFromYYYYMMDD(to);
        XMLGregorianCalendar xmlFromDate = DateTimeUtils.convertDateToXMLGregorianCalendar(fromDate);
        XMLGregorianCalendar xmlToDate = DateTimeUtils.convertDateToXMLGregorianCalendar(toDate);

        try{
            PrintWriter out = response.getWriter();
            //Get payment run dates
            Collection <String> dateList = service.getNeoManagerBeanPort().findPaymentRunDates(xmlFromDate, xmlToDate);
            String dates = "";
            if (dateList != null && dateList.size() > 0) {
                for (String date : dateList) {
                    if (dates.length() > 0)
                        dates = dates + "," + date;
                    else
                        dates = date;
                }
                out.print(getName() + "|Dates=" + dates + "$");
            }
            else{
                out.print("Error|No payment dates|" + getName());
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetPaymentRunDatesCommand";
    }

}

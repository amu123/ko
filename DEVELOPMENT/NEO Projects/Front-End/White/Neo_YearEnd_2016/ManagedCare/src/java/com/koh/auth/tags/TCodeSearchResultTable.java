/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.tags;

import com.agile.security.webservice.TCode;
import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;


/**
 *
 * @author whauger
 */
public class TCodeSearchResultTable extends TagSupport {
    private String javascript;
    private String commandName;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            out.println("<label class=\"header\">T-Code Search Results</label></br></br>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>TCode</th><th>Description</th><th>Lab Code</th><th>Quantity</th><th>Lab Code Description</th><th>Tariff</th></tr>");

            ArrayList<TCode> tCodes = (ArrayList<TCode>) req.getAttribute("searchTCodeResult");
            if (tCodes != null) {
                for (int i = 0; i < tCodes.size(); i++) {
                    TCode tcode = tCodes.get(i);
                    out.println("<tr><form><td><label class=\"label\">" + tcode.getTcode() + "</label><input type=\"hidden\" name=\"code\" value=\"" + tcode.getTcode() + "\"/></td>" +
                            "<td><label class=\"label\">" + tcode.getDescription() + "</label><input type=\"hidden\" name=\"description\" value=\"" + tcode.getDescription() + "\"/></td>" +
                            "<td><label class=\"label\">" + tcode.getLabCode() + "</label><input type=\"hidden\" name=\"description\" value=\"" + tcode.getLabCode() + "\"/></td>" +
                            "<td><label class=\"label\">" + tcode.getQuantity() + "</label><input type=\"hidden\" name=\"description\" value=\"" + tcode.getQuantity() + "\"/></td>" +
                            "<td><label class=\"label\">" + tcode.getLabCodeDescription() + "</label><input type=\"hidden\" name=\"description\" value=\"" + tcode.getLabCodeDescription() + "\"/></td>" +
                            "<td><label class=\"label\">" + tcode.getTariff() + "</label><input type=\"hidden\" name=\"description\" value=\"" + tcode.getTariff() + "\"/></td></form></tr>");
                            //"<td><button name=\"opperation\" type=\"submit name=\"opperation\" type=\"submit\" " + javascript + " value=\"" + commandName + "\">Select</button></td></form></tr>");

                }
            }
            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in TCodeSearchResultTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

}

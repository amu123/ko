/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.tags;

import agility.za.documentprinting.DocumentPrintingRequest;
import agility.za.documentprinting.DocumentPrintingResponse;
import agility.za.documentprinting.PrintFilter;
import agility.za.documentprinting.PrintingType;
import com.koh.command.NeoCommand;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Christo
 */
public class PrintPoolList extends TagSupport {

    private String command;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            DocumentPrintingRequest request = new DocumentPrintingRequest();

            request.setPrintProcessType("ViewPrintJobs");
            String displayOption = (String) req.getAttribute("displayOption");
            String displayDay = (String) req.getAttribute("displayDay");
            String displayGroup = (String) req.getAttribute("displayGroup");
            String displayType = (String) req.getAttribute("displayType");
            String displayScheme = (String) req.getAttribute("displayScheme");

            PrintFilter filter = new PrintFilter();

            if (displayOption == null || displayOption.equals("") || displayOption.equalsIgnoreCase("ACTIVE")) {
                displayOption = "active";
                filter.setStatus("ACTIVE");
                request.setFilter(filter);
            } else if (displayOption.equalsIgnoreCase("FAILED")) {
                filter.setStatus("FAILED");
                request.setFilter(filter);
            } else if (displayOption.equalsIgnoreCase("SUCCESS")) {
                filter.setStatus("SUCCESS");
                request.setFilter(filter);
            }

            if (displayDay == null || displayDay.equals("")) {
                displayDay = "today";
                filter.setFromDate(addDay(0));
                request.setFilter(filter);
            } else if (displayDay.equals("all")) {
                //Do nothing
            } else if (displayDay.equals("today")) {
                filter.setFromDate(addDay(0));
                request.setFilter(filter);
            } else if (displayDay.equals("yesterday")) {
                filter.setFromDate(addDay(-1));
                request.setFilter(filter);
            } else if (displayDay.equals("7days")) {
                filter.setFromDate(addDay(-7));
                request.setFilter(filter);
            } else if (displayDay.equals("30days")) {
                filter.setFromDate(addDay(-30));
                request.setFilter(filter);
            }

            if (displayGroup != null && displayGroup.equals("groupType")) {
                filter.setGroup(true);
                request.setFilter(filter);
            }
            if (displayType != null && !displayType.equals("")) {
                filter.setType(displayType);
                request.setFilter(filter);
            }

            if (displayScheme == null || displayScheme.equals("") || displayScheme.equals("All")) {
                displayScheme = "All";
            } else if (displayScheme.equals("1")) {
                filter.setSchemeId(1);
                request.setFilter(filter);
            } else if (displayScheme.equals("2")) {
                filter.setSchemeId(2);
                request.setFilter(filter);
            }

            DocumentPrintingResponse response = NeoCommand.service.getNeoManagerBeanPort().processPrintRequest(request);

            List<PrintingType> printqueueList = response.getPrintEntity();
            out.println("<input type=\"hidden\" name=\"selectedDisplayOption\" id=\"selectedDisplayOption\" value=\"" + displayOption + "\" />");
            out.println("<input type=\"hidden\" name=\"selectedDisplayDay\" id=\"selectedDisplayDay\" value=\"" + displayDay + "\" />");
            out.println("<input type=\"hidden\" name=\"selectedDisplayGroup\" id=\"selectedDisplayGroup\" value=\"" + displayGroup + "\" />");
            out.println("<input type=\"hidden\" name=\"selectedDisplayType\" id=\"selectedDisplayType\" value=\"" + displayType + "\" />");
            out.println("<input type=\"hidden\" name=\"selectedDisplayScheme\" id=\"selectedDisplayScheme\" value=\"" + displayScheme + "\" />");

            out.println("<input type=\"hidden\" name=\"selectedFile\" id=\"selectedFile\" value=\"\" />");
            out.println("<input type=\"hidden\" name=\"selectedId\" id=\"selectedId\" value=\"\" />");
//            out.println("<label class=\"header\">Search Results</label><br/><br/>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");

            displayScheme(out, displayScheme);
            displayOption(out, displayOption);
            displayDay(out, displayDay);
            displayGroup(out, displayGroup);
            displayType(out, displayType, response.getAvailableType());


            out.println("<td><button type=\"submit\" name=\"filterD\" onClick=\"document.getElementById('opperation').value ='PrintPoolPauseCommand';\">PrintAll</button></td>");
            out.println("<td><button type=\"submit\" name=\"filterD\" onClick=\"document.getElementById('opperation').value ='PrintPoolFilterCommand';\">Filter</button></td></tr>");
            out.println("</table><table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");

            out.println("<tr><th>Description</th><th>Action</th><th>Status</th><th>Send Date</th><th>Process Date</th><th>Output Type</th><th>File Type</th><th>File Name</th><th>Printer</th><th>User</th><th></th><th></th></tr>");
            if (printqueueList != null && printqueueList.size() > 0) {
//                out.println("<tr>");
                ArrayList<String> memberNo = new ArrayList();
//                for (PrintingType index : printqueueList) {
//                    String s = index.getFileName().substring(0, index.getFileName().indexOf("-"));
//                    String t = index.getDescription();
//                    String a = s + " " + t;
//                    if(memberNo.contains(a)){
//                        System.out.println("Member Number duplicate not added: " + a + " scheme ID : " + index.getSchemeId());
//                    } else {
//                        memberNo.add(a);
//                        System.out.println("Member Number added to list: " + a + " scheme ID : " + index.getSchemeId());
//                    }             
//                }

                for (PrintingType index : printqueueList) {

                    out.println("<tr><td><label class=\"label\">" + index.getDescription() + "</label></td>"
                            + "<td><label class=\"label\">" + index.getAction() + "</label></td>"
                            + "<td><label class=\"label\">" + index.getStatus() + "</label></td>"
                            + "<td><label class=\"label\">" + convertDate(index.getSendDate()) + "</label></td>"
                            + "<td><label class=\"label\">" + convertDate(index.getProcessDate()) + "</label></td>"
                            + "<td><label class=\"label\">" + index.getOutputType() + "</label></td>"
                            + "<td><label class=\"label\">" + index.getFileType() + "</label></td>"
                            + "<td><label class=\"label\">" + index.getFileName() + "</label></td>"
                            + "<td><label class=\"label\">" + index.getPrinter() + "</label></td>"
                            + "<td><label class=\"label\">" + index.getUsers() + "</label></td>");

                    if (displayOption.equals("active")) {
                        out.println("<td><button type=\"submit\" name=\"com" + index.getFileName() + "\"  onClick=\"setParams('PrintPoolCommand','" + index.getFileName() + "','" + index.getPrintId() + "')\">Print</button></td>");
                        out.println("<td><button type=\"submit\" name=\"del" + index.getFileName() + "\"  onClick=\"setParams('PrintPoolDeleteCommand','" + index.getFileName() + "','" + index.getPrintId() + "')\">Delete</button></td></tr>");
                    } else {
                        out.println("</td></tr>");
                    }
                }
                out.println("</table>");
            }




            System.out.println("--End tag--");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in PrintPoolList tag", ex);
        }
        return super.doEndTag();
    }

    private String convertDate(XMLGregorianCalendar cal) {
        if (cal == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm");
//	        xc.setTimezone(getTimeZoneOffset());
        return sdf.format(cal.toGregorianCalendar().getTimeInMillis());

    }

    public void setCommand(String command) {
        this.command = command;
    }

    public XMLGregorianCalendar addDay(int add) {
        try {
            Calendar fromDate = Calendar.getInstance();
            fromDate.add(Calendar.DAY_OF_MONTH, add);
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(fromDate.getTime());
            DatatypeFactory fact = DatatypeFactory.newInstance();
            XMLGregorianCalendar cal = fact.newXMLGregorianCalendar(calendar);
            return cal;
        } catch (Exception e) {
            System.err.println(e);
        }
        return null;
    }

    private void displayOption(JspWriter out, String displayOption) throws IOException {
        out.println("<tr><td><select style=\"width:215px\" size=\"1\" name=\"displayOption\" value=\"" + displayOption + "\" >");

        if (displayOption.equalsIgnoreCase("active")) {
            out.println("<option selected=\"selected\" value=\"active\" >Active</option>");
            out.println("<option value=\"success\" >Success</option>");
            out.println("<option value=\"failed\" >Failed</option>");
            out.println("<option value=\"all\" >All</option>");
        } else if (displayOption.equalsIgnoreCase("FAILED")) {
            out.println("<option value=\"active\" >Active</option>");
            out.println("<option value=\"success\" >Success</option>");
            out.println("<option selected=\"selected\" value=\"failed\" >Failed</option>");
            out.println("<option value=\"all\" >All</option>");
        } else if (displayOption.equalsIgnoreCase("SUCCESS")) {
            out.println("<option value=\"active\" >Active</option>");
            out.println("<option selected=\"selected\" value=\"success\" >Success</option>");
            out.println("<option value=\"failed\" >Failed</option>");
            out.println("<option value=\"all\" >All</option>");
        } else {
            out.println("<option value=\"active\" >Active</option>");
            out.println("<option value=\"success\" >Success</option>");
            out.println("<option value=\"failed\" >Failed</option>");
            out.println("<option selected=\"selected\" value=\"all\" >All</option>");
        }
        out.println("</select> </td>");
    }

    @SuppressWarnings("UnusedAssignment")
    private void displayDay(JspWriter out, String displayDay) throws IOException {
        out.println("<td><select style=\"width:215px\" size=\"1\" name=\"displayDay\" value=\"" + displayDay + "\" >");
        System.out.println("displayDay:::: " + displayDay);
        if (displayDay == null || displayDay.equals("")) { //Currently it is today
            out.println("<option selected=\"selected\" value=\"today\" >Today</option>");
            out.println("<option value=\"yesterday\" >Yesterday</option>");
            out.println("<option value=\"7days\" >7 Days</option>");
            out.println("<option value=\"30days\" >30 Days</option>");
            out.println("<option value=\"all\" >All</option>");
            displayDay = "today";
        } else if (displayDay.equals("today")) {
            out.println("<option selected=\"selected\" value=\"today\" >Today</option>");
            out.println("<option value=\"yesterday\" >Yesterday</option>");
            out.println("<option value=\"7days\" >7 Days</option>");
            out.println("<option value=\"30days\" >30 Days</option>");
            out.println("<option value=\"all\" >All</option>");
        } else if (displayDay.equals("yesterday")) {
            out.println("<option value=\"today\" >Today</option>");
            out.println("<option selected=\"selected\" value=\"yesterday\" >Yesterday</option>");
            out.println("<option value=\"7days\" >7 Days</option>");
            out.println("<option value=\"30days\" >30 Days</option>");
            out.println("<option value=\"all\" >All</option>");
        } else if (displayDay.equals("7days")) {
            out.println("<option value=\"today\" >Today</option>");
            out.println("<option value=\"yesterday\" >Yesterday</option>");
            out.println("<option selected=\"selected\" value=\"7days\" >7 Days</option>");
            out.println("<option value=\"30days\" >30 Days</option>");
            out.println("<option value=\"all\" >All</option>");
        } else if (displayDay.equals("30days")) {
            out.println("<option value=\"today\" >Today</option>");
            out.println("<option value=\"yesterday\" >Yesterday</option>");
            out.println("<option value=\"7days\" >7 Days</option>");
            out.println("<option selected=\"selected\" value=\"30days\" >30 Days</option>");
            out.println("<option value=\"all\" >All</option>");
        } else if (displayDay.equals("all")) {
            out.println("<option value=\"today\" >Today</option>");
            out.println("<option value=\"yesterday\" >Yesterday</option>");
            out.println("<option value=\"7days\" >7 Days</option>");
            out.println("<option value=\"30days\" >30 Days</option>");
            out.println("<option selected=\"selected\" value=\"all\" >All</option>");
        }
        out.println("</select> </td>");
    }

    private void displayGroup(JspWriter out, String displayGroup) throws IOException {
        out.println("<td><select style=\"width:215px\" size=\"1\" name=\"displayGroup\" value=\"" + displayGroup + "\" >");
        if (displayGroup != null && displayGroup.equals("groupType")) {
            out.println("<option value=\"lastFirst\" >Last First</option>");
            out.println("<option selected=\"selected\" value=\"groupType\" >Group Type</option>");
        } else {
            out.println("<option selected=\"selected\" value=\"lastFirst\" >Last First</option>");
            out.println("<option value=\"groupType\" >Group Type</option>");
        }
        out.println("</select> </td>");
    }

    private void displayType(JspWriter out, String displayType, List<String> list) throws IOException {
        out.println("<td><select style=\"width:215px\" size=\"1\" name=\"displayType\" value=\"" + displayType + "\" >");
        out.println("<option value=\"\" ></option>");
        for (String val : list) {
            if (displayType != null && displayType.equals(val)) {
                out.println("<option selected=\"selected\" value=\"" + val + "\" >" + val + "</option>");
            } else {
                out.println("<option value=\"" + val + "\" >" + val + "</option>");
            }
        }
        out.println("</select> </td>");
    }

    private void displayScheme(JspWriter out, String scheme) throws IOException {
        //out.println("<tr>");
        out.println("<td><select style=\"width:215px\" size=\"1\" name=\"displayScheme\" value=\"" + scheme + "\" >");

        if (scheme == null || scheme.equals("") || scheme.equals("All")) {
            out.println("<option value=\"" + "All" + "\" selected=\"selected\" >" + "All" + "</option>");
            out.println("<option value=\"" + "1" + "\" >" + "Resolution Health" + "</option>");
            out.println("<option value=\"" + "2" + "\" >" + "Spectramed" + "</option>");
        } else if (scheme.equals("1")) {
            out.println("<option value=\"" + "All" + "\" >" + "All" + "</option>");
            out.println("<option value=\"" + "1" + "\" selected=\"selected\" >" + "Resolution Health" + "</option>");
            out.println("<option value=\"" + "2" + "\" >" + "Spectramed" + "</option>");
        } else if (scheme.equals("2")) {
            out.println("<option value=\"" + "All" + "\" >" + "All" + "</option>");
            out.println("<option value=\"" + "1" + "\" >" + "Resolution Health" + "</option>");
            out.println("<option value=\"" + "2" + "\" selected=\"selected\" >" + "Spectramed" + "</option>");
        }


        out.println("</select> </td>");
    }
}

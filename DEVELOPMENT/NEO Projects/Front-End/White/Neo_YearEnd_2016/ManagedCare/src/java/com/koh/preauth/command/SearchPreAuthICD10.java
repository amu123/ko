/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.command;

import com.koh.auth.dto.ICD10SearchResult;
import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.Diagnosis;
import neo.manager.DiagnosisSearchCriteria;

/**
 *
 * @author johanl
 */
public class SearchPreAuthICD10 extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        String code = request.getParameter("code");
        String description = request.getParameter("description");
        DiagnosisSearchCriteria search = new DiagnosisSearchCriteria();
        if (code != null && !code.equals(""))
            search.setCode(code);
        if (description != null && !description.equals(""))
            search.setDescription(description);

        ArrayList<ICD10SearchResult> icd10s = new ArrayList<ICD10SearchResult>();
        List<Diagnosis> diagList = new ArrayList<Diagnosis>();
        try {

            diagList = service.getNeoManagerBeanPort().findAllDiagnosisByCriteria(search);

            for (Diagnosis diag : diagList)
            {
                ICD10SearchResult icd10 = new ICD10SearchResult();
                icd10.setCode(diag.getCode());
                icd10.setDescription(diag.getDescription());
                icd10s.add(icd10);
            }

            if (diagList.size() == 0) {
                request.setAttribute("searchICD10ResultMessage", "No results found.");
            } else if (diagList.size() >= 100) {
                request.setAttribute("searchICD10ResultMessage", "Only first 100 results shown. Please refine search.");
            } else {
                request.setAttribute("searchICD10ResultMessage", null);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        /*
        ICD10SearchResult r = new ICD10SearchResult();
        r.setCode("I11");
        r.setDescription("You are sick");

        ICD10SearchResult r2 = new ICD10SearchResult();
        r2.setCode("Z65");
        r2.setDescription("You are very sick");

        icd10s.add(r);
        icd10s.add(r2);
        */

        request.setAttribute("searchICD10Result", icd10s);
        try {
            String nextJSP = "/PreAuth/PreAuthICD10Search.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SearchPreAuthICD10";
    }

}

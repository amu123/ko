/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author whauger
 */
public class LabelCheckboxSingle extends TagSupport {
    private String javascript;
    private String displayName;
    private String value;
    private String elementName;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            if(session.getAttribute(elementName) != null)
            {
                out.println("<td><input type=\"checkbox\" checked name=\"" + elementName + "\" id=\"" + elementName + "\" value=\"" + value + "\" " + javascript + "><label class=\"label\">" + displayName + "</label></input></td>");
            }
            else
            {
                out.println("<td><input type=\"checkbox\" name=\"" + elementName + "\" id=\"" + elementName + "\" value=\"" + value + "\" " + javascript + "><label class=\"label\">" + displayName + "</label></input></td>");
            }

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelCheckboxSingle tag", ex);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

}

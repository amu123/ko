/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ClaimCptCodes;
import neo.manager.ClaimIcd10;
import neo.manager.ClaimLine;
import neo.manager.ClaimReversals;
import neo.manager.Eye;
import neo.manager.Modifier;
import neo.manager.Ndc;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author johanl
 */
public class ReverseAndCorrectClaim extends NeoCommand {

    private static final Logger logger = Logger.getLogger(ReverseAndCorrectClaim.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("Reverse And Correct Claim Called");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        PrintWriter out = null;

        Date today = new Date(System.currentTimeMillis());

        String reversalCode = request.getParameter("reversalReason");
        String reversalCodeText = port.getValueFromCodeTableForId("Claim Reversal Codes", reversalCode);
       // String reverseWholeClaim = request.getParameter("reverseWholeClaim");

        //int claimId = Integer.parseInt(request.getParameter("memClaimId"));
        ClaimLine claimLine = (ClaimLine) session.getAttribute("memCLCLDetails");
        int claimId = claimLine.getClaimId();
        String reverseAndCorrect = request.getParameter("reverseAndCorrect");

        System.out.println("reversalCode = " + reversalCode);
        System.out.println("reversalCodeText = " + reversalCodeText);
        System.out.println("claimId = " + claimId);
        System.out.println("reverseAndcorrect = " + reverseAndCorrect);

        Security _secure = new Security();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        _secure.setCreatedBy(user.getUserId());
        _secure.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        _secure.setLastUpdatedBy(user.getUserId());
        _secure.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        _secure.setSecurityGroupId(user.getSecurityGroupId());

        ClaimReversals cr = new ClaimReversals();
        cr.setReversalCode(reversalCode);
        cr.setReversalDescription(reversalCodeText);
        cr.setClaimLineId(claimLine.getClaimLineId());

        try{
            String returnMsg = "";
            String message = "";
            out = response.getWriter();
            boolean success = false;
            if(reverseAndCorrect != null && reverseAndCorrect.equalsIgnoreCase("true")){
                ClaimLine newClaimLine = populateNewClaimLine(request, port, session);
                System.out.println("newClaimLine = " + newClaimLine);
                newClaimLine.setClaimId(claimId);
                success = port.reverseAndCorrectClaimLine(claimLine, newClaimLine, cr, _secure);
                message = "Claim Line - " + claimLine.getClaimLineId();
            } else {
                success = port.reverseAndReSaveClaim(claimId, cr, _secure);
                message = "Claim - " + claimId;
            }

            System.out.println("success = " + success);
            //session.setAttribute("revError", "Failed to reverse claimId - " + claimId);
            if (!success) {
                returnMsg = "Error|Failed to reverse " + message;
            } else {
                port.processClaim(claimId, _secure);
                returnMsg = "Done|" + message +  " successfully Reversed and Reprocessed";
            }
            //session.removeAttribute("revError");
            System.out.println("returnMsg = " + returnMsg);
            out.println(returnMsg);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("[fr] error = " + e.getMessage());
        } 

//        try {
//            String nextJSP = "/NClaims/ClaimHistory.jsp";
//            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
//            dispatcher.forward(request, response);
//
//        } catch (Exception ex) {
//            logger.log(Level.SEVERE, null, ex);
//        }

        return null;
    }
    
    private ClaimLine populateNewClaimLine(HttpServletRequest request, NeoManagerBean port, HttpSession session) {
        ClaimLine cl = new ClaimLine();
        cl.setServiceDateFrom(TabUtils.getXMLDateParam(request, "ClaimLine_DateStart"));
        cl.setServiceDateTo(TabUtils.getXMLDateParam(request, "ClaimLine_DateEnd"));
        String dep = request.getParameter("ClaimLine_Dependant");
        String[] deps = dep.split("_");
        cl.setInsuredPersonEntityId(new Integer(deps[0])); //todo
        cl.setInsuredPersonDependentCode(new Integer(deps[1]));  //todo
        cl.setTimeIn(request.getParameter("ClaimLine_Time_In"));
        cl.setTimeOut(request.getParameter("ClaimLine_Time_Out"));
        cl.setTarrifCodeNo(request.getParameter("ClaimLine_Tariff"));
        cl.setMultiplier(TabUtils.getIntParam(request, "ClaimLine_Multiplier"));
        cl.setUnits(TabUtils.getBDParam(request, "ClaimLine_Units").doubleValue());
        cl.setQuantity(TabUtils.getIntParam(request, "ClaimLine_Quantity"));
        cl.setLabInvoiceNumber(request.getParameter("ClaimLine_Invoice_Number"));
        cl.setLabOrderNumber(request.getParameter("ClaimLine_Order_Number"));
        cl.setClaimedAmount(TabUtils.getBDParam(request, "ClaimLine_Claimed_Amount").doubleValue());
        cl.setClaimLineStatusId(1);
        cl.setChequeRunID(0);
        cl.setChequeRunDate(null);
        cl.setProsessDate(null);
        Collection<String> authNumbers = cl.getAuthNumber();
        String authNumber = request.getParameter("ClaimLine_Authorization");
        if (authNumber != null && !"".equalsIgnoreCase(authNumber)) {
            authNumbers.add(authNumber);
            //cl.getAuthNumber().add(authNumber);
        }
        Collection<Integer> toothNumbers = cl.getToothNumber();
        //String toothNumber = request.getParameter("ClaimLine_Tooth_Numbers");
        String toothList = request.getParameter("toothList");
        System.out.println("ToothList = " + toothList);
        if (toothList != null && !"".equals(toothList)) {
            if(toothList.contains(",")){
                toothList = toothList.replace(",", " ");
            }
            String[] sArr = toothList.split(" ");
            for (String s : sArr) {
                toothNumbers.add(new Integer(s));
                //cl.getToothNumber().add(new Integer(s));
            }
        }
        Collection<ClaimCptCodes> cptCodes = cl.getCptCodes();
        //String cptCode = request.getParameter("ClaimLine_CPT");
        String cptList = request.getParameter("cptList");
        System.out.println("cptList = " + cptList);
        if (cptList != null && !"".equals(cptList)) {
            if(cptList.contains(",")){
                    cptList = cptList.replace(",", " ");
                }
            String[] sArr = cptList.split(" ");
            for (String s : sArr) {
                ClaimCptCodes ccptc = new ClaimCptCodes();
                ccptc.setCptCode(s);
                cptCodes.add(ccptc);
                //cl.getCptCodes().add(ccptc);
            }
        }
        Collection<ClaimIcd10> icd10 = cl.getIcd10();
        //String icdCode = request.getParameter("ClaimLine_Diagnosis");
        String icdList = request.getParameter("icdList");
        System.out.println("icdList = " + icdList);
        if (icdList != null && !"".equals(icdList)) {
            if(icdList.contains(",")){
                    icdList = icdList.replace(",", " ");
                }
            String[] sArr = icdList.split(" ");
            for (String s : sArr) {
                ClaimIcd10 cicd = new ClaimIcd10();
                cicd.setIcd10Code(s.toUpperCase());
                cicd.setReferingIcd10(0);
                icd10.add(cicd);
                //cl.getIcd10().add(cicd);
            }
        }
//        icdCode = request.getParameter("ClaimLine_Ref_Diagnosis");
        icdList = request.getParameter("icdRefList");
        System.out.println("icdRefList = " + icdList);
        if (icdList != null && !"".equals(icdList)) {
            if(icdList.contains(",")){
                    icdList = icdList.replace(",", " ");
                }
            String[] sArr = icdList.split(" ");
            for (String s : sArr) {
                ClaimIcd10 cicd = new ClaimIcd10();
                cicd.setIcd10Code(s.toUpperCase());
                cicd.setReferingIcd10(1);
                icd10.add(cicd);
                //cl.getIcd10().add(cicd);
            }
        }
        Collection<Eye> eyes = cl.getEyes();
        String lensLeft = request.getParameter("ClaimLine_Lens_Left");
        if (lensLeft != null && !"".equalsIgnoreCase(lensLeft)) {
            Eye eye = new Eye();
            eye.setEyeLR("L");
            eye.setLensesRX(lensLeft);
            eyes.add(eye);
            //cl.getEyes().add(eye);
        }
        String lensRight = request.getParameter("ClaimLine_Lens_Left");
        if (lensRight != null && !"".equalsIgnoreCase(lensRight)) {
            Eye eye = new Eye();
            eye.setEyeLR("R");
            eye.setLensesRX(lensRight);
            eyes.add(eye);
            //cl.getEyes().add(eye);
        }
        
        Collection<Ndc> ndcs = cl.getNdc();
        List<Ndc> clNdcList = (List<Ndc>) session.getAttribute("claimLineNdcList");

        if(clNdcList != null && !clNdcList.isEmpty()){
            System.out.println("clNdcList size = "  + clNdcList.size());
            for(Ndc nc : clNdcList){
                ndcs.add(nc);
                //cl.getNdc().add(nc);
            }
        }
        Collection<Modifier> modifier = cl.getModifiers();
        List<Modifier> modList = (List<Modifier>) session.getAttribute("modList");
        
        if(modList != null && !modList.isEmpty()){
            System.out.println("modList size = " + modList.size());
            for(Modifier mod : modList){
                modifier.add(mod);
            }
        }
        
        session.setAttribute("claimLineNdcList",null);
        session.setAttribute("NdcLine",null);
        session.setAttribute("modList",null);

        int claimID = TabUtils.getIntParam(request, "ClaimLine_claimId");
        System.out.println("ClaimID = " + claimID);
        //cl.setClaimLineId(port.saveClaimLine(cl, claimID, security, true));
        return cl;
    }

    @Override
    public String getName() {
        return "ReverseAndCorrectClaim";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.KeyValue;
import neo.manager.NeoManagerBean;

/**
 *
 * @author shimanem
 */
public class ViewDocumentsCommand extends NeoCommand {

    private static final Logger LOG = Logger.getLogger(ViewDocumentsCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {

            HttpSession session = request.getSession();
//            PrintWriter writer = response.getWriter();

            System.out.println("SERVER >>> Entered: Doucument Search/View TAB");
            NeoManagerBean port = service.getNeoManagerBeanPort();
            List<KeyValue> docGenCDLMap = port.getPDCChronicConditions();
            List<KeyValue> docGenOtherCDLMap = port.getPDCOtherChronicConditions();

            session.setAttribute("documentGenerationMap", docGenCDLMap);
            session.setAttribute("documentOtherCDLMap", docGenOtherCDLMap);

            String email = (String) session.getAttribute("email");
            System.out.println("Email >> " + email);
            String view = "/PDC/PDCDocumentView.jsp";
            RequestDispatcher dispatch = context.getRequestDispatcher(view);
            dispatch.forward(request, response);

        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "ViewDocumentsCommand";
    }
}

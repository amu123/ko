/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class LabelTextBoxDateReq extends TagSupport {

    private String elementName;
    private String displayname;
    private String valueFromSession = "No";
    private String javascript;
    private String mandatory = "no";

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        String clientVersion = (String) session.getAttribute("Client");
       
        try {
            out.println("<td align=\"left\" width=\"160px\"><label class=\"label\" for=\"" + elementName + "\">" + displayname + ":</label></td><td align=\"left\" width=\"200px\"> <input name=\"" + elementName + "\" id=\"" + elementName + "\" " + javascript + " ");
            if (valueFromSession.equalsIgnoreCase("Yes")) {
                String sessionVal = "";
                if (pageContext.getRequest().getAttribute(elementName) != null) {
                    sessionVal = "" + pageContext.getRequest().getAttribute(elementName);
                }
                out.println("value=\"" + sessionVal + "\" size=\"30\"/></td>");
            } else {
                out.println("size=\"30\"/></td>");
            }
            
            if (clientVersion != null && !clientVersion.equalsIgnoreCase("null") && clientVersion.equalsIgnoreCase("Sechaba")) {
                if (elementName.equalsIgnoreCase("MemberApp_brokerCertifiedDate")) {
                    out.print("<td></td>");
                } else {
                    if (mandatory.equalsIgnoreCase("yes")) {
                        out.print("<td><label class=\"red\">*</label></td>");
                    } else {
                        out.print("<td></td>");
                    }
                }
            } else {
                if (mandatory.equalsIgnoreCase("yes")) {
                    out.print("<td><label class=\"red\">*</label></td>");
                } else {
                    out.print("<td></td>");
                }
            }
            
            out.println("<td align=\"left\" width=\"30\"><img src=\"/ManagedCare/resources/Calendar.gif\" width=\"28\" height=\"28\" alt=\"Calendar\" onclick=\"displayDatePicker('" + elementName + "', this);\"/></td>");
            out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");

        } catch (java.io.IOException ex) {
            throw new JspException("Error in labelTextBoxDate tag", ex);
        }
        return super.doEndTag();
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public void setValueFromSession(String valueFromSession) {
        this.valueFromSession = valueFromSession;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.contribution.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.serv.PropertiesReader;
import com.koh.utils.DateTimeUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoManagerBean;

/**
 *
 * @author yuganp
 */
public class GenerateContributionCommand extends NeoCommand {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd");
   
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            String s = request.getParameter("command");
            if (s != null && !s.isEmpty()) {
                System.out.println("command : " + s);
                if (s.equalsIgnoreCase("genContrib")) {
                    generateContributions(request, response, context);
                } else if (s.equalsIgnoreCase("genInvoice")) {
                    generateInvoices(request, response, context);
                } else if (s.equalsIgnoreCase("genStatement")) {
                    generateStatements(request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(PaymentAllocationCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void generateContributions(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        try {
            String name = request.getParameter("Contribution_Date");
            int id = 0;
            javax.xml.datatype.XMLGregorianCalendar date = DateTimeUtils.convertDateToXMLGregorianCalendar(DATE_FORMAT.parse(name));
            int x = port.generateContributions(date, null, null, null, TabUtils.getSecurity(request));
            String   status = "OK";
            String   additionalData = "\"message\":\"Contributions has " + (x == 1 ? "" : "not") +  " been generated " + "\"";
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, null, null, additionalData));
        } catch (Exception ex) {
            Logger.getLogger(GenerateContributionCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    private void generateInvoices(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        try {
            String name = request.getParameter("Invoice_Date");
            int id = 0;
            javax.xml.datatype.XMLGregorianCalendar date = DateTimeUtils.convertDateToXMLGregorianCalendar(DATE_FORMAT.parse(name));
            boolean x = port.generateContributionInvoices(date, TabUtils.getSecurity(request));
            String   status = "OK";
            String   additionalData = "\"message\":\"Invoices generated " + x + "\"";
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, null, null, additionalData));
        } catch (Exception ex) {
            Logger.getLogger(GenerateContributionCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void generateStatements(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            String parameter = request.getParameter("Statement_Date");
            if (parameter == null) parameter = "";
            Date date = TabUtils.getDateParam(request, "Statement_Date");
            if (date == null) {
                date = new Date();
            }
            printStatementHeader(out, parameter, context.getContextPath());
            generateContributionsForBillingRun(out, date, request);
            String command = new PropertiesReader().getProperty("GroupBillingStatementCommand");
            ProcessBuilder pb = new ProcessBuilder(command, parameter);
            Map<String, String> env = pb.environment();
//            if (env != null) {
//                for (String s : env.keySet()) {
//                    System.out.println(s + " = " + env.get(s));
//                }
//            } else {
//                System.out.println("Null Environment");
//            }
            File f = new File(command);
            String absolutePath =f.getAbsolutePath(); 
            pb.directory(new File(absolutePath.substring(0,absolutePath.lastIndexOf(File.separator))));
            Process p = pb.start();
            System.out.println("Process Started");
            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String resultLine = in.readLine();
            while (resultLine != null) {
//                System.out.println(resultLine);
                printStatementLine(out, resultLine);
                resultLine = in.readLine();
            }
            printStatementFooter(out, p.exitValue());
            p.destroy();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
    
    private void generateContributionsForBillingRun(PrintWriter out, Date date, HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.set(Calendar.DAY_OF_MONTH, 1);  
            date = cal.getTime();
            printStatementLine(out, "Checking Contributions for " + DATE_FORMAT.format(date));
            int updateCount = port.generateContributions(DateTimeUtils.convertDateToXMLGregorianCalendar(date), null, null, null, TabUtils.getSecurity(request));
            if (updateCount == 0) {
                printStatementLine(out, "No contribution changes for " + DATE_FORMAT.format(date));
            } else {
                printStatementLine(out, "" + updateCount + " contribution changes were made for "+ DATE_FORMAT.format(date));
            }
            cal.add(Calendar.MONTH, 1);
            date = cal.getTime();
            printStatementLine(out, "Checking Contributions for " + DATE_FORMAT.format(date));
            updateCount = port.generateContributions(DateTimeUtils.convertDateToXMLGregorianCalendar(date), null, null, null, TabUtils.getSecurity(request));
            if (updateCount == 0) {
                printStatementLine(out, "No contribution changes for " + DATE_FORMAT.format(date));
            } else {
                printStatementLine(out, "" + updateCount + " contribution changes were made for "+ DATE_FORMAT.format(date));
            }
    }
    
    private void printStatementHeader(PrintWriter out, String date, String contextPath) {
        out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"><html>");
        out.println("<head><link rel=\"stylesheet\" href=\"" + contextPath + "/resources/styles.css\"/></head><body>");
        out.println("<label class=\"header\">Generating Group Billing Statements</label><center><label class=\"subheader\">Statement Generation output</label></center><div style='height: 400px; border: .2em solid #900; overflow: auto; '>");
        out.println("Statement Generation Started<br>");
        out.flush();
    }
    
    private void printStatementFooter(PrintWriter out, int exitValue) {
        out.println("</div><p>");
        if (exitValue == 0) {
             out.print("Statement generation has finished.");
        } else {
             out.print("An error occured during the generation of the statements.");
        }
        out.println("</p><button type='button' onclick='window.location=\"/ManagedCare/Security/Welcome.jsp\"; '>Close</button>");
        out.println("<button type='button' onclick='window.history.back();'>Return</button>");
        out.println("</html> ");
        out.flush();
    }
    
    private void printStatementLine(PrintWriter out, String line) {
        out.println("<label>"+line + "</label><br>");
        out.flush();
    }

    @Override
    public String getName() {
        return "GenerateContributionCommand";
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.math.BigDecimal;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthNappiProduct;
/**
 *
 * @author martins
 */
public class AuthProductSearchResultTag extends TagSupport {

    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        
        try {

            out.println("<td colspan=\"4\"><table width=\"800\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            List<AuthNappiProduct> productList = (List<AuthNappiProduct>) session.getAttribute("nappiList");
            if (productList != null) {
                out.println("<tr><th width=\"100\">Product Code</th><th width=\"450\">Product Discription</th><th width=\"40\">Package Size</th><th width=\"40\">Unit Price</th><th width=\"40\">Package Price</th><th width=\"30\"></th></tr>");
                for (int i =1; i <= productList.size(); i++){
                    AuthNappiProduct anp = productList.get(i-1);

                    double n = anp.getUnitPrice();
                    BigDecimal rounded = new BigDecimal(n);
                    rounded = rounded.setScale(2, BigDecimal.ROUND_HALF_UP);
                    out.println("<tr id=\"prodRow"+ anp.getNappiCode() + i +"\"><form>");
                    out.println("<td width=\"100\"><label class=\"label\">" + anp.getNappiCode() + "</label><input type=\"hidden\" name=\"prodCode\" id=\"prodCode\" value=\"" + anp.getNappiCode() + "\"/></td>");
                    out.println("<td width=\"450\"><label class=\"label\">" + anp.getProductDesc() + "</label><input type=\"hidden\" name=\"prodDesc\" id=\"prodDesc\" value=\"" + anp.getProductDesc() + "\"/></td>");
                    out.println("<td width=\"40\"><label class=\"label\">" + anp.getPackageSize() + "</label><input type=\"hidden\" name=\"prodQuan\" id=\"prodQuan\" value=\"" + anp.getQuantity() + "\"/></td>");
                    out.println("<td width=\"40\"><label class=\"label\">" +"R"+ rounded + "</label><input type=\"hidden\" name=\"prodRand\" id=\"prodRand\" value=\"" + anp.getUnitPrice()+ "\"/></td>");
                    out.println("<td width=\"40\"><label class=\"label\">" +"R"+ anp.getFixedPrice() + "</label><input type=\"hidden\" name=\"prodRand\" id=\"prodRand\" value=\"" + anp.getFixedPrice() + "\"/></td>");
                    out.println("<td width=\"30\"><button name=\"select\" type=\"button\" onClick=\"SelectProdList("+ i +");\" value=\"Select\">Select</button></td>");
                    out.println("</form></tr>");
                }
            }
            out.println("</table></td>");
            
            

        } catch (java.io.IOException ex) {
            throw new JspException("Error in NappiProductSearchTable tag", ex);
        }

        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}
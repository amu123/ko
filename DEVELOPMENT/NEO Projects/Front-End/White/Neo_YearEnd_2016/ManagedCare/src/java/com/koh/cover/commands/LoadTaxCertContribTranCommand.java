/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ContributionTransactionHistoryView;
import neo.manager.CoverProductDetails;
import neo.manager.FileData;
import neo.manager.NeoManagerBean;

/**
 *
 * @author almaries
 */
public class LoadTaxCertContribTranCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("LoadContribTranCommand : " + request.getParameter("buttonPressedExport"));

        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        String coverNum = (String) session.getAttribute("coverNum");
        Integer entityId = (Integer) session.getAttribute("memberCoverEntityId");
        Date st = TabUtils.getDateParam(request, "contrib_start_date");
        Date en = TabUtils.getDateParam(request, "contrib_end_date");
        Date xst = TabUtils.getDateParam(request, "exp_start_date");
        Date xen = TabUtils.getDateParam(request, "exp_end_date");
        Date ad = TabUtils.getDateParam(request, "contrib_ad_date");
        try {
            if (request.getParameter("buttonPressedExport") != null && request.getParameter("buttonPressedExport").toString().equals("Export") && !request.getParameter("buttonPressedExport").toString().isEmpty()) {
                Date endDate = new Date();
                Calendar cal = Calendar.getInstance();
                Date startDate;
                if (xst == null) {
                    cal.setTime(endDate);
                    cal.add(Calendar.MONTH, -6);
                    startDate = cal.getTime();
                } else {
                    startDate = xst;
                }
                if (xen == null) {
                    cal.setTime(endDate);
                    cal.add(Calendar.MONTH, 1);
                    endDate = cal.getTime();
                } else {
                    endDate = xen;
                }
                request.setAttribute("exp_end_date", DateTimeUtils.convertToYYYYMMDD(endDate));
                request.setAttribute("exp_start_date", DateTimeUtils.convertToYYYYMMDD(startDate));
                CoverProductDetails coverDetails = port.getProductDetailsDespiteCoverStatus(coverNum);
                FileData employerGroupBillingStatement = port.getTaxCertContributionStatement(coverDetails, DateTimeUtils.convertDateToXMLGregorianCalendar(xst), DateTimeUtils.convertDateToXMLGregorianCalendar(xen), "XLS");
                if (employerGroupBillingStatement == null || employerGroupBillingStatement.getData() == null) {
                } else {
                    byte[] data = employerGroupBillingStatement.getData();
                    ServletOutputStream os = response.getOutputStream();
                    String mimetype = context.getMimeType(employerGroupBillingStatement.getFileName());
                    response.setContentType((mimetype != null) ? mimetype : "application/octet-stream");
                    response.setContentLength(data.length);
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + employerGroupBillingStatement.getFileName() + "\"");
                    os.write(data);
                    os.flush();
                    os.close();

                }
            } else {
                Date endDate = new Date();
                Calendar cal = Calendar.getInstance();
                Date startDate;
                if (st == null) {
                    cal.setTime(endDate);
                    cal.add(Calendar.MONTH, -6);
                    startDate = cal.getTime();
                } else {
                    startDate = st;
                }
                if (en == null) {
                    cal.setTime(endDate);
                    cal.add(Calendar.MONTH, 1);
                    endDate = cal.getTime();
                } else {
                    endDate = en;
                }
                if (ad == null) {
                    ad = new Date();
                }
                List<ContributionTransactionHistoryView> memberContributionsHistory = port.fetchMemberContributionsHistory(coverNum, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), DateTimeUtils.convertDateToXMLGregorianCalendar(endDate), DateTimeUtils.convertDateToXMLGregorianCalendar(ad));
//            List<ContributionReceipts> unallocatedReceipts = port.fetchUnallocatedReceiptsByEntityId(entityId);
//            ContributionAge memberAgeAnalysis = port.getMemberAgeAnalysis(coverNum);
                request.setAttribute("Contributions", memberContributionsHistory);
//            request.setAttribute("Receipts", unallocatedReceipts);
//            request.setAttribute("memberAgeAnalysis", memberAgeAnalysis);
                request.setAttribute("contrib_end_date", DateTimeUtils.convertToYYYYMMDD(endDate));
                request.setAttribute("contrib_start_date", DateTimeUtils.convertToYYYYMMDD(startDate));
                request.setAttribute("contrib_ad_date", DateTimeUtils.convertToYYYYMMDD(ad));
                RequestDispatcher dispatcher = context.getRequestDispatcher("/Member/MemberContributionDetails.jsp");
                dispatcher.forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "LoadTaxCertContribTranCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author johanl
 */
public class CalculateOrthoPlanDate extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        int duration = Integer.parseInt(request.getParameter("duration"));
        String dateFrom = request.getParameter("authfrom");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date aDate = null;
        String futureDate = "";

        try {
            PrintWriter out = response.getWriter();
            try {
                aDate = sdf.parse(dateFrom);
                //XMLGregorianCalendar xDate = DateTimeUtils.convertDateToXMLGregorianCalendar(aDate);
                //fDate = service.getNeoManagerBeanPort().getOrthoPlannedDate(xDate, duration);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }

            if (aDate != null) {
                Calendar c = Calendar.getInstance();
                c.setTime(aDate);
                c.add(Calendar.MONTH, duration);
                
                Date furDate = c.getTime();
                futureDate = sdf.format(furDate);
                System.out.println("future date = " + futureDate);
                session.setAttribute("authToDate", futureDate);

                out.println("Done|" + futureDate);
            }else{
                out.println("Error|Incorrect From Date "+dateFrom);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "CalculateOrthoPlanDate";
    }
}

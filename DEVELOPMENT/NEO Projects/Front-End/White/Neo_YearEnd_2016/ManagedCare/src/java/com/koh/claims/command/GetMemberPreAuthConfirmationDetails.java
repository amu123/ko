/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import com.koh.preauth.dto.AuthConfirmationDetails;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthNoteDetail;
import neo.manager.AuthOrthoPlanDetails;
import neo.manager.AuthTariffDetails;
import neo.manager.Diagnosis;
import neo.manager.LabTariffDetails;
import neo.manager.NeoManagerBean;
import neo.manager.PreAuthConfirmationDetails;
import neo.manager.PreAuthDetails;
import neo.manager.PreAuthSearchCriteria;
import neo.manager.ProviderSearchDetails;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class GetMemberPreAuthConfirmationDetails extends NeoCommand {

    private Logger log = Logger.getLogger(GetMemberPreAuthConfirmationDetails.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        log.info("Inside GetMemberPreAuthConfirmationDetails");


        HttpSession session = request.getSession();

        NeoManagerBean port = service.getNeoManagerBeanPort();

        AuthConfirmationDetails authConfirm = new AuthConfirmationDetails();
        List<PreAuthDetails> paList = null;
        PreAuthConfirmationDetails preCon = null;
        boolean confirmation = true;

        String authTypeDesc = "";
        String authNumber = "" + session.getAttribute("authNumber");
        int authId = Integer.parseInt(session.getAttribute("authCode").toString());

        if (authId != 0 && !authNumber.trim().equals("")) {
            authConfirm.setAuthId(authId);
            authConfirm.setAuthNumber(authNumber);

            PreAuthSearchCriteria pas = new PreAuthSearchCriteria();
            pas.setAuthId(authId);
            pas.setAuthNumber(authNumber);
            pas.setDependantCode(-1);

            paList = port.findAllPreAuthDetailsByCriteria(pas);
            preCon = port.getAuthConfirmationDetails(authId, authNumber);

            if (paList != null && preCon != null) {
                authConfirm.setAuthConfirmation(preCon);
                authConfirm.setAuthDetails(paList);

            }
        }

        if (authConfirm.getAuthDetails() != null) {
            session.setAttribute("PreAuthConfirmation", authConfirm);

            session.setAttribute("treatingProvider_text", preCon.getProviderNumber());
            session.setAttribute("treatingProviderTel", preCon.getProviderTel());
            session.setAttribute("treatingProviderName", preCon.getProviderName());
            session.setAttribute("treatingProviderFax", preCon.getProviderFax());

            session.setAttribute("memberName", preCon.getCoverName());
            session.setAttribute("memberSurname", preCon.getCoverSurname());

            //dob check
            String dob = "";
            if (preCon.getDateOfBirth() != null) {
                dob = new SimpleDateFormat("yyyy/MM/dd").format(preCon.getDateOfBirth().toGregorianCalendar().getTime());
            }

            session.setAttribute("memberDOB", dob);
            session.setAttribute("memberIdNum", preCon.getCoverIdNumber());
            session.setAttribute("memberAge", preCon.getCoverAge());
            session.setAttribute("memberGender", preCon.getCoverGender());
            session.setAttribute("schemeName", preCon.getScheme());
            session.setAttribute("schemeOptionName", preCon.getSchemeOption());
            session.setAttribute("memberContact", preCon.getCoverContact());
            session.setAttribute("memberStatus", preCon.getCoverStatus());

            //gerenric auth fields
            PreAuthDetails pa = paList.get(0);
            String authType = pa.getAuthType();
            String authDisclaimer = "";

            authTypeDesc = port.getValueFromCodeTableForTableId(89, authType);

            //set auth detail provider names
            String labProvNum = pa.getLabProviderNo();
            String facProvNum = pa.getFacilityProviderNo();
            String anaeProvNum = pa.getAnaesthetistProviderNo();


            if (labProvNum != null && !labProvNum.equalsIgnoreCase("")) {
                ProviderSearchDetails pd = service.getNeoManagerBeanPort().findPracticeAjaxDetailsByNumber(labProvNum);
                session.setAttribute("labProvDesc", pd.getProviderName());
            }
            if (facProvNum != null && !facProvNum.equalsIgnoreCase("")) {
                ProviderSearchDetails pd = service.getNeoManagerBeanPort().findPracticeAjaxDetailsByNumber(facProvNum);
                session.setAttribute("facProvDesc", pd.getProviderName());
            }
            if (anaeProvNum != null && !anaeProvNum.equalsIgnoreCase("")) {
                ProviderSearchDetails pd = service.getNeoManagerBeanPort().findPracticeAjaxDetailsByNumber(anaeProvNum);
                session.setAttribute("anaesthetistProvDesc", pd.getProviderName());
            }



            String aDate = new SimpleDateFormat("yyyy/MM/dd").format(pa.getAuthorisationDate().toGregorianCalendar().getTime());
            XMLGregorianCalendar authStart = pa.getAuthStartDate();
            XMLGregorianCalendar authEnd = pa.getAuthEndDate();
            String aFrom = "";
            String aTo = "";
            if (authStart != null) {
                aFrom = new SimpleDateFormat("yyyy/MM/dd").format(pa.getAuthStartDate().toGregorianCalendar().getTime());
            }
            if (authEnd != null) {
                aTo = new SimpleDateFormat("yyyy/MM/dd").format(pa.getAuthEndDate().toGregorianCalendar().getTime());
            }

            session.setAttribute("memberNum_text", pa.getCoverNumber());
            session.setAttribute("depListValues", pa.getDependantCode());
            session.setAttribute("authDate", aDate);
            session.setAttribute("authFrom", aFrom);
            session.setAttribute("authTo", aTo);

            String aStatus = port.getValueFromCodeTableForId("Auth Status", pa.getAuthStatus());
            if (pa.getAuthStatus().trim().equals("2") || pa.getAuthStatus().trim().equals("3")) {
                session.setAttribute("authNo", "");
            } else {
                session.setAttribute("authNo", pa.getAuthNumber());
            }
            session.setAttribute("authStatus", aStatus);
            //session.setAttribute("notes", pa.getNotes()); old notes
            //set new notes list
            List<AuthNoteDetail> notes = pa.getAuthNotes();
            List<AuthNoteDetail> externalNotes = new ArrayList<AuthNoteDetail>();
            for (AuthNoteDetail an : notes) {
                boolean addExternal = false;
                if (session.getAttribute("ccPreAuthOnScreen") != null) {
                    addExternal = true;
                }

                if (addExternal) {
                    externalNotes.add(an);
                } else {
                    if (an.getNoteType().getId().equals("2")) {
                        externalNotes.add(an);
                    }
                }

            }
            session.setAttribute("authNoteList", externalNotes);

            if (!authType.trim().equals("4") && !authType.trim().equals("5")) {
                if (authType.trim().equalsIgnoreCase("6")) {
                    session.setAttribute("newNappiListArray", pa.getAuthTariffs());
                } else {
                    session.setAttribute("tariffListArray", pa.getAuthTariffs());
                }
            }
            session.setAttribute("aICD", pa.getAdmissionICD());
            session.setAttribute("pICD", pa.getPrimaryICD());
            //set ocd descriptions
            Diagnosis d1 = service.getNeoManagerBeanPort().getDiagnosisForCode(pa.getAdmissionICD());
            Diagnosis d2 = service.getNeoManagerBeanPort().getDiagnosisForCode(pa.getPrimaryICD());
            if (d1 != null) {
                session.setAttribute("aICDDesc", d1.getDescription());
            }
            if (d2 != null) {
                session.setAttribute("pICDDesc", d2.getDescription());
            }

            //set type specific session variables secICDDesc coMorICDDesc
            if (authType.trim().equals("1") || authType.trim().equals("4")) {
                Diagnosis d3 = service.getNeoManagerBeanPort().getDiagnosisForCode(pa.getSecondaryICD());
                Diagnosis d4 = service.getNeoManagerBeanPort().getDiagnosisForCode(pa.getCoMorbidityICD());
                if (d3 != null) {
                    session.setAttribute("secICDDesc", d3.getDescription());
                }
                if (d4 != null) {
                    session.setAttribute("coMorICDDesc", d4.getDescription());
                }
            }

            if (authType.trim().equals("1")) {
                session.setAttribute("facProv", pa.getFacilityProviderNo());
                session.setAttribute("secICD", pa.getSecondaryICD());
                session.setAttribute("coMorICD", pa.getCoMorbidityICD());
                session.setAttribute("preLensRx", pa.getPreviousLensRx());
                session.setAttribute("curLensRx", pa.getCurrentLensRx());
                session.setAttribute("numDays", pa.getNumberOfDays());

                //authDisclaimer = "Optical Auth is Subject to Available Benefits";

            } else if (authType.trim().equals("2") || authType.trim().equals("13")) {
                String subType = port.getValueFromCodeTableForId("Dental Sub Type", pa.getAuthSubType());

                session.setAttribute("dentalSubType", subType);
                List<LabTariffDetails> savedLabList = pa.getAuthLabTariffs();
                List<LabTariffDetails> labList = new ArrayList<LabTariffDetails>();
                List<LabTariffDetails> noLabList = new ArrayList<LabTariffDetails>();
                for (LabTariffDetails labs : savedLabList) {
                    if (labs.getLabCodeInd().trim().equals("1")) {
                        labList.add(labs);
                    } else if (labs.getLabCodeInd().trim().equals("0")) {
                        noLabList.add(labs);
                    }
                }
                session.setAttribute("labProv", pa.getLabProviderNo());
                session.setAttribute("amountClaimed", pa.getAmountClaimed());

                session.setAttribute("labTariffs", labList);
                session.setAttribute("SavedDentalNoLabCodeList", noLabList);

                //authDisclaimer = "Dental Auth is Subject to Available Benefits";

            } else if (authType.trim().equals("3")) {
                AuthOrthoPlanDetails ort = pa.getOrthoPlanDisplay();

                session.setAttribute("planDuration", ort.getDuration());
                session.setAttribute("amountClaimed", ort.getTotalAmount());
                session.setAttribute("deposit", ort.getDeposit());
                session.setAttribute("firstInstall", ort.getFirstInstallment());
                session.setAttribute("remainInstall", ort.getRemainingInstallment());
                session.setAttribute("planFrom", new SimpleDateFormat("yyyy/MM/dd").format(ort.getPlanStartDate().toGregorianCalendar().getTime()));
                session.setAttribute("planEstimateEnd", new SimpleDateFormat("yyyy/MM/dd").format(ort.getPlanEstimateEndDate().toGregorianCalendar().getTime()));

                //authDisclaimer = "Orthodontic Auth is Subject to Available Benefits";

            } else if (authType.trim().equals("4")) {

                session.setAttribute("tariffListArray", pa.getAuthTariffs());

                session.setAttribute("cptList", pa.getAuthCPTDetails());

                session.setAttribute("locList", pa.getHospitalLOCDetails());
                session.setAttribute("ruleList", pa.getAuthRuleResponse());

                session.setAttribute("secICD", pa.getSecondaryICD());
                session.setAttribute("coMorICD", pa.getCoMorbidityICD());
                //session.setAttribute("theatreTime", pa.getTheatreTime());
                session.setAttribute("amendedDate", new SimpleDateFormat("yyyy/MM/dd").format(pa.getAmendedDate().toGregorianCalendar().getTime()));
                session.setAttribute("facilityProv", pa.getFacilityProviderNo());
                session.setAttribute("anaesthetistProv", pa.getAnaesthetistProviderNo());

                //set lookups
                String reimburse = port.getValueFromCodeTableForId("Reimbursement Model", pa.getReinbursementModel());
                String pmb = port.getValueFromCodeTableForId("YesNo Type", pa.getPmb());

                String funder = port.getValueFromCodeTableForId("Funder Details", pa.getFunderDetails());
                session.setAttribute("reimbursementModel", reimburse);
                session.setAttribute("pmb", pa.getPmb());
                session.setAttribute("pmb_display", pmb);
                session.setAttribute("coPay", pa.getCoPaymentAmount());
                session.setAttribute("funder", funder);

                //authDisclaimer = "Hospital Auth is Subject to Available Benefits";

            } else if (authType.trim().equals("5")) {
                System.out.println("The Auth number is ******** " + authNumber);
                if (authNumber != null && !authNumber.trim().isEmpty()) {
                    List<AuthTariffDetails> medList = port.findPreAuthMedicines(authNumber);
                    System.out.println("The nappi list size is " + medList.size());
                    session.setAttribute("AuthNappiTariffs", medList);
                    List<AuthTariffDetails> tarrifList = port.findPreAuthTarrifs(authNumber);
                    System.out.println("The tarriff list size is " + tarrifList.size());
                    session.setAttribute("AuthBasketTariffs", tarrifList);
                    //authDisclaimer = "Condition Specific Auth is Subject to Available Benefits";
                }
            } else if (authType.trim().equals("6")) {
                //authDisclaimer = "Nappi Auth is Subject to Available Benefits \\n";
            } else if (authType.trim().equals("7")) {
                //authDisclaimer = "Tariff Auth is Subject to Available Benefits";
            } else if (authType.trim().equals("8")) {
                //authDisclaimer = "Above Benefit Auth is Subject to Available Benefits";
            } else if (authType.trim().equals("9")) {
                session.setAttribute("ruleList", pa.getAuthRuleResponse());
            } else if (authType.trim().equals("10")) {
                session.setAttribute("ruleList", pa.getAuthRuleResponse());
            }

            authDisclaimer = authTypeDesc + " Auth is Subject to Available Benefits <br /><br /> " + "*Authorisation is given only for the stated procedure and or reason for admission. <br /> " + "Any amendments need to be pre-authorised. This authorisation is not a guarantee of <br /> " + "payment and is given based on the information available at the time and is subject <br /> " + "to the validity of the membership, scheme rules, available benefits and scheme tariff schedules.";

            session.setAttribute("authDisclaimer", authDisclaimer);

        } else {
            System.out.println("pre auth details not set");
            confirmation = false;
        }

        //set preauth onscreen for confirmation page
        session.setAttribute("ccAuthOnScreen", "/Claims/MemberPreAuthorisationDetails.jsp");

        if (confirmation == false) {
            //print error 
            try {
                PrintWriter out = response.getWriter();
                out.println("<html>");
                out.println("<head>");
                out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                out.println("</head>");
                out.println("<body>");
                out.println("<center>");
                out.println("<table cellspacing=\"4\">");
                out.println("<tr>");
                out.println("<td><label class=\"header\">An Error Occured on this Authorisation</label></td>");
                out.println("</tr>");
                out.println("</table>");
                out.println("<p> </p>");
                out.println("</center>");
                out.println("</body>");
                out.println("</html>");

                response.setHeader("Refresh", "3; URL=/ManagedCare/Claims/MemberPreAuthorisationDetails.jsp");

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } else {
            response.setHeader("Refresh", "1; URL=/ManagedCare/Claims/PreAuthCallCenterConfirmation.jsp");
        }

        return null;
    }

    @Override
    public String getName() {

        return "GetMemberPreAuthConfirmationDetails";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.agile.security.webservice.EmailAttachment;
import com.agile.security.webservice.EmailContent;
import com.koh.brokerFirm.BrokerMapping;
import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.cover.MemberMainMapping;
import com.koh.employer.command.TabUtils;
import com.koh.fe.command.LoginCommand;
import com.koh.serv.PropertiesReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;
import org.apache.log4j.Logger;

/**
 *
 * @author princes
 */
public class SaveCallCenterContactDetails extends NeoCommand {

    private Logger logger = Logger.getLogger(SaveCallCenterContactDetails.class);
    NeoManagerBean port = service.getNeoManagerBeanPort();
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        logger.info("Inside SaveCallCenterContactDetails");
        HttpSession session = request.getSession();
        List<AddressDetails> ad = new ArrayList<AddressDetails>();
        List<ContactDetails> cd = new ArrayList<ContactDetails>();
        int entity = (Integer) session.getAttribute("coverEntityId");

        System.out.println("entity " + entity);

        AddressDetails physical = new AddressDetails();
        physical.setAddressType("Physical");
        String memberPhysicalAddressLine1 = (String) request.getParameter("memberPhysicalAddressLine1");
        String memberPhysicalAddressLine2 = (String) request.getParameter("memberPhysicalAddressLine2");
        String memberPhysicalAddressLine3 = (String) request.getParameter("memberPhysicalAddressLine3");
        String memberPhysicalCityAddrees = (String) request.getParameter("memberPhysicalCityAddrees");
        String memberPhysicalCodeAddress = (String) request.getParameter("memberPhysicalCodeAddress");

        physical.setAddressLine1(memberPhysicalAddressLine1);
        physical.setAddressLine2(memberPhysicalAddressLine2);
        physical.setAddressLine3(memberPhysicalAddressLine3);
        //physical.setAddressType("1");
        physical.setAddressUse(1);
        physical.setCountryId(1);
        physical.setPrimaryInd("Y");
        physical.setPostalCode(memberPhysicalCodeAddress);
        physical.setRegion(memberPhysicalCityAddrees);
        physical.setAddressTypeId(1);
        ad.add(physical);

        AddressDetails postal = new AddressDetails();
        postal.setAddressType("Postal");
        String memberPostalAddressLine1 = (String) request.getParameter("memberPostalAddressLine1");
        String memberPostalAddressLine2 = (String) request.getParameter("memberPostalAddressLine2");
        String memberPostalAddressLine3 = (String) request.getParameter("memberPostalAddressLine3");
        String memberPostalCityAddress = (String) request.getParameter("memberPostalCityAddress");
        String memberPostalCodeAddress = (String) request.getParameter("memberPostalCodeAddress");

        postal.setAddressLine1(memberPostalAddressLine1);
        postal.setAddressLine2(memberPostalAddressLine2);
        postal.setAddressLine3(memberPostalAddressLine3);
        //postal.setAddressType("2");
        postal.setPrimaryInd("Y");
        postal.setPostalCode(memberPostalCodeAddress);
        postal.setRegion(memberPostalCityAddress);
        postal.setAddressUse(1);
        postal.setCountryId(1);
        postal.setAddressTypeId(2);
        ad.add(postal);

        String memberHomeTelNumber = (String) request.getParameter("memberHomeTelNumber");
        String memberFaxNo = (String) request.getParameter("memberFaxNo");
        String memberWorkNo = (String) request.getParameter("memberWorkNo");
        //String memberContactPersonTelNo = (String) request.getParameter("memberContactPersonTelNo");
        String memberCellNo = (String) request.getParameter("memberCellNo");
        String memberEmailAddress = (String) request.getParameter("memberEmailAddress");

        /*System.out.println("memberHomeTelNumber " + memberHomeTelNumber);
         System.out.println("memberFaxNo " + memberFaxNo);
         System.out.println("memberWorkNo " + memberWorkNo);
         System.out.println("memberContactPersonTelNo " + memberContactPersonTelNo);
         System.out.println("memberCellNo " + memberCellNo);
         System.out.println("memberEmailAddress " + memberEmailAddress);*/

        ContactDetails tel = new ContactDetails();
        tel.setCommunicationMethodId(5);
        tel.setMethodDetails(memberHomeTelNumber);
        tel.setPrimaryIndicationId(1);
        cd.add(tel);

        ContactDetails fax = new ContactDetails();
        fax.setCommunicationMethodId(2);
        fax.setMethodDetails(memberFaxNo);
        fax.setPrimaryIndicationId(1);
        cd.add(fax);

        ContactDetails workNo = new ContactDetails();
        workNo.setCommunicationMethodId(4);
        workNo.setMethodDetails(memberWorkNo);
        workNo.setPrimaryIndicationId(1);
        cd.add(workNo);

        ContactDetails cell = new ContactDetails();
        cell.setCommunicationMethodId(3);
        cell.setMethodDetails(memberCellNo);
        cell.setPrimaryIndicationId(1);
        cd.add(cell);

        ContactDetails tell = new ContactDetails();
        tell.setCommunicationMethodId(5);
        tell.setMethodDetails(memberHomeTelNumber);
        tell.setPrimaryIndicationId(1);
        cd.add(tell);

        ContactDetails email = new ContactDetails();
        email.setCommunicationMethodId(1);
        email.setMethodDetails(memberEmailAddress);
        email.setPrimaryIndicationId(1);
        cd.add(email);

        NeoUser neoUser = (NeoUser) request.getSession().getAttribute("persist_user");

        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);

        if (neoUser == null) {
            neoUser = new NeoUser();
            neoUser.setSecurityGroupId(1);
            neoUser.setUserId(1);
        }
                
/********************************************************************************
 * This is commented out because it causes the printpool to generate/(mail or print) 2 documents
 * solution to this was by creating a new backend method [saveContactAndAddressDetails]
* 
 *       //Save Postal
*        String contactLocation = port.saveContactListDetails(cd, sec, entity);
 *              
*
 *       //Save Physical
*        String addressLocation = port.saveAddressListDetails(ad, sec, entity);
********************************************************************************/
        
        String contactAdressLocation = port.saveContactAndAddressDetails(cd, ad, sec, entity);
        
        //--------------------MAILING Functionality-----------------//
            String coverNumber = port.getCoverNumberForEntity(entity);
            List<CoverDetails> coverD = port.getCoverDetailsByCoverNumber(coverNumber);
            
            boolean hasEmail = port.memberHasEmail(entity);
            if (hasEmail){
                InputStream in = null;
                try {
                    contactAdressLocation = new PropertiesReader().getProperty("DocumentIndexIndexDrive") + contactAdressLocation;
                    ContactDetails contactDetail = port.getContactDetails(entity, 1);
                    String emailFrom = "noreply@agilityghs.co.za";
                    String scheme = "Agility";
                    String emailTo = contactDetail.getMethodDetails();
                    int productId = Integer.parseInt(String.valueOf(session.getAttribute("productId")));

                    if (productId == 1){
                        scheme = "Resolution Health";
                        emailFrom = "Noreply@agilityghs.co.za";
                    }
                    else if (productId == 2){
                        scheme = "Spectramed";
                        emailFrom = "Noreply@spectramed.co.za";
                    }
                    else if (productId == 3){
                        scheme = "Sizwe";
                        emailFrom = "do-not-reply@sizwe.co.za";
                    }
                    boolean emailSent = false;

                    System.out.println("DocumentIndexIndexDrive: " + contactAdressLocation);
                    //Creating a Byte out of the pdf
                    File file = new File(contactAdressLocation);
                    if (file.exists()) {
                        in = new FileInputStream(file);
                        int contentLength = (int) file.length();
                        logger.info("The content length is " + contentLength);
                        ByteArrayOutputStream temporaryOutput;
                        if (contentLength != -1) {
                            temporaryOutput = new ByteArrayOutputStream(contentLength);
                        } else {
                            temporaryOutput = new ByteArrayOutputStream(20480);
                        }
                        byte[] bufer = new byte[512];
                        while (true) {
                            int len = in.read(bufer);
                            if (len == -1) {
                                break;
                            }
                            temporaryOutput.write(bufer, 0, len);
                        }
                        in.close();
                        temporaryOutput.close();
                        byte[] array = temporaryOutput.toByteArray();
                        //Creating a Byte out of the pdf
                        if (emailTo != null && !emailTo.equalsIgnoreCase("")) {
                            String subject = scheme;
                            if (productId == 1) {
                                subject = "Your Resolution Health Customer Information Update";
                            } else if (productId == 2) {
                                subject = "Your Spectramed Customer Information Update";
                            }
                            String emailMsg = null;
                            EmailContent content = new EmailContent();
                            content.setContentType("text/plain");
                            EmailAttachment attachment = new EmailAttachment();
                            attachment.setContentType("application/octet-stream");
                            attachment.setName("Member_Information.pdf");
                            content.setFirstAttachment(attachment);
                            content.setFirstAttachmentData(array);
                            content.setSubject(subject);
                            content.setEmailAddressFrom(emailFrom);
                            content.setEmailAddressTo(emailTo);
                            content.setProductId(productId);
                            content.setMemberCoverNumber(coverNumber);
                            content.setMemberTitle(coverD.get(0).getTitle());
                            content.setMemberInitials(coverD.get(0).getInitials());
                            content.setMemberName(coverD.get(0).getName());
                            content.setMemberSurname(coverD.get(0).getSurname());
                            
                            //Determine msg for email according to type of document
                            content.setType("updateToContactDetails");
                            /*If document type is custom, this message added here will be displayed in the email. The email method will automatically add the 
                             the enquiries and call center aswell as the kind regards at the end of the message*/
                            content.setContent(emailMsg);
                            
                            //
                            emailSent = true;
                            boolean result = true;
                            //
//                            emailSent = com.koh.command.FECommand.service.getAgileManagerPort().sendEmailWithOrWithoutAttachment(content);
//                            logger.info("Email sent : " + emailSent);
//                            boolean result = false;
//                            if(emailSent){
//                                result = port.mailCommunicationAudit(Integer.parseInt(String.valueOf(session.getAttribute("coverEntityId"))), emailTo, "Email sending successful", 15);
//                            } else {
//                                result = port.mailCommunicationAudit(Integer.parseInt(String.valueOf(session.getAttribute("coverEntityId"))), emailTo, "Email sending Failed", 15);
//                            }
                            logger.info("MailCommunicationAudit created : " + result);
                            if (emailSent && result){
                                session.setAttribute("MailResultMessage", "Member Details Updated");
                            } else {
                                session.setAttribute("MailResultMessage", "Unable to update Member Details");
                            }
                        } else {
                        }
                    } else {
                    }
                } catch (FileNotFoundException ex) {
                } catch (IOException ex) {
                } finally {
                    try {
                        if (in != null) {
                            in.close();
                        }
                    } catch (IOException ex) {
                    }
                }
            }
            //--------------------MAILING Functionality-----------------//
        
        
        for (ContactDetails con : cd) {
            if (con.getCommunicationMethodId() == 1) {
                session.setAttribute("memberEmailAddress", con.getMethodDetails());
            } else if (con.getCommunicationMethodId() == 2) {
                session.setAttribute("memberFaxNo", con.getMethodDetails());
            } else if (con.getCommunicationMethodId() == 3) {
                session.setAttribute("memberCellNo", con.getMethodDetails());
            } else if (con.getCommunicationMethodId() == 4) {
                session.setAttribute("memberWorkNo", con.getMethodDetails());
            } else if (con.getCommunicationMethodId() == 5) {
                session.setAttribute("memberHomeTelNumber", con.getMethodDetails());
            }
        }
        session.setAttribute("pMemContactDetails", cd);

        for (AddressDetails details : ad) {
            if (details.getAddressType().equals("Physical")) {
                //Physical
                session.setAttribute("memberPhysicalAddressLine1", details.getAddressLine1());
                session.setAttribute("memberPhysicalAddressLine2", details.getAddressLine2());
                session.setAttribute("memberPhysicalAddressLine3", details.getAddressLine3());
                session.setAttribute("memberPhysicalCityAddrees", details.getRegion());
                session.setAttribute("memberPhysicalCodeAddress", details.getPostalCode());
            } else if (details.getAddressType().equals("Postal")) {
                //Postal
                session.setAttribute("memberPostalAddressLine1", details.getAddressLine1());
                session.setAttribute("memberPostalAddressLine2", details.getAddressLine2());
                session.setAttribute("memberPostalAddressLine3", details.getAddressLine3());
                session.setAttribute("memberPostalCityAddress", details.getRegion());
                session.setAttribute("memberPostalCodeAddress", details.getPostalCode());
            }
        }
        
        String result = validateAndSave(request);
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
        } catch (IOException ex) {
            ex.printStackTrace();
        } 

        session.setAttribute("pMemAddressDetails", ad);

        try {

            String nextJSP = "/Claims/MemberAddressDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "SaveCallCenterContactDetails";
    }
    
    private String validateAndSave(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Map<String, String> errors = MemberMainMapping.validateContact(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (save(request)) {
                additionalData = "\"message\":\"Member Contact Preference Details Updated\"";
                
                int entityId = (Integer)session.getAttribute("coverEntityId");
                //set contact preferences
                List<ContactPreference> conPref = port.getAllContactPreferences(entityId);
                MemberMainMapping.setContactPrefDetails(request, conPref);

                ArrayList<ContactPreference> conPrefDetailsList = new ArrayList<ContactPreference>();
                conPrefDetailsList = (ArrayList<ContactPreference>) port.getAllContactPreferences(entityId);
                session.setAttribute("memberContactPrefListDetail", conPrefDetailsList);
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error updating the member contact preference details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
    }
    
    private boolean save(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);
        
        int entityId = MemberMainMapping.getEntityId(request);
        
        try {
            List<ContactPreference> adList = MemberMainMapping.getContactPrefDetails(request, neoUser, entityId);
            port.saveContactPreferenceDetails(adList, sec, entityId);
            
            return true;
        } catch (Exception e) {
            return false;
        }
    }  
}

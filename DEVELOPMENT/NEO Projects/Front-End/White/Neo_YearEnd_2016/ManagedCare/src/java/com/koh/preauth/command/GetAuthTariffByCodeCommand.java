/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthTariffDetails;
import neo.manager.NeoManagerBean;
import neo.manager.TariffCode;
import neo.manager.Treatment;

/**
 *
 * @author johanl
 */
public class GetAuthTariffByCodeCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        PrintWriter out = null;
        TariffCode t = null;
        Treatment t2 = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        //get values from request
        String authPeriod = "" + session.getAttribute("authPeriod");
        String authType = "" + session.getAttribute("authType");

        try {
            out = response.getWriter();
            if (authPeriod.equalsIgnoreCase("2010")) {
                String tariffCode = request.getParameter("tariffCode");

                if (tariffCode != null && !tariffCode.trim().equalsIgnoreCase("")) {
                    int tCode = Integer.parseInt(tariffCode);
                    String provTypeId = "";
                    if (authType.trim().equals("11")) {
                        provTypeId = "" + session.getAttribute("facilityProvDiscTypeId");
                    } else {
                        provTypeId = "" + session.getAttribute("providerDiscTypeId");
                    }
                    
                    System.out.println("### provTypeId " + provTypeId);
                    
                    String optionDesc = "" + session.getAttribute("schemeOptionName");

                    //get treatment for code
                    t2 = port.getTreatmentForResoCode(provTypeId, tCode, optionDesc);

                    if (t2 != null) {
                        double quantity = 0.0d;
                        if (t2.getUnits() == 0.0d) {
                            quantity = 1.0d;
                        } else {
                            quantity = t2.getUnits();
                        }
                        session.setAttribute("clinicOriginalTA", t2.getPrice());
                        session.setAttribute("clinicOriginalTQ", quantity);

                        session.setAttribute("provType", provTypeId);
                        session.setAttribute("tariffDesc", t2.getDescription());
                        session.setAttribute("tariffCode", t2.getCode());
                        session.setAttribute("tariffQuantity", quantity);
                        session.setAttribute("tariffAmount", t2.getPrice());

                        out.print("Done|" + t2.getCode() + "|" + t2.getDescription()
                                + "|" + t2.getPrice() + "|" + quantity);
                    } else {
                        String awType = "" + session.getAttribute("awType");
                        System.out.println("authType in tariff = " + authType);
                        System.out.println("awType in tariff = " + awType);
                        AuthTariffDetails at = new AuthTariffDetails();

                        if (authType != null && !authType.trim().equals("")
                                && !authType.trim().equalsIgnoreCase("null")) {
                            if (awType != null && !awType.trim().equals("")
                                    && !awType.trim().equalsIgnoreCase("null")) {

                                if (authType.trim().equals("4") && awType.trim().equalsIgnoreCase("bbe")) {

                                    at = port.getSpecialistTreatment(tariffCode, provTypeId);
                                    if (at != null) {
                                        double quantity = 0.0d;
                                        if (at.getQuantity().trim().equalsIgnoreCase("")) {
                                            quantity = 1.0d;
                                        } else {
                                            quantity = new Double(at.getQuantity());
                                        }
                                        session.setAttribute("clinicOriginalTA", at.getAmount());
                                        session.setAttribute("clinicOriginalTQ", quantity);

                                        session.setAttribute("provType", provTypeId);
                                        session.setAttribute("tariffDesc", at.getTariffDesc());
                                        session.setAttribute("tariffCode", at.getTariffCode());
                                        session.setAttribute("tariffQuantity", quantity);
                                        session.setAttribute("tariffAmount", at.getAmount());

                                        out.print("Done|" + at.getTariffCode() + "|" + at.getTariffDesc()
                                                + "|" + at.getAmount() + "|" + quantity);

                                    } else {
                                        out.print("Error|No such tariff");
                                    }
                                } else {
                                    out.print("Error|No such tariff");
                                }
                            } else {
                                out.print("Error|No such tariff");
                            }
                        } else {
                            out.print("Error|No such tariff");
                        }

                    }
                } else {
                    out.print("Error|Tariff Code is Empty");
                }

            } else if (Integer.parseInt(authPeriod) >= 2011) {

                String tariffCode = request.getParameter("tariffCode");

                String onScreen = "" + session.getAttribute("onScreen");

                if (tariffCode != null && !tariffCode.trim().equalsIgnoreCase("")) {

                    String provTypeId = "";
                    String provNum = "";
                    if (authType.trim().equals("11")) {
                        provTypeId = "" + session.getAttribute("facilityProvDiscTypeId");
                        provNum = "" + session.getAttribute("facilityProv_text");
                    } else {
                        provTypeId = "" + session.getAttribute("providerDiscTypeId");
                        provNum = "" + session.getAttribute("provider_text");
                        if(provTypeId == null || provTypeId.equalsIgnoreCase("null")){
                            provTypeId = "" + session.getAttribute("treatingProviderDiscTypeId");
                            System.out.println("t provTypeId " + provTypeId);
                        }
                        if(provNum == null || provNum.equalsIgnoreCase("null")){
                            provNum = "" + session.getAttribute("treatingProvider_text");
                        }
                    }

                    int pId = Integer.parseInt("" + session.getAttribute("scheme"));
                    int oId = Integer.parseInt("" + session.getAttribute("schemeOption"));
                    XMLGregorianCalendar authDate = DateTimeUtils.convertDateToXMLGregorianCalendar(sdf.parse("" + session.getAttribute("authDate")));

                    System.out.println("product = " + pId);
                    System.out.println("option = " + oId);
                    System.out.println("service date = " + authDate);
                    System.out.println("prov disc = " + provTypeId);
                    System.out.println("provider num = " + provNum);

                    String icd = "" + session.getAttribute("primaryICD_text");
                    List<String> icd10 = new ArrayList<String>();
                    icd10.add(icd);
                    System.out.println("tariff code = " + tariffCode);
                    t = port.getTariffCode(pId, oId, authDate, provTypeId, provNum, provNum, icd10, null, tariffCode);

                    if (t != null) {
                        double quantity = 0.0d;
                        if (t.getUnits() == 0.0d) {
                            quantity = 1.0d;
                        } else {
                            quantity = t.getUnits();
                        }
                        session.setAttribute("clinicOriginalTA", t.getPrice());
                        session.setAttribute("clinicOriginalTQ", quantity);

                        session.setAttribute("provType", provTypeId);
                        session.setAttribute("tariffDesc", t.getDescription());
                        session.setAttribute("tariffCode", t.getCode());
                        session.setAttribute("tariffQuantity", quantity);
                        session.setAttribute("tariffAmount", t.getPrice());

                        session.removeAttribute("noTariffFound");

                        out.print("Done|" + t.getCode() + "|" + t.getDescription()
                                + "|" + t.getPrice() + "|" + quantity);

                    } else {
                        session.removeAttribute("tariffDesc");
                        session.removeAttribute("tariffCode_text");
                        session.removeAttribute("tariffCode_error");

                        session.removeAttribute("tariffQuantity");
                        session.removeAttribute("tariffQuantity_error");

                        session.removeAttribute("tariffAmount");
                        session.removeAttribute("tariffAmount_error");
                        session.setAttribute("noTariffFound", "true");
                        out.print("Error|No such tariff");

                    }

                } else {
                    out.print("Error|Tariff Code is Empty");
                }
            }

        } catch (Exception e) {
            System.out.println("Exception occurd in GetAuthTariffByCodeCommand");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetAuthTariffByCodeCommand";

    }
}

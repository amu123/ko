/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.calltrack.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CallTrackSearchCriteria;
import neo.manager.CallWorkbenchDetails;

/**
 *
 * @author johanl
 */
public class SeachCallTrackHistoryCommand extends NeoCommand {
    private String nextJSP;
    private Object _datatypeFactory;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        CallTrackSearchCriteria ctSearch = new CallTrackSearchCriteria();
        boolean checkError = false;
        // String userName = request.getParameter("userName");
        String refNo = request.getParameter("refNo");
        String provNum = request.getParameter("provNum");
        String memNo = request.getParameter("memNum");
        String dateFrom = request.getParameter("dateFrom");
        String dateTo = request.getParameter("dateTo");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        // SEARCH BY USER
        // if (!userName.trim().equalsIgnoreCase("")) {
        //   ctSearch.setUserName(userName);
        // }
        //SEARCH BY REFERENCE NUMBER
        if (!refNo.trim().equalsIgnoreCase("")) {
            ctSearch.setRefNo(refNo);
        }
        //SEARCH BY PROVIDER NUMBER
        if (!provNum.trim().equalsIgnoreCase("")) {
            ctSearch.setProviderNumber(provNum);
        }
        //SEARCH BY COVER NUMBER
        if (!memNo.trim().equalsIgnoreCase("")) {
            ctSearch.setCoverNumber(memNo);
        }
//search by Date From and  Date To 
        try {

            if (dateFrom != null && !dateFrom.equalsIgnoreCase("") && dateTo != null && !dateTo.equalsIgnoreCase("")) {
                
                Pattern pattern = Pattern.compile("\\s");
                Matcher matcher = pattern.matcher(dateFrom);
                boolean found = matcher.find();

                Matcher matcher2 = pattern.matcher(dateTo);
                boolean found2 = matcher2.find();
                
                Date tFrom = null;
                Date tTo = null;
                XMLGregorianCalendar xTreatFrom = null;
                XMLGregorianCalendar xTreatTo = null;
                
                if (!found) {
                    tFrom = dateFormat.parse(dateFrom);
                } else {
                    tFrom = dateTimeFormat.parse(dateFrom);
                }
                
                if (!found2) {
                    tTo = dateFormat.parse(dateTo);
                } else {
                    tTo = dateTimeFormat.parse(dateTo);
                }
                
                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                xTreatTo = DateTimeUtils.convertDateToXMLGregorianCalendar(tTo);

                ctSearch.setStartDate(xTreatFrom);
                ctSearch.setEndDate(xTreatTo);
                
                request.setAttribute("dateFrom", dateFrom);
                request.setAttribute("dateTo", dateTo);
                //session.setAttribute("dateFrom", null);
                //session.setAttribute("dateTo", null);
            }

            System.out.println("dateTo" + dateTo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<CallWorkbenchDetails> cwList = service.getNeoManagerBeanPort().getCallWorkbenchByCriteria(ctSearch);
        System.out.println("Size of CallWorkbenchDetails list : " + (cwList!=null?cwList.size():"empty"));
        if (cwList.size() == 0) {
            request.setAttribute("callSearchResultsMessage", "No results found.");
        } else if (cwList.size() >= 100) {
            request.setAttribute("callSearchResultsMessage", "Only first 100 results shown. Please refine search.");
        } else {
            request.setAttribute("callSearchResultsMessage", null);
        }

        if (cwList.size() > 0) {
            session.setAttribute("searchCallHistory", cwList);
        } else {
            //System.out.println("error with getCallWorkbenchByCriteria, list size = " + cwList.size());
            List<CallWorkbenchDetails> sesionList = (List<CallWorkbenchDetails>) session.getAttribute("searchCallHistory");
            if (sesionList != null) {
                if (sesionList.size() != 0) {
                    session.removeAttribute("searchCallHistory");
                }
            }
        }
        request.setAttribute("CallHistorySearch", "true");
        //nextJSP = "/Calltrack/CallHistory.jsp";

        try {
            nextJSP = "/Calltrack/CallHistory.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public XMLGregorianCalendar convertDateXML(Date date) {

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getDatatypeFactory().newXMLGregorianCalendar(calendar);

    }

    public static String convertXMLGregorianCalendarToDate(XMLGregorianCalendar date) {
        if (date == null) {
            return "";
        } else {
            String DATE_FORMAT = "yyyy/MM/dd";
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
            return sdf.format(date.toGregorianCalendar().getTime());

        }
    }

    private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                ex.printStackTrace();
            }
        }

        return (DatatypeFactory) _datatypeFactory;
    }

    @Override
    public String getName() {
        return "SeachCallTrackHistoryCommand";
    }
}

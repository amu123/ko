/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

/**
 *
 * @author josephm
 */
import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoUser;

/**
 *
 * @author josephm
 */
public class AllocateUserProfileToSessionCommand extends NeoCommand {
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
    
        System.out.println("Inside the AllocateUserProfileToSessionCommand");
        
        HttpSession session = request.getSession();
        String in = request.getParameter("userId");
        Integer index = new Integer(in);
        
        //System.out.println("The index value is " + index);
        
        if(in != null) {
        
            NeoUser user = service.getNeoManagerBeanPort().getUserSafeDetailsById(index);
            session.setAttribute("name", user.getName());
            session.setAttribute("surname", user.getSurname());
            session.setAttribute("email", user.getEmailAddress());
            session.setAttribute("username", user.getUsername());
            session.setAttribute("selected", "selected item");
            session.setAttribute("userId", index);
            
            //need to put a status field
            if(user.getPassAccessLeft() == 0) {
            
                session.setAttribute("accountStatus", "Locked");
            }else {
            
                session.setAttribute("accountStatus", "Unlocked");
            }
            
            //System.out.println("the responsibility size is " + user.getSecurityResponsibility().size());
        }

        try {
        
            String nextJSP = "/SystemAdmin/UpdateUserProfileFromSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        }catch(Exception ex) {
        
            ex.printStackTrace();
        }
        
        return null;
    }
    
    
    @Override
    public String getName() {
    
        return "AllocateUserProfileToSessionCommand";
    }
    
}
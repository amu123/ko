/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Johan-NB
 */
public class AuthBasketICDDisplay extends TagSupport {

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

        List<String> basketList = (List<String>) session.getAttribute("basketICDs");
        String authSub = "" + session.getAttribute("authSub");
        String conType = port.getValueFromCodeTableForTableId(92, authSub);

        try {

            out.println("<tr><td colspan=\"5\">");
            out.println("<label class=\"header\">Allowed ICD Selection for " + conType + "</label></br></br>");
            out.println("</td></tr>");

            out.println("<tr><td colspan=\"5\">");
            out.println("<table width=\"500\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>#</th><th align=\"left\">ICD</th><th align=\"left\">Discription</th><th></th></tr>");

            int x = 1;
            for (String basketIcd : basketList) {
                String[] icdValues = basketIcd.split("\\|");
                String icd = icdValues[0];
                String desc = icdValues[1];
                out.println("<tr>");

                out.println("<td><label class=\"label\">" + x + "</label></td>");
                out.println("<td>" +
                                "<label width=\"100\" class=\"label\">" + icd + "</label>" +
                                "<input type=\"hidden\" name=\"prefPICD\" value=\"" + icd + "\"/>" +
                            "</td>");

                out.println("<td width=\"350\"><label class=\"label\">" + desc + "</label></td>");
                out.println("<td><button name=\"opperation\" type=\"button\" onclick=\"submitWithAction('ReturnFromBasketICD');\">Select</button></td>");
                out.println("</tr>");
                x++;
            }

            out.println("</table>");
            out.println("</td></tr>");

        } catch (IOException io) {
            io.printStackTrace();
        }

        return super.doEndTag();
    }
}

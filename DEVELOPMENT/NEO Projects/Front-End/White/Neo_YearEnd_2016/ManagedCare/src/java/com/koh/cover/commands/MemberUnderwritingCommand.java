/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.koh.command.NeoCommand;
import com.koh.cover.MemberMainMapping;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Exception;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author princes
 */
public class MemberUnderwritingCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        try {
            String s = request.getParameter("buttonPressed");
            System.out.println("buttonPressed " + s);
            if (s != null && !s.isEmpty()) {
                if (s.equalsIgnoreCase("Add Penalty")) {
                    addDetails(request, response, context);
                } else if (s.equalsIgnoreCase("Update")) {
                    updateUnderwriting(request, response, context);
                } else if (s.equalsIgnoreCase("DeleteGeneral")) {
                    deleteUnderwriting(request, response, context);
                } else if (s.equalsIgnoreCase("AddGeneral")) {
                    forwardToAddMemberUnderwriting(request, response, context);
                } else if (s.equalsIgnoreCase("forwardToMemberUnderwritingDetails")) {
                    forwardToMemberUnderwritingDetails(request, response, context);
                } else if (s.equalsIgnoreCase("SaveMemberUnderwritingDetails")) {
                    addGeneralUnderwriting(request, response, context);
                } else if (s.equalsIgnoreCase("UpdateGeneral")) {
                    updateGeneralUnderwriting(request, response, context);
                } else if (s.equalsIgnoreCase("AddConSpec")) {
                    viewAddConditionSpecific(request, response, context);
                } else if (s.equalsIgnoreCase("SearchICD")) {
                    searchICD(request, response, context);
                } else if (s.equalsIgnoreCase("SaveConSpecific")) {
                    addConditionSpecific(request, response, context);
                } else if (s.equalsIgnoreCase("delConSpecific")) {
                    deleteConditionSpecificUnderwriting(request, response, context);

                } else if (s.equalsIgnoreCase("SaveConcession")) {
                    saveConcessionType(request, response, context);

                }


            }

//            request.setAttribute("MemAppSearchResults", col);
//            context.getRequestDispatcher("/MemberApplication/MemberAppSearchResults.jsp").forward(request, response);
        } catch (java.lang.Exception ex) {
            Logger.getLogger(MemberUnderwritingCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser) request.getSession().getAttribute("persist_user");
    }

    private void viewAddConditionSpecific(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        context.getRequestDispatcher("/Member/AddConditionSpecific.jsp").forward(request, response);

    }

    private void searchICD(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
//            request.setAttribute("MemAppSearchResults", col);
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        request.setAttribute("diagCodeId", "Disorder_diagCode_text");
        request.setAttribute("diagNameId", "Disorder_diagName");
        request.setAttribute("diagCodeId2", "disorderId_text");
        context.getRequestDispatcher("/Member/MemberICDSearch.jsp").forward(request, response);

    }

    private void updateUnderwriting(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        NeoUser neoUser = getNeoUser(request);
        Integer depLJP = new Integer(request.getParameter("depLJP"));
        Integer depCoverId = new Integer(request.getParameter("depCoverId"));
        Date contributionStartDate = DateTimeUtils.convertFromYYYYMMDD("2012/01/01");
        //System.err.println("coverNumber : " + session.getAttribute("coverNumber"));
        String coverNumber = (String) session.getAttribute("coverNumber");
        int depNumber = 99;

        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        for (CoverDetails cov : cdList) {
            if (cov.getCoverDetailsId() == depCoverId) {
                depNumber = cov.getDependentNumber();
                cov.setPenaltyLateJoinId(depLJP);
            }
        }
        session.setAttribute("MemberCoverDependantDetails", cdList);

        /*
         * System.err.println("depLJP " + depLJP);
         * System.err.println("depCoverId " + depCoverId);
         * System.err.println("userId " + neoUser.getUserId());
         * System.err.println("contributionStartDate " + contributionStartDate);
         * System.err.println("coverNumber " + coverNumber);
         * System.err.println("depNumber " + depNumber);
         */
        port.updateLateJoinPenalty(depCoverId, neoUser.getUserId(), depLJP, coverNumber, depNumber);
        port.updateContributionsAndBenefits(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(contributionStartDate), TabUtils.getSecurity(request));
    }

    private void deleteUnderwriting(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        NeoUser neoUser = getNeoUser(request);
        String coverNumber = (String) session.getAttribute("coverNumber");
        Integer writingEntityId = new Integer(request.getParameter("writingEntityId"));
        Integer exclutionId = new Integer(request.getParameter("exclutionId"));
        Integer exclutionType = new Integer(request.getParameter("exclutionType"));
        Date contributionStartDate = DateTimeUtils.convertFromYYYYMMDD("2012/01/01");
        int depNumber = 99;

        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        for (CoverDetails cov : cdList) {
            if (cov.getEntityId() == writingEntityId) {
                depNumber = cov.getDependentNumber();
            }
        }

        /*
         * System.err.println("writingEntityId " + writingEntityId);
         * System.err.println("exclutionId " + exclutionId);
         * System.err.println("userId " + neoUser.getUserId());
         * System.err.println("contributionStartDate " + contributionStartDate);
         * System.err.println("exclutionType " + exclutionType);
         * System.err.println("depNumber " + depNumber);
         */

        port.updateWaitingPeriodEndDate(writingEntityId, neoUser.getUserId(), exclutionId, coverNumber, depNumber, exclutionType, 0);
        //port.updateContributionsAndBenefits(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(contributionStartDate), TabUtils.getSecurity(request));
        String page = new MemberMaintenanceTabContentCommand().getMemberUnderwriting(request, writingEntityId);
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(page);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void deleteConditionSpecificUnderwriting(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        NeoUser neoUser = getNeoUser(request);
        String coverNumber = (String) session.getAttribute("coverNumber");
        Integer writingEntityId = new Integer(request.getParameter("writingEntityId"));
        Integer exclutionId = new Integer(request.getParameter("exclutionId"));
        Integer exclutionType = new Integer(request.getParameter("exclutionType"));
        String icdCode = request.getParameter("ICDCode");
        String icdDescription = request.getParameter("ICDDescription");
        Date contributionStartDate = DateTimeUtils.convertFromYYYYMMDD("2012/01/01");
        int depNumber = 99;

        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        for (CoverDetails cov : cdList) {
            if (cov.getEntityId() == writingEntityId) {
                depNumber = cov.getDependentNumber();
            }
        }

        /*
         * System.err.println("writingEntityId " + writingEntityId);
         * System.err.println("exclutionId " + exclutionId);
         * System.err.println("userId " + neoUser.getUserId());
         * System.err.println("contributionStartDate " + contributionStartDate);
         * System.err.println("exclutionType " + exclutionType);
         * System.err.println("depNumber " + depNumber);
         */
        int diagID = 0;
        neo.manager.DiagnosisSearchCriteria criteria = new neo.manager.DiagnosisSearchCriteria();
        criteria.setCode(request.getParameter("ICDCode"));
        criteria.setDescription(request.getParameter("ICDDescription"));
        List<neo.manager.Diagnosis> diag = port.findAllDiagnosisByCriteria(criteria);
        diagID = diag.get(0).getId();
        port.updateWaitingPeriodEndDate(writingEntityId, neoUser.getUserId(), exclutionId, coverNumber, depNumber, exclutionType, diagID);
        //port.updateContributionsAndBenefits(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(contributionStartDate), TabUtils.getSecurity(request));
        String page = new MemberMaintenanceTabContentCommand().getMemberUnderwriting(request, writingEntityId);
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(page);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void forwardToAddMemberUnderwriting(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        String page = "/Member/AddMemberUnderwriting.jsp";
        try {
            request.setAttribute("writingEntityId", request.getParameter("writingEntityId"));
            request.setAttribute("writingPMB", request.getParameter("writingPMB"));
            RequestDispatcher dispatcher = context.getRequestDispatcher(page);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void forwardToMemberUnderwritingDetails(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Integer writingEntityId = new Integer(request.getParameter("writingEntityId"));
        String page = "";
        try {
            page = new MemberMaintenanceTabContentCommand().getMemberUnderwriting(request, writingEntityId);
            RequestDispatcher dispatcher = context.getRequestDispatcher(page);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void addGeneralUnderwriting(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        NeoUser neoUser = getNeoUser(request);
        String page = "";
        try {
            String coverNumber = (String) session.getAttribute("coverNumber");
            int writingEntityId = new Integer(request.getParameter("writingEntityId"));
            int duration = new Integer(request.getParameter("GeneralwaitingMonths"));
            int writingPMB = new Integer(request.getParameter("writingPMB"));
            //Integer exclutionId = new Integer(request.getParameter("exclutionId"));
            //Integer exclutionType = new Integer(request.getParameter("exclutionType"));
            Date contributionStartDate = DateTimeUtils.convertFromYYYYMMDD("2012/01/01");
            int depNumber = 99;
            CoverDetails dependent = null;

            List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
            for (CoverDetails cov : cdList) {
                if (cov.getEntityId() == writingEntityId) {
                    depNumber = cov.getDependentNumber();
                    dependent = cov;
                }
            }

            /*
             * System.err.println("writingEntityId " + writingEntityId);
             * System.err.println("exclutionId " + exclutionId);
             * System.err.println("userId " + neoUser.getUserId());
             * System.err.println("contributionStartDate " + contributionStartDate);
             * System.err.println("exclutionType " + exclutionType);
             * System.err.println("depNumber " + depNumber);
             * System.err.println("writingPMB " + writingPMB);
             */

            //port.saveGeneralExclusion(dependent, writingEntityId, neoUser.getUserId(), writingPMB);
            port.saveGeneralExclusionDuration(dependent, writingEntityId, neoUser.getUserId(), writingPMB, duration);
            //port.updateContributionsAndBenefits(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(contributionStartDate), TabUtils.getSecurity(request));
            page = new MemberMaintenanceTabContentCommand().getMemberUnderwriting(request, writingEntityId);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(page);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void addConditionSpecific(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = MemberMainMapping.validateUnderwritting(request);
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        NeoUser neoUser = getNeoUser(request);
        String coverNumber = (String) session.getAttribute("coverNumber");
        int period = new Integer(request.getParameter("conditionSpecificDeptList"));
        //Integer exclutionId = new Integer(request.getParameter("exclutionId"));
        //Integer exclutionType = new Integer(request.getParameter("exclutionType"));
        int depNumber = new Integer(request.getParameter("conDeptList"));
        String disorder_diagCode = request.getParameter("Disorder_diagCode_text");
        int diagId = !request.getParameter("disorderId_text").isEmpty() ? new Integer(request.getParameter("disorderId_text")) : 0;
        Date contributionStartDate = DateTimeUtils.convertFromYYYYMMDD("2012/01/01");
        CoverDetails dependent = null;

        Date inceptionDate = null;

        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        for (CoverDetails cov : cdList) {
            if (cov.getDependentNumber() == depNumber) {
                dependent = cov;
                inceptionDate = DateTimeUtils.getDateFromXMLGregorianCalendar(cov.getSchemeJoinDate());
            }
        }


        /*
         * System.err.println("coverNumber " + dependent.getCoverNumber());
         * System.err.println("period " + period); System.err.println("userId "
         * + neoUser.getUserId()); System.err.println("contributionStartDate " +
         * contributionStartDate); System.err.println("Denpendent Number " +
         * depNumber); System.err.println("entityNumber " +
         * dependent.getEntityId()); System.err.println("disorder_diagCode " +
         * disorder_diagCode); System.err.println("Id " + diagId);
         */

        String strStartDate = request.getParameter("startDate");
        //String strEndDate = request.getParameter("endDate");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date startDate = null;
        //Date endDate = null;
        try {
            startDate = !strStartDate.isEmpty() ? dateFormat.parse(strStartDate) : null;

            if (startDate.before(inceptionDate)) {
                errors.put("startDate_error", "Start Date Should Not Be Less Than Join Date : " + dateFormat.format(inceptionDate));
            }



            //endDate = dateFormat.parse(strEndDate);        

            //if start date is greater than enddate

//            if(startDate.after(endDate)){
//                errors.put("endDate_error","End Date Should not be Less Than Start Date");
//                errors.put("startDate_error","Start Date Should not be Greater Than End Date"); 
//            }

        } catch (Exception e) {
        }
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        //port.saveSpecificConditionExclusion(dependent, period, neoUser.getUserId(), disorder_diagCode, diagId);
        System.out.println("Status: " + status);
        if ("OK".equalsIgnoreCase(status)) {
            port.saveSpecificConditionExclusion(dependent, period, neoUser.getUserId(), disorder_diagCode, diagId, startDate != null ? DateTimeUtils.convertDateToXMLGregorianCalendar(startDate) : null, null);
            String page = new MemberMaintenanceTabContentCommand().getMemberUnderwriting(request, dependent.getEntityId());
            try {
                RequestDispatcher dispatcher = context.getRequestDispatcher(page);
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        String result = TabUtils.buildJsonResult(status, validationErros, null, null);
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        //port.updateContributionsAndBenefits(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(contributionStartDate), TabUtils.getSecurity(request));
    }

    private void updateGeneralUnderwriting(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        NeoUser neoUser = getNeoUser(request);
        String coverNumber = (String) session.getAttribute("coverNumber");
        Integer writingEntityId = new Integer(request.getParameter("writingEntityId"));
        Integer exclutionId = new Integer(request.getParameter("exclutionId"));
        //Integer exclutionType = new Integer(request.getParameter("exclutionType"));
        Integer writingPMB = new Integer(request.getParameter("writingPMB"));
        Date contributionStartDate = DateTimeUtils.convertFromYYYYMMDD("2012/01/01");
        int depNumber = 99;
        CoverDetails dependent = null;

        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        for (CoverDetails cov : cdList) {
            if (cov.getEntityId() == writingEntityId) {
                depNumber = cov.getDependentNumber();
                dependent = cov;
            }
        }

        /*
         * System.err.println("writingEntityId " + writingEntityId);
         * System.err.println("exclutionId " + exclutionId);
         * System.err.println("userId " + neoUser.getUserId());
         * System.err.println("contributionStartDate " + contributionStartDate);
         * System.err.println("exclutionType " + exclutionType);
         * System.err.println("depNumber " + depNumber);
         * System.err.println("writingPMB " + writingPMB);
         */
        port.updateWaitingPeriodsExclusion(dependent, neoUser.getUserId(), writingPMB, exclutionId);
        //port.updateWaitingPeriodEndDate(writingEntityId, neoUser.getUserId(), exclutionId, coverNumber, depNumber, exclutionType);
        //port.saveGeneralExclusion(dependent, writingEntityId, neoUser.getUserId(), writingPMB);
        //port.updateContributionsAndBenefits(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(contributionStartDate), TabUtils.getSecurity(request));
        String page = new MemberMaintenanceTabContentCommand().getMemberUnderwriting(request, writingEntityId);
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(page);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void addDetails(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));

        context.getRequestDispatcher("/Member/AddMemberUnderwriting.jsp").forward(request, response);
    }

    private int getIntParam(HttpServletRequest request, String paramName) {
        try {
            String maxDepStr = request.getParameter(paramName);
            return Integer.parseInt(maxDepStr);
        } catch (java.lang.Exception e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }

    private String getDateStr(XMLGregorianCalendar xgc) {
        try {
            Date dt = xgc.toGregorianCalendar().getTime();
            return DateTimeUtils.convertToYYYYMMDD(dt);
        } catch (java.lang.Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
    }

    private boolean saveConcessionType(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoUser neoUser = (NeoUser) request.getSession().getAttribute("persist_user");
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        try {
            CoverDetailsAdditionalInfo addInfo = getConceType(request, neoUser);
            addInfo = port.saveConcessionType(addInfo);

            int infoTypeid = 9;
            List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
            int CommonentityID = 0;
            for (CoverDetails cd1 : cdList) {
                if (cd1.getDependentTypeId() == 17) {
                    CommonentityID = cd1.getEntityCommonId();


                }
                System.out.println("start method");
                ValidateConcessionUnderwriting(request);
                //context.getRequestDispatcher("/Member/MemberUnderwriting.jsp").forward(request, response);
                System.out.println("end method");
                System.out.println("end method");
            }

            return true;
        } catch (java.lang.Exception e) {
            System.out.println("Method eecuting in saveConceDetails   boolean : false ");
            e.printStackTrace();
            return false;
        }
    }

    public static CoverDetailsAdditionalInfo getConceType(HttpServletRequest request, neo.manager.NeoUser neoUser) {
        //CoverDetailsAdditionalInfo ma = (CoverDetailsAdditionalInfo) TabUtils.getDTOFromRequest(request, MemberUnderwritingCommand.class, MemberMainMapping.CONCE_MAPPINGS, "MemberUnderwriting");

        CoverDetailsAdditionalInfo ma = new CoverDetailsAdditionalInfo();
        Date today = new Date();
        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Integer entityId = (Integer) session.getAttribute("entityId");
        Integer concessionTypeValue = new Integer(request.getParameter("ConcesType"));
        String coverNumber = (String) session.getAttribute("coverNumber");
        String strStartDate = request.getParameter("ConcessionStart");
        String strEndDate = request.getParameter("ConcessionEnd");
        int infoTypeid = 9;
        List<CoverDetails> cd = port.getCoverDetailsByCoverNumber(coverNumber);
        int CommonentityID = 0;
        String coverStatus = "";
        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        for (CoverDetails cd1 : cdList) {
            if (cd1.getDependentTypeId() == 17) {
                CommonentityID = cd1.getEntityCommonId();

            }
        }


        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date startDate = null;
        Date endDates = null;
        try {
            if (concessionTypeValue == 1) {
                startDate = !strStartDate.isEmpty() ? dateFormat.parse(strStartDate) : null;
                endDates = !strEndDate.isEmpty() ? dateFormat.parse(strEndDate) : endDate;

            } else {

                startDate = !strStartDate.isEmpty() ? dateFormat.parse(strStartDate) : null;
                endDates = !strEndDate.isEmpty() ? dateFormat.parse(strEndDate) : null;

            }
        } catch (ParseException ex) {
            Logger.getLogger(MemberUnderwritingCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        int createdBy = neoUser.getUserId();
        int lastUpdatedby = neoUser.getUserId();
        int secGroupId = neoUser.getSecurityGroupId();
        try {
            ma.setEntityCommonId(CommonentityID);
            ma.setInfoTypeId(infoTypeid);
            ma.setInfoValue(String.valueOf(concessionTypeValue));
            ma.setSecurityGroupId(neoUser.getSecurityGroupId());
            ma.setLastUpdatedBy(neoUser.getUserId());
            ma.setCreatedBy(neoUser.getUserId());
            ma.setEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
            ma.setInfoStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
            ma.setInfoEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDates));
            return ma;
        } catch (Exception e) {
            System.out.println("Printing Exception  : " + e);
        }

        return ma;

    }

    private void ValidateConcessionUnderwriting(HttpServletRequest request) throws IOException, ServletException {

        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = new NeoUser();
        HttpSession session = request.getSession();
        String coverNumber = (String) session.getAttribute("coverNumber");
        int infoTypeid = 9;

        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        int CommonentityID = 0;
        for (CoverDetails cd1 : cdList) {
            if (cd1.getDependentTypeId() == 17) {
                CommonentityID = cd1.getEntityCommonId();

            }
        }

        int conceTypeExistValue = port.getConcessionTypebyEntityCommId(CommonentityID, infoTypeid);

        session.setAttribute("validateConceType", conceTypeExistValue);

        //return JSP_FOLDER + "MemberUnderwriting.jsp";
    }

    @Override
    public String getName() {
        return "MemberUnderwritingCommand";
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.calltrack.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ProviderDetails;

/**
 *
 * @author josephm
 */
public class FindProviderByCodeCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        String provider = request.getParameter("providerNumber");
        ProviderDetails details = service.getNeoManagerBeanPort().fetchProviderNameForAjax(provider);

        try {
             PrintWriter out = response.getWriter();
             
                if(details.getProviderName() != null && details.getProviderSurname() != null){
                     String providerDesc = "";
                     providerDesc = details.getDisciplineType().getId() +" - "+ details.getDisciplineType().getValue();

                     out.print(getName() + "|ProviderName="+details.getProviderName()+ " " +details.getProviderSurname()+"|ProviderDiscipline="+providerDesc+"$");
                    
                 }
                 else {

                     out.print("Error|No such provider|" + getName());
                     //request.setAttribute("providerNumber", "");
                 }

             if(details.getDisciplineType() != null){
                    session.setAttribute("disciplineCode", details.getDisciplineType().getId());
             }
        }
        catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "FindProviderByCodeCommand";
    }
}

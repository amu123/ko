/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.ClaimNoteSearchCriteria;
import neo.manager.ClaimNoteSearchResults;
import neo.manager.ClaimNotes;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author janf
 */
public class ClaimNotesCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        String command = request.getParameter("command");
        System.out.println("Operation: ClaimNotesCommand - Command: " + command);

        if ("FetchCaimNotes".equalsIgnoreCase(command)) {
            System.out.println("memClaimId = " + session.getAttribute("memClaimId"));
            int claimId = Integer.parseInt(String.valueOf(session.getAttribute("memClaimId")));
            request.setAttribute("claimId", claimId);
            List<ClaimNotes> cn = port.fetchClaimNotesByClaimId(claimId);
            if (cn != null && !cn.isEmpty()) {
                request.setAttribute("ClaimNotes", cn);
                System.out.println("cn is not null or empty");
            } else {
                System.out.println("Empty Cn");
            }
            request.setAttribute("claimId", claimId);
            try {
                String nextJSP;
                nextJSP = "/NClaims/ClaimsNotes_NoteSummary.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if ("AddNote".equalsIgnoreCase(command)) {
            session.setAttribute("claimId", session.getAttribute("memClaimId"));
            try {
                String nextJSP;
                nextJSP = "/NClaims/ClaimNotes_AddNote.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if ("ReturnToViewNotes".equalsIgnoreCase(command)) {
            try {
                String nextJSP;
                nextJSP = "/NClaims/ClaimsNotesDetails.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if ("SaveNote".equalsIgnoreCase(command)) {
            try {
                ClaimNotes cn = saveClaimNote(request, port, session);
                System.out.println("Claim Id : " + cn.getClaimId());
                System.out.println("Note Id : " + cn.getNoteId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("ViewNote".equalsIgnoreCase(command)) {
            String snoteId = request.getParameter("noteId");
            System.out.println("NoteId = " + snoteId);
            if (snoteId != null && snoteId != "undifined") {
                int noteId = Integer.parseInt(snoteId);
                request.setAttribute("noteDetails", port.fetchClaimNoteByNoteId(noteId));
            }

            try {
                String nextJSP;
                nextJSP = "/NClaims/ClaimsNotes_ViewNote.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if ("SearchNotes".equalsIgnoreCase(command)) {
            try {
                ClaimNoteSearchCriteria cnsc = new ClaimNoteSearchCriteria();
                cnsc.setBachNumber(request.getParameter("ClaimNotes_SearchBachNumber"));
                cnsc.setClaimNumber(request.getParameter("ClaimNotes_SearchClaimNumber"));
                cnsc.setPracticeNumber(request.getParameter("ClaimNotes_SearchPracticeNumber"));
                cnsc.setCoverNumber(request.getParameter("ClaimNotes_SearchCoverNumber"));

                Collection<ClaimNoteSearchResults> result = port.findAllNotesByCriteria(cnsc);

                request.setAttribute("SearchNoteResults", result);
                System.out.println(result.size());

                RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/ClaimNote_SearchNotesSummary.jsp");
                dispatcher.forward(request, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private ClaimNotes saveClaimNote(HttpServletRequest request, NeoManagerBean port, HttpSession session) {
        ClaimNotes cn = new ClaimNotes();
        System.out.println("[frs]saveClaim claimId = " + request.getParameter("claimId"));
        Date today = new Date(System.currentTimeMillis());
        XMLGregorianCalendar xToday = DateTimeUtils.convertDateToXMLGregorianCalendar(today);
        XMLGregorianCalendar xEnd = DateTimeUtils.convertDateToXMLGregorianCalendar(DateTimeUtils.convertFromYYYYMMDD("2999/12/31"));
        String note = request.getParameter("noteDetails");
        System.out.println("[frs]xEnd = " + xEnd);
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        System.out.println("user = " + user);
        cn.setClaimId(Integer.parseInt(request.getParameter("claimId")));
        cn.setCreatedBy(user.getUserId());
        cn.setLastUpdatedBy(user.getUserId());
        cn.setSecurityGroupId(user.getSecurityGroupId());
//        cn.setNoteDetails(request.getParameter("notes"));
        System.out.println("[fr]notes = " + request.getParameter("notes"));
        cn.setNoteDetails(note);
        System.out.println("[fr]note = " + note);
        cn.setNoteDate(xToday);
        cn.setCreationDate(xToday);
        cn.setEffectiveStartDate(xToday);
        cn.setEffectiveEndDate(xEnd);
        cn.setLastUpdateDate(xToday);

        cn.setNoteId(port.saveClaimNote(cn));
        return cn;
    }

    @Override
    public String getName() {
        return "ClaimNotesCommand";
    }

}

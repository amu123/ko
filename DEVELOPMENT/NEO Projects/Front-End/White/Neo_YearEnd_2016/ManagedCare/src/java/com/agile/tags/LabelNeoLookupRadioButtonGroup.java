/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

import com.koh.command.NeoCommand;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.LookupValue;

/**
 *
 * @author johanl
 */
public class LabelNeoLookupRadioButtonGroup extends TagSupport {
    private String elementName;
    private String lookupId;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            @SuppressWarnings("static-access")
            List<LookupValue> lookUps = NeoCommand.service.getNeoManagerBeanPort().getCodeTable(new Integer(lookupId));
            out.println("<table>");
            String selectedRadio = "" + session.getAttribute(elementName);
            for (int i = 0; i < lookUps.size(); i++) {
                LookupValue lookupValue = lookUps.get(i);

                out.println("<tr><td><input type=\"radio\" name=\"" + elementName + "\" value=\"" + lookupValue.getId() + "\" ");
                if(lookupValue.getId().toString().equalsIgnoreCase(selectedRadio)){
                    out.println("checked ");
                }
                if(javaScript != null){
                    out.println(javaScript);
                }
                out.println("><label class=\"label\">" + lookupValue.getValue() + "</label></input></td></tr>");

            }
            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabeLookuplRadioButtonGroup tag", ex);
        }
        return super.doEndTag();

    }
    
    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover;

import static com.koh.command.NeoCommand.service;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import neo.manager.*;
import org.apache.log4j.Logger;

/**
 *
 * @author johanl
 */
public class MemberMainMapping {

    private static Logger logger = Logger.getLogger(MemberMainMapping.class);
    private static final String[][] COVER_FIELD_MAPPINGS = {
        {"CoverDetails_option", "Option", "no", "int", "CoverDetails", "optionId"},
        {"CoverDetails_coverNumber", "CoverNumber", "no", "str", "CoverDetails", "coverNumber"}
    };
    private static final String[][] PERSON_FIELD_MAPPINGS = {
        {"PersonDetails_title", "Title", "yes", "int", "PersonDetails", "titleId"},
        {"PersonDetails_initials", "Initials", "yes", "str", "PersonDetails", "initials", "0", "10"},
        {"PersonDetails_firstName", "First Name", "yes", "str", "PersonDetails", "name", "0", "50"},
        {"PersonDetails_secondName", "Preferred Name", "no", "str", "PersonDetails", "secondName", "0", "30"},
        {"PersonDetails_lastName", "Last Name", "yes", "str", "PersonDetails", "surname", "0", "30"},
        {"PersonDetails_gender", "Gender", "yes", "int", "PersonDetails", "genderId"},
        {"PersonDetails_dob", "Date Of Birth", "yes", "date", "PersonDetails", "dateOfBirth"},
        {"PersonDetails_idType", "Identification Type", "yes", "int", "PersonDetails", "identificationTypeId"},
        {"PersonDetails_idNumber", "Identification Number", "yes", "str", "PersonDetails", "iDNumber", "0", "13"},
        {"PersonDetails_homeLanguage", "Language", "yes", "int", "PersonDetails", "languageId"},
        {"PersonDetails_maritalStatus", "Marital Status", "yes", "int", "PersonDetails", "maritalStatusId"}};
    private static final String[][] CONTACT_FIELD_MAPPINGS = {
        {"ContactDetails_email", "Email Address", "no", "str", "ContactDetails", "", "0", "50"},
        {"ContactDetails_faxNo", "Fax No", "no", "str", "ContactDetails", "", "0", "50"},
        {"ContactDetails_cell", "Cellphone Number", "no", "str", "ContactDetails", "", "0", "50"},
        {"ContactDetails_telNoWork", "Telephone Number(Work)", "no", "str", "ContactDetails", "", "0", "50"},
        {"ContactDetails_telNoHome", "Telephone Number(Home)", "no", "str", "ContactDetails", "", "0", "50"}};
    private static final String[][] CONTACT_PREF_FIELD_MAPPINGS = {
        {"ConPrefDetails_claimProc", "Claim Processing", "no", "str", "ContactDetails", ""},
        {"ConPrefDetails_contriStmt", "Contributions Statement", "no", "str", "ContactDetails", ""},
        {"ConPrefDetails_claimStmt", "Claim Statement", "no", "str", "ContactDetails", ""},
        {"ConPrefDetails_claimPay", "Claim Payment", "no", "str", "ContactDetails", ""},
        {"ConPrefDetails_taxCert", "Tax Certificate", "no", "str", "ContactDetails", ""},
        {"ConPrefDetails_preauth", "Pre-Auths", "no", "str", "ContactDetails", ""},
        {"ConPrefDetails_other", "Other", "no", "str", "ContactDetails", ""}};
    private static final String[][] ADDRESS_FIELD_MAPPINGS = {
        {"AddressDetails_pos_addressLine1", "Line 1", "no", "str", "AddressDetails", "", "0", "100"},
        {"AddressDetails_pos_addressLine2", "Line 2", "no", "str", "AddressDetails", "", "0", "100"},
        {"AddressDetails_pos_addressLine3", "Line 3", "no", "str", "AddressDetails", "", "0", "100"},
        {"AddressDetails_pos_postalCode", "Postal Code", "no", "str", "AddressDetails", "", "0", "10"},
        {"AddressDetails_res_addressLine1", "Line 1", "no", "str", "AddressDetails", "", "0", "100"},
        {"AddressDetails_res_addressLine2", "Line 2", "no", "str", "AddressDetails", "", "0", "100"},
        {"AddressDetails_res_addressLine3", "Line 3", "no", "str", "AddressDetails", "", "0", "100"},
        {"AddressDetails_res_postalCode", "Postal Code", "no", "str", "AddressDetails", "", "0", "10"}};
    private static final String[][] BANK_FIELD_MAPPINGS = {
        {"BankingDetails_contri_paymentMethod", "Payment Method", "no", "str", "BankingDetails", ""},
        {"BankingDetails_contri_bankName", "Name of Bank", "no", "int", "BankingDetails", ""},
        {"BankingDetails_contri_bankBranchId", "Branch", "no", "int", "BankingDetails", ""},
        {"BankingDetails_contri_accountType", "Account Type", "no", "int", "BankingDetails", ""},
        {"BankingDetails_contri_accountHolder", "Account Holder", "no", "str", "BankingDetails", ""},
        {"BankingDetails_contri_accountNumber", "Account Number", "no", "numberField", "BankingDetails", ""},
        {"BankingDetails_pay_bankName", "Name of Bank", "no", "int", "BankingDetails", ""},
        {"BankingDetails_pay_bankBranchId", "Branch", "no", "int", "BankingDetails", ""},
        {"BankingDetails_pay_accountType", "Account Type", "no", "int", "BankingDetails", ""},
        {"BankingDetails_pay_accountHolder", "Account Holder", "no", "str", "BankingDetails", ""},
        {"BankingDetails_pay_accountNumber", "Account Number", "no", "numberField", "BankingDetails", ""}};
    private static final String[][] NOTES_MAPPINGS = {
        {"memberEntityId", "", "no", "int", "PersonDetails", "entityId"},
        {"notesListSelect", "Note Type", "no", "int", "PersonDetails", "noteType"},
        {"MemberAppNotesAdd_details", "Details", "yes", "str", "PersonDetails", "noteDetails"}
    };
    private static final String[][] BROKER_MAPPINGS = {
        {"Disorder_diagCode_text", "Code", "yes", "str", "Broker", ""},
        {"BrokerApp_StartDate", "Start Date", "yes", "date", "Broker", "effectiveStartDate"},};
    private static final String[][] BROKER_MAPPINGS_SECHABA = {
        {"BrokerApp_EndDate", "End Date", "yes", "date", "Broker", "effectiveEndDate"},};
    private static final String[][] BROKER_CONSULTANT_MAPPINGS = {
        {"Disorder_diagCode2_text", "Consultant Code", "yes", "str", "BrokerConsultant", ""},
        {"BrokerConsultantApp_StartDate", "Start Date", "yes", "date", "BrokerConsultant", "effectiveStartDate"},};
    private static final String[][] BROKER_CONSULTANT_MAPPINGS_SECHABA = {
        {"BrokerApp_EndDate", "End Date", "yes", "date", "BrokerConsultant", "effectiveEndDate"},};
    private static final String[][] OPTION_MAPPINGS = {
        {"OptionChangeApp_optionId", "New Option", "yes", "int", "Option", ""},
        {"OptionChangeApp_StartDate", "Start Date", "yes", "date", "Option", ""},
        {"OptionChangeApp_IncomeCategory", "Income Category", "no", "int", "Option", ""},};
    private static final String[][] INCEPTION_MAPPINGS = {
        {"inceptionStartDate", "Inception Date", "yes", "date", "Inc", ""},};
    private static final String[][] GROUP_MAPPINGS = {
        {"GroupApp_brokerHistoryCode", "New Option", "no", "str", "Group", ""},
        {"GroupApp_brokerHistoryName", "Name", "no", "str", "Group", ""},
        {"GroupApp_StartDate", "End Date", "no", "date", "Group", "effectiveStartDate"},};
    private static final String[][] PAYOUT_MAPPINGS = {
        {"PayoutType_payoutType", "Pay Out Type", "yes", "str", "PayoutType", "infoValue"},
        {"PayoutType_payoutStartDate", "Start Date", "yes", "date", "PayoutType", "effectiveStartDate"},};
    private static final String[][] SUSPEND_MAPPINGS = {
        {"suspensionReason", "Suspend Reason", "yes", "int", "Sus", ""},
        {"suspensionStartDate", "Start Date", "yes", "date", "Sus", "effectiveStartDate"},};
    private static final String[][] REINSTATE_MAPPINGS = {
        {"suspensionReason", "Dependant", "no", "int", "RE", ""},};
    private static final String[][] RESIGN_MAPPINGS = {
        {"deptList", "Dependant", "no", "int", "Resign", ""},
        {"resignReason", "Resign Reason", "yes", "int", "Resign", ""},
        {"deseasedReason", "Deseased Reason", "yes", "int", "Resign", ""},
        {"resignStartDate", "Start Date", "yes", "date", "Resign", ""},};
    private static final String[][] MEMBER_ADD_INFO_MAPPING = {
        {"MAI_ID", "Add Info", "no", "int", "MAI", "entityId"},
        {"MAI_EmpNo", "Employee Number", "no", "str", "MAI", "employeeNumber", "0", "30"},
        //{"MAI_Income", "Income Amount", "no", "int", "MAI", "income", "0", "10"},
        {"MAI_TaxNo", "Tax Number", "no", "str", "MAI", "taxNumber", "0", "30"},
        {"MAI_DODate", "Debit Order Date", "no", "int", "MAI", "debitOrderDate"},
        //{"MAI_PayMethod", "Payment Method", "no", "int", "MAI", "paymentMethod"},
        {"MAI_RiskCat", "Risk Category", "no", "str", "MAI", "riskCategory"},
        //{"MAI_Subsidy", "Member Contribution Portion", "no", "bd", "MAI", "subsidyAmount"},
        {"MAI_Category", "Member Category", "yes", "int", "MAI", "memberCategoryInd"},
        {"MAI_DebtCollectionSelect", "Debt Collection", "no", "str", "MAI", "hasDebt"},
        {"MAI_HandedOverSelect", "Handed Over", "no", "str", "MAI", "debtHandedOver"},
        {"MAI_HandedOverDateSelect", "Date Handed Over", "no", "date", "MAI", "debtHandedOverDate"}
    };
    private static final String[][] MEMBER_INCOME_MAPPING = {
        {"MAI_newIncome", "Income Amount", "yes", "str", "MAI", "", "0", "10"},
        {"MAI_StartDate", "Start Date", "yes", "date", "MAI", ""}
    };
    private static final String[][] MEMBER_SUBSIDY_MAPPING = {
        {"MAI_newSubsidy", "Subsidy", "yes", "str", "MAI", "", "0", "10"},
        {"MAI_StartDate", "Start Date", "yes", "date", "MAI", ""}
    };
    private static final String[][] STUDENT_DEPENDANT_MAPPING = {
        {"MAI_newStudent", "Student Dependant", "yes", "str", "MAI", ""},
        {"MAI_newStudentStartDate", "Start Date", "yes", "date", "MAI", ""}
    };
    private static final String[][] HANDICAP_STUDENT_MAPPING = {
        {"MAI_newHandicap", "Handicap Dependant", "yes", "str", "MAI", ""},
        {"MAI_newHandicapStartDate", "Start Date", "yes", "date", "MAI", ""}
    };
    private static final String[][] DEPENANT_TYPE_MAPPING = {
        {"MAI_newDependantType", "Dependant Type", "no", "str", "MAI", ""},
        {"MAI_newDependantTypeStartDate", "Start Date", "no", "date", "MAI", ""}
    };
    private static final String[][] FINACIAL_DEPENDANT_MAPPING = {
        {"MAI_newFinanciallyDependant", "Financially Dependant", "yes", "int", "MAI", ""},
        {"MAI_newFinanciallyDependantStartDate", "Start Date", "yes", "date", "MAI", ""}
    };
    private static final String[][] RELATIONSHIP_TYPE_MAPPING = {
        {"MAI_newRelType", "Relationship Type", "yes", "str", "MAI", ""},
        {"MAI_newRelTypeStartDate", "Start Date", "yes", "date", "MAI", ""}
        
    };
    private static final String[][] DEBIT_DATE_MAPPING = {
        {"MAI_newDebitDate", "Debit Date", "yes", "int", "MAI", ""},
        {"MAI_newDebitDateStartDate", "Start Date", "yes", "date", "MAI", ""}
    };
    private static final String[][] PAMENT_METHOD_MAPPING = {
        {"MAI_newPayment", "Payment Method", "yes", "int", "MAI", ""},
        {"MAI_newPaymentStartDate", "Start Date", "yes", "date", "MAI", ""}
    };
    private static final String[][] DEP_SWAP_MAPPING = {
        {"newPrincipalEntityId", "New Principal Member", "yes", "int", "DEP", ""},
        {"depSwapStartDate", "Start Date", "yes", "date", "DEP", ""},
        {"swapGroupCode_text", "Group", "yes", "str", "DEP", ""},
        {"swapBrokerCode_text", "Broker", "yes", "str", "DEP", ""}
    };
    private static final String[][] UNDERWRIITING_FIELD_MAPPINGS = {
        {"Disorder_diagCode_text", "ICD", "yes", "str", "Underwritings", ""},
        {"startDate_text", "Start Date", "no", "date", "Underwriting", ""}
    };
   
   public static final String[][] CONCE_MAPPINGS = {
         
        {"ConcesType","Concession", "yes", "str", "MemberUnderwriting", "concesFlag"},
        {"ConcessionStart","Start Date", "yes", "date", "MemberUnderwriting", "concessionStartDate"},
        {"ConcessionEnd","End Date", "yes", "date", "MemberUnderwriting", "concessionEndDate"}
    };

    public static Map<String, String> validateConceType(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, CONCE_MAPPINGS);
        return errors;
    }

    //GET MAPPINGS
    private static final Map<String, String[]> COVER_FIELD_MAP;
    private static final Map<String, String[]> PERSON_FIELD_MAP;
    private static final Map<String, String[]> CONTACT_FIELD_MAP;
    private static final Map<String, String[]> CONTACT_PREF_FIELD_MAP;
    private static final Map<String, String[]> ADDRESS_FIELD_MAP;
    private static final Map<String, String[]> BANK_FIELD_MAP;

    //COVER
    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : COVER_FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        COVER_FIELD_MAP = hm;
    }

    public static Map<String, String[]> getCoverMapping() {
        return COVER_FIELD_MAP;
    }

    //PERSON
    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : PERSON_FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        PERSON_FIELD_MAP = hm;
    }

    public static Map<String, String[]> getPersonMapping() {
        return PERSON_FIELD_MAP;
    }

    //CONTACT
    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : CONTACT_FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        CONTACT_FIELD_MAP = hm;
    }

    public static Map<String, String[]> getContactMapping() {
        return CONTACT_FIELD_MAP;
    }

    //CONTACT PREFERENCE
    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : CONTACT_PREF_FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        CONTACT_PREF_FIELD_MAP = hm;
    }

    public static Map<String, String[]> getContactPrefMapping() {
        return CONTACT_PREF_FIELD_MAP;
    }

    //ADDRESS
    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : ADDRESS_FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        ADDRESS_FIELD_MAP = hm;
    }

    public static Map<String, String[]> getAddressMapping() {
        return ADDRESS_FIELD_MAP;
    }

    //BAKING
    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : BANK_FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        BANK_FIELD_MAP = hm;
    }

    public static Map<String, String[]> getBankingMapping() {
        return BANK_FIELD_MAP;
    }

    /**
     * ************************VALIDATION************************
     */
    //COVER
    public static Map<String, String> validateCover(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, COVER_FIELD_MAPPINGS);
        return errors;
    }

    //ADDRESS
    public static Map<String, String> validateAddress(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, ADDRESS_FIELD_MAPPINGS);
        return errors;
    }
    //CONTACT

    public static Map<String, String> validateContact(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, CONTACT_FIELD_MAPPINGS);
        return errors;
    }
    //BANKING

    public static Map<String, String> validateBanking(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, BANK_FIELD_MAPPINGS);
        return errors;
    }
    //PERSONAL

    public static Map<String, String> validatePerson(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, PERSON_FIELD_MAPPINGS);
        return errors;
    }

    public static Map<String, String> validateNotes(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, NOTES_MAPPINGS);
        return errors;
    }

    public static Map<String, String> validateBroker(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
            TabUtils.validate(request, errors, BROKER_MAPPINGS);
        return errors;
    }

    public static Map<String, String> validateBrokerConsultant(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
            TabUtils.validate(request, errors, BROKER_CONSULTANT_MAPPINGS);
        return errors;
    }
    
    public static Map<String, String> validateBrokerTermination(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
            TabUtils.validate(request, errors, BROKER_MAPPINGS_SECHABA);
        return errors;
    }

    public static Map<String, String> validateBrokerConsultantTermination(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
            TabUtils.validate(request, errors, BROKER_CONSULTANT_MAPPINGS_SECHABA);
        return errors;
    }

    public static Map<String, String> validateChangeOption(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, OPTION_MAPPINGS);
        return errors;
    }

    public static Map<String, String> validateInceptionDate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, INCEPTION_MAPPINGS);
        return errors;
    }

    public static Map<String, String> validateDepSwap(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, DEP_SWAP_MAPPING);
        HttpSession session = request.getSession();
        List<CoverDetails> covList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        Date mainDate = new Date();
        Date depDate = new Date();
         for (CoverDetails cd : covList) {
            if (cd.getDependentTypeId() == 17) {
                mainDate = DateTimeUtils.getDateFromXMLGregorianCalendar(cd.getCoverStartDate());
            } else {
                if (cd.getEntityId() == TabUtils.getIntParam(request, "newPrincipalEntityId")) {
                    depDate = DateTimeUtils.getDateFromXMLGregorianCalendar(cd.getCoverStartDate());
                }
            }
        }
        if (depDate.after(mainDate)) {
            errors.put("newPrincipalEntityId_error", "Cannot perform dependant swap.  New principal members start date is greater than current principal members start date.");
        } else {
            errors.put("newPrincipalEntityId_error", "");
        }
        return errors;
    }

    public static Map<String, String> validateGroup(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, GROUP_MAPPINGS);
        return errors;
    }
    
    public static Map<String, String> validatePayoutType(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, PAYOUT_MAPPINGS);
        return errors;
    }

    public static Map<String, String> validateSuspension(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, SUSPEND_MAPPINGS);
        return errors;
    }

    public static Map<String, String> validateReInstate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, REINSTATE_MAPPINGS);
        return errors;
    }

    public static Map<String, String> validateResign(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, RESIGN_MAPPINGS);
        return errors;
    }
    public static Map<String, String> validateMemberAddInfo(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, MEMBER_ADD_INFO_MAPPING);
        return errors;
    }

    public static Map<String, String> validateMemberIncome(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, MEMBER_INCOME_MAPPING);
        return errors;
    }

    public static Map<String, String> validateMemberSubsidy(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, MEMBER_SUBSIDY_MAPPING);
        return errors;
    }

    public static Map<String, String> validateDependentType(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, DEPENANT_TYPE_MAPPING);
        return errors;
    }

    public static Map<String, String> validateFinacialDependant(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FINACIAL_DEPENDANT_MAPPING);
        return errors;
    }
    
    public static Map<String, String> validateRelationshipType(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, RELATIONSHIP_TYPE_MAPPING);
        return errors;
    }

    public static Map<String, String> validateDebitDate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, DEBIT_DATE_MAPPING);
        return errors;
    }

    public static Map<String, String> validatePaymentMethod(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, PAMENT_METHOD_MAPPING);
        return errors;
    }

    public static Map<String, String> validateStudentDependant(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, STUDENT_DEPENDANT_MAPPING);
        return errors;
    }

    public static Map<String, String> validateHandicapDependant(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, HANDICAP_STUDENT_MAPPING);
        return errors;
    }
    public static Map<String, String> validateUnderwritting(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, UNDERWRIITING_FIELD_MAPPINGS);
        return errors;
    }

    /**
     * ************************PERSON DETAILS************************
     */
    public static  PersonDetails getPersonDetail(HttpServletRequest request, int entityId) {
         PersonDetails pd = (PersonDetails) TabUtils.getDTOFromRequest(request, PersonDetails.class, PERSON_FIELD_MAPPINGS, "PersonDetails");
        return pd;
    }

     public static void setPersonDetail(HttpServletRequest request, PersonDetails pd) {
        if (pd == null) {
            return;
        } else {
            TabUtils.setRequestFromDTO(request, pd, PERSON_FIELD_MAPPINGS, "PersonDetails");
        }
    }

    public static void setCoverAddDetails(HttpServletRequest request, List<CoverDetailsAdditionalInfo> covDetails) {
        
        List<neo.manager.LookupValue> lookUpsClient = service.getNeoManagerBeanPort().findCodeTableForLookupType(213);
        
        if (covDetails == null) {
            return;
        }
        for (CoverDetailsAdditionalInfo cov : covDetails) {
            if (cov == null) {
                continue;
            }
            switch (cov.getInfoTypeId()) {
                case 1:
                    if (cov.getInfoValue() != null) {
                        request.setAttribute("MAI_DepType", getDependantTypeValue(cov.getInfoValue()));
                    }
                    break;
                case 2:
                    request.setAttribute("MAI_Student", getYesNoValue(cov.getInfoValue()));
                    break;
                case 3:
                    request.setAttribute("MAI_Handicap", getYesNoValue(cov.getInfoValue()));
                    break;
                case 4:
                    request.setAttribute("MAI_Income", cov.getInfoValue());
                    break;
                case 5:
                    request.setAttribute("MAI_FinanciallyDependant", getYesNoValue(cov.getInfoValue()));
                    break;
                case 6:
                    request.setAttribute("MAI_Subsidy", cov.getInfoValue());
                    break;
                case 7:
                    request.setAttribute("MAI_DebitDate", getDebitDate(cov.getInfoValue()));
                    break;
                case 8:
                    request.setAttribute("MAI_PayMethod", getPaymentMethod(cov.getInfoValue()));
                    break;
                case 213:
                    request.setAttribute("MAI_RelType", getRelationshipType(cov.getInfoValue(), lookUpsClient));
                    break;
                case 21:
                    ProviderDetails docDetails = getDoctorDetails(cov);
                    if (docDetails.getPracticeNumber() != null) {
                        request.setAttribute("MAI_doctorPracticeNumber", docDetails.getPracticeNumber());
                    }
                    
                    if (docDetails.getPracticeName() != null) {
                        request.setAttribute("MAI_doctorPracticeName", docDetails.getPracticeName());
                    }
                    
                    if (docDetails.getNetworkName() != null) {
                        request.setAttribute("MAI_doctorPracticeNetwork", docDetails.getNetworkName());
                    }
                    
                    if (docDetails.getBhfStartDate() != null) {
                        request.setAttribute("MAI_doctorPracticeStartDate", new SimpleDateFormat("yyyy/MM/dd").format(docDetails.getBhfStartDate().toGregorianCalendar().getTime()));
                    }
                    break;
                case 22:
                    ProviderDetails dentDetails = getDentistDetails(cov);
                    if (dentDetails.getPracticeNumber() != null) {
                        request.setAttribute("MAI_dentistPracticeNumber", dentDetails.getPracticeNumber());
                    }
                    
                    if (dentDetails.getPracticeName() != null) {
                        request.setAttribute("MAI_dentistPracticeName", dentDetails.getPracticeName());
                    }
                    
                    if (dentDetails.getNetworkName() != null) {
                        request.setAttribute("MAI_dentistPracticeNetwork", dentDetails.getNetworkName());
                    }
                    
                    if (dentDetails.getBhfStartDate() != null) {
                        request.setAttribute("MAI_dentistPracticeStartDate", new SimpleDateFormat("yyyy/MM/dd").format(dentDetails.getBhfStartDate().toGregorianCalendar().getTime()));
                    }
                    break;
                case 23:
                    ProviderDetails optDetails = getOptometristDetails(cov);
                    if (optDetails.getPracticeNumber() != null) {
                        request.setAttribute("MAI_optometristPracticeNumber", optDetails.getPracticeNumber());
                    }
                    
                    if (optDetails.getPracticeName() != null) {
                        request.setAttribute("MAI_optometristPracticeName", optDetails.getPracticeName());
                    }
                    
                    if (optDetails.getNetworkName() != null) {
                        request.setAttribute("MAI_optometristPracticeNetwork", optDetails.getNetworkName());
                    }
                    
                    if (optDetails.getBhfStartDate() != null) {
                        request.setAttribute("MAI_optometristPracticeStartDate", new SimpleDateFormat("yyyy/MM/dd").format(optDetails.getBhfStartDate().toGregorianCalendar().getTime()));
                    }
                    break;
            }
        }
    }

    private static String getYesNoValue(String value) {
        if (value.equals("0")) {
            return "No";
        } else {
            return "Yes";
        }
    }

    public static String getDependantTypeValue(String value) {
        if (value.equals("18")) {
            return "Adult";
        } else {
            return "Child";
        }
    }
    
 private static String getConceTypeMethod(String value) {
        if (value.equals("0")) {
            return "No";
        } else if (value.equals("1")) {
            return "Under 35 concession ";
        } else {
            return "";
        }
    }
    private static String getPaymentMethod(String value) {
        if (value.equals("1")) {
            return "Debit Order";
        } else if (value.equals("2")) {
            return "Electronic Transfer";
        } else {
            return "";
        }
    }
    
    private static String getRelationshipType(String value, List<neo.manager.LookupValue> lookUpsClient){

        String relationshipType = "";

        for (neo.manager.LookupValue lv : lookUpsClient) {
            if (value.equals(lv.getId())) {
                relationshipType = lv.getValue();
                System.out.println("relationshipType: " + relationshipType);
            }
        }

        return relationshipType;

//        if (value.equals("1")) {
//            return "Wife";
//        } else if (value.equals("2")) {
//            return "Husband";
//        } else if (value.equals("3")) {
//            return "Daughter";
//        } else if (value.equals("4")) {
//            return "Son";
//        } else {
//            return "";
//        }
    }

    private static String getDebitDate(String value) {
        if (value.equals("1")) {
            return "31th";
        } else if (value.equals("2")) {
            return "1st";
        } else if (value.equals("3")) {
            return "5th";
        } else if (value.equals("4")) {
            return "6th";
        } else {
            return "";
        }
    }
    
    private static ProviderDetails getDoctorDetails(CoverDetailsAdditionalInfo cov) {
        ProviderDetails dets = new ProviderDetails();
        if (cov.getInfoTypeId() == 21) {
            ProviderSearchCriteria search = new ProviderSearchCriteria();
            search.setEntityTypeID(3);

            if (cov.getInfoValue() != null && !cov.getInfoValue().equals("")) {
                search.setPracticeNumber(cov.getInfoValue());

                List<ProviderDetails> pdList = new ArrayList<ProviderDetails>();
                pdList = service.getNeoManagerBeanPort().findAllProvidersByCriteria(search);

                for (ProviderDetails pd : pdList) {
                    if (cov.getInfoValue().equals(pd.getPracticeNumber())) {
                        dets.setPracticeNumber(pd.getPracticeNumber());
                        dets.setPracticeName(pd.getPracticeName());
                        dets.setNetworkName(pd.getNetworkName());
                        dets.setBhfStartDate(cov.getStartDate());
                    }
                }
            }
        }
        return dets;
    }
    
    private static ProviderDetails getDentistDetails(CoverDetailsAdditionalInfo cov) {
        ProviderDetails dets = new ProviderDetails();
        if (cov.getInfoTypeId() == 22) {
            ProviderSearchCriteria search = new ProviderSearchCriteria();
            search.setEntityTypeID(3);

            if (cov.getInfoValue() != null && !cov.getInfoValue().equals("")) {
                search.setPracticeNumber(cov.getInfoValue());

                List<ProviderDetails> pdList = new ArrayList<ProviderDetails>();
                pdList = service.getNeoManagerBeanPort().findAllProvidersByCriteria(search);

                for (ProviderDetails pd : pdList) {
                    if (cov.getInfoValue().equals(pd.getPracticeNumber())) {
                        dets.setPracticeNumber(pd.getPracticeNumber());
                        dets.setPracticeName(pd.getPracticeName());
                        dets.setNetworkName(pd.getNetworkName());
                        dets.setBhfStartDate(cov.getStartDate());
                    }
                }
            }
        }
        return dets;
    }
    
    private static ProviderDetails getOptometristDetails(CoverDetailsAdditionalInfo cov) {
        ProviderDetails dets = new ProviderDetails();
        if (cov.getInfoTypeId() == 23) {
            ProviderSearchCriteria search = new ProviderSearchCriteria();
            search.setEntityTypeID(3);

            if (cov.getInfoValue() != null && !cov.getInfoValue().equals("")) {
                search.setPracticeNumber(cov.getInfoValue());

                List<ProviderDetails> pdList = new ArrayList<ProviderDetails>();
                pdList = service.getNeoManagerBeanPort().findAllProvidersByCriteria(search);

                for (ProviderDetails pd : pdList) {
                    if (cov.getInfoValue().equals(pd.getPracticeNumber())) {
                        dets.setPracticeNumber(pd.getPracticeNumber());
                        dets.setPracticeName(pd.getPracticeName());
                        dets.setNetworkName(pd.getNetworkName());
                        dets.setBhfStartDate(cov.getStartDate());
                    }
                }
            }
        }
        return dets;
    }

    public static Member getMemberAddInfo(HttpServletRequest request) {
        Member pd = (Member) TabUtils.getDTOFromRequest(request, Member.class, MEMBER_ADD_INFO_MAPPING, "MAI");
        return pd;
    }

    public static Member getMemberIncome(HttpServletRequest request) {
        Member pd = (Member) TabUtils.getDTOFromRequest(request, Member.class, MEMBER_INCOME_MAPPING, "MAI");
        return pd;
    }

    public static void setMemberAddInfo(HttpServletRequest request, Member pd) {
        if (pd == null) {
            return;
        } else {
            TabUtils.setRequestFromDTO(request, pd, MEMBER_ADD_INFO_MAPPING, "MAI");
        }
    }
    
    public static void setPayoutType(HttpServletRequest request, CoverAdditionalInfo pd) {
        if (pd == null) {
            return;
        } else {
            TabUtils.setRequestFromDTO(request, pd, PAYOUT_MAPPINGS, "PayoutType");
        }
    }

    /**
     * ************************COVER DETAILS************************
     */
    public static CoverDetails getCoverDetails(HttpServletRequest request, int entityId) {
        logger.info("**************************************************");
        logger.info("getCoverDetail : " + entityId);
        CoverDetails cd = (CoverDetails) TabUtils.getDTOFromRequest(request, CoverDetails.class, COVER_FIELD_MAPPINGS, "CoverDetails");
        return cd;
    }

    public static void setCoverDetails(HttpServletRequest request, CoverDetails cd) {
        if (cd == null) {
            return;
        } else {
            logger.info("Setting coverDetails...");
            TabUtils.setRequestFromDTO(request, cd, COVER_FIELD_MAPPINGS, "CoverDetails");
        }
    }

    /**
     * ************************CONTACT DETAILS************************
     */
    public static List<ContactDetails> getContactDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<ContactDetails> cdl = new ArrayList<ContactDetails>();
        /*String email = request.getParameter("ContactDetails_email");
        String fax = request.getParameter("ContactDetails_faxNo");
        String cell = request.getParameter("ContactDetails_cell");
        String work = request.getParameter("ContactDetails_telNoWork");
        String home = request.getParameter("ContactDetails_telNoHome");*/
        
        cdl.add(getContactDetail(request, "ContactDetails_email", 1));
        cdl.add(getContactDetail(request, "ContactDetails_faxNo", 2));
        cdl.add(getContactDetail(request, "ContactDetails_cell", 3));
        cdl.add(getContactDetail(request, "ContactDetails_telNoWork", 4));
        cdl.add(getContactDetail(request, "ContactDetails_telNoHome", 5));

        /*if (email != null && !email.equalsIgnoreCase("") && !email.equalsIgnoreCase("null")) {
            cdl.add(getContactDetail(request, "ContactDetails_email", 1));
        }
        if (fax != null && !fax.equalsIgnoreCase("") && !fax.equalsIgnoreCase("null")) {
            cdl.add(getContactDetail(request, "ContactDetails_faxNo", 2));
        }
        if (cell != null && !cell.equalsIgnoreCase("") && !cell.equalsIgnoreCase("null")) {
            cdl.add(getContactDetail(request, "ContactDetails_cell", 3));
        }
        if (work != null && !work.equalsIgnoreCase("") && !work.equalsIgnoreCase("null")) {
            cdl.add(getContactDetail(request, "ContactDetails_telNoWork", 4));
        }
        if (home != null && !home.equalsIgnoreCase("") && !home.equalsIgnoreCase("null")) {
            cdl.add(getContactDetail(request, "ContactDetails_telNoHome", 5));
        }*/
        
        return cdl;
    }

    private static ContactDetails getContactDetail(HttpServletRequest request, String param, int commType) {
        ContactDetails cd = new ContactDetails();
        cd.setCommunicationMethodId(commType);
        cd.setMethodDetails(request.getParameter(param));
        cd.setPrimaryIndicationId(1);

        return cd;
    }

    public static void setContactDetails(HttpServletRequest request, Collection<ContactDetails> contactDetails) {
        if (contactDetails == null) {
            return;
        }
        for (ContactDetails cd : contactDetails) {
            if (cd == null) {
                continue;
            }
            String fieldStart = "";
            fieldStart = cd.getCommunicationMethodId() == 1 ? "ContactDetails_email" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 2 ? "ContactDetails_faxNo" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 3 ? "ContactDetails_cell" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 4 ? "ContactDetails_telNoWork" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 5 ? "ContactDetails_telNoHome" : fieldStart;
            request.setAttribute(fieldStart, cd.getMethodDetails() == null ? "" : cd.getMethodDetails());

        }
    }

    /**
     * ************************CONTACT PREFERENCE
     * DETAILS************************
     */
    public static List<ContactPreference> getContactPrefDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<ContactPreference> cdl = new ArrayList<ContactPreference>();

        String claimProc = request.getParameter("ConPrefDetails_claimProc");
        String contriStmt = request.getParameter("ConPrefDetails_contriStmt");
        String claimStmt = request.getParameter("ConPrefDetails_claimStmt");
        String claimPay = request.getParameter("ConPrefDetails_claimPay");
        String taxCert = request.getParameter("ConPrefDetails_taxCert");
        String preauth = request.getParameter("ConPrefDetails_preauth");
        String other = request.getParameter("ConPrefDetails_other");

        if (claimProc != null && !claimProc.equalsIgnoreCase("") && !claimProc.equalsIgnoreCase("99")) {
            cdl.add(getContactPrefDetails(request, "ConPrefDetails_claimProc", 1));
        }
        if (contriStmt != null && !contriStmt.equalsIgnoreCase("") && !contriStmt.equalsIgnoreCase("99")) {
            cdl.add(getContactPrefDetails(request, "ConPrefDetails_contriStmt", 2));
        }
        if (claimStmt != null && !claimStmt.equalsIgnoreCase("") && !claimStmt.equalsIgnoreCase("99")) {
            cdl.add(getContactPrefDetails(request, "ConPrefDetails_claimStmt", 3));
        }
        if (claimPay != null && !claimPay.equalsIgnoreCase("") && !claimPay.equalsIgnoreCase("99")) {
            cdl.add(getContactPrefDetails(request, "ConPrefDetails_claimPay", 4));
        }
        if (taxCert != null && !taxCert.equalsIgnoreCase("") && !taxCert.equalsIgnoreCase("99")) {
            cdl.add(getContactPrefDetails(request, "ConPrefDetails_taxCert", 5));
        }
        if (preauth != null && !preauth.equalsIgnoreCase("") && !preauth.equalsIgnoreCase("99")) {
            cdl.add(getContactPrefDetails(request, "ConPrefDetails_preauth", 6));
        }
        if (other != null && !other.equalsIgnoreCase("") && !other.equalsIgnoreCase("99")) {
            cdl.add(getContactPrefDetails(request, "ConPrefDetails_other", 7));
        }

        return cdl;
    }

    private static ContactPreference getContactPrefDetails(HttpServletRequest request, String param, int commType) {
        ContactPreference cd = new ContactPreference();
        cd.setCommunicationTypeId(commType);

        String conPrefType = request.getParameter(param);

        if (conPrefType != null && !conPrefType.equalsIgnoreCase("") && !conPrefType.equalsIgnoreCase("99")) {
            String type = conPrefType.substring(0, 1);
            int pref = Integer.parseInt(conPrefType.substring(1, 2));

            cd.setContactAddressType(type.equals("A") ? pref : 0);
            cd.setContactDetailType(type.equals("C") ? pref : 0);

        } else {
            cd.setContactAddressType(0);
            cd.setContactDetailType(0);
        }

        return cd;
    }

    public static void setContactPrefDetails(HttpServletRequest request, Collection<ContactPreference> conPref) {
        if (conPref == null) {
            return;
        }
        for (ContactPreference cd : conPref) {
            if (cd == null) {
                continue;
            }
            String fieldStart = "";
            fieldStart = cd.getCommunicationTypeId() == 1 ? "ConPrefDetails_claimProc" : fieldStart;
            fieldStart = cd.getCommunicationTypeId() == 2 ? "ConPrefDetails_contriStmt" : fieldStart;
            fieldStart = cd.getCommunicationTypeId() == 3 ? "ConPrefDetails_claimStmt" : fieldStart;
            fieldStart = cd.getCommunicationTypeId() == 4 ? "ConPrefDetails_claimPay" : fieldStart;
            fieldStart = cd.getCommunicationTypeId() == 5 ? "ConPrefDetails_taxCert" : fieldStart;
            fieldStart = cd.getCommunicationTypeId() == 6 ? "ConPrefDetails_preauth" : fieldStart;
            fieldStart = cd.getCommunicationTypeId() == 7 ? "ConPrefDetails_other" : fieldStart;

            String commMethod = "";
            if (cd.getContactAddressType() != 0) {
                commMethod = "A" + cd.getContactAddressType();
            }
            if (cd.getContactDetailType() != 0) {
                commMethod = "C" + cd.getContactDetailType();
            }

            request.setAttribute(fieldStart, commMethod);

        }
    }

    /**
     * ************************ADDRESS DETAILS************************
     */
    public static List<AddressDetails> getAddressDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<AddressDetails> cdl = new ArrayList<AddressDetails>();
        cdl.add(getAddressDetail(request, "AddressDetails_pos_", 2));
        cdl.add(getAddressDetail(request, "AddressDetails_res_", 1));
        return cdl;
    }

    private static AddressDetails getAddressDetail(HttpServletRequest request, String param, int addrType) {
        AddressDetails ad = new AddressDetails();
        ad.setAddressTypeId(addrType);
        ad.setPrimaryInd("Y");
        ad.setAddressUse(1);
        ad.setAddressLine1(request.getParameter(param + "addressLine1"));
        ad.setAddressLine2(request.getParameter(param + "addressLine2"));
        ad.setAddressLine3(request.getParameter(param + "addressLine3"));
        ad.setPostalCode(request.getParameter(param + "postalCode"));
        return ad;
    }

    public static void setAddressDetails(HttpServletRequest request, Collection<AddressDetails> addressDetails) {
        if (addressDetails == null) {
            return;
        }
        for (AddressDetails ad : addressDetails) {
            if (ad == null) {
                continue;
            }
            String fieldStart = ad.getAddressTypeId() == 1 ? "AddressDetails_res_" : "AddressDetails_pos_";
            request.setAttribute(fieldStart + "addressLine1", ad.getAddressLine1());
            request.setAttribute(fieldStart + "addressLine2", ad.getAddressLine2());
            request.setAttribute(fieldStart + "addressLine3", ad.getAddressLine3());
            request.setAttribute(fieldStart + "postalCode", ad.getPostalCode());
        }
    }

    
    
    public static void setConcessionDetails(HttpServletRequest request, List<CoverDetailsAdditionalInfo> covDetails)  {
        
        if (covDetails == null) {
            return;
        }
        for (CoverDetailsAdditionalInfo cov : covDetails) {
            if (cov == null) {
                continue;
            }
            switch (cov.getInfoTypeId()) {
                case 1:
                    if (cov.getInfoValue() != null) {
                        request.setAttribute("ConcesType", getConceTypeMethod(cov.getInfoValue()));
                    }
                  
        break;
        
        
        
    }
        }}
    /**
     * ************************BANKING DETAILS************************
     */
    public static List<BankingDetails> getBankingDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<BankingDetails> cdl = new ArrayList<BankingDetails>();
        /*
         * if (request.getParameter("BankingDetails_contri_bankName") != null &&
         * !request.getParameter("BankingDetails_contri_bankName").isEmpty()) {
         * cdl.add(getBankingDetails(request, "BankingDetails_contri_", 2)); }
         * if (request.getParameter("BankingDetails_pay_bankName") != null &&
         * !request.getParameter("BankingDetails_pay_bankName").isEmpty()) {
         * cdl.add(getBankingDetails(request, "BankingDetails_pay_", 1)); }
         */


        cdl.add(getBankingDetails(request, "BankingDetails_contri_", 2));


        cdl.add(getBankingDetails(request, "BankingDetails_pay_", 1));

        return cdl;
    }

    private static BankingDetails getBankingDetails(HttpServletRequest request, String param, int accUse) {
        BankingDetails bd = new BankingDetails();
        String paymentMethod = accUse == 1 ? request.getParameter(param + "paymentMethod") : "B";

        bd.setPaymentMethodId(paymentMethod);
        bd.setAccountUseId(accUse);
        if (request.getParameter(param + "bankName") != null && !request.getParameter(param + "bankName").isEmpty()) {
            bd.setBankId(Integer.parseInt(request.getParameter(param + "bankName")));
        }
        if (request.getParameter(param + "bankBranchId") != null && !request.getParameter(param + "bankBranchId").isEmpty()) {
            bd.setBranchId(Integer.parseInt(request.getParameter(param + "bankBranchId")));
        }
        bd.setAccountTypeId(request.getParameter(param + "accountType"));
        bd.setAccountName(request.getParameter(param + "accountHolder"));
        bd.setAccountNo(request.getParameter(param + "accountNumber"));

        return bd;
    }

    public static void setBankingDetails(HttpServletRequest request, Collection<BankingDetails> bankingDetails) {
        if (bankingDetails == null) {
            return;
        }
        for (BankingDetails bd : bankingDetails) {
            if (bd == null || bd.getAccountUseId() == 3) {
                continue;
            }
            String fieldStart = bd.getAccountUseId() == 2 ? "BankingDetails_contri_" : "BankingDetails_pay_";
            System.out.println("Payment from database " + bd.getPaymentMethodId() + " : " + fieldStart + "paymentMethod");
            if (bd.getAccountUseId() == 2) {
                request.setAttribute(fieldStart + "paymentMethod", bd.getPaymentMethodId());
            }

            request.setAttribute(fieldStart + "bankName", bd.getBankId());
            request.setAttribute(fieldStart + "bankBranchId", bd.getBranchId());
            request.setAttribute(fieldStart + "accountType", bd.getAccountTypeId());
            request.setAttribute(fieldStart + "accountHolder", bd.getAccountName());
            request.setAttribute(fieldStart + "accountNumber", bd.getAccountNo());
        }
    }

    public static int getEntityId(HttpServletRequest request) {
        return (Integer) request.getSession().getAttribute("memberCoverEntityId");
    }

    public static EntityNotes getBrokerAppNotes(HttpServletRequest request, NeoUser neoUser) {
        EntityNotes ma = (EntityNotes) TabUtils.getDTOFromRequest(request, EntityNotes.class, NOTES_MAPPINGS, "PersonDetails");
        Date today = new Date();
        String str1 = request.getParameter("MemberAppNotesAdd_details");
        String str2 = request.getParameter("memberEntityId");
        String str3 = request.getParameter("notesListSelect");
        ma.setNoteDetails(request.getParameter("MemberAppNotesAdd_details"));
        ma.setEntityId(new Integer(request.getParameter("memberEntityId")));
        ma.setNoteType(new Integer(request.getParameter("notesListSelect")));
        ma.setSecurityGroupId(neoUser.getSecurityGroupId());
        ma.setLastUpdatedBy(neoUser.getUserId());
        ma.setCreatedBy(neoUser.getUserId());
        ma.setNoteDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ma.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ma.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
        ma.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        ma.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        return ma;
    }
    
    public static CoverAdditionalInfo getNominatedProviderDoctorDetails(HttpServletRequest request) {
        CoverAdditionalInfo ca = new CoverAdditionalInfo();
        
        String practiceNimber = request.getParameter("MAI_doctorPracticeNumber");
        Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_doctorPracticeStartDate"));
        
        ca.setInfoTypeId(21);
        ca.setInfoValue(practiceNimber);
        ca.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        return ca;
    }
    
    public static CoverAdditionalInfo getNominatedProviderDentistDetails(HttpServletRequest request) {
        CoverAdditionalInfo ca = new CoverAdditionalInfo();
        
        String practiceNimber = request.getParameter("MAI_dentistPracticeNumber");
        Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_dentistPracticeStartDate"));
        
        ca.setInfoTypeId(22);
        ca.setInfoValue(practiceNimber);
        ca.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        return ca;
    }
    
    public static CoverAdditionalInfo getNominatedProviderOptometristDetails(HttpServletRequest request) {
        CoverAdditionalInfo ca = new CoverAdditionalInfo();
        
        String practiceNimber = request.getParameter("MAI_optometristPracticeNumber");
        Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_optometristPracticeStartDate"));
        
        ca.setInfoTypeId(23);
        ca.setInfoValue(practiceNimber);
        ca.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        return ca;
    }
    
    public static CoverAdditionalInfo getNetworkIPA(HttpServletRequest request) {
        CoverAdditionalInfo ca = new CoverAdditionalInfo();
        
        String networkIPA = request.getParameter("PersonDetails_network_IPA");
        Date startDate = new Date();
        
        ca.setInfoTypeId(25);
        ca.setInfoValue(networkIPA);
        ca.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        return ca;
    }

    public static MemberBroker getBrokerDetails(HttpServletRequest request, int entityId, NeoUser neoUser) {
        MemberBroker ba = new MemberBroker();
        Date today = new Date();
        if (request.getParameter("BrokerApp_EndDate") != null && !request.getParameter("BrokerApp_EndDate").isEmpty()) {
            System.out.println("brokerEntityId: " + request.getAttribute("brokerEntityId"));
            Integer brokerEntityId = (Integer) request.getAttribute("brokerEntityId");
            ba.setMemberEntityId(entityId);
            ba.setBrokerEntityId(brokerEntityId);
            ba.setLastUpdatedBy(neoUser.getUserId());
            ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
            ba.setSecurityGroupId(2);
            Date endDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("BrokerApp_EndDate"));
            ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        } else {
            ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
            ba.setMemberEntityId(entityId);
            ba.setBrokerEntityId(new Integer(request.getParameter("brokerEntityId")));
            ba.setCreatedBy(neoUser.getUserId());
            ba.setLastUpdatedBy(neoUser.getUserId());
            ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
            ba.setSecurityGroupId(2);
            if (request.getParameter("BrokerApp_StartDate") != null && !request.getParameter("BrokerApp_StartDate").isEmpty()) {
                Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("BrokerApp_StartDate"));
                ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
            }
            Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
            ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        }
        return ba;
    }

    public static MemberBroker getBrokerDetailsForInceptionDate(HttpServletRequest request, int entityId, NeoUser neoUser) {
        MemberBroker ba = new MemberBroker();
        Date today = new Date();
            int ConBroEntityId = (Integer) request.getSession().getAttribute("ConBroEntityId");
            ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
            ba.setMemberEntityId(entityId);
            ba.setBrokerEntityId(ConBroEntityId);
            ba.setCreatedBy(neoUser.getUserId());
            ba.setLastUpdatedBy(neoUser.getUserId());
            ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
            ba.setSecurityGroupId(2);
            if (request.getParameter("inceptionStartDate") != null && !request.getParameter("inceptionStartDate").isEmpty()) {
                Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("inceptionStartDate"));
                ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
            }
            Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
            ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));

        return ba;
    }

    public static MemberBrokerConsultant getBrokerConsultantDetails(HttpServletRequest request, int entityId, NeoUser neoUser) {
        System.out.println("request consultantEntityId" + request.getParameter("consultantEntityId"));
        MemberBrokerConsultant ba = new MemberBrokerConsultant();
        Date today = new Date();
        if (request.getParameter("BrokerApp_EndDate") != null && !request.getParameter("BrokerApp_EndDate").isEmpty()) {
            ba.setMemberEntityId(entityId);
            Integer conEntityId = (Integer) request.getAttribute("consultantEntityId");
            
            ba.setBrokerConsultantEntityId(conEntityId);
            ba.setLastUpdatedBy(neoUser.getUserId());
            ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
            ba.setSecurityGroupId(2);
            
            Date endDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("BrokerApp_EndDate"));
            ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        } else {
            ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
            ba.setMemberEntityId(entityId);
            ba.setBrokerConsultantEntityId(new Integer(request.getParameter("consultantEntityId")));
            ba.setCreatedBy(neoUser.getUserId());
            ba.setLastUpdatedBy(neoUser.getUserId());
            ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
            ba.setSecurityGroupId(2);
            if (request.getParameter("BrokerConsultantApp_StartDate") != null && !request.getParameter("BrokerConsultantApp_StartDate").isEmpty()) {
                Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("BrokerConsultantApp_StartDate"));
                ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
            }

            Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
            ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        }
        return ba;
    }

    public static MemberBrokerConsultant getBrokerConsultantDetailsForInceptionDate(HttpServletRequest request, int entityId, NeoUser neoUser) {
        System.out.println("request consultantEntityId " + request.getSession().getAttribute("ConBroEntityId"));
        MemberBrokerConsultant ba = new MemberBrokerConsultant();
        int ConBroEntityId = (Integer) request.getSession().getAttribute("ConBroEntityId");
        Date today = new Date();
        ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setMemberEntityId(entityId);
        ba.setBrokerConsultantEntityId(ConBroEntityId);
        ba.setCreatedBy(neoUser.getUserId());
        ba.setLastUpdatedBy(neoUser.getUserId());
        ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setSecurityGroupId(2);
        if (request.getParameter("inceptionStartDate") != null && !request.getParameter("inceptionStartDate").isEmpty()) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("inceptionStartDate"));
            ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        }

        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
        ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));

        return ba;

    }

    public static CoverDetails getOptionChange(HttpServletRequest request, int entityId, NeoUser neoUser) {
        HttpSession session = request.getSession();
        String coverNumber = (String) session.getAttribute("coverNumber");
        CoverDetails cd = new CoverDetails();
        cd.setCoverNumber(coverNumber);


        cd.setOptionId(new Integer(request.getParameter("OptionChangeApp_optionId")));
        if (request.getParameter("OptionChangeApp_StartDate") != null && !request.getParameter("OptionChangeApp_StartDate").isEmpty()) {
            GregorianCalendar gcal = new GregorianCalendar();
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("OptionChangeApp_StartDate"));
            gcal.setTime(startDate);
            gcal.set(Calendar.DAY_OF_MONTH, 1);
            cd.setCoverStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(gcal.getTime()));
        }

        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
        cd.setCoverEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));

        return cd;

    }

    public static MemberEmployer getGroupDetails(HttpServletRequest request, int entityId, NeoUser neoUser) {

        MemberEmployer ba = new MemberEmployer();
        Date today = new Date();
        ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setMemberEntityId(entityId);
        ba.setEmployerEntityId(new Integer(request.getParameter("employerEntityId")));
        ba.setCreatedBy(neoUser.getUserId());
        ba.setLastUpdatedBy(neoUser.getUserId());
        ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setSecurityGroupId(2);
        if (request.getParameter("GroupApp_StartDate") != null && !request.getParameter("GroupApp_StartDate").isEmpty()) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("GroupApp_StartDate"));
            ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        }

        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
        ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));

        return ba;

    }

    public static MemberEmployer getGroupDetailsForInceptionDate(HttpServletRequest request, int entityId, NeoUser neoUser) {
        int GroupIncEntityId = (Integer) request.getSession().getAttribute("GroupIncEntityId");
        MemberEmployer ba = new MemberEmployer();
        Date today = new Date();
        ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setMemberEntityId(entityId);
        ba.setEmployerEntityId(GroupIncEntityId);
        ba.setCreatedBy(neoUser.getUserId());
        ba.setLastUpdatedBy(neoUser.getUserId());
        ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setSecurityGroupId(2);
        if (request.getParameter("inceptionStartDate") != null && !request.getParameter("inceptionStartDate").isEmpty()) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("inceptionStartDate"));
            ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        }

        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
        ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));

        return ba;

    }

    public static Suspension getReInstateDetails(HttpServletRequest request, int entityId, NeoUser neoUser) {

        Suspension ba = new Suspension();
        Date today = new Date();
        ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));;
        ba.setCreatedBy(neoUser.getUserId());
        ba.setLastUpdatedBy(neoUser.getUserId());
        ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setSecurityGroupId(2);
        ba.setStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        /*
         * if (request.getParameter("reinstateStartDate") != null &&
         * !request.getParameter("reinstateStartDate").isEmpty()) { Date
         * startDate =
         * DateTimeUtils.convertFromYYYYMMDD(request.getParameter("reinstateStartDate"));
         * ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
         * }
         */

        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
        ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));

        return ba;

    }

    public static Suspension getSuspensionDetails(HttpServletRequest request, int entityId, NeoUser neoUser) {

        Suspension ba = new Suspension();
        Date today = new Date();
        ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setCreatedBy(neoUser.getUserId());
        ba.setLastUpdatedBy(neoUser.getUserId());
        ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setSecurityGroupId(2);
        if (request.getParameter("suspensionStartDate") != null && !request.getParameter("suspensionStartDate").isEmpty()) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("suspensionStartDate"));
            ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
            ba.setStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        } else {
            ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
            ba.setStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        }

        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
        ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        ba.setEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));

        ba.setSuspensionReason(new Integer(request.getParameter("suspensionReason")));
        return ba;

    }

    public static Suspension getResignDetails(HttpServletRequest request, int entityId, NeoUser neoUser) {

        Suspension ba = new Suspension();
        Date today = new Date();
        ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));;
        ba.setCreatedBy(neoUser.getUserId());
        ba.setLastUpdatedBy(neoUser.getUserId());
        ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setSecurityGroupId(2);
        ba.setStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));

        if (request.getParameter("resignStartDate") != null && !request.getParameter("resignStartDate").isEmpty()) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("resignStartDate"));
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            cal.set(Calendar.DAY_OF_MONTH, 1);
            cal.add(Calendar.MONTH, 1);
            cal.add(Calendar.DATE, -1);
            ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(cal.getTime()));
        }


        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
        ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));

        return ba;

    }
}

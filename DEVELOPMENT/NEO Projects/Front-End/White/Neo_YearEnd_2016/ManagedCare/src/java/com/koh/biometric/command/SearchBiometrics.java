/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import com.koh.pdc.command.PreviewAuthCommand;
import com.koh.utils.DateTimeUtils;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.BiometricsLoad;

/**
 *
 * @author johanl
 */
public class SearchBiometrics extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        String coverNumber = request.getParameter("memberNum_text");
        String depNo = request.getParameter("depListValues");
        String dateTo = request.getParameter("dateTo");
        String dateFrom = request.getParameter("dateFrom");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        request.setAttribute("biometricResultMsg", "");

        if (dateFrom == null || dateFrom.equalsIgnoreCase("")
                || dateFrom.equalsIgnoreCase("null")) {
            //set fixed 6month history search
            Date today = new Date(System.currentTimeMillis());
            Calendar cal = Calendar.getInstance();
            cal.setTime(today);
            cal.add(Calendar.MONTH, -6);
            Date startDate = cal.getTime();

            dateFrom = DateTimeUtils.dateFormat.format(startDate).substring(0, 8) + "01";
            dateTo = DateTimeUtils.dateFormat.format(today);

            request.setAttribute("biometricResultMsg", "Biometric detail retrieved  for the last 6 months history");
        }

        session.setAttribute("dateTo", dateTo);
        session.setAttribute("dateFrom", dateFrom);
        session.setAttribute("memberNum_text",coverNumber);
        session.setAttribute("depListValues", depNo);
        
        BiometricsLoad b = null;
        System.out.println("Date from : " + dateFrom + " Date to : " + dateTo);

        if ((dateFrom != null && !dateFrom.equalsIgnoreCase("")) && (dateTo != null && !dateTo.equalsIgnoreCase(""))) {

            Date tFrom = null;
            Date tTo = null;
            XMLGregorianCalendar xTreatFrom = null;
            XMLGregorianCalendar xTreatTo = null;

            try {
                tFrom = dateFormat.parse(dateFrom);
                tTo = dateFormat.parse(dateTo);

                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                xTreatTo = DateTimeUtils.convertDateToXMLGregorianCalendar(tTo);

                b = service.getNeoManagerBeanPort().fetchAllBiometrics(coverNumber, new Integer(depNo), xTreatFrom, xTreatTo);

            } catch (java.text.ParseException ex) {
                Logger.getLogger(PreviewAuthCommand.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            b = service.getNeoManagerBeanPort().fetchAllBiometrics(coverNumber, new Integer(depNo), null, null);
        }



        HashMap<Integer, Integer> bioType = new HashMap<Integer, Integer>();

        if (b != null) {
            if (b.getAsthma().size() > 0) {
                bioType.put(1, 1);
                session.setAttribute("getAsthma", b.getAsthma());
            }
            if (b.getHypertension().size() > 0) {
                bioType.put(2, 2);
                session.setAttribute("getHypertension", b.getHypertension());
            }
            if (b.getChronic().size() > 0) {
                bioType.put(3, 3);
                session.setAttribute("getChronic", b.getChronic());
            }
            if (b.getCardiac().size() > 0) {
                bioType.put(4, 4);
                session.setAttribute("getCardiac", b.getCardiac());
            }
            if (b.getDiabetes().size() > 0) {
                bioType.put(5, 5);
                session.setAttribute("getDiabetes", b.getDiabetes());
            }
            if (b.getCoronary().size() > 0) {
                bioType.put(6, 6);
                session.setAttribute("getCoronary", b.getCoronary());
            }
            if (b.getCopd().size() > 0) {
                bioType.put(7, 7);
                session.setAttribute("getCopd", b.getCopd());
            }
            if (b.getHyperlipidaemia().size() > 0) {
                bioType.put(8, 8);
                session.setAttribute("getHyperlipidaemia", b.getHyperlipidaemia());
            }
            if (b.getDepression().size() > 0) {
                bioType.put(9, 9);
                session.setAttribute("getDepression", b.getDepression());
            }
            /*if (b.getPathology().size() > 0) {
             System.out.println("getPathology()() " + b.getPathology().size());
             bioType.put(10, 10);
             session.setAttribute("getPathology", b.getPathology());
             }*/
            if (b.getGeneral().size() > 0) {
                bioType.put(10, 10);
                session.setAttribute("getGeneral", b.getGeneral());
            }
            if (b.getOther().size() > 0) {
                bioType.put(11, 11);
                session.setAttribute("getOther", b.getOther());
            }
            session.setAttribute("bioList", bioType);
            System.out.println("BIOLIST = " + bioType);
        } else {
            request.setAttribute("biometricResultMsg", "No results found, Please update search criteria");
        }
        try {
            String nextJSP = "/biometrics/SearchBiometrics.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SearchBiometrics";
    }
}

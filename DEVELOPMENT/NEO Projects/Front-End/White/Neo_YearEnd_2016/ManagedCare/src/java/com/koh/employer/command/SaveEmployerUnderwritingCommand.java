/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.employer.command;

import com.koh.command.NeoCommand;
import com.koh.cover.commands.MemberUnderwritingCommand;
import com.koh.employer.EmployerCommunicationMapping;
import com.koh.employer.EmployerUnderwritingMapping;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.EmployerUnderwriting;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author yuganp
 */
public class SaveEmployerUnderwritingCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String result = validateAndSave(port, request);
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SaveEmployerUnderwritingCommand";
    }

    private String validateAndSave(NeoManagerBean port, HttpServletRequest request) {
        Map<String, String> errors = EmployerUnderwritingMapping.validate(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
         System.out.println("status" + status);       
        if ("OK".equalsIgnoreCase(status)) {
        
            if (save(port, request)) {
                additionalData = "\"message\":\"Underwriting Details Saved\"";
                 System.out.println("errors" + validationErros);
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving the Underwriting Details.\"";
                
            System.out.println("errors" + validationErros);
           
            }
        }
        
        else {
             System.out.println("errors" + validationErros);
            additionalData = "\"message\":\"There were validation errors.\"";
        }
        return TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
        
    }

    private boolean save(NeoManagerBean port, HttpServletRequest request) {
        NeoUser neoUser = getNeoUser(request);
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);
       try {
            EmployerUnderwriting eg = EmployerUnderwritingMapping.getEmployerEligibility(request, neoUser);
            eg = port.saveEmployerUnderwriting(eg);
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
    }
 
    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser)request.getSession().getAttribute("persist_user");
    }

}

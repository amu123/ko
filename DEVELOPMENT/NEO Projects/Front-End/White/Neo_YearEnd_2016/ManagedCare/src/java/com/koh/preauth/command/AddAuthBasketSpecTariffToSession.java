/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author Johan-NB
 */
public class AddAuthBasketSpecTariffToSession extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        try {

            String provType = "" + session.getAttribute("provCatType");
            System.out.println("provider type = "+provType);
            String btCode = request.getParameter("basketCode");
            String btDesc = request.getParameter("basketDesc");
            String amount = request.getParameter("basketAmount");
            System.out.println("basket amount = "+amount);

            double tAmount = Double.valueOf(amount);
            String freq = request.getParameter("tariffFreq");
            System.out.println("freq = "+freq);
            int tFeq = 0;
            if (freq != null && !freq.trim().equalsIgnoreCase("")) {
                tFeq = new Integer(freq);

                List<AuthTariffDetails> basketList = (List<AuthTariffDetails>) session.getAttribute("AuthBasketTariffs");
                if (basketList == null) {
                    basketList = new ArrayList<AuthTariffDetails>();
                }
                //set values to list
                AuthTariffDetails ab = new AuthTariffDetails();
                ab.setProviderType(provType);
                ab.setTariffCode(btCode);
                ab.setTariffDesc(btDesc);
                ab.setAmount(tAmount);
                ab.setFrequency(tFeq);
                ab.setTariffType("2");
                ab.setSpecificTariffInd(1);

                basketList.add(ab);

                session.setAttribute("AuthBasketTariffs", basketList);

                session.removeAttribute("providerCategory");
                session.removeAttribute("provCatType");
                session.removeAttribute("btCode");
                session.removeAttribute("btDesc");
                session.removeAttribute("tariffAmount");
                session.removeAttribute("tariffFreq");

                System.out.println("Done|");

            }else{
                System.out.println("Error|");
            }

        } catch (Exception ex) {
            System.out.println("AddAuthBasketTariffToSession error : " + ex.getMessage());
            System.out.println("Error|");
        }

        try {
            String nextJSP = "/PreAuth/AuthSpecificBasketTariff.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "AddAuthBasketSpecTariffToSession";
    }

}

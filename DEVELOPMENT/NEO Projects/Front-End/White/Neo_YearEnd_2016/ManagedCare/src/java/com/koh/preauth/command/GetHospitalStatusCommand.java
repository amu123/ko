/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.XmlUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.LookupValue;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Johan-NB
 */
public class GetHospitalStatusCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter out = null;
        HttpSession session = request.getSession();
        String xmlStr = "";
        String status = request.getParameter("status");
        System.out.println("GetHospitalStatusCommand auth status = " + status);
        int lookupId = 0;
        if(status.trim().equals("1")){
            lookupId = 135;
        }else if(status.trim().equals("2") || status.trim().equals("3")){
            lookupId = 136;
        }

        response.setHeader("Cache-Control", "no-cache");
        response.setContentType("text/xml");
        //get new document
        @SuppressWarnings("static-access")
        Document doc = new XmlUtils().getDocument();
        //create xml elements
        Element root = doc.createElement("HospitalStatus");
        //add root to document
        doc.appendChild(root);
        try {
            out = response.getWriter();

            List<neo.manager.LookupValue> lookUps = service.getNeoManagerBeanPort().getCodeTable(new Integer(lookupId));

            if (lookUps.size() != 0) {
         
                for (LookupValue look : lookUps) {

                    String lookInfo = look.getId() +"|"+ look.getValue();
                    //create info element
                    Element statusInfo = doc.createElement("StatusInfo");
                    statusInfo.setTextContent(lookInfo);
                    //add element to root
                    root.appendChild(statusInfo);
                }

            } else {
                root.setTextContent("Error|Lookup not found");
                System.out.println("lookup id error : "+lookupId);
            }
            //return xml object
            try {
                xmlStr = XmlUtils.getXMLString(doc);
            } catch (IOException ex) {
                ex.printStackTrace();
                xmlStr = "";

            }
            out.println(xmlStr);
        } catch (IOException io) {
            io.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "GetHospitalStatusCommand";
    }
}

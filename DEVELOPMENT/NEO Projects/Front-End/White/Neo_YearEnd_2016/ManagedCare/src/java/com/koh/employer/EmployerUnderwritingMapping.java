/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.employer;

import com.koh.cover.commands.MemberUnderwritingCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import neo.manager.EmployerUnderwriting;
import neo.manager.NeoUser;

/**
 *
 * @author yuganp
 */
public class EmployerUnderwritingMapping {

    private static final String[][] FIELD_MAPPINGS = {
        {"EmployerGroup_entityId", "", "no", "int", "EmployerUnderwriting", "EmployerEntityId"},
        {"EmployerUnderwriting_waitingPeriod", "Apply Waiting Periods", "no", "int", "EmployerUnderwriting", "WaitingPeriod"},
        {"EmployerUnderwriting_pmb", "Allow PMB", "no", "int", "EmployerUnderwriting", "PmbAllowed"},
        {"EmployerUnderwriting_pma", "Allow PMA", "no", "int", "EmployerUnderwriting", "PmaAllowed"},
        {"ConcesType", "Concession", "no", "int", "EmployerUnderwriting", "ConcessionFlag"},
        {"EmployerUnderwriting_startDate", "Consession Start", "no", "date", "EmployerUnderwriting", "ConcessionStartDate"},
        {"EmployerUnderwriting_endDate", "Consession End", "no", "date", "EmployerUnderwriting", "ConcessionEndDate"},
        {"EmployerUnderwriting_individual", "Individual Underwriting", "no", "int", "EmployerUnderwriting", "individualUnderwriting"},
        {"EmployerUnderwriting_branch", "Branch Underwriting", "no", "int", "EmployerUnderwriting", "branchUnderwriting"},
        {"EmployerUnderwriting_catA3MonthInd", "Category A 3 Month", "no", "int", "EmployerUnderwriting", "catA3MonthInd"},
        {"EmployerUnderwriting_catA12MonthInd", "Category A 12 Month", "no", "int", "EmployerUnderwriting", "catA12MonthInd"},
        {"EmployerUnderwriting_catB3MonthInd", "Category B 3 Month", "no", "int", "EmployerUnderwriting", "catB3MonthInd"},
        {"EmployerUnderwriting_catB12MonthInd", "Category B 12 Month", "no", "int", "EmployerUnderwriting", "catB12MonthInd"},
        {"EmployerUnderwriting_catC3MonthInd", "Category C 3 Month", "no", "int", "EmployerUnderwriting", "catC3MonthInd"},
        {"EmployerUnderwriting_catC12MonthInd", "Category C 12 Month", "no", "int", "EmployerUnderwriting", "catC12MonthInd"},};
    private static final Map<String, String[]> FIELD_MAP;

    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }
   
    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
         
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
       
       
        
           if ( request.getParameter("ConcesType").equalsIgnoreCase("1") || request.getParameter("ConcesType").equalsIgnoreCase("2")){
          
          if (request.getParameter("EmployerUnderwriting_startDate").equalsIgnoreCase("") 
                  || request.getParameter("EmployerUnderwriting_startDate") == null  ||
                   request.getParameter("EmployerUnderwriting_startDate").equalsIgnoreCase("null") ){
             
         errors.put("EmployerUnderwriting_startDate_error", "Please enter the Start Date.");
          
          }
          
           if ((!request.getParameter("EmployerUnderwriting_startDate").equalsIgnoreCase("") )
                  || (request.getParameter("EmployerUnderwriting_startDate") != null ) ||
                  ( !request.getParameter("EmployerUnderwriting_startDate").equalsIgnoreCase("null") )
                  
                   ){
               
               
               try {
               
               SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
               formatter.setLenient(false);
               Date startdate = formatter.parse((request.getParameter("EmployerUnderwriting_startDate").toString()));
               
               
               
             
            
               
         } catch (ParseException e) {
		
                 errors.put("EmployerUnderwriting_Format_error", "Incorrect Format");
	}
        
               
          
          }
          
      
      if(request.getParameter("EmployerUnderwriting_endDate").toString() !=null && request.getParameter("EmployerUnderwriting_endDate").toString().length() > 0){
 
      
        
               
               
               try {
               
               SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
               formatter.setLenient(false);
               Date startdate = formatter.parse((request.getParameter("EmployerUnderwriting_startDate").toString()));
               String endingDate=request.getParameter("EmployerUnderwriting_endDate").toString();
               
               Date enddate = formatter.parse(endingDate);
           
               if(startdate.after(enddate)){
                
                errors.put("EmployerUnderwriting_endDate_error", "End Date Must Be Greater Than Start Date");
                
                return errors;
            }
             
            
               
         } catch (ParseException e) {
		
                 errors.put("EmployerUnderwriting_Format_error", "Incorrect Format");
	}
        
               
          
          }
           } else{
               
               
          
           if(request.getParameter("EmployerUnderwriting_startDate").toString() !=null && request.getParameter("EmployerUnderwriting_startDate").toString().length() > 0){
      
               
               try {
               
               SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
               formatter.setLenient(false);
               Date startdate = formatter.parse((request.getParameter("EmployerUnderwriting_startDate").toString()));
               
               
               
             
            
               
         } catch (ParseException e) {
		
                 errors.put("EmployerUnderwriting_Format_error", "Incorrect Format");
	}
        
               
          
          }
          
      
      if(request.getParameter("EmployerUnderwriting_endDate").toString() !=null && request.getParameter("EmployerUnderwriting_endDate").toString().length() > 0){
 
      
        
               
               
               try {
               
               SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
               formatter.setLenient(false);
               Date startdate = formatter.parse((request.getParameter("EmployerUnderwriting_startDate").toString()));
               String endingDate=request.getParameter("EmployerUnderwriting_endDate").toString();
               
               Date enddate = formatter.parse(endingDate);
           
               if(startdate.after(enddate)){
                
                errors.put("EmployerUnderwriting_endDate_error", "End Date Must Be Greater Than Start Date");
                
                return errors;
            }
             
            
               
         } catch (ParseException e) {
		
                 errors.put("EmployerUnderwriting_Format_error", "Incorrect Format");
	}
        
               
          
          }
             
               
               
           }
               
               
           
           
           
           
        return errors;
    }

    public static EmployerUnderwriting getEmployerEligibility(HttpServletRequest request, NeoUser neoUser) {
        EmployerUnderwriting eg = (EmployerUnderwriting) TabUtils.getDTOFromRequest(request, EmployerUnderwriting.class, FIELD_MAPPINGS, "EmployerUnderwriting");
         eg.setSecurityGroupId(neoUser.getSecurityGroupId());
        eg.setLastUpdatedBy(neoUser.getUserId());
        return eg;
    }

    public static void setEmployerUnderwriting(HttpServletRequest request, EmployerUnderwriting employerUnderwriting) {
        TabUtils.setRequestFromDTO(request, employerUnderwriting, FIELD_MAPPINGS, "EmployerUnderwriting");
    }
}

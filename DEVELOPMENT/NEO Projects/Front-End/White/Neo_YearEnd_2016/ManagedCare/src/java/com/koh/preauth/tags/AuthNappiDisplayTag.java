/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author johanl
 */
public class AuthNappiDisplayTag extends TagSupport {
    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            out.println("<td colspan=\"7\"><table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Provider Type</th><th>Nappi Code</th><th>Nappi Desc</th><th>Item Cost</th>" +
                    "<th>Dosage</th><th>Quantity</th><th>Frequency</th></tr>");

            ArrayList<AuthTariffDetails> atList = (ArrayList<AuthTariffDetails>) session.getAttribute(sessionAttribute);

            if (atList != null && atList.size() != 0) {
                for (int i =1; i <= atList.size(); i++) {
                    AuthTariffDetails ab = atList.get(i-1);
                    String provCat = NeoCommand.service.getNeoManagerBeanPort().getValueFromCodeTableForTableId(59, ab.getProviderType());
                    out.println("<tr>" +
                                    "<td><label class=\"label\">" + provCat + "</label></td> " +
                                    "<td><label class=\"label\">" + ab.getTariffCode() + "</label></td> " +
                                    "<td width=\"200\"><input type=\"text\" value=\"" + ab.getTariffDesc() + "\" readonly /></td> " +
                                    "<td><label class=\"label\">" + ab.getAmount() + "</label></td> " +
                                    "<td><label class=\"label\">" + ab.getDosage() + "</label></td> " +
                                    "<td><label class=\"label\">" + ab.getQuantity() + "</label></td> " +
                                    "<td><label class=\"label\">" + ab.getFrequency() + "</label></td> " +
                               "</tr>");
                }
            }
            out.println("</table></td>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberSearchTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

}

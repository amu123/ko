/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application;

import com.koh.command.NeoCommand;
import com.koh.member.application.command.MemberApplicationSpecificQuestionsCommand;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class MemberConsultantSearchCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            NeoManagerBean port = service.getNeoManagerBeanPort();
            search(port, request, response, context);
        } catch (Exception ex) {
            Logger.getLogger(MemberApplicationSpecificQuestionsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void search(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        String code = request.getParameter("consultantCode");
        String name = request.getParameter("consultantName");
        
        System.out.println("Code " + code);
        System.out.println("Name " + name);
        
        List<KeyValueArray> kva = port.getBrokerConsultantList(code, name);
        
        System.out.println("Consultant Size " + kva.size());

        Object col = MapUtils.getMap(kva);

        request.setAttribute("ConsultantSearchResults", col);
        context.getRequestDispatcher("/MemberApplication/BrokeConsultantrMemberResults.jsp").forward(request, response);
    }

    @Override
    public String getName() {
        return "MemberConsultantSearchCommand";
    }
}

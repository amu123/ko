/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import java.util.Collection;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.NeoUser;
import neo.manager.SecurityResponsibility;

/**
 *
 * @author josephm
 */
public class NeoUserProfileSearchResultTable extends TagSupport {
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
 @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest request = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        
        try {
            out.println("<label class=\"header\">Users Details</label></br></br>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Username</th><th>Name</th><th>Surname</th><th>Email</th>"
                    + "<th>Responsibility Description<th>Action</th></th>"
                    + "</tr>");
            
            Collection<NeoUser> userProfile = (Collection<NeoUser>)session.getAttribute("userProfileList");
            if (userProfile != null && userProfile.size() > 0) {
                String description = "";
                for (NeoUser user : userProfile) {

                    out.print("<tr valign=\"top\">");

                    out.print("<td><label class=\"label\">" + user.getUsername() + "</label></td>");
                    out.print("<td><label class=\"label\">" + user.getName() + "</label></td>");
                    out.print("<td><label class=\"label\">" + user.getSurname() + "</label></td>");
                    out.print("<td><label class=\"label\">" + user.getEmailAddress() + "</label></td>");
                    if (user.getDescription() != null) {
                        out.print("<td><label class=\"label\">" + user.getDescription() + "</label></td>");
                    }else {
                    
                        out.print("<td><label class=\"label\">" + description + "</label></td>");
                    }
                    out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('AllocateUserProfileToSessionCommand', "+ user.getUserId() +")\"; value=\"AllocateUserProfileToSessionCommand\">Select</button></td>");

                    out.print("</tr>");
                    
                    for (SecurityResponsibility resp : user.getSecurityResponsibility()) {
                        
                        //System.out.println("Resp " + resp.getResponsibilityId());

                        session.setAttribute("respId"+user.getUserId(), resp.getResponsibilityId());
                        //System.out.println("The responsibility id is " + resp.getResponsibilityId());

                    }
                }
            }
 
        } catch (java.io.IOException ex) {
            throw new JspException("Error in NeoUserProfileSearchResultTable tag", ex);
        }
        
        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.utils;

import java.io.IOException;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;

/**
 *
 * @author johanl
 */
public class XmlUtils {

    public XmlUtils() {
    }

    public static Document getDocument() {
        Document doc = null;

        //get document builder factory
        DocumentBuilderFactory docBuildFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuild = null;

        //get document builder
        try {
            docBuild = docBuildFactory.newDocumentBuilder();

        } catch (ParserConfigurationException pc) {
            pc.printStackTrace();
        }
        //set document
        doc = docBuild.newDocument();

        return doc;
    }

    public static String getXMLString(Document doc) throws IOException {
		String s = null;
		if (doc!=null) {
			XMLSerializer serializer = new XMLSerializer();
			StringWriter sw = new StringWriter();
			serializer.setOutputCharStream(sw);
			serializer.serialize(doc);
			s = sw.toString();
		}else{
            System.out.println("document - null");
        }
		return s;
	}
}

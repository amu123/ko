/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CoverDetails;
import neo.manager.EAuthCoverDetails;

/**
 *
 * @author Johan-NB
 */
public class ReloadCoverHistoryByDate extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        String coverNumber = "" + session.getAttribute("policyHolderNumber_text");
        System.out.println("coverNumber = " + coverNumber);
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        String startDateStr = request.getParameter("startDate");
        String endDateStr = request.getParameter("endDate");
        System.out.println("endDateStr = " + endDateStr);

        List<EAuthCoverDetails> coverList = (List<EAuthCoverDetails>) session.getAttribute("allDepHistoryDetails");
        List<EAuthCoverDetails> newDepList = new ArrayList<EAuthCoverDetails>();
        if (endDateStr != null && !endDateStr.equalsIgnoreCase("")
                && startDateStr != null && !startDateStr.equalsIgnoreCase("")) {

            Date principalStart = null;
            Date principalEnd = null;
            try {
                principalStart = sdf.parse(startDateStr);
                principalEnd = sdf.parse(endDateStr);

            } catch (ParseException px) {
                px.printStackTrace();
            }

            for (EAuthCoverDetails cd : coverList) {
                
                if (cd.getCoverNumber().equals(coverNumber)) {
                    //System.out.println("cover dep = " + cd.getDependentNumber());
                    Date cdStartDateStr = cd.getCoverStartDate().toGregorianCalendar().getTime();

                    if ((cdStartDateStr.equals(principalStart) || cdStartDateStr.after(principalStart))
                            && (cdStartDateStr.equals(principalEnd) || cdStartDateStr.before(principalEnd))) {
                        newDepList.add(cd);
                    }
                }
            }

            System.out.println("new dep list size = " + newDepList.size());
            session.setAttribute("memberClaimSearchResult", newDepList);

        } else {
            System.out.println("error: xDate is null");
        }

        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher("/Claims/MemberDetails.jsp");
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "ReloadCoverHistoryByDate";
    }
}
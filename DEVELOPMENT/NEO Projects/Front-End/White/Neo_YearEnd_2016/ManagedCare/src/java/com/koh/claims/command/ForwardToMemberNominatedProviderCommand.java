/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author charlh
 */
public class ForwardToMemberNominatedProviderCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(ForwardToMemberNominatedProviderCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        logger.info("Inside ForwardToMemberNominatedProviderCommand");

        ViewMemberNominatedProviderDetailsCommand prov = new ViewMemberNominatedProviderDetailsCommand();
        prov.execute(request, response, context);
        return null;
    }

    @Override
    public String getName() {
        return "ForwardToMemberNominatedProviderCommand";
    }
}
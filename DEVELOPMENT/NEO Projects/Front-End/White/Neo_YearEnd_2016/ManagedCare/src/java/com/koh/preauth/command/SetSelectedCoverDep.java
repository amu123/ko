/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Johan-NB
 */
public class SetSelectedCoverDep extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        PrintWriter out = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        String nextJSP = request.getParameter("onScreen");
        boolean errors = false;
        //do update 
        /*if (nextJSP.equalsIgnoreCase("/PreAuth/GenericAuth_Update.jsp")) {
         String memberNo = "" + session.getAttribute("memberNum_text");
         int depCode = Integer.parseInt("" + session.getAttribute("depListValues"));

         String authPeriod = request.getParameter("authPeriod");
         String authMonthPeriod = request.getParameter("authMonthPeriod");
         session.setAttribute("authPeriod", authPeriod);
         session.setAttribute("authMonthPeriod", authMonthPeriod);

         Calendar cal = Calendar.getInstance();
         cal.setTime(new Date(System.currentTimeMillis()));
         cal.add(Calendar.MONTH, 4);
         Date fDate = cal.getTime();

         int aYear = Integer.parseInt(authPeriod);
         int aMonth = Integer.parseInt(authMonthPeriod);
         int fYear = Integer.parseInt(sdf.format(fDate).substring(0, 4));
         int fMonth = Integer.parseInt(sdf.format(fDate).substring(5, 7));

         if (aYear == fYear && aMonth > fMonth) {
         errors = true;
         session.setAttribute("authMonthPeriod_error", "Invalid Future Date Month Period");
         } else {
         session.setAttribute("authMonthPeriod_error", null);
         }

         if (!errors) {
         Date memberDate = null;
         XMLGregorianCalendar xMemDate = null;
         try {
         memberDate = new SimpleDateFormat("yyyy/MM/dd").parse(authPeriod + "/" + authMonthPeriod + "/01");
         xMemDate = DateTimeUtils.convertDateToXMLGregorianCalendar(memberDate);

         } catch (ParseException ex) {
         Logger.getLogger(SetSelectedCoverDep.class.getName()).log(Level.SEVERE, null, ex);
         }

         CoverDetails cd = port.getDependentForCoverByDate(memberNo, xMemDate, depCode);
         session.setAttribute("covDepListDetails", cd);
         }*/

        //} else {
        String depCode = request.getParameter("depListValues");
        System.out.println("SetSelectedCoverDep depCode selected = " + depCode);
        //int depCode = Integer.parseInt("" + session.getAttribute("depListValues"));

        session.setAttribute("depListValues", depCode);
        String screen = "" + session.getAttribute("onScreen");
        System.out.println("SetSelectedCoverDep screen = " + screen);
        nextJSP = screen;
        //}
        try {
            out = response.getWriter();
            out.println("proceed");
        } catch (IOException ex) {
            Logger.getLogger(SetSelectedCoverDep.class.getName()).log(Level.SEVERE, null, ex);
        }


        /*try {
         RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
         dispatcher.forward(request, response);
         } catch (Exception ex) {
         ex.printStackTrace();
         }*/
        return null;
    }

    @Override
    public String getName() {
        return "SetSelectedCoverDep";
    }
}

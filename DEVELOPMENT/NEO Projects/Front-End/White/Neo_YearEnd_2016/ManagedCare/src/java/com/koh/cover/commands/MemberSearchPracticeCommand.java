/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.koh.auth.dto.PracticeSearchResult;
import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.ProviderDetails;
import neo.manager.ProviderSearchCriteria;

/**
 *
 * @author charlh
 */
public class MemberSearchPracticeCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {        
        
        String practiceNumber = request.getParameter("pracNo");
        String practiceName = request.getParameter("pracName");
        String DSPType = request.getParameter("DSPType");
        
        ProviderSearchCriteria search = new ProviderSearchCriteria();
        
        if (practiceNumber != null && !practiceNumber.equals("")) {
            search.setPracticeNumber(practiceNumber);
        }
        if (practiceName != null && !practiceName.equals("")) {
            search.setPracticeName(practiceName);
        }
        
        search.setEntityTypeID(3);
        
        if (DSPType.equalsIgnoreCase("doctor")) {
            search.setCategory(String.valueOf(1));
        } else if (DSPType.equalsIgnoreCase("dentist")) {
            search.setCategory(String.valueOf(3));
        } else if (DSPType.equalsIgnoreCase("optometrist")) {
            search.setCategory(String.valueOf(2));
        }

        ArrayList<PracticeSearchResult> providers = new ArrayList<PracticeSearchResult>();
        
        try {
            
            List<ProviderDetails> pdList = service.getNeoManagerBeanPort().findAllProvidersByCategory(search);

            for (ProviderDetails pd : pdList) {
                PracticeSearchResult practice = new PracticeSearchResult();
                practice.setPracticeName(pd.getPracticeName());
                practice.setPracticeNumber(pd.getPracticeNumber());
                practice.setPracticeType(pd.getDisciplineType().getValue());
                practice.setPracticeNetwork(pd.getNetworkName());
                providers.add(practice);
            }

            if (pdList.isEmpty()) {
                request.setAttribute("searchPracticeResultMessage", "No results found.");
            } else if (pdList.size() >= 100) {
                request.setAttribute("searchPracticeResultMessage", "Only first 100 results shown. Please refine search.");
            } else {
                request.setAttribute("searchProviderResultMessage", null);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        request.setAttribute("PracticeSearchResults", providers);
        
        try {
            String nextJSP = "/Member/SearchPracticeResults.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "MemberSearchPracticeCommand";
    }
}
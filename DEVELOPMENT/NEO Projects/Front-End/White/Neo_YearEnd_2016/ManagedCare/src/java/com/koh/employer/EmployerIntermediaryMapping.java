/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.employer;

import com.koh.employer.command.TabUtils;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import neo.manager.EmployerBroker;
import neo.manager.NeoUser;

/**
 *
 * @author yuganp
 */
public class EmployerIntermediaryMapping {
    private static final String[][] FIELD_MAPPINGS = {
        {"EmployerGroup_entityId","","no", "int", "EmployerBroker","EmployerEntityId"},
        {"EmployerBroker_brokerRefNo","Broker Ref No.","no", "str", "EmployerBroker","BrokerRefNo"},
        {"EmployerBroker_branchCode","Name of Broker","no", "str", "EmployerBroker","brokerName"},
        {"EmployerBroker_brokerageName","Brokerage Name","no", "str", "EmployerBroker","BrokerageName"},
        {"EmployerBroker_rhRefNo","Resolution Brokerage Code","no", "str", "EmployerBroker","brokerageCode"},
        {"EmployerBroker_tel","Telephone No.","no", "str", "EmployerBroker","BrokerTelNo"},
        {"EmployerBroker_email","Email Address","no", "str", "EmployerBroker","BrokerEmail"},
        {"EmployerBroker_fax","Fax No.","no", "str", "EmployerBroker","BrokerFaxNo"},
        {"EmployerBroker_brokerId","Broker","no", "int", "EmployerBroker","brokerId"},
        {"EmployerBroker_brokerageId","Brokerage","no", "int", "EmployerBroker","brokerageId"}
    };
    
    private static final Map<String,String[]> FIELD_MAP;

    static {
        HashMap hm = new HashMap<String,String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }
    
    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
        return errors;
    }
    
    public static EmployerBroker getEmployerBroker(HttpServletRequest request,NeoUser neoUser) {
        EmployerBroker eg = (EmployerBroker)TabUtils.getDTOFromRequest(request, EmployerBroker.class, FIELD_MAPPINGS, "EmployerBroker");
        eg.setSecurityGroupId(neoUser.getSecurityGroupId());
        eg.setLastUpdatedBy(neoUser.getUserId());
        return eg;
    }

    public static void setEmployerBroker(HttpServletRequest request, EmployerBroker employerBroker) {
        TabUtils.setRequestFromDTO(request, employerBroker, FIELD_MAPPINGS, "EmployerBroker");
    }

    
}

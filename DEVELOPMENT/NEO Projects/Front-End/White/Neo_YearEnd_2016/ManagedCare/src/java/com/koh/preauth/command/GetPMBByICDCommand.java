/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.FormatUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthPMBDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class GetPMBByICDCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        PrintWriter out = null;

        String pICD = "" + session.getAttribute("primaryICD_text");
        String sICD = "" + session.getAttribute("secondaryICD_text");
        String cICD = "" + session.getAttribute("coMorbidityICD_text");
        String authType = "" + session.getAttribute("authType");

        String admitDate = "" + session.getAttribute("admissionDate");
        String admitTime = "" + session.getAttribute("admissionTime");
        XMLGregorianCalendar admissionDateTime = null;

        if ((admitDate != null && !admitDate.equalsIgnoreCase("") && !admitDate.equalsIgnoreCase("null"))
                && (admitTime != null && !admitTime.equalsIgnoreCase("") && !admitTime.equalsIgnoreCase("null"))) {
            try {
                Date dateAdd = FormatUtils.dateTimeNoSecFormat.parse(admitDate + " " + admitTime);
                admissionDateTime = DateTimeUtils.convertDateToXMLGregorianCalendar(dateAdd);
            } catch (ParseException ex) {
                Logger.getLogger(GetPMBByICDCommand.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        String primary = "";
        String secondary = "";
        String coMorbidity = "";

        if (pICD != null && !pICD.equalsIgnoreCase("null") && !pICD.equalsIgnoreCase("")) {
            primary = pICD;
        }
        if (sICD != null && !sICD.equalsIgnoreCase("null") && !sICD.equalsIgnoreCase("")) {
            secondary = sICD;
        }
        if (cICD != null && !cICD.equalsIgnoreCase("null") && !cICD.equalsIgnoreCase("")) {
            coMorbidity = cICD;
        }

        System.out.println("primary = " + primary);
        System.out.println("secondary = " + secondary);
        System.out.println("coMorbidity = " + coMorbidity);

        List<AuthPMBDetails> pmbList = port.getPMBDetailsByICD(primary, secondary, coMorbidity, admissionDateTime);
        if (!pmbList.isEmpty()) {
            System.out.println("pmbList size = " + pmbList.size());
            session.setAttribute("authPMBDetails", pmbList);
            session.setAttribute("pmb", "1");
        } else {
            session.removeAttribute("authPMBDetails");
            session.removeAttribute("savedAuthPMBs");
            session.setAttribute("pmb", "2");
        }
        try {
            out = response.getWriter();
            out.println("Done|");
        } catch (IOException ex) {
            Logger.getLogger(GetPMBByICDCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "GetPMBByICDCommand";
    }
}

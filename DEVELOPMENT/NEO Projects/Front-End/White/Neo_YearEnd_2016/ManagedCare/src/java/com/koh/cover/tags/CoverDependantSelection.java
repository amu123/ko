/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.tags;

import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CoverDetails;
import org.apache.log4j.Logger;

/**
 *
 * @author johanl
 */
public class CoverDependantSelection extends TagSupport {
    private String sessionAttribute;
    private static Logger logger = Logger.getLogger(CoverDependantSelection.class);

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute(sessionAttribute);
            
            logger.info("Total Dependants returned : " + cdList.size());

            StringBuilder sb = new StringBuilder();
            
            sb.append("<tr id='dependantListRow' style='visibility: hidden'>");
            sb.append("<td><select id='dependantSelect' name='dependantSelect'>");
            
            //ADD BLANK
            sb.append("<option value='99'></option>");
             
            if (cdList != null && !cdList.isEmpty()) {
                for (CoverDetails cd : cdList) {
                    sb.append("<option value='")
                            .append(cd.getDependentNumber())
                            .append("' >")
                            .append(cd.getDependentNumber())
                            .append(":")
                            .append(cd.getName())
                            .append("</option>");
                }
            }
            
            //CLOSE
            sb.append("</select></td></tr>");
            out.println(sb.toString());

        } catch (java.io.IOException ex) {
            throw new JspException("Error in CoverDependantSelection tag", ex);
        }

        return super.doEndTag();
    }

    public String getSessionAttribute() {
        return sessionAttribute;
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }
    
    
}

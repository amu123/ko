/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author johanl
 */
public class AuthNappiSearchResult extends TagSupport {
    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        List<AuthTariffDetails> nappiList = (List<AuthTariffDetails>) session.getAttribute("PreAuthNappiSearchList");
        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Nappi Code</th><th>Nappi Desc</th><th>Nappi Amount</th><th>Dosage</th><th>Quantity</th><th></th></tr>");

            if (nappiList != null) {
                for (AuthTariffDetails nap : nappiList) {

                    String quan = nap.getQuantity();
                    String dos = nap.getDosage();

                    if(quan == null || quan.trim().equalsIgnoreCase("null")){
                        quan = "";
                    }
                    if(dos == null || dos.trim().equalsIgnoreCase("null")){
                        dos = "";
                    }

                    out.println("<tr><form>" +
                            "<td>" +
                                "<label class=\"label\">" + nap.getTariffCode() + "</label>" +
                                "<input type=\"hidden\" name=\"napCode\" value=\"" + nap.getTariffCode() + "\"/>" +
                            "</td>" +

                            "<td width=\"200\">" +
                                "<label class=\"label\">" + nap.getTariffDesc() + "</label>" +
                                "<input type=\"hidden\" name=\"napDesc\" value=\"" + nap.getTariffDesc() + "\"/>" +
                            "</td>" +

                            "<td>" +
                                "<label class=\"label\">" + nap.getAmount() + "</label>" +
                                "<input type=\"hidden\" name=\"tariffAmount\" value=\"" + nap.getAmount() + "\"/>" +
                            "</td>" +

                            "<td>" +
                                "<label class=\"label\">" + dos + "</label>" +
                                "<input type=\"hidden\" name=\"tariffDosage\" value=\"" + dos + "\"/>" +
                            "</td>" +

                            "<td>" +
                                "<label class=\"label\">" + quan + "</label>" +
                                "<input type=\"hidden\" name=\"tariffQuan\" value=\"" + quan + "\"/>" +
                            "</td>" +

                            "<td>" +
                                "<button name=\"opperation\" type=\"submit\" " + javaScript + " value=\"" + commandName + "\">Select</button>" +
                            "</td>" +

                            "</form></tr>");
                }
            }
            out.println("</table>");

        } catch (IOException io) {
            io.printStackTrace();
        }

        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.MajorDepressionBiometrics;

/**
 *
 * @author princes
 */
public class BiometricMajorDepressionTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String javascript;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        List<MajorDepressionBiometrics> getDepression = (List<MajorDepressionBiometrics>) session.getAttribute("getDepression");
        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        String dateMeasured;
        try {

            out.print("<tr>");
            out.print("<th scope=\"col\">Date Measured</th>");
            out.print("<th scope=\"col\">Height</th>");
            out.print("<th scope=\"col\">Weight</th>");
            out.print("<th scope=\"col\">Blood Pressure Systolic</th>");
            out.print("<th scope=\"col\">Blood Pressure Diastolic</th>");
            out.print("<th scope=\"col\">HAM-D Score</th>");
            out.print("<th scope=\"col\">Select</th>");

//            out.print("<th scope=\"col\">Date Measured</th>");
//            out.print("<th scope=\"col\">ICD10</th>");
//            out.print("<th scope=\"col\">Excercise</th>");            
//            out.print("<th scope=\"col\">Ex Smoker Stopped</th>");
//            out.print("<th scope=\"col\">Alchohol Units</th>");
//            out.print("<th scope=\"col\">HAMD Score</th>");
//            out.print("<th scope=\"col\">Source</th>");
//            out.print("<th scope=\"col\">Notes</th>");
            out.print("</tr>");
            if (getDepression != null && getDepression.size() > 0) {
                for (MajorDepressionBiometrics d : getDepression) {

                    out.print("<tr>");
                    if (d.getDateMeasured() != null) {
                        Date mDate = d.getDateMeasured().toGregorianCalendar().getTime();
                        dateMeasured = formatter.format(mDate);
                        out.print("<td><center><label class=\"label\">" + dateMeasured + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getHeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + d.getHeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getWeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + d.getWeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getBloodPressureSystolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + d.getBloodPressureSystolic() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getBloodPressureDiastolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + d.getBloodPressureDiastolic() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getHamdScore() != 0) {
                        out.print("<td><center><label class=\"label\">" + d.getHamdScore() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("<td><center><button name=\"opperation\" type=\"button\" " + javascript + " value=\"" + d.getBiometricsId() + "\">Select</button></center></td>");

//                    if (d.getDateMeasured() != null) {
//                        Date mDate = d.getDateMeasured().toGregorianCalendar().getTime();
//                        dateMeasured = formatter.format(mDate);
//                        out.print("<td>" + dateMeasured + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (d.getIcd10Lookup() != null) {
//                        out.print("<td>" + d.getIcd10Lookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getExercisePerWeek() != 0) {
//                        out.print("<td>" + d.getExercisePerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    
//                    
//                    if (d.getYearsSinceStopped() > 0) {
//                        out.print("<td>" + d.getYearsSinceStopped() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getAlcoholUnitsPerWeek() != 0) {
//                        out.print("<td>" + d.getAlcoholUnitsPerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getHamdScore() != 0) {
//                        out.print("<td>" + d.getHamdScore() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getSourceLookup() != null) {
//                        out.print("<td>" + d.getSourceLookup().getValue()  + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getDetail() != null) {
//                        out.print("<td>" + d.getDetail() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
                    out.print("</tr>");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

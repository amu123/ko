/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import com.agile.tags.TagMethods;
import java.util.Collection;
import java.util.HashMap;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author princes
 */
public class LabelNoeUserByResposibilityDropDown extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String elementName;
    private String displayName;
    private String javaScript;
    private String resposibilityId;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        int respId = new Integer(resposibilityId);
        if (respId == 20) {
            try {
                out.println("<td><label class=\"label\">" + displayName + "</label></td>");
                out.println("<td><select name=\"" + elementName + "\" id=\"" + elementName + "\"");
                if (javaScript != null) {
                    out.print(javaScript);
                }
                out.println(">");
                String selectedBox = "" + session.getAttribute(elementName);
                //HashMap<Integer, String> users = (HashMap<Integer, String>) TagMethods.getUsersByRespId(20);

                Collection<String> userList = TagMethods.getUsersByRespId(20);
                Collection<String> adminList = TagMethods.getUsersByRespId(21);
                userList.addAll(adminList);
                session.setAttribute("NeoUserMap", userList);
                out.println("<option value=\"0\" selected ></option>");
                for (String s : userList) {

                    String[] values = s.split("\\|");
                    int idVal = Integer.parseInt(values[0]);
                    String name = values[1];
                    //add values to map

                    if (name.equalsIgnoreCase(selectedBox)) {
                        out.println("<option value=\"" + idVal + "\" selected >" + name + "</option>");
                    } else {
                        out.println("<option value=\"" + idVal + "\">" + name + "</option>");
                    }

                    //userMap.put(id, name);
                }
                out.println("</select></td>");

            } catch (java.io.IOException ex) {
                throw new JspException("Error in LabelLookupValueDropDown tag", ex);
            }
        } else {
            try {
                out.println("<td><label class=\"label\">" + displayName + "</label></td>");
                out.println("<td><select name=\"" + elementName + "\" id=\"" + elementName + "\"");
                if (javaScript != null) {
                    out.print(javaScript);
                }
                out.println(">");
                String selectedBox = "" + session.getAttribute(elementName);
                HashMap<Integer, String> users = (HashMap<Integer, String>) TagMethods.getMultipleUsersRespForPDC();
                session.setAttribute("NeoUserMap", users);
                Collection<Integer> uName = users.keySet();
                out.println("<option value=\"0\" selected ></option>");
                for (Integer i : uName) {
                    if (users.get(i).equalsIgnoreCase(selectedBox)) {
                        out.println("<option value=\"" + i + "\" selected >" + users.get(i) + "</option>");
                    } else {
                        out.println("<option value=\"" + i + "\">" + users.get(i) + "</option>");
                    }
                }
                out.println("</select></td>");

            } catch (java.io.IOException ex) {
                throw new JspException("Error in LabelLookupValueDropDown tag", ex);
            }
        }
        return super.doEndTag();
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setResposibilityId(String resposibilityId) {
        this.resposibilityId = resposibilityId;
    }
}

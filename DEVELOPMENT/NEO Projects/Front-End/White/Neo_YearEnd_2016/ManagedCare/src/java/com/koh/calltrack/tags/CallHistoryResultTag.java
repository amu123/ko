/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.calltrack.tags;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CallWorkbenchDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author josephm
 */
public class CallHistoryResultTag extends TagSupport {

    private String commandName;
    private String javaScript;
    HashMap<String, String> callMap = null;
    HashMap<String, String> queryMap = null;

    /**
     * Called by the container to invoke this tag. The implementation of this method is provided by the tag library developer, and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {

        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        ServletRequest request = pageContext.getRequest();
        String client = String.valueOf(pageContext.getServletContext().getAttribute("Client"));
        String history = "" + request.getAttribute("CallHistorySearch");
        String queries = "";
        System.out.println("CallWorkbench Start");
        //System.out.println("hisoty indc = "+history);
        List<CallWorkbenchDetails> cwList = new ArrayList<CallWorkbenchDetails>();
        if (history != null && !history.trim().equalsIgnoreCase("")
                && history.trim().equals("true")) {
            cwList = (List<CallWorkbenchDetails>) session.getAttribute("searchCallHistory");
            //cwList = (List<CallWorkbenchDetails>) session.getAttribute("WorkbenchCallTrackDetail");

        } else {
            String callWork = "" + request.getAttribute("CallWorkbench");
            if (callWork != null && !callWork.trim().equalsIgnoreCase("")
                    && callWork.trim().equals("true")) {
                //cwList = port.getCallWorkBenchDetails();
                cwList = (List<CallWorkbenchDetails>) session.getAttribute("WorkbenchDetails");
            }
        }

        try {

            System.out.println("commandName = " + commandName);

            String message = (String) request.getAttribute("callSearchResultsMessage");

            if (message != null && !message.equalsIgnoreCase("")) {
                out.println("<label class=\"red\">" + message + "</label></br></br>");
            }

            out.println("<table width=\"700\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Nr</th><th>Call Number</th><th>Call Status</th><th>Queried</th><th>Assigned User</th><th>Date last attended to</th><th></th></tr>");

            if (cwList == null) {

            } else {
                for (CallWorkbenchDetails cw : cwList) {
                    queries = cw.getCallQueries();
                    String qName = getCallQueryLookups(queries, port);
                    String calls = cw.getCallStatus();
                    String callTypes = getCallLookups(calls, port);
                    int userId = 0;
                    if (callTypes != null && (callTypes.equalsIgnoreCase("Open") || callTypes.equalsIgnoreCase("Closed"))) {
                        userId = cw.getResoUserId();
                    } else {
                        userId = cw.getRefferedUser();
                    }
                    NeoUser user = port.getUserSafeDetailsById(userId);
                    cw.setResoUserName(user.getUsername());
                    out.print("<tr><form>");
                    out.print("<td><label class=\"label\">" + cw.getCallTrackId() + "</label><input type=\"hidden\" name=\"trackId\" value=\"" + cw.getCallTrackId() + "\"/></td> ");
                    out.print("<td><label class=\"label\"> " + cw.getCallRefNo() + "</label></td> ");
                    out.print("<td><label class=\"label\"> " + callTypes + "</label> </td> ");
                    out.print("<td><label class=\"label\">" + qName + "</label></td> ");
                    out.print("<td><label class=\"label\"> " + cw.getResoUserName() + "</label> </td> ");
                    out.println("<td><label class=\"label\"> " + cw.getLastUpdate() + "</label></td>");
                    if (client != null && client.equalsIgnoreCase("Sechaba")) {
                        out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('WorkflowContentCommand', " + cw.getCallTrackId() + ")\"; value=\"WorkflowContentCommand\">View</button></td></td></form></tr>");
//                        out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('UpdateCallTrackingCommand', " + cw.getCallTrackId() + ")\"; value=\"UpdateCallTrackingCommand\">View</button></td></td></form></tr>");
                    }
                    if (client != null && client.equalsIgnoreCase("Agility")) {
                        out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('UpdateCallTrackingCommand', " + cw.getCallTrackId() + ")\"; value=\"UpdateCallTrackingCommand\">View</button></td></td></form></tr>");
                    }

                }
                out.println("</table>");
            }

        } catch (IOException ex) {
            Logger.getLogger(CallWorkbenchResultTableTag.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("We are on the exception " + ex.getMessage());
        }
        System.out.println("CallWorkbench End");
        return super.doEndTag();

    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavascript(String javascript) {
        this.javaScript = javascript;
    }

    private String getCallLookups(String callId, NeoManagerBean port) {

        String call = null;
        if (callMap == null || callMap.isEmpty()) {
            callMap = new HashMap<String, String>();
            Collection<LookupValue> lookupList = port.getCodeTable(105);
            for (LookupValue v : lookupList) {
                callMap.put(v.getId(), v.getValue());
            }
        }

        call = callMap.get(callId);

        return call;
    }

    private String getCallQueryLookups(String queries, NeoManagerBean port) {
        String qNameListTag = "";
        ArrayList<String> queryNames = new ArrayList();

        if (queries != null && queries.contains("QC")) {
            if (queryMap == null || queryMap.isEmpty()) {
                queryMap = new HashMap<String, String>();
                Collection<LookupValue> lookupList = port.getCodeTable(151);
                for (LookupValue lv : lookupList) {
                    queryMap.put(lv.getId(), lv.getValue());
                }
            }

            String[] selectedQs = queries.split(",");
            for (String s : selectedQs) {
                String q = queryMap.get(s);
                queryNames.add(q);
            }
        }

        if (queryNames != null) {
            qNameListTag = "<ol>";
            for (String qn : queryNames) {
                if (qn != null) {
                    qNameListTag += "<li><label class=\"label\">" + qn + "</label></li>";
                }
            }
            qNameListTag += "</ol>";

        }
        return qNameListTag;
    }

}

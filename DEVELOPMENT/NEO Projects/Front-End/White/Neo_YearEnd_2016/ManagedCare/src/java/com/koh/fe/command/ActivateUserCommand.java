/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoUser;

/**
 *
 * @author gerritj
 */
public class ActivateUserCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        
        if(session.getAttribute("userid") == null){
            session.setAttribute("userid", request.getParameter("user_id"));
        }

        int user_id; 
        
        if(request.getParameter("user_id") == null || request.getParameter("user_id") == "" || request.getParameter("user_id") == "null"){
            user_id = Integer.parseInt(session.getAttribute("userid").toString());
        }else{
            user_id = new Integer(request.getParameter("user_id"));
        }
        
        int workitem_id = new Integer(request.getParameter("workitem_id"));
        ArrayList<Integer> resp = new ArrayList<Integer>();
        Enumeration e = request.getParameterNames();
        
        boolean fail1 = false;
        boolean fail2 = true;
        while (e.hasMoreElements()) {
            String pName = "" + e.nextElement();
            if(request.getParameter(pName) == null || request.getParameter(pName) == "" || request.getParameter(pName) == "null"){
                if(session.getAttribute("name") == null){
                    fail1 = true;
                }
            }
            if (pName.contains("resp_")) {
                resp.add(new Integer(request.getParameter(pName)));
                fail2 = false;
            }
        }
        
        String nextJSP;
        try {
            if(fail1 == true || fail2 == true){
                request.setAttribute("error", "No Responsibilities Selected");
                ActivateUserWLCommand getUser = new ActivateUserWLCommand();
                request.setAttribute("external_reference", user_id);
                getUser.execute(request, response, context);
            }else{
                service.getNeoManagerBeanPort().activateuserAndAssignResponsibilities(user_id, workitem_id, resp, user);
                request.setAttribute("success", "User activated");
                nextJSP = "/SystemAdmin/ActivateUserWL.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ActivateUserCommand";
    }
}

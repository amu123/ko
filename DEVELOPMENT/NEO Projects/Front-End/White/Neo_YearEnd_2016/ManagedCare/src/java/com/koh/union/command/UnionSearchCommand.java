/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.union.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.Union;

/**
 *
 * @author shimanem
 */
public class UnionSearchCommand extends NeoCommand {

    private static final Logger logger = Logger.getLogger(UnionSearchCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        try {
            String unionNumber = request.getParameter("_unionNumber");
            String unionName = request. getParameter("_unionName");

            logger.log(Level.INFO, ">> *** Union Number : {0}", unionNumber);
            logger.log(Level.INFO, ">> *** Union Name : {0}", unionName);

            Union union = new Union();
            if (unionNumber != null && !unionNumber.equals("")) {
                union.setUnionNumber(unionNumber);
            }
            if (unionName != null && !unionName.equals("")) {
                union.setUnionName(unionName);
            }

            ArrayList<Union> unionList = (ArrayList<Union>) port.getAllUnions(union);

            if (unionList != null) {
                session.setAttribute("unionListDetails", unionList);
                context.getRequestDispatcher("/Union/searchUnion.jsp").forward(request, response);
            } else {
                session.setAttribute("unionListDetails", null);
            }

        } catch (ServletException ex) {
            logger.info(">> Error Occured while retrieving Union List <<");
        } catch (IOException ex) {
            logger.info(">> Error Occured while retrieving Union List <<");
        }

        return null;
    }

    @Override
    public String getName() {
        return "UnionSearchCommand";
    }

}

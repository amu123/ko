/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 * For super user to update other people's details
 * @author josephm
 */
public class UpdateMyProfileCommand extends NeoCommand {
    
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        System.out.println("Inside UpdateMyProfileCommand");
        boolean checkError = false;
        boolean checkMissingField = false;
        NeoManagerBean port = service.getNeoManagerBeanPort();

        HttpSession session = request.getSession();

        NeoUser loggedInUser = (NeoUser) session.getAttribute("persist_user");

        NeoUser updatingUser = new NeoUser();

        try {
            PrintWriter out = response.getWriter();

            String name = request.getParameter("name");
            if (name != null && !name.equalsIgnoreCase("")) {
                updatingUser.setName(name);
            } else {

                checkMissingField = true;
            }
            String surname = request.getParameter("surname");
            if (surname != null && !surname.equalsIgnoreCase("")) {
                updatingUser.setSurname(surname);
            } else {

                checkMissingField = true;
            }
            String email = request.getParameter("email");
            if (email != null && !email.equalsIgnoreCase("")) {
                updatingUser.setEmailAddress(email);
            } else {

                checkMissingField = true;
            }

            if (checkMissingField) {

                session.setAttribute("name_error", "All the fields are required");
                this.saveScreenToSession(request);
            } else {

                //checkError = port.validateUserDetailsByCriteria(updatingUser);

                if (checkError) {

                    out.println("<html>");
                    out.println("<head>");
                    out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<center>");
                    out.println("<table cellspacing=\"4\">");
                    out.println("<tr>");
                    out.println("<td><label class=\"error\"><h2>Duplicate details</h2></label></td>");
                    out.println("</tr>");
                    out.println("</table>");
                    out.println("<p> </p>");
                    out.println("</center>");
                    out.println("</body>");
                    out.println("</html>");
                } else {

                    port.updateUserDetails(loggedInUser.getUserId(), name, surname, email);
                    loggedInUser.setName(name);
                    loggedInUser.setSurname(surname);
                    loggedInUser.setEmailAddress(email);

                    out.println("<html>");
                    out.println("<head>");
                    out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<center>");
                    out.println("<table cellspacing=\"4\">");
                    out.println("<tr>");
                    out.println("<td><label class=\"header\">User details successfully updated</label></td>");
                    out.println("</tr>");
                    out.println("</table>");
                    out.println("<p> </p>");
                    out.println("</center>");
                    out.println("</body>");
                    out.println("</html>");
                }
            }
            response.setHeader("Refresh", "5; URL=/ManagedCare/SystemAdmin/UpdateMyProfile.jsp");

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }
    
    @Override
    public String getName() {
    
        return "UpdateMyProfileCommand";
    }
    
}
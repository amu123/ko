/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.employer.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import net.sf.json.JSONArray;
import org.apache.log4j.Logger;

/**
 *
 * @author yuganp
 */
public class MemberEmployerCommand extends NeoCommand{
    
    private static final Logger logger = Logger.getLogger(MemberEmployerCommand.class);
    
    private PrintWriter out = null;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
         HttpSession session = request.getSession();
        
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        int memberEntityId = (Integer) session.getAttribute("memberCoverEntityId");
        
        logger.info("Current Member Entity Id  : " + memberEntityId);
        
        try {
           
            List<LookupValue> employerList = port.getEmployerLookup();
            
            logger.info("Total employees returned : " + employerList.size());
            
            JSONArray array = new JSONArray();
            array.addAll(employerList);
            
            logger.info("Total employees as json : " + array.toString());
            
            out = response.getWriter();
            out.println(array.toString());
            
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            
            out.close();
        }
                
        return null;
    }

    @Override
    public String getName() {
        return "MemberEmployerCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.XmlUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CoverDetails;
import neo.manager.CoverProductDetails;
import neo.manager.CoverSearchCriteria;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author johanl
 */
public class GetCoverDetailsByNumberWithDate extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter out = null;
        HttpSession session = request.getSession();
        String xmlStr = "";
        String memberNo = request.getParameter("number");
        System.out.println("memberNo = " + memberNo);
        String dateStr = request.getParameter("date");
        
        if (memberNo != null && !memberNo.trim().equalsIgnoreCase("") &&
                dateStr != null && !dateStr.trim().equalsIgnoreCase("")) {
            
            Date date = null;
            XMLGregorianCalendar xDate = null;
            try{
                date = new SimpleDateFormat("yyyy/MM/dd").parse(dateStr);
                xDate = DateTimeUtils.convertDateToXMLGregorianCalendar(date);
                
            }catch(ParseException px){
                px.printStackTrace();
            }
            
            List<CoverDetails> cdList = service.getNeoManagerBeanPort().getCoverDetailsByCoverNumberByDate(memberNo, xDate);
            
            if (cdList != null && cdList.isEmpty() == false) {
                session.setAttribute("covMemListDetails", cdList);

                //set option
                CoverProductDetails prodDetails = service.getNeoManagerBeanPort().getCoverProductDetailsByCoverNumberByDate(memberNo, xDate);

                response.setHeader("Cache-Control", "no-cache");
                response.setContentType("text/xml");
                //get new document
                @SuppressWarnings("static-access")
                Document doc = new XmlUtils().getDocument();
                //create xml elements
                Element root = doc.createElement("EAuthCoverDetails");
                //add root to document
                doc.appendChild(root);
                try {
                    out = response.getWriter();
                    if (cdList.size() != 0) {
                        //response.setStatus(200);

                        //set product & option names
                        Element prodDetail = doc.createElement("ProductDetails");
                        Element prodName = doc.createElement("ProductName");
                        Element optName = doc.createElement("OptionName");

                        //set product details content
                        prodName.setTextContent(prodDetails.getProductName());
                        optName.setTextContent(prodDetails.getOptionName());
                        //set product detail ids to session
                        session.setAttribute("scheme", prodDetails.getProductId());
                        session.setAttribute("schemeOption", prodDetails.getOptionId());
                        session.setAttribute("schemeName", prodDetails.getProductName());
                        session.setAttribute("schemeOptionName", prodDetails.getOptionName());
                        //add elements to product details
                        prodDetail.appendChild(prodName);
                        prodDetail.appendChild(optName);
                        //add prodDetails element to root
                        root.appendChild(prodDetail);

                        Element member = doc.createElement("MemberDetails");
                        for (CoverDetails cd : cdList) {
                            //create member detail string
                            String dob = new SimpleDateFormat("yyyy/MM/dd").format(cd.getDateOfBirth().toGregorianCalendar().getTime());
                            String depType = cd.getDependentType().substring(0, 1);
                            //String ecDepInfo = cd.getDependentNumber() + "|" + cd.getName() + "-" + cd.getSurname() + "-" + cd.getDependentType() + "-" + dob;
                            String ecDepInfo = cd.getDependentNumber() + "|" + depType + "-" + cd.getName() + "-" + dob;
                            ecDepInfo = ecDepInfo.replace(" Member", "");
                            //create info element
                            Element depInfo = doc.createElement("DependantInfo");
                            depInfo.setTextContent(ecDepInfo);
                            //add element to root
                            member.appendChild(depInfo);
                        }
                        root.appendChild(member);
                    } else {
                        root.setTextContent("ERROR|Member not found");
                    }
                    //return xml object
                    try {
                        xmlStr = XmlUtils.getXMLString(doc);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                        xmlStr = "";

                    }
                    out.println(xmlStr);
                } catch (IOException io) {
                    io.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetCoverDetailsByNumberWithDate";
    }
}

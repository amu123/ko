/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ContactDetails;
import neo.manager.CoverDetails;
import neo.manager.CoverPDCDetails;
import neo.manager.EntityTimeDetails;
import neo.manager.EventDetails;
import neo.manager.LookupValue;
import neo.manager.MainList;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.PdcSearchCriteria;
import neo.manager.PersonDetails;
import neo.manager.WorkList;

/**
 *
 * @author princes
 */
public class ViewWorkBenchCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        System.out.println("entered ViewWorkBenchCommand");

        try {

            //Clearing session from MainList attributes
            session.removeAttribute("source");
            session.removeAttribute("sourceRisk");
            session.removeAttribute("assignedDate");
            session.removeAttribute("riskRating");
            session.removeAttribute("eventStatus");
            //session.removeAttribute("workListId");
            //session.removeAttribute("eventId");
            session.removeAttribute("pdcType");
            session.removeAttribute("pdcTypeRisk");
            session.removeAttribute("descrition");

            String dependentNumber = request.getParameter("dependentNumber");
            String workListID = request.getParameter("workListID");
            String eventIdParam = request.getParameter("EVENT_ID");
            NeoUser user = (NeoUser) session.getAttribute("persist_user");

            if (workListID == null || workListID.trim().equals("") || workListID.trim().equalsIgnoreCase("null")) {
                workListID = String.valueOf(session.getAttribute("workListId"));
            } else {
                session.setAttribute("workListId", workListID);
                System.out.println(">> WorkListID from WorkBench : " + workListID);
            }

            Integer eventID = null;
            System.out.println("EVENT_ID: " + eventIdParam);
            if (eventIdParam != null && !eventIdParam.equalsIgnoreCase("") && !eventIdParam.equalsIgnoreCase("null")) {
                eventID = new Integer(eventIdParam.trim());
            } else {
                String eventIDSession = "" + session.getAttribute("eventId");
                if(eventIDSession != null && !eventIDSession.equals("") && !eventIDSession.equalsIgnoreCase("null")){
                    eventID = new Integer(eventIDSession);
                }else{
                    eventID = new Integer("0");
                }
            }
            session.setAttribute("eventId", eventID);

            if (dependentNumber == null) {
                dependentNumber = request.getParameter("ID_text");
                if(dependentNumber == null || dependentNumber.equals("")){
                    dependentNumber = "" + session.getAttribute("dependentNumber");
                }
            }

            System.out.println("dependentNumber: " + dependentNumber);
            session.setAttribute("listIndex", workListID);
            session.setAttribute("dependentNumber", dependentNumber);
            session.setAttribute("dependantNumber", dependentNumber);
            session.setAttribute("searchView", null);
            String nextJSP = null;

            NeoManagerBean port = service.getNeoManagerBeanPort();

            if (dependentNumber != null) {
                int depNo = Integer.parseInt(dependentNumber);
                List<MainList> work = (ArrayList<MainList>) session.getAttribute("mainList");
                CoverDetails cv = null;
                MainList mainList = null;

                System.out.println("work.size(): " + work.size());

                if (session.getAttribute("MemberCoverDetails") != null) {
                    List<CoverDetails> covList = (List<CoverDetails>) session.getAttribute("MemberCoverDetails");

                    System.out.println("covList.size(): " + covList.size());

                    for (CoverDetails mn : covList) {
                        if (depNo == mn.getDependentNumber()) {
                            cv = mn;
                            break;
                        }
                    }

                    if (cv != null) {
                        PdcSearchCriteria search = new PdcSearchCriteria();
                        if(workListID != null && !workListID.equals("") && !workListID.equalsIgnoreCase("null")){
                           search.setWorkListID(new Integer(workListID)); 
                        }else{
                           search.setWorkListID(0);
                        }
                        
                        search.setCoverNumber(cv.getCoverNumber());
                        search.setDependentNumber(cv.getDependentNumber());
                        System.out.println(">>>>>>>>>>>>> Case 1 - Dep Code: " + cv.getDependentNumber());
                        work = port.findWorkbenchDetailsByCriteria(search, user, ""+session.getAttribute("persist_user_pdcAdmin"));

                        for (MainList mnl : work) {
                            if (mnl != null && mnl.getDependentNumber() == cv.getDependentNumber()) {
                                mainList = mnl;
                                break;
                            }
                        }
                    }

                } else {
                    System.out.println(">>>>>>>>>>>>> Case 2 - dep Code: " + depNo);
                    for (MainList wrk : work) {
                        boolean match = wrk.getDependentNumber() == depNo;
                        System.out.println("Match: (" + match + ") :: wrk.getDependentNumber()= " + wrk.getDependentNumber() + ", depNo= " + depNo);
                        if (wrk.getDependentNumber() == depNo) {
                            mainList = wrk;
                            break;
                        }
                    }
                }

                EventDetails ed = null;
                if (mainList != null) {
                    if (mainList.getEventId() != 0) {
                        eventID = mainList.getEventId();
                    } else {
                        mainList.setEventId(eventID);
                    }

                    System.out.println("EventID: " + eventID);
                    ed = port.getEventDetailsByEventId(eventID);
                    CoverPDCDetails list = new CoverPDCDetails();
                    
                    if(ed != null && ed.getEventId() > 0){
                        System.out.println("ed.getEventId(): " + ed.getEventId() + ", ed.getCoverNumber(): " + ed.getCoverNumber() + ", ed.getDependentNumber(): " + ed.getDependentNumber());
                        session.setAttribute("eventId", ed.getEventId());
                        list = port.getCoverProductDetailsByCoverNumberForPDC(ed.getCoverNumber(), ed.getDependentNumber());
                    }else{
                        String coverNum = "" + request.getParameter("coverNumber");
                        System.out.println("COVER NUMBER = " + coverNum);
                        if(coverNum == null || coverNum.equals("") || coverNum.equalsIgnoreCase("null")){
                            coverNum = "" + session.getAttribute("policyNumber");
                        }
                        list = port.getCoverProductDetailsByCoverNumberForPDC(coverNum, new Integer(dependentNumber));
                    }

                    System.out.println("list.getCoverNumber(): " + list.getCoverNumber());
                    session.setAttribute("policyNumber",list.getCoverNumber());
                    session.setAttribute("memberNum_text", list.getCoverNumber());
                    session.setAttribute("memberNumber_text", list.getCoverNumber());
                    String s;
                    Format formatter = new SimpleDateFormat("yyyy/MM/dd");

                    Date endDate = list.getEndDate().toGregorianCalendar().getTime();
                    s = formatter.format(endDate);

                    if (!s.equals("2999/12/31")) {
                        System.out.println("Member is not active");
                        session.setAttribute("activeMember", "Member is not active");
                    } else {
                        System.out.println("Member is active");
                        session.setAttribute("activeMember", "");
                    }

                    System.out.println("list.getEndDate() " + s);

                    if (list.getPatientName() != null) {
                        String a = list.getPatientName().substring(1, list.getPatientName().length());
                        String b = list.getPatientName().substring(0, 1);
                        String c = b + a.toLowerCase();
                        session.setAttribute("patientName", c);
                    }

                    if (list.getPatientSurname() != null) {
                        String d = list.getPatientSurname().substring(1, list.getPatientSurname().length());
                        String e = list.getPatientSurname().substring(0, 1);
                        String f = e + d.toLowerCase();
                        session.setAttribute("surname", f);
                    }

                    if (list.getPatientName() != null) {

//                        session.setAttribute("workListId", mainList.getWorklistID());
//                        session.setAttribute("eventId", ed.getEventId());
                        if(ed != null && ed.getEventId() > 0){
                            session.setAttribute("EventIDForAssignHistory", ed.getEventId());
                            if(ed.getEventType() != null){
                                session.setAttribute("pdcTypeRisk", ed.getEventType().getId());
                                session.setAttribute("pdcType", ed.getEventType().getValue());
                            }
                            
                            session.setAttribute("descrition", ed.getEventDescription());
                            session.setAttribute("authorizationNumber", ed.getAuthNumber());
                            session.setAttribute("eventStatus", ed.getEventStatus().getValue());
                            session.setAttribute("eventDate", ed.getEventDate());
                            session.setAttribute("riskRatingReason", ed.getEventReason());
                            session.setAttribute("RRRid", ed.getEventRuleNumber());
                            session.setAttribute("source", ed.getEventSource().getValue());
                            session.setAttribute("sourceRisk", ed.getEventSource().getId());
                            session.setAttribute("ICDdescrition", ed.getEventICDCode());
                            session.setAttribute("policyNumber", ed.getCoverNumber());
                            session.setAttribute("dependantNumber", ed.getDependentNumber());
                            // Sesion Variables for claims
                            session.setAttribute("policyHolderNumber", ed.getCoverNumber());
                            session.setAttribute("depCode", ed.getDependentNumber());
                        }

                        System.out.println(">>>>>>>> userBy getUserSafeDetailsById");
                        NeoUser userBy = port.getUserSafeDetailsById(mainList.getAssignedBy());

                        System.out.println(">>>>>>>> userTo getUserSafeDetailsById");
                        NeoUser userTo = port.getUserSafeDetailsById(mainList.getAssignedTo());

                        session.setAttribute("assignedBy", userBy.getUsername());
                        session.setAttribute("assignedTo", userTo.getUsername());
                        session.setAttribute("patientNumber", list.getCoverNumber());
                        session.setAttribute("patientGender", list.getGender().getValue());
                        session.setAttribute("patientAge", "to be set");
                        session.setAttribute("riskRating", mainList.getEventRisk());
                        session.setAttribute("assignedDate", mainList.getAssignedDate());
                        session.setAttribute("productName", list.getProductName());
                        session.setAttribute("optionName", list.getOptionName());

                        session.setAttribute("entityId", list.getEntityId());

                        //Info needed for patient details
                        System.out.println(">>>>>>>> getPersonDetailsByEntityId");
                        PersonDetails person = service.getNeoManagerBeanPort().getPersonDetailsByEntityId(list.getEntityId());

                        session.setAttribute("title", person.getTitle());
                        session.setAttribute("initials", person.getInitials());
                        session.setAttribute("idNumber", person.getIDNumber());
                        session.setAttribute("matitalStatus", person.getMaritalStatus());
                        session.setAttribute("homeLanguage", person.getHomeLanguage());
                        session.setAttribute("employerName", person.getEmployer());
                        session.setAttribute("incomeCategoty", person.getIncomeCategory());
                        session.setAttribute("incomeCategoty", person.getJobTitle());

                        System.out.println(">>>>>>>> getEntityAge");
                        EntityTimeDetails eTime = port.getEntityAge(list.getDateOfBith());

                        session.setAttribute("patientAge", eTime.getYears());
                        Date update = list.getDateOfBith().toGregorianCalendar().getTime();
                        s = formatter.format(update);

                        session.setAttribute("birthDate", s);
                        session.setAttribute("policyStatus", list.getCoverStatus());
                        session.setAttribute("productId", list.getProductId());
                        session.setAttribute("optionId", list.getOptionId());

                        nextJSP = "/PDC/PolicyHolderDetails.jsp";

                        System.out.println("The nextJSP is PolicyHolderDetails.");

                        WorkList worklist = new WorkList();
                        //NeoUser user = (NeoUser) session.getAttribute("persist_user");

                        worklist.setAssignedTo(user.getUserId());

                        LookupValue reason = new LookupValue();
                        LookupValue notify = new LookupValue();

                        System.out.println("Assigned by " + userBy.getUsername());
                        System.out.println("Assigned to " + userTo.getUsername());

                        System.out.println("UserTo ID= " + userTo.getUserId());
                        System.out.println("User ID= " + user.getUserId());

                        if (userBy.getUsername().equals("super") && user.getUserId() != userTo.getUserId()) {

                            worklist.setAssignedTo(user.getUserId());
                            worklist.setAssignedBy(mainList.getAssignedBy());
                            reason.setId("2");

                            worklist.setAssigmentReason(reason);
                            worklist.setWorkListId(mainList.getWorklistID());
                            notify.setId("2");

                            worklist.setAssigmentNotification(notify);
                            worklist.setAssigmentDetails("");
                            //worklist.setEventStatus(mainList.getEventStatus());
                            worklist.setCoverNumber(mainList.getCoverNumber());
                            worklist.setDependentNumber(mainList.getDependentNumber());

                            System.out.println(">>>>>>>> assignPHMtoWorklistItem");
                            port.assignPHMtoWorklistItem(worklist);

                        }

                        System.out.println(">>>>>>>> getAllContactDetails");
                        ArrayList<ContactDetails> contact = (ArrayList<ContactDetails>) port.getAllContactDetails(list.getEntityId());

                        for (ContactDetails cd : contact) {
                            if (cd.getCommunicationMethod().equals("E-mail")) {
                                session.setAttribute("email", cd.getMethodDetails());
                                session.setAttribute("userEmail", cd.getMethodDetails());
                            } else if (cd.getCommunicationMethod().equals("Fax")) {
                                session.setAttribute("faxNumber", cd.getMethodDetails());
                            } else if (cd.getCommunicationMethod().equals("Cell")) {
                                session.setAttribute("cellNumber", cd.getMethodDetails());
                            } else if (cd.getCommunicationMethod().equals("Tel (work)")) {
                                session.setAttribute("workNumber", cd.getMethodDetails());
                            } else if (cd.getCommunicationMethod().equals("Tel (home)")) {
                                session.setAttribute("homeNumber", cd.getMethodDetails());
                            }
                        }
                    }

                } else {
                    nextJSP = "/PDC/Information.jsp";
                }

            }

            session.removeAttribute("authDetails");
            session.removeAttribute("pdcNoteList");
            session.setAttribute("fromSearch", "yes");

            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            System.out.println("NextJSP >: " + nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "ViewWorkBenchCommand";
    }
}

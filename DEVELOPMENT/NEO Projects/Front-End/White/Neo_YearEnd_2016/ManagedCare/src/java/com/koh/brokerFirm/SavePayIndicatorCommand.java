/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.brokerFirm;

import com.koh.broker_firm.application.commands.BrokerFirmViewCommand;
import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.BrokerFirm;
import neo.manager.BrokerFirmConcessionDetails;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author chrisdp
 */
public class SavePayIndicatorCommand extends NeoCommand {

    private static final String JSP_FOLDER = "/Broker/";

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        Boolean saved = false;
        String currentPayIndString = session.getAttribute("currentCommissionInd").toString();
        String sNewPayInd = request.getParameter("FirmApp_CommisionInd");
        String sFirmEntityId = request.getParameter("brokerFirmEntityId");       
        String sDate = request.getParameter("BrokerFirm_StartDate");
        
        int currentPayInd = 0;
        int newPayInd = 999;
        int firmEntityId = 0;
                
        try {
            firmEntityId = Integer.parseInt(sFirmEntityId);
            currentPayInd = Integer.parseInt(currentPayIndString);
            newPayInd = Integer.parseInt(sNewPayInd);                  
        } catch (NumberFormatException e){
            e.printStackTrace();
        }        
        Date startDate = null;
        XMLGregorianCalendar startDateCal = null;
        
        try {
            startDate = new SimpleDateFormat("yyyy/MM/dd").parse(sDate);
            startDateCal = DateTimeUtils.convertDateToXMLGregorianCalendar(startDate);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        if (currentPayInd == newPayInd || newPayInd == 999) {
            saved = false;
            System.out.println("Invalid Information");
        } else {
            try{
                BrokerFirm bf = port.fetchBrokerFirmByEntityId(firmEntityId);
                bf.setLastUpdatedBy(user.getUserId());
                bf.setSecurityGroupId(user.getSecurityGroupId());
                bf.setCommisionInd(newPayInd);
                BrokerFirm brokerFirm = port.saveBrokerFirmApplication(bf); 
                saved = port.updateBrokerFirmCommissionInd(newPayInd, firmEntityId, startDateCal, user.getUserId());
            }catch (Exception ex){
                ex.printStackTrace();
            } 
        }

        if (saved) {
            session.setAttribute("currentCommissionInd", newPayInd);
            
            try {
                String page = "";
                page = getFirmDetails(request, firmEntityId);
                //RequestDispatcher dispatcher = context.getRequestDispatcher(page);
                context.getRequestDispatcher(page).forward(request, response);    
            } catch (ServletException ex) {
                Logger.getLogger(BrokerFirmViewCommand.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(BrokerFirmViewCommand.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }          
        return null;
    }

    @Override
    public String getName() {
        return "SavePayIndicatorCommand";
    }

    public String getFirmDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        BrokerFirm bf = port.fetchBrokerFirmByEntityId(entityId);
        
        if (entityId != 0) {
            
            BrokerFirmMainMapping.setFirmApplication(request, bf);
            BrokerFirmConcessionDetails ba = port.fetchBrokerConcessionByBrokerEntity(entityId);
            BrokerFirmMainMapping.setConcessionFirmApplication(request, ba);
            String commInd = "";

            if (bf.getCommisionInd() == 0) {
                commInd = "No";
            } else if (bf.getCommisionInd() == 1) {
                commInd = "Yes";
            }

            request.setAttribute("firmAppName", bf.getBrokerFirmName());
            request.setAttribute("firmAppCode", bf.getBrokerFirmCode());
            request.setAttribute("brokerFirmEntityId", bf.getEntityId());
            request.setAttribute("brokerFirmTile", bf.getContactTitle());
            request.setAttribute("brokerFirmContactPerson", bf.getContactFirstName());
            request.setAttribute("brokerFirmContactSurname", bf.getContactLastName());
            request.setAttribute("firmRegion", bf.getRegion());
            request.setAttribute("VIPBroker", bf.getVipBrokerInd());
            request.setAttribute("commissionInd", commInd);
            session.setAttribute("currentCommissionInd", bf.getCommisionInd());
           
            // No Need Anymore session.setAttribute("cameFromChangeInd", 1);// CommissionNavFlag 
            if (ba != null) {
                request.setAttribute("brokerFirmEntityId", ba.getFirmEntityId());
                request.setAttribute("concessionFlag", ba.getFirmEntityId());
                request.setAttribute("FirmApp_ConcessionStart", DateTimeUtils.convertXMLGregorianCalendarToDate(ba.getConcessionStartDate()));
                request.setAttribute("FirmApp_ConcessionEnd", DateTimeUtils.convertXMLGregorianCalendarToDate(ba.getConcessionEndDate()));
            }
        }
        return JSP_FOLDER + "BrokerFirm.jsp";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.util.Collection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.EntityPaymentRunDetails;
import neo.manager.NeoManagerBean;
import neo.manager.PaymentRunSearchCriteria;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ViewPracticePaymentDetailsCommand extends NeoCommand {

    private Logger log = Logger.getLogger(ViewProviderDetailsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        log.info("Inside ViewPracticePaymentDetailsCommand");

        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        PaymentRunSearchCriteria payRun = new PaymentRunSearchCriteria();
        String providerNumber = "" + session.getAttribute("practiceNumber");

        if (session.getAttribute("practiceName") != null) {
            String pracName = "" + session.getAttribute("practiceName");
            if (!pracName.equalsIgnoreCase("")) {
                payRun.setEntitySurname(pracName);
            }
        }

        log.info("The variable providerNumber is " + providerNumber);

        payRun.setEntityNo(providerNumber);
        payRun.setEntityType("Practice");

        Collection<EntityPaymentRunDetails> paymentRunDetailsList = null;
        if (session.getAttribute("practicePaymentRunDetails") != null) {
            paymentRunDetailsList = (Collection<EntityPaymentRunDetails>) session.getAttribute("practicePaymentRunDetails");
        } else {
            //clearing claim payment check
            boolean isClearingUser = (Boolean) session.getAttribute("persist_user_ccClearing");
            if(isClearingUser){
                payRun.setClearingClaimPayment(true);
                
            }else{
                payRun.setClearingClaimPayment(false);
                
            }
            paymentRunDetailsList = port.getPaymentRunDetailForEntity(payRun, null);
            
        }
        session.setAttribute("practicePaymentRunDetails", paymentRunDetailsList);
        
        try {
            //String nextJSP = "/Claims/ProviderDetailsTabbedPage.jsp";
            String nextJSP = "/Claims/PracticePaymentDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "ViewPracticePaymentDetailsCommand";
    }
}

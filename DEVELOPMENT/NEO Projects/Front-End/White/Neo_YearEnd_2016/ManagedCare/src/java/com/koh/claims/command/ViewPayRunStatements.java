/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Johan-NB
 */
public class ViewPayRunStatements extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);

        HttpSession session = request.getSession();

        String payRunScreen = "" + session.getAttribute("onScreen");
        String payRunStmtDate = "" + session.getAttribute("payRunStmtDate");
        String stmtDateStr = "";
        String stmtTypeStr = "";

        SimpleDateFormat sTOd = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat dTOs = new SimpleDateFormat("yyyy/MM/dd");

        try {
            if (payRunScreen != null && !payRunScreen.equalsIgnoreCase("")
                    && payRunStmtDate != null && !payRunStmtDate.equalsIgnoreCase("")) {

                Date payStmtDate = dTOs.parse(payRunStmtDate);
                Calendar payDate = Calendar.getInstance();
                payDate.setTime(payStmtDate);


                String stmtType = "";
                if (payRunScreen.equalsIgnoreCase("practicePayRun")) {
                    stmtType = "2";
                    stmtTypeStr = "Practice";
                } else {
                    stmtType = "1";
                    stmtTypeStr = "Member";
                }

                String path = new PropertiesReader().getProperty("statements");
                //System.out.println("statement directory: " + path);
                File stmt_dir = new File(path);
                String[] stmtDated = stmt_dir.list();

//                System.out.println("The Date list is not null " + stmtDated);
//                System.out.println("The size of the list is " + stmtDated.length);

                Map<String, String> stmtDateMap = new HashMap<String, String>();
                if (stmtDated != null) {
                    for (String s : stmtDated) {
                        Calendar stmtFolderDate = Calendar.getInstance();
                        try {
                            Date d = sTOd.parse(s);
                            stmtFolderDate.setTime(d);
                            int payYear = payDate.get(Calendar.YEAR);
                            int stmtYear = stmtFolderDate.get(Calendar.YEAR);
                            int payDayOfYear = payDate.get(Calendar.DAY_OF_YEAR);
                            int stmtDayOfYear = stmtFolderDate.get(Calendar.DAY_OF_YEAR);

                            int yearCalc = stmtYear - payYear;
                            int dateCalc = stmtDayOfYear - payDayOfYear;
                            //dateCalc = Math.abs(dateCalc);

                            if (dateCalc == 0 && yearCalc == 0) {
                                stmtDateStr = dTOs.format(stmtFolderDate.getTime());
                                stmtDateMap.put(String.valueOf(dateCalc), stmtDateStr);

                            } else if (dateCalc < 0 && dateCalc > -3 && yearCalc == 0) {
                                stmtDateStr = dTOs.format(stmtFolderDate.getTime());
                                stmtDateMap.put(String.valueOf(dateCalc), stmtDateStr);
                            } else if (dateCalc > 0 && dateCalc < 6  && yearCalc == 0) {
                                stmtDateStr = dTOs.format(stmtFolderDate.getTime());
                                stmtDateMap.put(String.valueOf(dateCalc), stmtDateStr);
                            }

                        } catch (ParseException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
                //map validation
                int stmtDayInt = 0;
                if (stmtDateMap.containsKey("0")) {
                    stmtDateStr = stmtDateMap.get("0");
                } else {
                    Set<String> days = stmtDateMap.keySet();
                    for (String day : days) {
                        System.out.println("payment day = " + day);
                        int dayInt = Integer.parseInt(day);

                        if (stmtDayInt != 0) {
                            if (stmtDayInt < dayInt) {
                                stmtDayInt = dayInt;
                            }
                        } else {
                            stmtDayInt = dayInt;
                        }
                    }
                }
                String stmtDates = stmtDateMap.get(String.valueOf(stmtDayInt));
                if (stmtDates != null && !stmtDates.equalsIgnoreCase("")) {
                    System.out.println("payment STATEMENT day = " + stmtDates);
                    session.setAttribute("payRunStmtType", stmtType);
                    session.setAttribute("stmtDateStr", stmtDates);
                    session.setAttribute("stmtTypeStr", stmtTypeStr);

                    RequestDispatcher dispatcher = context.getRequestDispatcher("/Claims/PaymentRunStatements.jsp");
                    dispatcher.forward(request, response);

                } else {
                    String errorReturn = "";
                    if (stmtType.equalsIgnoreCase("1")) {
                        errorReturn = "/ManagedCare/Claims/MemberPaymentDetails.jsp";

                    } else {
                        errorReturn = "/ManagedCare/Claims/PracticePaymentDetails.jsp";

                    }
                    printErrorResult(request, response, errorReturn);

                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewPayRunStatements";
    }

    public void printErrorResult(HttpServletRequest request, HttpServletResponse response, String returnScreen) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");

            out.println("<td><label class=\"header\">No records found.</label></td>");

            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "2; URL=" + returnScreen + "");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }
}

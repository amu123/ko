/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.FormatUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthHospitalLOC;

/**
 *
 * @author johanl
 */
public class AuthLevelOfCareListDisplay extends TagSupport {

    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            if (javaScript != null) {
                out.println("<td colspan=\"5\"><table class=\"" + javaScript + "\" style=\"cellpadding=\"5\"; cellspacing=\"0\";\">");
            } else {
                out.println("<td colspan=\"5\"><table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            }

            out.println("<tr>"
                    + "<th align=\"left\">Level of Care</th>"
                    + "<th align=\"left\">Length Of Stay</th>"
                    + "<th align=\"left\">Approved Length Of Stay</th>"
                    + "<th align=\"left\">Date From</th>"
                    + "<th align=\"left\">Date To</th>"
                    + "<th align=\"left\">Ward Tariff</th>"
                    + "<th align=\"left\">Estimated Cost</th>"
                    + "<th align=\"left\">Savings Achieved</th>"
                    + "</tr>");

            ArrayList<AuthHospitalLOC> locList = (ArrayList<AuthHospitalLOC>) session.getAttribute(sessionAttribute);
            double totalEst = 0.0d;
            String totalEstimatedAmount = "";
            if (locList != null && !locList.isEmpty()) {
                locList = (ArrayList<AuthHospitalLOC>) DateTimeUtils.orderLOCListByDate(locList);
                String facilityDisc = "" + session.getAttribute("facilityProvDiscTypeId");
                int provDesc = 0;
                if (facilityDisc != null && !facilityDisc.equalsIgnoreCase("null")
                        && !facilityDisc.equalsIgnoreCase("")) {
                    provDesc = Integer.parseInt(facilityDisc);
                }

                for (AuthHospitalLOC loc : locList) {

                    double los = loc.getLengthOfStay();
                    if (los != 0.0d) {

                        totalEst += loc.getEstimatedCost();

                        String wardTariff = "";
                        int wardLookupID = 280;
                        int locTypeID = 125;
                        boolean noDiscPrefix = false;

                        if (provDesc == 79) {
                            wardLookupID = 281;
                            locTypeID = 272;

                        } else if (provDesc == 55) {
                            wardLookupID = 265;
                            locTypeID = 273;

                        } else if (provDesc == 59) {
                            wardLookupID = 266;
                            locTypeID = 274;

                        } else if (provDesc == 49) {
                            wardLookupID = 267;
                            locTypeID = 275;

                        } else if (provDesc == 76) {
                            wardLookupID = 268;
                            locTypeID = 276;

                        } else if (provDesc == 47) {
                            wardLookupID = 269;
                            locTypeID = 277;

                        } else if (provDesc == 56) {
                            wardLookupID = 270;
                            locTypeID = 278;

                        } else if (provDesc == 77) {
                            wardLookupID = 271;
                            locTypeID = 279;

                        }

                        System.out.println("wardLookupId = " + wardLookupID);
                        System.out.println("locTypeId = " + locTypeID);
                        
                        wardTariff = NeoCommand.service.getNeoManagerBeanPort().getValueFromCodeTableForTableId(wardLookupID, loc.getLevelOfCare());
                        String wardTariffDisplay = "";
                        if (noDiscPrefix) {
                            wardTariffDisplay = wardTariff;
                        } else {
                            wardTariffDisplay = provDesc + wardTariff;
                        }

                        String locDesc = NeoCommand.service.getNeoManagerBeanPort().getValueFromCodeTableForTableId(locTypeID, loc.getLevelOfCare());

                        out.println("<tr>"
                                + "<td><label class=\"label\">" + locDesc + "</label></td>"
                                + "<td><label class=\"label\">" + loc.getLengthOfStay() + "</label></td>"
                                + "<td><label class=\"label\">" + loc.getApprovedLos() + "</label></td>"
                                + "<td><label class=\"label\">" + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(loc.getDateFrom().toGregorianCalendar().getTime()) + "</label></td>"
                                + "<td><label class=\"label\">" + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(loc.getDateTo().toGregorianCalendar().getTime()) + "</label></td>"
                                + "<td><label class=\"label\">" + wardTariff + "</label></td>"
                                + "<td><label class=\"label\">" + loc.getEstimatedCost() + "</label></td>"
                                + "<td><label class=\"label\">" + loc.getSavingsAchieved() + "</label></td>"
                                + "</tr>");
                    }
                }
                if (totalEst != 0.0) {
                    session.setAttribute("estimatedWardCost", totalEst);
                    totalEstimatedAmount = FormatUtils.decimalFormat.format(totalEst);
                    out.println("<tr><td colspan=\"8\"></td></tr>");
                    out.println("<tr>"
                            + "<th colspan=\"6\" align=\"left\">Total Estimated Ward Amount :</td>"
                            + "<th colspan=\"2\" align=\"left\">" + totalEstimatedAmount + "</td>"
                            + "</tr>");
                }

            }
            out.println("</table></td>");
        } catch (java.io.IOException ex) {
            ex.printStackTrace();
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.employer.command;

import com.koh.command.NeoCommand;
import com.koh.employer.EmployerGroupMapping;
import com.koh.member.application.MemberAppNotesMapping;
import com.koh.member.application.command.MemberApplicationHospitalCommand;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.*;

/**
 *
 * @author princes
 */
public class EmployeeAppNotesCommands extends NeoCommand{
       @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        try {
            String s = request.getParameter("buttonPressed");
            if (s != null && !s.isEmpty()) {
                System.out.println("buttonPressed : " + s);
                if (s.equalsIgnoreCase("Add Note")) {
                    addDetails(port, request, response, context);
                } else if (s.equalsIgnoreCase("SaveButton")) {
                    saveDetails(port, request, response, context);
                } else if (s.equalsIgnoreCase("SelectButton")) {
                    getNotesList(port, request, response, context);
                } else if (s.equalsIgnoreCase("ViewButton")) {
                    viewNote(port, request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(MemberApplicationHospitalCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
       
    private void addDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        request.setAttribute("noteId", null);
        int appNum = getStrToInt(request.getParameter("memberAppNumber"));
        context.getRequestDispatcher("/Employer/EmployerAppNotesAdd.jsp").forward(request, response);
    }
    
    private void viewNote(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        int appNum = getStrToInt(request.getParameter("memberAppNumber"));
        int noteId = getStrToInt(request.getParameter("noteId"));
        //        String s = port.fetchMember
        EntityNotes employerAppNoteByNoteId = port.fetchEmployerAppNoteByNoteId(noteId);
        request.setAttribute("noteDetails", employerAppNoteByNoteId.getNoteDetails());
//        MemberAppNotesMapping.setMemberAppNotes(request, memberAppNoteByNoteId);
        context.getRequestDispatcher("/Employer/EmployerAppNotesAdd.jsp").forward(request, response);
    }
    
    private void saveDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = EmployerGroupMapping.validateNotes(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (saveDetails(port, request)) {
                
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving.\"";
            }
        } else {
            additionalData = null;
        }
        if ("OK".equalsIgnoreCase(status)) {
            getNotesList(port, request, response, context);
        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }
         
        
    }
    
    private boolean saveDetails(NeoManagerBean port, HttpServletRequest request) {
        NeoUser neoUser = (NeoUser)request.getSession().getAttribute("persist_user");
        try {
            EntityNotes app = EmployerGroupMapping.getEmployerAppNotes(request, neoUser);
            app.setNoteDetails(request.getParameter("noteDetails"));
            port.saveEmployerAppNotes(app);
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
    }
    
    private void getNotesList(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
            int employerEntityId = getIntParam(request, "employerEntityId");
            int noteNum = getIntParam(request, "notesListSelect");
            request.setAttribute("notesListSelect", request.getParameter("notesListSelect"));
            if (noteNum == 99) {
                
            } else {
                List<KeyValueArray> kva = port.fetchNotesList(214,employerEntityId, noteNum,8);
                Object col = MapUtils.getMap(kva);
                request.setAttribute("EmployeeAppNoteDetails", col);
                
            }
            context.getRequestDispatcher("/Employer/EmployeeAppNotesDetails.jsp").forward(request, response);
    }
       
    private int getIntParam(HttpServletRequest request, String paramName) {
        try {
            String maxDepStr = request.getParameter(paramName);
            return Integer.parseInt(maxDepStr);
        } catch (java.lang.Exception e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }
    
    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser)request.getSession().getAttribute("persist_user");
    }
    
    private int getStrToInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (java.lang.Exception e) {
            System.out.println(s + " = " + e.getMessage());
        }
        return 0;
    }
    

    @Override
    public String getName() {
        return "EmployeeAppNotesCommands";
    }
    
    
 
}

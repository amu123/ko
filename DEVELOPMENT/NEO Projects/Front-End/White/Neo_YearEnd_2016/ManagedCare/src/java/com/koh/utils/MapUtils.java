/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.utils;

import static com.koh.command.NeoCommand.service;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import neo.manager.CarePathDetails;
import neo.manager.KeyValue;
import neo.manager.KeyValueArray;
import neo.manager.KeyObject;
import neo.manager.KeyObjectArray;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;


/**
 *
 * @author johanl
 */
public class MapUtils {

    public static Map<String, String> getMap(KeyValueArray kva) {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (KeyValue kv : kva.getKeyValList()) {
            map.put(kv.getKey(), kv.getValue());
        }
        return map;
    }

    public static List<Map<String, String>> getMap(List<KeyValueArray> list) {
        List<Map<String, String>> al = new ArrayList<Map<String, String>>();
        for (KeyValueArray kv : list) {
            al.add(getMap(kv));
        }
        return al;
    }
    
    public static com.koh.utils.KeyValueArray getKeyValueArray(Map<String, String> map) {
        com.koh.utils.KeyValueArray kv = new com.koh.utils.KeyValueArray();
        if (map != null && !map.isEmpty()) {
            for (String s : map.keySet()) {
                kv.put(s, map.get(s));
            }
        }
        return kv;
    }
    
    public static List<neo.manager.KeyValueArray> getKeyValueArray(List<Map<String, String>> mapList) {
        List<neo.manager.KeyValueArray> list = new ArrayList<neo.manager.KeyValueArray>();
        if (mapList != null && !mapList.isEmpty()) {
            for (Map<String, String> map : mapList) {
                list.add(getKeyValueArray(map));
            }
        }
        return list;
    }

    public static Map<String, Object> getMap(KeyObjectArray kva) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        for (KeyObject kv : kva.getKeyValList()) {
            map.put(kv.getKey(), kv.getValue());
        }
        return map;
    }

    public static List<Map<String, Object>> getObjectMap(List<KeyObjectArray> list) {
        List<Map<String, Object>> al = new ArrayList<Map<String, Object>>();
        for (KeyObjectArray kv : list) {
            al.add(getMap(kv));
        }
        return al;
    }
    
    public static com.koh.utils.KeyObjectArray getKeyObjectArray(Map<String, Object> map) {
        com.koh.utils.KeyObjectArray kv = new com.koh.utils.KeyObjectArray();
        if (map != null && !map.isEmpty()) {
            for (String s : map.keySet()) {
                kv.put(s, map.get(s));
            }
        }
        return kv;
    }
    
    public static List<neo.manager.KeyObjectArray> getKeyObjectArray(List<Map<String, Object>> mapList) {
        List<neo.manager.KeyObjectArray> list = new ArrayList<neo.manager.KeyObjectArray>();
        if (mapList != null && !mapList.isEmpty()) {
            for (Map<String, Object> map : mapList) {
                list.add(getKeyObjectArray(map));
            }
        }
        return list;
    }

    public static Map<LookupValue, List<CarePathDetails>> buildMapForPDCCarePath(List<CarePathDetails> cpList, int workListId, int checkStatus){
        Map<LookupValue, List<CarePathDetails>> retMap = new LinkedHashMap<LookupValue, List<CarePathDetails>>();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        List<LookupValue> cpDistinctList = port.getAllDistinctCarePathTemplatesForDep(workListId);
        
        //build hashmap from distinct and complete list
        for(LookupValue lv : cpDistinctList){
            List<CarePathDetails> currentCpList = new ArrayList<CarePathDetails>();
            for(CarePathDetails cp : cpList){
                if(lv.getId().equals(""+cp.getTemplateID()) && cp.getAssignedStatus() == checkStatus){
                    currentCpList.add(cp);
                }
            }
            retMap.put(lv, currentCpList);
        }
        
        return retMap;
    }
}

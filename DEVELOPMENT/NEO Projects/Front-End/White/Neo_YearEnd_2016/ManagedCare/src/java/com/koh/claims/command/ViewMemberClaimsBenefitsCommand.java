/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Claim;
import org.apache.log4j.Logger;
import javax.servlet.RequestDispatcher;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CcClaimLine;
import neo.manager.CcClaimSearchResult;
import neo.manager.ClaimCCSearchCriteria;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author josephm
 */
public class ViewMemberClaimsBenefitsCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(ViewMemberClaimsBenefitsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);

        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        ClaimCCSearchCriteria search = new ClaimCCSearchCriteria();
        CcClaimSearchResult benefitClaims = new CcClaimSearchResult();
        List<CcClaimLine> benClaimLines = new ArrayList<CcClaimLine>();

        neo.manager.Security _secure = new neo.manager.Security();
        _secure.setCreatedBy(user.getUserId());
        _secure.setLastUpdatedBy(user.getUserId());
        _secure.setSecurityGroupId(2);

        search.setResultCountMin(0);
        search.setResultCountMax(100);
        
        search.setDependentCode(-1);

        String onBenefitScreen = "" + session.getAttribute("onBenefitScreen");
        if (onBenefitScreen != null && !onBenefitScreen.equalsIgnoreCase("")) {

            if (onBenefitScreen.equalsIgnoreCase("memberBenefits")) {
                String coverNumber = "" + session.getAttribute("policyHolderNumber");
                int benId = Integer.parseInt("" + session.getAttribute("benId"));
                boolean isThres = Boolean.parseBoolean("" + session.getAttribute("isThres"));

                search.setCoverNumber(coverNumber);
                search.setBenefitId(benId);
                search.setThresholdClaim(isThres);


            } else if (onBenefitScreen.equalsIgnoreCase("MemberBenefitClaimResult.jsp")) {

                if (session.getAttribute("memBenClaimSearch") != null) {
                    search = (ClaimCCSearchCriteria) session.getAttribute("memBenClaimSearch");
                }
                if (session.getAttribute("memBenClaimSearchResult") != null) {
                    benefitClaims = (CcClaimSearchResult) session.getAttribute("memBenClaimSearchResult");
                }

                int claimLineCount = benefitClaims.getResultCounter();

                int resultMin = search.getResultCountMin();
                int resultMax = search.getResultCountMax();

                String pageDirection = request.getParameter("mbcPageDirection");
                if (pageDirection != null && !pageDirection.equalsIgnoreCase("")) {

                    logger.info("pageDirection triggered ");
                    logger.info("pageDirection total claimLineCount = " + claimLineCount);
                    logger.info("pageDirection resultMin = " + resultMin);
                    logger.info("pageDirection resultMax = " + resultMax);

                    if (pageDirection.equalsIgnoreCase("forward")) {
                        resultMin = resultMin + 100;
                        resultMax = resultMax + 100;

                    } else if (pageDirection.equalsIgnoreCase("backward")) {
                        resultMin = resultMin - 100;
                        resultMax = resultMax - 100;
                    }
                    logger.info("pageDirection new resultMin = " + resultMin);
                    logger.info("pageDirection new resultMax = " + resultMax);

                    search.setResultCountMin(resultMin);
                    search.setResultCountMax(resultMax);

                }

                //set filter values
                String claimIdStr = "" + session.getAttribute("claimId");
                String tCodeStr = "" + session.getAttribute("tariffCode");
                String fromDateStr = "" + session.getAttribute("tFromDate");
                String pracNo = "" + session.getAttribute("pracNum_text");
                String depCodeStr = "" + session.getAttribute("depListValues");
                int depCode = -1;
                boolean resetMaxMin = false;

                if(depCodeStr != null && !depCodeStr.equalsIgnoreCase("") && !depCodeStr.equalsIgnoreCase("99")){
                    depCode = Integer.parseInt(depCodeStr);
                    search.setDependentCode(depCode);
                }else{
                    search.setDependentCode(depCode);
                }
                
                
                if (claimIdStr != null && !claimIdStr.equalsIgnoreCase("")) {
                    int claimId = Integer.parseInt(claimIdStr);
                    search.setClaimId(claimId);
                    resetMaxMin = true;

                } else {
                    search.setClaimId(0);
                }

                if (tCodeStr != null && !tCodeStr.equalsIgnoreCase("")) {
                    search.setTariffCode(tCodeStr);
                    resetMaxMin = true;

                } else {
                    search.setTariffCode("");
                }

                if (fromDateStr != null && !fromDateStr.equalsIgnoreCase("")) {
                    Date d = null;
                    XMLGregorianCalendar xDate = null;
                    try {
                        d = sdf.parse(fromDateStr);
                        xDate = DateTimeUtils.convertDateToXMLGregorianCalendar(d);

                    } catch (ParseException px) {
                        px.printStackTrace();
                    }
                    if (xDate != null) {
                        search.setServiceDateFrom(xDate);
                        resetMaxMin = true;
                    }

                } else {
                    search.setServiceDateFrom(null);
                }

                if (pracNo != null && !pracNo.equalsIgnoreCase("")) {
                    search.setPracticeNumber(pracNo);
                    resetMaxMin = true;

                } else {
                    search.setPracticeNumber("");
                }

                if (resetMaxMin) {
                    search.setResultCountMin(0);
                    search.setResultCountMax(100);
                }

            }
            benefitClaims = port.findCCClaimsForBenefitBySearchCriteria(search, _secure);
            benClaimLines = benefitClaims.getClaimLineDetails();
            int totalClaimLines = benefitClaims.getResultCounter();

            session.setAttribute("totalMemBenClaims", totalClaimLines);
            session.setAttribute("memBenClaimSearch", search);
            session.setAttribute("memBenClaimSearchResult", benefitClaims);
            session.setAttribute("memBenClaimClaimLines", benClaimLines);

            if (benClaimLines.isEmpty() == true) {
                request.setAttribute("memBenClaimResultMsg", "No results found");
            }

            try {
                String nextJSP = "/Claims/MemberBenefitClaimResult.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public String getName() {

        return "ViewMemberClaimsBenefitsCommand";
    }
}

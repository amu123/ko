/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverDetails;
import neo.manager.CoverSearchCriteria;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class MemberDocumentSearchCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------In MemberDocumentSearchCommand---------------------");
            NeoManagerBean port = service.getNeoManagerBeanPort();
            HttpSession session = request.getSession();
            
            request.setAttribute("folderList", new PropertiesReader().getProperty("DocumentIndexWorkFolder"));
            request.setAttribute("listOrWork", "Work");
            
            String memberNumber = request.getParameter("memberNumber");
            CoverSearchCriteria cs = new CoverSearchCriteria();
            //cs.setCoverNumber(memberNumber);
            cs.setCoverNumberExact(memberNumber);
            logger.info("Memeber no = " + memberNumber);
            
            boolean viewStaffuser = (Boolean) session.getAttribute("persist_user_viewStaff");
            boolean viewStaff = true;
            if (!viewStaffuser) {
                List<CoverDetails> covList = port.getCoverPrincipleList(cs);
                for (CoverDetails cov : covList) {
                    if (cov.getMemberCategory() == 2) {
                        memberNumber = null;
                        viewStaff = false;
                        break;
                    }
                }
            }
            if (memberNumber != null && !memberNumber.trim().equals("")) {
                IndexDocumentRequest req = new IndexDocumentRequest();
                NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
                req.setAgent(loggedInUser);
                req.setIndexType("IndexForMember");
                req.setEntityNumber(memberNumber);
                req.setSrcDrive(new PropertiesReader().getProperty("DocumentIndexIndexDrive"));
                IndexDocumentResponse resp = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
                if (resp.getIndexForMember() != null) {
                    logger.info("Index size = " + resp.getIndexForMember().size());
                }
                if (!resp.isIsSuccess()) {
                    logger.error("Error getting indexes .... " + resp.getMessage());
                    errorMessage(request, response, resp.getMessage());
                    return "MemberDocumentSearchCommand";
                }
                request.setAttribute("indexList", resp.getIndexForMember());
            }
            request.setAttribute("memberNumber", memberNumber);
            if (!viewStaff) {
                request.setAttribute("errorMsg", "User access denied. Please contact your supervisor");
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/MemberDocuments.jsp");
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "MemberDocumentSearchCommand";
    }

    private void errorMessage(HttpServletRequest request, HttpServletResponse response, String msg) throws Exception {
        request.setAttribute("errorMsg", msg);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/MemberDocuments.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    public String getName() {
        return "MemberDocumentSearchCommand";
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author johanl
 */
public class GenericTariffTableTag extends TagSupport {

    private String elementName;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            ServletRequest req = pageContext.getRequest();
            HttpSession session = pageContext.getSession();
            out.println("<td colspan=\"6\">" +
                    "<label class=\"subheader\">Allocate Tariff to Authorisation</label>" +
                    "<table width=\"500\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            //if (!elementName.trim().equalsIgnoreCase("")) {
                out.println("<tr><th>Tariff Desc</th><th colspan=\"2\">Tariff Code</th>" +
                        "<th>Quantity</th><th>Amount</th></tr>");
                out.println("<tr><form>" +
                        "<td><input type=\"text\" id=\"tariffDesc\" name=\"tariffDesc\" size=\"40\" disabled=\"disabled\"/></td>" +
                        "<td><input type=\"text\" id=\"tariffCode\" name=\"tariffCode\" size=\"15\" /></td>" +
                        "<td><img src=\"/ManagedCare/resources/Search.gif\" width=\"25\" height=\"23\" alt=\"Search\" border=\"0\" onClick=\"getTariffForCode();\" /></td>" +
                        "<td><input type=\"text\" id=\"tariffQuantity\" name=\"tariffQuantity\" size=\"15\" onChange=\"CalculateTariffQuantity(this.value);\"/></td>" +
                        "<td><input type=\"text\" id=\"tariffAmount\" name=\"tariffAmount\" size=\"15\" readonly=\"readonly\"/></td>" +
                        "</tr>");

                out.println("<tr>" +
                        "<td width=\"200px\" align=\"left\"><label id=\"tariffError\" class=\"error\"></label></td>" +
                        "<td colspan=\"4\" align=\"right\"><button type=\"button\" " + javaScript + " name=\"opperation\" value=\"" + commandName + "\">Add Tariff</button></td></form></tr>");
            //}
            out.println("</table></td>");
        } catch (IOException ex) {
            Logger.getLogger(GenericTariffTableTag.class.getName()).log(Level.SEVERE, null, ex);
        }
        return super.doEndTag();
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }
 
    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

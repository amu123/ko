/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.NeoCoverDetailsDependantFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Biometrics;
import neo.manager.ContributionLine;
import neo.manager.Contributions;
import neo.manager.CoverDetails;
import neo.manager.CoverDetailsAdditionalInfo;
import neo.manager.CoverSearchCriteria;
import neo.manager.EAuthCoverDetails;
import neo.manager.Member;
import neo.manager.MemberEmployerBrokerInfo;
import neo.manager.NeoManagerBean;
import neo.manager.UnionMember;
import org.apache.log4j.Logger;
import org.eclipse.core.internal.registry.Contribution;
import neo.manager.LookupValue;

/**
 *
 * @author janf
 */
public class ViewMemberInfoCommand extends NeoCommand {

    private static Logger logger = Logger.getLogger(ViewMemberInfoCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        logger.info("Inside ViewMemberInfoCommand");
        System.out.println("Inside ViewMemberInfoCommand");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        List<EAuthCoverDetails> coverList = null;
        List<EAuthCoverDetails> pMemList = null;
        List<EAuthCoverDetails> allDepList = null;

        String coverNumber = "";
        String onScreen = request.getParameter("onScreen");
        if (onScreen != null && onScreen.equalsIgnoreCase("MemberCCLogin")) {
            coverNumber = request.getParameter("policyNumber_text");

        } else if (request.getParameter("policyNumber_text") != null) {
            coverNumber = request.getParameter("policyNumber_text");

        } else {
            coverNumber = "" + session.getAttribute("policyHolderNumber_text");

        }
        System.out.println("coverNumber = " + coverNumber);

//        if (coverNumber != null && !coverNumber.trim().equals("null")
//                && !coverNumber.trim().equals("")) {
//            if (session.getAttribute("allDepHistoryDetails") != null) {
//                coverList = (List<EAuthCoverDetails>) session.getAttribute("allDepHistoryDetails");
//            } else {
//                coverList = new ArrayList<EAuthCoverDetails>();
//                CoverSearchCriteria csc = new CoverSearchCriteria();
//                csc.setCoverNumberExact(coverNumber);
//                coverList = port.eAuthCoverSearch(csc);
//
//                for (EAuthCoverDetails e : coverList) {
//                    if (e.getDependantType().equalsIgnoreCase("Principal Member")) {
//                        session.setAttribute("memberCategory", setMemberCategory(Integer.toString(e.getMemberCategory())));
//                    }
//                }
//            }
//
//            pMemList = new NeoCoverDetailsDependantFilter().EAuthCoverDetailsPrincipleMemberFilter(coverList, coverNumber);
//            allDepList = new NeoCoverDetailsDependantFilter().EAuthCoverDetailsDependantFilterByPMemEndDate(coverList, coverNumber);
//            session.setAttribute("principleHistoryDetails", pMemList);
//            session.setAttribute("allDepHistoryDetails", coverList);
//        }
        //get principle member history
        CoverDetails c = port.getEAuthResignedMembers(coverNumber);
        int entityId = c.getEntityId();
//        session.setAttribute("coverEntityId", entityId);
//        if (c.getCoverNumber() != null && !c.getCoverNumber().equalsIgnoreCase("") && !c.getCoverNumber().equalsIgnoreCase(coverNumber) && coverNumber.contains(c.getCoverNumber())) {
//            System.out.println("coverDiff = " + c.getCoverNumber() + " - " + coverNumber);
//            coverNumber = c.getCoverNumber();
//        }


        /* FIND AND SET EMPLOYER DETAILS*/
//        MemberEmployerBrokerInfo info = port.getMemberEmployerBrokerInfo(coverNumber);
//        List<CoverDetails> cd = port.getCoverDetailsByCoverNumber(coverNumber);
//        int CommonentityID = 0;
//        int product = 0;
//        int dependentId = 99;
//        for (CoverDetails cd1 : cd) {
//            if (cd1.getDependentTypeId() == 17) {
//                CommonentityID = cd1.getEntityCommonId();
//                product = cd1.getProductId();
//                dependentId = 0;
//            }
//        }
        // For Sechaba
//        String client = String.valueOf(context.getAttribute("Client"));
//        if (client.equalsIgnoreCase("Sechaba")) {
//            logger.info("** Setting up for Union Member Link **");
//            logger.info("** Cover_Number : " + coverNumber);
//            logger.info("** Dependent_ID : " + dependentId);
//            UnionMember unionMember = port.findUnionMemberDetailsByID(coverNumber, dependentId);
//            if (unionMember != null) {
//                session.setAttribute("memberUnionLink", unionMember.getUnionName());
//            }
//        }
//        List<CoverDetailsAdditionalInfo> covDetails = port.getDependentAdditionalDetails(CommonentityID);
//        String paymentMethod = "";
//        String paymentDate = null;
//        SimpleDateFormat dt = new SimpleDateFormat("yyyy/MM/dd");
//        for (CoverDetailsAdditionalInfo cov : covDetails) {
//            if (cov.getInfoTypeId() == 8) {
//                paymentMethod = getPaymentMethod(cov.getInfoValue());
//
//            }
//            if (cov.getInfoTypeId() == 7) {
//                String payment = getDebitDate(cov.getInfoValue());
//                //paymentDate =  dt.format(cov.getStartDate().toGregorianCalendar().getTime());
//                paymentDate = ("(" + payment + ")");
//
//            }
//        }
        Member mem = port.fetchMemberAddInfo(entityId);

//        if (product == 1) {
//            session.setAttribute("productDescription", "Resolution Health");
//        } else if (product == 2) {
//            session.setAttribute("productDescription", "Spectramed");
//        } else if (product == 3) {
//            session.setAttribute("productDescription", "Sizwe");
//        }
        String riskRating = mem.getRiskCategory();
        List<LookupValue> lookup = port.getCodeTable(189);
        if(riskRating != null){
            for(LookupValue lv : lookup){
                if(riskRating.equalsIgnoreCase(lv.getId())){
                    riskRating = lv.getValue();
                }
            }
        }

//        session.setAttribute("productId", product);
//        session.setAttribute("memberVIPBroker", info.getVIPBroker());
//        session.setAttribute("memberEmployerName", info.getEmployerName());
//        session.setAttribute("memberEmployerType", info.getGroupType());
//        session.setAttribute("memberEmployerBillMethod", info.getBillingMethod());
//        session.setAttribute("memberEmployerAdministrator", info.getAdministrator());
//        session.setAttribute("memberPaymentMethod", paymentMethod);
//        session.setAttribute("memberPaymentDate", paymentDate);
        session.setAttribute("memberEmployeeNumber", mem.getEmployeeNumber());
        session.setAttribute("memberEmployeeTaxNumber", mem.getTaxNumber());
        //session.setAttribute("memberEmployeeIncomeAmount", mem.getIncome());
        //session.setAttribute("memberEmployerContributionPortion", mem.getSubsidyAmount());
        session.setAttribute("memberEmployeeRiskCategory", riskRating);

        //Getting underwriting for warning message
//        boolean underwritingWarning = port.checkCoverExclusionHistoryByCover(coverNumber);
//        System.out.println("underwritingWarning: " + underwritingWarning);
//        session.setAttribute("warningW", underwritingWarning);
//
//        List<neo.manager.LookupValue> lookUpsClient = service.getNeoManagerBeanPort().findCodeTableForLookupType(341);
//        if (lookUpsClient != null) {
//            neo.manager.LookupValue val = lookUpsClient.get(0);
//            if (val != null) {
//                System.out.println("Client : " + val.getValue());
//                session.setAttribute("Client", val.getValue());
//            }
//        }
//
////        if(info.getBrokerInitials() != null && info.getBrokerSurname() != null &&info.getBrokerCode() != null){
//        if (info.getBrokerCode() != null) {
//            session.setAttribute("memberBrokerCode", (info.getBrokerSurname() == null ? "" : info.getBrokerSurname()) + " " + (info.getBrokerCode() == null ? "" : info.getBrokerCode()) + "");
//        } else if (info.getConsultantCode() != null) {
//            session.setAttribute("memberBrokerCode", (info.getConsultantSurname() == null ? "" : info.getConsultantSurname()) + " " + (info.getConsultantCode() == null ? "" : info.getConsultantCode()) + "");
//        }
        try {
            System.out.println("nextJsp = /Claims/MemberInformation.jsp");
            RequestDispatcher dispatcher = context.getRequestDispatcher("/Claims/MemberInformation.jsp");
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewMemberInfoCommand";
    }

    private static String getPaymentMethod(String value) {
        if (value.equals("1")) {
            return "Debit Order";
        } else if (value.equals("2")) {
            return "Electronic Transfer";
        } else {
            return "";
        }
    }

    private static String setMemberCategory(String value) {
        String memberCategory = "";
        List<neo.manager.LookupValue> lookUpsClient = service.getNeoManagerBeanPort().findCodeTableForLookupType(223);
        for (neo.manager.LookupValue v : lookUpsClient) {
            if (value.equalsIgnoreCase(v.getId())) {
                memberCategory = v.getValue();
            }
        }
        return memberCategory;
    }

    private static String getDebitDate(String value) {
        if (value.equals("1")) {
            return "31th";
        } else if (value.equals("2")) {
            return "1st";
        } else if (value.equals("3")) {
            return "5th";
        } else if (value.equals("4")) {
            return "6th";
        } else {
            return "";
        }
    }

}

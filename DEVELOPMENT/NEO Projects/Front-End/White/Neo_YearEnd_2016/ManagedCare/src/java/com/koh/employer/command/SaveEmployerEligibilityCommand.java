/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.employer.command;

import com.koh.command.NeoCommand;
import com.koh.employer.EmployerEligibilityMapping;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.EmployerEligibility;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author yuganp
 */
public class SaveEmployerEligibilityCommand extends NeoCommand {
   
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String result = validateAndSave(port, request);
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SaveEmployerEligibilityCommand";
    }

    private String validateAndSave(NeoManagerBean port, HttpServletRequest request) {
        Map<String, String> errors = EmployerEligibilityMapping.validate(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (save(port, request)) {
                additionalData = "\"message\":\"Eligibility Details Saved\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving the Eligibility Details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
    }

    private boolean save(NeoManagerBean port, HttpServletRequest request) {
        NeoUser neoUser = getNeoUser(request);
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);
        try {
            EmployerEligibility eg = EmployerEligibilityMapping.getEmployerEligibility(request, neoUser);
            eg = port.saveEmployerEligibility(eg);
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
    }
 
    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser)request.getSession().getAttribute("persist_user");
    }

}

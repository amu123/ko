/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.workflow.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.WorkflowItem;
import neo.manager.WorkflowItemDetails;

/**
 *
 * @author janf
 */
public class ViewWorkbenchDetails extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("in ViewWorkbenchDetails");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        //NeoUser user = (NeoUser) session.getAttribute("persist_user");

        ArrayList<WorkflowItemDetails> arrWfItemDetails = null;
        System.out.println("request test = " + request.getParameter("workflowSearch"));
        request.setAttribute("workflowSearch", request.getParameter("workflowSearch"));

        int itemId;
        if (null != request.getParameter("itemId")) {
            itemId = Integer.valueOf(request.getParameter("itemId"));

            arrWfItemDetails = (ArrayList<WorkflowItemDetails>) port.getWorkflowItemDetailsByItemID(itemId);

            //<editor-fold defaultstate="collapsed" desc="Populating Notes">
            ArrayList arrEmailNotes = new ArrayList();
            for (int i = 0; i < arrWfItemDetails.size(); i++) {
                if (null != arrWfItemDetails.get(i).getNotes() && !arrWfItemDetails.get(i).getNotes().isEmpty()) {
                    arrEmailNotes.add(arrWfItemDetails.get(i).getNotes());
                }
            }
            request.setAttribute("wfNotes", arrEmailNotes); //Get Full wfNote Count
            for (int i = 0; i < arrEmailNotes.size(); i++) {
                request.setAttribute("wfNote" + i, arrEmailNotes.get(i)); //Get Eatch note
            }
            //</editor-fold>

            request.setAttribute("arrWfItemDetails", arrWfItemDetails);

            WorkflowItem arrWFItem = port.getWorkflowItemByItemID(itemId);
            //List<WorkflowItem> arrWFItem = port.getWorkflowItemByUserID(user.getUserId());

            if (arrWFItem.getItemID().equals(itemId)) {

                //<editor-fold defaultstate="collapsed" desc="Adding WorkFlow Status">
                List<LookupValue> arrWFStatus = port.getCodeTable(350);

                int wfItemStatus = arrWFItem.getStatusID();
                for (LookupValue wfStatus : arrWFStatus) {
                    if (wfItemStatus == Integer.parseInt(String.valueOf(wfStatus.getId()))) {
                        arrWFItem.setStatusName(wfStatus.getValue());
                    }
                }
                //</editor-fold>
                
                //<editor-fold defaultstate="collapsed" desc="Adding WorkFlow Resolution Reason">
                List<LookupValue> arrWFReason = port.getCodeTable(369);
                
                int wfResolutionReason = arrWFItem.getReasonID();
                for(LookupValue wfReason : arrWFReason){
                    if(wfResolutionReason == Integer.parseInt(String.valueOf(wfReason.getId()))){
                        request.setAttribute("resolutionReason", wfReason.getValue());
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="Adding WorkFlow Complaint Flag">
                //Rather convert the char here than to do it in the JSP side
                if (null != arrWFItem.getIsComplaint()) {
                    int intCharValueComplaint = arrWFItem.getIsComplaint();
                    char charValueComplaint = (char) (intCharValueComplaint);
                    String stringValueComplaint = Character.toString(charValueComplaint);

                    request.setAttribute("isComplaint", stringValueComplaint);
                }
                //</editor-fold>

                request.setAttribute("wfPanel", arrWFItem);
                arrWFItem.getUserID();
                NeoUser neoUsr = port.getUserSafeDetailsById(Integer.parseInt(String.valueOf(arrWFItem.getUserID())));
                request.setAttribute("userName", neoUsr.getUsername());
                request.setAttribute("entityNumber", arrWFItem.getEntityNumber());//JSP Page requires this [valueFromRequest="entityNumber"]
            }

        }
        try {
            String nextJSP = "/Workflow/WorkflowDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewWorkbenchDetails";
    }

}

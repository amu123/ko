/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.LookupValue;

/**
 *
 * @author Johan-NB
 */
public class SetBenefitToAuth extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        String benIdStr = request.getParameter("benId");
        String benDesc = request.getParameter("benDesc");

        System.out.println("benefit selected = "+benIdStr+" - "+benDesc);

        LookupValue benAllocated = new LookupValue();
        benAllocated.setId(benIdStr);
        benAllocated.setValue(benDesc);                
        session.setAttribute("benAllocated", benAllocated);
        
        String onScreen = ""+session.getAttribute("onScreen");
        System.out.println("SetBenefitToAuth onScreen = " + onScreen);   
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(onScreen);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SetBenefitToAuth";
    }
}

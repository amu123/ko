/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CommunicationLog;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;
import org.apache.log4j.Logger;

/**
 *
 * @author dewaldo
 */
public class ViewProviderCommunicationCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(ForwardToMemberClaimsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();

        logger.info("Inside ViewProviderCommunicationCommand");

        try {
            getProvderCommLog(session, request);
            String nextJSP = "/Claims/providerCommunication.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void getProvderCommLog(HttpSession session, HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        logger.info("ViewProviderCommunicationCommand - getProviderCommunication()");
        int entityId = new Integer(String.valueOf(session.getAttribute("provEntityID")));
        logger.info("entityId : " + entityId);
        request.setAttribute("entityId", entityId);
        List<CommunicationLog> commLogForEntityId = port.getCommLogForEntityId(entityId, null, null, null);
        request.setAttribute("communicationsLog", commLogForEntityId);
    }

    @Override
    public String getName() {
        return "ViewProviderCommunicationCommand";
    }
}

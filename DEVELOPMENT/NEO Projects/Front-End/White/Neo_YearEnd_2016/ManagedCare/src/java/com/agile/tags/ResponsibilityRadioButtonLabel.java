/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.LookupValue;
import neo.manager.NeoUser;
import neo.manager.SecurityResponsibility;

/**
 *
 * @author josephm
 */
public class ResponsibilityRadioButtonLabel extends TagSupport {

    private String numberPerRow;
    private String elementName;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        boolean admin = false;

        for (SecurityResponsibility security : user.getSecurityResponsibility()) {

            if (security.getResponsibilityId() == 1 || security.getResponsibilityId() == 13) {

                admin = true;
            }
        }

        //System.out.println("The logged in user is " + user.getName());
        // System.out.println("The Description is " + user.getDescription());

        try {
            ArrayList<LookupValue> values = (ArrayList<LookupValue>) TagMethods.getAllResponsibilities();
            int numPerRow = new Integer(numberPerRow);
            int count = 0;
            if (values != null) {
                for (int i = 0; i < values.size(); i++) {
                    if (count == 0) {
                        out.print("<tr>");
                    }
                    LookupValue lookupValue = values.get(i);
                    String userId = (session.getAttribute("userId") == null ? "" : session.getAttribute("userId").toString());
                    String respId = (session.getAttribute("respId" + userId) == null) ? "" : session.getAttribute("respId" + userId).toString();

                    if (!lookupValue.getId().equalsIgnoreCase("1") && !lookupValue.getId().equalsIgnoreCase("13")) {

                        if (lookupValue.getId().equalsIgnoreCase(respId)) {

                            out.print("<td><input type=\"radio\" name=\"" + elementName + "_" + 1 + "\" value=\"" + lookupValue.getId() + "\" checked><label>" + lookupValue.getValue() + "</label></input></td>");
                        } else {
                            out.print("<td><input type=\"radio\" name=\"" + elementName + "_" + 1 + "\" value=\"" + lookupValue.getId() + "\" ><label>" + lookupValue.getValue() + "</label></input></td>");
                        }
                        count++;
                    }
                    if (session.getAttribute("respId") != null) {
                        String value = ""+i+1;
                        if (admin && lookupValue.getId().equalsIgnoreCase("13") && value.equalsIgnoreCase(respId.toString())) {

                            out.print("<td><input type=\"radio\" name=\"" + elementName + "_" + 1 + "\" value=\"" + lookupValue.getId() + "\" checked><label>" + lookupValue.getValue() + "</label></input></td>");
                        } else if (admin && lookupValue.getId().equalsIgnoreCase("13")) {
                            out.print("<td><input type=\"radio\" name=\"" + elementName + "_" + 1 + "\" value=\"" + lookupValue.getId() + "\" ><label>" + lookupValue.getValue() + "</label></input></td>");
                        }
                    }

                    if (count == numPerRow) {
                        out.println("</tr>");
                        count = 0;
                    }
                }
            }
            //<tr><td><input type="checkbox" name="asdf" value="ON" ><label>ON</label></input></td></tr>
        } catch (java.io.IOException ex) {
            throw new JspException("Error in ResponsibilityCheckBoxLabel tag", ex);
        }
        return super.doEndTag();
    }

    public void setNumberPerRow(String numberPerRow) {
        this.numberPerRow = numberPerRow;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }
}

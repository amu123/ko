/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.practice_management.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.practice.PracticeManagementMainMapping;
import java.util.Collection;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AddressDetails;
import neo.manager.BankingDetails;
import neo.manager.ContactDetails;
import neo.manager.ContactPreference;
import neo.manager.NeoManagerBean;
import neo.manager.PracticeCaptureDetails;
import neo.manager.PracticeNotes;
import org.apache.log4j.Logger;

/**
 *
 * @author almaries
 */
public class PracticeManagementTabContentCommand extends NeoCommand{

    private Logger logger = Logger.getLogger(PracticeManagementTabContentCommand.class);
    private static final String JSP_FOLDER = "/PracticeManagement/";
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        
        int entityId = (Integer) session.getAttribute("provNumEntityId");
        String practiceNum = (String) session.getAttribute("provNum");
        String tab = request.getParameter("tab");
        String page = "";
        
        if ("PracticeDetails".equalsIgnoreCase(tab)) {
            page = getPracticeDetails(request, practiceNum);

        } else if ("PracticeAddressDetails".equalsIgnoreCase(tab)) {
            page = getPracticeAddressDetails(request, entityId);

        } else if ("PracticeBankingDetails".equalsIgnoreCase(tab)) {
            page = getPracticeBankingDetails(request, entityId);

        } else if ("PracticeContactDetails".equalsIgnoreCase(tab)) {
            page = getPracticeContactDetails(request, entityId);

        } else if ("AuditTrail".equalsIgnoreCase(tab)) {
            page = getAuditTrail(request, entityId);

        } else if ("PracticeNotes".equalsIgnoreCase(tab)){
            page = getPracticeNotes(request, entityId);
        }
        
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(page);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return null;
    }

    @Override
    public String getName() {
        return "PracticeManagementTabContentCommand";
    }
    
    private String getPracticeDetails(HttpServletRequest request, String practiceNum) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        logger.info("getPracticeDetails()");
       if (practiceNum != "" || practiceNum != null) {
            Collection<PracticeCaptureDetails> pcd = port.getPracticeDetails(practiceNum);
            System.out.println("Practice Capture Details collection size = " + pcd.size());
            PracticeManagementMainMapping.setPracticeDetails(request, pcd);
        }

        return JSP_FOLDER + "ViewPracticeDetails.jsp";
    }
    
    private String getPracticeAddressDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        logger.info("getMemberAddressDetails()");
        if (entityId != 0) {
            List<AddressDetails> ad = port.getAddressDetailsByEntityId(entityId);
            System.out.println("Address list size = " + ad.size());
            PracticeManagementMainMapping.setAddressDetails(request, ad);
        }

        return JSP_FOLDER + "PracticeAddressDetails.jsp";
    }
    
    private String getPracticeBankingDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        if (entityId != 0) {
            //List<BankingDetails> bd = port.getBankDetailsByEntityId(entityId);
            List<BankingDetails> bd = port.getAllBankingDetails(entityId);
            System.out.println("Banking list size = " + bd.size());
            PracticeManagementMainMapping.setBankingDetails(request, bd);
        }
        return JSP_FOLDER + "PracticeBankingDetails.jsp";
    }
    
    private String getPracticeContactDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        logger.info("getPracticeContactDetails()");
        if (entityId != 0) {
            //set conatct details
            List<ContactDetails> con = port.getContactDetailsByEntityId(entityId);
            PracticeManagementMainMapping.setContactDetails(request, con);

            //set contact preferences
            List<ContactPreference> conPref = port.getAllContactPreferences(entityId);
            PracticeManagementMainMapping.setContactPrefDetails(request, conPref);

        }
        return JSP_FOLDER + "PracticeContactDetails.jsp";
    }
    
    private String getAuditTrail(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        logger.info("getAuditTrails()");
        /*if (entityId != 0) {
            //set conatct details
            List<ContactDetails> con = port.getContactDetailsByEntityId(entityId);
            PracticeManagementMainMapping.setContactDetails(request, con);

            //set contact preferences
            List<ContactPreference> conPref = port.getAllContactPreferences(entityId);
            PracticeManagementMainMapping.setContactPrefDetails(request, conPref);

        }*/
        return JSP_FOLDER + "PracticeAuditTrail.jsp";
    }
    
    private String getPracticeNotes(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        logger.info("getPracticeNotes()");
        logger.info("entityId = " + entityId);
        if (entityId != 0) {
            request.setAttribute("entityId", entityId);
            List<PracticeNotes> pn = port.fetchPracticeNotesByEntityId(entityId);
            if (pn != null && !pn.isEmpty()) {
                request.setAttribute("PracticeNotes", pn);
                System.out.println("pn is not null or empty");
            } else {
                System.out.println("Empty pn");
            }
        }

        return JSP_FOLDER + "PracticeNotes.jsp";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ContactDetails;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author princes
 */
public class CallDocumentViewCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        this.saveScreenToSession(request);
        String email = (String) session.getAttribute("memberNumber");
        String email1 = (String) session.getAttribute("employerEntityId");



        if (session.getAttribute("memberEmailAddress") == null) {
            List<ContactDetails> contactDetailsList = (ArrayList<ContactDetails>) session.getAttribute("pMemContactDetails");
            for (ContactDetails cd : contactDetailsList) {
                if (cd.getCommunicationMethodId() == 1) {
                    session.setAttribute("memberEmailAddress", cd.getMethodDetails());
                } else if (cd.getCommunicationMethodId() == 2) {
                    session.setAttribute("memberFaxNo", cd.getMethodDetails());
                } else if (cd.getCommunicationMethodId() == 3) {
                    session.setAttribute("memberCellNo", cd.getMethodDetails());
                } else if (cd.getCommunicationMethodId() == 4) {
                    session.setAttribute("memberWorkNo", cd.getMethodDetails());
                } else if (cd.getCommunicationMethodId() == 5) {
                    session.setAttribute("memberHomeTelNumber", cd.getMethodDetails());
                }
                session.setAttribute("communicationMethod", cd.getCommunicationMethod());
            }
        }
        
        try {
//            logger.info("------------In CallDocumentViewCommand---------------------");
//            request.setAttribute("folderList", new PropertiesReader().getProperty("DocumentIndexWorkFolder"));
//            request.setAttribute("listOrWork", "Work");
            String memberNumber = request.getParameter("memberNumber");
//            String memberNumber = (String)session.getAttribute("memberNumber");
//            logger.info("Memeber no = " + memberNumber);
//            if (memberNumber != null && !memberNumber.trim().equals("")) {
//                IndexDocumentRequest req = new IndexDocumentRequest();
//                NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
//                req.setAgent(loggedInUser.getUsername());
//                req.setIndexType("IndexForMember");
//                req.setMemberNumber(memberNumber);
//                req.setSrcDrive(new PropertiesReader().getProperty("DocumentIndexIndexDrive"));
//                IndexDocumentResponse resp = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
//                if(resp.getIndexForMember() != null){
//                logger.info("Index size = " + resp.getIndexForMember().size());
//                }
//                request.setAttribute("indexList",resp.getIndexForMember());
//            }
            request.setAttribute("memberNumber", memberNumber);

            RequestDispatcher dispatcher = request.getRequestDispatcher("/Claims/CallCenterViewDocuments.jsp");

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "CallDocumentViewCommand";
    }

    @Override
    public String getName() {
        return "CallDocumentViewCommand";
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author josephm
 */
public class ForwardToSearchICDCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        session.setAttribute("refreshed", true);

        try {
            String nextJSP = "/biometrics/ICD10Search.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        }catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ForwardToSearchICDCommand";
    }
}

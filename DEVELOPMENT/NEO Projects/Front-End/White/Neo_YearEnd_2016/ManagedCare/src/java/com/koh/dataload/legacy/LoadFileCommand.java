/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.dataload.legacy;

import com.agile.security.webservice.NeoUser;
import com.koh.command.NeoCommand;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author KatlegoM
 */
public class LoadFileCommand extends NeoCommand {

    private final static int LOAD_MEMBER_FILES = 1;
    private final static int LOAD_PROVIDER_FILE = 2;
    private final static int LOAD_DRUG_FILES = 3;

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sysFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

    NeoUser neoUser  = null;
    Date utilEndDate = null;
    Date todayDate   = null;
    int innerCommand = -1;

    String memberFileName       = null;
    String dependentFileName    = null;
    String providerFileName     = null;
    String genericDrugFile      = null;
    String nonGenericDrugFile   = null;
    String manufacturerDrugFile = null;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();

        innerCommand = 0;
        
        String nextJSP = "/SystemAdmin/LoadMember.jsp";

        if (request.getParameter("innerCommand") != null) {
            innerCommand = new Integer(request.getParameter("innerCommand"));
        }

        try {

            utilEndDate = sdf.parse("31/12/2999");
            String strToday  = sysFormat.format(new Date());
            todayDate   = sysFormat.parse(strToday);

            Enumeration senumeration = session.getAttributeNames();
            while(senumeration.hasMoreElements()) {
                String param = senumeration.nextElement().toString();
                System.out.println("Session "+ param + "!!!!!!!!!!!!!!!!! + " + session.getAttribute(param).toString());
            }

            Enumeration enumeration = request.getParameterNames();
            while(enumeration.hasMoreElements()) {
                String param = enumeration.nextElement().toString();
                System.out.println("Request "+ param + "!!!!!!!!!!!!!!!!! + " + request.getParameter(param));
            }

            clearScreenFromSession(request);  

            
            switch (innerCommand) {
                case LOAD_MEMBER_FILES :
                    if (getMemberFiles(session, request)) {
//                        port.loadMemberFile(memberFileName, dependentFileName);
                    }
                break;
                case LOAD_PROVIDER_FILE :
                    if (getProviderFile(session, request)) {
  //                      port.loadProviderFile(providerFileName);
                    }
                break;
                case LOAD_DRUG_FILES :
                    if (getDrugFiles(session, request)) {
     //                   port.loadDrugFiles(genericDrugFile, nonGenericDrugFile, manufacturerDrugFile);
                    }
                break;
            }       

            RequestDispatcher dispatcher = request.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "LoadFileCommand";
    }

    private boolean getMemberFiles(HttpSession session, HttpServletRequest request) {

        boolean loadFile = true;

        if (request.getParameter("memberFileName") != null) {
            System.out.println(request.getParameter("memberFileName"));
            memberFileName = request.getParameter("memberFileName");
        } else {
            loadFile = false;
            session.setAttribute("memberFileName_error", "Invalid Member File Name");
        }

        if (request.getParameter("dependentFileName") != null) {
            System.out.println(request.getParameter("dependentFileName"));
            dependentFileName = request.getParameter("dependentFileName");
        } else {
            loadFile = false;
            session.setAttribute("dependentFileName_error", "Invalid Dependent File Name");
        }

        return loadFile;
    }

    private boolean getProviderFile(HttpSession session, HttpServletRequest request) {

        boolean loadFile = true;

        if (request.getParameter("providerFileName") != null) {
            System.out.println(request.getParameter("providerFileName"));
            providerFileName = request.getParameter("providerFileName");
        } else {
            loadFile = false;
            session.setAttribute("providerFileName_error", "Invalid Provider File Name");
        }

        return loadFile;
    }

    private boolean getDrugFiles(HttpSession session, HttpServletRequest request) {

        boolean loadFile = true;

        if (request.getParameter("genDrugFileName") != null) {
            genericDrugFile = request.getParameter("genDrugFileName");
        } else {
            loadFile = false;
            session.setAttribute("genDrugFileName_error", "Invalid Generic Drug File Name");
        }

        if (request.getParameter("nonGenDrugFileName") != null) {
            nonGenericDrugFile = request.getParameter("nonGenDrugFileName");
        } else {
            loadFile = false;
            session.setAttribute("nonGenDrugFileName_error", "Invalid Non Generic Drug File Name");
        }

        if (request.getParameter("manDrugFileName") != null) {
            manufacturerDrugFile = request.getParameter("manDrugFileName");
        } else {
            loadFile = false;
            session.setAttribute("manDrugFileName_error", "Invalid Manufacturer Drug File Name");
        }

        return loadFile;
    }

    @Override
    public String getName() {
        return "LoadFileCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.Biometrics;
import neo.manager.ChronicDiseaseBiometrics;

/**
 *
 * @author josephm
 */
public class LoadChronicQuestionsCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        System.out.println("Inside LoadChronicQuestionsCommand");

        String coverNumber = request.getParameter("memberNumber");
        int  code = Integer.parseInt(request.getParameter("depListValues"));
        ChronicDiseaseBiometrics chronic = new ChronicDiseaseBiometrics();

        Biometrics biometrics = service.getNeoManagerBeanPort().fetchBiometricsTypeId(chronic, coverNumber, code);

        try {
            PrintWriter out = response.getWriter();

            if(biometrics != null && biometrics.getWeight() != 0.0) {

               out.print(getName() + "|Weight=" + biometrics.getWeight());
               out.print("|Exercise=" + biometrics.getExercisePerWeek());
               out.print("|Length=" + biometrics.getHeight());
               out.print("|BMI=" + biometrics.getBmi());
               out.print("|BloodPressureSystolic=" + biometrics.getBloodPressureSystolic());
               out.print("|BloodPressuredDiastolic=" + biometrics.getBloodPressureDiastolic());
               out.print("|SmokingQuestion=" + biometrics.getCigarettesPerDay());
               out.print("|StoppedSmoking=" + biometrics.getYearsSinceStopped());
               out.print("|AlcoholConsumption=" + biometrics.getAlcoholUnitsPerWeek() + "$");

            }else {

                out.println("Errror|No such bioemtrics type|" + getName());
            }

        }catch(Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "LoadChronicQuestionsCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.calltrack.command;


import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CallTrackDetails;
import neo.manager.CallWorkbenchDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class AllocateCallTrackWorkbench extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        NeoManagerBean port = service.getNeoManagerBeanPort();
        String trackIdString = "" + request.getParameter("trackId");
        int trackId = Integer.parseInt(trackIdString);
        //get call track details
        CallTrackDetails ct = port.getCallByTrackID(trackId);
        session.setAttribute("WorkbenchCallTrackDetail", ct);
        //get call workbench details
        
        Collection<CallWorkbenchDetails> cwList = port.getCallWorkbenchByTrackId(trackId);
        for (CallWorkbenchDetails cw : cwList) {
            String callQueried = cw.getCallQueries();
            String qNames = "";
            HashMap<String, String> queryMap = new HashMap<String, String>();
            ArrayList<String> queryNames = new ArrayList();
            Collection<LookupValue> lookupList = port.getCodeTable(106);
            for (LookupValue lv : lookupList) {
                queryMap.put(lv.getId(), lv.getValue());
            }
            String[] selectedQs = callQueried.split(",");
            for (String s : selectedQs) {
                String q = queryMap.get(s);
                queryNames.add(q);
            }
            if (queryNames != null) {
                String s = "";
                for (String qn : queryNames) {
                    s = s + qn + " ";
                }
                qNames = s;
            }
           // System.out.println("Queries after lookup changes = " + qNames);
            cw.setCallQueries(qNames);
        }
        session.setAttribute("UserCallWorkbench", cwList);
        
        try {
            String nextJSP = "/Calltrack/CallWorkbenchHistory.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "AllocateCallTrackWorkbench";
    }
}

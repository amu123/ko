/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CopdBiometrics;

/**
 *
 * @author princes
 */
public class BiometricCOPDandBronchiectasisTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String javascript;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        List<CopdBiometrics> getCopd = (List<CopdBiometrics>) session.getAttribute("getCopd");
        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        String dateMeasured;
        try {

            out.print("<tr>");

            out.print("<th scope=\"col\">Date Measured</th>");
            out.print("<th scope=\"col\">Height</th>");
            out.print("<th scope=\"col\">Weight</th>");
            out.print("<th scope=\"col\">Blood Pressure Systolic</th>");
            out.print("<th scope=\"col\">Blood Pressure Diastolic</th>");
            out.print("<th scope=\"col\">FEV1 Predicted</th>");
            out.print("<th scope=\"col\">FEV1/FVC</th>");
            out.print("<th scope=\"col\">Dyspnea scale - Impairment</th>");
            out.print("<th scope=\"col\">Select</th>");
//            out.print("<th scope=\"col\">Date Measured</th>");
//            out.print("<th scope=\"col\">ICD10</th>");
//            out.print("<th scope=\"col\">Excercise</th>");            
//            out.print("<th scope=\"col\">Ex Smoker Stopped</th>");
//            out.print("<th scope=\"col\">Alchohol Units</th>");
//            out.print("<th scope=\"col\">FEV1</th>");
//            out.print("<th scope=\"col\">FEV1 / FVC</th>");
//            out.print("<th scope=\"col\">Dyspnea Scale</th>");
//            out.print("<th scope=\"col\">Source</th>");
//            out.print("<th scope=\"col\">Notes</th>");
            out.print("</tr>");

            if (getCopd != null && getCopd.size() > 0) {
                for (CopdBiometrics c : getCopd) {
                    out.print("<tr>");
                    if (c.getDateMeasured() != null) {
                        Date mDate = c.getDateMeasured().toGregorianCalendar().getTime();
                        dateMeasured = formatter.format(mDate);
                        out.print("<td><center><label class=\"label\">" + dateMeasured + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getHeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + c.getHeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getWeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + c.getWeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getBloodPressureSystolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getBloodPressureSystolic() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getBloodPressureDiastolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getBloodPressureDiastolic() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getFev1Predicted() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getFev1Predicted() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getFev1FVC() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getFev1FVC() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getDyspneaScale() != null) {
                        out.print("<td><center><label class=\"label\">" + c.getDyspneaScale() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("<td><center><button name=\"opperation\" type=\"button\" " + javascript + " value=\"" + c.getBiometricsId() + "\">Select</button></center></td>");

//                    if (c.getDateMeasured() != null) {
//                        Date mDate = c.getDateMeasured().toGregorianCalendar().getTime();
//                        dateMeasured = formatter.format(mDate);
//                        out.print("<td>" + dateMeasured + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (c.getIcdLookup() != null) {
//                        out.print("<td>" + c.getIcdLookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getExercisePerWeek() != 0) {
//                        out.print("<td>" + c.getExercisePerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }                    
//
//                    if (c.getYearsSinceStopped() > 0) {
//                        out.print("<td>" + c.getYearsSinceStopped() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getAlcoholUnitsPerWeek() != 0) {
//                        out.print("<td>" + c.getAlcoholUnitsPerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getFev1Predicted() != 0) {
//                        out.print("<td>" + c.getFev1Predicted() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getFev1FVC() != 0) {
//                        out.print("<td>" + c.getFev1FVC() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getDyspneaLookup() != null) {
//                        out.print("<td>" + c.getDyspneaLookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getSourceLookup() != null) {
//                        out.print("<td>" + c.getSourceLookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getDetail() != null) {
//                        out.print("<td>" + c.getDetail() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
                    out.print("</tr>");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

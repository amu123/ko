/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.lang.Exception;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author josephm
 */
public class CaptureHyperlipidaemiaBiometricsCommand extends NeoCommand {
    private String nextJSP;
    private Object _datatypeFactory;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        HttpSession session = request.getSession();
        boolean checkError = false;
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        
        //clearAllFromSession(request);
        
        HyperlipidaemiaBiometrics hyperlipidaemia = new HyperlipidaemiaBiometrics();
        hyperlipidaemia.setBiometricTypeId("8");
        
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        //String exerciseInWeek = request.getParameter("exerciseQuestion");
        String exerciseInWeek = (String) session.getAttribute("exerciseQuestion_text");
        if (exerciseInWeek != null && !exerciseInWeek.equalsIgnoreCase("")) {
            if(exerciseInWeek.matches("[0-9]+")){
            int exerciseInWeekValue = Integer.parseInt(exerciseInWeek);
            hyperlipidaemia.setExercisePerWeek(exerciseInWeekValue);
            }else {
            
                session.setAttribute("exerciseQuestion_error", "The exercise field should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("exerciseQuestion_error", "The exercise field should be filled in");
            checkError = true;
        }*/
        
        //String currentSmoker = request.getParameter("currentSmoker");
        String currentSmoker = (String) session.getAttribute("currentSmoker_text");
        if (currentSmoker != null && !currentSmoker.equalsIgnoreCase("99")) {
            hyperlipidaemia.setCurrentSmoker(currentSmoker);
            LookupValue smoker = new LookupValue();
            smoker.setId(currentSmoker);
            smoker.setValue(port.getValueFromCodeTableForId("YesNo Type", currentSmoker));
            hyperlipidaemia.setCurrentSmokerLookup(smoker);
        } /*else {

            session.setAttribute("currentSmoker_error", "The current smoker field should be filled in");
            checkError = true;
        }*/
        
        //String numberOfCigaretes = request.getParameter("smokingQuestion");
        String numberOfCigaretes = (String) session.getAttribute("smokingQuestion_text");
        if (numberOfCigaretes != null && !numberOfCigaretes.equalsIgnoreCase("")) {
            if(numberOfCigaretes.matches("[0-9]+")){
            int numberOfCigaretesValue = Integer.parseInt(numberOfCigaretes);
            hyperlipidaemia.setCigarettesPerDay(numberOfCigaretesValue);
            }else {
            
                session.setAttribute("smokingQuestion_error", "The cigarettes field should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("smokingQuestion_error", "The cigarettes field should be filled in");
            checkError = true;
        }*/
        
        //String exSmoker = request.getParameter("exSmoker");
        String exSmoker = (String) session.getAttribute("exSmoker_text");
        if (exSmoker != null && !exSmoker.equalsIgnoreCase("99")) {
            hyperlipidaemia.setExSmoker(exSmoker);
        } /*else {

            session.setAttribute("exSmoker_error", "The ex smoker field should be filled in");
            checkError = true;
        }*/
        
       //String yearsSinceSmoking = request.getParameter("stopped");
        String yearsSinceSmoking = (String) session.getAttribute("stopped_text");
        if (yearsSinceSmoking != null && !yearsSinceSmoking.equalsIgnoreCase("")) {
            if (yearsSinceSmoking.matches("[0-9]+")) {
                int yearsSinceSmokingValue = Integer.parseInt(yearsSinceSmoking);
                hyperlipidaemia.setYearsSinceStopped(yearsSinceSmokingValue);
                 session.setAttribute("stopped_error", "");
            } else {
                session.setAttribute("stopped_error", "The years stopped field should be a numeric value");
                checkError = true;
            }
        }
        
        //String alcoholUnits = request.getParameter("alcoholConsumption");
        String alcoholUnits = (String) session.getAttribute("alcoholConsumption_text");
        if (alcoholUnits != null && !alcoholUnits.equalsIgnoreCase("")) {
            if(alcoholUnits.matches("[0-9]+")) {
            int alcoholUnitsValue = Integer.parseInt(alcoholUnits);
            hyperlipidaemia.setAlcoholUnitsPerWeek(alcoholUnitsValue);
            }else {
            
                session.setAttribute("alcoholConsumption_error", "The alcohol units field should be a numeric value");
                checkError = true; 
            }
        } /*else {

            session.setAttribute("alcoholConsumption_error", "The alcohol units field should be filled in");
            checkError = true;
        }*/
        
        //String weight = request.getParameter("weight");
        //String height = request.getParameter("length");
        String weight = (String) session.getAttribute("weight_text");
        String height = (String) session.getAttribute("length_text");
        if (weight != null && !weight.equalsIgnoreCase("")) {
            if (weight.matches("([0-9]+(\\.[0-9]+)?)+")) {
                if (height != null && !height.equalsIgnoreCase("")) {
                    if (height.matches("([0-9]+(\\.[0-9]+)?)+")) {
                        double heightValue = Double.parseDouble(height);
                        double weightValue = Double.parseDouble(weight);
                        hyperlipidaemia.setHeight(heightValue);
                        hyperlipidaemia.setWeight(weightValue);
                    } else {
                        session.setAttribute("length_error", "The length should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("length_error", "Require length to calculate BMI");
                    checkError = true;
                }
            } else {

                session.setAttribute("weight_error", "The weight should be a numeric value");
                checkError = true;
            }
        }/*else {

         session.setAttribute("weight_error", "The weight should be filled in");
         checkError = true;
         }*/
        
       if (height != null && !height.equalsIgnoreCase("")) {
            if (height.matches("([0-9]+(\\.[0-9]+)?)+")) {
                if (weight != null && !weight.equalsIgnoreCase("")) {
                    if (weight.matches("([0-9]+(\\.[0-9]+)?)+")) {
                        double heightValue = Double.parseDouble(height);
                        double weightValue = Double.parseDouble(weight);
                        hyperlipidaemia.setHeight(heightValue);
                        hyperlipidaemia.setWeight(weightValue);
                    } else {
                        session.setAttribute("weight_error", "The weight should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("weight_error", "Require weight to calculate BMI");
                    checkError = true;
                }
            } else {

                session.setAttribute("length_error", "The length should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("length_error", "The length should be filled in");
         checkError = true;
         }*/
        
        //String bmi = request.getParameter("bmi");
        String bmi = (String) session.getAttribute("bmi_text");
        if (bmi != null && !bmi.equalsIgnoreCase("")) {
            if(bmi.matches("([0-9]+(\\.[0-9]+)?)+")){
            double bmiValue = Double.parseDouble(bmi);
            hyperlipidaemia.setBmi(bmiValue);
            }else {
            
                session.setAttribute("bmi_error", "The BMI should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("bmi_error", "The BMI should be filled in");
            checkError = true;
        }*/
        
        //String bloodPresureSystolic = request.getParameter("bpSystolic");
        //String bloodPressureDiastolic = request.getParameter("bpDiastolic");
        String bloodPresureSystolic = (String) session.getAttribute("bpSystolic_text");
        String bloodPressureDiastolic = (String) session.getAttribute("bpDiastolic_text");
        if (bloodPresureSystolic != null && !bloodPresureSystolic.equalsIgnoreCase("")) {
            if (bloodPresureSystolic.matches("[0-9]+")) {
                if (bloodPressureDiastolic != null && !bloodPressureDiastolic.equalsIgnoreCase("")) {
                    if (bloodPressureDiastolic.matches("[0-9]+")) {
                        int bloodvalueSys = Integer.parseInt(bloodPresureSystolic);
                        int bloodValueDia = Integer.parseInt(bloodPressureDiastolic);
                        hyperlipidaemia.setBloodPressureSystolic(bloodvalueSys);
                        hyperlipidaemia.setBloodPressureDiastolic(bloodValueDia);
                    } else {

                        session.setAttribute("bpDiastolic_error", "The blood pressure should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("bpDiastolic_error", "The blood pressure should be filled in");
                    checkError = true;
                }
            } else {

                session.setAttribute("bpSystolic_error", "The blood pressure should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bpSystolic_error", "The blood pressure should be filled in");
         checkError = true;
         }*/
        
        if (bloodPressureDiastolic != null && !bloodPressureDiastolic.equalsIgnoreCase("")) {
            if (bloodPressureDiastolic.matches("[0-9]+")) {
                if (bloodPresureSystolic != null && !bloodPresureSystolic.equalsIgnoreCase("")) {
                    if (bloodPresureSystolic.matches("[0-9]+")) {
                        int bloodvalueSys = Integer.parseInt(bloodPresureSystolic);
                        int bloodValueDia = Integer.parseInt(bloodPressureDiastolic);
                        hyperlipidaemia.setBloodPressureSystolic(bloodvalueSys);
                        hyperlipidaemia.setBloodPressureDiastolic(bloodValueDia);
                    } else {

                        session.setAttribute("bpSystolic_error", "The blood pressure should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("bpSystolic_error", "The blood pressure should be filled in");
                    checkError = true;
                }
            } else {

                session.setAttribute("bpDiastolic_error", "The blood pressure should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bpDiastolic_error", "The blood pressure should be filled in");
         checkError = true;
         }*/

        //String memberNumber = request.getParameter("memberNumber_text");
        String memberNumber = (String) session.getAttribute("memberNumber_text");
        if(memberNumber != null && !memberNumber.equalsIgnoreCase("")) {
            hyperlipidaemia.setMemberNumber(memberNumber);
        }else {

            session.setAttribute("memberNumber_error", "The member number should be filled in");
            checkError = true;
        }
        
        //String dependantCode = request.getParameter("depListValues");
        String dependantCode = (String) session.getAttribute("depListValues_text");
        if (dependantCode != null && !dependantCode.equalsIgnoreCase("99") && !dependantCode.equalsIgnoreCase("")
                && !dependantCode.equalsIgnoreCase("null")) {

            int code = Integer.parseInt(dependantCode);
            hyperlipidaemia.setDependantCode(code);
        } else {

            session.setAttribute("depListValues_error", "The cover dependants should be filled in");
            checkError = true;
        }

        //String totalCholesterol = request.getParameter("totalCholesterol");
        String totalCholesterol = (String) session.getAttribute("totalCholesterol_text");
        if(totalCholesterol != null && !totalCholesterol.equalsIgnoreCase("")) {
            if(totalCholesterol.matches("([0-9]+(\\.[0-9]+)?)+")){
            double totalCholesterolValue = Double.parseDouble(totalCholesterol);
            hyperlipidaemia.setTotalCholesterol(totalCholesterolValue);
            session.removeAttribute("totalCholesterol_text");
            }else {
            
                session.setAttribute("totalCholesterol_error", "The cholesterol should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("totalCholesterol_error", "The cholesterol should be filled in");
            checkError = true;
        }*/
        
        //String ldlcMol = request.getParameter("ldlc");
        String ldlcMol = (String) session.getAttribute("ldlc_text");
        if(ldlcMol != null && !ldlcMol.equalsIgnoreCase("")) {
            if(ldlcMol.matches("([0-9]+(\\.[0-9]+)?)+")){
            double ldlcMolValue = Double.parseDouble(ldlcMol);
            hyperlipidaemia.setLdlcMol(ldlcMolValue);
            session.removeAttribute("ldlc_text");
            }else {
            
                session.setAttribute("ldlc_error", "The LDLC should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("ldlc_error", "The LDLC should be filled in");
            checkError = true;
        }*/
        
        String hdlcMol = (String) session.getAttribute("hdlc_text");
        if(hdlcMol != null && !hdlcMol.equalsIgnoreCase("")) {
            if(hdlcMol.matches("([0-9]+(\\.[0-9]+)?)+")){
            double hdlcMolValue = Double.parseDouble(hdlcMol);
            hyperlipidaemia.setHdlcMol(hdlcMolValue);
            session.removeAttribute("hdlc_text");
            }else {
            
                session.setAttribute("hdlc_error", "The HDLC should be a numeric value");
                checkError = true;
            }
        }
        
        String framingham = (String) session.getAttribute("framingham_text");
        if(framingham != null && !framingham.equalsIgnoreCase("")) {
            if(framingham.matches("([0-9]+(\\.[0-9]+)?)+")){
            double framinghamValue = Double.parseDouble(framingham);
            hyperlipidaemia.setFramingham(framinghamValue);
            session.removeAttribute("framingham_text");
            }else {
            
                session.setAttribute("framingham_error", "The Framingham should be a numeric value");
                checkError = true;
            }
        }
        
        String score = (String) session.getAttribute("score_text");
        if(score != null && !score.equalsIgnoreCase("")) {
            if(score.matches("([0-9]+(\\.[0-9]+)?)+")){
            double scoreValue = Double.parseDouble(score);
            hyperlipidaemia.setScore(scoreValue);
            session.removeAttribute("score_text");
            }else {
            
                session.setAttribute("score_error", "The Score should be a numeric value");
                checkError = true;
            }
        }
        
        //String triglycerideMol = request.getParameter("triglyceride");
        String triglycerideMol = (String) session.getAttribute("triglyceride_text");
        if(triglycerideMol != null && !triglycerideMol.equalsIgnoreCase("")) {
            if(triglycerideMol.matches("([0-9]+(\\.[0-9]+)?)+")){
            double triglycerideMolValue = Double.parseDouble(triglycerideMol);
            hyperlipidaemia.setTriglycerideMol(triglycerideMolValue);
            session.removeAttribute("triglyceride_text");
            }else {
            
                session.setAttribute("triglyceride_error", "The tryglyceride should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("triglyceride_error", "The tryglyceride should be filled in");
            checkError = true;
        }*/
        
        //String tcHdlRatio = request.getParameter("tcHdlRatio");
        String tcHdlRatio = (String) session.getAttribute("tcHdlRatio_text");
        if(tcHdlRatio != null && !tcHdlRatio.equalsIgnoreCase("")) {
            if(tcHdlRatio.matches("([0-9]+(\\.[0-9]+)?)+")){
           double tcHdlRatioValue = Double.parseDouble(tcHdlRatio);
            hyperlipidaemia.setTcHdlRatio(tcHdlRatioValue);
            session.removeAttribute("tcHdlRatio_text");
            }else {
            
                session.setAttribute("tcHdlRatio_error", "The HDL ratio should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("tcHdlRatio_error", "The HDL ratio should be filled in");
            checkError = true;
        }*/
        
        //String detail = request.getParameter("detail");
        String detail = (String) session.getAttribute("hyperlipaemiaDetail_text");
        if(detail != null && !detail.equalsIgnoreCase("")) {

            hyperlipidaemia.setDetail(detail);
            session.removeAttribute("hyperlipaemiaDetail_text");
        } /*else {

            session.setAttribute("detail_error", "The detail should be filled in");
            checkError = true;
        }*/
        
        //String treatingProvider = request.getParameter("providerNumber_text");
        String treatingProvider = (String) session.getAttribute("providerNumber_text");
        if(treatingProvider != null && !treatingProvider.equalsIgnoreCase("")) {

            hyperlipidaemia.setTreatingProvider(treatingProvider);
        } /*else {

            session.setAttribute("providerNumber_error", "The treating provider should be filled in");
            checkError = true;
        }*/

        //String icd10 = request.getParameter("icd10");
        //String icd10 = (String) session.getAttribute("icd10_text");
        String icd10 = (String) session.getAttribute("hyperlipidaemiaLookupValue");
        
        if(icd10 != null && !icd10.equalsIgnoreCase("99")) {

            hyperlipidaemia.setIcd10(icd10);
            LookupValue icd = new LookupValue();
            icd.setId(icd10);
            icd.setValue(port.getValueFromCodeTableForId("Hyperlipidaemia Biometric ICD", icd10));
            hyperlipidaemia.setIcd10Lookup(icd);
        } else {

            session.setAttribute("icd10_error", "The ICD10 should be filled in");
            checkError = true;
        }

        //String source = request.getParameter("source");
        String source = (String) session.getAttribute("source_text");
        if(source != null && !source.equalsIgnoreCase("99")) {

            hyperlipidaemia.setSource(source);
        } /*else {

            session.setAttribute("source_error", "The source should be filled in");
            checkError = true;
        }*/

//         SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String UpdateBiometrics = (String) session.getAttribute("UpdateBiometrics");
//        if (UpdateBiometrics != null && UpdateBiometrics.equals("true")) {
//            dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd");
//        }

        try {
            //String measuredDate = request.getParameter("dateMeasured");
            String measuredDate = (String) session.getAttribute("dateMeasured_text");
            if (measuredDate != null && !measuredDate.equalsIgnoreCase("")) {
//            Date dateMeasured = dateTimeFormat.parse(measuredDate);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                Pattern pattern = Pattern.compile("\\s");
                Matcher matcher = pattern.matcher(measuredDate);
                boolean found = matcher.find();

                Date tFrom = null;
                XMLGregorianCalendar xTreatFrom = null;

                if (!found) {
                    tFrom = dateFormat.parse(measuredDate);
                } else {
                    tFrom = dateTimeFormat.parse(measuredDate);
                }

                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                hyperlipidaemia.setDateMeasured(xTreatFrom);
            } else {

                session.setAttribute("dateMeasured_error", "The date measured should be filled in");
                checkError = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//         SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        try {
            //String received = request.getParameter("dateReceived");
            String received = (String) session.getAttribute("dateReceived_text");
            if (received != null && !received.equalsIgnoreCase("")) {
//            Date dateReceived = dateFormat.parse(request.getParameter("dateReceived"));
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                Pattern pattern = Pattern.compile("\\s");
                Matcher matcher = pattern.matcher(received);
                boolean found = matcher.find();

                Date tFrom = null;
                XMLGregorianCalendar xTreatFrom = null;

                if (!found) {
                    tFrom = dateFormat.parse(received);
                } else {
                    tFrom = dateTimeFormat.parse(received);
                }

                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                hyperlipidaemia.setDateReceived(xTreatFrom);
            } else {
                session.setAttribute("dateReceived_error", "The date received should be filled in");
                checkError = true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
                if(checkError) {
                    saveScreenToSession(request);
                    nextJSP = "/biometrics/CaptureBiometrics.jsp";         //CaptureHyperlipidaemia.jsp";

                }else {
                //Check if the user is UPDATING or CREATING
                    if (UpdateBiometrics != null && UpdateBiometrics.equals("true")) {
                        String oldBioID = (String) session.getAttribute("BiometricID");
                        System.out.println("OLDBIOID = " + oldBioID);

                        int userId = user.getUserId();
                        BiometricsId bId = port.saveHyperlipidaemiaBiometrics(hyperlipidaemia, userId, 1, oldBioID);
                        request.setAttribute("revNo",bId.getReferenceNo());
                        int hyperId= bId.getId();
                        nextJSP = "/biometrics/CaptureBiometrics.jsp";         //CaptureHyperlipidaemia.jsp";
                        //clearAllFromSession(request);
                        if(hyperId > 0) {
                            hyperlipidaemia.setBiometricsId(hyperId);
    //                        port.createTrigger(hyperlipidaemia);
                            session.setAttribute("updated", "Your biometrics data has been captured");
                        }else{
                        session.setAttribute("failed", "Sorry! Hyperlipidaemia Biometrics save has failed, Please try again!");
                        nextJSP = "/biometrics/CaptureBiometrics.jsp";         //CaptureHyperlipidaemia.jsp";
                        }
                    } else {
                        int userId = user.getUserId();
                        BiometricsId bId = port.saveHyperlipidaemiaBiometrics(hyperlipidaemia, userId, 1, null);
                        request.setAttribute("revNo",bId.getReferenceNo());
                        int hyperId= bId.getId();
                        nextJSP = "/biometrics/CaptureBiometrics.jsp";         //CaptureHyperlipidaemia.jsp";
                        //clearAllFromSession(request);
                        if(hyperId > 0) {
                            hyperlipidaemia.setBiometricsId(hyperId);
    //                        port.createTrigger(hyperlipidaemia);
                            session.setAttribute("updated", "Your biometrics data has been captured");
                        }else{
                        session.setAttribute("failed", "Sorry! Hyperlipidaemia Biometrics save has failed, Please try again!");
                        nextJSP = "/biometrics/CaptureBiometrics.jsp";         //CaptureHyperlipidaemia.jsp";
                        }
                    }
                }

            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
            
        }catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

//    public XMLGregorianCalendar convertDateXML(Date date) {
//
//        GregorianCalendar calendar = new GregorianCalendar();
//        calendar.setTime(date);
//        return getDatatypeFactory().newXMLGregorianCalendar(calendar);
//
//    }
//
//    private DatatypeFactory getDatatypeFactory() {
//        if (_datatypeFactory == null) {
//
//            try {
//                _datatypeFactory = DatatypeFactory.newInstance();
//            } catch (DatatypeConfigurationException ex) {
//                ex.printStackTrace();
//            }
//        }
//
//        return (DatatypeFactory) _datatypeFactory;
//    }

    @Override
    public String getName() {
        return "CaptureHyperlipidaemiaBiometricsCommand";
    }
}

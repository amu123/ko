/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.tags;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthRuleResponse;

/**
 *
 * @author johanl
 */
public class AuthRuleMessageDisplay extends TagSupport {
    private String sessionAttribute;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        List<AuthRuleResponse> ruleList = (List<AuthRuleResponse>) session.getAttribute(sessionAttribute);
        if(ruleList != null){
            try{
                if (javaScript != null) {
                    out.println("<td colspan=\"8\"><table class=\"" + javaScript + "\" style=\"cellpadding=\"5\"; cellspacing=\"0\";\">");
                } else {
                    out.println("<td colspan=\"8\"><table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                }                
                out.println("<tr><th align=\"left\">Rule No</th><th align=\"left\">Message Code</th><th align=\"left\">Status</th><th align=\"left\">ICD10</th>" +
                        "<th align=\"left\">CPT</th><th align=\"left\">NHRPL</th><th align=\"left\">Short Description</th><th align=\"left\">Long Description</th></tr>");

                for (AuthRuleResponse rule : ruleList) {
                    out.println("<tr>");
                    out.println("<td><label class=\"label\">" + rule.getRuleNo() + "</label></td>");
                    out.println("<td><label class=\"label\">" + rule.getMessageCode() + "</label></td>");
                    out.println("<td><label class=\"label\">" + rule.getSeverity() + "</label></td>");
                    out.println("<td><label class=\"label\">" + rule.getIcd10Codes() + "</label></td>");
                    out.println("<td><label class=\"label\">" + rule.getCptCodes() + "</label></td>");
                    out.println("<td><label class=\"label\">" + rule.getNHRPLCodes() + "</label></td>");
                    out.println("<td><label class=\"label\">" + rule.getShortDesc() + "</label></td>");
                    out.println("<td><label class=\"label\">" + rule.getLongDesc() + "</label></td>");
                    out.println("</tr>");
                }

                out.println("</table></td>");

            }catch(IOException io){
                io.printStackTrace();
            }
        }

        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

}

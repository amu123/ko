/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author josephm
 */
public class MultipartControllerForm extends SimpleTagSupport {
    private String action = "/ManagedCare/AgileController?opperation=LoadDataFileCommand";
    private String validate = "No";
    private String name;
    private String enctype = "multipart/form-data";

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
 public void doTag() throws JspException {
        JspWriter out = getJspContext().getOut();

        try {

            // TODO: insert code to write html before writing the body content.
            // e.g.:
            //
            // out.println("<strong>" + attribute_1 + "</strong>");
            out.println("<form name=\""+name+"\"");
            out.println("action=\"" + action+"\"");
            out.println("enctype=\""+enctype +"\"");
           // out.println("type=\"file\"");
            if(validate.equalsIgnoreCase("yes")){
                out.println("onsubmit=\"return validate('"+name+"')\" ");
            }
            out.println("method=\"POST\">");

            JspFragment f = getJspBody();
            if (f != null) f.invoke(out);

            out.println("</form>");

        } catch (java.io.IOException ex) {
            throw new JspException("Error in ControllerForm tag", ex);
        }
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setValidate(String validate) {
        this.validate = validate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEnctype(String enctype) {
        this.enctype = enctype;
    }

}

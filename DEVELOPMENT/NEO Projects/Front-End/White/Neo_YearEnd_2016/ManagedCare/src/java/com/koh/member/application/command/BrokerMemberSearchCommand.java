/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.broker_firm.application.commands.BrokerFirmSearchCommand;
import com.koh.command.NeoCommand;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.BrokerFirm;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class BrokerMemberSearchCommand extends NeoCommand {


    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            search(request, response, context);
        } catch (Exception ex) {
            Logger.getLogger(BrokerMemberSearchCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void search(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        String code = request.getParameter("brokerCode");
        String name = request.getParameter("brokerName");
        List<KeyValueArray> kva = port.getBrokerList(code, name, "");

        Object col = MapUtils.getMap(kva);

        request.setAttribute("BrokerSearchResults", col);
        context.getRequestDispatcher("/Member/MemberBrokerSearchResults.jsp").forward(request, response);
    }

    @Override
    public String getName() {
        return "BrokerMemberSearchCommand";
    }
}
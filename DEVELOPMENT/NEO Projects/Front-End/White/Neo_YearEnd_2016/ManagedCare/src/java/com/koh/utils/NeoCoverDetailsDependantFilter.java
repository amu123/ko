/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import neo.manager.CoverDetails;
import neo.manager.EAuthCoverDetails;

/**
 *
 * @author
 * Johan-NB
 */
public class NeoCoverDetailsDependantFilter {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    private SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");

    public NeoCoverDetailsDependantFilter() {
    }

    public List<EAuthCoverDetails> EAuthCoverDetailsDependantFilter(List<EAuthCoverDetails> objectList) {
        List<EAuthCoverDetails> newList = new ArrayList<EAuthCoverDetails>();
        String coverNumber = "";
        HashMap<Integer, String> dependantMap = new HashMap<Integer, String>();
        for (EAuthCoverDetails ecd : objectList) {
            String cdCover = ecd.getCoverNumber();
            System.out.println("cdCover = " + cdCover);
            if (coverNumber.equalsIgnoreCase("")) {
                coverNumber = cdCover;

            } else if (!coverNumber.equalsIgnoreCase("") && !coverNumber.equalsIgnoreCase(cdCover)) {
                System.out.println("coverNumber = " + coverNumber);
                dependantMap = new HashMap<Integer, String>();
                coverNumber = cdCover;
            }

            //remove duplicates
            if (dependantMap.isEmpty()) {
                dependantMap.put(ecd.getDependentNumber(), ecd.getDependantType());
                newList.add(ecd);

            } else if (!dependantMap.containsKey(ecd.getDependentNumber())) {
                dependantMap.put(ecd.getDependentNumber(), ecd.getDependantType());
                newList.add(ecd);
            }
        }

        return newList;
    }

    public List<EAuthCoverDetails> EAuthCoverDetailsDependantFilterByPMemEndDate(List<EAuthCoverDetails> objectList, String coverNumber) {
        List<EAuthCoverDetails> newList = new ArrayList<EAuthCoverDetails>();
        //String coverNumber = "";
        HashMap<Integer, String> dependantMap = new HashMap<Integer, String>();

//        String endDateStr = "";
//        for (EAuthCoverDetails ecd : objectList) {
//            String dependantType = ecd.getDependantType();
//            if (dependantType.equalsIgnoreCase("Principal Member")) {
//                endDateStr = dateFormat.format(ecd.getCoverEndDate().toGregorianCalendar().getTime());
//                break;
//            }
//        }
//        System.out.println("endDateStr = " + endDateStr);
//        if (!endDateStr.equalsIgnoreCase("")) {
//            for (EAuthCoverDetails ecd : objectList) {
//                String cdCover = ecd.getCoverNumber();
//                System.out.println("cdCover = " + cdCover);
//
//                //end date comparison
//                String depEndDateStr = dateFormat.format(ecd.getCoverEndDate().toGregorianCalendar().getTime());
//                System.out.println("depEndDateStr = " + depEndDateStr);
//                if (depEndDateStr.equalsIgnoreCase(endDateStr)) {
//                    if (coverNumber.equalsIgnoreCase("")) {
//                        coverNumber = cdCover;
//
//                    } else if (!coverNumber.equalsIgnoreCase("") && !coverNumber.equalsIgnoreCase(cdCover)) {
//                        System.out.println("coverNumber = " + coverNumber);
//                        dependantMap = new HashMap<Integer, String>();
//                        coverNumber = cdCover;
//                    }
//
//                    //remove duplicates
//                    if (dependantMap.isEmpty()) {
//                        dependantMap.put(ecd.getDependentNumber(), ecd.getDependantType());
//                        newList.add(ecd);
//
//                    } else if (!dependantMap.containsKey(ecd.getDependentNumber())) {
//                        dependantMap.put(ecd.getDependentNumber(), ecd.getDependantType());
//                        newList.add(ecd);
//                    }
//                }
//            }
//        }
        Date pmStartDate = null;
        Date pmEndDate = null;
        for (EAuthCoverDetails ecd : objectList) {
            if (ecd.getCoverNumber().equals(coverNumber)
                    && ecd.getDependantType().equalsIgnoreCase("Principal Member")) {
                pmStartDate = ecd.getCoverStartDate().toGregorianCalendar().getTime();
                pmEndDate = ecd.getCoverEndDate().toGregorianCalendar().getTime();
                newList.add(ecd); //Adding princible member first on the list
                break;
                /*if(pmStartDate == null){
                 pmStartDate = ecd.getCoverStartDate().toGregorianCalendar().getTime();
                
                 }else{
                 Date testStart = ecd.getCoverStartDate().toGregorianCalendar().getTime();
                 if(testStart.after(pmStartDate)){
                 pmStartDate = testStart;
                 }
                 break;
                 }   */
            }
        }

        if (pmStartDate != null && pmEndDate != null) {
            for (EAuthCoverDetails ecd : objectList) {
                if (ecd.getCoverNumber().equals(coverNumber)
                        && !ecd.getDependantType().equalsIgnoreCase("Principal Member")) { //Principal Member is already added
                    //String cdCover = ecd.getCoverNumber();
                    //System.out.println("cdCover = " + cdCover);

                    if (ecd.getCoverNumber().equals(coverNumber)) {

                        //end date comparison
                        Date depStartDateStr = ecd.getCoverStartDate().toGregorianCalendar().getTime();

//     The following two lines are commented out: Responsible for checking if the dependents are before the main member's join date
//     Reason: member 65300198097 has that issue and a call was logged to remove this check:
//                    if ((depStartDateStr.equals(pmStartDate) || depStartDateStr.after(pmStartDate))
//                            && (depStartDateStr.equals(pmEndDate) || depStartDateStr.before(pmEndDate))) {

                        /*
                         if (coverNumber.equalsIgnoreCase("")) {
                         coverNumber = cdCover;

                         } else if (!coverNumber.equalsIgnoreCase("") && !coverNumber.equalsIgnoreCase(cdCover)) {
                         System.out.println("coverNumber = " + coverNumber);
                         dependantMap = new HashMap<Integer, String>();
                         coverNumber = cdCover;
                         }*/

                        //remove duplicates
                        if (dependantMap.isEmpty()) {
                            dependantMap.put(ecd.getDependentNumber(), ecd.getDependantType());
                            newList.add(ecd);

                        } else if (!dependantMap.containsKey(ecd.getDependentNumber())) {
                            dependantMap.put(ecd.getDependentNumber(), ecd.getDependantType());
                            newList.add(ecd);
                        }
//                    }
                    }
                }
            }
        }

        return newList;
    }

    public List<EAuthCoverDetails> EAuthCoverDetailsPrincipleMemberFilter(List<EAuthCoverDetails> objectList, String coverNumber) {
        List<EAuthCoverDetails> newList = new ArrayList<EAuthCoverDetails>();
        for (EAuthCoverDetails ecd : objectList) {
            if (ecd.getCoverNumber().equals(coverNumber)
                    && ecd.getDependantType().equalsIgnoreCase("Principal Member")) {
                newList.add(ecd);
            }
        }

        return newList;
    }

    public List<CoverDetails> CoverDetailsDependantFilter(List<CoverDetails> objectList) {
        List<CoverDetails> newList = new ArrayList<CoverDetails>();
        String coverNumber = "";
        HashMap<Integer, String> dependantMap = new HashMap<Integer, String>();
        for (CoverDetails cd : objectList) {
            String cdCover = cd.getCoverNumber();
            if (coverNumber.equalsIgnoreCase("")) {
                coverNumber = cdCover;
                dependantMap = new HashMap<Integer, String>();

            } else if (!coverNumber.equalsIgnoreCase("") && !coverNumber.equalsIgnoreCase(cdCover)) {
                dependantMap = new HashMap<Integer, String>();

            }

            //remove duplicates
            if (dependantMap.isEmpty()) {
                dependantMap.put(cd.getDependentNumber(), cd.getDependentType());
                newList.add(cd);

            } else if (!dependantMap.containsKey(cd.getDependentNumber())) {
                dependantMap.put(cd.getDependentNumber(), cd.getDependentType());
                newList.add(cd);
            }

        }
        return newList;
    }
}

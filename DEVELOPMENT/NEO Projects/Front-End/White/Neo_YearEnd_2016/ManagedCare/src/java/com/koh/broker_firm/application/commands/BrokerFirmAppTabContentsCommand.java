/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker_firm.application.commands;

import com.koh.brokerFirm.BrokerFirmMainMapping;
import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;

/**
 *
 * @author princes
 */
public class BrokerFirmAppTabContentsCommand extends NeoCommand {

    private static final String JSP_FOLDER = "/Broker/";

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String tab = request.getParameter("tab");
        String page = "";
        int entityId = 0;
        if (request.getParameter("brokerFirmEntityId") != null && !request.getParameter("brokerFirmEntityId").isEmpty()) {
            entityId = new Integer(request.getParameter("brokerFirmEntityId"));
            request.setAttribute("brokerFirmEntityId", entityId);          
        }

        if ("FirmAppDetails".equalsIgnoreCase(tab)) {
            page = getFirmDetails(request, entityId);
        } else if ("Accreditation".equalsIgnoreCase(tab)) {
            page = getBrokerFirmAccreditation(request, entityId);
        } else if ("BankingDetails".equalsIgnoreCase(tab)) {
            page = getBankingDetails(request, entityId);
        } else if ("ContactDetails".equalsIgnoreCase(tab)) {
            page = getContactDetails(request);
        } else if ("Transactions".equalsIgnoreCase(tab)) {
            page = getAuditTrail(request);
        } else if ("CommissionTransactions".equalsIgnoreCase(tab)) {
            page = getCommissionTransactions(request, entityId);
        } else if ("Notes".equalsIgnoreCase(tab)) {
            page = getBrokerFirmNotes(request);
        } else if ("Documents".equalsIgnoreCase(tab)) {
            page = getDocuments(request, entityId);
        } else if ("CommunicationsLog".equalsIgnoreCase(tab)) {
            page = getCommunicationsLog(request);
        }

        RequestDispatcher dispatcher = context.getRequestDispatcher(page);
        try {
            dispatcher.forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(BrokerFirmAppTabContentsCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BrokerFirmAppTabContentsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }


        return null;
    }

    @Override
    public String getName() {
        return "BrokerFirmAppTabContentsCommand";
    }

    public String getFirmDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        if (entityId != 0) {
            BrokerFirm bf = port.fetchBrokerFirmByEntityId(entityId);
            BrokerFirmMainMapping.setFirmApplication(request, bf);
            BrokerFirmConcessionDetails ba = port.fetchBrokerConcessionByBrokerEntity(entityId);
            BrokerFirmMainMapping.setConcessionFirmApplication(request, ba);
            String commInd = "";
            
                if(bf.getCommisionInd() == 0){
                    commInd = "No";                   
                } else if(bf.getCommisionInd() == 1){
                    commInd = "Yes";  
                }
            
            request.setAttribute("firmAppName", bf.getBrokerAltCode());
            request.setAttribute("firmAppCode", bf.getBrokerFirmCode());
            request.setAttribute("brokerFirmEntityId", bf.getEntityId());
            request.setAttribute("brokerFirmTile", bf.getContactTitle());
            request.setAttribute("brokerFirmContactPerson", bf.getContactFirstName());
            request.setAttribute("brokerFirmContactSurname", bf.getContactLastName());
            request.setAttribute("firmRegion", bf.getRegion());  
            request.setAttribute("VIPBroker", bf.getVipBrokerInd());  
            //request.setAttribute("commissionInd", commInd);
            //request.setAttribute("FirmAppCommisionInd", bf.getCommisionInd());
            //session.setAttribute("currentCommissionInd", bf.getCommisionInd());
            if(ba!=null){
                request.setAttribute("brokerFirmEntityId", ba.getFirmEntityId()); 
                request.setAttribute("concessionFlag", ba.getFirmEntityId()); 
                request.setAttribute("FirmApp_ConcessionStart", DateTimeUtils.convertXMLGregorianCalendarToDate(ba.getConcessionStartDate())); 
                request.setAttribute("FirmApp_ConcessionEnd", DateTimeUtils.convertXMLGregorianCalendarToDate(ba.getConcessionEndDate())); 
            }
        }   
        return JSP_FOLDER + "BrokerFirmDetails.jsp";
    }

    public String getBrokerFirmAccreditation(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        if (entityId != 0) {
            List<BrokerAccred> ba = port.fetchBrokerAccredByEntityId(entityId);
            //System.out.println("ba size " + ba.size());
            request.setAttribute("accreList", ba);
        }
        return JSP_FOLDER + "Accreditation.jsp";
    }

    private String getBankingDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        if (entityId != 0) {
            List<BankingDetails> bd = port.getAllBankingDetails(entityId);
            //System.out.println("Banking list size = " + bd.size());
            BrokerFirmMainMapping.setBankingDetails(request, bd);
        }
        return JSP_FOLDER + "BankingDetails.jsp";
    }

    private String getCommissionTransactions(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        if (entityId != 0) {
            Date endDate = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(endDate);
            cal.add(Calendar.MONTH, -6);
            Date startDate = cal.getTime();
            cal.setTime(endDate);
            cal.add(Calendar.MONTH, 1);
            endDate = cal.getTime();
            DateTimeUtils.convertDateToXMLGregorianCalendar(endDate);
            List<KeyValueArray> kva = port.fetchCommissionPaymentsByDate(entityId, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
            List<Map<String, String>> map = MapUtils.getMap(kva);
            request.setAttribute("comm_start_date", DateTimeUtils.convertToYYYYMMDD(startDate));
            request.setAttribute("comm_end_date", DateTimeUtils.convertToYYYYMMDD(endDate));
            request.setAttribute("CommissionPayments", map);
        }
        return JSP_FOLDER + "CommissionTransactions.jsp";
    }

    private String getBrokerFirmNotes(HttpServletRequest request) {
//        List<KeyValueArray> kva = port.fetchQuestions(191);
//        Object obj = MapUtils.getMap(kva);
        //       Object obj = port.fetchAppQuestions(2);
//        request.setAttribute("MemAppGeneralQuestions", obj);
        return JSP_FOLDER + "BrokerFirmNotes.jsp";
    }

    private String getDocuments(HttpServletRequest request, int entityId) {
//        List<KeyValueArray> kva = port.fetchQuestions(191);
//        Object obj = MapUtils.getMap(kva);
        //       Object obj = port.fetchAppQuestions(2);
//        request.setAttribute("MemAppGeneralQuestions", obj);
        request.setAttribute("entityId", entityId);
        request.getSession().setAttribute("memberCoverEntityId", entityId);
        return JSP_FOLDER + "BrokerFirmDocuments.jsp";
    }

    private String getCommunicationsLog(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String brokerFirmEntityId = request.getParameter("brokerFirmEntityId");
        request.setAttribute("entityId", brokerFirmEntityId);
        List<CommunicationLog> commLogForEntityId = port.getCommLogForEntityId(Integer.parseInt(brokerFirmEntityId), null, null, null);
        request.setAttribute("communicationsLog", commLogForEntityId);
        return JSP_FOLDER + "BrokerFirmCommunicationsLog.jsp";    }

    private String getAuditTrail(HttpServletRequest request) {
        String brokerFirmEntityId = request.getParameter("brokerFirmEntityId");
//        List<KeyValueArray> kva = port.fetchQuestions(191);
//        Object obj = MapUtils.getMap(kva);
        //       Object obj = port.fetchAppQuestions(2);
//        request.setAttribute("MemAppGeneralQuestions", obj);
        return JSP_FOLDER + "BrokerFirmAuditTrail.jsp";
    }

    private String getContactDetails(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        request.setAttribute("firm_map", BrokerFirmMainMapping.getMapping());
        //String brokerFirmEntityId = request.getParameter("brokerFirmEntityId");
        String brokerFirmEntityId = request.getParameter("brokerFirmEntityId");
        String brokerFirmTile = request.getParameter("brokerFirmTile");
        String brokerFirmContactPerson = request.getParameter("brokerFirmContactPerson");
        String brokerFirmContactSurname = request.getParameter("brokerFirmContactSurname");
        String firmRegion = request.getParameter("firmRegion");
        request.setAttribute("brokerFirmTile", brokerFirmTile);
        request.setAttribute("brokerFirmContactPerson", brokerFirmContactPerson);
        request.setAttribute("brokerFirmContactSurname", brokerFirmContactSurname);
        request.setAttribute("brokerFirmEntityId", brokerFirmEntityId);
        request.setAttribute("firmRegion", firmRegion);

        /*
         * System.out.println("-- brokerFirmEntityId " + brokerFirmEntityId);
         * System.out.println("-- brokerFirmContactPerson " +
         * brokerFirmContactPerson); System.out.println("-- brokerFirmTile " +
         * brokerFirmTile); System.out.println("-- brokerFirmContactSurname " +
         * brokerFirmContactSurname); System.out.println("-- firmRegion " + firmRegion);
         */

        int entityId;
        if (brokerFirmEntityId != null && !brokerFirmEntityId.isEmpty()) {
            entityId = Integer.parseInt(brokerFirmEntityId);

            BrokerFirm eg = port.fetchBrokerFirmByEntityId(entityId);
            Collection<ContactDetails> cd = port.getContactDetailsByEntityId(entityId);
            Collection<AddressDetails> ad = port.getAddressDetailsByEntityId(entityId);
            BrokerFirmMainMapping.setBrokerFirm(request, eg);
            BrokerFirmMainMapping.setAddressDetails(request, ad);

            BrokerFirmMainMapping.setContactDetails(request, cd);
            //request.setAttribute("employerEntityId", employerId);
            //request.setAttribute("EmployerName", eg.getEmployerName());
            //request.setAttribute("EmployerNumber", eg.getEmployerNumber());
        }
        return JSP_FOLDER + "ContactDetails.jsp";
    }
}
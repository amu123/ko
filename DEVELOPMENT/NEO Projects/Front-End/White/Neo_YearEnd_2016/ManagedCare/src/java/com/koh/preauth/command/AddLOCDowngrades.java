/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.XmlUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import org.w3c.dom.Element;
import org.w3c.dom.Document;

/**
 *
 * @author Johan-NB
 */
public class AddLOCDowngrades extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();

        PrintWriter out = null;
        String xmlStr = "";
        response.setHeader("Cache-Control", "no-cache");
        response.setContentType("text/xml");
        response.setStatus(200);
        @SuppressWarnings("static-access")
        Document doc = new XmlUtils().getDocument();

        Element root = doc.createElement("DowngradeOptions");
        doc.appendChild(root);

        String levelOfCareType = request.getParameter("locType");
        List<LookupValue> lookups = port.getCodeTable(173);

        //System.out.println("levelOfCareType = "+levelOfCareType);
        
        for (LookupValue lv : lookups) {

            String[] downTypes = lv.getId().split("-");
            String downTo = downTypes[1];

            //System.out.println("downTo type = "+downTo);
            if (levelOfCareType.trim().equalsIgnoreCase(downTo)) {
                Element option = doc.createElement("Option");
                //System.out.println("found downTo");
                
                String optIdName = lv.getId() + "|" + lv.getValue();
                option.setTextContent(optIdName);
                
                root.appendChild(option);
            }
        }

        try {
            out = response.getWriter();
            try {
                xmlStr = XmlUtils.getXMLString(doc);
            } catch (IOException ex) {
                ex.printStackTrace();
                xmlStr = "";
            }
            //System.out.println("product option xml = "+xmlStr);
            out.println(xmlStr);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "AddLOCDowngrades";
    }
}

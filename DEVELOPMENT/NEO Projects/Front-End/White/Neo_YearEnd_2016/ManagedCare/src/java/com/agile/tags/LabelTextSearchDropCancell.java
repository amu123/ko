/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class LabelTextSearchDropCancell extends TagSupport {

    private String displayName;
    private String elementName;
    private String javaScriptOnDropdown;
    private String valueFromRequest = "";
    private String searchOpperation;
    private String onScreen;

    public void setOnScreen(String onScreen) {
        this.onScreen = onScreen;
    }

    public void setSearchOpperation(String searchOpperation) {
        this.searchOpperation = searchOpperation;
    }

    
    public void setValueFromRequest(String valueFromRequest) {
        this.valueFromRequest = valueFromRequest;
    }

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        try {
            if (!valueFromRequest.equalsIgnoreCase("")) {
                out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"250px\"><input disabled type=\"text\" id=\"" + elementName + "_text\" name=\"" + elementName + "_text\" value=\"" + req.getAttribute(valueFromRequest) + "\" size=\"30\"/>");
            } else {
                out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"250px\"><input disabled type=\"text\" id=\"" + elementName + "_text\" name=\"" + elementName + "_text\" value=\"\" size=\"30\"/>");
            }
            out.println("<td colspan=\"3\"align=\"left\"><table><tr><td><img src=\"/ManagedCare/resources/Search.gif\" width=\"28\" height=\"28\" alt=\"Search\" border=\"0\"/ onClick=\"submitWithAction('" + searchOpperation + "', '"+elementName+"', '"+onScreen+"')\";></td>");
            out.println("<td><select name=\"" + elementName + "_list\" WIDTH=\"200\" STYLE=\"width: 216px\">");
            try {
                ArrayList list = (ArrayList) session.getAttribute(elementName);
                System.out.println("trying to get list");
                System.out.println(list);
                if (list != null) {
                    for (int i = 0; i < list.size(); i++) {
                        Object object = list.get(i);
                        out.println("<option value=\"" + object.toString() + "\" selected>" + object.toString() + "</option>");
                        System.out.println(object.toString());

                        System.out.println(object.getClass());
                    }
                }
            } catch (ClassCastException cce) {
                //do nothing
//                out.println("<option value=\"" + session.getAttribute(elementName) + "\" selected>" + session.getAttribute(elementName) + "</option>");
//                System.out.println("Cant cast");
//                System.out.println(session.getAttribute(elementName));

            }
            out.println("</select></td>");
            out.println("<td><img src=\"/ManagedCare/resources/Close.gif\" width=\"28\" height=\"28\" alt=\"Clear\" border=\"0\"/></td></tr></table>");
            out.println("</td>");

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelTextSearchDropCancell tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setJavaScriptOnDropdown(String javaScriptOnDropdown) {
        this.javaScriptOnDropdown = javaScriptOnDropdown;
    }
}

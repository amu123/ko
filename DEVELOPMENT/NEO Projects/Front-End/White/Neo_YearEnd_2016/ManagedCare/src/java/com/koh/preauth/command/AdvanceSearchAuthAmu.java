package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static com.koh.command.NeoCommand.service;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import neo.manager.SearchResultsAmu;

public class AdvanceSearchAuthAmu extends NeoCommand {

    @Override
    public String getName() {
        return "AdvanceSearchAuthAmu";
    }

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        HttpSession session = request.getSession();
        List<SearchResultsAmu> search = null;
        
        String entityName = request.getParameter("entityName");
        String subject = request.getParameter("subject");
           
        if(entityName != null && subject != null){
            search = getResultByEntityNameAndSubject(entityName, subject);
        }
        
        if(search != null){
            
            session.setAttribute("items", search);

            try{
                String nextJSP = "/PreAuth/AmukelanimSearch.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

                dispatcher.forward(request, response);
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }

        return null;
    }

    private List<SearchResultsAmu> getResultByEntityNameAndSubject(String entityName, String subject) {
       
        List<SearchResultsAmu> search = null;
        
        if((entityName != null && entityName != "") && (subject != null && subject != "")){

                System.out.println("DO Something here using entity name and subject and return to jsp " + entityName + " " + subject);

                search = service.getNeoManagerBeanPort().searchByEntityAmuMethod(entityName, subject);
                
            }
        return search;
    }
   
}
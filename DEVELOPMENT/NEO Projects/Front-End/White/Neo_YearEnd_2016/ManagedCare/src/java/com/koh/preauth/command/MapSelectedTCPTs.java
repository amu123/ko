/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.FormatUtils;
import java.lang.Exception;
import com.koh.preauth.utils.ERPUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author johanl
 */
public class MapSelectedTCPTs extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        String nextJSP = "" + session.getAttribute("onScreen");

        //Simple date format for current system time only
        Calendar cal = Calendar.getInstance();
        cal.getTime();
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        List<AuthTariffCPT> tcListAll = (List<AuthTariffCPT>) session.getAttribute("mappedListOfCptTariffs");
        List<AuthTariffCPT> tcListNew = new ArrayList<AuthTariffCPT>();
        List<AuthCPTDetails> cptList = null;
        boolean cptIsNull = false;
        if (session.getAttribute("authCPTListDetails") != null) {
            cptList = (List<AuthCPTDetails>) session.getAttribute("authCPTListDetails");
            if (cptList == null && cptList.isEmpty()) {
                cptList = new ArrayList<AuthCPTDetails>();
            }
        } else {
            cptList = new ArrayList<AuthCPTDetails>();
        }

        HashMap<String, AuthCPTDetails> cptListMap = new HashMap<String, AuthCPTDetails>();
        for (AuthCPTDetails cpt : cptList) {
            cptListMap.put(cpt.getCptCode(), cpt);
        }

        System.out.println("MapSelectedTCPTs selectedValues = " + request.getParameter("selectedValues"));

        double totEstimatedWard = 0.0d;
        double totEstimatedRPL = 0.0d;
        double copay = 0.0d;

        String admission = "" + session.getAttribute("admissionDateTime");
        String pICD = "" + session.getAttribute("primaryICD_text");
        String facility = "" + session.getAttribute("facilityProvider_text");
        String facilityDisc = "" + session.getAttribute("facilityProvDiscTypeId");
        String prodId = "" + session.getAttribute("scheme");
        String optId = "" + session.getAttribute("schemeOption");
        int provDesc = Integer.parseInt(facilityDisc);
        int pID = Integer.parseInt(prodId);
        int oID = Integer.parseInt(optId);

        int wardLookup = getWARDLookupID(provDesc);

        String ttAmount = "" + session.getAttribute("mapTariffTotal");

        if (ttAmount != null && !ttAmount.equalsIgnoreCase("null") && !ttAmount.equalsIgnoreCase("")) {
            totEstimatedRPL = new Double(ttAmount);
        }

        boolean doSomething = false;

        String selectedValues = request.getParameter("selectedValues");

        if (selectedValues != null && !selectedValues.equalsIgnoreCase("")
                && !selectedValues.equalsIgnoreCase("null")) {
            doSomething = true;
        }

        if (doSomething) {

            String[] selectedValuesArr = selectedValues.split("\\|");
            for (String s : selectedValuesArr) {
                if (s != null && !s.equals("")) {
                    int index = Integer.parseInt(s);
                    AuthTariffCPT tcpt = tcListAll.get(index);
                    tcListNew.add(tcpt);
                }
            }

            for (AuthTariffCPT tc : tcListNew) {
                AuthCPTDetails cpt = new AuthCPTDetails();

                if (!cptListMap.containsKey(tc.getCptCode())) {

                    Procedure proc = service.getNeoManagerBeanPort().getProcedureForCode(tc.getCptCode());

                    if (proc != null) {
                        cpt.setCptCode(tc.getCptCode());
                        cpt.setCptCodeDesc(proc.getDescription());
                        cpt.setTheatreTime(60);
                        //cpt.setCoPaymentAmount(newCopay);

                        cptList.add(cpt);

                    } else {
                        System.out.println("MapSelectedTCPTs CPT call failed: " + tc.getCptCode());
                    }
                }
            }

            //skip LOC mapping on update
            System.out.println("skip LOC onScreen = " + nextJSP);
            if (!nextJSP.equalsIgnoreCase("/PreAuth/HospitalAuthDetails_Update.jsp")) {
                List<AuthLocLosDetails> locLosList = new ArrayList<AuthLocLosDetails>();

                boolean skipLocLosCalc = false;
                skipLocLosCalc = validateLocLocProvDesc(provDesc);

                if (skipLocLosCalc) {
                    if (cptList != null && !cptList.isEmpty()) {

                        locLosList = port.calculateLOSByCPT(cptList, facilityDisc);
                        System.out.println("locLosList size mapping tcpts = " + locLosList.size());
                        locLosList = DateTimeUtils.orderLocLosByType(locLosList);

                        if (locLosList != null && !locLosList.isEmpty()) {

                            System.out.println("locLosList size = " + locLosList.size());

                            Date aDate = null;
                            XMLGregorianCalendar xAdmission = null;
                            XMLGregorianCalendar previousEnd = null;

                            try {
                                aDate = FormatUtils.dateTimeFormat.parse(admission);
                                xAdmission = DateTimeUtils.convertDateToXMLGregorianCalendar(aDate);

                            } catch (ParseException ex) {
                                Logger.getLogger(MapSelectedTCPTs.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            List<AuthHospitalLOC> locList = new ArrayList<AuthHospitalLOC>();

                            int totLos = 0;
                            int x = 0;
                            for (AuthLocLosDetails ll : locLosList) {

                                if (previousEnd != null) {
                                    xAdmission = previousEnd;
                                    aDate = previousEnd.toGregorianCalendar().getTime();
                                }

                                String code = NeoCommand.service.getNeoManagerBeanPort().getValueFromCodeTableForTableId(wardLookup, ll.getLocType());

                                int los = ll.getLos();
                                totLos += los;

                                double estimatedWardAmount = 0.0d;
                                //load ward tariff if exist
                                if (code != null && !code.equals("")) {
                                    estimatedWardAmount = ERPUtils.getEstimatedCostByLOSForWard(xAdmission, pID, oID, code, facilityDisc, facility, pICD, los);
                                    totEstimatedWard += estimatedWardAmount;
                                }

                                //get end date form los 
                                Date endDate = DateTimeUtils.calculateLOCEndDateFromLOS(aDate, los);
                                previousEnd = DateTimeUtils.convertDateToXMLGregorianCalendar(endDate);

                                //set level of care
                                AuthHospitalLOC loc = new AuthHospitalLOC();
                                loc.setUserId(user.getUserId());

                                loc.setLevelOfCare(ll.getLocType());
                                loc.setLengthOfStay(los);
                                loc.setApprovedLos(los);
                                loc.setDateFrom(xAdmission);
                                loc.setDateTo(previousEnd);
                                loc.setEstimatedCost(estimatedWardAmount);

                                locList.add(loc);

                                x++;
                            }
                            //set totLos
                            String prevEndDateString = "";
                            String prevEndTimeString = "";
                            Date dateTo = null;
                            prevEndDateString = DateTimeUtils.convertXMLGregorianCalendarToDate(previousEnd);
                            prevEndTimeString = sdfTime.format(cal.getTime());

                            try {
                                dateTo = FormatUtils.dateTimeFormat.parse(prevEndDateString + " " + prevEndTimeString);
                                //previousEnd = DateTimeUtils.convertDateToXMLGregorianCalendar(dateTo);
                            } catch (ParseException ex) {
                                ex.printStackTrace();
                            }

                            session.setAttribute("los", totLos);
                            session.setAttribute("AuthLocList", locList);
                            session.setAttribute("dischargeDateTime", sdfDateTime.format(dateTo));
                            
                            //validate penalty
                            Calendar admissionCal = Calendar.getInstance();
                            Calendar authDateCal = Calendar.getInstance();

                            Date authorisationDate = null;
                            String authDate = "" + session.getAttribute("authDate");
                            try {
                                authorisationDate = FormatUtils.dateTimeFormat.parse(authDate);
                            } catch (ParseException ex) {
                                Logger.getLogger(MapSelectedTCPTs.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            Date admissionDate72 = null;
                            try {
                                admissionDate72 = FormatUtils.dateTimeNoSecFormat.parse(admission);
                            } catch (ParseException ex) {
                                Logger.getLogger(MapSelectedTCPTs.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            admissionCal.setTime(admissionDate72);
                            authDateCal.setTime(authorisationDate);
                            authDateCal.add(Calendar.HOUR_OF_DAY, -72);

                            session.setAttribute("isPenalty", null);
                            if (authDateCal.equals(admissionCal) || authDateCal.after(admissionCal)) {
                                session.setAttribute("isPenalty", "yes");
                            } else {
                                String dspCoPay = "" + session.getAttribute("isDSPPenalty");
                                if (dspCoPay.equalsIgnoreCase("no")) {
                                    session.setAttribute("isPenalty", null);
                                }
                                session.setAttribute("authPenFound", null);
                            }
                            
                        } else {
                            System.out.println("loc list is null");
                        }
                    }
                }
            }
        } else {
            List<AuthHospitalLOC> sessionLOCList = (List<AuthHospitalLOC>) session.getAttribute("AuthLocList");

            for (AuthHospitalLOC loc : sessionLOCList) {
                totEstimatedWard += loc.getEstimatedCost();
            }

        }

        //set co payment for cpt
        //cptList = (List<AuthCPTDetails>) session.getAttribute("authCPTListDetails");

        String admissionD = "" + session.getAttribute("admissionDate");
        Date admissionDate = null;
        XMLGregorianCalendar authStart = null;
        try {
            if (admissionD != null && !admissionD.equalsIgnoreCase("null")
                    && !admissionD.equalsIgnoreCase("")) {
                admissionDate = new SimpleDateFormat("yyyy/MM/dd").parse(admissionD);
                authStart = DateTimeUtils.convertDateToXMLGregorianCalendar(admissionDate);
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        //get auth member option id
        String optionId = "" + session.getAttribute("schemeOption");

        if (cptList != null && !cptList.isEmpty()) {
            session.setAttribute("authCPTListDetails", cptList);
            copay = CalculateCoPaymentAmounts.setCopayment(session);
        }
        System.out.println("co payment amount = " + copay);
        session.setAttribute("coPay", copay);

        //set amounts and los for mappings
        double hospInterim = 0.0d;
        hospInterim = totEstimatedRPL + totEstimatedWard;
        String hospInt = FormatUtils.decimalFormat.format(hospInterim);

        session.setAttribute("hospInterim", hospInt);

        try {
            System.out.println("MapSelectedTCPTs onscreen = " + nextJSP);
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "MapSelectedTCPTs";
    }

    public boolean validateLocLocProvDesc(int provDesc) {
        if (provDesc != 57 && provDesc != 58) {
            return false;
        } else {
            return true;
        }
    }

    public int getWARDLookupID(int providerType) {
        int wardId = 280;
        if (providerType == 79) {
            wardId = 281;

        } else if (providerType == 55) {
            wardId = 265;

        } else if (providerType == 59) {
            wardId = 266;

        } else if (providerType == 49) {
            wardId = 267;

        } else if (providerType == 76) {
            wardId = 268;

        } else if (providerType == 47) {
            wardId = 269;

        } else if (providerType == 56) {
            wardId = 270;

        } else if (providerType == 77) {
            wardId = 271;

        }
        return wardId;
    }
}

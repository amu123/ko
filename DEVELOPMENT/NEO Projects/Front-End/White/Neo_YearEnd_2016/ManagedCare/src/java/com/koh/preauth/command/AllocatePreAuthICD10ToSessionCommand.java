/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.auth.dto.ICD10SearchResult;
import com.koh.command.Command;
import com.koh.command.NeoCommand;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;

/**
 *
 * @author martins
 */
public class AllocatePreAuthICD10ToSessionCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        
        String code = request.getParameter("code");
        String desc = request.getParameter("description");
        session.setAttribute("icd_text", code);
        session.setAttribute("icdDescription", desc);
        
        try {
            String nextJSP = "/PreAuth/AuthNappiProductDetails.jsp";
            System.out.println("nextJSP " + nextJSP);
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            System.out.println("test");
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "AllocatePreAuthICD10ToSessionCommand";
    }
}

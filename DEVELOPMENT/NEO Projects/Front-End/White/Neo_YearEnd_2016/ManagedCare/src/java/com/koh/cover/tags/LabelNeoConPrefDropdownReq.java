/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.tags;

import com.koh.utils.LookupUtils;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.*;

/**
 *
 * @author johanl
 */
public class LabelNeoConPrefDropdownReq extends TagSupport {
    private String displayName;
    private String elementName;
    private String commType;
    private String javaScript;
    private String mandatory = "no";
    private String errorValueFromSession = "no";
    private String firstIndexSelected = "no";

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        ServletRequest request = pageContext.getRequest();
        
        
        try {
            out.println("<td><label class=\"label\" for=\"" + elementName + "\">" + displayName + ":</label></td>");
            out.println("<td><select style=\"width:215px\" name=\"" + elementName + "\" id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");
            String selectedBox = "" + pageContext.getRequest().getAttribute(elementName);
            //Neo Lookups
            
            List<neo.manager.LookupValue> lookUps = null;
            
            if(session.getAttribute("memberCoverEntityId") != null){
                int entityId = (Integer) session.getAttribute("memberCoverEntityId");
                lookUps = LookupUtils.seachContactPrefDropdown(entityId);
            }else if(session.getAttribute("coverEntityId") != null){
                int entityId2 = (Integer) session.getAttribute("coverEntityId");
                lookUps = LookupUtils.seachContactPrefDropdown(entityId2);
            }

            out.println("<option value=\"99\"></option>");
            boolean firstIndexSet = false;
            for (int i = 0; i < lookUps.size(); i++) {
                LookupValue lookupValue = lookUps.get(i);
                if (lookupValue.getId().equalsIgnoreCase(selectedBox)) {
                    out.println("<option value=\"" + lookupValue.getId() + "\" selected >" + lookupValue.getValue() + "</option>");
                } else {
                    if(firstIndexSelected.equalsIgnoreCase("yes")){
                        if(firstIndexSet){
                            out.println("<option value=\"" + lookupValue.getId() + "\" >" + lookupValue.getValue() + "</option>");
                        }else{
                            out.println("<option value=\"" + lookupValue.getId() + "\" selected >" + lookupValue.getValue() + "</option>");
                            firstIndexSet = true;
                            System.out.println("found first index");
                        }
                    }else{
                        out.println("<option value=\"" + lookupValue.getId() + "\" >" + lookupValue.getValue() + "</option>");
                    }
                    
                }
            }
            out.println("</select></td>");
            if (mandatory.equalsIgnoreCase("yes")) {
                out.println("<td><label class=\"red\">*</label></td>");
            } else {
                out.println("<td></td>");
            }
                       
            String sessionValerror = "";
            if (errorValueFromSession.equalsIgnoreCase("yes")) {

                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                }

                if (sessionValerror != null && !sessionValerror.equalsIgnoreCase("null")) {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
                } else {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
                }
            }

            /*
            if(errorValueFromSession.equalsIgnoreCase("yes")){
                String errorVal = "" + session.getAttribute(elementName + "_error");
                if(errorVal == null || errorVal.trim().equalsIgnoreCase("null")){
                    errorVal = "";
                }
                out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">"+ errorVal +"</label></td>");
            }else{
                out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
            }*/

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelNeoLookupValueDropDown tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setCommType(String commType) {
        this.commType = commType;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setErrorValueFromSession(String errorValueFromSession) {
        this.errorValueFromSession = errorValueFromSession;
    }

    public void setFirstIndexSelected(String firstIndexSelected) {
        this.firstIndexSelected = firstIndexSelected;
    }
    
//    private List<LookupValue> setContactToPrefFromReguest(HttpServletRequest request){
//        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
//        
//        List<LookupValue> lvList = new ArrayList<LookupValue>();
//        
//        int entityId = MemberMainMapping.getEntityId(request);
//        
//        //get address details
//        List<AddressDetails> adList = port.getAddressDetailsByEntityId(entityId);
//        
//        for (AddressDetails ad : adList) {
//            String id = "A"+ad.getAddressTypeId();
//            String value = port.getValueFromCodeTableForTableId(33, String.valueOf(ad.getAddressTypeId()));
//            
//            LookupValue lv = new LookupValue();
//            lv.setId(id);
//            lv.setValue(value);
//            
//            lvList.add(lv);
//        }
//        
//        //get contact details
//        List<ContactDetails> conList = port.getContactDetailsByEntityId(entityId);
//        for (ContactDetails con : conList) {
//            
//            String id = "C"+con.getCommunicationMethodId();
//            String value = port.getValueFromCodeTableForTableId(35, String.valueOf(con.getCommunicationMethodId()));
//            
//            LookupValue lv = new LookupValue();
//            lv.setId(id);
//            lv.setValue(value);
//            
//            lvList.add(lv);            
//        }        
//        
//        return lvList;
//    }
    
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author sphephelot
 */
public class MemberAppDocumentGenerationCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------In MemberDocumentGenerationCommand---------------------");

            String memberAppCoverNumber = request.getParameter("memberAppCoverNumber");
            String docType = request.getParameter("docType");
            logger.info("memberAppCoverNumber = " + memberAppCoverNumber);
            logger.info("docType = " + docType);
             Map<String, String> fieldsMap = new HashMap<String, String>();
            if (memberAppCoverNumber != null && !memberAppCoverNumber.trim().equals("")) {
                
                String filePath = NeoCommand.service.getNeoManagerBeanPort().processDocumentGenerationExtended(0, memberAppCoverNumber, docType, false);
               
                if(filePath !=null && !filePath.equals("")){
                   logger.info("Succesful Generation");
                   logger.info("File = " + filePath);
                   fieldsMap.put("DocGen_"+docType, filePath);
                }else { 
                    logger.error("Error generating .... " );
                    fieldsMap.put("DocGen_"+docType, filePath);
                }
            }
            String updateFields = TabUtils.convertMapToJSON(fieldsMap);
            request.setAttribute("memberAppCoverNumber", memberAppCoverNumber);
            String result = TabUtils.buildJsonResult(null, null, updateFields, null);
            PrintWriter out = response.getWriter();
            out.println(result);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "MemberAppDocumentGenerationCommand";
    }

    @Override
    public String getName() {
        return "MemberAppDocumentGenerationCommand";
    }
}

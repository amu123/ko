/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

/**
 *
 * @author josephm
 */
import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoUser;

/**
 *
 * @author josephm
 */
public class SearchUsersProfilesCommand extends NeoCommand {
    
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
    this.saveScreenToSession(request);
        System.out.println("Inside SearchUsersProfilesCommand");
        
        HttpSession session = request.getSession();
        
        NeoUser userSearch = new NeoUser();
        
        String name = request.getParameter("name");
        if (name != null && !name.equalsIgnoreCase("")) {
            userSearch.setName(name);
        }
        String surname = request.getParameter("surname");
        if(surname != null && !surname.equalsIgnoreCase("")) {
        
            userSearch.setSurname(surname);
        }
        
        String emailAdress = request.getParameter("email");
        if(emailAdress != null && !emailAdress.equalsIgnoreCase("")) {
        
            userSearch.setEmailAddress(emailAdress);
        }
        
        String username = request.getParameter("username");
        if(username != null && !username.equalsIgnoreCase("")) {
        
            userSearch.setUsername(username);
        }
        
        
        ArrayList<Integer> resp = new ArrayList<Integer>();
        Enumeration e = request.getParameterNames();
        while (e.hasMoreElements()) {
            String pName = "" + e.nextElement();
            if (pName.contains("resp_")) {
                resp.add(new Integer(request.getParameter(pName)));
            }
        }
        
        Collection<NeoUser> userList = service.getNeoManagerBeanPort().fetchNeoUsersByCriteria(userSearch, resp);
        
        session.setAttribute("userProfileList", userList);
        
        
        try {
            
            String nextJSP = "/SystemAdmin/UpdateUserProfile.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        
        }catch(Exception ex) {
        
            ex.printStackTrace();
        }
        
        return null;
    }
    
    @Override
    public String getName() {
    
        return "SearchUsersProfilesCommand";
    }
    
}

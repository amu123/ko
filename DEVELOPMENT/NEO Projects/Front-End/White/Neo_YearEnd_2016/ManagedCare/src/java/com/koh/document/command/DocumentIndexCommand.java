/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;

import agility.za.indexdocumenttype.DeleteDocType;
import agility.za.indexdocumenttype.IndexDocType;
import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;
import org.apache.log4j.Logger;
import java.lang.Exception;

/**
 *
 * @author Christo
 */
public class DocumentIndexCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------In Index command---------------------");

            String srcDrive = new PropertiesReader().getProperty("DocumentIndexWorkDrive");
            String indexDrive = new PropertiesReader().getProperty("DocumentIndexIndexDrive");
            String selectedFile = request.getParameter("selectedFile");
            String claimNumber = request.getParameter("claimNumber")!=null?request.getParameter("claimNumber"):"";
            String memberNumber = request.getParameter("memberNumber");
            String docType = request.getParameter("docType");
            String profileType = (String)request.getSession().getAttribute("profile")!=null?(String)request.getSession().getAttribute("profile"):"";
            String folderList = (String) request.getParameter("folderList");
            String listOrWork = (String) request.getParameter("listOrWork");
            String entityType = ((String) request.getParameter("entitytype"))!= null?((String) request.getParameter("entitytype")):"";
            if (selectedFile == null || selectedFile.equals("")) {
                request.setAttribute("folderList", folderList);
                request.setAttribute("listOrWork", listOrWork);
                request.setAttribute("indexEntityNumber",memberNumber);
                request.setAttribute("indexRefNumber",claimNumber);
                request.setAttribute("docType",docType);
                errorMessage(request, response, "No file selected",profileType);
                return "DocumentIndexCommand";
            }            
            if(profileType.contains("Generic") && (entityType == null || entityType.equals("") || entityType.equals("99"))){
                request.setAttribute("folderList", folderList);
                request.setAttribute("listOrWork", listOrWork);
                request.setAttribute("indexEntityNumber",memberNumber);
                request.setAttribute("indexRefNumber",claimNumber);
                request.setAttribute("docType",docType);
                errorMessage(request, response, "No entity type selected",profileType);
                return "DocumentIndexCommand";
            }
            if (memberNumber == null || memberNumber.equals("")) {
                request.setAttribute("folderList", folderList);
                request.setAttribute("listOrWork", listOrWork);
                request.setAttribute("docType",docType);
                if(profileType.contains("Generic")){
                    errorMessage(request, response, "No entity number selected",profileType);
                }else{
                    errorMessage(request, response, "No member number selected",profileType);
                }
                return "DocumentIndexCommand";
            }else if(!validateMemberNumebr(memberNumber , request.getSession())&& !profileType.contains("Generic")){
                
                request.setAttribute("folderList", folderList);
                request.setAttribute("listOrWork", listOrWork);
                request.setAttribute("indexEntityNumber",memberNumber);
                request.setAttribute("indexRefNumber",claimNumber);
                request.setAttribute("docType",docType);
                if(profileType.contains("Generic")){
                    errorMessage(request, response, "Please enter valid entity number",profileType);
                }else{
                    errorMessage(request, response, "Please enter valid member number",profileType);
                }
                return "DocumentIndexCommand";
            }
            
            if(profileType.contains("Claim") && (claimNumber == null || claimNumber.equals(""))){
                request.setAttribute("folderList", folderList);
                request.setAttribute("listOrWork", listOrWork);
                request.setAttribute("indexEntityNumber",memberNumber);
                request.setAttribute("indexRefNumber",claimNumber);
                request.setAttribute("docType",docType);
                errorMessage(request, response, "No claim number selected",profileType);
                return "DocumentIndexCommand";
            }else if(!validateClaimNumber(claimNumber,memberNumber) && claimNumber != null && !claimNumber.equals("")){
                request.setAttribute("folderList", folderList);
                request.setAttribute("listOrWork", listOrWork);
                request.setAttribute("indexEntityNumber",memberNumber);
                request.setAttribute("indexRefNumber",claimNumber);
                request.setAttribute("docType",docType);
                errorMessage(request, response, "Please enter valid claim number", profileType);
                return "DocumentIndexCommand";
            }
            
            logger.info("folderList = " + folderList);
            logger.info("selectedFile = " + selectedFile);
            IndexDocumentRequest req = new IndexDocumentRequest();
            NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
            req.setAgent(loggedInUser);
            req.setIndexType("Index");
            req.setSrcDrive(srcDrive);
            req.setEntityNumber(memberNumber);
            if(entityType!=null && !entityType.equals("")){
                req.setEntityType(Integer.parseInt(entityType));
            }else{
                req.setEntityType(5);
            }            
            IndexDocType indexDocType = new IndexDocType();
            indexDocType.setDestDrive(indexDrive);
            indexDocType.setFolder(folderList+"/"+profileType);
            indexDocType.setSrcFile(selectedFile);            
            indexDocType.setEntityNumber(memberNumber);
            if(claimNumber!=null && !claimNumber.equals("")){
                req.setRef(claimNumber);
                indexDocType.setDocType("10");
            }else{
                indexDocType.setDocType(docType);
            }
            if(entityType!=null && !entityType.equals("")){
                indexDocType.setEntityType(Integer.parseInt(entityType));
            }else{
                indexDocType.setEntityType(5);
            }            
            req.setIndexDoc(indexDocType);
            System.out.println("Before Webservice");
            IndexDocumentResponse res = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
            System.out.println("res = "+res.isIsSuccess());
            System.out.println("res = "+res.getMessage());
            if (res.isIsSuccess()) {
                req.setIndexDoc(null);
                req.setIndexType("Delete");
                DeleteDocType deleteDocType = new DeleteDocType();
                deleteDocType.setFolder(folderList+"/"+profileType);
                deleteDocType.setFilename(selectedFile);
                req.setDelete(deleteDocType);
                res = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
            } else {
                errorMessage(request, response, res.getMessage(),profileType);
                return "DocumentIndexCommand";              
            }
            request.setAttribute("folderList", folderList);
            request.setAttribute("listOrWork", listOrWork);
            RequestDispatcher dispatcher = null;
            if(profileType.toLowerCase().contains("claim")){
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentClaims.jsp");
                    }else if(profileType.toLowerCase().contains("member")){
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentMember.jsp");
                    }else{
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentGeneric.jsp");
                    }
            
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "DocumentIndexCommand";
    }

    @Override
    public String getName() {
        return "DocumentIndexCommand";
    }

    private boolean validateMemberNumebr(String memberNumebr , HttpSession session){
        boolean result = false;
        boolean viewStaff = (Boolean) session.getAttribute("persist_user_viewStaff");
        String email = "";
        String message="";
            NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
            CoverDetails c = port.getEAuthResignedMembers(memberNumebr);
            java.util.logging.Logger.getLogger(this.getClass().getName()).info("viewStaff " + viewStaff);
            if (!viewStaff) {
                if (c != null && c.getEntityId() > 0) {
                    Member mem = port.fetchMemberAddInfo(c.getEntityId());
                    c.setMemberCategory(mem.getMemberCategoryInd());
                    java.util.logging.Logger.getLogger(this.getClass().getName()).info("EntityId " + c.getEntityId() + " MemberCategoryInd " +mem.getMemberCategoryInd() );
                    if (c.getMemberCategory() != 2) {
                        //Get email contact details
                        ContactDetails cd = port.getContactDetails(c.getEntityId(), 1);
                        if (cd != null && cd.getPrimaryIndicationId() == 1) {
                            email = cd.getMethodDetails();
                        }
                        java.util.logging.Logger.getLogger(this.getClass().getName()).info("Response : " + getName() + "|Number=" + memberNumebr + "|Email=" + email + "$");
                        message = getName() + "|Number=" + memberNumebr + "|Email=" + email + "|entityId=" + c.getEntityId() + "$";
                    } else {
                        message = "Access|User access denied. Please contact your supervisor|" + getName();
                    }
                } else {
                    message = "Error|No such member|" + getName();
                }
            } else {
                if (c != null && c.getEntityId() > 0) {
                    java.util.logging.Logger.getLogger(this.getClass().getName()).info("EntityId " + c.getEntityId() + " No MemberCategoryInd loaded");
                    //Get email contact details
                    ContactDetails cd = port.getContactDetails(c.getEntityId(), 1);
                    if (cd != null && cd.getPrimaryIndicationId() == 1) {
                        email = cd.getMethodDetails();
                    }
                    java.util.logging.Logger.getLogger(this.getClass().getName()).info("Response : " + getName() + "|Number=" + memberNumebr + "|Email=" + email + "$");
                    message = getName() + "|Number=" + memberNumebr + "|Email=" + email + "|entityId=" + c.getEntityId() + "$";

                } else {
                    message = "Error|No such member|" + getName();
                }
            }
            
            if(!message.contains("Error")){
                    result = true;
                }else{
                    result = false;
                }
        return result;
    }
    
    private boolean  validateClaimNumber(String claimNumber,String coverNumber){
        boolean result=false;
        try{
            long claimNo = Long.parseLong(claimNumber);
            NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
            result = port.validateClaimForMember(claimNo, coverNumber);
        }catch(NumberFormatException ne){
            result = false;
        }catch(Exception e){
            logger.error("Failed to validate claim number", e);
            result = false;
        }
        return result;
    }
    
    private void errorMessage(HttpServletRequest request, HttpServletResponse response, String msg,String profileType) throws Exception {
        request.setAttribute("errorMsg", msg);
        RequestDispatcher dispatcher = null;
        if(profileType.toLowerCase().contains("claim")){
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentClaims.jsp");
                    }else if(profileType.toLowerCase().contains("member")){
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentMember.jsp");
                    }else{
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentGeneric.jsp");
                    }
        dispatcher.forward(request, response);
    }
}
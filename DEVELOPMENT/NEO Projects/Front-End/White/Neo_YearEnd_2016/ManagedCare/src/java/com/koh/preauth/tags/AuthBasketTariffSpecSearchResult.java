/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author Johan-NB
 */
public class AuthBasketTariffSpecSearchResult extends TagSupport {

    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        ServletRequest req = pageContext.getRequest();

        List<AuthTariffDetails> basketList = (List<AuthTariffDetails>) session.getAttribute("PreAuthBATSearchList");
        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");

            String message = (String) req.getAttribute("authBasketATSearchResultsMessage");

            if (message != null && !message.equalsIgnoreCase("")) {
                out.println("<label class=\"red\">" + message + "</label></br></br>");
            }

            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Provider Category</th><th>Tariff Code</th><th>Tariff Desc</th><th>Tariff Amount</th><th></th></tr>");

            if (basketList != null) {
                for (AuthTariffDetails ab : basketList) {
                    
                    out.println("<tr><form>"
                            + "<td>"
                            + "<label class=\"label\">" + ab.getProviderType() + "</label>"
                            + "<input type=\"hidden\" name=\"providerDesc\" value=\"" + ab.getProviderType() + "\"/>"
                            + "</td>"
                            + "<td>"
                            + "<label class=\"label\">" + ab.getTariffCode() + "</label>"
                            + "<input type=\"hidden\" name=\"btCode\" value=\"" + ab.getTariffCode() + "\"/>"
                            + "</td>"
                            + "<td width=\"200\">"
                            + "<label class=\"label\">" + ab.getTariffDesc() + "</label>"
                            + "<input type=\"hidden\" name=\"btDesc\" value=\"" + ab.getTariffDesc() + "\"/>"
                            + "</td>"
                            + "<td>"
                            + "<label class=\"label\">" + ab.getFrequency() + "</label>"
                            + "<input type=\"hidden\" name=\"tariffFreq\" value=\"" + ab.getFrequency() + "\"/>"
                            + "<input type=\"hidden\" name=\"tariffOption\" value=\"" + ab.getTariffOption() + "\"/>"
                            + "</td>"
                            + "<td>"
                            + "<button name=\"opperation\" type=\"submit\" " + javaScript + " value=\"" + commandName + "\">Select</button>"
                            + "</td>"
                            + "</form></tr>");
                }
            }
            out.println("</table>");

        } catch (IOException io) {
            io.printStackTrace();
        }

        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

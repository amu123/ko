/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.Claim;
import neo.manager.ClaimSearchCriteria;
import neo.manager.ClaimSearchResult;
import neo.manager.ClaimsBatch;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author princes
 */
public class ClaimsHistoryTabel extends TagSupport {

    private static final long serialVersionUID = 1L;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     * @return 
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

        System.out.println("entered ViewClaimsCommand");
        String coverNumber = (String) session.getAttribute("policyNumber");
        Integer depenNo = (Integer) session.getAttribute("dependantNumber");
        //Claim selectedClaim = (Claim) session.getAttribute("selected");
        System.out.println("coverNumber " + coverNumber);
        System.out.println("depenNo " + depenNo);
        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.MONTH, -6); // substract 6 month

        ClaimSearchCriteria search = new ClaimSearchCriteria();

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        try {
            String today = dateFormat.format(calendar.getTime());
            Date now = dateFormat.parse(today);
            XMLGregorianCalendar xmlNow = DateTimeUtils.convertDateToXMLGregorianCalendar(now);
            calendar.add(Calendar.MONTH, -6);
            String sixLater = dateFormat.format(calendar.getTime());
            Date sixMonths = dateFormat.parse(sixLater);
            XMLGregorianCalendar xmlSixMonthsLater = DateTimeUtils.convertDateToXMLGregorianCalendar(sixMonths);
            search.setProcessDateFrom(xmlSixMonthsLater);
            search.setProcessDateTo(xmlNow);

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }

        search.setCoverNumber(coverNumber);
        search.setDependentCode(depenNo);

        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        Security _secure = new Security();
        _secure.setCreatedBy(user.getUserId());
        _secure.setLastUpdatedBy(user.getUserId());
        _secure.setSecurityGroupId(2);

        ClaimSearchResult batch = port.fetchBatchClaimAndClaimLineIdsBySearchCriteria(search, _secure);
        int count = 0;
        List<Claim> claims = new ArrayList<Claim>();
        try {
            out.print("<tr>");
            out.print("<th scope=\"col\">Claim Number</th>");
            out.print("<th scope=\"col\">Practice Number</th>");
            out.print("<th scope=\"col\">Account Number</th>");
            out.print("<th scope=\"col\">Service Provider</th>");
            out.print("<th scope=\"col\">Discipline Type</th>");
            out.print("<th scope=\"col\">Member Number</th>");
            out.print("<th scope=\"col\">Action</th>");
            out.print("</tr>");

            for (ClaimsBatch claim : batch.getBatchs()) {

                for (Claim list_claim : claim.getClaims()) {
                    String catagory = port.getValueForId(59, list_claim.getClaimCategoryTypeId() + "");

                    out.print("<tr>");
                    out.print("<td>" + list_claim.getClaimId() + "</td>");
                    out.print("<td>" + list_claim.getPracticeNo() + "</td>");
                    if (list_claim.getPatientRefNo() != null) {
                        out.print("<td>" + list_claim.getPatientRefNo() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("<td>" + list_claim.getServiceProviderNo() + "</td>");
                    out.print("<td>" + catagory + "</td>");
                    out.print("<td>" + coverNumber + "</td>");
                    out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('ViewClaimDetailsCommand', " + count + ")\"; value=\"ViewClaimDetailsCommand\">Details</button></td>");
                    out.print("</tr>");
                    count++;
                    claims.add(list_claim);
                }
            }
            session.setAttribute("listOfClaims", claims);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }

        return super.doEndTag();
    }
}

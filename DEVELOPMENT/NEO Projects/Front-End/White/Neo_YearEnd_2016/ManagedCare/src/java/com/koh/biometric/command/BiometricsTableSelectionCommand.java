/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.pdc.command.CoverDependantResultCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.BiometricMemDetail;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import java.util.Comparator;

/**
 *
 * @author gerritr
 */
public class BiometricsTableSelectionCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        session.setAttribute("showSummaryTable", "false");
        String ddIndex = request.getParameter("ID_text");
        System.out.println("ddIndex: " + ddIndex);
        Collection<BiometricMemDetail> sessionTable = new ArrayList<BiometricMemDetail>();

        NeoManagerBean port = service.getNeoManagerBeanPort();

        ArrayList<LookupValue> arrIcdList = (ArrayList<LookupValue>) session.getAttribute("IcdList");
        ArrayList<LookupValue> mListNew = new ArrayList<LookupValue>();

        LookupValue icdList = new LookupValue();
        icdList.setId("0");
        icdList.setValue(" ");
        
        // SORTING THE VALUES ASC
        Collections.sort(arrIcdList, new Comparator<LookupValue>() {
            public int compare(LookupValue v1, LookupValue v2) {
                return v1.getValue().compareTo(v2.getValue());
            }
        });

        boolean firstRunFlag = true;
        for (LookupValue icd : arrIcdList) {//Insert blank space for the dropdown
            if (firstRunFlag == true) {
                if (icd.getId().equals("0")) {
                    mListNew.add(arrIcdList.get(0));
                } else {
                    mListNew.add(icdList);
                }
                firstRunFlag = false;
            }
            
            if (ddIndex.equals(icd.getId())) {
                sessionTable = (Collection<BiometricMemDetail>) session.getAttribute(icd.getValue());
                System.out.println("icd.getValue() - : " + icd.getValue());
                session.setAttribute("ddSelection", icd.getValue());
                mListNew.get(0).setId(icd.getId());
                mListNew.get(0).setValue(icd.getValue());
            } else {
                if (!mListNew.contains(icd)) {
                    mListNew.add(icd);
                }
            }
        }
        
        ArrayList arrSortedValue = new ArrayList();

        firstRunFlag = true;
        for (LookupValue arrUnSorted : mListNew) {
            if (firstRunFlag == true) {
                firstRunFlag = false;
            } else {
                arrSortedValue.add(arrUnSorted.getValue());
            }
        }

        

        arrIcdList = mListNew;
        for (int i = 0; i < arrSortedValue.size() - 1; i++) {
            for (LookupValue unsorted : arrIcdList) {
                if (unsorted.getValue().equals(arrSortedValue.get(i).toString())) {
                    mListNew.get(i + 1).setId(arrIcdList.get(i + 1).getId());
                    mListNew.get(i + 1).setValue(arrIcdList.get(i + 1).getValue());
                }
            }
        }
        session.setAttribute("IcdList", mListNew);
        System.out.println("sessionTable - size: " + sessionTable.size());
        if(mListNew != null && !mListNew.get(0).getValue().equals("") && !mListNew.get(0).getValue().equals(" "))
        {
            session.setAttribute("ViewBiometricsInfo", sessionTable);
        }
        if (ddIndex == null) {
            try {
                String nextJSP = "/biometrics/BiometricsView.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }

        PrintWriter out = null;
        try {
            out = response.getWriter();

            if (ddIndex != null) {
                out.print("Done|");
            } else {
                out.print("Failed|");
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(CoverDependantResultCommand.class
                    .getName()).log(Level.SEVERE, null, ex);
            System.out.print(
                    "Exception PRINTWRITER: " + ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "BiometricsTableSelectionCommand";
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.biometric.command;

import com.koh.command.Command;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.BiometricsTabs;
import org.apache.log4j.Logger;

/**
 *
 * @author martins
 */
public class CaptureSessionCommand extends Command {

    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        
            String value = request.getParameter("value_text");
            String elementName = request.getParameter("elementName_text");
            
            if ((value != null && !value.equals("")) || (elementName != null  && !elementName.equals(""))) {
                session.setAttribute(elementName + "_text", value);
            }else if(value == null || value.equals("")){
                session.setAttribute(elementName + "_text", "");
            }
            if ((value != null && !value.equals("")) || (elementName != null  && !elementName.equals(""))) {
                session.setAttribute(elementName + "text", value);
            }else if(value == null || value.equals("")){
                session.setAttribute(elementName + "text", "");
            }
            
            //System.out.println("### saving session: " + elementName + " value: " + session.getAttribute(elementName + "_text"));
            
//            if ((value != null && !value.equals("")) || (elementName != null  && !elementName.equals(""))) {
//                session.setAttribute(elementName, value);
//            }else if(elementName != null && value.equals("")){
//                session.setAttribute(elementName, "");
//            }
            return null;
        }

        @Override
        public String getName() {
            return "CaptureSessionCommand";
        }
    
}
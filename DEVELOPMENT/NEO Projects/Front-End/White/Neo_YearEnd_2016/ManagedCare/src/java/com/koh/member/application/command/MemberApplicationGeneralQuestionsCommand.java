/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.member.application.MemberAppCommon;
import com.koh.member.application.MemberAppDepMapping;
import com.koh.member.application.MemberAppGeneralDetailsMapping;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class MemberApplicationGeneralQuestionsCommand extends NeoCommand{
 
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        try {
            String s = request.getParameter("buttonPressed");
            if (s != null && !s.isEmpty()) {
                System.out.println("buttonPressed : " + s);
                if (s.equalsIgnoreCase("Add Details")) {
                    addDetails(port,request, response, context);
                } else if (s.equalsIgnoreCase("Save")) {
                    save(port,request, response, context);
                } else if (s.equalsIgnoreCase("SaveButton")) {
                    saveDetails(port,request, response, context);
                } else if (s.equalsIgnoreCase("EditButton")) {
                    editDetails(port,request, response, context);
                } else if (s.equalsIgnoreCase("RemoveButton")) {
                    removeDetails(port,request, response, context);
                }
            }

//            request.setAttribute("MemAppSearchResults", col);
//            context.getRequestDispatcher("/MemberApplication/MemberAppSearchResults.jsp").forward(request, response);
        } catch (java.lang.Exception ex) {
            Logger.getLogger(MemberApplicationGeneralQuestionsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void addDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        request.setAttribute("memberAppGenId", null);
        List<Map> list = getQuestionList(request);
        int appNum = getStrToInt(request.getParameter("memberAppNumber"));
        request.setAttribute("GeneralQuestions", list);
        request.setAttribute("DependentList", MemberAppCommon.getDependantListMap(service, appNum, request.getParameter("memberAppType")));
        context.getRequestDispatcher("/MemberApplication/MemberAppGeneralDetails.jsp").forward(request, response);
    }
    
    private void editDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        List<Map> list = getQuestionList(request);
        int appNum = getStrToInt(request.getParameter("memberAppNumber"));
        int genId = getStrToInt(request.getParameter("memberAppGenId"));
        request.setAttribute("GeneralQuestions", list);
        List<MemAppGenDetail> memAppGenDetailByAppNum = port.fetchMemAppGenDetailByAppNum(appNum);
        MemAppGenDetail genDetail = null;
        for (MemAppGenDetail gd : memAppGenDetailByAppNum) {
            if (gd.getGeneralDetailId() == genId) {
                genDetail = gd;
            }
        }
        request.setAttribute("DependentList", MemberAppCommon.getDependantListMap(service, appNum, request.getParameter("memberAppType")));
        MemberAppGeneralDetailsMapping.setMemAppGenDetail(request, genDetail);
        context.getRequestDispatcher("/MemberApplication/MemberAppGeneralDetails.jsp").forward(request, response);
    }
    
    private void removeDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        int appNum = getStrToInt(request.getParameter("memberAppNumber"));
        int genId = getStrToInt(request.getParameter("memberAppGenId"));
        port.removeMemberApplicationGenDetail(genId);
        context.getRequestDispatcher(new MemberAppTabContentsCommand().getGeneralQuestions(port,request)).forward(request, response);
    }
    
    private List<Map> getQuestionList(HttpServletRequest request) {
        List<Map> list = new ArrayList<Map>();
        Map map = request.getParameterMap();
        String check = "Member_General_";
        for (Object s : map.keySet()) {
            String key = s.toString();
            String value = request.getParameter(key);
            if (key != null && value!= null && value.equals("1") && key.length() > check.length()) {
                if (key.substring(0, check.length()).equalsIgnoreCase(check)) {
                    String data = key.substring(check.length());
                    String[] sarr = data.split("_");
                    if (sarr != null && sarr.length > 1) {
                        Map<String,String> depMap = new HashMap<String,String>();
                        depMap.put("id",sarr[0]);
                        depMap.put("value",sarr[1]);
                        list.add(depMap);
                        
                    }
                }
            }
        }
        return list;
    }
    
    private void save(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter out = null;
        try {
            List<AppAnswers> list = getAnswers(request);
            port.saveAppAnswers(list);
            out = response.getWriter();
            out.println(TabUtils.buildJsonResult("OK", null, null, "\"message\":\"Details have been saved.\""));
        } catch (IOException ex) {
            Logger.getLogger(MemberApplicationGeneralQuestionsCommand.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    private void saveDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = MemberAppGeneralDetailsMapping.validate(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (saveDetails(port, request)) {
                
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving.\"";
            }
        } else {
            additionalData = null;
        }
        if ("OK".equalsIgnoreCase(status)) {
            context.getRequestDispatcher(new MemberAppTabContentsCommand().getGeneralQuestions(port,request)).forward(request, response);
        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }
         
        
    }
    
    private boolean saveDetails(NeoManagerBean port, HttpServletRequest request) {
        NeoUser neoUser = (NeoUser)request.getSession().getAttribute("persist_user");
        try {
            MemAppGenDetail app = MemberAppGeneralDetailsMapping.getMemAppGenDetail(request, neoUser);
            port.saveMemAppGenDetail(app);
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
    }
    
    
    private List getAnswers(HttpServletRequest request) {
        List<AppAnswers> list = new ArrayList<AppAnswers>();
        int appNum = getStrToInt(request.getParameter("memberAppNumber"));
        Map map = request.getParameterMap();
        String check = "Member_General_";
        NeoUser user = (NeoUser)request.getSession().getAttribute("persist_user");
        for (Object s : map.keySet()) {
            String key = s.toString();
            String value = request.getParameter(key);
            if (key != null && value!= null) {
                if (key.length() > check.length() && key.substring(0, check.length()).equalsIgnoreCase(check)) {
                    String data = key.substring(check.length());
                    String[] sarr = data.split("_");
                    if (sarr != null && sarr.length > 1) {
                        int question = getStrToInt(sarr[0]);
                        if (question > 0) {
                            AppAnswers app = new AppAnswers();
                            app.setQuestionNumber(question);
                            app.setAnswerValue(value);
                            app.setApplicationNumber(appNum);
                            app.setCreatedBy(user.getUserId());
                            app.setLastUpdatedBy(user.getUserId());
                            app.setSecurityGroupId(user.getSecurityGroupId());
                            list.add(app);
                        }
                    }
                }
            }
        }
        return list;
    }
    
    private int getStrToInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (java.lang.Exception e) {
            System.out.println(s + " = " + e.getMessage());
        }
        return 0;
    }
    
    @Override
    public String getName() {
        return "MemberApplicationGeneralQuestionsCommand";
    }

   
}

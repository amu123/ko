/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ClaimLine;
import neo.manager.ClaimLineRejection;
import neo.manager.ClaimReversals;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author janf
 */
public class ReverseAndRejectClaim extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("Reverse And Reject Claim Called");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        PrintWriter out = null;
        
        Date today = new Date(System.currentTimeMillis());

        String rejectionCode = request.getParameter("rejectionReason");
        String rejectionCodeText = port.getValueFromCodeTableForId("Claim Rejection Codes", rejectionCode);
        String reversalCode = request.getParameter("reversalReason");
        String reversalCodeText = port.getValueFromCodeTableForId("Claim Reversal Codes", reversalCode);
        
        ClaimLine claimLine = (ClaimLine) session.getAttribute("memCLCLDetails");
        int claimId = claimLine.getClaimId();
        
        System.out.println("rejectionCode = " + rejectionCode);
        System.out.println("rejectionCodeText = " + rejectionCodeText);
        System.out.println("reversalCode = " + reversalCode);
        System.out.println("reversalCodeText = " + reversalCodeText);
        System.out.println("claimId = " + claimId);
        
        Security _secure = new Security();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        _secure.setCreatedBy(user.getUserId());
        _secure.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        _secure.setLastUpdatedBy(user.getUserId());
        _secure.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        _secure.setSecurityGroupId(user.getSecurityGroupId());
        
        //setQuantity to 1
        System.out.println("claimLine quantity = " + claimLine.getQuantity());
        if(claimLine.getQuantity() == 0){
            claimLine.setQuantity(1);
        }
        
        System.out.println("claimLine quantity = " + claimLine.getQuantity());
        
        ClaimReversals cr = new ClaimReversals();
        cr.setReversalCode(reversalCode);
        cr.setReversalDescription(reversalCodeText);
        
        ClaimLineRejection clr = new ClaimLineRejection();
        clr.setClaimLineId(claimLine.getClaimLineId());
        clr.setRejectionId(Integer.parseInt(rejectionCode));
        clr.setDescription(rejectionCodeText);
        
        try {
            String returnMessage = "";
            out = response.getWriter();
            String success ="";
            success = port.reverseAndRejectClaimLine(claimLine, cr, clr, user);
            
            if(success.equalsIgnoreCase("Sucsessful")) {
                returnMessage = "Done|Claim Line " + claimLine.getClaimLineId() + " successfully reversed and rejected";
            } else if(success.equalsIgnoreCase("Unsucsessful")) {
                returnMessage = "Error|Failed to reverse and reject Claim Line " + claimLine.getClaimLineId();
            } else if (success.equalsIgnoreCase("Reversed")) {
                returnMessage = "Error|Claim Line " + claimLine.getClaimLineId() + " was only reversed";
            }
            
//            String reversed =  port.reverseClaimLine(claimLine, cr, _secure);
//            System.out.println("reversed = " + reversed);
//            if (reversed.equals("Sucsessful")) {
//                returnMessage = "Done|ClaimLine " + claimLine.getClaimLineId() + " reversed";
//                success = port.rejectClaimLine(user, clr);
//                if(success){
//                    returnMessage += " and rejected";
//                } else {
//                    returnMessage += " but not rejected";
//                }
//            } else if (reversed.equals("Duplicate")) {
//                returnMessage = "Error|ClaimLine " + claimLine.getClaimLineId() + " already reversed";
//            } else if (reversed.equals("Reversal")) {
//                returnMessage = "Error|ClaimLine " + claimLine.getClaimLineId() + " is a reversal line!";
//            }
            
            
            
            out.println(returnMessage);
        } catch (Exception e){
            e.printStackTrace();
            String url = "http://stackoverflow.com/search?q=[java]+" + e.getMessage();
            out.println(url);
        }
        
        
        
        return null;
    }

    @Override
    public String getName() {
        return "ReverseAndRejectClaim";
    }
    
}

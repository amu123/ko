/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.brokerFirm.BrokerMapping;
import com.koh.brokerFirm.ConsultantMapping;
import com.koh.broker_firm.application.commands.BrokerAppTabContentsCommand;
import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.*;


/**
 *
 * @author princes
 */
public class ConsultantAppTabContentsCommand extends NeoCommand{
    
    private static final String JSP_FOLDER = "/Broker/";

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String tab = request.getParameter("tab");
        int entityId = 0;
        if (request.getParameter("consultantEntityId") != null && !request.getParameter("consultantEntityId").isEmpty()) {
            entityId = new Integer(request.getParameter("consultantEntityId"));
            request.setAttribute("consultantEntityId", entityId);
        }
        String consultantRegion = request.getParameter("consultantRegion");
        String page = "";
        if ("ConsultantAppDetails".equalsIgnoreCase(tab)) {
            page = getConsultantDetails(request);
        } else if ("Accreditation".equalsIgnoreCase(tab)) {
            page = getConsultantAccreditation(request, entityId);
        }  else if ("ContactDetails".equalsIgnoreCase(tab)) {
            page = getConsultantContactDetails(request, entityId);
        } else if ("ConsultantHistoty".equalsIgnoreCase(tab)) {
            page = getConsultantHistory(request, entityId);
        }else if ("AuditTrail".equalsIgnoreCase(tab)) {
            page = getConsultantAuditTrail(request);
        }else if ("Notes".equalsIgnoreCase(tab)) {
            page = getBrokerConsultantNotes(request);
        }else if ("Documents".equalsIgnoreCase(tab)) {
            page = getDocuments(request,entityId);
        }else if ("CoverDetails".equalsIgnoreCase(tab)) {
            page = getCoverDetails(request, entityId);
        }

            RequestDispatcher dispatcher = context.getRequestDispatcher(page);
        try {
            dispatcher.forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(BrokerAppTabContentsCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BrokerAppTabContentsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "ConsultantAppTabContentsCommand";
    }

    private String getConsultantDetails(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String consultantAppCode = request.getParameter("consultantAppCode");
        String consultantRegion = request.getParameter("consultantRegion");
        System.out.println("appConsultantCode " + consultantAppCode + " consultantRegion " + consultantRegion);
        if (consultantAppCode != null && !consultantAppCode.isEmpty()) {
            BrokerConsultant bf = port.getBrokerConsultantByCode(consultantAppCode);
            ConsultantMapping.setConsultantApplication(request, bf);
            /*System.out.println("tricon : " + ma.getContribAccountType() + "\t payments :" + ma.getContribAccountType());
            System.out.println("tricon : " + request.getAttribute("MemberApp_contribAccountType") + "\t payments :" + request.getAttribute("MemberApp_contribAccountType"));*/
            request.setAttribute("consultantEntityId", bf.getEntityId());
            request.setAttribute("consultantRegion", bf.getRegion());
            //request.setAttribute("brokerTitle", bf.getContactTitle());
            //request.setAttribute("brokerContactPerson", bf.getFirstNames());
            //request.setAttribute("memberAppNumber", ma.getApplicationNumber());
        } 
        return JSP_FOLDER + "ConsultantDetails.jsp";
    }

    public String getConsultantAccreditation(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        if (entityId != 0) {
            List<BrokerAccred> ba = port.fetchBrokerAccredByEntityId(entityId);
            request.setAttribute("accreList", ba);
        }
        return JSP_FOLDER + "ConsultantAccreditation.jsp";
    }
    
    public String getConsultantHistory(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        if (entityId != 0) {
            List<BrokerBrokerConsult> con = port.fetchBrokerToConsultantByEntityId(entityId);
            //System.out.println("Size of brokers " + con.size());
            request.setAttribute("accreList", con);
        }
        return JSP_FOLDER + "ConsultantHistory.jsp";
    }

    private String getConsultantAuditTrail(HttpServletRequest request) {

        return JSP_FOLDER + "ConsultantAuditTrail.jsp";
    }
    
    private String getBrokerConsultantNotes(HttpServletRequest request) {

        return JSP_FOLDER + "BrokerConsultantNotes.jsp";
    }
    
    private String getDocuments(HttpServletRequest request, int entityId) {
        request.setAttribute("entityId", entityId);
        request.getSession().setAttribute("memberCoverEntityId", entityId);
        return JSP_FOLDER + "ConsultantDocuments.jsp";
    }
    
    private String getCoverDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        if (entityId != 0) {
            List<MemberBrokerConsultant> con = port.findMemberBrokerConsultantByBrokerEntityId(entityId);
            //System.out.println("Size of members " + con.size());
            request.setAttribute("memList", con);
        }
        return JSP_FOLDER + "ConsultantCoverDetails.jsp";
    }

    private String getConsultantContactDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        request.setAttribute("broker_map", BrokerMapping.getMapping());
        String brokerTitle = request.getParameter("brokerTitle");
        String brokerContactPerson = request.getParameter("brokerContactPerson");
        String brokerContactSurname = request.getParameter("brokerContactSurname");
        String consultantRegion = request.getParameter("consultantRegion");
        request.setAttribute("consultantRegion", consultantRegion);
        request.setAttribute("brokerTitle", brokerTitle);
        request.setAttribute("brokerContactPerson", brokerContactPerson);
        request.setAttribute("brokerContactSurname", brokerContactSurname);
        
        //System.out.println("Contact consultantEntityId " + consultantEntityId);
        
        if (entityId != 0) {
            Broker eg = port.fetchBrokerByEntityId(entityId);
            Collection<ContactDetails> cd = port.getContactDetailsByEntityId(entityId);
            Collection<AddressDetails> ad = port.getAddressDetailsByEntityId(entityId);
            ConsultantMapping.setBroker(request, eg);
            ConsultantMapping.setAddressDetails(request, ad);
            ConsultantMapping.setContactDetails(request, cd);
            //request.setAttribute("employerEntityId", employerId);
            //request.setAttribute("EmployerName", eg.getEmployerName());
            //request.setAttribute("EmployerNumber", eg.getEmployerNumber());
        }
        return JSP_FOLDER + "ConsultantContactDetails.jsp";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.command;

import com.koh.command.NeoCommand;
import com.koh.utils.XmlUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverDetails;
import neo.manager.CoverProductDetails;
import neo.manager.CoverSearchCriteria;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author josephm
 */
public class GetCoverDetailsByNumberPortal extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("Inside GetCoverDetailsByNumberPortal");
        PrintWriter out = null;
        HttpSession session = request.getSession();
        String xmlStr = "";
        String memberNo = request.getParameter("number");
        String exactFlag = request.getParameter("exactCoverNum");
       //System.out.println(">>>> Exact Cover Number : " + exactFlag);
       // String another = request.getParameter("depListValues");
        int depNumber = 0;
        String depNo = (String) session.getAttribute("depListValues_text");
        System.out.println("The dependent number is " + session.getAttribute("depListValues_text"));
       // System.out.println("Another depended number is  " + another);
        if(depNo != null && !depNo.equalsIgnoreCase("")) {

            depNumber = Integer.parseInt(depNo);
            System.out.println("The dependent number is " + depNumber);
        }
        System.out.println("memberNo = " + memberNo);
        CoverSearchCriteria csc = new CoverSearchCriteria();
        if (memberNo != null && !memberNo.trim().equalsIgnoreCase("")) {
            if(exactFlag != null && exactFlag.equals("1")){
                csc.setCoverNumberExact(memberNo);
            }else {
                csc.setCoverNumber(memberNo);
            }
                
            List<CoverDetails> cdList = service.getNeoManagerBeanPort().getResignedMemberByCoverNumber(memberNo);
            CoverProductDetails prodDetails = service.getNeoManagerBeanPort().getCoverProductDetailsByCoverNumber(memberNo);

            response.setHeader("Cache-Control", "no-cache");
            response.setContentType("text/xml");

            //get new document
            @SuppressWarnings("static-access")
            Document doc = new XmlUtils().getDocument();
            //create xml elements
            Element root = doc.createElement("EAuthCoverDetails");
            //add root to document
            doc.appendChild(root);
            try {
                out = response.getWriter();
                if (cdList.size() != 0) {
                    //response.setStatus(200);
                    session.setAttribute("eAuthCoverList", cdList);

                    //set product & option names
                    Element prodDetail = doc.createElement("ProductDetails");
                    Element prodName = doc.createElement("ProductName");
                    Element optName = doc.createElement("OptionName");

                    //set product details content
                    prodName.setTextContent(prodDetails.getProductName());
                    optName.setTextContent(prodDetails.getOptionName());
                    //set product detail ids to session
                    session.setAttribute("scheme", prodDetails.getProductId());
                    session.setAttribute("schemeOption", prodDetails.getOptionId());
                    session.setAttribute("schemeName", prodDetails.getProductName());
                    session.setAttribute("schemeOptionName", prodDetails.getOptionName());
                    //add elements to product details
                    prodDetail.appendChild(prodName);
                    prodDetail.appendChild(optName);
                    //add prodDetails element to root
                    root.appendChild(prodDetail);

                    Element member = doc.createElement("MemberDetails");
                    for (CoverDetails cd : cdList) {
                        //create member detail string
                        if(cd.getDependentNumber() == depNumber || cd.getDependentNumber() == 0) {

                        String dob = new SimpleDateFormat("yyyy/MM/dd").format(cd.getDateOfBirth().toGregorianCalendar().getTime());
                        String depType = cd.getDependentType().substring(0, 1);
                        //String ecDepInfo = cd.getDependentNumber() + "|" + cd.getName() + "-" + cd.getSurname() + "-" + cd.getDependentType() + "-" + dob;
                        String ecDepInfo = cd.getDependentNumber() + "|" + depType + "-" + cd.getName() + "-" + dob;
                        ecDepInfo = ecDepInfo.replace(" Member", "");
                        //create info element
                        Element depInfo = doc.createElement("DependantInfo");
                        depInfo.setTextContent(ecDepInfo);
                        //add element to root
                        member.appendChild(depInfo);
                        }
                       
                    }
                    root.appendChild(member);

                } else {
                    root.setTextContent("ERROR|Member not found");
                }
                //return xml object
                try {
                    xmlStr = XmlUtils.getXMLString(doc);
                } catch (IOException ex) {
                    ex.printStackTrace();
                    xmlStr = "";

                }
                out.println(xmlStr);
            } catch (IOException io) {
                io.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetCoverDetailsByNumberPortal";
    }
}
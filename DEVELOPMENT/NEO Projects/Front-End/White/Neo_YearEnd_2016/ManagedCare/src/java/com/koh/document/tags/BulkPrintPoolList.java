/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.tags;

import agility.za.documentprinting.*;
import com.agile.security.webservice.LookupValue;
import com.agile.tags.TagMethods;
import com.koh.command.NeoCommand;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author princes
 */
public class BulkPrintPoolList extends TagSupport {

    private String command;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            DocumentPrintingRequest request = new DocumentPrintingRequest();

            request.setPrintProcessType("ViewPrintJobs");
            String displayOption = (String) req.getAttribute("displayOption");
            String displayDay = (String) req.getAttribute("displayDay");
            String displayGroup = (String) req.getAttribute("displayGroup");
            String displayType = (String) req.getAttribute("displayType");

            PrintFilter filter = new PrintFilter();

            if (displayOption == null || displayOption.equals("") || displayOption.equalsIgnoreCase("ACTIVE")) {
                displayOption = "active";
                filter.setStatus("1");
                request.setFilter(filter);
            } else if (displayOption.equalsIgnoreCase("FAILED")) {
                filter.setStatus("3");
                request.setFilter(filter);
            } else if (displayOption.equalsIgnoreCase("SUCCESS")) {
                filter.setStatus("2");
                request.setFilter(filter);
            }

            if (displayDay == null || displayDay.equals("")) {
                displayDay = "today";
                filter.setFromDate(addDay(0));
                request.setFilter(filter);
            } else if (displayDay.equals("all")) {
                //Do nothing
            } else if (displayDay.equals("today")) {
                filter.setFromDate(addDay(0));
                request.setFilter(filter);
            } else if (displayDay.equals("yesterday")) {
                filter.setFromDate(addDay(-1));
                request.setFilter(filter);
            } else if (displayDay.equals("7days")) {
                filter.setFromDate(addDay(-7));
                request.setFilter(filter);
            } else if (displayDay.equals("30days")) {
                filter.setFromDate(addDay(-30));
                request.setFilter(filter);
            }

            if (displayGroup != null && displayGroup.equals("groupType")) {
                filter.setGroup(true);
                request.setFilter(filter);
            }
            if (displayType != null && !displayType.equals("")) {
                filter.setType(displayType);
                request.setFilter(filter);
            }

            DocumentPrintingResponse response = NeoCommand.service.getNeoManagerBeanPort().processBulkPrintRequest(request);

            List<PrintBatchPool> batchList = response.getBatchPool();
            out.println("<input type=\"hidden\" name=\"selectedDisplayOption\" id=\"selectedDisplayOption\" value=\"" + displayOption + "\" />");
            out.println("<input type=\"hidden\" name=\"selectedDisplayDay\" id=\"selectedDisplayDay\" value=\"" + displayDay + "\" />");
            //out.println("<input type=\"hidden\" name=\"selectedDisplayGroup\" id=\"selectedFile\" value=\""+displayGroup+"\" />");
            //out.println("<input type=\"hidden\" name=\"selectedDisplayType\" id=\"selectedFile\" value=\""+displayType+"\" />");

            out.println("<input type=\"hidden\" name=\"selectedFile\" id=\"selectedFile\" value=\"\" />");
            out.println("<input type=\"hidden\" name=\"selectedId\" id=\"selectedId\" value=\"\" />");
//            out.println("<label class=\"header\">Search Results</label><br/><br/>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");

            displayOption(out, displayOption);
            displayDay(out, displayDay);
            //(out, displayGroup);
            //displayType(out, displayType, response.getAvailableType());
            out.println("<td><button type=\"submit\" name=\"filterD\" onClick=\"document.getElementById('opperation').value ='BulkPrintPoolFilterCommand';\">Filter</button></td></tr>");
            out.println("</table><table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");

            out.println("<tr><th>Batch Id</th><th>Batch Type</th><th>Status</th><th>Batch Count</th><th>Date Received</th><th>Process Date</th><th></th><th></th></tr>");
            String generatedDate = null;
            String status = null;
            String type = null;
            String printDate = "";
            if (batchList != null && batchList.size() > 0) {
//                out.println("<tr>");
                for (PrintBatchPool batch : batchList) {
                    if (batch.getGeneratedDate() != null) {
                        String dateTo = new SimpleDateFormat("yyyy/MM/dd  hh:mm:ss").format(batch.getGeneratedDate().toGregorianCalendar().getTime());
                        if (!dateTo.equalsIgnoreCase("2999/12/31")) {
                            generatedDate = dateTo;
                        }
                    }

                    if (batch.getPrintDate() != null) {
                        String pDate = new SimpleDateFormat("yyyy/MM/dd  hh:mm:ss").format(batch.getPrintDate().toGregorianCalendar().getTime());
                        printDate = pDate;
                    } else {
                        printDate = "";
                    }

                    if (batch.getBatchType() == 1) {
                        type = "Bulk Card";
                    } else {
                        type = "Unknown";
                    }

                    List<neo.manager.LookupValue> lookUps = NeoCommand.service.getNeoManagerBeanPort().getCodeTable(new Integer(250));
                    for (int i = 0; i < lookUps.size(); i++) {
                        neo.manager.LookupValue lookupValue = lookUps.get(i);
                        int id = new Integer(lookupValue.getId());
                        if (batch.getBatchStatus() == id) {
                            status = lookupValue.getValue();
                        }
                    }
                    out.println("<tr><td><label class=\"label\">" + batch.getBatchID() + "</label></td>"
                            + "<td><label class=\"label\">" + type + "</label></td>"
                            + "<td><label class=\"label\">" + status + "</label></td>"
                            + "<td><label class=\"label\">" + batch.getBatchCount() + "</label></td>"
                            + "<td><label class=\"label\">" + generatedDate + "</label></td>"
                            + "<td><label class=\"label\">" + printDate + "</label></td>");
                    if (displayOption.equals("active")) {
                        if (batch.getBatchStatus() == 3) {
                            out.println("<td><button type=\"submit\" name=\"com" + batch.getBatchID() + "\"  onClick=\"setParams('BulkPrintPoolCommand','" + batch.getBatchID() + "','" + batch.getBatchID() + "')\">Re-Print</button></td>");
                        } else if (batch.getBatchStatus() == 5) {
                            out.println("<td></td>");
                        } else {
                            out.println("<td><button type=\"submit\" name=\"com" + batch.getBatchID() + "\"  onClick=\"setParams('BulkPrintPoolCommand','" + batch.getBatchID() + "','" + batch.getBatchID() + "')\">Print</button></td>");
                        }
                        out.println("<td><button type=\"submit\" name=\"del" + batch.getBatchID() + "\"  onClick=\"setParams('BulkPrintPoolDeleteCommand','" + batch.getBatchID() + "','" + batch.getBatchID() + "')\">Delete</button></td></tr>");
                    } else {
                        out.println("</td></tr>");
                    }
                }
                out.println("</table>");
            }




            System.out.println("--End tag--");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in BulkPrintPoolList tag", ex);
        }
        return super.doEndTag();
    }

    private String convertDate(XMLGregorianCalendar cal) {
        if (cal == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm");
//	        xc.setTimezone(getTimeZoneOffset());
        return sdf.format(cal.toGregorianCalendar().getTimeInMillis());

    }

    public void setCommand(String command) {
        this.command = command;
    }

    public XMLGregorianCalendar addDay(int add) {
        try {
            Calendar fromDate = Calendar.getInstance();
            fromDate.add(Calendar.DAY_OF_MONTH, add);
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(fromDate.getTime());
            DatatypeFactory fact = DatatypeFactory.newInstance();
            XMLGregorianCalendar cal = fact.newXMLGregorianCalendar(calendar);
            return cal;
        } catch (Exception e) {
            System.err.println(e);
        }
        return null;
    }

    private void displayOption(JspWriter out, String displayOption) throws IOException {
        out.println("<tr><td><select style=\"width:215px\" size=\"1\" name=\"displayOption\" value=\"" + displayOption + "\" >");

        if (displayOption.equalsIgnoreCase("active")) {
            out.println("<option selected=\"selected\" value=\"active\" >Not Printed</option>");
            out.println("<option value=\"success\" >Print Success</option>");
            out.println("<option value=\"failed\" >Print Failed</option>");
            out.println("<option value=\"all\" >All</option>");
        } else if (displayOption.equalsIgnoreCase("FAILED")) {
            out.println("<option value=\"active\" >Not Printed</option>");
            out.println("<option value=\"success\" >Print Success</option>");
            out.println("<option selected=\"selected\" value=\"failed\" >Print Failed</option>");
            out.println("<option value=\"all\" >All</option>");
        } else if (displayOption.equalsIgnoreCase("SUCCESS")) {
            out.println("<option value=\"active\" >Not Printed</option>");
            out.println("<option selected=\"selected\" value=\"success\" >Print Success</option>");
            out.println("<option value=\"failed\" >Print Failed</option>");
            out.println("<option value=\"all\" >All</option>");
        } else {
            out.println("<option value=\"active\" >Not Printed</option>");
            out.println("<option value=\"success\" >Print Success</option>");
            out.println("<option value=\"failed\" >Print Failed</option>");
            out.println("<option selected=\"selected\" value=\"all\" >All</option>");
        }
        out.println("</select> </td>");
    }

    private void displayDay(JspWriter out, String displayDay) throws IOException {
        out.println("<td><select style=\"width:215px\" size=\"1\" name=\"displayDay\" value=\"" + displayDay + "\" >");
        System.out.println("displayDay:::: " + displayDay);
        if (displayDay == null || displayDay.equals("")) { //Currently it is today
            out.println("<option selected=\"selected\" value=\"today\" >Today</option>");
            out.println("<option value=\"yesterday\" >Yesterday</option>");
            out.println("<option value=\"7days\" >7 Days</option>");
            out.println("<option value=\"30days\" >30 Days</option>");
            out.println("<option value=\"all\" >All</option>");
            displayDay = "today";
        } else if (displayDay.equals("today")) {
            out.println("<option selected=\"selected\" value=\"today\" >Today</option>");
            out.println("<option value=\"yesterday\" >Yesterday</option>");
            out.println("<option value=\"7days\" >7 Days</option>");
            out.println("<option value=\"30days\" >30 Days</option>");
            out.println("<option value=\"all\" >All</option>");
        } else if (displayDay.equals("yesterday")) {
            out.println("<option value=\"today\" >Today</option>");
            out.println("<option selected=\"selected\" value=\"yesterday\" >Yesterday</option>");
            out.println("<option value=\"7days\" >7 Days</option>");
            out.println("<option value=\"30days\" >30 Days</option>");
            out.println("<option value=\"all\" >All</option>");
        } else if (displayDay.equals("7days")) {
            out.println("<option value=\"today\" >Today</option>");
            out.println("<option value=\"yesterday\" >Yesterday</option>");
            out.println("<option selected=\"selected\" value=\"7days\" >7 Days</option>");
            out.println("<option value=\"30days\" >30 Days</option>");
            out.println("<option value=\"all\" >All</option>");
        } else if (displayDay.equals("30days")) {
            out.println("<option value=\"today\" >Today</option>");
            out.println("<option value=\"yesterday\" >Yesterday</option>");
            out.println("<option value=\"7days\" >7 Days</option>");
            out.println("<option selected=\"selected\" value=\"30days\" >30 Days</option>");
            out.println("<option value=\"all\" >All</option>");
        } else if (displayDay.equals("all")) {
            out.println("<option value=\"today\" >Today</option>");
            out.println("<option value=\"yesterday\" >Yesterday</option>");
            out.println("<option value=\"7days\" >7 Days</option>");
            out.println("<option value=\"30days\" >30 Days</option>");
            out.println("<option selected=\"selected\" value=\"all\" >All</option>");
        }
        out.println("</select> </td>");
    }

    private void displayGroup(JspWriter out, String displayGroup) throws IOException {
        out.println("<td><select style=\"width:215px\" size=\"1\" name=\"displayGroup\" value=\"" + displayGroup + "\" >");
        if (displayGroup != null && displayGroup.equals("groupType")) {
            out.println("<option value=\"lastFirst\" >Last First</option>");
            out.println("<option selected=\"selected\" value=\"groupType\" >Group Type</option>");
        } else {
            out.println("<option selected=\"selected\" value=\"lastFirst\" >Last First</option>");
            out.println("<option value=\"groupType\" >Group Type</option>");
        }
        out.println("</select> </td>");
    }

    private void displayType(JspWriter out, String displayType, List<String> list) throws IOException {
        out.println("<td><select style=\"width:215px\" size=\"1\" name=\"displayType\" value=\"" + displayType + "\" >");
        out.println("<option value=\"\" ></option>");
        for (String val : list) {
            if (displayType != null && displayType.equals(val)) {
                out.println("<option selected=\"selected\" value=\"" + val + "\" >" + val + "</option>");
            } else {
                out.println("<option value=\"" + val + "\" >" + val + "</option>");
            }
        }
        out.println("</select> </td>");
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.LookupValue;
import neo.manager.NeoUser;

/**
 *
 * @author josephm
 */
public class PutSourceCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        HashMap<String, String> callMap = new HashMap<String, String>();
        Collection<LookupValue> lookupList = service.getNeoManagerBeanPort().getCodeTable(104);//TagMethods.getLookupValue(12);

        for (LookupValue v : lookupList) {

            callMap.put(v.getId(), v.getValue());
            System.out.println("The id is " + v.getId());
            System.out.println("The value is " + v.getValue());
        }


        try {
            PrintWriter out = response.getWriter();
            if (user.getUserId() == 12) {
                out.print("|Source=" + callMap.get("1") + "$");
            } else {

                if (user.getUserId() == 13) {

                    out.print("|Source=" + callMap.get("2") + "$");
                }
            }
            System.out.println("The value from the hashmap is "  +callMap.get("1"));
            System.out.println("The sendon value is " + callMap.get("2"));
            System.out.println("Checking if the hashmap is null " + callMap.isEmpty());

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "PutSourceCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import agility.za.documentprinting.DocumentPrintingRequest;
import agility.za.documentprinting.DocumentPrintingResponse;
import agility.za.documentprinting.PrintDocType;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class PrintPoolCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------In PrintPoolCommand---------------------");

            String selectedFile = request.getParameter("selectedFile");
            String selectedId = request.getParameter("selectedId");
            String displayOption = request.getParameter("displayOption");
            logger.info("selectedFile = " + selectedFile);
            logger.info("selectedId = " + selectedId);
            if (selectedFile != null && !selectedFile.trim().equals("")) {
                DocumentPrintingRequest req = new DocumentPrintingRequest();
                NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
                req.setAgent(loggedInUser.getUsername());
                req.setPrintProcessType("PrintDoc");
                
                req.setSrcPrintDrive(new PropertiesReader().getProperty("DocumentIndexIndexDrive"));
                PrintDocType docType = new PrintDocType(); 
//              docType.setFolder(new PropertiesReader().getProperty("PrintPoolFolder"));//removed after changes
                docType.setFilename(selectedFile);
                docType.setPrintId(new Long(selectedId));
                logger.info("Print");
                if(selectedFile.contains("CFM")){
                    docType.setPrinterName(new PropertiesReader().getProperty("CardPrinter"));
                } else {
                    docType.setPrinterName(new PropertiesReader().getProperty("BulkPrinter"));
                }
                req.setPrintDoc(docType);
                DocumentPrintingResponse resp = NeoCommand.service.getNeoManagerBeanPort().processPrintRequest(req);
                if(resp.isIsSuccess()){
                   logger.info("Print successful");
                } else {
                    logger.error("Error printing .... " + resp.getMessage());
                    errorMessage(request, response, resp.getMessage());
                    return "PrintPoolCommand";
                }
            }
            request.setAttribute("displayOption", displayOption);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/PrintPool.jsp");
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "PrintPoolCommand";
    }
    
    private void errorMessage(HttpServletRequest request, HttpServletResponse response, String msg) throws Exception {
        request.setAttribute("errorMsg", msg);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/PrintPool.jsp");
        dispatcher.forward(request, response);
    }   
    
    @Override
    public String getName() {
        return "PrintPoolCommand";
    }
}
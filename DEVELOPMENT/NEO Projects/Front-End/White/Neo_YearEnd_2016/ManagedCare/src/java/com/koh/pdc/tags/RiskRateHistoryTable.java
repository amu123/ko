/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author princes
 */
public class RiskRateHistoryTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
//        HttpSession session = pageContext.getSession();
//        List<DiabetesBiometrics> getDiabetes = (List<DiabetesBiometrics>) session.getAttribute("getDiabetes");
//        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
//        String dateMeasured;
        try {

            out.print("<tr>");
            out.print("<th scope=\"col\">Risk Rating</th>");
            out.print("<th scope=\"col\">Risk Assesssed Date</th>");
            out.print("<th scope=\"col\">User</th>");
            out.print("<th scope=\"col\">Risk Reason</th>");
            out.print("</tr>");
            out.print("<tr>");
            out.print("<td></td>");
            out.print("<td></td>");
            out.print("<td></td>");
            out.print("<td></td>");
            out.print("</tr>");

            /* if (getDiabetes != null && getDiabetes.size() > 0) {
                for (DiabetesBiometrics d : getDiabetes) {

                    out.print("<tr>");
                    if (d.getDateMeasured() != null) {
                        Date mDate = d.getDateMeasured().toGregorianCalendar().getTime();
                        dateMeasured = formatter.format(mDate);
                        out.print("<td>" + dateMeasured + "</td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getIcd10Lookup() != null) {
                        out.print("<td>" + d.getIcd10Lookup().getValue() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }

                    if (d.getExercisePerWeek() != 0) {
                        out.print("<td>" + d.getExercisePerWeek() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }

                    if (d.getYearsSinceStopped() > 0) {
                        out.print("<td>" + d.getYearsSinceStopped() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }

                    if (d.getAlcoholUnitsPerWeek() != 0) {
                        out.print("<td>" + d.getAlcoholUnitsPerWeek() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }

                    if (d.getHbAIC() != 0) {
                        out.print("<td>" + d.getHbAIC() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }

                    if (d.getRandomGlucose() != 0) {
                        out.print("<td>" + d.getRandomGlucose() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }

                    if (d.getFastingGlucose() != 0) {
                        out.print("<td>" + d.getFastingGlucose() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }

                    if (d.getTotalCholesterol() != 0.0) {
                        out.print("<td>" + d.getTotalCholesterol() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }

                    if (d.getLdlcMol() != 0) {
                        out.print("<td>" + d.getLdlcMol() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }

                    if (d.getHdlcMol() != 0) {
                        out.print("<td>" + d.getHdlcMol() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }

                    if (d.getTryglyseride() != 0) {
                        out.print("<td>" + d.getTryglyseride() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }

                    if (d.getProteinurianGL() != 0) {
                        out.print("<td>" + d.getProteinurianGL() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }

                    if (d.getBmi() != 0) {
                        out.print("<td>" + d.getBmi() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }

                    if (d.getTchdlRatio() != 0.0) {
                        out.print("<td>" + d.getTchdlRatio() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("</tr>");
                }
            }*/
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }
}

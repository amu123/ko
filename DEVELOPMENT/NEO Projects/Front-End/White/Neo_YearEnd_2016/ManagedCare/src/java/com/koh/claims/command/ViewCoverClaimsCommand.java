/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import com.koh.utils.NeoCoverDetailsDependantFilter;
import java.lang.Exception;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ViewCoverClaimsCommand extends NeoCommand {

    private static Logger logger = Logger.getLogger(ViewCoverClaimsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        logger.info("Inside ViewCoverClaimsCommand");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        
        String coverNumber = "";
        String onScreen = request.getParameter("onScreen");
        if (onScreen != null && onScreen.equalsIgnoreCase("MemberCCLogin")) {
            coverNumber = request.getParameter("policyNumber_text");

        } else if (request.getParameter("policyNumber_text") != null) {
            coverNumber = request.getParameter("policyNumber_text");

        } else {
            coverNumber = "" + session.getAttribute("policyHolderNumber_text");

        }
        System.out.println("coverNumber = " + coverNumber);

        //get principle member history
        CoverDetails c = port.getEAuthResignedMembers(coverNumber);
        int entityId = c.getEntityId();
        session.setAttribute("coverEntityId", entityId);
        if(c.getCoverNumber() != null && !c.getCoverNumber().equalsIgnoreCase("") && !c.getCoverNumber().equalsIgnoreCase(coverNumber) && coverNumber.contains(c.getCoverNumber())){
            System.out.println("coverDiff = " + c.getCoverNumber() + " - " + coverNumber);
            coverNumber = c.getCoverNumber();
        }
        
//        List<CoverDetailsAdditionalInfo> altMemNumDetails = port.getCoverAltMemberNumbers(entityId);
//        request.setAttribute("alt_MemNum_Details", altMemNumDetails);

        /* FIND AND SET EMPLOYER DETAILS*/
        MemberEmployerBrokerInfo info = port.getMemberEmployerBrokerInfo(coverNumber);
        List<CoverDetails> cd = port.getCoverDetailsByCoverNumber(coverNumber);
        int CommonentityID = 0;
        int product = 0;
        int dependentId = 99;
        for (CoverDetails cd1 : cd) {
            if (cd1.getDependentTypeId() == 17) {
                CommonentityID = cd1.getEntityCommonId();
                product = cd1.getProductId();
                dependentId = 0;
            }
        }
        
        // For Sechaba
        String client = String.valueOf(context.getAttribute("Client"));
        if (client.equalsIgnoreCase("Sechaba")) {
            logger.info("** Setting up for Union Member Link **");
            logger.info("** Cover_Number : " + coverNumber);
            logger.info("** Dependent_ID : " + dependentId);
            UnionMember unionMember = port.findUnionMemberDetailsByID(coverNumber, dependentId);
            if (unionMember != null) {
                session.setAttribute("memberUnionLink", unionMember.getUnionName());
            }
        }
        
        List<CoverDetailsAdditionalInfo> covDetails = port.getDependentAdditionalDetails(CommonentityID);
        String paymentMethod = "";
        String paymentDate = null;
        String incomeAmount = "";
        String contributions = "";
        SimpleDateFormat dt = new SimpleDateFormat("yyyy/MM/dd");
        for (CoverDetailsAdditionalInfo cov : covDetails) {
            if (cov.getInfoTypeId() == 8) {
                paymentMethod = getPaymentMethod(cov.getInfoValue());

            }
            if (cov.getInfoTypeId() == 7) {
                String payment = getDebitDate(cov.getInfoValue());
                //paymentDate =  dt.format(cov.getStartDate().toGregorianCalendar().getTime());
                paymentDate = ("(" + payment + ")");

            }
            if (cov.getInfoTypeId() == 4) {
                incomeAmount = cov.getInfoValue();
                System.out.println("incomeAmount = " + incomeAmount);

            }
            if (cov.getInfoTypeId() == 6) {
                contributions = cov.getInfoValue();
                System.out.println("contributions = " + contributions);

            }
        }
        if(product == 1){
            session.setAttribute("productDescription", "Resolution Health");
        }
        else if(product == 2){
            session.setAttribute("productDescription", "Spectramed");
        }else if(product == 3){
            session.setAttribute("productDescription", "Sizwe");
        }
        
        
        session.setAttribute("productId", product);
        session.setAttribute("memberVIPBroker", info.getVIPBroker());
        session.setAttribute("memberEmployerName", info.getEmployerName());
        session.setAttribute("memberEmployerType", info.getGroupType());
        session.setAttribute("memberEmployerBillMethod", info.getBillingMethod());
        session.setAttribute("memberEmployerAdministrator", info.getAdministrator());
        session.setAttribute("memberPaymentMethod", paymentMethod);
        session.setAttribute("memberPaymentDate", paymentDate);
        session.setAttribute("memberEmployeeIncomeAmount", incomeAmount);
        session.setAttribute("memberEmployerContributionPortion", contributions);
        
        //Getting underwriting for warning message
        boolean underwritingWarning = port.checkCoverExclusionHistoryByCover(coverNumber);
        System.out.println("underwritingWarning: "+underwritingWarning);
        session.setAttribute("warningW", underwritingWarning);
        
        List<neo.manager.LookupValue> lookUpsClient = service.getNeoManagerBeanPort().findCodeTableForLookupType(341);
        if (lookUpsClient != null) {
            neo.manager.LookupValue val = lookUpsClient.get(0);
            if (val != null) {
                System.out.println("Client : " + val.getValue());
                session.setAttribute("Client", val.getValue());
            }
        }

//        if(info.getBrokerInitials() != null && info.getBrokerSurname() != null &&info.getBrokerCode() != null){
        if (info.getBrokerCode() != null) {
            session.setAttribute("memberBrokerCode", (info.getBrokerSurname() == null ? "" : info.getBrokerSurname()) + " " + (info.getBrokerCode() == null ? "" : info.getBrokerCode()) + "");
        } else if (info.getConsultantCode() != null) {
            session.setAttribute("memberBrokerCode", (info.getConsultantSurname() == null ? "" : info.getConsultantSurname()) + " " + (info.getConsultantCode() == null ? "" : info.getConsultantCode()) + "");
        }
//        }



        String nextJSP = findandSetCoverDetails(port, entityId, session, coverNumber);

        try {
            System.out.println("nextJsp = " + nextJSP);
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {

        return "ViewCoverClaimsCommand";
    }

    private static String getPaymentMethod(String value) {
        if (value.equals("1")) {
            return "Debit Order";
        } else if (value.equals("2")) {
            return "Electronic Transfer";
        } else {
            return "";
        }
    }

    private static String getDebitDate(String value) {
        if (value.equals("1")) {
            return "31th";
        } else if (value.equals("2")) {
            return "1st";
        } else if (value.equals("3")) {
            return "5th";
        } else if (value.equals("4")) {
            return "6th";
        } else {
            return "";
        }
    }
    
    private static String setMemberCategory(String value) {
        String memberCategory = "";
        List<neo.manager.LookupValue> lookUpsClient = service.getNeoManagerBeanPort().findCodeTableForLookupType(223);
        for (neo.manager.LookupValue v : lookUpsClient) {
            if (value.equalsIgnoreCase(v.getId())) {
                memberCategory = v.getValue();
            }
        }
        return memberCategory;
    }

    private String findandSetCoverDetails(NeoManagerBean port, int entityId, HttpSession session, String coverNumber) {

        String nextJSP = "/Claims/MemberDetails.jsp";


        List<EAuthCoverDetails> coverList = null;
        List<EAuthCoverDetails> pMemList = null;
        List<EAuthCoverDetails> allDepList = null;
        
        if (coverNumber != null && !coverNumber.trim().equals("null")
                && !coverNumber.trim().equals("")) {
            if (session.getAttribute("allDepHistoryDetails") != null) {
                coverList = (List<EAuthCoverDetails>) session.getAttribute("allDepHistoryDetails");
            } else {
                coverList = new ArrayList<EAuthCoverDetails>();
                CoverSearchCriteria csc = new CoverSearchCriteria();
                csc.setCoverNumberExact(coverNumber);
                coverList = port.eAuthCoverSearch(csc);
                
                for (EAuthCoverDetails e : coverList) {
                    if (e.getDependantType().equalsIgnoreCase("Principal Member")) {
                        session.setAttribute("memberCategory", setMemberCategory(Integer.toString(e.getMemberCategory())));
                    }
                }
            }

            
            pMemList = new NeoCoverDetailsDependantFilter().EAuthCoverDetailsPrincipleMemberFilter(coverList, coverNumber);
            allDepList = new NeoCoverDetailsDependantFilter().EAuthCoverDetailsDependantFilterByPMemEndDate(coverList, coverNumber);
            session.setAttribute("principleHistoryDetails", pMemList);
            session.setAttribute("allDepHistoryDetails", coverList);
        }

        if (pMemList != null && pMemList.isEmpty() == false) {

            //address details
            ArrayList<AddressDetails> detailsAddressList = null;
            if (session.getAttribute("pMemAddressDetails") != null) {
                detailsAddressList = (ArrayList<AddressDetails>) session.getAttribute("pMemAddressDetails");
            } else {
                detailsAddressList = new ArrayList<AddressDetails>();
                detailsAddressList = (ArrayList<AddressDetails>) port.getAllAddressDetails(entityId);
            }
            session.setAttribute("pMemAddressDetails", detailsAddressList);

            //contact details
            ArrayList<ContactDetails> contactDetailsList = null;
            if (session.getAttribute("pMemContactDetails") != null) {
                contactDetailsList = (ArrayList<ContactDetails>) session.getAttribute("pMemContactDetails");

            } else {
                contactDetailsList = new ArrayList<ContactDetails>();
                contactDetailsList = (ArrayList<ContactDetails>) port.getAllContactDetails(entityId);
            }
            session.setAttribute("pMemContactDetails", contactDetailsList);

            //contact preferences
            ArrayList<ContactPreference> conPrefDetailsList = null;
            if (session.getAttribute("memberContactPrefListDetail") != null) {
                conPrefDetailsList = (ArrayList<ContactPreference>) session.getAttribute("memberContactPrefListDetail");

            } else {
                conPrefDetailsList = new ArrayList<ContactPreference>();
                conPrefDetailsList = (ArrayList<ContactPreference>) port.getAllContactPreferences(entityId);
            }
            session.setAttribute("memberContactPrefListDetail", conPrefDetailsList);

            //bank details
            ArrayList<BankingDetails> bankDetails = null;
            if (session.getAttribute("pMemBankDetails") != null) {
                bankDetails = (ArrayList<BankingDetails>) session.getAttribute("pMemBankDetails");
            } else {
                bankDetails = new ArrayList<BankingDetails>();
                bankDetails = (ArrayList<BankingDetails>) port.getAllBankingDetails(entityId);
            }
            session.setAttribute("pMemBankDetails", bankDetails);

            //payment run details - Table population is perormed inside that JSP [Theres a dropdown filtering the search]
            session.setAttribute("ProductID_CoverNumber", coverNumber);
            session.setAttribute("ProductIDTypeSelected", "null"); //Populate the DropdownBox
//            ArrayList<EntityPaymentRunDetails> paymentRunDetailsList = null;
//            if (session.getAttribute("memberPaymentRunDetails") != null) {
//                paymentRunDetailsList = (ArrayList<EntityPaymentRunDetails>) session.getAttribute("memberPaymentRunDetails");
//            } else {
//                paymentRunDetailsList = new ArrayList<EntityPaymentRunDetails>();
//                PaymentRunSearchCriteria payRun = new PaymentRunSearchCriteria();
//                payRun.setEntityType("Insured Person");
//                payRun.setEntityNo(coverNumber);
//                paymentRunDetailsList = (ArrayList<EntityPaymentRunDetails>) port.getPaymentRunDetailForEntity(payRun, "1");
//                if(paymentRunDetailsList.isEmpty())
//                {
//                    paymentRunDetailsList = (ArrayList<EntityPaymentRunDetails>) port.getPaymentRunDetailForEntity(payRun, "2");
//                }
//            }
//            session.setAttribute("memberPaymentRunDetails", paymentRunDetailsList);

            
        Member mem = port.fetchMemberAddInfo(entityId);
            
        session.setAttribute("showHandedOverFlag", "false");
        
        GregorianCalendar today = new GregorianCalendar();
    
        if(mem != null && mem.getDebtHandedOverDate() != null && mem.getDebtHandedOver().equalsIgnoreCase("1")){
            if(mem.getDebtHandedOverDate().toGregorianCalendar().before(today) || mem.getDebtHandedOverDate().toGregorianCalendar().equals(today)){
                session.setAttribute("showHandedOverFlag", "true");
                
                Calendar cal = Calendar.getInstance();
                cal.setTime(mem.getDebtHandedOverDate().toGregorianCalendar().getTime());
                int day = cal.get(Calendar.DATE);
                int month = cal.get(Calendar.MONTH)+1;
                int year = cal.get(Calendar.YEAR);
                session.setAttribute("debtHandedOverDate", year + "/" + month + "/" + day);
            }
        }

            logger.info("The size of the cover list is " + allDepList.size());
            session.setAttribute("memberClaimSearchResult", allDepList);
            session.setAttribute("policyHolderNumber_text", coverNumber);
            session.setAttribute("policyHolderNumber", coverNumber);

        } else {
            session.setAttribute("policyNumber_error", "Error occured with cover number");
            nextJSP = "/Claims/MemberClaims.jsp";
        }

        return nextJSP;
    }
}

package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static com.koh.command.NeoCommand.service;
import java.util.List;
import javax.servlet.RequestDispatcher;
import neo.manager.SearchResultsAmu;


public class ForwardToAdvanceSearchAmu extends NeoCommand {

    @Override
    public String getName() {
        return "ForwardToAdvanceSearchAmu";
    }

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        HttpSession session = request.getSession();
        session.setAttribute("refreshed", true);
        
        try {
            
            String nextJSP = "/PreAuth/AmuAdvanceSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

}
package com.koh.preauth.command;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author johanl
 */
public class RefreshCommand extends Command{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();

        try {
            String nextJSP = "" + session.getAttribute("refreshScreen");
            System.out.println("RefreshCommand refreshScreen = " + nextJSP);
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }        
        
        
        return null;
    }

    @Override
    public String getName() {
        return "RefreshCommand";
    }
    
}

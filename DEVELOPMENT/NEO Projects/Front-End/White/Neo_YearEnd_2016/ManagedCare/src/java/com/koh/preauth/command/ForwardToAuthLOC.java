/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import static com.koh.utils.DateTimeUtils.convertDateToXMLGregorianCalendar;
import static com.koh.utils.DateTimeUtils.convertToYYYYMMDD;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.BenefitLimitsDisplay;
import neo.manager.CoverClaimBenefitLimitStructure;
import neo.manager.EAuthCoverDetails;
import neo.manager.NeoManagerBean;
import neo.manager.Option;
import neo.manager.PreAuthSearchCriteria;
import neo.manager.SavingsContribution;

/**
 *
 * @author johanl
 */
public class ForwardToAuthLOC extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();

        String onScreen = "" + session.getAttribute("onScreen");
        String nextJSP = "/PreAuth/AuthSpecificLOC.jsp";
        String authType = "" + session.getAttribute("authType");

        String provDisc = "" + session.getAttribute("facilityProvDiscTypeId");

        if (authType.trim().equals("11") || authType.trim().equals("4")) {
            if (provDisc != null && !provDisc.equalsIgnoreCase("null") && !provDisc.equalsIgnoreCase("")) {
                session.setAttribute("facilityProv_error", null);
            } else {
                session.setAttribute("facilityProv_error", "Facility Provider Required");
                nextJSP = onScreen;
            }
        }
        
        System.out.println("provider discipline for ward lookup = " + provDisc);
        int provDiscisipline = 99;
        if (provDisc != null && !provDisc.equalsIgnoreCase("")
                && !provDisc.equalsIgnoreCase("null")) {
            provDiscisipline = Integer.parseInt(provDisc);
        }
        int wardLookup = getLOCLookupId(provDiscisipline);

        System.out.println("ward lookup id = " + wardLookup);
        session.setAttribute("locWardLookupId", wardLookup);
        
        ///////////////////////////////////////////////////////////////////
        getPsyDaysLeftFromBenefit(request);
        ///////////////////////////////////////////////////////////////////

        
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ForwardToAuthLOC";
    }

    public int getLOCLookupId(int providerType) {
        int wardId = 125;
        if (providerType == 79) {
            wardId = 272;

        } else if (providerType == 55) {
            wardId = 273;

        } else if (providerType == 59) {
            wardId = 274;

        } else if (providerType == 49) {
            wardId = 275;

        } else if (providerType == 76) {
            wardId = 276;

        } else if (providerType == 47) {
            wardId = 277;

        } else if (providerType == 56) {
            wardId = 278;

        } else if (providerType == 77) {
            wardId = 279;

        }

        return wardId;
    }
    
    public void getPsyDaysLeftFromBenefit(HttpServletRequest request){
        
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        String memberNumber = session.getAttribute("memberNum_text").toString();
        int depNumber = Integer.parseInt(session.getAttribute("depListValues").toString());
        int authPeriod = Integer.parseInt(session.getAttribute("authPeriod").toString());
        PreAuthSearchCriteria search = new PreAuthSearchCriteria();
        search.setCoverNumber(memberNumber);
        search.setDependantCode(depNumber);
        
        try{
            Date year = new SimpleDateFormat("yyyy/mm/dd").parse(authPeriod + "/01/01");
            XMLGregorianCalendar xyear = convertDateToXMLGregorianCalendar(year);
            search.setAuthDate(xyear);
        }catch(ParseException e){
            e.printStackTrace();
        }
        
        double inptDaysUsed = 0.0d;
        
        inptDaysUsed = port.getPsychiatricAuthsForMember(search);

        System.out.println("### inptDaysUsed: " + inptDaysUsed);
        
        session.setAttribute("inptLimitCount", 21);
        session.setAttribute("inptAvailCount", (21 - inptDaysUsed));
        session.setAttribute("inptUsedCount", inptDaysUsed);
        
//        Option schemeOptionDetails = (Option)session.getAttribute("schemeOptionDetails");
//        
//        System.out.println("### schemeOptionDetails.getOptionCode() " + schemeOptionDetails.getOptionCode());
//        System.out.println("### schemeOptionDetails.getOptionName() " + schemeOptionDetails.getOptionName());
//        System.out.println("### schemeOptionDetails.getOptionId() " + schemeOptionDetails.getOptionId());
//        
//        System.out.println("### scheme" + session.getAttribute("scheme"));
//        System.out.println("### schemeOption" + session.getAttribute("schemeOption"));
//        
//        String scheme = session.getAttribute("scheme").toString();
//        int schemeOption = Integer.parseInt(session.getAttribute("schemeOption").toString());
//        String psyBenefitCode = null;
//        
//        if(scheme.equalsIgnoreCase("1")){
//            System.out.println("Resolution");
//            System.out.println("### schemeName" + session.getAttribute("schemeName"));
//            switch (schemeOption) {
//                case 1:
//                    System.out.println("### 1 Hospital");
//                     psyBenefitCode = "H0B14";
//                    break;
//                case 2:
//                    System.out.println("### 2 Fundamental");
//                    break;
//                case 3:
//                    System.out.println("### 3 Progressive");
//                    break;
//                case 4:
//                    System.out.println("### 4 Prestige");
//                    break;
//                case 5:
//                    System.out.println("### 5 Foundation");
//                     psyBenefitCode = "F0B14";
//                    break;
//                case 6:
//                    System.out.println("### 6 Progressive Select");
//                    break;
//                case 7:
//                    System.out.println("### 7 Progressive Flex");
//                    psyBenefitCode = "P0E14";
//                    break;    
//                case 8:
//                    System.out.println("### 8 Progressive Saver");
//                    break;    
//                case 9:
//                    System.out.println("### 9 Core");
//                    break;    
//                case 10:
//                    System.out.println("### 10 Primary");
//                    break;    
//                case 11:
//                    System.out.println("### 11 Classic");
//                     //benefitCode = "80015"; //non PMB
//                    psyBenefitCode = "80017"; //PMB       
//                    break;    
//                case 12:
//                    System.out.println("### 12 Millennium");
//                    psyBenefitCode = "M0C14";
//                    break;    
//                case 13:
//                    System.out.println("### 13 Supreme");
//                     psyBenefitCode = "S0E14"; //
//                    break;    
//                
//                default:
//                    throw new AssertionError();
//            }
//        }else if(scheme.equalsIgnoreCase("2")){
//            System.out.println("### schemeName" + session.getAttribute("schemeName"));        
//            switch (schemeOption) {
//                case 14:
//                    System.out.println("### 14 Aqua");
//                     psyBenefitCode = "S1045";
//                    break;    
//                case 15:
//                    System.out.println("### 15 Azure");
//                     psyBenefitCode = "S0084";
//                    break;    
//                case 16:
//                    System.out.println("### 16 Cobalt");
//                     psyBenefitCode = "S3095";
//                    break;    
//                case 17:
//                    System.out.println("### 17 Cyan");
//                     psyBenefitCode = "S4052";
//                    break;    
//                case 18:
//                    System.out.println("### 18 Capri");
//                     psyBenefitCode = "S2066";
//                    break;
//                default:
//                    throw new AssertionError();
//            }
//        }
//        System.out.println("### psyBenefitCode " + psyBenefitCode);
//        Date today = new Date(System.currentTimeMillis());
//        String tday = convertToYYYYMMDD(today);
//        System.out.println("### today " + tday);
//        String benDateStr = tday;        
//        Collection<CoverClaimBenefitLimitStructure> benLimStruct = new ArrayList<CoverClaimBenefitLimitStructure>();
//        List<BenefitLimitsDisplay> benefitLimitsForDisplay = null;
//        //convert date
//        Date benDate = null;
//        XMLGregorianCalendar xBenDate = null;
//        try {
//            benDate =  new SimpleDateFormat("yyyy/MM/dd").parse(benDateStr);
//            System.out.println("### benDate " + benDate);
//            xBenDate = DateTimeUtils.convertDateToXMLGregorianCalendar(benDate);
//
//        } catch (ParseException px) {
//            px.printStackTrace();
//        }
//
//        if (xBenDate != null) {
////            benLimStruct = port.getBenefitLimitsForCover(memberNumber, depNumber, xBenDate);
//            benefitLimitsForDisplay = port.getBenefitLimitsForDisplay(memberNumber, depNumber, xBenDate);
//            
//            
//            System.out.println("Ben size :" + benefitLimitsForDisplay.size());
//            //check for Saver Benefit
//            boolean getSaverContribution = false;
//            for (BenefitLimitsDisplay bens : benefitLimitsForDisplay) {
//                if(bens.getBenefitType() == 2){
//                    getSaverContribution = true;
//                    break;
//                }                    
//            }
//            if(getSaverContribution){
//                SavingsContribution sc = port.getSaverBenefitContributionDetails(memberNumber, xBenDate);
////                session.setAttribute("memberBenSavingsContribution", sc);
//                System.out.println("memberBenSavingsContribution " + sc);
//            }
//            
////            session.setAttribute("memberBenefitsList", benLimStruct);
//            for(CoverClaimBenefitLimitStructure s: benLimStruct){
//                System.out.println("memberBenefitsList LowDesc: " + s.getLowDesc() + " MainDesc: " + s.getMainDesc() + " MidDesc: " + s.getMidDesc());                
//            }
////            session.setAttribute("memberBenefitsList2", benefitLimitsForDisplay);
//            for(BenefitLimitsDisplay s2: benefitLimitsForDisplay){
//                System.out.println("memberBenefitsList2 BenefitId: " + s2.getBenefitId() + " BenefitCode: " + s2.getBenefitCode() + " BenefitDescription: " + s2.getBenefitDescription() + " LimitCount: " + s2.getLimitCount() + " AvailCount: "  + s2.getAvailCount() + " UsedCount: " + s2.getUsedCount() );
//                if(psyBenefitCode != null){
//                    System.out.println("psyBenefitCode " + psyBenefitCode + " ?= s2.getBenefitCode() " + s2.getBenefitCode());
//                    if(s2.getBenefitCode().equalsIgnoreCase(psyBenefitCode)){
//                        System.out.println("### Benefit found! BenefitId: " + s2.getBenefitId() + " BenefitCode: " + s2.getBenefitCode() + " BenefitDescription: " + s2.getBenefitDescription() + " LimitCount: " + s2.getLimitCount() + " AvailCount: "  + s2.getAvailCount() + " UsedCount: " + s2.getUsedCount() );
//                        session.setAttribute("inptLimitCount", s2.getLimitCount());
//                        session.setAttribute("inptAvailCount", s2.getAvailCount());
//                        session.setAttribute("inptUsedCount", s2.getUsedCount());
//                    }
//                }
//            }
//            System.out.println("memberBenefitsList2 " + benefitLimitsForDisplay);
//            session.setAttribute("depBenDate_error", null);
            
//        } else {
////            session.setAttribute("depBenDate_error", "Incorrect Date: " + benDateStr);
//            System.out.println("Incorrect Date: " + benDateStr);
//        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import neo.manager.CommunicationHistoryList;
import neo.manager.NeoManagerBean;

/**
 *
 * @author dewaldo
 */
public class GetContactHistoryCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        Integer entity = (Integer) session.getAttribute("entityId");
        PrintWriter out = null;
        try {
            out = response.getWriter();
            NeoManagerBean port = service.getNeoManagerBeanPort();

            List<CommunicationHistoryList> comList = port.getCommHistory(entity);

            session.setAttribute("contactHistory", comList);

            if (comList.isEmpty()) {
                out.print("Failed");
            } else {
                out.print("Done|");
            }
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "GetContactHistoryCommand";
    }
}

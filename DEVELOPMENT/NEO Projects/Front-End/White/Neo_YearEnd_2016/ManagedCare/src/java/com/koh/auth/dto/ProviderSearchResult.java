/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.dto;

/**
 *
 * @author gerritj
 */
public class ProviderSearchResult {

    private String providerType;
    private String providerNumber;
    private String providerName;

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getProviderNumber() {
        return providerNumber;
    }

    public void setProviderNumber(String providerNumber) {
        this.providerNumber = providerNumber;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    } 


}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
//
 * @author gerritj
 */
public class SaveCallCenterPreAuth1Command extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        this.saveScreenToSession(request);
        String authType = "" + session.getAttribute("authType");
        
        try {
            String nextJSP = "";
            if (authType.trim().equalsIgnoreCase("1")) { //optical
                nextJSP = "/PreAuth/OpticalDetails.jsp";

            } else if (authType.trim().equalsIgnoreCase("2") || authType.trim().equalsIgnoreCase("13")) { //dental
                nextJSP = "/PreAuth/DentalDetails.jsp";

            }else if (authType.trim().equalsIgnoreCase("3")){
                nextJSP = "/PreAuth/OrthodonticDetails.jsp";

            }else if (authType.trim().equalsIgnoreCase("4")){
                session.setAttribute("newAuthIndicator", true);
                //check pmb
                GetPMBByICDCommand getPMB = new GetPMBByICDCommand();
                getPMB.execute(request, response, context);
                nextJSP = "/PreAuth/HospitalAuthDetails.jsp";
            
            }else if (authType.trim().equalsIgnoreCase("5")){
                nextJSP = "/PreAuth/ConditionSpecificAuth.jsp";

            }else if (authType.trim().equalsIgnoreCase("6")){
                nextJSP = "/PreAuth/OrthodonticDetails.jsp";
                
            }else if (authType.trim().equalsIgnoreCase("11")){
                nextJSP = "/PreAuth/HospiceDetails.jsp";
                //check pmb
                GetPMBByICDCommand getPMB = new GetPMBByICDCommand();
                getPMB.execute(request, response, context);
            }else{
                nextJSP = "/PreAuth/GenericAuthorisation.jsp";
            }
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return null;
    }

    @Override
    public String getName() {
        return "SaveCallCenterPreAuth1Command";
    }
}

package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static com.koh.command.NeoCommand.service;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import neo.manager.SearchResultsAmu;

public class SearchAuthAmukelanim extends NeoCommand {

    @Override
    public String getName() {
        return "SearchAuthAmukelanim";
    }

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        HttpSession session = request.getSession();
        List<SearchResultsAmu> search = null;
        
        String refNo = request.getParameter("refNo_text");
    
        if(refNo != null && !"".equals(refNo)){

            search = getResultByRefNo(refNo);
        }else{
            search = getResults();
        }
        
        if(search != null){
            
            session.setAttribute("items", search);
            
            try{
                String nextJSP = "/PreAuth/AmukelanimSearch.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

                dispatcher.forward(request, response);
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }

        return null;
    }
    
    private List<SearchResultsAmu> getResultByRefNo(String refNo) {
        
        List<SearchResultsAmu> search = null;
        if(refNo != null && !"".equals(refNo)){

            System.out.println("DO Something here and return to jsp " + refNo);

            search = service.getNeoManagerBeanPort().searchByRefAmuMethod(refNo);
            
        }
        
        return search;
    }
  
     private List<SearchResultsAmu> getResults() {
        
        List<SearchResultsAmu> search = null;

        search = service.getNeoManagerBeanPort().searchByResultsAmuMethod();
            
        return search;
    }
  
}
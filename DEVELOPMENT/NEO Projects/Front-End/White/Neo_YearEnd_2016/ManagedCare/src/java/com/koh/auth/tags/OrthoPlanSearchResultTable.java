/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.tags;

import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.OrthodonticPlan;

/**
 *
 * @author whauger
 */
public class OrthoPlanSearchResultTable extends TagSupport {
    private String javascript;
    private String commandName;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Discipline</th><th>Tariff Code</th><th>Total Amount</th><th>Deposit</th><th>First Installment</th><th>Monthly Installment</th><th>Duration</th><th>Select</th></tr>");

            ArrayList<OrthodonticPlan> plans = (ArrayList<OrthodonticPlan>) req.getAttribute("searchOrthoPlanResult");
            if (plans != null) {
                for (int i = 0; i < plans.size(); i++) {
                    OrthodonticPlan plan = plans.get(i);
                    out.println("<tr><form><td><label class=\"label\">" + plan.getDiscipline() + "</label><input type=\"hidden\" name=\"discipline\" value=\"" + plan.getDiscipline() + "\"/></td>" +
                            "<td><label class=\"label\">" + plan.getTariffCode() + "</label><input type=\"hidden\" name=\"code\" value=\"" + plan.getTariffCode() + "\"/></td>" +
                            "<td><label class=\"label\">R" + plan.getTotalAmount() + "</label><input type=\"hidden\" name=\"total\" value=\"" + plan.getTotalAmount() + "\"/></td>" +
                            "<td><label class=\"label\">R" + plan.getDeposit() + "</label><input type=\"hidden\" name=\"deposit\" value=\"" + plan.getDeposit() + "\"/></td>" +
                            "<td><label class=\"label\">R" + plan.getFirstInstallment() + "</label><input type=\"hidden\" name=\"first\" value=\"" + plan.getFirstInstallment() + "\"/></td>" +
                            "<td><label class=\"label\">R" + plan.getMonthlyInstallment() + "</label><input type=\"hidden\" name=\"monthly\" value=\"" + plan.getMonthlyInstallment() + "\"/></td>" +
                            "<td><label class=\"label\">" + plan.getDuration() + "</label><input type=\"hidden\" name=\"duration\" value=\"" + plan.getDuration() + "\"/></td>" +
                            "<td><button name=\"opperation\" name=\"opperation\" type=\"submit\" " + javascript + " value=\"" + commandName + "\">Select</button></td></form></tr>");

                }
            }
            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in OrthoPlanSearchResultTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

}

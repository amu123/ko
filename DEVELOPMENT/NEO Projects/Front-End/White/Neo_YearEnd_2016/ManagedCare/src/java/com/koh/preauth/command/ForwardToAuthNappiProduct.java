/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import com.koh.command.NeoCommand;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthNappiProduct;

/**
 *
 * @author martins
 */
public class ForwardToAuthNappiProduct extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        String onScreen = request.getParameter("onScreen");
        
        if(onScreen == null || onScreen == ""){
            onScreen = "" + session.getAttribute("onScreen");
        }
        
        session.setAttribute("onScreen", onScreen);
        String nextJSP = "/PreAuth/AuthNappiProductDetails.jsp";
        String authType = "" + session.getAttribute("authType");
        String provTypeId = "";
        
        try{
            String select = request.getParameter("selected");
            if(select != null){
                session.setAttribute("selected", select);

                List<AuthNappiProduct> productList = (List<AuthNappiProduct>) session.getAttribute("nappiList");
                AuthNappiProduct anp = productList.get(Integer.parseInt(select) - 1);
                session.setAttribute("productCode_text", anp.getNappiCode());
                session.setAttribute("quantity", anp.getQuantity());
                session.setAttribute("price", anp.getRand());
                session.setAttribute("fixPrice", anp.getFixedPrice());
                session.setAttribute("prodDesc", anp.getProductDesc());
                session.setAttribute("unitPrice", anp.getUnitPrice());
            } 
        }catch(Exception e){
            e.printStackTrace();
        }

        try {            
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ForwardToAuthNappiProduct";
    }
}

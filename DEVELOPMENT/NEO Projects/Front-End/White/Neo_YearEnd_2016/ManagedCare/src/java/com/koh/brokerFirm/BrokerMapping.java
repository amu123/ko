/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.brokerFirm;

import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import neo.manager.*;

/**
 *
 * @author princes
 */
public class BrokerMapping {

    private static final String[][] FIELD_MAPPINGS = {
        {"BrokerApp_brokerCode", "Broker Code", "yes", "str", "BrokerApp", "brokerCode","0","15"},
        {"brokerEntityId", "", "no", "int", "BrokerApp", "entityId"},
        {"BrokerApp_alternativeCode", "Alternative Code", "no", "str", "BrokerApp", "brokerAltCode","0","15"},
        {"BrokerApp_brokerInitials", "Initials", "yes", "str", "BrokerApp", "initials","0","10"},
        {"BrokerApp_brokerName", "Name", "yes", "str", "BrokerApp", "firstNames","0","50"},
        {"BrokerApp_brokerSurname", "Surname", "yes", "str", "BrokerApp", "surname","0","50"},
        {"BrokerApp_brokerPrefferedName", "Preferred Name", "no", "str", "BrokerApp", "nickName","0","50"},
        {"BrokerApp_brokerIdNumber", "ID Number", "no", "str", "BrokerApp", "idNumber","0","13"},
        {"BrokerApp_brokerDateOfBirth", "Date Of Birth", "no", "date", "BrokerApp", "dateOfBirth"},
        {"BrokerApp_brokerStatus", "Status", "yes", "int", "BrokerApp", "brokerStatus"},
        {"BrokerApp_brokerAlternativeID", "Alternative ID", "no", "str", "BrokerApp", "alternativeId","0","20"},
        {"BrokerApp_correspondanceLanguage", "Language", "yes", "int", "BrokerApp", "languageId"},
        {"BrokerApp_title", "Title", "no", "int", "BrokerApp", "contactTitle"},
        {"BrokerApp_contactName", "Contact First Name", "no", "str", "BrokerApp", "contactFirstName","0","50"},
        {"BrokerApp_region", "Region", "no", "int", "BrokerApp", "region"},
        {"BrokerApp_contactSurname", "Contact Last Name", "no", "str", "BrokerApp", "contactLastName","0","50"}                                           
    };
    
    private static final String[][] BANK_MAPPINGS = {
        
        {"BrokerBankApp_bank", "Name of Bank", "yes", "int", "BrokerBank", ""},
        {"BrokerBankApp_branch", "Branch", "yes", "int", "BrokerBank", ""},
        {"BrokerBankApp_accountType", "Account Type", "yes", "int", "BrokerBank", ""},
        {"BrokerBankApp_accountHolder", "Account Holder", "yes", "str", "BrokerBank", ""},
        {"BrokerBankApp_accountNo", "Account no.", "yes", "numberField", "BrokerBank", ""}
    };
    
    private static final String[][] HISTORY_MAPPINGS = {
        
        {"BrokerApp_brokerHistoryCode", "End Date", "no", "str", "Accreditation", ""}, 
        {"BrokerApp_brokerHistoryName", "End Date", "no", "str", "Accreditation", ""}, 
        {"BrokerApp_historyStartDate", "End Date", "yes", "date", "Accreditation", "effectiveStartDate"}, 
        {"Broker_historyEndDate", "End Date", "yes", "date", "Accreditation", "effectiveEndDate"}
    };
    
    private static final String[][] ACCREDITATION_MAPPINGS = {
        {"BrokerApp_brokerAccreditationType", "Accreditation Type", "yes", "int", "Accreditation", "accreditationType"},
        {"BrokerApp_brokerAccreditationNumber", "Accreditation Number", "yes", "str", "Accreditation", "accreditationNumber"},
        {"BrokerApp_brokerApplicationDate", "Application Date", "no", "date", "Accreditation", "applicationDate"},
        {"BrokerApp_brokerAuthorisationDate", "Authorisation Date", "no", "date", "Accreditation", "authorisationDate"},
        {"BrokerApp_brokerStartDate", "Start Date", "yes", "date", "Accreditation", "effectiveStartDate"},
        {"BrokerApp_brokerEndDate", "End Date", "yes", "date", "Accreditation", "effectiveEndDate"} 
    };
    
    private static final String[][] CONTACT_MAPPINGS = {
        {"BrokerApp_ContactDetails_telNo", "Telephone Number", "no", "str", "ContactDetails", ""},
        {"BrokerApp_ContactDetails_faxNo", "Fax No", "no", "str", "ContactDetails", ""},
        {"BrokerApp_ContactDetails_email", "Email Address", "no", "str", "ContactDetails", ""},        
        {"BrokerApp_ContactDetails_altEmail", "Alternative Email Address", "no", "str", "ContactDetails", ""},
        
        {"BrokerApp_PostalAddress_addressLine1", "Line 1", "no", "str", "AddressDetails", ""},
        {"BrokerApp_PostalAddress_addressLine2", "Line 2", "no", "str", "AddressDetails", ""},
        {"BrokerApp_PostalAddress_addressLine3", "Line 3", "no", "str", "AddressDetails", ""},
        {"BrokerApp_PostalAddress_postalCode", "Postal Code", "no", "str", "AddressDetails", ""},
        
        {"BrokerApp_ResAddress_addressLine1", "Line 1", "no", "str", "AddressDetails", ""},
        {"BrokerApp_ResAddress_addressLine2", "Line 2", "no", "str", "AddressDetails", ""},
        {"BrokerApp_ResAddress_addressLine3", "Line 3", "no", "str", "AddressDetails", ""},
        {"BrokerApp_ResAddress_postalCode", "Physical Code", "no", "str", "AddressDetails", ""}
    };
    private static final Map<String, String[]> FIELD_MAP;
    
    private static final String[][] NOTES_MAPPINGS = {
        {"brokerEntityId","","no", "int", "BrokerApp","entityId"},
        {"notesListSelect","Note Type","no", "int", "BrokerApp","noteType"},
        {"BrokerAppNotesAdd_details","Details","no", "str", "BrokerApp","noteDetails"}
    };

    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }

    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validateAccredittation(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, ACCREDITATION_MAPPINGS);
        return errors;
    }
    
    public static Map<String, String> validateNotes(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, NOTES_MAPPINGS);
        return errors;
    }
    
    public static Map<String, String> validateHistory(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, HISTORY_MAPPINGS);
        return errors;
    }
    
    public static Map<String, String> validateContact(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, CONTACT_MAPPINGS);
        return errors;
    }
    
    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
        return errors;
    }
        
    public static Map<String, String> validateBank(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, BANK_MAPPINGS);
        return errors;
    }    

    public static Broker getBrokerApplication(HttpServletRequest request, NeoUser neoUser) {
        Broker br = (Broker) TabUtils.getDTOFromRequest(request, Broker.class, FIELD_MAPPINGS, "BrokerApp");
        br.setSecurityGroupId(neoUser.getSecurityGroupId());
        br.setLastUpdatedBy(neoUser.getUserId());
        return br;
    }

    public static void setBrokerApplication(HttpServletRequest request, Broker brokerApplication) {
        TabUtils.setRequestFromDTO(request, brokerApplication, FIELD_MAPPINGS, "BrokerApp");
    }

    public static List<AddressDetails> getAddressDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<AddressDetails> cdl = new ArrayList<AddressDetails>();
        cdl.add(getAddressDetail(request, "BrokerApp_PostalAddress_", neoUser, entityId, 2));
        cdl.add(getAddressDetail(request, "BrokerApp_ResAddress_", neoUser, entityId, 1));
        return cdl;
    }

    private static AddressDetails getAddressDetail(HttpServletRequest request, String param, NeoUser neoUser, int entityId, int addrType) {
        AddressDetails ad = new AddressDetails();
        ad.setAddressTypeId(addrType);
        ad.setPrimaryInd("Y");
        ad.setAddressUse(1);
        ad.setAddressLine1(request.getParameter(param + "addressLine1"));
        ad.setAddressLine2(request.getParameter(param + "addressLine2"));
        ad.setAddressLine3(request.getParameter(param + "addressLine3"));
        ad.setPostalCode(request.getParameter(param + "postalCode"));
        //ad.setEntityId(entityId);
        //ad.setSecurityGroupId(neoUser.getSecurityGroupId());
        //ad.setLastUpdatedBy(neoUser.getUserId());
        return ad;
    }

    public static void setBroker(HttpServletRequest request, Broker broker) {
        TabUtils.setRequestFromDTO(request, broker, FIELD_MAPPINGS, "BrokerApp");
    }

    public static void setAddressDetails(HttpServletRequest request, Collection<AddressDetails> addressDetails) {
        if (addressDetails == null) {
            return;
        }
        for (AddressDetails ad : addressDetails) {
            if (ad == null) {
                continue;
            }
            String fieldStart = ad.getAddressTypeId() == 1 ? "BrokerApp_ResAddress_" : "BrokerApp_PostalAddress_";
            request.setAttribute(fieldStart + "addressLine1", ad.getAddressLine1());
            request.setAttribute(fieldStart + "addressLine2", ad.getAddressLine2());
            request.setAttribute(fieldStart + "addressLine3", ad.getAddressLine3());
            request.setAttribute(fieldStart + "postalCode", ad.getPostalCode());
        }
    }

    public static List<ContactDetails> getContactDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<ContactDetails> cdl = new ArrayList<ContactDetails>();
        cdl.add(getContactDetail(request, "BrokerApp_ContactDetails_telNo", neoUser, entityId, 4));
        cdl.add(getContactDetail(request, "BrokerApp_ContactDetails_faxNo", neoUser, entityId, 2));
        cdl.add(getContactDetail(request, "BrokerApp_ContactDetails_cell", neoUser, entityId, 3));
        cdl.add(getContactDetail(request, "BrokerApp_ContactDetails_email", neoUser, entityId, 1));
        cdl.add(getContactDetail(request, "BrokerApp_ContactDetails_altEmail", neoUser, entityId, 8));
        return cdl;
    }

    private static ContactDetails getContactDetail(HttpServletRequest request, String param, NeoUser neoUser, int entityId, int commType) {
        ContactDetails cd = new ContactDetails();
        cd.setCommunicationMethodId(commType);
        cd.setMethodDetails(request.getParameter(param));
        cd.setPrimaryIndicationId(1);
        //cd.setEntityId(entityId);
        //cd.setSecurityGroupId(neoUser.getSecurityGroupId());
        //cd.setLastUpdatedBy(neoUser.getUserId());
        return cd;
    }

    public static void setContactDetails(HttpServletRequest request, Collection<ContactDetails> contactDetails) {
        request.setAttribute("BrokerApp_ContactDetails_title", request.getAttribute("brokerTitle"));
        request.setAttribute("BrokerApp_ContactDetails_region", request.getAttribute("brokerRegion"));
        request.setAttribute("BrokerApp_ContactDetails_contactPerson", request.getAttribute("brokerContactPerson") + " " + request.getAttribute("brokerContactSurname"));
        if (contactDetails == null) {
            return;
        }
        for (ContactDetails cd : contactDetails) {
            if (cd == null) {
                continue;
            }
            String fieldStart = "";
            fieldStart = cd.getCommunicationMethodId() == 4 ? "BrokerApp_ContactDetails_telNo" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 2 ? "BrokerApp_ContactDetails_faxNo" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 3 ? "BrokerApp_ContactDetails_cell" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 1 ? "BrokerApp_ContactDetails_email" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 8 ? "BrokerApp_ContactDetails_altEmail" : fieldStart;
            request.setAttribute(fieldStart, cd.getMethodDetails() == null ? "" : cd.getMethodDetails());
        }
    }

    public static EmployerBilling getEmployerBilling(HttpServletRequest request, NeoUser neoUser) {
        EmployerBilling eg = (EmployerBilling) TabUtils.getDTOFromRequest(request, EmployerBilling.class, FIELD_MAPPINGS, "BrokerApp");
        eg.setSecurityGroupId(neoUser.getSecurityGroupId());
        eg.setLastUpdatedBy(neoUser.getUserId());
        return eg;
    }

    public static void setEmployerBilling(HttpServletRequest request, EmployerBilling employerBilling) {
        TabUtils.setRequestFromDTO(request, employerBilling, FIELD_MAPPINGS, "BrokerApp");
    }
    
    public static List<BankingDetails> getBankingDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<BankingDetails> cdl = new ArrayList<BankingDetails>();
        cdl.add(getBankingDetail(request, "BrokerBankApp_bank", neoUser, entityId, 1));
        cdl.add(getBankingDetail(request, "BrokerBankApp_branch", neoUser, entityId, 2));
        cdl.add(getBankingDetail(request, "BrokerBankApp_branchCode", neoUser, entityId, 4));
        cdl.add(getBankingDetail(request, "BrokerBankApp_accountType", neoUser, entityId, 5));
        cdl.add(getBankingDetail(request, "BrokerBankApp_accountHolder", neoUser, entityId, 6));
        cdl.add(getBankingDetail(request, "BrokerBankApp_accountNo", neoUser, entityId, 7));
        return cdl;
    }
    
    private static BankingDetails getBankingDetail(HttpServletRequest request, String param, NeoUser neoUser, int entityId, int commType) {
        BankingDetails cd = new BankingDetails();
        //cd.setCommunicationMethodId(commType);
        //cd.setMethodDetails(request.getParameter(param));
        //cd.setPrimaryIndicationId(1);
        //cd.setEntityId(entityId);
        //cd.setSecurityGroupId(neoUser.getSecurityGroupId());
        //cd.setLastUpdatedBy(neoUser.getUserId());
        return cd;
    }
    
    public static int getEntityId(HttpServletRequest request) {        
        return Integer.parseInt(request.getParameter("brokerEntityId"));
    }
    
    public static BrokerAccred getAccreditationDetails(HttpServletRequest request, int entityId, NeoUser neoUser, int brokerAccredID) {
        BrokerAccred ba = new BrokerAccred();
        Date today = new Date();
        ba.setBrokerAccredId(brokerAccredID);
        ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setEntityId(entityId);
        ba.setCreatedBy(neoUser.getUserId());
        ba.setLastUpdatedBy(neoUser.getUserId());
        ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setSecurityGroupId(2);
        
        ba.setAccreditationType(Integer.parseInt(request.getParameter("BrokerApp_brokerAccreditationType")));
        ba.setAccreditationNumber(request.getParameter("BrokerApp_brokerAccreditationNumber"));


        if (request.getParameter("BrokerApp_brokerApplicationDate") != null && !request.getParameter("BrokerApp_brokerApplicationDate").isEmpty()) {
            Date appDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("BrokerApp_brokerApplicationDate"));
            ba.setApplicationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(appDate));
        }

        if (request.getParameter("BrokerApp_brokerAuthorisationDate") != null && !request.getParameter("BrokerApp_brokerAuthorisationDate").isEmpty()) {
            Date authDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("BrokerApp_brokerAuthorisationDate"));
            ba.setAuthorisationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(authDate));
        }

        if (request.getParameter("BrokerApp_brokerStartDate") != null) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("BrokerApp_brokerStartDate"));
            ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        }


        if (request.getParameter("BrokerApp_brokerEndDate") != null) {
            Date endDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("BrokerApp_brokerEndDate"));
            ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        }
        return ba;
    }
    
    public static BrokerAccred getAccreditationDetailsForRemove(HttpServletRequest request, int entityId, NeoUser neoUser, int brokerAccredID) {
        BrokerAccred ba = new BrokerAccred();
        Date today = new Date();
        ba.setBrokerAccredId(brokerAccredID);
        ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setEntityId(entityId);
        ba.setCreatedBy(neoUser.getUserId());
        ba.setLastUpdatedBy(neoUser.getUserId());
        ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setSecurityGroupId(2);
        
        int brokerAccreditationType = 0;
        if(request.getAttribute("BrokerApp_brokerAccreditationType_").equals("1")){
            brokerAccreditationType = 1;
        }else if(request.getAttribute("BrokerApp_brokerAccreditationType_").equals("2")){
            brokerAccreditationType = 2;
        }
        ba.setAccreditationType(brokerAccreditationType);
        ba.setAccreditationNumber(request.getAttribute("BrokerApp_brokerAccreditationNumber_").toString());

        if (request.getAttribute("BrokerApp_brokerApplicationDate_") != null && !request.getAttribute("BrokerApp_brokerApplicationDate_").toString().isEmpty()) {
            Date appDate = DateTimeUtils.convertFromYYYYMMDD(request.getAttribute("BrokerApp_brokerApplicationDate_").toString());
            ba.setApplicationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(appDate));
        }

        if (request.getAttribute("BrokerApp_brokerAuthorisationDate_") != null && !request.getAttribute("BrokerApp_brokerAuthorisationDate_").toString().isEmpty()) {
            Date authDate = DateTimeUtils.convertFromYYYYMMDD(request.getAttribute("BrokerApp_brokerAuthorisationDate_").toString());
            ba.setAuthorisationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(authDate));
        }

        if (request.getAttribute("BrokerApp_brokerStartDate_") != null) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getAttribute("BrokerApp_brokerStartDate_").toString());
            ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        }


        if (request.getAttribute("BrokerApp_brokerEndDate_") != null) {
            Date endDate = DateTimeUtils.convertFromYYYYMMDD(request.getAttribute("BrokerApp_brokerEndDate_").toString());
            ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        }
        return ba;
    }

    public static BrokerBrokerFirm getBrokerHistoryDetails(HttpServletRequest request, int entityId, NeoUser neoUser) {

        BrokerBrokerFirm ba = new BrokerBrokerFirm();
        Date today = new Date();
        ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setBrokerEntityId(entityId);
        ba.setBrokerFirmEntityId(new Integer(request.getParameter("brokerFirmEntityId")));
        ba.setCreatedBy(neoUser.getUserId());
        ba.setLastUpdatedBy(neoUser.getUserId());
        ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setSecurityGroupId(2);
        String firmCode = request.getParameter("brokerFirmEntityId");
        if (request.getParameter("BrokerApp_historyStartDate") != null && !request.getParameter("BrokerApp_historyStartDate").isEmpty()) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("BrokerApp_historyStartDate"));
            ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        }


        if (request.getParameter("Broker_historyEndDate") != null && !request.getParameter("Broker_historyEndDate").isEmpty()) {
            Date endDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("Broker_historyEndDate"));
            ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        }
        return ba;
    }
    
    public static EntityNotes getBrokerAppNotes(HttpServletRequest request,NeoUser neoUser) {
        EntityNotes ma = (EntityNotes)TabUtils.getDTOFromRequest(request, EntityNotes.class, NOTES_MAPPINGS, "FirmApp");
        Date today = new Date();        
        ma.setNoteDetails(request.getParameter("BrokerAppNotesAdd_details"));
        ma.setEntityId(new Integer(request.getParameter("brokerEntityId")));
        ma.setNoteType(new Integer(request.getParameter("notesListSelect")));
        ma.setSecurityGroupId(neoUser.getSecurityGroupId());
        ma.setLastUpdatedBy(neoUser.getUserId());
        ma.setCreatedBy(neoUser.getUserId());
        ma.setNoteDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ma.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ma.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
        ma.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        ma.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));               
        return ma;
    }

    public static void setMemberBroker(HttpServletRequest request, Broker mb) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

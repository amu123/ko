/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.command.NeoCommand;
import com.koh.utils.MapUtils;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class ViewConsultantAuditTrailCommand extends NeoCommand{
       @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        try {
            String name = request.getParameter("auditList");
            String entityId = request.getParameter("consultantEntityId");
            int id = 0;
            try {
                id = Integer.parseInt(entityId);
            } catch (Exception e) {
                
            }
            List<KeyValueArray> kva = port.getAuditTrail(name, id);
            Object col = MapUtils.getMap(kva);
            request.setAttribute("ConsultantAuditDetails", col);
            context.getRequestDispatcher("/Broker/ConsultantAuditDetails.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ViewBrokerAuditTrailCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewConsultantAuditTrailCommand";
    }
 
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.Command;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author josephm
 */
public class AllocateBiometricsICD10ToSessionCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
         HttpSession session = request.getSession();

        String forField = "" + session.getAttribute("searchCalled");
        String onScreen = "" + session.getAttribute("onScreen");

        String name = forField.substring(forField.length() - 4, forField.length());
        if (name.equalsIgnoreCase("list")) {
            if (session.getAttribute(forField) != null) {
//                if (session.getAttribute(forField) instanceof ArrayList) {
                ArrayList<String> list = (ArrayList<String>) session.getAttribute(forField);
                list.add(request.getParameter("code") + " - " + request.getParameter("description"));
                session.setAttribute(forField, list);
//                }
            } else {
                ArrayList<String> list = new ArrayList<String>();
                list.add(request.getParameter("code") + " - " + request.getParameter("description"));
                session.setAttribute(forField, list);
            }

        } else {
            session.setAttribute(forField + "_text", request.getParameter("code"));// + " - " + request.getParameter("description"));
        }
        try {
            String nextJSP = onScreen;

            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "AllocateBiometricsICD10ToSessionCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthEmailConfirmation;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Johan-NB
 */
public class GeneratePDFForPrint extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean neo = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        //get values
        //memNo depNo html

        //String bodyHtml = request.getParameter("htmlStr");
        //System.out.println("bodyHTML = " + bodyHtml);

        String authStatus = "" + session.getAttribute("authStatusVal");

        String memNo = "" + session.getAttribute("memberNum_text");
        String authNo = "" + session.getAttribute("authNumberConf");
        String authType = "" + session.getAttribute("authType");
        String prodName = "" + session.getAttribute("schemeName");

        String authIdStr = "" + session.getAttribute("authDetailsID");
        int authId = Integer.parseInt(authIdStr);

        String authHtml = request.getParameter("authHtmlStr");
        String provHtml = request.getParameter("provHtmlStr");
        String memHtml = request.getParameter("memHtmlStr");
        String disclaimerHtml = request.getParameter("discHtmlStr");

        //System.out.println("authHtml : " + authHtml);
        //System.out.println("provHtml : " + provHtml);
        //System.out.println("memHtml : " + memHtml);

        String imgVal = "";
        if (authStatus.equalsIgnoreCase("2")) {
            imgVal = "x";

        } else if (authStatus.equalsIgnoreCase("3")) {
            imgVal = "y";
        }

        AuthEmailConfirmation eaCon = new AuthEmailConfirmation();
        eaCon.setAuthStatus(imgVal);
        eaCon.setAuthHtml(authHtml);
        eaCon.setProviderHtml(provHtml);
        eaCon.setMemberHtml(memHtml);
        eaCon.setDisclaimerHtml(disclaimerHtml);
        eaCon.setAuthType(authType);
        eaCon.setAuthNumber(authNo);
        eaCon.setAuthId(authId);
        eaCon.setProductName(prodName);


        byte[] pdf = neo.generatePreAuthConfirmation(eaCon);
        String displayConfirmation = request.getParameter("displayConfirmation");

        if (displayConfirmation != null && !displayConfirmation.equalsIgnoreCase("null")
                && !displayConfirmation.equalsIgnoreCase("")) {

            if (displayConfirmation.equalsIgnoreCase("no")) {

                //set byte array of generated html
                session.setAttribute("confirmationPDF", pdf);

                String memEmail = "" + session.getAttribute("confirmationMemberEmail");
                String treatEmail = "" + session.getAttribute("treatingProviderEmail");
                String facEmail1 = "" + session.getAttribute("facilityProvEmail");

                String treatProviderEmail = "";
                String facilityProviderEmail = "";

                String memberEmail = "";

                if (memEmail != null && !memEmail.equalsIgnoreCase("")
                        && !memEmail.equalsIgnoreCase("null")) {
                    memberEmail = memEmail;
                }

                //Treating Emails       
                if (treatEmail != null && !treatEmail.equalsIgnoreCase("")
                        && !treatEmail.equalsIgnoreCase("null")) {
                    treatProviderEmail = treatEmail;
                }

                //Facility Emails
                if (facEmail1 != null && !facEmail1.equalsIgnoreCase("")
                        && !facEmail1.equalsIgnoreCase("null")) {
                    facilityProviderEmail = facEmail1;
                }

                session.setAttribute("treatProvEmail", treatProviderEmail);
                session.setAttribute("facProvEmail", facilityProviderEmail);
                session.setAttribute("memberEmail", memberEmail);

                try {
                    RequestDispatcher dispatcher = context.getRequestDispatcher("/PreAuth/EmailConfirmation.jsp");
                    dispatcher.forward(request, response);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            } else {
                String fileName = "EAuthConfirmation for member " + memNo;
                try {
                    printPreview(request, response, pdf, fileName);
                } catch (IOException ex) {
                    Logger.getLogger(GeneratePDFForPrint.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
        return null;
    }

    @Override
    public String getName() {

        return "GeneratePDFForPrint";
    }

    public void printPreview(HttpServletRequest request, HttpServletResponse response, byte[] array, String fileName) throws IOException {
        OutputStream outStream = null;
        try {
            response.setHeader("Pragma", "no-cache"); //HTTP 1.0
            response.setDateHeader("Expires", 0); //prevents caching at the proxy server
            response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-disposition", "attachment; filename=" + fileName);

            response.setContentType("application/pdf");
            response.setContentLength(array.length);
            outStream = response.getOutputStream();
            outStream.write(array);
            outStream.flush();
            outStream.close();

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.practice_management.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.PracticeNotes;

/**
 *
 * @author janf
 */
public class PracticeManagementNotesCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        String command = request.getParameter("command");
        PrintWriter out = null;
        System.out.println("Operation: PracticeManagementNotesCommand - Command: " + command);

        if ("FetchPracticeNotes".equalsIgnoreCase(command)) {
            System.out.println("entityId = " + session.getAttribute("provNumEntityId"));
            int entityId = Integer.parseInt(String.valueOf(session.getAttribute("provNumEntityId")));
            request.setAttribute("entityId", entityId);
            List<PracticeNotes> pn = port.fetchPracticeNotesByEntityId(entityId);
            if (pn != null && !pn.isEmpty()) {
                request.setAttribute("PracticeNotes", pn);
                System.out.println("pn is not null or empty");
            } else {
                System.out.println("Empty pn");
            }
            request.setAttribute("entityId", entityId);
            try {
                String nextJSP;
                nextJSP = "/PracticeManagement/PracticeNotesSummary.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if ("ReturnToViewNotes".equalsIgnoreCase(command)) {
            try {
                String nextJSP;
                nextJSP = "/PracticeManagement/PracticeNotes.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if ("SaveNote".equalsIgnoreCase(command)) {
            try {
                PracticeNotes pn = savePracticeNote(request, port, session);
                System.out.println("Entity Id : " + pn.getEntityId());
                System.out.println("Note Id : " + pn.getNoteId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("ViewNote".equalsIgnoreCase(command)) {
            String snoteId = request.getParameter("noteId");
            System.out.println("NoteId = " + snoteId);
            String note = "";
            if (snoteId != null && snoteId != "undifined") {
                int noteId = Integer.parseInt(snoteId);
                note = port.fetchPracticeNoteByNoteId(noteId);
            }

            request.setAttribute("noteDetails", note);
            session.setAttribute("noteDetails", note);

            try {
                out = response.getWriter();
                out.print(note);
            } catch (Exception e) {
                System.out.println("Printwriter error : " + e.getMessage());
            }
        }else if ("AddNote".equalsIgnoreCase(command)) {
            int entityId = Integer.parseInt(String.valueOf(session.getAttribute("provNumEntityId")));
            request.setAttribute("entityId", entityId);
            session.setAttribute("entityId", entityId);
            try {
                String nextJSP;
                nextJSP = "/PracticeManagement/PracticeNotes_AddNote.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return null;
    }

    private PracticeNotes savePracticeNote(HttpServletRequest request, NeoManagerBean port, HttpSession session) {
        PracticeNotes pn = new PracticeNotes();
        System.out.println("[frs]savePracticeNote entityId = " + request.getParameter("entityId"));
        Date today = new Date(System.currentTimeMillis());
        XMLGregorianCalendar xToday = DateTimeUtils.convertDateToXMLGregorianCalendar(today);
        XMLGregorianCalendar xEnd = DateTimeUtils.convertDateToXMLGregorianCalendar(DateTimeUtils.convertFromYYYYMMDD("2999/12/31"));
        String noteDetails = request.getParameter("noteDetails");
        String notes = request.getParameter("notes");
        System.out.println("[frs]xEnd = " + xEnd);
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        System.out.println("user = " + user);
        pn.setEntityId(Integer.parseInt(request.getParameter("entityId")));
        pn.setCreatedBy(user.getUserId());
        pn.setLastUpdatedBy(user.getUserId());
        pn.setSecurityGroupId(user.getSecurityGroupId());
        System.out.println("[fr]notes = " + notes);
        System.out.println("[fr]noteDetails = " + noteDetails);
        if (noteDetails != null && !noteDetails.equalsIgnoreCase("")) {
            pn.setNoteDetails(noteDetails);
        } else {
            pn.setNoteDetails(notes);
        }
        pn.setNoteDate(xToday);
        pn.setCreationDate(xToday);
        pn.setEffectiveStartDate(xToday);
        pn.setEffectiveEndDate(xEnd);
        pn.setLastUpdateDate(xToday);

        pn.setNoteId(port.savePracticeNote(pn));
        return pn;
    }

    @Override
    public String getName() {
        return "PracticeManagementNotesCommand";
    }

}

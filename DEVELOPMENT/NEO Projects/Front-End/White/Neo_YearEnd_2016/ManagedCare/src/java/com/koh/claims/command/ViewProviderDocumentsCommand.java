/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author janf
 */
public class ViewProviderDocumentsCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        
        String providerNumber = "";
        if (session.getAttribute("practiceNumber") != null) {
            providerNumber = (String) session.getAttribute("practiceNumber");
        } else {
            providerNumber = request.getParameter("provNum_text");
        }
        System.out.println("EntityId = " + session.getAttribute("provEntityID"));
        System.out.println("providerNumber = " + providerNumber);

        request.setAttribute("entityId", session.getAttribute("provEntityID"));
        session.setAttribute("memberCoverEntityId", session.getAttribute("provEntityID"));
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher("/Claims/ProviderViewDocuments.jsp");
            dispatcher.forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(ViewProviderDocumentsCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ViewProviderDocumentsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewProviderDocumentsCommand";
    }

}

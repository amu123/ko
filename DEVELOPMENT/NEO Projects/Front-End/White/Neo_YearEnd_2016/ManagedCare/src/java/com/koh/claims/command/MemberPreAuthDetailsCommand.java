/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.NeoUser;
import neo.manager.PreAuthSearchCriteria;
import neo.manager.PreAuthSearchResults;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class MemberPreAuthDetailsCommand extends NeoCommand {

    private Logger log = Logger.getLogger(PreAuthDetailsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);

        log.info("Inside MemberPreAuthDetailsCommand");
        HttpSession session = request.getSession();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        log.info("The user id is " + user.getUsername());
        log.info("The user id is " + user.getUserId());

        String authNumber = request.getParameter("authNo");
        String authType = request.getParameter("authType");
        String authDate = request.getParameter("authDate");
        String coverNumber = "" + session.getAttribute("policyHolderNumber");
        String depNumber = request.getParameter("depListValues");

        log.info("LOgging info");
        log.info("The auth number is " + authNumber);
        log.info("The auth type is " + authType);
        log.info("The auth date " + authDate);
        log.info("The cover number is " + coverNumber);
        log.info("The dependenat number is " + depNumber);

        PreAuthSearchCriteria preAuthSearch = new PreAuthSearchCriteria();

        if (authNumber != null && !authNumber.trim().equalsIgnoreCase("")) {
            preAuthSearch.setAuthNumber(authNumber);
        }

        if (authType != null && !authType.trim().equalsIgnoreCase("99")) {
            preAuthSearch.setAuthType(authType);
        }

        if (authDate != null && !authDate.trim().equalsIgnoreCase("")) {
            XMLGregorianCalendar aDate = null;
            try {
                aDate = DateTimeUtils.convertDateToXMLGregorianCalendar(dateFormat.parse(authDate));
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            if (aDate != null) {
                preAuthSearch.setAuthDate(aDate);
            }
        }

        if (coverNumber != null && !coverNumber.trim().equalsIgnoreCase("")) {
            preAuthSearch.setCoverNumber(coverNumber);
        }

        if (depNumber != null && !depNumber.trim().equalsIgnoreCase("") && !depNumber.trim().equalsIgnoreCase("99")) {
            preAuthSearch.setDependantCode(Integer.parseInt(depNumber));
        }

        log.info("The auth number from the object " + preAuthSearch.getAuthNumber());
        log.info("The auth type from the object" + preAuthSearch.getAuthType());
        log.info("The auth date from the object" + preAuthSearch.getAuthDate());
        log.info("The cover number from the object" + preAuthSearch.getCoverNumber());
        log.info("The dependent number from the object is " + preAuthSearch.getDependantCode());


        List<PreAuthSearchResults> paResult = service.getNeoManagerBeanPort().findPreAuthDetailsByCriteria(user, preAuthSearch);
        if (paResult == null || paResult.isEmpty() == true) {
            request.setAttribute("authSearchResultsMessage", "No results found.");
        } else if (paResult.size() >= 100) {
            request.setAttribute("authSearchResultsMessage", "Only first 100 results shown. Please refine search.");
        } else {
            request.setAttribute("authSearchResultsMessage", null);
        }
        session.setAttribute("authSearchResults", paResult);

        log.info("The size fo the preauth list is " + paResult.size());

        try {

            //String nextJSP = "/Claims/MemberDetailsTabbedPage.jsp";
            String nextJSP = "/Claims/MemberPreAuthorisationDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        return null;
    }

    @Override
    public String getName() {

        return "MemberPreAuthDetailsCommand";
    }
}

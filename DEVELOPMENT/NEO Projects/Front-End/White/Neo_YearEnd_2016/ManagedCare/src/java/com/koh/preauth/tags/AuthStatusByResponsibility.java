/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.agile.tags.LabelNeoLookupValueDropDownError;
import com.agile.tags.LabelTextDisplay;
import com.koh.command.NeoCommand;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.SecurityResponsibility;

/**
 *
 * @author Johan-NB
 */
public class AuthStatusByResponsibility extends TagSupport {

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        HttpSession session = pageContext.getSession();
        boolean securityPass = false;
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        String authStatus = "" + session.getAttribute("authStatus");

        //get user responsibility
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        int userRespId = 0;
        for (SecurityResponsibility security : user.getSecurityResponsibility()) {
            userRespId = security.getResponsibilityId();
        }
        if (userRespId == 2 || userRespId == 3) {
            securityPass = true;
        }

        if (securityPass == true) {
            LabelNeoLookupValueDropDownError asDrop = new LabelNeoLookupValueDropDownError();
            asDrop.setDisplayName("Auth Status");
            asDrop.setElementName("authStatus");
            asDrop.setLookupId("111");
            asDrop.setMandatory("yes");
            asDrop.setErrorValueFromSession("yes");
            asDrop.setJavaScript("onChange=\"setHospStatus(this.value);\"");
            //required fields
            asDrop.setPageContext(pageContext);
            asDrop.doEndTag();

        } else {
            //get status desc
            String statusDesc = port.getValueFromCodeTableForId("Auth Status", authStatus);
            session.setAttribute("displayStatus", statusDesc);
            //create tag
            LabelTextDisplay asLabel = new LabelTextDisplay();
            asLabel.setDisplayName("Auth Status");
            asLabel.setElementName("displayStatus");
            asLabel.setValueFromSession("yes");
            //required fields
            asLabel.setPageContext(pageContext);
            asLabel.doEndTag();

        }

        return super.doEndTag();
    }
}

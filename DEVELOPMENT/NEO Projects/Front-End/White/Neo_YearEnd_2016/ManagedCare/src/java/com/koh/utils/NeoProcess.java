/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.utils;

import java.util.Date;

/**
 *
 * @author yuganp
 */
public class NeoProcess {
    private String applicationName;
    private String command;
    private Date startTime;
    private Process process;
    private int logId ;

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public int getLogId() {
        return logId;
    }

    public void setLogId(int logId) {
        this.logId = logId;
    }
    
}

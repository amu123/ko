/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.LookupValue;

/**
 *
 * @author johanl
 */
public class NappiSearchTag extends TagSupport {
    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        String napCode = "" + session.getAttribute("nappiCode");
        String napDesc = "" + session.getAttribute("nappiDesc");
        String napAmount = "" + session.getAttribute("nappiAmount");
        String napDosage = "" + session.getAttribute("nappiDosage");
        String napQuan = "" + session.getAttribute("nappiQuan");

        if (napCode == null || napCode.equals("") || napCode.equalsIgnoreCase("null")){
            napCode = "";
        }
        if (napDesc == null || napDesc.equals("") || napDesc.equalsIgnoreCase("null")){
            napDesc = "";
        }
        if (napAmount == null || napAmount.equals("") || napAmount.equalsIgnoreCase("null")){
            napAmount = "";
        }
        if (napDosage == null || napDosage.equals("") || napDosage.equalsIgnoreCase("null")){
            napDosage = "";
        }
        if (napQuan == null || napQuan.equals("") || napQuan.equalsIgnoreCase("null")){
            napQuan = "";
        }

        try {
            out.println("<td colspan=\"7\"><table width=\"900\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            //if (!elementName.trim().equalsIgnoreCase("")) {
            out.println("<tr><th>Provider Category</th><th>Nappi Code</th><th>Nappi Desc</th>" +
                    "<th>Item Cost</th><th>Dosage</th><th>Quantity</th><th>Frequency</th></tr>");

            out.println("<tr><form>");


            out.println("<td><select name=\"provCatType\" id=\"provCatType\">");
            List<neo.manager.LookupValue> lookUps = NeoCommand.service.getNeoManagerBeanPort().getCodeTable(59);
            for (int i = 0; i < lookUps.size(); i++) {
                LookupValue lookupValue = lookUps.get(i);
                out.println("<option value=\"" + lookupValue.getId() + "\" >" + lookupValue.getValue() + "</option>");
            }
            out.println("</select></td>");
            

            out.println("<td><input type=\"text\" size=\"15\" id=\"nCode\" name=\"nCode\" value=\"" + napCode + "\" readonly /></td>" +
                    "<td width=\"200\"><input type=\"text\" id=\"nDesc\" name=\"nDesc\" value=\"" + napDesc + "\" readonly /></td>" +
                    "<td><input type=\"text\" size=\"10\" id=\"nAmount\" name=\"nAmount\" value=\"" + napAmount + "\" readonly /></td>" +
                    "<td><input type=\"text\" size=\"10\" id=\"nDosage\" name=\"nDosage\" value=\"" + napDosage + "\" /></td>" +
                    "<td><input type=\"text\" size=\"10\" id=\"nQuan\" name=\"nQuan\" value=\"" + napQuan + "\" onChange=\"calNewAmount(this.value);\" /></td>" +
                    "<td><input type=\"text\" size=\"10\" id=\"nFreq\" name=\"nFreq\" value=\"\" onChange=\"validateFreq(this.value);\" /></td>" +
                    "</tr>");

            out.println("<tr>" +
                    "<td width=\"200px\" align=\"left\"><label id=\"napGrid_error\" class=\"error\"></label></td>" +
                    "<td colspan=\"6\" align=\"right\"><button type=\"button\" " + javaScript + " name=\"opperation\" value=\"" + commandName + "\">Add Nappi</button></td></form></tr>");
            //}
            out.println("</table></td>");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

}

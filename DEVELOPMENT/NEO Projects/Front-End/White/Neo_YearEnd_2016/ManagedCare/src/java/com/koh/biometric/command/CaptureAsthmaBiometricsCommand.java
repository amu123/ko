/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import java.lang.Exception;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author josephm
 */
public class CaptureAsthmaBiometricsCommand extends NeoCommand {

    private Object _datatypeFactory;
    private String nextJSP;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        boolean checkError = false;
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        //clearAllFromSession(request);

        AsthmaBiometrics asthma = new AsthmaBiometrics();

        asthma.setBiometricTypeId("1");

        NeoManagerBean port = service.getNeoManagerBeanPort();

        //String asthmaDaySymptoms = session.getAttribute(nextJSP) //request.getParameter("asthmaDaySym");
        String asthmaDaySymptoms = (String) session.getAttribute("asthmaDaySym_text");
        if (asthmaDaySymptoms != null && !asthmaDaySymptoms.equalsIgnoreCase("99")) {
            asthma.setAsthmaDayTimeSymptoms(asthmaDaySymptoms);
            LookupValue asthmaday = new LookupValue();
            asthmaday.setId(asthmaDaySymptoms);
            asthmaday.setValue(port.getValueFromCodeTableForId("Asthma Day", asthmaDaySymptoms));
            asthma.setAsthmaDaySymptomsLookup(asthmaday);
            session.removeAttribute("asthmaDaySym_text");
        } /*else {

         session.setAttribute("asthmaDaySym_error", "The day time symptoms should be filled in");
         checkError = true;
         }*/

        //String asthmaNightSymtoms = request.getParameter("asthmaNightSym");
        String asthmaNightSymtoms = (String) session.getAttribute("asthmaNightSym_text");  
        if (asthmaNightSymtoms != null && !asthmaNightSymtoms.equalsIgnoreCase("99")) {
            asthma.setAsthmaNightTimeSymtoms(asthmaNightSymtoms);
            LookupValue asthmanight = new LookupValue();
            asthmanight.setId(asthmaNightSymtoms);
            asthmanight.setValue(port.getValueFromCodeTableForId("Asthma Night", asthmaNightSymtoms));
            asthma.setAsthmaNightSymptomsLookup(asthmanight);
            session.removeAttribute("asthmaNightSym_text");
        } /*else {

         session.setAttribute("asthmaNightSym_error", "The night time symptoms should be filled in");
         checkError = true;
         }*/


        //String sabaUse = request.getParameter("sabaUse");
        String sabaUse = (String) session.getAttribute("sabaUse_text");
        if (sabaUse != null && !sabaUse.equalsIgnoreCase("99")) {
            asthma.setSabaUse(sabaUse);
            LookupValue saba = new LookupValue();
            saba.setId(sabaUse);
            saba.setValue(port.getValueFromCodeTableForId("Saba Use", sabaUse));
            asthma.setSabaUseLookup(saba);
            session.removeAttribute("sabaUse_text");
        } /*else {

         session.setAttribute("sabaUse_error", "The SABA use should be filled in");
         checkError = true;
         }*/

        //String detail = request.getParameter("detail");
        String detail = (String) session.getAttribute("astmanDetail_text");
        if (detail != null && !detail.equalsIgnoreCase("")) {
            asthma.setDetail(detail);
            session.removeAttribute("astmanDetail_text");
        } /*else {

         session.setAttribute("detail_error", "The detail should be filled in");
         checkError = true;
         }*/


        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        try {
            //String receivedDate = request.getParameter("dateReceived");
            String receivedDate = (String) session.getAttribute("dateReceived_text");
            if (receivedDate != null && !receivedDate.equalsIgnoreCase("")) {
                Date dateReceived = dateFormat.parse(receivedDate);
                asthma.setDateReceived(convertDateXML(dateReceived));
            }else {

             session.setAttribute("dateReceived_error", "The date received should be filled in");
             checkError = true;
             }
        } catch (Exception e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateTime = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String UpdateBiometrics = (String) session.getAttribute("UpdateBiometrics");
        if (UpdateBiometrics != null && UpdateBiometrics.equals("true")) {
            dateTime = new SimpleDateFormat("yyyy/MM/dd");
        }
        try {
            //String measuredDate = request.getParameter("dateMeasured");
            String measuredDate = (String) session.getAttribute("dateMeasured_text");
            if (measuredDate != null && !measuredDate.equalsIgnoreCase("")) {
                Date dateMeasured = dateTime.parse(measuredDate);
                asthma.setDateMeasured(convertDateXML(dateMeasured));
            }else {

             session.setAttribute("dateMeasured_error", "The date measured should be filled in");
             checkError = true;
             }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //String treatingProvider = request.getParameter("providerNumber_text");
        String treatingProvider = (String) session.getAttribute("providerNumber_text");
        if (treatingProvider != null && !treatingProvider.equalsIgnoreCase("")) {

            asthma.setTreatingProvider(treatingProvider);
        } /*else {

         session.setAttribute("providerNumber_et_error", "The treating provider should be filled in");
         checkError = true;
         }*/

        //String source = request.getParameter("source");
        String source = (String) session.getAttribute("source_text");
        if (source != null && !source.equalsIgnoreCase("99")) {

            asthma.setSource(source);
        } /*else {

         session.setAttribute("source_error", "The source should be filled in");
         checkError = true;
         }*/

        
        //String icd10 = request.getParameter("icd10");
        //String icd10 = (String) session.getAttribute("icd10_text"); 
        String icd10 = (String) session.getAttribute("asthmaLookupValue");
        if (icd10 != null && !icd10.equalsIgnoreCase("99")) {
            asthma.setIcd10(icd10);
            LookupValue icd = new LookupValue();
            icd.setId(icd10);
            icd.setValue(port.getValueFromCodeTableForId("Asthma Biometric ICD", icd10));
            asthma.setIcd10Lookup(icd);
        } else {

            session.setAttribute("icd10_error", "The ICD10 should be filled in");
            checkError = true;
        }


        //String bpef = request.getParameter("bpef");
        String bpef = (String) session.getAttribute("bpef_text");
        if (bpef != null && !bpef.equalsIgnoreCase("")) {
            if (bpef.matches("[0-9]+")) {
                double bpefValue = Double.parseDouble(bpef);
                if (bpefValue > 100 || bpefValue < 0) {
                    session.setAttribute("bpef_error", "PEF% must be between 0 and 100");
                    checkError = true;
                } else {
                    asthma.setBpef(bpefValue);
                    session.removeAttribute("bpef_text");
                }
            } else {
                session.setAttribute("bpef_error", "The PEF% should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bpef_error", "The PEF% should be filled in");
         checkError = true;
         }*/


        //String exerciseInWeek = request.getParameter("exerciseQuestion");
        String exerciseInWeek = (String) session.getAttribute("exerciseQuestion_text");
        if (exerciseInWeek != null && !exerciseInWeek.equalsIgnoreCase("")) {
            if (exerciseInWeek.matches("[0-9]+")) {
                int exerciseInWeekValue = Integer.parseInt(exerciseInWeek);
                asthma.setExercisePerWeek(exerciseInWeekValue);
            } else {
                session.setAttribute("exerciseQuestion_error", "The exercise field should be a numeric value");
                checkError = true;
            }
        } /*else {
         session.setAttribute("exerciseQuestion_error", "The exercise field should be filled in");
         checkError = true;
         }*/

        //String currentSmoker = request.getParameter("currentSmoker");
        String currentSmoker = (String) session.getAttribute("currentSmoker_text");
        if (currentSmoker != null && !currentSmoker.equalsIgnoreCase("99")) {
            asthma.setCurrentSmoker(currentSmoker);
            LookupValue smoker = new LookupValue();
            smoker.setId(currentSmoker);
            smoker.setValue(port.getValueFromCodeTableForId("YesNo Type", currentSmoker));
            asthma.setCurrentSmokerLookup(smoker);

            //session.setAttribute("currentSmoker", asthma.getCurrentSmoker());
        } /*else {

         session.setAttribute("currentSmoker_error", "The current smoker field should be filled in");
         checkError = true;
         }*/

        //String numberOfCigaretes = request.getParameter("smokingQuestion");
        String numberOfCigaretes = (String) session.getAttribute("smokingQuestion_text");
        if (numberOfCigaretes != null && !numberOfCigaretes.equalsIgnoreCase("")) {
            if (numberOfCigaretes.matches("[0-9]+")) {
                int numberOfCigaretesValue = Integer.parseInt(numberOfCigaretes);
                asthma.setCigarettesPerDay(numberOfCigaretesValue);
                //session.setAttribute("smokingQuestion", asthma.getCigarettesPerDay());
            } else {

                session.setAttribute("smokingQuestion_error", "The cigarettes field should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("smokingQuestion_error", "The cigarettes field should be filled in");
         checkError = true;
         }*/

        // might remove this
        //String exSmoker = request.getParameter("exSmoker");
        String exSmoker = (String) session.getAttribute("exSmoker_text");
        if (exSmoker != null && !exSmoker.equalsIgnoreCase("")) {         //equalsIgnoreCase("99")
            asthma.setExSmoker(exSmoker);
            LookupValue exsmoker = new LookupValue();
            exsmoker.setId(exSmoker);
            exsmoker.setValue(port.getValueFromCodeTableForId("YesNo Type", exSmoker));
            asthma.setExSmokerLookup(exsmoker);

            //session.setAttribute("exSmoker", asthma.getExSmoker());
        } /*else {

         session.setAttribute("exSmoker_error", "The ex smoker field should be filled in");
         checkError = true;
         }*/

        //String yearsSinceSmoking = request.getParameter("stopped");
        String yearsSinceSmoking = (String) session.getAttribute("stopped_text");
        if (yearsSinceSmoking != null && !yearsSinceSmoking.equalsIgnoreCase("")) {
            if (yearsSinceSmoking.matches("[0-9]+")) {
                int yearsSinceSmokingValue = Integer.parseInt(yearsSinceSmoking);
                asthma.setYearsSinceStopped(yearsSinceSmokingValue);
                session.setAttribute("stopped_error", "");
                
            } else {
                session.setAttribute("stopped_error", "The years stopped field should be a numeric value");
                checkError = true;
            }
        }

        //String alcoholUnits = request.getParameter("alcoholConsumption");
        String alcoholUnits = (String) session.getAttribute("alcoholConsumption_text");
        System.out.println("###alcohol value: " + alcoholUnits);
        if (alcoholUnits != null && !alcoholUnits.equalsIgnoreCase("")) {
            if (alcoholUnits.matches("[0-9]+")) {
                int alcoholUnitsValue = Integer.parseInt(alcoholUnits);
                asthma.setAlcoholUnitsPerWeek(alcoholUnitsValue);
            } else {
                session.setAttribute("alcoholConsumption_error", "The alcohol units field should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("alcoholConsumption_error", "The alcohol units field should be filled in");
         checkError = true;
         }*/

        //String weight = request.getParameter("weight");
        String weight = (String) session.getAttribute("weight_text");
        //String height = request.getParameter("length");
        String height = (String) session.getAttribute("length_text");
        if (weight != null && !weight.equalsIgnoreCase("")) {
            if (weight.matches("([0-9]+(\\.[0-9]+)?)+")) {
                if (height != null && !height.equalsIgnoreCase("")) {
                    if (height.matches("([0-9]+(\\.[0-9]+)?)+")) {
                        double heightValue = Double.parseDouble(height);
                        double weightValue = Double.parseDouble(weight);
                        asthma.setHeight(heightValue);
                        asthma.setWeight(weightValue);
                    } else {
                        session.setAttribute("length_error", "The length should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("length_error", "Require length to calculate BMI");
                    checkError = true;
                }
            } else {

                session.setAttribute("weight_error", "The weight should be a numeric value");
                checkError = true;
            }
        }/*else {

         session.setAttribute("weight_error", "The weight should be filled in");
         checkError = true;
         }*/

        if (height != null && !height.equalsIgnoreCase("")) {
            if (height.matches("([0-9]+(\\.[0-9]+)?)+")) {
                if (weight != null && !weight.equalsIgnoreCase("")) {
                    if (weight.matches("([0-9]+(\\.[0-9]+)?)+")) {
                        double heightValue = Double.parseDouble(height);
                        double weightValue = Double.parseDouble(weight);
                        asthma.setHeight(heightValue);
                        asthma.setWeight(weightValue);
                    } else {
                        session.setAttribute("weight_error", "The weight should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("weight_error", "Require weight to calculate BMI");
                    checkError = true;
                }
            } else {

                session.setAttribute("length_error", "The length should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("length_error", "The length should be filled in");
         checkError = true;
         }*/

        //String bmi = request.getParameter("bmi");
        String bmi = (String) session.getAttribute("bmi_text");
        if (bmi != null && !bmi.equalsIgnoreCase("")) {
            if (bmi.matches("([0-9]+(\\.[0-9]+)?)+")) {
                double bmiValue = Double.parseDouble(bmi);
                asthma.setBmi(bmiValue);
            } else {

                session.setAttribute("bmi_error", "The BMI should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bmi_error", "The BMI should be filled in");
         checkError = true;
         }*/


        //String bloodPresureSystolic = request.getParameter("bpSystolic");
        String bloodPresureSystolic = (String) session.getAttribute("bpSystolic_text");
        //String bloodPressureDiastolic = request.getParameter("bpDiastolic");
        String bloodPressureDiastolic = (String) session.getAttribute("bpDiastolic_text");
        if (bloodPresureSystolic != null && !bloodPresureSystolic.equalsIgnoreCase("")) {
            if (bloodPresureSystolic.matches("[0-9]+")) {
                if (bloodPressureDiastolic != null && !bloodPressureDiastolic.equalsIgnoreCase("")) {
                    if (bloodPressureDiastolic.matches("[0-9]+")) {
                        int bloodvalueSys = Integer.parseInt(bloodPresureSystolic);
                        int bloodValueDia = Integer.parseInt(bloodPressureDiastolic);
                        asthma.setBloodPressureSystolic(bloodvalueSys);
                        asthma.setBloodPressureDiastolic(bloodValueDia);
                    } else {

                        session.setAttribute("bpDiastolic_error", "The blood pressure should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("bpDiastolic_error", "The blood pressure should be filled in");
                    checkError = true;
                }
            } else {

                session.setAttribute("bpSystolic_error", "The blood pressure should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bpSystolic_error", "The blood pressure should be filled in");
         checkError = true;
         }*/

        if (bloodPressureDiastolic != null && !bloodPressureDiastolic.equalsIgnoreCase("")) {
            if (bloodPressureDiastolic.matches("[0-9]+")) {
                if (bloodPresureSystolic != null && !bloodPresureSystolic.equalsIgnoreCase("")) {
                    if (bloodPresureSystolic.matches("[0-9]+")) {
                        int bloodvalueSys = Integer.parseInt(bloodPresureSystolic);
                        int bloodValueDia = Integer.parseInt(bloodPressureDiastolic);
                        asthma.setBloodPressureSystolic(bloodvalueSys);
                        asthma.setBloodPressureDiastolic(bloodValueDia);
                    } else {

                        session.setAttribute("bpSystolic_error", "The blood pressure should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("bpSystolic_error", "The blood pressure should be filled in");
                    checkError = true;
                }
            } else {

                session.setAttribute("bpDiastolic_error", "The blood pressure should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bpDiastolic_error", "The blood pressure should be filled in");
         checkError = true;
         }*/


        //String memberNumber = request.getParameter("memberNumber_text");
        String memberNumber = (String) session.getAttribute("memberNumber_text");
        if (memberNumber != null && !memberNumber.equalsIgnoreCase("")) {

            asthma.setMemberNumber(memberNumber);
        } else {

            session.setAttribute("memberNumber_error", "The member number should be filled in");
            checkError = true;
        }


        //String dependantCode = request.getParameter("depListValues");
        String dependantCode = (String) session.getAttribute("depListValues_text");
        if (dependantCode != null && !dependantCode.equalsIgnoreCase("99") && !dependantCode.equalsIgnoreCase("")
                && !dependantCode.equalsIgnoreCase("null")) {

            int code = Integer.parseInt(dependantCode);
            asthma.setDependantCode(code);
        } else {

            session.setAttribute("depListValues_error", "The cover dependants should be filled in");
            checkError = true;
        }


        try {
            if (checkError) {

                saveScreenToSession(request);
                nextJSP = "/biometrics/CaptureBiometrics.jsp";   //CaptureAsthma.jsp";
            } else {
                //Check if the user is UPDATING or CREATING
                if (UpdateBiometrics != null && UpdateBiometrics.equals("true")) {
                    String oldBioID = (String) session.getAttribute("BiometricID");
                    System.out.println("OLDBIOID = " + oldBioID);
                    //clearAllFromSession(request);
                    int userId = user.getUserId();
                    nextJSP = "/biometrics/CaptureBiometrics.jsp";     //CaptureAsthma.jsp";
                    BiometricsId bId = port.saveAsthmaBiometrics(asthma, userId, 1, oldBioID);
                    int asthmaId = bId.getId();
                    request.setAttribute("revNo", bId.getReferenceNo());

                    if (asthmaId > 0) {

                        asthma.setBiometricsId(asthmaId);
//                    port.createTrigger(asthma);
                        session.setAttribute("updated", "Your biometrics data has been captured");
                    } else {
                        session.setAttribute("failed", "Sorry! Asthma Biometrics save has failed, Please try again!");
                        nextJSP = "/biometrics/CaptureBiometrics.jsp";     //CaptureAsthma.jsp";

                    }
                } else {
                    //clearAllFromSession(request);
                    int userId = user.getUserId();
                    nextJSP = "/biometrics/CaptureBiometrics.jsp";     //CaptureAsthma.jsp";
                    BiometricsId bId = port.saveAsthmaBiometrics(asthma, userId, 1, null);
                    int asthmaId = bId.getId();
                    request.setAttribute("revNo", bId.getReferenceNo());

                    if (asthmaId > 0) {

                        asthma.setBiometricsId(asthmaId);
//                    port.createTrigger(asthma);
                        session.setAttribute("updated", "Your biometrics data has been captured");
                    } else {
                        session.setAttribute("failed", "Sorry! Asthma Biometrics save has failed, Please try again!");
                        nextJSP = "/biometrics/CaptureBiometrics.jsp";     //CaptureAsthma.jsp";

                    }
                }
            }
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

            // The method to trigger the XML method to the rule engine
        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    public XMLGregorianCalendar convertDateXML(Date date) {

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getDatatypeFactory().newXMLGregorianCalendar(calendar);

    }

    private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                ex.printStackTrace();
            }
        }

        return (DatatypeFactory) _datatypeFactory;
    }

    @Override
    public String getName() {
        return "CaptureAsthmaBiometricsCommand";
    }
}

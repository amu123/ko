/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.GenericTariff;

/**
 *
 * @author josephm
 */
public class LoadICDCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        String tarrifCode = request.getParameter("");

        GenericTariff tafiff = service.getNeoManagerBeanPort().fetchTariffs(tarrifCode);


        session.setAttribute("tariffCodes", tafiff);

        try {
            String nextJSP = "/biometrics/CaptureGeneral.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        }catch(Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "LoadICDCommand";
    }
}

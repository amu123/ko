/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker_firm.application.commands;

import com.koh.brokerFirm.BrokerFirmMainMapping;
import com.koh.command.NeoCommand;
import com.koh.employer.EmployerGroupMapping;
import com.koh.employer.command.TabUtils;
import com.koh.member.application.MemberAppNotesMapping;
import com.koh.member.application.command.MemberApplicationHospitalCommand;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.*;

/**
 *
 * @author princes
 */
public class BrokerFirmNotesCommand  extends NeoCommand{
       @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
            Enumeration e = request.getParameterNames();
            while (e.hasMoreElements()) {
                String pName = "" + e.nextElement();
                System.out.println(pName + " " + request.getParameter(pName));
            }
        try {
            String s = request.getParameter("buttonPressed");
            if (s != null && !s.isEmpty()) {
                System.out.println("buttonPressed : " + s);
                if (s.equalsIgnoreCase("Add Note")) {
                    addDetails(request, response, context);
                } else if (s.equalsIgnoreCase("SaveButton")) {
                    saveDetails(request, response, context);
                } else if (s.equalsIgnoreCase("SelectButton")) {
                    getNotesList(request, response, context);
                } else if (s.equalsIgnoreCase("ViewButton")) {
                    viewNote(request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(MemberApplicationHospitalCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
       
    private void addDetails(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        request.setAttribute("noteId", null);
        context.getRequestDispatcher("/Broker/BrokerFirmAppNotesAdd.jsp").forward(request, response);
    }
    
    private void viewNote(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        
        int noteId = getStrToInt(request.getParameter("noteId"));
        //        String s = port.fetchMember
        EntityNotes employerAppNoteByNoteId = port.fetchEmployerAppNoteByNoteId(noteId);
        request.setAttribute("noteDetails", employerAppNoteByNoteId.getNoteDetails());
//        MemberAppNotesMapping.setMemberAppNotes(request, memberAppNoteByNoteId);
        context.getRequestDispatcher("/Broker/BrokerFirmAppNotesAdd.jsp").forward(request, response);
    }
    
    private void saveDetails(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = BrokerFirmMainMapping.validateNotes(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (saveDetails(request)) {
                
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving.\"";
            }
        } else {
            additionalData = null;
        }
        if ("OK".equalsIgnoreCase(status)) {
            getNotesList(request, response, context);
        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }
         
        
    }
    
    private boolean saveDetails(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = (NeoUser)request.getSession().getAttribute("persist_user");
        try {
            EntityNotes app = BrokerFirmMainMapping.getBrokerFirmAppNotes(request, neoUser);
            System.out.println("getEntityId " + app.getEntityId());
            System.out.println("getNoteDetails " + app.getNoteDetails());
            System.out.println("getNoteType " + app.getNoteType());
            app.setNoteDetails(request.getParameter("noteDetails"));
            port.saveEmployerAppNotes(app);
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
    }
    
    private void getNotesList(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
            int employerEntityId = getIntParam(request, "brokerFirmEntityId");
            int noteNum = getIntParam(request, "notesListSelect");
            request.setAttribute("notesListSelect", request.getParameter("notesListSelect"));
            if (noteNum == 99) {
                
            } else {
                List<KeyValueArray> kva = port.fetchNotesList(215,employerEntityId, noteNum,6);
                Object col = MapUtils.getMap(kva);
                request.setAttribute("BrokerFirmNoteDetails", col);
                
            }
            context.getRequestDispatcher("/Broker/BrokerFirmNotesDetails.jsp").forward(request, response);
    }
       
    private int getIntParam(HttpServletRequest request, String paramName) {
        try {
            String maxDepStr = request.getParameter(paramName);
            return Integer.parseInt(maxDepStr);
        } catch (java.lang.Exception e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }
    
    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser)request.getSession().getAttribute("persist_user");
    }
    
    private int getStrToInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (java.lang.Exception e) {
            System.out.println(s + " = " + e.getMessage());
        }
        return 0;
    }
    

    @Override
    public String getName() {
        return "BrokerFirmNotesCommand";
    }
    
    
 
}

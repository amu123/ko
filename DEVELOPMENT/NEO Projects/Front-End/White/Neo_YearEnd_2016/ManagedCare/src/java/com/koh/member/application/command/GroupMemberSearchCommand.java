/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class GroupMemberSearchCommand extends NeoCommand {


    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            search(request, response, context);
        } catch (Exception ex) {
            Logger.getLogger(GroupMemberSearchCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void search(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        HttpSession session = request.getSession();
        String code = request.getParameter("groupCode");
        String name = request.getParameter("groupName");
        int productId = (Integer)session.getAttribute("productId");
        List<KeyValueArray> kva = port.getEmployerList(name, code, productId);
        Object col = MapUtils.getMap(kva);;

        request.setAttribute("GroupSearchResults", col);
        context.getRequestDispatcher("/Member/MemberGroupSearchResults.jsp").forward(request, response);
    }

    @Override
    public String getName() {
        return "GroupMemberSearchCommand";
    }
}
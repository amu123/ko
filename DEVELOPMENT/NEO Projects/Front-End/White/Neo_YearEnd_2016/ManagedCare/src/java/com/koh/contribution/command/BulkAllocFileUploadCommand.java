/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.contribution.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.KeyValue;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;
import neo.manager.Security;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

/**
 *
 * @author yuganp
 */
public class BulkAllocFileUploadCommand extends NeoCommand {
    
    private String exportString = "";

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (isMultipart) {
          getFiles(request, response, context);
        } else {
            System.out.println("Not a file");
        }
        
        String command = request.getParameter("command");
        if(command.equalsIgnoreCase("Export")){
            try {
                exportToCSV(request, response, "BulkEmployerPaymentAllocation");
            } catch (IOException ex) {
                System.out.println("Error Exporting File = " + ex.getMessage());
                ex.printStackTrace();
            }
        }
        
        return null;
    }
    
    private void getFiles(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            ServletFileUpload upload = new ServletFileUpload();
            FileItemIterator iter = upload.getItemIterator(request);
            String fileDetails = "";
            String fileName = "";
            Map<String, String> paramsMap = new HashMap<String, String>();
            while (iter.hasNext()) {
                FileItemStream item = iter.next();
                String name = item.getFieldName();
                InputStream stream = item.openStream();
                if (item.isFormField()) {
                    paramsMap.put(name, Streams.asString(stream));
                    System.out.println("Form field " + name + " with value "
                        + paramsMap.get(name) + " detected.");
                } else {
                    fileDetails = Streams.asString(stream);
                    fileName = item.getName();
                    System.out.println("File field " + name + " with file name "
                        + item.getName() + " detected.");
                System.out.println(fileDetails);
                }
            }
            Map<String, String> errMap = new LinkedHashMap<String, String>();
            List<Map<String, String>> fileList = new ArrayList<Map<String, String>>();
            BigDecimal fileTotal = new BigDecimal(0);
            if (fileName.isEmpty() || fileDetails.isEmpty()) {
                errMap.put(" ", "No file selected");
            } else {
               fileTotal = processFile(fileDetails, errMap, fileList);
            }
            request.setAttribute("amount", paramsMap.get("amountUnallocated"));
            request.setAttribute("groupName", paramsMap.get("groupName"));
            request.setAttribute("groupNumber", paramsMap.get("groupNumber"));
            request.setAttribute("entityId", paramsMap.get("entityId"));
            
            if (!errMap.isEmpty()) {
                request.setAttribute("errMap", errMap);
                request.setAttribute("importResults", "Errors");
            } else {
                BigDecimal unallocatedAmount = new BigDecimal(paramsMap.get("amountUnallocated"));
                if (fileTotal.compareTo(unallocatedAmount) == 1) {
                    errMap.put("0", "File Total: " + fileTotal.toPlainString() + " is greater than unallocated amount: " + unallocatedAmount.toPlainString());
                    request.setAttribute("importResults", "Errors");
                    request.setAttribute("errMap", errMap);
                } else {
                    NeoManagerBean port = service.getNeoManagerBeanPort();
                    String groupNumber = paramsMap.get("groupNumber");
                    int entity_id = new Integer(paramsMap.get("entityId"));
                    List<KeyValueArray> keyValueArray = MapUtils.getKeyValueArray(fileList);
                    Security sec = TabUtils.getSecurity(request);
                    String dateStr = paramsMap.get("Contribution_Date");
                    Date dt = DateTimeUtils.convertFromYYYYMMDD(dateStr);
                    XMLGregorianCalendar xmlDt = DateTimeUtils.convertDateToXMLGregorianCalendar(dt);
                    Integer receiptId = null;
                    String receiptStr = paramsMap.get("ContribReceiptsId");
                    if (receiptStr != null && !receiptStr.isEmpty()) {
                        String[] sArr = receiptStr.split("_");
                        if (sArr != null && sArr.length == 2) {
                            try {
                                receiptId = Integer.parseInt(sArr[0]);
                            } catch (Exception e) {
                            }
                        }
                    }
                    List<KeyValueArray> bea = port.saveBulkEmployerAllocation(fileName, 4, groupNumber, entity_id, keyValueArray, xmlDt, receiptId, sec);
                    List<Map<String, String>> map = MapUtils.getMap(bea);
                    errMap.put("0", "File Total: " + fileTotal.toPlainString() + " is leass than or equeal to unallocated amount: " + unallocatedAmount.toPlainString());
                    request.setAttribute("resultList", map);
                    mapExportData(bea);
                }
                
            }
            context.getRequestDispatcher("/Contribution/EmployerBulkPaymentAllocation.jsp").forward(request, response);
            
        } catch (ServletException ex) {
            Logger.getLogger(BulkAllocFileUploadCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileUploadException ex) {
            Logger.getLogger(BulkAllocFileUploadCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BulkAllocFileUploadCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private BigDecimal processFile(String fileDetails, Map<String, String> errMap, List<Map<String, String>> fileList) {
        BigDecimal total = new BigDecimal(0);
        try {
            exportString = "Line,Member Number,ID Number,Employee Number,Amount,Status\n";
            String[] lines =  fileDetails.split("\\r?\\n");
            int i = 1;
            for (String line : lines) {
                if (line != null && !line.isEmpty()) {
                    String[] sArr = line.split(",");
                    if (sArr.length == 4) {
                        if ("Member Number".equalsIgnoreCase(sArr[0])) {
                            i++;
                            continue;
                        }
                        String memNum = sArr[0];
                        String idNum = sArr[1];
                        String employeeNumber = sArr[2];
                        String sAmount = sArr[3];
                        BigDecimal amount = null;
                        try {
                            amount = new BigDecimal(sAmount.replace(" ", ""));
                        } catch (Exception ex) {
                            
                        }
                        if (amount == null) {
                            errMap.put(Integer.toString(i),"Invalid amount: " + line);
                            i++;
                            continue;
                        }
//                        if (memNum.isEmpty() && idNum.isEmpty() && employeeNumber.isEmpty()) {
                        if (memNum.isEmpty() && idNum.isEmpty()) {
//                            errMap.put(Integer.toString(i),"At least one of (Member Number/ ID Number/ Employee Number) is required: " + line);
                            errMap.put(Integer.toString(i),"Member Number or ID Number is required: " + line);
                        }
                        total = total.add(amount);
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("line", Integer.toString(i));
                        map.put("memNum", memNum == null ? "" : memNum.replaceAll(" ", ""));
                        map.put("idNum", idNum == null ? "" : idNum.replaceAll(" ", ""));
                        map.put("employeeNumber", employeeNumber);
                        map.put("amount", sAmount == null ? "0.00" : sAmount.replace(" ", ""));
                        fileList.add(map);
                    } else {
                        errMap.put(Integer.toString(i),(sArr.length < 4 ? "Too few items: " : "Too many items: ") +  line);
                    }
                }
                i++;
            }
        } catch (Exception e) {
            
        }
        return total;
    }
    
    private void mapExportData(List<KeyValueArray> kva){
        String linenmr = "";
        String memNum = "";
        String idNum = "";
        String employeeNumber = "";
        String amount = "";
        String status = "";
        for (KeyValueArray kv : kva) {
            linenmr = "";
            memNum = "";
            idNum = "";
            employeeNumber = "";
            status = "";
            amount = "";
            for (KeyValue k : kv.getKeyValList()) {
                System.out.println("key : " + k.getKey() + " - Value = " + k.getValue());
                if(k.getKey().equals("line")){
                   linenmr = k.getValue();
                }
                if(k.getKey().equals("memNum")){
                   memNum = "," + k.getValue();
                }
                if(k.getKey().equals("idNum")){
                   idNum = "," + k.getValue();
                }
                if(k.getKey().equals("employeeNumber")){
                   employeeNumber = "," + k.getValue();
                }
                if(k.getKey().equals("amount")){
                   amount = "," + k.getValue();
                }
                if(k.getKey().equals("status")){
                   status =  k.getValue();
                   status = (status.equalsIgnoreCase("5")) ? "Allocated" : "Not Allocated";
                   status = "," + status;
                }
            }
            exportString += linenmr + memNum + idNum + employeeNumber + amount + status + "\n";
        }
    }
    
    private void exportToCSV(HttpServletRequest request, HttpServletResponse response, String fileName) throws IOException {
        try {
            PrintWriter out = null;
            File file = new File("C:/temp/" + fileName + ".csv");
            if (!file.exists()) {
                file.createNewFile();
            }
            Writer writer = new BufferedWriter(new FileWriter(file));
            writer.write(exportString);
            writer.close();

            InputStream in = new FileInputStream(file);

            int contentLength = (int) file.length();
            System.out.println("The content length is " + contentLength);
//            out = response.getWriter();
//            out.print(file.toURI().toURL());
//            response.reset();

            ByteArrayOutputStream temporaryOutput;
            if (contentLength != -1) {
                temporaryOutput = new ByteArrayOutputStream(contentLength);
            } else {
                temporaryOutput = new ByteArrayOutputStream(20480);
            }

            byte[] bufer = new byte[512];
            while (true) {

                int len = in.read(bufer);
                if (len == -1) {

                    break;
                }
                temporaryOutput.write(bufer, 0, len);
            }
            in.close();
            byte[] bytes = temporaryOutput.toByteArray();
            printPreview(request, response, bytes, fileName + ".csv");
            //openCSVFile(file);
        } catch (Exception e) {
            //Unable to create writer
            System.err.println("Unable to create a writer");
        }
    }

    private void printPreview(HttpServletRequest request, HttpServletResponse response, byte[] array, String fileName) throws IOException {
        OutputStream outStream = null;
        try {
            System.out.println("fileName = " + fileName);
            response.setHeader("Pragma", "no-cache"); //HTTP 1.0
            response.setDateHeader("Expires", 0); //prevents caching at the proxy server
            response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            response.setHeader("Content-Type", "text/csv");
            response.setHeader("Content-Transfer-Encoding", "binary");

            //response.setContentType("text/csv");
            response.setContentLength(array.length);
            outStream = response.getOutputStream();
            outStream.write(array);
            outStream.flush();
            outStream.close();

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return "BulkAllocFileUploadCommand";
    }
}

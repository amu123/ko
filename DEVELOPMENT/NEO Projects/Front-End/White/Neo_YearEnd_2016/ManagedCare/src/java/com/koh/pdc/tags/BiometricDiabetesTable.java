/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.DiabetesBiometrics;

/**
 *
 * @author princes
 */
public class BiometricDiabetesTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String javascript;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        List<DiabetesBiometrics> getDiabetes = (List<DiabetesBiometrics>) session.getAttribute("getDiabetes");
        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        String dateMeasured;
        try {

            out.print("<tr>");

            out.print("<th scope=\"col\">Date Measured</th>");
            out.print("<th scope=\"col\">Height</th>");
            out.print("<th scope=\"col\">Weight</th>");
            out.print("<th scope=\"col\">BMI</th>");
            out.print("<th scope=\"col\">Blood Pressure Systolic</th>");
            out.print("<th scope=\"col\">Blood Pressure Diastolic</th>");
            out.print("<th scope=\"col\">HBA1C(%)</th>");
            out.print("<th scope=\"col\">Random Glucose</th>");
            out.print("<th scope=\"col\">Fasting Glucose</th>");
            out.print("<th scope=\"col\">Total Cholesterol</th>");
            out.print("<th scope=\"col\">LDLC mmol/l</th>");
            out.print("<th scope=\"col\">HDLC mmol/l</th>");
            out.print("<th scope=\"col\">Tryglyceride mmol/l</th>");
            out.print("<th scope=\"col\">Protein in urine mg/dL</th>");
            out.print("<th scope=\"col\">TC: HDL ratio</th>");
            out.print("<th scope=\"col\">Select</th>");

//            out.print("<th scope=\"col\">Date Measured</th>");
//            out.print("<th scope=\"col\">ICD10</th>");
//            out.print("<th scope=\"col\">Excercise</th>");            
//            out.print("<th scope=\"col\">Ex Smoker Stopped</th>");
//            out.print("<th scope=\"col\">Alchohol Units</th>");
//            out.print("<th scope=\"col\">HbAIC%</th>");
//            out.print("<th scope=\"col\">Random Glucose mmol/l</th>");
//            out.print("<th scope=\"col\">Fasting Glucose mmol/l</th>");
//            out.print("<th scope=\"col\">Total Cholestrol</th>");
//            out.print("<th scope=\"col\">LDLC mmol/l</th>");
//            out.print("<th scope=\"col\">HDLC mmol/l</th>");
//            out.print("<th scope=\"col\">Tryglyser ide mmol/l</th>");
//            out.print("<th scope=\"col\">Proteinuria g/l</th>");
//            out.print("<th scope=\"col\">BMI</th>");
//            out.print("<th scope=\"col\">TC:HDL Ratio</th>");
            out.print("</tr>");

            if (getDiabetes != null && getDiabetes.size() > 0) {
                for (DiabetesBiometrics d : getDiabetes) {

                    out.print("<tr>");
                    if (d.getDateMeasured() != null) {
                        Date mDate = d.getDateMeasured().toGregorianCalendar().getTime();
                        dateMeasured = formatter.format(mDate);
                        out.print("<td><center><label class=\"label\">" + dateMeasured + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getHeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + d.getHeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getWeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + d.getWeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getBmi() != 0) {
                        out.print("<td><center><label class=\"label\">" + d.getBmi() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getBloodPressureSystolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + d.getBloodPressureSystolic() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getBloodPressureDiastolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + d.getBloodPressureDiastolic() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getHbAIC() != 0) {
                        out.print("<td><center><label class=\"label\">" + d.getHbAIC() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getRandomGlucose() != 0) {
                        out.print("<td><center><label class=\"label\">" + d.getRandomGlucose() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getFastingGlucose() != 0) {
                        out.print("<td><center><label class=\"label\">" + d.getFastingGlucose() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getTotalCholesterol() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + d.getTotalCholesterol() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getLdlcMol() != 0) {
                        out.print("<td><center><label class=\"label\">" + d.getLdlcMol() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getHdlcMol() != 0) {
                        out.print("<td><center><label class=\"label\">" + d.getHdlcMol() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getTryglyseride() != 0) {
                        out.print("<td><center><label class=\"label\">" + d.getTryglyseride() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getProteinurianGL() != 0) {
                        out.print("<td><center><label class=\"label\">" + d.getProteinurianGL() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (d.getTchdlRatio() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + d.getTchdlRatio() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("<td><center><button name=\"opperation\" type=\"button\" " + javascript + " value=\"" + d.getBiometricsId() + "\">Select</button></center></td>");
//
//                    if (d.getDateMeasured() != null) {
//                        Date mDate = d.getDateMeasured().toGregorianCalendar().getTime();
//                        dateMeasured = formatter.format(mDate);
//                        out.print("<td><label class=\"label\">" + dateMeasured + "</label></td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (d.getIcd10Lookup() != null) {
//                        out.print("<td>" + d.getIcd10Lookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getExercisePerWeek() != 0) {
//                        out.print("<td>" + d.getExercisePerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }                    
//
//                    if (d.getYearsSinceStopped() > 0) {
//                        out.print("<td>" + d.getYearsSinceStopped() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getAlcoholUnitsPerWeek() != 0) {
//                        out.print("<td>" + d.getAlcoholUnitsPerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getHbAIC() != 0) {
//                        out.print("<td>" + d.getHbAIC() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getRandomGlucose() != 0) {
//                        out.print("<td>" + d.getRandomGlucose() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getFastingGlucose() != 0) {
//                        out.print("<td>" + d.getFastingGlucose() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getTotalCholesterol() != 0.0) {
//                        out.print("<td>" + d.getTotalCholesterol() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getLdlcMol() != 0) {
//                        out.print("<td>" + d.getLdlcMol() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getHdlcMol() != 0) {
//                        out.print("<td>" + d.getHdlcMol() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getTryglyseride() != 0) {
//                        out.print("<td>" + d.getTryglyseride() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getProteinurianGL() != 0) {
//                        out.print("<td>" + d.getProteinurianGL() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getBmi() != 0) {
//                        out.print("<td>" + d.getBmi() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (d.getTchdlRatio() != 0.0) {
//                        out.print("<td>" + d.getTchdlRatio() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
                    out.print("</tr>");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

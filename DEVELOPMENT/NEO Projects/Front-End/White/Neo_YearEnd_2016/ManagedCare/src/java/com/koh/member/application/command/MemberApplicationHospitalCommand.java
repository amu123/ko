/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.member.application.MemberAppCommon;
import com.koh.member.application.MemberAppDepMapping;
import com.koh.member.application.MemberAppHospitalDetailsMapping;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class MemberApplicationHospitalCommand extends NeoCommand{
 
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        try {
            String s = request.getParameter("buttonPressed");
            if (s != null && !s.isEmpty()) {
                System.out.println("buttonPressed : " + s);
                if (s.equalsIgnoreCase("Add Details")) {
                    addDetails(port,request, response, context);
                } else if (s.equalsIgnoreCase("SaveButton")) {
                    saveDetails(port,request, response, context);
                } else if (s.equalsIgnoreCase("EditButton")) {
                    editDetails(port,request, response, context);
                } else if (s.equalsIgnoreCase("RemoveButton")) {
                    removeDetails(port,request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(MemberApplicationHospitalCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void addDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        int appNum = getStrToInt(request.getParameter("memberAppNumber"));
        request.setAttribute("memberAppHospId", null);
        request.setAttribute("DependentList", MemberAppCommon.getDependantListMap(service, appNum, request.getParameter("memberAppType")));
        context.getRequestDispatcher("/MemberApplication/MemberAppHospitalDetails.jsp").forward(request, response);
    }
    
    private void saveDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = MemberAppHospitalDetailsMapping.validate(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (saveDetails(port,request)) {
                
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving.\"";
            }
        } else {
            additionalData = null;
        }
        if ("OK".equalsIgnoreCase(status)) {
            context.getRequestDispatcher(new MemberAppTabContentsCommand().getMemberAppHospital(port,request)).forward(request, response);
        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }
         
        
    }
    
    private boolean saveDetails(NeoManagerBean port, HttpServletRequest request) {
        NeoUser neoUser = (NeoUser)request.getSession().getAttribute("persist_user");
        try {
            MemAppHospDetail app = MemberAppHospitalDetailsMapping.getMemAppHospDetail(request, neoUser);
            port.saveMemAppHospDetail(app);
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
    }
    
    private void editDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        int appNum = getStrToInt(request.getParameter("memberAppNumber"));
        int genId = getStrToInt(request.getParameter("memberAppHospId"));
        List<MemAppHospDetail> memAppHospDetailByAppNum = port.fetchMemAppHospDetailByAppNum(appNum);
        MemAppHospDetail hospDetail = null;
        for (MemAppHospDetail hd : memAppHospDetailByAppNum) {
            if (hd.getHospDetailId() == genId) {
                hospDetail = hd;
            }
        }
        request.setAttribute("DependentList", MemberAppCommon.getDependantListMap(service, appNum, request.getParameter("memberAppType")));
        MemberAppHospitalDetailsMapping.setMemAppHospDetail(request, hospDetail);
        context.getRequestDispatcher("/MemberApplication/MemberAppHospitalDetails.jsp").forward(request, response);
    }
    
    private void removeDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        int appNum = getStrToInt(request.getParameter("memberAppNumber"));
        int hospId = getStrToInt(request.getParameter("memberAppHospId"));
        port.removeMemberApplicationHospDetail(hospId);
        context.getRequestDispatcher(new MemberAppTabContentsCommand().getMemberAppHospital(port,request)).forward(request, response);
    }
    
    private int getStrToInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (java.lang.Exception e) {
            System.out.println(s + " = " + e.getMessage());
        }
        return 0;
    }
    
    @Override
    public String getName() {
        return "MemberApplicationHospitalCommand";
    }

   
}

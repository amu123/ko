/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.automation.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.ClaimSearchCriteria;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.PaymentRun;
import neo.manager.PaymentRunDetails;
import neo.manager.Product;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author janf
 */
public class PaymentRunCommand extends NeoCommand {

    private static PaymentRun paymentrun = null;
    private static double TOTAL_AMOUNT = 0.0d;
    private String exportString = "";

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String command = request.getParameter("command");
        Date today = new Date(System.currentTimeMillis());
        PrintWriter out = null;
        Product prod = null;

        String totalLines = "";

        System.out.println("Operation: PaymentRunCommand - Command: " + command);
        if ("paymentRun".equalsIgnoreCase(command)) {
            String processFrom = request.getParameter("processFrom");
            String processTo = request.getParameter("processTo");
            int productId = Integer.parseInt(request.getParameter("productId"));
            System.out.println("processFrom = " + processFrom);
            System.out.println("processTo = " + processTo);
            System.out.println("productId = " + productId);

            ClaimSearchCriteria claimSearch = new ClaimSearchCriteria();

            if (productId != 0) {
                prod = port.findProductWithID(productId);
                claimSearch.setProductId(prod.getProductId());
            }

            if (processFrom != null && !processFrom.equalsIgnoreCase("")) {
                try {
                    Date date = new SimpleDateFormat("yyyy/MM/dd").parse(processFrom);
                    XMLGregorianCalendar xProcessFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(date);
                    claimSearch.setProcessDateFrom(xProcessFrom);
                } catch (ParseException ex) {
                    System.out.println("ParseExeption processFrom = " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
            if (processTo != null && !processTo.equalsIgnoreCase("")) {
                try {
                    Date date = new SimpleDateFormat("yyyy/MM/dd").parse(processTo);
                    XMLGregorianCalendar xProcessTo = DateTimeUtils.convertDateToXMLGregorianCalendar(date);
                    claimSearch.setProcessDateTo(xProcessTo);
                } catch (ParseException ex) {
                    System.out.println("ParseExeption ProcessTo = " + ex.getMessage());
                    ex.printStackTrace();
                }
            }

            claimSearch.setFinanceClaims(true); // check if claim has been processed
            claimSearch.setForPaymentRun(true);

            java.text.DecimalFormat desFormat = new java.text.DecimalFormat("###.########");
            Double claimedAmount = 0.0d;
            Double totClaimedAmount = 0.0d;
            try {

                neo.manager.PaymentRun payRunResult = port.performPaymentRun(prod, claimSearch);

                paymentrun = payRunResult;
                TOTAL_AMOUNT = 0.0d;
                if (payRunResult != null) {
                    List<PaymentRunDetails> paymentDetails = payRunResult.getPaymentsDetails();
                    //System.out.println("Payment result count  : " + paymentDetails.size());
                    totalLines = "" + paymentDetails.size();
                    exportString = "Name Surname, Entity Id,Entity Type,Amount,Date\n";

                    for (PaymentRunDetails paymentClaim : paymentDetails) {

                        try {
                            claimedAmount = desFormat.parse(desFormat.format(paymentClaim.getAmount())).doubleValue();
                        } catch (Exception e) {
                            System.out.println("Error finance =" + e.getMessage());
                        }

                        TOTAL_AMOUNT += paymentClaim.getAmount();
                        if(paymentClaim.getEntityName() != null){
                            exportString +=  paymentClaim.getEntityName()+ " " + paymentClaim.getEntitySurname() + ',' + paymentClaim.getEntityId() + ',' + paymentClaim.getEntityType() + ',' + paymentClaim.getAmount() + ',' + paymentClaim.getPaymentRunDate() + '\n';
                        } else {
                            exportString +=  paymentClaim.getEntitySurname() + ',' + paymentClaim.getEntityId() + ',' + paymentClaim.getEntityType() + ',' + paymentClaim.getAmount() + ',' + paymentClaim.getPaymentRunDate() + '\n';
                        }
                    }
                    session.setAttribute("PaymentRunSummary", paymentDetails);
                }
            } catch (Exception e) {
                System.out.println("Payment Run Exception = " + e.getMessage());
                e.printStackTrace();
            }

            try {
                out = response.getWriter();
                String result = String.format("%.2f", TOTAL_AMOUNT);
                out.println("DONE|" + totalLines + "|" + result);
            } catch (IOException ex) {
                System.out.println("Exception = " + ex.getMessage());
                ex.printStackTrace();
            }
        } else if ("approveRun".equalsIgnoreCase(command)) {
            PaymentRun approvePayRun = paymentrun;
            NeoUser user = (NeoUser) session.getAttribute("persist_user");
            approvePayRun.setCreatedBy(user.getUserId());

            if (TOTAL_AMOUNT != 0) {
                try {
                    port.approvePayments(approvePayRun);
                    out = response.getWriter();
                    out.println("DONE|Payment Run was succesful approved");
                } catch (Exception e) {
                    System.out.println("Error with approving payment run: " + e.getMessage());
                    out.println("ERROR|");
                    e.printStackTrace();
                }
            } else {
                try {
                    out = response.getWriter();
                    out.println("ERROR|Total Amount is zero");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

        } else if ("exportRun".equalsIgnoreCase(command)) {
            try {
                exportToCSV(request,response);
                //out = response.getWriter();
                //out.println("done");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if ("populateRunSummary".equalsIgnoreCase(command)) {
            request.setAttribute("PaymentRunSummary", session.getAttribute("PaymentRunSummary"));
            try {
                RequestDispatcher dispatcher = context.getRequestDispatcher("/Automation/PaymentRun_DetailSummary.jsp");
                dispatcher.forward(request, response);
            } catch (Exception ex) {
                System.out.println("Dispatcher Exeption = " + ex.getMessage());
                ex.printStackTrace();
            } 
        }

        return null;
    }
    
    private void exportToCSV(HttpServletRequest request, HttpServletResponse response) throws IOException{
        try{
            PrintWriter out = null;
            File file = new File("C:/temp/exportPayRun.csv");
            if(!file.exists()){
                file.createNewFile();
            }
            Writer writer = new BufferedWriter(new FileWriter(file));
            writer.write(exportString);
            writer.close();
            
            InputStream in = new FileInputStream(file);

            int contentLength = (int) file.length();
            System.out.println("The content length is " + contentLength);
//            out = response.getWriter();
//            out.print(file.toURI().toURL());
//            response.reset();

            ByteArrayOutputStream temporaryOutput;
            if (contentLength != -1) {
                temporaryOutput = new ByteArrayOutputStream(contentLength);
            } else {
                temporaryOutput = new ByteArrayOutputStream(20480);
            }

            byte[] bufer = new byte[512];
            while (true) {

                int len = in.read(bufer);
                if (len == -1) {

                    break;
                }
                temporaryOutput.write(bufer, 0, len);
            }
            in.close();
            byte[] bytes = temporaryOutput.toByteArray();
            printPreview(request, response, bytes, "exportPayRun.csv");
            //openCSVFile(file);
        }catch(Exception e){
            //Unable to create writer
            System.err.println("Unable to create a writer");
        }
    }

    public void openCSVFile(File file) throws IOException {

        Runtime run = Runtime.getRuntime();

        String localOS = System.getProperty("os.name").toLowerCase();
        boolean mac = localOS.startsWith("mac os x");
        if (mac) {

            run.exec("open " + file);
        } else {

            run.exec("cmd.exe /c start " + file);
        }
    }
    
    private void printPreview(HttpServletRequest request, HttpServletResponse response, byte[] array, String fileName) throws IOException {
        OutputStream outStream = null;
        try {
            System.out.println("fileName = " + fileName);
            response.setHeader("Pragma", "no-cache"); //HTTP 1.0
            response.setDateHeader("Expires", 0); //prevents caching at the proxy server
            response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            response.setHeader("Content-Type", "text/csv");
            response.setHeader("Content-Transfer-Encoding", "binary");

            //response.setContentType("text/csv");
            response.setContentLength(array.length);
            outStream = response.getOutputStream();
            outStream.write(array);
            outStream.flush();
            outStream.close();

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return "PaymentRunCommand";
    }
}

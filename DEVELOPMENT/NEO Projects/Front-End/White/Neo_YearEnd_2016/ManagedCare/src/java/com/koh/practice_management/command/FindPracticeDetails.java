/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.practice_management.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.PracticeNetworkDetails;
import neo.manager.ProviderSearchDetails;

/**
 *
 * @author almaries
 */
public class FindPracticeDetails extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;

        String providerNumber = request.getParameter("provNum");
        String element = request.getParameter("element");
        if (providerNumber == null || providerNumber.equalsIgnoreCase("")
                || providerNumber.equalsIgnoreCase("null")) {
            providerNumber = "" + request.getAttribute("provNum");
            element = "" + request.getAttribute("element");
        }

        try {
            out = response.getWriter();
            if (providerNumber != null && !providerNumber.trim().equalsIgnoreCase("null")) {
                ProviderSearchDetails pd = getPSDByNumber(providerNumber, element, session);

                if (pd.getProviderNo() != null && !pd.getProviderNo().trim().equals("")) {
                    String provDescId = pd.getDisciplineType().getId();
                    String provDesc = pd.getDisciplineType().getValue();
                    String providerDesc = provDescId + " - " + provDesc;
                    String providerName = pd.getProviderName();

                    setNetwork(providerNumber, element, session);


                    //String[] provNameSur = providerName.split("\\-");
                    //String provName = provNameSur[0];
                    //String provSurname = provNameSur[1];

                    String responseText = "Done|ProviderName=" + providerName + "|ProviderDiscipline=" + providerDesc + "$";
                    out.println(responseText);
                } else {
                    out.println("Error|No such practice|" + getName());
                }

            } else {
                out.println("Error|No such practice|" + getName());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "FindPracticeDetails";
    }

    public ProviderSearchDetails getPSDByNumber(String providerNumber, String element, HttpSession session) {
        ProviderSearchDetails pd = service.getNeoManagerBeanPort().findPracticeAjaxDetailsWithEmail(providerNumber);
        if (pd.getProviderNo() != null && !pd.getProviderNo().trim().equals("")) {
            session.setAttribute(element, providerNumber);
            int entityID = pd.getEntityID();
            
            System.out.println("getPSDByNumber entity : " + entityID);
            
            String providerName = pd.getProviderName();
            String provDescId = pd.getDisciplineType().getId();
            String provDesc = pd.getDisciplineType().getValue();
            session.setAttribute(element + "EntityId", entityID);
            session.setAttribute(element + "Name", providerName);
            session.setAttribute(element + "Surname", providerName);
            session.setAttribute(element + "DiscType", provDesc);
            session.setAttribute(element + "DiscTypeId", provDescId);
        }
        return pd;
    }

    private void setNetwork(String provNum, String element, HttpSession session) {
        //get network
        PracticeNetworkDetails pnd = service.getNeoManagerBeanPort().getPracticeNetworkByNumberForToday(provNum);
        String networkName = "None";
        if (pnd != null) {
            networkName = pnd.getNetworkDescription();
        }
        session.setAttribute(element + "Network", networkName);
    }
}

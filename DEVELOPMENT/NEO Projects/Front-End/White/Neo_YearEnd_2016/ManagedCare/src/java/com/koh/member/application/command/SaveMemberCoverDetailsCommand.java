/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import com.koh.cover.MemberMainMapping;
import com.koh.cover.commands.MemberMaintenanceTabContentCommand;
import com.koh.employer.EmployerGroupMapping;
import com.koh.employer.command.TabUtils;
import com.koh.fe.command.LoginCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;
import org.apache.commons.lang.time.DateUtils;

/**
 *
 * @author princes
 */
public class SaveMemberCoverDetailsCommand extends NeoCommand {

//    NeoManagerBean port = service.getNeoManagerBeanPort();
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        String s = request.getParameter("buttonPressed");
        System.out.println("buttonPressed " + s);
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        
        List<neo.manager.LookupValue> lookUpsClient = service.getNeoManagerBeanPort().findCodeTableForLookupType(341);
        if (lookUpsClient != null) {
            neo.manager.LookupValue val = lookUpsClient.get(0);
            if (val != null) {
                System.out.println("Client : " + val.getValue());
                session.setAttribute("Client", val.getValue());
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("AddOptionButton")) {
            } else if (s.equalsIgnoreCase("LinkBrokerAndConsultantButton")) {
                try {
                    linkMemberToBroker(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (s.equalsIgnoreCase("SearchGroupButton")) {

                try {
                    searchGroup(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else if (s.equalsIgnoreCase("SearchBrokerButton")) {

                try {
                    searchBroker(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (s.equalsIgnoreCase("SearchConsultantButton")) {

                try {
                    searchConsultant(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (s.equalsIgnoreCase("SearchSwapGroupButton")) {

                try {
                    searchSwapGroup(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else if (s.equalsIgnoreCase("SearchSwapBrokerButton")) {

                try {
                    searchSwapBroker(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("SaveDepSwapButton")) {

                try {
                    saveDepSwap(request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("LinkGroupButton")) {

                try {
                    linkMemberToGroup(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("SaveBrokerMemberButton")) {

                try {
                    saveBroker(request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("SaveConsultantMemberButton")) {

                try {
                    saveConsultant(request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("SaveGroupMemberButton")) {

                try {
                    saveGroup(request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("SuspendMemberButton")) {

                try {
                    viewMemberSuspesion(port, request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("SaveMemberSuspensionButton")) {

                try {
                    saveSuspension(request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("LiftSuspensionButton")) {

                try {
                    liftSuspension(request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("ReinstateMemberButton")) {

                try {
                    viewReinstateMember(port, request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("SaveReinstateMemberButton")) {

                try {
                    reInstateMember(request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("ResignMemberButton")) {

                try {
                    viewResignMember(port, request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("DepSwapButton")) {

                try {
                    viewDepSwap(port, request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("SaveResignMemberButton")) {

                try {
                    resignMember(request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("ChangeOptionButton")) {

                try {
                    viewChangeOption(request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("SaveChangeOptionButton")) {

                try {
                    saveChangeOption(request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (s.equalsIgnoreCase("ChangeInceptionButton")) {

                try {
                    viewViewInceptionDate(port, request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (s.equalsIgnoreCase("SaveInceptionButton")) {

                try {
                    saveInceptionDate(port, request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (s.equalsIgnoreCase("CheckClaimsAndAuthsAfterDate")) {

                try {
                    CheckClaimsAndAuthsAfterDate(port, request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (s.equalsIgnoreCase("CheckMemTypeAndGroupType")) {

                try {
                    CheckMemTypeAndGroupType(port, request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("CheckBrokerConcession")) {

                try {
                    CheckBrokerConcession(port, request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("CheckGroupConcession")) {

                try {
                    CheckGroupConcession(port, request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("SaveBenefitPayoutType")) {

                try {
                    validateBenefitPayoutType(port, request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
             if (s.equalsIgnoreCase("LinkMembertoUnionButton")) {

                try {
                    LinkMembertoUnion(port, request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (s.equalsIgnoreCase("TerminateButtonPressed")) {

                try {
                    terminateBroker(port, request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s.equalsIgnoreCase("terminateBroker")) {
                try {
                    terminateBrokerAndConsultant(port, request, response, context);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberCoverDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        // String result = validateAndSave(port, request);
        try {
            PrintWriter out = response.getWriter();
            // out.println(result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SaveMemberCoverDetailsCommand";
    }

    private void searchBroker(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
//            request.setAttribute("MemAppSearchResults", col);
        System.out.println("searchBroker");
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        request.setAttribute("diagCodeId", "Disorder_diagCode_text");
        request.setAttribute("diagNameId", "Disorder_diagName");
        context.getRequestDispatcher("/Member/BrokerSearch.jsp").forward(request, response);

    }

    private void searchConsultant(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
//            request.setAttribute("MemAppSearchResults", col);
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        request.setAttribute("diagCodeId", "Disorder_diagCode2_text");
        request.setAttribute("diagNameId", "Disorder_diagName2");
        context.getRequestDispatcher("/Member/ConsultantSearch.jsp").forward(request, response);

    }

    private void searchGroup(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
//            request.setAttribute("MemAppSearchResults", col);
        System.out.println("searchGroup");
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        request.setAttribute("diagCodeId", "Disorder_diagCode_text");
        request.setAttribute("diagNameId", "Disorder_diagName");
        context.getRequestDispatcher("/Member/GroupSearch.jsp").forward(request, response);

    }

    private void searchSwapBroker(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        System.out.println("searchBroker");
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        request.setAttribute("diagCodeId", "swapBrokerCode_text");
        request.setAttribute("diagNameId", "swapBrokerName");
        context.getRequestDispatcher("/Member/BrokerSearch.jsp").forward(request, response);

    }

    private void searchSwapGroup(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        System.out.println("searchGroup");
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        request.setAttribute("diagCodeId", "swapGroupCode_text");
        request.setAttribute("diagNameId", "swapGroupName");
        context.getRequestDispatcher("/Member/GroupSearch.jsp").forward(request, response);

    }

    private void linkMemberToBroker(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        context.getRequestDispatcher("/Member/LinkMemberToBroker.jsp").forward(request, response);
    }
    
    private void terminateBroker(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        context.getRequestDispatcher("/Member/TerminateBroker.jsp").forward(request, response);
    }
    
    private void terminateBrokerAndConsultant(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }

        HttpSession session = request.getSession();
        String conBro = (String) session.getAttribute("ConBro");
        
        if (conBro != null && conBro.equalsIgnoreCase("Broker")) {
            Map<String, String> errors = MemberMainMapping.validateBrokerTermination(request);

            if (request.getParameter("memberEntityId") == null || request.getParameter("memberEntityId").isEmpty()) {
                errors.put("BrokerApp_EndDate_error", "Please search for a broker");
            }
            if (request.getParameter("BrokerApp_EndDate") == null || request.getParameter("BrokerApp_EndDate").isEmpty()) {
                errors.put("BrokerApp_EndDate_error", "Enter End Date");
            }

            String validationErros = TabUtils.convertMapToJSON(errors);
            String status = TabUtils.getStatus(errors);
            String additionalData = null;
            String updateFields = null;
            NeoUser neoUser = getNeoUser(request);

            int entityId = new Integer(request.getParameter("memberEntityId"));

            if ("OK".equalsIgnoreCase(status)) {
                if (saveBrokerTermination(request)) {
                } else {
                    status = "ERROR";
                    additionalData = "\"message\":\"There was an error saving member broker information.\"";
                }
            } else {
                additionalData = null;
            }

            if ("OK".equalsIgnoreCase(status)) {
                context.getRequestDispatcher(new MemberMaintenanceTabContentCommand().getMemberCoverDetails(request, entityId)).forward(request, response);

            } else {
                PrintWriter out = response.getWriter();
                out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
            }

        } else if (conBro != null && conBro.equalsIgnoreCase("Consultant")) {
            Map<String, String> errors = MemberMainMapping.validateBrokerConsultantTermination(request);

            if (request.getParameter("memberEntityId") == null || request.getParameter("memberEntityId").isEmpty()) {
                errors.put("BrokerApp_EndDate_error", "Please search for a consultant");
            }
            if (request.getParameter("BrokerApp_EndDate") == null || request.getParameter("BrokerApp_EndDate").isEmpty()) {
                errors.put("BrokerApp_EndDate_error", "Enter End Date");
            }

            String validationErros = TabUtils.convertMapToJSON(errors);
            String status = TabUtils.getStatus(errors);
            String additionalData = null;
            String updateFields = null;
            NeoUser neoUser = getNeoUser(request);
            
            int entityId = new Integer(request.getParameter("memberEntityId"));

            if ("OK".equalsIgnoreCase(status)) {
                if (saveConsultantTermination(request)) {
                } else {
                    status = "ERROR";
                    additionalData = "\"message\":\"There was an error saving member consultant information.\"";
                }
            } else {
                additionalData = null;
            }

            if ("OK".equalsIgnoreCase(status)) {
                context.getRequestDispatcher(new MemberMaintenanceTabContentCommand().getMemberCoverDetails(request, entityId)).forward(request, response);

            } else {
                PrintWriter out = response.getWriter();
                out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
            }
        }
    }

    private void linkMemberToGroup(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        context.getRequestDispatcher("/Member/LinkMemberToGroup.jsp").forward(request, response);

    }

    private void viewMemberSuspesion(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        context.getRequestDispatcher("/Member/SuspendMember.jsp").forward(request, response);

    }

    private void viewReinstateMember(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        context.getRequestDispatcher("/Member/ReInstateMember.jsp").forward(request, response);

    }

    private void viewResignMember(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        HttpSession session = request.getSession();
        List<LookupValue> lvReasons = port.getResignReasons();
        List<LookupValue> deseasedReasons = port.getDeseasedReasons();
        session.setAttribute("LookupList", lvReasons);  //For the Resign Reason Dropdown
        session.setAttribute("DeseasedLookupList", deseasedReasons);  //For the Deseased Reason Dropdown
        
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        context.getRequestDispatcher("/Member/ResignMember.jsp").forward(request, response);
    }

    private void viewChangeOption(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        HttpSession session = request.getSession();
        int productId = (Integer)session.getAttribute("productId");
        request.setAttribute("productId", ""+productId);
        
        context.getRequestDispatcher("/Member/ChangeMemberOption.jsp").forward(request, response);
    }

    private void viewViewInceptionDate(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        context.getRequestDispatcher("/Member/InceptionDate.jsp").forward(request, response);

    }

    private void viewDepSwap(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        HttpSession session = request.getSession();
        int entityId = (Integer) session.getAttribute("memberCoverEntityId");
        List<MemberEmployer> ba = port.findEmployerGroupsAndBrokerForMemberByMemberEntityId(entityId);
        boolean setBroker = false;
        boolean setGroup = false;
        int checkBroker = 0;
        for (MemberEmployer me : ba) {
            Date endDate = DateTimeUtils.getDateFromXMLGregorianCalendar(me.getEffectiveEndDate());
            if (endDate != null && endDate.after(new Date())) {
            }
            if (me.getDetailType().equalsIgnoreCase("Broker")) {
                if (me.getEffectiveEndDate().toString().substring(0, 4).equals("2999")) {
                    checkBroker++;
                    request.setAttribute("swapBrokerName", me.getName());
                    request.setAttribute("swapBrokerCode_text", me.getCode());
                    request.setAttribute("brokerEntityId", me.getEntityId());
                } else if (checkBroker == 0) {
                    request.setAttribute("swapBrokerName", me.getName());
                    request.setAttribute("swapBrokerCode_text", me.getCode());
                    request.setAttribute("brokerEntityId", me.getEntityId());
                }


                if (me.getDetailType().equalsIgnoreCase("Broker") && !setBroker) {
                    request.setAttribute("swapBrokerName", me.getName());
                    request.setAttribute("swapBrokerCode_text", me.getCode());
                    request.setAttribute("brokerEntityId", me.getEntityId());
                    setBroker = true;
                }
            }
            if (me.getDetailType().equalsIgnoreCase("Group") && !setGroup) {
                request.setAttribute("swapGroupName", me.getName());
                request.setAttribute("swapGroupCode_text", me.getCode());
                request.setAttribute("groupEntityId", me.getEntityId());
                setGroup = true;
            }
        }
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        context.getRequestDispatcher("/Member/DependantSwap.jsp").forward(request, response);
    }

    private void saveDepSwap(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = MemberMainMapping.validateDepSwap(request);

        List<CoverDetails> cdList = (List<CoverDetails>) request.getSession().getAttribute("MemberCoverDependantDetails");
        HashMap<Integer, String> depListJoinDate = (HashMap<Integer, String>) request.getSession().getAttribute("depListJoinDate");
        String joinDate = null;
        for (CoverDetails cov : cdList) {
            if (request.getParameter("newPrincipalEntityId").equals(cov.getEntityId() + "")) {
                joinDate = new SimpleDateFormat("yyyy/MM/dd").format(cov.getSchemeJoinDate().toGregorianCalendar().getTime());
            }
        }

        if (!joinDate.equals(depListJoinDate.get(17))) {
            errors.put("depSwapStartDate_error", "Dependant join date is not the same as principal member's join date");
        }

        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        NeoUser neoUser = getNeoUser(request);
        int entityId = new Integer(request.getParameter("memberEntityId"));

        if ("OK".equalsIgnoreCase(status)) {
            if (saveDepSwap(request)) {
                additionalData = "\"message\":\"The dependant swap has been processed.\"";
                additionalData = additionalData + ", \"forward\":\"/ManagedCare/Security/Welcome.jsp\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error performing the dependant swap.\"";
            }
        } else {
            additionalData = null;
        }

        if ("OK".equalsIgnoreCase(status)) {
            context.getRequestDispatcher(new MemberMaintenanceTabContentCommand().getMemberCoverDetails(request, entityId)).forward(request, response);

        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }

    }

    private boolean saveDepSwap(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = getNeoUser(request);
        int oldMemberEntityId = TabUtils.getIntParam(request, "memberEntityId");
        int newMemberEntityId = TabUtils.getIntParam(request, "newPrincipalEntityId");
        int groupEntityId = TabUtils.getIntParam(request, "employerEntityId");
        int brokerEntityId = TabUtils.getIntParam(request, "brokerEntityId");
        XMLGregorianCalendar startDate = TabUtils.getXMLDateParam(request, "depSwapStartDate");
        try {
            return port.saveDependantSwap(oldMemberEntityId, newMemberEntityId, groupEntityId, brokerEntityId, startDate, TabUtils.getSecurity(request));
        } catch (java.lang.Exception e) {
            return false;
        }

    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser) request.getSession().getAttribute("persist_user");
    }

    private void saveBroker(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = MemberMainMapping.validateBroker(request);
        System.out.println("mb.getMemberBrokerId()" + request.getParameter("brokerEntityId"));
        //String brokerCode = request.getParameter("brokerCode");
        
            if (request.getParameter("brokerEntityId") == null || request.getParameter("brokerEntityId").isEmpty()) {
                errors.put("BrokerApp_StartDate_error", "Please search for a broker");
            }
            if (request.getParameter("BrokerApp_StartDate") == null || request.getParameter("BrokerApp_StartDate").isEmpty()) {
                errors.put("BrokerApp_StartDate_error", "Enter Start Date");
            }
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        NeoUser neoUser = getNeoUser(request);

        int entityId = new Integer(request.getParameter("memberEntityId"));

        if ("OK".equalsIgnoreCase(status)) {
            if (saveBroker(request)) {
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving member broker information.\"";
            }
        } else {
            additionalData = null;
        }

        if ("OK".equalsIgnoreCase(status)) {
            context.getRequestDispatcher(new MemberMaintenanceTabContentCommand().getMemberCoverDetails(request, entityId)).forward(request, response);

        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }
    }

    private void saveConsultant(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = MemberMainMapping.validateBrokerConsultant(request);

        if (request.getParameter("consultantEntityId") == null || request.getParameter("consultantEntityId").isEmpty()) {
            errors.put("BrokerConsultantApp_StartDate_error", "Please search for a consultant");
        }
        if (request.getParameter("BrokerConsultantApp_StartDate") == null || request.getParameter("BrokerConsultantApp_StartDate").isEmpty()) {
            errors.put("BrokerConsultantApp_StartDate_error", "Enter Start Date");
        }

        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        NeoUser neoUser = getNeoUser(request);
        System.out.println("errors " + errors);
        System.out.println("status " + status);
        int entityId = new Integer(request.getParameter("memberEntityId"));

        if ("OK".equalsIgnoreCase(status)) {
            if (saveConsultant(request)) {
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving member consultant information.\"";
            }
        } else {
            additionalData = null;
        }

        if ("OK".equalsIgnoreCase(status)) {
            context.getRequestDispatcher(new MemberMaintenanceTabContentCommand().getMemberCoverDetails(request, entityId)).forward(request, response);

        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }
    }

    private void saveChangeOption(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = MemberMainMapping.validateChangeOption(request);
        HttpSession session = request.getSession();
        String backDated = null;
        if (request.getParameter("OptionChangeApp_StartDate") != null && !request.getParameter("OptionChangeApp_StartDate").isEmpty()) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("OptionChangeApp_StartDate"));
            Calendar past = Calendar.getInstance();
            Calendar future = Calendar.getInstance();
            Calendar optionDate = Calendar.getInstance();
            optionDate.setTime(startDate);
            past.add(Calendar.MONTH, -3);
            future.add(Calendar.MONTH, 2);

            Calendar today = Calendar.getInstance();
            int currMonth = today.get(Calendar.MONTH);
            //int currYear = today.get(Calendar.YEAR);
            //cal.add(Calendar.DAY_OF_WEEK_IN_MONTH, 1);
            //birth.add(Calendar.DATE, -91);

            Date maxDate = past.getTime();
            Date minDate = future.getTime();
            Date curr = optionDate.getTime();
            /*if (curr.before(maxDate)) {
             //errors.put("OptionChangeApp_StartDate_error", "Start date can't be more than three months back");
             backDated = (String) session.getAttribute("backDated");
             if (backDated == null || backDated.isEmpty()) {
             errors.put("OptionChangeApp_StartDate_error", "Back dating option dates MUST be signed off by GM Membership & Contributions before processing. Ensure relevant processes were followed prior to action. Click save to continue");
             session.setAttribute("backDated", "Warning");
             } else {
             session.setAttribute("backDated", null);
             }
             }*/
            HashMap<Integer, String> depListJoinDate = new HashMap<Integer, String>();
            depListJoinDate = (HashMap<Integer, String>) session.getAttribute("depListJoinDate");
            Date coverJoinDate = DateTimeUtils.convertFromYYYYMMDD(depListJoinDate.get(17));
            if (curr.before(coverJoinDate)) {
                errors.put("OptionChangeApp_StartDate_error", "option start date before the member registration date");
            }

            /*
            if (curr.after(minDate)) {
                errors.put("OptionChangeApp_StartDate_error", "Start date can't be more than two months in future");
            }*/

            if (currMonth != 0) {
                if (request.getParameter("OptionChangeApp_Reason") == null || request.getParameter("OptionChangeApp_Reason").isEmpty()) {
                    errors.put("OptionChangeApp_Reason_error", "Please provide a reason for the late option change");
                }

            }
        }

        int incomeCategoty = 0;
        int newOptionId = 0;
        try {
            if (request.getParameter("OptionChangeApp_IncomeCategory") != null && !request.getParameter("OptionChangeApp_IncomeCategory").isEmpty()) {
                incomeCategoty = new Integer(request.getParameter("OptionChangeApp_IncomeCategory"));
            }

            if (request.getParameter("OptionChangeApp_optionId") != null && !request.getParameter("OptionChangeApp_optionId").isEmpty()) {
                newOptionId = new Integer(request.getParameter("OptionChangeApp_optionId"));
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (newOptionId == 5 && incomeCategoty == 0) {
            errors.put("OptionChangeApp_IncomeCategory_error", "Income Categoty is mendontory for foundation option");
        }

        String suspendMember = (String) session.getAttribute("suspendMember");

        if (suspendMember != null && suspendMember.equalsIgnoreCase("Suspend")) {
            errors.put("OptionChangeApp_StartDate_error", "Option change is not allowed for this member because the member is suspended");
        }

        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        NeoUser neoUser = getNeoUser(request);

        int entityId = new Integer(request.getParameter("memberEntityId"));

        if ("OK".equalsIgnoreCase(status)) {
            if (saveChangeOption(request)) {
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error changing the option.\"";
            }
        } else {
            additionalData = null;
        }

        if ("OK".equalsIgnoreCase(status)) {
            context.getRequestDispatcher(new MemberMaintenanceTabContentCommand().getMemberCoverDetails(request, entityId)).forward(request, response);

        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }


    }

    private void saveInceptionDate(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = MemberMainMapping.validateInceptionDate(request);
        HttpSession session = request.getSession();
        HashMap<Integer, String> depListCreationDate = new HashMap<Integer, String>();
        session.setAttribute("inceptMsg", null);
        if (request.getParameter("inceptionStartDate") != null && !request.getParameter("inceptionStartDate").isEmpty()) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("inceptionStartDate"));
            Date maxDate = DateTimeUtils.convertFromYYYYMMDD("2012/01/01");
            Calendar inceptionDate = Calendar.getInstance();
            inceptionDate.setTime(startDate);
            

            int depNumber = new Integer(request.getParameter("deptList"));
            //cal.add(Calendar.DAY_OF_WEEK_IN_MONTH, 1);
            //birth.add(Calendar.DATE, -91);
            depListCreationDate = (HashMap<Integer, String>) session.getAttribute("depCreationDate");
            Date coverCreationDate = DateTimeUtils.convertFromYYYYMMDD(depListCreationDate.get(depNumber));
            List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
            String coverNumber = (String) session.getAttribute("coverNumber");
            int depType = 0;
            for (CoverDetails cov : cdList) {
                if (depNumber == cov.getDependentNumber()) {
                    depType = cov.getDependentTypeId();
                }
            }
            
            XMLGregorianCalendar xmlActivationDate = port.getDependentActivationDate(coverNumber, depNumber);
            Date activationDate = xmlActivationDate.toGregorianCalendar().getTime();
            System.out.println("Activation Date " + activationDate);
            
            
            //This code is to get the number of months
            Calendar start = Calendar.getInstance();
            start.setTime(activationDate);

            Calendar end = Calendar.getInstance();
            end.setTime(startDate);

            int monthsBetween = 0;
            int dateDiff = end.get(Calendar.DAY_OF_MONTH)-start.get(Calendar.DAY_OF_MONTH);      

            if(dateDiff<0) {
                int borrrow = end.getActualMaximum(Calendar.DAY_OF_MONTH);           
                 dateDiff = (end.get(Calendar.DAY_OF_MONTH)+borrrow)-start.get(Calendar.DAY_OF_MONTH);
                 monthsBetween--;

            if(dateDiff>0) {
                     monthsBetween++;
                 }
            }
            else {
                monthsBetween++;
            }      
            monthsBetween += end.get(Calendar.MONTH)-start.get(Calendar.MONTH);      
            monthsBetween  += (end.get(Calendar.YEAR)-start.get(Calendar.YEAR))*12;
            
            //Back Date
            //Calendar past = Calendar.getInstance();
            //past.setTime(activationDate);
            //past.add(Calendar.MONTH, -3);
            //Date maxDate = past.getTime();

            //Future Date
            Calendar future = Calendar.getInstance();
            future.setTime(new Date());
            future.add(Calendar.MONTH, 2);
            Date minDate = future.getTime();

            Date requiredDate = inceptionDate.getTime();
            Date backDated = (Date) session.getAttribute("backDated");
            Date futureDatedClaims = (Date) session.getAttribute("futureDatedClaims");
            Date futureDatedAuth = (Date) session.getAttribute("futureDatedAuth");
            //System.out.println("requiredDate " + requiredDate + " activationDate " + activationDate);

            if (requiredDate.before(activationDate)) {
                if (backDated == null) {
                    errors.put("inceptionStartDate_error", "Back dating inception dates MUST be signed off by GM Membership & Contributions before processing.  Signed off docs must be sent for scanning. Click save to continue");
                    session.setAttribute("backDated", requiredDate);
                }
            } else {
                session.setAttribute("backDated", null);
            }

            if (requiredDate.before(maxDate)) {
                errors.put("inceptionStartDate_error", "Start date can't be beyond 2012/01/01");
            }

            if (requiredDate.after(minDate)) {
                errors.put("inceptionStartDate_error", "Start date can't be more than two months in future");
            }

            String preMesg = "Future dating inception dates MUST be signed off by GM Membership & Contributions before processing.  Signed off docs must be sent for scanning";
            String claimMesg = "Future dating inception dates MUST be signed off by GM Membership & Contributions before processing.  Signed off docs must be sent for scanning";
            if (startDate.after(coverCreationDate)) {

                ClaimSearchCriteria search = new ClaimSearchCriteria();
                if (depType != 17) {
                    search.setDependentCode(depNumber);
                    claimMesg = "Future dating inception dates MUST be signed off by GM Membership & Contributions before processing.  Signed off docs must be sent for scanning";
                }
                search.setCoverNumber(coverNumber);
                //List<ClaimsBatch> claims = port.findClaimBySearchCriteria(search);
                int claimSize = port.checkIfDependantHasClaims(coverNumber, depNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(requiredDate));
                //System.err.println("Claim Size " + claimSize + " futureDatedClaims " + futureDatedClaims);
                if (claimSize > 0) {
                    if (futureDatedClaims == null) {
                        errors.put("inceptionStartDate_error", claimMesg);
                        request.getSession().setAttribute("futureDatedClaims", coverCreationDate);
                    } else {
                        request.getSession().setAttribute("futureDatedClaims", null);
                    }
                }
                PreAuthSearchCriteria pas = new PreAuthSearchCriteria();
                NeoUser user = (NeoUser) session.getAttribute("persist_user");
                pas.setCoverNumber(coverNumber);
                if (depType != 17) {
                    pas.setDependantCode(depNumber);
                    preMesg = "Future dating inception dates MUST be signed off by GM Membership & Contributions before processing.  Signed off docs must be sent for scanning";
                }
                //List<PreAuthSearchResults> auths = port.findPreAuthDetailsByCriteria(user, pas);
                int preAuths = port.checkIfDependantHasPreAuth(coverNumber, depNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(requiredDate));
                //System.err.println("Pre Auths size " + preAuths + " futureDatedAuth " + futureDatedAuth);
                if (preAuths > 0) {
                    if (futureDatedAuth == null) {
                        errors.put("inceptionStartDate_error", preMesg);
                        request.getSession().setAttribute("futureDatedAuth", coverCreationDate);
                    } else {
                        request.getSession().setAttribute("futureDatedAuth", null);
                    }
                }
            }

            Date prinDate = new Date();
            Date depDate = null;
            boolean errorDate = false;

            for (CoverDetails cov : cdList) {
                if (cov.getDependentTypeId() == 17) {
                    //prinDate = cov.getCreationDate().toGregorianCalendar().getTime();
                    prinDate = cov.getSchemeJoinDate().toGregorianCalendar().getTime();
                }
            }

            for (CoverDetails cd : cdList) {
                //depDate = cd.getCreationDate().toGregorianCalendar().getTime();
                depDate = cd.getSchemeJoinDate().toGregorianCalendar().getTime();
                //if (!(cd.getCreationDate().toGregorianCalendar().getTime().compareTo(prinDate) == 0)) {
                if (!(cd.getSchemeJoinDate().toGregorianCalendar().getTime().compareTo(prinDate) == 0)) {
                    errorDate = true;
                }
                if (depType != 17) {
                    if (startDate.before(prinDate)) {
                        errors.put("inceptionStartDate_error", "Dependant Start Date is before principal member 's start date");
                    }
                }

                if (depType == 17) {
                    if (errorDate) {
                        if (depDate.before(startDate) && cd.getDependentTypeId() != 17) {
                            errors.put("inceptionStartDate_error", "Dependant Start Date is before the new start date");
                        }
                    }
                }

            }


            String suspendMember = (String) session.getAttribute("suspendMember");

            if (suspendMember != null && suspendMember.equalsIgnoreCase("Suspend")) {
                errors.put("inceptionStartDate_error", "Inception change is not allowed for this member because the member is suspended");
            }

            String validationErros = TabUtils.convertMapToJSON(errors);
            String status = TabUtils.getStatus(errors);
            String additionalData = null;
            String updateFields = null;
            NeoUser neoUser = getNeoUser(request);

            int entityId = new Integer(request.getParameter("memberEntityId"));
            String memberNumber = (String) session.getAttribute("memberNumber");
            String value = memberNumber;
            System.out.println("Member number here " + memberNumber);
            Suspension suspension = port.getSuspention(value);
           Suspension mb = MemberMainMapping.getResignDetails(request, entityId, neoUser);
            if ("OK".equalsIgnoreCase(status)) {
                 System.out.println("Entering inception date...");
                 System.out.println("User " + neoUser.getUserId() + " is authorised = " + port.isAuthorisedForMembershipChange(neoUser.getUserId()));   

                 String client = String.valueOf(context.getAttribute("Client"));
                 if (client != null && client.equals("Sechaba")) {
                        if ((port.isAuthorisedForMembershipChange(neoUser.getUserId())) || (monthsBetween >= -2)) {
                            System.out.println("the value is " + port.isAuthorisedForMembershipChange(neoUser.getUserId()));
                            if (saveInceptionDate(request)) {
                                session.setAttribute("backDated", null);
                                request.getSession().setAttribute("futureDatedAuth", null);
                                request.getSession().setAttribute("futureDatedAuth", null);
                            } else {
                                status = "ERROR";
                                additionalData = "\"message\":\"There was an error changing the inception date.\"";
                            }
                        } else {
                            String category = "Inception";
                            String entityNum = memberNumber + "";
                            String wfNotes = "Authorisation required to change inception date " 
                                    + entityNum + " with inception date "  + mb.getEffectiveStartDate();
                            System.out.println("Print that note " + wfNotes);
                            Integer wfStatus = 1; // Open                        

                            WorkflowItem wi = setCallTrackingWorkflowItem(entityId, entityNum, category, wfStatus, wfNotes, neoUser.getUserId());
                            port.captureGenericWorkflowItem(wi);
                            status = "ERROR";
                            additionalData = "\"message\":\"You are not authorised to make this change. Workflow item created.\"";
                        }   
                }else {  
                if (saveInceptionDate(request)) {
                } else {
                    status = "ERROR";
                    additionalData = "\"message\":\"There was an error changing the inception date of the member.\"";
                }
            }
            } else {
                additionalData = null;
            }

            if ("OK".equalsIgnoreCase(status)) {
                context.getRequestDispatcher(new MemberMaintenanceTabContentCommand().getMemberCoverDetails(request, entityId)).forward(request, response);

            } else {
                PrintWriter out = response.getWriter();
                out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
            }


        }
    }

    private void saveSuspension(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = MemberMainMapping.validateSuspension(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        NeoUser neoUser = getNeoUser(request);

        int entityId = new Integer(request.getParameter("memberEntityId"));

        if ("OK".equalsIgnoreCase(status)) {
            if (saveSuspention(request)) {
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error while suspening the member.\"";
            }
        } else {
            additionalData = null;
        }

        if ("OK".equalsIgnoreCase(status)) {
            context.getRequestDispatcher(new MemberMaintenanceTabContentCommand().getMemberCoverDetails(request, entityId)).forward(request, response);

        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }


    }

    private boolean saveBroker(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = getNeoUser(request);
        Security sec = new Security();
        try {
            int entityId = new Integer(request.getParameter("memberEntityId"));
            String coverNumber = (String) request.getSession().getAttribute("coverNumber");
            MemberBroker mb = MemberMainMapping.getBrokerDetails(request, entityId, neoUser);
            sec.setCreatedBy(neoUser.getUserId());

            port.saveMemberToBrokerLink(mb);
            //System.err.println("Borker Start Date " + mb.getEffectiveStartDate() + " Cover Number " + coverNumber + " user " + sec.getCreatedBy());
            port.recalculateCommissions(0, 0, coverNumber, mb.getEffectiveStartDate(), sec);
            request.getSession().setAttribute("ConBroEntityId", mb.getBrokerEntityId());
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }

    }
    
    private boolean saveBrokerTermination(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = getNeoUser(request);
        Security sec = new Security();
        
        try {
            int entityId = new Integer(request.getParameter("memberEntityId"));
            int brokerEntityId = 0;
            String coverNumber = (String) request.getSession().getAttribute("coverNumber");
            List<MemberBroker> broker = port.findMemberBrokerByMemberEntityId(entityId);
            for (MemberBroker b : broker) {
                int date = b.getEffectiveEndDate().compare(DateTimeUtils.convertDateToXMLGregorianCalendar(new Date()));
                if (date == 1) {
                    brokerEntityId = b.getBrokerEntityId();
                }
            }
            
            request.setAttribute("brokerEntityId", brokerEntityId);
            MemberBroker mb = MemberMainMapping.getBrokerDetails(request, entityId, neoUser);
            sec.setCreatedBy(neoUser.getUserId());

            port.saveMemberToBrokerLinkTermination(mb);
            //System.err.println("Borker Start Date " + mb.getEffectiveStartDate() + " Cover Number " + coverNumber + " user " + sec.getCreatedBy());
            port.recalculateCommissions(0, 0, coverNumber, mb.getEffectiveEndDate(), sec);
            request.getSession().setAttribute("ConBroEntityId", mb.getBrokerEntityId());
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }

    }

    private boolean saveConsultant(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = getNeoUser(request);
        Security sec = new Security();
        try {
            int entityId = new Integer(request.getParameter("memberEntityId"));
            String coverNumber = (String) request.getSession().getAttribute("coverNumber");
            sec.setCreatedBy(neoUser.getUserId());
            MemberBrokerConsultant mb = MemberMainMapping.getBrokerConsultantDetails(request, entityId, neoUser);
            port.saveMemberToBrokerConsultantLink(mb);
            //System.err.println("Consultant Start Date " + mb.getEffectiveStartDate() + " Cover Number " + coverNumber + " user " + sec.getCreatedBy());
            port.recalculateCommissions(0, 0, coverNumber, mb.getEffectiveStartDate(), sec);
            request.getSession().setAttribute("ConBroEntityId", mb.getBrokerConsultantEntityId());
            return true;
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            return false;
        }

    }
    
    private boolean saveConsultantTermination(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = getNeoUser(request);
        Security sec = new Security();
        try {
            int entityId = new Integer(request.getParameter("memberEntityId"));
            int conEntityId = 0;
            int ConBroEntityId = (Integer) request.getSession().getAttribute("ConBroEntityId");
            
            List<MemberBrokerConsultant> consultant = port.findMemberBrokerConsultantByBrokerEntityId(ConBroEntityId);
            
            for (MemberBrokerConsultant c : consultant) {
                int date = c.getEffectiveEndDate().compare(DateTimeUtils.convertDateToXMLGregorianCalendar(new Date()));
                if (date == 1) {
                    conEntityId = c.getBrokerConsultantEntityId();
                }
            }
            
            request.setAttribute("consultantEntityId", conEntityId);
            String coverNumber = (String) request.getSession().getAttribute("coverNumber");
            sec.setCreatedBy(neoUser.getUserId());
            MemberBrokerConsultant mb = MemberMainMapping.getBrokerConsultantDetails(request, entityId, neoUser);
            
            port.saveMemberToBrokerConsultantLinkTermination(mb);
            //System.err.println("Consultant Start Date " + mb.getEffectiveStartDate() + " Cover Number " + coverNumber + " user " + sec.getCreatedBy());
            port.recalculateCommissions(0, 0, coverNumber, mb.getEffectiveEndDate(), sec);
            request.getSession().setAttribute("ConBroEntityId", mb.getBrokerConsultantEntityId());
            return true;
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    private boolean saveChangeOption(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();

        NeoUser neoUser = getNeoUser(request);
        int entityId = new Integer(request.getParameter("memberEntityId"));
        Security sec = new Security();
        Date today = new Date();
        Calendar todayC = Calendar.getInstance();
        int currMonth = todayC.get(Calendar.MONTH);

        sec.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        sec.setSecurityGroupId(2);

        try {
            CoverDetails cd = MemberMainMapping.getOptionChange(request, entityId, neoUser);

            int incomeCategoty = 0;
            try {
                if (request.getParameter("OptionChangeApp_IncomeCategory") != null && !request.getParameter("OptionChangeApp_IncomeCategory").isEmpty()) {
                    incomeCategoty = new Integer(request.getParameter("OptionChangeApp_IncomeCategory"));
                }

                if (currMonth != 0) {
                    if (request.getParameter("OptionChangeApp_Reason") != null && !request.getParameter("OptionChangeApp_Reason").isEmpty()) {
                        cd.setStatusId(100);
                        cd.setStatusReasonId(new Integer(request.getParameter("OptionChangeApp_Reason")));
                    }
                }

            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            
            String networkIPA = request.getParameter("optionChangeApp_network_IPA");
            
            port.changeCoverOption(cd, cd.getCoverStartDate(), incomeCategoty, sec, networkIPA);
            port.updateContributionsAndBenefits(cd.getCoverNumber(), cd.getCoverStartDate(), TabUtils.getSecurity(request));
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }

    }

    private boolean saveInceptionDate(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        NeoUser neoUser = getNeoUser(request);
        HashMap<Integer, Integer> depList = new HashMap<Integer, Integer>();
        int entityId = new Integer(request.getParameter("memberEntityId"));
        depList = (HashMap<Integer, Integer>) session.getAttribute("depEntity");
        String coverNumber = (String) session.getAttribute("coverNumber");
        Security sec = new Security();
        Date today = new Date();

        sec.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        sec.setSecurityGroupId(2);

        try {
            if (request.getParameter("inceptionStartDate") != null && !request.getParameter("inceptionStartDate").isEmpty()) {
                GregorianCalendar gcal = new GregorianCalendar();
                Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("inceptionStartDate"));
                //gcal.setTime(startDate);
                //gcal.set(Calendar.DAY_OF_MONTH, 1);                
                port.changeMemberInceptionDate(depList.get(new Integer(request.getParameter("deptList"))), DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), neoUser.getUserId());
                port.updateContributionsAndBenefits(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), TabUtils.getSecurity(request));
                String ConBro = (String) request.getSession().getAttribute("ConBro");
                //System.err.println("Broker and Consultant " + request.getSession().getAttribute("ConBro"));
                List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
                int depNumber = new Integer(request.getParameter("deptList"));
                int depType = 0;
                for (CoverDetails cov : cdList) {
                    if (depNumber == cov.getDependentNumber()) {
                        depType = cov.getDependentTypeId();
                    }
                }
                //System.err.println("-----depType " + depType);
                if (depType == 17) {
                    if (ConBro.equals("Consultant")) {
                        //System.err.println("Link Consultant");
                        MemberBrokerConsultant mb = MemberMainMapping.getBrokerConsultantDetailsForInceptionDate(request, entityId, neoUser);
                        port.saveMemberToBrokerConsultantLink(mb);
                    } else if (ConBro.equals("Broker")){
                        //System.err.println("Link Broker");
                        MemberBroker mb = MemberMainMapping.getBrokerDetailsForInceptionDate(request, entityId, neoUser);
                        port.saveMemberToBrokerLink(mb);
                    }

                    MemberEmployer mb = MemberMainMapping.getGroupDetailsForInceptionDate(request, entityId, neoUser);
                    port.saveMemberToGroupLink(mb);
                }
            }
            return true;
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    private boolean saveSuspention(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = getNeoUser(request);
        int entityId = new Integer(request.getParameter("memberEntityId"));
        HttpSession session = request.getSession();
        String memberNumber = (String) session.getAttribute("memberNumber");
        try {
            Suspension mb = MemberMainMapping.getSuspensionDetails(request, entityId, neoUser);
            port.suspendCoverDetails(memberNumber, mb);
            session.setAttribute("coverStatus", "Suspend");
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }

    }

    private void liftSuspension(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        HttpSession session = request.getSession();
        NeoUser neoUser = getNeoUser(request);
        int entityId = (Integer) session.getAttribute("memberCoverEntityId");
        liftSuspention(request);

        context.getRequestDispatcher(new MemberMaintenanceTabContentCommand().getMemberCoverDetails(request, entityId)).forward(request, response);

    }

    private boolean liftSuspention(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = getNeoUser(request);
        HttpSession session = request.getSession();
        String memberNumber = (String) session.getAttribute("memberNumber");
        int entityId = new Integer(request.getParameter("memberEntityId"));
        try {
            //Suspension mb = MemberMainMapping.getSuspensionDetails(request, entityId, neoUser);
            port.liftSuspesionCoverDetails(memberNumber, neoUser.getUserId());
            session.setAttribute("coverStatus", "Active");
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }

    }

    private void reInstateMember(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = MemberMainMapping.validateReInstate(request);

        String additionalData = null;
        String updateFields = null;
        NeoUser neoUser = getNeoUser(request);
        HttpSession session = request.getSession();
        boolean save = true;

        request.setAttribute("diffoption", null);

        String principalStatus = (String) session.getAttribute("principalStatus");
        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        int depNumber = new Integer(request.getParameter("deptList"));
        int depType = 0;
        for (CoverDetails cov : cdList) {
            if (depNumber == cov.getDependentNumber()) {
                depType = cov.getDependentTypeId();
            }
        }
        if (!principalStatus.equals("Active") && depType == 17) {
            save = true;
        } else if (!principalStatus.equals("Active") && depType != 17) {
            errors.put("deptList_error", "Pricipal member is not active on the cover");
        }

        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);

        int entityId = (Integer) session.getAttribute("memberCoverEntityId");
        if ("OK".equalsIgnoreCase(status)) {
            if (reInstateMemmber(request)) {
            } else {
                status = "ERROR";
                if (request.getAttribute("diffoption") == null) {
                    additionalData = "\"message\":\"There was an error re-instating the member.\"";

                } else {
                    additionalData = "\"message\":\"Dependent is on a different option.  Cannot Re-instate.\"";

                }
            }
        } else {
            additionalData = null;
        }

        if ("OK".equalsIgnoreCase(status)) {
            context.getRequestDispatcher(new MemberMaintenanceTabContentCommand().getMemberCoverDetails(request, entityId)).forward(request, response);

        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }

    }

    private boolean reInstateMemmber(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = getNeoUser(request);
        HttpSession session = request.getSession();
        int depCoverOptionId = 0;
        int memCoverOptionId = 0;
        String memberNumber = (String) session.getAttribute("memberNumber");
        int entityId = new Integer(request.getParameter("memberEntityId"));
        try {
            Suspension mb = MemberMainMapping.getReInstateDetails(request, entityId, neoUser);
            List<CoverDetails> coverDetails = port.getResignedMemberByCoverNumber(memberNumber);
            Integer depNum = new Integer(request.getParameter("deptList"));

            List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
            for (CoverDetails cov : cdList) {
                if (depNum == cov.getDependentNumber()) {
                    depCoverOptionId = cov.getOptionId();
                }

                if (cov.getDependentTypeId() == 17) {
                    memCoverOptionId = cov.getOptionId();
                }

            }

            if (depCoverOptionId == memCoverOptionId) {
                port.reinstateCoverDetails(memberNumber, new Integer(request.getParameter("deptList")), mb.getEffectiveStartDate(), neoUser.getUserId());
                for (CoverDetails cd : coverDetails) {
                    if (cd.getDependentNumber() == depNum) {
                        port.updateContributionsAndBenefits(memberNumber, cd.getCoverEndDate(), TabUtils.getSecurity(request));
                        break;
                    }
                }
            } else {
                //port.reinstateDependentToAnotherOptionCoverDetails(memberNumber, new Integer(request.getParameter("deptList")), mb.getEffectiveStartDate(), neoUser.getUserId(),memCoverOptionId,depCoverOptionId);
                request.setAttribute("diffoption", "Dependent is on different option.  Cannot Re-instate");
                return false;
            }



            session.setAttribute("coverStatus", "Active");
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }

    }

    private void resignMember(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = MemberMainMapping.validateResign(request);
        
        String resignationReason = request.getParameter("resignReason");
        
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String additionalData = null;
        String updateFields = null;
        NeoUser neoUser = getNeoUser(request);
        HttpSession session = request.getSession();
        
        if (!resignationReason.equals(8)) {
            errors.put("deseasedReason_error", "");
        }        
        
        int entityId = (Integer) session.getAttribute("memberEntityID");
        String memberNumber = (String) session.getAttribute("memberNumber");
        String value = memberNumber;
        Suspension suspension = port.getSuspention(value);
        Suspension mb = MemberMainMapping.getResignDetails(request, entityId, neoUser);
        if (suspension.getEffectiveStartDate() != null) {
            System.out.println(suspension.getStartDate());
            Date supensionSartDate = DateTimeUtils.getDateFromXMLGregorianCalendar(suspension.getEffectiveStartDate());
            Date resignDate = DateTimeUtils.getDateFromXMLGregorianCalendar(mb.getEffectiveStartDate());
            if (supensionSartDate.before(resignDate)) {
                errors.put("resignStartDate_error", "Resign date must be less than suspension start Date");
            }
        }
        //**************************************Warning Error***************************************************
     /*
         * String warning = null; String coverNum = (String)
         * session.getAttribute("coverNum"); session.setAttribute("warningW",
         * null);
         *
         * int claims = port.checkIfmemberHasClaims(coverNum);
         *
         * if (claims > 0) { warning = "claims"; } PreAuthSearchCriteria pas =
         * new PreAuthSearchCriteria(); NeoUser user = (NeoUser)
         * session.getAttribute("persist_user"); pas.setCoverNumber(coverNum);
         * List<PreAuthSearchResults> auths =
         * port.findPreAuthDetailsByCriteria(user, pas);
         *
         * if (auths.size() > 0) { warning = "pre"; }
         *
         * if (claims > 0 && auths.size() > 0) { warning = "both"; }
         * session.setAttribute("warningW", warning);
         */
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        System.out.println("status: " + status);
 
        if ("OK".equalsIgnoreCase(status)) {
            System.out.println("Entering resignMember...");
            System.out.println("User " + neoUser.getUserId() + " is authorised = " + port.isAuthorisedForMembershipChange(neoUser.getUserId()));   
            
            String client = String.valueOf(context.getAttribute("Client"));          
            if (client != null && client.equals("Sechaba")) {
                if (checkClaimsAndAuthsAfterDate(port, request, response, context)) {
                    if (port.isAuthorisedForMembershipChange(neoUser.getUserId())) {
                        if (resignMember(request)) {
                        } else {
                            status = "ERROR";
                            additionalData = "\"message\":\"There was an error resign the member.\"";
                        }
                    } else {
                        String category = "Resignation";
                        String entityNum = memberNumber + "";
                        String wfNotes = "Authorisation required to backdate resign cover " 
                                + entityNum + " with resign date "  + mb.getEffectiveStartDate()
                                + ". Claims and Pre-Auths where found.";
                        Integer wfStatus = 1; // Open                        
                                
                        WorkflowItem wi = setCallTrackingWorkflowItem(entityId, entityNum, category, wfStatus, wfNotes, neoUser.getUserId());
                        service.getNeoManagerBeanPort().captureGenericWorkflowItem(wi);
                        
                        status = "ERROR";
                        additionalData = "\"message\":\"You are not authorised to make this change. Workflow item created.\"";
                    }
                } else {
                    if (resignMember(request)) {
                    } else {
                        status = "ERROR";
                        additionalData = "\"message\":\"There was an error resign the member.\"";
                    }
                }
            } else {
                if (resignMember(request)) {
                } else {
                    status = "ERROR";
                    additionalData = "\"message\":\"There was an error resign the member.\"";
                }
            }
        } else {
            additionalData = null;
        }        

        if ("OK".equalsIgnoreCase(status)) {
            context.getRequestDispatcher(new MemberMaintenanceTabContentCommand().getMemberCoverDetails(request, entityId)).forward(request, response);

        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }
    }

    private boolean resignMember(HttpServletRequest request) {
        System.out.println("Entered resignMember...");
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = getNeoUser(request);
        HttpSession session = request.getSession();
        String memberNumber = (String) session.getAttribute("memberNumber");
        int entityId = new Integer(request.getParameter("memberEntityId"));
        try {
            Suspension mb = MemberMainMapping.getResignDetails(request, entityId, neoUser);
            mb.setSuspensionReason(new Integer(request.getParameter("resignReason")));
            System.out.println("deseasedReason: " + request.getParameter("deseasedReason"));
            
            if (request.getParameter("deseasedReason").equals("")) {
                mb.setDeceasedReason(0);
            } else {
                mb.setDeceasedReason(new Integer(request.getParameter("deseasedReason")));
            }
            
            port.updateCoverDetailsResignation(mb, memberNumber, mb.getEffectiveStartDate(), new Integer(request.getParameter("deptList")), new Integer(request.getParameter("resignReason")), neoUser.getUserId());
            port.updateContributionsAndBenefits(memberNumber, mb.getEffectiveStartDate(), TabUtils.getSecurity(request));
            session.setAttribute("coverStatus", "Resign");
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }

    }

    private void saveGroup(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = MemberMainMapping.validateGroup(request);

        if (request.getParameter("employerEntityId") == null || request.getParameter("employerEntityId").isEmpty()) {
            errors.put("GroupApp_StartDate_error", "Please search for a group");
        }
        if (request.getParameter("GroupApp_StartDate") == null || request.getParameter("GroupApp_StartDate").isEmpty()) {
            errors.put("GroupApp_StartDate_error", "Enter Start Date");
        }

        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        NeoUser neoUser = getNeoUser(request);

        int entityId = new Integer(request.getParameter("memberEntityId"));

        if ("OK".equalsIgnoreCase(status)) {
            if (saveGroup(request)) {
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving member group information.\"";
            }
        } else {
            additionalData = null;
        }

        if ("OK".equalsIgnoreCase(status)) {
            context.getRequestDispatcher(new MemberMaintenanceTabContentCommand().getMemberCoverDetails(request, entityId)).forward(request, response);

        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }


    }

    private boolean saveGroup(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        NeoUser neoUser = getNeoUser(request);
        int entityId = new Integer(request.getParameter("memberEntityId"));
        String memberNumber = (String) session.getAttribute("memberNumber");
        try {
            MemberEmployer mb = MemberMainMapping.getGroupDetails(request, entityId, neoUser);



            port.saveMemberToGroupLink(mb);
            port.updateContributionsAndBenefits(memberNumber, mb.getEffectiveStartDate(), TabUtils.getSecurity(request));
            request.getSession().setAttribute("GroupIncEntityId", mb.getEmployerEntityId());
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }

    }

    private void CheckClaimsAndAuthsAfterDate(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        HttpSession session = request.getSession();
        String memberNumber = (String) session.getAttribute("memberNumber");
        Integer depNo = new Integer(request.getParameter("deptList"));
        Date dateParam = TabUtils.getDateParam(request, "resignStartDate");

        String message;
        if (dateParam == null) {
            message = "";
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dateParam);
            cal.set(Calendar.DAY_OF_MONTH, 1);
            cal.add(Calendar.MONTH, 1);
            cal.add(Calendar.DATE, -1);
            String stringFromDate = TabUtils.getStringFromDate(cal.getTime());
            XMLGregorianCalendar xmlDateParam = DateTimeUtils.convertDateToXMLGregorianCalendar(cal.getTime());
            int claimCount = port.getClaimCountForMemberAfterDate(memberNumber, depNo, xmlDateParam);
            int authCount = port.getAuthCountForMemberAfterDate(memberNumber, depNo, xmlDateParam);

            if (claimCount != 0 || authCount != 0) {
                message = claimCount > 0 ? "There are " + claimCount + " active claim(s) for the member after " + stringFromDate + ".\r\r" : "";
                message = authCount > 0 ? message + "There are " + authCount + " active Pre-Auths(s) for the member after " + stringFromDate + ".\r\r" : message;
                message = message + "Press OK to continue resigning the member or cancel to stop.";
            } else {
                message = "";
            }

        }
        PrintWriter out = response.getWriter();
        out.println(message);
        out.flush();
        out.close();
    }

    private void CheckMemTypeAndGroupType(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        HttpSession session = request.getSession();
        Integer entityId = (Integer) session.getAttribute("memberEntityID");
        Date dateParam = TabUtils.getDateParam(request, "resignStartDate");
        Member memTypeFetch = port.fetchMemberAddInfo(entityId);
        int memCat = memTypeFetch.getMemberCategoryInd();
        String coverNumber = (String) session.getAttribute("coverNumber");
        MemberEmployerBrokerInfo info = port.getMemberEmployerBrokerInfo(coverNumber);
        String groupType = info.getGroupType();
        String warning = "";
        if (memCat != 0 || groupType != "") {

            warning = memCat == 3 ? "This is a VIP  Member - Inform Broker ," + ".\r\r" : warning;
            warning = groupType.equalsIgnoreCase("VIP") ? "This is a VIP Group - Inform Broker " + ".\r\r" : warning;
            warning = memCat == 3 && groupType.equalsIgnoreCase("VIP") ? "This is a VIP  Member /Group - Inform Broker " + ".\r\r" : warning;
            warning = warning + "Press OK to continue or Cancel to stop.";
        } else {
            warning = "";
        }

        PrintWriter out = response.getWriter();
        out.println(warning);
        out.flush();
        out.close();
    }

    private void CheckBrokerConcession(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        HttpSession session = request.getSession();
        String brokerCode = request.getParameter("Disorder_diagCode_text");

        int brokerIdFetch = port.findMemberBrokerConcessionByBrokerCode(brokerCode);


        String warning = "";
        if (brokerIdFetch == 1) {

            warning = brokerIdFetch == 1 ? "Member associated to an existing broker concession" + ".\r\r" : "";
            warning = warning + "Press OK to continue to link member to Broker or Cancel to stop.";
        } else {
            warning = "";
        }

        PrintWriter out = response.getWriter();
        out.println(warning);
        out.flush();
        out.close();
    }

    private void CheckGroupConcession(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        HttpSession session = request.getSession();
        int entityId = new Integer(request.getParameter("employerEntityId"));
        int empEntityId = port.fetchMemberGroupConcessionByEntityId(entityId);
        String warning = "";
        if (empEntityId == 1) {

            warning = empEntityId == 1 ? "Member associated to an existing group concession " + ".\r\r" : "";
            warning = warning + "Press OK to continue to link member to Group or Cancel to stop.";
        } else if (empEntityId == 2) {

            warning = empEntityId == 2 ? "Member associated to an existing group concession " + ".\r\r" : "";
            warning = warning + "Press OK to continue to link member to Group or Cancel to stop.";
        } else {
            warning = "";
        }

        PrintWriter out = response.getWriter();
        out.println(warning);
        out.flush();
        out.close();

    }
    
    private void validateBenefitPayoutType(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        HttpSession session = request.getSession();
        session.removeAttribute("PayoutStatus");
        Map<String, String> errors = MemberMainMapping.validatePayoutType(request);
        
        //additional validation
        String payoutStartDateStr = request.getParameter("PayoutType_payoutStartDate");
        if (payoutStartDateStr != null && !payoutStartDateStr.isEmpty() && payoutStartDateStr != "") {
            Date payoutStartDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("PayoutType_payoutStartDate"));
            
            Calendar calendar = new GregorianCalendar(2015,00,01);
            Date minDate = calendar.getTime();
            if(!payoutStartDate.equals(minDate) && payoutStartDate.before(minDate)){
                errors.put("PayoutType_payoutStartDate_error", "Start Date cannot be before 2015/01/01.");
            }
            
            Date current = DateUtils.truncate(new Date(), Calendar.DATE);
            if(!payoutStartDate.equals(current) && payoutStartDate.before(current)){
                errors.put("PayoutType_payoutStartDate_error", "Start Date cannot be before current date.");
            }
        }

        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;

        int entityId = new Integer(request.getParameter("memberEntityId"));
        
        if ("OK".equalsIgnoreCase(status)) {
            if (SaveBenefitPayoutType(request)) {
                session.setAttribute("PayoutStatus", "Saved");
            } else {
                session.setAttribute("PayoutStatus", "There was an error saving benefit pay out type information");
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving benefit pay out type information.\"";
            }
        } else {
            additionalData = null;
        }

        if ("OK".equalsIgnoreCase(status)) {
            context.getRequestDispatcher(new MemberMaintenanceTabContentCommand().getMemberCoverDetails(request, entityId)).forward(request, response);

        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }
    }
    
    private boolean SaveBenefitPayoutType(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        HttpSession session = request.getSession();
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);
        
        try {
            int infoId = 10;
            String infoValue = request.getParameter("PayoutType_payoutType");
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("PayoutType_payoutStartDate"));
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(startDate);
            
            int day = calendar.get(Calendar.DATE);
            if(day != 1){
                int month = calendar.get(Calendar.MONTH);
                calendar.set(Calendar.DATE, 1);
                calendar.set(Calendar.MONTH, month+1);
            }
            startDate = calendar.getTime();
            
            System.out.println("coverNumber : " + session.getAttribute("coverNumber"));
            String coverNumber = (String) session.getAttribute("coverNumber");
            port.saveCoverAddInfo(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), infoValue, infoId, sec);
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
    }
    
    private Boolean checkClaimsAndAuthsAfterDate(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        HttpSession session = request.getSession();
        String memberNumber = (String) session.getAttribute("memberNumber");
        Integer depNo = new Integer(request.getParameter("deptList"));
        Date dateParam = TabUtils.getDateParam(request, "resignStartDate");

        String message;
        if (dateParam == null) {
            return false;
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dateParam);
            cal.set(Calendar.DAY_OF_MONTH, 1);
            cal.add(Calendar.MONTH, 1);
            cal.add(Calendar.DATE, -1);
            String stringFromDate = TabUtils.getStringFromDate(cal.getTime());
            XMLGregorianCalendar xmlDateParam = DateTimeUtils.convertDateToXMLGregorianCalendar(cal.getTime());
            int claimCount = port.getClaimCountForMemberAfterDate(memberNumber, depNo, xmlDateParam);
            int authCount = port.getAuthCountForMemberAfterDate(memberNumber, depNo, xmlDateParam);

            if (claimCount != 0 || authCount != 0) {
                System.out.println("Has claims and preauths");
                return true; 
            } else {
                System.out.println("No claims and preauths");
                return false;
            }
        }
    }
    
    private WorkflowItem setCallTrackingWorkflowItem(Integer entityID, String entityNum, String category, Integer statusID, String notes, Integer userID) {
        WorkflowItem wi = new WorkflowItem(); 
        
        wi.setSourceID(1); // Source is call tracking
	wi.setProductID(3); // Sizwe
	wi.setQueueID(11); // Set Correct Queue ID
	wi.setStatusID(statusID);
	wi.setSubject("Membership " + category + " " + entityNum); // Member Number/Provider Number + " " + Category 
        
        ///////////////////
        wi.setEntityID(entityID);
        wi.setEntityNumber(entityNum);
        wi.setNotes(notes);
        ///////////////////
        
	wi.setUserID(null); // User assigned to
	wi.setCreatedBy(userID);
	wi.setLastUpdatedBy(userID);
	wi.setDocumentID(null);

        return wi;
    }    
    
    private void LinkMembertoUnion(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        HttpSession session = request.getSession();
        int entityId = (Integer) session.getAttribute("memberCoverEntityId");
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));        
        context.getRequestDispatcher("/Member/LinkMemberToUnion.jsp").forward(request, response);
    }
}

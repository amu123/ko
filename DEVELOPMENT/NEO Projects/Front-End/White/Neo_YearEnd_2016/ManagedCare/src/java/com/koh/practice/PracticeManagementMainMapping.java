/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.practice;

import static com.koh.command.NeoCommand.service;
import com.koh.employer.command.TabUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import neo.manager.AddressDetails;
import neo.manager.BankingDetails;
import neo.manager.ContactDetails;
import neo.manager.ContactPreference;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.PracticeCaptureDetails;
import neo.manager.ProviderDetails;
import org.apache.log4j.Logger;

/**
 *
 * @author almaries
 */
public class PracticeManagementMainMapping {

//    private static final Logger logger = Logger.getLogger(PracticeManagementMainMapping.class);
    private static final String[][] CONTACT_FIELD_MAPPINGS = {
        {"ContactDetails_email", "Email Address", "no", "str", "ContactDetails", "", "0", "50"},
        {"ContactDetails_faxNo", "Fax No", "no", "str", "ContactDetails", "", "0", "50"},
        {"ContactDetails_cell", "Cellphone Number", "no", "str", "ContactDetails", "", "0", "50"},
        {"ContactDetails_telNoWork", "Telephone Number(Work)", "no", "str", "ContactDetails", "", "0", "50"},
        {"ContactDetails_telNoHome", "Telephone Number(Home)", "no", "str", "ContactDetails", "", "0", "50"}};
    private static final String[][] ADDRESS_FIELD_MAPPINGS = {
        {"AddressDetails_pos_addressLine1", "Line 1", "no", "str", "AddressDetails", "", "0", "100"},
        {"AddressDetails_pos_addressLine2", "Line 2", "no", "str", "AddressDetails", "", "0", "100"},
        {"AddressDetails_pos_addressLine3", "Line 3", "no", "str", "AddressDetails", "", "0", "100"},
        {"AddressDetails_pos_postalCode", "Postal Code", "no", "str", "AddressDetails", "", "0", "10"},
        {"AddressDetails_res_addressLine1", "Line 1", "no", "str", "AddressDetails", "", "0", "100"},
        {"AddressDetails_res_addressLine2", "Line 2", "no", "str", "AddressDetails", "", "0", "100"},
        {"AddressDetails_res_addressLine3", "Line 3", "no", "str", "AddressDetails", "", "0", "100"},
        {"AddressDetails_res_postalCode", "Postal Code", "no", "str", "AddressDetails", "", "0", "10"}};
    private static final String[][] BANK_FIELD_MAPPINGS = {
        {"BankingDetails_contri_paymentMethod", "Payment Method", "no", "str", "BankingDetails", ""},
        {"BankingDetails_contri_bankName", "Name of Bank", "no", "int", "BankingDetails", ""},
        {"BankingDetails_contri_bankBranchId", "Branch", "no", "int", "BankingDetails", ""},
        {"BankingDetails_contri_accountType", "Account Type", "no", "int", "BankingDetails", ""},
        {"BankingDetails_contri_accountHolder", "Account Holder", "no", "str", "BankingDetails", ""},
        {"BankingDetails_contri_accountNumber", "Account Number", "no", "numberField", "BankingDetails", ""},
        {"BankingDetails_pay_bankName", "Name of Bank", "no", "int", "BankingDetails", ""},
        {"BankingDetails_pay_bankBranchId", "Branch", "no", "int", "BankingDetails", ""},
        {"BankingDetails_pay_accountType", "Account Type", "no", "int", "BankingDetails", ""},
        {"BankingDetails_pay_accountHolder", "Account Holder", "no", "str", "BankingDetails", ""},
        {"BankingDetails_pay_accountNumber", "Account Number", "no", "numberField", "BankingDetails", ""}};
    private static final String[][] CONTACT_PREF_FIELD_MAPPINGS = {
        {"ConPrefDetails_claimProc", "Claim Processing", "no", "str", "ContactDetails", ""},
        {"ConPrefDetails_contriStmt", "Contributions Statement", "no", "str", "ContactDetails", ""},
        {"ConPrefDetails_claimStmt", "Claim Statement", "no", "str", "ContactDetails", ""},
        {"ConPrefDetails_claimPay", "Claim Payment", "no", "str", "ContactDetails", ""},
        {"ConPrefDetails_taxCert", "Tax Certificate", "no", "str", "ContactDetails", ""},
        {"ConPrefDetails_preauth", "Pre-Auths", "no", "str", "ContactDetails", ""},
        {"ConPrefDetails_other", "Other", "no", "str", "ContactDetails", ""}};
    //GET MAPPINGS
    private static final Map<String, String[]> CONTACT_FIELD_MAP;
    private static final Map<String, String[]> ADDRESS_FIELD_MAP;
    private static final Map<String, String[]> BANK_FIELD_MAP;
    private static final Map<String, String[]> CONTACT_PREF_FIELD_MAP;

    //CONTACT
    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : CONTACT_FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        CONTACT_FIELD_MAP = hm;
    }

    public static Map<String, String[]> getContactMapping() {
        return CONTACT_FIELD_MAP;
    }

    //CONTACT PREFERENCE
    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : CONTACT_PREF_FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        CONTACT_PREF_FIELD_MAP = hm;
    }

    public static Map<String, String[]> getContactPrefMapping() {
        return CONTACT_PREF_FIELD_MAP;
    }

    //ADDRESS
    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : ADDRESS_FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        ADDRESS_FIELD_MAP = hm;
    }

    public static Map<String, String[]> getAddressMapping() {
        return ADDRESS_FIELD_MAP;
    }

    //BAKING
    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : BANK_FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        BANK_FIELD_MAP = hm;
    }

    public static Map<String, String[]> getBankingMapping() {
        return BANK_FIELD_MAP;
    }

    //CONTACT
    public static Map<String, String> validateContact(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, CONTACT_FIELD_MAPPINGS);
        return errors;
    }

    //ADDRESS
    public static Map<String, String> validateAddress(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, ADDRESS_FIELD_MAPPINGS);
        return errors;
    }

    //BANKING
    public static Map<String, String> validateBanking(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, BANK_FIELD_MAPPINGS);
        return errors;
    }

    /**
     * ************************CONTACT DETAILS************************
     * @param request
     * @param neoUser
     * @param entityId
     * @return 
     */
    public static List<ContactDetails> getContactDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<ContactDetails> cdl = new ArrayList<ContactDetails>();
        String email = request.getParameter("ContactDetails_email");
        String fax = request.getParameter("ContactDetails_faxNo");
        String cell = request.getParameter("ContactDetails_cell");
        String work = request.getParameter("ContactDetails_telNoWork");
        String home = request.getParameter("ContactDetails_telNoHome");

        if (email != null && !email.equalsIgnoreCase("") && !email.equalsIgnoreCase("null")) {
            cdl.add(getContactDetail(request, "ContactDetails_email", 1));
        }
        if (fax != null && !fax.equalsIgnoreCase("") && !fax.equalsIgnoreCase("null")) {
            cdl.add(getContactDetail(request, "ContactDetails_faxNo", 2));
        }
        if (cell != null && !cell.equalsIgnoreCase("") && !cell.equalsIgnoreCase("null")) {
            cdl.add(getContactDetail(request, "ContactDetails_cell", 3));
        }
        if (work != null && !work.equalsIgnoreCase("") && !work.equalsIgnoreCase("null")) {
            cdl.add(getContactDetail(request, "ContactDetails_telNoWork", 4));
        }
        if (home != null && !home.equalsIgnoreCase("") && !home.equalsIgnoreCase("null")) {
            cdl.add(getContactDetail(request, "ContactDetails_telNoHome", 5));
        }
        return cdl;
    }

    private static ContactDetails getContactDetail(HttpServletRequest request, String param, int commType) {
        ContactDetails cd = new ContactDetails();
        cd.setCommunicationMethodId(commType);
        cd.setMethodDetails(request.getParameter(param));
        cd.setPrimaryIndicationId(1);

        return cd;
    }

    public static void setContactDetails(HttpServletRequest request, Collection<ContactDetails> contactDetails) {
        if (contactDetails == null) {
            return;
        }
        for (ContactDetails cd : contactDetails) {
            if (cd == null) {
                continue;
            }
            String fieldStart = "";
            fieldStart = cd.getCommunicationMethodId() == 1 ? "ContactDetails_email" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 2 ? "ContactDetails_faxNo" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 3 ? "ContactDetails_cell" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 4 ? "ContactDetails_telNoWork" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 5 ? "ContactDetails_telNoHome" : fieldStart;
            request.setAttribute(fieldStart, cd.getMethodDetails() == null ? "" : cd.getMethodDetails());

        }
    }

    /**
     * ************************ADDRESS DETAILS************************
     * @param request
     * @param neoUser
     */
    public static List<AddressDetails> getAddressDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<AddressDetails> cdl = new ArrayList<AddressDetails>();
        cdl.add(getAddressDetail(request, "AddressDetails_pos_", 2));
        cdl.add(getAddressDetail(request, "AddressDetails_res_", 1));
        return cdl;
    }

    private static AddressDetails getAddressDetail(HttpServletRequest request, String param, int addrType) {
        AddressDetails ad = new AddressDetails();
        ad.setAddressTypeId(addrType);
        ad.setPrimaryInd("Y");
        ad.setAddressUse(1);
        ad.setAddressLine1(request.getParameter(param + "addressLine1"));
        ad.setAddressLine2(request.getParameter(param + "addressLine2"));
        ad.setAddressLine3(request.getParameter(param + "addressLine3"));
        ad.setPostalCode(request.getParameter(param + "postalCode"));
        return ad;
    }

    public static void setAddressDetails(HttpServletRequest request, Collection<AddressDetails> addressDetails) {
        if (addressDetails == null) {
            return;
        }
        for (AddressDetails ad : addressDetails) {
            if (ad == null) {
                continue;
            }
            String fieldStart = ad.getAddressTypeId() == 1 ? "AddressDetails_res_" : "AddressDetails_pos_";
            request.setAttribute(fieldStart + "addressLine1", ad.getAddressLine1());
            request.setAttribute(fieldStart + "addressLine2", ad.getAddressLine2());
            request.setAttribute(fieldStart + "addressLine3", ad.getAddressLine3());
            request.setAttribute(fieldStart + "postalCode", ad.getPostalCode());
        }
    }

    /**
     * ************************BANKING DETAILS************************
     * @param request
     * @param neoUser
     * @param entityId
     * @return 
     */
    public static List<BankingDetails> getBankingDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<BankingDetails> cdl = new ArrayList<BankingDetails>();
        /*
         * if (request.getParameter("BankingDetails_contri_bankName") != null &&
         * !request.getParameter("BankingDetails_contri_bankName").isEmpty()) {
         * cdl.add(getBankingDetails(request, "BankingDetails_contri_", 2)); }
         * if (request.getParameter("BankingDetails_pay_bankName") != null &&
         * !request.getParameter("BankingDetails_pay_bankName").isEmpty()) {
         * cdl.add(getBankingDetails(request, "BankingDetails_pay_", 1)); }
         */

        cdl.add(getBankingDetails(request, "BankingDetails_", 3));

        //cdl.add(getBankingDetails(request, "BankingDetails_pay_", 1));
        return cdl;
    }

    private static BankingDetails getBankingDetails(HttpServletRequest request, String param, int accUse) {
        BankingDetails bd = new BankingDetails();
        String paymentMethod = request.getParameter(param + "paymentMethod");

        bd.setPaymentMethodId(paymentMethod);
        bd.setAccountUseId(accUse);
        if (request.getParameter(param + "bankName") != null && !request.getParameter(param + "bankName").isEmpty()) {
            bd.setBankId(Integer.parseInt(request.getParameter(param + "bankName")));
        }
        if (request.getParameter(param + "bankBranchId") != null && !request.getParameter(param + "bankBranchId").isEmpty()) {
            bd.setBranchId(Integer.parseInt(request.getParameter(param + "bankBranchId")));
        }
        bd.setAccountTypeId(request.getParameter(param + "accountType"));
        bd.setAccountName(request.getParameter(param + "accountHolder"));
        bd.setAccountNo(request.getParameter(param + "accountNumber"));

        return bd;
    }

    public static void setBankingDetails(HttpServletRequest request, Collection<BankingDetails> bankingDetails) {
        if (bankingDetails == null) {
            return;
        }
        for (BankingDetails bd : bankingDetails) {
            if (bd == null) {
                continue;
            }
            String fieldStart = "BankingDetails_";
            System.out.println("Payment from database " + bd.getPaymentMethodId() + " : " + fieldStart + "paymentMethod");
            if (bd.getAccountUseId() == 3) {
                request.setAttribute(fieldStart + "paymentMethod", bd.getPaymentMethodId());
                request.setAttribute(fieldStart + "bankName", bd.getBankId());
                request.setAttribute(fieldStart + "bankBranchId", bd.getBranchId());
                request.setAttribute(fieldStart + "accountType", bd.getAccountTypeId());
                request.setAttribute(fieldStart + "accountHolder", bd.getAccountName());
                request.setAttribute(fieldStart + "accountNumber", bd.getAccountNo());
            }
        }
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String pracNum = request.getSession().getAttribute("provNum") + "";
        ProviderDetails practiceDetails = port.getPracticeDetailsForEntityByNumber(pracNum);
        if(practiceDetails.getCashPractice().getId().equalsIgnoreCase("1")){
            request.getSession().setAttribute("BankingDetails_Cash_Practice_checked", "true");
        } else {
            request.getSession().setAttribute("BankingDetails_Cash_Practice_checked", "false");
        }
    }

    /**
     * ************************CONTACT PREFERENCE
     * DETAILS************************
     * @param request
     * @param neoUser
     * @param entityId
     * @return 
     */
    public static List<ContactPreference> getContactPrefDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<ContactPreference> cdl = new ArrayList<ContactPreference>();

        String claimProc = request.getParameter("ConPrefDetails_claimProc");
        String contriStmt = request.getParameter("ConPrefDetails_contriStmt");
        String claimStmt = request.getParameter("ConPrefDetails_claimStmt");
        String claimPay = request.getParameter("ConPrefDetails_claimPay");
        String taxCert = request.getParameter("ConPrefDetails_taxCert");
        String preauth = request.getParameter("ConPrefDetails_preauth");
        String other = request.getParameter("ConPrefDetails_other");

        if (claimProc != null && !claimProc.equalsIgnoreCase("") && !claimProc.equalsIgnoreCase("99")) {
            cdl.add(getContactPrefDetails(request, "ConPrefDetails_claimProc", 1));
        }
        if (contriStmt != null && !contriStmt.equalsIgnoreCase("") && !contriStmt.equalsIgnoreCase("99")) {
            cdl.add(getContactPrefDetails(request, "ConPrefDetails_contriStmt", 2));
        }
        if (claimStmt != null && !claimStmt.equalsIgnoreCase("") && !claimStmt.equalsIgnoreCase("99")) {
            cdl.add(getContactPrefDetails(request, "ConPrefDetails_claimStmt", 3));
        }
        if (claimPay != null && !claimPay.equalsIgnoreCase("") && !claimPay.equalsIgnoreCase("99")) {
            cdl.add(getContactPrefDetails(request, "ConPrefDetails_claimPay", 4));
        }
        if (taxCert != null && !taxCert.equalsIgnoreCase("") && !taxCert.equalsIgnoreCase("99")) {
            cdl.add(getContactPrefDetails(request, "ConPrefDetails_taxCert", 5));
        }
        if (preauth != null && !preauth.equalsIgnoreCase("") && !preauth.equalsIgnoreCase("99")) {
            cdl.add(getContactPrefDetails(request, "ConPrefDetails_preauth", 6));
        }
        if (other != null && !other.equalsIgnoreCase("") && !other.equalsIgnoreCase("99")) {
            cdl.add(getContactPrefDetails(request, "ConPrefDetails_other", 7));
        }

        return cdl;
    }

    private static ContactPreference getContactPrefDetails(HttpServletRequest request, String param, int commType) {
        ContactPreference cd = new ContactPreference();
        cd.setCommunicationTypeId(commType);

        String conPrefType = request.getParameter(param);

        if (conPrefType != null && !conPrefType.equalsIgnoreCase("") && !conPrefType.equalsIgnoreCase("99")) {
            String type = conPrefType.substring(0, 1);
            int pref = Integer.parseInt(conPrefType.substring(1, 2));

            cd.setContactAddressType(type.equals("A") ? pref : 0);
            cd.setContactDetailType(type.equals("C") ? pref : 0);

        } else {
            cd.setContactAddressType(0);
            cd.setContactDetailType(0);
        }

        return cd;
    }

    public static void setContactPrefDetails(HttpServletRequest request, Collection<ContactPreference> conPref) {
        if (conPref == null) {
            return;
        }
        for (ContactPreference cd : conPref) {
            if (cd == null) {
                continue;
            }
            String fieldStart = "";
            fieldStart = cd.getCommunicationTypeId() == 1 ? "ConPrefDetails_claimProc" : fieldStart;
            fieldStart = cd.getCommunicationTypeId() == 2 ? "ConPrefDetails_contriStmt" : fieldStart;
            fieldStart = cd.getCommunicationTypeId() == 3 ? "ConPrefDetails_claimStmt" : fieldStart;
            fieldStart = cd.getCommunicationTypeId() == 4 ? "ConPrefDetails_claimPay" : fieldStart;
            fieldStart = cd.getCommunicationTypeId() == 5 ? "ConPrefDetails_taxCert" : fieldStart;
            fieldStart = cd.getCommunicationTypeId() == 6 ? "ConPrefDetails_preauth" : fieldStart;
            fieldStart = cd.getCommunicationTypeId() == 7 ? "ConPrefDetails_other" : fieldStart;

            String commMethod = "";
            if (cd.getContactAddressType() != 0) {
                commMethod = "A" + cd.getContactAddressType();
            }
            if (cd.getContactDetailType() != 0) {
                commMethod = "C" + cd.getContactDetailType();
            }

            request.setAttribute(fieldStart, commMethod);

        }
    }

    /*Setting Practice Details*/
    public static void setPracticeDetails(HttpServletRequest request, Collection<PracticeCaptureDetails> practiceDetails) {
        if (practiceDetails == null) {
            return;
        }
        for (PracticeCaptureDetails pd : practiceDetails) {
            if (pd == null) {
                continue;
            }
            String fieldStart = "PracticeDetails_";
            if (pd.getPracticeType().equalsIgnoreCase("3")) {
                request.setAttribute(fieldStart + "discipline", pd.getDiscipline());
                request.setAttribute(fieldStart + "subDiscipline", pd.getSubDiscipline());
                request.setAttribute(fieldStart + "practiceNo", pd.getPracticeNo());
                request.setAttribute(fieldStart + "providerNo", pd.getProviderNo());
                request.setAttribute(fieldStart + "practiceName", pd.getPracticeName());
                request.setAttribute(fieldStart + "groupInd", pd.getGroupInd());
                request.setAttribute(fieldStart + "numberOfPartners", pd.getNumberOfPartners());
                request.setAttribute(fieldStart + "effectiveDate", pd.getEffectiveDate());
                request.setAttribute(fieldStart + "cashPractice", pd.getCashPractice());
                request.setAttribute(fieldStart + "entityCommId", pd.getPracticeEntityCommonId());
                request.setAttribute(fieldStart + "practiceType", pd.getPracticeType());
                request.setAttribute("oldRestriction", pd.getCashPractice());
            } else {
                request.setAttribute(fieldStart + "councilNo", pd.getCouncilNo());
                request.setAttribute(fieldStart + "providerTitle", pd.getProviderTitle());
                request.setAttribute(fieldStart + "providerInitials", pd.getProviderInitials());
                request.setAttribute(fieldStart + "providerFirstName", pd.getProviderFirstName());
                request.setAttribute(fieldStart + "providerSurname", pd.getProviderSurname());
                request.setAttribute(fieldStart + "providerIdNo", pd.getProviderIdNo());
            }
        }
    }

    /*getting EntityID*/
    public static int getEntityId(HttpServletRequest request) {
        return (Integer) request.getSession().getAttribute("provNumEntityId");
    }
}

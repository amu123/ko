/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.utils;


import com.koh.command.NeoCommand;
import com.koh.utils.FormatUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import neo.manager.AuthMiniAuditDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class KPIntegration {

    private static NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

    public KPIntegration() {
    }

    public static boolean reprocessAuth(String authNumber, int authId) {
        boolean reprocess = false; 

        List<AuthMiniAuditDetails> maList = port.returnMiniAuditByAuthNumber(authNumber);

        if (!maList.isEmpty()) {

            AuthMiniAuditDetails newAuth = maList.get(0);
            AuthMiniAuditDetails oldAuth = maList.get(1);

            HashMap<String, Integer> oldCptMap = new HashMap<String, Integer>();
            HashMap<String, Integer> oldTariffMap = new HashMap<String, Integer>();
            boolean foundNewCpt = false;
            boolean foundNewTariff = false;
            boolean foundNewCptMod = false;
            int changedValueCount = 0;

            //auth status
            String newAuthStatus = newAuth.getAuthStatus();
            String oldAuthStatus = oldAuth.getAuthStatus();

            //first validate if new status was changed Accept or Reject
            if (!oldAuthStatus.equalsIgnoreCase(newAuthStatus)) {
                if (newAuthStatus.equalsIgnoreCase("1") || newAuthStatus.equalsIgnoreCase("2")) {

                    if (newAuthStatus.equalsIgnoreCase("1")) {
                        port.deleteAuthFirings(authId);
                    }

                    System.out.println("status change, not sending to KP - OLD " + oldAuthStatus + ", NEW " + newAuthStatus);
                    return reprocess;
                }
            }

            //auth start
            Date newStart = newAuth.getAuthStart().toGregorianCalendar().getTime();
            Date oldStart = oldAuth.getAuthStart().toGregorianCalendar().getTime();

            //auth end
            Date newEnd = newAuth.getAuthEnd().toGregorianCalendar().getTime();
            Date oldEnd = oldAuth.getAuthEnd().toGregorianCalendar().getTime();

            //Primary icd
            String newICD = newAuth.getPrimaryICD();
            String oldICD = oldAuth.getPrimaryICD();
            
            //pmb
            String newPMB = newAuth.getPmbCodes();
            String oldPMB = oldAuth.getPmbCodes();

            //provider
            String newProvider = newAuth.getProviderNumber();
            String oldProvider = oldAuth.getProviderNumber();

            //cpt
            String[] newCpts = null;
            if (newAuth.getCptCodes() != null && !newAuth.getCptCodes().equalsIgnoreCase("")) {
                newCpts = newAuth.getCptCodes().split(",");
            }
            String[] oldCpts = null;
            if (oldAuth.getCptCodes() != null && !oldAuth.getCptCodes().equalsIgnoreCase("")) {
                oldCpts = oldAuth.getCptCodes().split(",");
            }

            //tariffs
            String[] newTariffs = null;
            if (newAuth.getTariffCodes() != null && !newAuth.getTariffCodes().equalsIgnoreCase("")) {
                newTariffs = newAuth.getTariffCodes().split(",");
            }
            String[] oldTariffs = null;
            if (oldAuth.getTariffCodes() != null && !oldAuth.getTariffCodes().equalsIgnoreCase("")) {
                oldTariffs = oldAuth.getTariffCodes().split(",");
            }

            //validate auth start
            if (!oldStart.equals(newStart)) {
                System.out.println("oldStart (" + FormatUtils.dateTimeFormat.format(oldStart) + ") changed to - " + FormatUtils.dateTimeFormat.format(newStart));
                changedValueCount++;
            }

            //validate auth end 
            if (!oldEnd.equals(newEnd)) {
                System.out.println("oldEnd (" + FormatUtils.dateTimeFormat.format(oldEnd) + ") changed to - " + FormatUtils.dateTimeFormat.format(newEnd));
                changedValueCount++;
            }

            //validate Primary ICD
            if (!oldICD.trim().equalsIgnoreCase(newICD)) {
                System.out.println("oldICD (" + oldICD + ") changed to - " + newICD);
                changedValueCount++;
            }
            //validate PMB for override
            if(oldPMB == null){
                oldPMB = "";
            }
            if(newPMB == null){
                newPMB = "";
            }
            
            if (newPMB.equalsIgnoreCase("") && !oldPMB.equalsIgnoreCase("")) {
                System.out.println("pmb override trigger");
                changedValueCount++;
            }
            if (oldPMB.equalsIgnoreCase("") && !newPMB.equalsIgnoreCase("")) {
                System.out.println("pmb added on update");
                changedValueCount++;
            }

            //validate Provider
            if (!oldProvider.trim().equalsIgnoreCase(newProvider)) {
                System.out.println("oldProvider (" + oldProvider + ") changed to - " + newProvider);
                changedValueCount++;
            }

            //set old CPTs
            int cptNullCount = 0;
            if (oldCpts != null) {
                for (int x = 0; x < oldCpts.length; x++) {
                    String cpt = oldCpts[x];
                    if (cpt != null && !cpt.equalsIgnoreCase("")) {
                        oldCptMap.put(cpt, x + 1);
                        System.out.println("old cpts = " + cpt);
                    }
                }
            } else {
                cptNullCount++;
            }
            //check old if new exist
            if (newCpts != null) {
                for (int i = 0; i < newCpts.length; i++) {
                    String cpt = newCpts[i];
                    if (cpt != null && !cpt.equalsIgnoreCase("")) {
                        boolean checkMap = oldCptMap.containsKey(cpt);
                        System.out.println("cpt checkMap = " + checkMap);
                        if (!checkMap) {
                            foundNewCpt = true;
                            changedValueCount++;
                            break;
                        }
                    }
                }
            } else {
                cptNullCount++;
            }

            //set old tariff
            int tariffNullCount = 0;
            if (oldTariffs != null) {
                for (int x = 0; x < oldTariffs.length; x++) {
                    String tariff = oldTariffs[x];
                    if (tariff != null && !tariff.equalsIgnoreCase("")) {
                        oldTariffMap.put(tariff, x + 1);
                    }
                }
            } else {
                tariffNullCount++;
            }
            //check old if new exist
            if (newTariffs != null) {
                for (int i = 0; i < newTariffs.length; i++) {
                    String tariff = newTariffs[i];
                    if (tariff != null && !tariff.equalsIgnoreCase("")) {
                        boolean checkMap = oldTariffMap.containsKey(tariff);
                        if (!checkMap) {
                            foundNewTariff = true;
                            changedValueCount++;
                            break;
                        }
                    }
                }
            } else {
                tariffNullCount++;
            }
            System.out.println("foundNewCpt = " + foundNewCpt);
            System.out.println("foundNewTariff = " + foundNewTariff);
            System.out.println("foundNewCptMod = " + foundNewCptMod);

            //CPT
            if (foundNewCpt || cptNullCount == 1) {
                changedValueCount++;
            }
            if (cptNullCount == 0) {
                if (newCpts.length != oldCpts.length) {
                    changedValueCount++;
                }
            }

            //TARIFFS
            if (foundNewTariff || tariffNullCount == 1) {
                changedValueCount++;
            }
            if (tariffNullCount == 0) {
                if (newTariffs.length != newTariffs.length) {
                    changedValueCount++;
                }
            }

            if (changedValueCount > 0) {
                reprocess = true;
            }

        }
        return reprocess;
    }


}

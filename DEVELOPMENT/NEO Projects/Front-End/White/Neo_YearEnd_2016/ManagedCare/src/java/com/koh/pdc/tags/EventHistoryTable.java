/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.MainList;
import com.koh.command.NeoCommand;
import java.util.Collection;
import java.util.Date;
import neo.manager.NeoUser;
import neo.manager.PdcSearchCriteria;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;

/**
 *
 * @author princes
 */
public class EventHistoryTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    private int minResult;
    private int maxResult = 100;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        //ArrayList<MainList> list = (ArrayList<MainList>) session.getAttribute("eventHistory");
//        String client = session.getAttribute("Client").toString();
       ServletContext sctx = pageContext.getServletContext();
       String client = String.valueOf(sctx.getAttribute("Client"));
        
        Collection<MainList> mainList = new ArrayList<MainList>();

        PdcSearchCriteria searchS = (PdcSearchCriteria) session.getAttribute("searchHistory");

        NeoUser user;
        String depenNo = (String) session.getAttribute("dependentNumber").toString();
        String coverNumber = (String) session.getAttribute("policyNumber");

        Format formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String createdDate;
        //String pageDirection = request.getParameter("pageDirection");

        boolean forwardButton;
        boolean backwardButton;

        PdcSearchCriteria search = new PdcSearchCriteria();

        if (searchS != null) {
            search = searchS;
        } else {
            minResult = minResult + 0;
            maxResult = maxResult + 0;
            search.setMinResult(minResult);
            search.setMaxResult(maxResult);
            search.setDependentNumber(new Integer(depenNo));
            search.setCoverNumber(coverNumber);
            session.setAttribute("searchHistory", search);
        }

        mainList = NeoCommand.service.getNeoManagerBeanPort().getPolicyHolderHistoryEvents(search);

        session.setAttribute("mainList", mainList);

        try {

            out.print("<tr>");
            if (client.equalsIgnoreCase("Sechaba")) {
                out.print("<th scope=\"col\">PCC Type</th>");
            }else{
                out.print("<th scope=\"col\">PDC Type</th>");
            }
            out.print("<th scope=\"col\">Event Source</th>");
            out.print("<th scope=\"col\">Event Date</th>");
            out.print("<th scope=\"col\">User Actioned</th>");
            out.print("<th scope=\"col\">Risk Rating</th>");
            out.print("<th scope=\"col\">Description</th>");
            out.print("<th scope=\"col\">Risk Rating Reason</th>");
            out.print("<th scope=\"col\">Rule Number</th>");
            out.print("<th scope=\"col\">Notes</th>");
            out.print("</tr>");

            for (MainList eh : mainList) {
                out.print("<tr>");
                out.print("<td><label class=\"label\">" + eh.getEventType().getValue() + "</label></td>");
                out.print("<td><label class=\"label\">" + eh.getEventSource().getValue() + "</label></td>");
                Date create = eh.getCreatedDate().toGregorianCalendar().getTime();
                createdDate = formatter.format(create);
                out.print("<td><label class=\"label\">" + createdDate + "</label></td>");
                user = NeoCommand.service.getNeoManagerBeanPort().getUserSafeDetailsById(eh.getAssignedBy());

                if (user.getUsername() != null) {
                    out.print("<td><label class=\"label\">" + user.getUsername() + "</label></td>");
                } else {
                    out.print("<td></td>");
                }

                out.print("<td><label class=\"label\">" + eh.getEventRisk() + "</label></td>");
                out.print("<td><label class=\"label\">" + eh.getEventDescription() + "</label></td>");
                out.print("<td><label class=\"label\">" + eh.getEventRatingReason() + "</label></td>");

                if (eh.getRuleNumber() != 0) {
                    out.print("<td><label class=\"label\">" + eh.getRuleNumber() + "</label></td>");
                } else {
                    out.print("<td></td>");
                }

                if (eh.getRiskRatingNotes() != null && !eh.getRiskRatingNotes().equalsIgnoreCase("")) {
                    out.print("<td><label class=\"label\">" + eh.getRiskRatingNotes() + "</label></td>");
                } else {
                    out.print("<td></td>");
                }

                out.print("</tr>");
            }

            if (mainList != null && mainList.size() >= 100) {
                forwardButton = true;
                backwardButton = true;

                if (backwardButton || forwardButton) {
                    out.println("<tr><th colspan=\"9\" align=\"right\" >");

                    if (backwardButton) {
                        out.println("<button name=\"opperation\" type=\"button\" onClick=\"submitPageAction('NextEventHistoryListCommand', 'backward')\"; value=\"\">Previous</button>");
                    }
                    if (forwardButton) {
                        out.println("<button name=\"opperation\" type=\"button\" onClick=\"submitPageAction('NextEventHistoryListCommand', 'forward')\"; value=\"\">Next</button>");
                    }

                    out.println("</th></tr>");
                }

            }

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.contribution.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.MapUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author yuganp
 */
public class PaymentAllocationCommand extends NeoCommand{

    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            String s = request.getParameter("command");
            if (s != null && !s.isEmpty()) {
                System.out.println("command : " + s);
                if (s.equalsIgnoreCase("SaveMember")) {
                    saveMember(request, response, context);
                } else if (s.equalsIgnoreCase("SaveEmployer")) {
                    saveEmployer(request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(PaymentAllocationCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "PaymentAllocationCommand";
    }

    private void saveMember(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String contribPrefix = "contrib_";
        List<Map<String,String>> aList = new ArrayList<Map<String,String>>();
        String coverNo = request.getParameter("coverNumber");
        int entityId = TabUtils.getIntParam(request, "entityId");
        Map pMap = request.getParameterMap();
        for ( Object p : pMap.keySet()) {
            String key = p.toString();
            if (key != null && key.startsWith(contribPrefix)) {
                String idParts = key.substring(contribPrefix.length());
                if (idParts != null && idParts.length() >= 8) {
                    String[] idPartArr = idParts.split("_");
                    idParts = idParts.substring(0,4) + "/" + idParts.substring(4,6) + "/" + idParts.substring(6);
                    
                    String value = request.getParameter(key);
                    if (value != null && !value.isEmpty()) {
                        Map<String,String> map = new HashMap<String, String>();
                        map.put("date", idParts);
                        map.put("amount", value);
                        map.put("product", idPartArr[1]);
                        map.put("option", idPartArr[2]);
                        map.put("entityId", idPartArr[3]);
                        map.put("entityTypeId",idPartArr[4]);
                        aList.add(map);
                    }
                }
            }
        }
//        for (Map<String,String> map : aList) {
//            for (String s : map.keySet()) {
//                System.out.println (s + " = " + map.get(s));
//            }
//        }
        Integer receiptId = null;
        String receiptStr = request.getParameter("ContribReceiptsId");
        if (receiptStr != null && !receiptStr.isEmpty()) {
            String[] sArr = receiptStr.split("_");
            if (sArr != null && sArr.length == 2) {
                try {
                    receiptId = Integer.parseInt(sArr[0]);
                } catch (Exception e) {
                }
            }
        }        
        List<KeyValueArray> kList = MapUtils.getKeyValueArray(aList);
        int result = port.savePaymentAllocation(2, entityId, coverNo, kList,receiptId, TabUtils.getSecurity(request));
        System.out.println(result);
    }

    private void saveEmployer(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String contribPrefix = "contrib_";
        List<Map<String,String>> aList = new ArrayList<Map<String,String>>();
        int entityId = TabUtils.getIntParam(request, "entityId");
        Map pMap = request.getParameterMap();
        for ( Object p : pMap.keySet()) {
            String key = p.toString();
//            System.out.println(key + " = " +request.getParameter(key).toString());
            if (key != null && key.startsWith(contribPrefix)) {
                String idParts = key.substring(contribPrefix.length());
//                String product = request.getParameter("product_" + idParts);
//                String option = request.getParameter("option_" + idParts);
//                String cover = request.getParameter("cover_" + idParts);
                if (idParts != null && idParts.length() >= 8) {
                    String date = idParts.substring(0,4) + "/" + idParts.substring(4,6) + "/" + idParts.substring(6,8);
                    String product = "1";
                    String[] idPartArr = idParts.split("_");
                    String cover = idPartArr[1];
                    String option = idPartArr[2];
//                    idParts = idParts.substring(0,4) + "/" + idParts.substring(4,6) + "/" + idParts.substring(6);
                    String value = request.getParameter(key);
                    if (value != null && !value.isEmpty()) {
                        Map<String,String> map = new HashMap<String, String>();
                        map.put("date", date);
                        map.put("amount", value);
                        map.put("product", product);
                        map.put("option", option);
                        map.put("cover", cover);
                        aList.add(map);
                        
                    }
                }
            }
        }
//        for (Map<String,String> map : aList) {
//            for (String s : map.keySet()) {
//                System.out.println (s + " = " + map.get(s));
//            }
//        }
        Integer receiptId = null;
        String receiptStr = request.getParameter("ContribReceiptsId");
        if (receiptStr != null && !receiptStr.isEmpty()) {
            String[] sArr = receiptStr.split("_");
            if (sArr != null && sArr.length == 2) {
                try {
                    receiptId = Integer.parseInt(sArr[0]);
                } catch (Exception e) {
                }
            }
        }
        List<KeyValueArray> kList = MapUtils.getKeyValueArray(aList);
        int result = port.savePaymentAllocation(1, entityId, null, kList, receiptId, TabUtils.getSecurity(request));
        System.out.println(result);
    }
    
}

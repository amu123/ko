/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthHistoryNotes;

/**
 *
 * @author Johan-NB
 */
public class HistoryNotesSearchResult extends TagSupport {

    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        ServletRequest req = pageContext.getRequest();

        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");

            String message = (String) req.getAttribute("authHNResultsMessage");

            if (message != null && !message.equalsIgnoreCase("")) {
                out.println("<label class=\"red\">" + message + "</label></br></br>");
            }

            out.println("<table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Member Number</th><th>Dep.</th><th align=\"left\">Created By</th>"
                    + "<th>Creation Date</th><th align=\"left\">Notes</th></tr>");

            List<AuthHistoryNotes> noteList = (List<AuthHistoryNotes>) session.getAttribute("authHistoryNotes");
            if (noteList != null && noteList.isEmpty() == false) {
                for (AuthHistoryNotes notes : noteList) {
                    String creationDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(notes.getCreationDate().toGregorianCalendar().getTime());

                    out.println("<tr>");
                    out.println("<td width=\"80\"><label class=\"label\">" + notes.getCoverNumber() + "</label></td>");
                    out.println("<td width=\"45\"><label class=\"label\">" + notes.getDependantCode() + "</label></td>");
                    out.println("<td width=\"80\"><label class=\"label\">" + notes.getCreatedBy() + "</label></td>");
                    out.println("<td width=\"80\"><label class=\"label\">" + creationDate + "</label></td>");
                    out.println("<td width=\"500\"><label class=\"label\">" + notes.getNotes() + "</label></td>");
                    out.println("</tr>");

                }
            }

            out.println("</table>");


        } catch (java.io.IOException ex) {
            throw new JspException("Error in HistoryNotesSearchResult tag", ex);
        }
        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

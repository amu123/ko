/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import com.koh.utils.MapUtils;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.KeyValue;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author yuganp
 */
public class MemberApplicationSearchCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        request.getAttribute("memberAppStatus"); //getApplicationStatus());
        boolean viewStaff = (Boolean) session.getAttribute("persist_user_viewStaff");

        try {
            String name = request.getParameter("memberName");
            String surname = request.getParameter("memberSurname");
            String idNum = request.getParameter("memberIdNum");
            String number = request.getParameter("memberNumber");
            String userName = request.getParameter("userName");


            List<KeyValueArray> kva = port.getMemberApplicationList(number, idNum, name, surname, userName);
            //System.out.println("Size " + kva.size());
            List<Map<String, String>> list = MapUtils.getMap(kva);
            List<Map<String, String>> newList = new ArrayList<Map<String, String>>();
            for (Map<String, String> map : list) {
                if (!viewStaff) {
                    if (!"2".equalsIgnoreCase(map.get("memberCategory"))) {
                        newList.add(map);
                    }
                } else {
                    newList.add(map);
                }
            }
            if (!viewStaff) {
                if (kva != null && newList != null) {
                    if (kva.size() > 0) {
                        if (newList.size() == 0) {
                            request.setAttribute("appSearchResultsMessage", "Yes");
                        }
                    }
                }
            }
            
            request.setAttribute("MemAppSearchResults", newList);
            if (userName != null && !userName.isEmpty()) {
                if (userName.length() < 3) {
                    request.setAttribute("userLength", "Yes");
                    request.setAttribute("MemAppSearchResults", null);
                }
            }            
            session.setAttribute("throughAppSearch", "yes");
            context.getRequestDispatcher("/MemberApplication/MemberAppSearchResults.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(MemberApplicationSearchCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "MemberApplicationSearchCommand";
    }

    public static Map<String, String> getMap(KeyValueArray kva) {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (KeyValue kv : kva.getKeyValList()) {
            map.put(kv.getKey(), kv.getValue());
        }
        return map;
    }
}

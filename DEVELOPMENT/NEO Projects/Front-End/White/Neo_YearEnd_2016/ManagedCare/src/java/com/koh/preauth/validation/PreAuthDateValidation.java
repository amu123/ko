/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.validation;

import com.koh.command.Command;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverDetails;
import neo.manager.ProviderDetails;

/**
 *
 * @author Johan-NB
 */
public class PreAuthDateValidation extends Command {

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    private boolean futureDateError = false;
    private boolean monthPeriodError = false;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;

        String dateFRec = request.getParameter("dateF").trim();
        String dateTRec = request.getParameter("dateT").trim();
        String fromElement = request.getParameter("fe").trim();
        String toElement = request.getParameter("te").trim();
        String authType = request.getParameter("authType").trim();
        String authPeriod = request.getParameter("authPeriod").trim();
        String authMonthPeriod = request.getParameter("authMonthPeriod").trim();

        String skipPeriod = request.getParameter("skip");

        //get values from session if request is null
        if (authType == null || authType.equalsIgnoreCase("null")) {
            authType = "" + session.getAttribute("authType");
        }
        if (authPeriod == null || authPeriod.equalsIgnoreCase("null")) {
            authPeriod = "" + session.getAttribute("authPeriod");
        }
        if (authMonthPeriod == null || authMonthPeriod.equalsIgnoreCase("null")) {
            System.out.println("PreAuthDateValidation month period = " + authMonthPeriod);
            authMonthPeriod = "" + session.getAttribute("authMonthPeriod");
        }
        System.out.println("authPeriod = " + authPeriod);

        boolean valid = true;
        String returned = DateValidation(dateFRec, dateTRec, fromElement, toElement, authType, authPeriod, authMonthPeriod, skipPeriod);
        String[] vals = returned.split("\\|");
        valid = Boolean.parseBoolean(vals[0]);
        System.out.println("valid validation returned - " + valid);
        String errors = returned.substring(returned.indexOf("|") + 1, returned.length());

        //return response
        try {
            out = response.getWriter();
            if (valid) {
                out.println("Done|");
            } else {
                out.println("Error|" + errors);
            }

        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            out.close();
        }

        return null;
    }

    @Override
    public String getName() {
        return "PreAuthDateValidation";
    }

    public String DateValidation(String dateFRec, String dateTRec, String fromElement,
            String toElement, String authType, String authPeriod, String authMonthPeriod,
            String skipPeriod) {

        boolean hospFlag = false;
        boolean validDate = true;
        String returnStr = "";
        String errorMsg = "";

        if (fromElement != null && !fromElement.equalsIgnoreCase("")) {
            if (dateFRec != null && !dateFRec.equalsIgnoreCase("")) {
                //validate from date format
                validDate = checkFormat(dateFRec);
                if (validDate) {
                    //check if date is the the range of the auth period selected
                    //if (skipPeriod.trim().equalsIgnoreCase("no")) {
                    //validDate = validateAuthPeriod(dateFRec, authPeriod, authMonthPeriod);
                    //}

                    if (validDate) {
                        //if (!authType.equalsIgnoreCase("12") && !authType.trim().equals("11")) {
                        //  validDate = validateDateTime(dateFRec);
                        //hospFlag = true;
                        //} else {
                        validDate = validateDate(dateFRec);
                        //}

                        if (validDate) {
                            //check if from date was captured
                            if (dateTRec != null && !dateTRec.equalsIgnoreCase("") && !dateTRec.equalsIgnoreCase("undefined")) {
                                SimpleDateFormat format = null;

                                //validate from date format
                                validDate = checkFormat(dateTRec);

                                if (validDate) {
                                    //validate end date
                                    //if (hospFlag) {
                                    //validDate = validateDateTime(dateTRec);
                                    //format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                                    //} else {
                                    validDate = validateDate(dateTRec);
                                    format = new SimpleDateFormat("yyyy/MM/dd");
                                    //}

                                    //validate from date against end date
                                    if (validDate) {
                                        validDate = validateBeforeDate(format, dateFRec, dateTRec);
                                        if (!validDate) {
                                            errorMsg = errorMsg + "|" + toElement + ":Incorrect Date (End Before Start)";
                                        }
                                    }
                                } else {
                                    System.out.println("date validation from date not within auth period");
                                    errorMsg = errorMsg + "|" + toElement + ":Incorrect Date Format";
                                    validDate = false;
                                }

//                                if (validDate) {
//                                    if (!fromElement.trim().equalsIgnoreCase("tariffDate")) {
//                                        //validate member dates
//                                        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("covMemListDetails");
//                                        String selectedCD = "" + session.getAttribute("depListValues");
//                                        CoverDetails ac = new CoverDetails();
//                                        for (CoverDetails cd : cdList) {
//                                            if (cd.getCoverNumber().equalsIgnoreCase(selectedCD)) {
//                                                ac = cd;
//                                                break;
//                                            }
//                                        }
//                                        Date authStart = null;
//                                        Date authEnd = null;
//                                        Calendar aStart = Calendar.getInstance();
//                                        Calendar aEnd = Calendar.getInstance();
//                                        boolean resignation = false;
//
//                                        if (ac.getCoverEndDate() != null) {
//                                            Date coverStart = ac.getSchemeJoinDate().toGregorianCalendar().getTime();
//
//                                            try {
//                                                authStart = format.parse(dateFRec);
//                                                authEnd = format.parse(dateFRec);
//                                            } catch (ParseException ex) {
//                                                Logger.getLogger(PreAuthDateValidation.class.getName()).log(Level.SEVERE, null, ex);
//                                            }
//
//                                            Calendar acStart = Calendar.getInstance();
//
//                                            acStart.setTime(coverStart);
//
//                                            aStart.setTime(authStart);
//                                            aEnd.setTime(authEnd);
//
//                                            if (aStart.before(acStart)) {
//                                                System.out.println("cover start after auth start");
//                                                resignation = true;
//
//                                            }
//                                        }
//
//                                        if (ac.getCoverEndDate() != null) {
//
//                                            Date coverEnd = ac.getCoverEndDate().toGregorianCalendar().getTime();
//                                            Calendar acEnd = Calendar.getInstance();
//                                            acEnd.setTime(coverEnd);
//
//                                            if (aEnd.after(acEnd)) {
//                                                System.out.println("cover end before auth end");
//                                                resignation = true;
//
//                                            }
//                                        }
//
//                                        if (resignation) {
//                                            validDate = false;
//                                            if (authType.trim().equalsIgnoreCase("12") || authType.trim().equals("11")) {
//                                                errorMsg = errorMsg + "|memberNum:Member not active for auth period";
//                                            } else {
//                                                errorMsg = errorMsg + "|" + toElement + ":Member not active for auth period";
//                                            }
//                                        }
//                                    }
//                                }

                            }

                        }

                    } else {
                        if (futureDateError) {
                            System.out.println("date validation from date to far in the future");
                            errorMsg = errorMsg + "|" + fromElement + ":Incorrect Date (Future From Date further than 4 months)";
                        } else {
                            if (monthPeriodError) {
                                System.out.println("date validation from date not within auth month period");
                                errorMsg = errorMsg + "|" + fromElement + ":Incorrect Date (Auth Period Month is for " + authMonthPeriod + ")";
                            } else {
                                System.out.println("date validation from date not within auth period");
                                errorMsg = errorMsg + "|" + fromElement + ":Incorrect Date (Auth Period is for " + authPeriod + ")";
                            }
                        }
                        validDate = false;
                    }
                } else {
                    System.out.println("date validation from date has incorrect format");
                    errorMsg = errorMsg + "|" + fromElement + ":Incorrect Date Format";
                    validDate = false;
                }
            } else {
                System.out.println("date validation from date null");
                String reqMsg = "Auth From Date is Required";
                if (fromElement.equalsIgnoreCase("tariffDate")) {
                    reqMsg = "Auth Tariff Date is Required";
                }

                errorMsg = errorMsg + "|" + fromElement + ":" + reqMsg + "";
                validDate = false;
            }
        } else {
            System.out.println("date validation element is null");
            validDate = false;
        }
        returnStr = validDate + errorMsg;

        System.out.println(
                "PreAuthDateValidation returnStr = " + returnStr);
        return returnStr;
    }

    public boolean checkFormat(String date) {
        boolean valid = true;
        Pattern p = Pattern.compile("[0-9]{4}[/][0-9]{2}[/][0-9]{2}([ ][0-9]{2}[:][0-9]{2}[:][0-9]{2})?$");
        Matcher m = p.matcher(date);
        valid = m.matches();
        return valid;
    }

    public boolean validateDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        boolean valid = true;
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);

        if (Integer.parseInt(day) > 31) {
            valid = false;
        }
        if (Integer.parseInt(month) > 12) {
            valid = false;
        }

        if (valid) {
            Date parsedDate = null;
            try {
                parsedDate = sdf.parse(date);
            } catch (ParseException px) {
                System.out.println("Auth Date ParseException on - " + date + " [message] " + px.getMessage());
                valid = false;
            }
        }
        return valid;
    }

    public boolean validateDateTime(String dateTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        boolean valid = true;

        String date = dateTime.substring(0, 10);
        valid = validateDate(date);

        System.out.println("validateDateTime dt value = " + dateTime);

        if (valid) {
            String hh = dateTime.substring(11, 13);
            String mm = dateTime.substring(14, 16);
            String ss = dateTime.substring(17, 19);

            if (Integer.parseInt(hh) >= 24) {
                valid = false;
            }
            if (Integer.parseInt(mm) > 59) {
                valid = false;
            }
            if (Integer.parseInt(ss) > 59) {
                valid = false;
            }

            if (valid) {
                Date parsedDate = null;
                try {
                    parsedDate = sdf.parse(dateTime);
                } catch (ParseException px) {
                    System.out.println("Auth DateTime ParseException on - " + date + " [message] " + px.getMessage());
                    valid = false;
                }
            }
        }
        return valid;
    }

    public boolean validateBeforeDate(SimpleDateFormat format, String dateF, String dateT) {
        boolean valid = true;

        Date dateFrom = null;
        Date dateTo = null;

        try {
            dateFrom = format.parse(dateF);
            dateTo = format.parse(dateT);
        } catch (ParseException px) {
            System.out.println("validateBeforeDate px = " + px.getMessage());
            valid = false;
        }
        if (valid) {
            if (dateTo.before(dateFrom) == true) {// || dateTo.compareTo(dateFrom) == 0) {
                valid = false;
            }
        }
        return valid;
    }

    public boolean validateAuthPeriod(String date, String period, String periodMonth) {
        boolean valid = true;
        try {
            Calendar fromCal = Calendar.getInstance();
            Calendar futCal = Calendar.getInstance();
            Calendar nowCal = Calendar.getInstance();
            Date today = new Date(System.currentTimeMillis());

            fromCal.setTime(sdf.parse(date));
            nowCal.setTime(today);
            futCal.setTime(today);
            futCal.add(Calendar.MONTH, 4);
            String year = sdf.format(today).substring(0, 4);
            String nextYear = sdf.format(futCal.getTime()).substring(0, 4);

            String from = date.substring(0, 4);
            String fromMonth = date.substring(5, 7);
            if (!from.equalsIgnoreCase(period)) {
                valid = false;
            }
            if (!fromMonth.equalsIgnoreCase(periodMonth)) {
                monthPeriodError = true;
                valid = false;
            }

            //check if future date
            if (Integer.parseInt(year) < Integer.parseInt(nextYear)) {
                if (futCal.before(fromCal)) {
                    futureDateError = true;
                    valid = false;
                }
            }
        } catch (ParseException ex) {
            Logger.getLogger(PreAuthDateValidation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return valid;
    }
}

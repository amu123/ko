/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import static com.koh.command.NeoCommand.service;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.NeoManagerBean;
import neo.manager.PdcNotes;

/**
 *
 * @author pavaniv
 */
/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
public class PdcNoteListDisplay extends TagSupport {

    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            NeoManagerBean port = service.getNeoManagerBeanPort();
            Integer entityId = (Integer) session.getAttribute("entityId");
            List<PdcNotes> noteList = port.retrievePdcNotes(entityId);
            out.println("<td colspan=\"5\"><table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Creation Date</th><th align=\"left\">Created By</th><th align=\"left\">Note</th></tr>");
            if (noteList == null) {
                noteList = (List<PdcNotes>) session.getAttribute(sessionAttribute);
            }
            if (noteList != null) {
                for (PdcNotes notes : noteList) {

                    String creationDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(notes.getCreationDate().toGregorianCalendar().getTime());

                    out.println("<tr><td><label class=\"label\">" + creationDate + "</label></td>");
                    out.println("<td width=\"150\"><label class=\"label\">" + notes.getUserName() + "</label></td>");
                    out.println("<td width=\"400\"><label class=\"label\">" + notes.getNoteDetails() + "</label></td>");

                }
            }
            out.println("</table></td>");
        } catch (IOException ex) {
            throw new JspException("Error in PdcNoteListDisplay tag", ex);
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

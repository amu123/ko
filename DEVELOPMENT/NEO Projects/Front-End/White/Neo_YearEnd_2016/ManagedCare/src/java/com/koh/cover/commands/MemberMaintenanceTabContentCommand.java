/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.koh.command.NeoCommand;
import com.koh.cover.MemberMainMapping;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.io.PrintWriter;
import java.lang.Exception;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
//import javax.mail.Session;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;
import org.apache.log4j.Logger;

/*
 *
 * @author johanl
 */
public class MemberMaintenanceTabContentCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(MemberMaintenanceTabContentCommand.class);
    private static final String JSP_FOLDER = "/Member/";
    private static String CLIENT = "";
//    NeoManagerBean port = service.getNeoManagerBeanPort();

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        CLIENT = String.valueOf(context.getAttribute("Client"));

        Object memnum = session.getAttribute("memberCoverNumber");
        String memberCoverNoCheck = request.getParameter("memberCoverNoCheck");

        String CustomerCare_Side = String.valueOf(session.getAttribute("CustomerCareSide"));
        if (CustomerCare_Side != null && !CustomerCare_Side.isEmpty() && CustomerCare_Side.equalsIgnoreCase("true")) { //Accessing this page via CustomerCare
            memberCoverNoCheck = memnum.toString();
        } else {
            if (memnum == null || memberCoverNoCheck == null || !memberCoverNoCheck.equalsIgnoreCase(memnum.toString())) {
                try {
                    PrintWriter out = response.getWriter();
                    out.println("<div class='neonotificationnormal neonotificationerror'>");
                    out.println("<span>Duplicate browser session detected</span>");
                    out.println("</div>");
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }

        int entityId = (Integer) session.getAttribute("memberCoverEntityId");
        int memberEntityCommon = (Integer) session.getAttribute("memberEntityCommon");

        String tab = request.getParameter("tab");
        if (String.valueOf(session.getAttribute("CustomerCareSide")).equalsIgnoreCase("true")) {
            tab = "DocumentGeneration";
        }

        logger.info("tab : " + tab);      
        String page = "";
        if ("MemberSummary".equalsIgnoreCase(tab)) {
            //page = getMemberSummaryDetails(request, memberEntityCommon, memberCoverNoCheck);
            page = getMemberDetails(request, entityId, memberEntityCommon);
        } else if ("MemberEntityDetails".equalsIgnoreCase(tab)) {
            page = getMemberDetails(request, entityId, memberEntityCommon);

        } else if ("MemberAddressDetails".equalsIgnoreCase(tab)) {
            page = getMemberAddressDetails(request, entityId);

        } else if ("MemberBankingDetails".equalsIgnoreCase(tab)) {
            page = getMemberBankingDetails(request, entityId);

        } else if ("MemberContactDetails".equalsIgnoreCase(tab)) {
            page = getMemberContactDetails(request, entityId);

        } else if ("MemberCover".equalsIgnoreCase(tab)) {
            page = getMemberCoverDetails(request, entityId);

        } else if ("MemberCoverAudit".equalsIgnoreCase(tab)) {
            page = getMemberCoverAudit(request);

        } else if ("MemberContributions".equalsIgnoreCase(tab)) {
            page = getMemberContributions(request, entityId);
        } else if ("MemberUnderwriting".equalsIgnoreCase(tab)) {

            page = getMemberUnderwriting(request, entityId);
        } else if ("Notes".equalsIgnoreCase(tab)) {
            page = getMemberNotes(request, entityId);
        } else if ("MemberDocuments".equalsIgnoreCase(tab)) {
            page = getMemberDocuments(request, response, entityId, memnum.toString());
        } else if ("DocumentGeneration".equalsIgnoreCase(tab)) {
            page = getMemberDocumentGeneration(request, entityId);
        } else if ("MemberCommunication".equalsIgnoreCase(tab)) {
            page = getMemberCommunication(request, entityId);
        }
        session.setAttribute("memberCoverEntityId", entityId);
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(page);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "MemberMaintenanceTabContentCommand";
    }

    private String getMemberDetails(HttpServletRequest request, int entityId, int entityCommon) {
        HttpSession session = request.getSession();
        List<neo.manager.LookupValue> lookUpsClient = service.getNeoManagerBeanPort().findCodeTableForLookupType(341);
        if (lookUpsClient != null) {
            neo.manager.LookupValue val = lookUpsClient.get(0);
            if (val != null) {
                System.out.println("Client : " + val.getValue());
                session.setAttribute("Client", val.getValue());
            }
        }
        
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Object memnum = request.getSession().getAttribute("memberCoverNumber");
        logger.info("getMemberDetails()");
        if (entityId != 0) {
            PersonDetails pd = port.getPersonDetailsByEntityId(entityId);
            Member mem = port.fetchMemberAddInfo(entityId);
            
            int memberEntityCommon = (Integer) request.getSession().getAttribute("memberEntityCommon");
            int networkIPA = 0;
            if (port.fetchNetworkIPAByEntityCommonID(memberEntityCommon) == null || port.fetchNetworkIPAByEntityCommonID(memberEntityCommon).equalsIgnoreCase("")) {
                request.setAttribute("PersonDetails_network_IPA", "");
            } else {
                networkIPA = Integer.parseInt(port.fetchNetworkIPAByEntityCommonID(memberEntityCommon));
                request.setAttribute("PersonDetails_network_IPA", networkIPA);
            }
            
            List<CoverDetailsAdditionalInfo> covDetails = port.getDependentAdditionalDetails(entityCommon);
//            List<CoverDetailsAdditionalInfo> altMemNumDetails = port.getCoverAltMemberNumbers(entityId);
//            request.setAttribute("alt_MemNum_Details", altMemNumDetails);

            CoverDetails coverDetails = port.findPrincipalForDepCoverNumber(memnum.toString());
            
            session.setAttribute("optionId", coverDetails.getOptionId());
            Integer depType = (Integer) session.getAttribute("memberCoverDepType");
            request.setAttribute("MAI_DepType", MemberMainMapping.getDependantTypeValue(depType + ""));
            MemberMainMapping.setPersonDetail(request, pd);
            MemberMainMapping.setMemberAddInfo(request, mem);
            request.setAttribute("MAI_PayMethod", "");
            MemberMainMapping.setCoverAddDetails(request, covDetails);
        }

        return JSP_FOLDER + "MemberEntityDetails.jsp";
    }

    private String getMemberNotes(HttpServletRequest request, int intityId) {
//        List<KeyValueArray> kva = port.fetchQuestions(191);
//        Object obj = MapUtils.getMap(kva);
        //       Object obj = port.fetchAppQuestions(2);
//        request.setAttribute("MemAppGeneralQuestions", obj);
        return JSP_FOLDER + "MemberNotes.jsp";
    }

    private String getMemberDocuments(HttpServletRequest request, HttpServletResponse response, int intityId, String memNum) {
//        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        request.setAttribute("memberNumber", memNum);
//          List<ContactDetails> con =
//          port.getContactDetailsByEntityId(intityId);
//          MemberMainMapping.setContactDetails(request, con); try {
//          request.setAttribute("folderList", new
//          PropertiesReader().getProperty("DocumentIndexWorkFolder"));
//          request.setAttribute("listOrWork", "Work"); //String memberNumber =
//          request.getParameter("memberNumber"); String memberNumber = (String)
//          session.getAttribute("memberNumber"); logger.info("Memeber no = " +
//          memberNumber); if (memberNumber != null &&
//          !memberNumber.trim().equals("")) { IndexDocumentRequest req = new
//          IndexDocumentRequest(); NeoUser loggedInUser = (NeoUser)
//          request.getSession().getAttribute("persist_user");
//          req.setAgent(loggedInUser.getUsername());
//          req.setIndexType("IndexForMember");
//          req.setMemberNumber(memberNumber); req.setSrcDrive(new
//          PropertiesReader().getProperty("DocumentIndexIndexDrive"));
//          IndexDocumentResponse resp =
//          NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
//         
//          if (resp.getIndexForMember() != null) { logger.info("Index size = " +
//          resp.getIndexForMember().size()); } for (IndexForMember index :
//          resp.getIndexForMember()) { String typeName = index.getDocType();
//          Pattern p = Pattern.compile("([0-9]*)"); Matcher m =
//          p.matcher(typeName); if (m.matches()) { typeName =
//          port.getValueForId(212, typeName); index.setDocType(typeName); } }
//         
//          request.setAttribute("indexList", resp.getIndexForMember()); }
//          request.setAttribute("memberNumber", memberNumber);
//         
//          //RequestDispatcher dispatcher =
//          request.getRequestDispatcher("/Claims/CallCenterViewDocuments.jsp");
//          //dispatcher.forward(request, response); } catch (Exception ex) {
//          ex.printStackTrace(); }
         
        return JSP_FOLDER + "MemberDocumentSorting.jsp";
    }

    private String getMemberAddressDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        logger.info("getMemberAddressDetails()");
        if (entityId != 0) {
            List<AddressDetails> ad = port.getAddressDetailsByEntityId(entityId);
            System.out.println("Address list size = " + ad.size());
            MemberMainMapping.setAddressDetails(request, ad);
        }

        return JSP_FOLDER + "MemberAddressDetails.jsp";
    }

    private String getMemberDocumentGeneration(HttpServletRequest request, int entityId) {
        logger.info("getMemberDocumentGeneration()");
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        String coverNumber = (String) session.getAttribute("memberCoverNumber");
        List<CoverDetails> covList = port.getDependantsForPrincipleMemberByDate(coverNumber);
        String coverStatus = "";
        for (CoverDetails cd : covList) {
            if (cd.getDependentTypeId() == 17) {
                coverStatus = cd.getStatus();
                session.setAttribute("coverStatus", coverStatus);
            }
        }


        if (entityId != 0) {
            request.setAttribute("entityId", entityId);
        }
        if (String.valueOf(session.getAttribute("CustomerCareSide")).equalsIgnoreCase("true")) {
            return "/Claims/GenerateMemberCertificates.jsp"; //For membership
        } else {
            return JSP_FOLDER + "MemberDocumentGeneration.jsp";
        }
    }

    private String getMemberCommunication(HttpServletRequest request, int entityId) {
        logger.info("getMemberCommunication()");
        if (entityId != 0) {
            request.setAttribute("entityId", entityId);
        }
        return JSP_FOLDER + "MemberCommunication.jsp";
    }

    private String getMemberBankingDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        if (entityId != 0) {
            //List<BankingDetails> bd = port.getBankDetailsByEntityId(entityId);
            List<BankingDetails> bd = port.getAllBankingDetails(entityId);
            System.out.println("Banking list size = " + bd.size());
            MemberMainMapping.setBankingDetails(request, bd);
        }
        return JSP_FOLDER + "MemberBankingDetails.jsp";
    }

    private String getMemberContactDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        logger.info("getMemberContactDetails()");
        if (entityId != 0) {
            //set conatct details
            List<ContactDetails> con = port.getContactDetailsByEntityId(entityId);
            MemberMainMapping.setContactDetails(request, con);

            //set contact preferences
            List<ContactPreference> conPref = port.getAllContactPreferences(entityId);
            MemberMainMapping.setContactPrefDetails(request, conPref);

        }
        return JSP_FOLDER + "MemberContactDetails.jsp";
    }

    public String getMemberCoverDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        logger.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        List<MemberEmployer> ba = port.findEmployerGroupsAndBrokerForMemberByMemberEntityId(entityId);
        // this is to assure that the comparing only happens on an active record.
        // {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        GregorianCalendar cal = new GregorianCalendar();
        Date endDate = null;
        XMLGregorianCalendar xmlDate = null;
                
        try {
            endDate = sdf.parse("2999/12/31");
            cal.setTime(endDate);
            xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(MemberMaintenanceTabContentCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DatatypeConfigurationException ex) {
            java.util.logging.Logger.getLogger(MemberMaintenanceTabContentCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        // }
        int broker = 0;
        int group = 0;
        int consultant = 0;
        int option = 0;
        int brokerAndConsultant = 0;
        request.getSession().setAttribute("ConBro", "None");
        boolean foundBroker = false;
        int product = 0;
        for (MemberEmployer me : ba) {
            product = me.getProduct();
            System.out.println("product set as : " + product);
            if (me.getDetailType().equalsIgnoreCase("Broker") && me.getEffectiveEndDate().compare(xmlDate) == 0) {
                broker = 1;
                //System.err.println("Broker Effective end date " + me.getEffectiveEndDate().toString().substring(0, 4)  + " EntityId " + me.getEntityId());
                BrokerBrokerConsult cons = port.fetchConsultantLinkedToBrokerByBrokerEntityId(me.getEntityId());
                if (cons != null) {
                    request.getSession().setAttribute("consultantForBroker", cons.getConsultantName());
                    me.setConsultantName(cons.getConsultantName());
                }
                if (me.getEffectiveEndDate().toString().substring(0, 4).equals("2999")) {
                    request.getSession().setAttribute("ConBro", "Broker");
                    foundBroker = true;
                    request.getSession().setAttribute("ConBroEntityId", me.getEntityId());
                }
            }

            if (me.getDetailType().equalsIgnoreCase("Group") && me.getEffectiveEndDate().compare(xmlDate) == 0) {
                group = 2;

                if (me.getEffectiveEndDate().toString().substring(0, 4).equals("2999")) {
                    request.getSession().setAttribute("GroupIncEntityId", me.getEntityId());
                    //System.err.println("Group Effective end date " + me.getEffectiveEndDate().toString().substring(0, 4) + " EntityId " + me.getEntityId());
                }
            }

            if (me.getDetailType().equalsIgnoreCase("Consultant") && me.getEffectiveEndDate().compare(xmlDate) == 0) {
                consultant = 3;
                //System.err.println("Cons Effective end date " + me.getEffectiveEndDate().toString().substring(0, 4) + " EntityId " + me.getEntityId());
                if (me.getEffectiveEndDate().toString().substring(0, 4).equals("2999")) {
                    request.getSession().setAttribute("ConBroEntityId", me.getEntityId());
                    if (!foundBroker) {
                        request.getSession().setAttribute("ConBro", "Consultant");
                    }
                    request.getSession().setAttribute("consultantForBroker", null);
                }
            }

            if (me.getDetailType().equalsIgnoreCase("Option") && me.getEffectiveEndDate().compare(xmlDate) == 0) {
                option = 4;
            }
        }

        if (broker == 1 && group == 2) {
            broker = 3;
            group = 3;
        }

        if (consultant == 0 && broker == 0) {
            brokerAndConsultant = 10;
        }

        String coverNumber = "" + session.getAttribute("memberCoverNumber");
        CoverAdditionalInfo payoutType = port.getClassicPayoutType(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(new Date()));

        MemberMainMapping.setPayoutType(request, payoutType);

        request.setAttribute("detailsList", ba);
        request.setAttribute("group", group);
        request.setAttribute("option", option);
        request.setAttribute("product", product);
        request.setAttribute("brokerAndConsultant", brokerAndConsultant);

        List<CoverDetails> covList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        
        String coverStatus = "";
        ArrayList<UnionMember> unionMember = null;
        for (CoverDetails cd : covList) {
            if (cd.getDependentTypeId() == 17) {
                logger.info("** Cover Number : " + cd.getCoverNumber());
                logger.info("** Dependent Type ID : " + cd.getDependentTypeId());
                logger.info("** CLIENT_ACTIVE : " + CLIENT);
                coverStatus = cd.getStatus();
                if (CLIENT.equalsIgnoreCase("Sechaba")) {
                    unionMember = (ArrayList<UnionMember>) port.retrieveUnionMemberDetails(cd.getCoverNumber(), 0);
                    if (unionMember != null) {
                        session.setAttribute("memberUnionLinkedDetails", unionMember);
                    }
                }
                session.setAttribute("coverStatus", coverStatus);
                session.setAttribute("productId", cd.getProductId());
                session.setAttribute("optionId", cd.getOptionId());
            }
        }
        repopulateCoverStatus(request);
        return JSP_FOLDER + "MemberCoverDetailsMaintenance.jsp";
    }

    public void repopulateCoverStatus(HttpServletRequest request) {
        HttpSession session = request.getSession();
        NeoManagerBean port = this.service.getNeoManagerBeanPort();


        String coverNumber = "" + session.getAttribute("memberCoverNumber");
        String coverStatus = "";
        String depStatus = "";

        List<CoverDetails> covList = port.getDependantsForPrincipleMemberByDate(coverNumber);
        session.setAttribute("MemberCoverDependantDetails", covList);

        boolean showResignBTN = false;
        for (CoverDetails cd : covList) {
            if (cd.getCoverEndDate() != null){
                String covEndDate = new SimpleDateFormat("yyyy/MM/dd").format(cd.getCoverEndDate().toGregorianCalendar().getTime());
                if (covEndDate.equalsIgnoreCase("2999/12/31")){
                    showResignBTN = true;
                }
            }
            if (cd.getDependentTypeId() == 17) {
                coverStatus = cd.getStatus();
                session.setAttribute("coverStatus", cd.getStatus());
                session.setAttribute("principalStatus", cd.getStatus());
                session.setAttribute("memberEntityID", cd.getEntityId());
                session.setAttribute("coverNumber", cd.getCoverNumber());
                System.out.println("Status " + coverStatus);
                if (cd.getCoverEndDate() != null) {
                    String dateTo = new SimpleDateFormat("yyyy/MM/dd").format(cd.getCoverEndDate().toGregorianCalendar().getTime());
                    if (!dateTo.equalsIgnoreCase("2999/12/31")) {
                        session.setAttribute("coverStatus", "both");
                    }

                    if (cd.getStatus().equalsIgnoreCase("Suspend")) {
                        depStatus = cd.getStatus();
                        session.setAttribute("suspendMember", cd.getStatus());
                    }

                }
            } else if (cd.getStatus().equalsIgnoreCase("Resign")) {
                depStatus = cd.getStatus();
                session.setAttribute("depStatus", cd.getStatus());

            } else if (cd.getCoverEndDate() != null) {
                String dateTo = new SimpleDateFormat("yyyy/MM/dd").format(cd.getCoverEndDate().toGregorianCalendar().getTime());
                if (!dateTo.equalsIgnoreCase("2999/12/31")) {
                    session.setAttribute("coverStatus", "both");
                }
            }
        }

        session.setAttribute("CanResign", showResignBTN);
        
        if ((coverStatus != null && coverStatus.equalsIgnoreCase("Active")) && (depStatus != null && depStatus.equalsIgnoreCase("Resign"))) {
            coverStatus = "both";
            session.setAttribute("coverStatus", coverStatus);
        }

    }

    private String getMemberCoverAudit(HttpServletRequest request) {
        return JSP_FOLDER + "MemberCoverAudit.jsp";
    }

    private String getMemberContributions(HttpServletRequest request, int entityId) {
//        Contributions contrib = port.getContributionsForMember(entityId);
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        session.setAttribute("ageAnalysisProductID", 0);
        String coverNum = (String) session.getAttribute("coverNum");
        Integer optionId = (Integer) session.getAttribute("memberCoverOptionId");
        Integer depNumber = (Integer) session.getAttribute("memberCoverDepNum");
        Date endDate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.MONTH, -6);
        Date startDate = cal.getTime();
        cal.setTime(endDate);
        cal.add(Calendar.MONTH, 1);
        endDate = cal.getTime();
        DateTimeUtils.convertDateToXMLGregorianCalendar(endDate);
        List<ContributionTransactionHistoryView> memberContributionsHistory = port.fetchMemberContributionsHistory(coverNum, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), DateTimeUtils.convertDateToXMLGregorianCalendar(endDate), DateTimeUtils.convertDateToXMLGregorianCalendar(new Date()));
        List<ContributionReceipts> unallocatedReceipts = port.fetchUnallocatedReceiptsByEntityId(entityId, null);
        ArrayList<CoverDetails> cov = (ArrayList) session.getAttribute("MemberCoverDetails");
        CoverDetails covMemb = null;
        for (CoverDetails c : cov) {
            if (c.getCoverNumber().equals(coverNum)) {
                covMemb = c;
            }
        }
        int productId = 1;
        if (covMemb != null) {
            System.out.println("Member Product Name : " + covMemb.getProductName());
            productId = covMemb.getProductId();
            session.setAttribute("ageAnalysisProductID", covMemb.getProductId());
        } else {
            System.out.println("Member Product name = NULL!");
        }
        List<KeyValueArray> debitOrderHistory = port.fetchDebitOrderHistory(coverNum, null, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), DateTimeUtils.convertDateToXMLGregorianCalendar(endDate), productId);
        List<ContributionAge> memberAgeAnalysis = port.getAgeAnalysis(coverNum, null);
        List<KeyValueArray> refunds = port.fetchEntityRefundTransactionsByEntityId(entityId);

        //if (optionId != null && (optionId == 12 || optionId == 8)) {
        List<Map<String, String>> map = MapUtils.getMap(port.getCoverSavingsDetails(coverNum));
        request.setAttribute("savingsLimits", map);
        if (map != null && map.size() > 0) {
            boolean interest = false;
            boolean jnl = false;
            boolean claims = false;
            boolean savings = false;
            boolean contrib = false;
            boolean savingsRecov = false;
            for (Map<String, String> m : map) {
                if (!"0".equals(m.get("interest"))) {
                    interest = true;
                }
                if (!"0".equals(m.get("jnl"))) {
                    jnl = true;
                }
                if (!"0".equals(m.get("claimsRecovered"))) {
                    claims = true;
                }
                if (!"0".equals(m.get("savingsPaid"))) {
                    savings = true;
                }
                if (!"0".equals(m.get("contribRecovered"))) {
                    contrib = true;
                }
                if (!"0".equals(m.get("savingsRecovered"))) {
                    savingsRecov = true;
                }
            }
            request.setAttribute("hasInt", interest ? "yes" : "");
            request.setAttribute("hasJnl", jnl ? "yes" : "");
            request.setAttribute("hasClaim", claims ? "yes" : "");
            request.setAttribute("hasSav", savings ? "yes" : "");
            request.setAttribute("hasContrib", contrib ? "yes" : "");
            request.setAttribute("hasSavRecov", savingsRecov ? "yes" : "");
        }
//        }
//        List<Map<String, String>> map = MapUtils.getMap(debitOrderHistory);
        request.setAttribute("Contributions", memberContributionsHistory);
        request.setAttribute("Receipts", unallocatedReceipts);
        request.setAttribute("memberAgeAnalysis", memberAgeAnalysis);
        request.setAttribute("contrib_end_date", DateTimeUtils.convertToYYYYMMDD(endDate));
        request.setAttribute("contrib_start_date", DateTimeUtils.convertToYYYYMMDD(startDate));
        request.setAttribute("contrib_ad_date", DateTimeUtils.convertToYYYYMMDD(endDate));
        request.setAttribute("debitOrder", MapUtils.getMap(debitOrderHistory));
        request.setAttribute("receiptRefunds", MapUtils.getMap(refunds));
        return JSP_FOLDER + "MemberContributions.jsp";
    }

    public String getMemberUnderwriting(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        String coverNum = (String) session.getAttribute("coverNum");
        int depNumber = (Integer) session.getAttribute("memberCoverDepNum");
        String warning = null;
        List<AuthCoverExclusions> underwriting = port.getActiveCoverExclusionsByCoverDependant(coverNum, depNumber + "");
        if (underwriting != null && underwriting.size() > 0) {
            for (AuthCoverExclusions ce : underwriting) {
                if (ce.getExclusionDateFrom() != null && ce.getExclusionDateTo() != null) {
                    ce.setDuration(String.valueOf(DateTimeUtils.getMonthsDiff(ce.getExclusionDateFrom(), ce.getExclusionDateTo())));
                }
            }
        }
        CoverDetailsAdditionalInfo ma = new CoverDetailsAdditionalInfo();

        session.setAttribute("warningW", null);


        int claims = port.checkIfmemberHasClaims(coverNum);

        if (claims > 0) {
            warning = "claims";
        }
        PreAuthSearchCriteria pas = new PreAuthSearchCriteria();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        pas.setCoverNumber(coverNum);
        List<PreAuthSearchResults> auths = port.findPreAuthDetailsByCriteria(user, pas);

        if (auths.size() > 0) {
            warning = "pre";
        }

        if (claims > 0 && auths.size() > 0) {
            warning = "both";
        }
        session.setAttribute("warningW", warning);

        String coverNumber = (String) session.getAttribute("coverNumber");
        int infoTypeid = 9;
        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        int CommonentityID = 0;

        for (CoverDetails cd1 : cdList) {
            if (cd1.getDependentTypeId() == 17) {
                CommonentityID = cd1.getEntityCommonId();
            }
        }

        int conceTypeExistValue = port.getConcessionTypebyEntityCommId(CommonentityID, infoTypeid);
        session.setAttribute("validateConceType", conceTypeExistValue);

        ma = port.fetchConcessionTypebyEntityCommId(CommonentityID);
        if (ma != null && ma.getInfoValue() != null) {

            request.setAttribute("ConcesType", ma.getInfoValue());
            request.setAttribute("ConcessionStart", DateTimeUtils.convertXMLGregorianCalendarToDate(ma.getInfoStartDate()));
            request.setAttribute("ConcessionEnd", DateTimeUtils.convertXMLGregorianCalendarToDate(ma.getInfoEndDate()));
            request.setAttribute("Underwriting", underwriting);
        }
        return JSP_FOLDER + "MemberUnderwriting.jsp";
    }

    private String formatDouble(double d) {
        DecimalFormat df = new DecimalFormat("0.00");
        return df.format(d);
    }

    private String getMemberSummaryDetails(HttpServletRequest request, int entityCommon, String coverNumber) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        List<MemberSummary> memberSummaryListObj = port.getmemberSummary(entityCommon);
        CoverSearchCriteria critObj = new CoverSearchCriteria();
        critObj.setCoverNumber(coverNumber);
        // List<CoverDetails> coverDetailsListObj = port.getCoverPrincipleList(critObj);
        List<CoverDetails> coverDetailsListObj = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        boolean isFutureInception = false;
        Date today = new Date();
        Date endDate = null;
        try {
            endDate = new SimpleDateFormat("yyyy/MM/dd").parse("2999/12/31");
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(MemberMaintenanceTabContentCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        String inceptionDate = null;
        for (CoverDetails coverObj : coverDetailsListObj) {
            if (coverObj.getDependentTypeId() == 17 && coverObj.getCoverStartDate().toGregorianCalendar().getTime().after(today)
                    && coverObj.getCoverEndDate().toGregorianCalendar().getTime().equals(endDate)) {
                isFutureInception = true;
                inceptionDate = new SimpleDateFormat("yyyy/MM/dd").format(coverObj.getCoverStartDate().toGregorianCalendar().getTime());
            }
        }
        request.setAttribute("memberSummaryDetails", memberSummaryListObj);
        request.setAttribute("isFutureInception", isFutureInception);
        request.setAttribute("coverInceptionDate", inceptionDate);
        return JSP_FOLDER + "MemberSummaryDetail.jsp";
    }
}

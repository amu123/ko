/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import com.agile.security.webservice.EmailAttachment;
import com.agile.security.webservice.EmailContent;
import com.koh.command.FECommand;
import static com.koh.command.FECommand.service;
import com.koh.serv.PropertiesReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class SubmitTaxCertificateCommand extends FECommand {

    private Logger logger = Logger.getLogger(SubmitTaxCertificateCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        logger.info("Inside SubmitTaxCertificateCommand");
        this.saveScreenToSession(request);

        String actionType = request.getParameter("opperationParam");
        String memberNo = request.getParameter("memberNo_text");
        String emailTo = request.getParameter("emailAddress");
        String yearRange = request.getParameter("yearRange");
        int productId = new Integer(String.valueOf(session.getAttribute("schemeOption")));

        logger.info("The context path is " + context.getContextPath());

        boolean emailSent = false;

        try {
            String path = new PropertiesReader().getProperty("directory");
            System.out.println("directory: " + path);

            String scheme = "";
            if (productId == 1) {
                scheme = "Resolution Health";
                path += "\\ResolutionHealth";
            } else if (productId == 2) {
                scheme = "Spectramed";
                path += "\\Spectramed";
            } else {
                scheme = "Agility";
            }

            String pathToCert = path + "\\" + yearRange + "\\" + memberNo + ".pdf";

            logger.info("Path to cert: " + pathToCert);

            File file = new File(pathToCert);

            // URI fileURI = file.toURI();
            //URL url = fileURI.toURL();
            //URLConnection connection = url.openConnection();
            //InputStream in = connection.getInputStream();
            //int contentLength = connection.getContentLength();
            InputStream in = new FileInputStream(file);
            int contentLength = (int) file.length();
            logger.info("The content length is " + contentLength);
            // logger.info("The url is " + url);
            //logger.info("file URI host " + fileURI.getPath());
            //logger.info("The scheme " + fileURI.getScheme());
            ByteArrayOutputStream temporaryOutput;
            if (contentLength != -1) {
                temporaryOutput = new ByteArrayOutputStream(contentLength);
            } else {
                temporaryOutput = new ByteArrayOutputStream(20480);
            }

            byte[] bufer = new byte[512];
            while (true) {

                int len = in.read(bufer);
                if (len == -1) {

                    break;
                }
                temporaryOutput.write(bufer, 0, len);
            }
            in.close();
            temporaryOutput.close();

            byte[] array = temporaryOutput.toByteArray();

            // test to see if the file is corrupt

            
            String fileName = scheme + "_" + yearRange + "_Tax_" + memberNo + ".pdf";

            String preview = "Preview";
            logger.info("actionType is " + actionType);

            if (actionType != null && actionType.equalsIgnoreCase("Preview")) {
                printPreview(request, response, array, fileName);

            } else if (emailTo != null && !emailTo.equalsIgnoreCase("")) {
                String emailFrom = "";
                
                if(productId == 2){
                   emailFrom = "Noreply@spectramed.co.za"; 
                } else {
                    emailFrom = "Noreply@agilityghs.co.za";
                }
                
                String subject = scheme + " Tax Certificate";
                String emailMsg = null;
                EmailContent content = new EmailContent();
                content.setContentType("text/plain");
                EmailAttachment attachment = new EmailAttachment();
                attachment.setContentType("application/octet-stream");
                attachment.setName(fileName);
                content.setFirstAttachment(attachment);
                content.setFirstAttachmentData(array);
                content.setSubject(subject);
                content.setEmailAddressFrom(emailFrom);
                content.setEmailAddressTo(emailTo);
                content.setProductId(productId);
                //Determine msg for email according to type of document ('claim', 'statement', 'preAuth', 'benefit' 'custom', 'other')
                content.setType("tax");
                /*If document type is custom, this message added here will be displayed in the email. The email method will automatically add the 
                 the enquiries and call center aswell as the kind regards at the end of the message*/
                content.setContent(emailMsg);
                emailSent = service.getAgileManagerPort().sendEmailWithOrWithoutAttachment(content);
                logger.info("Email sent : " + emailSent);
                printEmailResult(request, response, emailSent);
            } else {
                printEmailResult(request, response, emailSent);
            }


        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } catch (IOException ioex) {

            // FileNotFound exception
            printErrorResult(request, response);
            logger.info(ioex.getMessage());
            System.out.println("The ioex.getMessage() " + ioex.getMessage());
            ioex.printStackTrace();
        }

        return null;
    }

    public void printPreview(HttpServletRequest request, HttpServletResponse response, byte[] array, String fileName) throws IOException {
        OutputStream outStream = null;
        try {

            response.setHeader("Pragma", "no-cache"); //HTTP 1.0
            response.setDateHeader("Expires", 0); //prevents caching at the proxy server
            response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-disposition", "attachment; filename=" + fileName);

            response.setContentType("application/pdf");
            response.setContentLength(array.length);
            outStream = response.getOutputStream();
            outStream.write(array);
            outStream.flush();
            outStream.close();

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void printResult(HttpServletRequest request, HttpServletResponse response, boolean result) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (result) {
                out.println("<td><label class=\"header\">Tax Certificate emailed.</label></td>");
            } else {
                //out.println("<td><label class=\"header\">No payment(s) found.</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    public void printEmailResult(HttpServletRequest request, HttpServletResponse response, boolean result) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (result) {
                out.println("<td><label class=\"header\">Tax Certificate emailed.</label></td>");
            } else {
                out.println("<td><label class=\"header\">Email sending failed, Either email address is missing or incorrect.</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "2; URL=/ManagedCare/Statement/TaxCertificate.jsp");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    public void printErrorResult(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");

            out.println("<td><label class=\"header\">No records found.</label></td>");

            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "2; URL=/ManagedCare/Statement/TaxCertificate.jsp");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    @Override
    public String getName() {

        return "SubmitTaxCertificateCommand";
    }
}

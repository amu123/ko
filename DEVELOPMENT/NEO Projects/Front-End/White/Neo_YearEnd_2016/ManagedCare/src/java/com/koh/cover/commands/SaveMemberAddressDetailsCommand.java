/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.agile.security.webservice.EmailAttachment;
import com.agile.security.webservice.EmailContent;
import com.koh.command.NeoCommand;
import com.koh.cover.MemberMainMapping;
import com.koh.employer.command.TabUtils;
import com.koh.fe.command.LoginCommand;
import com.koh.serv.PropertiesReader;
import java.io.*;
import java.lang.Exception;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;
import org.apache.log4j.Logger;

/**
 *
 * @author johanl
 */
public class SaveMemberAddressDetailsCommand extends NeoCommand {

    
    private Logger logger = Logger.getLogger(SaveMemberAddressDetailsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        logger.info("^^^^^^^^^^^^^^^^^^^ SaveMemberAddressDetailsCommand execute ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        String result = validateAndSave(request);
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
        } catch (IOException ex) {
            logger.error(ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "SaveMemberAddressDetailsCommand";
    }

    private String validateAndSave(HttpServletRequest request) {
        Map<String, String> errors = MemberMainMapping.validateAddress(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (save(request)) {
                additionalData = "\"message\":\"Member Address Details Updated\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error updating the member address details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
    }

    private boolean save(HttpServletRequest request) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);
        
        int entityId = MemberMainMapping.getEntityId(request);
        
        try {
            List<AddressDetails> adList = MemberMainMapping.getAddressDetails(request, neoUser, entityId);
            String addressLocation = new PropertiesReader().getProperty("DocumentIndexIndexDrive") + port.saveAddressListDetails(adList, sec, entityId);
            
            //--------------------MAILING Functionality-----------------//
            String coverNumber = port.getCoverNumberForEntity(entityId);
            List<CoverDetails> coverD = port.getCoverDetailsByCoverNumber(coverNumber);
            boolean hasEmail = port.memberHasEmail(entityId);
            if (hasEmail){
                ContactDetails contactDetail = port.getContactDetails(entityId, 1);
                InputStream in = null;
                try {
                    String emailFrom = "Noreply@agilityghs.co.za";
                    String scheme = String.valueOf(session.getAttribute("schemeName"));
                    String emailTo = contactDetail.getMethodDetails();
                    int productId = 0;

                    if (scheme.equalsIgnoreCase("Resolution Health")) {
                        productId = 1;
                        emailFrom = "Noreply@agilityghs.co.za";
                    } else if (scheme.equalsIgnoreCase("Spectramed")) {
                        productId = 2;
                        emailFrom = "Noreply@spectramed.co.za";
                    } else if (scheme.equalsIgnoreCase("Sizwe")){
                        productId = 3;
                        emailFrom = "do-not-reply@sizwe.co.za";
                    }
                    boolean emailSent = false;

                    System.out.println("DocumentIndexIndexDrive: " + addressLocation);
                    //Creating a Byte out of the pdf
                    File file = new File(addressLocation);
                    if (file.exists()) {
                        in = new FileInputStream(file);
                        int contentLength = (int) file.length();
                        logger.info("The content length is " + contentLength);
                        ByteArrayOutputStream temporaryOutput;
                        if (contentLength != -1) {
                            temporaryOutput = new ByteArrayOutputStream(contentLength);
                        } else {
                            temporaryOutput = new ByteArrayOutputStream(20480);
                        }
                        byte[] bufer = new byte[512];
                        while (true) {
                            int len = in.read(bufer);
                            if (len == -1) {
                                break;
                            }
                            temporaryOutput.write(bufer, 0, len);
                        }
                        in.close();
                        temporaryOutput.close();
                        byte[] array = temporaryOutput.toByteArray();
                        //Creating a Byte out of the pdf
                        if (emailTo != null && !emailTo.equalsIgnoreCase("")) {
                            String subject = scheme;
                            if (productId == 1) {
                                subject = "Your Resolution Health Customer Information Update";
                            } else if (productId == 2) {
                                subject = "Your Spectramed Customer Information Update";
                            }
                            String emailMsg = null;
                            EmailContent content = new EmailContent();
                            content.setContentType("text/plain");
                            EmailAttachment attachment = new EmailAttachment();
                            attachment.setContentType("application/octet-stream");
                            attachment.setName("Member_Information.pdf");
                            content.setFirstAttachment(attachment);
                            content.setFirstAttachmentData(array);
                            content.setSubject(subject);
                            content.setEmailAddressFrom(emailFrom);
                            content.setEmailAddressTo(emailTo);
                            content.setProductId(productId);
                            content.setMemberCoverNumber(coverNumber);
                            content.setMemberTitle(coverD.get(0).getTitle());
                            content.setMemberInitials(coverD.get(0).getInitials());
                            content.setMemberName(coverD.get(0).getName());
                            content.setMemberSurname(coverD.get(0).getSurname());
                            //Determine msg for email according to type of document
                            content.setType("updateToContactDetails");
                            /*If document type is custom, this message added here will be displayed in the email. The email method will automatically add the 
                             the enquiries and call center aswell as the kind regards at the end of the message*/
                            content.setContent(emailMsg);
//                            emailSent = com.koh.command.FECommand.service.getAgileManagerPort().sendEmailWithOrWithoutAttachment(content);
//                            logger.info("Email sent : " + emailSent);
//                            if(emailSent){
//                                boolean result = port.mailCommunicationAudit(Integer.parseInt(String.valueOf(entityId)), emailTo, "Email sending successful", 15);
//                            } else{
//                                boolean result = port.mailCommunicationAudit(Integer.parseInt(String.valueOf(entityId)), emailTo, "Email sending Failed", 15);
//                            }
                        } else {
                        }
                    } else {
                    }

                } catch (FileNotFoundException ex) {
                        org.apache.log4j.Logger.getLogger(this.getClass()).info("FileNotFoundException: " + ex.getMessage());
                } catch (IOException ex) {
                        org.apache.log4j.Logger.getLogger(this.getClass()).info("IOException: " + ex.getMessage());
                } finally {
                    try {
                        if (in != null) {
                            in.close();
                        }
                    } catch (IOException ex) {
                        org.apache.log4j.Logger.getLogger(this.getClass()).info("IOException: " + ex.getMessage());
                    }
                }
            }
            //--------------------MAILING Functionality-----------------//
            
            return true;
        } catch (Exception e) {
            org.apache.log4j.Logger.getLogger(this.getClass()).info("Exception: " + e.getMessage());
            return false;
        }
    }
}

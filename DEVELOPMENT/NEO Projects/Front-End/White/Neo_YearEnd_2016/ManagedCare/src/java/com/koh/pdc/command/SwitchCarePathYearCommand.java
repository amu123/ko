/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.MapUtils;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CarePathDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author nick
 */
public class SwitchCarePathYearCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("entered SwitchCarePathYearCommand");
        HttpSession session = request.getSession();
        
        String incOrDec = "" + request.getParameter("incOrDecYear");
        String cy = "" + session.getAttribute("pdcCurrentSelectedYearID");
        session.setAttribute("pdcCarePathScrollTrue", "true");
        int currYear = -1;
        try{
            currYear = Integer.parseInt(cy);
        }catch(NumberFormatException e){
            System.out.println("Could not format number for current PDC year: " + e.getMessage());
        }
        
        List<String> years = (List<String>) session.getAttribute("pdcUsableYears");
        if(incOrDec.equals("+")){
            session.setAttribute("pdcCurrentSelectedYear", years.get(currYear+1));
            session.setAttribute("pdcCurrentSelectedYearID", currYear+1);
        }else{
            session.setAttribute("pdcCurrentSelectedYear", years.get(currYear-1));
            session.setAttribute("pdcCurrentSelectedYearID", currYear-1);
        }
        
        try {
            String nextJSP = "/PDC/ViewDependantCarepathDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return null;
    }

    @Override
    public String getName() {
        return "SwitchCarePathYearCommand";
    }
}

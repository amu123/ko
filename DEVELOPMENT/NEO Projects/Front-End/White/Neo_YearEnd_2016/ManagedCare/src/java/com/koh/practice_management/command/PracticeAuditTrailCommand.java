/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.practice_management.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.MapUtils;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;
import org.apache.log4j.Logger;

/**
 *
 * @author almaries
 */
public class PracticeAuditTrailCommand extends NeoCommand{

    private static final Logger logger = Logger.getLogger(PracticeAuditTrailCommand.class);
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        try {
            String name = request.getParameter("auditList");
            String entityId = request.getParameter("provNumEntityId");
            
            logger.info("name : " + name);
            logger.info("entityId: " + entityId);
            
            
            int id = 0;
            try {
                id = Integer.parseInt(entityId);
                logger.info("id: " + id);
            } catch (Exception e) {
                logger.error(e);
                
            }
            List<KeyValueArray> kva = port.getAuditTrail(name, id);
            logger.info("total kva returned : " +  kva.size());
            
            /*for (KeyValueArray kVa : kva) {
                List<KeyValue> kVl =  kVa.getKeyValList();
                for (KeyValue kv : kVl) {
                    logger.info("key : " +  kv.getKey());
                    logger.info("value : " +  kv.getValue());
                }
            } */
            
            Object col = MapUtils.getMap(kva);
            request.setAttribute("PracticeAuditDetails", col);
            context.getRequestDispatcher("/PracticeManagement/PracticeAuditTrailDetails.jsp").forward(request, response);
        } catch (Exception ex) {
            logger.error(ex);
        } 
        
        return null;
    }

    @Override
    public String getName() {
        return "PracticeAuditTrailCommand";
    }
    
}

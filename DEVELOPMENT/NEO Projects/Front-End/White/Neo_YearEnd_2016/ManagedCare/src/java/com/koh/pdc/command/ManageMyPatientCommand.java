/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.Command;
import static com.koh.command.NeoCommand.service;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;

/**
 *
 * @author dewaldo
 */
public class ManageMyPatientCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("entered ManageMyPatientCommand");

        //List<TaskHistoryList> retObject = service.getNeoManagerBeanPort().getAssignmentHistory(id);
        //session name from tag file
        //session.setAttribute("assignHistory", retObject);
        
        //check if dependant has an existing carepath or not
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        int workListId = 0;
        try{
            workListId = Integer.parseInt(""+session.getAttribute("workListId"));
        }catch(NumberFormatException e){
            System.out.println("Could not format workListId in session: " + e.getMessage());
        }

        boolean cpExists = port.checkIfDependantHasExistingCarePath(workListId);
        session.setAttribute("depHasCarePath", cpExists);
        session.setAttribute("pdcCarePathScrollTrue", "false");
        
        try {
            String nextJSP = "/PDC/ManageMyPatient.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ManageMyPatientCommand";
    }
}

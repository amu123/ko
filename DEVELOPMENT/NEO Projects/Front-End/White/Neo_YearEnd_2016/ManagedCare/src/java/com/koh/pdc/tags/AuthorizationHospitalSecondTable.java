/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import com.koh.command.NeoCommand;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.NeoManagerBean;
import neo.manager.PreAuthDetails;

/**
 *
 * @author princes
 */
public class AuthorizationHospitalSecondTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        String fDetails;
        String aStatus;
        String rRela;
        //ArrayList<Diagnosis> list = (ArrayList<Diagnosis>) session.getAttribute("diagnosis_list");
        Collection<PreAuthDetails> details = (Collection<PreAuthDetails>) session.getAttribute("authDetails");
        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        String authDate;
        try {

            out.print("<tr>");
            out.print("<th scope=\"col\">Remibursement Model</th>");
            out.print("<th scope=\"col\">Funder Details</th>");
            out.print("<th scope=\"col\">Authorization Status</th>");
            out.print("<th scope=\"col\">Notes</th>");
            out.print("<th scope=\"col\">Requestor Pat Rel</th>");
            out.print("<th scope=\"col\">Caller Reason</th>");
            out.print("<th scope=\"col\">Requestor Contact Number</th>");
            out.print("<th scope=\"col\">Autorization Date</th>");
            out.print("</tr>");
            for (PreAuthDetails list : details) {
                if (list.getAuthType().equals("4")) {
                    out.print("<tr>");
                    if (list.getReinbursementModel() != null) {
                        String relModel = port.getValueFromCodeTableForId("Reimbursement Model", list.getReinbursementModel());
                        out.print("<td>" + relModel + "</td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (list.getFunderDetails() != null) {
                        fDetails = port.getValueFromCodeTableForId("Funder Details", list.getFunderDetails());
                        out.print("<td>" + fDetails + "</td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (list.getAuthStatus() != null) {
                        aStatus = port.getValueFromCodeTableForId("Hospital Auth Status", list.getAuthStatus());
                        out.print("<td>" + aStatus + "</td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (list.getNotes() != null) {
                        out.print("<td>" + list.getNotes() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (list.getRequestorRelationship() != null) {
                        rRela = port.getValueFromCodeTableForId("Caller Relationship", list.getRequestorRelationship());
                        out.print("<td>" + rRela + "</td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (list.getRequestorReason() != null) {
                        out.print("<td>" + list.getRequestorReason() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (list.getRequestorContact() != null) {
                        out.print("<td>" + list.getRequestorContact() + "</td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (list.getAuthorisationDate() != null) {
                        Date authdate = list.getAuthorisationDate().toGregorianCalendar().getTime();
                        authDate = formatter.format(authdate);
                        out.print("<td>" + authDate + "</td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("</tr>");
                }
            }

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }
}

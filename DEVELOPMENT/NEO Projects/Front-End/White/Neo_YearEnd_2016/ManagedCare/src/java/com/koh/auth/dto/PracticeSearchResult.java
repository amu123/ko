/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.dto;

/**
 *
 * @author whauger
 */
public class PracticeSearchResult {

    private String practiceType;
    private String practiceNumber;
    private String practiceName;
    private String practiceNetwork;

    public String getPracticeName() {
        return practiceName;
    }

    public void setPracticeName(String practiceName) {
        this.practiceName = practiceName;
    }

    public String getPracticeNumber() {
        return practiceNumber;
    }

    public void setPracticeNumber(String practiceNumber) {
        this.practiceNumber = practiceNumber;
    }

    public String getPracticeType() {
        return practiceType;
    }

    public void setPracticeType(String practiceType) {
        this.practiceType = practiceType;
    }

    public String getPracticeNetwork() {
        return practiceNetwork;
    }

    public void setPracticeNetwork(String practiceNetwork) {
        this.practiceNetwork = practiceNetwork;
    }


}

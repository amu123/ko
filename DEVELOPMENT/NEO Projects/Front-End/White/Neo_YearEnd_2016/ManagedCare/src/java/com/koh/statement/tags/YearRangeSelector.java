/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.tags;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author WernerH
 */
public class YearRangeSelector extends TagSupport {
    private String elementName;
    private String displayName;
    private String javaScript;
    private String valueFromSession;
    private String mandatory;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        //generate generic dates for period
        Date today = new Date(System.currentTimeMillis());
        String ts = new SimpleDateFormat("yyyy/MM/dd").format(today);

        ts = ts.substring(0, 4);
        String yearCurrent = ts;
        
        //switch over date validation
        boolean doSwitch = false;
        Calendar fCal = Calendar.getInstance();
        fCal.setTime(today);
        fCal.add(Calendar.MONTH, -4);
        String yearSwitch = new SimpleDateFormat("yyyy/MM/dd").format(fCal.getTime());
        yearSwitch = yearSwitch.substring(0, 4);
        
        if (yearSwitch.equalsIgnoreCase(yearCurrent)) {
            doSwitch = true;
        }

        try {
            out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td>");
            out.println("<td align=\"left\" width=\"200px\"><select style=\"width:215px\" name=\"" + elementName + "\" id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");
            if (valueFromSession.trim().equalsIgnoreCase("yes")) {
                String sessionVal = "" + session.getAttribute(elementName);
                
                if (doSwitch) {
                    for (int i = 0; i < 7; i++) { //Changed from 5 -> 8 to add 2009 & 2010
                        String year = String.valueOf(Integer.parseInt(ts) - i);
                        if (sessionVal.equals(year)) {
                            out.println("<option value=\"" + year + "\" selected>" + year + "</option>");
                        } else {
                            out.println("<option value=\"" + year + "\">" + year + "</option>");
                        }
                    }
                } else {
                    for (int i = 1; i < 8; i++) {  //Changed from 6 -> 8 to add 2009 & 2010
                        String year = String.valueOf(Integer.parseInt(ts) - i);
                        if (sessionVal.equals(year)) {
                            out.println("<option value=\"" + year + "\" selected>" + year + "</option>");
                        } else {
                            out.println("<option value=\"" + year + "\">" + year + "</option>");
                        }
                    }
                }

            }
            out.println("</select></td>");
            if (mandatory.equalsIgnoreCase("yes")) {
                out.println("<td><label class=\"red\">*</label></td>");
            } else {
                out.println("<td></td>");
            }
            //search column
            out.println("<td width=\"30px\"></td>");
            out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");

        } catch (java.io.IOException ex) {
            throw new JspException("Error in YearRangeSelector tag", ex);
        }

        return super.doEndTag();

    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setValueFromSession(String valueFromSession) {
        this.valueFromSession = valueFromSession;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }
}

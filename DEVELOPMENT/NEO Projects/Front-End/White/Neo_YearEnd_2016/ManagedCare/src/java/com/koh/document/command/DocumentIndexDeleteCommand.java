/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;

import agility.za.indexdocumenttype.DeleteDocType;
import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import agility.za.indexdocumenttype.SplitDocType;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class DocumentIndexDeleteCommand extends NeoCommand {
    private Logger logger = Logger.getLogger(this.getClass());
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------In DocumentIndexDeleteCommand---------------------");
            String folderList = (String) request.getParameter("folderList");
            String profileType = (String)request.getSession().getAttribute("profile")!=null?(String)request.getSession().getAttribute("profile"):"";
            logger.info("folderList"+folderList);
            String workDrive = new PropertiesReader().getProperty("DocumentIndexWorkDrive");
            if(folderList==null){
                folderList =  new PropertiesReader().getProperty("DocumentIndexWorkFolder");
            }
            request.setAttribute("folderList",folderList);
            request.setAttribute("listOrWork", "Work");
            String selectedFile = request.getParameter("selectedFile");
            if (selectedFile == null || selectedFile.equals("")) {
                errorMessage(request, response, "No file selected",profileType);
                return "DocumentIndexDeleteCommand";
            }
            
            IndexDocumentRequest req = new IndexDocumentRequest();
            NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
            req.setAgent(loggedInUser);
            req.setIndexType("Delete");
            req.setSrcDrive(workDrive);

            req.setEntityNumber("n/a");
            DeleteDocType deleteDocType = new DeleteDocType();
            deleteDocType.setFilename(selectedFile);
            deleteDocType.setFolder((new PropertiesReader().getProperty("DocumentIndexWorkFolder"))+"/"+profileType);
            req.setDelete(deleteDocType);
            System.out.println("Before Webservice");
            IndexDocumentResponse res = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
            if(!res.isIsSuccess()){
                errorMessage(request, response, res.getMessage(),profileType);
                return "DocumentIndexDeleteCommand";                              
            }            

            RequestDispatcher dispatcher = null;
            if(profileType.toLowerCase().contains("claim")){
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentClaims.jsp");
                    }else if(profileType.toLowerCase().contains("member")){
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentMember.jsp");
                    }else{
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentGeneric.jsp");
                    }
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "DocumentIndexDeleteCommand";
    }
    @Override
    public String getName() {
        return "DocumentIndexDeleteCommand";
    }
    
    private void errorMessage(HttpServletRequest request, HttpServletResponse response, String msg,String profileType ) throws Exception {
        request.setAttribute("errorMsg", msg);
        RequestDispatcher dispatcher = null;
        if(profileType.toLowerCase().contains("claim")){
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentClaims.jsp");
                    }else if(profileType.toLowerCase().contains("member")){
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentMember.jsp");
                    }else{
                        dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentGeneric.jsp");
                    }
        dispatcher.forward(request, response);
    }    
}
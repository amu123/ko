/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.employer.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;
import net.sf.json.JSONArray;
import org.apache.log4j.Logger;

/**
 *
 * @author yuganp
 */
public class BrokerListCommand extends NeoCommand{
    
    private static final Logger logger = Logger.getLogger(BrokerListCommand.class);
    
    private PrintWriter out = null;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        HttpSession session = request.getSession();
        
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        try {
            
             out = response.getWriter();
           
            List<Broker> brokersList = port.getAllBrokerList();
           
            //MEMBER ENTITY ID
            int entityId = (Integer) session.getAttribute("memberCoverEntityId");
        
            //LIST OF MEMBER BROKER LINKS
            List<MemberBroker> memberBrokers = port.findMemberBrokerByMemberEntityId(entityId);
           
            if (memberBrokers.isEmpty()) {
                logger.error("Cannot find broker for member");
                return null;
            } 
            
            Broker brokerForMember = null;
            
            //FIND BROKER LINKED TO MEMBER
            for (Broker broker : brokersList) {
                
                //LOOP THROUHG MEMBER BROKER LINKS
                for (MemberBroker membBro : memberBrokers) {
                   
                    if (membBro.getBrokerEntityId()==broker.getEntityId()) {
                        
                        brokerForMember = broker;
                        
                        break;
                    } 
                }
                
                if (brokerForMember!=null) break;
            }
            
            
            //LIST TO SEND BACK TO FRONTEND
            List<BrokerLookup> brokerLookup = new ArrayList<BrokerLookup>();
           
            for (Broker b : brokersList) {

                if (b.equals(brokerForMember)) {
                    brokerLookup.add(new BrokerLookup(b.getEntityId(), b.getBrokerCode(),true));
                } else {
                    brokerLookup.add(new BrokerLookup(b.getEntityId(), b.getBrokerCode()));
                }

            }
          
            JSONArray array = new JSONArray();
            array.addAll(brokerLookup);
            
            out.println(array.toString());
            
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            if (out!=null)
            out.close();
        }
                
        return null;
    }

    @Override
    public String getName() {
        return "BrokerListCommand";
    }

}

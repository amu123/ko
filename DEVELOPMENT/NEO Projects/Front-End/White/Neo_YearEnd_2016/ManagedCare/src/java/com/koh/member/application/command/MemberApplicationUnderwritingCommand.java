/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.cover.MemberMainMapping;
import com.koh.cover.commands.MemberMaintenanceTabContentCommand;
import com.koh.employer.command.TabUtils;
import com.koh.member.application.MemberAppCommon;
import com.koh.member.application.MemberAppGeneralDetailsMapping;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Exception;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Calendar;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class MemberApplicationUnderwritingCommand extends NeoCommand {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd");

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            NeoManagerBean port = service.getNeoManagerBeanPort();
            String s = request.getParameter("buttonPressed");
            if (s != null && !s.isEmpty()) {
                System.out.println("buttonPressed : " + s);
                if (s.equalsIgnoreCase("Add Details")) {
                    addDetails(port, request, response, context);
                } else if (s.equalsIgnoreCase("Save")) {
                    save(port, request, response, context);
                } else if (s.equalsIgnoreCase("SaveButton")) {
                    saveDetails(port, request, response, context);//this one
                } else if (s.equalsIgnoreCase("EditButton")) {
//                    editDetails(request, response, context);
                } else if (s.equalsIgnoreCase("RemoveButton")) {
                    removeDetails(port, request, response, context);
                } else if (s.equalsIgnoreCase("Update")) {
                    saveLPJ(port, request, response, context);
                } else if (s.equalsIgnoreCase("AddGenWait")) {
                    addWaitingPeriod(port, request, response, context);
                } else if (s.equalsIgnoreCase("Delete")) {
                    removeWaitingPeriod(port, request, response, context);
                } else if (s.equalsIgnoreCase("SearchICD")) {
                    searchICD(request, response, context);
                } else if (s.equalsIgnoreCase("Search")) {
                    search(port, request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(MemberApplicationUnderwritingCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void addDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        context.getRequestDispatcher("/MemberApplication/MemberAppConditionSpecific.jsp").forward(request, response);
    }

    private void save(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoUser neoUser = (NeoUser)request.getSession().getAttribute("persist_user");
        PrintWriter out = null;
        try {
            int applyWait = getStrToInt(request.getParameter("MemberAppUnderwriting_waitingPeriod"));
            Map map = request.getParameterMap();
            List<Integer> intList = new ArrayList<Integer>();
            for (Object s : map.keySet()) {
                String ss = s.toString();
                if (ss != null && !ss.isEmpty()) {
                    if (ss.startsWith("MemberAppUnderwriting_generalWaiting_")) {
                        String s2 = ss.substring(ss.lastIndexOf("_")+1);
                        try {
                            Integer i = Integer.parseInt(s2);
                            intList.add(i);
                        } catch (Exception e) {
                            System.out.println(s2 + ": " + e.getMessage());
                        }
                    }
                }
            }
            List<MemAppUnderwriting> uList = new ArrayList<MemAppUnderwriting>();
            for (Integer i : intList) {
                MemAppUnderwriting mu = new MemAppUnderwriting();
                mu.setAllowPmbInd(getStrToInt(request.getParameter("MemberAppUnderwriting_allowPMB_"+i)));
                mu.setApplicationNumber(getStrToInt(request.getParameter("memberAppNumber")));
                mu.setApplyUnderwritingInd(applyWait);
                Date startDate = getDate(request.getParameter("MemberAppUnderwriting_end_date_"+i));
                Date endDate = getDate(request.getParameter("MemberAppUnderwriting_start_date_"+i));
                if (startDate != null) {
                    mu.setConcessionStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
                }
                if (endDate != null) {
                    mu.setConcessionEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
                }
                mu.setConditionSpecificInd(getStrToInt(request.getParameter("MemberAppUnderwriting_conditionSpecific_"+i)));
                mu.setDependentNumber(i);
                mu.setGeneralWaitingInd(getStrToInt(request.getParameter("MemberAppUnderwriting_generalWaiting_"+i)));
                mu.setLjp(getStrToBigDecimal(request.getParameter("MemberAppUnderwriting_ljp_"+i)));
                mu.setCreatedBy(neoUser.getUserId());
                mu.setLastUpdatedBy(neoUser.getUserId());
                mu.setSecurityGroupId(neoUser.getSecurityGroupId());
                uList.add(mu);
            }
            if (!uList.isEmpty()) {
                port.saveMemAppUnderwriting(uList);
            }
            out = response.getWriter();
            out.println(TabUtils.buildJsonResult("OK", null, null, "\"message\":\"Details have been saved.\""));
        } catch (Exception ex) {
            Logger.getLogger(MemberApplicationGeneralQuestionsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            if (out != null) out.close();
            }
        }

    private void saveLPJ(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter out = null;
        try {
            List<MemAppUnderwriting> uList = new ArrayList<MemAppUnderwriting>();
            String dependantNumber = request.getParameter("memberAppDepNo");
            String memberAppNumber = request.getParameter("memberAppNumber");
            String lpj = request.getParameter("MemberAppUnderwriting_ljp_" + dependantNumber);
            String pmb = request.getParameter("MemberAppUnderwriting_allowPMB_" + dependantNumber);
            String genWaitInd = request.getParameter("MemberAppUnderwriting_generalWaiting_"+ dependantNumber);
            
            List<MemAppUnderwriting> memAppUnderwriting = port.fetchMemAppUnderwritingByAppNum(getStrToInt(memberAppNumber));
            for (MemAppUnderwriting mu : memAppUnderwriting) {
                if (mu.getDependentNumber() != getStrToInt(dependantNumber.trim())) {
                    System.out.println("member number " + mu.getDependentNumber());
                    System.out.println("member number " + getStrToInt(dependantNumber.trim()));
                    uList.add(mu);
                }
            }

            MemAppUnderwriting memApp = getUnderwritingWritting(port, dependantNumber, memberAppNumber);
            memApp.setLjp(new BigDecimal(getStrToInt(lpj)));
            memApp.setGeneralWaitingInd(!genWaitInd.isEmpty() ? Integer.parseInt(genWaitInd) : 1);
            memApp.setAllowPmbInd(getStrToInt(pmb.trim()));
            memApp.setApplicationNumber(getStrToInt(memberAppNumber));
            memApp.setDependentNumber(getStrToInt(dependantNumber));
            uList.add(memApp);
            if (!uList.isEmpty()) {
                int i = port.saveMemAppUnderwriting(uList);
            }
            out = response.getWriter();
            out.println(TabUtils.buildJsonResult("OK", null, null, "\"message\":\"Details have been saved.\""));
        } catch (Exception ex) {
            Logger.getLogger(MemberApplicationGeneralQuestionsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    private void addWaitingPeriod(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoUser neoUser = (NeoUser)request.getSession().getAttribute("persist_user");
        PrintWriter out = null;
        try {
            List<MemAppUnderwriting> uList = new ArrayList<MemAppUnderwriting>();
            String dependantNumber = request.getParameter("memberAppDepNo");
            String pmb = request.getParameter("MemberAppUnderwriting_allowPMB_" + dependantNumber);
            String memberAppNumber = request.getParameter("memberAppNumber");
            String genWaitInd = request.getParameter("MemberAppUnderwriting_generalWaiting_"+ dependantNumber);
            String ljp = request.getParameter("MemberAppUnderwriting_ljp_"+ dependantNumber);
//            List<MemAppUnderwriting> memApp = port.fetchMemAppUnderwritingByAppNum(getStrToInt(memberAppNumber));
//            MemAppUnderwriting mu2 = null;
//            if (!memApp.isEmpty()) {
//                for (MemAppUnderwriting mu : memApp) {
//                    if (mu.getDependentNumber() == getStrToInt(dependantNumber.trim())) {                        
//                        mu.setApplyUnderwritingInd(1);
//                        mu.setGeneralWaitingInd(!genWaitInd.isEmpty() ? Integer.parseInt(genWaitInd) : 1);
//                        mu.setAllowPmbInd(getStrToInt(pmb.trim()));
//                        mu.setApplicationNumber(getStrToInt(memberAppNumber));
//                        mu.setCreatedBy(neoUser.getUserId());
//                        mu.setLastUpdatedBy(neoUser.getUserId());
//                        mu.setSecurityGroupId(neoUser.getSecurityGroupId());
//                        uList.add(mu);
//                    } else {
//                        uList.add(mu);
//                        mu2 = new MemAppUnderwriting();
//                        mu2.setDependentNumber(getStrToInt(dependantNumber.trim()));
//                        mu2.setApplyUnderwritingInd(1);
//                        mu2.setGeneralWaitingInd(!genWaitInd.isEmpty() ? getStrToInt(genWaitInd) : 1);
//                        mu2.setAllowPmbInd(getStrToInt(pmb.trim()));
//                        mu2.setApplicationNumber(getStrToInt(memberAppNumber));
//                        mu2.setCreatedBy(neoUser.getUserId());
//                        mu2.setLastUpdatedBy(neoUser.getUserId());
//                        mu2.setSecurityGroupId(neoUser.getSecurityGroupId());
//                        uList.add(mu2);
//                    }
//                }
//            } else {
                MemAppUnderwriting mu = new MemAppUnderwriting();
                mu.setDependentNumber(getStrToInt(dependantNumber.trim()));
                mu.setApplyUnderwritingInd(1);
                mu.setGeneralWaitingInd(!genWaitInd.isEmpty() ? Integer.parseInt(genWaitInd) : 1);
                mu.setAllowPmbInd(getStrToInt(pmb.trim()));
                mu.setLjp(getStrToBigDecimal(ljp));
                mu.setApplicationNumber(getStrToInt(memberAppNumber));
                mu.setCreatedBy(neoUser.getUserId());
                mu.setLastUpdatedBy(neoUser.getUserId());
                mu.setSecurityGroupId(neoUser.getSecurityGroupId());
                uList.add(mu);
//            }

            Date endDate = getDate(request.getParameter("MemberAppUnderwriting_start_date_" + dependantNumber));
            Date startDate = getDate(request.getParameter("MemberAppUnderwriting_end_date_" + dependantNumber));
//            if (startDate != null) {
//                memApp.setConcessionStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
//            }
//            if (endDate != null) {
//                memApp.setConcessionEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
//            }
//            uList.add(memApp);
            if (!uList.isEmpty()) {
                int i = port.saveMemAppUnderwriting(uList);
            }
            out = response.getWriter();
            out.println(TabUtils.buildJsonResult("OK", null, null, "\"message\":\"Details have been saved.\""));
        } catch (Exception ex) {
            Logger.getLogger(MemberApplicationGeneralQuestionsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    private void removeWaitingPeriod(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter out = null;
        try {
            List<MemAppUnderwriting> uList = new ArrayList<MemAppUnderwriting>();
            String dependantNumber = request.getParameter("memberAppDepNo");
            String pmb = request.getParameter("MemberAppUnderwriting_allowPMB_" + dependantNumber);
            String memberAppNumber = request.getParameter("memberAppNumber");            

            List<MemAppUnderwriting> memApp = port.fetchMemAppUnderwritingByAppNum(getStrToInt(memberAppNumber));
            if (!memApp.isEmpty()) {
                for (MemAppUnderwriting mu : memApp) {
                    if (mu.getDependentNumber() == getStrToInt(dependantNumber.trim())) {
                        mu.setApplyUnderwritingInd(0);
                        mu.setGeneralWaitingInd(0);
                        mu.setAllowPmbInd(0);
                        mu.setApplicationNumber(getStrToInt(memberAppNumber));
                        uList.add(mu);
                    } else {
                        uList.add(mu);
                    }
                }
            }
//            Date endDate = getDate(request.getParameter("MemberAppUnderwriting_start_date_" + dependantNumber));
//            Date startDate = getDate(request.getParameter("MemberAppUnderwriting_end_date_" + dependantNumber));
//            if (startDate != null) {
//                memApp.setConcessionStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
//            }
//            if (endDate != null) {
//                memApp.setConcessionEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
//            }
           
            if (!uList.isEmpty()) {
                int i = port.saveMemAppUnderwriting(uList);
            }
            out = response.getWriter();
            out.println(TabUtils.buildJsonResult("OK", null, null, "\"message\":\"Details have been saved.\""));
            request.getRequestDispatcher(new MemberAppTabContentsCommand().getMemberAppUnderwriting(port, request)).forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(MemberApplicationGeneralQuestionsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
        
    }

    private MemAppUnderwriting getUnderwritingWritting(NeoManagerBean port, String depNumber, String appNum) {
        List<MemAppUnderwriting> memAppUnderwriting = port.fetchMemAppUnderwritingByAppNum(getStrToInt(appNum));
        for (MemAppUnderwriting mu : memAppUnderwriting) {
            return mu.getDependentNumber() == getStrToInt(depNumber) ? mu : new MemAppUnderwriting();
        }
        return new MemAppUnderwriting();
    }

    private void saveDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {      
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        
        Map<String, String> errors = MemberMainMapping.validateUnderwritting(request);
        String strStartDate = request.getParameter("startDate");
        String strEndDate = request.getParameter("endDate");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String ICDCode = request.getParameter("Disorder_diagCode_text");
        Calendar cal = Calendar.getInstance(); 
        Date startDate = null;
        Date endDate = null;
        Integer appNum = new Integer((String) request.getAttribute("memberAppNumber"));
        
        //clear errors
        errors.put("Disorder_diagCode_error","");
        errors.put("startDate_error","");
        errors.put("endDate_error","");
        errors.put("saveButton_error","");
        
        MemberApplication ma = port.getMemberApplicationByApplicationNumber(appNum);
        Date inceptionDate = null;
        XMLGregorianCalendar coverStartDate = ma.getCoverStartDate();
        if(coverStartDate == null){
            errors.put("saveButton_error", "* No inception date found. Provide inception date on capture screen.");
        }
        else{
            inceptionDate = coverStartDate.toGregorianCalendar().getTime();
        }
        System.out.println("Inception Date: " + inceptionDate);
        int period = Integer.parseInt(request.getParameter("conditionSpecificDeptList"));
        
        try {
            if(ICDCode == "" || ICDCode == null || ICDCode.isEmpty()){
                errors.put("Disorder_diagCode_error","ICD is required");
            }
            
            if(strStartDate == "" || strStartDate == null || strStartDate.isEmpty()){
                startDate = inceptionDate;
            }
            else{
                startDate = dateFormat.parse(strStartDate);
            }
            
            if(strEndDate == "" || strEndDate == null || strEndDate.isEmpty()){
                cal.setTime(startDate);
                cal.set(Calendar.MONTH, (cal.get(Calendar.MONTH)+period));
                endDate = cal.getTime();
            }
            else{
                endDate = dateFormat.parse(strEndDate);
            }
            
            if (startDate.before(inceptionDate)) {
                errors.put("startDate_error", "Effective start date of waiting period cannot be before inception date");
            }
            
            //if start date is greater than enddate
            if(startDate.after(endDate)){
                 errors.put("endDate_error","The end date cannot be before the start date");
            }
            
            System.out.println("Start Date: "+startDate);
            System.out.println("End Date: "+endDate);
        } catch (Exception e) {

        }
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        System.out.println("Status: "+ status);
        
        String additionalData = null;
        if(status != "VALIDATION"){
            status = "OK";
            if (saveDetails(port, request, startDate, endDate)) {

            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving.\"";
            }
        }
        System.out.println("Status: "+ status);
        if ("OK".equalsIgnoreCase(status)) {
            context.getRequestDispatcher(new MemberAppTabContentsCommand().getMemberAppUnderwriting(port, request)).forward(request, response);
        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, null, additionalData));
        }
    }

    private boolean saveDetails(NeoManagerBean port, HttpServletRequest request, Date startDate, Date endDate) {
        NeoUser neoUser = (NeoUser)request.getSession().getAttribute("persist_user");
        try {
            MemAppCondWait cw = new MemAppCondWait();
            String disorder_diagCode = request.getParameter("Disorder_diagCode_text");
            cw.setApplicationNumber(getStrToInt(request.getParameter("memberAppNumber")));
            cw.setDependentNumber(getStrToInt(request.getParameter("memberAppDepNo")));
            cw.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
            cw.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
            cw.setDiagCode(disorder_diagCode);
            cw.setCreatedBy(neoUser.getUserId());
            cw.setLastUpdatedBy(neoUser.getUserId());
            cw.setSecurityGroupId(neoUser.getSecurityGroupId());
            port.saveMemAppCondWait(cw);
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
    }

    private void removeDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        String status = "OK";
        String additionalData = null;
        if (removeDetails(port, request)) {
                
        } else {
            status = "ERROR";
            additionalData = "\"message\":\"There was an error removing conditional waiting period.\"";
        }
        if ("OK".equalsIgnoreCase(status)) {
            context.getRequestDispatcher(new MemberAppTabContentsCommand().getMemberAppUnderwriting(port, request)).forward(request, response);
        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, null, null, additionalData));
        }
    }

    private boolean removeDetails(NeoManagerBean port, HttpServletRequest request) {
        try {
            Security sec = TabUtils.getSecurity(request);
            int waitId = TabUtils.getIntParam(request, "memberAppCondWaitId");
            port.removeMemAppCondWait(waitId, sec);
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
    }
    
    private void searchICD(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
//            request.setAttribute("MemAppSearchResults", col);
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        request.setAttribute("diagCodeId", "Disorder_diagCode_text");
        request.setAttribute("diagNameId", "Disorder_diagName");
        request.setAttribute("diagCodeId2", "disorderId_text");
        context.getRequestDispatcher("/MemberApplication/MemberAppCondWaitSearch.jsp").forward(request, response);

    }
    
    private void search(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        DiagnosisSearchCriteria criteria = new DiagnosisSearchCriteria();
        criteria.setCode(request.getParameter("ICDCode"));  
        criteria.setDescription(request.getParameter("ICDDescription"));
        List<Diagnosis> diag = port.findAllDiagnosisByCriteria(criteria);
        request.setAttribute("ICD10SearchResults", diag);
        context.getRequestDispatcher("/MemberApplication/MemberAppCondWaitResults.jsp").forward(request, response);
    }

    private int getStrToInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (java.lang.Exception e) {
            System.out.println(s + " = " + e.getMessage());
        }
        return 0;
    }

    private BigDecimal getStrToBigDecimal(String s) {
        try {
            return new BigDecimal(s);
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

    private Date getDate(String dateStr) {
        try {
            return DATE_FORMAT.parse(dateStr);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String getName() {
        return "MemberApplicationUnderwritingCommand";
    }
    
}

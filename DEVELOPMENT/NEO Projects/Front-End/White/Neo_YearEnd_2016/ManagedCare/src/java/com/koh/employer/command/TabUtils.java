/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.employer.command;

import com.koh.employer.EmployerGroupMapping;
import com.koh.utils.DateTimeUtils;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author yuganp
 */
public class TabUtils {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd");
    public static final String INT_MESSAGE = " must be a number.";
    public static final String DATE_MESSAGE = " must be in the format (yyyy/mm/dd)";
    public static final String NAME_MESSAGE = " Allowed";
    public static final String NUMBER_FIELD_MESSAGE = " must only contain numbers [0 - 9]";

    public static Map<String, String> checkMandatory(HttpServletRequest request, String fieldPrefix, String[] requiredFields, String[] requiredFieldDescriptions) {
        Map<String, String> errors = new HashMap<String, String>();
        for (int i = 0; i < requiredFields.length; i++) {
            String value = request.getParameter(fieldPrefix + requiredFields[i]);
            if (value == null || value.trim().isEmpty()) {
                errors.put(fieldPrefix + requiredFields[i] + "_error", requiredFieldDescriptions[i] + " is required");
            } else {
                errors.put(fieldPrefix + requiredFields[i] + "_error", "");
            }
        }
        return errors;
    }

    private static boolean isValid(Map<String, String> errors) {
        boolean result = true;
        if (errors == null) {
            return result;
        }
        for (String s : errors.keySet()) {
            String error = errors.get(s);
            if (error != null && !error.isEmpty()) {
                result = false;
                break;
            }
        }
        return result;
    }

    public static String convertMapToJSON(Map<String, String> errors) {
        if (errors == null || errors.isEmpty()) {
            return "";
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            for (String s : errors.keySet()) {
                sb.append("{");
                sb.append("\"id\":");
                sb.append("\"" + s + "\",");
                sb.append("\"value\":");
                sb.append("\"" + errors.get(s) + "\"");
                sb.append("},");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append("]");
            return sb.toString();
        }
    }

    public static String buildJsonResult(String status, String validationErrors, String updateFields, String additionalData) {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"status\":\"" + status + "\"");
        if (validationErrors != null && !validationErrors.isEmpty()) {
            sb.append(",\"validation_errors\":" + validationErrors);
        }
        if (updateFields != null && !updateFields.isEmpty()) {
            sb.append(",\"update_fields\":" + updateFields);
        }
        if (additionalData != null && !additionalData.isEmpty()) {
            sb.append("," + additionalData);
        }
        sb.append("}");
        String s = sb.toString();

        return s.trim();
    }

    public static String getStatus(Map<String, String> errors) {
        return isValid(errors) ? "OK" : "VALIDATION";
    }

    public static boolean isValidDate(String value) {
        try {
            DATE_FORMAT.parse(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public static boolean isOneWordStr(String value) {
        return value.contains(" ") ? false : true;
    }
    
    public static boolean isNumberField(String value) {
        return value!=null && !value.isEmpty() ? Pattern.matches("[0-9]+",value) : true;
    }
    
    public static boolean isRecent(String value) {
        Calendar cal = Calendar.getInstance();;
        cal.add(Calendar.YEAR, -100);
        Date maxDate = cal.getTime();
        
        Calendar cal2 = Calendar.getInstance();;
        cal2.add(Calendar.YEAR, 100);
        Date maxDate2 = cal2.getTime();
        try {
            Date input = DATE_FORMAT.parse(value);
            if (input.before(maxDate) || input.after(maxDate2)) {
                if (value.equals("2999/12/31")) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public static boolean isValidInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }   

    public static Date getEndDate() {
        try {
            return DATE_FORMAT.parse("2999/12/31");
        } catch (Exception e) {
            return null;
        }
    }

    public static int getIntParam(HttpServletRequest request, String param) {
        try {
            String paramValue = request.getParameter(param);
            if (paramValue != null && !paramValue.isEmpty()) {
                return Integer.parseInt(paramValue);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public static BigDecimal getBDParam(HttpServletRequest request, String param) {
        try {
            String paramValue = request.getParameter(param);
            if (paramValue != null && !paramValue.isEmpty()) {
                return new BigDecimal(paramValue);
            }
        } catch (Exception e) {
        }
        return new BigDecimal(0);
    }

    public static String getStringFromDate(Date dt) {
        return DATE_FORMAT.format(dt);
    }

    public static Date getDateParam(HttpServletRequest request, String param) {
        try {
            String paramValue = request.getParameter(param);
            if (paramValue != null && !paramValue.isEmpty()) {
                return DATE_FORMAT.parse(paramValue);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static javax.xml.datatype.XMLGregorianCalendar getXMLDateParam(HttpServletRequest request, String param) {
        try {
            String paramValue = request.getParameter(param);
            if (paramValue != null && !paramValue.isEmpty()) {
                return DateTimeUtils.convertDateToXMLGregorianCalendar(DATE_FORMAT.parse(paramValue));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static void validate(HttpServletRequest request, Map<String, String> errors, String[][] fieldMappings) {
        for (String[] s : fieldMappings) {
            String value = request.getParameter(s[0]);
            boolean valid = true;
            if (s[2].equalsIgnoreCase("yes") && (value == null || value.trim().isEmpty())) {
                String field;
                if (s[0].endsWith("_text")) {
                    field = s[0].substring(0, s[0].length() - 5);
                } else {
                    field = s[0];
                }
                errors.put(field + "_error", s[1] + " is required.");
                valid = false;
            } else if (value != null && !value.trim().isEmpty()) {
                if (s[3].equalsIgnoreCase("int")) {
                    valid = isValidInt(value);
                    errors.put(s[0] + "_error", s[1] + INT_MESSAGE);
                } else if (s[3].equalsIgnoreCase("date")) {
                    valid = isValidDate(value);
                    valid = isRecent(value);
                    errors.put(s[0] + "_error", s[1] + DATE_MESSAGE);
                } else if (s[3].equalsIgnoreCase("OneWordStr")){
                    valid = isOneWordStr(value);
                    errors.put(s[0] + "_error", "Only " +s[1] + NAME_MESSAGE);
                } else if (s[3].equalsIgnoreCase("numberField")){
                    valid = isNumberField(value);
                    errors.put(s[0] + "_error", s[1] + NUMBER_FIELD_MESSAGE);
                }
                
            }
            if (valid) {
                if (s.length > 7) {
                    int min = getIntValue(s[6]);
                    int max = getIntValue(s[7]);
                    if (!(value == null || value.trim().isEmpty())) {
                        if (value.length() < min || value.length() > max) {
                            String field = "";
                            if (s[0].endsWith("_text")) {
                                field = s[0].substring(0, s[0].length() - 5);
                            } else {
                                field = s[0];
                            }
                            if (min == 0) {
                                errors.put(field + "_error", s[1] + " must be less than " + (max + 1) + " characters");
                            } else {
                                errors.put(field + "_error", s[1] + " must be between " + min + " and " + max + " characters");
                            }
                            valid = false;
                        }
                    }
                }
            }
            if (valid) {
                errors.put(s[0] + "_error", "");
            }
        }
    }

    public static Object getDTOFromRequest(HttpServletRequest request, Class className, String[][] mapping, String mapValue) {
        try {
            Object classInstance;
            classInstance = className.newInstance();
            Hashtable methodArguments = new Hashtable();
            Map<String, Method> methods = new HashMap<String, Method>();
            for (Method m : className.getDeclaredMethods()) {
                methods.put(m.getName(), m);
                if (m.getParameterTypes().length > 0) {
                    methodArguments.put(m.getName(), m.getParameterTypes()[0].getName());
                }
            }
            for (String[] s : mapping) {
                if (s.length > 5) {
                    if (s[4].equalsIgnoreCase(mapValue) && !s[5].isEmpty()) {
                        String S5 = s[5];
                        S5 = S5.substring(0, 1).toUpperCase() + S5.substring(1);
                        String methodName = "set" + S5;
                        String paramName = (String) methodArguments.get(methodName);
                        if (paramName == null) {
                            continue;
                        }
                        Class paramTypes[] = new Class[1];
                        Object arguments[] = new Object[1];
                        if (s[3].equalsIgnoreCase("int")) {
                            paramTypes[0] = int.class;
                            arguments[0] = TabUtils.getIntParam(request, s[0]);
                        } else if (s[3].equalsIgnoreCase("str")) {
                            paramTypes[0] = java.lang.String.class;
                            arguments[0] = request.getParameter(s[0]);
                        } else if (s[3].equalsIgnoreCase("date")) {
                            //paramTypes[0] = java.util.Date.class;
                            paramTypes[0] = javax.xml.datatype.XMLGregorianCalendar.class;
                            arguments[0] = TabUtils.getXMLDateParam(request, s[0]);
                        } else if (s[3].equalsIgnoreCase("bd")) {
                            paramTypes[0] = java.math.BigDecimal.class;
                            arguments[0] = getBDParam(request, s[0]);
                        } else if (s[3].equalsIgnoreCase("OneWordStr")) {
                            paramTypes[0] = java.lang.String.class;
                            arguments[0] = request.getParameter(s[0]);
                        } else if (s[3].equalsIgnoreCase("numberField")) {
                            paramTypes[0] = java.lang.String.class;
                            arguments[0] = request.getParameter(s[0]);
                        }
                        Method method = className.getMethod(methodName, paramTypes);
                        method.invoke(classInstance, arguments);
                    }
                }
            }
            return classInstance;
        } catch (InstantiationException ex) {
            Logger.getLogger(EmployerGroupMapping.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(EmployerGroupMapping.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setRequestFromDTO(HttpServletRequest request, Object obj, String[][] mapping, String mapValue) {
        if (obj == null) {
//            System.out.println("Setting from DTO to null");
            return;
        }
//        System.out.println("Setting from DTO " + obj.getClass().getName());
        Method[] methods = obj.getClass().getDeclaredMethods();
        Object[] objParam = new Object[0];
        for (String[] s : mapping) {
            if (s.length < 6 || !s[4].equalsIgnoreCase(mapValue) || s[5].isEmpty()) {
                continue;
            }
            for (Method m : methods) {
                String S5 = s[5];
                S5 = S5.substring(0, 1).toUpperCase() + S5.substring(1);
                if (m.getName().equalsIgnoreCase("get" + S5)) {
                    try {
                        Object value = m.invoke(obj, objParam);
//                        System.out.println("Setting " + s[0] + " to " + value);
                        if (value == null) {
                            request.setAttribute(s[0], "");
                        } else if (value instanceof java.util.Date) {
                            request.setAttribute(s[0], getStringFromDate((Date) value));
                        } else if (value instanceof javax.xml.datatype.XMLGregorianCalendar) {
                            request.setAttribute(s[0], getStringFromDate(((javax.xml.datatype.XMLGregorianCalendar) value).toGregorianCalendar().getTime()));
                        } else {
                            request.setAttribute(s[0], value.toString());
                        }
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(TabUtils.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(TabUtils.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InvocationTargetException ex) {
                        Logger.getLogger(TabUtils.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }

    }

    private static int getIntValue(String s) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            return 0;
        }
    }

    public static Security getSecurity(HttpServletRequest request) {
        NeoUser neoUser = (NeoUser) request.getSession().getAttribute("persist_user");
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);
        return sec;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverDetails;
import neo.manager.CoverPDCDetails;
import neo.manager.NeoManagerBean;
import neo.manager.PdcSearchCriteria;
import neo.manager.PersonDetails;

/**
 *
 * @author gerritr
 */
public class CoverDependantResultFilterCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        int in = new Integer(request.getParameter("ID_text"));
        session.setAttribute("listIndex", in);
        session.setAttribute("searchView", null);
        //String nextJSP = null;

        List<CoverDetails> cDetails = (List<CoverDetails>) session.getAttribute("MemberCoverDetails");
        if (!cDetails.isEmpty()) {
            CoverDetails cds = cDetails.get(in);
            PdcSearchCriteria search = new PdcSearchCriteria();
            search.setCoverNumber(cds.getCoverNumber());

            Format formatter = new SimpleDateFormat("yyyy/MM/dd");
            String s = "";
            CoverPDCDetails list = port.getCoverProductDetailsByCoverNumberForPDC(cds.getCoverNumber(), cds.getDependentNumber());

            Date endDate = list.getEndDate().toGregorianCalendar().getTime();
            s = formatter.format(endDate);

            if (!s.equals("2999/12/31")) {
                session.setAttribute("activeMember", "Member is not active");
            } else {
                session.setAttribute("activeMember", "");
            }
            if (list.getPatientName() != null) {
                String a = list.getPatientName().substring(1, list.getPatientName().length());
                String b = list.getPatientName().substring(0, 1);
                String c = b + a.toLowerCase();
                session.setAttribute("patientName", c);
            }

            if (list.getPatientSurname() != null) {
                String d = list.getPatientSurname().substring(1, list.getPatientSurname().length());
                String e = list.getPatientSurname().substring(0, 1);
                String f = e + d.toLowerCase();
                session.setAttribute("surname", f);
            }

            if (list.getPatientName() != null) {

                //Info needed for patient details
                PersonDetails person = service.getNeoManagerBeanPort().getPersonDetailsByEntityId(list.getEntityId());

                session.setAttribute("policyNumber", cds.getCoverNumber());
                session.setAttribute("policyStatus", list.getCoverStatus());
                session.setAttribute("dependantNumber", cds.getDependentNumber());
                session.setAttribute("title", person.getTitle());
                session.setAttribute("initials", person.getInitials());
                session.setAttribute("patientNumber", list.getCoverNumber());
                session.setAttribute("patientGender", list.getGender().getValue());

                Date update = list.getDateOfBith().toGregorianCalendar().getTime();
                s = formatter.format(update);
                session.setAttribute("birthDate", s);
                session.setAttribute("idNumber", person.getIDNumber());
                session.setAttribute("matitalStatus", person.getMaritalStatus());
                session.setAttribute("homeLanguage", person.getHomeLanguage());
                session.setAttribute("employerName", person.getEmployer());
                session.setAttribute("incomeCategoty", person.getIncomeCategory());
                session.setAttribute("incomeCategoty", person.getJobTitle());
                session.setAttribute("productName", list.getProductName());
                session.setAttribute("optionName", list.getOptionName());
            }
//            try {
//                
//                nextJSP = "/PDC/PolicyHolderDetails.jsp";
//                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
//                System.out.println("NextJSP: " + nextJSP);
//                dispatcher.forward(request, response);
//
//            } catch (Exception ex) {
//                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//            }

            PrintWriter out = null;

            try {
                out = response.getWriter();

                if (cDetails != null) {
                    out.print("Done|");
                } else {
                    out.print("Failed|");
                }
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(CoverDependantResultCommand.class
                        .getName()).log(Level.SEVERE, null, ex);
                System.out.print(
                        "Exception PRINTWRITER: " + ex);
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "CoverDependantResultFilterCommand";
    }
}

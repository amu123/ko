/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AnswerDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class SubmitQuestionSetCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        System.out.println("entered SaveQuestionSetCommand");

        NeoManagerBean port = service.getNeoManagerBeanPort();

        HashMap<String, Integer> answer_list = (HashMap<String, Integer>) session.getAttribute("answered_list");
        HashMap<Integer, String> list = new HashMap<Integer, String>();

        Integer answer_size = (Integer) session.getAttribute("answered_list_size");
        int size = answer_size;
        Integer radion_size = (Integer) session.getAttribute("radion_size");
        int rSize = radion_size;

        Integer answerId = (Integer) session.getAttribute("answerId");
        String coverNumber = (String) session.getAttribute("policyNumber");
        Integer depenNo = (Integer) session.getAttribute("dependantNumber");
        int dependentNumber = depenNo;

        Collection<AnswerDetails> answerDetails = new ArrayList<AnswerDetails>();
        for (int i = 0; i < size; i++) {
            AnswerDetails a = new AnswerDetails();
            String ans = request.getParameter("question_" + i);

            if (ans != null) {
                if (!ans.equals("")) {
                    /*System.out.println("Question " + i + ": Answer: " + request.getParameter("question_" + i) +
                    " | Question Id " + answer_list.get("question_" + i));*/
                    if (answerId != null) {
                        a.setAnswerId(answerId);
                    }
                    a.setQuestionId(answer_list.get("question_" + i));
                    a.setAnswer(ans);
                    answerDetails.add(a);
                    list.put(answer_list.get("question_" + i), ans);
                }
            }

        }

        for (int i = 900; i <= rSize; i++) {
            AnswerDetails a = new AnswerDetails();
            String ans = request.getParameter("question_" + i);
            System.out.println("-------------Radio Value: " + ans);
            if (ans != null) {
                if (!ans.equals("")) {
                    /*System.out.println("Question " + i + ": Answer: " + request.getParameter("question_" + i) +
                    " | Question Id " + radion_list.get("question_" + i));*/
                    if (answerId != null) {
                        a.setAnswerId(answerId);
                    }
                    a.setQuestionId(new Integer(ans));
                    a.setAnswer(ans);
                    answerDetails.add(a);
                    list.put(new Integer(ans), ans);

                }
            }
        }

        session.setAttribute("sessionAnswers", list);
        session.setAttribute("sessionHash", "yes");
        session.setAttribute("catched", "yes");

        for (AnswerDetails ans : answerDetails) {
            System.out.println("getAnswerId :" + ans.getAnswerId());
            System.out.println("getQuestionId " + ans.getQuestionId());
            System.out.println("getAnswer " + ans.getAnswer());
            System.out.println("-------------------------------- " + answerDetails.size());
        }

        String view = (String) session.getAttribute("qaction");
        Integer quesId = (Integer) session.getAttribute("questionnaireId");
        if (view.equals("create")) {
            port.createNewFinalQuestionnaireAnswers((List<AnswerDetails>) answerDetails, coverNumber, dependentNumber, quesId);
            //System.out.println("createNewFinalQuestionnaireAnswers");
        } else if (view.equals("save")) {
            port.submitFinalQuestionnaireAnswers((List<AnswerDetails>) answerDetails, answerId);
            //System.out.println("submitFinalQuestionnaireAnswers");
        }

        try {
            String nextJSP = "/PDC/CaseManagement.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "SubmitQuestionSetCommand";
    }
}

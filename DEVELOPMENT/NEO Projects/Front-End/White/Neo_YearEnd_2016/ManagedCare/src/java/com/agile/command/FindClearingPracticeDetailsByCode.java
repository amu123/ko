/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ProviderSearchDetails;

/**
 *
 * @author johanl
 */
public class FindClearingPracticeDetailsByCode extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;

        String providerNumber = request.getParameter("provNum");
        String element = request.getParameter("element");
        try {
            out = response.getWriter();
            if (providerNumber != null && !providerNumber.trim().equalsIgnoreCase("null")) {
                ProviderSearchDetails pd = service.getNeoManagerBeanPort().findPracticeAjaxDetailsByNumber(providerNumber);

                if (pd.getProviderNo() != null && !pd.getProviderNo().trim().equals("")) {
                    String provDescId = pd.getDisciplineType().getId();
                    String provDesc = pd.getDisciplineType().getValue();
                    String providerDesc = provDescId + " - " + provDesc;
                    String providerName = pd.getProviderName();

                    String responseText = "";
                    if (pd.getClearingNetworkDetails() == null) {
                        responseText = "Error|Invalid Network Provider";

                    } else {
                        String networkProvider = pd.getClearingNetworkDetails().getPracticeNo();
                        if (providerNumber.trim().equalsIgnoreCase(networkProvider)) {
                            responseText = "Error|Invalid Network Provider";
                            
                        } else {

                            session.setAttribute(element + "Name", providerName);
                            session.setAttribute(element + "Surname", providerName);
                            session.setAttribute(element + "DiscType", provDesc);
                            session.setAttribute(element + "DiscTypeId", provDescId);

                            responseText = "Done|ProviderName=" + providerName + "|ProviderDiscipline=" + providerDesc + "$";
                        }
                    }

                    out.println(responseText);
                } else {
                    out.println("Error|No such provider|" + getName());
                }

            } else {
                out.println("Error|No such provider|" + getName());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "FindClearingPracticeDetailsByCode";
    }
}

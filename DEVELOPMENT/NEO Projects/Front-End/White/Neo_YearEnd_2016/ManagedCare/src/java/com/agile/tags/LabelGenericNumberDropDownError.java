/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.log4j.Logger;

/**
 *
 * @author marcelp
 */
public class LabelGenericNumberDropDownError extends TagSupport {

    private Logger logger = Logger.getLogger(this.getClass());
    private String displayName;
    private String elementName;
    private int listSize;
    private String javaScript;
    private String mandatory = "no";
    private String errorValueFromSession = "no";

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        try {
            out.println("<td><label class=\"label\">" + displayName + ":</label></td>");
            out.println("<td><select style=\"width:215px\" name=\"" + elementName + "\" id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");
            String selectedBox = "" + session.getAttribute(elementName);
            String iterator;
            out.println("<option value=\"\" selected ></option>");
            for (int i = 0; i < listSize; i++) {
                iterator = ""+i;
                 if (iterator.equalsIgnoreCase(selectedBox)) {
                    out.println("<option value=\"" + i + "\" selected >" + i + "</option>");
                } else {
                    out.println("<option value=\"" + i + "\" >" + i + "</option>");
                }
            }
            out.println("</select></td>");
            if (mandatory.equalsIgnoreCase("yes")) {
                out.print("<td><label class=\"red\">*</label></td>");
            } else {
                out.print("<td></td>");
            }

            String sessionValerror = "";
            if (errorValueFromSession.equalsIgnoreCase("yes")) {

                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                }

                if (sessionValerror != null && !sessionValerror.equalsIgnoreCase("null")) {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
                } else {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
                }
            }

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelGenericNumberDropDownError tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setListSize(int listSize) {
        this.listSize = listSize;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setErrorValueFromSession(String errorValueFromSession) {
        this.errorValueFromSession = errorValueFromSession;
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.PdcUsers;

/**
 *
 * @author chrisdp
 */
public class PdcLoadAssignedUsers extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;
        NeoManagerBean port = service.getNeoManagerBeanPort();
        ArrayList<PdcUsers> pdcUsers = (ArrayList) port.getDetailsforPdcUsers();

//        for (PdcUsers c : pdcUsers) {
//            System.out.println("LookupVal: " + c.getValue());
//            System.out.println("LookupMean: " + c.getMeaning());
//        }
        try {
            out = response.getWriter();
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(PdcLoadAssignedUsers.class.getName()).log(Level.SEVERE, null, ex);
        }

        session.setAttribute("pdcUserDetails", pdcUsers);
        System.out.println("pdcUserDetails Session content ======" + session.getAttribute("pdcUserDetails").toString());

        if (out != null) {
            if (pdcUsers.isEmpty()) {
                out.print("Failed|");
                System.out.println("FAILED|PDC User details list is empty.");
            } else {
                out.print("Done|");
                System.out.println("DONE|PDC User details list is populated.");
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "PdcLoadAssignedUsers";
    }
}

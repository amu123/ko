/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.nclaims.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Modifier;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author janf
 */
public class ClaimLineModifierCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String command = request.getParameter("command");
        HttpSession session = request.getSession();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        System.out.println("Operation: ClaimLineModifierCommand - Command: " + command);


        if ("NewModifierLine".equalsIgnoreCase(command)) {
            try{
                List<Modifier> modList = (List<Modifier>) session.getAttribute("modList");
                String capOrEdit = request.getParameter("capOrEdit");
                System.out.println("capOrEdit = " + capOrEdit);
                System.out.println("modList = " + modList);

                if(modList != null && !modList.isEmpty()){
                    session.setAttribute("modList", modList);
                    System.out.println("clNdcList size = " + modList.size());
                }

                if("edit".equalsIgnoreCase(capOrEdit)){
                    RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/EditClaims_EditModifier.jsp");
                    dispatcher.forward(request, response);
                } else {
                    RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/CaptureClaims_CaptureModifier.jsp");
                    dispatcher.forward(request, response);
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            
        } else if("SaveModifierLine".equalsIgnoreCase(command)){
            try {
                Modifier mod = saveModifierLine(request, port, session);
                System.out.println("Modifier : " + mod.getModifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            
        } else if("fetchModifierLines".equalsIgnoreCase(command)){
            try{
                List<Modifier> modifier = (List<Modifier>) session.getAttribute("modList");

                if(modifier != null && !modifier.isEmpty()){
                    System.out.println("modifier not empty");
                    session.setAttribute("modList", modifier);
                }
            
                RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/CaptureClaims_ModifierSummary.jsp");
                dispatcher.forward(request, response);
            } catch(Exception e){
                e.printStackTrace();
            }
        } else if("removeModifierFromList".equalsIgnoreCase(command)){
            List<Modifier> modList = (List<Modifier>) session.getAttribute("modList");
            try {
                int index = Integer.parseInt(request.getParameter("modifierIndex"));
                Modifier removeObj = modList.get(index);
                modList.remove(removeObj);

                session.setAttribute("modList", modList);

            } catch (Exception e) {
                e.printStackTrace();
            }
            
            try {
                String nextJSP = "/NClaims/CaptureClaims_ModifierSummary.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

                dispatcher.forward(request, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if("modifyModifierFromList".equalsIgnoreCase(command)){
            List<Modifier> modList = (List<Modifier>) session.getAttribute("modList");
            String ti = request.getParameter("modifierIndex");
            int index = Integer.parseInt(ti);

            try {
                PrintWriter out = response.getWriter();
                Modifier modifyObject = modList.get(index);

                out.print("Done|"
                        + modifyObject.getModifier() + "|"
                        + modifyObject.getQuantity()+ "|"
                        + modifyObject.getTotalTime());


                //remove current object from session value
                modList.remove(modifyObject);

                session.setAttribute("modList", modList);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    private Modifier saveModifierLine(HttpServletRequest request, NeoManagerBean port,HttpSession session) {
        List<Modifier> modList = (List<Modifier>) session.getAttribute("modList");
        
        Modifier modifier = new Modifier();
        modifier.setModifier(request.getParameter("ClaimLine_ModifierCode"));
        modifier.setQuantity(new Double(request.getParameter("ClaimLine_ModifierUnits")));
        modifier.setTotalTime(new Integer(request.getParameter("ClaimLine_ModifierTime")));
        System.out.println("modifier = " + modifier.getModifier() + "\nQuantity = " + modifier.getQuantity() + "\nTime = " + modifier.getTotalTime());
        
        if(modList != null && !modList.isEmpty()){
            modList.add(modifier);
            System.out.println("Not empty = " + modList.size());
        } else {
            modList = new ArrayList<Modifier>();
            modList.add(modifier);
            System.out.println("Empty = " + modList.size());
        }
        
        session.setAttribute("modList", modList);
        return modifier;
    }

    @Override
    public String getName() {
        return "ClaimLineModifierCommand";
    }
    
}

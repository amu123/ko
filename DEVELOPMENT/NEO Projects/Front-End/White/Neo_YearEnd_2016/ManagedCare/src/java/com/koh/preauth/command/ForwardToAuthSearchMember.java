/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.auth.command.*;
import com.koh.command.Command;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author gerritj
 */
public class ForwardToAuthSearchMember extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        String authUpdate = "" + session.getAttribute("AuthUpdate");
        if ((authUpdate != null) && (!authUpdate.trim().equalsIgnoreCase("")) &&
           (!authUpdate.trim().equalsIgnoreCase("null")) &&
            (authUpdate.trim().equalsIgnoreCase("true"))) {

            System.out.println("update found: ForwardToSearchMemberCommand");
            String memNo = "" + session.getAttribute("memberNumber");
            System.out.println("Member Number recieved from auth screen = " + memNo);
            //force member search
            new SearchMemberCommand().execute(request, response, context);

        } else {
            try {
                String nextJSP = "/Auth/MemberSearch.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

                dispatcher.forward(request, response);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }



        return null;
    }

    @Override
    public String getName() {
        return "ForwardToAuthSearchMember";
    }

    /*
    private void saveScreenToSession(HttpServletRequest request) {
    HttpSession session = request.getSession();
    Enumeration<String> e = request.getParameterNames();
    while (e.hasMoreElements()) {
    String param = e.nextElement();
    if (param.length() > 4) {
    String name = param.substring(param.length() - 4, param.length());
    if (!name.equalsIgnoreCase("list")) {
    session.setAttribute(param, request.getParameter(param));
    }
    } else {
    session.setAttribute(param, request.getParameter(param));
    }

    }
    }*/
}

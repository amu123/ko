/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author princes
 */
public class TabButton extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String buttonCommand1;
    private String buttonDisplay1;
    private String buttonCommand2;
    private String buttonDisplay2;
    private String buttonCommand3;
    private String buttonDisplay3;
    private String buttonCommand4;
    private String buttonDisplay4;
    private String buttonCommand5;
    private String buttonDisplay5;
    private String buttonCommand6;
    private String buttonDisplay6;
    private String buttonDisplay7;
    private String buttonCommand7;
    private int numberOfButtons;
    private String disabledDisplay;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        //HttpSession session = pageContext.getSession();
        //ArrayList<Diagnosis> list = (ArrayList<Diagnosis>) session.getAttribute("diagnosis_list");
        try {

            out.println("<table cellpadding=\"0\" cellspacing=\"0\">");
            for (int count = 0; count < numberOfButtons; count++) {
                switch (count) {
                    case 0:
                        if (disabledDisplay.equals(buttonDisplay1)) {
                            out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand1 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay1 + "</button></td>");
                        } else {
                            out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand1 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay1 + "</button></td>");
                        }
                        break;
                    case 1:
                        if (disabledDisplay.equals(buttonDisplay2)) {
                            out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand2 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay2 + "</button></td>");
                        } else {
                            out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand2 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay2 + "</button></td>");
                        }
                        break;
                    case 2:
                        if (disabledDisplay.equals(buttonDisplay3)) {
                            out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand3 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay3 + "</button></td>");
                        } else {
                            out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand3 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay3 + "</button></td>");
                        }
                        break;
                    case 3:
                        if (disabledDisplay.equals(buttonDisplay4)) {
                            out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand4 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay4 + "</button></td>");
                        } else {
                            out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand4 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay4 + "</button></td>");
                        }
                        break;
                    case 4:
                        if (disabledDisplay.equals(buttonDisplay5)) {
                            out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand5 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay5 + "</button></td>");
                        } else {
                            out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand5 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay5 + "</button></td>");
                        }
                        break;
                    case 5:
                        if (disabledDisplay.equals(buttonDisplay6)) {
                            out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand6 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay6 + "</button></td>");
                        } else {
                            out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand6 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay6 + "</button></td>");
                        }
                        break;
                    case 6:
                        if (disabledDisplay.equals(buttonDisplay7)) {
                            out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand7 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay7 + "</button></td>");
                        } else {
                            out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand7 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay7 + "</button></td>");
                        }
                        break;
                    default:
                        break;
                }

            }
            out.println("</table>");

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }

    public void setButtonCommand1(String button1) {
        this.buttonCommand1 = button1;
    }

    public void setButtonDisplay1(String buttonDisplay1) {
        this.buttonDisplay1 = buttonDisplay1;
    }

    public void setButtonCommand2(String button2) {
        this.buttonCommand2 = button2;
    }

    public void setButtonDisplay2(String buttonDisplay2) {
        this.buttonDisplay2 = buttonDisplay2;
    }

    public void setButtonCommand3(String button3) {
        this.buttonCommand3 = button3;
    }

    public void setButtonDisplay3(String buttonDisplay3) {
        this.buttonDisplay3 = buttonDisplay3;
    }

    public void setButtonCommand4(String button4) {
        this.buttonCommand4 = button4;
    }

    public void setButtonDisplay4(String buttonDisplay4) {
        this.buttonDisplay4 = buttonDisplay4;
    }

    public void setButtonCommand5(String button5) {
        this.buttonCommand5 = button5;
    }

    public void setButtonDisplay5(String buttonDisplay5) {
        this.buttonDisplay5 = buttonDisplay5;
    }

    public void setButtonCommand6(String buttonCommand6) {
        this.buttonCommand6 = buttonCommand6;
    }

    public void setButtonDisplay6(String buttonDisplay6) {
        this.buttonDisplay6 = buttonDisplay6;
    }

    public void setDisabledDisplay(String disabledDisplay) {
        this.disabledDisplay = disabledDisplay;
    }

    public void setNumberOfButtons(int numberOfButtons) {
        this.numberOfButtons = numberOfButtons;
    }

    public String getButtonCommand7() {
        return buttonCommand7;
    }

    public String getButtonDisplay7() {
        return buttonDisplay7;
    }

    public void setButtonCommand7(String buttonCommand7) {
        this.buttonCommand7 = buttonCommand7;
    }

    public void setButtonDisplay7(String buttonDisplay7) {
        this.buttonDisplay7 = buttonDisplay7;
    }

}

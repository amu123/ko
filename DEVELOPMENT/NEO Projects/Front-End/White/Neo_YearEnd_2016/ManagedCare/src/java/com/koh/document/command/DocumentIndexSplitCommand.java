/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;

import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class DocumentIndexSplitCommand extends NeoCommand {
    private Logger logger = Logger.getLogger(this.getClass());
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------In DocumentIndexSplitCommand---------------------");
            String selectedFile = request.getParameter("selectedFile");
            request.setAttribute("selectedFile", selectedFile);
            request.setAttribute("folderList",new PropertiesReader().getProperty("DocumentIndexWorkFolder"));
            request.setAttribute("listOrWork", "Work");
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Indexing/SplitDocument.jsp");
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "DocumentIndexSplitCommand";
    }
    @Override
    public String getName() {
        return "DocumentIndexSplitCommand";
    }

}
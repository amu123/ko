/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;


/**
 *
 * @author whauger
 */
public class LabelDropDownButton extends TagSupport {
    private String displayName;
    private String elementName;
    private String mandatory = "no";
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            out.println("<td><label class=\"label\">" + displayName + "</label></td>");
            out.println("<td><select name=\"" + elementName + "\" id=\"" + elementName + "\"></select></td>");
            if (mandatory.equalsIgnoreCase("yes")) {
                out.print("<td><label class=\"red\">*</label></td>");
            } else {
                out.print("<td></td>");
            }
            out.println("<td><button type=\"button\" name=\"opperation\" value=\"" + elementName + "\" ");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">Find</button></td>");

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelDropDownButton tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

}

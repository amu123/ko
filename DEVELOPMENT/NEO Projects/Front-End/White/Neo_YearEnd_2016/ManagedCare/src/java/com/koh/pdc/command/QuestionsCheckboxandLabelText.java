/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AnswerDetails;
import neo.manager.PdcAnswer;
import neo.manager.PdcQuestion;
import neo.manager.Questionnaire;

/**
 *
 * @author princes
 */
public class QuestionsCheckboxandLabelText extends TagSupport {

    private static final long serialVersionUID = -8081880396502034805L;

    private String elementName;
    private String displayName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return 
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {

        try {
            JspWriter out = pageContext.getOut();
            HttpSession session = pageContext.getSession();

            HashMap<Integer, String> dates = (HashMap<Integer, String>) session.getAttribute("dates");
            HashMap<Integer, String> answer_list = new HashMap<Integer, String>();

            HashMap<String, Integer> answered_list = new HashMap<String, Integer>();
            HashMap<String, Integer> radioAnsweredList = new HashMap<String, Integer>();
            String view = (String) session.getAttribute("tapView");
            String displayed = (String) session.getAttribute("displayed");
            String sessionHash = (String) session.getAttribute("sessionHash");
            String catched = (String) session.getAttribute("catched");
            Integer quesId = (Integer) session.getAttribute("questionnaireId");
            String reason = (String) session.getAttribute("reason");
            System.out.println("reason " + reason + " questionnaireId " + quesId);
            String diff = (String) session.getAttribute("diff");
            String answered = null;
            PdcAnswer answers = null;
            int count = 0;
            int elementCout = 0;
            int radio = 900;

            //System.out.println("tapView from session :" + view);
            //System.out.println("Displayed from session :" + displayed);
            if (dates != null) {
                if (view.equals("double") || view.equals("single")) {

                    if (view.equals("single")) {
                        answers = (PdcAnswer) session.getAttribute("answers");
                        // System.out.println("answers");
                    } else {
                        answers = (PdcAnswer) session.getAttribute("newAnswers");
                        // System.out.println("answers");
                    }

                    System.out.println("diff " + diff);
                    System.out.println("sessionHash" + sessionHash);

                    List<AnswerDetails> answerDetails = answers.getAnswerDetails();
                    session.setAttribute("answerId", answers.getAnswerId());
                    if (sessionHash == null) {
                        if (diff.equals("no")) {
                            System.out.println("**** Get answers from OBJECT");
                            for (AnswerDetails ansd : answerDetails) {
                                answer_list.put(ansd.getQuestionId(), ansd.getAnswer());

                            }
                        }
                    } else {
                        answer_list = (HashMap<Integer, String>) session.getAttribute("sessionAnswers");
                        System.out.println("**** Get answers from HASHMAP");
                    }
                }
            } else if (catched != null) {
                answer_list = (HashMap<Integer, String>) session.getAttribute("sessionAnswers");
                System.out.println("**** Get answers from HASHMAP catch");
            }

            //System.out.println("quesId " + quesId);
            //System.out.println("reason " + reason);
            ArrayList<Questionnaire> q = (ArrayList<Questionnaire>) NeoCommand.service.getNeoManagerBeanPort().getQuestionnaireByPDCtypeId(new Integer(reason), quesId);

            if (q.size() > 0) {
                System.out.println("The is a questionnaire for this");
                Questionnaire questionnaire = q.get(0);
                List<PdcQuestion> question = questionnaire.getQuestions();
                for (PdcQuestion gues : question) {
                    if (dates != null || catched != null) {
                        if (view.equals("double") || view.equals("single")) {
                            answered = answer_list.get(gues.getQuestionId());
                        }
                    }
                    if (gues.getQuestionOrder() == 0) {
                        if (answered != null) {
                            out.println("<tr><td align=\"left\" width=\"160px\"><label><b>" + gues.getQuestion() + ":</b></label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "_" + elementCout + "\" value=\"" + answered + "\"size=\"30\" " + javaScript + "/></td></tr>");
                        } else {
                            out.println("<tr><td align=\"left\" width=\"160px\"><label><b>" + gues.getQuestion() + ":</b></label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "_" + elementCout + "\" size=\"30\" " + javaScript + "/></td></tr>");
                        }
                    } else if (answered != null) {
                        out.println("<tr><td align=\"left\" width=\"360px\" colspan=\"2\"><input type=\"checkbox\" name=\"" + elementName + "_" + elementCout + "\" checked=\"checked\" onClick=\"hideDIV('" + count + "')\"; value=\"1\"><label><b>" + gues.getQuestion() + "</b></label></input></td></tr>");
                        out.println("<tbody id=\"" + count + "\">");
                    } else {
                        out.println("<tr><td align=\"left\" width=\"360px\" colspan=\"2\"><input type=\"checkbox\" name=\"" + elementName + "_" + elementCout + "\" onClick=\"hideDIV('" + count + "')\"; value=\"1\"><label><b>" + gues.getQuestion() + "</b></label></input></td></tr>");
                        out.println("<tbody id=\"" + count + "\" style=\"display:none\">");
                    }

                    answered_list.put(elementName + "_" + elementCout, gues.getQuestionId());
                    List<PdcQuestion> subQuestion = gues.getSubQuestions();
                    /*if (!displayed.equals("old")){
                out.println("<tbody id=\"" + count + "\" style=\"display:none\">");
                }*/

                    for (PdcQuestion subQ : subQuestion) {
                        elementCout++;
                        answered_list.put(elementName + "_" + elementCout, subQ.getQuestionId());

                        String subAnswered = answer_list.get(subQ.getQuestionId());
                        if (subAnswered != null) {
                            System.out.println("subQ " + subQ.getQuestionRating());
                            if (subQ.getQuestionRating() != null) {
                                if (subQ.getQuestionRating().equals("R")) {

                                    out.println("<tr><td><input type=\"radio\" name=\"" + elementName + "_" + radio + "\" value=\"" + subQ.getQuestionId() + "\" checked " + javaScript + "><label class=\"label\">" + subQ.getQuestion() + "</label></input></td></tr>");
                                } else {
                                    out.println("<tr><td align=\"left\" width=\"160px\"><label>" + subQ.getQuestion() + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "_" + elementCout + "\" value=\"" + subAnswered + "\"size=\"30\"/></td></tr>");
                                }
                            } else {
                                out.println("<tr><td align=\"left\" width=\"160px\"><label>" + subQ.getQuestion() + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "_" + elementCout + "\" value=\"" + subAnswered + "\"size=\"30\"/></td></tr>");
                            }
                        } else if (subQ.getQuestionRating() != null) {
                            if (subQ.getQuestionRating().equals("R")) {
                                radioAnsweredList.put(elementName + "_" + radio, subQ.getQuestionId());
                                System.out.println("Answered " + elementName + "_" + radio + " - " + subQ.getQuestionId());
                                out.println("<tr><td><input type=\"radio\" name=\"" + elementName + "_" + radio + "\" value=\"" + subQ.getQuestionId() + "\" " + javaScript + "><label class=\"label\">" + subQ.getQuestion() + "</label></input></td></tr>");
                            } else {
                                out.println("<tr><td align=\"left\" width=\"160px\"><label>" + subQ.getQuestion() + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "_" + elementCout + "\" size=\"30\"/></td></tr>");
                            }
                        } else {
                            out.println("<tr><td align=\"left\" width=\"160px\"><label>" + subQ.getQuestion() + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "_" + elementCout + "\" size=\"30\"/></td></tr>");
                        }
                    }
                    //if (!displayed.equals("old")){
                    out.println("</tbody>");
                    //}
                    radio++;
                    elementCout++;
                    count++;
                }
                session.setAttribute("answered_list", answered_list);
                session.setAttribute("answered_list_size", elementCout);
                session.setAttribute("radio_answered_list", radioAnsweredList);
                session.setAttribute("radion_size", radio);
                System.out.println("radion_size " + radio);
            }

        } catch (IOException ex) {
            Logger.getLogger(QuestionsCheckboxandLabelText.class.getName()).log(Level.SEVERE, null, ex);
        }
        return super.doEndTag();
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.union.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Union;
import neo.manager.UnionAddressDetails;
import neo.manager.UnionRepresentative;

/**
 *
 * @author shimanem
 */
public class UnionCapturerCommand extends NeoCommand {

    private static final Logger logger = Logger.getLogger(UnionCapturerCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        logger.info(">> *** Creating New Union [Frontend] *** <<");
        NeoManagerBean port = service.getNeoManagerBeanPort();

        createUnion(port, request, response, context);

        return null;
    }

    public void createUnion(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        try {
            NeoUser neoUser = getNeoUser(request);
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            String message = "";

            String unionName = request.getParameter("unionName");
            String unionNumber = request.getParameter("unionNumber");
            String unionAltNumber = request.getParameter("altUnionNumber");
            String repName = request.getParameter("repName");
            String repSurname = request.getParameter("repSurname");
            String unionRepCapacity = request.getParameter("unionRepCapacity");
            String unionRegion = request.getParameter("unionRegion");
            String inceptionDate = request.getParameter("unionInceptionDate");
            String terminationDate = request.getParameter("unionTerminationDate");
            // Contact Components
            String contactNumber = request.getParameter("contactNumber");
            String faxNumber = request.getParameter("faxNumber");
            String email = request.getParameter("emailAddress");
            String alternateEmail = request.getParameter("altEmailAddress");
            // Postal Address Components
            String postLine1 = request.getParameter("postLine1");
            String postLine2 = request.getParameter("postLine2");
            String postLine3 = request.getParameter("postLine3");
            String postLocation = request.getParameter("postLocation");
            String postCode = request.getParameter("postCode");
            // Physical Address Components
            String physicalLine1 = request.getParameter("physicalLine1");
            String physicalLine2 = request.getParameter("physicalLine2");
            String physicalLine3 = request.getParameter("physicalLine3");
            String physicalLocation = request.getParameter("physicalLocation");
            String physicalCode = request.getParameter("physicalCode");

            Union union = new Union();
            UnionRepresentative unionRepresentative = new UnionRepresentative();
            UnionAddressDetails addressDetails = new UnionAddressDetails();

            union.setUnionNumber(unionNumber);
            union.setUnionAlternativeNumber(unionAltNumber);
            union.setUnionName(unionName);
            union.setInceptionDate(DateTimeUtils.convertDateToXMLGregorianCalendar(format.parse(inceptionDate)));
            union.setTerminationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(format.parse(terminationDate)));
            //unionRepresentative.setUnionID(unionNumber);
            unionRepresentative.setRepresentativeCapacityID(Integer.parseInt(unionRepCapacity));
            unionRepresentative.setName(repName);
            unionRepresentative.setSurname(repSurname);
            unionRepresentative.setContactNumber(contactNumber);
            unionRepresentative.setFaxNumber(faxNumber);
            unionRepresentative.setEmail(email);
            unionRepresentative.setAltEmail(alternateEmail);
            unionRepresentative.setRegionID(Integer.parseInt(unionRegion));

            addressDetails.setPostAddressLine1(postLine1);
            addressDetails.setPostAddressLine2(postLine2);
            addressDetails.setPostAddressLine3(postLine3);
            addressDetails.setPostAddressLocation(postLocation);
            addressDetails.setPostAddressCode(postCode);
            addressDetails.setPhysicalAddressLine1(physicalLine1);
            addressDetails.setPhysicalAddressLine2(physicalLine2);
            addressDetails.setPhysicalAddressLine3(physicalLine3);
            addressDetails.setPhysicalAddressLocation(physicalLocation);
            addressDetails.setPhysicalAddressCode(physicalCode);

            boolean blnValid = port.createNewUnion(union, unionRepresentative, addressDetails, neoUser);
            logger.log(Level.INFO, "CREATION STATUS : {0}", blnValid);
            if (blnValid == true) {
                message = "Union Created Successfully";
                responseMsg(message, "/ManagedCare/Union/searchUnion.jsp", response);
            } else {
                message = "Unable to Create a New Union, Please contact Administrator";
                responseMsg(message, "/ManagedCare/Union/newUnion.jsp", response);
            }
        } catch (ParseException ex) {
            Logger.getLogger(UnionCapturerCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void responseMsg(String message, String url, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();

            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            out.println("<td><label class=\"header\"><h2>" + message + "</h2></label></td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "3; URL=" + url); // /ManagedCare/Union/WorkbenchDetails.jsp

        } catch (IOException ex) {
            Logger.getLogger(UnionCapturerCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser) request.getSession().getAttribute("persist_user");
    }

    @Override
    public String getName() {
        return "UnionCapturerCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.tags;

import com.koh.command.NeoCommand;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.ClaimIcd10;
import neo.manager.ClaimLine;
import neo.manager.ClaimReversals;
import neo.manager.ClaimRuleMessage;
import neo.manager.CoverProductDetails;
import neo.manager.Drug;
import neo.manager.Modifier;
import neo.manager.Ndc;
import neo.manager.NeoManagerBean;
import neo.manager.ProviderDetails;
import neo.manager.TariffCode;

/**
 *
 * @author Johan-NB
 */
public class PracticeClaimLineClaimLineTable extends TagSupport {

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        String serviceDateFrom = "";
        String paymentDaye = "";
        String paymentDate = "";

        System.out.println("entered PracticeClaimLineClaimLineTable");

        int depenNo = -1;
        boolean isClearProviderClaim = (Boolean) session.getAttribute("persist_isClearingClaims");

        //set cover number
        String coverNumber = "" + session.getAttribute("pracClaimResultMemNum");
        System.out.println("coverNumber selectecd = " + coverNumber);
        //get service provider number
        String serviceProviderNumber = "" + session.getAttribute("pracClaimProvNum");
        System.out.println("practice selectecd = " + serviceProviderNumber);
        ClaimLine claimLine = (ClaimLine) session.getAttribute("pracCLCLDetails");

        double paidAmount = 0.0d;
        if (session.getAttribute("pracDetailedClaimPaidAmount") != null) {
            paidAmount = Double.parseDouble("" + session.getAttribute("pracDetailedClaimPaidAmount"));
        }

        try {
            out.println("<label class=\"header\">Detailed ClaimLine Results</label></br></br>");
            out.println("<table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");

            //first table header
            out.print("<tr>");
            //out.print("<th align=\"left\" >ClaimLine Number</th>");
            out.print("<th align=\"left\" >Dependant Code</th>");
            out.print("<th align=\"left\" >Treatment Date</th>");
            out.print("<th align=\"left\" >ICD10 Code</th>");
            out.print("<th align=\"left\" >Tariff Code</th>");
            out.print("<th align=\"left\" >Tariff Description</th>");
            out.print("<th align=\"left\" >Nappi Code</th>");
            out.print("<th align=\"left\" >Nappi Description</th>");
            out.print("<th align=\"left\" >Claimed Amount</th>");
            out.print("<th align=\"left\" >Tariff Amount</th>");
            out.print("<th align=\"left\" >Paid Amount</th>");
            if(isClearProviderClaim){
                out.print("<th align=\"left\" >Overcharge Amount</th>");
            }
            out.print("</tr>");

            String icds = "";
            List<String> icd10 = new ArrayList<String>();

            List<ClaimIcd10> icdList = claimLine.getIcd10();
            if (icdList != null && !icdList.isEmpty()) {
                for (ClaimIcd10 icd : icdList) {
                    icds = icd.getIcd10Code();
                }
                icd10.add(icds);
            }

            System.out.println("PracticeClaimLineClaimLineTable serviceProviderNumber = " + serviceProviderNumber);

            ProviderDetails proDet = port.getProviderDetailsForEntityByNumber(serviceProviderNumber);
            CoverProductDetails cpd = port.getCoverProductDetailsByCoverNumber(coverNumber);
            TariffCode tariff = port.getTariffCode(cpd.getProductId(), cpd.getOptionId(), claimLine.getServiceDateFrom(), proDet.getDisciplineType().getId(), proDet.getPracticeNumber(), proDet.getPracticeNumber(), icd10, null, claimLine.getTarrifCodeNo());

            out.print("<tr valign=\"top\">");

            //depCode
            out.print("<td><label class=\"label\">" + claimLine.getInsuredPersonDependentCode() + "</label></td>");

            //treatment date
            Date serviceDate = claimLine.getServiceDateFrom().toGregorianCalendar().getTime();
            serviceDateFrom = formatter.format(serviceDate);
            out.print("<td><label class=\"label\">" + serviceDateFrom + "</label></td>");

            //icds
            if (icds != null) {
                out.print("<td><label class=\"label\">" + icds + "</label></td>");
            } else {
                out.print("<td></td>");
            }

            //tariffs
            if (tariff != null) {
                out.print("<td><label class=\"label\">" + tariff.getCode() + "</label></td>");
                out.print("<td><label class=\"label\">" + tariff.getDescription() + "</label></td>");
            } else {
                out.print("<td></td>");
                out.print("<td></td>");
            }

            //nappi's
            Ndc ndc = null;
            System.out.println("The NDC is " + claimLine.getNdc());
            System.out.println("The NDC size is " + claimLine.getNdc().size());

            if (claimLine.getNdc() != null && claimLine.getNdc().size() != 0) {
                ArrayList<Ndc> ndcs = (ArrayList<Ndc>) claimLine.getNdc();
                for (Ndc ndc1 : ndcs) {
                    if (ndc1.getPrimaryNdcIndicator() == 1) {
                        ndc = ndc1;
                        break;
                    }
                    ndc = ndc1;
                    System.out.println("The NDC primary indicator " + ndc1.getPrimaryNdcIndicator());
                }
            }
            if (ndc != null) {
                Drug d = port.getDrugForCode(ndc.getNdcCode(), proDet.getDisciplineType().getId());
                if (d != null) {
                    out.print("<td><label class=\"label\">" + d.getCode() + "</label></td>");
                    out.print("<td><label class=\"label\">" + d.getName() + "</label></td>");
                }else{
                    out.print("<td><label class=\"label\">" + ndc.getNdcCode() + "</label></td>");
                    out.print("<td><label class=\"label\">Nappi Code not found on Drug List</label></td>");                    
                }
            } else {
                out.print("<td></td>");
                out.print("<td></td>");
            }

            //Amounts
            double claimedAmount = 0.0d;
            double tariffAmount = 0.0d;
            double overpaidAmount = 0.0d;
            String overPaid = "0.0";
            
            if(isClearProviderClaim){
                //do clearing calculation
                //claimed amount (clearing_original_claimed_amount)
                claimedAmount = claimLine.getClaimLineClearing().getClaimedAmount();
                //tariff amount (cl tariff amount - clc admin fee)
                double adminAmount = claimLine.getClaimLineClearing().getAdminAmount();
                if (adminAmount < 0) {
                    tariffAmount = claimLine.getTarrifAmount() - Math.abs(adminAmount);
                } else {
                    tariffAmount = claimLine.getTarrifAmount() - adminAmount;
                }
                //overpaid amount (claimed amount - tariff amount)
                double overPaidCalc = 0.0d;
                overPaidCalc = claimedAmount - tariffAmount;
                if(overPaidCalc > 0.0d){
                    overpaidAmount = overPaidCalc;
                    overPaid = new DecimalFormat("#####0.00").format(overpaidAmount);
                }
                
            }else{
                claimedAmount = claimLine.getClaimedAmount();
                tariffAmount = claimLine.getTarrifAmount();
                
            }
            //claimed amount
            out.print("<td><label class=\"label\">" + new DecimalFormat("#####0.00").format(claimedAmount) + "</label></td>");
            //tariff amount
            out.print("<td><label class=\"label\">" + new DecimalFormat("#####0.00").format(tariffAmount) + "</label></td>");
            //paid amount
            out.print("<td><label class=\"label\">" + new DecimalFormat("#####0.00").format(paidAmount) + "</label></td>");
            
            if(isClearProviderClaim){
                //overpaid amount
                out.print("<td><label class=\"label\">" + overPaid + "</label></td>");                
            }


            out.print("</tr></table><br/><br/>");

            //rejection reason table
            Integer status = claimLine.getClaimLineStatusId();
            String payStatus = port.getValueForId(83, status.toString());
            if (payStatus.equalsIgnoreCase("rejected")) {
                out.print("<table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");

                List<ClaimRuleMessage> crmList = claimLine.getRuleMessages();
                if (crmList != null && crmList.isEmpty() == false) {
                    out.println("<tr><th align=\"left\">Rejection Code</th><th align=\"left\">Rejection Reason</th></tr>");

                    for (ClaimRuleMessage crm : crmList) {
                        out.println("<tr valign=\"top\">"
                                + "<td>"
                                + "<label class=\"label\">" + crm.getMessageCode() + "</label>"
                                + "</td>"
                                + "<td>"
                                + "<label class=\"label\">" + crm.getSeverity() + " : " + crm.getShortMsgDescription() + "</label>"
                                + "</td>"
                                + "</tr>");
                    }

                } else {
                    out.println("<tr><td><label class=\"label\">No Rejection Reason Found</label></td></tr>");
                }

                out.println("</table><br/>");
            } else if (payStatus.equalsIgnoreCase("reversed")) {
                ClaimReversals cr = claimLine.getClaimReversals();
                out.print("<table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                
                if (cr != null) {
                    out.println("<tr><th align=\"left\">Reversal Code</th><th align=\"left\">Reversal Reason</th></tr>");
                    out.println("<tr valign=\"top\">"
                            + "<td>"
                            + "<label class=\"label\">" + cr.getReversalCode() + "</label>"
                            + "</td>"
                            + "<td>"
                            + "<label class=\"label\">" + cr.getReversalDescription() + "</label>"
                            + "</td>"
                            + "</tr>");
                } else {
                    out.println("<tr><td><label class=\"label\">No Reversal Reason Found</label></td></tr>");
                }
                out.println("</table><br/>");

            }

            //modifiers
            List<Modifier> modList = claimLine.getModifiers();
            if (modList != null && modList.isEmpty() == false) {
                out.print("<table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.println("<tr><th align=\"left\">Modifier</th><th align=\"left\">Modifier Amount</th></tr>");

                for (Modifier mod : modList) {
                    out.println("<tr valign=\"top\">"
                            + "<td>"
                            + "<label class=\"label\">" + mod.getModifier() + "</label>"
                            + "</td>"
                            + "<td>"
                            + "<label class=\"label\">" + new DecimalFormat("#####0.00").format(mod.getModifierTariffAmount()) + "</label>"
                            + "</td>"
                            + "</tr>");
                }
                out.println("</table>");

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.doEndTag();
    }
}

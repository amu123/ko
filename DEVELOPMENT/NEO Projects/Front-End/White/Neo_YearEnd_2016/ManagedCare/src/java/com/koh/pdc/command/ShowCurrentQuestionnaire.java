/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.Command;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author princes
 */
public class ShowCurrentQuestionnaire extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        System.out.println("entered ShowCurrentQuestionnaire");

        HashMap<String, Integer> answer_list = (HashMap<String, Integer>) session.getAttribute("answered_list");
        Integer answer_size = (Integer) session.getAttribute("answered_list_size");

        Integer radion_size = (Integer) session.getAttribute("radion_size");
        int rSize = radion_size;

        HashMap<Integer, String> list = new HashMap<Integer, String>();
        int size = answer_size;

        for (int i = 0; i < size; i++) {

            String ans = request.getParameter("question_" + i);

            if (ans != null) {
                if (!ans.equals("")) {
                    System.out.println("Question " + i + ": Answer: " + request.getParameter("question_" + i)
                            + " | Question Id " + answer_list.get("question_" + i));
                    list.put(answer_list.get("question_" + i), ans);

                }
            }

        }

        for (int i = 900; i <= rSize; i++) {

            String ans = request.getParameter("question_" + i);
            System.out.println("-------------Radio Value: " + ans);
            if (ans != null) {
                if (!ans.equals("")) {
                    /*System.out.println("Question " + i + ": Answer: " + request.getParameter("question_" + i) +
                    " | Question Id " + radion_list.get("question_" + i));*/
                    list.put(new Integer(ans), ans);

                }
            }

        }

        session.setAttribute("sessionAnswers", list);
        session.setAttribute("sessionHash", "yes");
        session.setAttribute("displayed", "old");

        try {
            String nextJSP = "/PDC/OldQuestionnaire.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ShowCurrentQuestionnaire";
    }
}

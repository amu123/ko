/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
//import javax.servlet.jsp.tagext.JspFragment;
//import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class LabelTextSearchTextCancel extends TagSupport {

    private String displayName;
    private String elementName;
    private String searchOpperation;
    private String onScreen;
    private String valueFromRequest = "";
    private String valueFromSession = "No";
    private String javaScript = "";

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setOnScreen(String onScreen) {
        this.onScreen = onScreen;
    }

    public void setValueFromSession(String valueFromSession) {
        this.valueFromSession = valueFromSession;
    }

    public void setSearchOpperation(String searchOpperation) {
        this.searchOpperation = searchOpperation;
    }

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        try {
            if (valueFromSession.equalsIgnoreCase("Yes")) {
                String sessionVal = "";
                if(session.getAttribute(elementName) != null)
                    sessionVal = "" + session.getAttribute(elementName);
                String sessionVal2 = "";
                if(session.getAttribute(elementName+"_text") != null)
                    sessionVal2 = "" + session.getAttribute(elementName + "_text");
                out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"250px\"><input type=\"text\" id=\"" + elementName + "_text\" name=\"" + elementName + "_text\" value=\"" + sessionVal2 + "\" size=\"30\"" + javaScript + "/>");
                    out.println("<td colspan=\"3\"align=\"left\"><table><tr><td><img src=\"/ManagedCare/resources/Search.gif\" width=\"28\" height=\"28\" alt=\"Search\" border=\"0\"/ onClick=\"submitWithAction('" + searchOpperation + "', '"+elementName+"', '"+onScreen+"')\";></td>");
                    out.println("<td><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"" + sessionVal + "\" size=\"30\"/></td>");
            } else {

                if (!valueFromRequest.equalsIgnoreCase("")) {
                    out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"250px\"><input type=\"text\" id=\"" + elementName + "_text\" name=\"" + elementName + "_text\" value=\"" + req.getAttribute(valueFromRequest) + "\" size=\"30\"" + javaScript + "/>");
                    out.println("<td colspan=\"3\"align=\"left\"><table><tr><td><img src=\"/ManagedCare/resources/Search.gif\" width=\"28\" height=\"28\" alt=\"Search\" border=\"0\"/ onClick=\"submitWithAction('" + searchOpperation + "' , '"+elementName+"', '"+onScreen+"')\";></td>");
                    out.println("<td><input disabled type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"" + req.getAttribute(valueFromRequest) + "\" size=\"30\"/></td>");
                } else {
                    out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"250px\"><input type=\"text\" id=\"" + elementName + "_text\" name=\"" + elementName + "_text\" value=\"\" size=\"30\"" + javaScript + "/>");
                    out.println("<td colspan=\"3\"align=\"left\"><table><tr><td><img src=\"/ManagedCare/resources/Search.gif\" width=\"28\" height=\"28\" alt=\"Search\" border=\"0\"/ onClick=\"submitWithAction('" + searchOpperation + "', '"+elementName+"', '"+onScreen+"')\";></td>");
                    out.println("<td><input disabled type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"\" size=\"30\"/></td>");

                }
            }
            out.println("<td><img src=\"/ManagedCare/resources/Close.gif\" width=\"28\" height=\"28\" alt=\"Clear\" border=\"0\" onClick=\"clearElement('" + elementName + "')\"/></td></tr></table>");
            out.println("</td>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelTextSearchTextCancel tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setValueFromRequest(String valueFromRequest) {
        this.valueFromRequest = valueFromRequest;
    }
}

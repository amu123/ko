/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.command;

import com.koh.auth.command.SearchMemberCommand;
import com.koh.auth.dto.MemberSearchResult;
import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import static com.koh.utils.FormatUtils.dateFormat;
import com.koh.utils.NeoCoverDetailsDependantFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Exception;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class ClaimHeaderCommand  extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String command = request.getParameter("command");
        System.out.println("Operation: ClaimHeaderCommand - Command: " + command);
        if ("SaveClaimHeader".equalsIgnoreCase(command)) {
            System.out.println("Saving claim Header");
            Claim claim = saveClaim(request, port);
            String updateFields = null;
            Map<String, String> fieldsMap = new HashMap<String, String>();
            fieldsMap.put("ClaimHeader_batchId", new Integer(claim.getBatchId()).toString());
            fieldsMap.put("ClaimHeader_claimId", new Integer(claim.getClaimId()).toString());
            updateFields = TabUtils.convertMapToJSON(fieldsMap);
            String jsonResult = TabUtils.buildJsonResult("OK", null, updateFields, null);
            try {
                PrintWriter out = response.getWriter();
                out.println(jsonResult);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
        } else if ("SearchProvider".equalsIgnoreCase(command)) {
            try {
                ProviderSearchCriteria psc = new ProviderSearchCriteria();
                psc.setInitials(request.getParameter("ClaimHeader_SearchProviderInitials"));
                psc.setName(request.getParameter("ClaimHeader_SearchProviderName"));
                psc.setSurname(request.getParameter("ClaimHeader_SearchProviderSurname"));
                Collection<ProviderDetails> result = port.findAllProvidersByCriteria(psc);
                request.setAttribute("SearchProviderResults", result);
                System.out.println(result.size());
                for(ProviderDetails prov : result) {
                    System.out.println(prov.getProviderNumber() + " - " + prov.getProviderCategory().getValue());
                }
                RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/CaptureClaims_SearchProviderResults.jsp");
                dispatcher.forward(request, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("SearchRefProvider".equalsIgnoreCase(command)) {
            try {
                ProviderSearchCriteria psc = new ProviderSearchCriteria();
                psc.setInitials(request.getParameter("ClaimHeader_SearchRefProviderInitials"));
                psc.setName(request.getParameter("ClaimHeader_SearchRefProviderName"));
                psc.setSurname(request.getParameter("ClaimHeader_SearchRefProviderSurname"));
                Collection<ProviderDetails> result = port.findAllProvidersByCriteria(psc);
                System.out.println(result.size());
                request.setAttribute("SearchProviderResults", result);
                RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/CaptureClaims_SearchRefProviderResults.jsp");
                dispatcher.forward(request, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            
        } else if ("SearchPractice".equalsIgnoreCase(command)) {
            try {
                ProviderSearchCriteria psc = new ProviderSearchCriteria();
                psc.setPracticeName(request.getParameter("ClaimHeader_SearchPracticeName"));
                psc.setPracticeNumber(request.getParameter("ClaimHeader_SearchPracticeNumber"));
                psc.setDiciplineType(request.getParameter("ClaimHeader_SearchPracticeType"));
                Collection<ProviderDetails> result = port.findAllPracticesByCriteria(psc);
                request.setAttribute("SearchPracticeResults", result);
                
                RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/CaptureClaims_SearchPracticeResults.jsp");
                dispatcher.forward(request, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            
        } else if ("fetchBatchAndClaims".equalsIgnoreCase(command)) {
            ClaimsBatch batchAndClaimWithoutClaimline = port.fetchBatchAndClaimWithoutClaimline(new Integer(TabUtils.getIntParam(request, "batchId")));
            
            if (batchAndClaimWithoutClaimline != null && batchAndClaimWithoutClaimline.getClaims() != null
                    && !batchAndClaimWithoutClaimline.getClaims().isEmpty()) {
                for (Claim c : batchAndClaimWithoutClaimline.getClaims()) {
                    //get cover details
                    String coverNumber = "";
                    
                    int categoryID = 0;

                    if (c != null) {
                        coverNumber = c.getCoverNumber();
                        String claimId = "" + c.getClaimId();
                        CoverDetails cd = port.getPrincipalMemberDetailsToday(coverNumber);
                        List<ClaimLine> cls = port.fetchClaimLineForClaim(c.getClaimId());
                        
                        if (!cls.isEmpty()) {
                            String lineCap = "lineCaptured" + claimId;

                            request.setAttribute(lineCap, "1");
                        }

                        /*/get provider details
                        String providerNumber = "";
                        providerNumber = c.getPracticeNo();
                        ProviderDetails providerDetails = port.getProviderDetailsForEntityByNumber(providerNumber);*/

                        categoryID = c.getClaimCategoryTypeId();

                        List<neo.manager.LookupValue> providerCategoryLookUp = NeoCommand.service.getNeoManagerBeanPort().getCodeTable(59);
                        String category = categoryID + "";
                        for (int i = 0; i < providerCategoryLookUp.size(); i++) {
                            if (providerCategoryLookUp.get(i).getId().matches(category)) {
                                category = providerCategoryLookUp.get(i).getValue();
                            }
                        }
                        request.setAttribute("categoryName" + c.getClaimId(), category);
                        request.setAttribute("principalEID" + c.getClaimId(), cd.getEntityId());
                        //request.setAttribute("providerDiscID" + c.getClaimId(), providerDetails.getDisciplineType().getId());

                    }
                }
            }

            request.setAttribute("ClaimBatch", batchAndClaimWithoutClaimline);
            request.setAttribute("batchId", request.getParameter("batchId"));
            String update = request.getParameter("update");
            System.out.println("update = " + update);
            try {
                if(update != null && update.equalsIgnoreCase("true")){
                    RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/EditClaims_ClaimSummary.jsp");
                    dispatcher.forward(request, response);
                } else {
                    RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/CaptureClaims_ClaimSummary.jsp");
                    dispatcher.forward(request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("findPractice".equalsIgnoreCase(command)) {
            
        } else if ("findClaimCoverNumber".equalsIgnoreCase(command)) {
            String coverNumber = request.getParameter("coverNumber");
            Collection<CoverDetails> cds = port.getResignedMemberByCoverNumber(coverNumber);
            String result = "";
            for (CoverDetails cd : cds) {
                if (cd.getDependentTypeId()==17) {
                    result = cd.getCoverNumber() + "|" + cd.getSurname() + "," + cd.getName();
                }
            }
            try {
                PrintWriter out = response.getWriter();
                out.print(result);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
            
        } else if ("findClaimPractice".equalsIgnoreCase(command)) {
            String val = request.getParameter("practiceNumber");
            ProviderDetails practiceDetails = port.getPracticeDetailsForEntityByNumber(val);
            String result = "";
            if (practiceDetails != null) {
//                result = practiceDetails.getPracticeName();
                if (practiceDetails.getCashPractice().getId().equals("1") || practiceDetails.getCashPractice().getId().equals("2") || practiceDetails.getProviderCategory().getValue().equalsIgnoreCase("Pharmacy")){
                    result = "Cash|" + practiceDetails.getPracticeName();
                } else {
                    result = "Normal|" + practiceDetails.getPracticeName();
                }
            }
            try {
                PrintWriter out = response.getWriter();
                out.print(result);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
        } else if ("findClaimProvider".equalsIgnoreCase(command)) {
            String val = request.getParameter("providerNumber");
            ProviderDetails providerDetails = port.getProviderDetailsForEntityByNumber(val);;
            String result = "";
            if (providerDetails != null) {
                result = providerDetails.getSurname() + ", " + providerDetails.getInitials() + "||" + providerDetails.getProviderCategory().getValue();
            }
            try {
                PrintWriter out = response.getWriter();
                out.print(result);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
            
            
        } else if ("processClaimBatch".equalsIgnoreCase(command)) {
            String status = "";
            String additionalData = ""; 
            try {
                Security security = TabUtils.getSecurity(request);
                port.processRulesInBatch(TabUtils.getIntParam(request, "batchId"), security);
                status = "OK";
                additionalData = "\"message\":\"Claim Batch Processed.\"";
            } catch (Exception e) {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error processing the claim batch.\"";
                
            }
            String jsonResult = TabUtils.buildJsonResult(status, null, null, additionalData);
            try {
                PrintWriter out = response.getWriter();
                out.print(jsonResult);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else if("SearchMember".equalsIgnoreCase(command)){
            String memberNumber = request.getParameter("ClaimHeader_SearchMemberNumber");
            String firstName = request.getParameter("ClaimHeader_SearchMemberName");
            String surname = request.getParameter("ClaimHeader_SearchMemberSurname");
            String product = request.getParameter("ClaimHeader_SearchMemberScheme");
            String option = request.getParameter("ClaimHeader_SearchMemberSchemeOptions");
            String idNumber = request.getParameter("ClaimHeader_SearchMemberIDNum");
            String dateOfBirth = request.getParameter("ClaimHeader_SearchDOB");
            String test = request.getParameter("ClaimHeader_SearchMemberDependant");
            System.out.println("memnum = " + memberNumber + "\nname = " + firstName + "\nsurname = " + surname + "\nProdcut = " + product + "\noption = " + option + "\nidNum = " + idNumber + "\nDoB = " + dateOfBirth);
            CoverSearchCriteria search = new CoverSearchCriteria();
            
            boolean flag = false;
            if (firstName != null && !firstName.equals("")) {
                flag = true;
                search.setCoverName(firstName);
            }
            if (surname != null && !surname.equals("")) {
                flag = true;
                search.setCoverSurname(surname);
            }
            if (product != null && !product.equals("") && !product.equals("99")) {
                search.setProductId(Integer.parseInt(product));
            }
            if (option != null && !option.equals("") && !option.equals("99")) {
                search.setOptionId(Integer.parseInt(option));
            }
            if (idNumber != null && !idNumber.equals("")) {
                flag = true;
                search.setCoverIDNumber(idNumber);
            }
            if (dateOfBirth != null && !dateOfBirth.equals("")) {
                flag = true;
                XMLGregorianCalendar dob = null;
                try {
                    dob = DateTimeUtils.convertDateToXMLGregorianCalendar(dateFormat.parse(dateOfBirth));
                } catch (ParseException ex) {
                    Logger.getLogger(SearchMemberCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                search.setDateOfBirth(dob);
            }
            
            if (memberNumber != null && !memberNumber.equals("")) {
                if (!flag) {
                    if (memberNumber.length() < 4) {
                        //errorMsg = ": Cover Number Must be at least 4 characters";
                        flag = true;
                        search.setCoverNumber(memberNumber);
                    } else {
                        flag = true;
                        search.setCoverNumber(memberNumber);
                    }

                } else {
                    flag = true;
                    search.setCoverNumber(memberNumber);
                }
            }
            ArrayList<MemberSearchResult> members = new ArrayList<MemberSearchResult>();
            List<EAuthCoverDetails> pdList = new ArrayList<EAuthCoverDetails>();
            //boolean viewStaff = (Boolean) session.getAttribute("persist_user_viewStaff");
            try {
                if (flag) {
                    pdList = service.getNeoManagerBeanPort().eAuthCoverSearch(search);

                    //filter dependants of pdList to only display one dependant record for each cover dependant
                    NeoCoverDetailsDependantFilter filter = new NeoCoverDetailsDependantFilter();
                    pdList = filter.EAuthCoverDetailsDependantFilter(pdList);

                    for (EAuthCoverDetails cd : pdList) {
//                        if (!viewStaff) {
//                            if (cd.getMemberCategory() == 2) {
//                                continue;
//                            }
//                        }

                        MemberSearchResult member = new MemberSearchResult();
                        member.setMemberNumber(cd.getCoverNumber());
                        member.setDependant("0" + cd.getDependentNumber());
                        member.setName(cd.getCoverName());
                        member.setSurname(cd.getCoverSurname());
                        member.setScheme(cd.getProduct());
                        member.setPlan(cd.getOption());
                        member.setStatus(cd.getCoverStatus());
                        member.setBenefitFromDate(cd.getCoverStartDate().toGregorianCalendar().getTime());
                        if (cd.getCoverEndDate() != null) {
                            member.setBenefitToDate(cd.getCoverEndDate().toGregorianCalendar().getTime());
                        }
                        if (cd.getJoinDate() != null) {
                            member.setJoinDate(cd.getJoinDate().toGregorianCalendar().getTime());
                        }
                        members.add(member);
                    }
                    request.setAttribute("SearchMembersResults", members);
//                    session.setAttribute("searchMem_error", null);
                } else {
//                    session.setAttribute("searchMem_error", "Please Refine Search " + errorMsg);
                }
//                if (!viewStaff) {
//                    if (pdList != null && members != null) {
//                        if (pdList.size() > 0) {
//                            if (members.size() == 0) {
//                                //session.setAttribute("searchMem_error", "User access denied. Please contact your supervisor " + errorMsg);
//                                request.setAttribute("error", "User access denied. Please contact your supervisor");
//                            }
//                        }
//                    }
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                String nextJSP = "/NClaims/CaptureClaims_SearchMemberResults.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

                dispatcher.include(request, response);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            
//            try {
//                ProviderSearchCriteria psc = new ProviderSearchCriteria();
//                psc.setPracticeName(request.getParameter("ClaimHeader_SearchPracticeName"));
//                Collection<ProviderDetails> result = port.findAllPracticesByCriteria(psc);
//                request.setAttribute("SearchMemberResults", result);
//                
//                RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/CaptureClaims_SearchMemberResults.jsp");
//                dispatcher.forward(request, response);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        } 

        return null;
    }

    private Claim saveClaim(HttpServletRequest request, NeoManagerBean port) {
        Claim claim = new Claim();
        claim.setPracticeNo(request.getParameter("ClaimHeader_PracticeNumber"));
        claim.setServiceProviderNo(request.getParameter("ClaimHeader_ProviderNumber"));
        claim.setReferingProviderNo(request.getParameter("ClaimHeader_ReferringProviderNumber"));
        String refNumber = request.getParameter("ClaimHeader_ReferalNumber");
        //System.out.println("RefNumber(still to do in backend etc) = " + refNumber);
        claim.setReceiveDate(TabUtils.getXMLDateParam(request, "ClaimHeader_DateReceived"));
        claim.setClaimSubmittedTotal(new Double(request.getParameter("ClaimHeader_ClaimTotal")));
        claim.setCurrencyId(1);
        claim.setPatientRefNo(request.getParameter("ClaimHeader_AccountNumber"));
        claim.setClaimReferencenumber(request.getParameter("ClaimHeader_AccountNumber"));
        claim.setScanIndexNumber(request.getParameter("ClaimHeader_ScanIndexNumber"));
        claim.setRecipient(request.getParameter("ClaimHeader_ReceiptBy"));
        claim.setCoverNumber(request.getParameter("ClaimHeader_CoverNumber"));
        System.out.println(request.getParameter("ClaimHeader_Motivation"));
        if ("true".equalsIgnoreCase(request.getParameter("ClaimHeader_Motivation"))) {
            claim.setMotivationReceived("Y");
        } else {
            claim.setMotivationReceived("N");
        }
        Security security = TabUtils.getSecurity(request);
        int batchId = TabUtils.getIntParam(request, "ClaimHeader_batchId");
        if (batchId == 0) {
            ClaimsBatch currentBatch = new ClaimsBatch();
            currentBatch.setBatchTypeId(1);
            currentBatch.setDateReceived(TabUtils.getXMLDateParam(request, "ClaimHeader_DateReceived"));
            currentBatch.setBatchId(port.saveBatch(currentBatch, security));
            batchId = currentBatch.getBatchId();
        }
        System.out.println("Batch: " + batchId);
        claim.setBatchId(batchId);
        claim.setClaimId(port.saveClaim(claim, batchId, security));
        System.out.println("claim: " + claim.getClaimId());
        return claim;
    }
    
    @Override
    public String getName() {
        return "ClaimHeaderCommand";
    }
    
}

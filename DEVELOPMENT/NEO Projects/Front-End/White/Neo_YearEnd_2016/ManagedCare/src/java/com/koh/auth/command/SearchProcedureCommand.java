/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.command;

import com.koh.auth.dto.ProcedureSearchResult;
import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.Procedure;
import neo.manager.ProcedureSearchCriteria;

/**
 *
 * @author whauger
 */

public class SearchProcedureCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        String code = request.getParameter("code");
        String description = request.getParameter("description");
        ProcedureSearchCriteria search = new ProcedureSearchCriteria();
        if (code != null && !code.equals(""))
            search.setCode(code);
        if (description != null && !description.equals(""))
            search.setDescription(description);

        ArrayList<ProcedureSearchResult> procedures = new ArrayList<ProcedureSearchResult>();
        List<Procedure> procList = new ArrayList<Procedure>();
        try {
           procList = service.getNeoManagerBeanPort().findAllProceduresByCriteria(search);

           for (Procedure proc : procList)
           {
             ProcedureSearchResult procedure = new ProcedureSearchResult();
             procedure.setCode(proc.getCode());
             procedure.setDescription(proc.getDescription());
             procedures.add(procedure);
           }

        } catch (Exception e) {
            e.printStackTrace();
        }
        /*
        TariffSearchResult r = new TariffSearchResult();
        r.setCode("10011");
        r.setDescription("Expensive");

        TariffSearchResult r2 = new TariffSearchResult();
        r2.setCode("20022");
        r2.setDescription("Very Expensive");

        tariffs.add(r);
        tariffs.add(r2);
        */

        request.setAttribute("searchProcedureResult", procedures);
        try {
            String nextJSP = "/Auth/ProcedureSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SearchProcedureCommand";
    }

}

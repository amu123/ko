/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.practice_management.command;

import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Johan-NB
 */
public class ClearPracticeScreen extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.clearScreenFromSession(request);
        try {
            String nextJSP = "/SystemAdmin/PracticeCapture.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ClearPracticeScreen";
    }
}

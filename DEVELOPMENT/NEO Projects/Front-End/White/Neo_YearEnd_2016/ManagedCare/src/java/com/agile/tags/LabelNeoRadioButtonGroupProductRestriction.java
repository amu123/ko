/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import static com.koh.command.NeoCommand.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.ProductRestriction;

/**
 *
 * @author marcelp
 */
public class LabelNeoRadioButtonGroupProductRestriction extends TagSupport {

    private String elementName;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        Integer userId = new Integer((session.getAttribute("userId") == null ? "" : session.getAttribute("userId").toString()));
        System.out.println("generating tag for user - "+ userId);
        NeoManagerBean port = service.getNeoManagerBeanPort();
        List<ProductRestriction> userRLst = port.getUserProductRestriction(userId);
        HashMap<Integer, String> optionMap = new HashMap<Integer, String>();
        optionMap.put(1, "Resolution Health");
        optionMap.put(2, "Spectramed");
        optionMap.put(999, "Both");
        Boolean allowReso = false;
        Boolean allowSpec = false;
        System.out.println("query list size:" + userRLst.size());
        for (ProductRestriction rest : userRLst) {
            System.out.println("<<<<productID: "+ rest.getProductID());
            if (rest.getProductID() == 1) {
                System.out.println("<<<<print out prod 1");
                allowReso = true;
            }
            if (rest.getProductID() == 2) {
                System.out.println("<<<<print out prod 2");
                allowSpec = true;
            }
        }
        try {
            int count = 0;
            out.print("<tr>");
            for (Entry<Integer, String> element : optionMap.entrySet()) {


                if (!allowReso && !allowSpec && element.getKey() == 999) {
                    System.out.println("<<<<print out tag 1");
                    out.print("<td><input type=\"radio\" name=\"" + elementName + "\" value=\"" + element.getKey() + "\" checked><label>" + element.getValue() + "</label></input></td>");
                } else if (allowReso && !allowSpec && element.getKey() == 1) {
                    System.out.println("<<<<print out tag 2");
                    out.print("<td><input type=\"radio\" name=\"" + elementName + "\" value=\"" + element.getKey() + "\" checked><label>" + element.getValue() + "</label></input></td>");
                } else if (!allowReso && allowSpec && element.getKey() == 2) {
                    System.out.println("<<<<print out tag 3");
                    out.print("<td><input type=\"radio\" name=\"" + elementName + "\" value=\"" + element.getKey() + "\" checked><label>" + element.getValue() + "</label></input></td>");
                } else {
                    System.out.println("<<<<print out tag 4");
                    out.print("<td><input type=\"radio\" name=\"" + elementName + "\" value=\"" + element.getKey() + "\" ><label>" + element.getValue() + "</label></input></td>");
                }

            }
            out.println("</tr>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelNeoRadioButtonGroupProductRestriction tag", ex);
        }

        return super.doEndTag();
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }
}

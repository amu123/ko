/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.utils;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

/**
 *
 * @author johanl
 */
public class FormatUtils {

    //date formats
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    public static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    public static final SimpleDateFormat dateTimeNoSecFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
    public static final SimpleDateFormat timeNoSecFormat = new SimpleDateFormat("HH:mm");
    public static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    public static final String timePattern = Pattern.compile("^([01]?[0-9]|2[0-3]):[0-5][0-9]").pattern();
    //decimal format
    public static final DecimalFormat decimalFormat = new DecimalFormat(".00");
    
    //BigDecimal
    public static String formatBigDecimal(BigDecimal bd) {
        if (bd == null) {
            return "0.00";
        } else {
            try {
                return bd.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            } catch (Exception e) {
                return "0.00";
            }
        }
    }
}


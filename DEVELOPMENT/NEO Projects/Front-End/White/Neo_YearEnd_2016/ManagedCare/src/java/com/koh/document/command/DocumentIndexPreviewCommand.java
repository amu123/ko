/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;


import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import java.io.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class DocumentIndexPreviewCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        logger.info("----------------Inside DocumentIndexPreviewCommand-----------");
        try {
            String selectedFile = request.getParameter("selectedFile")!=null?request.getParameter("selectedFile"):"";
            String folderList = (String) request.getParameter("folderList");
            String listOrWork = (String) request.getParameter("listOrWork");
            String claimNumber = request.getParameter("claimNumber")!=null?request.getParameter("claimNumber"):"";
            String memberNumber = request.getParameter("memberNumber");
            String docType = request.getParameter("docType");
            String profileType = (String)request.getSession().getAttribute("profile");
            logger.info("folderList === " + folderList);
            if (folderList == null) {
                folderList =  new PropertiesReader().getProperty("DocumentIndexScanFolder");
            }
            logger.info("folderList === " + folderList);
            String srcDrive = new PropertiesReader().getProperty("DocumentIndexScanDrive");
            logger.info("srcDrive" + srcDrive);
            String destDrive = new PropertiesReader().getProperty("DocumentIndexWorkDrive");
            logger.info("destDrive" + destDrive);
            logger.info("Filename = " + selectedFile);
            String location = "";
            logger.info("listOrWork  == " + listOrWork);
            if (listOrWork != null && listOrWork.equals("Work")) {
                logger.info("In work folder not doing a backend call");
                location = destDrive + folderList+"/"+profileType + "/" + selectedFile;
            } else {
                logger.info("ERRRROOOORRRRR");
            }
            logger.info("File to open = " + location);
            if (location == null || location.equals("")) {
                request.setAttribute("folderList", folderList);
                request.setAttribute("listOrWork", listOrWork);
                request.setAttribute("indexEntityNumber",memberNumber);
                request.setAttribute("indexRefNumber",claimNumber);
                request.setAttribute("docType",docType);
                request.setAttribute("errorMsg", "No records found.");
                RequestDispatcher dispatcher = null;
                if(profileType.toLowerCase().contains("claim")){
                    dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentClaims.jsp");
                }else if(profileType.toLowerCase().contains("member")){
                    dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentMember.jsp");
                }else{
                    dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentGeneric.jsp");
                }
                dispatcher.forward(request, response);
                return null;
            }else if(selectedFile == null || selectedFile.equals("")){
                request.setAttribute("folderList", folderList);
                request.setAttribute("listOrWork", listOrWork);
                request.setAttribute("indexEntityNumber",memberNumber);
                request.setAttribute("indexRefNumber",claimNumber);
                request.setAttribute("docType",docType);
                request.setAttribute("errorMsg", "No file selected");
                RequestDispatcher dispatcher = null;
                if(profileType.toLowerCase().contains("claim")){
                    dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentClaims.jsp");
                }else if(profileType.toLowerCase().contains("member")){
                    dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentMember.jsp");
                }else{
                    dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentGeneric.jsp");
                }
                dispatcher.forward(request, response);
                return null;
            }
            String patchVar = location;
            logger.info("Path: " + patchVar);
            File file = new File(patchVar);
            InputStream in = new FileInputStream(file);
            int contentLength = (int) file.length();
            logger.info("The content length is " + contentLength);
            ByteArrayOutputStream temporaryOutput;
            if (contentLength != -1) {
                temporaryOutput = new ByteArrayOutputStream(contentLength);
            } else {
                temporaryOutput = new ByteArrayOutputStream(20480);
            }

            byte[] bufer = new byte[512];
            while (true) {

                int len = in.read(bufer);
                if (len == -1) {

                    break;
                }
                temporaryOutput.write(bufer, 0, len);
            }
            in.close();
            temporaryOutput.close();

            byte[] array = temporaryOutput.toByteArray();
            request.setAttribute("folderList", folderList);
            request.setAttribute("listOrWork", listOrWork);
            printPreview(request, response, array, location);

        } catch (Exception e) {
            logger.error("Exception in DocumentIndexPreviewCommand = " + e.getMessage());
        }
        return "DocumentIndexPreviewCommand";
    }

    public void printPreview(HttpServletRequest request, HttpServletResponse response, byte[] array, String fileName) throws IOException {
        OutputStream outStream = null;
        try {

            response.setHeader("Pragma", "no-cache"); //HTTP 1.0
            response.setDateHeader("Expires", 0); //prevents caching at the proxy server
            response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-disposition", "attachment; filename=" + fileName);

            response.setContentType("application/pdf");
            response.setContentLength(array.length);
            outStream = response.getOutputStream();
            outStream.write(array);
            outStream.flush();
            outStream.close();

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void printErrorResult(HttpServletRequest request, HttpServletResponse response,String profileType) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");

            out.println("<td><label class=\"header\">No records found.</label></td>");

            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");
            if(profileType.toLowerCase().contains("claim")){
                response.setHeader("Refresh", "2; URL=/ManagedCare/Indexing/IndexDocumentClaims.jsp");
            }else if(profileType.toLowerCase().contains("member")){
                response.setHeader("Refresh", "2; URL=/ManagedCare/Indexing/IndexDocumentMember.jsp");
            }else{
                response.setHeader("Refresh", "2; URL=/ManagedCare/Indexing/IndexDocumentGeneric.jsp");
            }

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }
    
    @Override
    public String getName() {
        return "DocumentIndexPreviewCommand";
    }
}

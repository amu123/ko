/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import agility.za.documentprinting.DocumentPrintingRequest;
import agility.za.documentprinting.DocumentPrintingResponse;
import agility.za.documentprinting.PrintDocType;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class PrintPoolFilterCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------PrintPoolFilterCommand---------------------");


            String displayOption = request.getParameter("displayOption");
            String displayDay =  request.getParameter("displayDay");
            String displayGroup =  request.getParameter("displayGroup");
            String displayType =  request.getParameter("displayType");
            String displayScheme =  request.getParameter("displayScheme");
            
            logger.info("displayOption = " + displayOption);
            logger.info("displayDay = " + displayDay);
            logger.info("displayGroup = " + displayGroup);
            logger.info("displayType = " + displayType);
            logger.info("displayScheme = " + displayScheme);
            
            request.setAttribute("displayOption", displayOption);
            request.setAttribute("displayDay", displayDay);
            request.setAttribute("displayGroup", displayGroup);
            request.setAttribute("displayType", displayType);
            request.setAttribute("displayScheme", displayScheme);
                        
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Statement/PrintPool.jsp");
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            logger.error(response, ex);
        }
        return "PrintPoolFilterCommand";
    }

    @Override
    public String getName() {
        return "PrintPoolFilterCommand";
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.EAuthCoverDetails;

/**
 *
 * @author johanl
 */
public class EAuthCoverDetailListTag extends TagSupport {

    private String elementName;
    private String displayName;
    private String valueFromSession = "no";
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        HttpSession session = pageContext.getSession();
        ServletRequest request = pageContext.getRequest();
        ServletResponse response = pageContext.getResponse();
        PrintWriter out = null;

        try {
            out = response.getWriter();
            out.println("<td><label class=\"label\">" + displayName + ":</label></td>");

            out.println("<td>");
            if (javaScript != null) {
                out.println("<select name=\"" + elementName + "\" id=\"" + elementName + "\" "+ javaScript +" >");
            }
            out.println("<select name=\"" + elementName + "\" id=\"" + elementName + "\">");

            if (valueFromSession.trim().equalsIgnoreCase("yes")) {
                String selectedBox = "" + session.getAttribute(elementName);
                List<EAuthCoverDetails> ecList = (List<EAuthCoverDetails>) session.getAttribute("eAuthCoverList");
                if (ecList.size() != 0) {
                    for (int i = 0; i < ecList.size(); i++) {
                        EAuthCoverDetails ec = ecList.get(i);
                        String ecDepInfo = ec.getCoverName() + "|" + ec.getCoverSurname() + "|" + ec.getDependantType();
                        if (ecDepInfo.equalsIgnoreCase(selectedBox)) {
                            out.println("<option value=\"" + ecDepInfo + "\" selected >" + ecDepInfo + "</option>");
                        } else {
                            out.println("<option value=\"" + ecDepInfo + "\" >" + ecDepInfo + "</option>");
                        }
                    }
                }else{
                    out.println("<option></option>");
                }
            }
            out.println("</select></td>");
        } catch (IOException io) {
            io.printStackTrace();
        }finally{
            out.close();
        }
        return super.doEndTag();
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setValueFromSession(String valueFromSession) {
        this.valueFromSession = valueFromSession;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.employer.command.TabUtils;
import com.koh.serv.PropertiesReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.Claim;
import neo.manager.ClaimCptCodes;
import neo.manager.ClaimIcd10;
import neo.manager.ClaimLine;
import neo.manager.CoverDetails;
import neo.manager.Eye;
import neo.manager.Modifier;
import neo.manager.Ndc;
import neo.manager.NeoManagerBean;
import neo.manager.Security;

/**
 *
 * @author janf
 */
public class EditClaimCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String command = request.getParameter("command");
        Date today = new Date(System.currentTimeMillis());
        PrintWriter out = null;
        System.out.println("Operation: EditClaimCommand - Command: " + command);
        if ("populateClaimLine".equalsIgnoreCase(command)) {

            //int claimLineId = Integer.parseInt("" + session.getAttribute("memberDetailedClaimLineId"));
            System.out.println("claimLineId = " + request.getParameter("claimLineId"));
            String sClaimLineId = request.getParameter("claimLineId");
            int claimLineId = 0;
            if(sClaimLineId != null && !sClaimLineId.equalsIgnoreCase("") && !sClaimLineId.equalsIgnoreCase("undefined")){
                claimLineId = Integer.parseInt(request.getParameter("claimLineId"));
                System.out.println("claimLineId = " + claimLineId);
            } else {
                claimLineId = Integer.parseInt("" + session.getAttribute("memberDetailedClaimLineId"));
            }
            //ClaimLine cl = (ClaimLine) session.getAttribute("memCLCLDetails");
            ClaimLine cl = port.fetchClaimLineById(claimLineId);
            System.out.println("ClaimLineId met session = " + claimLineId);
            System.out.println("ClaimLineId = " + cl.getClaimLineId());
            System.out.println("ClaimID = " + cl.getClaimId());
            session.setAttribute("claimId", cl.getClaimId());
            session.setAttribute("ClaimLine_claimId", cl.getClaimId());
            session.setAttribute("claimLineId", claimLineId);
            Claim c = port.fetchClaimForClaimLine(cl.getClaimId());
            request.setAttribute("ClaimLine_Practice_Type", c.getServiceProviderDisciplineId());
            int Category = c.getClaimCategoryTypeId();
            
            System.out.println("Category = ." + Category + "." + c.getClaimCategoryTypeId());
            System.out.println("[fr]practice = " + c.getPracticeNo() + "\nprovider = " + c.getServiceProviderNo());
            String practiceNo = c.getPracticeNo();
            String providerNo = c.getServiceProviderNo();

            XMLGregorianCalendar xserviceDateFrom = cl.getServiceDateFrom();
            XMLGregorianCalendar xserviceDateTo = cl.getServiceDateTo();
            String serviceDateFrom = "";
            if(xserviceDateFrom != null){
                serviceDateFrom = xserviceDateFrom.toString();
                serviceDateFrom = serviceDateFrom.substring(0,10);
                serviceDateFrom = serviceDateFrom.replace('-', '/');
            }
            
            String serviceDateTo = "";
            if(xserviceDateTo != null){
                serviceDateTo = xserviceDateTo.toString();
                serviceDateTo = serviceDateTo.substring(0,10);
                serviceDateTo = serviceDateTo.replace('-', '/');
            }
            
            String coverNum = c.getCoverNumber();
            String timeIn = cl.getTimeIn();
            String timeOut = cl.getTimeOut();
            String tarrif = cl.getTarrifCodeNo();
            String practiceDiscId = c.getServiceProviderDisciplineId();
            int multiplier = cl.getMultiplier();
            double units = cl.getUnits();
            int quantity = 1;
            String labInvoiceNumber = cl.getLabInvoiceNumber();
            String labOrderNumber = cl.getLabOrderNumber();
            double claimedAmount = cl.getClaimedAmount();
            List<String> authNumbers = cl.getAuthNumber();
            List<Integer> toothNumbers = cl.getToothNumber();
            List<ClaimCptCodes> cptCodes = cl.getCptCodes();
            List<ClaimIcd10> icd10 = cl.getIcd10();
            List<Eye> eyes = cl.getEyes();
            List<Ndc> ndcs = cl.getNdc();
            List<Modifier> modifier = cl.getModifiers();
            
//            List<CoverDetails> covList = port.getDependantsForPrincipleMemberByDate(coverNum);
//            session.setAttribute("covList", covList);
            
            System.out.println("serviceDateFrom = " + serviceDateFrom + "\nService To = " + serviceDateTo
                    + "\nTimeIn = " + timeIn + "\nTimeOut = " + timeOut + "\nTarrif = " + tarrif
                    + "\nmultiplier = " + multiplier + "\nunits = " + units
                    + "\nquan = " + quantity + "\nlabInvoice = " + labInvoiceNumber + "\nlaborder = " + labOrderNumber + "\namount = " + claimedAmount 
                    + "\nCoverNumber = " + coverNum + "\npracticeDiscId = " + practiceDiscId);

            String authNum ="";
            for (String auth : authNumbers) {
                System.out.println("Auth = " + auth);
                authNum += "," + auth;
            }
            if(!authNum.equalsIgnoreCase("")){
                authNum = authNum.substring(1);
            }
            System.out.println("auth = " + authNum);
            
            String teeth = "";
            for (int tooth : toothNumbers) {
                System.out.println("tooth = " + tooth);
                teeth += "," + tooth;
            }
            if(!teeth.equalsIgnoreCase("")){
                teeth = teeth.substring(1);
            }
            
            String icd = "";
            String refIcd = "";
            for (ClaimIcd10 icds : icd10) {
                System.out.println("icd = " + icds.getIcd10Code() +  " " + icds.getReferingIcd10());
                if(icds.getReferingIcd10() == 0){
                    icd += "," + icds.getIcd10Code();
                } else {
                    refIcd += "," + icds.getIcd10Code();
                }
            }
            if(!icd.equalsIgnoreCase("")){
                icd = icd.substring(1);
            }
            if(!refIcd.equalsIgnoreCase("")){
                refIcd = refIcd.substring(1);
            }
            
            String leftEye = "";
            String rightEye = "";
            for (Eye eye : eyes) {
                System.out.println("eyes = " + eye + eye.getEyeLR());
                if(eye.getEyeLR().equalsIgnoreCase("R")){
                    rightEye += "," + eye.getLensesRX();
                } else {
                    leftEye += "," + eye.getLensesRX();
                }
            }
            
            for (Ndc ndc : ndcs) {
                System.out.println("ndc = " + ndc.getNdcCode());
            }
            session.setAttribute("claimLineNdcList",ndcs);
            
            for (Modifier mod : modifier) {
                System.out.println("mod = " + mod.getModifier());
            }
            System.out.println("Modifier = " + modifier);
            if(modifier != null && !modifier.isEmpty()){
                System.out.println("Mod not null");
                session.setAttribute("modList", modifier);
                request.setAttribute("modList", modifier);
            } else {
                System.out.println("Mod is null");
                session.setAttribute("modList", null);
                request.setAttribute("modList", null);
            }
            
            String cpts = "";
            for(ClaimCptCodes cpt : cptCodes){
                System.out.println("cpt = " + cpt);
                cpts += "," + cpt.getCptCode();
            }
            if(!cpts.equalsIgnoreCase("")){
                cpts = cpts.substring(1);
            }
            XMLGregorianCalendar runDate = cl.getChequeRunDate();
            System.out.println("rundate = " + runDate);
            
            String result = Category + "|" + coverNum +  "|" + serviceDateFrom +  "|" + serviceDateTo +  "|"
                    + icd + "|" + refIcd +  "|" + tarrif +  "|" + modifier +  "|" + multiplier +  "|" + units +  "|" + quantity +  "|" + authNum +  "|"
                    + ndcs +  "|" + claimedAmount +  "|" + rightEye + "|" + leftEye +  "|" + teeth +  "|" + labInvoiceNumber +  "|" + labOrderNumber  +  "|"
                    + timeIn +  "|" + timeOut +  "|" + cpts + "|" + practiceDiscId + "|" + providerNo + "|" + practiceNo;
            
            try {
                out = response.getWriter();
                out.print(result);
            } catch (Exception e) {
                e.printStackTrace();
            }

//            try {
//                RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/EditClaims_ClaimLine.jsp");
//                System.out.println("dispatcher = " + dispatcher.toString());
//                dispatcher.forward(request, response);
//
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
        } else if("updateClaimLine".equalsIgnoreCase(command)){
            ClaimLine cl = updateClaimLine(request, port, session);
            System.out.println("Claim Line: " + cl.getClaimLineId());
            System.out.println("claimLineId = " + session.getAttribute("claimLineId"));
            session.setAttribute("claimLineId", "");
            System.out.println("claimLineId = " + session.getAttribute("claimLineId"));
        } else if("editClaimLine".equalsIgnoreCase(command)){
            System.out.println("editClaimLine Called");
            //ClaimLine clz = (ClaimLine) session.getAttribute("memCLCLDetails");
            String sclaimLineId = request.getParameter("claimLineId");
            ClaimLine cl;//= new ClaimLine();
            //int claimLineId = Integer.parseInt("" + session.getAttribute("claimLineId"));
            //System.out.println("claimLineId = " + claimLineId);
            if(sclaimLineId != null && !sclaimLineId.equalsIgnoreCase("") && !sclaimLineId.equalsIgnoreCase("undefined")){
                int claimLineId = Integer.parseInt(sclaimLineId);
                cl = port.fetchClaimLineById(claimLineId);
                session.setAttribute("claimLineId", request.getParameter("claimLineId") );
            } else {
                cl = (ClaimLine) session.getAttribute("memCLCLDetails");
                request.setAttribute("claimLineId", cl.getClaimLineId());
            }
            
            System.out.println("ClaimID = " + cl.getClaimId());
            System.out.println("ClaimLine_claimId = " + request.getParameter("ClaimLine_claimId"));
            System.out.println("claimId req = " + request.getParameter("claimId"));
            System.out.println("getClaimLineId (error) = " + cl.getClaimLineId());
            System.out.println("getClaimLineId req(error) = " + request.getParameter("claimLineId"));
            session.setAttribute("claimId", cl.getClaimId());
            Claim c = port.fetchClaimForClaimLine(cl.getClaimId());
            System.out.println("c.getCoverNumber() = " + c.getCoverNumber());
            List<CoverDetails> covList = port.getDependantsForPrincipleMemberByDate(c.getCoverNumber());
            session.setAttribute("covList", covList);
            
            //check if reverse or update
            String reverse = request.getParameter("reverse");
            System.out.println("reverse = " + reverse);
            if(reverse != null && reverse.equalsIgnoreCase("true")){
                session.setAttribute("reverse", reverse);
            } else {
                session.setAttribute("reverse","");
            }
            try {
                RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/EditClaims_ClaimLine.jsp");
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if("addClaimLine".equalsIgnoreCase(command)){
            System.out.println("addClaimLine Called");
            ClaimLine cl = (ClaimLine) session.getAttribute("memCLCLDetails");
            Claim c = port.fetchClaimForClaimLine(cl.getClaimId());
            String coverNum = c.getCoverNumber();
            List<CoverDetails> covList = port.getDependantsForPrincipleMemberByDate(coverNum);
            System.out.println("ClaimID = " + cl.getClaimId());
            session.setAttribute("claimId", cl.getClaimId());
            session.setAttribute("claimLineId", "");
            request.setAttribute("ClaimLine_Practice_Type", c.getServiceProviderDisciplineId());
            session.setAttribute("test", c.getServiceProviderDisciplineId());
            
            session.setAttribute("covList", covList);
            request.setAttribute("ClaimLine_CoverNumber", coverNum);
            
            System.out.println("providerdiscID = " + c.getServiceProviderDisciplineId());
            try {
                RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/EditClaims_ClaimLine.jsp");
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if("getPaymentRunStatus".equalsIgnoreCase(command)) {
            ClaimLine cl = (ClaimLine) session.getAttribute("memCLCLDetails");
            XMLGregorianCalendar runDate = cl.getChequeRunDate();
            System.out.println("runDate = " + runDate);
            String result = "";
            
            if (runDate == null){
                System.out.println("not yet through payment run");
                result = "NoRun";
            } else {
                System.out.println("went through payment run");
                result = "Run";
            }
            
            try {
                out = response.getWriter();
                out.print(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if("editClaim".equalsIgnoreCase(command)){
            System.out.println("editClaim Called");
            ClaimLine cl = (ClaimLine) session.getAttribute("memCLCLDetails");
            Claim c = port.fetchClaimForClaimLine(cl.getClaimId());
            request.setAttribute("editClaimLineId", cl.getClaimLineId());
            request.setAttribute("editClaimId", cl.getClaimId());
            session.setAttribute("editClaimLineId", cl.getClaimLineId());
            session.setAttribute("editClaimId", cl.getClaimId());
            session.setAttribute("ClaimHeader_batchId", c.getBatchId());
            request.setAttribute("ClaimHeader_batchId", c.getBatchId());
            System.out.println("BatchId = " + c.getBatchId());
            try {
                RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/EditClaims_ClaimHeader.jsp");
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if ("exportClaim".equalsIgnoreCase(command)) {
            this.saveScreenToSession(request);
            String sclaimId = String.valueOf(session.getAttribute("memClaimId"));
            int claimId = Integer.parseInt(sclaimId);
            System.out.println("claimId = " + claimId);
            String result = "";
            try {
                String indexDrive = new PropertiesReader().getProperty("DocumentIndexWorkDrive");
                result = port.claimsExport(claimId, indexDrive);
                String[] resultArr = result.split("\\|");

                String fileName = "";
                System.out.println("Result = " + result);
                if (resultArr[0].equalsIgnoreCase("DONE")) {
                    fileName = resultArr[1];
                    System.out.println("fileName = " + fileName);
                    if (fileName == null || fileName.equals("")) {
                        return null;
                    }
                } else {
                    return null;
                }

                System.out.println("Path: " + indexDrive + fileName);
                File file = new File(indexDrive + fileName);
                InputStream in = new FileInputStream(file);
                
                int contentLength = (int) file.length();
                System.out.println("The content length is " + contentLength);
//                out = response.getWriter();
//                out.print(file.toURI().toURL());
//                response.reset();
                
                ByteArrayOutputStream temporaryOutput;
                if (contentLength != -1) {
                    temporaryOutput = new ByteArrayOutputStream(contentLength);
                } else {
                    temporaryOutput = new ByteArrayOutputStream(20480);
                }

                byte[] bufer = new byte[512];
                while (true) {

                    int len = in.read(bufer);
                    if (len == -1) {

                        break;
                    }
                    temporaryOutput.write(bufer, 0, len);
                }
                in.close();
                
                byte[] bytes = temporaryOutput.toByteArray();

                printPreview(request, response, bytes, fileName);

            } catch (MalformedURLException ex) {

                ex.printStackTrace();
            } catch (IOException ioex) {
                // FileNotFound exception
                System.out.println(ioex.getMessage());
                System.out.println("The ioex.getMessage() " + ioex.getMessage());
                ioex.printStackTrace();
            }
        } else if("changeRecipient".equalsIgnoreCase(command)){
            String recipient = request.getParameter("recipient");
            int claimId = Integer.parseInt(request.getParameter("claimId"));
            System.out.println("recipient = " + recipient + "\nclaimId = " + claimId);
            String newRecipient = "";

            
            if(recipient.equalsIgnoreCase("3")){
                newRecipient = "5";
            } else if(recipient.equalsIgnoreCase("5")){
                newRecipient = "3";
            }
            
            boolean result = port.changeClaimRecipient(claimId, newRecipient);
            
            try {
                out = response.getWriter();
                out.print(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }
    private ClaimLine updateClaimLine(HttpServletRequest request, NeoManagerBean port, HttpSession session) {
        ClaimLine cl = new ClaimLine();
        cl.setServiceDateFrom(TabUtils.getXMLDateParam(request, "ClaimLine_DateStart"));
        cl.setServiceDateTo(TabUtils.getXMLDateParam(request, "ClaimLine_DateEnd"));
        String dep = request.getParameter("ClaimLine_Dependant");
        String[] deps = dep.split("_");
        System.out.println("dep  = " + dep);
        cl.setInsuredPersonEntityId(new Integer(deps[0])); //todo
        cl.setInsuredPersonDependentCode(new Integer(deps[1]));  //todo
        cl.setTimeIn(request.getParameter("ClaimLine_Time_In"));
        cl.setTimeOut(request.getParameter("ClaimLine_Time_Out"));
        cl.setTarrifCodeNo(request.getParameter("ClaimLine_Tariff").toUpperCase());
        cl.setMultiplier(TabUtils.getIntParam(request, "ClaimLine_Multiplier"));
        cl.setUnits(TabUtils.getBDParam(request, "ClaimLine_Units").doubleValue());
        cl.setQuantity(TabUtils.getIntParam(request, "ClaimLine_Quantity"));
        cl.setLabInvoiceNumber(request.getParameter("ClaimLine_Invoice_Number"));
        cl.setLabOrderNumber(request.getParameter("ClaimLine_Order_Number"));
        cl.setClaimedAmount(TabUtils.getBDParam(request, "ClaimLine_Claimed_Amount").doubleValue());
        cl.setClaimLineStatusId(1);
        cl.setChequeRunID(0);
        cl.setChequeRunDate(null);
        cl.setProsessDate(null);
        Collection<String> authNumbers = cl.getAuthNumber();
        String authNumber = request.getParameter("ClaimLine_Authorization");
        if (authNumber != null && !"".equalsIgnoreCase(authNumber)) {
            authNumbers.add(authNumber);
            //cl.getAuthNumber().add(authNumber);
        }
        Collection<Integer> toothNumbers = cl.getToothNumber();
        //String toothNumber = request.getParameter("ClaimLine_Tooth_Numbers");
        String toothList = request.getParameter("toothList");
        System.out.println("ToothList = " + toothList);
        if (toothList != null && !"".equals(toothList)) {
            if(toothList.contains(",")){
                toothList = toothList.replace(",", " ");
            }
            String[] sArr = toothList.split(" ");
            for (String s : sArr) {
                toothNumbers.add(new Integer(s));
                //cl.getToothNumber().add(new Integer(s));
            }
        }
        Collection<ClaimCptCodes> cptCodes = cl.getCptCodes();
        //String cptCode = request.getParameter("ClaimLine_CPT");
        String cptList = request.getParameter("cptList");
        System.out.println("cptList = " + cptList);
        if (cptList != null && !"".equals(cptList)) {
            if(cptList.contains(",")){
                    cptList = cptList.replace(",", " ");
                }
            String[] sArr = cptList.split(" ");
            for (String s : sArr) {
                ClaimCptCodes ccptc = new ClaimCptCodes();
                ccptc.setCptCode(s);
                cptCodes.add(ccptc);
                //cl.getCptCodes().add(ccptc);
            }
        }
        Collection<ClaimIcd10> icd10 = cl.getIcd10();
        //String icdCode = request.getParameter("ClaimLine_Diagnosis");
        String icdList = request.getParameter("icdList");
        System.out.println("icdList = " + icdList);
        if (icdList != null && !"".equals(icdList)) {
            if(icdList.contains(",")){
                    icdList = icdList.replace(",", " ");
                }
            String[] sArr = icdList.split(" ");
            for (String s : sArr) {
                ClaimIcd10 cicd = new ClaimIcd10();
                cicd.setIcd10Code(s.toUpperCase());
                cicd.setReferingIcd10(0);
                icd10.add(cicd);
                //cl.getIcd10().add(cicd);
            }
        }
//        icdCode = request.getParameter("ClaimLine_Ref_Diagnosis");
        icdList = request.getParameter("icdRefList");
        System.out.println("icdRefList = " + icdList);
        if (icdList != null && !"".equals(icdList)) {
            if(icdList.contains(",")){
                    icdList = icdList.replace(",", " ");
                }
            String[] sArr = icdList.split(" ");
            for (String s : sArr) {
                ClaimIcd10 cicd = new ClaimIcd10();
                cicd.setIcd10Code(s.toUpperCase());
                cicd.setReferingIcd10(1);
                icd10.add(cicd);
                //cl.getIcd10().add(cicd);
            }
        }
        Collection<Eye> eyes = cl.getEyes();
        String lensLeft = request.getParameter("ClaimLine_Lens_Left");
        if (lensLeft != null && !"".equalsIgnoreCase(lensLeft)) {
            Eye eye = new Eye();
            eye.setEyeLR("L");
            eye.setLensesRX(lensLeft);
            eyes.add(eye);
            //cl.getEyes().add(eye);
        }
        String lensRight = request.getParameter("ClaimLine_Lens_Left");
        if (lensRight != null && !"".equalsIgnoreCase(lensRight)) {
            Eye eye = new Eye();
            eye.setEyeLR("R");
            eye.setLensesRX(lensRight);
            eyes.add(eye);
            //cl.getEyes().add(eye);
        }
        
        Collection<Ndc> ndcs = cl.getNdc();
        List<Ndc> clNdcList = (List<Ndc>) session.getAttribute("claimLineNdcList");
        System.out.println("clNdcList = " + clNdcList);

        if(clNdcList != null && !clNdcList.isEmpty()){
            System.out.println("clNdcList size = "  + clNdcList.size());
            for(Ndc nc : clNdcList){
                ndcs.add(nc);
                //cl.getNdc().add(nc);
            }
        }
        Collection<Modifier> modifier = cl.getModifiers();
        List<Modifier> modList = (List<Modifier>) session.getAttribute("modList");
        System.out.println("modList = " + modList);
        if(modList != null && !modList.isEmpty()){
            System.out.println("modList size = " + modList.size());
            for(Modifier mod : modList){
                modifier.add(mod);
            }
        }
        
        session.setAttribute("claimLineNdcList",null);
        session.setAttribute("NdcLine",null);
        session.setAttribute("modList",null);

        Security security = TabUtils.getSecurity(request);
        int claimID = TabUtils.getIntParam(request, "ClaimLine_claimId");
        int claimLineId = TabUtils.getIntParam(request,"claimLineId");
        System.out.println("ClaimLineId = " + claimLineId);
        if(claimLineId == 0){
            System.out.println("Save");
            cl.setClaimLineId(port.saveClaimLine(cl, claimID, security,true));
        } else {
            System.out.println("Update");
            cl.setClaimLineId(claimLineId);
            cl.setClaimLineId(port.saveClaimLine(cl, claimID, security,false));
        }
        //cl.setClaimLineId(claimLineId);
        System.out.println("ClaimID = " + claimID);
        System.out.println("Quantity = " + cl.getQuantity());
        System.out.println("cl == null = " + (cl == null));
        System.out.println("ClaimId van cl = " + cl.getClaimId() + "\nClaimLineId = " + cl.getClaimLineId() + " - " + cl.getClaimLineNumber());
        return cl;
    }
    
    private void printPreview(HttpServletRequest request, HttpServletResponse response, byte[] array, String fileName) throws IOException {
        OutputStream outStream = null;
        try {
            System.out.println("fileName = " + fileName);
            response.setHeader("Pragma", "no-cache"); //HTTP 1.0
            response.setDateHeader("Expires", 0); //prevents caching at the proxy server
            response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            response.setHeader("Content-Type", "text/csv");
            response.setHeader("Content-Transfer-Encoding", "binary");

            //response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setContentLength(array.length);
            outStream = response.getOutputStream();
            outStream.write(array);
            outStream.flush();
            outStream.close();
            System.out.println("[frs]DONE = " + Arrays.toString(array));

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    @Override
    public String getName() {
        return "EditClaimCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CommunicationLog;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class MemberCommunicationLogCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------MemberCommunicationLogCommand---------------------");

            String memberEntityId = request.getParameter("memberEntityId");
            logger.info("memberEntityId = " + memberEntityId);
            if (memberEntityId == null) {
                memberEntityId = (String) request.getAttribute("memberEntityId");
                logger.info("memberEntityId 2 = " + memberEntityId);
            }
            String displayOption = request.getParameter("displayOption");
            logger.info("displayOption = " + displayOption);
//             Map<String, String> fieldsMap = new HashMap<String, String>();
//                   fieldsMap.put("commLog", "<tr><td></td></tr>");
//                   fieldsMap.put("someTest", "Bla bla");
//            String updateFields = TabUtils.convertMapToJSON(fieldsMap);
//            String result = TabUtils.buildJsonResult(null, null, null, "<tr><td></td></tr>");
//            logger.info(result);
            PrintWriter out = response.getWriter();
            NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
            if (displayOption == null || displayOption.equals("")) {
                displayOption = "0";
            }

            List<LookupValue> values = port.getCodeTable(220);
            System.out.println("Values size = " + values.size());
            LookupValue lookupValue = new LookupValue();
            lookupValue.setId("0");
            lookupValue.setValue("All");
            values.add(lookupValue);
            out.println("<table id=\"commLog\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><td><select style=\"width:215px\" size=\"1\" name=\"displayOption\" value=\"" + displayOption + "\" >");
            for (LookupValue val : values) {
                if (displayOption.equalsIgnoreCase(val.getId())) {
                    out.println("<option selected=\"selected\" value=\"" + val.getId() + "\" >" + val.getValue() + "</option>");
                } else {
                    out.println("<option value=\"" + val.getId() + "\" >" + val.getValue() + "</option>");
                }
            }

            out.println("</select> </td>");
            out.println("<td><input type=\"button\" id=\"filterD\" name=\"filterD\" value=\"Filter\" onClick=\"submitFormWithAjaxPost(this.form, 'commLog');\" ></input></td></tr>");
            List<CommunicationLog> commLogs = NeoCommand.service.getNeoManagerBeanPort().getCommunicationLogForEntityId(new Integer(memberEntityId), new Integer(displayOption));

            out.println("<tr><th>Sms Log Id</th><th>Contact Details</th><th>Status</th><th>Type</th><th>Attempt Date</th><th>Communication Method</th></tr>");
            if (commLogs != null && commLogs.size() > 0) {
                out.println("<div id=\"commLog\" >");
                for (CommunicationLog log : commLogs) {
                    out.println("<tr><td><label class=\"label\">" + log.getClaimsSmsLogId() + "</label></td>"
                            + "<td><label class=\"label\">" + log.getContactDetails() + "</label></td>"
                            + "<td><label class=\"label\">" + log.getStatus() + "</label></td>"
                            + "<td><label class=\"label\">" + convertType(log.getClaimNumber()) + "</label></td>"
                            + "<td><label class=\"label\">" + convertDate(log.getAttemptDate()) + "</label></td>"
                            + "<td><label class=\"label\">" + log.getCommunicationType() + "</label></td>");

                    out.println("</tr>");
                }
                out.println("</div>");
            }
            out.println("</table>");
            System.out.println("--ajax--");


            request.setAttribute("displayOption", displayOption);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "MemberCommunicationLogCommand";
    }

    private String convertDate(XMLGregorianCalendar cal) {
        if (cal == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//	        xc.setTimezone(getTimeZoneOffset());
        return sdf.format(cal.toGregorianCalendar().getTimeInMillis());

    }

    private String convertType(int id) {
        if (id < 200) {
            NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
            return port.getValueForId(220, ""+id);
        }
        return "" + id;
    }

    @Override
    public String getName() {
        return "MemberCommunicationLogCommand";
    }
}
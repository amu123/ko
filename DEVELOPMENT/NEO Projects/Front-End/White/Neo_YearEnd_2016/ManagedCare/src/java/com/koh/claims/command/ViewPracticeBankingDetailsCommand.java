/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.util.Collection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.BankingDetails;
import neo.manager.EntityPaymentRunDetails;
import neo.manager.NeoManagerBean;
import neo.manager.PaymentRunSearchCriteria;
import neo.manager.ProviderDetails;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ViewPracticeBankingDetailsCommand extends NeoCommand {

    private Logger log = Logger.getLogger(ViewProviderDetailsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        log.info("Inside ViewPracticeBankingDetailsCommand");

        HttpSession session = request.getSession();

        NeoManagerBean port = service.getNeoManagerBeanPort();

        String providerNumber = "" + session.getAttribute("practiceNumber");

        log.info("The variable providerNumber is " + providerNumber);


        ProviderDetails provDetails = port.getPracticeDetailsForEntityByNumber(providerNumber);

        Collection<BankingDetails> bankDetails = port.getAllBankingDetails(provDetails.getEntityId());

        for (BankingDetails bank : bankDetails) {

            session.setAttribute("pracBankName", bank.getBankName());
            session.setAttribute("pracBranchCode", bank.getBranchCode());
            session.setAttribute("pracBranchName", bank.getBranchName());
            session.setAttribute("pracAccountNumber", bank.getAccountNo());
            session.setAttribute("pracAccountName", bank.getAccountName());
            session.setAttribute("pracAccountType", bank.getAccountType());
        }

        try {
            
            //String nextJSP = "/Claims/ProviderDetailsTabbedPage.jsp";
            String nextJSP = "/Claims/PracticeBankingDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "ViewPracticeBankingDetailsCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CcClaimLineSearchResult;
import neo.manager.Claim;
import neo.manager.ClaimsBatch;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class ViewClaimLineDetails extends NeoCommand {

    private static final Logger logger = Logger.getLogger(ViewClaimLineDetails.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        //logger.info("Inside ViewMemberClaimsDetailsCommand");
        int selectedClaimId = 0;
        String fromCLScreen = request.getParameter("onClaimLineScreen");
        if (fromCLScreen == null || fromCLScreen.equalsIgnoreCase("")) {
            selectedClaimId = Integer.parseInt(request.getParameter("memClaimId"));
            //logger.log(Level.INFO, "The selected claim id ", selectedClaimId);
            System.out.println("ViewClaimLineDetails selected claim id = " + selectedClaimId);
        }
         List<ClaimsBatch> claimList = (List<ClaimsBatch>) session.getAttribute("viewClaimSearchResults");
         boolean foundMemNum = false;
         for(ClaimsBatch claim : claimList){
            for(Claim list_claim : claim.getClaims()){
                if(list_claim.getClaimId() == selectedClaimId){
                    session.setAttribute("memberNumber", list_claim.getCoverNumber());
                    foundMemNum = true;
                    break;
                }
            }
            //break;
            if (foundMemNum){
                break;
            }
         }
         

        String serviceProviderNumber = request.getParameter("memClaimProvNum");

        CcClaimLineSearchResult claimLineResults = new CcClaimLineSearchResult();
        claimLineResults = (CcClaimLineSearchResult) session.getAttribute("memCCClaimLineSearchResult");

        if (claimLineResults != null && claimLineResults.getClaimLines().isEmpty() == false) {

            int claimLineCount = claimLineResults.getClaimLineCount();
            int claimId = claimLineResults.getClaimId();
            int resultMin = claimLineResults.getResultMin();
            int resultMax = claimLineResults.getResultMax();
            String pageDirection = request.getParameter("mcPageDirection");
            if (pageDirection != null && !pageDirection.equalsIgnoreCase("")) {

                //logger.log(Level.INFO, "pageDirection triggered ");
                logger.log(Level.INFO, "pageDirection claimLineCount = ", claimLineCount);
                logger.log(Level.INFO, "pageDirection claimId = ", claimId);
                logger.log(Level.INFO, "pageDirection resultMin = ", resultMin);
                logger.log(Level.INFO, "pageDirection resultMax = ", resultMax);

                if (pageDirection.equalsIgnoreCase("forward")) {
                    resultMin = resultMin + 100;
                    resultMax = resultMax + 100;

                } else if (pageDirection.equalsIgnoreCase("backward")) {
                    resultMin = resultMin - 100;
                    resultMax = resultMax - 100;
                }
                logger.log(Level.INFO, "pageDirection new resultMin = ", resultMin);
                logger.log(Level.INFO, "pageDirection new resultMax = ", resultMax);

                claimLineResults = port.fetchCCClaimLineIdsByClaimId(false, claimId, resultMin, resultMax);

            } else {
                if (selectedClaimId != 0) {
                    claimLineResults = port.fetchCCClaimLineIdsByClaimId(false, selectedClaimId, 0, 100);
                }
                session.setAttribute("memClaimResultProvNum", serviceProviderNumber);
            }
        } else {

            if (selectedClaimId != 0) {
                claimLineResults = port.fetchCCClaimLineIdsByClaimId(false, selectedClaimId, 0, 100);
            }
            session.setAttribute("memClaimResultProvNum", serviceProviderNumber);

        }
        session.setAttribute("memCCClaimLineSearchResult", claimLineResults);
        session.setAttribute("memCCClaimLineDetails", claimLineResults.getClaimLines());

        try {
            String nextJSP = "/NClaims/ClaimHistory.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "ViewClaimLineDetails";
    }

}

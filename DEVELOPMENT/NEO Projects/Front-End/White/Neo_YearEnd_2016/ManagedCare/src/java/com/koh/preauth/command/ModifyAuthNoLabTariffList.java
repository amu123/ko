/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.LabTariffDetails;
import neo.manager.NeoUser;

/**
 *
 * @author johanl
 */
public class ModifyAuthNoLabTariffList extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        PrintWriter out = null;

        String labTAction = request.getParameter("tAction");
        String tariff = request.getParameter("tariff");
        String toothNumber = request.getParameter("toothNum");


        List<LabTariffDetails> noLabTList = (List<LabTariffDetails>) session.getAttribute("DentalNoLabCodeList");
        List<LabTariffDetails> savedNoLabList = (List<LabTariffDetails>) session.getAttribute("SavedDentalNoLabCodeList");
        if (savedNoLabList == null) {
            savedNoLabList = new ArrayList<LabTariffDetails>();
        }
        int refCount = 0;
        if (labTAction.trim().equalsIgnoreCase("save")) {
            for (LabTariffDetails noLabS : savedNoLabList) {
                if (tariff.trim().equalsIgnoreCase(noLabS.getClinicalTariff())) {
                    System.out.println("got lab code");
                    refCount++;
                    break;
                }
            }
            if (refCount == 0) {
                for (LabTariffDetails labT : noLabTList) {
                    if (tariff.trim().equalsIgnoreCase(labT.getClinicalTariff())) {

                        LabTariffDetails savedNoLab = new LabTariffDetails();
                        double amount = 0.0d;
                        savedNoLab.setUserId(user.getUserId());
                        savedNoLab.setLabCodeInd("0");
                        savedNoLab.setProviderType(labT.getProviderType());
                        savedNoLab.setClinicalTariff(labT.getClinicalTariff());
                        savedNoLab.setToothNumber(toothNumber);
                        savedNoLab.setLabCodeAmount(amount);

                        savedNoLabList.add(savedNoLab);

                        break;
                    }
                }
            } else {
                System.out.println("refCount = " + refCount);
            }

        } else if (labTAction.trim().equalsIgnoreCase("remove")) {
            for (LabTariffDetails savedNoLab : savedNoLabList) {
                if (tariff.trim().equalsIgnoreCase(savedNoLab.getClinicalTariff())) {
                        savedNoLabList.remove(savedNoLab);
                        break;
                }
            }
        }
        session.setAttribute("SavedDentalNoLabCodeList", savedNoLabList);

        try {
            out = response.getWriter();
            out.println("Done|");
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            out.close();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ModifyAuthNoLabTariffList";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CoverDetails;
import neo.manager.CoverSearchCriteria;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class MemberSearchByCriteria extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        System.out.println("in MemberSearchByCriteria <<<<<<<<<<<<<");
        HttpSession session = request.getSession();
        String onScreen = request.getParameter("onScreen");

        String coverNumber = request.getParameter("memNo");
        String idNumber = request.getParameter("idNo");
        String initials = request.getParameter("initials");
        String surname = request.getParameter("surname");
        String dob = request.getParameter("dob");

        CoverSearchCriteria cs = new CoverSearchCriteria();

        //cover number set
        if (coverNumber != null && !coverNumber.trim().equals("")
                && !coverNumber.trim().equalsIgnoreCase("null")) {
            cs.setCoverNumber(coverNumber);
        }
        //id number set
        if (idNumber != null && !idNumber.trim().equals("")
                && !idNumber.trim().equalsIgnoreCase("null")) {
            cs.setCoverIDNumber(idNumber);
        }
        //initials set
        if (initials != null && !initials.trim().equals("")
                && !initials.trim().equalsIgnoreCase("null")) {
            cs.setCoverInitial(initials);
        }
        //surname set
        if (surname != null && !surname.trim().equals("")
                && !surname.trim().equalsIgnoreCase("null")) {
            cs.setCoverSurname(surname);
        }
        //date of birth set
        if (dob != null && !dob.trim().equals("")
                && !dob.trim().equalsIgnoreCase("null")) {

            Date cDOB = null;
            XMLGregorianCalendar xDOB = null;
            try {
                cDOB = DateTimeUtils.dateFormat.parse(dob);
                xDOB = DateTimeUtils.convertDateToXMLGregorianCalendar(cDOB);

            } catch (ParseException px) {
                px.printStackTrace();
            }
            cs.setDateOfBirth(xDOB);
        }

        List<CoverDetails> covList = port.getCoverPrincipleList(cs);
        List<CoverDetails> cov2List = new ArrayList<CoverDetails>();
        boolean viewStaff = (Boolean) session.getAttribute("persist_user_viewStaff");

        if (covList.isEmpty()) {
            request.setAttribute("coverSearchResultsMessage", "No results found.");
        } else if (covList.size() >= 100) {
            request.setAttribute("coverSearchResultsMessage", "Only first 100 results shown. Please refine search.");
        } else {
            request.setAttribute("coverSearchResultsMessage", null);
        }
        Map<String, Integer> userProductRest = (Map<String, Integer>) session.getAttribute("persist_productViewRestriction");
        Boolean skipMember = false;
        if (!covList.isEmpty()) {
            System.out.println("member query size = " + covList.size());
            for (CoverDetails cov : covList) {
                skipMember = false;
                if (!viewStaff) {
                    if (cov.getMemberCategory() == 2) {
                        continue;
                    }
                }
                for (Map.Entry<String, Integer> entry : userProductRest.entrySet()) {
                    if (cov.getProductName().equals("Resolution Health")) {
                        if (entry.getValue() == 0 && entry.getKey().equals("allowReso")) {
                            request.setAttribute("coverSearchResultsMessage", "Result list excludes Resolution Health members due to user restrictions");
                            skipMember = true;
                            continue;
                        }
                    }
                    if (cov.getProductName().equals("Spectramed")) {
                        if (entry.getValue() == 0 && entry.getKey().equals("allowSpec")) {
                            request.setAttribute("coverSearchResultsMessage", "Result list excludes Spectramed members due to user restrictions");
                            skipMember = true;
                            continue;
                        }
                    }
                }
                if (skipMember) {
                    continue;
                }
                cov2List.add(cov);
            }
            if (!viewStaff) {
                if (covList.size() > 0) {
                    if (cov2List.isEmpty()) {
                        request.setAttribute("coverSearchResultsMessage", "User access denied. Please contact your supervisor ");
                    }
                }
            }

            session.setAttribute("MemberCoverDetails", cov2List);
        }

        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(onScreen);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "MemberSearchByCriteria";
    }
}

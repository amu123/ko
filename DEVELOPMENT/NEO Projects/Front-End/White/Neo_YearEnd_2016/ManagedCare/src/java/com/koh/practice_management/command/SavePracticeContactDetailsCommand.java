/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.practice_management.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.cover.MemberMainMapping;
import com.koh.employer.command.TabUtils;
import com.koh.fe.command.LoginCommand;
import com.koh.practice.PracticeManagementMainMapping;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.ContactDetails;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.ProviderDetails;
import neo.manager.Security;

/**
 *
 * @author almaries
 */
public class SavePracticeContactDetailsCommand extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String result = validateAndSave(request);
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
            //response.setHeader("Refresh", "1; URL=/ManagedCare/PracticeManagement/ContactDetails.jsp");
        } catch (IOException ex) {
            ex.printStackTrace();
        }       
        return null;
    }

    @Override
    public String getName() {
        return "SavePracticeContactDetailsCommand";
    }
    
        private String validateAndSave(HttpServletRequest request) {
        Map<String, String> errors = PracticeManagementMainMapping.validateContact(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (save(request)) {
                additionalData = "\"message\":\"Practice Contact Details Updated\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error updating the practice contact details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
    }

    private boolean save(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);
        
        int entityId = PracticeManagementMainMapping.getEntityId(request);
        
        try {
            List<ContactDetails> adList = PracticeManagementMainMapping.getContactDetails(request, neoUser, entityId);
            port.saveContactListDetails(adList, sec, entityId);
            
            String practiceNumber = ""+request.getSession().getAttribute("provNum");
            System.out.println("Practice Number: "+practiceNumber);
            ProviderDetails list = port.getProviderDetailsForEntityByNumber(practiceNumber);

            if (list != null) {
                int provEntityId = list.getEntityId();
                port.saveContactListDetails(adList, sec, provEntityId);
            }
            
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthHistoryNotes;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Johan-NB
 */
public class SearchAuthHistoryNotes extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        boolean memberSet = false;
        boolean memberValid = false;
        boolean getMethod = false;

        String memNo = request.getParameter("noteMemberNum");
        String depNum = request.getParameter("noteDepListValues");
        String crDate = request.getParameter("crDate");
        int depCode = 0;
        Date creationDate = null;
        XMLGregorianCalendar xCreation = null;

        if (memNo != null && !memNo.equalsIgnoreCase("")) {
            memberSet = true;
            memberValid = validateNum(memNo, "main");
        }

        if (depNum == null || depNum.equalsIgnoreCase("null")
                || depNum.equalsIgnoreCase("")) {
            depCode = -1;
        } else {
            memberValid = validateNum(depNum, "dep");
            depCode = Integer.parseInt(depNum);
        }

        if (crDate != null && !crDate.equalsIgnoreCase("")) {
            try {
                creationDate = sdf.parse(crDate);
                xCreation = DateTimeUtils.convertDateToXMLGregorianCalendar(creationDate);
            } catch (ParseException ex) {
                Logger.getLogger(SearchAuthHistoryNotes.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (memberSet == true && memberValid == true) {
            getMethod = true;
        } else if (memberSet == false) {
            getMethod = true;
        }

        if (getMethod == true) {
            List<AuthHistoryNotes> notes = port.getAuthHistoryNotesByCriteria(memNo, depCode, xCreation);
            if (notes.size() == 0) {
                request.setAttribute("authHNResultsMessage", "No results found.");
            } else if (notes.size() >= 100) {
                request.setAttribute("authHNResultsMessage", "Only first 100 results shown. Please refine search.");
            } else {
                request.setAttribute("authHNResultsMessage", null);
            }
            session.setAttribute("authHistoryNotes", notes);
        }

        try {
            String nextJSP = "/PreAuth/AuthHistoryNotes.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;

    }

    @Override
    public String getName() {
        return "SearchAuthHistoryNotes";
    }

    private boolean validateNum(String num, String type) {
        boolean valid = true;
        String validChars = "0123456789";
        char strChar;

        if (type.equalsIgnoreCase("dep")) {
            if (num.length() > 2) {
                valid = false;
            }
        }

        for (int i = 0; i < num.length(); i++) {
            strChar = num.charAt(i);
            if (validChars.indexOf(strChar) == -1) {
                valid = false;
            }
        }
        return valid;
    }
}

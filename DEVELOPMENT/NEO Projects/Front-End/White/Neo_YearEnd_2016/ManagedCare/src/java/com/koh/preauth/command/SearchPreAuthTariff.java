/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthBasketTariffSearch;
import neo.manager.AuthTariffDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class SearchPreAuthTariff extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        this.saveScreenToSession(request);
        NeoManagerBean port = service.getNeoManagerBeanPort();
        List<AuthTariffDetails> btList = null;
        List<AuthTariffDetails> basketSpecList = null;

        //mandatory fields
        int authSub = Integer.parseInt("" + session.getAttribute("authSub"));
        int productId = Integer.parseInt("" + session.getAttribute("scheme"));
        int optionId = Integer.parseInt("" + session.getAttribute("schemeOption"));
        String pIcd = "" + session.getAttribute("primaryICD_text");
        XMLGregorianCalendar authStartDate = null;
        try {
            authStartDate = DateTimeUtils.convertDateToXMLGregorianCalendar(new SimpleDateFormat("yyyy/MM/dd").parse("" + session.getAttribute("authFromDate")));
        } catch (ParseException ex) {
            Logger.getLogger(SearchPreAuthTariff.class.getName()).log(Level.SEVERE, null, ex);
        }
        String providerNo = "" + session.getAttribute("treatingProvider_text");

        //search fiels
        String provDesc = (String) session.getAttribute("provDesc");
        String code = (String) session.getAttribute("code");
        String desc = (String) session.getAttribute("description");

        String basket = (String) session.getAttribute("basketSearch");

        AuthBasketTariffSearch bts = new AuthBasketTariffSearch();
        bts.setProductId(productId);
        bts.setOptionId(optionId);

        if (code != null && !code.trim().equals("")) {
            bts.setCode(code);
        }
        if (desc != null && !desc.trim().equalsIgnoreCase("")) {
            bts.setCodeDesc(desc);
        }
        if (provDesc != null && !provDesc.trim().equals("")) {
            bts.setProviderDiscipline(provDesc);
        }

        if (basket != null && !basket.trim().isEmpty()) {

            bts.setBasketType(authSub);
            bts.setPrimaryICD(pIcd);

            btList = port.getTreatmentFromAuthBasket(bts);
            System.out.println("In getTreatmentFromAuthBasket " + basket);

        } else {


            bts.setAuthStartDate(authStartDate);
            bts.setProviderNo(providerNo);
            btList = port.getTreatmentForAuthBasket(bts);

            //match main tariffs with basket specific
            bts.setCode("");
            bts.setProviderDiscipline("");
            bts.setBasketType(authSub);
            bts.setPrimaryICD(pIcd);

            basketSpecList = port.getTreatmentFromAuthBasket(bts);
            if(basketSpecList != null){
                System.out.println("basketSpecList size = "+basketSpecList.size());
                //match search result with basket specific tariffs
                for (AuthTariffDetails at : btList) {
                    for (AuthTariffDetails st : basketSpecList) {
                        if(at.getTariffCode().equals(st.getTariffCode())){
                            btList.remove(at);
                        }
                    }
                }

            }else{
                System.out.println("basketSpecList null");
            }
            System.out.println("In getTreatmentForAuthBasket " + basket);
        }
        //List<AuthTariffDetails> basketList = new ArrayList<AuthTariffDetails>();
        if (btList != null) {
            System.out.println("btList size = " + btList.size());
//            for (AuthTariffDetails at : btList) {
//                AuthTariffDetails ab = new AuthTariffDetails();
//
//                at.setProviderType(provDesc);
//                ab.setTariffCode(at.getTariffCode());
//                ab.setTariffDesc(at.getTariffDesc());
//                ab.setAmount(at.getAmount());
//                ab.setFrequency(at.getFrequency());
//
//                basketList.add(ab);
//            }
            session.setAttribute("PreAuthBTSearchList", btList);
        }
        try {
            String nextJSP = null;

            if (basket != null && !basket.trim().isEmpty()) {
                nextJSP = "/PreAuth/PreAuthSearchTariffBasket.jsp";
            } else {
                nextJSP = "/PreAuth/PreAuthTariffSearch.jsp";
            }
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SearchPreAuthTariff";
    }
}

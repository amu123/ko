/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.Command;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.GenericTariff;

/**
 *
 * @author josephm
 */
public class RemoveBiometricsTariffCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();

         PrintWriter out = null;
        System.out.println("inside RemoveAuthTariffCommand");
        ArrayList<GenericTariff> atList = (ArrayList<GenericTariff>) session.getAttribute("tariffListArray");
        try {
            if (atList != null && atList.size() != 0) {
                String tCode = request.getParameter("tTableCode");
                String tQuan = request.getParameter("tTableQuan");
                for (GenericTariff at : atList) {
                    if (tCode.equals(at.getTariffCode())) {
                            session.setAttribute("tariffDescription", at.getTariffDescription());
                            session.setAttribute("tariffCode", at.getTariffCode());
                            session.setAttribute("testResult", at.getTestResult());
                            out.print(getName() + "|" + at.getTariffCode() + "|" + at.getTariffDescription() +
                                    "|" + at.getTestResult());
                            atList.remove(at);
                            break;
                    }
                }
                session.setAttribute("tariffListArray", atList);
            }

            String nextJSP = "/biometrics/";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        }catch(Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "RemoveBiometricsTariffCommand";
    }
}

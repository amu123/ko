/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

/**
 *
 * @author Princes
 *
 */
import com.koh.command.NeoCommand;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.Claim;
import neo.manager.ClaimCCSearchCriteria;
import neo.manager.ClaimsBatch;
import neo.manager.NeoManagerBean;
import neo.manager.ProviderDetails;

/**
 *
 * @author Johan-NB
 */
public class ViewMemberClaimSearchResultPDC extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String sessionAttribute = "";

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        HttpSession session = pageContext.getSession();
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        //System.out.println("entered ViewMemberClaimSearchResult");
        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");

            String message = (String) req.getAttribute("memberClaimSearchResultsMessage");
            if (message != null && !message.equalsIgnoreCase("")) {
                out.println("<label class=\"red\">" + message + "</label></br></br>");
            }

            //validate session for batch list
            //System.out.println("ViewMemberClaimSearchResult sessionAttribute = " + sessionAttribute);
            List<ClaimsBatch> claimList = null;
            if (sessionAttribute != null && !sessionAttribute.equalsIgnoreCase("")) {
                claimList = (List<ClaimsBatch>) session.getAttribute(sessionAttribute);
            }

            if (claimList == null) {
                System.out.println("claimList is null");
            } else {
                System.out.println("claimList is not null : size = " + claimList.size());
            }

            if (claimList != null && claimList.isEmpty() == false) {

                //boolean addForwardNav = false;
                //boolean addBackwardNav = false;
                int claimCount = Integer.parseInt(session.getAttribute("memCCClaimSearchResultCount").toString());
                ClaimCCSearchCriteria csCri = (ClaimCCSearchCriteria) session.getAttribute("memCCClaimSearch");

                System.out.println("claimCount = " + claimCount);
                System.out.println("csCri.getResultCountMin() = " + csCri.getResultCountMin());
                System.out.println("csCri.getResultCountMax() = " + csCri.getResultCountMax());

                /*if (claimCount == 100 && csCri.getResultCountMin() == 0) {
                addForwardNav = true;

                } else if (claimCount == 100 && csCri.getResultCountMin() > 0) {
                addForwardNav = true;
                addBackwardNav = true;

                } else if (claimCount < 100 && csCri.getResultCountMin() > 0) {
                addBackwardNav = true;
                }*/
                out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.print("<tr>");
                out.print("<th align=\"left\">Claim Type</th>");
                out.print("<th align=\"left\">Claim Number</th>");
                out.print("<th align=\"left\">Account Number</th>");
                out.print("<th align=\"left\">Practice Number</th>");
                out.print("<th align=\"left\">Practice Name</th>");
                out.print("<th align=\"left\">Service Provider</th>");
                out.print("<th align=\"left\">Service Provider Name</th>");
                out.print("<th align=\"left\">Discipline Type</th>");
                out.print("<th align=\"left\">Member Number</th>");
                out.print("<th align=\"left\">Treatment From</th>");
                out.print("<th align=\"left\">Treatment To</th>");
                out.print("<th align=\"left\">Total Claimed</th>");
                out.print("<th align=\"left\">Total Paid</th>");
                out.print("<th align=\"left\">Action</th>");
                out.print("</tr>");

                for (ClaimsBatch claim : claimList) {

                    String claimType;
                    if (claim.getEdiId() == null || claim.getEdiId().equalsIgnoreCase("")) {
                        claimType = "Paper";
                    } else {
                        claimType = "EDI";
                    }

                    for (Claim list_claim : claim.getClaims()) {
                        String catagory = port.getValueForId(59, list_claim.getClaimCategoryTypeId() + "");
                        ProviderDetails practice = port.getPracticeDetailsForEntityByNumber(list_claim.getPracticeNo());

                        out.print("<tr>");
                        out.print("<td><label class=\"label\">" + claimType + "</label></td>");
                        out.print("<td><label class=\"label\">" + list_claim.getClaimId() + "</label></td>");
                        if (list_claim.getPatientRefNo() != null) {
                            out.print("<td><label class=\"label\">" + list_claim.getPatientRefNo() + "</label></td>");
                        } else {
                            out.print("<td></td>");
                        }
                        out.print("<td><label class=\"label\">" + list_claim.getPracticeNo() + "</label></td>");
                        out.print("<td><label class=\"label\">" + practice.getPracticeName() + "</label></td>");
                        out.print("<td><label class=\"label\">" + list_claim.getServiceProviderNo() + "</label></td>");
                        out.print("<td><label class=\"label\">" + practice.getPracticeName() + "</label></td>");
                        out.print("<td><label class=\"label\">" + catagory + "</label></td>");
                        out.print("<td><label class=\"label\">" + list_claim.getCoverNumber() + "</label></td>");
                        out.print("<td><label class=\"label\">" + dateFormat.format(list_claim.getServiceDateFrom().toGregorianCalendar().getTime()) + "</label></td>");
                        out.print("<td><label class=\"label\">" + dateFormat.format(list_claim.getServiceDateTo().toGregorianCalendar().getTime()) + "</label></td>");

                        //set totals
                        String totalAmountClaimed = "";
                        String totalAmountPaid = "";

                        if (list_claim.getClaimCalculatedTotal() != 0.0d) {
                            totalAmountClaimed = new DecimalFormat("#.##").format(list_claim.getClaimCalculatedTotal());
                        }
                        if (list_claim.getClaimCalculatedPaidTotal() != 0.0d) {
                            totalAmountPaid = new DecimalFormat("#.##").format(list_claim.getClaimCalculatedPaidTotal());
                        }

                        out.print("<td><label class=\"label\">" + totalAmountClaimed + "</label></td>");
                        out.print("<td><label class=\"label\">" + totalAmountPaid + "</label></td>");

                        out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('ViewMemberClaimLineDetailsPDC', '" + list_claim.getClaimId() + "','" + list_claim.getServiceProviderNo() + "')\"; value=\"\">Details</button></td>");
                        //out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('ViewMemberClaimsDetailsCommand', " + count + ")\"; value=\"" + commandName + "\">Details</button></td>");
                        out.print("</tr>");

                    }
                }

                /*if (addBackwardNav || addForwardNav) {
                out.println("<tr><td colspan=\"14\" align=\"right\" >");

                if (addForwardNav) {
                out.println("<button name=\"opperation\" type=\"button\" onClick=\"submitPageAction('GetMemberClaimClaimLineDetailsCommand', 'forward')\"; value=\"\">Next</button>");
                }
                if (addBackwardNav) {
                out.println("<button name=\"opperation\" type=\"button\" onClick=\"submitPageAction('GetMemberClaimClaimLineDetailsCommand', 'backward')\"; value=\"\">Previous</button>");
                }

                out.println("</td></tr>");
                }*/
            }
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        } catch (NumberFormatException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }
}

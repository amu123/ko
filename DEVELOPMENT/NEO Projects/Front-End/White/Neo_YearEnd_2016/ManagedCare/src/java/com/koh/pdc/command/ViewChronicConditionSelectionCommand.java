/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CarePathLookup;
import neo.manager.NeoManagerBean;

/**
 *
 * @author nick
 */
public class ViewChronicConditionSelectionCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("entered ViewChronicConditionSelectionCommand");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        //get all active carepath options from database and set in session
        List<CarePathLookup> cpl = port.getAllAvailableCarePaths();
        session.setAttribute("cdlCarepathListPDC", cpl);
        
        try {
            String nextJSP = "/PDC/SelectNewChronicCondition.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewChronicConditionSelectionCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import com.koh.command.NeoCommand;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthCPTDetails;
import neo.manager.AuthHospitalLOC;
import neo.manager.PreAuthConfirmationDetails;
import neo.manager.PreAuthDetails;

/**
 *
 * @author princes
 */
public class AuthorizationHospitalFirstTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        Format formatter = new SimpleDateFormat("yyyy/MM/dd");

        String admissionDate;
        Collection<PreAuthDetails> details = (Collection<PreAuthDetails>) session.getAttribute("authDetails");
        List<PreAuthDetails> hospital = new ArrayList<PreAuthDetails>();
        try {

            out.print("<tr>");
            out.print("<th>Auth Number</th>");
            out.print("<th>Provider Number</th>");
            out.print("<th>Provider Name</th>");
            out.print("<th>ICD10 Code</th>");
            out.print("<th>CPT</th>");
            out.print("<th>Admission Date</th>");
            out.print("<th>Length Of Stay</th>");
            out.print("<th>Action</th>");
            out.print("</tr>");
            int count = 0;
            if (details != null && details.size() > 0) {
                for (PreAuthDetails list : details) {

                    if (list.getAuthType().equals("4")) {
                        String cpts = "";
                        if (list.getAuthCPTDetails() != null) {
                            for (AuthCPTDetails cptList : list.getAuthCPTDetails()) {
                                if (cpts.length() > 0) {
                                    cpts = cpts + ", " + cptList.getCptCode();
                                } else {
                                    cpts = cptList.getCptCode();
                                }
                            }
                        }

                        double lengthOfStay = 0.0d;
                        if (list.getHospitalLOCDetails() != null) {
                            for (AuthHospitalLOC locList : list.getHospitalLOCDetails()) {
                                lengthOfStay += locList.getLengthOfStay();
                            }
                        }
                        out.print("<tr>");
                        if (list.getAuthNumber() != null) {
                            out.print("<td><label class=\"label\">" + list.getAuthNumber() + "</label></td>");
                        } else {
                            out.print("<td>&nbsp;</td>");
                        }
                        if (list.getTreatingProviderNo() != null) {
                            out.print("<td><label class=\"label\">" + list.getTreatingProviderNo() + "</label></td>");
                            PreAuthConfirmationDetails preAuth = NeoCommand.service.getNeoManagerBeanPort().getAuthConfirmationDetails(list.getAuthDetailId(), list.getAuthNumber());
                            out.print("<td><label class=\"label\">" + preAuth.getProviderName() + "</label></td>");
                        } else {
                            out.print("<td>&nbsp;</td>");
                            out.print("<td>&nbsp;</td>");
                        }
                        if (list.getPrimaryICD() != null) {
                            out.print("<td><label class=\"label\">" + list.getPrimaryICD() + "</label></td>");
                        } else {
                            out.print("<td>&nbsp;</td>");
                        }
                        out.print("<td><label class=\"label\">" + cpts + "</label></td>");
                        Date authdate = list.getAuthStartDate().toGregorianCalendar().getTime();
                        admissionDate = formatter.format(authdate);
                        out.print("<td><label class=\"label\">" + admissionDate + "</label></td>");
                        out.print("<td><label class=\"label\">" + lengthOfStay + "</label></td>");
                        out.println("<td>"
                                + "<button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('GetAuthConfirmationDetailsForPDC','" + list.getAuthNumber() + "', " + list.getAuthDetailId() + ")\"; value=\"GetAuthConfirmationDetailsForPDC\">Select</button>"
                                + "</td>");
                        out.print("</tr>");
                        count++;
                        hospital.add(list);
                    }
                }
                session.setAttribute("hospitalList", hospital);
            }

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }
}

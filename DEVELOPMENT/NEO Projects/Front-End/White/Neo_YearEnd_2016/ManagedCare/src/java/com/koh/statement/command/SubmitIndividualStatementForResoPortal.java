/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import com.agile.security.webservice.EmailAttachment;
import com.agile.security.webservice.EmailContent;
import com.koh.command.FECommand;
import static com.koh.command.FECommand.service;
import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author josephm
 */
public class SubmitIndividualStatementForResoPortal extends FECommand {

    @Override
    @SuppressWarnings("static-access")
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

        String actionType = request.getParameter("opperationParam");
        String memberNo = request.getParameter("memberNo_text");
        String providerNo = request.getParameter("providerNo_text");
        String fromDateString = request.getParameter("statementFromDate");
        String toDateString = request.getParameter("statementToDate");
        String paymentDateString = request.getParameter("paymentRunDate");
        String paymentNextDateString = null;
        String emailTo = request.getParameter("emailAddress");

        if (paymentDateString != null) {
            paymentNextDateString = DateTimeUtils.addDays(paymentDateString, 1);
        }
        String urlString = "";
        boolean paymentPresent = false;
        boolean emailSent = false;


        if (memberNo != null && memberNo.length() > 0) {
            if (paymentDateString != null) {
                //Build URL string
                String urlMemberPart1 = "http://yarona:7778/reports/rwservlet?server=rep_yarona_YAR_iASMiddle&report=/opt/oracle/iASMiddle/NEO/reports/MEMBER_STATEMENT.rdf";
                String urlMemberPart2 = "&P_START_DATE=" + paymentDateString;
                String urlMemberPart3 = "&P_END_DATE=" + paymentNextDateString;
                String urlMemberPart4 = "&P_MEMBER_NO=" + memberNo;
                String urlMemberPart5 = "&userid=neostmnt/neostmnt@neoprod&desformat=pdf&destype=cache";


                urlString = urlMemberPart1 + urlMemberPart2 + urlMemberPart3 + urlMemberPart4 + urlMemberPart5;
                System.out.println(urlMemberPart1 + urlMemberPart2 + urlMemberPart3 + urlMemberPart4);


                //Check if payment run is present for given details
                Date fromDate = DateTimeUtils.convertFromYYYYMMDD(paymentDateString);
                Date toDate = DateTimeUtils.convertFromYYYYMMDD(paymentNextDateString);
                XMLGregorianCalendar xmlFromDate = DateTimeUtils.convertDateToXMLGregorianCalendar(fromDate);
                XMLGregorianCalendar xmlToDate = DateTimeUtils.convertDateToXMLGregorianCalendar(toDate);
                paymentPresent = port.findPaymentsForMember(xmlFromDate, xmlToDate, memberNo);
            } else {
                //Build URL string
                String urlMemberPart1 = "http://yarona:7778/reports/rwservlet?server=rep_yarona_YAR_iASMiddle&report=/opt/oracle/iASMiddle/NEO/reports/MEMBER_STATEMENT.rdf";
                String urlMemberPart2 = "&P_START_DATE=" + fromDateString;
                String urlMemberPart3 = "&P_END_DATE=" + toDateString;
                String urlMemberPart4 = "&P_MEMBER_NO=" + memberNo;
                String urlMemberPart5 = "&userid=neostmnt/neostmnt@neoprod&desformat=pdf&destype=cache";

                urlString = urlMemberPart1 + urlMemberPart2 + urlMemberPart3 + urlMemberPart4 + urlMemberPart5;
                System.out.println(urlMemberPart1 + urlMemberPart2 + urlMemberPart3 + urlMemberPart4);

                //Check if payment run is present for given details
                Date fromDate = DateTimeUtils.convertFromYYYYMMDD(fromDateString);
                Date toDate = DateTimeUtils.convertFromYYYYMMDD(toDateString);
                XMLGregorianCalendar xmlFromDate = DateTimeUtils.convertDateToXMLGregorianCalendar(fromDate);
                XMLGregorianCalendar xmlToDate = DateTimeUtils.convertDateToXMLGregorianCalendar(toDate);
                paymentPresent = port.findPaymentsForMember(xmlFromDate, xmlToDate, memberNo);
            }

        } else if (providerNo != null && providerNo.length() > 0) {
            if (paymentDateString != null) {
                //Build URL string
                String urlProviderPart1 = "http://yarona:7778/reports/rwservlet?server=rep_yarona_YAR_iASMiddle&report=/opt/oracle/iASMiddle/NEO/reports/PROVIDER_STATEMENT.rdf";
                String urlProviderPart2 = "&P_START_DATE=" + paymentDateString;
                String urlProviderPart3 = "&P_END_DATE=" + paymentNextDateString;
                String urlProviderPart4 = "&P_PRACTICE_NO=" + providerNo;
                String urlProviderPart5 = "&userid=neostmnt/neostmnt@neoprod&desformat=pdf&destype=cache";

                urlString = urlProviderPart1 + urlProviderPart2 + urlProviderPart3 + urlProviderPart4 + urlProviderPart5;
                System.out.println(urlProviderPart1 + urlProviderPart2 + urlProviderPart3 + urlProviderPart4);

                //Check if payment run is present for given details
                Date fromDate = DateTimeUtils.convertFromYYYYMMDD(paymentDateString);
                Date toDate = DateTimeUtils.convertFromYYYYMMDD(paymentNextDateString);
                XMLGregorianCalendar xmlFromDate = DateTimeUtils.convertDateToXMLGregorianCalendar(fromDate);
                XMLGregorianCalendar xmlToDate = DateTimeUtils.convertDateToXMLGregorianCalendar(toDate);
                LookupValue entityType = new LookupValue();
                entityType.setValue("Practice");
                paymentPresent = port.findPaymentRunForEntity(xmlFromDate, xmlToDate, entityType, providerNo);
            } else {
                //Build URL string
                String urlProviderPart1 = "http://yarona:7778/reports/rwservlet?server=rep_yarona_YAR_iASMiddle&report=/opt/oracle/iASMiddle/NEO/reports/PROVIDER_STATEMENT.rdf";
                String urlProviderPart2 = "&P_START_DATE=" + fromDateString;
                String urlProviderPart3 = "&P_END_DATE=" + toDateString;
                String urlProviderPart4 = "&P_PRACTICE_NO=" + providerNo;
                String urlProviderPart5 = "&userid=neostmnt/neostmnt@neoprod&desformat=pdf&destype=cache";

                urlString = urlProviderPart1 + urlProviderPart2 + urlProviderPart3 + urlProviderPart4 + urlProviderPart5;
                System.out.println(urlProviderPart1 + urlProviderPart2 + urlProviderPart3 + urlProviderPart4);

                //Check if payment run is present for given details
                Date fromDate = DateTimeUtils.convertFromYYYYMMDD(fromDateString);
                Date toDate = DateTimeUtils.convertFromYYYYMMDD(toDateString);
                XMLGregorianCalendar xmlFromDate = DateTimeUtils.convertDateToXMLGregorianCalendar(fromDate);
                XMLGregorianCalendar xmlToDate = DateTimeUtils.convertDateToXMLGregorianCalendar(toDate);
                LookupValue entityType = new LookupValue();
                entityType.setValue("Practice");
                paymentPresent = port.findPaymentRunForEntity(xmlFromDate, xmlToDate, entityType, providerNo);
            }
        }
        /**
         * ***************** Put the logic to do an automatic login
         * ***************************
         */
        try {
            NeoUser user = port.authenticateUser("administrator", "admin");
            session.setAttribute("persist_user", user);
            session.setAttribute("memberNum", memberNo);
            session.setAttribute("providerNo", providerNo);
            session.setAttribute("statementFromDate", fromDateString);
            session.setAttribute("statementToDate", toDateString);
            session.setAttribute("paymentRunDate", paymentDateString);
            session.setAttribute("emailAddress", emailTo);

            System.out.println("*******************Printing the values********************************* ");
            System.out.println("The member number is " + memberNo);
            System.out.println("The provider number is " + providerNo);
            System.out.println("The statement from date is " + fromDateString);
            System.out.println("The statement to date is " + toDateString);
            System.out.println("The payment run date " + paymentDateString);
            System.out.println("The email address " + emailTo);

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        if (paymentPresent) {
            //Generate statemtent
            try {
                URL url = new URL(urlString);
                URLConnection connection = url.openConnection();
                // Since you get a URLConnection, use it to get the InputStream
                InputStream in = connection.getInputStream();
                // Now that the InputStream is open, get the content length
                int contentLength = connection.getContentLength();
                System.out.println("Length : " + contentLength);
                System.out.println("The url string is " + urlString);

                // To avoid having to resize the array over and over and over as
                // bytes are written to the array, provide an accurate estimate of
                // the ultimate size of the byte array
                ByteArrayOutputStream tmpOut;

                tmpOut = new ByteArrayOutputStream(contentLength);


                byte[] buf = new byte[512];
                while (true) {
                    int len = in.read(buf);
                    if (len == -1) {
                        break;
                    }
                    tmpOut.write(buf, 0, len);
                }
                in.close();
                tmpOut.close(); // No effect, but good to do anyway to keep the metaphor alive

                byte[] array = tmpOut.toByteArray();

                //Lines below used to test if file is corrupt
                //FileOutputStream fos = new FileOutputStream("C:\\Temp\\report.pdf");
                //fos.write(array);
                //fos.close();

                //Create file name
                String fileName = null;
                if (paymentDateString != null) {
                    fileName = "Statement_" + paymentDateString + ".pdf";
                } else {
                    fileName = "Statement_" + fromDateString + "-" + toDateString + ".pdf";
                }

                if (actionType != null && actionType.equalsIgnoreCase("Preview")) {
                    printPreview(request, response, array, fileName);

                } else if (emailTo != null && emailTo.length() > 0) {
                    //Mail file as attachment
                    String emailFrom = "Noreply@agilityghs.co.za";
                    String subject = "Resolution Health Statement";
                    String emailMsg = null;

                    EmailContent content = new EmailContent();
                    content.setContentType("text/plain");
                    EmailAttachment attachment = new EmailAttachment();
                    attachment.setContentType("application/octet-stream");
                    attachment.setName(fileName);
                    content.setFirstAttachment(attachment);
                    content.setFirstAttachmentData(array);
                    content.setSubject(subject);
                    content.setEmailAddressFrom(emailFrom);
                    content.setEmailAddressTo(emailTo);
                    content.setProductId(1);
                    //Determine msg for email according to type of document ('claim', 'statement', 'preAuth', 'benefit' 'custom', 'other')
                    content.setType("statement");
                    /*If document type is custom, this message added here will be displayed in the email. The email method will automatically add the 
                     the enquiries and call center aswell as the kind regards at the end of the message*/
                    content.setContent(emailMsg);
                    emailSent = service.getAgileManagerPort().sendEmailWithOrWithoutAttachment(content);
                    System.out.println("Email sent : " + emailSent);
                    printEmailResult(request, response, emailSent);
                } else {
                    printEmailResult(request, response, emailSent);
                }
            } catch (MalformedURLException e) {
                System.out.println("MalformedURLException.");
                e.printStackTrace();
            } catch (IOException ex) {
                System.out.println("IOException.");
                ex.printStackTrace();
            }
        } else {
            printResult(request, response, paymentPresent);
        }

        return null;
    }

    public void printPreview(HttpServletRequest request, HttpServletResponse response, byte[] array, String fileName) {
        OutputStream outStream = null;
        try {

            response.setHeader("Pragma", "no-cache"); //HTTP 1.0
            response.setDateHeader("Expires", 0); //prevents caching at the proxy server
            response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-disposition", "attachment; filename=" + fileName);

            response.setContentType("application/pdf");
            response.setContentLength(array.length);
            outStream = response.getOutputStream();
            outStream.write(array);
            outStream.flush();
            outStream.close();

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void printResult(HttpServletRequest request, HttpServletResponse response, boolean result) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (result) {
                out.println("<td><label class=\"header\">Statement emailed.</label></td>");
            } else {
                out.println("<td><label class=\"header\">No payment(s) found.</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    public void printEmailResult(HttpServletRequest request, HttpServletResponse response, boolean result) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (result) {
                out.println("<td><label class=\"header\">Statement emailed.</label></td>");
            } else {
                out.println("<td><label class=\"header\">Email sending failed.</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    @Override
    public String getName() {

        return "SubmitIndividualStatementForResoPortal";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import agility.za.documentenginetype.IndexForMemberFilter;
import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import agility.za.indexdocumenttype.IndexForMember;
import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.serv.PropertiesReader;
import com.koh.utils.DateTimeUtils;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author janf
 */
public class ForwardToPreAuthDocumentCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("inForwardToPreAuthDocumentCommand");
        HttpSession session = request.getSession();
        this.saveScreenToSession(request);
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Map map = request.getParameterMap();
        System.out.println("GETTING PARAMETER MAP");
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        System.out.println("END OF PARAMETER MAP");
        String authNumber = session.getAttribute("authNumber") + "";
        String aId = session.getAttribute("authCode") + "";
        String returnFlag = session.getAttribute("returnFlag") + "";

        int authId = 0;
        if (aId != null && !aId.trim().equalsIgnoreCase("")) {
            try {
                authId = new Integer(aId.trim());
            } catch (NumberFormatException nf) {
                System.out.println("no auth id found");
            }
        }

        request.setAttribute("authNumber", authNumber);
        System.out.println("authNumber = " + authNumber);
        System.out.println("authId = " + authId);
        System.out.println("returnFlag = " + returnFlag);

        session.setAttribute("authNumber", authNumber);
        session.setAttribute("authCode", authId);
        session.setAttribute("returnFlag", returnFlag);

        try {

            String docKind = request.getParameter("docKind");
            String docType = request.getParameter("docType");

            if (docType == null) {
                docType = "34";
            }
            System.out.print("docType = " + docType);
            if (docKind == null) {
                docKind = "212";
            }
            System.out.print("docKind = " + docKind);

            session.setAttribute("withEmail", docType);
            request.setAttribute("folderList", new PropertiesReader().getProperty("DocumentIndexWorkFolder"));
            request.setAttribute("listOrWork", "Work");
            request.setAttribute("docKind", docKind);
            request.setAttribute("memberDocType", docType);
            //String memberNumber = request.getParameter("memberNumber");
            //String memberNumber = (String) session.getAttribute("memberNumber");
            System.out.println("memberNumber = " + authNumber);
            if (authNumber != null && !authNumber.trim().equals("")) {
                IndexDocumentRequest req = new IndexDocumentRequest();
                NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
                req.setAgent(loggedInUser);
                req.setIndexType("IndexForMemberByFilter");
                req.setEntityNumber(authNumber);
                req.setSrcDrive(new PropertiesReader().getProperty("DocumentIndexIndexDrive"));
                IndexForMemberFilter filter = new IndexForMemberFilter();
                filter.setDocKind(docKind);
                filter.setDocType(docType);

                req.setFilter(filter);
                IndexDocumentResponse resp = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
                for (IndexForMember index : resp.getIndexForMember()) {
                    String typeName = index.getDocType();
                    Pattern p = Pattern.compile("([0-9]*)");
                    Matcher m = p.matcher(typeName);
                    if (m.matches()) {
                        typeName = port.getValueForId(212, typeName);
                        index.setDocType(typeName);
                    }
                }

                request.setAttribute("indexList", resp.getIndexForMember());
                session.setAttribute("indexResponse", resp);

            } else {
                System.out.println("Auth Number is null not getting documents");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        String page = "/PreAuth/PreAuthDocuments.jsp";
        try {
            context.getRequestDispatcher(page).forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ForwardToPreAuthDocumentCommand";
    }

}

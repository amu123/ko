/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.tags;

import com.agile.security.webservice.EAuthSearchResult;
import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author whauger
 */
public class AuthSearchResultTable extends TagSupport {
    private String commandName;
    private String javascript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Auth Number</th><th>Member Number</th><th>Dependant Number</th><th>Auth Date</th>" +
                    "<th>Valid From</th><th>Valid To</th><th>Status</th></tr>");

            
            ArrayList<EAuthSearchResult> eaDetails = (ArrayList<EAuthSearchResult>) req.getAttribute("eAuthSearchResult");
            if (eaDetails != null) {
                System.out.println("eAuth search result size = "+eaDetails.size());
                for (EAuthSearchResult ear : eaDetails) {
                    
                    out.println("<tr><form><td><label class=\"label\">" + ear.getAuthNo() + "</label><input type=\"hidden\" name=\"memAuthorisationNo\" value=\"" + ear.getAuthNo() + "\"/></td> " +
                            "<td><label class=\"label\">" + ear.getMemberNo() + "</label><input type=\"hidden\" name=\"memNum\" value=\"" + ear.getMemberNo() + "\"/></td> " +
                            "<td><label class=\"label\">" + ear.getDepNo() + "</label><input type=\"hidden\" name=\"dependantNum\" value=\"" + ear.getDepNo() + "\"/></td> " +
                            "<td><label class=\"label\">" + ear.getAuthDate() + "</label><input type=\"hidden\" name=\"code\" value=\"" + ear.getAuthDate() + "\"/></td> " +
                            "<td><label class=\"label\">" + ear.getValidFrom() + "</label><input type=\"hidden\" name=\"code\" value=\"" + ear.getValidFrom() + "\"/></td> " +
                            "<td><label class=\"label\">" + ear.getValidTo() + "</label><input type=\"hidden\" name=\"code\" value=\"" + ear.getValidTo() + "\"/></td> " +
                            "<td><label class=\"label\">Approved</label><input type=\"hidden\" name=\"description\" value=\"Approved\"/></td></form></tr>");
                    
                }
                                    
            }
            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in AuthSearchResultTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

}

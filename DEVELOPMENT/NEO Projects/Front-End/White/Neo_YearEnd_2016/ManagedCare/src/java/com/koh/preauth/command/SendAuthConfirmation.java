/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.agile.security.webservice.AgileManager;
import com.agile.security.webservice.EmailAttachment;
import com.agile.security.webservice.EmailContent;
import com.koh.command.FECommand;
import static com.koh.command.FECommand.service;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.koh.utils.FormatUtils;

/**
 *
 * @author johanl
 */
public class SendAuthConfirmation extends FECommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        AgileManager port = service.getAgileManagerPort();
        HttpSession session = request.getSession();


        String memNo = "" + session.getAttribute("memberNum_text");
        String authNo = "" + session.getAttribute("authNumberConf");
        String fileName = "EAuthConfirmation for member: " + memNo + ".pdf";
        String emails = request.getParameter("emails");
        System.out.println("emails = " + emails);   

        byte[] array = (byte[]) session.getAttribute("confirmationPDF");

        String[] emails1 = emails.split("\\|");

        String failedMails = "";
        boolean send = true;

        for (String s : emails1) {
            if (s != null && !s.equalsIgnoreCase("")
                    && !s.equalsIgnoreCase("null")) {
                boolean sendSuccess = false;
                sendSuccess = sendEmail(fileName, memNo, s, array, port, request);

                if (!sendSuccess) {
                    send = sendSuccess;
                    failedMails = "email: " + s + ", ";
                }
            }
        }

        if (!failedMails.equalsIgnoreCase("")) {
            failedMails = failedMails.substring(0, failedMails.length() - 2);
        }

        //log emails
        try {
            logEmailDetails(emails, failedMails, memNo, authNo);

        } catch (IOException ex) {
            Logger.getLogger(SendAuthConfirmation.class.getName()).log(Level.SEVERE, null, ex);

        }
        printEmailResult(request, response, send, failedMails);

        return null;
    }

    @Override
    public String getName() {
        return "SendAuthConfirmation";
    }

    public boolean sendEmail(String fileName, String memNo, String emailTo, byte[] array, AgileManager port, HttpServletRequest request) {
        boolean emailSent = false;
        HttpSession session = request.getSession();

        String emailFrom = "preauth@agilityghs.co.za";
        String subject = "Electronic Authorisation - Member : " + memNo;
        int productId = new Integer(String.valueOf(session.getAttribute("productId")));
        String scheme = (String) session.getAttribute("schemeName");
        
        if (productId == 0) {
            if (scheme.trim().equalsIgnoreCase("Resolution Health")) {
                productId = 1;
            } else if (scheme.trim().equalsIgnoreCase("Spectramed")) {
                 productId = 2;
            }
        }

        System.out.println("Scheme: " + productId + " | " + scheme);
        
        String emailMsg = null;// \n\n"
        //+ "Resolution Health Medical Scheme";

        EmailContent content = new EmailContent();
        content.setContentType("text/plain");
        EmailAttachment attachment = new EmailAttachment();
        attachment.setContentType("application/octet-stream");
        attachment.setName(fileName);
        content.setFirstAttachment(attachment);
        content.setFirstAttachmentData(array);
        content.setSubject(subject);
        content.setEmailAddressFrom(emailFrom);
        content.setEmailAddressTo(emailTo);
        content.setProductId(productId);
        //Determine msg for email according to type of document ('claim', 'statement', 'preAuth', 'benefit' 'custom', 'other')
        content.setType("preAuth");
        /*If document type is custom, this message added here will be displayed in the email. The email method will automatically add the 
         the enquiries and call center aswell as the kind regards at the end of the message*/
        content.setContent(emailMsg);
        emailSent = service.getAgileManagerPort().sendEmailWithOrWithoutAttachment(content);

        return emailSent;

    }

    public void printEmailResult(HttpServletRequest request, HttpServletResponse response, boolean result, String reason) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (result && reason.equalsIgnoreCase("")) {
                out.println("<td><label class=\"header\">Auth Confirmation emailed.</label></td>");
            } else {
                out.println("<td><label class=\"header\">Email sending failed, To address: " + reason + "</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "2; URL=/ManagedCare/PreAuth/PreAuthConfirmation.jsp");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    public void logEmailDetails(String emails, String notSend, String memberNo, String authNo) throws IOException {

        String fileDate = new SimpleDateFormat("yyyy-MM-dd").format(System.currentTimeMillis());
        String fileName = "C:\\NEO_Email_LOG\\" + fileDate + "_emails.txt";

        File file = new File(fileName);
        FileWriter fs = new FileWriter(file, true);
        BufferedWriter out = new BufferedWriter(fs);

        String dateTime = FormatUtils.dateTimeFormat.format(System.currentTimeMillis());

        out.write("****************************************************************************");
        out.newLine();
        out.write("EMAIL CONFIRMATION : AUTH - " + authNo + " for MEMBER - " + memberNo + "");
        out.newLine();
        out.write("****************************************************************************");
        out.newLine();
        out.write("email_log : " + dateTime + " : emails send - " + emails + "");
        out.newLine();
        out.write("email_log : " + dateTime + " : emails failed - " + notSend + "");
        out.newLine();
        out.newLine();
        out.close();
    }
}

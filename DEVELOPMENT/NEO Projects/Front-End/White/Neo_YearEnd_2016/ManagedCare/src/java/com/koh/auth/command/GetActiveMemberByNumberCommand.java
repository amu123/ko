/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.Collection;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.ContactDetails;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author whauger
 */

public class GetActiveMemberByNumberCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("Inside GetActiveMemberByNumberCommand");
        String number = request.getParameter("number");
        String email = "";
        NeoManagerBean port = service.getNeoManagerBeanPort();
        CoverDetails c = port.getPrincipalMemberDetailsToday(number);
        
        try{
            PrintWriter out = response.getWriter();
            if(c != null && c.getEntityId() > 0){
                //Get email contact details
                ContactDetails cd = port.getContactDetails(c.getEntityId(), 1);
                if (cd != null && cd.getPrimaryIndicationId() == 1) {
                    email = cd.getMethodDetails();
                }
                out.print(getName() + "|Number=" + number + "|Email=" + email + "$");
            }else{
                out.print("Error|No such member|" + getName());
            }
        //******************REQUEST ITERATOR
        java.util.Enumeration attributeNames = request.getParameterNames();
        while (attributeNames.hasMoreElements()) {
            String name = String.valueOf(attributeNames.nextElement());
            String value = String.valueOf(request.getParameter(name));
            request.setAttribute(name, value);
        }
        //******************REQUEST ITERATOR
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetActiveMemberByNumberCommand";
    }

}

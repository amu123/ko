/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import com.koh.command.NeoCommand;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author princes
 */
public class QuestionnaireReasonDropDown extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String displayName;
    private String elementName;
    private String javaScript;
    private final NeoCommand neo = new NeoCommand() {

        @Override
        public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
            return null;
        }

        @Override
        public String getName() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    };

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @SuppressWarnings("static-access")
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        /*//List<PdcAnswer> questionnaires = (List<PdcAnswer>) session.getAttribute("dropDownDates");


        if (questionnaires.size() > 0) {

        try {
        out.println("<td><label class=\"label\">" + displayName + ":</label></td>");
        out.println("<td><select name=\"" + elementName + "\" id=\"" + elementName + "\"");
        if (javaScript != null) {
        out.print(javaScript);
        }
        out.println(">");


        Format formatter = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss");
        HashMap<Integer, String> dates = new HashMap<Integer, String>();

        for (int i = 0; i < questionnaires.size(); i++) {
        PdcAnswer questionnaire = questionnaires.get(i);

        Date date = questionnaire.getAnsweredDate().toGregorianCalendar().getTime();
        dates.put(i, formatter.format(date));
        out.println("<option value=\"" + i + "\" >" + formatter.format(date) + "</option>");

        }
        out.println("</select></td>");
        //session.setAttribute("dates", dates);
        } catch (java.io.IOException ex) {
        throw new JspException("Error in QuestionnaireReasonDropDown tag", ex);
        }
        }else{
        //session.setAttribute("tapView","double");
        //session.setAttribute("qaction", "create");
        }*/
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.utils.FormatUtils;
import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.io.PrintWriter;
import java.lang.Exception;
import java.math.BigDecimal;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;

//Amukelani Import
import com.koh.preauth.command.SearchAuthAmukelanim;

/**
 *
 * @author nick
 */
public class CheckIfMemberHasOHAuth extends NeoCommand{
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        boolean foundOHAuth;

        try{
            PrintWriter out = response.getWriter();
            String coverNumber = "" + session.getAttribute("memberNum_text");
            String authSubType = "" + request.getParameter("subType");
            
            foundOHAuth = port.checkIfMemberHasOHAuth(coverNumber, authSubType);
            
            if(foundOHAuth){
                session.setAttribute("spmOHCopayTrigger", "0");
            }else{
                session.setAttribute("spmOHCopayTrigger", "1");
            }
            
            out.print("Success|" + foundOHAuth + "|" + getName());
            
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    @Override
    public String getName() {
        return "CheckIfMemberHasOHAuth";
    }
    
}
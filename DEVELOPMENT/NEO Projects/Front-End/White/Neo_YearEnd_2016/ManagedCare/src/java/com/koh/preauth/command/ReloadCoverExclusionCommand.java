/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;


import com.koh.command.Command;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthCoverExclusions;

/**
 *
 * @author johanl
 */
public class ReloadCoverExclusionCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        List<AuthCoverExclusions> aeList = (List<AuthCoverExclusions>) request.getAttribute("AuthExclusionList");
        if (aeList != null) {
            request.removeAttribute("AuthExclusionList");
        }else{
            session.removeAttribute("AuthExclusionList");
        }
        request.setAttribute("reloadExclusion", "true");
        try {
            String nextJSP = "/PreAuth/GenericAuthorisation.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "ReloadCoverExclusionCommand";
    }
}

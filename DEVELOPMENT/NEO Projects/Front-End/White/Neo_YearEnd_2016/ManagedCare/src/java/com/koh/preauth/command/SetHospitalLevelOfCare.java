/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.FormatUtils;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthHospitalLOC;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author johanl
 */
public class SetHospitalLevelOfCare extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;

        try {
            out = response.getWriter();

            List<AuthHospitalLOC> locList = (List<AuthHospitalLOC>) session.getAttribute("AuthLocList");
            if (locList != null && locList.size() > 0) {
                locList = DateTimeUtils.orderLOCListByDate(locList);
                AuthHospitalLOC firstLoc = locList.get(0);
                String dateFrom = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(firstLoc.getDateFrom().toGregorianCalendar().getTime());
                String dateTo = "";
                double los = 0.0d;
                double estimateCost = 0.0d;

                for (AuthHospitalLOC loc : locList) {
                    dateTo = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(loc.getDateTo().toGregorianCalendar().getTime());
                    los = los + loc.getLengthOfStay();
                    estimateCost = estimateCost + loc.getEstimatedCost();
                }
                session.setAttribute("admissionDateTime", dateFrom);
                session.setAttribute("dischargeDateTime", dateTo);
                session.setAttribute("los", los);
                session.setAttribute("amountClaimed", estimateCost);
                
                //validate penalty
                Calendar admissionCal = Calendar.getInstance();
                Calendar authDateCal = Calendar.getInstance();

                Date authorisationDate = null;
                String authDate = "" + session.getAttribute("authDate");
                try {
                    if (authDate.length() == 10) {
                        authorisationDate = FormatUtils.dateFormat.parse(authDate);
                    } else {
                        if (authDate.contains("T")) {
                            authDate = authDate.replace("T", " ");
                        }
                        if (authDate.contains(".")) {
                            int i = authDate.indexOf(".");
                            authDate = authDate.substring(0, i);
                        }
                        if (authDate.contains("-")) {
                            authDate = authDate.replaceAll("-", "/");
                        }
                        System.out.println("AUTH DATE FINAL = " + authDate);
                        authorisationDate = FormatUtils.dateTimeFormat.parse(authDate);
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(SetHospitalLevelOfCare.class.getName()).log(Level.SEVERE, null, ex);
                }

                String admission = "" + session.getAttribute("admissionDateTime");
                Date admissionDate72 = null;
                try {
                    admissionDate72 = FormatUtils.dateTimeNoSecFormat.parse(admission);
                } catch (ParseException ex) {
                    Logger.getLogger(SetHospitalLevelOfCare.class.getName()).log(Level.SEVERE, null, ex);
                }

                admissionCal.setTime(admissionDate72);
                authDateCal.setTime(authorisationDate);
                authDateCal.add(Calendar.HOUR_OF_DAY, -72);

                session.setAttribute("isPenalty", null);
                if (authDateCal.equals(admissionCal) || authDateCal.after(admissionCal)) {
                    session.setAttribute("isPenalty", "yes");
                } else {
                    String dspCoPay = "" + session.getAttribute("isDSPPenalty");
                    if (dspCoPay.equalsIgnoreCase("no")) {
                        session.setAttribute("isPenalty", null);
                    }
                    session.setAttribute("authPenFound", null);
                }
                
                double totalTariff = 0.0d;
                double totalWard = 0.0d;

//                String hospAmount = "" + session.getAttribute("estimatedWardCost");
//                String tarAmount = "" + session.getAttribute("totTariffCost");

                ArrayList<AuthTariffDetails> atList = (ArrayList<AuthTariffDetails>) session.getAttribute("tariffListArray");

                if (atList != null) {
                    for (AuthTariffDetails at : atList) {
                        totalTariff += at.getAmount();
                    }
                }

                totalWard = estimateCost;

                double newAmountClaimed = totalTariff + totalWard;

                session.setAttribute("hospInterim", FormatUtils.decimalFormat.format(newAmountClaimed));

                out.println("Done|");
            } else {
                out.println("Error|Level Of Care is Mandatory");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SetHospitalLevelOfCare";
    }
}

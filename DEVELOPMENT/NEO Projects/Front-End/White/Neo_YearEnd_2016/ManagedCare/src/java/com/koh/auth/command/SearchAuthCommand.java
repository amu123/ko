/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.auth.command;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.agile.security.webservice.EAuthSearchCriteria;
import com.agile.security.webservice.EAuthSearchResult;
import com.koh.command.FECommand;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.RequestDispatcher;

/**
 *
 * @author johanl
 */
public class SearchAuthCommand extends FECommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        EAuthSearchCriteria eaCri = new EAuthSearchCriteria();
        String authNo = request.getParameter("authNo");
        String memberNo = request.getParameter("memberNo");
        String depCode = request.getParameter("depNo");
        String authDate = request.getParameter("authDate");
        String validFrom = request.getParameter("validFrom");
        String validTo = request.getParameter("validTo");

        System.out.println("authNo = "+authNo);
        System.out.println("memberNo = "+memberNo);
        System.out.println("depCode = "+depCode);
        System.out.println("authDate = "+authDate);
        System.out.println("validFrom = "+validFrom);
        System.out.println("validTo = "+validTo);


        // SEARCH BY AUTH NUMBER
        if (!authNo.trim().equalsIgnoreCase("")) {
            eaCri.setAuthNumber(authNo);
        }
        //SEARCH BY MEMBER NUMBER
        if (!memberNo.trim().equalsIgnoreCase("")) {
            eaCri.setMemberNumber(memberNo);
        }
        //SEARCH BY DEPENDANT CODE
        if (!depCode.trim().equalsIgnoreCase("")) {
            eaCri.setDependantNumber(depCode);
        }
        //SEARCH BY AUTH DATE
        if (!authDate.trim().equalsIgnoreCase("")) {
            eaCri.setAuthDate(authDate);
        }
        //SEARCH BY AUTH VALID FROM
        if (!validFrom.trim().equalsIgnoreCase("")) {
            eaCri.setValidFrom(validFrom);
        }
        //SEARCH BY AUTH VALID TO
        if (!validTo.trim().equalsIgnoreCase("")) {
            eaCri.setValidTo(validTo);
        }

        Collection<EAuthSearchResult> eaResult = new ArrayList<EAuthSearchResult>();
        List<EAuthSearchResult> wsCall = new ArrayList<EAuthSearchResult>();
        
        try {
            wsCall = service.getAgileManagerPort().searchAuth(eaCri);
            for (EAuthSearchResult ea : wsCall) {
                EAuthSearchResult ear = new EAuthSearchResult();
                ear.setAuthNo(ea.getAuthNo());
                ear.setAuthDate(ea.getAuthDate());
                ear.setMemberNo(ea.getMemberNo());
                ear.setDepNo(ea.getDepNo());
                ear.setValidFrom(ea.getValidFrom());
                ear.setValidTo(ea.getValidTo());
                ear.setAuthStatus(ea.getAuthStatus());
                eaResult.add(ear);

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        request.setAttribute("eAuthSearchResult", eaResult);
        try {
            String nextJSP = "/Auth/AuthSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "SearchAuthCommand";
    }
}

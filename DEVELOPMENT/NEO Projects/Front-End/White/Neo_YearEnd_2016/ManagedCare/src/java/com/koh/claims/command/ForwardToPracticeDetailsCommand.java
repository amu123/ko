/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.util.Collection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.BankingDetails;
import neo.manager.EntityPaymentRunDetails;
import neo.manager.NeoManagerBean;
import neo.manager.PaymentRunSearchCriteria;
import neo.manager.ProviderDetails;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ForwardToPracticeDetailsCommand extends NeoCommand{

        private Logger log = Logger.getLogger(ViewProviderDetailsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        log.info("Inside ForwardToPracticeDetailsCommand");

        HttpSession session = request.getSession();

        NeoManagerBean port = service.getNeoManagerBeanPort();

        PaymentRunSearchCriteria payRun = new PaymentRunSearchCriteria();


        String providerNumber = ""+session.getAttribute("practiceNumber");

        log.info("The variable providerNumber is " + providerNumber);

        payRun.setEntityNo(providerNumber);

        ProviderDetails provDetails =  port.getPracticeDetailsForEntityByNumber(providerNumber);

       Collection<BankingDetails> bankDetails = port.getAllBankingDetails(provDetails.getEntityId());

       Collection<EntityPaymentRunDetails> paymentRunDetailsList = port.getPaymentRunDetailForEntity(payRun, null);

        log.info("The provider entity id is " + provDetails.getEntityId());
        log.info("The banking details list is " + bankDetails.size());

        if(provDetails != null) {

            session.setAttribute("practiceNumber", provDetails.getPracticeNumber());
            session.setAttribute("practiceName", provDetails.getPracticeName());
            session.setAttribute("practiceSurname", provDetails.getProviderName());
            session.setAttribute("disciplineType", provDetails.getDisciplineType().getValue());
            session.setAttribute("networkIndicator", provDetails.getIdentificationType());
            session.setAttribute("status", provDetails.getProviderStatus().getValue());
        }

        for(BankingDetails bank: bankDetails) {

            session.setAttribute("pracBankName", bank.getBankName());
            session.setAttribute("pracBranchCode", bank.getBranchCode());
            session.setAttribute("pracBranchName", bank.getBranchName());
            session.setAttribute("pracAccountNumber", bank.getAccountNo());
            session.setAttribute("pracAccountName", bank.getAccountName());
            session.setAttribute("pracAccountType", bank.getAccountType());
        }


        log.info("The provider number is " + providerNumber);

//        log.info("Inside ViewProviderDetailsCommand ");

        log.info("The provider payment run list " + paymentRunDetailsList.size());

         request.setAttribute("practicePaymentRunDetails", paymentRunDetailsList);

        try {
            //String nextJSP = "/Claims/ProviderDetailsTabbedPage.jsp";
            String nextJSP = "/Claims/PracticeDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        }catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "ForwardToPracticeDetailsCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthHospitalLOC;
import neo.manager.NeoUser;

/**
 *
 * @author johanl
 */
public class LoadDefaultHospitalLOC extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        PrintWriter out = null;

        try {
                out = response.getWriter();
                Date dateF = new Timestamp(System.currentTimeMillis());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                //change date for auth period
                String dt = sdf.format(dateF);
                String authPeriod = "" + session.getAttribute("authPeriod");

                String dateWithPeriod = authPeriod + dt.substring(dt.indexOf("/"), dt.length());
                try {
                    dateF = sdf.parse(dateWithPeriod);
                } catch (ParseException px) {
                    px.printStackTrace();
                }

                session.setAttribute("reimbursementModel", "3");
                session.setAttribute("admissionDateTime", new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(dateF));

                out.println("Done|");

        } catch (Exception e) {
            e.printStackTrace();
            out.println("Error|");
        } finally {
            out.close();
        }
        return null;
    }

    @Override
    public String getName() {
        return "LoadDefaultHospitalLOC";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.calltrack.tags;

import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.EAuthCoverDetails;

/**
 *
 * @author josephm
 */
public class MemberCoverSearchTag extends TagSupport {

    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        //System.out.println("Have we got inside the tag");
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            out.println("<label class=\"header\">Cover Details</label></br></br>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Expand</th><th>Member Name</th><th>Surname</th><th>Dependant Type</th>"
                    + "<th>Dependant Code</th><th>Status</th><th>Join Date</th>"
                    + "<th>Option From</th><th>Option To</th>"
                    + "</tr>");
//
            List<EAuthCoverDetails> members = (List<EAuthCoverDetails>) req.getAttribute("memberCoverDetails");
            String lastMember = "";
            boolean mustClose = false;
            if (members != null && members.size() > 0) {
                for (int i = 0; i < members.size(); i++) {
                    EAuthCoverDetails cover = members.get(i);

                    if (!cover.getCoverNumber().equalsIgnoreCase(lastMember)) {
                        if (i != 0) { // org
                            out.println("</tbody><tr id=\"" + cover.getCoverNumber() + "main\"><form><td align=\"center\">"
                                    + "<a id=\"" + cover.getCoverNumber() + "plus\" style=\"display:none\" href=\"#\" onClick=\"showRows('" + cover.getCoverNumber() + "')\"><img src=\"resources/plus.gif\" width=\"18\" height=\"18\" alt=\"Logout\" border=\"0\"/></a>"
                                    + "<a id=\"" + cover.getCoverNumber() + "minus\" href=\"#\" onClick=\"hideRows('" + cover.getCoverNumber() + "')\"><img src=\"resources/minus.gif\" width=\"18\" height=\"18\" alt=\"Logout\" border=\"0\"/></a>"
                                    + "</td>");


                            // out.println("</tbody><tr id=\"" + cover.getCoverNumber() + "main\"><form><td align=\"center\">" +
                            // "<a id=\""+cover.getCoverNumber()+"minus\"  href=\"#\" onClick=\"hideRows('" + cover.getCoverNumber() + "')\"><img src=\"resources/minus.gif\" width=\"18\" height=\"18\" alt=\"Logout\" border=\"0\"/></a>" +
                            //"<a id=\""+cover.getCoverNumber()+"plus\" style=\"display:none\" href=\"#\" onClick=\"showRows('" + cover.getCoverNumber() + "')\"><img src=\"resources/plus.gif\" width=\"18\" height=\"18\" alt=\"Logout\" border=\"0\"/></a>" +
                            // "</td>");
                            mustClose = false;
                        } else {// org
                            out.println("<tr id=\"" + cover.getCoverNumber() + "main\"><form><td align=\"center\">"
                                    + "<a id=\"" + cover.getCoverNumber() + "plus\" style=\"display:none\" href=\"#\" onClick=\"showRows('" + cover.getCoverNumber() + "')\"><img src=\"resources/plus.gif\" width=\"18\" height=\"18\" alt=\"Logout\" border=\"0\"/></a>"
                                    + "<a id=\"" + cover.getCoverNumber() + "minus\" href=\"#\" onClick=\"hideRows('" + cover.getCoverNumber() + "')\"><img src=\"resources/minus.gif\" width=\"18\" height=\"18\" alt=\"Logout\" border=\"0\"/></a>"
                                    + "</td>");


                            //out.println("<tr id=\"" + cover.getCoverNumber() + "main\"><form><td align=\"center\">" +

                            // "<a id=\""+cover.getCoverNumber()+"minus\" href=\"#\" onClick=\"hideRows('" + cover.getCoverNumber() + "')\"><img src=\"resources/minus.gif\" width=\"18\" height=\"18\" alt=\"Logout\" border=\"0\"/></a>" +
                            // "<a id=\""+cover.getCoverNumber()+"plus\" style=\"display:none\" href=\"#\" onClick=\"showRows('" + cover.getCoverNumber() + "')\"><img src=\"resources/plus.gif\" width=\"18\" height=\"18\" alt=\"Logout\" border=\"0\"/></a>" +

                            //"</td>");
                            mustClose = false;
                        }

                    } else {
                        if (!mustClose) {
                            // out.println("<tbody id=\"" + cover.getCoverNumber() + "\" style=\"display:none\">");
                            out.println("<tbody id=\"" + cover.getCoverNumber() + "\" style=\"display:''\">");

                            mustClose = true;
                        }
                        out.println("<tr><form><td></td>");
                    }

                    out.println("<td><label class=\"label\">" + cover.getCoverName() + "</label><input type=\"hidden\" name=\"memNo\" value=\"" + cover.getCoverNumber() + "\"/></td>"
                            + "<td><label class=\"label\">" + cover.getCoverSurname() + "</label><input type=\"hidden\" name=\"dependant\" value=\"" + cover.getDependantType() + "\"/></td>"
                            + "<td><label class=\"label\">" + cover.getDependantType() + "</label><input type=\"hidden\" name=\"name\" value=\"" + cover.getCoverName() + "\"/></td>"
                            + "<td><label class=\"label\">" + cover.getDependentNumber() + "</label><input type=\"hidden\" name=\"surname\" value=\"" + cover.getCoverSurname() + "\"/></td>"
                            + "<td><label class=\"label\">" + cover.getCoverStatus() + "</label></td>");

                    //join date
                    if (cover.getJoinDate() != null) {
                        String dateFrom = new SimpleDateFormat("yyyyMMdd").format(cover.getJoinDate().toGregorianCalendar().getTime());
                        out.println("<td><label class=\"label\">" + dateFrom + "</label></td>");
                    } else {
                        out.println("<td></td>");
                    }
                    //benefit start
                    if (cover.getCoverStartDate() != null) {
                        String dateFrom = new SimpleDateFormat("yyyyMMdd").format(cover.getCoverStartDate().toGregorianCalendar().getTime());
                        out.println("<td><label class=\"label\">" + dateFrom + "</label></td>");
                    } else {
                        out.println("<td></td>");
                    }
                    //benefit end
                    if (cover.getCoverEndDate() != null) {
                        String dateTo = new SimpleDateFormat("yyyyMMdd").format(cover.getCoverEndDate().toGregorianCalendar().getTime());
                        out.println("<td><label class=\"label\">" + dateTo + "</label></td>");
                    } else {
                        out.println("<td></td>");
                    }

                    //System.out.println("From the tag, the cover number is  " + cover.getCoverNumber());


                    //out.println("<td><button name=\"opperation\" type=\"submit\" " + javaScript + " value=\"" + commandName + "\">Select</button></td></form>");
                    lastMember = cover.getCoverNumber();
                    if (i == members.size() - 1 && mustClose) {
                        out.println("<tbody></tr>");
                    } else {
                        out.println("</tr>");
                    }
                }
            }


            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberSearchTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

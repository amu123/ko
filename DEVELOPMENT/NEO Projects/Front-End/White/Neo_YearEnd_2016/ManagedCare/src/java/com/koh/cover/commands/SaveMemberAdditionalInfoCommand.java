/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.koh.command.NeoCommand;
import com.koh.cover.MemberMainMapping;
import com.koh.employer.command.TabUtils;
import com.koh.fe.command.LoginCommand;
import com.koh.member.application.command.SaveMemberApplicationCommand;
import com.koh.member.application.command.SaveMemberCoverDetailsCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Exception;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class SaveMemberAdditionalInfoCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String s = request.getParameter("buttonPressed");
        System.out.println("buttonPressed " + s);

        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();

        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        Integer depNo = (Integer) session.getAttribute("memberCoverDepNum");
        Date birthDate = null;
        int depType = 0;
        session.setAttribute("depMessage", null);
        for (CoverDetails cov : cdList) {
            if (cov.getDependentNumber() == depNo) {
                birthDate = cov.getDateOfBirth().toGregorianCalendar().getTime();
                depType = cov.getDependentTypeId();
            }
        }
        if (depType == 19) {
            if (calculateAge(birthDate) < 21) {
                session.setAttribute("depMessage", "child");
            }
        }

        if (depType == 18) {
            if (calculateAge(birthDate) > 23) {
                session.setAttribute("depMessage", "adult");
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("changeIncomeButton".trim())) {
                try {
                    viewMemberIncome(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("changeDebitDateButton".trim())) {
                try {
                    viewMemberDebitDate(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("changePaymentButton".trim())) {
                try {
                    viewMemberPaymentMethod(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("changeSubsidyButton".trim())) {
                try {
                    viewMemberSubsidy(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("SaveMemberDebitDate".trim())) {
                PrintWriter out = null;
                try {
                    out = response.getWriter();
                    String result = validateAndSaveDebitDate(request);
                    out.println(result);
                    return null;
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberAdditionalInfoCommand.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    out.close();
                }
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("SaveMemberPaymentMethod".trim())) {
                PrintWriter out = null;
                try {
                    out = response.getWriter();
                    String result = validateAndSavePaymentMethod(request);
                    out.println(result);
                    return null;
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberAdditionalInfoCommand.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    out.close();
                }
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("SaveMemberSubsidy".trim())) {
                PrintWriter out = null;
                try {
                    out = response.getWriter();
                    String result = validateAndSaveSubsidy(request);
                    out.println(result);
                    return null;
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberAdditionalInfoCommand.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    out.close();
                }
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("SaveMemberIncome".trim())) {
                PrintWriter out = null;
                try {
                    out = response.getWriter();
                    String result = validateAndSaveIncome(request);
                    out.println(result);
                    return null;
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberAdditionalInfoCommand.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    out.close();
                }
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("SaveDepType".trim())) {
                PrintWriter out = null;
                try {
                    out = response.getWriter();
                    String result = validateAndSaveDepType(request);
                    out.println(result);
                    return null;
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberAdditionalInfoCommand.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    out.close();
                }
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("SaveFinType".trim())) {
                PrintWriter out = null;
                try {
                    out = response.getWriter();
                    String result = validateAndSaveFinType(request);
                    out.println(result);
                    return null;
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberAdditionalInfoCommand.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    out.close();
                }
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("SaveStudent".trim())) {
                PrintWriter out = null;
                try {
                    out = response.getWriter();
                    String result = validateAndSaveStudent(request);
                    out.println(result);
                    return null;
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberAdditionalInfoCommand.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    out.close();
                }
            }
        }

//        SaveRelType
        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("SaveRelType".trim())) {
                PrintWriter out = null;
                try {
                    out = response.getWriter();
                    //Chnage belo method to validateAndSave
                    String result = validateAndSaveRelType(request);
                    out.println(result);
                    return null;
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberAdditionalInfoCommand.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    out.close();
                }
            }
        }
        
        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("SaveHandicap".trim())) {
                PrintWriter out = null;
                try {
                    out = response.getWriter();
                    String result = validateAndSaveHandicap(request);
                    out.println(result);
                    return null;
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberAdditionalInfoCommand.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    out.close();
                }
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("changeDepType".trim())) {
                try {
                    viewDepType(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("changeFinType".trim())) {
                try {
                    viewFinType(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("changeStudent".trim())) {
                try {
                    viewStudent(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("changeHandicap".trim())) {
                try {
                    viewHandicap(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }


        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("changeRelType".trim())) {
                try {
                    viewRelType(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }
          
        try {
            PrintWriter out = response.getWriter();
            String result = validateAndSave(request);
            out.println(result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private void viewMemberIncome(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        HttpSession session = request.getSession();
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        session.setAttribute("memberView", "Income");
        context.getRequestDispatcher("/Member/AddMemberIncome.jsp").forward(request, response);
    }

    private void viewMemberPaymentMethod(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        HttpSession session = request.getSession();
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        session.setAttribute("memberView", "Payment");
        context.getRequestDispatcher("/Member/AddMemberIncome.jsp").forward(request, response);
    }

    private void viewMemberDebitDate(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        HttpSession session = request.getSession();
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        session.setAttribute("memberView", "Debit");
        context.getRequestDispatcher("/Member/AddMemberIncome.jsp").forward(request, response);
    }

    private void viewMemberSubsidy(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        HttpSession session = request.getSession();
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        session.setAttribute("memberView", "Subsidy");
        context.getRequestDispatcher("/Member/AddMemberIncome.jsp").forward(request, response);

    }

    private void viewDepType(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("backDated", null);
        session.setAttribute("forwardDated", null);
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        session.setAttribute("depTypeView", "depType");
        request.setAttribute("DependantTypeList", getDependantTypes(request));
        context.getRequestDispatcher("/Member/MemberDepType.jsp").forward(request, response);

    }
    
    private void viewRelType(NeoManagerBean port, HttpServletRequest request , HttpServletResponse response , ServletContext context) throws ServletException, IOException{
        HttpSession session = request.getSession();
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        session.setAttribute("depTypeView", "relType");
        context.getRequestDispatcher("/Member/MemberDepType.jsp").forward(request, response);
    }
    private void viewFinType(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        HttpSession session = request.getSession();
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        session.setAttribute("depTypeView", "finType");
        context.getRequestDispatcher("/Member/MemberDepType.jsp").forward(request, response);

    }

    private void viewStudent(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        HttpSession session = request.getSession();
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        session.setAttribute("depTypeView", "student");
        context.getRequestDispatcher("/Member/MemberDepType.jsp").forward(request, response);

    }

    private void viewHandicap(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        HttpSession session = request.getSession();
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        session.setAttribute("depTypeView", "handicap");
        context.getRequestDispatcher("/Member/MemberDepType.jsp").forward(request, response);

    }

    private String validateAndSave(HttpServletRequest request) {
        Map<String, String> errors = MemberMainMapping.validateMemberAddInfo(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (save(request)) {
                additionalData = "\"message\":\"Member Additional Details Updated\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error updating the member additional details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
    }

    private boolean save(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        HttpSession session = request.getSession();

        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);

//        int entityId = MemberMainMapping.getEntityId(request);

        try {
            //          PersonDetails pd = MemberMainMapping.getPersonDetail(request, entityId);
            Member mem = MemberMainMapping.getMemberAddInfo(request);
            Integer entityID = (Integer) session.getAttribute("memberEntityID");

            mem.setEntityId(entityID);
            mem.setCreatedBy(neoUser.getUserId());
            mem.setSecurityGroupId(neoUser.getSecurityGroupId());
            mem.setLastUpdatedBy(neoUser.getUserId());
            port.saveMemberAddInfo(mem);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private String validateAndSaveDebitDate(HttpServletRequest request) {
        Map<String, String> errors = MemberMainMapping.validateDebitDate(request);

        /*
         * if (request.getParameter("MAI_newDebitDateStartDate") != null &&
         * !request.getParameter("MAI_newDebitDateStartDate").isEmpty()) { Date
         * startDate =
         * DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newDebitDateStartDate"));
         * Calendar past = Calendar.getInstance(); Calendar future =
         * Calendar.getInstance(); Calendar optionDate = Calendar.getInstance();
         * optionDate.setTime(startDate); past.add(Calendar.MONTH, -3);
         * future.add(Calendar.MONTH, 2);
         *
         * Date maxDate = past.getTime(); Date minDate = future.getTime(); Date
         * curr = optionDate.getTime();
         *
         * if (curr.before(maxDate)) {
         * errors.put("MAI_newDebitDateStartDate_error", "Start date can't be
         * more than three months back"); }
         *
         * if (curr.after(minDate)) {
         * errors.put("MAI_newDebitDateStartDate_error", "Start date can't be
         * more than two months in future"); } }
         */
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        Map<String, String> fieldsMap = new HashMap<String, String>();
        if ("OK".equalsIgnoreCase(status)) {
            if (saveDebitDate(request)) {
                fieldsMap.put("incomeStatus", "ok");
                System.out.println("Debit value " + request.getParameter("MAI_newDebitDate"));
                fieldsMap.put("MAI_DebitDate", getDebitDate(request.getParameter("MAI_newDebitDate")));

                additionalData = "\"message\":\"Member Debit Date Updated\"";
            } else {
                status = "ERROR";
                fieldsMap.put("dependantStatus", "error");
                additionalData = "\"message\":\"There was an error updating debit date details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, TabUtils.convertMapToJSON(fieldsMap), additionalData);
    }

    private String validateAndSavePaymentMethod(HttpServletRequest request) {
        Map<String, String> errors = MemberMainMapping.validatePaymentMethod(request);

        /*
         * if (request.getParameter("MAI_newPaymentStartDate") != null &&
         * !request.getParameter("MAI_newPaymentStartDate").isEmpty()) { Date
         * startDate =
         * DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newPaymentStartDate"));
         * Calendar past = Calendar.getInstance(); Calendar future =
         * Calendar.getInstance(); Calendar optionDate = Calendar.getInstance();
         * optionDate.setTime(startDate); past.add(Calendar.MONTH, -3);
         * future.add(Calendar.MONTH, 2);
         *
         * Date maxDate = past.getTime(); Date minDate = future.getTime(); Date
         * curr = optionDate.getTime();
         *
         * if (curr.before(maxDate)) {
         * errors.put("MAI_newPaymentStartDate_error", "Start date can't be more
         * than three months back"); }
         *
         * if (curr.after(minDate)) {
         * errors.put("MAI_newPaymenteStartDate_error", "Start date can't be
         * more than two months in future"); } }
         */
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        Map<String, String> fieldsMap = new HashMap<String, String>();
        if ("OK".equalsIgnoreCase(status)) {
            if (savePaymentMethod(request)) {
                fieldsMap.put("incomeStatus", "ok");
                System.out.println("Payment value " + request.getParameter("MAI_newPayment"));
                fieldsMap.put("MAI_PayMethod", getPaymentMethod(request.getParameter("MAI_newPayment")));

                additionalData = "\"message\":\"Member Payment Method Updated\"";
            } else {
                status = "ERROR";
                fieldsMap.put("dependantStatus", "error");
                additionalData = "\"message\":\"There was an error updating the dependant finacial details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, TabUtils.convertMapToJSON(fieldsMap), additionalData);
    }

    private String validateAndSaveIncome(HttpServletRequest request) {
        Map<String, String> errors = MemberMainMapping.validateMemberIncome(request);
        String income = request.getParameter("MAI_newIncome");
        if (!checkIfNumber(income)) {
            errors.put("MAI_newIncome_error", "Invalid income");
        }
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        Map<String, String> fieldsMap = new HashMap<String, String>();
        if ("OK".equalsIgnoreCase(status)) {
            if (saveIncome(request)) {
                fieldsMap.put("incomeStatus", "ok");
                fieldsMap.put("MAI_Income", request.getParameter("MAI_newIncome"));

                additionalData = "\"message\":\"Member Income Updated\"";
            } else {
                status = "ERROR";
                fieldsMap.put("incomeStatus", "error");
                additionalData = "\"message\":\"There was an error updating the member income details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, TabUtils.convertMapToJSON(fieldsMap), additionalData);
    }

    private String validateAndSaveSubsidy(HttpServletRequest request) {
        Map<String, String> errors = MemberMainMapping.validateMemberSubsidy(request);
        String validationErros = TabUtils.convertMapToJSON(errors);

        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        Map<String, String> fieldsMap = new HashMap<String, String>();
        if ("OK".equalsIgnoreCase(status)) {
            if (saveSubsidy(request)) {
                fieldsMap.put("subsidyStatus", "ok");
                fieldsMap.put("MAI_Subsidy", request.getParameter("MAI_newSubsidy"));

                additionalData = "\"message\":\"Member Subsidy Updated\"";
            } else {
                status = "ERROR";
                fieldsMap.put("subsidyStatus", "error");
                additionalData = "\"message\":\"There was an error updating the member income details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, TabUtils.convertMapToJSON(fieldsMap), additionalData);
    }

    private String validateAndSaveDepType(HttpServletRequest request) {
        Map<String, String> errors = MemberMainMapping.validateDependentType(request);
        HttpSession session = request.getSession();
        String additionalData = null;
        String updateFields = null;
        Map<String, String> fieldsMap = new HashMap<String, String>();
        int depcode = (Integer) session.getAttribute("memberCoverDepNum");

        HashMap<Integer, String> depListJoinDateByCode = new HashMap<Integer, String>();
        depListJoinDateByCode = (HashMap<Integer, String>) session.getAttribute("depListJoinDateByCode");
        Date coverJoinDate = DateTimeUtils.convertFromYYYYMMDD(depListJoinDateByCode.get(depcode));
        int action = new Integer(request.getParameter("depTypeSelect"));
        String depType = request.getParameter("MAI_newDependantType");

        if (action == 2) {
            if (depType == null || depType.isEmpty()) {
                errors.put("MAI_newDependantType_error", "Dependent Type is required");
            }

            if (request.getParameter("MAI_newDependantTypeStartDate2") != null && !request.getParameter("MAI_newDependantTypeStartDate2").isEmpty()) {
                Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newDependantTypeStartDate2"));
                Calendar past = Calendar.getInstance();
                Calendar future = Calendar.getInstance();
                Calendar optionDate = Calendar.getInstance();
                optionDate.setTime(startDate);
                //past.add(Calendar.MONTH, -3);
                //future.add(Calendar.MONTH, 2);
                String backDated = null;
                String forwardDated = null;

                Date maxDate = past.getTime();
                Date minDate = future.getTime();
                Date curr = optionDate.getTime();

                if (curr.before(maxDate)) {
                    backDated = (String) session.getAttribute("backDated");
                    if (backDated == null || backDated.isEmpty()) {
                        errors.put("MAI_newDependantTypeStartDate2_error", "Back dating changes MUST be signed off by GM Membership & Contributions before processing. Ensure relevant processes were followed prior to action");
                        session.setAttribute("backDated", "Warning");
                    } else {
                        session.setAttribute("backDated", null);
                    }
                }

                if (curr.before(coverJoinDate)) {
                    errors.put("MAI_newDependantTypeStartDate2_error", "Start date can't be less than the dependant join date");
                }

                if (curr.after(minDate)) {
                    forwardDated = (String) session.getAttribute("forwardDated");
                    if (forwardDated == null || forwardDated.isEmpty()) {
                        errors.put("MAI_newDependantTypeStartDate2_error", "Future dating changes MUST be signed off by GM Membership & Contributions before processing. Ensure relevant processes were followed prior to action");
                        session.setAttribute("forwardDated", "Warning");
                    } else {
                        session.setAttribute("forwardDated", null);
                    }
                }
            } else {
                errors.put("MAI_newDependantTypeStartDate2_error", "Start Date is required");
            }
        } else {
            if (request.getParameter("MAI_newDependantTypeStartDate") != null && !request.getParameter("MAI_newDependantTypeStartDate").isEmpty()) {
                Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newDependantTypeStartDate"));
                Calendar past = Calendar.getInstance();
                Calendar future = Calendar.getInstance();
                Calendar optionDate = Calendar.getInstance();
                optionDate.setTime(startDate);
                //past.add(Calendar.MONTH, -3);
                //future.add(Calendar.MONTH, 2);
                String backDated = null;
                String forwardDated = null;

                Date maxDate = past.getTime();
                Date minDate = future.getTime();
                Date curr = optionDate.getTime();

                if (curr.before(maxDate)) {
                    backDated = (String) session.getAttribute("backDated");
                    if (backDated == null || backDated.isEmpty()) {
                        errors.put("MAI_newDependantTypeStartDate_error", "Back dating changes MUST be signed off by GM Membership & Contributions before processing. Ensure relevant processes were followed prior to action");
                        session.setAttribute("backDated", "Warning");
                    } else {
                        session.setAttribute("backDated", null);
                    }
                }

                if (curr.after(minDate)) {
                    forwardDated = (String) session.getAttribute("forwardDated");
                    if (forwardDated == null || forwardDated.isEmpty()) {
                        errors.put("MAI_newDependantTypeStartDate_error", "Future dating changes MUST be signed off by GM Membership & Contributions before processing. Ensure relevant processes were followed prior to action");
                        session.setAttribute("forwardDated", "Warning");
                    } else {
                        session.setAttribute("forwardDated", null);
                    }
                }

                if (curr.before(coverJoinDate)) {
                    errors.put("MAI_newDependantTypeStartDate_error", "Start date can't be less than the dependant join date");
                }


            } else {
                errors.put("MAI_newDependantTypeStartDate_error", "Start Date is required");
            }
        }
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        if ("OK".equalsIgnoreCase(status)) {
            if (saveDepType(request)) {
                fieldsMap.put("dependantStatus", "ok");
                fieldsMap.put("MAI_DepType", getDependantTypeValue(request.getParameter("MAI_newDependantType")));
                session.setAttribute("backDated", null);
                session.setAttribute("forwardDated", null);

                additionalData = "\"message\":\"Dependant Type Updated\"";
            } else {
                status = "ERROR";
                fieldsMap.put("dependantStatus", "error");
                additionalData = "\"message\":\"There was an error updating the dependent type.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, TabUtils.convertMapToJSON(fieldsMap), additionalData);
    }

    private String validateAndSaveFinType(HttpServletRequest request) {
        Map<String, String> errors = MemberMainMapping.validateFinacialDependant(request);

        if (request.getParameter("MAI_newFinanciallyDependantStartDate") != null && !request.getParameter("MAI_newFinanciallyDependantStartDate").isEmpty()) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newFinanciallyDependantStartDate"));
            Calendar past = Calendar.getInstance();
            Calendar future = Calendar.getInstance();
            Calendar optionDate = Calendar.getInstance();
            optionDate.setTime(startDate);
            past.add(Calendar.MONTH, -3);
            future.add(Calendar.MONTH, 2);

            Date maxDate = past.getTime();
            Date minDate = future.getTime();
            Date curr = optionDate.getTime();

            if (curr.before(maxDate)) {
                errors.put("MAI_newFinanciallyDependantStartDate_error", "Start date can't be more than three months back");
            }

            if (curr.after(minDate)) {
                errors.put("MAI_newFinanciallyDependantStartDate_error", "Start date can't be more than two months in future");
            }
        }
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        Map<String, String> fieldsMap = new HashMap<String, String>();
        if ("OK".equalsIgnoreCase(status)) {
            if (saveFinType(request)) {
                fieldsMap.put("dependantStatus", "ok");
                fieldsMap.put("MAI_FinanciallyDependant", getYesNoValue(request.getParameter("MAI_newFinanciallyDependant")));

                additionalData = "\"message\":\"Member Finacially Dependant Updated\"";
            } else {
                status = "ERROR";
                fieldsMap.put("dependantStatus", "error");
                additionalData = "\"message\":\"There was an error updating the dependant finacial details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, TabUtils.convertMapToJSON(fieldsMap), additionalData);
    }

    private String validateAndSaveRelType(HttpServletRequest request) {
        Map<String, String> errors = MemberMainMapping.validateRelationshipType(request);

        List<neo.manager.LookupValue> lookUpsClient = service.getNeoManagerBeanPort().findCodeTableForLookupType(213);
        
        if (request.getParameter("MAI_newRelTypeStartDate") != null && !request.getParameter("MAI_newRelTypeStartDate").isEmpty()) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newRelTypeStartDate"));
            Calendar past = Calendar.getInstance();
            Calendar future = Calendar.getInstance();
            Calendar optionDate = Calendar.getInstance();
            optionDate.setTime(startDate);
            past.add(Calendar.MONTH, -3);
            future.add(Calendar.MONTH, 2);

            Date maxDate = past.getTime();
            Date minDate = future.getTime();
            Date curr = optionDate.getTime();

            if (curr.before(maxDate)) {
                errors.put("MAI_newRelTypeStartDate_error", "Start date can't be more than three months back");
            }

            if (curr.after(minDate)) {
                errors.put("MAI_newRelTypeStartDate_error", "Start date can't be more than two months in future");
            }
        }
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        Map<String, String> fieldsMap = new HashMap<String, String>();
        if ("OK".equalsIgnoreCase(status)) {
            if (saveRelType(request)) {
                fieldsMap.put("dependantStatus", "ok");
                fieldsMap.put("MAI_RelType", getRelationshipTypeValue(request.getParameter("MAI_newRelType"), lookUpsClient));

                additionalData = "\"message\":\"Member Relationship Type Updated\"";
            } else {
                status = "ERROR";
                fieldsMap.put("dependantStatus", "error");
                additionalData = "\"message\":\"There was an error updating the dependant finacial details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, TabUtils.convertMapToJSON(fieldsMap), additionalData);
    }
    
    private String validateAndSaveStudent(HttpServletRequest request) {
        Map<String, String> errors = MemberMainMapping.validateStudentDependant(request);

        if (request.getParameter("MAI_newStudentStartDate") != null && !request.getParameter("MAI_newStudentStartDate").isEmpty()) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newStudentStartDate"));
            Calendar past = Calendar.getInstance();
            Calendar future = Calendar.getInstance();
            Calendar optionDate = Calendar.getInstance();
            optionDate.setTime(startDate);
            past.add(Calendar.MONTH, -3);
            future.add(Calendar.MONTH, 2);

            Date maxDate = past.getTime();
            Date minDate = future.getTime();
            Date curr = optionDate.getTime();

            if (curr.before(maxDate)) {
                errors.put("MAI_newStudentStartDate_error", "Start date can't be more than three months back");
            }

            if (curr.after(minDate)) {
                errors.put("MAI_newStudentStartDate_error", "Start date can't be more than two months in future");
            }
        }
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        Map<String, String> fieldsMap = new HashMap<String, String>();
        if ("OK".equalsIgnoreCase(status)) {
            if (saveStudent(request)) {
                fieldsMap.put("dependantStatus", "ok");
                fieldsMap.put("MAI_Student", getYesNoValue(request.getParameter("MAI_newStudent")));

                additionalData = "\"message\":\"Student Dependant Updated\"";
            } else {
                status = "ERROR";
                fieldsMap.put("dependantStatus", "error");
                additionalData = "\"message\":\"There was an error updating the student details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, TabUtils.convertMapToJSON(fieldsMap), additionalData);
    }

    private String validateAndSaveHandicap(HttpServletRequest request) {
        Map<String, String> errors = MemberMainMapping.validateHandicapDependant(request);
        if (request.getParameter("MAI_newHandicapStartDate") != null && !request.getParameter("MAI_newHandicapStartDate").isEmpty()) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newHandicapStartDate"));
            Calendar past = Calendar.getInstance();
            Calendar future = Calendar.getInstance();
            Calendar optionDate = Calendar.getInstance();
            optionDate.setTime(startDate);
            past.add(Calendar.MONTH, -3);
            future.add(Calendar.MONTH, 2);

            Date maxDate = past.getTime();
            Date minDate = future.getTime();
            Date curr = optionDate.getTime();

            if (curr.before(maxDate)) {
                errors.put("MAI_newHandicapStartDate_error", "Start date can't be more than three months back");
            }

            if (curr.after(minDate)) {
                errors.put("MAI_newHandicapStartDate_error", "Start date can't be more than two months in future");
            }
        }
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        Map<String, String> fieldsMap = new HashMap<String, String>();
        if ("OK".equalsIgnoreCase(status)) {
            if (saveHandicap(request)) {
                fieldsMap.put("dependantStatus", "ok");
                fieldsMap.put("MAI_Handicap", getYesNoValue(request.getParameter("MAI_newHandicap")));

                additionalData = "\"message\":\"Handiccped Dependent Updated\"";
            } else {
                status = "ERROR";
                fieldsMap.put("dependantStatus", "error");
                additionalData = "\"message\":\"There was an error updating dependent handicap details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, TabUtils.convertMapToJSON(fieldsMap), additionalData);
    }

    private boolean saveIncome(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        HttpSession session = request.getSession();
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);

//        int entityId = MemberMainMapping.getEntityId(request);
        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        Integer depNum = (Integer) session.getAttribute("memberCoverDepNum");
        try {
            int infoId = 4;
            Date startDate = null;
            System.out.println("MAI_newIncome : " + request.getParameter("MAI_newIncome") + "depNumber " + depNum);
            System.out.println("MAI_StartDate : " + request.getParameter("MAI_StartDate"));
            String income = request.getParameter("MAI_newIncome");
            if (request.getParameter("MAI_StartDate") != null && !request.getParameter("MAI_StartDate").isEmpty()) {
                startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_StartDate"));
            }
            System.out.println("coverNumber : " + session.getAttribute("coverNumber"));
            String coverNumber = (String) session.getAttribute("coverNumber");
            port.saveCoverAddInfo(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), income, infoId, sec);
            port.updateContributionsAndBenefits(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), TabUtils.getSecurity(request));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean savePaymentMethod(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        HttpSession session = request.getSession();
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);

//        int entityId = MemberMainMapping.getEntityId(request);

        try {
            int infoId = 8;
            Date startDate = null;
            System.out.println("MAI_newPayment : " + request.getParameter("MAI_newPayment"));
            System.out.println("MAI_newPaymentStartDate : " + request.getParameter("MAI_newPaymentStartDate"));
            String income = request.getParameter("MAI_newPayment");
            if (request.getParameter("MAI_newPaymentStartDate") != null && !request.getParameter("MAI_newPaymentStartDate").isEmpty()) {
                startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newPaymentStartDate"));
            }
            System.out.println("coverNumber : " + session.getAttribute("coverNumber"));
            String coverNumber = (String) session.getAttribute("coverNumber");
            port.saveCoverAddInfo(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), income, infoId, sec);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean saveDebitDate(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        HttpSession session = request.getSession();
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);

//        int entityId = MemberMainMapping.getEntityId(request);

        try {
            int infoId = 7;
            Date startDate = null;
            System.out.println("MAI_newDebitDate : " + request.getParameter("MAI_newDebitDate"));
            System.out.println("MAI_newDebitDateStartDate : " + request.getParameter("MAI_newDebitDateStartDate"));
            String income = request.getParameter("MAI_newDebitDate");
            if (request.getParameter("MAI_newDebitDateStartDate") != null && !request.getParameter("MAI_newDebitDateStartDate").isEmpty()) {
                startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newDebitDateStartDate"));
            }
            System.out.println("coverNumber : " + session.getAttribute("coverNumber"));
            String coverNumber = (String) session.getAttribute("coverNumber");
            port.saveCoverAddInfo(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), income, infoId, sec);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean saveSubsidy(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        HttpSession session = request.getSession();
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);

//        int entityId = MemberMainMapping.getEntityId(request);

        try {
            int infoId = 6;
            Date startDate = null;
            System.out.println("MAI_newSubsidy : " + request.getParameter("MAI_newSubsidy"));
            System.out.println("MAI_StartDate : " + request.getParameter("MAI_StartDate"));
            String subsidy = request.getParameter("MAI_newSubsidy");
            if (request.getParameter("MAI_StartDate") != null && !request.getParameter("MAI_StartDate").isEmpty()) {
                startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_StartDate"));
            }
            System.out.println("coverNumber : " + session.getAttribute("coverNumber"));
            String coverNumber = (String) session.getAttribute("coverNumber");
            port.saveCoverAddInfo(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), subsidy, infoId, sec);
            port.updateContributionsAndBenefits(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), TabUtils.getSecurity(request));
            
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean saveDepType(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        HttpSession session = request.getSession();
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);
        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
//        int entityId = MemberMainMapping.getEntityId(request);
        int depNum = (Integer) session.getAttribute("memberCoverDepNum");
        String depType = null;
        String coverNumber = (String) session.getAttribute("coverNumber");
        int action = new Integer(request.getParameter("depTypeSelect"));
        try {
            Date startDate = null;
            //String income = request.getParameter("MAI_newIncome");

            System.out.println("Entity Id : " + session.getAttribute("memberCoverEntityId"));
            Integer entityId = (Integer) session.getAttribute("memberCoverEntityId");
            if (action == 2) {
                depType = request.getParameter("MAI_newDependantType");
                if (request.getParameter("MAI_newDependantTypeStartDate2") != null && !request.getParameter("MAI_newDependantTypeStartDate2").isEmpty()) {
                    startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newDependantTypeStartDate2"));
                }
            } else {
                depType = ((Integer) session.getAttribute("memberCoverDepType")).toString();
                if (request.getParameter("MAI_newDependantTypeStartDate") != null && !request.getParameter("MAI_newDependantTypeStartDate").isEmpty()) {
                    startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newDependantTypeStartDate"));
                }
            }
            port.changeDependantAdditionalInfo(new Integer(entityId), 1, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), depType, neoUser.getUserId());
            port.updateContributionsAndBenefits(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), TabUtils.getSecurity(request));


            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean saveFinType(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        HttpSession session = request.getSession();
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);

//        int entityId = MemberMainMapping.getEntityId(request);
        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        Integer depNum = (Integer) session.getAttribute("memberCoverDepNum");
        String coverNumber = (String) session.getAttribute("coverNumber");
        try {
            Date startDate = null;

            if (request.getParameter("MAI_newFinanciallyDependantStartDate") != null && !request.getParameter("MAI_newFinanciallyDependantStartDate").isEmpty()) {
                startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newFinanciallyDependantStartDate"));
            }
            System.out.println("Entity Id : " + session.getAttribute("memberCoverEntityId"));
            Integer entityId = (Integer) session.getAttribute("memberCoverEntityId");
            //port.saveCoverAddInfo(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), income, sec);
            port.changeDependantAdditionalInfo(new Integer(entityId), 5, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), request.getParameter("MAI_newFinanciallyDependant"), neoUser.getUserId());
            port.updateContributionsAndBenefits(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), TabUtils.getSecurity(request));



            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    private boolean saveRelType(HttpServletRequest request) {
        System.out.println("saveRelType called");
        System.out.println("saveRelType : "+request.getParameter("MAI_newRelType"));
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        HttpSession session = request.getSession();
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);

        String coverNumber = (String) session.getAttribute("coverNumber");
        try {
            Date startDate = null;

            if (request.getParameter("MAI_newRelTypeStartDate") != null && !request.getParameter("MAI_newRelTypeStartDate").isEmpty()) {
                startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newRelTypeStartDate"));
            }
            System.out.println("Entity Id : " + session.getAttribute("memberCoverEntityId"));
            Integer entityId = (Integer) session.getAttribute("memberCoverEntityId");
            //port.saveCoverAddInfo(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), income, sec);
            port.changeDependantAdditionalInfo(new Integer(entityId), 213, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), request.getParameter("MAI_newRelType"), neoUser.getUserId());
            port.updateContributionsAndBenefits(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), TabUtils.getSecurity(request));

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean saveStudent(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        HttpSession session = request.getSession();
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);
        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
//        int entityId = MemberMainMapping.getEntityId(request);
        Integer depNum = (Integer) session.getAttribute("memberCoverDepNum");
        String coverNumber = (String) session.getAttribute("coverNumber");

        try {
            Date startDate = null;
            System.out.println("MAI_newStudent : " + request.getParameter("MAI_newStudent"));
            System.out.println("MAI_newStudentStartDate : " + request.getParameter("MAI_newStudentStartDate"));
            //String income = request.getParameter("MAI_newIncome");
            if (request.getParameter("MAI_newStudentStartDate") != null && !request.getParameter("MAI_newStudentStartDate").isEmpty()) {
                startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newStudentStartDate"));
            }
            System.out.println("Entity Id : " + session.getAttribute("memberCoverEntityId"));
            Integer entityId = (Integer) session.getAttribute("memberCoverEntityId");
            //port.saveCoverAddInfo(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), income, sec);
            port.changeDependantAdditionalInfo(new Integer(entityId), 2, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), request.getParameter("MAI_newStudent"), neoUser.getUserId());
            port.updateContributionsAndBenefits(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), TabUtils.getSecurity(request));


            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean saveHandicap(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        HttpSession session = request.getSession();
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);

//        int entityId = MemberMainMapping.getEntityId(request);
        List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
        Integer depNum = (Integer) session.getAttribute("memberCoverDepNum");
        String coverNumber = (String) session.getAttribute("coverNumber");
        try {
            Date startDate = null;
            System.out.println("MAI_newHandicap : " + request.getParameter("MAI_newHandicap"));
            System.out.println("MAI_newHandicapStartDate : " + request.getParameter("MAI_newHandicapStartDate"));
            //String income = request.getParameter("MAI_newHandicapStartDate");
            if (request.getParameter("MAI_newHandicapStartDate") != null && !request.getParameter("MAI_newHandicapStartDate").isEmpty()) {
                startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("MAI_newHandicapStartDate"));
            }
            System.out.println("Entity Id : " + session.getAttribute("memberCoverEntityId"));
            Integer entityId = (Integer) session.getAttribute("memberCoverEntityId");
            //port.saveCoverAddInfo(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), income, sec);
            port.changeDependantAdditionalInfo(new Integer(entityId), 3, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), request.getParameter("MAI_newHandicap"), neoUser.getUserId());
            port.updateContributionsAndBenefits(coverNumber, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), TabUtils.getSecurity(request));


            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private List<Map> getDependantTypes(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Integer depType = (Integer) session.getAttribute("memberCoverDepType");
        List<Map> dlist = new ArrayList();
        Map<String, String> depMap = null;
        if (depType == 19) {
            depMap = new HashMap<String, String>();
            depMap.put("id", "18");
            depMap.put("value", "Adult");
            dlist.add(depMap);
        }

        if (depType == 18) {
            depMap = new HashMap<String, String>();
            depMap.put("id", "19");
            depMap.put("value", "Child");
            dlist.add(depMap);
        }
        return dlist;
//        request.setAttribute("DependentList", dlist);

    }

    private String getYesNoValue(String value) {
        if (value.equals("0")) {
            return "No";
        } else {
            return "Yes";
        }
    }
    
    private static String getRelationshipTypeValue(String value, List<neo.manager.LookupValue> lookUpsClient){
        
        String relationshipType = "";
        
        for (neo.manager.LookupValue lv : lookUpsClient) {
            if (value.equals(lv.getId())) {
                relationshipType = lv.getValue();
                System.out.println("relationshipType: " + relationshipType);
            }
        }
        return relationshipType;
//        if(value == null || value.isEmpty()){
//            return "";
//        }else if (value.equals("1")) {
//            return "Wife";
//        } else if (value.equals("2")) {
//            return "Husband";
//        } else if (value.equals("3")) {
//            return "Daughter";
//        } else if (value.equals("4")) {
//            return "Son";
//        } else {
//            return "";
//        }
    }

    private String getDependantTypeValue(String value) {
        if (value.equals("18")) {
            return "Adult";
        } else {
            return "Child";
        }
    }

    private String getPaymentMethod(String value) {
        if (value.equals("1")) {
            return "Debit Order";
        } else {
            return "Electronic Transfer";
        }
    }

    public Integer calculateAge(Date date) {
        if (date == null) {
            return null;
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date);
        Calendar cal2 = Calendar.getInstance();
        int i = 0;
        while (cal1.before(cal2)) {
            cal1.add(Calendar.YEAR, 1);
            i += 1;
        }
        return i - 1;
    }

    private static String getDebitDate(String value) {
        String debitOrderDate = "";
        List<neo.manager.LookupValue> lookUpsClient = service.getNeoManagerBeanPort().findCodeTableForLookupType(197);
        for (neo.manager.LookupValue lv : lookUpsClient) {
            if (value.equals(lv.getId())) {
                debitOrderDate = lv.getValue();
                System.out.println("debitOrderDate: " + debitOrderDate);
            }
        }
        return debitOrderDate;
    }
//        if (value.equals("1")) {
//            return "31th";
//        } else if (value.equals("2")) {
//            return "1st";
//        } else if (value.equals("3")) {
//            return "5th";
//        } else if (value.equals("4")) {
//            return "6th";
//        } else {
//            return "";
//        }
//    }

    public boolean checkIfNumber(String in) {

        try {

            Double.parseDouble(in);

        } catch (NumberFormatException ex) {
            return false;
        }

        return true;
    }

    @Override
    public String getName() {
        return "SaveMemberAdditionalInfoCommand";
    }
}

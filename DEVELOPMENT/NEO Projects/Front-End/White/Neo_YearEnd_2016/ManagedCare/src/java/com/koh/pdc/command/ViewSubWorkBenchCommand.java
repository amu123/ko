/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverDetails;
import neo.manager.EventDetails;
import neo.manager.NeoManagerBean;
import neo.manager.PdcSearchCriteria;

/**
 *
 * @author dewaldo
 */
public class ViewSubWorkBenchCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        //Clearing session from MainList attributes

        String returnflag = String.valueOf(session.getAttribute("returnFlag"));

        if (returnflag == null || returnflag.trim().equals("") || returnflag.trim().equals("null")) {
            session.removeAttribute("subWorkBenchResult");

            NeoManagerBean port = service.getNeoManagerBeanPort();

            PdcSearchCriteria search = new PdcSearchCriteria();
            search.setWorkListID(new Integer(request.getParameter("workListID")));
            Collection<EventDetails> eventList = port.findWorkbenchEvents(search);

            session.setAttribute("workListId", request.getParameter("workListID"));

            for (EventDetails ed : eventList) {

                if (ed.getAuthId() != 0) {
                    try {
                        CoverDetails cd = port.getDependentForCoverByDate(ed.getCoverNumber(), null, ed.getDependentNumber());

                        boolean reject = false;
                        if (cd.getEntityId() == 0) {
                            System.out.println("rejected entityId = " + cd.getEntityId());
                            reject = true;
                        }

                        if (cd.getStatus().trim().equalsIgnoreCase("") || cd.getStatus().trim().equalsIgnoreCase("resign")) {
                            System.out.println("cd.getStatus() = " + cd.getStatus());
                            reject = true;
                        }

                        if (!reject) {
                            ed.setOperation("ForwardToPreAuthConfirmation");
                        } else {
                            ed.setOperation("ForwardToPreAuthRejectedConfirmation");
                        }

                    } catch (Exception e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    }
                }
            }

            System.out.println("eventList size: " + eventList.size());
            session.setAttribute("subWorkBenchResult", eventList);
        }

        try {
            session.removeAttribute("returnFlag");
            RequestDispatcher dispatcher = context.getRequestDispatcher("/PDC/ViewSubWorkBench.jsp");
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "ViewSubWorkBenchCommand";
    }
}

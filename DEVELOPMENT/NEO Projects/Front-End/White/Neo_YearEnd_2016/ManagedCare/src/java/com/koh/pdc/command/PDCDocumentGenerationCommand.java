/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import agility.za.documentenginetype.BaseDocumentResponse;
import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;

/**
 *
 * @author shimanem
 */
public class PDCDocumentGenerationCommand extends NeoCommand {

    private static final Logger LOG = Logger.getLogger(PDCDocumentGenerationCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        //PrintWriter writer = null;
        HttpSession session = request.getSession();

        try {

            NeoManagerBean port = service.getNeoManagerBeanPort();
            System.out.println(">>> PDC DOCUMENT GENERATION COMMAND  <<<");

            int entityID = Integer.parseInt(String.valueOf(session.getAttribute("entityId")));
            String coverNumber = (String) session.getAttribute("policyNumber");
            String productName = (String) session.getAttribute("productName");
            int treatmentValue = Integer.parseInt(String.valueOf(request.getParameter("docId")));
            String docName = (String) request.getParameter("docName");
            int treatmentTypeID = Integer.parseInt(String.valueOf(request.getParameter("treatmentTypeID")));

            String outputLocationForSession;
            //System.out.println("\nVALUES IN THE FRONTEND <<<< \nEntity : " + entityID + "\nMember Number : " + coverNumber + "\nProduct Name : " + productName + "\nTreatment TYPEID : " + treatmentTypeID + "\nTreatment Value (Doc ID): " + treatmentValue + "\nDocument Name : " + docName);

            if (treatmentTypeID == 290 || treatmentTypeID == 291) {
                BaseDocumentResponse res = port.generatePDCBenefitLetter(productName, entityID, coverNumber, treatmentTypeID, treatmentValue);
                if (res.isIsSuccess()) {
                    System.out.println("FILE LOCATION : >> " + res.getFolderAndFile());
                    outputLocationForSession = docName + treatmentValue;

                    session.setAttribute(outputLocationForSession, res.getFolderAndFile());

                    session.setAttribute("filePath", res.getFolderAndFile());
                    session.setAttribute("type", treatmentTypeID);
                    session.setAttribute("docValueID", treatmentValue);
                    //writer.print("Done#");

                } else {
                    //writer.print("Error");
                }
            }

            try {
                RequestDispatcher dispatcher = request.getRequestDispatcher("/PDC/PDCDocumentGeneration.jsp");
                dispatcher.forward(request, response);

            } catch (ServletException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            //writer.close();
        }
        return null;

    }

    @Override
    public String getName() {
        return "PDCDocumentGenerationCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ForwardToPreAuthCommand extends NeoCommand {
    
    private Logger log = Logger.getLogger(ForwardToPreAuthCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);

        log.info("Inside ForwardToPreAuthCommand");

        try {
            String nextJSP = "/PreAuth/PreAuthSearch.jsp";
            RequestDispatcher dispacther = context.getRequestDispatcher(nextJSP);
            dispacther.forward(request, response);

        }catch(Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {

        return "ForwardToPreAuthCommand";
    }

}

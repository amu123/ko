/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.tags;

import agility.za.documentprinting.DocumentPrintingRequest;
import agility.za.documentprinting.DocumentPrintingResponse;
import agility.za.documentprinting.PrintingType;
import com.koh.command.NeoCommand;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CommunicationLog;
import neo.manager.CoverDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Christo
 */
public class MemberCommunicationList extends TagSupport {

    private String command;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            DocumentPrintingRequest request = new DocumentPrintingRequest();
            String memberEntityId = req.getParameter("memberEntityId");

            String displayOption = (String) req.getAttribute("displayOption");
            if (displayOption == null || displayOption.equals("")) {
                displayOption = "0";
            }
//            request.setFilter(displayOption);
//            fileListType.setFolder(new PropertiesReader().getProperty("PrintPoolFolder"));
//            request.setFileList(fileListType);
//            request.setSrcDrive(new PropertiesReader().getProperty("PrintPoolDrive"));

            NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

            List<LookupValue> values = port.getCodeTable(220);
            System.out.println("Values size = " + values.size());
            LookupValue lookupValue = new LookupValue();
            lookupValue.setId("0");
            lookupValue.setValue("All");
            values.add(lookupValue);
            out.println("<input type=\"hidden\" name=\"memberEntityId\" id=\"memberEntityId\" value=\"" + memberEntityId + "\" />");
            out.println("<label class=\"header\">Search Results</label><br/><br/>");
            out.println("<table id=\"commLog\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><td><select style=\"width:215px\" size=\"1\" name=\"displayOption\" value=\"" + displayOption + "\" >");
            for (LookupValue val : values) {
                if (displayOption.equalsIgnoreCase(val.getId())) {
                    out.println("<option selected=\"selected\" value=\"" + val.getId() + "\" >" + val.getValue() + "</option>");
                } else {
                    out.println("<option value=\"" + val.getId() + "\" >" + val.getValue() + "</option>");
                }
            }

            out.println("</select> </td>");
            out.println("<td><input type=\"button\" id=\"filterD\" name=\"filterD\" value=\"Filter\" onClick=\"submitFormWithAjaxPost(this.form, 'commLog');\" ></input></td></tr>");
            List<CommunicationLog> commLogs = NeoCommand.service.getNeoManagerBeanPort().getCommunicationLogForEntityId(new Integer(memberEntityId), new Integer(displayOption));

            out.println("<tr><th>Sms Log Id</th><th>Contact Details</th><th>Status</th><th>Type</th><th>Attempt Date</th><th>Communication Method</th></tr>");
            if (commLogs != null && commLogs.size() > 0) {
                for (CommunicationLog log : commLogs) {
                    out.println("<tr><td><label class=\"label\">" + log.getClaimsSmsLogId() + "</label></td>"
                            + "<td><label class=\"label\">" + log.getContactDetails() + "</label></td>"
                            + "<td><label class=\"label\">" + log.getStatus() + "</label></td>"
                            + "<td><label class=\"label\">" + convertType(log.getClaimNumber()) + "</label></td>"
                            + "<td><label class=\"label\">" + convertDate(log.getAttemptDate()) + "</label></td>"
                            + "<td><label class=\"label\">" + log.getCommunicationType() + "</label></td>");

                    out.println("</tr>");
                }
            }
            out.println("</table>");
            req.setAttribute("memberEntityId", memberEntityId);
            System.out.println("--End tag--");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in PrintPoolList tag", ex);
        }
        return super.doEndTag();
    }

    private String convertDate(XMLGregorianCalendar cal) {
        if (cal == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//	        xc.setTimezone(getTimeZoneOffset());
        return sdf.format(cal.toGregorianCalendar().getTimeInMillis());

    }

    private String convertType(int id) {
        if (id < 200) {
            NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
            return port.getValueForId(220, ""+id);
        }
        return "" + id;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}

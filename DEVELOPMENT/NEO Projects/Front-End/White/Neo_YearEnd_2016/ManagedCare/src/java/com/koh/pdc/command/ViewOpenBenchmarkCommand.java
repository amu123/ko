/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.Command;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.PdcSearchCriteria;

/**
 *
 * @author princes
 */
public class ViewOpenBenchmarkCommand extends Command {

    private int minResult;
    private int maxResult = 100;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        //System.out.println("entered AlllocateCallHistoryToSessionCommand");
        session.removeAttribute("eventTaskTable"); // to Alt between tables 
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        PdcSearchCriteria search = new PdcSearchCriteria();

        System.out.println("Page 1 : " + search.getMinResult() + " Page : " + search.getMaxResult() + " maxResult " + maxResult + " minResult " + minResult);

        minResult = minResult + 0;
        maxResult = maxResult + 0;
        search.setMinResult(minResult);
        search.setMaxResult(maxResult);

        String coverNumber = request.getParameter("policyNumber");
        String dateFrom = request.getParameter("dateFrom");
        String dateTo = request.getParameter("dateTo");
        String pdcType = request.getParameter("pdcType");
        String riskRating = request.getParameter("riskRating");
        String pdcUser = request.getParameter("pdcUser");
        String eventStatus = request.getParameter("eventStatus");

        if (coverNumber != null) {
            search.setCoverNumber(coverNumber);
        }

        if (!pdcType.equals("0")) {
            search.setPdcType(new Integer(pdcType));
        }

        if (!riskRating.equals("0")) {
            search.setRiskRating(new Integer(riskRating));

        }

        if (!pdcUser.equals("0")) {
            search.setUser(new Integer(pdcUser));

        }

        if (!eventStatus.equals("0")) {
            search.setStatus(new Integer(eventStatus));

        }

        if ((dateFrom != null && !dateFrom.equalsIgnoreCase("")) && (dateTo != null && !dateTo.equalsIgnoreCase(""))) {
            
            Pattern pattern = Pattern.compile("\\s");
            Matcher matcher = pattern.matcher(dateFrom);
            boolean found = matcher.find();
            
            Matcher matcher2 = pattern.matcher(dateTo);
            boolean found2 = matcher2.find();
            
            Date tFrom = null;
            Date tTo = null;
            XMLGregorianCalendar xTreatFrom = null;
            XMLGregorianCalendar xTreatTo = null;

            try {
                
                if (!found) {
                    tFrom = dateFormat.parse(dateFrom);
                } else {
                    tFrom = dateTimeFormat.parse(dateFrom);
                }
                
                if (!found2) {
                    tTo = dateFormat.parse(dateTo);
                } else {
                    tTo = dateTimeFormat.parse(dateTo);
                }

                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                xTreatTo = DateTimeUtils.convertDateToXMLGregorianCalendar(tTo);

                search.setDateFrom(xTreatFrom);
                search.setDateTo(xTreatTo);

            } catch (java.text.ParseException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }

        }

        session.setAttribute("search", search);
        try {
            String nextJSP = "/PDC/ViewWorkBench.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "ViewOpenBenchmarkCommand";
    }
}

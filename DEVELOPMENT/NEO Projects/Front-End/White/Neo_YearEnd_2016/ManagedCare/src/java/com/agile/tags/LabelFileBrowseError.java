/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author josephm
 */
public class LabelFileBrowseError extends TagSupport {
    private String elementName;
    private String displayName;
    private String valueFromSession = "no";
    private String valueFromRequest = "";
    private String mandatory = "no";

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
  public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();

        try {
            String validations = "";
            String sessionValerror = "";
            String sessionVal = "";

            if (valueFromSession.equalsIgnoreCase("Yes")) {

                if (session.getAttribute(elementName) != null) {
                    sessionVal = "" + session.getAttribute(elementName);
                }

                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                }

                out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input style=\"background:#ebebe4 \" type=\"file\" class=\"button\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"" + sessionVal + "\" size=\"30\" " + "/></td>");

            } else {
                if (!valueFromRequest.equalsIgnoreCase("")) {
                    out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"file\" class=\"button\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"" + req.getAttribute(valueFromRequest) + "\" size=\"30\" " + "/></td>");
                } else {
                    out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"file\" class=\"button\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"\" size=\"30\" " + "/></td>");
                }
            }

            if (mandatory.equalsIgnoreCase("yes")) {
                out.print("<td><label class=\"red\">*</label></td>");
            }
            else {
                out.print("<td></td>");
            }

            if (valueFromSession.equalsIgnoreCase("Yes") && sessionValerror != null && !sessionValerror.equalsIgnoreCase("null") && !sessionValerror.isEmpty()) {
                out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
            } else {
                out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
            }

            String att = (String) pageContext.getAttribute("validate");
            if (att != null) {
                pageContext.setAttribute("validate", att + "" + validations);
            } else {
                pageContext.setAttribute("validate", "" + validations);
            }

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelFileBrowseBoxError tag", ex);
        }
        return super.doEndTag();
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setValueFromSession(String valueFromSession) {
        this.valueFromSession = valueFromSession;
    }

    public void setValueFromRequest(String valueFromRequest) {
        this.valueFromRequest = valueFromRequest;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.auth.dto.MemberSearchResult;
import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.Collection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ForwardToMemberDetailsCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(ForwardToMemberDetailsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        logger.info("Inside ForwardToMemberDetailsCommand");

        ViewCoverClaimsCommand ccComm = new ViewCoverClaimsCommand();
        ccComm.execute(request, response, context);

        return null;
    }

    @Override
    public String getName() {

        return "ForwardToMemberDetailsCommand";
    }
}

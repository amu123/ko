/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.command;

import com.agile.security.webservice.DentalTCode;
import com.agile.security.webservice.TCode;
import com.koh.command.FECommand;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author whauger
 */

public class SearchTCodeCommand extends FECommand {

     @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        String tCode = request.getParameter("code");
        String dentalCode = request.getParameter("dental");
       
        List<TCode> tcodeList = new ArrayList<TCode>();
        List<DentalTCode> dentalTcodeList = new ArrayList<DentalTCode>();
        try {
            if (tCode != null && !tCode.equals(""))
            {
                tcodeList = service.getAgileManagerPort().findTCodeTable(tCode);
                request.setAttribute("searchTCodeResult", tcodeList);
            }
            else if (dentalCode != null && !dentalCode.equals(""))
            {
                dentalTcodeList = service.getAgileManagerPort().findTCodeTablesForDentalCode(new Integer(dentalCode).intValue());
                request.setAttribute("searchDentalTCodeResult", dentalTcodeList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*
        TariffSearchResult r = new TariffSearchResult();
        r.setCode("10011");
        r.setDescription("Expensive");

        TariffSearchResult r2 = new TariffSearchResult();
        r2.setCode("20022");
        r2.setDescription("Very Expensive");

        tariffs.add(r);
        tariffs.add(r2);
        */

        //request.setAttribute("searchTCodeResult", tcodeList);
        try {
            String nextJSP = "/Auth/TCodeSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SearchTCodeCommand";
    }

}

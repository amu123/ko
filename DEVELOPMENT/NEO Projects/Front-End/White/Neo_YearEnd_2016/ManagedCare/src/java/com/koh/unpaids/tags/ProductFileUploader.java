/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.unpaids.tags;

import com.koh.command.NeoCommand;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.Product;

/**
 *
 * @author dewaldo
 */
public class ProductFileUploader extends TagSupport {

    private static final long serialVersionUID = 1L;

    public String action;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     * @return 
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        try {

            @SuppressWarnings("static-access")
            List<Product> prodList = NeoCommand.service.getNeoManagerBeanPort().fetchAllProducts();

            if (prodList != null) {
                //Add upload location
                for (Product prod : prodList) {
                    out.println("<form action=\"/ManagedCare/AgileController?opperation=" + getAction() + "&schemeOption=" + prod.getProductId() + "\" enctype=\"multipart/form-data\" method=\"POST\">");
                    out.println("<br/>");
                    out.println("<table class=\"list\" style=\"border-collapse:collapse; border-width:1px; width:500px;\"><tr><th>" + prod.getProductName() + " File Upload </th></tr></table>");
                    out.println("<table class=\"list\" style=\"border-collapse:collapse; border-width:1px; width:500px;\">");
                    out.println("<tr><input type=\"file\" name=\"fileName\" size=\"75\"/></tr>");
                    out.println("<tr><div id=\"load" + prod.getProductId() + "\" style=\"width:500px\"></div></tr>");
                    out.println("<tr><br/><input style=\"position:absolute\" type=\"submit\" id=\"submit" + prod.getProductId() + "\" value=\"Upload\" onClick=\"showLoad" + prod.getProductId() + "();\"/></tr></table>");
                    out.println("<br/><br/></form>");

                    out.println("<script>");
                    out.println("function showLoad" + prod.getProductId() + "() {");
                    out.println("document.getElementById(\"load" + prod.getProductId() + "\").innerHTML = \"Processing...\";}");
                    out.println("</script>");
                }
            }

        } catch (java.io.IOException ex) {
            throw new JspException("Error in ProductFileUploader tag", ex);
        }

        return super.doEndTag();
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }
}

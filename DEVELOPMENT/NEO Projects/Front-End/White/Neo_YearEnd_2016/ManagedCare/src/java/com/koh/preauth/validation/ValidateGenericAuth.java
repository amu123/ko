/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.validation;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthTariffDetails;
import neo.manager.Diagnosis;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.ProviderSearchDetails;

/**
 *
 * @author johanl
 */
public class ValidateGenericAuth extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);

        PrintWriter out = null;
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String client = context.getAttribute("Client").toString();
        String errorResponse = "";
        String error = "";
        int errorCount = 0;
        boolean orthoFlag = false;
        boolean noTariffFlag = false;

        String authDate = "" + session.getAttribute("authDate");
        String memberNo = "" + session.getAttribute("memberNum_text");
        String deplist = "" + session.getAttribute("depListValues");
        String treatProv = "" + session.getAttribute("treatingProvider_text");
        String pICD = "" + session.getAttribute("primaryICD_text");
        List<AuthTariffDetails> tariffList = (List<AuthTariffDetails>) session.getAttribute("tariffListArray");
        List<AuthTariffDetails> nappiList = (List<AuthTariffDetails>) session.getAttribute("newNappiListArray");
        LookupValue ben = (LookupValue) session.getAttribute("benAllocated");
        String reqName = "" + session.getAttribute("requestorName");
        String reqRelationship = "" + session.getAttribute("requestorRelationship");
        String reqReason = "" + session.getAttribute("requestorReason");
        String reqContact = "" + session.getAttribute("requestorContact");
        String authType = "" + session.getAttribute("authType");
        String authPeriod = "" + session.getAttribute("authPeriod");
        String authMonthPeriod = "" + session.getAttribute("authMonthPeriod");
        String pmb = "" + session.getAttribute("pmb");
        String copay = "" + session.getAttribute("copay");
        String isPMB = "" + session.getAttribute("isPMB");
        String foundPMB = "" + session.getAttribute("foundPMB");

        if (authPeriod == null || authPeriod.trim().equalsIgnoreCase("99")) {
            errorCount++;
            error = error + "|authPeriod:Authorsation Period is Mandatory";
        } else {
            //test auth period future date
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(System.currentTimeMillis()));
            cal.add(Calendar.MONTH, 4);
            Date fDate = cal.getTime();

            int aYear = Integer.parseInt(authPeriod);
            int aMonth = Integer.parseInt(authMonthPeriod);
            int fYear = Integer.parseInt(sdf.format(fDate).substring(0, 4));
            int fMonth = Integer.parseInt(sdf.format(fDate).substring(5, 7));

            if (aYear == fYear && aMonth > fMonth) {
                errorCount++;
                error = error + "|authMonthPeriod:Invalid Future Date Month Period";
            }
        }

        if (authType == null || authType.trim().equalsIgnoreCase("99")) {
            errorCount++;
            error = error + "|authType:Authorsation Type is Mandatory";
        } else if (authType.trim().equals("3")) {
            orthoFlag = true;

        } else if (authType.trim().equalsIgnoreCase("6") || authType.trim().equalsIgnoreCase("7") || authType.trim().equalsIgnoreCase("8") || authType.trim().equalsIgnoreCase("9")
                || authType.trim().equalsIgnoreCase("10")) {
            String authStatus = "" + session.getAttribute("authStatus");
            String authFrom = "" + session.getAttribute("authFromDate");
            String authTo = "" + session.getAttribute("authToDate");

            //Create DateValidation instance 
            PreAuthDateValidation pad = new PreAuthDateValidation();

            if (!authType.trim().equalsIgnoreCase("9") && !authType.trim().equalsIgnoreCase("10")) {
                if (authStatus == null || authStatus.trim().equalsIgnoreCase("99")) {
                    errorCount++;
                    error = error + "|authStatus:Auth Status is Mandatory";
                }
            }

            if (authFrom == null || authFrom.trim().equalsIgnoreCase("")) {
                errorCount++;
                error = error + "|authFromDate:Auth From Date is Mandatory";
            } else {
                String vs = pad.DateValidation(authFrom, authTo, "authFromDate", "authToDate", authType, authPeriod, authMonthPeriod, "no");
                String[] returned = vs.split("\\|");
                boolean validDate = Boolean.parseBoolean(returned[0]);
                if (!validDate) {
                    String errorsReturned = vs.substring(vs.indexOf("|"), vs.length());
                    System.out.println("from date validation error(s) returned = " + errorsReturned);
                    errorCount++;
                    error = error + errorsReturned;
                }
            }
            if (authTo == null || authTo.trim().equalsIgnoreCase("")) {
                errorCount++;
                error = error + "|authToDate:Auth To Date is Mandatory";
            } else {
                String vs = pad.DateValidation(authFrom, authTo, "authFromDate", "authToDate", authType, authPeriod, authMonthPeriod, "no");
                String[] returned = vs.split("\\|");
                boolean validDate = Boolean.parseBoolean(returned[0]);
                if (!validDate) {
                    String errorsReturned = vs.substring(vs.indexOf("|"), vs.length());
                    System.out.println("to date validation error(s) returned = " + errorsReturned);
                    errorCount++;
                    error = error + errorsReturned;
                }
            }

        } else if (authType.trim().equalsIgnoreCase("4") || authType.trim().equalsIgnoreCase("5") || authType.trim().equalsIgnoreCase("6") || authType.trim().equalsIgnoreCase("11")) {
            System.out.println("authType = " + authType);
            noTariffFlag = true;
        }

        if (authType.trim().equalsIgnoreCase("6")) {
            if (nappiList == null || nappiList.size() == 0) {
                errorCount++;
                error = error + "|nappiButton:Nappi Details are Mandatory";
            }
        }

        //radiology pmb validation 
        if (authType.trim().equalsIgnoreCase("9")) {
            if (pmb.equalsIgnoreCase("null") || pmb.trim().equals("99")) {
                errorCount++;
                error = error + "|pmb:PMB is Mandatory";
            }
            if (foundPMB.equalsIgnoreCase("yes")) {
                if (isPMB.equalsIgnoreCase("no")) {
                    errorCount++;
                    error = error + "|pmbButton:PMB detail is Mandatory";
                }
            }

            //copay check
            if (!copay.equalsIgnoreCase("null") && !copay.equalsIgnoreCase("")) {
                boolean coPayError = false;
                //point check
                boolean firstPoint = copay.contains(".");
                if (firstPoint == true) {
                    String[] dotSize = copay.split("\\.");
                    if (dotSize.length > 2) {
                        coPayError = true;
                    }
                }
                //number validation
                if (!client.equalsIgnoreCase("Sechaba")) {
                    if (coPayError != true) {
                        char strChar;
                        String strValidChars = "0123456789.";

                        for (int i = 0; i < copay.length(); i++) {
                            strChar = copay.charAt(i);
                            if (strValidChars.indexOf(strChar) == -1) {
                                errorCount++;
                                error = error + "|coPay:Incorrect Amount = " + copay;
                                break;
                            }
                        }

                    } else {
                        errorCount++;
                        error = error + "|coPay:Amount allows only one .";
                    }
                }

            }
        }

        if (authType.trim().equalsIgnoreCase("7")) {
            String benSubType = "" + session.getAttribute("benSubType");
            if (benSubType == null || benSubType.equalsIgnoreCase("") || benSubType.equalsIgnoreCase("99")) {
                errorCount++;
                error = error + "|benSubType:Benefit Sub Type is Mandatory";
            }

            if (ben == null || ben.getId().equals("")) {
                errorCount++;
                error = error + "|authBenefitButton:Benefit selection is Mandatory";
            }
        }

        if (authDate == null || authDate.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|authDate:Auth Date is Mandatory";
        }

        if (memberNo == null || memberNo.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|memberNum:Member Number is Mandatory";
        }
        if (deplist == null || deplist.trim().equalsIgnoreCase("") || deplist.trim().equalsIgnoreCase("null")) {
            errorCount++;
            error = error + "|memberNum:Dependant Selection is Mandatory";
        }

        if (treatProv == null || treatProv.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|treatingProvider:Treating Provider is Mandatory";
        } else {
            ProviderSearchDetails pd = port.findProviderAjaxDetailsByNumber(treatProv);
            if (pd.getProviderNo() == null || pd.getProviderNo().trim().equals("")) {
                errorCount++;
                error = error + "|treatingProvider:Invalid Treating Provider";
            }
        }

        if (pICD == null || pICD.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|primaryICD:Primary ICD is Mandatory";
        } else {
            boolean checkComma = false;
            checkComma = pICD.contains(",");
            Character firstChar = pICD.charAt(0);
            boolean checkFirst = Character.isLowerCase(firstChar);

            if (checkFirst == true) {
                errorCount++;
                error = error + "|primaryICD:LowerCase ICD is Not Allowed";

            } else if (checkComma == true) {
                errorCount++;
                error = error + "|primaryICD:More Than 1 ICD is Not Allowed";

            } else {
                Diagnosis d = port.getDiagnosisForCode(pICD);
                if (d == null) {
                    errorCount++;
                    error = error + "|primaryICD:No such diagnosis";
                }
            }
        }

        if (noTariffFlag == false) {
            if (tariffList == null || tariffList.size() == 0) {
                errorCount++;
                error = error + "|authTariffButton:Clinical Tariff is Mandatory";
            } else if (tariffList.size() > 1 && orthoFlag == true) {
                errorCount++;
                error = error + "|authTariffButton:Orthodontic Auth Requires Only One Tariff";
            }
        }

        if (reqName == null || reqName.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|requestorName:Requestor Name is Mandatory";
        }

        if (reqRelationship == null || reqRelationship.trim().equalsIgnoreCase("99")) {
            errorCount++;
            error = error + "|requestorRelationship:Requestor Relationship is Mandatory";
        }

        if (reqReason == null || reqReason.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|requestorReason:Requestor Reason is mandatory";
        }

        if (reqContact == null || reqContact.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|requestorContact:Requestor Contact is Mandatory";
        }

        if (errorCount > 0) {
            errorResponse = "ERROR" + error;
        } else if (errorCount == 0) {
            errorResponse = "Done|";
        }
        System.out.println("error " + error);
        try {
            out = response.getWriter();
            out.println(errorResponse);

        } catch (Exception ex) {
            System.out.println("ValidateGenericAuth error : " + ex.getMessage());
        } finally {
            out.close();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ValidateGenericAuth";
    }
}

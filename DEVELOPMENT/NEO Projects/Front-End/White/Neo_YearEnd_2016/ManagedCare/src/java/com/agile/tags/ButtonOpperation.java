/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class ButtonOpperation extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String displayname;
    private String span;
    private String commandName;
    private String align = "center";
    private String type;
    private String javaScript = "";

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {

        try {
            String output;
            if (!javaScript.equalsIgnoreCase("")) {
                output = "<td colspan=\"" + span + "\" align=\"" + align + "\"><button name=\"opperation\" type=\"" + type + "\" " + javaScript + " value=\"" + commandName + "\">" + displayname + "</button></td>";
            } else {
                output = "<td colspan=\"" + span + "\" align=\"" + align + "\"><button name=\"opperation\" type=\"" + type + "\" value=\"" + commandName + "\">" + displayname + "</button></td>";
            }

            System.out.println(output);
            
            pageContext.getOut().write(output);

        } catch (java.io.IOException ex) {
            throw new JspException("Error in ButtonOpperation tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public void setSpan(String span) {
        this.span = span;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setType(String type) {
        this.type = type;
    }

}

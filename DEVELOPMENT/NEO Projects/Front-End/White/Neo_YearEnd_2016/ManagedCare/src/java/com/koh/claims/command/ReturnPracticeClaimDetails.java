/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Johan-NB
 */
public class ReturnPracticeClaimDetails extends NeoCommand{

    private static final Logger logger = Logger.getLogger(ReturnPracticeClaimDetails.class.getName());
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        String onScreen = request.getParameter("onScreen");
        String previousJsp = "";

        if (onScreen != null && !onScreen.equalsIgnoreCase("")) {

            if (onScreen.equalsIgnoreCase("PracticeDetailedClaimLine.jsp")) {
                previousJsp = "/Claims/PracticeClaimsHistoryDetails.jsp";

            } else if (onScreen.equalsIgnoreCase("PracticeClaimsHistoryDetails.jsp")) {
                previousJsp = "/Claims/PracticeClaimSearch.jsp";

            }
            
            try {
                RequestDispatcher dispatcher = context.getRequestDispatcher(previousJsp);
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
        
        return null;
    }

    @Override
    public String getName() {
        return "ReturnPracticeClaimDetails";
    }
    
}

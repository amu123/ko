/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.calltrack.command;

import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CallTrackSearchCriteria;
import neo.manager.CallWorkbenchDetails;

/**
 *
 * @author josephm
 */
public class ProviderCallTrackHistoryCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        Collection<CallWorkbenchDetails>  workbench = new ArrayList<CallWorkbenchDetails>();
       CallTrackSearchCriteria ctSearch = new CallTrackSearchCriteria();

       String providerNumber = request.getParameter("providerNumber");
       String coverNumber = request.getParameter("coverNumber");
       String callNumber = request.getParameter("callNumber");

        if (!providerNumber.trim().equalsIgnoreCase("")) {
            ctSearch.setProviderNumber(providerNumber);
        }
       if(!coverNumber.trim().equalsIgnoreCase("")) {

           ctSearch.setCoverNumber(coverNumber);
       }
       if(!callNumber.trim().equalsIgnoreCase("")) {

           ctSearch.setRefNo(callNumber);
       }

        List<CallWorkbenchDetails> cwList = service.getNeoManagerBeanPort().getCallWorkbenchByCriteria(ctSearch);
        if (cwList.size() > 0) {
        session.setAttribute("searchCallHistory", cwList);
        } else {
       // System.out.println("error with getCallWorkbenchByCriteria, list size = " + cwList.size());
        }
        request.setAttribute("CallHistorySearch", "true");
        
        try {

            String nextJSP = "/Calltrack/ProviderCallTrackingHistory.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        }
        catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ProviderCallTrackHistoryCommand";
    }
}

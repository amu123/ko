/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.LookupUtils;
import com.koh.utils.XmlUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Exception;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Christo
 */
public class GetICD10ListCommand extends NeoCommand {
private Logger logger = Logger.getLogger(this.getClass());
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter out = null;
        String xmlStr = "";
logger.info("------------In GetICD10ListCommand---------------------");
        NeoUser user = (NeoUser) request.getSession().getAttribute("persist_user");
        if (request.getParameter("cdlId") != null && !request.getParameter("cdlId").isEmpty()) {
            try {
            int cdl = new Integer(request.getParameter("cdlId"));
            logger.info("cdl: " + cdl);
            List<neo.manager.LookupValue> lookUps = NeoCommand.service.getNeoManagerBeanPort().getCodeTable( cdl);
logger.info("lookUps: " + lookUps);
            response.setHeader("Cache-Control", "no-cache");
            response.setContentType("text/xml");
            response.setStatus(200);

            @SuppressWarnings("static-access")
            Document doc = new XmlUtils().getDocument();

            Element root = doc.createElement("ICD10Filter");


            for (LookupValue lv : lookUps) {
                Element option = doc.createElement("ICD_10");

                String branchIdName = lv.getId() + "|" + lv.getValue();
                logger.info("branchIdName: " + branchIdName);
                option.setTextContent(branchIdName);

                root.appendChild(option);

            }

            doc.appendChild(root);

                out = response.getWriter();

                try {
                    xmlStr = XmlUtils.getXMLString(doc);
                    logger.info(xmlStr);
                } catch (IOException ex) {
                    ex.printStackTrace();
                    xmlStr = "";

                }
                //System.out.println("product option xml = "+xmlStr);
                out.println(xmlStr);


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetICD10ListCommand";
    }
}

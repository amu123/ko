/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import java.io.PrintWriter;
import neo.manager.LabTariffDetails;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoUser;

/**
 *
 * @author johanl
 */
public class ModifyAuthLabTariffList extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        PrintWriter out = null;
       
        String labTAction = request.getParameter("tAction");
        String tariff = request.getParameter("tariff");
        int labCode = Integer.parseInt(request.getParameter("labCode"));
        double amount = new Double(request.getParameter("tariffAmount"));
        String toothNumber = request.getParameter("toothNum");
        int quantity = Integer.parseInt(request.getParameter("tariffQuantity"));

        List<LabTariffDetails> labTList = (List<LabTariffDetails>) session.getAttribute("DentalLabCodeList");
        List<LabTariffDetails> savedLabList = (List<LabTariffDetails>) session.getAttribute("SavedDentalLabCodeList");
        if (savedLabList == null) {
            savedLabList = new ArrayList<LabTariffDetails>();
        }       
        
        if (labTAction.trim().equalsIgnoreCase("save")) {
            int refCount = 0;
            for (LabTariffDetails labS : savedLabList) {
                if (tariff.trim().equalsIgnoreCase(labS.getClinicalTariff())) {
                    System.out.println("got tariff");
                    if (labCode == labS.getLabCode()) {
                        System.out.println("got lab code");
                        refCount++;
                        break;
                    }
                }
            }
            if (refCount == 0) {
                for (LabTariffDetails labT : labTList) {
                    if (tariff.trim().equalsIgnoreCase(labT.getClinicalTariff())) {
                        if (labCode == labT.getLabCode()) {
                            LabTariffDetails savedLab = new LabTariffDetails();

                            savedLab.setUserId(user.getUserId());
                            savedLab.setLabCodeInd("1");
                            savedLab.setProviderType(labT.getProviderType());
                            savedLab.setClinicalTariff(labT.getClinicalTariff());
                            savedLab.setLabCode(labT.getLabCode());
                            savedLab.setLabCodeDisc(labT.getLabCodeDisc());
                            //savedLab.setTCode(labT.getTCode());
                            savedLab.setLabCodeAmount(amount);
                            savedLab.setLabCodeQuantity(quantity);
                            savedLab.setToothNumber(toothNumber);

                            savedLabList.add(savedLab);

                            break;
                        }
                    }
                }
            }

        } else if (labTAction.trim().equalsIgnoreCase("remove")) {
            for (LabTariffDetails savedLab : savedLabList) {
                if (tariff.trim().equalsIgnoreCase(savedLab.getClinicalTariff())) {
                    if (labCode == savedLab.getLabCode()) {
                        savedLabList.remove(savedLab);
                        break;
                    }
                }
            }
        }
        session.setAttribute("SavedDentalLabCodeList", savedLabList);

        try {
            out = response.getWriter();
            out.println("Done|");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ModifyAuthLabTariffList";
    }
}

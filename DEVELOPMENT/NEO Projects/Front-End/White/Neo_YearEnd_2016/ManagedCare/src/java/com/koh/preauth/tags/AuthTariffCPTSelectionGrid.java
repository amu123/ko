/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthCPTDetails;
import neo.manager.AuthTariffCPT;
import neo.manager.AuthTariffDetails;
import neo.manager.Procedure;

/**
 *
 * @author johanl
 */
public class AuthTariffCPTSelectionGrid extends TagSupport {

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        List<AuthTariffCPT> tcListAll = (List<AuthTariffCPT>) session.getAttribute("mappedListOfCptTariffs");
        List<AuthTariffDetails> tariffList = (List<AuthTariffDetails>) session.getAttribute("tariffListArray");
        List<AuthCPTDetails> cptList = (List<AuthCPTDetails>) session.getAttribute("authCPTListDetails");

        HashMap<String, String> tariffDescMap = new HashMap<String, String>();
        HashMap<String, String> cptMap = new HashMap<String, String>();

        
        for (AuthTariffDetails at : tariffList) {
            tariffDescMap.put(at.getTariffCode(), at.getTariffDesc());
        }
        if (cptList != null && !cptList.isEmpty()) {
            for (AuthCPTDetails cpt : cptList) {
                cptMap.put(cpt.getCptCode(), cpt.getCptCode());
            }
        }

        try {

            out.println("<td colspan=\"5\"><table id=\"tcptSelection\" width=\"600px\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");

            out.println("<tr>"
                    + "<th align=\"left\">Tariff Code</th>"
                    + "<th align=\"left\">Tariff Description</th>"
                    + "<th align=\"left\">CPT</th>"
                    + "<th align=\"left\">CPT description</th>"
                    + "<th align=\"left\"><input id=\"selectAll\" type=\"checkbox\" /></th>"
                    + "</tr>");

            int index = 0;
            for (AuthTariffCPT tc : tcListAll) {
                Procedure proc = NeoCommand.service.getNeoManagerBeanPort().getProcedureForCode(tc.getCptCode());

                String cptDesc = "#ERROR# cpt not found in NEO";
                if (proc != null) {
                    cptDesc = proc.getDescription();
                }

                String tariffDesc = "";
                boolean foundTariff = tariffDescMap.containsKey(tc.getTariffCode());
                if (foundTariff) {
                    tariffDesc = tariffDescMap.get(tc.getTariffCode());
                }

                out.println("<tr id=\"tcRow" + index + "\">"
                        + "<td><label class=\"label\">" + tc.getTariffCode() + "</label></td> "
                        + "<td width=\"250\"><label class=\"label\">" + tariffDesc + "</label></td> "
                        + "<td><label class=\"label\">" + tc.getCptCode() + "</label></td> "
                        + "<td width=\"250\"><label class=\"label\">" + cptDesc + "</label></td> ");

                boolean foundCPT = cptMap.containsKey(tc.getCptCode());
                if (foundCPT) {
                    out.println("<td><input id=\"boxesSelected\" type=\"checkbox\" checked disabled/></td> ");
                } else {
                    out.println("<td><input id=\"boxes\" type=\"checkbox\" /></td> ");
                }

                out.println("</tr>");

                index++;
            }
            out.println("</table></td>");


        } catch (IOException ex) {
            Logger.getLogger(AuthTariffCPTSelectionGrid.class.getName()).log(Level.SEVERE, null, ex);
        }
        return super.doEndTag();
    }
}

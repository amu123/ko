/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.preauth.dto.AuthLabTariffListDisplay;
import com.koh.utils.DateTimeUtils;
import neo.manager.LabTariffDetails;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author johanl
 */
public class AuthLabTariffListDisplayTag extends TagSupport {

    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

//********************** NO LABS ******************************
        try {
            ArrayList<LabTariffDetails> atList = (ArrayList<LabTariffDetails>) session.getAttribute(sessionAttribute);

            
            ArrayList<LabTariffDetails> noLabList = (ArrayList<LabTariffDetails>) session.getAttribute("SavedDentalNoLabCodeList");
            ArrayList<LabTariffDetails> savedLabList = (ArrayList<LabTariffDetails>) session.getAttribute("SavedDentalLabCodeList");

            System.out.println("");

            String lastTariff = "";
            int hiddenCount = 0;
            out.println("<td colspan=\"5\">");

            if (noLabList != null && !noLabList.isEmpty()) {
                out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.println("<tr><th align=\"left\">Tariff Code</th><th align=\"left\">Tooth Number</th></tr>");
                for (LabTariffDetails noLab : noLabList) {
                    String tooth = noLab.getToothNumber();
                    if (tooth == null) {
                        tooth = "";
                    }
                    out.println("<tr><td><label class=\"label\">" + noLab.getClinicalTariff() + "</label></td> "
                            + "<td><label class=\"label\">" + tooth + "</label></td></tr>");
                }
                out.println("</table>");
                out.println("<br></br>");

            }

//******************* LABS *****************************
            if (atList != null && !atList.isEmpty()) {
                Collections.sort(atList, new Comparator<LabTariffDetails>() {

                public int compare(LabTariffDetails o1, LabTariffDetails o2) {
                    if (o1.getClinicalTariff() == null || o2.getClinicalTariff() == null) {
                        return 0;
                    }
                    return o2.getClinicalTariff().compareTo(o1.getClinicalTariff());
                }
            });
                
                System.out.println("session list size for lab = " + atList.size());
                out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");

                out.println("<tr>"
                        + "<th align=\"left\">Tariff Code</th>"
                        + "<th align=\"left\">Lab Code</th>"
                        + "<th align=\"left\">Lab Amount(Sum)</th>"
                        + "</tr>");

                /*
                 * for(int x = 0; x < atList.size(); x++){ String labCodeList =
                 * ""; double tariffTotal = 0.00;
                 *
                 * LabTariffDetails tariff = atList.get(x);
                 *
                 * if (lastTariff.equalsIgnoreCase(tariff.getClinicalTariff()))
                 * { hiddenCount++; out.println("<tr id=\"" + lastTariff +
                 * "hidden" + x + "\" style=\"display:none\"><td></td>");
                 *
                 * } else { lastTariff = tariff.getClinicalTariff(); }
                 */

                String currentTariff = "";
                String labCSV = "";
                double tariffTotal = 0.0d;
                double labTotal = 0.0d;
                String trimmedLabCodeList = "";
                ArrayList<AuthLabTariffListDisplay> labList = new ArrayList<AuthLabTariffListDisplay>();

                int count = 1;
                AuthLabTariffListDisplay testLab = new AuthLabTariffListDisplay();

                for (LabTariffDetails at : atList) {

                    System.out.println("getClinicalTariff = " + at.getClinicalTariff());

                    if (currentTariff.equals("")) {

                        System.out.println("setting currentTariff");

                        currentTariff = at.getClinicalTariff();
                        labCSV += String.valueOf(at.getLabCode()) + ',';
                        tariffTotal += at.getLabCodeAmount();
                    } else if (currentTariff.equals(at.getClinicalTariff())) {

                        System.out.println("tariff still the same");

                        currentTariff = at.getClinicalTariff();
                        labCSV += String.valueOf(at.getLabCode()) + ',';
                        tariffTotal += at.getLabCodeAmount();
                    }

                    if (!currentTariff.equals(at.getClinicalTariff())) {

                        System.out.println("current tariff " + currentTariff + " is changing to = " + at.getClinicalTariff());

                        testLab.setTariff(currentTariff);
                        trimmedLabCodeList = labCSV.substring(0, labCSV.length() - 1);
                        testLab.setLabCodeCSV(trimmedLabCodeList);
                        testLab.setTariffTotal(tariffTotal);
                        labList.add(testLab);

                        labCSV = "";
                        tariffTotal = 0.00;
                        trimmedLabCodeList = "";
                        testLab = new AuthLabTariffListDisplay();

                        currentTariff = at.getClinicalTariff();
                        labCSV += String.valueOf(at.getLabCode()) + ',';
                        tariffTotal += at.getLabCodeAmount();

                    }
                    if (count == atList.size()) {

                        System.out.println("hit end of list");

                        testLab.setTariff(currentTariff);
                        trimmedLabCodeList = labCSV.substring(0, labCSV.length() - 1);
                        testLab.setLabCodeCSV(trimmedLabCodeList);
                        testLab.setTariffTotal(tariffTotal);

                        labList.add(testLab);

                    }
                    count++;
                }

                for (AuthLabTariffListDisplay atDisp : labList) {
                    System.out.println("tariff - " + atDisp.getTariff());
                    System.out.println("labCSV - " + atDisp.getLabCodeCSV());
                }

                //for (int i = 0; i < labList.size(); i++) {
                for (AuthLabTariffListDisplay lab : labList) {
                    //AuthLabTariffListDisplay lab = labList.get(i);
                    labTotal += lab.getTariffTotal();
                    out.println("<tr><td><label class=\"label\">" + lab.getTariff() + "</label></td> ");
                    out.println("<td><label class=\"label\">" + lab.getLabCodeCSV() + "</label></td> ");
                    out.println("<td><label class=\"label\">" + DateTimeUtils.decimalFormat.format(lab.getTariffTotal()) + "</label></td></tr> ");

                }

                /*
                 * for (int i = 0; i < atList.size(); i++) {
                 *
                 * System.out.println("!!!!!!!!!!IN INNER LOOP!!!!!!!!!");
                 *
                 * LabTariffDetails lab = atList.get(i); labCodeList +=
                 * String.valueOf(lab.getLabCode()) + ','; tariffTotal +=
                 * lab.getLabCodeAmount();
                 *
                 * /* String tooth = lab.getToothNumber(); if (tooth == null) {
                 * tooth = ""; } else { tooth = lab.getToothNumber(); }
                 */

                /*
                 * for(int y = 0; y < labCodeArray.length - 1; y++){ labCodeList
                 * += labCodeArray[y]; }
                 */


                //} 
                out.println("<tr><th colspan=\"2\" align=\"left\">Total Lab Amount: " +"</th><th align=\"left\">" + DateTimeUtils.decimalFormat.format(labTotal)+ "</th></tr>"); //TOTAL LABEL!!!!
                out.println("</table>");
                out.println("</td>");
                //}
            }

        } catch (java.io.IOException ex) {
            ex.printStackTrace();
        }

        return super.doEndTag();

    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

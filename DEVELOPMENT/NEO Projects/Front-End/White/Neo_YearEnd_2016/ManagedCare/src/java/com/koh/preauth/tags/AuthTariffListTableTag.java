/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import java.util.ArrayList;
import java.util.Formatter;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author johanl
 */
public class AuthTariffListTableTag extends TagSupport {

    private String sessionAttribute;
    private String commandName;
    private String javaScript;
    private String tariffType;
    private String tAmountF;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();

        try {
//            ArrayList<AuthTariffDetails> atList = (ArrayList<AuthTariffDetails>) session.getAttribute(sessionAttribute);
//
//            if (atList != null && atList.size() != 0) {
//                System.out.println("tariff list size = " + atList.size());
//                for (int i =1; i <= atList.size(); i++) {
//                    AuthTariffDetails at = atList.get(i-1);
//                    out.println("<tr id=\"tariffRow"+at.getTariffCode()+ i +"\"><form>" +
//                                    "<td width=\"200\"><label class=\"label\">" + at.getTariffDesc() + "</label></td> " +
//                                    "<td><label class=\"label\">" + at.getTariffCode() + "</label><input type=\"hidden\" name=\"tTableCode\" id=\"tTableCode\" value=\"" + at.getTariffCode() + "\"/></td> " +
//                                    "<td><label class=\"label\">" + at.getQuantity() + "</label><input type=\"hidden\" name=\"tTableQuan\" id=\"tTableQuan\" value=\"" + at.getQuantity() + "\"/></td> " +
//                                    "<td><label class=\"label\">" + at.getAmount() + "</label></td> " +
//                                    "<td><button type=\"button\" onClick=\"ModifyTariffFromList(" + i + ");\" >Modify</button></td>" +
//                                    "<td><button type=\"button\" onClick=\"RemoveTariffFromList(" + i + ");\" >Remove</button></td>" +
//                               "</form></tr>");
//                }
//            }
//            out.println("</table></td>");

            out.println("<td colspan=\"6\"><table width=\"600\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            ArrayList<AuthTariffDetails> atList = (ArrayList<AuthTariffDetails>) session.getAttribute(sessionAttribute);
            if (atList != null) {
                out.println("<tr><th>Tariff Code</th><th>Tariff Discription</th><th>Tariff Quantity</th><th>Tariff Amount</th><th>Provider</th><th></th><th></th></tr>");
                for (int i =1; i <= atList.size(); i++){
                    AuthTariffDetails at = atList.get(i-1);
                    Formatter bs = new Formatter();
                    Formatter format = bs.format("%10.2f",at.getAmount() );
                    String tAmountF = format.toString();
                    out.println("<tr id=\"tariffRow"+ at.getTariffCode() + i +"\"><form>");
                    out.println("<td><label class=\"label\">" + at.getTariffCode() + "</label><input type=\"hidden\" name=\"tTableCode\" id=\"tTableCode\" value=\"" + at.getTariffCode() + "\"/></td>");
                    out.println("<td width=\"200\"><label class=\"label\">" + at.getTariffDesc() + "</label></td>");
                    out.println("<td><label class=\"label\">" + at.getQuantity()  + "</label><input type=\"hidden\" name=\"tTableQuan\" id=\"tTableQuan\" value=\"" + at.getQuantity()  + "\"/></td>");
                    out.println("<td><label class=\"label\">" + tAmountF  + "</label></td>");
                    out.println("<td><label class=\"label\">" + at.getProviderNum()  + "</label></td>");
                    out.println("<td><button type=\"button\" onClick=\"ModifyTariffFromList(" + i + ");\" >Modify</button></td>");
                    out.println("<td><button type=\"button\" onClick=\"RemoveTariffFromList(" + i + ");\" >Remove</button></td>");
                    out.println("</form></tr>");
                }
            }
            out.println("</table></td>");


        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberSearchTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setTariffType(String tariffType) {
        this.tariffType = tariffType;
    }
    
     public void settAmountF(String tAmountF){
        this.tAmountF = tAmountF;
    }
}

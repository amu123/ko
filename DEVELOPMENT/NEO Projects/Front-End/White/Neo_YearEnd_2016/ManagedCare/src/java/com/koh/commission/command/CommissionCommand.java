/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.commission.command;

import com.koh.command.NeoCommand;
import com.koh.contribution.command.GenerateContributionCommand;
import com.koh.contribution.command.PaymentAllocationCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.FileData;
import neo.manager.NeoManagerBean;

/**
 *
 * @author yuganp
 */
public class CommissionCommand extends NeoCommand {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd");

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            String s = request.getParameter("command");
            if (s != null && !s.isEmpty()) {
                System.out.println("command : " + s);
                if (s.equalsIgnoreCase("genCommission")) {
                    generateCommissions(request, response, context);
                } else if (s.equalsIgnoreCase("genCommissionReport")) {
                    generateComissionReport(request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(PaymentAllocationCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void generateCommissions(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        try {
            String name = request.getParameter("Commission_Date");
            int id = 0;
            javax.xml.datatype.XMLGregorianCalendar date = DateTimeUtils.convertDateToXMLGregorianCalendar(DATE_FORMAT.parse(name));
            boolean result = port.generateCommissions(date, TabUtils.getSecurity(request));
            String   status = "OK";
            String   additionalData = "\"message\":\"Comissions have been generated" +  "\"";
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, null, null, additionalData));
        } catch (Exception ex) {
            Logger.getLogger(GenerateContributionCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    private void generateComissionReport(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        FileData report = port.getCommisionRunReport();
        if (report == null || report.getData() == null) {
            
        } else {
            byte[] data = report.getData();
            ServletOutputStream os = response.getOutputStream();
            String mimetype = context.getMimeType(report.getFileName());
            response.setContentType( (mimetype != null) ? mimetype : "application/octet-stream" );
            response.setContentLength(data.length);
            response.setHeader( "Content-Disposition", "attachment; filename=\"" + report.getFileName() + "\"" );
            os.write(data);
            os.flush();
            os.close();
        }
    }
    
    @Override
    public String getName() {
       return "CommissionCommand";
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.auth.dto.PracticeSearchResult;
import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.employer.command.TabUtils;
import com.koh.member.application.MemberAppDepMapping;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class MemberDependantAppCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            NeoManagerBean port = service.getNeoManagerBeanPort();
            String s = request.getParameter("buttonPressed");
            if (s != null && !s.isEmpty()) {
                System.out.println("buttonPressed : " + s);
                if (s.equalsIgnoreCase("Add Dependant")) {
                    addDetails(port, request, response, context);
                } else if (s.equalsIgnoreCase("EditButton")) {
                    editDetails(port, request, response, context);
                } else if (s.equalsIgnoreCase("RemoveButton")) {
                    removeDetails(port, request, response, context);
                } else if (s.equalsIgnoreCase("SaveButton")) {
                    save(port, request, response, context);
                }
            }

            if (s != null && !s.isEmpty()) {
                if (s.equalsIgnoreCase("doctorSearch".trim())) {
                    try {
                        searchDoctor(port, request, response, context);
                    } catch (ServletException ex) {
                        Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return null;
                }
            }

            if (s != null && !s.isEmpty()) {
                if (s.equalsIgnoreCase("dentistSearch".trim())) {
                    try {
                        searchDentist(port, request, response, context);
                    } catch (ServletException ex) {
                        Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return null;
                }
            }

            if (s != null && !s.isEmpty()) {
                if (s.equalsIgnoreCase("optometristSearch".trim())) {
                    try {
                        searchOptometrist(port, request, response, context);
                    } catch (ServletException ex) {
                        Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return null;
                }
            }
//            request.setAttribute("MemAppSearchResults", col);
//            context.getRequestDispatcher("/MemberApplication/MemberAppSearchResults.jsp").forward(request, response);
        } catch (java.lang.Exception ex) {
            Logger.getLogger(MemberApplicationSpecificQuestionsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void addDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        
        int maxDepNo = port.getMaxDependentNumberByCoverNumber(request.getParameter("memberAppCoverNumber"));        
        MemberApplication ma = port.getMemberApplicationByCoverNumber(request.getParameter("memberAppCoverNumber"));
        request.setAttribute("optionId", ma.getOptionId());
        
        request.setAttribute("memberAppDepNumber", Integer.toString(maxDepNo + 1));
//        request.setAttribute("memberAppDepNumber", Integer.toString(getIntParam(request,"maxDep") + 1));
        request.setAttribute("memberAppNumber", request.getParameter("memberAppNumber"));
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        request.setAttribute("DependantTypeList", getDependantTypes());
        request.setAttribute("memberAppCoverNumber", request.getParameter("memberAppCoverNumber"));
        
        context.getRequestDispatcher("/MemberApplication/MemberAppDepAdd.jsp").forward(request, response);
    }

    private int getIntParam(HttpServletRequest request, String paramName) {
        try {
            String maxDepStr = request.getParameter(paramName);
            return Integer.parseInt(maxDepStr);
        } catch (java.lang.Exception e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }

    private void editDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        MemberApplication ma = port.getMemberApplicationByCoverNumber(request.getParameter("memberAppCoverNumber"));
        request.setAttribute("optionId", ma.getOptionId());
        MemberDependantApp depApp = null;
        request.setAttribute("memberAppDepNumber", request.getParameter("depNo"));
        request.setAttribute("memberAppNumber", request.getParameter("memberAppNumber"));
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        int appNum = getIntParam(request, "memberAppNumber");
        int depNo = getIntParam(request, "depNo");
        if (request.getSession().getAttribute("Client").equals("Sechaba")) {
           depApp = port.getMemberApplicationByDependantApplicationNumberSechaba(appNum, depNo);
        } else {
           depApp = port.getMemberApplicationByDependantApplicationNumber(appNum, depNo);
        }
        
        Enumeration e = request.getAttributeNames();
        while (e.hasMoreElements()) {

            Object s = e.nextElement();
            System.out.println(s.toString() + " = " + request.getAttribute(s.toString()));
        }
        request.setAttribute("memberAppCoverNumber", request.getParameter("memberAppCoverNumber"));
        request.setAttribute("DependantTypeList", getDependantTypes());
        
        if (request.getSession().getAttribute("Client").equals("Sechaba")) {
            MemberAppDepMapping.setMemberAppDependantSechaba(request, depApp);
            if (depApp.getDoctorPracticeNumber() != null && !depApp.getDoctorPracticeNumber().equals("")) {
                PracticeSearchResult practice = setProviderDetails(depApp.getDoctorPracticeNumber());
                request.setAttribute("MemberDependantApp_doctorName", practice.getPracticeName());
                request.setAttribute("MemberDependantApp_doctorNetwork", practice.getPracticeNetwork());
            }

            if (depApp.getDentistPracticeNumber() != null && !depApp.getDentistPracticeNumber().equals("")) {
                PracticeSearchResult practice = setProviderDetails(depApp.getDentistPracticeNumber());
                request.setAttribute("MemberDependantApp_dentistName", practice.getPracticeName());
                request.setAttribute("MemberDependantApp_dentistNetwork", practice.getPracticeNetwork());
            }

            if (depApp.getOptometristPracticeNumber() != null && !depApp.getOptometristPracticeNumber().equals("")) {
                PracticeSearchResult practice = setProviderDetails(depApp.getOptometristPracticeNumber());
                request.setAttribute("MemberDependantApp_optometristName", practice.getPracticeName());
                request.setAttribute("MemberDependantApp_optometristNetwork", practice.getPracticeNetwork());
            }

            request.setAttribute("MemberDependantApp_network_IPARow", Integer.getInteger(depApp.getNetworkIpa()));
        } else {
            MemberAppDepMapping.setMemberAppDependant(request, depApp);
        }
        
        context.getRequestDispatcher("/MemberApplication/MemberAppDepAdd.jsp").forward(request, response);
    }
    
    private PracticeSearchResult setProviderDetails (String practiceNumber) {
        ProviderSearchCriteria search = new ProviderSearchCriteria();
        PracticeSearchResult practice = null;
        if (practiceNumber != null && !practiceNumber.equals("")) {
            search.setPracticeNumber(practiceNumber);
        }
        
        search.setEntityTypeID(3);
        
        List<ProviderDetails> pdList = new ArrayList<ProviderDetails>();
        
        try {
            pdList = service.getNeoManagerBeanPort().findAllProvidersByCriteria(search);
            for (ProviderDetails pd : pdList) {
                practice = new PracticeSearchResult();
                practice.setPracticeName(pd.getPracticeName());
                practice.setPracticeNumber(pd.getPracticeNumber());
                practice.setPracticeType(pd.getDisciplineType().getValue());
                practice.setPracticeNetwork(pd.getNetworkName());
            }
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
        return practice;
    }
    
    private String setNetworkIPA(String networkIPA) {
        String ipa = null;
        List<neo.manager.LookupValue> lookUpsClient = service.getNeoManagerBeanPort().findCodeTableForLookupType(351);
        for (neo.manager.LookupValue lc : lookUpsClient) {
            if (lc.getId().equalsIgnoreCase(networkIPA)) {
                ipa = lc.getValue();
            }
        }
        return ipa;
    }

    private void removeDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        int appNum = getIntParam(request, "memberAppNumber");
        int depNo = getIntParam(request, "depNo");
        port.removeMemberApplicationDependant(appNum, depNo);
        context.getRequestDispatcher(new MemberAppTabContentsCommand().getMemberDepAppDetails(port, request)).forward(request, response);
    }

    private void save(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = MemberAppDepMapping.validate(request);
        String depApp_identificationNumber = (String) request.getParameter("MemberDependantApp_identificationNumber");
        String checkDublicate = (String) request.getSession().getAttribute("throughAppSearch");
        boolean appValidation = false;
        boolean validUser = false;
        NeoUser user = (NeoUser) request.getSession().getAttribute("persist_user");
        for (SecurityResponsibility res : user.getSecurityResponsibility()) {
            if (res.getResponsibilityId() == 19) {
                validUser = true;
            }
        }
        //System.err.println("validUser Dependant " + validUser + " ID " + depApp_identificationNumber + " checkDublicate " + checkDublicate);
        if (depApp_identificationNumber != null && !depApp_identificationNumber.isEmpty() && checkDublicate == null) {
            List<KeyValueArray> kva = port.getDependentById(depApp_identificationNumber);
            if (kva.size() > 0) {
                appValidation = true;
                errors.put("MemberDependantApp_identificationNumber_error", "Dependent is already in another application");
            }

            if (!appValidation) {
                List<CoverDetails> existingRec = port.getDependantsForPrincipleMemberByDateAndIdNumber(depApp_identificationNumber);
                if (existingRec.size() > 0) {
                    for (CoverDetails dep : existingRec) {
                        if (dep.getStatus().equals("Active")) {
                            if (!validUser) {
                                errors.put("MemberDependantApp_identificationNumber_error", "Dependent is already active in another cover");
                            } else {
                                if (request.getSession().getAttribute("Active") == null) {
                                    errors.put("MemberDependantApp_identificationNumber_error", "Dependent is already active in another cover. Click save again to override");
                                    request.getSession().setAttribute("Active", depApp_identificationNumber);
                                } else if (!request.getSession().getAttribute("Active").toString().equals(depApp_identificationNumber)) {
                                    errors.put("MemberDependantApp_identificationNumber_error", "Dependent is already active in another cover. Click save again to override");
                                    request.getSession().setAttribute("Active", depApp_identificationNumber);
                                }
                            }
                        } else if (dep.getStatus().equals("Suspend")) {
                            errors.put("MemberDependantApp_identificationNumber_error", "Dependent is already suspended in another cover");
                        } else if (dep.getStatus().equals("Resign")) {
                            if (request.getSession().getAttribute("Resign") == null) {
                                errors.put("MemberDependantApp_identificationNumber_error", "Dependent is already resigned in another cover. Click save again to continue");
                                request.getSession().setAttribute("Resign", depApp_identificationNumber);
                            } else if (!request.getSession().getAttribute("Resign").toString().equals(depApp_identificationNumber)) {
                                errors.put("MemberDependantApp_identificationNumber_error", "Dependent is already resigned in another cover. Click save again to continue");
                                request.getSession().setAttribute("Resign", depApp_identificationNumber);
                            }
                        }
                    }
                }
            }
        }
        
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (save(port, request)) {
                request.getSession().setAttribute("Resign", null);
                request.getSession().setAttribute("Active", null);
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving the dependant.\"";
            }
        } else {
            additionalData = null;
        }
        if ("OK".equalsIgnoreCase(status)) {
            List<MemberDependantApp> depList = port.getMemberApplicationDependantByApplicationNumber(getIntParam(request, "memberAppNumber"));
            request.setAttribute("memberAppNumber", request.getParameter("memberAppNumber"));
            request.setAttribute("memberAppCoverNumber", request.getParameter("memberAppCoverNumber"));
            request.setAttribute("depList", depList);
            if (depList == null || depList.isEmpty()) {
                request.setAttribute("maxDep", "0");
            } else {
                int i = 0;
                for (MemberDependantApp depApp : depList) {
                    if (depApp.getDependentNumber() > i) {
                        i = depApp.getDependentNumber();
                    }
                    request.setAttribute("depList_dob_" + depApp.getDependentNumber(), getDateStr(depApp.getDateOfBirth()));
                }
                request.setAttribute("maxDep", Integer.toString(i));
            }
            context.getRequestDispatcher("/MemberApplication/MemberDependantApp.jsp").forward(request, response);
        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }
    }

    private boolean save(NeoManagerBean port, HttpServletRequest request) {
        NeoUser neoUser = getNeoUser(request);
        try {
            if (request.getSession().getAttribute("Client").equals("Sechaba")) {
                MemberDependantApp depApp = MemberAppDepMapping.getMemberAppDependantSechaba(request, neoUser);
                depApp = port.saveMemberApplicationDependantSechaba(depApp);
            } else {
                MemberDependantApp depApp = MemberAppDepMapping.getMemberAppDependant(request, neoUser);
                depApp = port.saveMemberApplicationDependant(depApp);
            }
            
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
    }
    
    private void searchDoctor(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("practiceName", "MemberDependantApp_doctorName");
        request.setAttribute("practiceNumber", "MemberDependantApp_doctorPracticeNumber");
        request.setAttribute("practiceNetwork", "MemberDependantApp_doctorNetwork");
        request.setAttribute("DSPType", "doctor");
        
        context.getRequestDispatcher("/MemberApplication/MemberDependantAppSearchPractice.jsp").forward(request, response);
    }
    
    private void searchDentist(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("practiceName", "MemberDependantApp_dentistName");
        request.setAttribute("practiceNumber", "MemberDependantApp_dentistPracticeNumber");
        request.setAttribute("practiceNetwork", "MemberDependantApp_dentistNetwork");
        request.setAttribute("DSPType", "dentist");
        
        context.getRequestDispatcher("/MemberApplication/MemberDependantAppSearchPractice.jsp").forward(request, response);
    }
    
    private void searchOptometrist(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("practiceName", "MemberDependantApp_optometristName");
        request.setAttribute("practiceNumber", "MemberDependantApp_optometristPracticeNumber");
        request.setAttribute("practiceNetwork", "MemberDependantApp_optometristNetwork");
        request.setAttribute("DSPType", "optometrist");
        
        context.getRequestDispatcher("/MemberApplication/MemberDependantAppSearchPractice.jsp").forward(request, response);
    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser) request.getSession().getAttribute("persist_user");
    }

    private String getDateStr(XMLGregorianCalendar xgc) {
        try {
            Date dt = xgc.toGregorianCalendar().getTime();
            return DateTimeUtils.convertToYYYYMMDD(dt);
        } catch (java.lang.Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
    }

    private List<Map> getDependantTypes() {
        List<Map> dlist = new ArrayList();
        Map<String, String> depMap = new HashMap<String, String>();
        depMap.put("id", "18");
        depMap.put("value", "Adult");
        dlist.add(depMap);
        depMap = new HashMap<String, String>();
        depMap.put("id", "19");
        depMap.put("value", "Child");
        dlist.add(depMap);
        return dlist;
//        request.setAttribute("DependentList", dlist);

    }

    @Override
    public String getName() {
        return "MemberDependantAppCommand";
    }
}

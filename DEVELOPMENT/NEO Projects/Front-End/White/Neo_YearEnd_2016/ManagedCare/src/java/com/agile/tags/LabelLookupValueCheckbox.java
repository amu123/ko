/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

import com.agile.security.webservice.LookupValue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author johanl
 */
public class LabelLookupValueCheckbox extends TagSupport {
    private String elementName;
    private String displayName;
    private String lookupId;
    private String javascript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            HttpSession session = pageContext.getSession();
            out.println("<table width=\"298\" height=\"253\" align=\"left\" cellpadding=\"4\" cellspacing=\"4\">");
            out.println("<tr><th width=\"280\" align=\"left\">"+ displayName +"</th></tr>");
            out.println("<tr align=\"left\" valign=\"top\"><td>");
            String selectedBox = "" + session.getAttribute(elementName);
            ArrayList<LookupValue> lookUps = (ArrayList<LookupValue>) TagMethods.getLookupValue(new Integer(lookupId));
            for (int i = 0; i < lookUps.size(); i++) {
                LookupValue lookupValue = lookUps.get(i);
                if (lookupValue.getId().toString().equalsIgnoreCase(selectedBox)) {
                     out.print("<div><input type=\"checkbox\" name=\"" + elementName + "_" + i + "\" value=\"" + lookupValue.getId() + "\" checked><label>" + lookupValue.getValue() + "</label></input></div>");
                    //out.println("<div><input type=\"checkbox\" value=\"" + lookupValue.getId() + "\" checked=\"checked\" /><label>" + lookupValue.getValue() + "</label></div>");
                } else {
                    out.print("<div><input type=\"checkbox\" name=\"" + elementName + "_" + i + "\" value=\"" + lookupValue.getId() + "\" ><label>" + lookupValue.getValue() + "</label></div>");
                    //out.println("<div><input type=\"checkbox\" value=\"" + lookupValue.getId() + "\" /><label>" + lookupValue.getValue() + "</label></div>");
                }
            }
            out.println("</td><tr></table>");
        } catch (IOException ex) {
            Logger.getLogger(LabelLookupValueCheckbox.class.getName()).log(Level.SEVERE, null, ex);
        }
        return super.doEndTag();
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

}

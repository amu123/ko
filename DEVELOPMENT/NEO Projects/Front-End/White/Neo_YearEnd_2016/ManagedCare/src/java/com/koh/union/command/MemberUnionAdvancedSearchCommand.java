/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.union.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.Union;

/**
 *
 * @author shimanem
 */
public class MemberUnionAdvancedSearchCommand extends NeoCommand {
     private static final Logger logger = Logger.getLogger(UnionAdvancedSearchCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
        try {
            NeoManagerBean port = service.getNeoManagerBeanPort();
            search(port, request, response, context);
        } catch (ServletException ex) {
            Logger.getLogger(UnionAdvancedSearchCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UnionAdvancedSearchCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public void search(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        String unionNumber = request.getParameter("_union_No");
        String unionName = request.getParameter("_union_Name");

        logger.log(Level.INFO, ">> *** Union Number : {0}", unionNumber);
        logger.log(Level.INFO, ">> *** Union Name : {0}", unionName);

        Union union = new Union();
        if (unionNumber != null && !unionNumber.equals("")) {
            union.setUnionNumber(unionNumber);
        }
        if (unionName != null && !unionName.equals("")) {
            union.setUnionName(unionName);
        }
        ArrayList<Union> unionList = (ArrayList<Union>) port.findUnion(union);

        if (unionList != null) {
            request.setAttribute("MemberUnionResults", unionList);
            context.getRequestDispatcher("/Member/MemberUnionSearchResults.jsp").forward(request, response);
        } else {
            request.setAttribute("MemberUnionResults", null);
        }

    }

    @Override
    public String getName() {
        return "MemberUnionAdvancedSearchCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.dto;

/**
 *
 * @author johanl
 */
public class AuthBasketTariffs {

    private String providerType;
    private String tariffDesc;
    private String tariffCode;
    private double tariffAmount;
    private int tariffFrequency;

    public AuthBasketTariffs(){}

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public double getTariffAmount() {
        return tariffAmount;
    }

    public void setTariffAmount(double tariffAmount) {
        this.tariffAmount = tariffAmount;
    }

    public String getTariffCode() {
        return tariffCode;
    }

    public void setTariffCode(String tariffCode) {
        this.tariffCode = tariffCode;
    }

    public String getTariffDesc() {
        return tariffDesc;
    }

    public void setTariffDesc(String tariffDesc) {
        this.tariffDesc = tariffDesc;
    }

    public int getTariffFrequency() {
        return tariffFrequency;
    }

    public void setTariffFrequency(int tariffFrequency) {
        this.tariffFrequency = tariffFrequency;
    }

    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthNoteDetail;

/**
 *
 * @author Johan-NB
 */
public class AuthNoteListDisplay extends TagSupport {

    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            out.println("<td colspan=\"5\"><table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Creation Date</th><th align=\"left\">Created By</th><th align=\"left\">Note</th><th>Note Type</th></tr>");

            List<AuthNoteDetail> noteList = (List<AuthNoteDetail>) session.getAttribute(sessionAttribute);
            if (noteList != null) {
                for (int i = 1; i <= noteList.size(); i++) {
                    AuthNoteDetail notes = noteList.get(i - 1);

                    String creationDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(notes.getCreationDate().toGregorianCalendar().getTime());

                    out.println("<tr><td><label class=\"label\">" + creationDate + "</label></td>");
                    out.println("<td width=\"150\"><label class=\"label\">" + notes.getCreatedBy() + "</label></td>");
                    out.println("<td width=\"400\"><label class=\"label\">" + notes.getNotes() + "</label></td>");
                    out.println("<td><label class=\"label\">" + notes.getNoteType().getValue() + "</label></td></tr>");

                }
            }
            out.println("</table></td>");
        } catch (IOException ex) {
            throw new JspException("Error in AuthNoteListDisplay tag", ex);
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

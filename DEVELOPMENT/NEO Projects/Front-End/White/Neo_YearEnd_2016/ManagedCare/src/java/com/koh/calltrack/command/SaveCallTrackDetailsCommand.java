/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.calltrack.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import neo.manager.CallTrackDetails;
import neo.manager.CallWorkbenchDetails;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;
import com.koh.command.NeoCommand;
import com.agile.security.webservice.EmailContent;
import com.koh.command.FECommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.mail.internet.InternetAddress;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.ContactDetails;
import neo.manager.CoverDetails;
import neo.manager.PersonDetails;
import neo.manager.WorkflowItem;

/**
 *
 * @author josephm
 */
public class SaveCallTrackDetailsCommand extends NeoCommand {

    private String nextJSP;
    private final Logger log = Logger.getLogger(SaveCallTrackDetailsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String emailFrom = "";
        String scheme = "";
        String emailTo = "";
        
        boolean checkError = false;
        HttpSession session = request.getSession();
        ArrayList<ContactDetails> contact;
        PersonDetails pd;
        String entityID = String.valueOf(session.getAttribute("entityID"));
        
        session.removeAttribute("provNum_error");
        session.removeAttribute("providerNumber_error");
        session.removeAttribute("memberNum_error");
        session.removeAttribute("callerName_error");
        session.removeAttribute("callerContact_error");
        session.removeAttribute("callStatus_error");
        session.removeAttribute("memberNumber_error");
        session.removeAttribute("notes_error");
        session.removeAttribute("callType_error");
        
        if(entityID == null || entityID.equalsIgnoreCase("null")){ //session entityID does not exist for Call Tracking Users
            entityID = String.valueOf(session.getAttribute("memberCoverEntityId"));
        }
        if(entityID == null || entityID.equalsIgnoreCase("null") && request.getParameter("callType").equals("1")){
            checkError = true;
            session.setAttribute("memberNumber_error", "mandatory field");
            contact = null;
            pd = null;
        }else if(!request.getParameter("callType").equals("2") && request.getParameter("callType") != null &&  !request.getParameter("callType").equals("null") && !request.getParameter("callType").equals("") && !request.getParameter("callType").equals("99")){
            contact = (ArrayList<ContactDetails>) service.getNeoManagerBeanPort().getAllContactDetails(new Integer(entityID));
            pd = service.getNeoManagerBeanPort().getPersonDetailsByEntityId(new Integer(entityID));
        }else{
            contact = null;
            pd = null;
        }
        
        CallTrackDetails details = new CallTrackDetails();
        CallWorkbenchDetails cwdetails = new CallWorkbenchDetails();
        
        
        // Integer callId = (Integer) session.getAttribute("trackId");
        //Claims flags
        boolean claimsFirstFlag = false;
        boolean claimsSecondFlag = false;
        boolean claimsThirdFlag = false;
        //Statements flags
        boolean statementsFirstFlag = false;
        boolean statementsThirdFlag = false;
        //Benefits Flags
        boolean benefitsFirstFlag = false;
        boolean benefitsSecondFlag = false;
        boolean benefitsThirdFlag = false;
        //Premiums flags
        boolean premiumsFirstFlag = false;
        boolean premiumsSecondFlag = false;
        //Membership flags
        boolean membershipFirstFlag = false;
        boolean membershipThirdFlag = false;
        //Service Provider flags
        boolean seerviceProviderFirstFlag = false;
        boolean serviceProviderThirdFlag = false;
        // Foundation Flags
        boolean foundationFirstFlag = false;
        boolean foundationSecondFlag = false;

        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        String disciplinCode = "" + session.getAttribute("disciplineCode");
        details.setProviderDiscipline(disciplinCode);

        if (request.getParameter("callType") != null && !request.getParameter("callType").equals("") && !request.getParameter("callType").equals("99")) {
            details.setCallType(request.getParameter("callType"));
        }else{
            session.setAttribute("callType_error", "mandatory field");
            checkError = true;
        }

        if (request.getParameter("callTrackNumber") != null && !request.getParameter("callTrackNumber").equals("")) {
            details.setCallReference(request.getParameter("callTrackNumber"));
        }

        if (request.getParameter("callStatus") != null && !request.getParameter("callStatus").equals("") && !request.getParameter("callStatus").equals("99")) {
            details.setCallStatus(request.getParameter("callStatus"));
            details.setCallStatusId(Integer.parseInt(request.getParameter("callStatus")));
        }else{
             session.setAttribute("callStatus_error", "mandatory field");
             checkError = true;
        }
        if(details.getCallType() != null && !details.getCallType().equals("null") && !details.getCallType().equals("") && !details.getCallType().equals("99")){
            
        
        if (request.getParameter("refUser") != null && !request.getParameter("refUser").equals("") && request.getParameter("callStatus").equals("2")) {
            int referedUserId = Integer.parseInt(request.getParameter("refUser"));
            details.setReferedUserId(referedUserId);
        }

        if (request.getParameter("memberNumber_text") != null && !request.getParameter("memberNumber_text").equals("") && (details.getCallType().equals("1") || details.getCallType().equals("3") || details.getCallType().equals("4"))) {
            details.setCoverNumber(request.getParameter("memberNumber_text"));
        } else if (request.getParameter("memberNum") != null && !request.getParameter("memberNum").equalsIgnoreCase("")) {
            details.setCoverNumber(request.getParameter("memberNum"));
        } else {
            if (details.getCallType() != null && !details.getCallType().equals("null") && !details.getCallType().equals("") && !details.getCallType().equals("99") && !details.getCallType().equals("2")) {
                session.setAttribute("memberNumber_error", "mandatory field");
                checkError = true;
            }
        }

        if (request.getParameter("providerNumber_text") != null && !request.getParameter("providerNumber_text").equals("") && details.getCallType().equals("2")) {
            details.setProviderNumber(request.getParameter("providerNumber_text"));
        } else {
            if (request.getParameter("provNum") != null && !request.getParameter("provNum").equals("")) {
                details.setProviderNumber(request.getParameter("provNum"));
            } else if (details.getCallType() != null && !details.getCallType().equals("null") && !details.getCallType().equals("") && !details.getCallType().equals("99") && details.getCallType().equals("2")) {
//                session.setAttribute("provNum_error", "mandatory field");
                session.setAttribute("providerNumber_error", "mandatory field");
                checkError = true;
            }
        }

        if (request.getParameter("provName") != null && !request.getParameter("provName").equals("")) {
            details.setProviderNameSurname(request.getParameter("provName"));
        }


        if (request.getParameter("callerName") != null && !request.getParameter("callerName").equals("")) {
            details.setCallerName(request.getParameter("callerName"));
        } else {
            session.setAttribute("callerName_error", "mandatory field");
            checkError = true;
        }

        if (request.getParameter("callerContact") != null && !request.getParameter("callerContact").equals("")) {
            details.setContactDetails(request.getParameter("callerContact"));
            emailTo = details.getContactDetails();
        } else {
            session.setAttribute("callerContact_error", "mandatory field");
            checkError = true;
        }

        if (request.getParameter("notes") != null && !request.getParameter("notes").equals("") && request.getParameter("notes").length() > 1) {
            details.setNotes(request.getParameter("notes"));
        } else {
            session.setAttribute("notes_error", "missing fields");
            checkError = true;
        }
        // System.out.println("The call type is " + details.getCallType());
        }else{
            checkError = true;
        }
        //Contact type flag
        boolean contactTypeFlag = false;
        for (int i = 0; i < 8; i++) {
            if (request.getParameter("queryMethod_" + i) != null) {
                contactTypeFlag = false;
                break;
            } else {
                contactTypeFlag = true;
            }
        }

        boolean flag = false;
        for (int i = 0; i < 7; i++) {
            log.info("The call queries are " + request.getParameter("callQ_" + i));
            if (request.getParameter("callQ_" + i) != null) {
                flag = false;
                break;
            } else {
                flag = true;
            }
        }


        if (request.getParameter("callQ_0") != null) {
            // First category
            log.info("");
            for (int i = 0; i <= 4; i++) {

                if (request.getParameter("claimsFC_" + i) != null) {

                    claimsFirstFlag = false;
                    break;
                } else {

                    claimsFirstFlag = true;
                }
            }

            // Second category
            for (int i = 0; i < 8; i++) {

                if (request.getParameter("claimsSC_" + i) != null) {

                    claimsSecondFlag = false;
                    break;
                } else {

                    claimsSecondFlag = true;
                }
            }

            // Third Category

            for (int i = 0; i < 12; i++) {

                if (request.getParameter("claimsTC_" + i) != null) {

                    claimsThirdFlag = false;
                    break;
                } else {

                    claimsThirdFlag = true;
                }
            }

        }
        if (request.getParameter("callQ_1") != null) {

            //Statements first category

            for (int i = 0; i < 4; i++) {

                if (request.getParameter("statementsFC_" + i) != null) {

                    statementsFirstFlag = false;
                    break;
                } else {

                    statementsFirstFlag = true;
                }
            }

            //Statements Third category

            for (int i = 0; i < 5; i++) {

                if (request.getParameter("statementsTC_" + i) != null) {

                    statementsThirdFlag = false;
                    break;
                } else {
                    statementsThirdFlag = true;
                }
            }
        }
        if (request.getParameter("callQ_2") != null) {

            // Benefits First Category
            for (int i = 0; i < 8; i++) {

                if (request.getParameter("benefitsFCReso_" + i) != null || request.getParameter("benefitsFCSpec_" + i) != null) {

                    benefitsFirstFlag = false;
                    break;
                } else {

                    benefitsFirstFlag = true;
                }
            }
            //Benefits second category
            for (int i = 0; i < 7; i++) {

                if (request.getParameter("benefitsSC_" + i) != null) {

                    benefitsSecondFlag = false;
                    break;
                } else {

                    benefitsSecondFlag = true;
                }
            }

            //Benefits Third Category
            for (int i = 0; i < 12; i++) {

                if (request.getParameter("benefitsTC_" + i) != null) {

                    benefitsThirdFlag = false;
                    break;
                } else {

                    benefitsThirdFlag = true;
                }
            }

        }
        if (request.getParameter("callQ_3") != null) {

            // Premiums First Category
            for (int i = 0; i < 2; i++) {

                if (request.getParameter("prenmiumsFC_" + i) != null) {

                    premiumsFirstFlag = false;
                    break;
                } else {

                    premiumsFirstFlag = true;
                }
            }

            // Premiums Third Category
            for (int i = 0; i < 7; i++) {

                if (request.getParameter("premiumsTC_" + i) != null) {

                    premiumsSecondFlag = false;
                    break;
                } else {

                    premiumsSecondFlag = true;
                }
            }

        }
        if (request.getParameter("callQ_4") != null) {

            // Membership first category
            for (int i = 0; i < 3; i++) {

                if (request.getParameter("membershipFC_" + i) != null) {

                    membershipFirstFlag = false;
                    break;
                } else {

                    membershipFirstFlag = true;
                }
            }
            // Membershipt third category
            for (int i = 0; i < 16; i++) {

                if (request.getParameter("memberShipTC_" + i) != null) {

                    membershipThirdFlag = false;
                    break;
                } else {

                    membershipThirdFlag = true;
                }
            }

        }
        if (request.getParameter("callQ_5") != null) {

            // Service Provider First Category
            for (int i = 0; i < 7; i++) {

                if (request.getParameter("serviceProviderFC_" + i) != null) {

                    seerviceProviderFirstFlag = false;
                    break;
                } else {

                    seerviceProviderFirstFlag = true;
                }
            }

            // Service Provider Third Category
            for (int i = 0; i < 3; i++) {

                if (request.getParameter("serviceProviderTC_" + i) != null) {

                    serviceProviderThirdFlag = false;
                    break;
                } else {

                    serviceProviderThirdFlag = true;
                }
            }

        }
        if (request.getParameter("callQ_6") != null) {

            // Foundation First Category
            for (int i = 0; i < 5; i++) {

                if (request.getParameter("foundationFC_" + i) != null) {

                    foundationFirstFlag = false;
                    break;
                } else {

                    foundationFirstFlag = true;
                }
            }

            // Foundation Second Category
            for (int i = 0; i < 7; i++) {

                if (request.getParameter("foundationSC_" + i) != null) {

                    foundationSecondFlag = false;
                    break;
                } else {

                    foundationSecondFlag = true;
                }
            }
        }

        //Flag logic
//        if (contactTypeFlag || flag || claimsFirstFlag || claimsSecondFlag || claimsThirdFlag || statementsFirstFlag || statementsThirdFlag || benefitsFirstFlag || benefitsSecondFlag || benefitsThirdFlag || premiumsFirstFlag || premiumsSecondFlag || membershipFirstFlag || membershipThirdFlag || seerviceProviderFirstFlag || serviceProviderThirdFlag || foundationFirstFlag || foundationSecondFlag) {
//            session.setAttribute("mandatoryfailed", "missing checkbox fields");
//            log.info("Session " + session.getAttribute("mandatoryfailed"));
//        }
        if (contactTypeFlag || flag) {
            session.setAttribute("mandatoryfailed", "missing checkbox fields");
            log.info("Session " + session.getAttribute("mandatoryfailed"));
        }
        //clearAllFromSession(request);
        Enumeration e = request.getParameterNames();
        Collection<CallTrackDetails> resps = new ArrayList<CallTrackDetails>();
        String callQ = "";
        String query = "";
        boolean init = true;
        String product = request.getParameter("product") + "";
        String benefitFC;
        if (product.equals("1")) {
            benefitFC = "benefitsFCReso_";
        } else {
            benefitFC = "benefitsFCSpec_";
        }

        while (e.hasMoreElements()) {
            String pName = "" + e.nextElement();

            // if ((pName.contains("callQ_") | pName.contains("disciplineCategory_") | pName.contains("benefitSub_") | pName.contains("claimSub_") | pName.contains("membershipSub_") | pName.contains("underwritingSub_") | pName.contains("queryMethod_")) && (!pName.contains("_identifier"))) {
            if ((pName.contains("callQ") | (pName.contains("queryMethod")) | pName.contains("claimsFC_") | pName.contains("claimsSC_") | pName.contains("claimsTC_") | pName.contains("statementsFC_") | pName.contains("statementsTC_") | pName.contains(benefitFC) | pName.contains("benefitsSC_") | pName.contains("benefitsTC_") | pName.contains("prenmiumsFC_") | pName.contains("premiumsTC_") | pName.contains("membershipFC_") | pName.contains("memberShipTC_") | pName.contains("serviceProviderFC_") | pName.contains("serviceProviderTC_") | pName.contains("foundationFC_") | pName.contains("foundationSC_")) && (!pName.contains("_identifier"))) {
                String identifier;
                //int respId = 0;
                String respId = "";

                //respId  = new Integer(request.getParameter(pName));
                respId = request.getParameter(pName);
                // System.out.println("The parameter is " + pName);
                //System.out.println("The role is " + respId);
                //identifier = request.getParameter(pName + "_identifier");
                identifier = request.getParameter(pName);

                if (identifier != null) {
                    if (init) {
                        callQ += "," + identifier;
                    } else {
                        callQ += "," + identifier;
                    }
                }


            }
        }

        if (callQ.startsWith(",")) {

            query = callQ.substring(1);
        }
        details.setCallQuery(query);

        try {
            details.setResoUserName(user.getUsername());
            details.setResoUserId(user.getUserId());
            cwdetails.setResoUserName(user.getUsername());
            int userId = user.getUserId();


//            if (checkError || contactTypeFlag || flag || claimsFirstFlag || claimsSecondFlag || claimsThirdFlag || statementsFirstFlag || statementsThirdFlag || benefitsFirstFlag || benefitsSecondFlag || benefitsThirdFlag || premiumsFirstFlag || premiumsSecondFlag || membershipFirstFlag || membershipThirdFlag || seerviceProviderFirstFlag || serviceProviderThirdFlag || foundationFirstFlag || foundationSecondFlag) {
//                saveScreenToSession(request);
//                nextJSP = "/Calltrack/LogNewCall.jsp";
//            } else {
            if (checkError || contactTypeFlag || flag) {
                saveScreenToSession(request);
                nextJSP = "/Calltrack/LogNewCall.jsp";
            } else {
                
                boolean isValid = false;

                /**
                 * **Comment out when testing****
                 */
                if (contact != null) {
                    for (ContactDetails cd : contact) {
                        if (cd.getCommunicationMethod().equals("E-mail")) {
                            if(cd.getMethodDetails() == null) {
                                emailTo = "";
                            } else {
                                emailTo = cd.getMethodDetails().trim();
                            }                        
                        }
                    }
                }
                //Validate member email address
                try {
                    new InternetAddress(emailTo).validate();
                    isValid = true;
                } catch (Exception ex) {
                    //Member Mail failed now trying contact details for call tracking
                    try {
                        emailTo = details.getContactDetails().trim();
                        new InternetAddress(emailTo).validate();
                        isValid = true;
                    } catch (Exception ex2) {
                        System.out.println("MAIL| Member does not have a valid email address");
                    }                
                } 
                
                CoverDetails coverDetails = service.getNeoManagerBeanPort().getEAuthResignedMembers(details.getCoverNumber());

                boolean sendSms = false;
                
                if (request.getParameter("callType") != null && !request.getParameter("callType").equals("") && request.getParameter("callType").equals("1")) {
                    Date today = new Date();
                    if (today.after(DateTimeUtils.getDateFromXMLGregorianCalendar(coverDetails.getCoverEndDate()))) {
                        sendSms = false;
                        System.out.println("Cover ended. Not sending SMS");
                    } else {
                        sendSms = true;
                    }
                } else {
                    sendSms = false;
                }
                                    
                //validate email first, so that the backend can determine when not to send the sms
                
                int success = 0;
                String referenceNum = null;
                
                String client = String.valueOf(context.getAttribute("Client"));
                if(client != null && client.equals("Sechaba")) {
                    referenceNum = service.getNeoManagerBeanPort().saveCallTrackDetailsReturnRefNum(details, isValid, sendSms);  
                    
                    if(referenceNum != null) {
                        success = 1;
                    }                   
                } else {
                    success = service.getNeoManagerBeanPort().saveCallTrackDetails(details, isValid, sendSms);
                }
                
                
                clearAllFromSession(request);
                log.info("The session variable is " + session.getAttribute("mandatoryfailed"));
                nextJSP = "/Calltrack/CallTrackworkbenchForUser.jsp";

                System.out.println("product     = " + product);
                System.out.println("Email = " + emailTo);
                
                //Only send mail when call is closed
                if (isValid && success > 0 && details.getCallStatus().equals("3") && details.getCallType().equals("1") && !product.equals("") && sendSms) {
                    if (product.equals("1")) {
                        scheme = "Resolution Health";
                        emailFrom = "Noreply@agilityghs.co.za";
                    } else if (product.equals("2")) {
                        scheme = "Spectramed";
                        emailFrom = "noreply@spectramed.co.za";
                    } else if (product.equals("3")){
                        scheme = "Sizwe";
                        emailFrom = "do-not-reply@sizwe.co.za";
                    }
                    

                    String subject = scheme;
                    String emailMsg = null;
                    if (details.getCallType().equals("1")) {
                        subject += " - Member Survey";
                    }

                    EmailContent content = new EmailContent();
                    if(pd != null){
                        content.setName(pd.getName());
                        content.setSurname(pd.getSurname());
                    }
                    
                    content.setMemberCoverNumber(details.getCoverNumber());
                    content.setContentType("text/plain");
                    content.setSubject(subject.trim());
                    content.setEmailAddressFrom(emailFrom);
                    content.setEmailAddressTo(emailTo);
                    content.setProductId(new Integer(product.trim()));

                    //Determine msg for email according to type of document
                    content.setType("survey");
                    /*If document type is custom, this message added here will be displayed in the email. The email method will automatically add the 
                     the enquiries and call center aswell as the kind regards at the end of the message*/
                    content.setContent(emailMsg);
                    boolean emailSent;
                    System.out.println("In email code");
                    emailSent = FECommand.service.getAgileManagerPort().sendEmailWithOrWithoutAttachment(content);

                    if (emailSent) {
                        System.out.println("MAIL| Survey email sent successfully!");
                    }
                } else {
                    System.out.println("MAIL| Survey email NOT sent!");
                }
                
                if(client != null && client.equals("Sechaba")) {
                    String callCat = "";
                    String refNum = referenceNum + "";
                    Integer status = 0;
                    
                    if(details.getCallStatus() != null) {
                        if(details.getCallStatus().equals("1")) {
                            status = 1; // Open
                        } else if(details.getCallStatus().equals("3")) {
                            status = 5; // Closed
                        }
                    }
                    
                    for (int i = 0; i < 7; i++) {
                        if (request.getParameter("callQ_" + i) != null) {
                            System.out.println("The call queries are " + request.getParameter("callQ_" + i));
                            callCat = request.getParameter("callQ_" + i);
                            break;
                        }
                    }
                            
                    String entityNum = "";
                            
                    if(details.getProviderNumber() != null && !details.getProviderNumber().equals("")){
                        entityNum = details.getProviderNumber();
                    } else if(details.getCoverNumber() != null && !details.getCoverNumber().equals("")) {
                        entityNum = details.getCoverNumber();
                    }         
                   
                    String notes = details.getNotes();
                    Integer userID = details.getResoUserId();
                    
                    WorkflowItem wi = setCallTrackingWorkflowItem(Integer.valueOf(entityID), refNum, entityNum, callCat, status, notes, userID);                   
                    service.getNeoManagerBeanPort().captureGenericWorkflowItem(wi);
                }
            } 
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "SaveCallTrackDetailsCommand";
    }
    
    private WorkflowItem setCallTrackingWorkflowItem(Integer entityID, String refNum, String entityNum, String category, Integer statusID, String notes, Integer userID) {
        WorkflowItem wi = new WorkflowItem();
        Integer queueID = 0;
        
        if(category != null) {
            if(category.equals("QC10")) {
                System.out.println("Call Centre Admin");
                queueID = 3; // Call Centre Admin
            } else if(category.equals("QC1")) {
                System.out.println("Claims");
                queueID = 8; // Claims
            } else if(category.equals("QC2")) {
                System.out.println("Statements");
                queueID = 16; // Queries
            } else if(category.equals("QC3")) {
                System.out.println("Benefits");
                queueID = 16; // Queries
            } else if(category.equals("QC4")) {
                System.out.println("Premiums");
                queueID = 17; 
            } else if(category.equals("QC5")) {
                System.out.println("Membership");
                queueID = 11; // Membership
            } else if(category.equals("QC6")) {
                System.out.println("Service Provider");
                queueID = 14; // Provider Updates
            }  else if(category.equals("QC7")) {
                System.out.println("Chronic");
                queueID = 7; // Chronic Updates
            }   else if(category.equals("QC8")) {
                System.out.println("Oncology");
                queueID = 12; // Oncology Updates
            }   else if(category.equals("QC9")) {
                System.out.println("Wellcare");
                queueID = 10; // Wellcare Updates - HIV Case Management
            }   else if(category.equals("QC11")) {
                System.out.println("Group");
                queueID = 14; // Group Updates
            }    else if(category.equals("QC12")) {
                System.out.println("Broker");
                queueID = 2; // Group Updates
            } else {
                System.out.println("Queue not set");
                queueID = 16;
            }
        }
        
        wi.setSourceID(1); // Source is call tracking
	wi.setProductID(3); // Sizwe
	wi.setQueueID(queueID); // Set Correct Queue ID for Mailbox
	wi.setStatusID(statusID);
	wi.setSubject("Call Tracking " + entityNum); // Member Number/Provider Number + " " + Category 
        wi.setRefNum(refNum);
        
        ///////////////////
        wi.setEntityID(entityID);
        wi.setEntityNumber(entityNum);
        wi.setNotes(notes);
        ///////////////////
        
	wi.setUserID(null); // User assigned to
	wi.setCreatedBy(userID);
	wi.setLastUpdatedBy(userID);
	wi.setDocumentID(null);

        return wi;
    }
}

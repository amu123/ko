/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.tags;

import com.koh.command.NeoCommand;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.LookupValue;

/**
 *
 * @author josephm
 */
public class LabelNeoLookupToolTip extends TagSupport {
    private String displayName;
    private String elementName;
    private String lookupId;
    private String javaScript;
    private String mandatory;
    private String numericValidation;
    private String buttonName;
    private String tooltipId;
    private String enabled;
    private String valueFromSession;
    private String valueFromRequest;
    
    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
      public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        try {
            String validations = "";
            String sessionVal = "";
            String sessionValerror = "";
            out.println("<td><label class=\"label\">" + displayName + ":</label></td>");
            out.println("<td><select name=\"" + elementName + "\" id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");
            String selectedBox = "" + session.getAttribute(elementName);
            //Neo Lookups
            List<neo.manager.LookupValue> lookUps = NeoCommand.service.getNeoManagerBeanPort().getCodeTable(new Integer(lookupId));
            for (int i = 0; i < lookUps.size(); i++) {
                LookupValue lookupValue = lookUps.get(i);
                if (lookupValue.getId().equalsIgnoreCase(selectedBox)) {
                    out.println("<option value=\"" + lookupValue.getId() + "\" selected >" + lookupValue.getValue() + "</option>");
                } else {
                    out.println("<option value=\"" + lookupValue.getId() + "\" >" + lookupValue.getValue() + "</option>");
                }
            }
            out.println("</select></td>");

            ////////////////////////

            if (valueFromSession.equalsIgnoreCase("Yes")) {
                if (session.getAttribute(elementName) != null) {
                    sessionVal = "" + session.getAttribute(elementName);
                }
                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                }
                if (enabled != null){
                    out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"" + sessionVal + "\" size=\"30\" " + javaScript + " disabled=\"disabled\"/></td>");
                }else{
                    out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"" + sessionVal + "\" size=\"30\" " + javaScript +  "/></td>");
                }

            } else {
                if (!valueFromRequest.equalsIgnoreCase("")) {
                    out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"" + valueFromRequest + "\" size=\"30\" " + javaScript + "/></td>");
                } else {
                    out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"\" size=\"30\" " + javaScript + "/></td>");
                }
            }
            if (mandatory.equalsIgnoreCase("yes")) {
                out.print("<td><label class=\"red\">*</label></td>");
                validations += "ret = check_mandatory(thisForm, '" + elementName + "', '" + elementName + "_error');if(ret != true){toRet = false;} ";
            }
            else {
                out.print("<td></td>");
            }
            out.println("<td align=\"left\"><input type=\"button\" name=\"button\" id=\"button\" value=\"?\" tooltipText=\"" + tooltipId + "\" width=\"28\" height=\"28\" alt=\"Search\" border=\"0\" </td>");

            if (numericValidation.equalsIgnoreCase("yes")) {
                validations += "ret = isNumeric(thisForm, '" + elementName + "', '" + elementName + "_error');if(ret != true){toRet = false;} ";
            }
            if (valueFromSession.equalsIgnoreCase("Yes") && sessionValerror != null && !sessionValerror.equalsIgnoreCase("null")) {
                out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
            } else {
                out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
            }
            String att = (String) pageContext.getAttribute("validate");
            if (att != null) {
                pageContext.setAttribute("validate", att + "" + validations);
            } else {
                pageContext.setAttribute("validate", "" + validations);
            }

            ////////////////////////

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelNeoLookupValueDropDown tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setNumericValidation(String numericValidation) {
        this.numericValidation = numericValidation;
    }

    public void setButtonName(String buttonName) {
        this.buttonName = buttonName;
    }

    public void setTooltipId(String tooltipId) {
        this.tooltipId = tooltipId;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public void setValueFromSession(String valueFromSession) {
        this.valueFromSession = valueFromSession;
    }

    public void setValueFromRequest(String valueFromRequest) {
        this.valueFromRequest = valueFromRequest;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.calltrack.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ProviderDetails;

/**
 *
 * @author gerritr
 */
public class WorkflowFindPracticeByCodeCommand extends NeoCommand {

     @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        String provider = request.getParameter("providerNumber");
        ProviderDetails details = service.getNeoManagerBeanPort().fetchPracticeNameForAjax(provider);

        
        //******************REQUEST ITERATOR
        java.util.Enumeration attributeNames = request.getParameterNames();
        while (attributeNames.hasMoreElements()) {
            String name = String.valueOf(attributeNames.nextElement());
            String value = String.valueOf(request.getParameter(name));
            String viewClass = String.valueOf(request.getParameter(name).getClass());
            request.setAttribute(name, value);
        }
        //******************REQUEST ITERATOR
        
        try {
             PrintWriter out = response.getWriter();

                if(details.getPracticeName() != null){
                     String providerDesc = "";
                     providerDesc = details.getDisciplineType().getId() +" - "+ details.getDisciplineType().getValue();

                     out.print(getName() + "|ProviderName="+details.getPracticeName()+ "|ProviderDiscipline="+providerDesc+"$");

                 }
                 else {

                     out.print("Error|No such provider|" + getName());
                     //request.setAttribute("providerNumber", "");
                 }

             if(details.getDisciplineType() != null){
                    request.setAttribute("disciplineCode", details.getDisciplineType().getId());
             }
        }
        catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "WorkflowFindPracticeByCodeCommand";
    }

}

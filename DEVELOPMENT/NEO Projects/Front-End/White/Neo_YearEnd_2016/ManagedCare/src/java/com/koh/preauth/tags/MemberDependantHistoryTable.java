/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.claims.tags.*;
import com.koh.auth.dto.MemberSearchResult;
import com.koh.command.NeoCommand;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CoverDetails;
import neo.manager.EAuthCoverDetails;
import neo.manager.NeoManagerBean;
import neo.manager.Option;
import neo.manager.PersonDetails;

/**
 *
 * @author josephm
 */
public class MemberDependantHistoryTable extends TagSupport {

    private String javaScript;
    private String commandName;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {

        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        try {

            //ArrayList<CoverDetails> members = (ArrayList<CoverDetails>) session.getAttribute("memberClaimSearchResult");
            ArrayList<EAuthCoverDetails> members = (ArrayList<EAuthCoverDetails>) session.getAttribute("memberClaimSearchResult");
            String lastMember = "";
            boolean mustClose = false;
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            if (members != null && !members.isEmpty()) {

                //out.println("<label class=\"header\">Dependants</label></br></br>");
                out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.println("<tr>"
                        + "<th align=\"left\">Option</th>"
                        + "<th align=\"left\">Code</th>"
                        + "<th align=\"left\">First Name</th>"
                        + "<th align=\"left\">Surname</th>"
                        + "<th align=\"left\">Date Of Birth</th>"
                        + "<th align=\"left\">Gender</th>"
                        + "<th align=\"left\">Id Number</th>"
                        + "<th align=\"left\">Status</th>"
                        + "<th align=\"left\">Join Date</th>"
                        + "<th align=\"left\">Option Date</th>"
                        + "<th align=\"left\">Resigned Date</th>"
                        + "</tr>");

                for (int i = 0; i < members.size(); i++) {
                    //CoverDetails cd = members.get(i);
                    EAuthCoverDetails cd = members.get(i);

                    //Option opt = port.findOptionWithID(cd.getOptionId());
                    Option opt = port.findOptionWithID(Integer.parseInt(cd.getOptionLookup().getId()));

                    String dateOfBirth = "";
                    String joinDate = "";
                    String benefitDate = "";
                    String resignDate = "";

                    int entityId = cd.getCoverEntityId();
                    System.out.println("cover: " + cd.getCoverNumber() + " dep:" + cd.getDependentNumber() + " eId = " + cd.getCoverEntityId());
                    PersonDetails pd = port.getPersonDetailsByEntityId(entityId);

                    //dob
                    if (pd.getDateOfBirth() != null) {
                        dateOfBirth = dateFormat.format(pd.getDateOfBirth().toGregorianCalendar().getTime());
                    }
                    
                    //benefit start date
                    if (cd.getCoverStartDate() != null) {
                        benefitDate = dateFormat.format(cd.getCoverStartDate().toGregorianCalendar().getTime());

                    }
                    
                    //join date
                    if (cd.getJoinDate() != null) {
                        joinDate = dateFormat.format(cd.getJoinDate().toGregorianCalendar().getTime());
                    } else {
                        joinDate = dateFormat.format(cd.getCoverStartDate().toGregorianCalendar().getTime());
                    }

                    //resign date
                    if (cd.getCoverEndDate() != null) {
                        String dateTo = new SimpleDateFormat("yyyy/MM/dd").format(cd.getCoverEndDate().toGregorianCalendar().getTime());
                        if (!dateTo.equalsIgnoreCase("2999/12/31")) {
                            resignDate = dateTo;
                        }

                    }

                    out.println("<tr>"
                            + "<td><label class=\"label\">" + opt.getOptionName() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getDependentNumber() + "</label></td>"
                            + "<td><label class=\"label\">" + pd.getName() + "</label></td>"
                            + "<td><label class=\"label\">" + pd.getSurname() + "</label></td>"
                            + "<td><label class=\"label\">" + dateOfBirth + "</label></td>"
                            + "<td><label class=\"label\">" + pd.getGenderDesc() + "</label></td>"
                            + "<td><label class=\"label\">" + pd.getIDNumber() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getCoverStatus() + "</label></td>"
                            + "<td><label class=\"label\">" + joinDate + "</label></td>"
                            + "<td><label class=\"label\">" + benefitDate + "</label></td>"
                            + "<td><label class=\"label\">" + resignDate + "</label></td>"
                            + "</tr>");

                    // out.println("<td><button name=\"opperation\" type=\"submit\" " + javaScript + " value=\"" + commandName + "\">Select</button></td></form>");

                }
            }


            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberDependantHistoryTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }
}

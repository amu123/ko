/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import com.koh.utils.FormatUtils;
import com.koh.utils.DateTimeUtils;
import java.util.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthHospitalLOC;

/**
 *
 * @author johanl
 */
public class RemoveAuthLOCFromList extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        String ti = request.getParameter("locIndex");
        int index = Integer.parseInt(ti);

        List<AuthHospitalLOC> locList = (List<AuthHospitalLOC>) session.getAttribute("AuthLocList");

        if (locList != null) {

            AuthHospitalLOC removedLoc = locList.get(index);
            boolean justRemove = false;
            if (locList.size() == (index + 1)) {
                justRemove = true;
            }

            //before remove reorder and recalculate loc dates and los
            Date removedStart = removedLoc.getDateFrom().toGregorianCalendar().getTime();
            System.out.println("removeStart = " + FormatUtils.dateTimeFormat.format(removedStart));
            locList.remove(removedLoc);
            System.out.println("new loc size after remove = " + locList.size());

            if (!justRemove) {

                Collections.sort(locList, new Comparator<AuthHospitalLOC>() {

                    public int compare(AuthHospitalLOC o1, AuthHospitalLOC o2) {
                        if (o1.getLevelOfCare() == null || o2.getLevelOfCare() == null) {
                            return 0;
                        }
                        return o1.getLevelOfCare().compareTo(o2.getLevelOfCare());
                    }
                });

                //if (reCalcPreviousDates) {
                System.out.println("remove loc index of new test add = " + index);

                boolean reCalcStartDates = true;

                for (int x = index; x < locList.size(); x++) {

                    AuthHospitalLOC loc = locList.get(x);
                    System.out.println("current loc id = " + loc.getLevelOfCare());
                    System.out.println("reCalcStartDates= " + reCalcStartDates);

                    if (reCalcStartDates) {

                        loc.setDateFrom(DateTimeUtils.convertDateToXMLGregorianCalendar(removedStart));

                        Date newEnd = null;
                        double los = loc.getLengthOfStay();

                        newEnd = DateTimeUtils.calculateLOCEndDateFromLOS(removedStart, los);

                        loc.setDateTo(DateTimeUtils.convertDateToXMLGregorianCalendar(newEnd));
                        reCalcStartDates = false;

                    } else {

                        //will always be the last entry
                        AuthHospitalLOC previousLoc = locList.get(x - 1);
                        //and recalc los
                        loc.setDateFrom(previousLoc.getDateTo());
                        double los = loc.getLengthOfStay();

                        Date currentNewEndDate = DateTimeUtils.calculateLOCEndDateFromLOS(loc.getDateFrom().toGregorianCalendar().getTime(), los);
                        XMLGregorianCalendar newEndDate = DateTimeUtils.convertDateToXMLGregorianCalendar(currentNewEndDate);
                        loc.setDateTo(newEndDate);


                    }
                    //x++;

                }


            } else {
                System.out.println("removing last loc from list");
                System.out.println("size = " + locList.size() + " , index = " + index + 1);
            }

            Collections.sort(locList, new Comparator<AuthHospitalLOC>() {

                public int compare(AuthHospitalLOC o1, AuthHospitalLOC o2) {
                    if (o1.getLevelOfCare() == null || o2.getLevelOfCare() == null) {
                        return 0;
                    }
                    return o1.getLevelOfCare().compareTo(o2.getLevelOfCare());
                }
            });
            session.setAttribute("AuthLocList", locList);

        }
        try {
            String nextJSP = "/PreAuth/AuthSpecificLOC.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "RemoveAuthLOCFromList";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import com.koh.preauth.command.CalculateCoPaymentAmounts;
import com.koh.preauth.utils.ERPUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.FormatUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthHospitalLOC;

/**
 *
 * @author johanl
 */
public class AuthLevelOfCareListTableTag extends TagSupport {

    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            out.println("<td colspan=\"9\"><table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr>"
                    + "<th align=\"left\">Level of Care</th>"
                    + "<th align=\"left\">Length Of Stay</th>"
                    + "<th align=\"left\">Approved Length Of Stay</th>"
                    + "<th align=\"left\">Date From</th>"
                    + "<th align=\"left\">Date To</th>"
                    + "<th align=\"left\">Ward Tariff</th>"
                    + "<th align=\"left\">Estimated Cost</th>"
                    + "<th align=\"left\">Savings Achieved</th>"
                    + "<th></th>"
                    + "<th></th>"
                    + "</tr>");

            ArrayList<AuthHospitalLOC> locList = (ArrayList<AuthHospitalLOC>) session.getAttribute(sessionAttribute);
            if (locList != null && !locList.isEmpty()) {
                locList = (ArrayList<AuthHospitalLOC>) DateTimeUtils.orderLOCListByDate(locList);
                int index = 0;

                String facilityDisc = "" + session.getAttribute("facilityProvDiscTypeId");
                int provDesc = 0;
                if (facilityDisc != null && !facilityDisc.equalsIgnoreCase("null")
                        && !facilityDisc.equalsIgnoreCase("")) {
                    provDesc = Integer.parseInt(facilityDisc);
                }

                String facility = "" + session.getAttribute("facilityProv_text");
                String prodId = "" + session.getAttribute("scheme");
                String optId = "" + session.getAttribute("schemeOption");
                String pICD = "" + session.getAttribute("primaryICD_text");

                int pID = Integer.parseInt(prodId);
                int oID = Integer.parseInt(optId);

                Date aDate = null;
                XMLGregorianCalendar xAdmission = null;
                String admission = "" + session.getAttribute("admissionDateTime");
                try {
                    aDate = FormatUtils.dateTimeFormat.parse(admission);
                    xAdmission = DateTimeUtils.convertDateToXMLGregorianCalendar(aDate);

                } catch (ParseException ex) {
                    Logger.getLogger(AuthLevelOfCareListTableTag.class.getName()).log(Level.SEVERE, null, ex);
                }

                double totalEstimatedWard = 0.0d;
                int i = 0;
                for (AuthHospitalLOC loc : locList) {

                    String wardTariff = "";
                    int wardLookupID = 280;
                    int locTypeID = 125;

                    if (provDesc == 79) {
                        wardLookupID = 281;
                        locTypeID = 272;

                    } else if (provDesc == 55) {
                        wardLookupID = 265;
                        locTypeID = 273;

                    } else if (provDesc == 59) {
                        wardLookupID = 266;
                        locTypeID = 274;

                    } else if (provDesc == 49) {
                        wardLookupID = 267;
                        locTypeID = 275;

                    } else if (provDesc == 76) {
                        wardLookupID = 268;
                        locTypeID = 276;

                    } else if (provDesc == 47) {
                        wardLookupID = 269;
                        locTypeID = 277;

                    } else if (provDesc == 56) {
                        wardLookupID = 270;
                        locTypeID = 278;

                    } else if (provDesc == 77) {
                        wardLookupID = 271;
                        locTypeID = 279;

                    }

                    wardTariff = NeoCommand.service.getNeoManagerBeanPort().getValueFromCodeTableForTableId(wardLookupID, loc.getLevelOfCare());
                    String locDesc = NeoCommand.service.getNeoManagerBeanPort().getValueFromCodeTableForTableId(locTypeID, loc.getLevelOfCare());

                    String hidden = loc.getLevelOfCareDesc() + "|" + loc.getLengthOfStay();

                    //recalculate ward amount by new los
                    double estimatedWardAmount = ERPUtils.getEstimatedCostByLOSForWard(xAdmission, pID, oID, wardTariff, facilityDisc, facility, pICD, loc.getLengthOfStay());
                    loc.setEstimatedCost(estimatedWardAmount);
                    totalEstimatedWard += estimatedWardAmount;

                    out.println("<tr id=\"locRow" + index + "\"><form>");
                    out.println(
                            "<td><label class=\"label\">" + locDesc + "</label><input type=\"hidden\" name=\"locHidden\" id=\"locHidden\" value=\"" + hidden + "\"/></td>"
                            + "<td><label class=\"label\">" + loc.getLengthOfStay() + "</label></td>"
                            + "<td><label class=\"label\">" + loc.getApprovedLos() + "</label></td>"
                            + "<td><label class=\"label\">" + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(loc.getDateFrom().toGregorianCalendar().getTime()) + "</label></td>"
                            + "<td><label class=\"label\">" + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(loc.getDateTo().toGregorianCalendar().getTime()) + "</label></td>"
                            + "<td><label class=\"label\">" + wardTariff + "</label></td>"
                            + "<td><label class=\"label\">" + loc.getEstimatedCost() + "</label></td>"
                            + "<td><label class=\"label\">" + loc.getSavingsAchieved() + "</label></td>"
                            + "<td><button type=\"button\" onClick=\"ModifyLOCFromList(" + i + ");\" >Modify</button></td>"
                            + "<td><button type=\"button\" onClick=\"RemoveLOCFromList(" + i + ");\" >Remove</button></td>"
                            + "</form></tr>");

                    i++;
                }
                session.setAttribute("estimatedWardCost", totalEstimatedWard);
                session.setAttribute(sessionAttribute, locList);
            }
            out.println("</table></td>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberSearchTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

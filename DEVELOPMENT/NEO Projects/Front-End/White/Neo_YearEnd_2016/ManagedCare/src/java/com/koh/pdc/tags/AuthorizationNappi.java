/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import com.koh.command.NeoCommand;
import java.util.Collection;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthTariffDetails;
import neo.manager.PreAuthConfirmationDetails;
import neo.manager.PreAuthDetails;

/**
 *
 * @author princes
 */
public class AuthorizationNappi extends TagSupport {

    private static final long serialVersionUID = 1L;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        Collection<PreAuthDetails> details = (Collection<PreAuthDetails>) session.getAttribute("authDetails");
        try {

            out.print("<tr>");
            out.print("<th scope=\"col\">Pre Auth Number</th>");
            out.print("<th scope=\"col\">Practise Number</th>");
            out.print("<th scope=\"col\">Practise Name</th>");
            out.print("<th scope=\"col\">ICD10 Code</th>");
            out.print("<th scope=\"col\">Tariff</th>");
            out.print("<th scope=\"col\">Action</th>");
            out.print("</tr>");
            int count = 0;
            if (details != null && details.size() > 0) {
                for (PreAuthDetails list : details) {

                    if (list.getAuthType().equals("6")) {
                        String tariffs = "";
                        for (AuthTariffDetails tariffsList : list.getAuthTariffs()) {
                            tariffs = tariffs + tariffsList.getTariffCode() + ", ";
                        }
                        out.print("<tr><form>");
                        if (list.getAuthNumber() != null) {
                            out.print("<td><label class=\"label\">" + list.getAuthNumber() + "</label></td>");
                        } else {
                            out.print("<td></td>");
                        }
                        if (list.getTreatingProviderNo() != null) {
                            out.print("<td><label class=\"label\">" + list.getTreatingProviderNo() + "</label></td>");
                            PreAuthConfirmationDetails preAuth = NeoCommand.service.getNeoManagerBeanPort().getAuthConfirmationDetails(list.getAuthDetailId(), list.getAuthNumber());
                            out.print("<td><label class=\"label\">" + preAuth.getProviderName() + "</label></td>");
                        } else {
                            out.print("<td>&nbsp;</td>");
                            out.print("<td>&nbsp;</td>");
                        }
                        if (list.getPrimaryICD() != null) {
                            out.print("<td><label class=\"label\">" + list.getPrimaryICD() + "</label></td>");
                        } else {
                            out.print("<td></td>");
                        }
                        out.print("<td><label class=\"label\">" + tariffs + "</label></td>");
                        out.println("<td>" + ""
                                + "<button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('GetAuthConfirmationDetailsForPDC','" + list.getAuthNumber() + "', " + list.getAuthDetailId() + ")\"; value=\"GetAuthConfirmationDetailsForPDC\">Select</button>"
                                + "</td>");
                        out.print("</form></tr>");
                        count++;
                    }
                }
            }

        } catch (java.io.IOException ex) {
            throw new JspException("Error in AuthorizationNappi tag", ex);
        }
        return super.doEndTag();
    }
}

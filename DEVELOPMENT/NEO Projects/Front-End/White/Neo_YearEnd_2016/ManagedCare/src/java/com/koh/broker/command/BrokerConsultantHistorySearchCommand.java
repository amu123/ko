/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.command.NeoCommand;
import com.koh.member.application.command.MemberApplicationSpecificQuestionsCommand;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.Broker;
import neo.manager.BrokerFirm;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class BrokerConsultantHistorySearchCommand extends NeoCommand{
 
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            search(request, response, context);
        } catch (Exception ex) {
            Logger.getLogger(MemberApplicationSpecificQuestionsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void search(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        String code = request.getParameter("brokerCode");
        String name = request.getParameter("brokerName");
        List<Broker> firm = port.getAllBrokersbyNameAndCode(code, name);
        request.setAttribute("BrokerSearchResults", firm);
        context.getRequestDispatcher("/Broker/BrokerHistoryResults.jsp").forward(request, response);
    }
    
    @Override
    public String getName() {
        return "BrokerConsultantHistorySearchCommand";
    }
}
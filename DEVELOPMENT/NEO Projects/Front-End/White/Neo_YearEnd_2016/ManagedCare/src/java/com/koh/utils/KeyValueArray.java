/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import neo.manager.KeyValue;

/**
 *
 * @author yuganp
 */
public class KeyValueArray extends neo.manager.KeyValueArray {
    
    public KeyValueArray() {
        keyValList = new ArrayList<KeyValue>();
    }
    
    public void put(String key, String value) {
        if (key == null) return;
        boolean found = false;
        for (KeyValue kv : keyValList) {
            if (key.equals(kv.getKey())){
                found = true;
                kv.setValue(value);
                break;
            }
        }
        if (!found) {
            KeyValue kv = new KeyValue();
            kv.setKey(key);
            kv.setValue(value);
            keyValList.add(kv);
        }
    }
    
    public String get(String key) {
        if (key == null) {
            return null;
        } else {
            for (KeyValue kv : keyValList) {
                if (key.equals(kv.getKey())) {
                    return kv.getValue();
                }
            }
            return null;
        }
    }
    
    public String getValue(String key) {
        return get(key);
    }
    
    public List<String> getKeySet() {
        ArrayList list = new ArrayList<String>();
        for (KeyValue kv : keyValList) {
            list.add(kv.getKey());
        }
        return list;
    }

    public void setKeyValList(List<KeyValue> keyValList) {
        this.keyValList = keyValList;
    }
}


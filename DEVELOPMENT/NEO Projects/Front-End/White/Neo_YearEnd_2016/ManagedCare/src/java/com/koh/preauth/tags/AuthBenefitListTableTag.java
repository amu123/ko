/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthBenefitLimits;

/**
 *
 * @author Johan-NB
 */
public class AuthBenefitListTableTag extends TagSupport {

    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        ServletRequest req = pageContext.getRequest();
        List<AuthBenefitLimits> benList = (List<AuthBenefitLimits>) session.getAttribute(sessionAttribute);

        try {
            String message = (String) req.getAttribute("authBenAllowResultsMessage");
            if (message != null && !message.equalsIgnoreCase("")) {
                out.println("<label class=\"red\">" + message + "</label></br></br>");
            }

            out.println("<tr><td><label class=\"subheader\">Benefits Details</label></td></tr></br></br>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr>"
                    + "<th align=\"left\">Limit</th>"
                    + "<th align=\"left\">Sub-Limit</th>"
                    + "<th align=\"left\">Sub-Sub-Limit</th>"
                    + "<th align=\"left\">Limit Amount</th>"
                    + "<th align=\"left\">Amount Used</th>"
                    + "<th align=\"left\">Amount Available</th>"
                    + "<th align=\"left\">Limit Count</th>"
                    + "<th align=\"left\">Count Used</th>"
                    + "<th align=\"left\">Count Available</th>"
                    + "<th align=\"left\">Action</th></tr>");


            boolean famBenIndNote = false;
            java.text.DecimalFormat desFormat = new java.text.DecimalFormat("#.##");
            if (benList != null && !benList.isEmpty()) {

                String mainBenefit = "";
                String midBenefit = "";

                for (AuthBenefitLimits ccb : benList) {

                    String mainB = ccb.getMainDesc();

                    String mainBenCode = ccb.getMainBenefitCode();
                    String midBenCode = ccb.getMidBenefitCode();

                    boolean addMainBenButton = false;
                    boolean addMidBenButton = false;

                    double mainAmount = 0.0d;
                    double mainAmountUsed = 0.0d;
                    double mainAmountAvail = 0.0d;
                    int mainCount = 0;
                    int mainCountUsed = 0;
                    int mainCountAvail = 0;
                    double midAmount = 0.0d;
                    double midAmountUsed = 0.0d;
                    double midAmountAvail = 0.0d;
                    int midCount = 0;
                    int midCountUsed = 0;
                    int midCountAvail = 0;


                    if (mainB != null && mainB.equals(mainBenefit)) {
                        mainB = "";

                    } else {
                        int mainBenId = ccb.getMainBenefitId();
                        mainBenefit = mainB;
                        mainB = "";

                        mainAmount = ccb.getMainLimitAmount();
                        mainAmountUsed = ccb.getMainLimitUsedAmount();
                        mainAmountAvail = ccb.getMainLimitAvailAmount();
                        mainCount = ccb.getMainLimitCount();
                        mainCountUsed = ccb.getMainLimitUsedCount();
                        mainCountAvail = ccb.getMainLimitAvailCount();

                        if (mainAmount != 0.0d) {
                            if (mainAmount == 9999999.99d) {
                                addMainBenButton = true;
                            }
                            try {
                                mainAmount = desFormat.parse(desFormat.format(mainAmount)).doubleValue();

                            } catch (ParseException ex) {
                                System.out.println("exception" + ex);
                            }
                        }
                        if (mainAmountUsed != 0.0d) {
                            try {
                                mainAmountUsed = desFormat.parse(desFormat.format(mainAmountUsed)).doubleValue();

                            } catch (ParseException ex) {
                                System.out.println("exception" + ex);
                            }
                        }

                        if (mainAmountAvail != 0.0d) {
                            if (mainAmountAvail > 0.0d) {
                                addMainBenButton = true;
                            }
                            try {
                                mainAmountAvail = desFormat.parse(desFormat.format(mainAmountAvail)).doubleValue();
                            } catch (ParseException ex) {
                                System.out.println("exception" + ex);
                            }
                        }

                        if (mainCount > 0 && mainCountAvail > 0) {
                            addMainBenButton = true;
                        }

                        if (ccb.getFamilyBenefitInd() != null && ccb.getFamilyBenefitInd().equalsIgnoreCase("main")) {
                            famBenIndNote = true;

                            out.println("<tr><td><label class=\"noteLabel\">" + mainBenefit + "</label></td>");
                            out.println("<td><label class=\"noteLabel\"></label></td>"
                                    + "<td><label class=\"noteLabel\"></label></td>"
                                    + "<td><label class=\"noteLabel\">" + mainAmount + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + mainAmountUsed + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + mainAmountAvail + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + mainCount + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + mainCountUsed + "</label></td>"
                                    + "<td><label class=\"noteLabel\">" + mainCountAvail + "</label></td>");

                        } else {
                            out.println("<tr>"
                                    + "<td><label class=\"label\">" + mainBenefit + "</label></td>"
                                    + "<td><label class=\"label\"></label></td>"
                                    + "<td><label class=\"label\"></label></td>"
                                    + "<td><label class=\"label\">" + mainAmount + "</label></td>"
                                    + "<td><label class=\"label\">" + mainAmountUsed + "</label></td>"
                                    + "<td><label class=\"label\">" + mainAmountAvail + "</label></td>"
                                    + "<td><label class=\"label\">" + mainCount + "</label></td>"
                                    + "<td><label class=\"label\">" + mainCountUsed + "</label></td>"
                                    + "<td><label class=\"label\">" + mainCountAvail + "</label></td>");
                        }



                        if (addMainBenButton) {
                            out.println("<td><button type=\"button\" onClick=\"submitWithAction('" + commandName + "','" + mainBenCode + "','" + mainBenefit + "');\" >Select</button></td>");
                        } else {
                            out.println("<td><label class=\"error\">Limit Exceeded</label></td>");
                        }
                        out.println("</tr>");

                    }

                    String midB = ccb.getMidDesc();
                    boolean midAdded = false;

                    if (midB != null) {
                        if (midB.equals(midBenefit)) {
                            midB = "";

                        } else {
                            int midBenId = ccb.getMidBenefitId();
                            midAdded = true;
                            midBenefit = midB;
                            midB = "";

                            midAmount = ccb.getMidLimitAmount();
                            midAmountUsed = ccb.getMidLimitUsedAmount();
                            midAmountAvail = ccb.getMidLimitAvailAmount();
                            midCount = ccb.getMidLimitCount();
                            midCountUsed = ccb.getMidLimitUsedCount();
                            midCountAvail = ccb.getMidLimitAvailCount();

                            if (midAmount != 0.0d) {
                                try {
                                    midAmount = desFormat.parse(desFormat.format(midAmount)).doubleValue();
                                } catch (ParseException ex) {
                                    System.out.println("exception" + ex);
                                }
                            }
                            if (midAmountUsed != 0.0d) {
                                try {
                                    midAmountUsed = desFormat.parse(desFormat.format(midAmountUsed)).doubleValue();
                                } catch (ParseException ex) {
                                    System.out.println("exception" + ex);
                                }
                            }
                            if (midAmountAvail != 0.0d) {
                                if (midAmountAvail > 0.0d) {
                                    addMidBenButton = true;
                                }
                                try {
                                    midAmountAvail = desFormat.parse(desFormat.format(midAmountAvail)).doubleValue();
                                } catch (ParseException ex) {
                                    System.out.println("exception" + ex);
                                }
                            }

                            if (midAmount == 9999999.99d) {
                                addMidBenButton = true;
                            }

                            if (midCount > 0 && midCountAvail > 0) {
                                addMidBenButton = true;
                            }

                            if (ccb.getFamilyBenefitInd() != null && ccb.getFamilyBenefitInd().equalsIgnoreCase("mid")) {
                                famBenIndNote = true;
                                out.println("<tr>"
                                        + "<td><label class=\"noteLabel\">" + mainB + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midBenefit + "</label></td>"
                                        + "<td><label class=\"noteLabel\"></label></td>"
                                        + "<td><label class=\"noteLabel\">" + midAmount + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midAmountUsed + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midAmountAvail + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midCount + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midCountUsed + "</label></td>"
                                        + "<td><label class=\"noteLabel\">" + midCountAvail + "</label></td>");

                            } else {
                                out.println("<tr>"
                                        + "<td><label class=\"label\">" + mainB + "</label></td>"
                                        + "<td><label class=\"label\">" + midBenefit + "</label></td>"
                                        + "<td><label class=\"label\"></label></td>"
                                        + "<td><label class=\"label\">" + midAmount + "</label></td>"
                                        + "<td><label class=\"label\">" + midAmountUsed + "</label></td>"
                                        + "<td><label class=\"label\">" + midAmountAvail + "</label></td>"
                                        + "<td><label class=\"label\">" + midCount + "</label></td>"
                                        + "<td><label class=\"label\">" + midCountUsed + "</label></td>"
                                        + "<td><label class=\"label\">" + midCountAvail + "</label></td>");

                            }



                            if (addMidBenButton) {
                                out.println("<td><button type=\"button\" onClick=\"submitWithAction('" + commandName + "','" + midBenCode + "','" + midBenefit + "');\" >Select</button></td>");
                            } else {
                                out.println("<td><label class=\"error\">Limit Exceeded</label></td>");
                            }
                            out.println("</tr>");

                        }
                    }
                }
            }

            out.println("</table>");
            //set family limit notification
            if (famBenIndNote) {
                out.println("<br/><table><tr align=\"left\"><td colspan=\"10\" ><label class=\"noteLabel\">* Note: Indicates Family Limits</label></td></tr></table>");
            }

        } catch (java.io.IOException ex) {
            throw new JspException("Error in AuthBenefitListDisplay tag", ex);
        }

        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

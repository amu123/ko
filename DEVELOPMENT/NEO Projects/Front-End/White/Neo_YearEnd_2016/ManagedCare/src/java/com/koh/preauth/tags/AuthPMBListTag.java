/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import static com.koh.command.NeoCommand.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthPMBDetails;
import neo.manager.NeoManagerBean;
import org.w3c.dom.Document;

/**
 *
 * @author johanl
 */
public class AuthPMBListTag extends TagSupport {
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String pmbFlag = "0";
        Map<String, String> pmbMap = new HashMap<String, String>();
        List<AuthPMBDetails> savedPMBList = (List<AuthPMBDetails>) session.getAttribute("savedAuthPMBs");
        //first add to hashmap
        if (savedPMBList != null && !savedPMBList.isEmpty()) {
            for (AuthPMBDetails pmb : savedPMBList) {
                String code = pmb.getPmbCode();
                System.out.println("Putting code in PMBMap: " + code);
                pmbMap.put(code, code);
            }
        }

        try {

            out.println("<td colspan=\"6\"><table id=\"pmbSelection\" width=\"800px\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr>"
                    + "<th align=\"left\">PMB Code</th>"
                    + "<th align=\"left\">Diagnosis</th>"
                    + "<th align=\"left\">Treatment</th>"
                    + "<th align=\"left\">Primary ICD</th>"
                    + "<th align=\"left\">Secondary ICD</th>"
                    + "<th align=\"left\">Co-Mobidity ICD</th>"
                    + "<th align=\"left\">Select</th>"
                    + "</tr>");

            ArrayList<AuthPMBDetails> pmbList = (ArrayList<AuthPMBDetails>) session.getAttribute("authPMBDetails");

            if (pmbList != null && !pmbList.isEmpty()) {
                
                int index = 0;
                for (AuthPMBDetails pmb : pmbList) {

                    String pICD = pmb.getPrimaryICD();
                    String sICD = pmb.getSecondaryICD();
                    String cICD = pmb.getComorbidityICD();

                    if (pICD == null || pICD.equalsIgnoreCase("null")) {
                        pICD = "";
                    }
                    if (sICD == null || sICD.equalsIgnoreCase("null")) {
                        sICD = "";
                    }
                    if (cICD == null || cICD.equalsIgnoreCase("null")) {
                        cICD = "";
                    }

                    out.println("<tr id=\"pmbRow" + index + "\">"
                            + "<td><label class=\"label\">" + pmb.getPmbCode() + "</label></td> "
                            + "<td width=\"300\"><label class=\"label\">" + pmb.getPmbDiagnosis() + "</label></td> "
                            + "<td width=\"300\"><label class=\"label\">" + pmb.getPmbTreatment() + "</label></td> "
                            + "<td><label class=\"label\">" + pICD + "</label></td> "
                            + "<td><label class=\"label\">" + sICD + "</label></td> "
                            + "<td><label class=\"label\">" + cICD + "</label></td>");

                    boolean foundCPT = pmbMap.containsKey(pmb.getPmbCode());
                    String selected = "false";
                    //|| session.getAttribute("foundPMB").toString().equalsIgnoreCase("yes")

                    if (session.getAttribute("updateAuthNumber") != null) {
                        pmbFlag = port.getPMBFlaged(session.getAttribute("updateAuthNumber").toString());
                        session.setAttribute("pmbFlag", pmbFlag);
                        try {
                            if (session.getAttribute("check").toString() != null) {
                                selected = session.getAttribute("pmbSelected").toString();
                            }
                        } catch (Exception e) {
                            System.out.println("selected is null");
                            if (pmbFlag.equalsIgnoreCase("1")) {
                                selected = "true";
                            }
                        }


                    }else{
                        try {
                            if (session.getAttribute("pmbSelected").toString() != null) {
                                selected = session.getAttribute("pmbSelected").toString();
                            }
                        } catch (Exception e) {
                            System.out.println("selected is null");
                        }
                    }
                        
                    if(foundCPT) {
                        out.println("<td><input id=\"boxes\" type=\"checkbox\" checked onclick=\"setCheck(this.checked)\" /></td> ");
                    } else {
                        out.println("<td><input id=\"boxes\" type=\"checkbox\" onclick=\"setCheck(this.checked)\" /></td> ");
                    }

                    out.println("</tr>");

                    index++;
                }
            }
            out.println("</table></td>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in AuthPMBListTag tag", ex);
        }

        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

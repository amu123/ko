/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import neo.manager.MemberDependantApp;
import neo.manager.NeoManagerBean;
import neo.manager.NeoManagerBeanService;

/**
 *
 * @author yuganp
 */
public class MemberAppCommon {
    
    public static Map<Integer,String> getDependantMap(NeoManagerBeanService service, int applicationNumber) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        List<MemberDependantApp> depList = port.getMemberApplicationDependantByApplicationNumber(applicationNumber);
        Map<Integer,String> map = new HashMap<Integer,String>();
        for (MemberDependantApp app :depList) {
            map.put(app.getDependentNumber(), "" + app.getDependentNumber() + " " + (app.getFirstName() == null ? "" : app.getFirstName()) + " " + (app.getLastName() == null ? "" : app.getLastName()));
        }
        return map;
    }

    public static List<Map> getDependantListMap(NeoManagerBeanService service, int applicationNumber, String appType) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        List<MemberDependantApp> depList = port.getMemberApplicationDependantByApplicationNumber(applicationNumber);
        List<Map> dlist = new ArrayList();
        for (MemberDependantApp da : depList) {
            if (appType != null && appType.equals("1")) {
//                if (da.getDependentNumber() == 0) {
//                    continue;
//                }
            }
            Map<String,String> depMap = new HashMap<String,String>();
            depMap.put("id",Integer.toString(da.getDependentNumber()));
            depMap.put("name",(da.getFirstName() == null ? "" : da.getFirstName()));            
            depMap.put("surname",(da.getLastName() == null ? "" :  da.getLastName()));
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            String birthDate = format.format(da.getDateOfBirth().toGregorianCalendar().getTime());
            depMap.put("birthDate",(da.getDateOfBirth() == null ? "" : birthDate));
            depMap.put("value","" + da.getDependentNumber() + " " + (da.getFirstName() == null ? "" : da.getFirstName()) + " " + (da.getLastName() == null ? "" : da.getLastName()));
            dlist.add(depMap);
        }
        return dlist;
        
    }
}

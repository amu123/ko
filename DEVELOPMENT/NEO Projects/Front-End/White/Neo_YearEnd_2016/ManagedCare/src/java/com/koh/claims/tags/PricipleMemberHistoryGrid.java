/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.tags;

import com.agile.security.webservice.SecurityResponsibility;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.EAuthCoverDetails;
import neo.manager.NeoUser;

/**
 *
 * @author Johan-NB
 */
public class PricipleMemberHistoryGrid extends TagSupport {

    private String sessionAttribute;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        ServletContext sctx = pageContext.getServletContext();
        String client = String.valueOf(sctx.getAttribute("Client"));

        try {
            ArrayList<EAuthCoverDetails> members = (ArrayList<EAuthCoverDetails>) session.getAttribute(sessionAttribute);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            if (members != null && !members.isEmpty()) {

                //out.println("<label class=\"header\">Dependants</label></br></br>");
                out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.println("<tr>"
                        + "<th align=\"left\">Cover Number</th>"
                        + "<th align=\"left\">Option</th>"
                        + "<th align=\"left\">Status</th>"
                        + "<th align=\"left\">Join Date</th>"
                        + "<th align=\"left\">Option Date</th>"
                        + "<th align=\"left\">Resigned Date</th>"
                        + "<th></th>"
                        + "</tr>");

                for (int i = 0; i < members.size(); i++) {
                    EAuthCoverDetails cd = members.get(i);
                    session.setAttribute("memberNumber", cd.getCoverNumber());

                    String joinDate = "";
                    String benefitDate = "";
                    String resignDate = "";

                    //benefit start date
                    if (cd.getCoverStartDate() != null) {
                        benefitDate = dateFormat.format(cd.getCoverStartDate().toGregorianCalendar().getTime());
                    }

                    //join date
                    if (cd.getJoinDate() != null) {
                        joinDate = dateFormat.format(cd.getJoinDate().toGregorianCalendar().getTime());
                    } else {
                        joinDate = dateFormat.format(cd.getCoverStartDate().toGregorianCalendar().getTime());
                    }

                    //resign date
                    String dateTo = "";
                    if (cd.getCoverEndDate() != null) {
                        dateTo = new SimpleDateFormat("yyyy/MM/dd").format(cd.getCoverEndDate().toGregorianCalendar().getTime());
                        if (!dateTo.equalsIgnoreCase("2999/12/31")) {
                            resignDate = dateTo;
                        }

                    }
                    NeoUser user = (NeoUser) session.getAttribute("persist_user");
                    session.setAttribute("saveOption", "yes");
                    /*System.out.println("Option Id " + cd.getOptionLookup().getId());
                    if ((cd.getOptionLookup().getId().equals("11")) || (cd.getOptionLookup().getId().equals("12")) || (cd.getOptionLookup().getId().equals("13"))) {
                        session.setAttribute("saveOption", "yes");
                        
                    }else{
                        session.setAttribute("saveOption", "no");
                    }*/
                    for (neo.manager.SecurityResponsibility resp : user.getSecurityResponsibility()){
                        if (resp.getResponsibilityId() == 9){
                             session.setAttribute("saveOption", "no");
                        }
                    }
                    String rowColor = "<tr>";
                    if(cd.getCoverStatus().equalsIgnoreCase("Suspend") && client.equalsIgnoreCase("Sechaba")){
                        rowColor = "<tr style=\"background-color: #ff4d4d\">";
                    }
                    
                    out.println(rowColor
                            + "<td><label class=\"label\">" + cd.getCoverNumber() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getOption() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getCoverStatus() + "</label></td>"
                            + "<td><label class=\"label\">" + joinDate + "</label></td>"
                            + "<td><label class=\"label\">" + benefitDate + "</label></td>"
                            + "<td><label class=\"label\">" + resignDate + "</label></td>");

                    if (javaScript != null && !javaScript.equalsIgnoreCase("")) {

                        if (javaScript.equalsIgnoreCase("memberMainSearch")) {
                            out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"reloadCoverDeps('" + cd.getCoverNumber() + "','" + cd.getCoverStartDate() + "','" + cd.getCoverEndDate() + "')\" >Details</button></td>");
                        } else {
                            out.println("<td><button name=\"opperation\" type=\"button\" " + javaScript + " >Details</button></td>");
                        }

                    } else {
                        out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"reloadCoverHistory('" + cd.getCoverNumber() + "','" + benefitDate + "','" + dateTo + "')\" >Details</button></td>");
                    }

                    out.println("</tr>");
                }
            }
            out.println("</table>");

        } catch (java.io.IOException ex) {
            throw new JspException("Error in PricipleMemberHistoryGrid tag", ex);
        }

        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

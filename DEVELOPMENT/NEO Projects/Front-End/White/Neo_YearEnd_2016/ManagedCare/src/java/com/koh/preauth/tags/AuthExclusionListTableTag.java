/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthCoverExclusions;
import neo.manager.Diagnosis;
import neo.manager.EntityTimeDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class AuthExclusionListTableTag extends TagSupport {

    private String commandName;
    private String javaScript;
    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        ServletRequest req = pageContext.getRequest();

        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

        try {
            List<AuthCoverExclusions> aeList = (List<AuthCoverExclusions>) session.getAttribute("AuthExclusionList");
            if (aeList != null) {
                System.out.println("AuthExclusionList size = " + aeList.size());
                out.println("<td colspan=\"9\"><table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.println("<tr>" +
                        "<th align=\"left\">Member Number</th>" +
                        "<th align=\"left\">Exclusion Type</th>" +
                        "<th align=\"left\">Diagnosis Code</th>" +
                        "<th colspan=\"2\" align=\"left\">Diagnosis Description</th>" +
                        "<th align=\"left\">Exclusion Start Date</th>" +
                        "<th align=\"left\">Exclusion End Date</th>" +
                        "<th align=\"left\">Duration</th>" +
                        "<th align=\"left\">PMB Allowed</th>" +
                        "</tr>");
                for (AuthCoverExclusions ae : aeList) {

                    String exclusionType = port.getValueFromCodeTableForTableId(82, ae.getExclusionType());

                    out.println("<tr><td><label class=\"label\">" + ae.getCoverNumber() + "</label></td>");
                    out.println("<td><label class=\"label\">" + exclusionType + "</label></td>");
                    //check diagmosis code
                    if(ae.getDiagnosisCode() != null){
                        out.println("<td><label class=\"label\">" + ae.getDiagnosisCode() + "</label></td>");
                        Diagnosis d = port.getDiagnosisForCode(ae.getDiagnosisCode());
                        if(d != null){
                            out.println("<td colspan=\"2\"><label class=\"label\">"+ d.getDescription() +"</label></td>");
                        }else{
                            out.println("<td colspan=\"2\"><label class=\"label\">*Description not found*</label></td>");
                        }
                        
                    }else{
                        out.println("<td></td>");
                        out.println("<td colspan=\"2\"></td>");
                    }
                    //check start date
                    if (ae.getExclusionDateFrom() != null) {
                        String dateFrom = new SimpleDateFormat("yyyy/MM/dd").format(ae.getExclusionDateFrom().toGregorianCalendar().getTime());
                        out.println("<td><label class=\"label\">" + dateFrom + "</label></td>");
                    } else {
                        out.println("<td></td>");
                    }
                    //check end date
                    if (ae.getExclusionDateTo() != null) {
                        String dateFrom = new SimpleDateFormat("yyyy/MM/dd").format(ae.getExclusionDateTo().toGregorianCalendar().getTime());
                        out.println("<td><label class=\"label\">" + dateFrom + "</label></td>");
                    } else {
                        out.println("<td></td>");
                    }
                    //calculating duration
                    XMLGregorianCalendar xmlFrom = ae.getExclusionDateFrom();
                    XMLGregorianCalendar xmlTo = ae.getExclusionDateTo();
                    @SuppressWarnings("static-access")
                    EntityTimeDetails etd = port.getTimeDifference(xmlFrom, xmlTo);
                    if(etd != null){
                        out.println("<td><label class=\"label\">" + etd.getReturnMsg() + "</label></td>");
                    }
                    out.println("<td><label class=\"label\">" + ae.getPmbAllowed() + "</label></td>");
                }

                out.println("</table></td>" +
                        "<tr><td>" +
                            "<button name=\"opperation\" type=\"submit\" " + javaScript + " value=\"" + commandName + "\">Close</button>" +
                         "</td></tr>");

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;
import neo.manager.AuthCoverExclusions;
import com.koh.utils.*;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Collection;
import neo.manager.MainList;
import neo.manager.NeoUser;
import neo.manager.PdcSearchCriteria;

/**
 *
 * @author gerritr
 */
public class CoverDependantResultCommand extends NeoCommand {

    private final int minResult = 0;
    private final int maxResult = 100;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        session.removeAttribute("eventTaskTable"); // to Alt between tables 
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        PdcSearchCriteria search = new PdcSearchCriteria();

        System.out.println("Page 1 : " + search.getMinResult() + " Page : " + search.getMaxResult() + " maxResult " + maxResult + " minResult " + minResult);

        search.setUser(user.getUserId());
        search.setMinResult(minResult);
        search.setMaxResult(maxResult);

        String coverNumber = request.getParameter("ID_text");
        System.out.println("CoverNumber - PolicyNumber: " + coverNumber);
//        String dateFrom = request.getParameter("dateFrom");
//        String dateTo = request.getParameter("dateTo");
//        String pdcType = request.getParameter("pdcType");
//        String riskRating = request.getParameter("riskRating");
//        String pdcUser = request.getParameter("pdcUser");
//        String eventStatus = request.getParameter("eventStatus");

        if (coverNumber != null) {
            search.setCoverNumber(coverNumber);
        }

        session.setAttribute("search", search);

        System.out.println("Search - Covernumber: " + search.getCoverNumber());

        //-------------Break!
        Collection<MainList> mainList = new ArrayList<MainList>();
        Collection<MainList> mList = new ArrayList<MainList>();

        mainList = NeoCommand.service.getNeoManagerBeanPort().findWorkbenchDetailsByCriteria(search, user, ""+session.getAttribute("persist_user_pdcAdmin"));

        System.out.println("mainList - Size: " + mainList.size());

        session.setAttribute("mainList", mainList);
        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        mList = DateTimeUtils.orderMainListByCreatedDate((ArrayList) mainList);

        //-----------Break #2 - Generate Session (used for MemberCoverDetails's Select button)
        int count = 0;
        int worklistNumber = 1;
        //List<MainList> arrHiddenTbl = new <MainList>ArrayList();
        for (MainList wl : mList) {
            //worklistNumber;
//                arrHiddenTbl.add(wl.getCoverNumber());
//                arrHiddenTbl.add(wl.getEventStatus().getValue());
//                arrHiddenTbl.add(wl.getEventSource().getValue());
//                arrHiddenTbl.add(wl.getEventType().getValue());
//                arrHiddenTbl.add(wl.getEventDescription());
//                NeoUser userBy = NeoCommand.service.getNeoManagerBeanPort().getUserSafeDetailsById(wl.getAssignedBy());
//                NeoUser userTo = NeoCommand.service.getNeoManagerBeanPort().getUserSafeDetailsById(wl.getAssignedTo());
//                arrHiddenTbl.add(userBy.getUsername());
//                arrHiddenTbl.add(userTo.getUsername());
//                arrHiddenTbl.add(wl.getEventRisk());
//                Date create = wl.getCreatedDate().toGregorianCalendar().getTime();
//                createdDate = formatter.format(create);
//                arrHiddenTbl.add(createdDate);
//                if ( wl.getClosedDate() != null) {
//                Date closed = wl.getClosedDate().toGregorianCalendar().getTime();
//                closedDate = formatter.format(closed);
//                arrHiddenTbl.add(closedDate);
//                }else{
//                arrHiddenTbl.add("");
//                }
//                if (wl.getEventStatus().getId().equals("1")){
//                //out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('ViewWorkBenchCommand', " + count + ")\"; value=\"ViewWorkBenchCommand\">Details</button></td>");
//                }else{
//                    //out.println("<td>&nbsp;</td>");
//                }
//                EventTask task = NeoCommand.service.getNeoManagerBeanPort().getEventTaskByEventId(wl.getEventId());
//                if (task.getEventId() != 0) {                    
//                    //out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('AssignTaskCommand', " + count + ")\"; value=\"AssignTaskCommand\">Task</button></td>");
//                } else {                    
//                    //out.println("<td>&nbsp;</td>");
//                }
//                
//                if(task.getTaskDueDate() != null){
//                    Date dueDate = DateTimeUtils.getDateFromXMLGregorianCalendar(task.getTaskDueDate());
//                    Date curDate = new Date();
//                    if (dueDate.equals(curDate)){
//                        arrHiddenTbl.add(DateTimeUtils.convertToYYYYMMDD(dueDate));
//                    } else if(dueDate.before(curDate)){
//                        arrHiddenTbl.add(DateTimeUtils.convertToYYYYMMDD(dueDate));
//                    } else if(dueDate.after(curDate)){
//                        arrHiddenTbl.add(DateTimeUtils.convertToYYYYMMDD(dueDate));
//                    } 
//                } else {
//                    arrHiddenTbl.add(" ");
//                }          
            count++;
            worklistNumber++;
        }
        System.out.println("WorklistNumber: " + worklistNumber);
        //System.out.println("arrHiddenTbl - Size: " + arrHiddenTbl.size());

        //-----------Break #3
        List<CoverDetails> covList = port.getDependantsForPrincipleMemberByDate(coverNumber);

        session.setAttribute("coverNum", coverNumber);
        List<AuthCoverExclusions> arrWait = new ArrayList<AuthCoverExclusions>();

        count = 0;
        ArrayList arrWorklistFlag = new ArrayList();
        ArrayList<MainList> workList = (ArrayList<MainList>) session.getAttribute("mainList");
        for (CoverDetails cd : covList) {
            try {
                if (workList != null) {
                    if (workList.size() > count) {
                        MainList workL = workList.get(count);
                        if (workL == null) {
                            arrWorklistFlag.add("no");
                        } else {
                            arrWorklistFlag.add("yes");
                        }
                    } else {
                        arrWorklistFlag.add("no");
                    }
                } else {
                    arrWorklistFlag.add("no");
                }
            } catch (IndexOutOfBoundsException e) {
                System.out.println("No WorkList members: " + e.getMessage());
                arrWorklistFlag.add("no");
            }

            List<AuthCoverExclusions> waitingPeriod = port.getActiveCoverExclusionsByCoverDependant(coverNumber, cd.getDependentNumber() + "");

            if (waitingPeriod != null && !waitingPeriod.isEmpty()) {
                for (AuthCoverExclusions wp : waitingPeriod) {

                    if (wp.getExclusionType().equals("4")) {
                        if (wp.getExclusionDateFrom() != null) {
                            arrWait.add(wp);
                        }
                    }
                }
            } else {
                arrWait.add(null); //adds null to fill the spaces
                System.out.println("Null added to arrWait [size : " + arrWait.size() + "]");
            }

            count++;
        }

        //Converting the dates
        ArrayList arrDoB = new ArrayList();
        ArrayList arrJoinDate = new ArrayList();
        ArrayList arrCoverStartDate = new ArrayList();
        ArrayList arrCoverEndDate = new ArrayList();
        ArrayList arrApplicationRecievedDate = new ArrayList();

        Date update = null;
        for (CoverDetails cov : covList) {
            if (cov.getDateOfBirth() != null) {
                update = cov.getDateOfBirth().toGregorianCalendar().getTime();
                arrDoB.add(formatter.format(update));
            } else {
                arrDoB.add(" ");
            }

            if (cov.getSchemeJoinDate() != null) {
                update = cov.getSchemeJoinDate().toGregorianCalendar().getTime();
                arrJoinDate.add(formatter.format(update));
            } else {
                arrJoinDate.add(" ");
            }

            if (cov.getCoverStartDate() != null) {
                update = cov.getCoverStartDate().toGregorianCalendar().getTime();
                arrCoverStartDate.add(formatter.format(update));
            } else {
                arrCoverStartDate.add(" ");
            }

            if (cov.getApplicationRecievedDate() != null) {
                update = cov.getApplicationRecievedDate().toGregorianCalendar().getTime();
                arrApplicationRecievedDate.add(formatter.format(update));
            } else {
                arrApplicationRecievedDate.add(" ");
            }

            if (cov.getCoverEndDate() != null) {
                update = cov.getCoverEndDate().toGregorianCalendar().getTime();
                arrCoverEndDate.add(formatter.format(update));
            } else {
                arrCoverEndDate.add(" ");
            }
        }
        session.setAttribute("covDateOfBirth", arrDoB);
        session.setAttribute("covJoinDate", arrJoinDate);
        session.setAttribute("covCoverStartDate", arrCoverStartDate);
        session.setAttribute("covCoverEndDate", arrCoverEndDate);
        session.setAttribute("covApplicationRecievedDate", arrApplicationRecievedDate);

        session.setAttribute("WorkListFlag", arrWorklistFlag);
        session.setAttribute("MemberCoverDetails", covList);
        System.out.println("MemberCoverDetails att: " + covList.size());
        session.setAttribute("WaitingPeriodDetails", arrWait);

        PrintWriter out = null;

        try {
            out = response.getWriter();

            if (search != null) {
                out.print("Done|");
            } else {
                out.print("Failed|");
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(CoverDependantResultCommand.class
                    .getName()).log(Level.SEVERE, null, ex);
            System.out.print(
                    "Exception PRINTWRITER: " + ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "CoverDependantResultCommand";
    }
}

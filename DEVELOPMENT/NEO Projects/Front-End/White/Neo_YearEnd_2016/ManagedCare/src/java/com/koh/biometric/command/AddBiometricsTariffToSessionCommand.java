/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.GenericTariff;

/**
 *
 * @author josephm
 */
public class AddBiometricsTariffToSessionCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
       // String tariffDesc = "" +request.getAttribute("tariffDescription");
        String tariffDesc = request.getParameter("tariffDescription");
        String tariffCode = ""+request.getParameter("tariffCode");
       // String tariffQuantity = "" + session.getAttribute("tariffQuantity");
       // String tariffAmount = "" + session.getAttribute("tariffAmount");
        //String testResult = ""+request.getAttribute("testResult");
                String testResult = request.getParameter("testResults");

        System.out.println("Debugging the values........");
        System.out.println("The tariff description " + request.getAttribute("tariffDescription"));
        System.out.println("The tariff code " +request.getAttribute("tariffCode"));
        System.out.println("The test results " +session.getAttribute("testResult"));

        
        GenericTariff at = new GenericTariff();
        //Collection<GenericTariff> sessionList = (ArrayList<GenericTariff>) session.getAttribute("biometricsTariffListArray");
        GenericTariff general = service.getNeoManagerBeanPort().fetchTariffs(tariffCode);


        //GenericTariff at = new GenericTariff();
        if (!tariffDesc.trim().equalsIgnoreCase("") && !tariffDesc.trim().equalsIgnoreCase("null")) {
            at.setTariffDescription(tariffDesc);           
        }
        if (!tariffCode.trim().equalsIgnoreCase("") && !tariffCode.trim().equalsIgnoreCase("null")) {
            at.setTariffCode(tariffCode);           
        }
        if (!testResult.trim().equalsIgnoreCase("") && !testResult.trim().equalsIgnoreCase("null")) {
            double test = Double.parseDouble(testResult);
            at.setTestResult(test);
       }

       // atList.add(at);
        //add array to session
        session.setAttribute("biometricsTariffListArray", at);
        //session.removeAttribute("tariffDesc");
        //session.removeAttribute("tariffCode");
        //session.removeAttribute("tariffQuantity");
        //session.removeAttribute("tariffAmount");


        try {
            String nextJSP = "/biometrics/BiometricsSpecificTariff.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "AddBiometricsTariffToSessionCommand";
    }
}

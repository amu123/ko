/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.auth.command;

import com.agile.security.webservice.AuthDetails;
import com.koh.command.FECommand;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.NeoUser;

/**
 *
 * @author gerritj
 */
public class SaveAuthDetailsCommand extends FECommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        try {
            HttpSession session = request.getSession();
            
            AuthDetails ad = new AuthDetails();

            //User detail
            NeoUser user = (NeoUser) session.getAttribute("persist_user");
            ad.setModifiedBy(user.getName() + " " + user.getSurname());
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTimeInMillis(System.currentTimeMillis());
            XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            ad.setModifiedDate(xmlCal);

            //Auth detail page
            ad.setScheme("" + session.getAttribute("scheme"));
            ad.setSchemeOption("" + session.getAttribute("schemeOption"));
            ad.setAuthType("" + session.getAttribute("authType"));

            /*
            String dental = "" + session.getAttribute("dental");

            if (authType.equalsIgnoreCase("1")) {
                ad.setAuthType("Optical");
            } else if (authType.equalsIgnoreCase("2")) {
                if (dental.equalsIgnoreCase("3")) {
                    ad.setAuthType("Orthodontic");
                } else {
                    ad.setAuthType("Dental");
                }
            }*/

            ad.setAuthorisationDate("" + session.getAttribute("authDate"));
            ad.setMemberNo("" + session.getAttribute("memNo_text"));
            String memberInfo = "" + session.getAttribute("memNo");
            String[] tokens = memberInfo.split("\\s");
            int num = tokens.length;
            String surname = "";
            ad.setFullnames(tokens[0]);
            for (int i = 1; i < num - 1; i++)
            {
                if (surname.equalsIgnoreCase(""))
                {
                    surname = tokens[i];
                }
                else
                {
                    surname = surname + " "  + tokens[i];
                }
            }
            ad.setSurname(surname);
            ad.setDependantCode(tokens[num - 1]);
            /*
            int pos = memberName.indexOf(" ");
            if (pos > 0) {
                
                String name = memberName.substring(0, pos);
                String surname = memberName.substring(pos+1, memberName.length());
                ad.setFullnames(name);
                ad.setSurname(surname);
            }*/
            ad.setCallerName("" + session.getAttribute("callerName"));
            ad.setCallerRelationship("" + session.getAttribute("callerRelationship"));
            ad.setCallerReason("" + session.getAttribute("callerReason"));
            ad.setCallerContact("" + session.getAttribute("callerContact"));

            //Dental detail page

            ad.setTreatingProviderNo(this.getValue(session.getAttribute("treatingProv_text")));
            ad.setTreatingProvider(this.getValue(session.getAttribute("treatingProv")));
            ad.setReferringProviderNo(this.getValue(session.getAttribute("referringProv_text")));
            ad.setReferringProvider(this.getValue(session.getAttribute("referringProv")));
            ad.setLabProviderNo(this.getValue(session.getAttribute("labProv_text")));
            ad.setLabProvider(this.getValue(session.getAttribute("labProv")));
            String icd = (String) session.getAttribute("picd10");
            int pos = icd.indexOf(" ");
            if (pos > 0) {
                String icdCode = icd.substring(0, pos);
                ad.setPrimaryIcd(icdCode);
            } else
            {
               ad.setPrimaryIcd(icd);
            }
           
            ArrayList<String> list = (ArrayList<String>) session.getAttribute("tariff_list");
            String tariff = "";
            if(list != null){
                for (int i = 0; i < list.size(); i++) {
                    String val = list.get(i);
                    if(i > 0)
                        tariff += ",";
                    tariff += val.substring(0, val.indexOf(" "));
                }
            }
            ad.setTarrifList(tariff);

            list = (ArrayList<String>) session.getAttribute("labCode_list");
            String labCodes = "";
            if(list != null){
                for (int i = 0; i < list.size(); i++) {
                    String val = list.get(i);
                    if(i > 0)
                        labCodes += ",";
                    labCodes += val.substring(0, val.indexOf(" "));
                }
            }
            ad.setLabCodeList(labCodes);

            ad.setQuantity(""+session.getAttribute("quantity"));
            ad.setToothNumber(""+session.getAttribute("toothNumber"));
            ad.setEstimatedCost(""+session.getAttribute("amountClaimed"));
            ad.setDateValidFrom(""+session.getAttribute("validFrom"));
            ad.setDateValidTo(""+session.getAttribute("validTo"));
            ad.setNotes("" + session.getAttribute("notes"));

            String checked = (String) session.getAttribute("hospAdmission");
            if (checked != null && !checked.equalsIgnoreCase(""))
                ad.setHospitalIndicator(checked);
            else
                ad.setHospitalIndicator("0");
            ad.setHospitalProviderNo(this.getValue(session.getAttribute("facilityProv_text")));
            ad.setHospitalProvider(this.getValue(session.getAttribute("facilityProv")));
            ad.setAnaesthetistCode(this.getValue(session.getAttribute("aneasthetist_text")));
            ad.setAnaesthetistName(this.getValue(session.getAttribute("aneasthetist")));

            list = (ArrayList<String>) session.getAttribute("sicd10_list");
            String secIcd = "";
            if(list != null){
                for (int i = 0; i < list.size(); i++) {
                    String val = list.get(i);
                    if(i > 0)
                        secIcd += ",";
                    secIcd += val.substring(0, val.indexOf(" "));
                }
            }
            ad.setSecondaryIcd(secIcd);

            list = (ArrayList<String>) session.getAttribute("comicd10_list");
            String coIcd = "";
            if(list != null){
                for (int i = 0; i < list.size(); i++) {
                    String val = list.get(i);
                    if(i > 0)
                        coIcd += ",";
                    coIcd += val.substring(0, val.indexOf(" "));
                }
            }
            ad.setTertiaryIcd(coIcd);

            ad.setPMB("" + session.getAttribute("pmb"));
            ad.setCoPayment("" + session.getAttribute("coPay"));
            ad.setTheatreTime("" + session.getAttribute("theatreTime"));
            ad.setAuthStatus("" + session.getAttribute("status"));
            ad.setFunder("" + session.getAttribute("funderDetails"));

            //Save auth object
            String authNumber = service.getAgileManagerPort().insertAuth(ad);

            //Create response page
            PrintWriter out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (authNumber != null && !authNumber.equalsIgnoreCase(""))
            {
                out.println("<td><label class=\"header\">Authorisation " + authNumber + " saved.</label></td>");
                this.clearAllFromSession(request);
            }
            else
            {
                out.println("<td><label class=\"header\">Authorisation saving failed.</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SaveAuthDetailsCommand";
    }

    private String getValue(Object obj)
    {
        String value = "";
        if (obj != null)
        {
            value = value + obj;
        }
        return value;
    }
}

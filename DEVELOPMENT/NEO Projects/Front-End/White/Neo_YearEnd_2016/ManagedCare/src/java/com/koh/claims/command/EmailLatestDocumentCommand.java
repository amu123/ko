/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import agility.za.indexdocumenttype.IndexDocumentResponse;
import agility.za.indexdocumenttype.IndexForMember;
import com.agile.security.webservice.AgileManager;
import com.agile.security.webservice.EmailAttachment;
import com.agile.security.webservice.EmailContent;
import com.koh.command.FECommand;
import static com.koh.command.FECommand.service;
import com.koh.command.NeoCommand;
import com.koh.document.command.LoadDocumentDropDownListCommand;
import com.koh.serv.PropertiesReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ContactDetails;
import neo.manager.CoverDetails;
import neo.manager.CoverProductDetails;
import neo.manager.EmployerGroup;
import neo.manager.EntityTimeDetails;
import neo.manager.MemberEmployerBrokerInfo;
import neo.manager.NeoManagerBean;
import neo.manager.PersonDetails;
import org.apache.log4j.Logger;

/**
 * Used in Customer Care's Membership Certificate || Membership's Overage-Dependant-Letter
 *
 * @author gerritr
 */
public class EmailLatestDocumentCommand extends FECommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        logger.info("Inside EmailLatestDocumentCommand");

        boolean emailSent = false;
        if (null != session.getAttribute("docType")) {
            String docType = String.valueOf(session.getAttribute("docType"));
            if (docType.equalsIgnoreCase("ODL")) {
                //<editor-fold defaultstate="collapsed" desc="Overage Dependant Letter">
                if (null != session.getAttribute("indexResponse")) {
                    PrintWriter out = null;
                    try {
                        IndexDocumentResponse resp = (IndexDocumentResponse) session.getAttribute("indexResponse");
                        List<IndexForMember> list = resp.getIndexForMember();
                        String indexId = request.getParameter("btnIndex");
                        String depEntityId = "";
                        int entityId = 0;
                        EmailContent content = new EmailContent();
                        for (IndexForMember indexForMember : list) {
                            if (indexForMember.getIndexId() == Long.parseLong(indexId)) {
                                String currentMemberCertLoc = new PropertiesReader().getProperty("DocumentIndexIndexDrive") + indexForMember.getLocation();
                                File file = new File(currentMemberCertLoc);
                                if (!file.exists()) {
                                    out = response.getWriter();
                                    out.println("PDF Not Found");
                                    return false;
                                }
                                depEntityId = indexForMember.getLocation().substring(indexForMember.getLocation().indexOf("_") + 1, indexForMember.getLocation().lastIndexOf("_"));
                                ArrayList<CoverDetails> cdList = (ArrayList<CoverDetails>) port.getCoverDetailsByCoverNumber(String.valueOf(session.getAttribute("coverNumber")));
                                for (CoverDetails cd : cdList) {
                                    if (cd.getDependentTypeId() == 17) {
                                        content.setMemberTitle(cd.getTitle());
                                        content.setMemberInitials(cd.getInitials());
                                        content.setMemberName(cd.getName());
                                        content.setMemberSurname(cd.getSurname());
                                        content.setMemberCoverNumber(cd.getCoverNumber());
                                        content.setMemberNumber(cd.getCoverNumber());
                                        entityId = cd.getEntityId();
                                        ArrayList<ContactDetails> arrContD = (ArrayList<ContactDetails>) port.getAllContactDetails(cd.getEntityId());
                                        for (ContactDetails contactDetails : arrContD) {
                                            if ("E-mail".equals(contactDetails.getCommunicationMethod())) {
                                                content.setEmailAddressTo(contactDetails.getMethodDetails());
                                            }
                                        }
                                    }
                                }
                                for (CoverDetails cd : cdList) {
                                    if (cd.getEntityId() == Integer.parseInt(depEntityId)) {
                                        
                                        EntityTimeDetails eTime = port.getAge(cd.getDateOfBirth());

                                        //<editor-fold defaultstate="collapsed" desc="Mailing Prep">
                                        CoverProductDetails cpd = (CoverProductDetails) port.getProductDetailsDespiteCoverStatus(cd.getCoverNumber());
                                        content.setType("Overage_Dependant_Letter-" + eTime.getYears());
                                        content.setProductId(cpd.getProductId());
                                        content.setDepName(cd.getName());
                                        content.setDepSurname(cd.getSurname());
                                        content.setDepBDay(cd.getDateOfBirth());
                                        content.setOptionName(cpd.getOptionName());
                                        ArrayList<EmployerGroup> arrEG = (ArrayList<EmployerGroup>) port.getEmployerGroupByCoverNumber(cd.getCoverNumber());
                                        if (arrEG.size() >= 1) {
                                            content.setGroupCode(arrEG.get(0).getEmployerNumber());
                                        }

                                        //</editor-fold>
                                    }
                                }
                                AgileManager agilePort = service.getAgileManagerPort();
                                emailSent = agilePort.sendEmailWithAttachmentsViaLoader(content, indexForMember.getLocation());

                            }
                        }
                        out = response.getWriter();
                        if (emailSent == true) {
                            boolean result = port.mailCommunicationAudit(Integer.parseInt(String.valueOf(entityId)), content.getEmailAddressTo(), "Email sending successful", 16);
                            if (result == false) {
                                System.out.println("mail|OverAgeDependantNEOSideAudit was unable to insert Audit");
                            }
                            out.println("Email Sent");
                        } else {
                            boolean result = port.mailCommunicationAudit(Integer.parseInt(String.valueOf(entityId)), content.getEmailAddressTo(), "Email sending Failed", 16);
                            System.out.println("mail|OverAgeDependantNEOSideAudit was unable to insert Audit");
                            out.println("Email Failed");
                        }
                    } catch (IOException ex) {
                        java.util.logging.Logger.getLogger(EmailLatestDocumentCommand.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        out.close();
                    }
                }
                //</editor-fold>
            } else if (docType.equalsIgnoreCase("MCT")) {
                //<editor-fold defaultstate="collapsed" desc="Membership Certificate">
                List<IndexForMember> list = (List<IndexForMember>) session.getAttribute("indexList");
                String currentMemberCertLoc = "";
                if (list != null || list.size() > 0) {
                    InputStream in = null;

                    try {
                        request.setAttribute("indexList", list.get(0));
                        //--------------------MAILING Functionality-----------------//
                        String emailFrom = "";
                        String scheme = "Agility";
                        String emailTo = request.getParameter("emailAddress");
                        String btnIndex = request.getParameter("btnIndex");

                        int productId = new Integer(String.valueOf(session.getAttribute("productId")));

                        for (IndexForMember index : list) {
                            String sIndex = String.valueOf(index.getIndexId());
                            if (sIndex.equalsIgnoreCase(btnIndex)) {
                                currentMemberCertLoc = new PropertiesReader().getProperty("DocumentIndexIndexDrive") + index.getLocation();
                            }
                        }

                        //--------------------MAILING Functionality-----------------//
                        if (productId == 1) {
                            scheme = "Resolution Health";
                            emailFrom = "Noreply@agilityghs.co.za";
                        } else if (productId == 2) {
                            scheme = "Spectramed";
                            emailFrom = "Noreply@spectramed.co.za";
                        } else if (productId == 3){
                            scheme = "Sizwe";
                            emailFrom = "do-not-reply@sizwe.co.za";
                        }
                        emailSent = false;

                        System.out.println("DocumentIndexIndexDrive: " + currentMemberCertLoc);
                        //Creating a Byte out of the pdf
                        File file = new File(currentMemberCertLoc);
                        if (file.exists()) {
                            in = new FileInputStream(file);
                            int contentLength = (int) file.length();
                            logger.info("The content length is " + contentLength);
                            ByteArrayOutputStream temporaryOutput;
                            if (contentLength != -1) {
                                temporaryOutput = new ByteArrayOutputStream(contentLength);
                            } else {
                                temporaryOutput = new ByteArrayOutputStream(20480);
                            }
                            byte[] bufer = new byte[512];
                            while (true) {
                                int len = in.read(bufer);
                                if (len == -1) {
                                    break;
                                }
                                temporaryOutput.write(bufer, 0, len);
                            }
                            in.close();
                            temporaryOutput.close();
                            byte[] array = temporaryOutput.toByteArray();
                            //Creating a Byte out of the pdf
                            if (emailTo != null && !emailTo.equalsIgnoreCase("")) {
                                String subject = scheme;
                                if (productId == 1) {
                                    subject = "Your Resolution Health Membership Certificate";
                                } else if (productId == 2) {
                                    subject = "Your Spectramed Membership Certificate";
                                }
                                String emailMsg = null;
                                EmailContent content = new EmailContent();
                                content.setContentType("text/plain");
                                EmailAttachment attachment = new EmailAttachment();
                                attachment.setContentType("application/octet-stream");
                                attachment.setName("Member_Certificate.pdf");
                                content.setFirstAttachment(attachment);
                                content.setFirstAttachmentData(array);
                                content.setSubject(subject);
                                content.setEmailAddressFrom(emailFrom);
                                content.setEmailAddressTo(emailTo);
                                content.setProductId(productId);
                                //Determine msg for email according to type of document
                                content.setType("memberCert");
                                /*If document type is custom, this message added here will be displayed in the email. The email method will automatically add the 
                         the enquiries and call center aswell as the kind regards at the end of the message*/
                                content.setContent(emailMsg);
                                emailSent = service.getAgileManagerPort().sendEmailWithOrWithoutAttachment(content);
                                logger.info("Email sent : " + emailSent);
                                PrintWriter out = response.getWriter();
                                if (emailSent == true) {
                                    boolean result = port.mailCommunicationAudit(Integer.parseInt(String.valueOf(session.getAttribute("coverEntityId"))), emailTo, "Email sending successful", 14);
                                    if (result == false) {
                                        System.out.println("mail|MemberCertificateAudit was unable to insert Audit");
                                    }
                                    out.println("Email Sent");
                                } else {
                                    boolean result = port.mailCommunicationAudit(Integer.parseInt(String.valueOf(session.getAttribute("coverEntityId"))), emailTo, "Email sending Failed", 14);
                                    System.out.println("mail|MemberCertificateAudit was unable to insert Audit");
                                    out.println("Email Failed");
                                }
                            } else {
                                PrintWriter out = response.getWriter();
                                out.println("Invalid Email");
                            }
                        } else {
                            PrintWriter out = response.getWriter();
                            out.println("PDF Not Found");
                        }

                    } catch (FileNotFoundException ex) {
                        java.util.logging.Logger.getLogger(EmailLatestDocumentCommand.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        java.util.logging.Logger.getLogger(EmailLatestDocumentCommand.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        try {
                            if (in != null) {
                                in.close();
                            }
                        } catch (IOException ex) {
                            java.util.logging.Logger.getLogger(EmailLatestDocumentCommand.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    //--------------------MAILING Functionality-----------------//
                }
                //</editor-fold>
            }
        }

        return null;
    }

    @Override
    public String getName() {
        return "EmailLatestDocumentCommand";
    }
}

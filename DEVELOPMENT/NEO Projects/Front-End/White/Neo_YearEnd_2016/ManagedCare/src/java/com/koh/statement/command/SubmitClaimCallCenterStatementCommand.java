/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import com.agile.security.webservice.EmailAttachment;
import com.agile.security.webservice.EmailContent;
import com.koh.command.FECommand;
import static com.koh.command.FECommand.service;
import com.koh.serv.PropertiesReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class SubmitClaimCallCenterStatementCommand extends FECommand {

    private Logger logger = Logger.getLogger(SubmitClaimCallCenterStatementCommand.class);
    private static String errorReturnPage = "";

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        logger.info("Inside SubmitClaimCallCenterStatementCommand");

        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        SimpleDateFormat dTOs = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat dTOYear = new SimpleDateFormat("yyyy");
        SimpleDateFormat sTOd = new SimpleDateFormat("yyyy/MM/dd");

        String actionType = request.getParameter("opperationParam");
        String stmtType = "1";
        boolean isClearProviderClaim = (Boolean) session.getAttribute("persist_user_ccClearing");

        boolean memberDir = false;
        String directoryType = "";
        String memberNo = "";
        String providerNo = "";
        String fileName = "";
        String emailTo = request.getParameter("emailAddress");
        String productId = request.getParameter("productId");

        String scheme = "Agility";
        String emailFrom = "";

        String requestScreen = request.getParameter("onScreen");
        if (requestScreen.equalsIgnoreCase("payRunStatement")) {
            errorReturnPage = "/ManagedCare/Claims/PaymentRunStatements.jsp";
            stmtType = "" + session.getAttribute("payRunStmtType");
            if (stmtType.equals("1")) {
                emailTo = request.getParameter("emailAddress");
            } else {
                emailTo = request.getParameter("practiceEmailAddress");
            }

        } 
        
        errorReturnPage = "/ManagedCare/Claims/MemberStatements.jsp";

        //date manipulation
        String stmtDate = request.getParameter("stmtDatesCorrect");
        
        System.out.println(":: Date - " + stmtDate + " ::");
        
        String dateStr = "";
        String stmtYear = "";

        try {
            Date d = sTOd.parse(stmtDate);
            dateStr = dTOs.format(d);
            stmtYear = dTOYear.format(d);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        logger.info("The statement type is " + stmtType); 
        if (isClearProviderClaim) {
            directoryType = "emcprovider";
            providerNo = "" + session.getAttribute("practiceNumber");
            //fileName = "EMC_" + stmtYear + "_Statement_" + providerNo + ".pdf";
            fileName = "Statement_" + providerNo + ".pdf";

        } else {
            if (stmtType.trim().equals("1")) {
                directoryType = "Member";
                // memberNo = request.getParameter("memberNo_text");
                memberNo = "" + session.getAttribute("policyHolderNumber");
                fileName = "Statement_" + memberNo + ".pdf";
                memberDir = true;
            } else if (stmtType.trim().equals("2")) {
                directoryType = "Provider";
                providerNo = "" + session.getAttribute("practiceNumber");
                fileName = "Statement_" + providerNo + ".pdf";
            }
        }


        boolean emailSent = false;
        try {
            String path = new PropertiesReader().getProperty("statements");
            //System.out.println("statements directory: " + path);
            
            String schemeOption = String.valueOf(productId.trim().charAt(0));
            logger.info("The product ID is " + schemeOption);
            
            if (schemeOption != null && !schemeOption.equalsIgnoreCase("")) {
                if (schemeOption.equalsIgnoreCase("1")) {
                    path += "ResolutionHealth\\";
                    scheme = "Resolution Health";
                    emailFrom = "Noreply@agilityghs.co.za";
                } else if (schemeOption.equalsIgnoreCase("2")) {
                    path += "Spectramed\\";
                    scheme = "Spectramed";
                    emailFrom = "Statements@spectramed.co.za";
                } else if (schemeOption.equalsIgnoreCase("3")){
                    path += "Sizwe\\";
                    scheme = "Sizwe";
                    emailFrom = "do-not-reply@sizwe.co.za";
                }
            }

            String patchVar = "";
            if (memberDir) {
                patchVar = path + dateStr + "\\" + directoryType + "\\Statement_" + memberNo + ".pdf";
            } else {
                patchVar = path + dateStr + "\\" + directoryType + "\\Statement_" + providerNo + ".pdf";
            }
            logger.info("Path: " + patchVar);

            File file = new File(patchVar);

            InputStream in = new FileInputStream(file);
            int contentLength = (int) file.length();
            logger.info("The content length is " + contentLength);

            ByteArrayOutputStream temporaryOutput;
            if (contentLength != -1) {
                temporaryOutput = new ByteArrayOutputStream(contentLength);
            } else {
                temporaryOutput = new ByteArrayOutputStream(20480);
            }

            byte[] bufer = new byte[512];
            while (true) {
                int len = in.read(bufer);
                if (len == -1) {
                    break;
                }
                temporaryOutput.write(bufer, 0, len);
            }
            in.close();
            temporaryOutput.close();

            byte[] array = temporaryOutput.toByteArray();

            // test to see if the file is corrupt
            logger.info("actionType is " + actionType);

            if (actionType != null && actionType.equalsIgnoreCase("Preview")) {
                printPreview(request, response, array, fileName);

            } else if (emailTo != null && !emailTo.equalsIgnoreCase("")) {
                
                String subject = "";

                if (isClearProviderClaim) {
                    emailFrom = "doctors@emconline.co.za";
                    subject = "EMC";
                    subject += " - Statements For Provider " + providerNo;

                } else {
                    subject = scheme;
                    if (memberDir) {
                        subject += " - Statements For Member " + memberNo;
                    } else {
                        subject += " - Statements For Provider " + providerNo + " For Payment Process Date " + stmtDate;
                    }
                }

                String emailMsg = null;

                EmailContent content = new EmailContent();
                content.setContentType("text/plain");
                EmailAttachment attachment = new EmailAttachment();
                attachment.setContentType("application/octet-stream");
                attachment.setName(fileName);
                content.setFirstAttachment(attachment);
                content.setFirstAttachmentData(array);
                content.setSubject(subject);
                content.setEmailAddressFrom(emailFrom);
                content.setEmailAddressTo(emailTo);
                content.setProductId(new Integer(schemeOption));
                //Determine msg for email according to type of document ('claim', 'statement', 'preAuth', 'benefit' 'custom', 'other')
                content.setType("claim");
                /*If document type is custom, this message added here will be displayed in the email. The email method will automatically add the 
                 the enquiries and call center aswell as the kind regards at the end of the message*/
                content.setContent(emailMsg);
                emailSent = service.getAgileManagerPort().sendEmailWithOrWithoutAttachment(content);
                logger.info("Email sent : " + emailSent);
                printEmailResult(request, response, emailSent);
            } else {
                printEmailResult(request, response, emailSent);
            }

        } catch (MalformedURLException ex) {
            logger.error(ex.getMessage());
        } catch (IOException ioex) {
            // FileNotFound exception
            printErrorResult(request, response);
            logger.error(ioex.getMessage());
        }
        return null;
    }

    @Override
    public String getName() {
        return "SubmitClaimCallCenterStatementCommand";
    }

    public void printPreview(HttpServletRequest request, HttpServletResponse response, byte[] array, String fileName) throws IOException {
        OutputStream outStream = null;
        try {
            response.setHeader("Pragma", "no-cache"); //HTTP 1.0
            response.setDateHeader("Expires", 0); //prevents caching at the proxy server
            response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-disposition", "attachment; filename=" + fileName);

            response.setContentType("application/pdf");
            response.setContentLength(array.length);
            outStream = response.getOutputStream();
            outStream.write(array);
            outStream.flush();
            outStream.close();

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void printResult(HttpServletRequest request, HttpServletResponse response, boolean result) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (result) {
                out.println("<td><label class=\"header\">Statement emailed.</label></td>");
            } else {
                //out.println("<td><label class=\"header\">No payment(s) found.</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    public void printEmailResult(HttpServletRequest request, HttpServletResponse response, boolean result) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (result) {
                out.println("<td><label class=\"header\">Statement emailed.</label></td>");
            } else {
                out.println("<td><label class=\"header\">Email sending failed, Either email address is missing or incorrect.</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "2; URL=" + errorReturnPage + "");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    public void printErrorResult(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");

            out.println("<td><label class=\"header\">No records found.</label></td>");

            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "2; URL=" + errorReturnPage + "");

        } catch (IOException ex) {
            //Logger.getLogger(SaveAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }
}

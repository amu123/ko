/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.tags;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthCoverExclusions;
import neo.manager.CoverDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class CoverDependentWaitingPeriod extends TagSupport {

    private String commandName;
    private String javaScript;
    private String sessionAttribute;
    private String onScreen;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

        try {
            List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute(sessionAttribute);

            if (cdList != null && cdList.isEmpty() == false) {
                //get dependant 
                int depCode = -1;
                if (session.getAttribute("depListValues") != null) {
                    String depCodeStr = "" + session.getAttribute("depListValues");
                    System.out.println("Member search grid depCodeStr = " + depCodeStr);
                    if (depCodeStr != null && !depCodeStr.equalsIgnoreCase("") && !depCodeStr.equalsIgnoreCase("null")) {
                        depCode = Integer.parseInt(depCodeStr);
                    }
                }

                out.println("<table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.println("<tr>"
                        + "<th align=\"left\">Dependant Code</th>"
                        + "<th align=\"left\">Dependant Name</th>"
                        + "<th align=\"left\">Dependant Surname</th>"
                        + "<th align=\"left\">Birth Date</th>"
                        + "<th align=\"left\">Start Date</th>"
                        + "<th align=\"left\">End Date</th>"
                        + "<th align=\"left\">PMB</th>"
                        + "<th align=\"left\"></th>"
                        + "<th align=\"left\"></th>"
                        + "</tr>");

                int counter = 0;
                List<LookupValue> values = port.getCodeTable(67);
                LookupValue lookupValue = new neo.manager.LookupValue();
                for (CoverDetails cd : cdList) {
                    //set cover details 
                    String dateOfBirth = "";
                    String wStartDate = "";
                    String wEndDate = "";
                    String wPMB = "";
                    boolean generalPeriod = false;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

                    //dob
                    if (cd.getDateOfBirth() != null) {
                        dateOfBirth = dateFormat.format(cd.getDateOfBirth().toGregorianCalendar().getTime());
                    }

                    List<AuthCoverExclusions> underwriting = port.getActiveCoverExclusionsByCoverDependant(cd.getCoverNumber(), cd.getDependentNumber() + "");
                    for (AuthCoverExclusions ce : underwriting) {
                        if (ce.getExclusionType().equals("4")) {
                            generalPeriod = true;
                        }
                    }
                    if (underwriting != null && underwriting.size() > 0 && generalPeriod) {
                        for (AuthCoverExclusions ce : underwriting) {
                            if (ce.getExclusionType().equals("4")) {
                                if (ce.getExclusionDateFrom() != null) {
                                    wStartDate = dateFormat.format(ce.getExclusionDateFrom().toGregorianCalendar().getTime());
                                }

                                if (ce.getExclusionDateFrom() != null) {
                                    wEndDate = dateFormat.format(ce.getExclusionDateTo().toGregorianCalendar().getTime());
                                }

                                wPMB = ce.getPmbAllowed();
                                out.println("<tr>"
                                        + "<td><label class=\"label\">" + cd.getDependentNumber() + "</label></td>"
                                        + "<td><label class=\"label\">" + cd.getName() + "</label></td>"
                                        + "<td><label class=\"label\">" + cd.getSurname() + "</label></td>"
                                        + "<td><label class=\"label\">" + dateOfBirth + "</label></td>"
                                        + "<td><label class=\"label\">" + wStartDate + "</label></td>"
                                        + "<td><label class=\"label\">" + wEndDate + "</label></td>"
                                        //+ "<td><label class=\"label\">" + wPMB + "</label></td>");
                                        + "<td><select style=\"width:100px\" size=\"1\" name=\"depSelectWP" + counter + "\" id=\"depSelectWP" + counter + "\" value=\"" + cd.getPenaltyLateJoin() + "\" onChange=\"$('#buttonUP" + counter + "').removeAttr('disabled');\";>");

                                for (LookupValue val : values) {
                                    if (ce.getPmbAllowed().equalsIgnoreCase(val.getValue())) {
                                        out.println("<option selected=\"selected\" value=\"" + val.getId() + "\" >" + val.getValue() + "</option>");
                                    } else {
                                        out.println("<option value=\"" + val.getId() + "\" >" + val.getValue() + "</option>");
                                    }
                                }
                                out.println("</select> </td>");

                                if (javaScript != null && !javaScript.equalsIgnoreCase("")) {
                                    if (javaScript.equalsIgnoreCase("memberDepSelect")) {
                                        out.println("<td><input type=\"button\" name=\"buttonDel" + counter + "\" id=\"buttonDel" + counter + "\" value=\"Delete\" onclick=\"document.getElementById('buttonPressedUnderwriting').value = 'DeleteGeneral';document.getElementById('writingEntityId').value = '"
                                                + cd.getEntityId() + "';document.getElementById('exclutionType').value = '" + ce.getExclusionType() + "';document.getElementById('exclutionId').value = '" + ce.getExclusionDetailId() + "';$('#buttonDel" + counter + "').attr('disabled',true);submitFormWithAjaxPostAdd(this.form, 'MemberUnderwriting')\"></td>");
                                    } else {
                                        out.println("<td><button name=\"opperation\" type=\"button\" " + javaScript + " >Delete</button></td>");
                                    }
                                }

                                if (javaScript != null && !javaScript.equalsIgnoreCase("")) {
                                    if (javaScript.equalsIgnoreCase("memberDepSelect")) {
                                        out.println("<td><input type=\"button\" name=\"buttonUP" + counter + "\" id=\"buttonUP" + counter + "\" value=\"Update\" disabled onclick=\"document.getElementById('buttonPressedUnderwriting').value = 'UpdateGeneral';document.getElementById('writingEntityId').value = '"
                                                + cd.getEntityId() + "';document.getElementById('exclutionId').value = '" + ce.getExclusionDetailId() + "';var node = document.getElementById('depSelectWP" + counter + "');document.getElementById('writingPMB').value = node.options[node.selectedIndex].value;submitFormWithAjaxPostAdd(this.form, 'MemberUnderwriting')\"></td>");
                                    } else {
                                        out.println("<td><button name=\"opperation\" type=\"button\" " + javaScript + " >Update</button></td>");
                                    }
                                }

                                out.println("</tr>");
                            }
                        }


                    } else {
                        out.println("<tr>"
                                + "<td><label class=\"label\">" + cd.getDependentNumber() + "</label></td>"
                                + "<td><label class=\"label\">" + cd.getName() + "</label></td>"
                                + "<td><label class=\"label\">" + cd.getSurname() + "</label></td>"
                                + "<td><label class=\"label\">" + dateOfBirth + "</label></td>"
                                + "<td><label class=\"label\">" + wStartDate + "</label></td>"
                                + "<td><label class=\"label\">" + wEndDate + "</label></td>"
                                //+ "<td><label class=\"label\">" + wPMB + "</label></td>");
                                + "<td><select style=\"width:100px\" size=\"1\" name=\"depSelectWP" + counter + "\" id=\"depSelectWP" + counter + "\" value=\"" + cd.getPenaltyLateJoin() + "\" onChange=\"$('#buttonUP" + counter + "').removeAttr('disabled');\";>");

                        for (LookupValue val : values) {
                            System.out.println("");
                            if (cd.getPenaltyLateJoin().equalsIgnoreCase(val.getValue())) {
                                out.println("<option selected=\"selected\" value=\"" + val.getId() + "\" >" + val.getValue() + "</option>");
                            } else {
                                out.println("<option value=\"" + val.getId() + "\" >" + val.getValue() + "</option>");
                            }
                        }
                        
                        if (javaScript != null && !javaScript.equalsIgnoreCase("")) {
                            if (javaScript.equalsIgnoreCase("memberDepSelect")) {
                                out.println("<td><input type=\"button\" name=\"buttonDel" + counter + "\" id=\"buttonDel" + counter + "\" value=\"Delete\" disabled onclick=\"document.getElementById('buttonPressedUnderwriting').value = 'Update';document.getElementById('depCoverId').value = '"
                                        + cd.getCoverDetailsId() + "';var node = document.getElementById('depSelectLJP" + counter + "');document.getElementById('depLJP').value = node.options[node.selectedIndex].value; submitFormWithAjaxPostAdd(this.form, 'MemberUnderwriting')\"></td>");
                            } else {
                                out.println("<td><button name=\"opperation\" type=\"button\" " + javaScript + " >Delete</button></td>");
                            }
                        }
                        
                        if (javaScript != null && !javaScript.equalsIgnoreCase("")) {
                            if (javaScript.equalsIgnoreCase("memberDepSelect")) {
                                out.println("<td><input type=\"button\" name=\"buttonUP" + counter + "\" id=\"buttonUP" + counter + "\" value=\"Add\" onclick=\"document.getElementById('buttonPressedUnderwriting').value = 'AddGeneral';document.getElementById('writingEntityId').value = '"
                                                + cd.getEntityId() + "';var node = document.getElementById('depSelectWP" + counter + "');document.getElementById('writingPMB').value = node.options[node.selectedIndex].value;submitFormWithAjaxPostAdd(this.form, 'MemberUnderwriting')\"></td>");
                            } else {
                                out.println("<td><button name=\"opperation\" type=\"button\" " + javaScript + " >Update</button></td>");
                            }
                        }
                    }
                    out.println("</tr>");
                    counter++;
                }
                out.println("</table>");

            }
        } catch (java.io.IOException ex) {
            throw new JspException("Error in CoverDependentPenaltyJoin tag", ex);
        }

        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setOnScreen(String onScreen) {
        this.onScreen = onScreen;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;

import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import agility.za.indexdocumenttype.MergeDocType;
import agility.za.indexdocumenttype.SplitDocType;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class DocumentIndexDoMergeCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------In DocumentIndexDoMergeCommand---------------------");
//            String selectedFile = 
            String[] selectedFiles = request.getParameterValues("selectedFile");

            String workDrive = new PropertiesReader().getProperty("DocumentIndexWorkDrive");
            String profileType = (String)request.getSession().getAttribute("profile")!=null?(String)request.getSession().getAttribute("profile"):"";
            String folderList = (String) request.getParameter("folderList");
            String listOrWork = (String) request.getParameter("listOrWork");
            request.setAttribute("folderList", folderList);
            request.setAttribute("listOrWork", listOrWork);
            if (selectedFiles == null || selectedFiles.length < 2) {
                errorMessage(request, response, "Select atleast 2 files to merge");
                return "DocumentIndexDoSplitCommand";
            }

            IndexDocumentRequest req = new IndexDocumentRequest();
            req.setIndexType("Merge");
            req.setSrcDrive(workDrive);
            NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
            req.setAgent(loggedInUser);
            req.setEntityNumber("n/a");
            MergeDocType mergeDocType = new MergeDocType();
            mergeDocType.setFolder(folderList+"/"+profileType);
            for (String filename : selectedFiles) {
                mergeDocType.getFilenames().add(filename);
            }
            req.setMerge(mergeDocType);
            System.out.println("Before Webservice");
            IndexDocumentResponse res = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
            if(!res.isIsSuccess()){
                errorMessage(request, response, res.getMessage());
                return "DocumentIndexDoMergeCommand";                              
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("/Indexing/MergeDocument.jsp");
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "DocumentIndexDoMergeCommand";
    }

    @Override
    public String getName() {
        return "DocumentIndexDoMergeCommand";
    }

    private void errorMessage(HttpServletRequest request, HttpServletResponse response, String msg) throws Exception {
        request.setAttribute("errorMsg", msg);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/Indexing/MergeDocument.jsp");
        dispatcher.forward(request, response);
    }
}
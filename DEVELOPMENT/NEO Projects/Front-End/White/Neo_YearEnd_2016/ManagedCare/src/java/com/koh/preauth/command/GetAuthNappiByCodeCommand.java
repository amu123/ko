/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthTariffDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class GetAuthNappiByCodeCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        PrintWriter out = null;
        AuthTariffDetails nap = new AuthTariffDetails();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        //get values from request
        String napCode = request.getParameter("napCode");
        try {
            out = response.getWriter();
            if (napCode != null && !napCode.trim().equalsIgnoreCase("")) {
                XMLGregorianCalendar authDate = null;
                String aDate = "" + session.getAttribute("authDate");
                boolean authDateValid = true;
                if (aDate.equalsIgnoreCase("null") || aDate.equalsIgnoreCase("")) {
                    authDateValid = false;

                } else {
                    try {
                        authDate = DateTimeUtils.convertDateToXMLGregorianCalendar(sdf.parse(aDate));
                    } catch (ParseException ex) {
                        ex.printStackTrace();
                        authDateValid = false;
                    }

                    if (authDateValid == true) {
                        //method
                        nap = port.getAuthNappiForCode(napCode, authDate);

                        if (nap != null) {

                            session.setAttribute("napDesc", nap.getTariffDesc());
                            session.setAttribute("napCode", nap.getTariffCode());

                            out.print("Done|" + nap.getTariffCode() + "|" + nap.getTariffDesc());

                        }
                    }else{
                            out.print("Error|Invalid Auth Date "+aDate);
                    }
                }
            }

        } catch (Exception e) {
            System.out.println("Exception occurd in GetAuthTariffByCodeCommand");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetAuthNappiByCodeCommand";
    }
}

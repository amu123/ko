/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.command;

import java.net.URL;
import javax.xml.namespace.QName;
import neo.manager.NeoManagerBeanService;

/**
 *
 * @author whauger
 * Base command for all Neo related processing
 */
public abstract class NeoCommand extends Command {
    //Port to neo web service, can be used by all commands extending from this command
    public static NeoManagerBeanService service = null;

    static{
        getWebService();
    }

    /**
     * Build the static port
     */
    private static void getWebService() {
        if (service == null)
        {
            try {
                String WS_URL_TriggerSoapHttpPort = "http://localhost:9494/Neo_ear-Neo_Manager/NeoManagerBean?wsdl";
                String WS_NAMESPACE_TriggerSoapHttpPort = "http://manager.neo/";
                String WS_SERVICE_TriggerSoapHttpPort = "NeoManagerBeanService";
                URL wsUrl = new URL(WS_URL_TriggerSoapHttpPort);
                QName wsQName = new QName(WS_NAMESPACE_TriggerSoapHttpPort, WS_SERVICE_TriggerSoapHttpPort);
                NeoManagerBeanService services = new NeoManagerBeanService(wsUrl, wsQName);
                service = services;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
  
}

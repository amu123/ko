/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker_firm.application.commands;

import com.koh.command.NeoCommand;
import com.koh.utils.MapUtils;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class BrokerFirmSearchCommand extends NeoCommand{
    
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        try {
            String brokerFirmCode = request.getParameter("brokerFirmCode");
            String brokerFirmName = request.getParameter("brokerFirmName");
            
            List<KeyValueArray> kva = port.getBrokerFirmList(brokerFirmCode, brokerFirmName);
            
            Object col = MapUtils.getMap(kva);                        

            request.setAttribute("BrokerFirmResults", col);
            context.getRequestDispatcher("/Broker/BrokerFirmSearchResults.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(BrokerFirmSearchCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "BrokerFirmSearchCommand";
    }


}


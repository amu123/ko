/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthCPTDetails;
import neo.manager.AuthTariffDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class ReturnAuthCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        boolean mapped = false;

        //get if cpt code must be mapped for tariff code(s)
        boolean check = false;
        if (session.getAttribute("mapCPTForTariff") != null) {
            check = (Boolean) session.getAttribute("mapCPTForTariff"); //authTariffButton
        }
        String searchCalled = "" + session.getAttribute("searchCalled");
        List<AuthTariffDetails> tList = (List<AuthTariffDetails>) session.getAttribute("tariffListArray");
        List<AuthCPTDetails> cptList = new ArrayList<AuthCPTDetails>();
        if (check) {
            mapped = true;
            if (searchCalled != null && !searchCalled.equalsIgnoreCase("") && (searchCalled.equalsIgnoreCase("authTariffButton") || searchCalled.equalsIgnoreCase("tariffCode"))) {
                if (tList != null && !tList.isEmpty()) {
                    String result = (String) new GetAuthCPTMappingForTariff().execute(request, response, context);
                    System.out.println("result = " + result);
                    String[] resultArr = result.split("\\|");
                    String completed = resultArr[0];
                    System.out.println("resultArr[1] = " + resultArr[1]);
                    mapped = Boolean.valueOf(resultArr[1]);
                    System.out.println("mapped = " + mapped);

                    if (completed.equalsIgnoreCase("done") && mapped) {
                        //set co payment for cpt
                        double copay = 0.0d;
                        cptList = (List<AuthCPTDetails>) session.getAttribute("authCPTListDetails");

                        String admissionD = "" + session.getAttribute("admissionDate");
                        Date admissionDate = null;
                        XMLGregorianCalendar authStart = null;
                        try {
                            if (admissionD != null && !admissionD.equalsIgnoreCase("null")
                                    && !admissionD.equalsIgnoreCase("")) {
                                admissionDate = new SimpleDateFormat("yyyy/MM/dd").parse(admissionD);
                                authStart = DateTimeUtils.convertDateToXMLGregorianCalendar(admissionDate);
                            }
                        } catch (ParseException ex) {
                            ex.printStackTrace();
                        }
                        //get auth member option id
                        String optionId = "" + session.getAttribute("schemeOption");

                        copay = CalculateCoPaymentAmounts.setCopayment(session);
                        System.out.println("co payment amount = " + copay);
                        session.setAttribute("coPay", copay);
                    }
                } else {
                    session.setAttribute("authCPTListDetails", null);
                }
            }
        } else {
            mapped = true;
            if (tList != null && !tList.isEmpty()) {
                session.setAttribute("authCPTListDetails", cptList);

            }
        }

        try {
            String nextJSP = "";
            if (mapped) {
                nextJSP = "" + session.getAttribute("onScreen");
            } else {
                nextJSP = "/PreAuth/AuthTariffCPTSelection.jsp";
            }

            System.out.println("ReturnAuthCommand onscreen = " + nextJSP);
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ReturnAuthCommand";
    }
}

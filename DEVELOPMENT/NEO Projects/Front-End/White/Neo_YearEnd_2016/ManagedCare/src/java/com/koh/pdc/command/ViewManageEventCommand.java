/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import neo.manager.LookupValue;
import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ContactDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class ViewManageEventCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        System.out.println("entered ViewManageEventCommand");

        Integer entity = (Integer) session.getAttribute("entityId");
        NeoManagerBean port = service.getNeoManagerBeanPort();

//        List<LookupValue> lookUps = port.getCodeTable(129);
        int entityId = entity;

        ArrayList<ContactDetails> contact = (ArrayList<ContactDetails>) port.getAllContactDetails(entityId);

        for (ContactDetails cd : contact) {
            if (cd.getCommunicationMethod().equals("E-mail")) {
                session.setAttribute("email", cd.getMethodDetails());
            } else if (cd.getCommunicationMethod().equals("Fax")) {
                session.setAttribute("faxNumber", cd.getMethodDetails());
            } else if (cd.getCommunicationMethod().equals("Cell")) {
                session.setAttribute("cellNumber", cd.getMethodDetails());
            } else if (cd.getCommunicationMethod().equals("Tel (work)")) {
                session.setAttribute("workNumber", cd.getMethodDetails());
            } else if (cd.getCommunicationMethod().equals("Tel (home)")) {
                session.setAttribute("homeNumber", cd.getMethodDetails());
            }
        }

        try {
            String nextJSP = "/PDC/ManageEvent.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    @Override
    public String getName() {
        return "ViewManageEventCommand";
    }
}

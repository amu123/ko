/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.LookupUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.LookupValue;
import neo.manager.NeoUser;
import neo.manager.PreAuthSearchCriteria;
import neo.manager.PreAuthSearchResults;

/**
 *
 * @author johanl
 */
public class SearchPreAuthCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        //print user info
        System.out.println("PreAuth Search performed by : userId = "+user.getUserId());

        String authNumber = request.getParameter("authNo");
        String authType = request.getParameter("authType");
        String authDate = request.getParameter("authDate");
        String coverNumber = request.getParameter("memberNum_text");
        String depNo = request.getParameter("depListValues");
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String prod = request.getParameter("scheme");
        String opt = request.getParameter("schemeOption");
        
        //set auth values to search criteria
        PreAuthSearchCriteria pas = new PreAuthSearchCriteria();
        if(authNumber != null && !authNumber.trim().equalsIgnoreCase("")){
            pas.setAuthNumber(authNumber);
        }
        if(authType != null && !authType.trim().equalsIgnoreCase("99")){
            pas.setAuthType(authType);
        }
        if(authDate != null && !authDate.trim().equalsIgnoreCase("")){
            XMLGregorianCalendar aDate = null;
            try {
               aDate = DateTimeUtils.convertDateToXMLGregorianCalendar(sdf.parse(authDate));
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            if(aDate != null){
                pas.setAuthDate(aDate);
            }
        }
        if(coverNumber != null && !coverNumber.trim().equalsIgnoreCase("")){
            pas.setCoverNumber(coverNumber);
        }
        if(depNo != null && !depNo.trim().equalsIgnoreCase("99")){
            pas.setDependantCode(Integer.parseInt(depNo));
        }else{
            pas.setDependantCode(-1);
        }
        if(name != null && !name.trim().equalsIgnoreCase("")){
            pas.setFirstName(name);
        }
        if(surname != null && !surname.trim().equalsIgnoreCase("")){
            pas.setLastName(surname);
        }
        if(prod != null && !prod.trim().equalsIgnoreCase("99")){
            pas.setScheme(prod);
        }
        System.out.println("opt = "+opt);
        if(opt != null && !opt.trim().equalsIgnoreCase("99")){
            pas.setSchemeOption(opt);
            System.out.println("option selected = "+pas.getSchemeOption());
        }

        List<PreAuthSearchResults> paResult = service.getNeoManagerBeanPort().findPreAuthDetailsByCriteria(user, pas);

        if (paResult.size() == 0) {
            request.setAttribute("authSearchResultsMessage", "No results found.");
        } else if (paResult.size() >= 100) {
            request.setAttribute("authSearchResultsMessage", "Only first 100 results shown. Please refine search.");
        } else {
            request.setAttribute("authSearchResultsMessage", null);
        }        
        
        session.setAttribute("authSearchResults", paResult);

        try {
            String nextJSP = "/PreAuth/PreAuthSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "SearchPreAuthCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.update.command;

import com.koh.preauth.command.*;
import com.koh.command.Command;
import com.koh.command.NeoCommand;
import java.util.HashMap;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthTariffDetails;
import neo.manager.NeoManagerBean;

/**
//
 * @author gerritj
 */
public class SaveCallCenterPreAuthUpdate extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        String copaySes = ""+ session.getAttribute("coPay");
        
        this.saveScreenToSession(request);
        String authType = "" + session.getAttribute("authType");
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        if(copaySes != null && !copaySes.equalsIgnoreCase("null")
                && !copaySes.equalsIgnoreCase("")){
            session.setAttribute("coPay", copaySes);
        }
        
        try {
            String nextJSP = "";
            if (authType.trim().equalsIgnoreCase("1")) { //optical
                nextJSP = "/PreAuth/OpticalDetails_Update.jsp";

            } else if (authType.trim().equalsIgnoreCase("2") || authType.trim().equalsIgnoreCase("13")) { //dental
                nextJSP = "/PreAuth/DentalDetails_Update.jsp";

            }else if (authType.trim().equalsIgnoreCase("3")){
                nextJSP = "/PreAuth/OrthodonticDetails_Update.jsp";

            }else if (authType.trim().equalsIgnoreCase("4")){
                nextJSP = "/PreAuth/HospitalAuthDetails_Update.jsp";
            
            }else if (authType.trim().equalsIgnoreCase("5")){
//               String authNumber = (String) session.getAttribute("updateAuthNumber");
//               System.out.println("The Auth number is ******** " + authNumber);
//               if(authNumber != null && !authNumber.trim().isEmpty()){
//               List<AuthTariffDetails> medList =  port.findPreAuthMedicines(authNumber);
//                System.out.println("The nappi list size is " + medList.size());
//               session.setAttribute("AuthNappiTariffs", medList);
//               List<AuthTariffDetails> tarrifList = port.findPreAuthTarrifs(authNumber);
//               System.out.println("The tarriff list size is " + tarrifList.size());
//               session.setAttribute("AuthBasketTariffs", tarrifList);
//              // session.setAttribute("fromUpdate", "yes");
//               }
                nextJSP = "/PreAuth/ConditionSpecificAuth_Update.jsp";

            }else if (authType.trim().equalsIgnoreCase("6")){
                nextJSP = "/PreAuth/OrthodonticDetails_Update.jsp";
                
            }else if (authType.trim().equalsIgnoreCase("11")){
                nextJSP = "/PreAuth/HospiceDetails_Update.jsp";

            }else{
                nextJSP = "/PreAuth/GenericAuth_Update.jsp";
            }
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return null;
    }

    @Override
    public String getName() {
        return "SaveCallCenterPreAuthUpdate";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.statement.command;

import agility.za.documentprinting.DocumentPrintingRequest;
import agility.za.documentprinting.DocumentPrintingResponse;
import agility.za.documentprinting.PrintDocType;
import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.serv.PropertiesReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.ContactDetails;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class MemberDocumentGenerationCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------In MemberDocumentGenerationCommand---------------------");

            String entityId = request.getParameter("entityId");
            String docType = request.getParameter("docType");
            logger.info("entityId = " + entityId);
            logger.info("docType = " + docType);
             Map<String, String> fieldsMap = new HashMap<String, String>();
            if (entityId != null && !entityId.trim().equals("")) { 
                int i = new Integer(entityId);
                String client = (String) request.getSession().getAttribute("Client");
                
                if (client.equalsIgnoreCase("Sechaba")){
                    ContactDetails cd = new ContactDetails();
                    String smsTo = request.getParameter(cd.getCommunicationMethod());
                    String smsStatus = "Pending";
                    NeoCommand.service.getNeoManagerBeanPort().smsCommunicationAudit(i, smsTo, smsStatus, i);
                } 
                
                String filePath = NeoCommand.service.getNeoManagerBeanPort().processDocumentGeneration(i, docType, false);
               
                if(filePath !=null && !filePath.equals("")){
                   logger.info("Succesful Generation");
                   logger.info("File = " + filePath);
                   fieldsMap.put("DocGen_"+docType, filePath);
                }else { 
                    logger.error("Error generating .... " );
                    fieldsMap.put("DocGen_"+docType, filePath);
                }
            }
            String updateFields = TabUtils.convertMapToJSON(fieldsMap);
            request.setAttribute("entityId", entityId);
            String result = TabUtils.buildJsonResult(null, null, updateFields, null);
            PrintWriter out = response.getWriter();
            out.println(result);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "MemberDocumentGenerationCommand";
    }

    @Override
    public String getName() {
        return "MemberDocumentGenerationCommand";
    }
}
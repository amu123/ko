/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.Broker;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class MemberBrokerSearchCommand  extends NeoCommand{
 
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            NeoManagerBean port = service.getNeoManagerBeanPort();
            search(port, request, response, context);
        } catch (Exception ex) {
            Logger.getLogger(MemberApplicationSpecificQuestionsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void search(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        String code = request.getParameter("brokerCode");
        String name = request.getParameter("brokerName");
        List<Broker> firm = port.getAllBrokersbyNameAndCode(code, name);
        request.setAttribute("BrokerSearchResults", firm);
        context.getRequestDispatcher("/MemberApplication/BrokerMemberResults.jsp").forward(request, response);
    }
    
    @Override
    public String getName() {
        return "MemberBrokerSearchCommand";
    }
}
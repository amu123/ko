/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.command;

import com.koh.auth.dto.ProviderSearchResult;
import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.ProviderDetails;
import neo.manager.ProviderSearchCriteria;

/**
 *
 * @author gerritj
 */
public class SearchProviderCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        String providerDiscipline = request.getParameter("provType");
        String providerNumber = request.getParameter("provNo");
        String providerName = request.getParameter("provName");
        ProviderSearchCriteria search = new ProviderSearchCriteria();
        if (providerDiscipline != null && !providerDiscipline.equals(""))
            search.setDiciplineType(providerDiscipline);
        if (providerNumber != null && !providerNumber.equals(""))
            search.setProviderNumber(providerNumber);
        if (providerName != null && !providerName.equals(""))
            search.setSurname(providerName);
        search.setEntityTypeID(4);

        ArrayList<ProviderSearchResult> providers = new ArrayList<ProviderSearchResult>();
        List<ProviderDetails> pdList = new ArrayList<ProviderDetails>();
        try {
           pdList = service.getNeoManagerBeanPort().findAllProvidersByCriteria(search);

           for (ProviderDetails pd : pdList)
           {
             ProviderSearchResult provider = new ProviderSearchResult();
             provider.setProviderName(pd.getInitials() + " " + pd.getSurname());
             provider.setProviderNumber(pd.getProviderNumber());
             provider.setProviderType(pd.getDisciplineType().getValue());             
             providers.add(provider);
           }

           if (pdList.size() == 0) {
               request.setAttribute("searchProviderResultMessage", "No results found.");
           } else if (pdList.size() >= 100) {
               request.setAttribute("searchProviderResultMessage", "Only first 100 results shown. Please refine search.");
           } else {
               request.setAttribute("searchProviderResultMessage", null);
           }

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
        ProviderSearchResult p = new ProviderSearchResult();
        p.setProviderName("Lancet");
        p.setProviderNumber("111111");
        p.setProviderType("054");

        ProviderSearchResult p2 = new ProviderSearchResult();
        p2.setProviderName("Lancet");
        p2.setProviderNumber("111111");
        p2.setProviderType("054");

        providers.add(p);
        providers.add(p2);
        */
         request.setAttribute("searchProviderResult", providers);
        try {
            String nextJSP = "/Auth/ProviderSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
         return null;
    }

    @Override
    public String getName() {
        return "SearchProviderCommand";
    }
}

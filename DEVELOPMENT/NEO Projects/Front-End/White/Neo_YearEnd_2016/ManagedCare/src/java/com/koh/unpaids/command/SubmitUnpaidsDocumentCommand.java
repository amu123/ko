/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.unpaids.command;

import com.koh.command.NeoCommand;
import com.koh.dataload.command.LoadDataFileCommand;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.beanio.BeanReader;
import org.beanio.StreamFactory;
import neo.manager.AcceptedList;
import neo.manager.UnpaidsList;
import neo.manager.SpmUnpaidsList;

/**
 *
 * @author dewaldo
 */
public class SubmitUnpaidsDocumentCommand extends NeoCommand {

    org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass().getName());
    static String uploadedFileName = "";
    int Error1 = 0;
    int Error2 = 0;
    static List<UnpaidsList> unpaids = new ArrayList<UnpaidsList>();
    static List<UnpaidsList> warnings = new ArrayList<UnpaidsList>();
    static List<AcceptedList> accepted = new ArrayList<AcceptedList>();
    static List<AcceptedList> rejected = new ArrayList<AcceptedList>();
    static List<SpmUnpaidsList> normal = new ArrayList<SpmUnpaidsList>();
    static List<SpmUnpaidsList> vetReport = new ArrayList<SpmUnpaidsList>();

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        session.setAttribute("unpaids", null);
        session.setAttribute("warnings", null);
        session.setAttribute("accepted", null);
        session.setAttribute("rejected", null);
        session.setAttribute("typeAccepted", null);
        session.setAttribute("typeUnpaids", null);
        int Scheme = new Integer(request.getParameter("schemeOption"));
        Error1 = 0;
        Error2 = 0;

        File in = fileUpload(session, request, response, Scheme);

        if (in == null || !in.exists()) {
            System.out.println("Please choose an existing file!");
            request.setAttribute("Message", "<label class=\"error\">ERROR: Please make sure the uploaded file is a text file!</label>");
            dispatch(request, response, "Unpaids/UnpaidClaims.jsp");
        } else {
            //reset lists
            unpaids = new ArrayList<UnpaidsList>();
            warnings = new ArrayList<UnpaidsList>();
            accepted = new ArrayList<AcceptedList>();
            rejected = new ArrayList<AcceptedList>();
            normal = new ArrayList<SpmUnpaidsList>();
            vetReport = new ArrayList<SpmUnpaidsList>();

            switch (Scheme) {
                case 1:
                    session.setAttribute("schemeOption", "1");
                    session.setAttribute("typeAccepted", "");
                    session.setAttribute("typeUnpaids", "");
                    session.setAttribute("typeNormal", "");
                    session.setAttribute("typeVetReport", "");
                    session.setAttribute("schemeOption", Scheme);

                    //Rejected's
                    try {
                        readFileUsingBeanIo(request, response, "Accepted", in);
                    } catch (Exception e) {
                        Error1++;
                        logger.error("ERROR: " + e);
                    }   //Unpaids

                    try {
                        readFileUsingBeanIo(request, response, "Unpaids", in);
                    } catch (Exception e) {
                        Error1++;
                        logger.error("ERROR: " + e);
                    }   

                    //Perform check to see if both types of unpaids failed
                    if (Error1 > 1) {
                        //Do garbage collection to ensure file can be deleted
                        System.gc();
                        //Delete non-relevant file
                        in.delete();

                        System.out.println("Could not determine type of Reso unpaid!");
                        request.setAttribute("Message", "<label class=\"error\">ERROR: Could not determine type of Resolution Health unpaid!</label>");
                        dispatch(request, response, "Unpaids/UnpaidClaims.jsp");

                    } else if (Error2 > 0) {
                        request.setAttribute("Message", "<label class=\"warning\">WARNING: Could not find any Resolution Health claim unpaids in the uploaded file!</label>");
                        dispatch(request, response, "Unpaids/UnpaidClaims.jsp");
                    } else {
                        request.setAttribute("Message", "");
                        dispatch(request, response, "Unpaids/viewUnpaidsResult.jsp");
                    }

                    break;
                case 2:
                    session.setAttribute("typeAccepted", "");
                    session.setAttribute("typeUnpaids", "");
                    session.setAttribute("typeNormal", "");
                    session.setAttribute("typeVetReport", "");
                    session.setAttribute("schemeOption", Scheme);

                    //NORMALS
                    try {
                        readFileUsingBeanIo(request, response, "Normal", in);
                    } catch (Exception e) {
                        Error1++;
                        logger.error("Spectramed claims unpaid normal ERROR: " + e);
                    }

                    //VET REPORT
                    try {
                        readFileUsingBeanIo(request, response, "VetReport", in);
                    } catch (Exception e) {
                        Error1++;
                        logger.error("Spectramed claims unpaid VetReport ERROR: " + e);
                    }

                    //Perform check to see if both types failed  
                    if (Error1 > 1) {
                        //Do garbage collection to ensure file can be deleted
                        System.gc();
                        //Delete non-relevant file
                        in.delete();

                        System.out.println("Could not determine type of Spectramed unpaid!");
                        request.setAttribute("Message", "<label class=\"error\">ERROR: Could not determine type of Spectramed unpaid!</label>");
                        dispatch(request, response, "Unpaids/UnpaidClaims.jsp");

                    } else if (Error2 > 0) {
                        request.setAttribute("Message", "<label class=\"warning\">WARNING: Could not find any Spectramed claim unpaids in the uploaded file!</label>");
                        dispatch(request, response, "Unpaids/UnpaidClaims.jsp");
                    } else {
                        request.setAttribute("Message", "");
                        dispatch(request, response, "Unpaids/viewUnpaidsResult.jsp");
                    }
                    break;
                default:
                    System.out.println("Could not determine scheme!");
                    request.setAttribute("Message", "<label class=\"error\">ERROR: Could not determine scheme!</label>");
                    dispatch(request, response, "Unpaids/UnpaidClaims.jsp");
                    break;
            }
        }

        return null;
    }

    private static boolean getFileExtention(File file, int scheme) {
        String fileName = file.getName();
        boolean check = false;
        if (fileName.lastIndexOf(".") > 0) {
            String ext = fileName.substring(fileName.lastIndexOf(".") + 1);
            if (ext.equalsIgnoreCase("txt")) {
                check = true;
            } else {
                System.out.println("Uploaded file type is :" + ext);
                if (scheme == 1 || scheme == 2) {
                    uploadedFileName = fileName + ".txt";
                    check = true;
                }
            }
        }
        return check;
    }

    /**
     * *********************************************************************
     * Read the contents of the Reso unpaid document
     * *********************************************************************
     * @param request
     * @param response
     * @param type
     * @param file
     */
    @SuppressWarnings({"Convert2Diamond", "ConvertToStringSwitch", "UnusedAssignment"})
    public void readFileUsingBeanIo(HttpServletRequest request, HttpServletResponse response, String type, File file) {
        HttpSession session = request.getSession();
        StreamFactory factory = StreamFactory.newInstance();
        //reads from Source Packages <default package>
        factory.loadResource("unpaidsMapping.xml");

        if (file.exists()) {

            BeanReader reader = factory.createReader(type, file.getAbsoluteFile());
            Object record = null;

            while ((record = reader.read()) != null) {
                if (null != reader.getRecordName()) {
                    if (reader.getRecordName().equalsIgnoreCase("unpaid")) {
                        UnpaidsList unpaid = (UnpaidsList) record;

                        if ("01".equals(unpaid.getIdentifier())) {
                            unpaids.add(unpaid);
                            //System.out.println(unpaid.toString());
                        } else if ("03".equals(unpaid.getIdentifier())) {
                            warnings.add(unpaid);
                            //System.out.println(unpaid.toString());
                        }

                    } else if (reader.getRecordName().equalsIgnoreCase("accept")) {
                        AcceptedList accept = (AcceptedList) record;

                        if ("ACCEPTED".equals(accept.getStatus())) {
                            accepted.add(accept);
                            //System.out.println(accept.toString());
                        } else if ("REJECTED".equals(accept.getStatus())) {
                            rejected.add(accept);
                            //System.out.println(accept.toString());
                        }

                    } else if (reader.getRecordName().equals("normalUnpaid")) {
                        SpmUnpaidsList norm = (SpmUnpaidsList) record;

                        if (norm.getReferenceType().contains("CLRFND") && norm.getRecordType().equalsIgnoreCase("SD")) {
                            normal.add(norm);
                        }

                    } else if (reader.getRecordName().equals("vetUnpaid")) {
                        SpmUnpaidsList vet = (SpmUnpaidsList) record;

                        if (vet.getReferenceType().contains("CLRFND") && vet.getRecordType().equalsIgnoreCase("DT")) {
                            vetReport.add(vet);
                        }

                    }
                }
            }

            if (unpaids.isEmpty() && warnings.isEmpty() && accepted.isEmpty() && rejected.isEmpty() && normal.isEmpty() && vetReport.isEmpty()) {
                Error2++;
                logger.info("Could not find any valid records...");
            } else {
                //setValues
                session.setAttribute("unpaids", unpaids);
                session.setAttribute("warnings", warnings);
                session.setAttribute("accepted", accepted);
                session.setAttribute("rejected", rejected);
                session.setAttribute("Normal", normal);
                session.setAttribute("VetReport", vetReport);
                session.setAttribute("type" + type, type);
            }
        } else {
            logger.info("File does not exist!");
            request.setAttribute("Message", "<label class=\"error\">Error: Please choose an existing file!</label>");
            dispatch(request, response, "Unpaids/UnpaidClaims.jsp");
        }
    }

    @Override
    public String getName() {
        return "SubmitUnpaidsDocumentCommand";
    }

    private File fileUpload(HttpSession session, HttpServletRequest request, HttpServletResponse response, int scheme) {

        File uploadedFile = null;
        try {
            // Check that we have a file upload request
            boolean isMultipart = ServletFileUpload.isMultipartContent(request);

            System.out.println("isMultipart : " + isMultipart);
            String schemeName = "";
            if (scheme == 1) {
                schemeName = "ResolutionHealth";
            } else if (scheme == 2) {
                schemeName = "Spectramed";
            }

            if (isMultipart) {

                // Create a factory for disk-based file items
                // FileItemFactory factory = new DiskFileItemFactory();
                DiskFileItemFactory itemFactory = new DiskFileItemFactory();
                itemFactory.setRepository(uploadedFile);
                itemFactory.setSizeThreshold(2000);

                // Create a new file upload handler
                // ServletFileUpload upload = new ServletFileUpload(factory);
                ServletFileUpload upload = new ServletFileUpload(itemFactory);
                // Parse the request
                List fileItems = upload.parseRequest(request);

                Iterator iter = fileItems.iterator();
                while (iter.hasNext()) {

                    FileItem item = (FileItem) iter.next();

                    if (item.getName() != null && !item.getName().equalsIgnoreCase("")) {
                        String itemName = item.getName();
                        //String uploadedFileName = itemName.subSequence(itemName.lastIndexOf("/")+1, itemName.length);
                        uploadedFileName = FilenameUtils.getName(itemName);

                        boolean check = getFileExtention(new File(uploadedFileName), scheme);

                        if (check) {
                            logger.info("uploadedFileName = " + uploadedFileName);

                            File direcory = new File("C:/temp/Unpaids/Claims/" + schemeName + "/");
                            if (!direcory.exists()) {
                                logger.info("Directory not found. Creating....");
                                direcory.mkdirs();
                                logger.info("Done");
                            } else {
                                logger.info("Directory exist... not creating.");
                            }

                            uploadedFile = new File("C:/temp/Unpaids/Claims/" + schemeName + "/" + uploadedFileName);

                            logger.info("Content Type is : " + item.getContentType());

                            // uploadedFile = new File(destination, item.getName());
                            item.write(uploadedFile);
                            logger.info("Upload completed successfully !!!!!!!!!!");
                        } else {
                            uploadedFile = null;
                        }
                    }
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            System.out.println("Failed to Proccess File!");
            request.setAttribute("Message", "<label class=\"error\">ERROR: Failed to process File!!</label>");
            dispatch(request, response, "Unpaids/UnpaidClaims.jsp");
        }
        return uploadedFile;
    }

    public void dispatch(HttpServletRequest request, HttpServletResponse response, String path) {
        try {
            request.getRequestDispatcher(path).forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }
}

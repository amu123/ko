/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverPDCDetails;
import neo.manager.EntityTimeDetails;
import neo.manager.NeoManagerBean;
import neo.manager.PdcNotes;
import neo.manager.PersonDetails;

/**
 *
 * @author princes
 */
public class ViewPolicyDetailsCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        System.out.println("entered ViewPolicyDetailsCommand");
        session.setAttribute("searchView", "yes");
        Integer entityId = (Integer) session.getAttribute("entityId");
        String deptCode = (String) session.getAttribute("dependantNumber");
        String coverNumber = (String) session.getAttribute("policyNumber");

        System.out.println("entityId : " + entityId + " deptCode : " + deptCode + " coverNumber : " + coverNumber);
        PersonDetails person = service.getNeoManagerBeanPort().getPersonDetailsByEntityId(entityId);
        CoverPDCDetails list = port.getCoverProductDetailsByCoverNumberForPDC(coverNumber, new Integer(deptCode));

        // Testing
        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        String s = "";

        Date endDate = list.getEndDate().toGregorianCalendar().getTime();
        s = formatter.format(endDate);

        if (!s.equals("2999/12/31")) {
            System.out.println("Member is not active");
            session.setAttribute("activeMember", "Member is not active");
        } else {
            System.out.println("Member is active");
            session.setAttribute("activeMember", "");
        }

        System.out.println("list.getEndDate() " + s);

        String nextJSP = "/PDC/PolicyHolderDetailsMemberSearchView.jsp";
        if (list.getCoverNumber() != null) {

            String a = list.getPatientName().substring(1, list.getPatientName().length());
            String b = list.getPatientName().substring(0, 1);
            String c = b + a.toLowerCase();
            session.setAttribute("policyStatus", list.getCoverStatus());

            String d = list.getPatientSurname().substring(1, list.getPatientSurname().length());
            String e = list.getPatientSurname().substring(0, 1);
            String f = e + d.toLowerCase();
            session.setAttribute("patientNumber", list.getCoverNumber());
            session.setAttribute("patientName", c);
            session.setAttribute("patientGender", list.getGender().getValue());
            session.setAttribute("patientAge", "to be set");

            session.setAttribute("productName", list.getProductName());
            session.setAttribute("optionName", list.getOptionName());
            session.setAttribute("surname", f);
            session.setAttribute("entityId", list.getEntityId());
            EntityTimeDetails eTime = new EntityTimeDetails();
            eTime = port.getEntityAge(list.getDateOfBith());
            session.setAttribute("patientAge", eTime.getYears());
            Date update = list.getDateOfBith().toGregorianCalendar().getTime();
            s = formatter.format(update);
            session.setAttribute("birthDate", s);
            session.setAttribute("productId", list.getProductId());
            session.setAttribute("optionId", list.getOptionId());

            session.setAttribute("title", person.getTitle());
            session.setAttribute("initials", person.getInitials());
            session.setAttribute("idNumber", person.getIDNumber());
            session.setAttribute("matitalStatus", person.getMaritalStatus());
            session.setAttribute("homeLanguage", person.getHomeLanguage());
            session.setAttribute("employerName", person.getEmployer());
            session.setAttribute("incomeCategoty", person.getIncomeCategory());
            session.setAttribute("incomeCategoty", person.getJobTitle());

            //Remove for Address infor from session
            session.removeAttribute("PhysicalAddress");
            session.removeAttribute("PhysicalAddress1");
            session.removeAttribute("patientPhysicalAddress2");
            session.removeAttribute("PhysicalCity");
            session.removeAttribute("PostalAddress");
            session.removeAttribute("PostalAddress1");
            session.removeAttribute("PostalAddress2");
            session.removeAttribute("PhysicalCity");
            session.removeAttribute("email");
            session.removeAttribute("faxNumber");
            session.removeAttribute("cellNumber");
            session.removeAttribute("workNumber");
            session.removeAttribute("homeNumber");

            session.removeAttribute("dates");
            session.removeAttribute("dropDownDates");
            session.removeAttribute("sessionHash");
            session.removeAttribute("catched");

            session.setAttribute("fromSearch", null);
        } else {
            nextJSP = "/PDC/Information.jsp";
        }

        //set pdc notes to session
        List<PdcNotes> noteList = port.retrievePdcNotes(entityId);//session.getAttribute("pdcNoteList");
        if (noteList != null && !noteList.isEmpty()) {
            session.setAttribute("pdcNoteList", noteList);
        }

        try {

            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewPolicyDetailsCommand";
    }
}

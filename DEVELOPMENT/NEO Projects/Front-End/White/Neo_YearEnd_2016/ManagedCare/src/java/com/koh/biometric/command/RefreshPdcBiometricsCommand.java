/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author dewaldvdb
 */
public class RefreshPdcBiometricsCommand extends NeoCommand {
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        HttpSession session = request.getSession();
        String depNo = request.getParameter("depListValues");
        session.setAttribute("depListValues", depNo);
        session.setAttribute("BioTypeSelected", "null"); 
        session.removeAttribute("cdl"); 
        session.removeAttribute("ViewSpecificBiometric"); 
        session.removeAttribute("historyTableFlag");
        
        String dateTo = request.getParameter("dateTo");
        String dateFrom = request.getParameter("dateFrom");
        if (dateFrom == null || dateFrom.equalsIgnoreCase("") || dateFrom.equalsIgnoreCase("null") || dateTo == null || dateTo.equalsIgnoreCase("") || dateTo.equalsIgnoreCase("null")) {
            //set fixed 6month history search
            Date today = new Date(System.currentTimeMillis());
            Calendar cal = Calendar.getInstance();
            cal.setTime(today);
            cal.add(Calendar.MONTH, -6);
            Date startDate = cal.getTime();

            dateFrom = DateTimeUtils.dateFormat.format(startDate).substring(0, 8) + "01";
            dateTo = DateTimeUtils.dateFormat.format(today);

            request.setAttribute("pdcBiometricResultMsg", "Biometric detail retrieved  for the last 6 months history");
        }
        session.setAttribute("dateTo", dateTo);
        session.setAttribute("dateFrom", dateFrom);
        
        try {
            String nextJSP = "/PDC/BiometricDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return null;
    }
    
    @Override
    public String getName() {
        return "RefreshPdcBiometricsCommand";
    }
}

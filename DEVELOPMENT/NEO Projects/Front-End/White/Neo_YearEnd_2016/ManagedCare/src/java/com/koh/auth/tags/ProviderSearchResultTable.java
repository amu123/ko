/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.auth.tags;

import com.koh.auth.dto.ProviderSearchResult;
import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class ProviderSearchResultTable extends TagSupport {

    private String javaScript;
    private String commandName;

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();

        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");

            String message = (String) req.getAttribute("searchProviderResultMessage");

            if (message != null && !message.equalsIgnoreCase(""))
            {
                out.println("<label class=\"red\">" + message + "</label></br></br>");
            }

            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Provider Number</th><th>Provider Name</th><th>Provider Type</th><th>Select</th>" +
                    "</tr>");

            ArrayList<ProviderSearchResult> providers = (ArrayList<ProviderSearchResult>) req.getAttribute("searchProviderResult");
            if (providers != null) {
                for (int i = 0; i < providers.size(); i++) {
                    ProviderSearchResult providerSearchResult = providers.get(i);
                    out.println("<tr><form method=\"post\"><td><label class=\"label\">" + providerSearchResult.getProviderNumber() + "</label><input type=\"hidden\" name=\"providerNumber\" value=\"" + providerSearchResult.getProviderNumber() + "\"/></td>" +
                            "<td><label class=\"label\">" + providerSearchResult.getProviderName() + "</label><input type=\"hidden\" name=\"providerName\" value=\"" + providerSearchResult.getProviderName() + "\"/></td>" +
                            "<td><label class=\"label\">" + providerSearchResult.getProviderType() + "</label><input type=\"hidden\" name=\"providerType\" value=\"" + providerSearchResult.getProviderType() + "\"/></td>" +                           
                            "<td><button name=\"opperation\" name=\"opperation\" type=\"submit\" " + javaScript + " value=\"" + commandName + "\">Select</button> </td></form></tr>");
                }
            }
            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in ProviderSearchResultTable tag", ex);
        }
        return super.doEndTag();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.contribution.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;
import neo.manager.Security;

/**
 *
 * @author yuganp
 */
public class RefundContribCommand extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            String s = request.getParameter("command");
            if (s != null && !s.isEmpty()) {
                System.out.println("command : " + s);
                if (s.equalsIgnoreCase("SearchMemberContribRefund")) {
                    searchMemberContribRefund(request, response, context);
                } else if (s.equalsIgnoreCase("SearchEmployerContribRefund")) {
                    searchEmployerContribRefund(request, response, context);
                } else if (s.equalsIgnoreCase("SelectMemberContribRefund")) {
                    selectMemberContribRefund(request, response, context);
                } else if (s.equalsIgnoreCase("SelectEmployerContribRefund")) {
                    selectEmployerContribRefund(request, response, context);
                } else if (s.equalsIgnoreCase("SaveMemberContribRefund")) {
                    saveMemberContribRefund(request, response, context);
                } else if (s.equalsIgnoreCase("SaveGroupContribRefund")) {
                    saveGroupContribRefund(request, response, context);
                } else if (s.equalsIgnoreCase("SearchMSARefund")) {
                    searchMSARefund(request, response, context);
                } else if (s.equalsIgnoreCase("SelectMemberMSARefund")) {
                    selectMemberMSARefund(request, response, context);
                } else if (s.equalsIgnoreCase("SaveMemberMSARefund")) {
                    saveMemberMSARefund(request, response, context);
                } else if (s.equalsIgnoreCase("SearchWriteOff")) {
                    searchWriteOff(request, response, context);
                } else if (s.equalsIgnoreCase("SelectMemberWriteOff")) {
                    selectMemberWriteOff(request, response, context);
                } else if (s.equalsIgnoreCase("SaveWriteOff")) {
                    saveMemberWriteOff(request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(ContributionBankStatementCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
       return "RefundContribCommand";
    }

    private void searchEmployerContribRefund(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException  {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String product = request.getParameter("scheme");
        List<KeyValueArray> employerList = port.getEmployerList(request.getParameter("groupName"), request.getParameter("groupNumber"), Integer.parseInt(product));
        List<Map<String, String>> map = MapUtils.getMap(employerList);
        request.setAttribute("EmployerSearchResults", map);
        context.getRequestDispatcher("/Contribution/RefundContribResults.jsp").forward(request, response);
    }
    
    private void searchMemberContribRefund(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException  {
        List<Map<String, String>> map = searchMember(request);
        request.setAttribute("MemberSearchResults", map);
        context.getRequestDispatcher("/Contribution/RefundContribResults.jsp").forward(request, response);
    }
    
    private void searchMSARefund(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException  {
        List<Map<String, String>> map = searchMember(request);
        request.setAttribute("MemberSearchResults", map);
        context.getRequestDispatcher("/Contribution/RefundMSAResults.jsp").forward(request, response);
    }

    private void searchWriteOff(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException  {
        List<Map<String, String>> map = searchMember(request);
        request.setAttribute("MemberSearchResults", map);
        context.getRequestDispatcher("/Contribution/WriteOffContribResults.jsp").forward(request, response);
    }

    private List<Map<String, String>> searchMember(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Map<String, String> crit = new HashMap<String,String>();
        Date dt = TabUtils.getDateParam(request, "dob");
        String dob = null;
        if (dt != null) {
            dob = TabUtils.getStringFromDate(dt);
        }
        crit.put("coverNo", request.getParameter("memNo"));
        crit.put("idNumber", request.getParameter("idNo"));
        crit.put("initials", request.getParameter("initials"));
        crit.put("surname", request.getParameter("surname"));
        crit.put("dob", dob);
        for (String s : crit.keySet()) {
            System.out.println(s + " = " + crit.get(s));
        }
        List<KeyValueArray> list = port.findMemberByMapSearchCriteria(MapUtils.getKeyValueArray(crit));
        List<Map<String, String>> map = MapUtils.getMap(list);
        return map;
    }
    
    private void selectMemberContribRefund(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException  {
        request.setAttribute("coverNo", request.getParameter("coverNo"));
        request.setAttribute("memberName", request.getParameter("memberName"));
        request.setAttribute("memberSurname", request.getParameter("memberSurname"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        request.setAttribute("target_div", request.getParameter("target_div"));
        context.getRequestDispatcher("/Contribution/RefundContribDetails.jsp").forward(request, response);
    }

    private void selectEmployerContribRefund(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException  {
        request.setAttribute("entityId", request.getParameter("entityId"));
        request.setAttribute("employerName", request.getParameter("employerName"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        request.setAttribute("target_div", request.getParameter("target_div"));
        NeoManagerBean port = service.getNeoManagerBeanPort();
        List<KeyValueArray> list = port.fetchEmployerMemberBalanceList(Integer.parseInt(request.getParameter("entityId")));
        request.setAttribute("employerMemberList", MapUtils.getMap(list));
        context.getRequestDispatcher("/Contribution/RefundEmployerContribDetails.jsp").forward(request, response);
    }

    private void selectMemberMSARefund(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException  {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        request.setAttribute("coverNo", request.getParameter("coverNo"));
        request.setAttribute("memberName", request.getParameter("memberName"));
        request.setAttribute("memberSurname", request.getParameter("memberSurname"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        request.setAttribute("target_div", request.getParameter("target_div"));
        long start = System.currentTimeMillis();
        List<Map<String, String>> map = MapUtils.getMap(port.getCoverSavingsDetails(request.getParameter("coverNo")));
        String currOptionId = request.getParameter("optionId");
        String status = request.getParameter("status");
        if (map != null && map.size() > 0) {
            boolean interest = false;
            boolean jnl = false;
            boolean claims = false;
            boolean savings = false;
            boolean payout = false;
            boolean eligible = false;
            boolean contrib = false;
            for (Map<String,String> m : map) {
                if (!"0".equals(m.get("interest"))) interest = true;
                if (!"0".equals(m.get("jnl"))) jnl = true;
                if (!"0".equals(m.get("claimsRecovered"))) claims = true;
                if (!"0".equals(m.get("savingsPaid"))) savings = true;
                if (!"0".equals(m.get("contribRecovered"))) contrib = true;
                
                if (currOptionId.equals(m.get("optionId"))) {
                    if (status.equals("4"))
                        eligible = true;
                } else {
                    eligible = true;
                }
                
                double balance = Double.parseDouble(m.get("balance"));
                if (balance > 0) {
                    m.remove("id");
                    m.put("id", "0");
                    payout = true;
                }
                
            }
            request.setAttribute("hasInt", interest ? "yes" : "");
            request.setAttribute("hasJnl", jnl ? "yes" : "");
            request.setAttribute("hasClaim", claims ? "yes" : "");
            request.setAttribute("hasSav", savings ? "yes" : "");
            request.setAttribute("hasPosBalance", payout ? "yes" : "");
            request.setAttribute("hasSavingsDue", eligible ? "yes" : "");
            request.setAttribute("hasContrib", contrib ? "yes" : "");
        }
        long end = System.currentTimeMillis();
        System.out.println("Get Savings Details: " + (end - start) + "ms");
        request.setAttribute("savingsLimits",map);
        context.getRequestDispatcher("/Contribution/RefundMSADetails.jsp").forward(request, response);
    }

    private void selectMemberWriteOff(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException  {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        request.setAttribute("coverNo", request.getParameter("coverNo"));
        request.setAttribute("entityId", request.getParameter("entityId"));
        request.setAttribute("memberName", request.getParameter("memberName"));
        request.setAttribute("memberSurname", request.getParameter("memberSurname"));
        request.setAttribute("main_div", request.getParameter("main_div"));
        request.setAttribute("target_div", request.getParameter("target_div"));
        long start = System.currentTimeMillis();
        List<Map<String, String>> map = MapUtils.getMap(port.fetchContribsForWriteOff(request.getParameter("coverNo")));
        long end = System.currentTimeMillis();
        System.out.println("Get Savings Details: " + (end - start) + "ms");
        request.setAttribute("writeOffs",map);
        context.getRequestDispatcher("/Contribution/WriteOffContrib.jsp").forward(request, response);
    }

    private void saveMemberContribRefund(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException  {
        Map<String, String> fieldsMap = validateContribRefund(request);
        String status = TabUtils.getStatus(fieldsMap);
        String additionalData = null;
        if ("OK".equalsIgnoreCase(status)) {
            boolean result = saveMemberContribRefund(request);
            if (result) {
                fieldsMap.put("transferRefundStatus", "ok");
                additionalData = "\"message\":\"Refund has been saved.\"";
            } else {
                status = "error";
                fieldsMap.put("transferRefundStatus", "error");
                additionalData = "\"message\":\"There was an error saving the refund.\"";
            }
        } else {
            fieldsMap.put("transferRefundStatus", "error");
        }
        String updateFields = TabUtils.convertMapToJSON(fieldsMap);
        String result = TabUtils.buildJsonResult(status, null, updateFields, additionalData);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println(result);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (out != null) out.close();
        }
    }
    
    private boolean saveMemberContribRefund(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        int entityId = TabUtils.getIntParam(request, "entityId");
        String coverNo = request.getParameter("coverNo");
        XMLGregorianCalendar date = DateTimeUtils.convertDateToXMLGregorianCalendar(TabUtils.getDateParam(request, "refundDate"));
        BigDecimal bdParam = TabUtils.getBDParam(request, "refundAmount");
        int optionId = TabUtils.getIntParam(request, "optionId");
        String narrative = request.getParameter("refundReason");
        Security security = TabUtils.getSecurity(request);
        return port.saveContributionRefund(entityId, coverNo, date, bdParam, optionId, narrative, security);
    }
    
    private Map<String, String> validateContribRefund(HttpServletRequest request) {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        String reason = request.getParameter("refundReason");
        fieldsMap.put("refundReason_error", (reason == null || reason.trim().isEmpty()) ? "Reason is required" : "");
        Date date = TabUtils.getDateParam(request, "refundDate");
        if (date == null) {
            fieldsMap.put("refundDate_error", "Date is required");
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            //cal.add(Calendar.MONTH, -7);
            //Date startDate = cal.getTime();
            //cal.add(Calendar.MONTH, 8);
            cal.add(Calendar.MONTH, 1);
            Date endDate = cal.getTime();
            /*if (date.before(startDate)) {
                fieldsMap.put("refundDate_error", "Date is too far back");
            } else*/ if (date.after(endDate)) {
                fieldsMap.put("refundDate_error", "Date is too far in the future");
            } else {
                fieldsMap.put("refundDate_error", "");
            }
        }
        BigDecimal bdParam = TabUtils.getBDParam(request, "refundAmount");
        if (bdParam == null || bdParam.equals(BigDecimal.ZERO)) {
            fieldsMap.put("refundAmount_error", "Invalid Amount");
        } else {
            fieldsMap.put("refundAmount_error", "");
        }
        return fieldsMap;
    }
    
    private void saveGroupContribRefund(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        Map<String, String> fieldsMap = validateGroupContribRefund(request);
        String status = TabUtils.getStatus(fieldsMap);
        String additionalData = null;
        if ("OK".equalsIgnoreCase(status)) {
            boolean result = saveGroupContribRefund(request);
            if (result) {
                fieldsMap.put("transferRefundStatus", "ok");
//                additionalData = "\"message\":\"Refund has been saved.\"";
            } else {
                status = "error";
                fieldsMap.put("transferRefundStatus", "error");
//                additionalData = "\"message\":\"There was an error saving the refund.\"";
            }
        } else {
            fieldsMap.put("transferRefundStatus", "error");
        }
        String updateFields = TabUtils.convertMapToJSON(fieldsMap);
        String result = TabUtils.buildJsonResult(status, null, updateFields, additionalData);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println(result);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (out != null) out.close();
        }
    }

    private boolean saveGroupContribRefund(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        int entityId = TabUtils.getIntParam(request, "entityId");
        XMLGregorianCalendar date = DateTimeUtils.convertDateToXMLGregorianCalendar(TabUtils.getDateParam(request, "refundDate"));
        String narrative = request.getParameter("refundReason");
        Security security = TabUtils.getSecurity(request);
        List<Map<String,String>> mapList = new ArrayList<Map<String,String>>();
        Map pMap = request.getParameterMap();
        for ( Object p : pMap.keySet()) {
            String key = p.toString();
//            System.out.println(key + " = " +request.getParameter(key).toString());
            if (key != null && key.startsWith("contrib_")) {
                String[] sArr = key.split("_");
                if (sArr.length == 2) {
                    String coverNo = sArr[1];
                    BigDecimal bdParam = TabUtils.getBDParam(request, key);
                    if (bdParam != null && bdParam.compareTo(BigDecimal.ZERO) != 0) {
                        Map<String,String> map = new HashMap<String, String>();
                        map.put("cover", coverNo);
                        map.put("amount", request.getParameter(key));
                        mapList.add(map);
                    }
                }
            }
        }
        if (mapList.isEmpty()) {
            return false;
        }
        List<KeyValueArray> kvaList = MapUtils.getKeyValueArray(mapList);
        return port.saveGroupContributionRefund(entityId, date, narrative, kvaList, security);
    }
    
    
    private Map<String, String> validateGroupContribRefund(HttpServletRequest request) {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        String reason = request.getParameter("refundReason");
        fieldsMap.put("refundReason_error", (reason == null || reason.trim().isEmpty()) ? "Reason is required" : "");
        Date date = TabUtils.getDateParam(request, "refundDate");
        if (date == null) {
            fieldsMap.put("refundDate_error", "Date is required");
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            //cal.add(Calendar.MONTH, -7);
            //Date startDate = cal.getTime();
            //cal.add(Calendar.MONTH, 8);
            cal.add(Calendar.MONTH, 1);
            Date endDate = cal.getTime();
            /*if (date.before(startDate)) {
                fieldsMap.put("refundDate_error", "Date is too far back");
            } else*/ if (date.after(endDate)) {
                fieldsMap.put("refundDate_error", "Date is too far in the future");
            } else {
                fieldsMap.put("refundDate_error", "");
            }
        }
        BigDecimal bdParam = TabUtils.getBDParam(request, "refundAmount");
        if (bdParam == null || bdParam.equals(BigDecimal.ZERO)) {
            fieldsMap.put("refundAmount_error", "Invalid Amount");
        } else {
            fieldsMap.put("refundAmount_error", "");
        }        
        return fieldsMap;
    }
    
    
    private void saveMemberMSARefund(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException  {
        Map<String, String> fieldsMap = validateMSARefund(request);
        String status = TabUtils.getStatus(fieldsMap);
        String additionalData = null;
        if ("OK".equalsIgnoreCase(status)) {
            boolean result = saveMemberMSARefund(request);
            if (result) {
                fieldsMap.put("transferRefundStatus", "ok");
                additionalData = "\"message\":\"Refund has been saved.\"";
            } else {
                status = "error";
                fieldsMap.put("transferRefundStatus", "error");
                additionalData = "\"message\":\"There was an error saving the refund.\"";
            }
        } else {
            fieldsMap.put("transferRefundStatus", "error");
        }
        String updateFields = TabUtils.convertMapToJSON(fieldsMap);
        String result = TabUtils.buildJsonResult(status, null, updateFields, additionalData);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println(result);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (out != null) out.close();
        }
    }
    
    private boolean saveMemberMSARefund(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String coverNo = request.getParameter("coverNo");
        XMLGregorianCalendar date = DateTimeUtils.convertDateToXMLGregorianCalendar(TabUtils.getDateParam(request, "refundDate"));
        String biRefNum = request.getParameter("biRefNum");
        BigDecimal claimAmount = getBDParam(request, "refundClaims");
        BigDecimal savingsAmount = getBDParam(request, "refundSavings");
        int optionId = TabUtils.getIntParam(request, "refundOptionId");
        Security security = TabUtils.getSecurity(request);
        return port.saveMSARefund(coverNo, optionId, date, claimAmount, savingsAmount, biRefNum, security);
    }

    private Map<String, String> validateMSARefund(HttpServletRequest request) {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        Date date = TabUtils.getDateParam(request, "refundDate");
        if (date == null) {
            fieldsMap.put("refundDate_error", "Date is required");
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.MONTH, -7);
            Date startDate = cal.getTime();
            cal.add(Calendar.MONTH, 8);
            Date endDate = cal.getTime();
            if (date.before(startDate)) {
                fieldsMap.put("refundDate_error", "Date is too far back");
            } else if (date.after(endDate)) {
                fieldsMap.put("refundDate_error", "Date is too far in the future");
            } else {
                fieldsMap.put("refundDate_error", "");
            }
        }
        String biRefNum = request.getParameter("biRefNum");
        if (biRefNum != null && !biRefNum.isEmpty()) {
            if (!TabUtils.isNumberField(biRefNum)) {
                fieldsMap.put("biRefNum_error", "BI number must be numeric");
            } else {
                fieldsMap.put("biRefNum_error", "");
            }
        } else {
            fieldsMap.put("biRefNum_error", "BI number is required");
        }
        BigDecimal claimAmount = getBDParam(request, "refundClaims");
        BigDecimal savingsAmount = getBDParam(request, "refundSavings");
        boolean hasClaim = !(claimAmount == null || claimAmount.compareTo(BigDecimal.ZERO) == -1);
        boolean hasSavings = !(savingsAmount == null || savingsAmount.compareTo(BigDecimal.ZERO) == 0 || savingsAmount.compareTo(BigDecimal.ZERO) == -1);
        fieldsMap.put("refundClaims_error", "");
        fieldsMap.put("refundSavings_error", "");
        if (!hasClaim && !(request.getParameter("refundClaims") == null || request.getParameter("refundClaims").isEmpty())) {
            fieldsMap.put("refundClaims_error", "Invalid Amount");
        }
        if (!hasSavings && !(request.getParameter("refundSavings") == null || request.getParameter("refundSavings").isEmpty())) {
            fieldsMap.put("refundSavings_error", "Invalid Amount");
        }
        if (!(hasClaim || hasSavings)) {
            if (fieldsMap.get("refundClaims_error").isEmpty()) fieldsMap.put("refundClaims_error", "At least one amount is required.");
            if (fieldsMap.get("refundSavings_error").isEmpty()) fieldsMap.put("refundSavings_error", "At least one amount is required.");
        }
        return fieldsMap;
    }
    
    private void saveMemberWriteOff(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException  {
        Map<String, String> fieldsMap = validateWriteOff(request);
        String status = TabUtils.getStatus(fieldsMap);
        String additionalData = null;
        if ("OK".equalsIgnoreCase(status)) {
            boolean result = saveMemberWriteOff(request);
            if (result) {
                fieldsMap.put("transferReverseStatus", "ok");
                additionalData = "\"message\":\"Write off has been saved.\"";
            } else {
                status = "error";
                fieldsMap.put("transferReverseStatus", "error");
                additionalData = "\"message\":\"There was an error saving the write off.\"";
            }
        } else {
            fieldsMap.put("transferReverseStatus", "error");
        }
        String updateFields = TabUtils.convertMapToJSON(fieldsMap);
        String result = TabUtils.buildJsonResult(status, null, updateFields, additionalData);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println(result);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (out != null) out.close();
        }
    }
    
    private boolean saveMemberWriteOff(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String coverNo = request.getParameter("coverNumber");
        String paymentNarrative = request.getParameter("paymentNarrative");
        XMLGregorianCalendar date = DateTimeUtils.convertDateToXMLGregorianCalendar(TabUtils.getDateParam(request, "contribDate"));
        BigDecimal amount = setWriteOffAmount(request);
        int optionId = TabUtils.getIntParam(request, "optionId");
        int entityId = TabUtils.getIntParam(request, "entityId");
        Security security = TabUtils.getSecurity(request);
        return port.saveContributionWriteOff(entityId, coverNo, date, amount, optionId, paymentNarrative, security);
    }
    
    private BigDecimal setWriteOffAmount(HttpServletRequest request){
        BigDecimal amount = getBDParam(request, "amount");
        BigDecimal balance = getBDParam(request, "balance");
        BigDecimal amountCorrected = amount;
        
        if(balance.compareTo(BigDecimal.ZERO) == -1 && amount.compareTo(BigDecimal.ZERO) == 1
                || balance.compareTo(BigDecimal.ZERO) == 1 && amount.compareTo(BigDecimal.ZERO) == -1){
            amountCorrected = amount.negate();
        }
        
        return amountCorrected;
    }

    private Map<String, String> validateWriteOff(HttpServletRequest request) {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        BigDecimal amount = setWriteOffAmount(request);
        BigDecimal balance = getBDParam(request, "balance");
        String reason = request.getParameter("paymentNarrative");
        fieldsMap.put("AmountToWriteOff_error", "");
        if (amount == null || amount.compareTo(BigDecimal.ZERO) == 0) {
            fieldsMap.put("AmountToWriteOff_error", "Amount is required");
        } else {
            if (amount.compareTo(balance) == 1) {
                fieldsMap.put("AmountToWriteOff_error", "Amount cannot be greater than the balance.");
            }
        }
        if (reason == null || reason.trim().isEmpty()) {
            fieldsMap.put("WriteOffReason_error", "Reason is required.");
        } else {
            fieldsMap.put("WriteOffReason_error", "");
        }
        return fieldsMap;
    }
    
    private BigDecimal getBDParam(HttpServletRequest request, String param) {
        try {
            String paramValue = request.getParameter(param);
            if (paramValue != null && !paramValue.isEmpty()) {
                return new BigDecimal(paramValue);
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }
    
}

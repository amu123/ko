/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
//import neo.manager.AuthHistoryDetails;

/**
 *
 * @author johanl
 */
public class AuthHistoryResultTable extends TagSupport {

    private String commandName;
    private String javascript;

    /**
     * Called by the container to invoke this tag.
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        System.out.println("Entering xml generation history table");
        try {
            out.println("<label class=\"header\">Authorisation History</label></br></br>");
            out.println("<table width=\"500\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");

//            ArrayList<AuthHistoryDetails> history = new ArrayList<AuthHistoryDetails>();
//            history = (ArrayList<AuthHistoryDetails>) session.getAttribute("authUpdateHistory");
//            if (history != null) {
//                System.out.println("history search result size = " + history.size());
//                for (AuthHistoryDetails his : history) {
//
//                    out.println("<tr><th colspan=\"4\">Modified By: " + his.getCreatedBy() + "</th></tr>");
//                    out.println("<tr><th colspan=\"4\">Modified Date: " + his.getCreationDate() + "</th></tr>");
//                    out.println("<tr><form>" +
//                            "<td><label class=\"label\">" + his.getHistoryId() + "</label><input type=\"hidden\" name=\"historyId\" value=\"" + his.getHistoryId() + "\"/></td> " +
//                            "<td><label class=\"label\">" + his.getAuthCode() + "</label></td> " +
//                            "<td><label class=\"label\">" + his.getAuthType() + "</label></td> " +
//                            "<td><button name=\"opperation\" type=\"submit\" " + javascript + " value=\"" + commandName + "\">View</button></td></form></tr>");
//
//                }
//                //out.println("<tr><td><button name=\"operation\" type=\"submit\" " + javascript + " value=\"" + commandName + "\">Select All</button></td></tr>");
//            }
            out.println("</table>");

        } catch (java.io.IOException ex) {
            throw new JspException("Error in AuthSearchResultTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

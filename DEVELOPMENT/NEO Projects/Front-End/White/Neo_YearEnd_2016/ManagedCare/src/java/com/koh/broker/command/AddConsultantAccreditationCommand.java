/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.brokerFirm.ConsultantMapping;
import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.BrokerAccred;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author princes
 */
public class AddConsultantAccreditationCommand extends NeoCommand{
    
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        request.setAttribute("consultantEntityId", request.getParameter("consultantEntityId"));
        try {
            String s = request.getParameter("buttonPressed");
            if (s != null && !s.isEmpty()) {
                if (s.equalsIgnoreCase("Add Accreditation")) {
                    addDetails(request, response, context);
                } else if (s.equalsIgnoreCase("SaveButton")) {
                    save(request, response, context);
                }
            }
            
//            request.setAttribute("MemAppSearchResults", col);
//            context.getRequestDispatcher("/MemberApplication/MemberAppSearchResults.jsp").forward(request, response);
        } catch (java.lang.Exception ex) {
            Logger.getLogger(AddConsultantAccreditationCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void addDetails(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));

        context.getRequestDispatcher("/Broker/AddConsultantAccreditation.jsp").forward(request, response);
    }
    
    private int getIntParam(HttpServletRequest request, String paramName) {
        try {
            String maxDepStr = request.getParameter(paramName);
            return Integer.parseInt(maxDepStr);
        } catch (java.lang.Exception e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }
        
    private void save(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = ConsultantMapping.validateAccreditation(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        int entityId = new Integer(request.getParameter("consultantEntityId"));
        
        if ("OK".equalsIgnoreCase(status)) {
            if (save(request, entityId)) {
                
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving accreditation.\"";
            }
        } else {
            additionalData = null;
        }

        if ("OK".equalsIgnoreCase(status)) {                        
            context.getRequestDispatcher(new ConsultantAppTabContentsCommand().getConsultantAccreditation(request, entityId)).forward(request, response);
            
        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }
        
    }
    
    private boolean save(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = getNeoUser(request);
        
        try {
            BrokerAccred ba = ConsultantMapping.getAccreditationDetails(request, entityId, neoUser);
            String save = "save";
            port.saveBrokerFirmAccreditation(ba, save);
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
        
    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser)request.getSession().getAttribute("persist_user");
    }
    
    private String getDateStr(XMLGregorianCalendar xgc) {
        try {
            Date dt = xgc.toGregorianCalendar().getTime();
            return DateTimeUtils.convertToYYYYMMDD(dt);
        } catch (java.lang.Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
    }
    


    @Override
    public String getName() {
        return "AddConsultantAccreditationCommand";
    }

   
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import neo.manager.MainList;
import neo.manager.NeoUser;
import neo.manager.PdcSearchCriteria;

/**
 *
 * @author princes
 */
public class ViewWorkBenchTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    private int minResult;
    private int maxResult = 100;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        Collection<MainList> mainList = new ArrayList<MainList>();
        Collection<MainList> mList = new ArrayList<MainList>();
        PdcSearchCriteria searchS = (PdcSearchCriteria) session.getAttribute("search");
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        String pdcAdmin = "" + session.getAttribute("persist_user_pdcAdmin");
        boolean forwardButton;
        boolean backwardButton;

        PdcSearchCriteria search = new PdcSearchCriteria();

        if (searchS != null) {
            search = searchS;
        } else {
            search.setUser(user.getUserId());
            minResult = minResult + 0;
            maxResult = maxResult + 0;
            search.setMinResult(minResult);
            search.setMaxResult(maxResult);
            session.setAttribute("search", search);
        }

        mainList = NeoCommand.service.getNeoManagerBeanPort().findWorkbenchDetailsByCriteria(search, user, pdcAdmin);
        if (mainList != null && !mainList.isEmpty()) {
            System.out.println("PDC WORK LIST SIZE = " + mainList.size());
        } else {
            System.out.println("PDC WORK LIST IS EMPTY!");
        }

        session.setAttribute("mainList", mainList);
        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        String dob = "";
        mList = DateTimeUtils.orderMainListByCreatedDate((ArrayList) mainList);

        try {
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.print("<tr>");
            out.print("<th scope=\"col\">Worklist No</th>");
            out.print("<th scope=\"col\">Member Number</th>");
            out.print("<th scope=\"col\">Dependant No</th>");
            out.print("<th scope=\"col\">Name</th>");
            out.print("<th scope=\"col\">Surname</th>");
            out.print("<th scope=\"col\">Date of Birth</th>");
            out.print("<th scope=\"col\">Assigned to</th>");
            out.print("<th scope=\"col\">Assigned by</th>");
            out.print("<th scope=\"col\"> </th>");
            out.print("<th scope=\"col\"> </th>");
            out.print("</tr>");
            int count = 0;
            int worklistNumber = 1;
            for (MainList wl : mList) {
                boolean hasCP = NeoCommand.service.getNeoManagerBeanPort().checkIfDependantHasExistingCarePath(wl.getWorklistID());
                if(hasCP){
                    //patient has carepath, check for any pending tasks this week
                    if (wl.getEarliestTask() != null && (wl.getEarliestTask().equals(wl.getCurrentWeek()))) {
                        //patient has a task to manage this week, mark blue else mark green for OK
                        out.print("<tr bgColor=\"#66d9ff\">");
                    } else {
                        out.print("<tr bgColor=\"#66ff99\">");
                    }
                }else{
                    //newly flagged patient, no carepath found, mark red
                    out.print("<tr bgColor=\"#ff4d4d\">");
                }
                
                
                out.print("<td><label class=\"label\">" + worklistNumber + "</label></td>");
                out.print("<td><label class=\"label\">" + wl.getCoverNumber() + "</label></td>");
                out.print("<td><label class=\"label\">" + wl.getDependentNumber() + "</label></td>");
                out.print("<td><label class=\"label\">" + wl.getName() + "</label></td>");
                out.print("<td><label class=\"label\">" + wl.getSurname() + "</label></td>");
                if (wl.getDateOfBirth() != null) {
                    Date dateOfBirth = wl.getDateOfBirth().toGregorianCalendar().getTime();
                    dob = formatter.format(dateOfBirth);
                    out.print("<td><label class=\"label\">" + dob + "</label></td>");
                } else {
                    out.print("<td></td>");
                }
                NeoUser userBy = NeoCommand.service.getNeoManagerBeanPort().getUserSafeDetailsById(wl.getAssignedBy());
                NeoUser userTo = NeoCommand.service.getNeoManagerBeanPort().getUserSafeDetailsById(wl.getAssignedTo());
                out.print("<td><label class=\"label\">" + userTo.getUsername() + "</label></td>");
                out.print("<td><label class=\"label\">" + userBy.getUsername() + "</label></td>");
                out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitDetailsWithAction('ViewSubWorkBenchCommand', " + wl.getWorklistID() + ")\"; value=\"ViewSubWorkBenchCommand\">Details</button></td>");

                String type;
                if (pdcAdmin != null && pdcAdmin.equalsIgnoreCase("true")) {
                    type = "Assign";
                     out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('ViewAssignPHMCommand', " + wl.getWorklistID()+ ")\"; value=\"ViewAssignPHMCommand\">" + type + "</button></td>");
                } else {
                    type = "Re-Assign";
                     out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('ViewReAssignPHMCommand', " + wl.getWorklistID()+ ")\"; value=\"ViewReAssignPHMCommand\">" + type + "</button></td>");
                }

                //out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('ViewReAssignPHMCommand', " + wl.getWorklistID()+ ")\"; value=\"ViewReAssignPHMCommand\">" + type + "</button></td>");

                out.print("</tr>");
                count++;
                worklistNumber++;
            }

            if (mainList != null && mainList.size() >= 100) {
                forwardButton = true;
                backwardButton = true;
                if (backwardButton || forwardButton) {
                    out.println("<tr><th colspan=\"14\" align=\"right\" >");

                    if (backwardButton) {
                        out.println("<button name=\"opperation\" type=\"button\" onClick=\"submitPageAction('NextworkbenchListCommand', 'backward')\"; value=\"\">Previous</button>");
                    }
                    if (forwardButton) {
                        out.println("<button name=\"opperation\" type=\"button\" onClick=\"submitPageAction('NextworkbenchListCommand', 'forward')\"; value=\"\">Next</button>");
                    }
                    out.println("</th></tr>");
                }
            }

            out.println("</table>");
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }
}

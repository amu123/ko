/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.biometric.command;

import com.koh.command.Command;
import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.BiometricsTabs;
import neo.manager.CopdBiometrics;
import neo.manager.Diagnosis;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author martins
 */
public class CaptureBiometricsCommand extends NeoCommand {

    private Object _datatypeFactory;
    private String next;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
        boolean checkError = false;
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        String client = String.valueOf(context.getAttribute("Client"));
        
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        List<BiometricsTabs> retObject = (List) session.getAttribute("bioObject");
        
        session.setAttribute("icd10_error", "");
        
        try{
            //String details = session.getAttribute("Adetails").toString();

            String memberNum = String.valueOf(session.getAttribute("memberNumber_text"));
            String depListValues = String.valueOf(session.getAttribute("depListValues_text"));
            String dateMeasured =  String.valueOf(session.getAttribute("dateMeasured_text"));
            String dateReceived = String.valueOf(session.getAttribute("dateReceived_text"));
            String bpiolic = String.valueOf(session.getAttribute("bpSystolic_text"));
            String bpDiastolic = String.valueOf(session.getAttribute("bpDiastolic_text"));
            String weight = String.valueOf(session.getAttribute("weight_text"));
            String length = String.valueOf(session.getAttribute("length_text"));
            String bmi = String.valueOf(session.getAttribute("bmi_text"));
            String alcoholConsumption = String.valueOf(session.getAttribute("alcoholConsumption_text"));
            String currentSmoker = String.valueOf(session.getAttribute("currentSmoker_text"));
            String smokingQuestion = String.valueOf(session.getAttribute("smokingQuestion_text"));
            String exSmoker = String.valueOf(session.getAttribute("exSmoker_text"));
            String stopped = String.valueOf(session.getAttribute("stopped_text"));
            String exerciseQuestion = String.valueOf(session.getAttribute("exerciseQuestion_text"));
            String providerNumber = String.valueOf(session.getAttribute("providerNumber_text"));
            String source = String.valueOf(session.getAttribute("source_text"));
            String icd10 = String.valueOf(session.getAttribute("icd10_text"));
            
            if(currentSmoker == null){
                session.removeAttribute("currentSmoker_text");
            }
            if(smokingQuestion == null){
                session.removeAttribute("smokingQuestion_text");
            }
            if(exSmoker == null){
                session.removeAttribute("exSmoker_text");
            }
            if(stopped == null){
                session.removeAttribute("stopped_text");
            }
            
            boolean emptyAsthma = false;
            String day = String.valueOf(session.getAttribute("asthmaDaySym_text"));
            String night = String.valueOf(session.getAttribute("asthmaNightSym_text"));
            String bpef = String.valueOf(session.getAttribute("bpef_text"));
            String sabs = String.valueOf(session.getAttribute("sabaUse_text"));
            String sadetalis = String.valueOf(session.getAttribute("astmanDetail_text"));
            
            if((day == null || day.equals("") || day.equals("null") || day.equals("0.0")) && (night == null || night.equals("") || night.equals("null") || night.equals("0.0")) && (bpef == null || bpef.equals("") || bpef.equals("null") || bpef.equals("0.0")) 
            && (sabs == null || sabs.equals("") || sabs.equals("null") || sabs.equals("0.0")) && (sadetalis == null || sadetalis.equals("") || sadetalis.equals("null") || sadetalis.equals("0.0"))){
                emptyAsthma = true;
                
                System.out.println("emptyAsthma is EMPTY");
            }
            else
            {
                
                System.out.println("emptyAsthma NOT EMPTY");
            }
            
            boolean emptyHypertension = false;
            String hypertensionDetails = String.valueOf(session.getAttribute("hypertensionDetails_text"));
            
            if(hypertensionDetails == null || hypertensionDetails.equals("") || hypertensionDetails.equals("null") || hypertensionDetails.equals("0.0")){
                emptyHypertension = true;
                
                System.out.println("emptyHypertension is EMPTY");
            }
            else
            {
                
                System.out.println("emptyHypertension NOT EMPTY");
            }
            
            boolean emptyRenal = false;
            String RenalsCreatinin = String.valueOf(session.getAttribute("sCreatinin_text"));
            String RenalProteinuria = String.valueOf(session.getAttribute("proteinuria_text"));
            String RenalDetail = String.valueOf(session.getAttribute("renalDetail_text"));
            
            
            if((RenalsCreatinin == null || RenalsCreatinin.equals("") || RenalsCreatinin.equals("null") || RenalsCreatinin.equals("0.0")) && (RenalProteinuria == null || RenalProteinuria.equals("") || RenalProteinuria.equals("null") || RenalProteinuria.equals("0.0")) && (RenalDetail == null || RenalDetail.equals("") || RenalDetail.equals("null") || RenalDetail.equals("0.0"))){
                emptyRenal = true;
                
                System.out.println("emptyRenal is EMPTY");
            }
            else
            {
                
                System.out.println("emptyRenal NOT EMPTY");
            }
            
            boolean emptyCardiac = false;
            String CardiacNyhaClass = String.valueOf(session.getAttribute("nyhaClass_text"));
            String CardiacLvef = String.valueOf(session.getAttribute("lvef_text"));
            String CardiacDetail = String.valueOf(session.getAttribute("CardiacDetail_text"));
            
            
            if((CardiacNyhaClass == null || CardiacNyhaClass.equals("") || CardiacNyhaClass.equals("null") || CardiacNyhaClass.equals("0.0")) && (CardiacLvef == null || CardiacLvef.equals("") || CardiacLvef.equals("null") || CardiacLvef.equals("0.0")) && (CardiacDetail == null || CardiacDetail.equals("") || CardiacDetail.equals("null") || CardiacDetail.equals("0.0"))){
                emptyCardiac = true;
                
                System.out.println("emptyCardiac is EMPTY");
            }
            else
            {
                
                System.out.println("emptyCardiac NOT EMPTY");
            }
            
            boolean emptyDiabetes = false;
            String DiabetesHbAIC = String.valueOf(session.getAttribute("diabetesHbAIC_text"));
            String DiabetesRandomGlucose = String.valueOf(session.getAttribute("diabetesRandomGlucose_text"));
            String DiabetesFastingGlucose = String.valueOf(session.getAttribute("diabetesFastingGlucose_text"));
            String DiabetesTotalCholesterol = String.valueOf(session.getAttribute("diabetesTotalCholesterol_text"));
            String Diabetesldlc = String.valueOf(session.getAttribute("diabetesldlc_text"));
            String Diabeteshdlc = String.valueOf(session.getAttribute("diabeteshdlc_text"));
            String DiabetesTryglyseride = String.valueOf(session.getAttribute("diabetesTryglyseride_text"));
            String DiabetesProteinuria = String.valueOf(session.getAttribute("diabetesProteinuria_text"));
            String DiabetesTchdlRation = String.valueOf(session.getAttribute("diabetesTchdlRation_text"));
            String DiabetesDetail = String.valueOf(session.getAttribute("diabetesDetail_text"));
            
            
            
            if((DiabetesHbAIC == null || DiabetesHbAIC.equals("") || DiabetesHbAIC.equals("null") || DiabetesHbAIC.equals("0.0")) && (DiabetesRandomGlucose == null || DiabetesRandomGlucose.equals("") || DiabetesRandomGlucose.equals("null") || DiabetesRandomGlucose.equals("0.0")) 
                    && (DiabetesFastingGlucose == null || DiabetesFastingGlucose.equals("") || DiabetesFastingGlucose.equals("null") || DiabetesFastingGlucose.equals("0.0")) 
                    && (DiabetesTotalCholesterol == null || DiabetesTotalCholesterol.equals("") || DiabetesTotalCholesterol.equals("null") || DiabetesTotalCholesterol.equals("0.0"))
                    && (Diabetesldlc == null || Diabetesldlc.equals("") || Diabetesldlc.equals("null") || Diabetesldlc.equals("0.0"))
                    && (Diabeteshdlc == null || Diabeteshdlc.equals("") || Diabeteshdlc.equals("null") || Diabeteshdlc.equals("0.0"))
                    && (DiabetesTryglyseride == null || DiabetesTryglyseride.equals("") || DiabetesTryglyseride.equals("null") || DiabetesTryglyseride.equals("0.0"))
                    && (DiabetesProteinuria == null || DiabetesProteinuria.equals("") || DiabetesProteinuria.equals("null") || DiabetesProteinuria.equals("0.0"))
                    && (DiabetesTchdlRation == null || DiabetesTchdlRation.equals("") || DiabetesTchdlRation.equals("null") || DiabetesTchdlRation.equals("0.0"))
                    && (DiabetesDetail == null || DiabetesDetail.equals("") || DiabetesDetail.equals("null") || DiabetesDetail.equals("0.0"))){
                emptyDiabetes = true;
                
                System.out.println("emptyDiabetes is EMPTY");
            }
            else
            {
                
                System.out.println("emptyDiabetes NOT EMPTY");
            }
            
            boolean emptyHyperlipidaemia = false;
            String TotalCholesterol = String.valueOf(session.getAttribute("fev1Predicted_text"));
            String Ldlc = String.valueOf(session.getAttribute("ldlc_text"));
            String totalCholesterol = String.valueOf(session.getAttribute("totalCholesterol_text"));
            String hdlc = String.valueOf(session.getAttribute("hdlc_text"));
            String framingham = String.valueOf(session.getAttribute("framingham_text"));
            String score = String.valueOf(session.getAttribute("score_text"));
            String Triglyceride = String.valueOf(session.getAttribute("triglyceride_text"));
            String TcHdlRatio = String.valueOf(session.getAttribute("tcHdlRatio_text"));
            String HyperlipaemiaDetail = String.valueOf(session.getAttribute("hyperlipaemiaDetail_text"));
            if((score == null || score.equals("") || score.equals("null") || score.equals("0.0")) 
                    && (framingham == null || framingham.equals("") || framingham.equals("null") || framingham.equals("0.0")) 
                    && (hdlc == null || hdlc.equals("") || hdlc.equals("null") || hdlc.equals("0.0")) 
                    && (totalCholesterol == null || totalCholesterol.equals("") || totalCholesterol.equals("null") || totalCholesterol.equals("0.0")) 
                    && (TotalCholesterol == null || TotalCholesterol.equals("") || TotalCholesterol.equals("null") || TotalCholesterol.equals("0.0")) 
                    && (Ldlc == null || Ldlc.equals("") || Ldlc.equals("null") || Ldlc.equals("0.0"))
                    && (Triglyceride == null || Triglyceride.equals("") || Triglyceride.equals("null") || Triglyceride.equals("0.0"))
                    && (TcHdlRatio == null || TcHdlRatio.equals("") || TcHdlRatio.equals("null") || TcHdlRatio.equals("0.0"))
                    && (HyperlipaemiaDetail == null || HyperlipaemiaDetail.equals("") || HyperlipaemiaDetail.equals("null") || HyperlipaemiaDetail.equals("0.0"))){
                emptyHyperlipidaemia = true;
                
                System.out.println("emptyHyperlipidaemia is EMPTY");
            }
            else
            {
                
                System.out.println("emptyHyperlipidaemia NOT EMPTY");
                
            }
            
            boolean emptyCoronary = false;
            String CoronaryHbAIC = String.valueOf(session.getAttribute("CoronaryHbAIC_text"));
            String CoronaryTotalCholesterol = String.valueOf(session.getAttribute("CoronaryTotalCholesterol_text"));
            String CoronaryLdlc = String.valueOf(session.getAttribute("CoronaryLdlc_text"));
            String CoronaryHdlc = String.valueOf(session.getAttribute("CoronaryHdlc_text"));
            String CoronaryTryglyseride = String.valueOf(session.getAttribute("CoronaryTryglyseride_text"));
            String CoronaryScoreRisks = String.valueOf(session.getAttribute("CoronaryScoreRisks_text"));
            String CoronaryTchdlRatio = String.valueOf(session.getAttribute("CoronaryTchdlRatio_text"));
            String CoronaryDetail = String.valueOf(session.getAttribute("coronaryDetail_text"));
            
            if((CoronaryHbAIC == null || CoronaryHbAIC.equals("") || CoronaryHbAIC.equals("null") || CoronaryHbAIC.equals("0.0")) && (CoronaryTotalCholesterol == null || CoronaryTotalCholesterol.equals("") || CoronaryTotalCholesterol.equals("null") || CoronaryTotalCholesterol.equals("0.0"))
                    && (CoronaryLdlc == null || CoronaryLdlc.equals("") || CoronaryLdlc.equals("null") || CoronaryLdlc.equals("0.0"))
                    && (CoronaryHdlc == null || CoronaryHdlc.equals("") || CoronaryHdlc.equals("null") || CoronaryHdlc.equals("0.0"))
                    && (CoronaryTryglyseride == null || CoronaryTryglyseride.equals("") || CoronaryTryglyseride.equals("null") || CoronaryTryglyseride.equals("0.0"))
                    && (CoronaryScoreRisks == null || CoronaryScoreRisks.equals("") || CoronaryScoreRisks.equals("null") || CoronaryScoreRisks.equals("0.0"))
                    && (CoronaryTchdlRatio == null || CoronaryTchdlRatio.equals("") || CoronaryTchdlRatio.equals("null") || CoronaryTchdlRatio.equals("0.0"))
                    && (CoronaryDetail == null || CoronaryDetail.equals("") || CoronaryDetail.equals("null") || CoronaryDetail.equals("0.0"))){
                emptyCoronary = true;
                
                System.out.println("emptyCoronary is EMPTY");
            }
            else
            {
                
                System.out.println("emptyCoronary NOT EMPTY");
                
            }
            
            boolean emptyCOPD = false;
            String Fev1Predicted = String.valueOf(session.getAttribute("fev1Predicted_text"));
            String Fev1FVC = String.valueOf(session.getAttribute("fev1FVC_text"));
            String Dyspnea = String.valueOf(session.getAttribute("dyspnea_text"));
            String COPDdetail = String.valueOf(session.getAttribute("COPDdetail_text"));
            if((CoronaryHbAIC == null || CoronaryHbAIC.equals("") || CoronaryHbAIC.equals("null") || CoronaryHbAIC.equals("0.0")) && (CoronaryTotalCholesterol == null || CoronaryTotalCholesterol.equals("") || CoronaryTotalCholesterol.equals("null") || CoronaryTotalCholesterol.equals("0.0"))
                    && (Fev1Predicted == null || Fev1Predicted.equals("") || Fev1Predicted.equals("null") || Fev1Predicted.equals("0.0"))
                    && (Fev1FVC == null || Fev1FVC.equals("") || Fev1FVC.equals("null") || Fev1FVC.equals("0.0"))
                    && (Dyspnea == null || Dyspnea.equals("") || Dyspnea.equals("null") || Dyspnea.equals("0.0"))
                    && (COPDdetail == null || COPDdetail.equals("") || COPDdetail.equals("null") || COPDdetail.equals("0.0"))){
                emptyCOPD = true;
                
                System.out.println("emptyCOPD is EMPTY");
            }
            else
            {
                
                System.out.println("emptyCOPD NOT EMPTY");
                
            }
            
            boolean emptyDepresion = false;
            String HamdScore = String.valueOf(session.getAttribute("hamdScore_text"));
            String DepressionDetail = String.valueOf(session.getAttribute("depressionDetail_text"));
            if((HamdScore == null || HamdScore.equals("") || HamdScore.equals("null") || HamdScore.equals("0.0")) && (DepressionDetail == null || DepressionDetail.equals("") || DepressionDetail.equals("null") || DepressionDetail.equals("0.0"))){
                emptyDepresion = true;
                
                System.out.println("emptyDepresion is EMPTY");
            }
            else
            {
                
                System.out.println("emptyDepresion NOT EMPTY");
                
            }
            
            boolean emptyOther = false;
            String otherDetails = String.valueOf(session.getAttribute("otherDetails_text"));
            if((otherDetails == null || otherDetails.equals("") || otherDetails.equals("") || otherDetails.equals("null") || otherDetails.equals("0.0"))){
                emptyOther = true;
                
                System.out.println("emptyOther is EMPTY");
            }
            else
            {
                
                System.out.println("emptyOther NOT EMPTY");
                
            }
            
            for (int i = 0; i < retObject.size(); i++) {
                
            if (retObject.get(i).getCD().equals("Asthma Biometric ICD") && emptyAsthma != true) {
                String asthmaICDlookupValue = retObject.get(i).getLookupValue();
                session.setAttribute("asthmaLookupValue", asthmaICDlookupValue);
                 
                CaptureAsthmaBiometricsCommand asthma = new CaptureAsthmaBiometricsCommand();
                asthma.execute(request, response, context);
           }
            if(client != null && client.equalsIgnoreCase("Sechaba")){
                if (retObject.get(i).getCD().equals("Hypertension Biometric ICD")) {
                    String hypertensionICDlookupValue = retObject.get(i).getLookupValue();
                    session.setAttribute("hypertensionLookupValue", hypertensionICDlookupValue);
                    CaptureHypertensionBiometricsCommand hypertension = new CaptureHypertensionBiometricsCommand();
                    hypertension.execute(request, response, context);
                } 
            }else{
                if (retObject.get(i).getCD().equals("Hypertension Biometric ICD") && emptyHypertension != true) {
                    String hypertensionICDlookupValue = retObject.get(i).getLookupValue();
                    session.setAttribute("hypertensionLookupValue", hypertensionICDlookupValue);
                    CaptureHypertensionBiometricsCommand hypertension = new CaptureHypertensionBiometricsCommand();
                    hypertension.execute(request, response, context);
                }
            }
            
            if(retObject.get(i).getCD().equals("Renal Biometric ICD") && emptyRenal != true ){
                String chronicICDlookupValue = retObject.get(i).getLookupValue();
                session.setAttribute("chronicLookupValue", chronicICDlookupValue);
                 
                CaptureChronicBiometricsCommand Chronic = new CaptureChronicBiometricsCommand();
                Chronic.execute(request, response, context);
            }
            if(retObject.get(i).getCD().equals("Cardiac Biometric ICD") && emptyCardiac != true){
                String cardiacICDlookupValue = retObject.get(i).getLookupValue();
                session.setAttribute("cardiacLookupValue", cardiacICDlookupValue);
                 
                CaptureCardiacBiometrics Cardiac = new CaptureCardiacBiometrics();
                Cardiac.execute(request, response, context);
            }
            if(retObject.get(i).getCD().equals("Diabetes Biometric ICD") && emptyDiabetes != true){
                String diabetesICDlookupValue = retObject.get(i).getLookupValue();
                session.setAttribute("diabetesLookupValue", diabetesICDlookupValue);
                 
                CaptureDiabetesBiometericsCommand Diabetes = new CaptureDiabetesBiometericsCommand();
                Diabetes.execute(request, response, context);
            }
            if(retObject.get(i).getCD().equals("Coronary Biometric ICD") && emptyCoronary != true){
                String coronaryICDlookupValue = retObject.get(i).getLookupValue();
                session.setAttribute("coronaryLookupValue", coronaryICDlookupValue);
                 
                CaptureCoronaryDiseaseCommand Coronary = new CaptureCoronaryDiseaseCommand();
                Coronary.execute(request, response, context);
            }
            if(retObject.get(i).getCD().equals("COPD Biometric ICD") && emptyCOPD != true){
                String COPDICDlookupValue = retObject.get(i).getLookupValue();
                session.setAttribute("COPDLookupValue", COPDICDlookupValue);
                 
                CaptureCOPDBiometricsCommand COPD = new CaptureCOPDBiometricsCommand();
                COPD.execute(request, response, context);
            }
            if(retObject.get(i).getCD().equals("Hyperlipidaemia Biometric ICD") && emptyHyperlipidaemia != true){
                String hyperlipidaemialookupValue = retObject.get(i).getLookupValue();
                session.setAttribute("hyperlipidaemiaLookupValue", hyperlipidaemialookupValue);
                
                CaptureHyperlipidaemiaBiometricsCommand Hyperlip = new CaptureHyperlipidaemiaBiometricsCommand();
                Hyperlip.execute(request, response, context);
                 
            }
            if(retObject.get(i).getCD().equals("Depression Biometric ICD") && emptyDepresion != true){
                String depressionlookupValue = retObject.get(i).getLookupValue();
                session.setAttribute("depressionLookupValue", depressionlookupValue);
                
                CaptureMajorDepressionCommand Hyperlip = new CaptureMajorDepressionCommand();
                Hyperlip.execute(request, response, context);
                
            }
            if(emptyOther != true || (client != null && client.equalsIgnoreCase("Sechaba"))){
                String otherlookupValue = retObject.get(i).getBlCDLvalue();
                String othericd = retObject.get(i).getLookupValue();
                session.setAttribute("otherlookupValue", otherlookupValue);
                session.setAttribute("othericd", othericd);
                
                CaptureOtherCommand other = new CaptureOtherCommand();
                other.execute(request, response, context); 
            }
            
            if(client != null && !client.equalsIgnoreCase("Sechaba")){
                if (emptyAsthma == true && emptyRenal == true && emptyCardiac == true
                        && emptyDiabetes == true && emptyCoronary == true && emptyCOPD == true
                        && emptyHyperlipidaemia == true && emptyDepresion == true && emptyOther == true) {
                    session.setAttribute("icd10_error", "Please enter some values into the CDL");
                    ForwardToEditBiometricGeneralInfoCommand forward = new ForwardToEditBiometricGeneralInfoCommand();
                    forward.execute(request, response, context);
                }
            }
            
        }
            
        }catch(Exception e){
            if(retObject == null){
                session.setAttribute("icd10_error", "Mandatory field");
                ForwardToEditBiometricGeneralInfoCommand forward = new ForwardToEditBiometricGeneralInfoCommand();
                forward.execute(request, response, context);
            }else{
                session.setAttribute("icd10_error", "error");
                ForwardToEditBiometricGeneralInfoCommand forward = new ForwardToEditBiometricGeneralInfoCommand();
                forward.execute(request, response, context);
                e.printStackTrace();
            }
            
        }
/*        }

        //clearAllFromSession(request);

        //CopdBiometrics copdBiometrics = new CopdBiometrics();
        //copdBiometrics.setBiometricTypeId("7");
*/

        return null;
    }

    @Override
    public String getName() {
        return "CaptureBiometricsCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import com.koh.utils.MapUtils;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.KeyValueArray;
import neo.manager.KeyValue;
import neo.manager.NeoManagerBean;

/**
 * @author yuganp
 */
public class MemberAppAuditCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        NeoManagerBean port = service.getNeoManagerBeanPort();

        try {
            String name = request.getParameter("auditList");
            String entityId = request.getParameter("applicationNumber");
            //System.out.println("entitiTID : " + entityId);
            int id = 0;
            try {
                id = Integer.parseInt(entityId);
            } catch (Exception e) {
                Logger.getLogger(MemberAppAuditCommand.class.getName()).log(Level.SEVERE, "Failed to parse entityID");
            }
            List<KeyValueArray> kva = port.getAuditTrail(name, id);
            for (KeyValueArray kVa : kva) {
                List<KeyValue> kVl = kVa.getKeyValList();
                for (KeyValue kv : kVl) {
                    System.out.println("key : " + kv.getKey());
                    System.out.println("value : " + kv.getValue());
                }
            }
            Object col = MapUtils.getMap(kva);
            request.setAttribute("MemberAppAuditDetails", col);
            context.getRequestDispatcher("/MemberApplication/MemberAppAuditDetails.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(MemberAppAuditCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "MemberAppAuditCommand";
    }
}

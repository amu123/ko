/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.Command;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author josephm
 */
public class RemoveAllFromSession extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("Attempting to clear the session...");
        try {
            clearAllFromSession(request);

        }catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;

    }

    @Override
    public String getName() {
        return "RemoveAllFromSession";
    }
}

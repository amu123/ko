/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application;

import com.koh.employer.command.TabUtils;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import neo.manager.MemAppGenDetail;
import neo.manager.MemberApplication;
import neo.manager.MemberDependantApp;
import neo.manager.NeoUser;

/**
 *
 * @author yuganp
 */
public class MemberAppGeneralDetailsMapping {

    private static final String[][] FIELD_MAPPINGS = {
        {"memberAppNumber","Application Number","no", "int", "MemberApp","ApplicationNumber"},
        {"memberAppGenId","General Id","no", "int", "MemberApp","generalDetailId"},
        {"General_detail_question","Question","yes", "int", "MemberApp","questionNumber"},
        {"General_detail_dependent","Dependant","yes", "int", "MemberApp","dependentNumber"},
        {"General_detail_details","Details","yes", "str", "MemberApp","details"}
    };
    
    private static final Map<String,String[]> FIELD_MAP;

    static {
        HashMap hm = new HashMap<String,String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }

    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
        return errors;
    }

    public static MemAppGenDetail getMemAppGenDetail(HttpServletRequest request,NeoUser neoUser) {
        MemAppGenDetail ma = (MemAppGenDetail)TabUtils.getDTOFromRequest(request, MemAppGenDetail.class, FIELD_MAPPINGS, "MemberApp");
        ma.setSecurityGroupId(neoUser.getSecurityGroupId());
        ma.setLastUpdatedBy(neoUser.getUserId());
        ma.setCreatedBy(neoUser.getUserId());
        return ma;
    }

    public static void setMemAppGenDetail(HttpServletRequest request, MemAppGenDetail memAppGenDetail) {
        TabUtils.setRequestFromDTO(request, memAppGenDetail, FIELD_MAPPINGS, "MemberApp");
    }
    
}

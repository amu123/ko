/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author gerritj
 */
public class RejectUserCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        int user_id = new Integer(request.getParameter("user_id"));
        int workitem_id = new Integer(request.getParameter("workitem_id"));

        service.getNeoManagerBeanPort().rejectUser(user_id, workitem_id);
        try {
            request.setAttribute("success", "User rejected");
            String nextJSP = "/SystemAdmin/ActivateUserWL.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "RejectUserCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.command;

import com.agile.security.webservice.AgileManagerService;
import java.net.URL;
import javax.xml.namespace.QName;


/**
 *
 * @author gerritj
 * Base command for all security and fe related webservice processing
 */
public abstract class FECommand extends Command {

    //Port to security web service, can be used by all commands extending from this command
    public static AgileManagerService service = null;
  
    static{
        getWebService();
    }

    /**
     * Build the static webservice port
     */
    private static void getWebService() {
        try {
            String WS_URL_TriggerSoapHttpPort = "http://localhost:9494/ManagedCareBackend/AgileManager?wsdl";
            String WS_NAMESPACE_TriggerSoapHttpPort = "http://webservice.security.agile.com/";
            String WS_SERVICE_TriggerSoapHttpPort = "AgileManagerService";
            URL wsUrl = new URL(WS_URL_TriggerSoapHttpPort);
            QName wsQName = new QName(WS_NAMESPACE_TriggerSoapHttpPort, WS_SERVICE_TriggerSoapHttpPort);
            AgileManagerService services = new AgileManagerService(wsUrl, wsQName);
            service = services;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.calltrack.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CallTrackDetails;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class UpdateCallTrackingCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        //NeoUser user = (NeoUser)session.getAttribute("persist_user");
        //String in = request.getParameter("trackIdUpdate");
        String in = request.getParameter("trackId");
        int index = new Integer(in);
        HttpServletRequest ReturnRequest = request;
        NeoManagerBean port = service.getNeoManagerBeanPort();

        if (in != null) {
            CallTrackDetails call = service.getNeoManagerBeanPort().getCallByTrackID(index);

            String memberNumber = call.getCoverNumber();
            CoverDetails c = port.getEAuthResignedMembers(memberNumber);

            session.setAttribute("productId", c.getProductId());
            session.setAttribute("callType", call.getCallType());
            session.setAttribute("callTrackNumber", call.getCallReference());
            session.setAttribute("callStatus", call.getCallStatusId());
            session.setAttribute("refUser", call.getReferedUserId());
            session.setAttribute("callStartDate", call.getCallStartDate());
            session.setAttribute("callEndDate", call.getCallEndDate());
            session.setAttribute("memberNumber_text", memberNumber);
            session.setAttribute("providerNumber_text", call.getProviderNumber());
            session.setAttribute("provName", call.getProviderNameSurname());
            session.setAttribute("discipline", call.getProviderDiscipline());
            session.setAttribute("callerName", call.getCallerName());
            session.setAttribute("callerContact", call.getContactDetails());
            session.setAttribute("notes", call.getNotes());
            session.setAttribute("callTrackingNotes", call.getNotes());
            // session variable for the provider
            //session.setAttribute("memNo", call.getCoverNumber());

            // setting the call queries in the session
            session.setAttribute("callQueries1", call.getQueries());
            session.setAttribute("callQueries2", call.getCallQuery());

            //System.out.println("The queries are1 " + session.getAttribute("callQueries1"));
            // System.out.println("The queries are2 " + session.getAttribute("callQueries2"));

            session.setAttribute("trackId", call.getCallTrackId());

            //int userId = user.getUserId();
            // System.out.println("The value of the idesx here before calling update is " + index);
            // System.out.println("The notte s are  " + call.getNotes());
            //port.updateCallTrackDetails(userId, index, call);

            //String memNo = "" + session.getAttribute("memberNumber_text");
            //String callType = "" + session.getAttribute("callType");
            // System.out.println("Member Number recieved from auth screen = " + memNo);

            //request.setAttribute("memberNumber_text", call.getCoverNumber());

            //force member search
            if (call.getCoverNumber() != null && !call.getCoverNumber().trim().equalsIgnoreCase("")
                    && !call.getCoverNumber().trim().equalsIgnoreCase("0")) {
                ReturnRequest = (HttpServletRequest) new SearchCallCoverByUpdateCommand().execute(request, response, context);
            }

        } else {

            session.removeAttribute("callType");
            session.removeAttribute("callTrackNumber");
            session.removeAttribute("callStatus");
            session.removeAttribute("refUser");
            session.removeAttribute("memberNumber_text");
            session.removeAttribute("providerNumber_text");
            session.removeAttribute("provName");
            session.removeAttribute("discipline");
            session.removeAttribute("callerName");
            session.removeAttribute("callerContact");
            session.removeAttribute("notes");
            session.removeAttribute("memNo");
            session.removeAttribute("callQueries1");
            session.removeAttribute("callQueries2");
        }

        try {
            String nextJSP = "/Calltrack/UpdateCall.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(ReturnRequest, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "UpdateCallTrackingCommand";
    }
}

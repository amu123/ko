/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import java.util.logging.Logger;
import com.agile.security.webservice.EmailAttachment;
import com.agile.security.webservice.EmailContent;
import com.koh.command.FECommand;
import static com.koh.command.FECommand.service;
import com.koh.serv.PropertiesReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.logging.Level;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author shimanem
 */
public class PDCDocumentDisplayViewCommand extends FECommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
        System.out.println(">>- PDC DOCUMENT VIEW/DISPLAY COMMAND -<<");
        try {
            String actionType = request.getParameter("status");
            String memberNumber = (String) session.getAttribute("policyNumber");
            String emailTo = request.getParameter("memberEmail");
            String fileName = request.getParameter("fileLocation");
            String indexDrive = new PropertiesReader().getProperty("DocumentIndexIndexDrive");
            int productId = new Integer(String.valueOf(session.getAttribute("productId")));
            boolean emailSent = false;
            session.setAttribute("email", emailTo);

            System.out.println("MEMBER #: " + memberNumber);
            System.out.println("FILE LOCATION : " + fileName);
            System.out.println("PATH : " + indexDrive + fileName);
            if (fileName == null || fileName.equals("")) {
                printErrorResult(request, response);
                return null;
            }
            File file = new File(indexDrive + fileName);
            InputStream in = new FileInputStream(file);
            int contentLength = (int) file.length();

            System.out.println("CONTENT LENGTH : " + contentLength);

            ByteArrayOutputStream temporaryOutput;
            if (contentLength != -1) {
                temporaryOutput = new ByteArrayOutputStream(contentLength);
            } else {
                temporaryOutput = new ByteArrayOutputStream(20480);
            }

            byte[] bufer = new byte[512];
            while (true) {

                int len = in.read(bufer);
                if (len == -1) {

                    break;
                }
                temporaryOutput.write(bufer, 0, len);
            }
            in.close();
            temporaryOutput.close();

            byte[] array = temporaryOutput.toByteArray();
            //printPreview(request, response, array, fileLocation);

            if (actionType != null && actionType.equalsIgnoreCase("Preview")) {
                printPreview(request, response, array, fileName);
            } else if (emailTo != null && !emailTo.equalsIgnoreCase("")) {
                String emailFrom = "Noreply@agilityghs.co.za";
                String subject = session.getAttribute("productName") + "  - Benefits For Member " + memberNumber;

                String emailMsg = null;

                EmailContent content = new EmailContent();
                content.setContentType("text/plain");
                EmailAttachment attachment = new EmailAttachment();
                attachment.setContentType("application/octet-stream");
                attachment.setName(fileName);
                content.setFirstAttachment(attachment);
                content.setFirstAttachmentData(array);
                content.setSubject(subject);
                content.setEmailAddressFrom(emailFrom);
                content.setEmailAddressTo(emailTo);
                content.setProductId(productId);
                //Determine msg for email according to type of document ('claim', 'statement', 'preAuth', 'benefit' 'custom', 'other')
                content.setType("benefit");
                /*If document type is custom, this message added here will be displayed in the email. The email method will automatically add the 
                 the enquiries and call center aswell as the kind regards at the end of the message*/
                content.setContent(emailMsg);
                emailSent = service.getAgileManagerPort().sendEmailWithOrWithoutAttachment(content);
                System.out.println("EMAIL SENT TO : " + emailSent);
                printEmailResult(request, response, emailSent);
            } else {
                printEmailResult(request, response, emailSent);
            }

        } catch (IOException ex) {
            Logger.getLogger(PDCDocumentDisplayViewCommand.class.getName()).log(Level.SEVERE, null, ex);
            printErrorResult(request, response);
        }
        return null;
    }

    private void printErrorResult(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");

            out.println("<td><label class=\"header\">No records found.</label></td>");

            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "2; URL=/ManagedCare/PDC/PDCDocumentView.jsp");

        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    public void printPreview(HttpServletRequest request, HttpServletResponse response, byte[] array, String fileName) throws IOException {
        OutputStream outStream = null;
        try {
            response.setHeader("Pragma", "no-cache"); //HTTP 1.0
            response.setDateHeader("Expires", 0); //prevents caching at the proxy server
            response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-disposition", "attachment; filename=" + fileName);

            response.setContentType("application/pdf");
            response.setContentLength(array.length);
            outStream = response.getOutputStream();
            outStream.write(array);
            outStream.flush();
            outStream.close();

        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void printEmailResult(HttpServletRequest request, HttpServletResponse response, boolean result) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            if (result) {
                out.println("<td><label class=\"header\">Benefit Letter email Sent Successfully!.</label></td>");
            } else {
                out.println("<td><label class=\"header\">Email sending failed, Either email address is missing or incorrect.</label></td>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "3; URL=/ManagedCare/PDC/PDCDocumentView.jsp");

        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    @Override
    public String getName() {
        return "PDCDocumentDisplayViewCommand";
    }
}

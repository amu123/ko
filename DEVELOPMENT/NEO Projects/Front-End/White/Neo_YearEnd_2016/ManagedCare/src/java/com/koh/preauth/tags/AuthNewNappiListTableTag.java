/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.tags;

import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author Johan-NB
 */
public class AuthNewNappiListTableTag extends TagSupport {
    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();

        try {
            out.println("<td colspan=\"6\"><table width=\"500\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            ArrayList<AuthTariffDetails> atList = (ArrayList<AuthTariffDetails>) session.getAttribute(sessionAttribute);
            if (atList != null) {
                out.println("<tr><th>Nappi Code</th><th>Nappi Discription</th><th></th><th></th></tr>");
                for (int i =1; i <= atList.size(); i++){
                    AuthTariffDetails at = atList.get(i-1);

                    out.println("<tr id=\"newNappiRow"+ at.getTariffCode() + i +"\"><form>");
                    out.println("<td><label class=\"label\">" + at.getTariffCode() + "</label><input type=\"hidden\" name=\"nTableCode\" id=\"nTableCode\" value=\"" + at.getTariffCode() + "\"/></td>");
                    out.println("<td width=\"200\"><label class=\"label\">" + at.getTariffDesc() + "</label><input type=\"hidden\" name=\"nTableDesc\" id=\"nTableDesc\" value=\"" + at.getTariffDesc() + "\"/></td>");
                    out.println("<td><button type=\"button\" onClick=\"ModifyNewNappiFromList(" + i + ");\" >Modify</button></td>");
                    out.println("<td><button type=\"button\" onClick=\"RemoveNewNappiFromList(" + i + ");\" >Remove</button></td>");
                    out.println("</form></tr>");
                }
            }
            out.println("</table></td>");


        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberSearchTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CommunicationLog;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;
import org.apache.log4j.Logger;

/**
 *
 * @author marcelp
 */
public class ForwardToMemberCommunicationCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(ForwardToMemberClaimsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();

        logger.info("Inside ForwardToMemberCommunicationCommand");

        try {
            getMemberCommLog(session, request);
            String nextJSP = "/Claims/CustomerCareMemberCommunication.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void getMemberCommLog(HttpSession session, HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        logger.info("ForwardToMemberCommunicationCommand - getMemberCommunication()");
        String coverNum = (String) session.getAttribute("policyHolderNumber");
        logger.info("coverNum : " + coverNum);
        request.setAttribute("coverNum", coverNum);
        CoverDetails c = port.getEAuthResignedMembers(coverNum);
        int entityId = c.getEntityId();
        logger.info("entityId : " + entityId);
        List<CommunicationLog> commLogForEntityId = port.getCommLogForEntityId(entityId, null, null, null);
        request.setAttribute("communicationsLog", commLogForEntityId);
    }
    
    @Override
    public String getName() {
        return "ForwardToMemberCommunicationCommand";
    }
}

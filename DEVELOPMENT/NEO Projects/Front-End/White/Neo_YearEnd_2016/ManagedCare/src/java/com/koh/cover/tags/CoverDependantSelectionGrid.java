/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.tags;

import com.koh.command.NeoCommand;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author johanl
 */
public class CoverDependantSelectionGrid extends TagSupport {

    private String commandName;
    private String javaScript;
    private String sessionAttribute;
    private String onScreen;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        ServletContext sctx = pageContext.getServletContext();
        String client = String.valueOf(sctx.getAttribute("Client"));

        try {
            List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute(sessionAttribute);
            HashMap<Integer, Integer> depList = new HashMap<Integer, Integer>();
            HashMap<Integer, String> depListCreationDate = new HashMap<Integer, String>();
            HashMap<Integer, String> depListJoinDate = new HashMap<Integer, String>();
            HashMap<Integer, String> depListJoinDateByCode = new HashMap<Integer, String>();
            HashMap<Integer, String> depListStatus = new HashMap<Integer, String>();
            String coverStatus = null;
            String depStatus = null;
            String resignedDate = "";
            session.setAttribute("coverStatus", null);
            session.setAttribute("depStatus", null);
            session.setAttribute("suspendMember", null);
            if (cdList != null && cdList.isEmpty() == false) {
                //get dependant 
                int depCode = -1;
                if (session.getAttribute("depListValues") != null) {
                    String depCodeStr = "" + session.getAttribute("depListValues");
                    System.out.println("Member search grid depCodeStr = " + depCodeStr);
                    if (depCodeStr != null && !depCodeStr.equalsIgnoreCase("") && !depCodeStr.equalsIgnoreCase("null")) {
                        depCode = Integer.parseInt(depCodeStr);
                    }
                }

                out.println("<table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.println("<tr>"
                        + "<th align=\"left\">Option</th>"
                        + "<th align=\"left\">Code</th>"
                        + "<th align=\"left\">First Name</th>"
                        + "<th align=\"left\">Surname</th>"
                        + "<th align=\"left\">Dependant Type</th>"
                        + "<th align=\"left\">Date Of Birth</th>"
                        + "<th align=\"left\">Gender</th>"
                        + "<th align=\"left\">Id Number</th>"
                        + "<th align=\"left\">Status</th>"
                        + "<th align=\"left\">Join Date</th>"
                        + "<th align=\"left\">Received Date</th>"
                        + "<th align=\"left\">Option Date</th>"
                        + "<th align=\"left\">Resigned Date</th>"
                        + "<th align=\"left\"></th>"
                        + "</tr>");

                boolean showResignBTN = false;
                for (CoverDetails cd : cdList) {
                    //set cover details 
                    String dateOfBirth = "";
                    String joinDate = "";
                    String receivedDate = "";
                    String benefitDate = "";
                    String resignDate = "";
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                    String cssClass = "label";
                    int entityId = cd.getEntityId();


                    System.out.println("cover: " + cd.getCoverNumber() + " dep:" + cd.getDependentNumber() + " eId = " + cd.getEntityId() + " : eIdCom " + cd.getEntityCommonId());
                    depList.put(cd.getDependentNumber(), cd.getEntityId());
                    depListCreationDate.put(cd.getDependentNumber(), new SimpleDateFormat("yyyy/MM/dd").format(cd.getCreationDate().toGregorianCalendar().getTime()));
                    depListJoinDate.put(cd.getDependentTypeId(), new SimpleDateFormat("yyyy/MM/dd").format(cd.getSchemeJoinDate().toGregorianCalendar().getTime()));
                    depListJoinDateByCode.put(cd.getDependentNumber(), new SimpleDateFormat("yyyy/MM/dd").format(cd.getSchemeJoinDate().toGregorianCalendar().getTime()));
                    depListStatus.put(cd.getDependentNumber(), cd.getStatus());
                    
                    if (cd.getCoverEndDate() != null){
                        String covEndDate = new SimpleDateFormat("yyyy/MM/dd").format(cd.getCoverEndDate().toGregorianCalendar().getTime());
                        if (covEndDate.equalsIgnoreCase("2999/12/31")){
                            showResignBTN = true;
                        }
                    }

                    if (cd.getDependentTypeId() == 17) {
                        coverStatus = cd.getStatus();
                        session.setAttribute("coverStatus", cd.getStatus());
                        session.setAttribute("principalStatus", cd.getStatus());
                        session.setAttribute("memberEntityID", cd.getEntityId());
                        session.setAttribute("coverNumber", cd.getCoverNumber());
                        System.out.println("Status " + coverStatus);
                        if (cd.getCoverEndDate() != null) {
                            String dateTo = new SimpleDateFormat("yyyy/MM/dd").format(cd.getCoverEndDate().toGregorianCalendar().getTime());
                            if (!dateTo.equalsIgnoreCase("2999/12/31")) {
                                resignDate = dateTo;
                                session.setAttribute("coverStatus", "both");
                                System.out.println("Status both: Resigned Member");
                            }

                            if (cd.getStatus().equalsIgnoreCase("Suspend")) {
                                depStatus = cd.getStatus();
                                session.setAttribute("suspendMember", cd.getStatus());
                                System.out.println("Suspend " + depStatus);
                            }

                        }
                    } else if (cd.getStatus().equalsIgnoreCase("Resign")) {
                        depStatus = cd.getStatus();
                        session.setAttribute("depStatus", cd.getStatus());
                        System.out.println("Resign " + depStatus);

                    } else if (cd.getCoverEndDate() != null) {
                        String dateTo = new SimpleDateFormat("yyyy/MM/dd").format(cd.getCoverEndDate().toGregorianCalendar().getTime());
                        if (!dateTo.equalsIgnoreCase("2999/12/31")) {
                            resignDate = dateTo;
                            session.setAttribute("coverStatus", "both");
                            System.out.println("Status both: Resigned Dependant");
                        }
                    }



                    //dob
                    if (cd.getDateOfBirth() != null) {
                        dateOfBirth = dateFormat.format(cd.getDateOfBirth().toGregorianCalendar().getTime());
                    }

                    //benefit start date
                    if (cd.getCoverStartDate() != null) {
                        benefitDate = dateFormat.format(cd.getCoverStartDate().toGregorianCalendar().getTime());
                    }

                    //join date
                    if (cd.getSchemeJoinDate() != null) {
                        joinDate = dateFormat.format(cd.getSchemeJoinDate().toGregorianCalendar().getTime());
                    } else {
                        joinDate = dateFormat.format(cd.getCoverStartDate().toGregorianCalendar().getTime());
                    }

                    //received date
                    System.out.println("Received Date " + cd.getApplicationRecievedDate());
                    if (cd.getApplicationRecievedDate() != null) {
                        receivedDate = dateFormat.format(cd.getApplicationRecievedDate().toGregorianCalendar().getTime());
                    }

                    //resign date
                    if (cd.getCoverEndDate() != null) {
                        String dateTo = new SimpleDateFormat("yyyy/MM/dd").format(cd.getCoverEndDate().toGregorianCalendar().getTime());
                        if (!dateTo.equalsIgnoreCase("2999/12/31")) {
                            resignDate = dateTo;
                        }

                    }
                    String rowColor = "<tr>";
                    if(cd.getStatus().equalsIgnoreCase("Suspend") && client.equalsIgnoreCase("Sechaba")){
                        rowColor = "<tr style=\"background-color: #ff4d4d\">";
                    }
                    out.println(rowColor
                            + "<td><label class=\"label\">" + cd.getOptionName() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getDependentNumber() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getName() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getSurname() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getDependentType() + "</label></td>"
                            + "<td><label class=\"label\">" + dateOfBirth + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getGender() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getIdNumber() + "</label></td>"
                            + "<td><label class=\"label\">" + cd.getStatus() + "</label></td>"
                            + "<td><label class=\"label\">" + joinDate + "</label></td>"
                            + "<td><label class=\"label\">" + receivedDate + "</label></td>"
                            + "<td><label class=\"label\">" + benefitDate + "</label></td>"
                            + "<td><label class=\"label\">" + resignDate + "</label></td>");

                    if (javaScript != null && !javaScript.equalsIgnoreCase("")) {

                        if (javaScript.equalsIgnoreCase("memberDepSelect")) {
                            out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitCoverDeps('" + cd.getCoverNumber() + "','" + cd.getEntityId() + "','" + cd.getDependentTypeId() + "','" + cd.getDependentNumber() + "','" + onScreen + "','" + cd.getOptionId() + "','" + cd.getEntityCommonId() + "')\" >Details</button></td>");
                        } else {
                            out.println("<td><button name=\"opperation\" type=\"button\" " + javaScript + " >Details</button></td>");
                        }

                    }
                    out.println("</tr>");
                }
                out.println("</table>");
                session.setAttribute("CanResign", showResignBTN);
            }
            System.out.println(coverStatus + " Both " + depStatus);
            if ((coverStatus != null && coverStatus.equalsIgnoreCase("Active")) && (depStatus != null && depStatus.equalsIgnoreCase("Resign"))) {
                coverStatus = "both";
                session.setAttribute("coverStatus", coverStatus);
                System.out.println("Both");
            }

            System.out.println("Cover Status " + coverStatus);
            session.setAttribute("depEntity", depList);
            session.setAttribute("depCreationDate", depListCreationDate);
            session.setAttribute("depListJoinDate", depListJoinDate);
            session.setAttribute("depListJoinDateByCode", depListJoinDateByCode);
            session.setAttribute("depListStatus", depListStatus);

        } catch (java.io.IOException ex) {
            throw new JspException("Error in PreAuthMemberSearchGrid tag", ex);
        }

        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setOnScreen(String onScreen) {
        this.onScreen = onScreen;
    }
}

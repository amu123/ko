/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import com.koh.command.NeoCommand;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.Claim;
import neo.manager.ClaimIcd10;
import neo.manager.ClaimLine;
import neo.manager.ClaimLineBenefit;
import neo.manager.CoverProductDetails;
import neo.manager.Drug;
import neo.manager.Ndc;
import neo.manager.NeoManagerBean;
import neo.manager.ProviderDetails;
import neo.manager.TariffCode;

/**
 *
 * @author princes
 */
public class ClaimsHistoryDetailTabel extends TagSupport {

    private static final long serialVersionUID = 1L;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        System.out.println("entered ClaimsHistoryDetailTabel");
//        String coverNumber = (String) session.getAttribute("policyNumber");
        Integer depenNo = (Integer) session.getAttribute("dependantNumber");
        Claim selectedClaim = (Claim) session.getAttribute("selected");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        String serviceDateFrom;
        String paymentDaye;
        List<String> list = new ArrayList<String>();

        try {
            out.print("<tr>");
            out.print("<th scope=\"col\">ClaimLine Number</th>");
            out.print("<th scope=\"col\">Dependant Code</th>");
            out.print("<th scope=\"col\">ICD10 Code</th>");
            out.print("<th scope=\"col\">Nappi Code</th>");
            out.print("<th scope=\"col\">Nappi Description</th>");
            out.print("<th scope=\"col\">Tarriff Code</th>");
            out.print("<th scope=\"col\">Tarriff Description</th>");
            out.print("<th scope=\"col\">Treatment Date</th>");
            out.print("<th scope=\"col\">Pay Status</th>");
            out.print("<th scope=\"col\">Amount Claimed</th>");
            out.print("<th scope=\"col\">Amount Paid</th>");
            out.print("<th scope=\"col\">Payment Date</th>");
            out.print("</tr>");
            if (selectedClaim != null) {
                List<ClaimLine> batchsub = port.fetchClaimLinesDetailsById(selectedClaim.getClaimLineIds());

                for (ClaimLine claimLine : batchsub) {

                    Collection<ClaimLineBenefit> clb = port.fetchClaimLineBenefit(claimLine.getClaimLineId());
                    double benifitTotalPaid = 0.00;
                    for (ClaimLineBenefit claimLineBenefit : clb) {
                        benifitTotalPaid = benifitTotalPaid + claimLineBenefit.getAmmountPaid();
                    }

                    String icds = null;
                    // String[] list = new String[12];
                    int count = 0;
                    for (ClaimIcd10 icd : claimLine.getIcd10()) {
                        icds = icd.getIcd10Code();
                        list.add(icds);
                        count++;
                    }

                    ProviderDetails proDet = port.getProviderDetailsForEntityByNumber(selectedClaim.getServiceProviderNo());

                    CoverProductDetails cpd = port.getCoverProductDetailsByCoverNumber(selectedClaim.getCoverNumber());
                    TariffCode t = port.getTariffCode(cpd.getProductId(), cpd.getOptionId(), claimLine.getServiceDateFrom(), proDet.getDisciplineType().getId(), proDet.getPracticeNumber(), proDet.getProviderNumber(), list, null, claimLine.getTarrifCodeNo());
                    //Treatment t = port.getTreatmentForResoCodeForCover(proDet.getDisciplineType().getId(), new Integer(claimLine.getTarrifCodeNo()), cpd.getOptionName(), cpd.getCoverNumber(), claimLine.getInsuredPersonDependentCode(), claimLine.getServiceDateFrom());

                    Integer status = claimLine.getClaimLineStatusId();

                    out.print("<tr>");
                    out.print("<td>" + claimLine.getClaimLineId() + "</td>");
                    out.print("<td>" + depenNo + "</td>");

                    if (icds != null) {
                        out.print("<td>" + icds + "</td>");
                    } else {
                        out.print("<td></td>");
                    }
                    Ndc ndc = new Ndc();
                    if (claimLine.getNdc() != null && !claimLine.getNdc().isEmpty()) {
                        ArrayList<Ndc> ndcs = (ArrayList<Ndc>) claimLine.getNdc();
                        for (Ndc ndc1 : ndcs) {
                            if (ndc1.getPrimaryNdcIndicator() == 1) {
                                ndc = ndc1;
                                break;
                            }
                        }
                    }
                    if (ndc.getNdcCode() != null) {
                        Drug d = port.getDrugForCode(ndc.getNdcCode(), proDet.getDisciplineType().getId());
                        out.print("<td>" + d.getCode() + "</td>");
                        out.print("<td>" + d.getDescription() + "</td>");
                    } else {
                        out.print("<td></td>");
                        out.print("<td></td>");
                    }
                    if (t != null) {
                        out.print("<td>" + t.getCode() + "</td>");
                        out.print("<td>" + t.getDescription() + "</td>");
                    } else {
                        out.print("<td></td>");
                        out.print("<td></td>");
                    }
                    Date serviceDate = claimLine.getServiceDateFrom().toGregorianCalendar().getTime();
                    serviceDateFrom = formatter.format(serviceDate);
                    out.print("<td>" + serviceDateFrom + "</td>");
                    out.print("<td>" + port.getValueForId(83, status.toString()) + "</td>");
                    out.print("<td>" + claimLine.getClaimedAmount() + "</td>");
                    out.print("<td>" + benifitTotalPaid + "</td>");
                    if (claimLine.getChequeRunDate() != null) {
                        Date payDate = claimLine.getServiceDateFrom().toGregorianCalendar().getTime();
                        paymentDaye = formatter.format(payDate);
                        out.print("<td>" + paymentDaye + "</td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("</tr>");

                }
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.brokerFirm;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;

/**
 *
 * @author chrisdp
 */
public class ChangePayIndicatorCommand extends NeoCommand {
  
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        //HttpSession session = request.getSession();
        try {
            viewChangeIndicator(port, request, response, context);
        } catch (ServletException ex) {
            Logger.getLogger(ChangePayIndicatorCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ChangePayIndicatorCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ChangePayIndicatorCommand";
    }

    private void viewChangeIndicator(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        request.setAttribute("brokerFirmEntityId", request.getParameter("brokerFirmEntityId"));    
        request.setAttribute("FirmApp_CommisionInd", session.getAttribute("currentCommissionInd"));
 
        context.getRequestDispatcher("/Broker/ChangePayIndicator.jsp").forward(request, response);
    }
}

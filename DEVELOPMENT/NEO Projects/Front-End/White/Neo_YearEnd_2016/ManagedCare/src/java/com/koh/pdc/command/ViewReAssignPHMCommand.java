/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import neo.manager.HistoryList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author princes
 */
public class ViewReAssignPHMCommand extends NeoCommand {

    Logger logger = Logger.getLogger(ViewReAssignPHMCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        logger.info(">> Inside ViewReAssignPHMCommand <<");

        String workListID = "" + session.getAttribute("listIndex"); // loads from session in the WorkBench

        if (workListID != null && !workListID.equals("") && !workListID.equals("null")) {
            logger.log(Level.INFO, ">> PHM WorkListID [session] : {0}", workListID);
            session.setAttribute("listIndex", workListID);
            int intWorkListId = Integer.parseInt(workListID);
            List<HistoryList> retObject = service.getNeoManagerBeanPort().getAssignmentHistory(intWorkListId);
            session.setAttribute("assignHistory", retObject); //session name from tag file
        }        

        try {
            String nextJSP = "/PDC/AssignPHM.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewReAssignPHMCommand";
    }
}

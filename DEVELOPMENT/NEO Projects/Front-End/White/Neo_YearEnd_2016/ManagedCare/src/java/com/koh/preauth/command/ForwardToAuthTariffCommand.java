/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author johanl
 */
public class ForwardToAuthTariffCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();

        String onScreen = "" + session.getAttribute("onScreen");
        String nextJSP = "/PreAuth/AuthSpecificTariff.jsp";
        String authType = "" + session.getAttribute("authType");
        String provTypeId = "";
        boolean proceed = true;
        
        if (authType.trim().equals("11") || authType.trim().equals("4")) {
            provTypeId = "" + session.getAttribute("facilityProvDiscTypeId");
            if (provTypeId != null && !provTypeId.equalsIgnoreCase("null") && !provTypeId.equalsIgnoreCase("")) {
                proceed = true;
                session.setAttribute("facilityProv_error", null);
            } else {
                proceed = false;
                session.setAttribute("facilityProv_error", "Facility Provider Required");
                nextJSP = onScreen;
            }
        }

        //set hospital auth check
        if (proceed) {
            System.out.println("ForwardToAuthTariffCommand onScreen = " + onScreen);
            session.setAttribute("mapCPTForTariff", false);
            if (onScreen.startsWith("/PreAuth/HospitalAuthDetails") || onScreen.startsWith("/PreAuth/HospiceDetails")) {
                session.setAttribute("mapCPTForTariff", true);
            }
        }

        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ForwardToAuthTariffCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.FECommand;
import java.util.ArrayList;
import java.util.Collection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//import neo.manager.AuthHistoryDetails;

/**
 *
 * @author johanl
 */
public class AllocateAuthHistoryToSession extends FECommand {

    private String commandName;
    private String javascript;

    @Override
    @SuppressWarnings("static-access")
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
//        AuthHistoryDetails ahDetail = new AuthHistoryDetails();
//        HttpSession session = request.getSession();
//        Collection<AuthHistoryDetails> history = (ArrayList<AuthHistoryDetails>) session.getAttribute("authUpdateHistory");
//        String historyRecord = "" + request.getParameter("historyId");
//        System.out.println("selected hisID = " + historyRecord);
//        int historyId = Integer.parseInt(historyRecord);
//        if (history != null) {
//            System.out.println("history search result size = " + history.size());
//
//            for (AuthHistoryDetails ah : history) {
//                if (historyId == ah.getHistoryId()) {
//                    String xmlString = "" + ah.getXmlOutput();
//                    @SuppressWarnings("static-access")
//                    String htmlGeneratedFromXml = "";//port.generateHtmlAuthHistory(xmlString);
//                    //System.out.println("XML-HTML in tag = " + htmlGeneratedFromXml);
//                    ah.setXmlOutput(htmlGeneratedFromXml);
//                    ahDetail = ah;
//                    break;
//                }
//            }
//        }
//        session.setAttribute("fullAuthHistory", ahDetail);
//        try {
//            String nextJSP = "/Auth/AuthUpdateHistoryChanges.jsp";
//            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
//            dispatcher.forward(request, response);
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
        return null;
    }

    @Override
    public String getName() {
        return "AllocateAuthHistoryToSession";
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

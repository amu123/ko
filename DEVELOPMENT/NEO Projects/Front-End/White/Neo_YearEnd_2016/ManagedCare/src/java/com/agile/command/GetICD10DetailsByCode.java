/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Diagnosis;

/**
 *
 * @author johanl
 */
public class GetICD10DetailsByCode extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;
        session.setAttribute("icdDescription", "");
        String code = request.getParameter("code");
        String element = request.getParameter("element");
        String description = "";
        String client = String.valueOf(context.getAttribute("Client"));
        
        Diagnosis d = service.getNeoManagerBeanPort().getDiagnosisForCode(code);
        
        try{
            out = response.getWriter();
            if(d != null){
                //set values to session
                session.setAttribute(element+"Code", d.getCode());
                if(client != null && client.equalsIgnoreCase("Sechaba") && d.getCode().equalsIgnoreCase("B24")){
                    description = "UNSPECIFIED HUMAN IMMUNODEFICIENCY VIRUS [HIV] DISEASE";
                }else{
                    description = d.getDescription();
                }
                session.setAttribute(element+"Description", description);

                out.print(getName() + "|Code="+d.getCode()+"|Description="+description+"$");

            }else{
                out.print("Error|No such diagnosis|" + getName());
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetICD10DetailsByCode";
    }

}

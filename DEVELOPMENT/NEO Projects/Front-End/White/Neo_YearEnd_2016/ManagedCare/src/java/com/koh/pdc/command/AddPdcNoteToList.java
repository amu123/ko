/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author pavaniv
 */
public class AddPdcNoteToList extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
//        String notes = request.getParameter("notes");

        try {
            saveDetails(port, request, response, context);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void saveDetails(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws IOException, ServletException {
        Map<String, String> errors = PdcNoteMapping.validateNotes(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (saveDetails(port, request)) {

            } else {
                status = "ERROR";

                additionalData = "\"message\":\"There was an error saving.\"";
            }
        } else {
            additionalData = null;
        }
        if ("OK".equalsIgnoreCase(status)) {
            getNotesList(request, response, context);
        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }

    }

    private boolean saveDetails(NeoManagerBean port, HttpServletRequest request) {
        NeoUser neoUser = (NeoUser) request.getSession().getAttribute("persist_user");

        try {
            PdcNotes app = PdcNoteMapping.getPdcNotes(request, neoUser);
            port.savePdcNotes(app);
            return true;
        } catch (java.lang.Exception e) {
            return false;
        }
    }

    private void getNotesList(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        String notes = (String) request.getParameter("notes");
        XMLGregorianCalendar xmlCreationDate;
        Date today = new Date(System.currentTimeMillis());
        HttpSession session = request.getSession();

        if (notes != null && !notes.trim().equalsIgnoreCase("")) {

            try {
                NeoUser user = (NeoUser) session.getAttribute("persist_user");
                xmlCreationDate = DateTimeUtils.convertDateToXMLGregorianCalendar(today);
                List<PdcNotes> noteList = (List<PdcNotes>) session.getAttribute("pdcNoteList");

                if (noteList == null) {
                    noteList = new ArrayList<PdcNotes>();
                }
                
                PdcNotes an = new PdcNotes();

                //generate user info for display
                String uName = user.getName();
                String uSurname = user.getSurname();
                String desc = user.getDescription();

                String createdBy;
                if (uName.trim().equalsIgnoreCase("") && uSurname.trim().equalsIgnoreCase("")) {
                    createdBy = desc;
                } else {
                    createdBy = uName + " " + uSurname;
                }

                an.setCreationDate(xmlCreationDate);
                an.setCreatedBy(user.getUserId());
                an.setNoteDetails(notes);
                an.setUserName(createdBy);

                noteList.add(an);

                Collections.sort(noteList, new Comparator<PdcNotes>() {
                    public int compare(PdcNotes o1, PdcNotes o2) {
                        if (o1.getCreationDate() == null || o2.getCreationDate() == null) {
                            return 0;
                        }
                        return o2.getCreationDate().toGregorianCalendar().getTime().compareTo(o1.getCreationDate().toGregorianCalendar().getTime());
                    }
                });

                session.setAttribute("pdcNoteList", noteList);

            } catch (java.lang.Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }

        try {
            String nextJSP = "/PDC/NoteDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser) request.getSession().getAttribute("persist_user");
    }

    @Override
    public String getName() {
        return "AddPdcNoteToList";
    }
}

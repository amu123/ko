/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.utils;

import java.sql.Connection;
import java.sql.DriverManager;
/**
 *
 * @author johanl
 */
public class DatabaseConnection {

    public DatabaseConnection() {
        connectDB();
    }
    public Connection connectDB() {
        Connection jdbcConnection = null;
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            jdbcConnection = DriverManager.getConnection("jdbc:jtds:sqlserver://aghsql:1433/ManagedCareDev", "neo", "demo");
        } catch (Exception ex) {
            String connectMsg = "Could not connect to the database: " + ex.getMessage() + " " + ex.getLocalizedMessage();
            System.out.println(connectMsg);
        }
        return jdbcConnection;
    }

}

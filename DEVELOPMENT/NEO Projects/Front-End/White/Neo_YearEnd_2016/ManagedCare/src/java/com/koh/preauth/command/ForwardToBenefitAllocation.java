/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthBenefitLimits;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Johan-NB
 */
public class ForwardToBenefitAllocation extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();

        List<AuthBenefitLimits> allBen = new ArrayList<AuthBenefitLimits>();
        boolean valid = false;
        String error = "";
        System.out.println("option - "+session.getAttribute("schemeOption"));
        if (session.getAttribute("schemeOption") != null) {

            //String optIdStr = "" + session.getAttribute("schemeOption");
            int optionId = 0;
            String memNo = "" + session.getAttribute("memberNum_text");
            String depCodeStr = "" + session.getAttribute("depListValues");
            
            String authTypeStr = "";
            String authPeriod = "";
            String authMonthPeriod = "";
            if (session.getAttribute("authType") != null) {
                authTypeStr = "" + session.getAttribute("authType");
                if (session.getAttribute("authPeriod") != null) {
                    valid = true;
                    authPeriod = "" + session.getAttribute("authPeriod");
                    authMonthPeriod = "" + session.getAttribute("authMonthPeriod");
                    authPeriod = authPeriod + "/"+authMonthPeriod+"/01";

                } else {
                    error = "authPeriod";
                }

            } else {
                error = "authType";
            }

            //option
            String fromDateStr = "" + session.getAttribute("authFromDate");
            if (fromDateStr == null || fromDateStr.trim().equalsIgnoreCase("")) {
                error = "authDate";

            } else {
                Date fromDate = null;
                XMLGregorianCalendar xDate = null;
                try {
                    fromDate = new SimpleDateFormat("yyyy/MM/dd").parse(fromDateStr);
                } catch (ParseException px) {
                    px.printStackTrace();
                    error = "authDate";
                }
                if (error.equalsIgnoreCase("")) {
                    xDate = DateTimeUtils.convertDateToXMLGregorianCalendar(fromDate);
                    int depCode = Integer.parseInt(depCodeStr);

                    CoverDetails cd = service.getNeoManagerBeanPort().getDependentForCoverByDate(memNo, xDate, depCode);
                    optionId = cd.getOptionId();
                }
            }
            
            if (optionId != 0 && authTypeStr != null && !authTypeStr.equalsIgnoreCase("")
                    && authPeriod != null && !authPeriod.equalsIgnoreCase("")) {

                int authType = Integer.parseInt(authTypeStr);
                XMLGregorianCalendar authDate = null;
                try {
                    authDate = DateTimeUtils.convertDateToXMLGregorianCalendar(new SimpleDateFormat("yyyy/MM/dd").parse(authPeriod));
                } catch (ParseException ex) {
                    Logger.getLogger(ForwardToBenefitAllocation.class.getName()).log(Level.SEVERE, null, ex);
                }
                allBen = port.getAuthProductBenefits(authType, authDate, memNo, depCodeStr);
            }

        } else {
            error = "option";
        }
        
        String onScreen = "";
        if (valid) {
            onScreen = "/PreAuth/PreAuthBenefitAllocation.jsp";
            session.setAttribute("authListBenefit", allBen);
            session.setAttribute("authBenefitButton", null);
            
            if(allBen == null || allBen.isEmpty() == true){
                request.setAttribute("authBenAllowResultsMessage", "No Benefits found for Auth period. Member was not active for this period.");
            }

        } else {
            onScreen = "" + session.getAttribute("onScreen");
            String errorMsg = "";
            if (error.equalsIgnoreCase("option")) {
                errorMsg = "Member Option Required";

            } else if (error.equalsIgnoreCase("authType")) {
                errorMsg = "Auth Type Required";

            } else if (error.equalsIgnoreCase("authPeriod")) {
                errorMsg = "Auth Period Required";
                
            } else if (error.equalsIgnoreCase("authDate")) {
                errorMsg = "Auth Start Date Required";

            }

            session.setAttribute("authBenefitButton", errorMsg);

        }

        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(onScreen);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ForwardToBenefitAllocation";
    }
}

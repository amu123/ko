/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

import com.koh.command.Command;
import com.koh.utils.UrlUtil;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoMenu;
import neo.manager.NeoUser;
import neo.manager.SecurityResponsibility;

/**
 *
 * @author gerritj
 */
public class LeftMenuCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        try {
            String client = String.valueOf(context.getAttribute("Client"));
            System.out.println("[fr]Client (LEFTMENUCOMMAND)= " +client);
            PrintWriter out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");

            out.println("<link rel=\"stylesheet\" href=\"./resources/treeMenu/jquery.treeview.css\" />");
            out.println("<script type=\"text/javascript\" src=\"./resources/treeMenu/jquery.min.js\"></script>");
            out.println("<script src=\"./resources/treeMenu/jquery.cookie.js\" type=\"text/javascript\"></script>");
            out.println("<script src=\"./resources/treeMenu/jquery.treeview.js\" type=\"text/javascript\"></script>");
            out.println("<script type=\"text/javascript\" src=\"./resources/treeMenu/demo.js\"></script>");

            out.println("<script language=\"JavaScript\">");
            out.println("function clearAllFromSession(){"
                    +"var url=\"/ManagedCare/AgileController\"; "
                    + "var data = {'opperation':'LeftMenuClearAllFromSession','id':new Date().getTime()};"
                    + "$.post(url, data);"
                    + "}");
            out.println("</script>");

            out.println("</head>");
            out.println("<body>");
            out.println("<div id=\"treecontrol\">");
            out.println("<a title=\"Collapse the entire tree below\" href=\"#\" style=\"color: #666666;font-family: Arial, Helvetica, sans-serif;font-size: 12px;\"> Collapse All</a> |");
            out.println("<a title=\"Expand the entire tree below\" href=\"#\" style=\"color: #666666;font-family: Arial, Helvetica, sans-serif;font-size: 12px;\"> Expand All</a> |");
            out.println("<a title=\"Toggle the tree below, opening closed branches, closing open branches\" href=\"#\" style=\"color: #666666;font-family: Arial, Helvetica, sans-serif;font-size: 12px;\">Toggle All</a>");
            out.println("</div>");

            NeoUser user = (NeoUser) session.getAttribute("persist_user");
            if (user != null) {
                ArrayList<SecurityResponsibility> respList = (ArrayList) user.getSecurityResponsibility();
                if (respList != null) {
                    for (int i = 0; i < respList.size(); i++) {
                        SecurityResponsibility resp = respList.get(i);
                        ArrayList<NeoMenu> menuItems = (ArrayList) resp.getMenuItems();
                        if (menuItems != null) {
                            int parent = new Integer(request.getParameter("application"));
                            out.println(buildTree(menuItems, parent));

//                            for (int j = 0; j < menuItems.size(); j++) {
//                                MenuItem menuItem = menuItems.get(j);
//
//                            }
                        }
                    }
                }
            }
            out.println("<form action=\"/ManagedCare/AgileController\" name=\"leftMenu\">");
            out.println("<input type=\"hidden\" name=\"opperation\" value=\"LeftMenuCommand\" size=\"30\" />");
            out.println("<input type=\"hidden\" name=\"application\" value=\"0\" size=\"30\" />");
            out.println("</form>");
            if(client.equalsIgnoreCase("Agility")){
            out.println("<img src=\"/ManagedCare/resources/KO_powered by.jpg\" alt=\"PowerdBy\" style=\"bottom: 0; position: absolute; z-index: -1\">");
            } else if(client.equalsIgnoreCase("Sechaba")){
                out.println("<img src=\"/ManagedCare/resources/KOH_powered by.jpg\" alt=\"PowerdBy\" style=\"bottom: 0; position: absolute; z-index: -1\">");
            }
            out.println("</body>");
            out.println("</html>");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String buildTree(ArrayList<NeoMenu> menuItems, int parent) {
        String tree = "<ul id=\"red\" class=\"treeview-red\">";
        for (int i = 0; i < menuItems.size(); i++) {
            NeoMenu item = menuItems.get(i);
            if (item.getParentMenuItem() == parent) {

                //is child
                //add to list
                if (item.getAttributes().getAction() != null) {
                    String href = UrlUtil.getUrl(item.getAttributes().getAction(), "appName", item.getDescription());
                    tree += "<li><span><a href=\" " + href + "\" target=\"worker\">" + item.getDisplayName() + "</a></span>";
                } else {
                    tree += "<li><span>" + item.getDisplayName() + "</span>";
                }

                tree += returnULForParent(menuItems, item.getMenuItemId(), item.getMenuItemId());
                tree += "</li>";
            }
        }
        tree += "</ul>";
        return tree;
    }

    private String returnULForParent(ArrayList<NeoMenu> menuItems, int parent, int origParent) {
        String ul = "";
        boolean ulAdded = false;
        for (int i = 0; i < menuItems.size(); i++) {
            NeoMenu item = menuItems.get(i);
            if (item.getParentMenuItem() == parent) {
                String newUL = returnULForParent(menuItems, item.getMenuItemId(), origParent);
                if (!ulAdded) {
                    ulAdded = true;
                }
                if (item.getAttributes().getAction() != null) {
                    String href = UrlUtil.getUrl(item.getAttributes().getAction(), "appName", item.getDescription());
                    ul += "<li><span><a href=\" " + href + "\" target=\"worker\" onClick=\"clearAllFromSession();\">" + item.getDisplayName() + "</a></span>";
                } else {
                    ul += "<li><span>" + item.getDisplayName() + "</span>";
                }
                ul += newUL;
                ul += "</li>";
            }
        }
        if (ulAdded) {
                    ul = "<ul" + (ul == null || ul.isEmpty() || parent == origParent ? "" : " style='display:none;' ") + ">" + ul;
            ul += "</ul>";
        }

        return ul;
    }

    @Override
    public String getName() {
        return "LeftMenuCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.auth.dto.ICD10SearchResult;
import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Diagnosis;
import neo.manager.DiagnosisSearchCriteria;

/**
 *
 * @author martins
 */
public class SearchPreAthICD10Command extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
       HttpSession session = request.getSession();
       String code = request.getParameter("code");
       String description = request.getParameter("description");
       DiagnosisSearchCriteria search = new DiagnosisSearchCriteria();
       if(code != null && !code.equalsIgnoreCase("")) {

           search.setCode(code);
       }
       if(description != null && !description.equalsIgnoreCase("")) {

           search.setDescription(description);
       }

        ArrayList<ICD10SearchResult> icd10s = new ArrayList<ICD10SearchResult>();
        List<Diagnosis> diagList = new ArrayList<Diagnosis>();

        try {

            diagList = service.getNeoManagerBeanPort().findAllDiagnosisByCriteria(search);

            for (Diagnosis diag : diagList)
            {
                ICD10SearchResult icd10 = new ICD10SearchResult();
                icd10.setCode(diag.getCode());
                icd10.setDescription(diag.getDescription());
                icd10s.add(icd10);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        request.setAttribute("searchICD10Result", icd10s);
        session.setAttribute("searchICD10Result", icd10s);
        try {
            String nextJSP = "/PreAuth/ICD10Search.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

       return null;
    }

    @Override
    public String getName() {
        return "SearchPreAthICD10Command";
    }
}

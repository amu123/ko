/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author johanl
 */
public class AuthNappiTableTag extends TagSupport {
    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        try {
            out.println("<td colspan=\"8\"><table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Provider Category</th><th>Nappi Code</th><th>Nappi Desc</th><th>Amount</th>" +
                    "<th>Dosage</th><th>Quantity</th><th>Frequency</th><th></th></tr>");

            ArrayList<AuthTariffDetails> basketList = (ArrayList<AuthTariffDetails>) session.getAttribute(sessionAttribute);
            if (basketList != null) {
                for (int i = 1; i <= basketList.size(); i++) {
                    AuthTariffDetails ab = basketList.get(i - 1);
                    String provCat = NeoCommand.service.getNeoManagerBeanPort().getValueFromCodeTableForTableId(59, ab.getProviderType());
                    String hidden = ab.getTariffCode() + "|" + ab.getTariffDesc()+"|"+ab.getFrequency();

                    out.println("<tr id=\"napRow" + hidden + i + "\"><form>");
                    out.println(
                            "<td>" +
                                "<label class=\"label\">" + provCat + "</label>" +
                                "<input type=\"hidden\" name=\"napHidden\" id=\"napHidden\" value=\"" + hidden + "\"/>" +
                            "</td>" +
                            "<td>" +
                                "<label class=\"label\">" + ab.getTariffCode() + "</label>" +
                            "</td>" +
                            "<td width=\"200\">" +
                                "<label class=\"label\">" + ab.getTariffDesc() + "</label>" +
                            "</td>" +
                            "<td>" +
                                "<label class=\"label\">" + ab.getAmount() + "</label>" +
                            "</td>" +
                            "<td>" +
                                "<label class=\"label\">" + ab.getDosage() + "</label>" +
                            "</td>" +
                            "<td>" +
                                "<label class=\"label\">" + ab.getQuantity() + "</label>" +
                            "</td>" +
                            "<td>" +
                                "<label class=\"label\">" + ab.getFrequency() + "</label>" +
                            "</td>" +
                            "<td>" +
                                "<button type=\"button\" onClick=\"ModifyNappiFromList(" + i + ");\" >Modify</button>" +
                            "</td>" +
                            "</form></tr>");
                }
                out.println("</table></td>");
            }
        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberSearchTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;

import agility.za.documentenginetype.IndexForMemberFilter;
import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import agility.za.indexdocumenttype.IndexForMember;
import com.koh.command.NeoCommand;
import com.koh.cover.MemberMainMapping;
import com.koh.cover.commands.MemberMaintenanceTabContentCommand;
import com.koh.serv.PropertiesReader;
import com.koh.utils.DateTimeUtils;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ContactDetails;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author sphephelot
 */
public class CallCenterDocumentListCommand extends NeoCommand{
    private Logger logger = Logger.getLogger(MemberMaintenanceTabContentCommand.class);
    private static final String JSP_FOLDER = "/Claims/";

   @Override
   public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();               
        int entityId = (Integer) session.getAttribute("coverEntityId");
        List<ContactDetails> con = port.getContactDetailsByEntityId(entityId);
        MemberMainMapping.setContactDetails(request, con);
        try {
           
            
            String docKind = request.getParameter("docKind");
            String docType = request.getParameter("docType");            
            String minDate =  request.getParameter("minDate");
            String maxDate =  request.getParameter("maxDate");
            
            request.setAttribute("docKind",docKind);
            request.setAttribute("docType",docType);            
            request.setAttribute("minDate",minDate);
            request.setAttribute("maxDate",maxDate);
                     
            session.setAttribute("docType", docType);
            request.setAttribute("folderList", new PropertiesReader().getProperty("DocumentIndexWorkFolder"));
            request.setAttribute("listOrWork", "Work");
            //String memberNumber = request.getParameter("memberNumber");
            String memberNumber = (String) session.getAttribute("memberNumber");
            logger.info("Memeber no = " + memberNumber);
            if (memberNumber != null && !memberNumber.trim().equals("")) {
                IndexDocumentRequest req = new IndexDocumentRequest();
                NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
                req.setAgent(loggedInUser);
                req.setIndexType("IndexForMemberByFilter");
                req.setEntityNumber(memberNumber);
                req.setSrcDrive(new PropertiesReader().getProperty("DocumentIndexIndexDrive"));
                IndexForMemberFilter filter = new IndexForMemberFilter();
                filter.setDocKind(docKind);
                filter.setDocType(docType);
                if(!minDate.isEmpty()   || !maxDate.isEmpty()){
                filter.setMinDate(DateTimeUtils.convertDateToXMLGregorianCalendar(new Date(minDate)));
                filter.setMaxDate(DateTimeUtils.convertDateToXMLGregorianCalendar(new Date(maxDate)));
                }
                req.setFilter(filter);
                IndexDocumentResponse resp = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
                if (resp.getIndexForMember() != null) {
                    logger.info("Index size = " + resp.getIndexForMember().size());
                }
                for (IndexForMember index : resp.getIndexForMember()) {
                    String typeName = index.getDocType();
                    Pattern p = Pattern.compile("([0-9]*)");
                    Matcher m = p.matcher(typeName);
                    System.out.println("Type Name >> " + typeName);
                    if (m.matches()) {
                        typeName = port.getValueForId(212, typeName);                        
                        index.setDocType(typeName);
                    }
                }
                request.setAttribute("indexList", resp.getIndexForMember());
                session.setAttribute("indexList", resp.getIndexForMember()); //Used in EmailLatestDocumentCommand.java
                System.out.println("INDEEX LIST >> " + resp.getIndexForMember().get(0).getLocation());
            }
            request.setAttribute("memberNumber", memberNumber);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        String page = JSP_FOLDER + "CallCenterViewDocuments.jsp";
        try {
            context.getRequestDispatcher(page).forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }       
        return null;
    }
    @Override
    public String getName() {
        return "CallCenterDocumentListCommand";
    }  
}

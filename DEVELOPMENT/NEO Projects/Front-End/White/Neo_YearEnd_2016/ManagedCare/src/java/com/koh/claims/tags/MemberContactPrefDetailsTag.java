/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.tags;

import com.koh.command.NeoCommand;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.ContactPreference;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Johan-NB
 */
public class MemberContactPrefDetailsTag extends TagSupport {

    private String sessionAttribute;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

        try {
            ArrayList<ContactPreference> conPrefDetailsList = (ArrayList<ContactPreference>) session.getAttribute(sessionAttribute);
            if (conPrefDetailsList != null && !conPrefDetailsList.isEmpty()) {
                out.println("<table width=\"200px\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.print("<tr>");
                out.print("<th align=\"left\">Communication Method</th>");
                out.print("<th align=\"left\">Communication Type</th>");
                out.print("</tr>");

                for (ContactPreference conP : conPrefDetailsList) {
                    out.print("<tr valign=\"top\">");
                    
                    if (conP.getDetailType().equalsIgnoreCase("C")) {
                        out.print("<td><label class=\"label\">" + conP.getCommunicationMethod() + "</label></td>");
                        out.print("<td><label class=\"label\">" + conP.getCommunicationType() + "</label></td>");
                    } else if (conP.getDetailType().equalsIgnoreCase("A")) {
                        if (conP.getContactAddressType() == 1) {
                            out.print("<td><label class=\"label\">Physical</label></td>");
                        } else {
                            out.print("<td><label class=\"label\">Postal</label></td>");
                        }
                        out.print("<td><label class=\"label\">" + conP.getCommunicationType() + "</label></td>");
                    }
                    out.print("</tr>");

                }

                out.println("</table>");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }
}

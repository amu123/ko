/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import com.koh.command.NeoCommand;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.Product;

/**
 *
 * @author johanl
 */
public class LabelProductDropDown extends TagSupport {

    private String displayName;
    private String elementName;
    private String javaScript;
    private String mandatory = "no";
    private String valueFromSession = "no";
    private String disable = "no";

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        try {
            if (getDisable().equalsIgnoreCase("yes")) {
                setDisable("disabled");
            } else {
                setDisable("");
            }
            out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td>");
            out.println("<td align=\"left\" width=\"200px\"><select style=\"width:215px\" name=\"" + elementName + "\" id=\"" + elementName + "\" " + getDisable());
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");
            @SuppressWarnings("static-access")
            List<Product> prodList = NeoCommand.service.getNeoManagerBeanPort().fetchAllProducts();
            out.println("<option value=\"99\"></option>");
            if (prodList != null) {
                for (Product prod : prodList) {
                    String prodId = String.valueOf(prod.getProductId());
                    if (valueFromSession.trim().equalsIgnoreCase("yes")) {
                        String sessionVal = "" + session.getAttribute(elementName);
                        if (sessionVal.equals(prodId)) {
                            out.println("<option value=" + prodId + " selected >" + prod.getProductName() + "</option>");
                        } else {
                            out.println("<option value=" + prodId + ">" + prod.getProductName() + "</option>");
                        }

                    } else {
                        out.println("<option value=" + prodId + ">" + prod.getProductName() + "</option>");
                    }
                }
            }
            out.println("</select></td>");
            if (mandatory.equalsIgnoreCase("yes")) {
                out.println("<td><label class=\"red\">*</label></td>");
            } else {
                out.println("<td></td>");
            }
            //search column
            out.println("<td width=\"30px\"></td>");
            out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelNeoLookupValueDropDown tag", ex);
        }

        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setValueFromSession(String valueFromSession) {
        this.valueFromSession = valueFromSession;
    }

    /**
     * @return the disable
     */
    public String getDisable() {
        return disable;
    }

    /**
     * @param disable the disable to set
     */
    public void setDisable(String disable) {
        this.disable = disable;
    }
}

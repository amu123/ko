/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoUser;

/**
 *
 * @author josephm
 */
public class LockUserCommand extends NeoCommand {
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
    
        System.out.println("Inside LockUserCommand");
        HttpSession session = request.getSession();
        NeoUser user = (NeoUser)session.getAttribute("persist_user");
        Integer userId = (Integer) session.getAttribute("userId");
        service.getNeoManagerBeanPort().lockUser(user, userId);
        
        System.out.println("The user id is " + userId);
        System.out.println("The logged in user is " + user.getUserId());
        System.out.println("The password access left " + user.getPassAccessLeft());

        try {
        
            PrintWriter out = response.getWriter();

            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            out.println("<td><label class=\"header\"><h2>User has been successfully blocked from the system</h2></label></td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            response.setHeader("Refresh", "2; URL=/ManagedCare/SystemAdmin/UpdateUserProfile.jsp");
        }catch(Exception ex) {
        
            ex.printStackTrace();
        }
        
        return null;
    }
    
    @Override
    public String getName() {
    
        return "LockUserCommand";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.union.command;

import com.koh.employer.command.TabUtils;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author shimanem
 */
public class UnionMemberMapping {

    private static final Logger logger = Logger.getLogger(UnionMemberMapping.class.getName());

    private static final String[][] FIELD_MAPPINGS = {{"memberCoverNumber", "", "yes", "str", "UnionMember", "memberCoverNumber"},
    {"memberCoverDepType", "", "yes", "str", "UnionMember", "memberCoverDepType"},
    {"employerGroup_unionName", "Union Name", "yes", "str", "UnionMember", "employerGroup_unionName"},
    {"employerGroup_unionID", "Union ID", "yes", "int", "UnionMember", "employerGroup_unionID"},
    {"employerGroup_unionNumber", "Union Number", "yes", "str", "UnionMember", "employerGroup_unionNumber"},
    {"member_repName", "Representative Name", "yes", "str", "UnionMember", "member_repName"},
    {"member_repSurname", "Representative Surname", "yes", "str", "UnionMember", "member_repSurname"},
    {"member_unionRegion", "Region", "yes", "int", "UnionMember", "member_unionRegion"},
    {"member_unionInceptionDate", "Inception Date", "yes", "date", "UnionMember", "member_unionInceptionDate"}};

    private static final Map<String, String[]> FIELD_MAP;

    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }

    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
        return errors;
    }

}

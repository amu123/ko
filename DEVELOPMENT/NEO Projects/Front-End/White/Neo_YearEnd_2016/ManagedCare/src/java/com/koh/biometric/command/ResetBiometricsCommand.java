/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.biometric.command;

import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author martins
 */
public class ResetBiometricsCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        String pdcFlag = "" + session.getAttribute("pdcFlag");
        System.out.println("PDC FLAG ON PREAUTH RETURN TO SEARCH ==== " + pdcFlag);
        if(pdcFlag != null && !pdcFlag.equals("") && !pdcFlag.equalsIgnoreCase("null")){
            String memberNum = "" + session.getAttribute("memberNum_text");
            System.out.println("GOT MEMBER NUMBER TO SAVE = " + memberNum);
            this.clearAllFromSession(request);
            session.setAttribute("memberNumber_text",memberNum);
            session.setAttribute("pdcFlag", pdcFlag);
        }else{
            this.clearAllFromSession(request);
        }

        try {
            String nextJSP = "/biometrics/CaptureBiometrics.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        }
        catch(Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "ResetBiometricsCommand";
    }
}

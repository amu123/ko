/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author johanl
 */
public class ButtonOpperationLabelError extends TagSupport {
    private String displayName;
    private String elementName;
    private String type;
    private String javaScript;
    private String valueFromSession = "no";
    private String commandName;
    private String mandatory = "no";
    private String align = "right";

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();

        try {
            if (!javaScript.equalsIgnoreCase("")) {
                out.println("<td colspan=\"2\" align=\"" + align + "\"><button name=\"opperation\" type=\"" + type + "\" " + javaScript + " value=\"" + commandName + "\">" + displayName + "</button></td>");
            } else {
                out.println("<td colspan=\"2\" align=\"" + align + "\"><button name=\"opperation\" type=\"" + type + "\" value=\"" + commandName + "\">" + displayName + "</button></td>");
            }

            if (mandatory.equalsIgnoreCase("yes")) {
                out.println("<td><label class=\"red\">*</label></td>");
            } else {
                out.println("<td></td>");
            }

            if(valueFromSession.trim().equalsIgnoreCase("yes")){
                //set error label
                String error = "" + session.getAttribute(elementName+"_error");
                if(error != null && !error.trim().equalsIgnoreCase("") &&
                        !error.equalsIgnoreCase("null")){
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">"+ error +"</label></td>");
                }else{
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
                }
            }else{
                out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setValueFromSession(String valueFromSession) {
        this.valueFromSession = valueFromSession;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }
    
    public void setAlign(String align) {
        this.align = align;
    }

}

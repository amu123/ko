/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.PdcCDLStatus;

/**
 *
 * @author marcelp
 */
public class SaveMemberCDLStatusCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("in SaveMemberHIVStatusCommand");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        try {
            PdcCDLStatus pdcCDLStatus = new PdcCDLStatus();
            setPdcHivStatusObj(pdcCDLStatus, session, request);
            int saveResult = port.savePdcMemberCDLStatus(pdcCDLStatus);
            if (saveResult == 0) {
                session.setAttribute("SaveMemberCDLStatusCommand_error", "Error saving member CDL status");
            }
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        ViewCDLStatusCommand viewCommand = new ViewCDLStatusCommand();
        return viewCommand.execute(request, response, context);
    }

    private void setPdcHivStatusObj(PdcCDLStatus pdcCDLStatus, HttpSession session, HttpServletRequest request) {
        Date today = new Date();
        int hivStatusInd;
        hivStatusInd = Integer.parseInt(request.getParameter("hivStatusInd"));
        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        pdcCDLStatus.setCreatedBy(user.getUserId());
        pdcCDLStatus.setLastUpdatedBy(user.getUserId());
        pdcCDLStatus.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        pdcCDLStatus.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        pdcCDLStatus.setEntityId(Integer.parseInt(session.getAttribute("entityId").toString()));
        pdcCDLStatus.setRegisteredInd(hivStatusInd);
        pdcCDLStatus.setCdlCategory(1);
    }

    @Override
    public String getName() {
        return "SaveMemberCDLStatusCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author johanl
 */
public class AllocateNappiToSession extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        String code = request.getParameter("napCode");
        String desc = request.getParameter("napDesc");
        String amount = request.getParameter("tariffAmount");
        String dosage = request.getParameter("tariffDosage");
        String quan = request.getParameter("tariffQuan");

        session.setAttribute("nappiCode", code);
        session.setAttribute("nappiDesc", desc);
        session.setAttribute("nappiAmount", amount);
        session.setAttribute("nappiDosage", dosage);
        session.setAttribute("nappiQuan", quan);

        session.setAttribute("defaultNappiAmount", amount);
        session.setAttribute("defaultNappiQuan", quan);

        try {
            String nextJSP = "/PreAuth/AuthSpecificNappi.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "AllocateNappiToSession";
    }
}

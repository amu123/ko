/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.calltrack.command;

import com.koh.command.NeoCommand;
import com.koh.utils.MapUtils;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.KeyValue;
import neo.manager.KeyValueArray;
import neo.manager.ProviderDetails;

/**
 *
 * @author martins
 */
public class WorkflowFindGroupsByNumberCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        String groupName = "";
        String groupNumber = request.getParameter("groupNum");
        int productId = 3;//hardcoded to Sizwe for now
        List<KeyValueArray> kva = service.getNeoManagerBeanPort().getEmployerList(groupName, groupNumber, productId);

        try {
            PrintWriter out = response.getWriter();

            if (kva.size() == 1) {
                for (KeyValueArray s : kva) {
                    List<KeyValue> t = s.getKeyValList();
                    for (KeyValue d : t) {
                        if(d.getKey().equalsIgnoreCase("employerName")){
                            request.setAttribute("groupName_text", d.getValue());
                            out.print(getName() + "|GroupName="+d.getValue() + "$");
                        }
                    }
                }
            } else {
                out.print("Error|No Such Group|" + getName());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return null;
    }

    @Override
    public String getName() {
        return "WorkflowFindGroupsByNumberCommand";
    }

}

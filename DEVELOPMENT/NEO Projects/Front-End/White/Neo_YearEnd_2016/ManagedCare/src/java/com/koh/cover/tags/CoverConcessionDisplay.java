/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.tags;

import com.koh.command.NeoCommand;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.*;

/**
 *
 * @author pavaniv
 */
public class CoverConcessionDisplay extends TagSupport {

    private String commandName;
    private String javaScript;
    private String sessionAttribute;
    private String onScreen;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        String coverNumber = (String) session.getAttribute("coverNumber");
        //int infoTypeid = 9;
        try {
            List<CoverDetails> cdLists = (List<CoverDetails>) session.getAttribute(sessionAttribute);
            List<LookupValue> values = port.getCodeTable(261);
            LookupValue lookupValue = new neo.manager.LookupValue();
            if (cdLists != null && cdLists.isEmpty() == false) {

               
                List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("MemberCoverDependantDetails");
                int CommonentityID = 0;
                for (CoverDetails cd1 : cdList) {
                    if (cd1.getDependentTypeId() == 17) {
                        CommonentityID = cd1.getEntityCommonId();


                    }
                }
         
                
     String covinfo = port.findConcessionTypebyEntityCommId(CommonentityID);        
                
                out.println("<table width=\"14%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.println("<tr>");
            
                   out.println("<th>Concession:</th>");
                 
                out.println("<td><label class=\"label\">" + covinfo + "</label></td>");
                 out.println("</tr>");
            out.println("</table>");
            }
           
            
        } catch (java.io.IOException ex) {
            throw new JspException("Error in CoverConcessionDisplay tag", ex);
        }
        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setOnScreen(String onScreen) {
        this.onScreen = onScreen;
    }
}
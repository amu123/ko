package com.koh.employer.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.Parameters;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author andreg
 */
public class EmployerTerminationCommand extends NeoCommand {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd");

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println(this.getClass().getName());
        System.out.println(request.toString());

        NeoManagerBean port = service.getNeoManagerBeanPort();
        String entityId = request.getParameter("employerEntityId");
        request.setAttribute("EmployerGroup_entityId", entityId);
        String date = request.getParameter("Employer_termination_date");

        //TODO get date of last member
        Date lastDate;
        try {
            lastDate = DateTimeUtils.getDateFromXMLGregorianCalendar(port.getLastMemberTerminationDateByGroup(Integer.parseInt(entityId)));
        } catch (Exception e) {
            lastDate = null;
        }
        if (lastDate != null && lastDate.after(DateTimeUtils.setToLastDayOfMonth(getDate(date)))) {
            try {
                response.getWriter().println(TabUtils.buildJsonResult("ERROR", null, null, "\"message\":\"There are still active members in this group or member termination date is after group termination date.\""));
            } catch (IOException ex) {
                Logger.getLogger(EmployerTerminationCommand.class.getName()).log(Level.WARNING, null, ex);
            }
            return null;
        }
        
        boolean res = updateAppStatus(port, request, getDate(date));

        try {
            if (res) {
                response.getWriter().println(TabUtils.buildJsonResult("OK", null, null, "\"message\":\"Group terminated\""));
                context.getRequestDispatcher("/Employer/EmployerSearch.jsp").forward(request, response);
            } else {
                response.getWriter().println(TabUtils.buildJsonResult("ERROR", null, null, "\"message\":\"An error occured while trying to terminate group\""));
            }
        } catch (ServletException ex) {
            Logger.getLogger(EmployerTerminationCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EmployerTerminationCommand.class.getName()).log(Level.WARNING, null, ex);
        } 

        return null;
    }

    @Override
    public String getName() {
        return "EmployerTerminationCommand";
    }

    private Date getDate(String dateStr) {
        try {
            return DATE_FORMAT.parse(dateStr);
        } catch (Exception e) {
            return null;
        }
    }

    private boolean updateAppStatus(NeoManagerBean port, HttpServletRequest request, Date startDate) {
        int entityId = Parameters.getIntParam(request, "employerEntityId");
        startDate = DateTimeUtils.setToLastDayOfMonth(startDate);
        if (entityId > 0) {
            int status = 9; //Terminate
            Security sec = new Security();
            NeoUser user = (NeoUser) request.getSession().getAttribute("persist_user");
            sec.setCreatedBy(user.getUserId());
            sec.setLastUpdatedBy(user.getUserId());
            int result = port.updateEmployerStatus(entityId, status, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), sec);
            return true;
        } else {
            return false;
        }
    }
}

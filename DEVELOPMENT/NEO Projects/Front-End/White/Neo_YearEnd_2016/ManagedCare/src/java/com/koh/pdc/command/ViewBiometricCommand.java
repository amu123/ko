/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AsthmaBiometrics;
import neo.manager.Biometrics;
import neo.manager.BiometricsTabs;
import neo.manager.CardiacBiometrics;
import neo.manager.ChronicDiseaseBiometrics;
import neo.manager.CopdBiometrics;
import neo.manager.CoronaryBiometrics;
import neo.manager.DiabetesBiometrics;
import neo.manager.GeneralBiometrics;
import neo.manager.HyperlipidaemiaBiometrics;
import neo.manager.HypertensionBiometrics;
import neo.manager.MajorDepressionBiometrics;
import neo.manager.NeoManagerBean;
import neo.manager.OtherBiometrics;

/**
 *
 * @author dewaldvdb
 */
public class ViewBiometricCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        String bioName = request.getParameter("bioName");
        String bioID = request.getParameter("biometricID");
        session.setAttribute("BiometricID", bioID);
        String nextJSP = request.getParameter("onScreen");
        String memNumber = (String) session.getAttribute("memberNumber_text");
        boolean editBTNshow = false;
        if (request.getParameter("cdlDropdown") != null || !"".equals(request.getParameter("cdlDropdown"))) {
            String cdl = request.getParameter("cdlDropdown");
            session.setAttribute("cdl", Integer.parseInt(cdl));
            session.setAttribute("cdlDropdown", cdl);
        }

        String depNo = null;
        if (session.getAttribute("depListValues_text") != null) {
            depNo = (String) session.getAttribute("depListValues_text").toString();
        }
        if (depNo == null) {
            depNo = "0";
        }
        if (nextJSP == null || nextJSP.equalsIgnoreCase("") || nextJSP.equalsIgnoreCase("null")) {
            nextJSP = "/PDC/BiometricDetails.jsp";
        }
        System.out.println("nextJSP - ViewBiometricCommand: " + nextJSP);

        List<Biometrics> bioMetric = null;

        if (bioName.equals("Asthma")) {
            List<AsthmaBiometrics> getAsthma = (List<AsthmaBiometrics>) session.getAttribute("getAsthma");

            AsthmaBiometrics bioNew = new AsthmaBiometrics();
            for (AsthmaBiometrics o : getAsthma) {
                String asthmaBioID = String.valueOf(o.getBiometricsId());
                if (asthmaBioID.equals(bioID)) {
                    bioNew = o;
                }
            }
            session.setAttribute("asthmaDaySym", bioNew.getAsthmaDayTimeSymptoms());
            session.setAttribute("asthmaNightSym", bioNew.getAsthmaNightTimeSymtoms());
            session.setAttribute("bpef", bioNew.getBpef());
            session.setAttribute("sabaUse", bioNew.getSabaUse());
            session.setAttribute("astmanDetail", bioNew.getDetail());

            //For updating detection in CaptureBiometrics
            session.setAttribute("asthmaDaySym_text", String.valueOf(bioNew.getAsthmaDayTimeSymptoms()));
            session.setAttribute("asthmaNightSym_text", String.valueOf(bioNew.getAsthmaNightTimeSymtoms()));
            session.setAttribute("bpef_text", String.valueOf((int) bioNew.getBpef()));
            session.setAttribute("sabaUse_text", String.valueOf(bioNew.getSabaUse()));
            session.setAttribute("astmanDetail_text", String.valueOf(bioNew.getDetail()));
            session.setAttribute("source_text", String.valueOf(bioNew.getSource()));

            if (bioNew.getDateReceived() != null) {
                session.setAttribute("dateReceived", DateTimeUtils.convertToYYYYMMDD(DateTimeUtils.getDateFromXMLGregorianCalendar(bioNew.getDateReceived())));
            } else {
                session.setAttribute("dateReceived", "");
            }
            session.setAttribute("providerNumber_text", String.valueOf(bioNew.getTreatingProvider()));
            session.setAttribute("source", bioNew.getSource());
            List<BiometricsTabs> icdMeaning = port.getBiometricMeaningFromValue(bioNew.getIcd10(), "120");
            if (!icdMeaning.isEmpty()) {
                session.setAttribute("icd10_text", icdMeaning.get(0).getLookupMeaning());
            }
            session.setAttribute("historyTableFlag", "1");
            session.setAttribute("ViewSpecificBiometric", bioNew);
            bioMetric = port.fetchBiometrics(Integer.parseInt(bioID), memNumber);
        }

        if (bioName.equals("Hypertension")) {
            List<HypertensionBiometrics> getHypertension = (List<HypertensionBiometrics>) session.getAttribute("getHypertension");
            HypertensionBiometrics bioNew = new HypertensionBiometrics();

            for (HypertensionBiometrics o : getHypertension) {
                String hypertensionBioID = String.valueOf(o.getBiometricsId());
                if (hypertensionBioID.equals(bioID)) {
                    bioNew = o;
                }
            }

            HypertensionBiometrics bioHypertension = new HypertensionBiometrics();
            bioMetric = port.fetchBiometrics(Integer.parseInt(bioID), memNumber);

            session.setAttribute("hypertensionDetails_text", String.valueOf(bioNew.getDetail()));
            session.setAttribute("source_text", String.valueOf(bioNew.getSource()));
            session.setAttribute("hypertensionDetails", bioNew.getDetail());

            if (bioNew.getDateReceived() != null) {
                session.setAttribute("dateReceived", DateTimeUtils.convertToYYYYMMDD(DateTimeUtils.getDateFromXMLGregorianCalendar(bioNew.getDateReceived())));
            } else {
                session.setAttribute("dateReceived", "");
            }
            session.setAttribute("providerNumber_text", String.valueOf(bioNew.getTreatingProvider()));
            session.setAttribute("source", bioNew.getSource());
            List<BiometricsTabs> icdMeaning = port.getBiometricMeaningFromValue(bioNew.getIcd10(), "112");
            if (!icdMeaning.isEmpty()) {
                session.setAttribute("icd10_text", icdMeaning.get(0).getLookupMeaning());
            }
            session.setAttribute("historyTableFlag", "2");
            session.setAttribute("ViewSpecificBiometric", bioNew);
        }

        if (bioName.equals("ChronicRenalDisease")) {
            List<ChronicDiseaseBiometrics> getChronic = (List<ChronicDiseaseBiometrics>) session.getAttribute("getChronic");
            ChronicDiseaseBiometrics bioNew = new ChronicDiseaseBiometrics();

            for (ChronicDiseaseBiometrics o : getChronic) {
                String chronicDiseaseBioID = String.valueOf(o.getBiometricsId());
                if (chronicDiseaseBioID.equals(bioID)) {
                    bioNew = o;
                }
            }

            ChronicDiseaseBiometrics bioChronicDisease = new ChronicDiseaseBiometrics();
            bioMetric = port.fetchBiometrics(Integer.parseInt(bioID), memNumber);

            session.setAttribute("sCreatinin_text", String.valueOf(bioNew.getSCreatinin()));
            session.setAttribute("proteinuria_text", String.valueOf(bioNew.getProteinuria()));
            session.setAttribute("renalDetail_text", String.valueOf(bioNew.getDetail()));
            session.setAttribute("source_text", String.valueOf(bioNew.getSource()));

            session.setAttribute("sCreatinin", bioNew.getSCreatinin());
            session.setAttribute("proteinuria", bioNew.getProteinuria());
            session.setAttribute("renalDetail", bioNew.getDetail());

            if (bioNew.getDateReceived() != null) {
                session.setAttribute("dateReceived", DateTimeUtils.convertToYYYYMMDD(DateTimeUtils.getDateFromXMLGregorianCalendar(bioNew.getDateReceived())));
            } else {
                session.setAttribute("dateReceived", "");
            }
            session.setAttribute("providerNumber_text", String.valueOf(bioNew.getTreatingProvider()));
            session.setAttribute("source", bioNew.getSource());
            List<BiometricsTabs> icdMeaning = port.getBiometricMeaningFromValue(bioNew.getIcd10(), "113");
            if (!icdMeaning.isEmpty()) {
                session.setAttribute("icd10_text", icdMeaning.get(0).getLookupMeaning());
            }
            session.setAttribute("historyTableFlag", "3");
            session.setAttribute("ViewSpecificBiometric", bioNew);
        }

        if (bioName.equals("CardiacFailure")) {
            List<CardiacBiometrics> getCardiac = (List<CardiacBiometrics>) session.getAttribute("getCardiac");
            CardiacBiometrics bioNew = new CardiacBiometrics();

            for (CardiacBiometrics o : getCardiac) {
                String cardiacBioID = String.valueOf(o.getBiometricsId());
                if (cardiacBioID.equals(bioID)) {
                    bioNew = o;
                }
            }

            CardiacBiometrics bioCardiac = new CardiacBiometrics();
            bioMetric = port.fetchBiometrics(Integer.parseInt(bioID), memNumber);

            session.setAttribute("nyhaClass_text", String.valueOf(bioNew.getNyhaClass()));
            session.setAttribute("lvef_text", String.valueOf(bioNew.getLvef()));
            session.setAttribute("CardiacDetail_text", String.valueOf(bioNew.getDetail()));
            session.setAttribute("source_text", String.valueOf(bioNew.getSource()));

            session.setAttribute("nyhaClass", bioNew.getNyhaClass());
            session.setAttribute("lvef", bioNew.getLvef());
            session.setAttribute("CardiacDetail", bioNew.getDetail());

            if (bioNew.getDateReceived() != null) {
                session.setAttribute("dateReceived", DateTimeUtils.convertToYYYYMMDD(DateTimeUtils.getDateFromXMLGregorianCalendar(bioNew.getDateReceived())));
            } else {
                session.setAttribute("dateReceived", "");
            }
            session.setAttribute("providerNumber_text", String.valueOf(bioNew.getTreatingProvider()));
            session.setAttribute("source", bioNew.getSource());
            List<BiometricsTabs> icdMeaning = port.getBiometricMeaningFromValue(bioNew.getIcd10(), "114");
            if (!icdMeaning.isEmpty()) {
                session.setAttribute("icd10_text", icdMeaning.get(0).getLookupMeaning());
            }
            session.setAttribute("historyTableFlag", "4");
            session.setAttribute("ViewSpecificBiometric", bioNew);
        }

        if (bioName.equals("Diabetes")) {
            List<DiabetesBiometrics> getDiabetes = (List<DiabetesBiometrics>) session.getAttribute("getDiabetes");
            DiabetesBiometrics bioNew = new DiabetesBiometrics();

            for (DiabetesBiometrics o : getDiabetes) {
                String diabetesBioID = String.valueOf(o.getBiometricsId());
                if (diabetesBioID.equals(bioID)) {
                    bioNew = o;
                }
            }

            DiabetesBiometrics bioDiabetes = new DiabetesBiometrics();
            bioMetric = port.fetchBiometrics(Integer.parseInt(bioID), memNumber);

            session.setAttribute("diabetesHbAIC_text", String.valueOf(bioNew.getHbAIC()));
            session.setAttribute("diabetesRandomGlucose_text", String.valueOf(bioNew.getRandomGlucose()));
            session.setAttribute("diabetesFastingGlucose_text", String.valueOf(bioNew.getFastingGlucose()));
            session.setAttribute("diabetesTotalCholesterol_text", String.valueOf(bioNew.getTotalCholesterol()));
            session.setAttribute("diabetesldlc_text", String.valueOf(bioNew.getLdlcMol()));
            session.setAttribute("diabeteshdlc_text", String.valueOf(bioNew.getHdlcMol()));
            session.setAttribute("diabetesTryglyseride_text", String.valueOf(bioNew.getTryglyseride()));
            session.setAttribute("diabetesProteinuria_text", String.valueOf(bioNew.getProteinurianGL()));
            session.setAttribute("diabetesTchdlRation_text", String.valueOf(bioNew.getTchdlRatio()));
            session.setAttribute("diabetesDetail_text", String.valueOf(bioNew.getDetail()));
            session.setAttribute("source_text", String.valueOf(bioNew.getSource()));

            session.setAttribute("diabetesHbAIC", bioNew.getHbAIC());
            session.setAttribute("diabetesRandomGlucose", bioNew.getRandomGlucose());
            session.setAttribute("diabetesFastingGlucose", bioNew.getFastingGlucose());
            session.setAttribute("diabetesTotalCholesterol", bioNew.getTotalCholesterol());
            session.setAttribute("diabetesldlc", bioNew.getLdlcMol());
            session.setAttribute("diabeteshdlc", bioNew.getHdlcMol());
            session.setAttribute("diabetesTryglyseride", bioNew.getTryglyseride());
            session.setAttribute("diabetesProteinuria", bioNew.getProteinurianGL());
            session.setAttribute("diabetesTchdlRation", bioNew.getTchdlRatio());
            session.setAttribute("diabetesDetail", bioNew.getDetail());

            if (bioNew.getDateReceived() != null) {
                session.setAttribute("dateReceived", DateTimeUtils.convertToYYYYMMDD(DateTimeUtils.getDateFromXMLGregorianCalendar(bioNew.getDateReceived())));
            } else {
                session.setAttribute("dateReceived", "");
            }
            session.setAttribute("providerNumber_text", String.valueOf(bioNew.getTreatingProvider()));
            session.setAttribute("source", bioNew.getSource());
            List<BiometricsTabs> icdMeaning = port.getBiometricMeaningFromValue(bioNew.getIcd10(), "115");
            if (!icdMeaning.isEmpty()) {
                session.setAttribute("icd10_text", icdMeaning.get(0).getLookupMeaning());
            }
            session.setAttribute("historyTableFlag", "5");
            session.setAttribute("ViewSpecificBiometric", bioNew);
        }

        if (bioName.equals("Coronary")) {
            List<CoronaryBiometrics> getCoronary = (List<CoronaryBiometrics>) session.getAttribute("getCoronary");
            CoronaryBiometrics bioNew = new CoronaryBiometrics();

            for (CoronaryBiometrics o : getCoronary) {
                String coronaryBioID = String.valueOf(o.getBiometricsId());
                if (coronaryBioID.equals(bioID)) {
                    bioNew = o;
                }
            }

            CoronaryBiometrics bioCoronary = new CoronaryBiometrics();
            bioMetric = port.fetchBiometrics(Integer.parseInt(bioID), memNumber);

            session.setAttribute("CoronaryHbAIC_text", String.valueOf(bioNew.getHbAIC()));
            session.setAttribute("CoronaryTotalCholesterol_text", String.valueOf(bioNew.getTotalCholesterol()));
            session.setAttribute("CoronaryLdlc_text", String.valueOf(bioNew.getLdlcMol()));
            session.setAttribute("CoronaryHdlc_text", String.valueOf(bioNew.getHdlcMol()));
            session.setAttribute("tryglyseride_text", String.valueOf(bioNew.getTryglyserideMol()));
            session.setAttribute("CoronaryTryglyseride_text", String.valueOf(bioNew.getTryglyserideMol()));
            session.setAttribute("CoronaryTchdlRatio_text", String.valueOf(bioNew.getTcHdlRatio()));
            session.setAttribute("coronaryDetail_text", String.valueOf(bioNew.getDetail()));
            session.setAttribute("source_text", String.valueOf(bioNew.getSource()));

            session.setAttribute("CoronaryHbAIC", bioNew.getHbAIC());
            session.setAttribute("CoronaryTotalCholesterol", bioNew.getTotalCholesterol());
            session.setAttribute("CoronaryLdlc", bioNew.getLdlcMol());
            session.setAttribute("CoronaryHdlc", bioNew.getHdlcMol());
            session.setAttribute("tryglyseride", bioNew.getTryglyserideMol());
            session.setAttribute("CoronaryTchdlRatio", bioNew.getTcHdlRatio());
            session.setAttribute("coronaryDetail", bioNew.getDetail());

            if (bioNew.getDateReceived() != null) {
                session.setAttribute("dateReceived", DateTimeUtils.convertToYYYYMMDD(DateTimeUtils.getDateFromXMLGregorianCalendar(bioNew.getDateReceived())));
            } else {
                session.setAttribute("dateReceived", "");
            }
            session.setAttribute("providerNumber_text", String.valueOf(bioNew.getTreatingProvider()));
            session.setAttribute("source", bioNew.getSource());
            List<BiometricsTabs> icdMeaning = port.getBiometricMeaningFromValue(bioNew.getIcd10(), "116");
            if (!icdMeaning.isEmpty()) {
                session.setAttribute("icd10_text", icdMeaning.get(0).getLookupMeaning());
            }
            session.setAttribute("historyTableFlag", "6");
            session.setAttribute("ViewSpecificBiometric", bioNew);
        }

        if (bioName.equals("Bronchiectasis")) {
            List<CopdBiometrics> getCopd = (List<CopdBiometrics>) session.getAttribute("getCopd");
            CopdBiometrics bioNew = new CopdBiometrics();

            for (CopdBiometrics o : getCopd) {
                String copdBioID = String.valueOf(o.getBiometricsId());
                if (copdBioID.equals(bioID)) {
                    bioNew = o;
                }
            }

            CopdBiometrics bioCopd = new CopdBiometrics();
            bioMetric = port.fetchBiometrics(Integer.parseInt(bioID), memNumber);

            session.setAttribute("fev1Predicted_text", String.valueOf(bioNew.getFev1Predicted()));
            session.setAttribute("fev1FVC_text", String.valueOf(bioNew.getFev1FVC()));
            session.setAttribute("dyspnea_text", String.valueOf(bioNew.getDyspneaScale()));
            session.setAttribute("COPDdetail_text", String.valueOf(bioNew.getDetail()));
            session.setAttribute("source_text", String.valueOf(bioNew.getSource()));

            session.setAttribute("fev1Predicted", bioNew.getFev1Predicted());
            session.setAttribute("fev1FVC", bioNew.getFev1FVC());
            session.setAttribute("dyspnea", bioNew.getDyspneaScale());
            session.setAttribute("COPDdetail", bioNew.getDetail());

            if (bioNew.getDateReceived() != null) {
                session.setAttribute("dateReceived", DateTimeUtils.convertToYYYYMMDD(DateTimeUtils.getDateFromXMLGregorianCalendar(bioNew.getDateReceived())));
            } else {
                session.setAttribute("dateReceived", "");
            }
            session.setAttribute("providerNumber_text", String.valueOf(bioNew.getTreatingProvider()));
            session.setAttribute("source", bioNew.getSource());
            List<BiometricsTabs> icdMeaning = port.getBiometricMeaningFromValue(bioNew.getIcd10(), "117");
            if (!icdMeaning.isEmpty()) {
                session.setAttribute("icd10_text", icdMeaning.get(0).getLookupMeaning());
            }
            session.setAttribute("historyTableFlag", "7");
            session.setAttribute("ViewSpecificBiometric", bioNew);
        }

        if (bioName.equals("Hyperlipidaemia")) {
            List<HyperlipidaemiaBiometrics> getHyperlipidaemia = (List<HyperlipidaemiaBiometrics>) session.getAttribute("getHyperlipidaemia");
            HyperlipidaemiaBiometrics bioNew = new HyperlipidaemiaBiometrics();

            for (HyperlipidaemiaBiometrics o : getHyperlipidaemia) {
                String hyperlipidaemiaBioID = String.valueOf(o.getBiometricsId());
                if (hyperlipidaemiaBioID.equals(bioID)) {
                    bioNew = o;
                }
            }

            HyperlipidaemiaBiometrics bioHyperlipidaemia = new HyperlipidaemiaBiometrics();
            bioMetric = port.fetchBiometrics(Integer.parseInt(bioID), memNumber);

            session.setAttribute("totalCholesterol_text", String.valueOf(bioNew.getTotalCholesterol()));
            session.setAttribute("ldlc_text", String.valueOf(bioNew.getLdlcMol()));
            session.setAttribute("triglyceride_text", String.valueOf(bioNew.getTriglycerideMol()));
            session.setAttribute("tcHdlRatio_text", String.valueOf(bioNew.getTcHdlRatio()));
            session.setAttribute("hyperlipaemiaDetail_text", String.valueOf(bioNew.getDetail()));
            session.setAttribute("hdlc_text", String.valueOf(bioNew.getHdlcMol()));
            session.setAttribute("framingham_text", String.valueOf(bioNew.getFramingham()));
            session.setAttribute("score_text", String.valueOf(bioNew.getScore()));
            session.setAttribute("source_text", String.valueOf(bioNew.getSource()));

            session.setAttribute("totalCholesterol", bioNew.getTotalCholesterol());
            session.setAttribute("ldlc", bioNew.getLdlcMol());
            session.setAttribute("triglyceride", bioNew.getTriglycerideMol());
            session.setAttribute("tcHdlRatio", bioNew.getTcHdlRatio());
            session.setAttribute("hyperlipaemiaDetail", bioNew.getDetail());
            session.setAttribute("hdlc", bioNew.getHdlcMol());
            session.setAttribute("framingham", bioNew.getFramingham());
            session.setAttribute("score", bioNew.getScore());
            if (bioNew.getDateReceived() != null) {
                session.setAttribute("dateReceived", DateTimeUtils.convertToYYYYMMDD(DateTimeUtils.getDateFromXMLGregorianCalendar(bioNew.getDateReceived())));
            } else {
                session.setAttribute("dateReceived", "");
            }
            session.setAttribute("providerNumber_text", String.valueOf(bioNew.getTreatingProvider()));
            session.setAttribute("source", bioNew.getSource());
            List<BiometricsTabs> icdMeaning = port.getBiometricMeaningFromValue(bioNew.getIcd10(), "118");
            if (!icdMeaning.isEmpty()) {
                session.setAttribute("icd10_text", icdMeaning.get(0).getLookupMeaning());
            }
            session.setAttribute("historyTableFlag", "8");
            session.setAttribute("ViewSpecificBiometric", bioNew);
        }

        if (bioName.equals("MajorDepression")) {
            List<MajorDepressionBiometrics> getDepression = (List<MajorDepressionBiometrics>) session.getAttribute("getDepression");
            MajorDepressionBiometrics bioNew = new MajorDepressionBiometrics();

            for (MajorDepressionBiometrics o : getDepression) {
                String majorDepressionBioID = String.valueOf(o.getBiometricsId());
                if (majorDepressionBioID.equals(bioID)) {
                    bioNew = o;
                }
            }

            MajorDepressionBiometrics bioMajorDepression = new MajorDepressionBiometrics();
            bioMetric = port.fetchBiometrics(Integer.parseInt(bioID), memNumber);

            session.setAttribute("hamdScore_text", String.valueOf(bioNew.getHamdScore()));
            session.setAttribute("depressionDetail_text", String.valueOf(bioNew.getDetail()));
            session.setAttribute("source_text", String.valueOf(bioNew.getSource()));

            session.setAttribute("hamdScore", bioNew.getHamdScore());
            session.setAttribute("depressionDetail", bioNew.getDetail());

            if (bioNew.getDateReceived() != null) {
                session.setAttribute("dateReceived", DateTimeUtils.convertToYYYYMMDD(DateTimeUtils.getDateFromXMLGregorianCalendar(bioNew.getDateReceived())));
            } else {
                session.setAttribute("dateReceived", "");
            }
            session.setAttribute("providerNumber_text", String.valueOf(bioNew.getTreatingProvider()));
            session.setAttribute("source", bioNew.getSource());
            List<BiometricsTabs> icdMeaning = port.getBiometricMeaningFromValue(bioNew.getIcd10(), "119");
            if (!icdMeaning.isEmpty()) {
                session.setAttribute("icd10_text", icdMeaning.get(0).getLookupMeaning());
            }
            session.setAttribute("historyTableFlag", "9");
            session.setAttribute("ViewSpecificBiometric", bioNew);
        }

        if (bioName.equals("General")) {
            List<GeneralBiometrics> getGeneral = (List<GeneralBiometrics>) session.getAttribute("getGeneral");
            List<GeneralBiometrics> generalList = new ArrayList<GeneralBiometrics>();

            for (GeneralBiometrics o : getGeneral) {
                String generalBioID = String.valueOf(o.getBiometricsId());
                if (generalBioID.equals(bioID)) {
                    generalList.add(o);
                }
            }

            GeneralBiometrics bioGeneral = new GeneralBiometrics();
            bioMetric = port.fetchBiometrics(Integer.parseInt(bioID), memNumber);

            session.setAttribute("dateReceived", generalList.get(0).getDateReceived());
            session.setAttribute("providerNumber_text", generalList.get(0).getTreatingProvider());
            session.setAttribute("source", generalList.get(0).getSource());
            List<BiometricsTabs> icdMeaning = port.getBiometricMeaningFromValue(generalList.get(0).getIcd10(), "0");
            if (!icdMeaning.isEmpty()) {
                session.setAttribute("icd10_text", icdMeaning.get(0).getLookupMeaning());
            }
            session.setAttribute("historyTableFlag", "10");
            session.setAttribute("ViewSpecificBiometric", generalList);
        }

        if (bioName.equals("Other")) {
            List<OtherBiometrics> getOther = (List<OtherBiometrics>) session.getAttribute("getOther");
            OtherBiometrics bioNew = new OtherBiometrics();

            for (OtherBiometrics o : getOther) {
                String otherBioID = String.valueOf(o.getBiometricsId());
                if (otherBioID.equals(bioID)) {
                    bioNew = o;
                }
            }
            OtherBiometrics bioOther = new OtherBiometrics();
            bioMetric = port.fetchBiometrics(Integer.parseInt(bioID), memNumber);

            if (bioNew.getDateReceived() != null) {
                session.setAttribute("dateReceived", DateTimeUtils.convertToYYYYMMDD(DateTimeUtils.getDateFromXMLGregorianCalendar(bioNew.getDateReceived())));
            } else {
                session.setAttribute("dateReceived", "");
            }
            session.setAttribute("providerNumber_text", String.valueOf(bioNew.getTreatingProvider()));
            session.setAttribute("source_text", String.valueOf(bioNew.getSource()));
            session.setAttribute("source", String.valueOf(bioNew.getSource()));
            session.setAttribute("otherDetails_text", String.valueOf(bioNew.getDetail()));
            session.setAttribute("otherDetails", String.valueOf(bioNew.getDetail()));
            List<BiometricsTabs> icdMeaning = port.getBiometricMeaningFromValue(bioNew.getIcd10(), bioNew.getIcd10TypeID());
            if (!icdMeaning.isEmpty()) {
                session.setAttribute("icd10_text", icdMeaning.get(0).getLookupMeaning());
            }
            session.setAttribute("historyTableFlag", "11");
            session.setAttribute("ViewSpecificBiometric", bioNew);
        }
        if (nextJSP.equals("/PDC/BiometricDetails.jsp")) {
            session.setAttribute("searchCalled", "showTable");
        } else if (nextJSP.equals("/biometrics/BiometricsView.jsp")) {
            session.setAttribute("showSummaryTable", "true");
        } else {
            System.out.println("NextJSP : " + nextJSP);
        }

        if (bioMetric != null && !bioMetric.isEmpty()) {
            session.setAttribute("bpSystolic", bioMetric.get(0).getBloodPressureSystolic());
            session.setAttribute("bpDiastolic", bioMetric.get(0).getBloodPressureDiastolic());
            session.setAttribute("weight", bioMetric.get(0).getWeight());
            session.setAttribute("length", bioMetric.get(0).getHeight());
            session.setAttribute("bmi", bioMetric.get(0).getBmi());
            session.setAttribute("alcoholConsumption", bioMetric.get(0).getAlcoholUnitsPerWeek());
            session.setAttribute("currentSmoker_text", bioMetric.get(0).getCurrentSmoker());
            session.setAttribute("smokingQuestion", bioMetric.get(0).getCigarettesPerDay());
            session.setAttribute("exSmoker_text", bioMetric.get(0).getExSmoker());
            session.setAttribute("stopped", bioMetric.get(0).getYearsSinceStopped());
            session.setAttribute("exerciseQuestion", bioMetric.get(0).getExercisePerWeek());

            //For updating detection in CaptureBiometrics
            session.setAttribute("bpSystolic_text", bioMetric.get(0).getBloodPressureSystolic());
            session.setAttribute("bpDiastolic_text", bioMetric.get(0).getBloodPressureDiastolic());
            session.setAttribute("weight_text", bioMetric.get(0).getWeight());
            session.setAttribute("length_text", bioMetric.get(0).getHeight());
            session.setAttribute("bmi_text", bioMetric.get(0).getBmi());
            session.setAttribute("alcoholConsumption_text", String.valueOf(bioMetric.get(0).getAlcoholUnitsPerWeek()));
            session.setAttribute("currentSmoker_text", bioMetric.get(0).getCurrentSmoker());
            session.setAttribute("smokingQuestion_text", bioMetric.get(0).getCigarettesPerDay());
            session.setAttribute("exSmoker_text", bioMetric.get(0).getExSmoker());
            session.setAttribute("stopped_text", bioMetric.get(0).getYearsSinceStopped());
            session.setAttribute("exerciseQuestion_text", String.valueOf(bioMetric.get(0).getExercisePerWeek()));

            session.setAttribute("UpdateBiometrics", "true"); //Flag to update instead of creatings
            session.setAttribute("trueAntiReload", "trueAntiReload");
            session.setAttribute("oldICD", session.getAttribute("icd10_text"));//To Fix the G45.0 to I20.0 conflict
        }

        session.setAttribute("editButtonShow", editBTNshow);

        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "ViewBiometricCommand";
    }
}

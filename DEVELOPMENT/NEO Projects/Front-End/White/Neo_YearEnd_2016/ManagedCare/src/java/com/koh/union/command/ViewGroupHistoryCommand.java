/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.union.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.UnionRepresentative;

/**
 *
 * @author shimanem
 */
public class ViewGroupHistoryCommand extends NeoCommand {

    private static final Logger logger = Logger.getLogger(ViewGroupHistoryCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        logger.info(">> ***** Union GroupHistory Command ***** <<");
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();

        String unionNumber = String.valueOf(session.getAttribute("unionNumber"));
        String unionName = String.valueOf(session.getAttribute("unionName"));
        String unionID = String.valueOf(session.getAttribute("unionID"));

        logger.log(Level.INFO, ">> *** Union Number : {0}", unionNumber);
        logger.log(Level.INFO, ">> *** Union Name : {0}", unionName);
        logger.log(Level.INFO, ">> *** Union ID : {0}", unionID);

        try {

            ArrayList<UnionRepresentative> groupHistoryresults = (ArrayList<UnionRepresentative>) port.retrieveUnionGroupHistory(Integer.parseInt(unionID));
            if (groupHistoryresults != null) {
                session.setAttribute("unionGroupHistory", groupHistoryresults);
                String nextJSP = "/Union/unionGroupHistory.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);
            } else {
                session.setAttribute("unionGroupHistory", null);
                String nextJSP = "/Union/unionGroupHistory.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                dispatcher.forward(request, response);
            }

        } catch (ServletException ex) {
            Logger.getLogger(ViewUnionDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ViewUnionDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewGroupHistoryCommand";
    }

}

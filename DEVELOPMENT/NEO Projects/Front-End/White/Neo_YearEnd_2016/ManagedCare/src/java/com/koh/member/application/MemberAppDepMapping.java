/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application;

import com.koh.employer.command.TabUtils;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import neo.manager.MemberApplication;
import neo.manager.MemberDependantApp;
import neo.manager.NeoUser;

/**
 *
 * @author yuganp
 */
public class MemberAppDepMapping {

    private static final String[][] FIELD_MAPPINGS = {
        {"memberAppNumber","Application Number","no", "int", "MemberApp","ApplicationNumber"},
        {"memberAppDepNumber","Application Number","no", "int", "MemberApp","dependentNumber"},
        {"MemberDependantApp_dependant_type","Dependant Type","yes", "int", "MemberApp","dependantType"},
        {"MemberDependantApp_title","Title","no", "int", "MemberApp","Title"},
        {"MemberDependantApp_initials","Initials","yes", "str", "MemberApp","Initials","0","10"},
        {"MemberDependantApp_firstName","First Name","yes", "OneWordStr", "MemberApp","FirstName","0","50"},
        {"MemberDependantApp_secondName","Preferred Name","no", "str", "MemberApp","SecondName","0","30"},
        {"MemberDependantApp_lastName","Surname","yes", "str", "MemberApp","LastName","0","30"},
        {"MemberDependantApp_preferredName","Preferred Name","no", "str", "MemberApp","PreferredName","0","50"},
        {"MemberDependantApp_gender","Gender","yes", "int", "MemberApp","Gender"},
        {"MemberDependantApp_DateOfBirth","Date Of Birth","yes", "date", "MemberApp","DateOfBirth"},
        {"MemberDependantApp_identificationType","Identification Type","no", "int", "MemberApp","IdentificationType"},
        {"MemberDependantApp_identificationNumber","Identification Number","yes", "str", "MemberApp","IdentificationNumber","5","13"},
        {"MemberDependantApp_relationshipType","Relationship Type","no", "int", "MemberApp","relationshipType"},
        {"MemberDependantApp_relationshipOther","Relationship Other","no", "str", "MemberApp","relationshipOther","0","50"},
        {"MemberDependantApp_maritalStatus","Married?","no", "int", "MemberApp","marriedInd"},
        {"MemberDependantApp_disabledInd","Disabled?","no", "int", "MemberApp","disabledInd"},
        {"MemberDependantApp_studentInd","Student?","no", "int", "MemberApp","studentInd"},
        {"MemberDependantApp_financiallyDependantInd","Financially Dependant?","no", "int", "MemberApp","financiallyDependantInd"},
        {"MemberDependantApp_earnIncomeInd","Earn Income?","no", "int", "MemberApp","earnIncomeInd"},
        {"MemberDependantApp_monthlyIncome","Monthly Income","no", "int", "MemberApp","monthlyIncome"},
        {"MemberDependantApp_height","Height","no", "int", "MemberApp","height"},
        {"MemberDependantApp_weight","Weight","no", "int", "MemberApp","weight"},
        {"MemberDependantApp_principalPracticeName","Practice Name","no", "str", "MemberApp","nominatedName","0","50"},
        {"MemberDependantApp_principalNonPracticeTel","Telephone Number","no", "str", "MemberApp","nominatedTel","0","15"},
        {"MemberDependantApp_principalPracticeNumber","Practice Number","no", "str", "MemberApp","nominatedDoctorNumber"}
    };
    
    private static final String[][] FIELD_MAPPINGS_SECHABA = {
        {"memberAppNumber","Application Number","no", "int", "MemberApp","ApplicationNumber"},
        {"memberAppDepNumber","Application Number","no", "int", "MemberApp","dependentNumber"},
        {"MemberDependantApp_dependant_type","Dependant Type","yes", "int", "MemberApp","dependantType"},
        {"MemberDependantApp_title","Title","no", "int", "MemberApp","Title"},
        {"MemberDependantApp_initials","Initials","yes", "str", "MemberApp","Initials","0","10"},
        {"MemberDependantApp_firstName","First Name","yes", "OneWordStr", "MemberApp","FirstName","0","50"},
        {"MemberDependantApp_secondName","Preferred Name","no", "str", "MemberApp","SecondName","0","30"},
        {"MemberDependantApp_lastName","Surname","yes", "str", "MemberApp","LastName","0","30"},
        {"MemberDependantApp_preferredName","Preferred Name","no", "str", "MemberApp","PreferredName","0","50"},
        {"MemberDependantApp_gender","Gender","yes", "int", "MemberApp","Gender"},
        {"MemberDependantApp_DateOfBirth","Date Of Birth","yes", "date", "MemberApp","DateOfBirth"},
        {"MemberDependantApp_identificationType","Identification Type","no", "int", "MemberApp","IdentificationType"},
        {"MemberDependantApp_identificationNumber","Identification Number","yes", "str", "MemberApp","IdentificationNumber","5","13"},
        {"MemberDependantApp_relationshipType","Relationship Type","no", "int", "MemberApp","relationshipType"},
        {"MemberDependantApp_relationshipOther","Relationship Other","no", "str", "MemberApp","relationshipOther","0","50"},
        {"MemberDependantApp_maritalStatus","Married?","no", "int", "MemberApp","marriedInd"},
        {"MemberDependantApp_disabledInd","Disabled?","no", "int", "MemberApp","disabledInd"},
        {"MemberDependantApp_studentInd","Student?","no", "int", "MemberApp","studentInd"},
        {"MemberDependantApp_financiallyDependantInd","Financially Dependant?","no", "int", "MemberApp","financiallyDependantInd"},
        {"MemberDependantApp_earnIncomeInd","Earn Income?","no", "int", "MemberApp","earnIncomeInd"},
        {"MemberDependantApp_monthlyIncome","Monthly Income","no", "int", "MemberApp","monthlyIncome"},
        {"MemberDependantApp_height","Height","no", "int", "MemberApp","height"},
        {"MemberDependantApp_weight","Weight","no", "int", "MemberApp","weight"},
        {"MemberDependantApp_principalPracticeName","Practice Name","no", "str", "MemberApp","nominatedName","0","50"},
        {"MemberDependantApp_principalNonPracticeTel","Telephone Number","no", "str", "MemberApp","nominatedTel","0","15"},
        {"MemberDependantApp_principalPracticeNumber","Practice Number","no", "str", "MemberApp","nominatedDoctorNumber"},
        {"MemberApp_network_IPA","Network IPA","no", "str", "MemberApp","networkIpa"},
        {"MemberDependantApp_doctorPracticeNumber","BHF Practice Number","no", "str", "MemberApp","doctorPracticeNumber"},
        {"MemberDependantApp_dentistPracticeNumber","BHF Practice Number","no", "str", "MemberApp","dentistPracticeNumber"},
        {"MemberDependantApp_optometristPracticeNumber","BHF Practice Number","no", "str", "MemberApp","optometristPracticeNumber"}
    };
    
    private static final Map<String,String[]> FIELD_MAP;

    static {
        HashMap hm = new HashMap<String,String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }

    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = null;
        if (request.getSession().getAttribute("Client").equals("Agility")) {
            errors  = new HashMap<String, String>();
            TabUtils.validate(request, errors, FIELD_MAPPINGS);
        } else if (request.getSession().getAttribute("Client").equals("Sechaba")) {
            errors  = new HashMap<String, String>();
            TabUtils.validate(request, errors, FIELD_MAPPINGS_SECHABA);
        }
        return errors;
    }

    public static MemberDependantApp getMemberAppDependant(HttpServletRequest request,NeoUser neoUser) {
        MemberDependantApp ma = (MemberDependantApp)TabUtils.getDTOFromRequest(request, MemberDependantApp.class, FIELD_MAPPINGS, "MemberApp");
        ma.setSecurityGroupId(neoUser.getSecurityGroupId());
        ma.setLastUpdatedBy(neoUser.getUserId());
        ma.setCreatedBy(neoUser.getUserId());
        return ma;
    }
    
    public static MemberDependantApp getMemberAppDependantSechaba(HttpServletRequest request,NeoUser neoUser) {
        MemberDependantApp ma = (MemberDependantApp)TabUtils.getDTOFromRequest(request, MemberDependantApp.class, FIELD_MAPPINGS_SECHABA, "MemberApp");
        ma.setSecurityGroupId(neoUser.getSecurityGroupId());
        ma.setLastUpdatedBy(neoUser.getUserId());
        ma.setCreatedBy(neoUser.getUserId());
        return ma;
    }

    public static void setMemberAppDependant(HttpServletRequest request, MemberDependantApp memberAppDep) {
        TabUtils.setRequestFromDTO(request, memberAppDep, FIELD_MAPPINGS, "MemberApp");
    }
    
    public static void setMemberAppDependantSechaba(HttpServletRequest request, MemberDependantApp memberAppDep) {
        TabUtils.setRequestFromDTO(request, memberAppDep, FIELD_MAPPINGS_SECHABA, "MemberApp");
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.command;

import com.koh.command.NeoCommand;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author johanl
 */
public class ReturnClaimDetails extends NeoCommand {

    private static final Logger logger = Logger.getLogger(ReturnClaimDetails.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();

        String onScreen = request.getParameter("onScreen");
        String previousJsp = "";

        if (onScreen != null && !onScreen.equalsIgnoreCase("")) {

            if (onScreen.equalsIgnoreCase("DetailedClaimLine.jsp")) {
                previousJsp = "/NClaims/ClaimHistory.jsp";

            } else if (onScreen.equalsIgnoreCase("ClaimHistory.jsp")) {
                previousJsp = "/NClaims/ViewClaims.jsp";

            } else if (onScreen.equalsIgnoreCase("ClaimsNotesDetails.jsp")){
                previousJsp = "/NClaims/ClaimHistory.jsp";
            }
            
            System.out.println("onScreen = " + onScreen + "\npreviousJsp = " + previousJsp);
            try {
                RequestDispatcher dispatcher = context.getRequestDispatcher(previousJsp);
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "ReturnClaimDetails";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.command;

import com.koh.command.NeoCommand;
import com.koh.utils.XmlUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoManagerBean;
import neo.manager.Option;
import neo.manager.OptionVersion;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author johanl
 */
public class GetProductOptionCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter out = null;
        String xmlStr = "";
        
        int prodId = Integer.parseInt(request.getParameter("prodId"));
        String currentOptions = request.getParameter("currentOptions");
        
        if (currentOptions == null || currentOptions.equals("null")) {
            currentOptions = "true";
        }
        
        System.out.println("currentOptions " + currentOptions);
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        List<Option> optlist = port.findOptionsForProduct(prodId);
        
        response.setHeader("Cache-Control", "no-cache");
        response.setContentType("text/xml");
        response.setStatus(200);
        @SuppressWarnings("static-access")
        Document doc = new XmlUtils().getDocument();

        Element root = doc.createElement("ProductOptions");
        doc.appendChild(root);
        
        if(currentOptions != null && currentOptions.equals("true")){
            List<OptionVersion> optionVersions = port.findOptionVersionsForProduct(new Integer(prodId));
                for (OptionVersion op : optionVersions) {
                    Element option = doc.createElement("Option");
                    String optionName = null;
                    
                    for (Option opt : optlist) {
                        if (opt.getOptionId() == op.getOptionId()) {
                            optionName = opt.getOptionName();
                            break;
                        }
                    }
                    
                    String optIdName = String.valueOf(op.getOptionId()) + "|" + optionName;
                    option.setTextContent(optIdName);
                            
                    root.appendChild(option);
                }
        } else{
            for (Option opt : optlist) {
                Element option = doc.createElement("Option");
                
                String optIdName = String.valueOf(opt.getOptionId()) + "|" + opt.getOptionName();
                option.setTextContent(optIdName);
                
                root.appendChild(option);
            }
        }
        
        try {
            out = response.getWriter();

            try {
                xmlStr = XmlUtils.getXMLString(doc);
            } catch (IOException ex) {
                ex.printStackTrace();
                xmlStr = "";

            }
            //System.out.println("product option xml = "+xmlStr);
            out.println(xmlStr);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetProductOptionCommand";
    }
}

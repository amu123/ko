/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author princes
 */
public class QuestionnaireHistotyTab extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String buttonCommand1;
    private String buttonDisplay1;
    private String buttonCommand2;
    private String buttonDisplay2;
    private String disabledDisplay;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        HashMap<Integer, String> dates = (HashMap<Integer, String>) session.getAttribute("dates");

        String view = (String) session.getAttribute("tapView");

        String displayedView = (String) session.getAttribute("displayed");
        System.out.println("---tapView in tab:" + view);
        System.out.println("---displayed in tab:" + displayedView);

        if (dates != null) {
            if (!view.equals("single")) {

                try {
                    String qDate = (String) session.getAttribute("qDate");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                    Date mdate = new Date();
                    Date date = sdf.parse(qDate);
                    mdate.setTime(date.getTime());

                    /*DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                    Date date = new Date();
                    dateFormat.format(date);*/
                    out.println("<table cellpadding=\"0\" cellspacing=\"0\">");

                    if (displayedView.equals("current")) {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand1 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay1 + "-"
                                + dateFormat.format(mdate) + "</button></td>");
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand2 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay2 + "</button></td>");
                        System.out.println("---In current:" + displayedView);
                    } else {

                        System.out.println("---In old:" + displayedView);
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand1 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay1 + "-"
                                + dateFormat.format(mdate) + "</button></td>");
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand2 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay2 + "</button></td>");
                    }
                    out.println("</table>");

                } catch (ParseException e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                } catch (IOException e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                }
            }
        }
        return super.doEndTag();
    }

    public void setButtonCommand1(String buttonCommand1) {
        this.buttonCommand1 = buttonCommand1;
    }

    public void setButtonDisplay1(String buttonDisplay1) {
        this.buttonDisplay1 = buttonDisplay1;
    }

    public void setButtonCommand2(String buttonCommand2) {
        this.buttonCommand2 = buttonCommand2;
    }

    public void setButtonDisplay2(String buttonDisplay2) {
        this.buttonDisplay2 = buttonDisplay2;
    }

    public void setDisabledDisplay(String disabledDisplay) {
        this.disabledDisplay = disabledDisplay;
    }
}

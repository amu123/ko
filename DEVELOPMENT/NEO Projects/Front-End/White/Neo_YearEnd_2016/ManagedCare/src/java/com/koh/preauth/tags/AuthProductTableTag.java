/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import java.math.BigDecimal;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthNappiProduct;

/**
 *
 * @author martins
 */
public class AuthProductTableTag extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String sessionAttribute;
    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        List<AuthNappiProduct> dbproductList = null;

        try {
            out.println("<td colspan=\"6\"><table width=\"800\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            try {
                dbproductList = NeoCommand.service.getNeoManagerBeanPort().getNappiUpdate(session.getAttribute("updateAuthNumber").toString().trim());
            } catch (Exception e) {
                System.out.println("No nappi products found");
            }

            List<AuthNappiProduct> productList = (List<AuthNappiProduct>) session.getAttribute("productNappiList");

            if (productList != null) {
                out.println("<tr><th align=\"left\">Product Code</th><th align=\"left\">Product Description</th>"
                        + "<th align=\"left\">Quantity</th><th align=\"left\">Unit Price</th><th align=\"left\">Price</th><th align=\"left\">Status</th>"
                        + "<th align=\"left\">Date From</th><th align=\"left\">Date To</th>"
                        + "<th align=\"left\">Remove</th><th align=\"left\">Modify</th></tr>");
                for (int i = 1; i <= productList.size(); i++) {
                    AuthNappiProduct anp = productList.get(i - 1);
                    double n = anp.getUnitPrice();
                    BigDecimal rounded = new BigDecimal(n);
                    rounded = rounded.setScale(2, BigDecimal.ROUND_HALF_UP);
                    out.println("<tr id=\"anpRow" + anp.getNappiCode() + i + "\"><form>");
                    out.println("<td><label class=\"label\">" + anp.getNappiCode() + "</label></td>");
                    out.println("<td><label class=\"label\">" + anp.getProductDesc() + "</label></td>");
                    out.println("<td><label class=\"label\">" + anp.getQuantity() + "</label></td>");
                    out.println("<td><label class=\"label\">" + "R" + rounded + "</label></td>");
                    out.println("<td><label class=\"label\">" + "R" + anp.getRand() + "</label></td>");
//                    out.println("<td><label class=\"label\">" + anp.getICD10()+ "</label></td>");
                    out.println("<td><label class=\"label\">" + anp.getStatus() + "</label></td>");
                    out.println("<td><label class=\"label\">" + anp.getDateFrom() + "</label></td>");
                    out.println("<td><label class=\"label\">" + anp.getDateTo() + "</label></td>");
                    out.println("<td><button type=\"button\" onClick=\"RemoveProductNappiFromList(" + i + ");\" >Remove</button></td>");
                    out.println("<td><button type=\"button\" onClick=\"ModProductNappiList(" + i + ");\" >Modify</button></td>");
                    out.println("</form></tr>");
                }
            }

            if (session.getAttribute("updateAuthNumber") != null) {
                System.out.println("### updateAuthNumber " + session.getAttribute("updateAuthNumber"));
                if (dbproductList != null) {
                    if (productList == null) {
                        out.println("<tr><th align=\"left\">Product Code</th><th align=\"left\">Product Description</th>"
                                + "<th align=\"left\">Quantity</th><th align=\"left\">Unit Price</th><th align=\"left\">Price</th><th align=\"left\">Status</th>"
                                + "<th align=\"left\">Date From</th><th align=\"left\">Date To</th>"
                                + "<th align=\"left\">Remove</th><th align=\"left\">Modify</th></tr>");
                    }
                    for (int i = 1; i <= dbproductList.size(); i++) {
                        AuthNappiProduct anp = dbproductList.get(i - 1);
                        double n = anp.getUnitPrice();
                        BigDecimal rounded = new BigDecimal(n);
                        rounded = rounded.setScale(2, BigDecimal.ROUND_HALF_UP);
                        //out.println("<tr id=\"anpRow" + anp.getNappiCode() + i + "\"><form>");
                        out.println("<td><label class=\"label\">" + anp.getNappiCode() + "</label></td>");
                        out.println("<td><label class=\"label\">" + anp.getProductDesc() + "</label></td>");
                        out.println("<td><label class=\"label\">" + anp.getQuantity() + "</label></td>");
                        out.println("<td><label class=\"label\">" + "R" + rounded + "</label></td>");
                        out.println("<td><label class=\"label\">" + "R" + anp.getRand() + "</label></td>");
//                    out.println("<td><label class=\"label\">" + anp.getICD10()+ "</label></td>");
                        out.println("<td><label class=\"label\">" + anp.getStatus() + "</label></td>");
                        out.println("<td><label class=\"label\">" + anp.getDateFrom() + "</label></td>");
                        out.println("<td><label class=\"label\">" + anp.getDateTo() + "</label></td>");
                        out.println("<td><button type=\"button\" onClick=\"RemoveProductNappiFromList(" + i + ");\" >Remove</button></td>");
                        out.println("<td><button type=\"button\" onClick=\"ModProductNappiList(" + i + ");\" >Modify</button></td>");
                        out.println("</form></tr>");
                    }
                }
            }
            out.println("</table></td>");

        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberSearchTable tag", ex);
        }

        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

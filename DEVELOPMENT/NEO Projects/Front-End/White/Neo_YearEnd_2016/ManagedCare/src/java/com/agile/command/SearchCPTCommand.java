/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.command;

import com.koh.auth.dto.ProcedureSearchResult;
import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Procedure;
import neo.manager.ProcedureSearchCriteria;

/**
 *
 * @author johanl
 */
public class SearchCPTCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();

        String code = "" + session.getAttribute("code");
        String description = "" + session.getAttribute("description");

        ProcedureSearchCriteria search = new ProcedureSearchCriteria();
        if (code != null && !code.equals(""))
            search.setCode(code);
        if (description != null && !description.equals(""))
            search.setDescription(description);

        ArrayList<ProcedureSearchResult> procedures = new ArrayList<ProcedureSearchResult>();
        List<Procedure> procList = new ArrayList<Procedure>();
        try {
           procList = service.getNeoManagerBeanPort().findAllProceduresByCriteria(search);

           for (Procedure proc : procList)
           {
             ProcedureSearchResult procedure = new ProcedureSearchResult();
             procedure.setCode(proc.getCode());
             procedure.setDescription(proc.getDescription());
             procedures.add(procedure);
           }

           if (procList.size() == 0) {
               request.setAttribute("searchProcedureResultMessage", "No results found.");
           } else if (procList.size() >= 100) {
               request.setAttribute("searchProcedureResultMessage", "Only first 100 results shown. Please refine search.");
           } else {
               request.setAttribute("searchProcedureResultMessage", null);
           }

        } catch (Exception e) {
            e.printStackTrace();
        }

        request.setAttribute("searchProcedureResult", procedures);

        try {
            String nextJSP = "/PreAuth/AuthCPTSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SearchCPTCommand";
    }

}

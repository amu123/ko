/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.util.ArrayList;
import java.util.Collection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AddressDetails;
import neo.manager.ContactDetails;
import neo.manager.EntityPaymentRunDetails;
import neo.manager.NeoManagerBean;
import neo.manager.PaymentRunSearchCriteria;
import org.apache.log4j.Logger;

/**
 *
 * @author gerritr
 */
public class ProductIDSelectionCommand extends NeoCommand {
    private Logger log = Logger.getLogger(ViewMemberAddressDetailsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        log.info("Inside ProductIDSelectionCommand");
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String nextJSP = "";
        String providerOrMember = String.valueOf(session.getAttribute("ProviderOrMember"));
        String ProdSelectedIndex = String.valueOf(session.getAttribute("ProductIDSelection_text"));
        if(providerOrMember != null && providerOrMember.equalsIgnoreCase("Provider"))
        {
            nextJSP = "/Claims/PracticePaymentDetails.jsp";
            PaymentRunSearchCriteria payRunProvider = (PaymentRunSearchCriteria) session.getAttribute("payRun");
            ArrayList<EntityPaymentRunDetails> paymentRunDetailsListProvider = (ArrayList<EntityPaymentRunDetails>) port.getPaymentRunDetailForEntity(payRunProvider, ProdSelectedIndex);
            session.setAttribute("practicePaymentRunDetails", paymentRunDetailsListProvider);
            session.setAttribute("ShowTable", "true");
            System.out.println("paymentRunDetailsListProvider - size : " + paymentRunDetailsListProvider.size());
            System.out.println("payRunProvider: " + payRunProvider);
            System.out.println("ProdSelectedIndex: " + ProdSelectedIndex);
        }
        else
        {
            String coverNumber = String.valueOf(session.getAttribute("ProductID_CoverNumber"));
            nextJSP = "/Claims/MemberPaymentDetails.jsp";
            ArrayList<EntityPaymentRunDetails> paymentRunDetailsList;
            PaymentRunSearchCriteria payRun = new PaymentRunSearchCriteria();
            payRun.setEntityType("Insured Person");
            payRun.setEntityNo(coverNumber);
            paymentRunDetailsList = (ArrayList<EntityPaymentRunDetails>) port.getPaymentRunDetailForEntity(payRun, ProdSelectedIndex);

            session.setAttribute("memberPaymentRunDetails", paymentRunDetailsList);
        }
        session.setAttribute("ProductIDTypeSelected", ProdSelectedIndex);
        session.removeAttribute("ProductIDSelection_text");
        session.removeAttribute("ProductIDSelectiontext");
        

        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch(Exception ex) {

            ex.printStackTrace();
        }
        
        return null;
    }

    @Override
    public String getName() {
        return "ProductIDSelectionCommand";
    }
}

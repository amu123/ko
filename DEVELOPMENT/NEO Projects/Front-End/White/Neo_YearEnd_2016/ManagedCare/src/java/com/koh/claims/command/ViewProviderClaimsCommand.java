/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Claim;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ViewProviderClaimsCommand extends NeoCommand {
    
    private Logger logger = Logger.getLogger(ViewProviderClaimsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        logger.info("Inside ViewProviderClaimsCommand");
        System.out.println("Inside ViewProviderClaimsCommand");
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();

        String index = request.getParameter("listIndex");
        session.setAttribute("listIndex", index);

        List<Claim> listClaims = (List<Claim>) session.getAttribute("listOfClaims");
        if (index != null) {
            int in = new Integer(index);

            //System.out.println("Size : In Command" + listClaims.size());
            Claim selectedClaim = listClaims.get(in);
            session.setAttribute("selected", selectedClaim);
            logger.info("And the selected claim is " + selectedClaim.getClaimId());
        }

        logger.info("The list of claims are  " + listClaims.size());


        try {

           // String nextJSP = "/Claims/PracticeClaimsDetails.jsp";
            String nextJSP = "/Claims/PracticeClaimsHistoryDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        }catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    public String getName() {

        return "ViewProviderClaimsCommand";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.tags;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.Claim;
import neo.manager.ClaimCCSearchCriteria;
import neo.manager.ClaimSearchResult;
import neo.manager.ClaimsBatch;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.ProviderDetails;
import neo.manager.Security;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletRequest;
import neo.manager.CcClaimSearchResult;

/**
 *
 * @author josephm
 */
public class ViewMemberClaimsTable extends TagSupport {

    private String commandName;
    private String javaScript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {

        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        ServletRequest request = pageContext.getRequest();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        Security _secure = new Security();
        _secure.setCreatedBy(user.getUserId());
        _secure.setLastUpdatedBy(user.getUserId());
        _secure.setSecurityGroupId(2);

        String onScreen = "";
        int benefitId = 0;
        int dependantCode = -1;

        System.out.println("entered ViewMemberClaimsTable");
        String coverNumber = (String) session.getAttribute("policyHolderNumber");
        System.out.println("coverNumber " + coverNumber);
        String idNumber = "" + session.getAttribute("idNumber");
        System.out.println("idNumber " + idNumber);
        String practiceNumber = "";

        CoverDetails details = port.getCoverDetailsByCoverIdNumber(idNumber);
        ClaimCCSearchCriteria search = new ClaimCCSearchCriteria();

        //set cover number
        search.setCoverNumber(coverNumber);

        //Benefit to claims drilldown
        if (session.getAttribute("onBenefitScreen") != null) {

            onScreen = "" + session.getAttribute("onBenefitScreen");
            benefitId = Integer.parseInt("" + session.getAttribute("benId"));
            dependantCode = Integer.parseInt("" + session.getAttribute("depCode"));
            System.out.println("BCDD: onScreen = " + onScreen);
            System.out.println("BCDD: benefitId = " + benefitId);
            System.out.println("BCDD: dependantCode = " + dependantCode);
            search.setDependentCode(dependantCode);
            search.setBenefitId(benefitId);


        } else {
            String treatFrom = "";
            String treatTo = "";
            //set service dates
            Calendar c1 = Calendar.getInstance();
            c1.add(Calendar.MONTH, -6); // substract 6 month

            Calendar calendar = Calendar.getInstance();

            try {
                String today = dateFormat.format(calendar.getTime());
                Date now = dateFormat.parse(today);
                XMLGregorianCalendar xmlNow = DateTimeUtils.convertDateToXMLGregorianCalendar(now);
                calendar.add(Calendar.MONTH, -6);
                String sixLater = dateFormat.format(calendar.getTime());
                Date sixMonths = dateFormat.parse(sixLater);
                XMLGregorianCalendar xmlSixMonthsLater = DateTimeUtils.convertDateToXMLGregorianCalendar(sixMonths);
                search.setServiceDateFrom(xmlSixMonthsLater);
                search.setServiceDateTo(xmlNow);

            } catch (Exception e) {
                e.printStackTrace();
            }
            
            treatFrom = request.getParameter("treatmentFrom");
            treatTo = request.getParameter("treatmentTo");
            
            if (treatFrom != null && treatTo != null) {

                Date tFrom = null;
                Date tTo = null;
                XMLGregorianCalendar xTreatFrom = null;
                XMLGregorianCalendar xTreatTo = null;

                try {
                    tFrom = dateFormat.parse(treatFrom);
                    tTo = dateFormat.parse(treatTo);

                    xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                    xTreatTo = DateTimeUtils.convertDateToXMLGregorianCalendar(tTo);

                    search.setServiceDateFrom(xTreatFrom);
                    search.setServiceDateTo(xTreatTo);

                } catch (java.text.ParseException ex) {
                    Logger.getLogger(ViewMemberClaimsTable.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            practiceNumber = request.getParameter("practiceNumber_text");
            if (practiceNumber != null && !practiceNumber.equalsIgnoreCase("")) {
                search.setPracticeNumber(practiceNumber);
            }

        }

        CcClaimSearchResult batch = port.findCCClaimBySearchCriteria(search, _secure);
        int count = 0;
        List<Claim> claims = new ArrayList<Claim>();
        try {
            out.println("<label class=\"header\">Claims Details</label></br></br>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.print("<tr>");
            out.print("<th scope=\"col\">Claim Number</th>");
            out.print("<th scope=\"col\">Account Number</th>");
            out.print("<th scope=\"col\">Practice Number</th>");
            out.print("<th scope=\"col\">Practice Name</th>");
            out.print("<th scope=\"col\">Service Provider</th>");
            out.print("<th scope=\"col\">Service Provider Name</th>");
            out.print("<th scope=\"col\">Discipline Type</th>");
            out.print("<th scope=\"col\">Member Number</th>");
            out.print("<th scope=\"col\">Service Date From</th>");
            out.print("<th scope=\"col\">Service Date To</th>");
            out.print("<th scope=\"col\">Action</th>");
            out.print("</tr>");


            for (ClaimsBatch claim : batch.getBatchs()) {

                for (Claim list_claim : claim.getClaims()) {
                    String catagory = port.getValueForId(59, list_claim.getClaimCategoryTypeId() + "");
                    ProviderDetails practice = port.getPracticeDetailsForEntityByNumber(list_claim.getPracticeNo());
                    ProviderDetails practice2 = port.getPracticeDetailsForEntityByNumber(list_claim.getServiceProviderNo());

                    out.print("<tr>");
                    out.print("<td><label class=\"label\">" + list_claim.getClaimId() + "</label></td>");
                    if (list_claim.getPatientRefNo() != null) {
                        out.print("<td><label class=\"label\">" + list_claim.getPatientRefNo() + "</label></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("<td><label class=\"label\">" + list_claim.getPracticeNo() + "</label></td>");
                    out.print("<td><label class=\"label\">" + practice.getPracticeName() + "</label></td>");
                    out.print("<td><label class=\"label\">" + list_claim.getServiceProviderNo() + "</label></td>");
                    out.print("<td><label class=\"label\">" + practice2.getPracticeName() + "</label></td>");
                    out.print("<td><label class=\"label\">" + catagory + "</label></td>");
                    out.print("<td><label class=\"label\">" + coverNumber + "</label></td>");
                    out.print("<td><label class=\"label\">" + dateFormat.format(list_claim.getServiceDateFrom().toGregorianCalendar().getTime()) + "</label></td>");
                    out.print("<td><label class=\"label\">" + dateFormat.format(list_claim.getServiceDateTo().toGregorianCalendar().getTime()) + "</label></td>");
                    out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('ViewMemberClaimsDetailsCommand', '" + count + "','"+ list_claim.getPracticeNo() +"')\"; value=\"" + commandName + "\">Details</button></td>");
                    out.print("</tr>");
                    count++;
                    claims.add(list_claim);
                }
            }
            session.setAttribute("listOfClaims", claims);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.LookupValue;

/**
 *
 * @author Johan-NB
 */
public class PreAuthTypeLookupDropdownError extends TagSupport {

    private String displayName;
    private String elementName;
    private String javaScript;
    private String mandatory = "no";
    private String errorValueFromSession = "no";
    private HttpSession session = null;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        session = pageContext.getSession();
        try {
            out.println("<td><label class=\"label\">" + displayName + ":</label></td>");
            out.println("<td><select style=\"width:215px\" name=\"" + elementName + "\" id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");
            String selectedBox = "" + session.getAttribute(elementName);
            //Neo Lookups
            List<neo.manager.LookupValue> lookUps = getRestrictedAuthTypes();
            out.println("<option value=\"99\"></option>");
            for (int i = 0; i < lookUps.size(); i++) {
                LookupValue lookupValue = lookUps.get(i);
                if (lookupValue.getId().equalsIgnoreCase(selectedBox)) {
                    out.println("<option value=\"" + lookupValue.getId() + "\" selected >" + lookupValue.getValue() + "</option>");
                } else {
                    out.println("<option value=\"" + lookupValue.getId() + "\" >" + lookupValue.getValue() + "</option>");
                }
            }
            out.println("</select></td>");
            if (mandatory.equalsIgnoreCase("yes")) {
                out.println("<td><label class=\"red\">*</label></td>");
            } else {
                out.println("<td></td>");
            }

            String sessionValerror = "";
            if (errorValueFromSession.equalsIgnoreCase("yes")) {

                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                }

                if (sessionValerror != null && !sessionValerror.equalsIgnoreCase("null")) {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
                } else {
                    out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
                }
            }

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelNeoLookupValueDropDown tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setErrorValueFromSession(String errorValueFromSession) {
        this.errorValueFromSession = errorValueFromSession;
    }

    public List<LookupValue> getRestrictedAuthTypes() {
        List<neo.manager.LookupValue> lookUps = NeoCommand.service.getNeoManagerBeanPort().getCodeTable(89);
        List<neo.manager.LookupValue> newLookUps = new ArrayList<LookupValue>();
        List<neo.manager.LookupValue> lookUpsReturn = new ArrayList<LookupValue>();

        boolean superFlag = (Boolean) session.getAttribute("persist_user_sup");
        boolean teamLeadFlag = (Boolean) session.getAttribute("persist_user_teamLead");
        boolean pdcUserFlag = (Boolean) session.getAttribute("persist_user_pdcUser");
        boolean pdcAdminFlag = (Boolean) session.getAttribute("persist_user_pdcAdmin");

        for (LookupValue lv : lookUps) {
            //if(!lv.getId().equals("5") && !lv.getId().equals("6") && !lv.getId().equals("8")){
            if(!lv.getId().equals("6") && !lv.getId().equals("8")){
                newLookUps.add(lv);
            }
        }

            System.out.println("superFlag = " + superFlag);
            if (superFlag || teamLeadFlag || pdcUserFlag || pdcAdminFlag) {
                lookUpsReturn.addAll(newLookUps);

            } else {
                for (LookupValue lv : newLookUps) {
                    LookupValue lvReturn = new LookupValue();
                    if (!lv.getId().equals("7") && !lv.getId().equals("12")) {
                        lvReturn.setId(lv.getId());
                        lvReturn.setValue(lv.getValue());
                        lookUpsReturn.add(lvReturn);
                    }
                }
            }
        return lookUpsReturn;
    }
}

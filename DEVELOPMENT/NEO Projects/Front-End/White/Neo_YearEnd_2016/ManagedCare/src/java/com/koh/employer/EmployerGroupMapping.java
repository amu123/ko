/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.employer;

import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class EmployerGroupMapping {
    private static final String[][] FIELD_MAPPINGS = {
        {"EmployerGroup_entityId","Entity Id","no", "int", "EmployerGroup","EntityId"},
        {"EmployerGroup_product","Product","yes", "int", "EmployerGroup","ProductId"},
        {"EmployerGroup_administrator","Administrator","yes", "str", "EmployerGroup", "administrator","0","100"},
        {"EmployerGroup_employerGroupType","Group Type","yes", "int", "EmployerGroup", "EmployerGroupType"},
        {"EmployerGroup_parentEntityId","Parent Employer","no", "int", "EmployerGroup", "ParentEntityId"},
        {"EmployerGroup_employerNumber","Group Number","yes", "str", "EmployerGroup", "EmployerNumber","0","15"},
        {"EmployerGroup_employerName","Group Name","yes", "str", "EmployerGroup", "EmployerName","0","100"},
        {"EmployerGroup_tradingName","Trading Name","no", "str", "EmployerGroup", "TradingName","0","100"},
        {"EmployerGroup_companyRegistrationNo","Registration No","no", "str", "EmployerGroup", "CompanyRegistrationNo"},
        {"EmployerGroup_vatRegistrationNo","Vat No","no", "str", "EmployerGroup", "VatRegistrationNo","0","30"},
        {"EmployerGroup_natureOfBusinessId","Nature Of Business","no", "int", "EmployerGroup", "NatureOfBusinessId"},
        {"EmployerGroup_ApplicationReceiveDate","Application Receive Date","yes", "date", "EmployerGroup", "receivedDate"},
        {"EmployerGroup_certifiedDate","Certified Date","yes", "date", "EmployerGroup", "signedDate"},
        {"EmployerGroup_startDate","Inception Date","yes", "date", "EmployerGroup", "inceptionDate"},
        {"EmployerGroup_ContactDetails_region","Region","no", "int", "EmployerGroup", "regionId"},
        {"EmployerGroup_ContactDetails_title","Title","no", "int", "EmployerGroup", "ContactPersonTitle"},
        {"EmployerGroup_ContactDetails_contactPerson","Contact Person","no", "str", "EmployerGroup", "ContactPerson","0","50"},
        {"EmployerGroup_ContactDetails_telNo","Telephone Number","no", "str", "ContactDetails", "","0","50"},
        {"EmployerGroup_ContactDetails_faxNo","Fax No", "no","Str", "ContactDetails","","0","50"},
        {"EmployerGroup_ContactDetails_email","Email Address","no", "str", "ContactDetails", "","0","50"},
        {"EmployerGroup_ContactDetails_altEmail","Alternative Email Address","no", "str", "ContactDetails", "","0","50"},
        {"EmployerGroup_PostalAddress_addressLine1","Line 1","no", "str", "AddressDetails", "","0","100"},
        {"EmployerGroup_PostalAddress_addressLine2","Line 2","no", "str", "AddressDetails", "","0","100"},
        {"EmployerGroup_PostalAddress_addressLine3","Line 3","no", "str", "AddressDetails", "","0","100"},
        {"EmployerGroup_PostalAddress_postalCode","Postal Code","no", "str", "AddressDetails", "","0","10"},
        {"EmployerGroup_ResAddress_addressLine1","Line 1","no", "str", "AddressDetails", "","0","100"},
        {"EmployerGroup_ResAddress_addressLine2","Line 2","no", "str", "AddressDetails", "","0","100"},
        {"EmployerGroup_ResAddress_addressLine3","Line 3","no", "str", "AddressDetails", "","0","100"},
        {"EmployerGroup_ResAddress_postalCode","Postal Code","no", "str", "AddressDetails", "","0","10"},
    };
    
    private static final String[][] NOTES_MAPPINGS = {
        {"employerEntityId","","no", "int", "EmployeeApp","entityId"},
        {"notesListSelect","Note Type","no", "int", "EmployeeApp","noteType"},
        {"EmployeerAppNotesAdd_details","Details","no", "str", "EmployeeApp","noteDetails"}
    };
    
    private static final Map<String,String[]> FIELD_MAP;

    static {
        HashMap hm = new HashMap<String,String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }
    
    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
        return errors;
    }
    
    public static Map<String, String> validateNotes(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, NOTES_MAPPINGS);
        return errors;
    }

    public static EmployerGroup getEmployerGroup(HttpServletRequest request,NeoUser neoUser) {
        EmployerGroup eg = (EmployerGroup)TabUtils.getDTOFromRequest(request, EmployerGroup.class, FIELD_MAPPINGS, "EmployerGroup");
        eg.setSecurityGroupId(neoUser.getSecurityGroupId());
        eg.setLastUpdatedBy(neoUser.getUserId());
        return eg;
    }

    public static List<ContactDetails> getContactDetails(HttpServletRequest request,NeoUser neoUser, int entityId) {
        List<ContactDetails> cdl = new ArrayList<ContactDetails>();
        cdl.add(getContactDetail(request, "EmployerGroup_ContactDetails_telNo",neoUser, entityId, 5));
        cdl.add(getContactDetail(request, "EmployerGroup_ContactDetails_faxNo",neoUser, entityId, 2));
        cdl.add(getContactDetail(request, "EmployerGroup_ContactDetails_email",neoUser, entityId, 1));
        cdl.add(getContactDetail(request, "EmployerGroup_ContactDetails_altEmail",neoUser, entityId, 8));
        return cdl;
    }

    private static ContactDetails getContactDetail(HttpServletRequest request, String param,NeoUser neoUser, int entityId, int commType) {
        ContactDetails cd = new ContactDetails();
        cd.setCommunicationMethodId(commType);
        cd.setMethodDetails(request.getParameter(param));
        cd.setPrimaryIndicationId(1);
        //cd.setEntityId(entityId);
        // cd.setSecurityGroupId(neoUser.getSecurityGroupId());
        // cd.setLastUpdatedBy(neoUser.getUserId());
        Date effectiveStartDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("EmployerGroup_startDate"));
        cd.setEffectiveFrom(DateTimeUtils.convertDateToXMLGregorianCalendar(effectiveStartDate));
        return cd;
    }

    public static List<AddressDetails> getAddressDetails(HttpServletRequest request,NeoUser neoUser, int entityId) {
        List<AddressDetails> cdl = new ArrayList<AddressDetails>();
        cdl.add(getAddressDetail(request, "EmployerGroup_PostalAddress_",neoUser, entityId,2));
        cdl.add(getAddressDetail(request, "EmployerGroup_ResAddress_",neoUser, entityId,1));
        return cdl;
    }

    private static AddressDetails getAddressDetail(HttpServletRequest request, String param,NeoUser neoUser, int entityId, int addrType) {
        AddressDetails ad = new AddressDetails();
        ad.setAddressTypeId(addrType);
        ad.setPrimaryInd("Y");
        ad.setAddressUse(1);
        ad.setAddressLine1(request.getParameter(param + "addressLine1"));
        ad.setAddressLine2(request.getParameter(param + "addressLine2"));
        ad.setAddressLine3(request.getParameter(param + "addressLine3"));
        ad.setPostalCode(request.getParameter(param + "postalCode"));
        Date effectiveStartDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("EmployerGroup_startDate"));
        ad.setEffectiveStart(DateTimeUtils.convertDateToXMLGregorianCalendar(effectiveStartDate));
        //ad.setEntityId(entityId);
        //ad.setSecurityGroupId(neoUser.getSecurityGroupId());
        //ad.setLastUpdatedBy(neoUser.getUserId());
        return ad;
    }

    public static void setEmployerGroup(HttpServletRequest request, EmployerGroup employerGroup) {
        TabUtils.setRequestFromDTO(request, employerGroup, FIELD_MAPPINGS, "EmployerGroup");
    }
    
    public static EntityNotes getEmployerAppNotes(HttpServletRequest request,NeoUser neoUser) {
        EntityNotes ma = (EntityNotes)TabUtils.getDTOFromRequest(request, EntityNotes.class, NOTES_MAPPINGS, "EmplyeeApp");
        Date today = new Date();        
        ma.setNoteDetails(request.getParameter("EmployeerAppNotesAdd_details"));
        ma.setEntityId(new Integer(request.getParameter("employerEntityId")));
        ma.setNoteType(new Integer(request.getParameter("notesListSelect")));
        ma.setSecurityGroupId(neoUser.getSecurityGroupId());
        ma.setLastUpdatedBy(neoUser.getUserId());
        ma.setCreatedBy(neoUser.getUserId());
        ma.setNoteDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ma.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ma.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
        ma.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        ma.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));               
        return ma;
    }

    public static void setAddressDetails(HttpServletRequest request, Collection<AddressDetails> addressDetails) {
        if (addressDetails == null) return;
        for (AddressDetails ad : addressDetails) {
            if (ad == null) continue;
            String fieldStart = ad.getAddressTypeId() == 1 ? "EmployerGroup_ResAddress_" : "EmployerGroup_PostalAddress_";
            request.setAttribute(fieldStart+ "addressLine1", ad.getAddressLine1());
            request.setAttribute(fieldStart+ "addressLine2", ad.getAddressLine2());
            request.setAttribute(fieldStart+ "addressLine3", ad.getAddressLine3());
            request.setAttribute(fieldStart+ "postalCode", ad.getPostalCode());
        }
    }

    public static void setContactDetails(HttpServletRequest request,Collection<ContactDetails> contactDetails) {
        if (contactDetails == null) return;
        for (ContactDetails cd : contactDetails) {
            if (cd == null) continue;
            String fieldStart = "";
            fieldStart = cd.getCommunicationMethodId() == 5 ? "EmployerGroup_ContactDetails_telNo" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 2 ? "EmployerGroup_ContactDetails_faxNo" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 1 ? "EmployerGroup_ContactDetails_email" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 8 ? "EmployerGroup_ContactDetails_altEmail" : fieldStart;
            request.setAttribute(fieldStart, cd.getMethodDetails() == null ? "" : cd.getMethodDetails());
        }
    }

}

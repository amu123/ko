/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.UserAssignedTask;

/**
 *
 * @author chrisdp
 */
public class ViewPDCTasksByUserCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String id = request.getParameter("tasksPerUser");
        String status = request.getParameter("tasks");
        //HashMap<Integer, String> users = (HashMap<Integer, String>) session.getAttribute("NeoUserMap");
        Collection<String> users = (Collection<String>) session.getAttribute("NeoUserMap");
        String userName = "";
        for (String s : users) {
            String[] values = s.split("\\|");
            String id2 = values[0];
            String name = values[1];
            if (id2.equals(id)) {
                userName = name;
                break;
            }
        }

        Collection<UserAssignedTask> et;
        System.out.println("User ID: " + id);
        System.out.println("User Name: " + userName);
        System.out.println("Task Stat: " + status);

        et = port.getEventTaskByAssigneeId(Integer.parseInt(id), status);
        session.setAttribute("eventTaskTable", et);
        session.setAttribute("taskAssignedTo", userName);

        try {
            String nextJSP = "/PDC/ViewWorkBench.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewPDCTasksByUserCommand";
    }
}

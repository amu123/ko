/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import static com.koh.cover.MemberMainMapping.getDependantTypeValue;
import com.koh.utils.NeoCoverDetailsDependantFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverDetails;
import neo.manager.CoverDetailsAdditionalInfo;
import neo.manager.CoverSearchCriteria;
import neo.manager.EAuthCoverDetails;
import neo.manager.Member;
import neo.manager.MemberApplication;
import neo.manager.NeoManagerBean;
import neo.manager.PersonDetails;
import neo.manager.ProviderDetails;
import neo.manager.ProviderSearchCriteria;
import org.apache.log4j.Logger;

/**
 *
 * @author charlh
 */
public class ViewMemberNominatedProviderDetailsCommand extends NeoCommand {

    private Logger log = Logger.getLogger(ViewMemberAddressDetailsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();

        // Main Member Start...
        int memberEntityCommon = (Integer) session.getAttribute("memberEntityCommon");
        String memnum = (String) request.getSession().getAttribute("memberCoverNumber");
        CoverDetails coverDetails = port.findPrincipalForDepCoverNumber(memnum);
        request.setAttribute("DSP_dependantName", coverDetails.getName());
        request.setAttribute("DSP_dependantSurname", coverDetails.getSurname());
        request.setAttribute("DSP_dependantCode", coverDetails.getDependentNumber());

        List<CoverDetailsAdditionalInfo> covDetails = port.getDependentAdditionalDetails(memberEntityCommon);
        for (CoverDetailsAdditionalInfo ca : covDetails) {
            if (ca.getInfoTypeId() == 1) {
                if (ca.getInfoValue() != null) {
                    request.setAttribute("DSP_dependantCode", ca.getInfoValue());
                }
            } else if (ca.getInfoTypeId() == 21) {
                if (ca.getInfoValue() != null && !ca.getInfoValue().equalsIgnoreCase("")) {
                    ProviderDetails doctDetails = port.getPracticeDetailsForEntityByNumber(ca.getInfoValue());
                    request.setAttribute("DSP_doctorPracticeNumber", doctDetails.getPracticeNumber());
                    request.setAttribute("DSP_doctorPracticeName", doctDetails.getPracticeName());
                    request.setAttribute("DSP_doctorPracticeNetwork", doctDetails.getNetworkName());
                    if (doctDetails.getBhfStartDate() != null) {
                        request.setAttribute("DSP_doctorPracticeStartDate", new SimpleDateFormat("yyyy/MM/dd").format(doctDetails.getBhfStartDate().toGregorianCalendar().getTime()));
                    }
                    if (doctDetails.getBhfEndDate() != null) {
                        request.setAttribute("DSP_doctorPracticeEndDate", new SimpleDateFormat("yyyy/MM/dd").format(doctDetails.getBhfEndDate().toGregorianCalendar().getTime()));
                    }
                }
            } else if (ca.getInfoTypeId() == 22) {
                if (ca.getInfoValue() != null && !ca.getInfoValue().equalsIgnoreCase("")) {
                    ProviderDetails dentDetails = port.getPracticeDetailsForEntityByNumber(ca.getInfoValue());
                    request.setAttribute("DSP_dentistPracticeNumber", dentDetails.getPracticeNumber());
                    request.setAttribute("DSP_dentistPracticeName", dentDetails.getPracticeName());
                    request.setAttribute("DSP_dentistPracticeNetwork", dentDetails.getNetworkName());
                    if (dentDetails.getBhfStartDate() != null) {
                        request.setAttribute("DSP_dentistPracticeStartDate", new SimpleDateFormat("yyyy/MM/dd").format(dentDetails.getBhfStartDate().toGregorianCalendar().getTime()));
                    }
                    if (dentDetails.getBhfEndDate() != null) {
                        request.setAttribute("DSP_dentistPracticeEndDate", new SimpleDateFormat("yyyy/MM/dd").format(dentDetails.getBhfEndDate().toGregorianCalendar().getTime()));
                    }
                }
            } else if (ca.getInfoTypeId() == 23) {
                if (ca.getInfoValue() != null && !ca.getInfoValue().equalsIgnoreCase("")) {
                    ProviderDetails optDetails = port.getPracticeDetailsForEntityByNumber(ca.getInfoValue());
                    request.setAttribute("DSP_optometristPracticeNumber", optDetails.getPracticeNumber());
                    request.setAttribute("DSP_optometristPracticeName", optDetails.getPracticeName());
                    request.setAttribute("DSP_optometristPracticeNetwork", optDetails.getNetworkName());
                    if (optDetails.getBhfStartDate() != null) {
                        request.setAttribute("DSP_optometristPracticeStartDate", new SimpleDateFormat("yyyy/MM/dd").format(optDetails.getBhfStartDate().toGregorianCalendar().getTime()));
                    }
                    if (optDetails.getBhfEndDate() != null) {
                        request.setAttribute("DSP_optometristPracticeEndDate", new SimpleDateFormat("yyyy/MM/dd").format(optDetails.getBhfEndDate().toGregorianCalendar().getTime()));
                    }
                }
            }
        }
        // Main Member End...

        // Start of Dependants...
        CoverSearchCriteria csc = new CoverSearchCriteria();
        csc.setCoverNumberExact(memnum);
        List<EAuthCoverDetails> activeDependantsCoverList = new ArrayList();
        List<EAuthCoverDetails> coverList = port.eAuthCoverSearch(csc);
        for (EAuthCoverDetails cl : coverList) {
            if (!cl.getDependantType().equalsIgnoreCase("Principal Member") && cl.getCoverStatus().equalsIgnoreCase("Active")) {
                activeDependantsCoverList.add(cl);
            }
        }
        request.setAttribute("allDependantsList", activeDependantsCoverList);

        // Dependant Add Info
//        List<CoverDetailsAdditionalInfo> dependantCoverAddInfo = port.getDependentAdditionalDetails(memberEntityCommon);
        // End of Dependants...
        try {
            String nextJSP = "/Claims/MemberNominatedProviderDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private static ProviderDetails getDoctorDetails(CoverDetailsAdditionalInfo cov) {
        ProviderDetails dets = new ProviderDetails();
        if (cov.getInfoTypeId() == 21) {
            ProviderSearchCriteria search = new ProviderSearchCriteria();
            search.setEntityTypeID(4);

            if (cov.getInfoValue() != null && !cov.getInfoValue().equals("")) {
                search.setPracticeNumber(cov.getInfoValue());

                List<ProviderDetails> pdList = service.getNeoManagerBeanPort().findAllProvidersByCriteria(search);

                for (ProviderDetails pd : pdList) {
                    if (cov.getInfoValue().equals(pd.getPracticeNumber())) {
                        dets.setPracticeNumber(pd.getPracticeNumber());
                        dets.setPracticeName(pd.getPracticeName());
                        dets.setNetworkName(pd.getNetworkName());
                        dets.setBhfStartDate(cov.getStartDate());
                    }
                }
            }
        }
        return dets;
    }

    private static ProviderDetails getDentistDetails(CoverDetailsAdditionalInfo cov) {
        ProviderDetails dets = new ProviderDetails();
        if (cov.getInfoTypeId() == 22) {
            ProviderSearchCriteria search = new ProviderSearchCriteria();
            search.setEntityTypeID(4);

            if (cov.getInfoValue() != null && !cov.getInfoValue().equals("")) {
                search.setPracticeNumber(cov.getInfoValue());

                List<ProviderDetails> pdList = service.getNeoManagerBeanPort().findAllProvidersByCriteria(search);
                ProviderDetails ptest = service.getNeoManagerBeanPort().getProviderDetailsForEntityByNumber(cov.getInfoValue());

                for (ProviderDetails pd : pdList) {
                    if (cov.getInfoValue().equals(pd.getPracticeNumber())) {
                        dets.setPracticeNumber(pd.getPracticeNumber());
                        dets.setPracticeName(pd.getPracticeName());
                        dets.setNetworkName(pd.getNetworkName());
                        dets.setBhfStartDate(cov.getStartDate());
                    }
                }
            }
        }
        return dets;
    }

    private static ProviderDetails getOptometristDetails(CoverDetailsAdditionalInfo cov) {
        ProviderDetails dets = new ProviderDetails();
        if (cov.getInfoTypeId() == 23) {
            ProviderSearchCriteria search = new ProviderSearchCriteria();
            search.setEntityTypeID(4);

            if (cov.getInfoValue() != null && !cov.getInfoValue().equals("")) {
                search.setPracticeNumber(cov.getInfoValue());

                List<ProviderDetails> pdList = service.getNeoManagerBeanPort().findAllProvidersByCriteria(search);

                for (ProviderDetails pd : pdList) {
                    if (cov.getInfoValue().equals(pd.getPracticeNumber())) {
                        dets.setPracticeNumber(pd.getPracticeNumber());
                        dets.setPracticeName(pd.getPracticeName());
                        dets.setNetworkName(pd.getNetworkName());
                        dets.setBhfStartDate(cov.getStartDate());
                    }
                }
            }
        }
        return dets;
    }

    @Override
    public String getName() {
        return "ViewMemberNominatedProviderDetailsCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import java.util.ArrayList;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.LookupValue;

/**
 *
 * @author josephm
 */
public class ResponsibilityRadioActivationLabel extends TagSupport {
    private String numberPerRow = "1";
    private String elementName;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();

        try {
            ArrayList<LookupValue> values = (ArrayList<LookupValue>)TagMethods.getAllResponsibilities();
            int numPerRow = new Integer(numberPerRow);
            int count = 0;
            if(values != null){
                for (int i = 0; i < values.size(); i++) {
                    if(count == 0){
                        out.print("<tr>");
                    }
                    LookupValue lookupValue = values.get(i);

                    out.print("<td><input type=\"radio\" name=\""+elementName+"_"+1+"\" value=\""+lookupValue.getId()+"\" ><label>"+lookupValue.getValue()+"</label></input></td>");
                    count++;

                    
                    if(count == numPerRow){
                        out.println("</tr>");
                        count = 0;
                    }
                }
            }
            //<tr><td><input type="checkbox" name="asdf" value="ON" ><label>ON</label></input></td></tr>
        } catch (java.io.IOException ex) {
            throw new JspException("Error in ResponsibilityCheckBoxLabel tag", ex);
        }
        return super.doEndTag();
    }

    public void setNumberPerRow(String numberPerRow) {
        this.numberPerRow = numberPerRow;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }
}

    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Formatter;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author johanl
 */
public class AuthTariffListDisplayTag extends TagSupport {

    private String sessionAttribute;
    private String commandName;
    private String javaScript;
    private String sessionVal;
    private String elementName;
    private String tAmountF;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();

        try {
            if (javaScript != null) {
                out.println("<td colspan=\"5\"><table class=\"" + javaScript + "\" style=\"cellpadding=\"5\"; cellspacing=\"0\";\">");
            } else {
                out.println("<td colspan=\"5\"><table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            }
            out.println("<tr><th align=\"left\">Tariff Code</th><th align=\"left\">Quantity</th><th align=\"left\">Amount</th><th align=\"left\">Tariff Desc</th><th align=\"left\">Provider</th></tr>");

            ArrayList<AuthTariffDetails> atList = (ArrayList<AuthTariffDetails>) session.getAttribute(sessionAttribute);
            double totalTAmount = 0.0d;
            String totalTariffAmount = "";
            if (atList != null && atList.size() != 0) {
                for (int i = 1; i <= atList.size(); i++) {
                    AuthTariffDetails at = atList.get(i - 1);
                    Formatter bs = new Formatter();
                    Formatter format = bs.format("%10.2f", at.getAmount());
                    String tAmountF = format.toString();

                    totalTAmount += at.getAmount();

                    out.println("<tr id=\"tariffDisplay1" + i + i + "\">"
                            + "<td><label class=\"label\">" + at.getTariffCode() + "</label></td> "
                            + "<td><label class=\"label\">" + at.getQuantity() + "</label></td> "
                            + "<td><label class=\"label\">" + tAmountF + "</label></td> "
                            + "<td width=\"350\"><label class=\"label\">" + at.getTariffDesc() + "</label></td> "
                            + "<td><label class=\"label\">" + at.getProviderNum() + "</label></td> "
                            + "</tr>");
                }
                if (totalTAmount != 0.0) {
//                   totalTariffAmount = new DecimalFormat("#.##").format(totalTariffAmount);
                    out.println("<tr><td colspan=\"5\"></td></tr>");
                    out.println("<tr>"
                            + "<th colspan=\"3\" align=\"left\">Total tariff amount :</td>"
                            + "<th colspan=\"3\" align=\"left\">" + totalTariffAmount + "</td>"
                            + "</tr>");

                }
            }
            out.println("</table></td>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in MemberSearchTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setSessionAttribute(String sessionAttribute) {
        this.sessionAttribute = sessionAttribute;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void settAmountF(String tAmountF) {
        this.tAmountF = tAmountF;
    }

}

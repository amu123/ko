/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.employer.command.TabUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Session;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ClaimLine;
import neo.manager.CoverDetails;
import neo.manager.Drug;
import neo.manager.Modifier;
import neo.manager.Ndc;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author janf
 */
public class ClaimLineNdcCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String command = request.getParameter("command");
        HttpSession session = request.getSession();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        System.out.println("Operation: ClaimLineNdcCommand - Command: " + command);


        if ("NewNappiLine".equalsIgnoreCase(command)) {
            try {
//                List<CoverDetails> covList = port.getDependantsForPrincipleMemberByDate(request.getParameter("coverNumber"));
//                List<ClaimLine> cls = port.fetchClaimLineForClaim(TabUtils.getIntParam(request, "claimId"));
                List<Ndc> ndcs = (List<Ndc>) request.getAttribute("claimLineNdcList");
                List<Ndc> clNdcList = (List<Ndc>) session.getAttribute("claimLineNdcList");
                String capOrEdit = request.getParameter("capOrEdit");
                System.out.println("capOrEdit = " + capOrEdit);

//                if (cl != null) {
////                    Security security = TabUtils.getSecurity(request);
//                    int claimID = TabUtils.getIntParam(request, "ClaimLine_claimId");
//                    System.out.println("ClaimLineId" + cl.getClaimLineId());
//                    System.out.println("claimID" + claimID);
//                    ndcs = cl.getNdc();
//                }
//                
//                if(ndcs != null && !ndcs.isEmpty()){
//                    request.setAttribute("NdcLine", ndcs);
//                    request.setAttribute("claimLineNdcList", ndcs);
//                    System.out.println("ndc size = " + ndcs.size());
//                }
                
                if(clNdcList != null && !clNdcList.isEmpty()){
                    session.setAttribute("NdcLine", clNdcList);
                    session.setAttribute("claimLineNdcList",clNdcList);
                    System.out.println("clNdcList size = " + clNdcList.size());
                }

//                request.setAttribute("ClaimLines", cls);
//                request.setAttribute("NdcLine", ndcs);
//                request.setAttribute("claimLineNdcList", ndcs);
                request.setAttribute("claimLines", request.getParameter("claimLines"));
//                request.setAttribute("ClaimLine_Category", request.getParameter("category"));
//                request.setAttribute("coverNumber", request.getParameter("coverNumber"));
//                request.setAttribute("covList", covList);
                if("edit".equalsIgnoreCase(capOrEdit)){
                    RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/EditClaims_EditNdc.jsp");
                    dispatcher.forward(request, response);
                } else {
                    RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/CaptureClaims_CaptureNdc.jsp");
                    dispatcher.forward(request, response);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("SaveNdc".equalsIgnoreCase(command)) {
            try {
//                ClaimLine cl = (ClaimLine) request.getAttribute("claimLineClaimNappi");
//                System.out.println("cl = " + cl.getClaimId());

                Ndc ndc = saveNappiLine(request, port,session);
                System.out.println("Ndc Code: " + ndc.getNdcCode());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if ("fetchNappiLines".equalsIgnoreCase(command)) {
//            List<ClaimLine> cls = port.fetchClaimLineForClaim(TabUtils.getIntParam(request, "claimId"));
//            List<Ndc> ndc = null;
            List<Ndc> clNdcList = (List<Ndc>) session.getAttribute("claimLineNdcList");
//            ClaimLine cl = (ClaimLine) request.getAttribute("claimLineClaimNappi");

//            if (cl != null) {
//                ndc = cl.getNdc();
//                request.setAttribute("NdcLine", ndc);
//                request.setAttribute("claimId", request.getParameter("claimId"));
//                System.out.println("CL != null");
//            }
            
            if(clNdcList != null && !clNdcList.isEmpty()){
                System.out.println("clNdcList Not Empty");
                request.setAttribute("NdcLine", clNdcList);
                session.setAttribute("NdcLine", clNdcList);
                request.setAttribute("claimLineNdcList", clNdcList);
                session.setAttribute("claimLineNdcList", clNdcList);
            } else {
                System.out.println("clNdcList Emtpy");
            }
            
            try {
                RequestDispatcher dispatcher = context.getRequestDispatcher("/NClaims/CaptureClaims_NdcSummary.jsp");
                dispatcher.forward(request, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("findNappi".equalsIgnoreCase(command)) {
            String nappi = request.getParameter("nappi");
            String result = "";
            String practiceType = request.getParameter("practiceType");
            System.out.println("nappi = " + nappi);
            System.out.println("PracticeType = " + practiceType);
            
            Drug drug = port.getDrugForCode(nappi, practiceType);
            if(drug != null) {
                result = drug.getCode() + " - " + drug.getName();
                
                String ndcDesc = "nappiDesc" + drug.getCode();
                System.out.println("nappi desc = " + ndcDesc);
                session.setAttribute(ndcDesc, drug.getName());
            } else {
                //result = nappi + " - Invalid Nappi Code";
            }
            
            System.out.println("result = " + result);
            try{
            PrintWriter out = response.getWriter();
            out.print(result);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if("removeNappiFromList".equalsIgnoreCase(command)){
            List<Ndc> ndcList = (List<Ndc>) session.getAttribute("claimLineNdcList");
            try {
                int index = Integer.parseInt(request.getParameter("nappiIndex"));
                Ndc removeObj = ndcList.get(index);
                ndcList.remove(removeObj);

                session.setAttribute("claimLineNdcList", ndcList);
                session.setAttribute("NdcLine", ndcList);

            } catch (Exception e) {
                e.printStackTrace();
            }
            
            try {
                String nextJSP = "/NClaims/CaptureClaims_NdcSummary.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

                dispatcher.forward(request, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if("modifyNappiFromList".equalsIgnoreCase(command)){
            List<Ndc> ndcList = (List<Ndc>) session.getAttribute("claimLineNdcList");
            String ti = request.getParameter("nappiIndex");
            int index = Integer.parseInt(ti);

            try {
                PrintWriter out = response.getWriter();
                Ndc modifyObject = ndcList.get(index);

                out.print("Done|"
                        + modifyObject.getNdcCode() + "|"
                        + modifyObject.getDosage()+ "|"
                        + modifyObject.getQuantity() + "|"
                        + modifyObject.getAmount() + "|"
                        + modifyObject.getPrimaryNdcIndicator());


                //remove current object from session value
                ndcList.remove(modifyObject);

                session.setAttribute("claimLineNdcList", ndcList);
                session.setAttribute("NdcLine", ndcList);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if("getTotals".equalsIgnoreCase(command)){
            List<Ndc> ndcList = (List<Ndc>) session.getAttribute("claimLineNdcList");
            double total = 0.d;
            
            for(Ndc ndcs : ndcList){
                total += ndcs.getAmount();
            }
            
            String result = String.format("%.2f", total);
            
            try {
                PrintWriter out = response.getWriter();
                out.print(result);
            } catch (IOException ex) {
                Logger.getLogger(ClaimLineNdcCommand.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return null;
    }

    private Ndc saveNappiLine(HttpServletRequest request, NeoManagerBean port,HttpSession session) {
        ClaimLine cl = (ClaimLine) request.getAttribute("claimLineClaimNappi");
        List<Ndc> clNdcList = (List<Ndc>) session.getAttribute("claimLineNdcList");
        String code = request.getParameter("ClaimLine_NappiCode");
        String quantity = request.getParameter("ClaimLine_NappiQuantity");
        String dosage = request.getParameter("ClaimLine_NappiDosage");
        String amount = request.getParameter("ClaimLine_NappiAmount");
        String primaryNdcIndicator = request.getParameter("ClaimLine_NappiPrimary");
        System.out.println("code " + code + "\nQuantity = " + quantity + "\nDosage = " + dosage + "\nAmount = " + amount + "\nPrimary = " + primaryNdcIndicator);
        
        Ndc ndc = new Ndc();
        ndc.setAmount(new Double(request.getParameter("ClaimLine_NappiAmount")));
        ndc.setDosage(request.getParameter("ClaimLine_NappiDosage"));
        ndc.setNdcCode(request.getParameter("ClaimLine_NappiCode"));
        ndc.setQuantity(new Double(request.getParameter("ClaimLine_NappiQuantity")));
        int primaryNdc = 0;
        if (primaryNdcIndicator.equalsIgnoreCase("true")) {
            primaryNdc = 1;
        }
        ndc.setPrimaryNdcIndicator(primaryNdc);
        
        if (clNdcList != null && !clNdcList.isEmpty()) {
            clNdcList.add(ndc);
            System.out.println("clNdcList not empty");
        } else {
            clNdcList = new ArrayList<Ndc>();
            clNdcList.add(ndc);
            System.out.println("clNdcList empty");
        }
        session.setAttribute("claimLineNdcList", clNdcList);
        session.setAttribute("NdcLine", clNdcList);
        request.setAttribute("claimLineClaimNappi", cl);
        request.setAttribute("claimLineClaimNappiList", ndc);

        return ndc;
    }

    @Override
    public String getName() {
        return "ClaimLineNdcCommand";
    }

}

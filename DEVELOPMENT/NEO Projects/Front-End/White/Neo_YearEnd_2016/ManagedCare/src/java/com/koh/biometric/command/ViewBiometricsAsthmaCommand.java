/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.GeneralBiometrics;
import neo.manager.NeoManagerBean;
import neo.manager.PdcNotes;
import neo.manager.PersonDetails;

/**
 *
 * @author martins
 */
public class ViewBiometricsAsthmaCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        /* HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        Integer entityId = (Integer) session.getAttribute("entityId");
        String searchView = (String) session.getAttribute("searchView");
        PersonDetails person = service.getNeoManagerBeanPort().getPersonDetailsByEntityId(entityId);



        session.setAttribute("title", person.getTitle());
        session.setAttribute("initials", person.getInitials());
        session.setAttribute("idNumber", person.getIDNumber());
        session.setAttribute("matitalStatus", person.getMaritalStatus());
        session.setAttribute("homeLanguage", person.getHomeLanguage());
        session.setAttribute("employerName", person.getEmployer());
        session.setAttribute("incomeCategoty", person.getIncomeCategory());
        session.setAttribute("incomeCategoty", person.getJobTitle());

        //Remove for Address infor from session
        session.removeAttribute("PhysicalAddress");
        session.removeAttribute("PhysicalAddress1");
        session.removeAttribute("patientPhysicalAddress2");
        session.removeAttribute("PhysicalCity");
        session.removeAttribute("PostalAddress");
        session.removeAttribute("PostalAddress1");
        session.removeAttribute("PostalAddress2");
        session.removeAttribute("PhysicalCity");
        session.removeAttribute("email");
        session.removeAttribute("faxNumber");
        session.removeAttribute("cellNumber");
        session.removeAttribute("workNumber");
        session.removeAttribute("homeNumber");

        session.removeAttribute("dates");
        session.removeAttribute("dropDownDates");
        session.removeAttribute("sessionHash");
        session.removeAttribute("catched"); 

        String nextJSP = null;

        if (searchView != null){

            nextJSP = "/PDC/PolicyHolderDetailsMemberSearchView.jsp";
        }else{
            nextJSP = "/PDC/PolicyHolderDetails.jsp";
        }
                
        //set pdc notes to session
        List<PdcNotes> noteList = port.retrievePdcNotes(entityId);//session.getAttribute("pdcNoteList");
        if (noteList != null && !noteList.isEmpty()) {
            session.setAttribute("pdcNoteList",noteList);
        }        */
        
        String nextJSP ="/biometrics/CaptureAsthma.jsp";

        try {
            
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewBiometricsAsthmaCommand";
    }
}

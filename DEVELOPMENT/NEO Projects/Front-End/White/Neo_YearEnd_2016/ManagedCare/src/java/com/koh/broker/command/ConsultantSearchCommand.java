/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker.command;

import com.koh.broker_firm.application.commands.BrokerFirmSearchCommand;
import com.koh.command.NeoCommand;
import com.koh.utils.MapUtils;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class ConsultantSearchCommand extends NeoCommand{
    
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        try {
            String consultantCode = request.getParameter("consultantCode");
            String consultantName = request.getParameter("consultantName");
                                  
            List<KeyValueArray> kva = port.getBrokerConsultantList(consultantCode, consultantName);
                       
            Object col = MapUtils.getMap(kva);                        

            request.setAttribute("ConsultantResults", col);
            context.getRequestDispatcher("/Broker/ConsultantSearchResults.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(BrokerFirmSearchCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ConsultantSearchCommand";
    }


}

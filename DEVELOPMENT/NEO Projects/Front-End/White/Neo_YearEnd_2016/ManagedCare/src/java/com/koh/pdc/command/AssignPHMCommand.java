/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import neo.manager.WorkList;
import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.LookupValue;
import neo.manager.MainList;
import neo.manager.NeoUser;
import com.koh.utils.DateTimeUtils;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author princes
 */
public class AssignPHMCommand extends NeoCommand {
    Logger logger = Logger.getLogger(AssignPHMCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        String in = (String) session.getAttribute("listIndex");
        logger.info("The index is " + in);
        int AssignTo = new Integer(request.getParameter("assginPMH"));
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        String rea = (String) request.getParameter("reasonForAssignment");
        String mgs = (String) request.getParameter("caseDetails");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        java.util.Date today = new java.util.Date();
        String todayDate = sdf.format(today);

        PrintWriter out = null;
        String msg = "FAILED: Please provide all the required details before re-assigning";

        if (in != null && !in.equalsIgnoreCase("")) {

            if ((AssignTo != 0) && (user != null) && (rea != null && !rea.equalsIgnoreCase(""))) {

                int index = new Integer(in);
                ArrayList<MainList> workList = (ArrayList<MainList>) session.getAttribute("mainList");
                MainList mainList = workList.get(0);
            
                java.util.Date date = DateTimeUtils.convertFromYYYYMMDD(todayDate);
                XMLGregorianCalendar cal = DateTimeUtils.convertDateToXMLGregorianCalendar(date);
                WorkList worklist = new WorkList();
                worklist.setWorkListId(mainList.getWorklistID());
                worklist.setAssignedBy(user.getUserId());
                worklist.setAssignedTo(AssignTo);
                worklist.setCreatedDate(mainList.getCreatedDate());
                worklist.setAssignmentDate(cal);
                worklist.setCoverNumber(mainList.getCoverNumber());
                worklist.setDependentNumber(mainList.getDependentNumber());

                LookupValue reason = new LookupValue();
                reason.setId(rea);
                worklist.setAssigmentReason(reason);

                LookupValue notify = new LookupValue();
                String not = (String) request.getParameter("notifyPatient");
                if (not != null) {
                    notify.setId("1");
                    /*PdcCommunication comm = new PdcCommunication();
                     NeoUser u = FECommand.port.getUserById(AssignTo);
                     comm.setEmail(u.getEmailAddress());
                     comm.setMessage(mgs);
                     comm.setTriggerType("PDCCommunication");
                     comm.setSubject("PDC Test");
                     System.out.println("Email " + u.getEmailAddress());
                     System.out.println("Message " + mgs);
                     port.sendPDCCommunication(comm);*/
                } else {
                    notify.setId("2");
                }
                worklist.setAssigmentNotification(notify);

                worklist.setAssigmentDetails(mgs);
                int rowAdded = service.getNeoManagerBeanPort().assignPHMtoWorklistItem(worklist);
                logger.log(Level.INFO, ">> ManagedCare >> RowAdded : " + rowAdded);

                if (rowAdded == 1) {
                    msg = "Task has been successfully assigned to user";
                } else {
                    msg = "Assignment was Unsuccessful!";
                }

                try {
                    out = response.getWriter();

                    out.println("<html>");
                    out.println("<head>");
                    out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<center>");
                    out.println("<table cellspacing=\"4\">");
                    out.println("<tr>");
                    out.println("<td><label class=\"header\"><h2>" + msg + "</h2></label></td>");
                    out.println("</tr>");
                    out.println("</table>");
                    out.println("<p> </p>");
                    out.println("</center>");
                    out.println("</body>");
                    out.println("</html>");

                    response.setHeader("Refresh", "3; URL=/ManagedCare/PDC/WorkbenchDetails.jsp");
                } catch (IOException ex) {
                    Logger.getLogger(AssignPHMCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    out = response.getWriter();

                    out.println("<html>");
                    out.println("<head>");
                    out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<center>");
                    out.println("<table cellspacing=\"4\">");
                    out.println("<tr>");
                    out.println("<td><label class=\"header\"><h2>" + msg + "</h2></label></td>");
                    out.println("</tr>");
                    out.println("</table>");
                    out.println("<p> </p>");
                    out.println("</center>");
                    out.println("</body>");
                    out.println("</html>");

                    response.setHeader("Refresh", "5; URL=/ManagedCare/PDC/WorkbenchDetails.jsp");
                } catch (IOException ex) {
                    Logger.getLogger(AssignPHMCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "AssignPHMCommand";
    }
}

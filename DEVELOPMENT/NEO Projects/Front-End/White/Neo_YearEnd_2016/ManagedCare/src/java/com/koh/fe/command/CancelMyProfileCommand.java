/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

/**
 *
 * @author josephm
 */
import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author josephm
 */
public class CancelMyProfileCommand extends NeoCommand {
    
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
    
        System.out.println("Inside CancelMyProfileCommand");
        
        try {
            String nextJSP = "/SystemAdmin/UpdateMyProfile.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        
        }catch(Exception ex) {
        
            ex.printStackTrace();
        }
        
        return null;
    }
    
    @Override
    public String getName() {
    
        return "CancelMyProfileCommand";
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.tags;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author josephm
 */
public class ClaimsTabbedButton extends TagSupport {

    private String buttonCommand1;
    private String buttonDisplay1;
    private String buttonCommand2;
    private String buttonDisplay2;
    private String buttonCommand3;
    private String buttonDisplay3;
    private String buttonCommand4;
    private String buttonDisplay4;
    private String buttonCommand5;
    private String buttonDisplay5;
    private String buttonCommand6;
    private String buttonDisplay6;
    private String buttonCommand7;
    private String buttonDisplay7;
    private String buttonCommand8;
    private String buttonDisplay8;
    private String buttonCommand9;
    private String buttonDisplay9;
    private String buttonCommand10;
    private String buttonDisplay10;
    private String buttonCommand11;
    private String buttonDisplay11;
    private int numberOfButtons;
    private String disabledDisplay;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {

                JspWriter out = pageContext.getOut();
        //HttpSession session = pageContext.getSession();
        //ArrayList<Diagnosis> list = (ArrayList<Diagnosis>) session.getAttribute("diagnosis_list");
        try {

            out.println("<table cellpadding=\"0\" cellspacing=\"0\">");
            for (int count = 0; count < numberOfButtons; count++) {
                if (count == 0) {
                    if (disabledDisplay.equals(buttonDisplay1)) {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand1 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay1 + "</button></td>");
                    } else {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand1 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay1 + "</button></td>");
                    }
                } else if (count == 1) {
                    if (disabledDisplay.equals(buttonDisplay2)) {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand2 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay2 + "</button></td>");
                    } else {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand2 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay2 + "</button></td>");
                    }
                } else if (count == 2) {
                    if (disabledDisplay.equals(buttonDisplay3)) {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand3 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay3 + "</button></td>");
                    } else {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand3 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay3 + "</button></td>");
                    }
                } else if (count == 3) {
                    if (disabledDisplay.equals(buttonDisplay4)) {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand4 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay4 + "</button></td>");
                    } else {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand4 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay4 + "</button></td>");
                    }
                } else if (count == 4) {
                    if (disabledDisplay.equals(buttonDisplay5)) {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand5 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay5 + "</button></td>");
                    } else {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand5 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay5 + "</button></td>");
                    }
                } else if (count == 5) {
                    if (disabledDisplay.equals(buttonDisplay6)) {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand6 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay6 + "</button></td>");
                    } else {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand6 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay6 + "</button></td>");
                    }
                } else if (count == 6) {
                    if (disabledDisplay.equals(buttonDisplay7)) {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand7 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay7 + "</button></td>");
                    } else {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand7 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay7 + "</button></td>");
                    }
                } else if(count == 7) {
                      if (disabledDisplay.equals(buttonDisplay8)) {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand8 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay8 + "</button></td>");
                    } else {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand8 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay8 + "</button></td>");
                    }
                } else if(count == 8) {
                      if (disabledDisplay.equals(buttonDisplay9)) {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand9 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay9 + "</button></td>");
                    } else {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand9 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay9 + "</button></td>");
                    }
                } else if(count == 9) {
                      if (disabledDisplay.equals(buttonDisplay10)) {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand10 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay10 + "</button></td>");
                    } else {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand10 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay10 + "</button></td>");
                    }
                }else if(count == 10) {
                      if (disabledDisplay.equals(buttonDisplay11)) {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" disabled=\"disabled\" onClick=\"submitWithAction('" + buttonCommand11 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay11 + "</button></td>");
                    } else {
                        out.println("<td><button name=\"opperation\" type=\"button\" class=\"btn\" onClick=\"submitWithAction('" + buttonCommand11 + "')\"; value=\"AssignTaskCommand\">" + buttonDisplay11 + "</button></td>");
                    }
                }
            }
            out.println("</table>");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.doEndTag();
    }

    public void setButtonCommand1(String buttonCommand1) {
        this.buttonCommand1 = buttonCommand1;
    }

    public void setButtonDisplay1(String buttonDisplay1) {
        this.buttonDisplay1 = buttonDisplay1;
    }

    public void setButtonCommand2(String buttonCommand2) {
        this.buttonCommand2 = buttonCommand2;
    }

    public void setButtonDisplay2(String buttonDisplay2) {
        this.buttonDisplay2 = buttonDisplay2;
    }

    public void setButtonCommand3(String buttonCommand3) {
        this.buttonCommand3 = buttonCommand3;
    }

    public void setButtonDisplay3(String buttonDisplay3) {
        this.buttonDisplay3 = buttonDisplay3;
    }

    public void setButtonCommand4(String buttonCommand4) {
        this.buttonCommand4 = buttonCommand4;
    }

    public void setButtonDisplay4(String buttonDisplay4) {
        this.buttonDisplay4 = buttonDisplay4;
    }

    public void setButtonCommand5(String buttonCommand5) {
        this.buttonCommand5 = buttonCommand5;
    }

    public void setButtonDisplay5(String buttonDisplay5) {
        this.buttonDisplay5 = buttonDisplay5;
    }

    public void setButtonCommand6(String buttonCommand6) {
        this.buttonCommand6 = buttonCommand6;
    }

    public void setButtonDisplay6(String buttonDisplay6) {
        this.buttonDisplay6 = buttonDisplay6;
    }

    public void setButtonCommand7(String buttonCommand7) {
        this.buttonCommand7 = buttonCommand7;
    }

    public void setButtonDisplay7(String buttonDisplay7) {
        this.buttonDisplay7 = buttonDisplay7;
    }

    public void setButtonCommand8(String buttonCommand8) {

        this.buttonCommand8 = buttonCommand8;
    }
    public void setButtonDisplay8(String buttonDisplay8) {

        this.buttonDisplay8 = buttonDisplay8;
    }

    public String getButtonCommand9() {
        return buttonCommand9;
    }

    public void setButtonCommand9(String buttonCommand9) {
        this.buttonCommand9 = buttonCommand9;
    }

    public String getButtonDisplay9() {
        return buttonDisplay9;
    }

    public void setButtonDisplay9(String buttonDisplay9) {
        this.buttonDisplay9 = buttonDisplay9;
    }

    public String getButtonCommand10() {
        return buttonCommand10;
    }

    public void setButtonCommand10(String buttonCommand10) {
        this.buttonCommand10 = buttonCommand10;
    }

    public String getButtonDisplay10() {
        return buttonDisplay10;
    }

    public void setButtonDisplay10(String buttonDisplay10) {
        this.buttonDisplay10 = buttonDisplay10;
    }
    
    public void setNumberOfButtons(int numberOfButtons) {
        this.numberOfButtons = numberOfButtons;
    }

    public void setDisabledDisplay(String disabledDisplay) {
        this.disabledDisplay = disabledDisplay;
    }

    public String getButtonCommand11() {
        return buttonCommand11;
    }

    public void setButtonCommand11(String buttonCommand11) {
        this.buttonCommand11 = buttonCommand11;
    }

    public String getButtonDisplay11() {
        return buttonDisplay11;
    }

    public void setButtonDisplay11(String buttonDisplay11) {
        this.buttonDisplay11 = buttonDisplay11;
    }
}

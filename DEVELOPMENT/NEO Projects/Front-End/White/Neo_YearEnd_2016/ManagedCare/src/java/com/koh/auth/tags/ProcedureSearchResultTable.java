/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.tags;

import com.koh.auth.dto.ProcedureSearchResult;
import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author whauger
 */
public class ProcedureSearchResultTable extends TagSupport {

    private String javascript;
    private String commandName;

    /**
     * Called by the container to invoke this tag.
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");

            String message = (String) req.getAttribute("searchProcedureResultMessage");

            if (message != null && !message.equalsIgnoreCase(""))
            {
                out.println("<label class=\"red\">" + message + "</label></br></br>");
            }

            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Code</th><th>Description</th><th>Select</th></tr>");

            ArrayList<ProcedureSearchResult> procedures = (ArrayList<ProcedureSearchResult>) req.getAttribute("searchProcedureResult");
            if (procedures != null) {
                for (int i = 0; i < procedures.size(); i++) {
                    ProcedureSearchResult procedure = procedures.get(i);
                    out.println("<tr><form method=\"post\"><td><label class=\"label\">" + procedure.getCode() + "</label><input type=\"hidden\" name=\"code\" value=\"" + procedure.getCode() + "\"/></td>" +
                            "<td><label class=\"label\">" + procedure.getDescription() + "</label><input type=\"hidden\" name=\"description\" value=\"" + procedure.getDescription() + "\"/></td>" +
                            "<td><button name=\"opperation\" name=\"opperation\" type=\"submit\" " + javascript + " value=\"" + commandName + "\">Select</button></td></form></tr>");

                }
            }
            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in ProcedureSearchResultTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

}

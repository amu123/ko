/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author whauger
 */
public class LabelTextSearchText extends TagSupport {
    private String displayName;
    private String elementName;
    private String onScreen;
    private String valueFromRequest = "no";
    private String valueFromSession = "no";
    private String searchFunction;
    private String searchOperation;
    private String mandatory = "no";
    private String javaScript = "";

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        try {

            if (valueFromSession.equalsIgnoreCase("yes")) {

                String sessionVal = "";
                String sessionValerror = "";

                if(session.getAttribute(elementName+"_text") != null) {
                    sessionVal = "" + session.getAttribute(elementName+"_text");
                    //System.out.println("Got value : " + sessionVal);
                }

                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                    //System.out.println("Got error value : " + sessionValerror);
                }
                
                out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input  autocomplete='off' type='text' id=\"" + elementName + "_text\" name=\"" + elementName + "_text\" value=\"" + sessionVal + "\" size=\"30\" " + javaScript + "/></td>");
                if (mandatory.equalsIgnoreCase("yes")) {
                    out.println("<td><label class=\"red\">*</label></td>");
                } else {
                    out.println("<td></td>");
                }
                out.println("<td align=\"left\"><img src=\"/ManagedCare/resources/Search.gif\" width=\"28\" height=\"28\" alt=\"Search\" border=\"0\" onClick=\"submitWithAction('" + searchOperation + "', '"+elementName+"', '"+onScreen+"')\";/></td>");
                if (!sessionValerror.equalsIgnoreCase("") && !sessionValerror.equalsIgnoreCase("null"))
                    out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
                else
                    out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
                //out.println("<td><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"" + sessionVal + "\" size=\"30\"/></td>");
            } else {

                if (valueFromRequest.equalsIgnoreCase("yes")) {
                    out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "_text\" name=\"" + elementName + "_text\" value=\"" + req.getAttribute(valueFromRequest) + "\" size=\"30\" " + javaScript + "/></td>");
                    if (mandatory.equalsIgnoreCase("yes")) {
                        out.println("<td><label class=\"red\">*</label></td>");
                    } else {
                        out.println("<td></td>");
                    }
                    out.println("<td align=\"left\"><img src=\"/ManagedCare/resources/Search.gif\" width=\"28\" height=\"28\" alt=\"Search\" border=\"0\" onClick=\"submitWithAction('" + searchOperation + "' , '"+elementName+"', '"+onScreen+"')\";/></td>");
                    out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
                    //out.println("<td><input disabled type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"" + req.getAttribute(valueFromRequest) + "\" size=\"30\"/></td>");
                } else {
                    out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "_text\" name=\"" + elementName + "_text\" value=\"\" size=\"30\" " + javaScript + "/></td>");
                    if (mandatory.equalsIgnoreCase("yes")) {
                        out.println("<td><label class=\"red\">*</label></td>");
                    } else {
                        out.println("<td></td>");
                    }
                    out.println("<td align=\"left\"><img src=\"/ManagedCare/resources/Search.gif\" width=\"28\" height=\"28\" alt=\"Search\" border=\"0\" onClick=\"submitWithAction('" + searchOperation + "', '"+elementName+"', '"+onScreen+"')\";/></td>");
                    out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
                    //out.println("<td><input disabled type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"\" size=\"30\"/></td>");

                }
            }
             
            //out.println("<td><img src=\"/ManagedCare/resources/Close.gif\" width=\"28\" height=\"28\" alt=\"Clear\" border=\"0\" onClick=\"clearElement('" + elementName + "')\"/></td></tr></table>");
            //out.println("</td>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelTextSearchText tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setOnScreen(String onScreen) {
        this.onScreen = onScreen;
    }

    public void setValueFromRequest(String valueFromRequest) {
        this.valueFromRequest = valueFromRequest;
    }

    public void setValueFromSession(String valueFromSession) {
        this.valueFromSession = valueFromSession;
    }

    public void setSearchFunction(String searchFunction) {
        this.searchFunction = searchFunction;
    }

    public void setSearchOperation(String searchOperation) {
        this.searchOperation = searchOperation;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

}

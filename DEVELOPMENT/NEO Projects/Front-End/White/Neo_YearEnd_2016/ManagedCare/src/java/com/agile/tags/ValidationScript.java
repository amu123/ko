/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class ValidationScript extends TagSupport {

    private String enabled;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();

        try {

            out.println("<script type=\"text/javascript\">");
            out.println("function validate(thisForm){");
            out.println("clearErrors();");
            out.println("var toRet = true;");
            out.println("var ret = true;");
            out.println(pageContext.getAttribute("validate"));
//            out.println("");
//            out.println("");
            out.println("return toRet;");
//            out.println("return false;");
            out.println("}");

            out.println("</script>");


        } catch (java.io.IOException ex) {
            throw new JspException("Error in ValidationScript tag", ex);
        }
        return super.doEndTag();
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }
}

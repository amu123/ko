/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthCPTDetails;

/**
 *
 * @author johanl
 */
public class ModifyAuthCPTCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        ArrayList<AuthCPTDetails> cptList = (ArrayList<AuthCPTDetails>) session.getAttribute("authCPTListDetails");
        try {
            PrintWriter out = response.getWriter();
            if (cptList != null) {
                String cptCode = request.getParameter("cpt");
                String cptDesc = request.getParameter("desc");

                for (AuthCPTDetails cpt : cptList) {
                    if (cptCode.equalsIgnoreCase(cpt.getCptCode())) {
                        //session.setAttribute("cptDesc",cptDesc);
                        session.setAttribute("cptCodeDesc",cptDesc);
                        out.print("Done|" + cpt.getCptCode() + "|" + cpt.getTheatreTime() + "|" +
                                cpt.getProcedureLink()+"|"+cptDesc);
                        cptList.remove(cpt);
                        break;
                    }
                }
                session.setAttribute("authCPTListDetails", cptList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "ModifyAuthCPTCommand";
    }
}

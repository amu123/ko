/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.ChronicDiseaseBiometrics;

/**
 *
 * @author princes
 */
public class BiometricChronicRenalDiseaseTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String javascript;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        List<ChronicDiseaseBiometrics> getChronic = (List<ChronicDiseaseBiometrics>) session.getAttribute("getChronic");
        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        String dateMeasured;
        try {

            out.print("<tr>");
            out.print("<th scope=\"col\">Date Measured</th>");
            out.print("<th scope=\"col\">Height</th>");
            out.print("<th scope=\"col\">Weight</th>");
            out.print("<th scope=\"col\">Blood Pressure Systolic</th>");
            out.print("<th scope=\"col\">Blood Pressure Diastolic</th>");
            out.print("<th scope=\"col\">S Creatinine</th>");
            out.print("<th scope=\"col\">Proteinuria</th>");
            out.print("<th scope=\"col\">Select</th>");

//            out.print("<th scope=\"col\">ICD10</th>");
//            out.print("<th scope=\"col\">Excercise</th>");            
//            out.print("<th scope=\"col\">Ex Smoker Years Stopped</th>");
//            out.print("<th scope=\"col\">Alchohol Units</th>");
//            out.print("<th scope=\"col\">BP SBP/DBP mmHg</th>");
//            out.print("<th scope=\"col\">S Creating pmo/l</th>");
//            out.print("<th scope=\"col\">Proteinuria g/L</th>");
//            out.print("<th scope=\"col\">Source</th>");
//            out.print("<th scope=\"col\">Notes</th>");
            out.print("</tr>");

            if (getChronic != null && getChronic.size() > 0) {
                for (ChronicDiseaseBiometrics c : getChronic) {
                    out.print("<tr>");
                    if (c.getDateMeasured() != null) {
                        Date mDate = c.getDateMeasured().toGregorianCalendar().getTime();
                        dateMeasured = formatter.format(mDate);
                        out.print("<td><center><label class=\"label\">" + dateMeasured + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getHeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + c.getHeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getWeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + c.getWeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getBloodPressureSystolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getBloodPressureSystolic() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getBloodPressureDiastolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getBloodPressureDiastolic() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getSCreatinin() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getSCreatinin() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getProteinuria() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getProteinuria() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("<td><center><button name=\"opperation\" type=\"button\" " + javascript + " value=\"" + c.getBiometricsId() + "\">Select</button></center></td>");
//                    
//                    if (c.getIcd10Lookup() != null) {
//                        out.print("<td>" + c.getIcd10Lookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (c.getExercisePerWeek() != 0) {
//                        out.print("<td>" + c.getExercisePerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }                    
//
//                    if (c.getYearsSinceStopped() > 0) {
//                        out.print("<td>" + c.getYearsSinceStopped() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (c.getAlcoholUnitsPerWeek() != 0) {
//                        out.print("<td>" + c.getAlcoholUnitsPerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getBloodPressureSystolic() != 0) {
//                        out.print("<td>" + c.getBloodPressureSystolic() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getSCreatinin() != 0) {
//                        out.print("<td>" + c.getSCreatinin() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getProteinuria() != 0) {
//                        out.print("<td>" + c.getProteinuria() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getSourceLookup() != null) {
//                        out.print("<td>" + c.getSourceLookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getDetail() != null) {
//                        out.print("<td>" + c.getDetail() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
                    out.print("</tr>");
                }
            }

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

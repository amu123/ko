/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author josephm
 */
public class CalculateTariffTestResulsCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
        PrintWriter out = null;

        double tariffAmount = new Double("" + session.getAttribute("OriginalTariffAmount"));
        //double tQuan = new Double("" + session.getAttribute("OriginalTariffQuantity"));
        //double tariffQuantity = new Double(request.getParameter("tariffQ"));
        double testResults = new Double(request.getParameter("testResults"));
        String tariffDesciption = "" + request.getParameter("tariffDescription");
        double tAmount = 0.0d;
        double newTAmount = 0.0d;

        System.out.println("tQuan and amount = " + testResults + " - " + tariffAmount);
                
        try {

            out = response.getWriter();
            out.print(getName() + "|" + tariffDesciption + "|" + testResults);

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "CalculateTariffTestResulsCommand";
    }
}

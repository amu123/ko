/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.utils;

import java.util.ArrayList;
import java.util.List;
import neo.manager.KeyObject;

/**
 *
 * @author yuganp
 */
public class KeyObjectArray  extends neo.manager.KeyObjectArray{
    public KeyObjectArray() {
        keyValList = new ArrayList<KeyObject>();
    }
    
    public void put(String key, Object value) {
        if (key == null) return;
        boolean found = false;
        for (KeyObject kv : keyValList) {
            if (key.equals(kv.getKey())){
                found = true;
                kv.setValue(value);
                break;
            }
        }
        if (!found) {
            KeyObject kv = new KeyObject();
            kv.setKey(key);
            kv.setValue(value);
            keyValList.add(kv);
        }
    }
    
    public Object get(String key) {
        if (key == null) {
            return null;
        } else {
            for (KeyObject kv : keyValList) {
                if (key.equals(kv.getKey())) {
                    return kv.getValue();
                }
            }
            return null;
        }
    }
    
    public Object getValue(String key) {
        return get(key);
    }
    
    public List<String> getKeySet() {
        ArrayList list = new ArrayList<String>();
        for (KeyObject kv : keyValList) {
            list.add(kv.getKey());
        }
        return list;
    }

    public void setKeyValList(List<KeyObject> keyValList) {
        this.keyValList = keyValList;
    }
    
}

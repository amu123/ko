/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author johanl
 */
public class ForwardToPreAuthConfirmation extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        String authNumber = request.getParameter("authNumber");
        String aId = request.getParameter("authId");
        String returnFlag = request.getParameter("returnFlag");

        int authId = 0;
        if (aId != null && !aId.trim().equalsIgnoreCase("")) {
            try {
                authId = new Integer(aId.trim());
            } catch (NumberFormatException nf) {
                System.out.println("no auth id found");
            }
        }

        session.setAttribute("authNumber", authNumber);
        session.setAttribute("authCode", authId);
        session.setAttribute("returnFlag", returnFlag);
        session.setAttribute("docReturnFlag","Confirmation");
        System.out.println("in ForwardToPreAuthConfirmation");

        try {
            PrintWriter out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center>");
            out.println("<table cellspacing=\"4\">");
            out.println("<tr>");
            out.println("<td><label class=\"header\">Authorisation " + authNumber + " , with ID " + authId + " was selected</label></td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("<p> </p>");
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");

            GetAuthConfirmationDetails getConfirmed = new GetAuthConfirmationDetails();
            getConfirmed.execute(request, response, context);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ForwardToPreAuthConfirmation";
    }
}

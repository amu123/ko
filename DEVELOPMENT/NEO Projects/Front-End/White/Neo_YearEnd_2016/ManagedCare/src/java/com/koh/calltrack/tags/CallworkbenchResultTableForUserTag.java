/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.calltrack.tags;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CallWorkbenchDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author josephm
 */
public class CallworkbenchResultTableForUserTag extends TagSupport {

    private String commandName;
    private String javaScript;
    HashMap<String, String> callMap = null;
    HashMap<String, String> queryMap = null;

    /**
     * Called by the container to invoke this tag. The implementation of this method is provided by the tag library developer, and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {

        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        ServletRequest request = pageContext.getRequest();
        String client = String.valueOf(pageContext.getServletContext().getAttribute("Client"));
        String history = "" + request.getAttribute("CallHistorySearch");
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        String queries = "";
        System.out.println("CallWorkbench Start");
        List<CallWorkbenchDetails> cwList = new ArrayList<CallWorkbenchDetails>();
        //List<CallWorkbenchDetails> refList = new ArrayList<CallWorkbenchDetails>();
        if (history != null && !history.trim().equalsIgnoreCase("")
                && history.trim().equals("true")) {
            cwList = (List<CallWorkbenchDetails>) session.getAttribute("searchCallHistory");
            //System.out.println("cwList = (List<CallWorkbenchDetails>) session.getAttribute(searchCallHistory);");

        } else {
            String callWork = "" + request.getAttribute("CallWorkbench");
            System.out.println("CallWorkbench = " + callWork);
            if (callWork != null && !callWork.trim().equalsIgnoreCase("")
                    && callWork.trim().equals("true")) {
                cwList = port.getCallWorkBenchDetailsByUser(user.getUserId());
                //refList = port.getCallWorkBenchDetailsByReferredUser(user.getUserId());
                System.out.println("CallWorkBench size for userid " + user.getUserId() + " = " + cwList.size());
            }
        }

        try {

            out.println("<table width=\"700\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Nr</th><th>Call Number</th><th>Call Status</th><th>Queried</th><th>Assigned User</th><th>Date last attended to</th><th></th></tr>");
            //System.out.println("Just before the while loop");
            if (cwList == null) {

            } else {

                for (CallWorkbenchDetails cw : cwList) {
                    queries = cw.getCallQueries();
                    String qName = getCallQueryLookups(queries, port);
                    String calls = cw.getCallStatus();
                    String callTypes = getCallLookups(calls, port);
                    //int userId =  cw.getResoUserId();
                    //user =  port.getUserSafeDetailsById(userId);
                    if (callTypes != null && callTypes.equalsIgnoreCase("Open") && (cw.getResoUserId() == user.getUserId())) {
                        cw.setResoUserName(user.getUsername());
                        out.print("<tr>");
                        out.print("<td><label class=\"label\">" + cw.getCallTrackId() + "</label><input type=\"hidden\" name=\"trackId\" value=\"" + cw.getCallTrackId() + "\"/></td> ");
                        out.print("<td><label class=\"label\"> " + cw.getCallRefNo() + "</label></td> ");
                        out.print("<td><label class=\"label\"> " + callTypes + "</label> </td> ");
                        out.print("<td><label class=\"label\">" + qName + "</label></td> ");
                        out.print("<td><label class=\"label\"> " + cw.getResoUserName() + "</label> </td> ");
                        out.println("<td><label class=\"label\"> " + cw.getLastUpdate() + "</label></td>");
                        if (client != null && client.equalsIgnoreCase("Sechaba")) {
                            out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('WorkflowContentCommand', " + cw.getCallTrackId() + ")\"; value=\"WorkflowContentCommand\">View</button></td></tr>");
//                            out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('UpdateCallTrackingCommand', " + cw.getCallTrackId() + ")\"; value=\"UpdateCallTrackingCommand\">View</button></td></tr>");
                        }
                        if (client != null && client.equalsIgnoreCase("Agility")) {
                            out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('UpdateCallTrackingCommand', " + cw.getCallTrackId() + ")\"; value=\"UpdateCallTrackingCommand\">View</button></td></tr>");
                        }
                    }
                }
                //The reffered user loop
//            for (CallWorkbenchDetails cw : refList) {
//                queries = cw.getCallQueries();
//                String qName = getCallQueryLookups(queries, port);
//                String calls = cw.getCallStatus();
//                String callTypes = getCallLookups(calls, port);
//                int userId =  cw.getResoUserId();
//
//                user =  port.getUserSafeDetailsById(userId);
//                String username = user.getUsername();
//                //System.out.println("The username is " + username);
//                cw.setResoUserName(username);
//                if(callTypes.equalsIgnoreCase("Referred") && (cw.getResoUserId() == user.getUserId())){
//                out.print("<tr><form>");
//                out.print("<td><label class=\"label\">" + cw.getCallTrackId() + "</label><input type=\"hidden\" name=\"trackId\" value=\"" + cw.getCallTrackId() + "\"/></td> ");
//                out.print("<td><label class=\"label\"> " + cw.getCallRefNo() + "</label></td> ");
//                out.print("<td><label class=\"label\"> " + callTypes + "</label> </td> ");
//                out.print("<td><label class=\"label\">" + qName + "</label></td> ");
//                out.print("<td><label class=\"label\"> " + cw.getResoUserName() + "</label> </td> ");
//                out.println("<td><label class=\"label\"> " + cw.getLastUpdate() + "</label></td>");
//                out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('UpdateCallTrackingCommand', "+ cw.getCallTrackId() +")\"; value=\"UpdateCallTrackingCommand\">View</button></td></td></form></tr>");
//                }
//            }
                out.println("</table>");
            }

        } catch (IOException ex) {
            Logger.getLogger(CallWorkbenchResultTableTag.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("We are on the exception " + ex.getMessage());
        }
        System.out.println("CallWorkbench End");
        return super.doEndTag();

    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    private String getCallLookups(String callId, NeoManagerBean port) {

        String call = null;
        if (callMap == null || callMap.isEmpty()) {
            callMap = new HashMap<String, String>();
            Collection<LookupValue> lookupList = port.getCodeTable(105);
            for (LookupValue v : lookupList) {
                callMap.put(v.getId(), v.getValue());
            }
        }

        call = callMap.get(callId);

        return call;
    }

    private String getCallQueryLookups(String queries, NeoManagerBean port) {
        String qNameListTag = "";
        ArrayList<String> queryNames = new ArrayList();
        /*if(queries.contains("QM")){
        Collection<LookupValue> lookupList = port.getCodeTable(141);
        for (LookupValue lv : lookupList) {
        queryMap.put(lv.getId(), lv.getValue());
        }
        }*/
        if (queries != null && queries.contains("QC")) {
            if (queryMap == null || queryMap.isEmpty()) {
                queryMap = new HashMap<String, String>();
                Collection<LookupValue> lookupList = port.getCodeTable(151);
                for (LookupValue lv : lookupList) {
                    queryMap.put(lv.getId(), lv.getValue());
                }
            }

            String[] selectedQs = queries.split(",");
            for (String s : selectedQs) {
                String q = queryMap.get(s);
                queryNames.add(q);
            }
        }
        /*if(queries.contains("DS")){
        Collection<LookupValue> lookupList = port.getCodeTable(143);
        for (LookupValue lv : lookupList) {
        queryMap.put(lv.getId(), lv.getValue());
        }
        }
        if(queries.contains("BS")){
        Collection<LookupValue> lookupList = port.getCodeTable(144);
        for (LookupValue lv : lookupList) {
        queryMap.put(lv.getId(), lv.getValue());
        }
        }
        if(queries.contains("CS")){
        Collection<LookupValue> lookupList = port.getCodeTable(145);
        for (LookupValue lv : lookupList) {
        queryMap.put(lv.getId(), lv.getValue());
        }
        }
        if(queries.contains("MS")){
        Collection<LookupValue> lookupList = port.getCodeTable(146);
        for (LookupValue lv : lookupList) {
        queryMap.put(lv.getId(), lv.getValue());
        }
        }
        if(queries.contains("US")){
        Collection<LookupValue> lookupList = port.getCodeTable(147);
        for (LookupValue lv : lookupList) {
        queryMap.put(lv.getId(), lv.getValue());
        }
        }*/
        //  if (queries != null && !queries.equalsIgnoreCase(""))
        //  {

        if (queryNames != null) {
            qNameListTag = "<ol>";
            for (String qn : queryNames) {
                if (qn != null) {
                    qNameListTag += "<li><label class=\"label\">" + qn + "</label></li>";
                }
            }
            qNameListTag += "</ol>";

        }
        // }
        return qNameListTag;
    }

}

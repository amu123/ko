/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.member.application.command;

import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import agility.za.indexdocumenttype.IndexForMember;
import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.member.application.MemberAppCommon;
import com.koh.member.application.MemberAppMapping;
import com.koh.serv.PropertiesReader;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.io.PrintWriter;
import java.lang.Exception;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.xmlbeans.XmlDate;
/**
 *
 * @author yuganp
 */
public class MemberAppTabContentsCommand  extends NeoCommand {

    private static final String JSP_FOLDER = "/MemberApplication/";

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("in MemberAppTabContentsCommand");
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String tab = request.getParameter("tab");
        String page = "";
        Enumeration e = request.getParameterNames();
        System.out.println("tab contents -- = " + tab);
        HttpSession session = request.getSession();

        List<neo.manager.LookupValue> lookUpsClient = service.getNeoManagerBeanPort().findCodeTableForLookupType(341);
        if (lookUpsClient != null) {
            neo.manager.LookupValue val = lookUpsClient.get(0);
            if (val != null) {
                System.out.println("Client : " + val.getValue());
                session.setAttribute("Client", val.getValue());
            }
        }

        while (e.hasMoreElements()) {
            Object o = e.nextElement();
            System.out.println(o.toString() + " = " + request.getParameter(o.toString()));
        }
        if ("MemberAppDetails".equalsIgnoreCase(tab)) {
            page = getMemberAppDetails(port, request);
        } else if ("Dependants".equalsIgnoreCase(tab)) {
            page = getMemberDepAppDetails(port, request);
        } else if ("SpecificQuestions".equalsIgnoreCase(tab)) {
            page = getSpecificQuestions(port, request);
        } else if ("GeneralQuestions".equalsIgnoreCase(tab)) {
            page = getGeneralQuestions(port, request);
        } else if ("HospitalAdmission".equalsIgnoreCase(tab)) {
            page = getMemberAppHospital(port, request);
        } else if ("ChronicMedication".equalsIgnoreCase(tab)) {
            page = getMemberAppChronic(port, request);
        } else if ("Underwriting".equalsIgnoreCase(tab)) {
            page = getMemberAppUnderwriting(port, request);
        } else if ("Notes".equalsIgnoreCase(tab)) {
            page = getMemberAppNotes(request);
        } else if ("AuditTrail".equalsIgnoreCase(tab)) {
            page = getMemberAppAuditTrail(request);
        } else if ("Documents".equalsIgnoreCase(tab)) {
            page = getMemberAppDocuments(request);
        } else if ("DocumentsGeneration".equalsIgnoreCase(tab)) {
            page = getMemberAppDocumentsForGeneration(request);
        }
        PrintWriter out = null;
        try {            
            if (page.isEmpty()) {
                int status = getIntParam(request, "memberAppStatus");
                if (status == 7) {
                    request.setAttribute("memberAppNumber", request.getParameter("memberAppNumber"));
                    request.setAttribute("target_div", request.getParameter("target_div"));
                    request.setAttribute("main_div", request.getParameter("main_div"));
                    
                    String appNumStr = request.getParameter("memberAppNumber");
                    System.out.println("appNumStr"+appNumStr);
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                    String startDate = null;
                    int appNum;
                    
                    if (appNumStr != null && !appNumStr.isEmpty()) {
                        appNum = Integer.parseInt(appNumStr);
                        MemberApplication ma = port.getMemberApplicationByApplicationNumber(appNum);
                        if(ma.getCoverStartDate() != null){
                            startDate = dateFormat.format(ma.getCoverStartDate().toGregorianCalendar().getTime());
                        }
                        request.setAttribute("inceptionDate", startDate);
                    }
                    
                    
                    RequestDispatcher dispatcher = context.getRequestDispatcher(JSP_FOLDER + "MemberAppActivate.jsp");
                    dispatcher.forward(request, response);
                } else if (status == 2) {
                    String client = String.valueOf(context.getAttribute("Client"));
                    if (client != null && client.equals("Sechaba")) {
                        System.out.println("Product" + client);
                            if (client != null && client.equals("Sechaba")) {
                                request.getAttribute("memberAppCoverNumber"); //, ma.getCoverNumber());
                                request.getAttribute("memberAppNumber"); //, ma.getApplicationNumber());
                                request.setAttribute("memberAppNumber", request.getParameter("memberAppNumber"));
                                request.setAttribute("memberAppCoverNumber", request.getParameter("memberAppCoverNumber"));
                                request.setAttribute("target_div", request.getParameter("target_div"));
                                request.setAttribute("main_div", request.getParameter("main_div"));
                                System.out.println("APP NUMBER " + request.getParameter("memberAppNumber"));
                                System.out.println(" COVER NUMBER " + request.getParameter("memberAppCoverNumber"));

                                String appNumStr = request.getParameter("memberAppNumber");
                                System.out.println("appNumStr" + appNumStr);
                                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                                String startDate = null;
                                int appNum;
                                if (appNumStr != null && !appNumStr.isEmpty()) {
                                    appNum = Integer.parseInt(appNumStr);
                                    MemberApplication ma = port.getMemberApplicationByApplicationNumber(appNum);
                                    if (ma.getCoverStartDate() != null) {
                                        startDate = dateFormat.format(ma.getCoverStartDate().toGregorianCalendar().getTime());
                                    }
                                    request.setAttribute("inceptionDate", startDate);
                                }
                                ArrayList<LookupValue> lookUps = (ArrayList<LookupValue>) NeoCommand.service.getNeoManagerBeanPort().getCodeTable(new Integer(349));
                                List<OutstandingDocuments> od = port.getOutstandingDocuments(Integer.parseInt(appNumStr));
                                int size = lookUps.size();
                                if (od != null && od.size() > 0) {
                                    for (int y = 0; y < od.size(); y++) {
                                        for (int x = size - 1; x > -1; x--) {
                                            if (lookUps.get(x).getId().contains(od.get(y).getOutstandingDocumentsCode())) {
                                                lookUps.remove(x);

                                            }
                                        }
                                    }
                                    request.setAttribute("documentListOptions", lookUps);
                                } else {

                                    System.out.println("The appNum does not exists");
                                    request.setAttribute("documentListOptions", lookUps);
                                }

                                System.out.println("Size of lookups after " + lookUps.size());

                                RequestDispatcher dispatcher = context.getRequestDispatcher(JSP_FOLDER + "OutstandingDocuments.jsp");
                                dispatcher.forward(request, response);
                            }
                        }
                } else {
                    updateAppStatus(port, request);
                    out = response.getWriter();
                    out.println(TabUtils.buildJsonResult("OK", null, null, "\"message\":\"Status has been changed.\""));
                }
            } else {
                request.setAttribute("memberAppType", request.getParameter("memberAppType"));
                RequestDispatcher dispatcher = context.getRequestDispatcher(page);
                dispatcher.forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (out != null) out.close();
        }
        return null;
    }

    @Override
    public String getName() {
        return "MemberApplicationTabContentsCommand";
    }

    private String getMemberAppDetails(NeoManagerBean port,HttpServletRequest request) {
        String appNumStr = request.getParameter("memberAppNumber");
        String coverNumr = request.getParameter("memberAppCoverNumber");
        System.out.println("appNumStr" + appNumStr);
        System.out.println("memberAppCoverNumber " + coverNumr);
        int appNum;
        MemberApplication ma = null;
        
        if (appNumStr != null && !appNumStr.isEmpty()) {
            appNum = Integer.parseInt(appNumStr);
            
            if (request.getSession().getAttribute("Client").equals("Sechaba")) {
                ma = port.getMemberApplicationByApplicationNumberSechaba(appNum);
                request.setAttribute("optionId", ma.getOptionId());
                
                ProviderSearchCriteria search = new ProviderSearchCriteria();
                search.setEntityTypeID(3);

                if (ma.getDoctorPracticeNumber() != null && !ma.getDoctorPracticeNumber().equals("")) {
                    search.setPracticeNumber(ma.getDoctorPracticeNumber());

                    List<ProviderDetails> pdList = new ArrayList<ProviderDetails>();
                    pdList = service.getNeoManagerBeanPort().findAllProvidersByCriteria(search);

                    for (ProviderDetails pd : pdList) {
                        if (ma.getDoctorPracticeNumber().equals(pd.getPracticeNumber())) {
                            request.setAttribute("MemberApp_doctorName", pd.getPracticeName());
                            request.setAttribute("MemberApp_doctorNetwork", pd.getNetworkName());
                            request.setAttribute("memberAppNumber", appNumStr);
                        }
                    }
                }

                if (ma.getDentistPracticeNumber() != null && !ma.getDentistPracticeNumber().equals("")) {
                    search.setPracticeNumber(ma.getDentistPracticeNumber());

                    List<ProviderDetails> pdList = new ArrayList<ProviderDetails>();
                    pdList = service.getNeoManagerBeanPort().findAllProvidersByCriteria(search);

                    for (ProviderDetails pd : pdList) {
                        if (ma.getDentistPracticeNumber().equals(pd.getPracticeNumber())) {
                            request.setAttribute("MemberApp_dentistName", pd.getPracticeName());
                            request.setAttribute("MemberApp_dentistNetwork", pd.getNetworkName());
                        }
                    }
                }

                if (ma.getOptometristPracticeNumber() != null && !ma.getOptometristPracticeNumber().equals("")) {
                    search.setPracticeNumber(ma.getOptometristPracticeNumber());

                    List<ProviderDetails> pdList = new ArrayList<ProviderDetails>();
                    pdList = service.getNeoManagerBeanPort().findAllProvidersByCriteria(search);

                    for (ProviderDetails pd : pdList) {
                        if (ma.getOptometristPracticeNumber().equals(pd.getPracticeNumber())) {
                            request.setAttribute("MemberApp_optometristName", pd.getPracticeName());
                            request.setAttribute("MemberApp_optometristNetwork", pd.getNetworkName());
                        }
                    }
                }
                MemberAppMapping.setMemberApplicationSechaba(request, ma);                
            } else {
                ma = port.getMemberApplicationByApplicationNumber(appNum);
                MemberAppMapping.setMemberApplication(request, ma);
            }
         
            request.setAttribute("MemberName", ma.getFirstName() + " " + ma.getLastName());
            request.setAttribute("memberAppNumber", ma.getApplicationNumber());
            request.setAttribute("memberAppCoverNumber", ma.getCoverNumber());
            
            List<MemberApplication> brokerList = port.getMemberApplicationBrokerCode(ma.getCoverNumber());
            if(ma.getBrokerConsultantId() != 0){
                request.setAttribute("brokerConsultant", 2);
                if(!brokerList.isEmpty()){
                    request.setAttribute("MemberApp_brokerConsultantCode", brokerList.get(0).getBrokerConsultantCode());
                }
            }
            else{
                request.setAttribute("brokerConsultant", 1);
                if(!brokerList.isEmpty()){
                    request.setAttribute("MemberApp_brokerCode", brokerList.get(0).getBrokerCode());
                }
            }
            
            request.getSession().setAttribute("memberAppCoverNumber", ma.getCoverNumber());
        } else {
            String coverNo = request.getParameter("memberAppCoverNumber");
            Security sec = new Security();
            NeoUser user = getNeoUser(request);
            sec.setCreatedBy(user.getUserId());
            sec.setLastUpdatedBy(user.getUserId());
            sec.setSecurityGroupId(user.getSecurityGroupId());
            
            KeyValueArray kva = port.createNewMemberApplication(sec, coverNo);
            Map<String, String> map = MapUtils.getMap(kva);
            String appNo = map.get("ApplicationNumber");
            String coverNum = map.get("CoverNumber");
            request.setAttribute("memberAppNumber", appNo);
            request.setAttribute("memberAppCoverNumber", coverNum);
            request.setAttribute("MemberApp_correspondanceLanguage", "2");
            if (coverNo != null && !coverNo.isEmpty()) {
                request.setAttribute("MemberApp_application_type", 1);
                CoverDetails cd = port.getPrincipalMemberDetailsToday(coverNo);
                setCoverDetails(port, request, cd, appNo, coverNo);
            }
        }

        return JSP_FOLDER + "MemberAppDetails.jsp";
    }
    
    private void setCoverDetails(NeoManagerBean port, HttpServletRequest request,CoverDetails cd, String appNo, String coverNo) {
        System.out.println(" in set cover details :" +cd);
        PersonDetails pd = cd != null ? port.getPersonDetailsByEntityId(cd.getEntityId()) : null;
        
        if (pd != null) {
            List<ContactDetails> con = port.getContactDetailsByEntityId(cd.getEntityId());
            MemberApplication ma = new MemberApplication();

            System.out.println("setting values :" + pd.getName());
            request.setAttribute("MemberApp_title", null);
            request.setAttribute("MemberApp_initials", pd.getInitials());
            ma.setInitials(pd.getInitials());
            request.setAttribute("MemberApp_firstName", pd.getName());
            ma.setFirstName(pd.getName());
            request.setAttribute("MemberApp_lastName", pd.getSurname());
            ma.setLastName(pd.getSurname());
//            request.setAttribute("MemberApp_gender", pd.getGender());
            request.setAttribute("MemberApp_DateOfBirth", DateTimeUtils.convertXMLGregorianCalendarToDate(pd.getDateOfBirth()));
            ma.setDateOfBirth(pd.getDateOfBirth());
            request.setAttribute("MemberApp_identificationNumber", pd.getIDNumber());
            ma.setIdentificationNumber(pd.getIDNumber());
            request.setAttribute("MemberApp_employerEntityId", "0");
            ma.setEmployerEntityId(0);
            request.setAttribute("MemberApp_correspondanceLanguage", pd.getLanguageId());
            ma.setCorrespondanceLanguage(pd.getLanguageId());
//            request.setAttribute("MemberApp_gender", "1");
//            request.setAttribute("MemberApp_identificationType", "2");
            request.setAttribute("MemberApp_productId", cd.getProductId());
            ma.setProductId(cd.getProductId());
            request.setAttribute("MemberApp_optionId", cd.getOptionId());
            ma.setOptionId(cd.getOptionId());
            //request.setAttribute("MemberApp_paymentMethod", "2");
            ma.setPaymentMethod(2);            
            if (con != null) {
                for (ContactDetails cd1 : con) {
                    if (cd1 == null) {
                        continue;
                    }
                    String fieldStart = "";
                    switch (cd1.getCommunicationMethodId()) {
                        case 3: fieldStart = "MemberApp_telMobile1";
                            ma.setTelMobile1(cd1.getMethodDetails());
                            break;
                        case 4: fieldStart = "MemberApp_telWork";
                            ma.setTelWork(cd1.getMethodDetails());
                            break;
                        case 5: fieldStart = "MemberApp_telHome";
                            ma.setTelHome(cd1.getMethodDetails());
                            break;
                    }
                    request.setAttribute(fieldStart, cd1.getMethodDetails() == null ? "" : cd1.getMethodDetails());
                }
            }
            try {
                ma.setApplicationStatus(1);
                ma.setApplicationNumber(getIntValue(appNo));
                ma.setCoverNumber(coverNo);
                    port.saveMemberApplication(ma);
            } catch (Exception e) {
                
            }
        }
    }  

    protected String getMemberDepAppDetails(NeoManagerBean port, HttpServletRequest request) {
        List<MemberDependantApp> depList = port.getMemberApplicationDependantByApplicationNumber(getIntParam(request, "memberAppNumber"));
        request.setAttribute("memberAppNumber", request.getParameter("memberAppNumber"));
        request.setAttribute("memberAppCoverNumber", request.getParameter("memberAppCoverNumber"));
        request.setAttribute("depList", depList);

        MemberApplication ma = port.getMemberApplicationByCoverNumber(request.getParameter("memberAppCoverNumber"));
        request.setAttribute("optionId", ma.getOptionId());

        int maxDep = 0;
        if (depList != null && !depList.isEmpty()) {
            for (MemberDependantApp depApp : depList) {
                if (depApp.getDependentNumber() > maxDep) {
                    maxDep = depApp.getDependentNumber();
                }
            }
        }
        System.out.println("Max dep app " + maxDep);
        String appType = request.getParameter("memberAppType");
        System.out.println("App Type " + appType + " " + request.getParameter("memberAppCoverNumber"));
        if (appType != null && appType.equals("1")) {
            List<CoverDetails> dependentsForCoverNumber = port.getDependentsForCoverNumber(request.getParameter("memberAppCoverNumber"));
            if (dependentsForCoverNumber != null && !dependentsForCoverNumber.isEmpty()) {
                for (CoverDetails cd : dependentsForCoverNumber) {
        System.out.println("dep app " + cd.getDependentNumber());
                    if (cd.getDependentNumber() > maxDep) {
                        maxDep = cd.getDependentNumber();
                    }
                }
            }
        }
        System.out.println("Max dep total " + maxDep);
        request.setAttribute("maxDep", Integer.toString(maxDep));
        return JSP_FOLDER + "MemberDependantApp.jsp";
    }

    protected String getSpecificQuestions(NeoManagerBean port, HttpServletRequest request) {
        request.setAttribute("memberAppNumber", request.getParameter("memberAppNumber"));
        int appNum = getIntParam(request, "memberAppNumber");
        Object obj = port.fetchAppQuestionsByGroupTypeAndAppNum(1, appNum );
        request.setAttribute("MemAppSpecificQuestions", obj);
        List<MemAppSpecificDetail> fetchMemAppSpecificDetailByAppNum = port.fetchMemAppSpecificDetailByAppNum(appNum);
        request.setAttribute("MemberAppSpecDetails", fetchMemAppSpecificDetailByAppNum);
        request.setAttribute("MemberAppGenDepMap", MemberAppCommon.getDependantMap(service, appNum));
        return JSP_FOLDER + "MemberAppSpecificQuestions.jsp";
    }

    protected String getGeneralQuestions(NeoManagerBean port, HttpServletRequest request) {
        request.setAttribute("memberAppNumber", request.getParameter("memberAppNumber"));
        int appNum = getIntParam(request, "memberAppNumber");
        Object obj = port.fetchAppQuestionsByGroupTypeAndAppNum(2, appNum);
        request.setAttribute("MemAppGeneralQuestions", obj);
        List<MemAppGenDetail> fetchMemAppGenDetailByAppNum = port.fetchMemAppGenDetailByAppNum(appNum);
        request.setAttribute("MemberAppGenDetails", fetchMemAppGenDetailByAppNum);
        request.setAttribute("MemberAppGenDepMap", MemberAppCommon.getDependantMap(service, appNum));
        request.setAttribute("MemberAppGenQMap", getQuestionMap((List<AppQuestions>)obj));
        return JSP_FOLDER + "MemberAppGeneralQuestions.jsp";
    }
    
    
    private Map<Integer,String> getQuestionMap(List<AppQuestions> q) {
        Map<Integer,String> map = new HashMap<Integer,String>();
        for (AppQuestions app :q) {
            map.put(app.getQuestionNumber(), app.getQuestionValue());
        }
        return map;
    }

    protected String getMemberAppHospital(NeoManagerBean port, HttpServletRequest request) {
        request.setAttribute("memberAppNumber", request.getParameter("memberAppNumber"));
        int appNum = getIntParam(request, "memberAppNumber");
        Object hd = port.fetchMemAppHospDetailByAppNum(appNum);
        request.setAttribute("MemberAppHospDetails", hd);
        request.setAttribute("DepMap", MemberAppCommon.getDependantMap(service, appNum));
        return JSP_FOLDER + "MemberAppHospital.jsp";
    }

    protected String getMemberAppChronic(NeoManagerBean port, HttpServletRequest request) {
        request.setAttribute("memberAppNumber", request.getParameter("memberAppNumber"));
        int appNum = getIntParam(request, "memberAppNumber");
        Object hd = port.fetchMemAppChronicDetailByAppNum(appNum);
        request.setAttribute("MemberAppChronicDetails", hd);
        request.setAttribute("DepMap", MemberAppCommon.getDependantMap(service, appNum));
        return JSP_FOLDER + "MemberAppChronic.jsp";
    }

    protected String getMemberAppUnderwriting(NeoManagerBean port, HttpServletRequest request) {
        int appNum = getIntParam(request, "memberAppNumber");
          NeoUser neoUser = (NeoUser) request.getSession().getAttribute("persist_user");
        request.setAttribute("memberAppCoverNumber", request.getParameter("memberAppCoverNumber"));
       String coverNumber = (String)request.getAttribute("memberAppCoverNumber");
       int brokerId =0;
        System.out.println("coverNumber"+coverNumber);
        //int cov = Integer.parseInt(coverNumber);
      HttpSession session = request.getSession();

            
        List<MemAppCondWait> memAppCondWait = port.fetchMemAppCondWaitByAppNum(appNum);
       
        List<MemAppUnderwriting> memAppUnderwriting = port.fetchMemAppUnderwritingByAppNum(appNum);
         
          String appNumStr = request.getParameter("memberAppNumber");
       
        int appNums;
        request.setAttribute("CondWaitList", memAppCondWait);
        int groupConcessionCode = port.fetchGroupConcessionByCovNum(coverNumber);
         
         if (appNumStr != null && !appNumStr.isEmpty()) {
            appNums = Integer.parseInt(appNumStr);
        MemberApplication ma = port.getMemberApplicationByApplicationNumber(appNum);
        
           // MemberAppMapping.setMemberApplication(request, ma);
         brokerId = ma.getBrokerId();
         
         }     // request.setAttribute("memberBrokerId", request.getParameter("memberBrokerId"));
         

         String warning = null;
        session.setAttribute("warningW", null);
        if(groupConcessionCode > 0){
           warning = "groupConcession";
        }
         int brokerConcession = port.fetchMemberBrokerConcessionByentityId(brokerId);
        System.out.println("brokerConcession"+brokerConcession);
        if (brokerConcession > 0) {
            warning = "brokerConcession";
        }  
        if (brokerConcession > 0 && groupConcessionCode > 0) {
            warning = "brokerGroupConcession";
        }
       session.setAttribute("warningW", warning);
       System.out.println("warning message"+session.getAttribute("warningW"));
//        request.setAttribute("UnderList", MemberAppCommon.getDependantMap(service, appNum));
        request.setAttribute("DependentList", MemberAppCommon.getDependantListMap(service, appNum, request.getParameter("memberAppType")));
        request.setAttribute("memberAppNumber", request.getParameter("memberAppNumber"));
        for (MemAppUnderwriting mu : memAppUnderwriting ) {
            if (mu.getDependentNumber() == 0) {
                request.setAttribute("MemberAppUnderwriting_waitingPeriod", mu.getApplyUnderwritingInd());
                
            }
            String startDate = DateTimeUtils.convertXMLGregorianCalendarToDate(mu.getConcessionStartDate());
            String endDate = DateTimeUtils.convertXMLGregorianCalendarToDate(mu.getConcessionEndDate());
            request.setAttribute("MemberAppUnderwriting_generalWaiting_" + mu.getDependentNumber(), mu.getGeneralWaitingInd());
            request.setAttribute("MemberAppUnderwriting_start_date_" + mu.getDependentNumber(), startDate);
            request.setAttribute("MemberAppUnderwriting_end_date_" + mu.getDependentNumber(), endDate);
            request.setAttribute("MemberAppUnderwriting_allowPMB_" + mu.getDependentNumber(), mu.getAllowPmbInd());
            request.setAttribute("MemberAppUnderwriting_conditionSpecific_" + mu.getDependentNumber(), mu.getConditionSpecificInd());
            request.setAttribute("MemberAppUnderwriting_ljp_" + mu.getDependentNumber(), mu.getLjp());
  
        }
        
        Enumeration e = request.getAttributeNames();
        while (e.hasMoreElements()) {
            Object o = e.nextElement();
            System.out.println(o.toString() + " = " + request.getAttribute(o.toString()));
        }
        return JSP_FOLDER + "MemberAppUnderwriting.jsp";
    }

    private String getMemberAppNotes(HttpServletRequest request) {
        request.setAttribute("memberAppNumber", request.getParameter("memberAppNumber"));
        System.out.println("memResp = " + request.getParameter("menuResp"));
        request.setAttribute("menuResp", request.getParameter("menuResp"));

        return JSP_FOLDER + "MemberAppNotes.jsp";
    }

    private String getMemberAppAuditTrail(HttpServletRequest request) {
        request.setAttribute("memberAppNumber", request.getParameter("memberAppNumber"));

        return JSP_FOLDER + "MemberAppAudit.jsp";
    }
    private String getMemberAppDocumentsForGeneration(HttpServletRequest request) {
        request.setAttribute("memberEntityId", request.getParameter("memberAppCoverNumber"));
        request.setAttribute("memberAppCoverNumber", request.getParameter("memberAppCoverNumber"));
        System.out.println(request.getParameter("memberAppCoverNumber"));
        return JSP_FOLDER + "MemAppDocumentGeneration.jsp";
    }
    private String getMemberAppDocuments(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        
        try {
            request.setAttribute("folderList", new PropertiesReader().getProperty("DocumentIndexWorkFolder"));
            request.setAttribute("listOrWork", "Work");
            String appNumStr = request.getParameter("memberAppNumber");
            System.out.println("appNumStr" + appNumStr);
            String memberNumber = (String) session.getAttribute("memberAppCoverNumber");

            if (memberNumber != null && !memberNumber.trim().equals("")) {
                IndexDocumentRequest req = new IndexDocumentRequest();
                NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
                req.setAgent(loggedInUser);
                req.setIndexType("IndexForMember");
                req.setEntityNumber(memberNumber);
                req.setSrcDrive(new PropertiesReader().getProperty("DocumentIndexIndexDrive"));
                IndexDocumentResponse resp = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);
                for (IndexForMember index : resp.getIndexForMember()) {
                    String typeName = index.getDocType();
                    Pattern p = Pattern.compile("([0-9]*)");
                    Matcher m = p.matcher(typeName);
                    if (m.matches()) {
                        typeName = port.getValueForId(212, typeName);
                        index.setDocType(typeName);
                    }
                }
                request.setAttribute("indexList", resp.getIndexForMember());
            }
            request.setAttribute("memberAppCoverNumber", memberNumber);
            request.setAttribute("memberAppNumber", appNumStr);
        } catch (Exception ex) {
            ex.printStackTrace();
        }       
        return JSP_FOLDER + "MemberAppDocumentFilter.jsp";
    }
    
    private void updateAppStatus(NeoManagerBean port, HttpServletRequest request) {
        int appNum = getIntParam(request, "memberAppNumber");
        int status = getIntParam(request, "memberAppStatus");
        Security sec = new Security();
        NeoUser user = getNeoUser(request);
        sec.setCreatedBy(user.getUserId());
        sec.setLastUpdatedBy(user.getUserId());
        sec.setSecurityGroupId(user.getSecurityGroupId());
        if (status == 7) {
            if (request.getSession().getAttribute("Client").equals("Sechaba")) {
                port.finaliseMemberApplicationSechaba(appNum, sec, DateTimeUtils.convertDateToXMLGregorianCalendar(new Date()));
            } else {
                port.finaliseMemberApplication(appNum, sec, DateTimeUtils.convertDateToXMLGregorianCalendar(new Date()));
            }
        } else {
            int result = port.updateMemberApplicationStatus(appNum, status, sec);
        }
    }
    
    private int getIntParam(HttpServletRequest request, String paramName) {
        try {
            String maxDepStr = request.getParameter(paramName);
            return Integer.parseInt(maxDepStr);
        } catch (java.lang.Exception e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }
    
    private int getIntValue(String value) {
        try {
            return Integer.parseInt(value);
        } catch (java.lang.Exception e) {
            return 0;
        }
    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser)request.getSession().getAttribute("persist_user");
    }
    

    

}

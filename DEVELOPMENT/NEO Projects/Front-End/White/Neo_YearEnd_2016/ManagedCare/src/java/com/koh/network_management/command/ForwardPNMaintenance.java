/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.network_management.command;

import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;

import java.net.URL;

import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import neo.manager.NeoUser;
import org.apache.log4j.Logger;


/**
 *
 * @author Leslie
 */
public class ForwardPNMaintenance extends NeoCommand {

    private Logger logger = Logger.getLogger(ForwardPNMaintenance.class);
    
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        logger.info("************ ForwardPNMaintenance *****************");
        
        String status = "";
        
        HttpSession session = request.getSession();
        
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
       
        com.koh.network_management.command.NeoUser tmpUser = 
                new com.koh.network_management.command.NeoUser(user.getUserId(),user.getSecurityGroupId(),user.getUsername(),user.getPassword());

        if (user==null) {
            status = "failed";
            return null;
        }
        
        try {

            String path = new PropertiesReader().getProperty("networkManagementServletURL");
            //String path = "http://163.197.202.33:8484/PracticeNetworkMaintenance/AuthenticateServlet";
            
            path = path + "?id=" + new Date().getTime();
            
            logger.info("URL Path to AuthenticateServlet : " + path);
            
            URL url = new URL(path);
            
            logger.info("Opening URL Connection...");
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(10000);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            
            conn.connect();
            logger.info("URL Connection Connected");
            
            ObjectOutputStream oos = new ObjectOutputStream(conn.getOutputStream());
            logger.info("Writing Object NeoUser to outputstream....");
            oos.writeObject(tmpUser);
            logger.info("Done");
            
            ObjectInputStream ois = new ObjectInputStream(conn.getInputStream());
            
            status = (String)ois.readObject();
            logger.info("status : " + status);
            
            ois.close();

            oos.close();
           
            conn.disconnect();
            
            PrintWriter out = response.getWriter();
            
            out.println(status);
            
        } catch (Exception e) {
            
            logger.error(e);
        
        }
            
        logger.info("************ ForwardPNMaintenance end *****************");  
        return null;
    }

    @Override
    public String getName() {
        return "ForwardPNMaintenance";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author johanl
 */
public class AddAuthBasketTariffToSession extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        try {
            PrintWriter out = response.getWriter();

            String provDesc = "" + session.getAttribute("providerDesc");
            System.out.println("provider discipline selected = " + provDesc);

            String btCode = "" + session.getAttribute("btCode");
            String btDesc = "" + session.getAttribute("btDesc");

            String tOpt = "" + session.getAttribute("tariffOption");
            System.out.println("selected tariff option = " + tOpt);
            int tariffOption = Integer.parseInt(tOpt);

            String freq = request.getParameter("tariffFreq");
            System.out.println("freq = " + freq);
            int tFeq = 0;
            if (freq != null && !freq.trim().equalsIgnoreCase("")) {
                tFeq = new Integer(freq);
                boolean valid = true;
                String errorMsg = "";

                List<AuthTariffDetails> basketList = (List<AuthTariffDetails>) session.getAttribute("AuthBasketTariffs");
                if (basketList == null || basketList.isEmpty() == true) {
                    basketList = new ArrayList<AuthTariffDetails>();

                } else {
                    //tariff code and option validation
                    for (AuthTariffDetails at : basketList) {
                        System.out.println("tariff in basket when adding = " + at.getTariffCode() + " - " + at.getProviderType());
                        if (at.getTariffCode().equals(btCode)) {
                            if (btCode.equals("0190")) {
                                //allow 0190, check discipline
                                if (provDesc.equals(at.getProviderType())) {
                                    System.out.println("Same discpline found for 0190");
                                    errorMsg = "Same Discpline for Tariff Code already added to basket";
                                    valid = false;
                                }

                            } else {
                                if (provDesc.equals(at.getProviderType())) {
                                    System.out.println("same code found - loaded code = " + at.getTariffCode() + ", selected code =" + btCode);
                                    errorMsg = "Same Tariff Code already added to basket";
                                    valid = false; 
                                }
                            }
                        }

                        int basketTO = at.getTariffOption();
                        if (basketTO != 0) {
                            if (basketTO == tariffOption) {
                                System.out.println("same tariff option found - loaded opt = " + at.getTariffOption() + ", selected opt =" + tariffOption);
                                errorMsg = "Specific test already added to basket";
                                valid = false;
                            }
                        }

                    }
                }
                if (valid) {
                    //set values to list
                    AuthTariffDetails ab = new AuthTariffDetails();
                    ab.setProviderType(provDesc);
                    ab.setTariffCode(btCode);
                    ab.setTariffDesc(btDesc);
                    ab.setTariffOption(tariffOption);
                    ab.setFrequency(tFeq);
                    ab.setTariffType("2");

                    basketList.add(ab);

                    session.setAttribute("AuthBasketTariffs", basketList);

                    session.removeAttribute("providerDesc");
                    session.removeAttribute("pcType");
                    session.removeAttribute("btCode");
                    session.removeAttribute("btDesc");
                    session.removeAttribute("btOption");
                    session.removeAttribute("tariffFreq");
                    session.removeAttribute("basketAddError");

                    System.out.println("Done|");
                } else {
                    System.out.println("Error|");
                    session.setAttribute("basketAddError", errorMsg);
                }

            } else {
                System.out.println("Error|");
            }

        } catch (Exception ex) {
            System.out.println("AddAuthBasketTariffToSession error : " + ex.getMessage());
            System.out.println("Error|");
        }

        try {
            String nextJSP = "/PreAuth/AuthSpecificBasketTariff.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "AddAuthBasketTariffToSession";
    }
}

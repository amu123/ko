/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoUser;

/**
 *
 * @author gerritj
 */
public class ActivateUserWLCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        int user_id;
        int id;
        
        if(request.getParameter("external_reference") == null || request.getParameter("external_reference") == "" || request.getParameter("external_reference") == "null"){
            user_id = Integer.parseInt(session.getAttribute("userid").toString());//Integer.parseInt(request.getAttribute("external_reference").toString());
        }else if(request.getAttribute("external_reference") == null || request.getAttribute("external_reference") == "" || request.getAttribute("external_reference") == "null"){
            user_id = new Integer(request.getParameter("external_reference"));
        }else{
            user_id = Integer.parseInt(session.getAttribute("userid").toString());
        }
        
        if(session.getAttribute("userid") != null){
            user_id = Integer.parseInt(session.getAttribute("userid").toString());
        }

        
        NeoUser user = service.getNeoManagerBeanPort().getUserSafeDetailsById(user_id);
        if (user != null) {
            session.setAttribute("name", user.getName());
            request.setAttribute("name", user.getName());
            request.setAttribute("user_id", request.getParameter("external_reference"));
            request.setAttribute("workitem_id", request.getParameter("workitem_id"));
            request.setAttribute("surname", user.getSurname());
            request.setAttribute("email", user.getEmailAddress());
            request.setAttribute("username", user.getUsername());
            String nextJSP = "/SystemAdmin/ActivateUser.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            try {
                dispatcher.forward(request, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //set error field
            request.setAttribute("error", "User does not exist");
            String nextJSP = "/SystemAdmin/ActivateUser.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            try {
                dispatcher.forward(request, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "ActivateUserWLCommand";
    }
}

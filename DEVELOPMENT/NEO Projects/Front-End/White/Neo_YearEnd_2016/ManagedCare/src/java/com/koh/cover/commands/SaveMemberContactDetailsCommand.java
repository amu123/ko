/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.agile.security.webservice.EmailAttachment;
import com.agile.security.webservice.EmailContent;
import com.koh.command.NeoCommand;
import com.koh.cover.MemberMainMapping;
import com.koh.employer.command.TabUtils;
import com.koh.fe.command.LoginCommand;
import com.koh.serv.PropertiesReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.Exception;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;

/**
 *
 * @author johanl
 */
public class SaveMemberContactDetailsCommand extends NeoCommand {

    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String result = validateAndSave(request);
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
            response.setHeader("Refresh", "1; URL=/ManagedCare/Member/MemberContactDetails.jsp");
        } catch (IOException ex) {
            ex.printStackTrace();
        }        
        return null;
    }

    @Override
    public String getName() {
        return "SaveMemberContactDetailsCommand";
    }
    
    private String validateAndSave(HttpServletRequest request) {
        Map<String, String> errors = MemberMainMapping.validateContact(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (save(request)) {
                additionalData = "\"message\":\"Member Contact Details Updated\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error updating the member contact details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
    }

    private boolean save(HttpServletRequest request) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);
        
        int entityId = MemberMainMapping.getEntityId(request);
        
        try {
            List<ContactDetails> adList = MemberMainMapping.getContactDetails(request, neoUser, entityId);
            String contactLocation = new PropertiesReader().getProperty("DocumentIndexIndexDrive") + port.saveContactListDetails(adList, sec, entityId);
            
            //--------------------MAILING Functionality-----------------//
            String coverNumber = port.getCoverNumberForEntity(entityId);
            List<CoverDetails> coverD = port.getCoverDetailsByCoverNumber(coverNumber);
            boolean hasEmail = port.memberHasEmail(entityId);
            if (hasEmail){
                ContactDetails contactDetail = port.getContactDetails(entityId, 1);

                InputStream in = null;
                try {
                    String emailFrom = "Noreply@agilityghs.co.za";
                    String scheme = String.valueOf(session.getAttribute("schemeName"));
                    String emailTo = contactDetail.getMethodDetails();
                    int productId = 0;

                    if (scheme.equalsIgnoreCase("Resolution Health")) {
                        productId = 1;
                        emailFrom = "Noreply@agilityghs.co.za";
                    } else if (scheme.equalsIgnoreCase("Spectramed")) {
                        productId = 2;
                        emailFrom = "Noreply@spectramed.co.za";
                    } else if (scheme.equalsIgnoreCase("Sizwe")){
                        productId = 3;
                        emailFrom = "do-not-reply@sizwe.co.za";
                    }
                    boolean emailSent = false;

                    System.out.println("DocumentIndexIndexDrive: " + contactLocation);
                    //Creating a Byte out of the pdf
                    File file = new File(contactLocation);
                    if (file.exists()) {
                        in = new FileInputStream(file);
                        int contentLength = (int) file.length();
                        org.apache.log4j.Logger.getLogger(this.getClass()).info("The content length is " + contentLength);
                        ByteArrayOutputStream temporaryOutput;
                        if (contentLength != -1) {
                            temporaryOutput = new ByteArrayOutputStream(contentLength);
                        } else {
                            temporaryOutput = new ByteArrayOutputStream(20480);
                        }
                        byte[] bufer = new byte[512];
                        while (true) {
                            int len = in.read(bufer);
                            if (len == -1) {
                                break;
                            }
                            temporaryOutput.write(bufer, 0, len);
                        }
                        in.close();
                        temporaryOutput.close();
                        byte[] array = temporaryOutput.toByteArray();
                        //Creating a Byte out of the pdf
                        if (emailTo != null && !emailTo.equalsIgnoreCase("")) {
                            String subject = scheme;
                            if (productId == 1) {
                                subject = "Your Resolution Health Customer Information Update";
                            } else if (productId == 2) {
                                subject = "Your Spectramed Customer Information Update";
                            }
                            String emailMsg = null;
                            EmailContent content = new EmailContent();
                            content.setContentType("text/plain");
                            EmailAttachment attachment = new EmailAttachment();
                            attachment.setContentType("application/octet-stream");
                            attachment.setName("Member_Information.pdf");
                            content.setFirstAttachment(attachment);
                            content.setFirstAttachmentData(array);
                            content.setSubject(subject);
                            content.setEmailAddressFrom(emailFrom);
                            content.setEmailAddressTo(emailTo);
                            content.setProductId(productId);
                            content.setMemberCoverNumber(coverNumber);
                            content.setMemberTitle(coverD.get(0).getTitle());
                            content.setMemberInitials(coverD.get(0).getInitials());
                            content.setMemberName(coverD.get(0).getName());
                            content.setMemberSurname(coverD.get(0).getSurname());
                            //Determine msg for email according to type of document
                            content.setType("updateToContactDetails");
                            /*If document type is custom, this message added here will be displayed in the email. The email method will automatically add the 
                             the enquiries and call center aswell as the kind regards at the end of the message*/
                            content.setContent(emailMsg);
                            emailSent = com.koh.command.FECommand.service.getAgileManagerPort().sendEmailWithOrWithoutAttachment(content);
                            org.apache.log4j.Logger.getLogger(this.getClass()).info("Email sent : " + emailSent);
                            boolean result;
                            if(emailSent){
                                result = port.mailCommunicationAudit(Integer.parseInt(String.valueOf(session.getAttribute("memberEntityID"))), emailTo, "Email sending successful", 15);
                            } else{
                                result = port.mailCommunicationAudit(Integer.parseInt(String.valueOf(session.getAttribute("memberEntityID"))), emailTo, "Email sending Failed", 15);
                            }
                        } else {
                        }
                    } else {
                    }

                } catch (FileNotFoundException ex) {
                        org.apache.log4j.Logger.getLogger(this.getClass()).info("FileNotFoundException: " + ex.getMessage());
                } catch (IOException ex) {
                        org.apache.log4j.Logger.getLogger(this.getClass()).info("IOException: " + ex.getMessage());
                } finally {
                    try {
                        if (in != null) {
                            in.close();
                        }
                    } catch (IOException ex) {
                        org.apache.log4j.Logger.getLogger(this.getClass()).info("IOException: " + ex.getMessage());
                    }
                }
            }
            //--------------------MAILING Functionality-----------------//
            
            return true;
        } catch (Exception e) {
            org.apache.log4j.Logger.getLogger(this.getClass()).info("Exception: " + e.getMessage());
            return false;
        }
    }
    
}

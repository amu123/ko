/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.union.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.cover.commands.MemberMaintenanceTabContentCommand;
import com.koh.employer.command.TabUtils;
import static com.koh.fe.command.LoginCommand.getNeoUser;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.UnionMember;

/**
 *
 * @author shimanem
 */
public class LinkMembershipToUnionCommand extends NeoCommand {

    private static final Logger logger = Logger.getLogger(LinkMembershipToUnionCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();

        String button = request.getParameter("buttonPressed");
        logger.log(Level.INFO, "Getting Union Details : {0}", button);
        try {
            if (button != null && !button.isEmpty()) {
                if (button.equalsIgnoreCase("advancedUnionSearch".trim())) {

                    viewAdvancedSearch(port, request, response, context);

                } else if (button.equalsIgnoreCase("SaveLinkUnionToMember".trim())) {
                    saveLinkMemberToUnion(port, request, response, context);
                }
            }
        } catch (ServletException ex) {
            Logger.getLogger(LinkMembershipToUnionCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LinkMembershipToUnionCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    private void viewAdvancedSearch(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        logger.info("Membership Union Link Search [Searching Union to Link Group...]");
        request.setAttribute("union_Id", "employerGroup_unionID");
        request.setAttribute("union_Number", "employerGroup_unionNumber");
        request.setAttribute("union_Name", "employerGroup_unionName");
        request.setAttribute("target_div", request.getParameter("target_div"));
        request.setAttribute("main_div", request.getParameter("main_div"));

        context.getRequestDispatcher("/Member/MemberUnionAdvanceSearch.jsp").forward(request, response);
    }

    private void saveLinkMemberToUnion(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        logger.info("** Link Union to Member **");
        Map<String, String> errors = UnionMemberMapping.validate(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        NeoUser neoUser = getNeoUser(request);

        int entityId = new Integer(request.getParameter("memberEntityId"));
        logger.log(Level.INFO, "** Link Union to Member : [ENTITY_ID] : {0}", entityId);
        logger.log(Level.INFO, "** Link Union to Member : [STATUS] : {0}", status);

        if ("OK".equalsIgnoreCase(status)) {
            if (saveUnionMember(request, neoUser)) {
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error linking Union to Member information.\"";
            }
        } else {
            additionalData = null;
        }

        if ("OK".equalsIgnoreCase(status)) {
            context.getRequestDispatcher(new MemberMaintenanceTabContentCommand().getMemberCoverDetails(request, entityId)).forward(request, response);

        } else {
            PrintWriter out = response.getWriter();
            out.println(TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData));
        }

    }

    private boolean saveUnionMember(HttpServletRequest request, NeoUser neoUser) {

        logger.info("** Getting Union Member Params **");
        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

        //NeoUser neoUser = getNeoUser(request);
        String strUnionID = String.valueOf(request.getParameter("employerGroup_unionID"));
        String strCoverNumber = String.valueOf(session.getAttribute("memberCoverNumber"));
        //String strCoverNumber = String.valueOf(request.getParameter("memberCoverNumber"));
        String memberRepName = String.valueOf(request.getParameter("member_repName"));
        String memberRepSurname = String.valueOf(request.getParameter("member_repSurname"));
        String memberUnionRegionId = String.valueOf(request.getParameter("member_unionRegion"));
        String memberUnionInceptionDate = String.valueOf(request.getParameter("member_unionInceptionDate"));

        try {

            UnionMember unionMember = new UnionMember();
            unionMember.setUnionID(Integer.parseInt(strUnionID));
            unionMember.setCoverNumber(strCoverNumber);
            unionMember.setDependentID(0);// The Dependent ID is set to zero since this is for Principal Members only, DependentType 17
            unionMember.setRepName(memberRepName);
            unionMember.setRepSurname(memberRepSurname);
            unionMember.setRegionID(Integer.parseInt(memberUnionRegionId));
            unionMember.setStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(format.parse(memberUnionInceptionDate)));

            boolean status = port.linkMemberToUnion(unionMember, neoUser);
            logger.log(Level.INFO, "** Getting Union Member Params [Status] : {0}", status);
            return status;

        } catch (ParseException ex) {
            Logger.getLogger(LinkMembershipToUnionCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public String getName() {
        return "LinkMembershipToUnionCommand";
    }
}

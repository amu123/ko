/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthHospitalLOC;
import neo.manager.AuthLocLosDetails;
import neo.manager.MainList;

/**
 *
 * @author johanl
 */
public class DateTimeUtils {

    private static DatatypeFactory _datatypeFactory;
    public static HashMap<XMLGregorianCalendar, AuthHospitalLOC> locListNew;
    //date formats
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    public static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    //decimal format
    public static final DecimalFormat decimalFormat = new DecimalFormat(".00");

    public static XMLGregorianCalendar convertDateToXMLGregorianCalendar(Date date) {

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        DatatypeFactory fact = getDatatypeFactory();
        if (fact != null) {
            return fact.newXMLGregorianCalendar(calendar);
        } else {
            return null;
        }
    }

    public static String convertXMLGregorianCalendarToDate(XMLGregorianCalendar date) {
        if (date == null) {
            return "";
        } else {
        String DATE_FORMAT = "yyyy/MM/dd";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        return sdf.format(date.toGregorianCalendar().getTime());
            
        }
    }

    public static Date getDateFromXMLGregorianCalendar(XMLGregorianCalendar date) {
        if (date == null) {
            return null;
        } else {
            return date.toGregorianCalendar().getTime();
        }
    }

    public static String convertToYYYYMMDD(Date date) {

        String DATE_FORMAT = "yyyy/MM/dd";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        String aDate = sdf.format(date);

        return aDate;
    }
    
    public static String convertToYYYYMMDDHHMMSS(Date date) {

        String DATE_FORMAT = "yyyy/MM/dd HH:MM:SS";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        String aDate = sdf.format(date);

        return aDate;
    }

    public static Date convertFromYYYYMMDD(String date) {

        Date aDate = new Date();
        String DATE_FORMAT = "yyyy/MM/dd";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        try {
            aDate = sdf.parse(date);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        return aDate;
    }
    
    public static Date convertFromYYYYMMDDHHMMSS(String date) {

        Date aDate = new Date();
        String DATE_FORMAT = "yyyy/MM/dd HH:MM:SS";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        try {
            aDate = sdf.parse(date);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        return aDate;
    }

    public static Date addDays(Date date, int days) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_WEEK, days);
        Date c = cal.getTime();
        return c;
    }

    public static String addDays(String dateString, int days) {
        Date date = convertFromYYYYMMDD(dateString);
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_WEEK, days);
        Date newDate = cal.getTime();
        return convertToYYYYMMDD(newDate);
    }
    
    public static int getMonthsDiff(Date dt1, Date dt2) {
        long dtDif = dt1.getTime() - dt2.getTime();
        long days = TimeUnit.MILLISECONDS.toDays(dtDif);
        return (int)Math.abs(Math.round(days/30.0));
        
    }

    public static int getMonthsDiff(XMLGregorianCalendar dt1, XMLGregorianCalendar dt2) {
        long dtDif = dt1.toGregorianCalendar().getTime().getTime() - dt2.toGregorianCalendar().getTime().getTime();
        long days = TimeUnit.MILLISECONDS.toDays(dtDif);
        return (int)Math.abs(Math.round(days/30.0));
        
    }

    private static DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                ex.printStackTrace();
            }
        }

        return _datatypeFactory;
    }

    public XMLGregorianCalendar webserviceConvert(String date, String format) {
        XMLGregorianCalendar xmlCal = null;
        try {
            Date objDate = new SimpleDateFormat(format).parse(date);
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(objDate);
            xmlCal = getDatatypeFactory().newXMLGregorianCalendar(cal);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return xmlCal;
    }

    public static Date setToLastDayOfMonth(Date date){
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
        return cal.getTime();
    }

    public static List<AuthHospitalLOC> orderLOCListByDate(List<AuthHospitalLOC> locList) {
        Collections.sort(locList, new Comparator<AuthHospitalLOC>() {
            public int compare(AuthHospitalLOC o1, AuthHospitalLOC o2) {
                if (o1.getDateFrom() == null || o2.getDateFrom() == null) {
                    return 0;
                }
                return o1.getDateFrom().toGregorianCalendar().getTime().compareTo(o2.getDateFrom().toGregorianCalendar().getTime());
            }
        });
        return locList;
    }

    public static List<AuthLocLosDetails> orderLocLosByType(List<AuthLocLosDetails> llList) {
        Collections.sort(llList, new Comparator<AuthLocLosDetails>() {
            public int compare(AuthLocLosDetails o1, AuthLocLosDetails o2) {
                if (o1.getLocType() == null || o2.getLocType() == null) {
                    return 0;
                }
                return Integer.parseInt(o1.getLocType()) - Integer.parseInt(o2.getLocType());
            }
        });
        return llList;
    }

    public static void setMapAndReorderLOCByDates(List<AuthHospitalLOC> locList) {
        locListNew = new HashMap<XMLGregorianCalendar, AuthHospitalLOC>();
        Collections.sort(locList, new Comparator<AuthHospitalLOC>() {
            public int compare(AuthHospitalLOC o1, AuthHospitalLOC o2) {
                if (o1.getDateFrom() == null || o2.getDateFrom() == null) {
                    return 0;
                }
                return o1.getDateFrom().toGregorianCalendar().getTime().compareTo(o2.getDateFrom().toGregorianCalendar().getTime());
            }
        });

        for (AuthHospitalLOC loc : locList) {
            locListNew.put(loc.getDateFrom(), loc);
        }
    }

    //LOC LOS calculation
    public static List<AuthHospitalLOC> orderAndRecalculateLOCDetails(List<AuthHospitalLOC> locList, AuthHospitalLOC newLoc) {

        //order by type id
        /*
         * HashMap<Integer, AuthHospitalLOC> locListNew = new HashMap<Integer,
         * AuthHospitalLOC>(); Collections.sort(locList, new
         * Comparator<AuthHospitalLOC>() {
         *
         * public int compare(AuthHospitalLOC o1, AuthHospitalLOC o2) { if
         * (o1.getLevelOfCare() == null || o2.getLevelOfCare() == null) { return
         * 0; } return Integer.parseInt(o1.getLevelOfCare()) -
         * Integer.parseInt(o2.getLevelOfCare()); } });
         *
         * for (AuthHospitalLOC loc : locList) {
         * locListNew.put(Integer.parseInt(loc.getLevelOfCare()), loc);
         * System.out.println("loc ids after sort = " + loc.getLevelOfCareDesc()
         * + " - " + loc.getLevelOfCare()); }
         */

        //order by date
        setMapAndReorderLOCByDates(locList);

        if (locList != null && !locList.isEmpty()) {
            //int listSize = locList.size();
            XMLGregorianCalendar newType = newLoc.getDateFrom();
            boolean reCalcPreviousDates = false;
            boolean reCalcNextDates = false;

            if (locListNew.containsKey(newType)) {
                System.out.println("error already exist");

            } else {

                for (int x = 0; x < locList.size(); x++) {

                    reCalcPreviousDates = false;
                    reCalcNextDates = false;

                    AuthHospitalLOC loc = locList.get(x);
                    XMLGregorianCalendar currentLocType = loc.getDateFrom();

                    if (currentLocType.toGregorianCalendar().after(newType.toGregorianCalendar())) {
                        reCalcNextDates = true;
                        break;

                    } else if (currentLocType.toGregorianCalendar().before(newType.toGregorianCalendar())) {
                        reCalcPreviousDates = true;
                    }
                }
                //add
                locList.add(newLoc);

                Collections.sort(locList, new Comparator<AuthHospitalLOC>() {
                    public int compare(AuthHospitalLOC o1, AuthHospitalLOC o2) {
                        if (o1.getDateFrom() == null || o2.getDateFrom() == null) {
                            return 0;
                        }
                        return o1.getDateFrom().toGregorianCalendar().getTime().compareTo(o2.getDateFrom().toGregorianCalendar().getTime());
                    }
                });

                for (AuthHospitalLOC loc : locList) {
                    locListNew.put(loc.getDateFrom(), loc);
                    System.out.println("2 loc ids after sort = " + loc.getLevelOfCareDesc() + " - "
                            + loc.getLevelOfCare());
                }

                //if (reCalcPreviousDates) {
                AuthHospitalLOC addedLoc = locListNew.get(newType);
                int newLocSize = locList.size();
                int locIndex = locList.indexOf(addedLoc);
                System.out.println("loc index of new test add = " + locIndex);
                System.out.println("new loc size = " + newLocSize);

                boolean indexIsFirst = false;
                boolean skipPrevious = false;

                for (int x = locIndex; x < newLocSize; x++) {

                    AuthHospitalLOC loc = locList.get(x);

                    if (reCalcNextDates) {
                        //first check previous
                        if (locIndex == 0) {
                            //just recalc all following dates
                            //indexIsFirst = true;
                            reCalcNextDates = true;
                            skipPrevious = true;

                            System.out.println("locIndex a = " + locIndex);


                        } else {
                            System.out.println("locIndex b = " + locIndex);

                            //indexIsFirst = false;
                            AuthHospitalLOC previousLoc = locList.get(x - 1);

                            //if (!indexIsFirst) {
                            XMLGregorianCalendar previousDate = previousLoc.getDateTo();
                            double newLos = 0.0d;
                            if (!skipPrevious) {
                                //check previous end date and set as new start
                                //override previous end with new start
                                XMLGregorianCalendar newStartDate = loc.getDateFrom();
                                previousLoc.setDateTo(newStartDate);

                                XMLGregorianCalendar previousStart = previousLoc.getDateFrom();
                                XMLGregorianCalendar previousEnd = previousLoc.getDateTo();

                                newLos = loc.getLengthOfStay();

                                newLos = DateTimeUtils.calculateLOSFromDates(previousStart.toGregorianCalendar().getTime(), previousEnd.toGregorianCalendar().getTime());
                                previousLoc.setLengthOfStay(newLos - 0.5d);

                                //call los by date, set and return changed values of previous

                                skipPrevious = true;

                            } else {
                                //only update current start with previous end 
                                //and recalc los
                                loc.setDateFrom(previousDate);
                                newLos = loc.getLengthOfStay();

                                previousLoc.setLengthOfStay(previousLoc.getLengthOfStay() - 0.5d);

                                Date currentNewEndDate = DateTimeUtils.calculateLOCEndDateFromLOS(loc.getDateFrom().toGregorianCalendar().getTime(), newLos);
                                XMLGregorianCalendar newEndDate = DateTimeUtils.convertDateToXMLGregorianCalendar(currentNewEndDate);
                                loc.setDateTo(newEndDate);

                                //XMLGregorianCalendar newStart = loc.getDateFrom();
                                //XMLGregorianCalendar currentEnd = loc.getDateTo();
                                //newLos = DateTimeUtils.calculateLOSFromDates(newStart.toGregorianCalendar().getTime(), currentEnd.toGregorianCalendar().getTime());
                                //loc.setLengthOfStay(newLos);

                            }
                            reCalcNextDates = true;

                            //}

                        }



                    } else if (reCalcPreviousDates) {
                        //will always be the last entry
                        AuthHospitalLOC previousLoc = locList.get(x - 1);
                        XMLGregorianCalendar newStartDate = loc.getDateFrom();
                        previousLoc.setDateTo(newStartDate);

                        XMLGregorianCalendar previousStart = previousLoc.getDateFrom();
                        XMLGregorianCalendar previousEnd = previousLoc.getDateTo();

                        double newLos = DateTimeUtils.calculateLOSFromDates(previousStart.toGregorianCalendar().getTime(), previousEnd.toGregorianCalendar().getTime());
                        previousLoc.setLengthOfStay(newLos - 0.5d);
                        //change previous date to new start and set to recalc next with skip prevoius
                        break;
                    } else {
                        System.out.println("LOC ERROR");
                    }
                    locIndex++;

                }

                //}

            }
        }

        return locList;
    }

    public static double calculateLOSFromDates(Date dateFrom, Date dateTo) {

        double los = 0.0d;
        //calandar calculation
        Calendar cLocFrom = Calendar.getInstance();
        cLocFrom.setTime(dateFrom);
        Calendar cLocTo = Calendar.getInstance();
        cLocTo.setTime(dateTo);
        //calendar from values
        int fromDayOfYear = cLocFrom.get(Calendar.DAY_OF_YEAR);
        int fromHour = cLocFrom.get(Calendar.HOUR_OF_DAY);
        int fromMin = cLocFrom.get(Calendar.MINUTE);
        //calendar to values
        int toDayOfYear = cLocTo.get(Calendar.DAY_OF_YEAR);
        int toHour = cLocTo.get(Calendar.HOUR_OF_DAY);
        int toMin = cLocTo.get(Calendar.MINUTE);
        //0.5 checks
        if (fromHour < 12) {
            los = los + 0.5;
        }
        if (toHour > 12 || (toHour == 12 && toMin >= 0)) {
            los = los + 0.5;
        }
        //subtracting days
        if (fromDayOfYear != toDayOfYear) {
            double dayDiff = toDayOfYear - fromDayOfYear;
            los = los + dayDiff;

        } else {
            if ((fromHour >= 12 && fromMin > 0) && toHour < 12) {
                los = los + 0.5;
            }
        }

        if (fromDayOfYear > toDayOfYear) {
            System.out.println("toDate before fromDate");
            return 0.0d;
        }
        System.out.println("calculated los = " + los);

        return los;
    }

    public static Date calculateLOCEndDateFromLOS(Date fromDate, double los) {
        //calandar calculation
        Calendar cLocFrom = Calendar.getInstance();
        cLocFrom.setTime(fromDate);
        Calendar cLocTo = Calendar.getInstance();

        //calendar from values
        int fromYear = cLocFrom.get(Calendar.YEAR);
        int fromMonth = cLocFrom.get(Calendar.MONTH);
        int fromDay = cLocFrom.get(Calendar.DATE);
        int fromHour = cLocFrom.get(Calendar.HOUR_OF_DAY);
        int fromMin = cLocFrom.get(Calendar.MINUTE);

        int toYear = 0;
        int toMonth = 0;
        int toDay = 0;
        int toHour = 0;
        int toSec = 0;
        int toMin = 0;

        los = los - 0.5;

        if (los == 0.0 || los == 0) {
            toYear = fromYear;
            toMonth = fromMonth;
            toDay = fromDay;

            if (fromHour < 12) {
                toHour = 11;
                toMin = 59;
            } else if (fromHour > 12 || (fromHour == 12 && fromMin > 1)) {
                toHour = 23;
                toMin = 59;
            }
            //cLocFrom.add(Calendar.HOUR_OF_DAY, 11);
        } else {
            double halfDays = los / 0.5;
            for (double x = 0.0d; x < halfDays; x++) {
                cLocFrom.add(Calendar.HOUR_OF_DAY, 12);
            }
            //set to date values
            toYear = cLocFrom.get(Calendar.YEAR);
            toMonth = cLocFrom.get(Calendar.MONTH);
            toDay = cLocFrom.get(Calendar.DATE);
            toHour = cLocFrom.get(Calendar.HOUR_OF_DAY);
            toMin = cLocFrom.get(Calendar.MINUTE);
        }

        //generate return
        cLocTo.set(toYear, toMonth, toDay, toHour, toMin, toSec);
        Date newDate = cLocTo.getTime();
        return newDate;
    }
    
        public static ArrayList<MainList> orderMainListByCreatedDate(ArrayList<MainList> mainList) {
        Collections.sort(mainList, new Comparator<MainList>(){
            public int compare(MainList o1, MainList o2) {
                if(o1.getCreatedDate() == null || o2.getCreatedDate() == null){
                    return 0;                   
                }
                return o2.getCreatedDate().toGregorianCalendar().getTime().compareTo(o1.getCreatedDate().toGregorianCalendar().getTime());
            }            
        });
            return mainList;
        }

}

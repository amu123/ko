/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.ClaimLine;
import neo.manager.ClaimLineOverride;
import neo.manager.ClaimLineOverrideResults;
import neo.manager.ClaimReversals;
import neo.manager.NeoManagerBean;
import neo.manager.NeoSecurityException_Exception;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author janf
 */
public class OverrideClaim extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Date today = new Date(System.currentTimeMillis());
        XMLGregorianCalendar xToday = DateTimeUtils.convertDateToXMLGregorianCalendar(today);
        PrintWriter out = null;

        int claimLineId = Integer.parseInt("" + session.getAttribute("memberDetailedClaimLineId"));
        ClaimLine claimLine = (ClaimLine) session.getAttribute("memCLCLDetails");
        String staleClaim = request.getParameter("staleClaim");
        System.out.println("[fr]staleClaim = " + staleClaim);
//        int claimId = claimLine.getClaimId();

//        String overideCode = request.getParameter("reversalReason");
//        String overrideCodeText = port.getValueFromCodeTableForId("Claim Reversal Codes", overideCode);
//
//        System.out.println("overideCode = " + overideCode);
//        System.out.println("overrideCodeText = " + overrideCodeText);
//        System.out.println("claimLineId = " + claimLineId);

        Security _secure = new Security();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        _secure.setCreatedBy(user.getUserId());
        _secure.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        _secure.setLastUpdatedBy(user.getUserId());
        _secure.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        _secure.setSecurityGroupId(user.getSecurityGroupId());

        ClaimLineOverride clo = new ClaimLineOverride();
        clo.setClaimLineId(claimLineId);
        clo.setDateOverriden(xToday);
        clo.setDescription("Manual Accept");
        //clo.setOverrideId(claimId);
        clo.setUserId(user.getUserId());
        

        try {
            String returnMsg = "";
            out = response.getWriter();
            
            if(staleClaim.equalsIgnoreCase("false")){
                ClaimLineOverrideResults clor  = port.overrideRejectedClaimLine(user, clo);
            
                if (!clor.isSucccess()) {
                    returnMsg = "Error|" + clor.getMessage();
                    //session.setAttribute("revError", "Failed to reverse claimId - " + claimId);
                } else {
                    //port.processClaim(claimId, _secure);
                    returnMsg = "Done|Claim line " + claimLineId + " Accepted";
                    //session.removeAttribute("revError");
                }
            } else {
                boolean success = port.overrideStaleClaim(claimLine.getClaimId(), clo, user);
                if(success){
                    returnMsg = "Done|Claim line " + claimLineId + " Accepted";
                } else {
                    returnMsg = "Error|Failed to override claim line " + claimLineId;
                }
            }
            out.println(returnMsg);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "OverrideClaim";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.lang.Exception;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;

/**
 *
 * @author almaries
 */
public class LoadTaxCertClaimsTranCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        System.out.println("LoadTaxCertClaimsTranCommand : " + request.getParameter("buttonPressedExport"));

        NeoManagerBean port = service.getNeoManagerBeanPort();
        HttpSession session = request.getSession();
        String coverNum = (String) session.getAttribute("policyHolderNumber");
        System.out.println("Cover Number: "+coverNum);
        
        String year = (String) session.getAttribute("yearRange");
                
        try {
            if (request.getParameter("buttonPressedExport") != null && request.getParameter("buttonPressedExport").toString().equals("Export") && !request.getParameter("buttonPressedExport").toString().isEmpty()) {
                CoverProductDetails coverDetails = port.getProductDetailsDespiteCoverStatus(coverNum);
                FileData taxCertClaims = null;
                                
                if(year != null) {
                    taxCertClaims = port.getMemberClaimsStatement(coverDetails, "XLS", new Integer(year));
                }
                               
                if (taxCertClaims == null || taxCertClaims.getData() == null) {
                } else {
                    byte[] data = taxCertClaims.getData();
                    ServletOutputStream os = response.getOutputStream();
                    String mimetype = context.getMimeType(taxCertClaims.getFileName());
                    response.setContentType((mimetype != null) ? mimetype : "application/octet-stream");
                    response.setContentLength(data.length);
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + taxCertClaims.getFileName() + "\"");
                    os.write(data);
                    os.flush();
                    os.close();

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    @Override
    public String getName() {
        return "LoadTaxCertClaimsTranCommand";
    }
    
}

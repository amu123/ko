/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.functions;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.Currencies;
import neo.manager.NeoManagerBean;

/**
 *
 * @author yuganp
 */
public class AgileELFunctions {
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy/MM/dd");
    private static final SimpleDateFormat SDTF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private static final java.text.DecimalFormat desFormat = new java.text.DecimalFormat("#.##");
    
    public static String formatXMLGregorianDate(XMLGregorianCalendar date) {
        if (date == null) {
            return "";
        } else {
            return SDF.format(date.toGregorianCalendar().getTime());
        }
        
    }
    
    public static String formatXMLGregorianDateTime(XMLGregorianCalendar date) {
        if (date == null) {
            return "";
        } else {
            return SDTF.format(date.toGregorianCalendar().getTime());
        }
        
    }
    
    public static String formatBigDecimal(BigDecimal bd) {
       if (bd == null) {
           return "";
       } else {
           return bd.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
       }
    }
    
    public static String formatDate(Date date) {
        if (date == null) {
            return "";
        } else {
            return SDF.format(date);
        }
    }
    
    public static Date dateAdd(Date date, String type, Integer value) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if ("day".equalsIgnoreCase(type)) {
            cal.add(Calendar.DATE, value);
        } else if ("month".equalsIgnoreCase(type)) {
            cal.add(Calendar.MONTH, value);
        } else if ("year".equalsIgnoreCase(type)) {
            cal.add(Calendar.YEAR, value);
        }
        return cal.getTime();
    }
    
    public static Date dateSet(Date date, Integer year, Integer month, Integer day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if (year != null) cal.set(Calendar.YEAR, year);
        if (month != null) cal.set(Calendar.MONTH, month);
        if (day != null) cal.set(Calendar.DATE, day);
        return cal.getTime();
    }
    
    public static String formatStringAmount(String s) {
        if (s == null) {
            return "0.00";
        } else {
            try {
                BigDecimal bd = new BigDecimal(s);
                return bd.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            } catch (Exception e) {
                return "0.00";
            }
        }
    }
    
    public static String escapeStr(String s) {
        if (s == null) {
            return "";
        } else {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < s.length(); i++) {
                char ch = s.charAt(i);
                if (ch == '\'') {
                    sb.append("\\\'");
                } else if (ch == '"') {
                    sb.append("\\");
                    sb.append('"');
                } else {
                    sb.append(ch);
                }
            }
            return sb.toString();
        }
    }
    
    public static String formatDouble(Double d) {
        try {
            return desFormat.format(d);
        } catch (Exception e) {
            System.out.println("exception" + e);
            return "0.00";
        }
    }

    public static List retrieveLookupValues(String lookupId/*, NeoUser*/) {
        System.out.println("Lookup: " + lookupId);
        //System.out.println("USER NAME FOR LOOKUP VALUE SEARCH ======= " + user.getUsername());
        if (isNumeric(lookupId)) {
            //System.out.println("[fr]True");
            List<neo.manager.LookupValue> lookUps = NeoCommand.service.getNeoManagerBeanPort().getCodeTable(new Integer(lookupId));

            System.out.println("Lookup size: " + lookUps.size());
            return lookUps;

        } else {
            //System.out.println("[fr]False");
            //List<neo.manager.LookupValue> lookUps = NeoCommand.service.getNeoManagerBeanPort().getCodeTableByName(user, lookupId);
            List<neo.manager.Currencies> lookUps = NeoCommand.service.getNeoManagerBeanPort().getCurrencyTable();

            System.out.println("Lookup size: " + lookUps.size());

            return lookUps;
        }
    }

    private static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.employer.command;

import com.koh.command.NeoCommand;
import com.koh.contribution.command.PaymentAllocationCommand;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ContributionAge;
import neo.manager.ContributionReceipts;
import neo.manager.CoverDetails;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author yuganp
 */
public class EmployerTransactionCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            String s = request.getParameter("command");
            if (s != null && !s.isEmpty()) {
                System.out.println("command : " + s);
                if (s.equalsIgnoreCase("AgeAnalysis")) {
                    getAgeAnalysis(request, response, context);
                } else if (s.equalsIgnoreCase("Unallocated")) {
                    getUnallocated(request, response, context);
                } else if (s.equalsIgnoreCase("Bank")) {
                    getBank(request, response, context);
                } else if (s.equalsIgnoreCase("DebitOrders")) {
                    getDebitOrders(request, response, context);
                } else if (s.equalsIgnoreCase("Refunds")) {
                    getRefunds(request, response, context);
                } else {
                    System.out.println("Error Command");
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(PaymentAllocationCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void getAgeAnalysis(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        int entityId = TabUtils.getIntParam(request, "id");
        List<ContributionAge> ageAnalysis = port.getAgeAnalysis(null, entityId);
        request.setAttribute("ageAnalysis", ageAnalysis);
        context.getRequestDispatcher("/Employer/EmployerTransactionsAge.jsp").forward(request, response);
    }

    private void getUnallocated(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        int entityId = TabUtils.getIntParam(request, "id");
        List<ContributionReceipts> unallocatedReceipts = port.fetchUnallocatedReceiptsByEntityId(entityId, null);
        request.setAttribute("Receipts", unallocatedReceipts);
        context.getRequestDispatcher("/Employer/EmployerTransactionsUnallocated.jsp").forward(request, response);

    }

    private void getBank(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        int entityId = TabUtils.getIntParam(request, "id");
        List<Map<String, String>> map = MapUtils.getMap(port.fetchBankStatementSummaryByEntityId(entityId));
        request.setAttribute("bankStatement", map);
        context.getRequestDispatcher("/Employer/EmployerTransactionsBank.jsp").forward(request, response);
    }

    private void getDebitOrders(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        int entityId = TabUtils.getIntParam(request, "id");
        Date endDate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.MONTH, -12);
        Date startDate = cal.getTime();
        HttpSession session = request.getSession();
        int productId = (Integer) session.getAttribute("productID");

        List<KeyValueArray> debitOrderHistory = port.fetchDebitOrderHistory(null, entityId, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), DateTimeUtils.convertDateToXMLGregorianCalendar(endDate), productId);
        System.out.println("DOH : " + debitOrderHistory.size());
        request.setAttribute("debitOrder", MapUtils.getMap(debitOrderHistory));
        context.getRequestDispatcher("/Employer/EmployerTransactionsDebitOrder.jsp").forward(request, response);

    }

    private void getRefunds(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        int entityId = TabUtils.getIntParam(request, "id");
        List<KeyValueArray> refunds = port.fetchEntityRefundTransactionsByEntityId(entityId);
        System.out.println("Refunds : " + refunds.size());
        request.setAttribute("receiptRefunds", MapUtils.getMap(refunds));
        context.getRequestDispatcher("/Employer/EmployerTransactionsRefunds.jsp").forward(request, response);
    }

    @Override
    public String getName() {
        return "EmployerTransactionCommand";
    }

}

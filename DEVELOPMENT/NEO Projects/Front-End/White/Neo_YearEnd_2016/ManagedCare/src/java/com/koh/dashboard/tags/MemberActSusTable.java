/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.dashboard.tags;

//import com.google.gson.Gson;
import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.MemberCountChart;
import neo.manager.MemberTotalCount;

/**
 *
 * @author pavaniv
 */
public class MemberActSusTable  extends TagSupport{

 List<Object> memberChartCount = new ArrayList<Object>();
    String memberChartValue;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        ServletRequest request = pageContext.getRequest();
        ServletContext context = pageContext.getServletContext();
        ServletResponse response = pageContext.getResponse();
        Collection<MemberTotalCount> mainList = new ArrayList<MemberTotalCount>();
        List<MemberCountChart> memChartList=new ArrayList<MemberCountChart>();
        
        mainList = NeoCommand.service.getNeoManagerBeanPort().getMemberCount();
        memChartList = NeoCommand.service.getNeoManagerBeanPort().getMemberChartCount();
        System.out.println("entered memeber count" + mainList.size());

        System.out.println("entered memChartList" + memChartList.size());


        request.setAttribute("mainList", mainList);



        
         try {
            out.print("<thead><tr>");
            out.print("<th scope=\"col\">Option</th>");
            out.print("<th scope=\"col\">Total Member Count</th>");
             out.print("<th scope=\"col\">Total Active</th>");
             out.print("<th scope=\"col\">Total Suspended</th>");
            out.print("</tr></thead>");
            int count = 0;


            for (MemberTotalCount w2 : mainList) {
                out.print("<tr>");

                out.print("<td><label class=\"label\">" + w2.getOptions() + "</td>");
                out.print("<td><label class=\"label\">" + w2.getTotalMemCount() + "</td>");
                out.print("<td><label class=\"label\">" + w2.getTotalActiveCount()+ "</td>");
                out.print("<td><label class=\"label\">" + w2.getTotalSuspendCount()+ "</td>");
                out.print("</tr>");
              
                count++;

            }
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }

        return super.doEndTag();
    }

   
   
}

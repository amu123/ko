/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;
import neo.manager.Option;
import neo.manager.Product;

/**
 *
 * @author Johan-NB
 */
public class GetPreAuthCoverByNumberAndDate extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        PrintWriter out = null;
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        String memberNo = "" + session.getAttribute("memberNum_text");
        String authYear = "" + session.getAttribute("authPeriod");
        String authMonth = "" + session.getAttribute("authMonthPeriod");
        String authType = "" + session.getAttribute("authType");
        System.out.println("memberNo = " + memberNo);
        System.out.println("authYear = " + authYear);
        System.out.println("authMonth = " + authMonth);
        System.out.println("authType = " + authType);
        boolean errors = false;
        String error = "";
        String optionName = "";
        String schemeName = "";

        //test period first
        Date now = new Date(System.currentTimeMillis());
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.MONTH, 4);
        Date fDate = cal.getTime();
 
        int aYear = Integer.parseInt(authYear);
        int aMonth = Integer.parseInt(authMonth);
        int fYear = Integer.parseInt(sdf.format(fDate).substring(0, 4));
        int fMonth = Integer.parseInt(sdf.format(fDate).substring(5, 7));

        if (aYear == fYear && aMonth > fMonth) {
            errors = true;
            session.setAttribute("authMonthPeriod_error", "Invalid Future Date Month Period");
        } else {
            session.setAttribute("authMonthPeriod_error", null);
        }

        if (!errors) {
            if (memberNo != null && !memberNo.trim().equalsIgnoreCase("")) {
                String authDateStr = authYear + "/" + authMonth + "/01";

                Date authDate = null;
                try {
                    authDate = sdf.parse(authDateStr);
                } catch (ParseException ex) {
                    Logger.getLogger(GetPreAuthCoverByNumberAndDate.class.getName()).log(Level.SEVERE, null, ex);
                }

                XMLGregorianCalendar xDate = DateTimeUtils.convertDateToXMLGregorianCalendar(authDate);
                List<CoverDetails> cdList = service.getNeoManagerBeanPort().getCoverDetailsByCoverNumberByDate(memberNo, xDate);

                if (cdList != null && cdList.isEmpty() == false) {
                    session.setAttribute("covMemListDetails", cdList);
                    //set product
                    int optId = -1;
                    int schemeID = -1;
                    for (CoverDetails cd : cdList) {
                        if (cd.getDependentTypeId() == 17) {
                            optId = cd.getOptionId();
                            schemeID = cd.getProductId();
                            break;
                        }
                    }

                    Product prod = port.findProductWithID(schemeID);
                    schemeName = prod.getProductName();
                    Option opt = port.findOptionWithID(optId);
                    optionName = opt.getOptionName();
                    //set product
                    session.setAttribute("scheme", schemeID);
                    session.setAttribute("schemeName", schemeName);
                    session.setAttribute("schemeOption", optId);
                    session.setAttribute("schemeOptionName", optionName);
                    session.setAttribute("schemeOptionDetails", opt);

                    boolean removeBenefits = false;
                    if (authType.equals("7")) {
                        removeBenefits = true;
                    }
                    if (authType.equals("12")) {
                        removeBenefits = true;
                        if (optionName.equalsIgnoreCase("Foundation")) {
                            errors = true;
                            session.setAttribute("memberNum_error", "Member Option not applicable on this Auth");
                        }
                    }
                    if (removeBenefits) {
                        session.removeAttribute("benAllocated");
                    }


                } else {
                    errors = true;
                    session.setAttribute("memberNum_error", "No Details Found");
                }
            } else {
                errors = true;
                session.setAttribute("memberNum_error", "Incorrect Member Number");
            }
            if (!errors) {
                session.setAttribute("memberNum_error", null);
            }
        }
        //CLEAR SESSION 
        if(errors){
            session.setAttribute("memberNum_text", "");
            if(session.getAttribute("covDepListDetails") != null){
                session.removeAttribute("covDepListDetails");
                session.removeAttribute("depListValues");
            }
        }

        String nextJSP = request.getParameter("onScreen");

        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "GetPreAuthCoverByNumberAndDate";
    }
}

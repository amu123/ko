/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.validation;

import com.koh.command.Command;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.LabTariffDetails;

/**
 *
 * @author Johan-NB
 */
public class ValidateDentalDetails extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);

        PrintWriter out = null;
        HttpSession session = request.getSession();

        String errorResponse = "";
        String error = "";
        int errorCount = 0;
        boolean emptyNoLab = false;
        boolean emptyLab = false;

        String dst = "" + session.getAttribute("dst");
        String afd = "" + session.getAttribute("afd");
        String atd = "" + session.getAttribute("atd");
        String as = "" + session.getAttribute("as");
        List<LabTariffDetails> labList = (List<LabTariffDetails>) session.getAttribute("SavedDentalLabCodeList");
        List<LabTariffDetails> noLabList = (List<LabTariffDetails>) session.getAttribute("SavedDentalNoLabCodeList");

        if (dst == null || dst.trim().equals("99")) {
            errorCount++;
            error = error + "|DentalSubType:Dental Sub Type is Mandatory";
        }

        //Date Validation
        PreAuthDateValidation pad = new PreAuthDateValidation();

        String authType = "" + session.getAttribute("authType");
        String authPeriod = "" + session.getAttribute("authPeriod");
        String authMonthPeriod = "" + session.getAttribute("authMonthPeriod");

        if (afd == null || afd.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|authFromDate:Auth From Date is Mandatory";
        } else {
            String vs = pad.DateValidation(afd, atd, "authFromDate", "authToDate", authType, authPeriod, authMonthPeriod, "no");
            String[] returned = vs.split("\\|");
            boolean validDate = Boolean.parseBoolean(returned[0]);
            if (!validDate) {
                String errorsReturned = vs.substring(vs.indexOf("|"), vs.length());
                System.out.println("from date validation error(s) returned = " + errorsReturned);
                errorCount++;
                error = error + errorsReturned;
            }
        }

        if (atd == null || atd.trim().equalsIgnoreCase("")) {
            errorCount++;
            error = error + "|authToDate:Auth From Date is Mandatory";
        } else {
            String vs = pad.DateValidation(afd, atd, "authFromDate", "authToDate", authType, authPeriod, authMonthPeriod, "no");
            String[] returned = vs.split("\\|");
            boolean validDate = Boolean.parseBoolean(returned[0]);
            if (!validDate) {
                String errorsReturned = vs.substring(vs.indexOf("|"), vs.length());
                System.out.println("to date validation error(s) returned = " + errorsReturned);
                errorCount++;
                error = error + errorsReturned;
            }
        }

        if (as == null || as.trim().equals("99")) {
            errorCount++;
            error = error + "|authStatus:Auth Status is Mandatory";
        }

        if (labList == null || labList.size() == 0) {
            emptyNoLab = true;
        }
        if (noLabList == null || noLabList.size() == 0) {
            emptyLab = true;
        }

        if (emptyLab == true && emptyNoLab == true) {
            errorCount++;
            error = error + "|labButton:Lab Tariff Detail id Mandatory";
        }

        if (errorCount > 0) {
            errorResponse = "Error" + error;
        } else if (errorCount == 0) {
            errorResponse = "Done|";
        }

        try {
            out = response.getWriter();
            out.println(errorResponse);

        } catch (Exception ex) {
            System.out.println("ValidateHospitalDetails error : " + ex.getMessage());
        } finally {
            out.close();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ValidateDentalDetails";
    }

    private boolean ValidateDate(String date) {
        boolean valid = true;
        String validChars = "0123456789/";
        char strChar;
        char strChar2;

        if (date.length() < 10 || date.length() > 10) {
            valid = false;
        }
        //  test strString consists of valid characters listed above
        for (int i = 0; i < date.length(); i++) {
            strChar = date.charAt(i);
            if (validChars.indexOf(strChar) == -1) {
                valid = false;
            }
        }
        // test if slash is at correct position (yyyy/mm/dd)
        strChar = date.charAt(4);
        strChar2 = date.charAt(7);
        if (strChar != '/' || strChar2 != '/') {
            valid = false;
        }
        return valid;
    }
}

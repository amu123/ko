/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ForwardToPreAuthResultsConfirmation extends NeoCommand {

    private Logger log = Logger.getLogger(ForwardToPreAuthResultsConfirmation.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        log.info("Inside ForwardToPreAuthResultsConfirmation command");

        try {


        }catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "ForwardToPreAuthResultsConfirmation";
    }
}

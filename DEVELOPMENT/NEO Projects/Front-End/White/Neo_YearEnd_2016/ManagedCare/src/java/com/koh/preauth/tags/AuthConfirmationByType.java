/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.tags;

import com.agile.tags.LabelTextDisplay;
import com.koh.preauth.dto.AuthConfirmationDetails;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.PreAuthDetails;

/**
 *
 * @author johanl
 */
public class AuthConfirmationByType extends TagSupport {

    private static final long serialVersionUID = 1L;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();

        AuthConfirmationDetails aCon = (AuthConfirmationDetails) session.getAttribute("PreAuthConfirmation");
        List<PreAuthDetails> paList = aCon.getAuthDetails();
        String authDisclaimer = "" + session.getAttribute("authDisclaimer");

        ServletContext sctx = pageContext.getServletContext();
        String client = String.valueOf(sctx.getAttribute("Client"));

        System.out.println("### conf client: " + client);
        PreAuthDetails pa;
        String authType;
        boolean skipTariffCopayDisclaimer = false;

        try {
            if (paList != null && !paList.isEmpty()) {
                pa = paList.get(0);
                authType = pa.getAuthType();

                //generic fields
                out.println("<tr><td><table id=\"authFieldDetails\">");
                out.println("<tr>");

                if (session.getAttribute("authNumber") != null && session.getAttribute("authNumber") != "") {
                    session.setAttribute("authNo", session.getAttribute("authNumber"));
                }

                //auth no
                LabelTextDisplay authNo = new LabelTextDisplay();
                authNo.setDisplayName("Auth Number");
                authNo.setElementName("authNo");
                authNo.setBoldDisplay("yes");
                authNo.setValueFromSession("yes");
                authNo.setPageContext(pageContext);
                authNo.doEndTag();
                //auth date
                LabelTextDisplay authDate = new LabelTextDisplay();
                authDate.setDisplayName("Auth Date");
                authDate.setElementName("authDate");
                authDate.setBoldDisplay("yes");
                authDate.setValueFromSession("yes");
                authDate.setPageContext(pageContext);
                authDate.doEndTag();
                out.println("</tr>");

                out.println("<tr>");
                //auth date from
                LabelTextDisplay authFrom = new LabelTextDisplay();
                authFrom.setDisplayName("Auth Valid From");
                authFrom.setElementName("authFrom");
                authFrom.setBoldDisplay("yes");
                authFrom.setValueFromSession("yes");
                authFrom.setPageContext(pageContext);
                authFrom.doEndTag();
                //auth date to
                LabelTextDisplay authTo = new LabelTextDisplay();
                authTo.setDisplayName("Auth Valid To");
                authTo.setElementName("authTo");
                authTo.setBoldDisplay("yes");
                authTo.setValueFromSession("yes");
                authTo.setPageContext(pageContext);
                authTo.doEndTag();
                out.println("</tr>");

                out.println("<tr>");
                //auth status
                LabelTextDisplay authStatus = new LabelTextDisplay();
                authStatus.setDisplayName("Auth Status");
                authStatus.setElementName("authStatus");
                authStatus.setBoldDisplay("yes");
                authStatus.setValueFromSession("yes");
                authStatus.setPageContext(pageContext);
                authStatus.doEndTag();
                out.println("</tr>");

                out.println("<tr><td colspan=\"5\"><hr size=\"3px\" bgcolor=\"#0000CC\"/></td></tr>");
                out.println("<tr><td colspan=\"5\"></td></tr>");
                out.println("<tr>");
                //admission icd
                LabelTextDisplay aICD = new LabelTextDisplay();
                aICD.setDisplayName("Admission ICD");
                aICD.setElementName("aICD");
                aICD.setBoldDisplay("yes");
                aICD.setValueFromSession("yes");
                aICD.setPageContext(pageContext);
                aICD.doEndTag();

                //admission icd
                LabelTextDisplay aICDDesc = new LabelTextDisplay();
                aICDDesc.setDisplayName("Description");
                aICDDesc.setElementName("aICDDesc");
                aICDDesc.setBoldDisplay("yes");
                aICDDesc.setValueFromSession("yes");
                aICDDesc.setPageContext(pageContext);
                aICDDesc.doEndTag();

                out.println("</tr>");
                out.println("<tr>");

                //primary icd
                LabelTextDisplay pICD = new LabelTextDisplay();
                pICD.setDisplayName("Primary ICD");
                pICD.setElementName("pICD");
                pICD.setBoldDisplay("yes");
                pICD.setValueFromSession("yes");
                pICD.setPageContext(pageContext);
                pICD.doEndTag();

                LabelTextDisplay pICDDesc = new LabelTextDisplay();
                pICDDesc.setDisplayName("Description");
                pICDDesc.setElementName("pICDDesc");
                pICDDesc.setBoldDisplay("yes");
                pICDDesc.setValueFromSession("yes");
                pICDDesc.setPageContext(pageContext);
                pICDDesc.doEndTag();
                out.println("</tr>");

                //set type specific fields
                if (authType != null) {
                    if (authType.trim().equals("1") || authType.trim().equals("4") || authType.trim().equals("11")) {

                        out.println("<tr><td colspan=\"5\"><hr size=\"3px\" bgcolor=\"#0000CC\"/></td></tr>");
                        out.println("<tr><td colspan=\"5\"></td></tr>");

                        //Secondary ICD
                        out.println("<tr>");

                        LabelTextDisplay secICD = new LabelTextDisplay();
                        secICD.setDisplayName("Secondary ICD");
                        secICD.setElementName("secICD");
                        secICD.setBoldDisplay("yes");
                        secICD.setValueFromSession("yes");
                        secICD.setPageContext(pageContext);
                        secICD.doEndTag();

                        LabelTextDisplay secICDDesc = new LabelTextDisplay();
                        secICDDesc.setDisplayName("Description");
                        secICDDesc.setElementName("secICDDesc");
                        secICDDesc.setBoldDisplay("yes");
                        secICDDesc.setValueFromSession("yes");
                        secICDDesc.setPageContext(pageContext);
                        secICDDesc.doEndTag();

                        out.println("</tr>");

                        //Co-Morbidity ICD 
                        out.println("<tr>");

                        LabelTextDisplay coMorICD = new LabelTextDisplay();
                        coMorICD.setDisplayName("Co-Morbidity ICD");
                        coMorICD.setElementName("coMorICD");
                        coMorICD.setBoldDisplay("yes");
                        coMorICD.setValueFromSession("yes");
                        coMorICD.setPageContext(pageContext);
                        coMorICD.doEndTag();

                        LabelTextDisplay coMorICDDesc = new LabelTextDisplay();
                        coMorICDDesc.setDisplayName("Description");
                        coMorICDDesc.setElementName("coMorICDDesc");
                        coMorICDDesc.setBoldDisplay("yes");
                        coMorICDDesc.setValueFromSession("yes");
                        coMorICDDesc.setPageContext(pageContext);
                        coMorICDDesc.doEndTag();
                        out.println("</tr>");

                        out.println("<tr><td colspan=\"5\"><hr size=\"3px\" bgcolor=\"#0000CC\"/></td></tr>");
                        out.println("<tr><td colspan=\"5\"></td></tr>");
                        //Facility Provider
                        out.println("<tr>");
                        LabelTextDisplay facProv = new LabelTextDisplay();
                        facProv.setDisplayName("Facility Provider");

                        if (authType.trim().equals("1")) {
                            facProv.setElementName("facProv");
                        } else {
                            facProv.setElementName("facilityProv");
                        }

                        facProv.setBoldDisplay("yes");
                        facProv.setValueFromSession("yes");
                        facProv.setPageContext(pageContext);
                        facProv.doEndTag();

                        LabelTextDisplay facProvName = new LabelTextDisplay();
                        facProvName.setDisplayName("");
                        facProvName.setElementName("facProvDesc");
                        facProvName.setBoldDisplay("no");
                        facProvName.setValueFromSession("yes");
                        facProvName.setPageContext(pageContext);
                        facProvName.doEndTag();

                        out.println("</tr>");
                        if (authType.trim().equals("4") || authType.trim().equals("11")) {
                            //Providers
                            out.println("<tr>");
                            LabelTextDisplay anaProv = new LabelTextDisplay();
                            anaProv.setDisplayName("Anaesthetist Provider");
                            anaProv.setElementName("anaesthetistProv");
                            anaProv.setBoldDisplay("yes");
                            anaProv.setValueFromSession("yes");
                            anaProv.setPageContext(pageContext);
                            anaProv.doEndTag();

                            LabelTextDisplay anaProvDesc = new LabelTextDisplay();
                            anaProvDesc.setDisplayName("");
                            anaProvDesc.setElementName("anaesthetistProvDesc");
                            anaProvDesc.setBoldDisplay("no");
                            anaProvDesc.setValueFromSession("yes");
                            anaProvDesc.setPageContext(pageContext);
                            anaProvDesc.doEndTag();

                            out.println("</tr>");
                        }

                    } else if (authType.trim().equals("2") || authType.trim().equals("13")) {
                        out.println("<tr><td colspan=\"5\"><hr size=\"3px\" bgcolor=\"#0000CC\"/></td></tr>");
                        out.println("<tr><td colspan=\"5\"></td></tr>");
                        //Lab Provider
                        out.println("<tr>");
                        LabelTextDisplay labProv = new LabelTextDisplay();
                        labProv.setDisplayName("Lab Provider");
                        labProv.setElementName("labProv");
                        labProv.setBoldDisplay("yes");
                        labProv.setValueFromSession("yes");
                        labProv.setPageContext(pageContext);
                        labProv.doEndTag();

                        LabelTextDisplay labProvDesc = new LabelTextDisplay();
                        labProvDesc.setDisplayName("");
                        labProvDesc.setElementName("labProvDesc");
                        labProvDesc.setBoldDisplay("no");
                        labProvDesc.setValueFromSession("yes");
                        labProvDesc.setPageContext(pageContext);
                        labProvDesc.doEndTag();
                        out.println("</tr>");
                    }

                    if (authType.trim().equals("1")) {
                        out.println("<tr><td colspan=\"5\"><hr size=\"3px\" bgcolor=\"#0000CC\"/></td></tr>");
                        out.println("<tr><td colspan=\"5\"></td></tr>");
                        out.println("<tr>");
                        //Previous Lens RX
                        LabelTextDisplay preLensRx = new LabelTextDisplay();
                        preLensRx.setDisplayName("Previous Lens RX");
                        preLensRx.setElementName("preLensRx");
                        preLensRx.setBoldDisplay("yes");
                        preLensRx.setValueFromSession("yes");
                        preLensRx.setPageContext(pageContext);
                        preLensRx.doEndTag();
                        //Current Lens RX
                        LabelTextDisplay curLensRx = new LabelTextDisplay();
                        curLensRx.setDisplayName("Current Lens RX");
                        curLensRx.setElementName("curLensRx");
                        curLensRx.setBoldDisplay("yes");
                        curLensRx.setValueFromSession("yes");
                        curLensRx.setPageContext(pageContext);
                        curLensRx.doEndTag();
                        out.println("</tr>");

                        out.println("<tr>");
                        //number of days
                        LabelTextDisplay numDays = new LabelTextDisplay();
                        numDays.setDisplayName("Number Of Days");
                        numDays.setElementName("numDays");
                        numDays.setBoldDisplay("yes");
                        numDays.setValueFromSession("yes");
                        numDays.setPageContext(pageContext);
                        numDays.doEndTag();
                        out.println("</tr>");

                    } else if (authType.trim().equals("2") || authType.trim().equals("13")) {
                        out.println("<tr><td colspan=\"5\"><hr size=\"3px\" bgcolor=\"#0000CC\"/></td></tr>");
                        out.println("<tr><td colspan=\"5\"></td></tr>");
                        out.println("<tr>");
                        //sub type
                        LabelTextDisplay dentalSubType = new LabelTextDisplay();
                        dentalSubType.setDisplayName("Dental Sub Type");
                        dentalSubType.setElementName("dentalSubType");
                        dentalSubType.setBoldDisplay("yes");
                        dentalSubType.setValueFromSession("yes");
                        dentalSubType.setPageContext(pageContext);
                        dentalSubType.doEndTag();
                        out.println("</tr>");

                        out.println("<tr>");
                        //Amount Claimed
                        LabelTextDisplay amountClaimed = new LabelTextDisplay();
                        amountClaimed.setDisplayName("Amount Claimed");
                        amountClaimed.setElementName("amountClaimed");
                        amountClaimed.setBoldDisplay("yes");
                        amountClaimed.setValueFromSession("yes");
                        amountClaimed.setPageContext(pageContext);
                        amountClaimed.doEndTag();
                        out.println("</tr>");

                    } else if (authType.trim().equals("3")) {
                        out.println("<tr><td colspan=\"5\"><hr size=\"3px\" bgcolor=\"#0000CC\"/></td></tr>");
                        out.println("<tr><td colspan=\"5\"></td></tr>");
                        out.println("<tr>");
                        //planDuration
                        LabelTextDisplay planDuration = new LabelTextDisplay();
                        planDuration.setDisplayName("Plan Duration");
                        planDuration.setElementName("planDuration");
                        planDuration.setBoldDisplay("yes");
                        planDuration.setValueFromSession("yes");
                        planDuration.setPageContext(pageContext);
                        planDuration.doEndTag();
                        out.println("</tr>");

                        out.println("<tr>");
                        //amount Claimed
                        LabelTextDisplay amountClaimed = new LabelTextDisplay();
                        amountClaimed.setDisplayName("Amount Claimed");
                        amountClaimed.setElementName("amountClaimed");
                        amountClaimed.setBoldDisplay("yes");
                        amountClaimed.setValueFromSession("yes");
                        amountClaimed.setPageContext(pageContext);
                        amountClaimed.doEndTag();
                        //deposit
                        LabelTextDisplay deposit = new LabelTextDisplay();
                        deposit.setDisplayName("Deposit Amount");
                        deposit.setElementName("deposit");
                        deposit.setBoldDisplay("yes");
                        deposit.setValueFromSession("yes");
                        deposit.setPageContext(pageContext);
                        deposit.doEndTag();
                        out.println("</tr>");

                        out.println("<tr>");
                        //First Installment
                        LabelTextDisplay firstInstall = new LabelTextDisplay();
                        firstInstall.setDisplayName("First Installment");
                        firstInstall.setElementName("firstInstall");
                        firstInstall.setBoldDisplay("yes");
                        firstInstall.setValueFromSession("yes");
                        firstInstall.setPageContext(pageContext);
                        firstInstall.doEndTag();
                        //Remaining Installment
                        LabelTextDisplay remainInstall = new LabelTextDisplay();
                        remainInstall.setDisplayName("Remaining Installment");
                        remainInstall.setElementName("remainInstall");
                        remainInstall.setBoldDisplay("yes");
                        remainInstall.setValueFromSession("yes");
                        remainInstall.setPageContext(pageContext);
                        remainInstall.doEndTag();
                        out.println("</tr>");

                        out.println("<tr>");
                        //First Installment
                        LabelTextDisplay planFrom = new LabelTextDisplay();
                        planFrom.setDisplayName("Plan Start");
                        planFrom.setElementName("planFrom");
                        planFrom.setBoldDisplay("yes");
                        planFrom.setValueFromSession("yes");
                        planFrom.setPageContext(pageContext);
                        planFrom.doEndTag();
                        //Remaining Installment
                        LabelTextDisplay planEstimateEnd = new LabelTextDisplay();
                        planEstimateEnd.setDisplayName("Plan Estimate End");
                        planEstimateEnd.setElementName("planEstimateEnd");
                        planEstimateEnd.setBoldDisplay("yes");
                        planEstimateEnd.setValueFromSession("yes");
                        planEstimateEnd.setPageContext(pageContext);
                        planEstimateEnd.doEndTag();
                        out.println("</tr>");

                    } else if (authType.trim().equals("4") || authType.trim().equals("11")) {
                        out.println("<tr><td colspan=\"5\"><hr size=\"3px\" bgcolor=\"#0000CC\"/></td></tr>");
                        out.println("<tr><td colspan=\"5\"></td></tr>");
                        out.println("<tr>");
                        //Reimbursement Model
                        LabelTextDisplay reimbursementModel = new LabelTextDisplay();
                        reimbursementModel.setDisplayName("Reimbursement Model");
                        reimbursementModel.setElementName("reimbursementModel");
                        reimbursementModel.setBoldDisplay("yes");
                        reimbursementModel.setValueFromSession("yes");
                        reimbursementModel.setPageContext(pageContext);
                        reimbursementModel.doEndTag();
                        out.println("</tr>");

                        out.println("<tr>");
                        //pmb
                        LabelTextDisplay pmb = new LabelTextDisplay();
                        pmb.setDisplayName("PMB Payment");
                        pmb.setElementName("pmb_display");
                        pmb.setBoldDisplay("yes");
                        pmb.setValueFromSession("yes");
                        pmb.setPageContext(pageContext);
                        pmb.doEndTag();
                        //coPay

                        if (!client.equalsIgnoreCase("Sechaba")) {

                            LabelTextDisplay coPay = new LabelTextDisplay();
                            coPay.setDisplayName("Co-Payment");
                            coPay.setElementName("coPay");
                            coPay.setBoldDisplay("yes");
                            coPay.setValueFromSession("yes");
                            coPay.setPageContext(pageContext);
                            coPay.doEndTag();
                            out.println("</tr>");

                            //72 hour penalty
                            String penSesVal = "" + session.getAttribute("penPercDisplay");
                            String penReasonSesVal = "" + session.getAttribute("penReasonDisplay");
                            if (penSesVal != null && !penSesVal.equalsIgnoreCase("")
                                    && !penSesVal.equalsIgnoreCase("null")) {
                                //penalties
                                out.println("<tr>");
                                //percentage
                                LabelTextDisplay pp = new LabelTextDisplay();
                                pp.setDisplayName("Late Auth Penalty Percentage");
                                pp.setElementName("penPercDisplay");
                                pp.setBoldDisplay("yes");
                                pp.setValueFromSession("yes");
                                pp.setPageContext(pageContext);
                                pp.doEndTag();

                                if (penReasonSesVal != null && !penReasonSesVal.equals("")
                                        && !penReasonSesVal.equalsIgnoreCase("null") && !penReasonSesVal.equalsIgnoreCase("penalty")) {
                                    //reason
                                    LabelTextDisplay pr = new LabelTextDisplay();
                                    pr.setDisplayName("Override Reason");
                                    pr.setElementName("penReasonDisplay");
                                    pr.setBoldDisplay("yes");
                                    pr.setValueFromSession("yes");
                                    pr.setPageContext(pageContext);
                                    pr.doEndTag();
                                }

                                out.println("</tr>");
                            }

                            //dsp copay
                            String dspPenSesVal = "" + session.getAttribute("dspPenPercDisplay");
                            if (dspPenSesVal != null && !dspPenSesVal.equalsIgnoreCase("")
                                    && !dspPenSesVal.equalsIgnoreCase("null")) {
                                //penalties
                                out.println("<tr>");
                                //percentage
                                LabelTextDisplay pp = new LabelTextDisplay();
                                pp.setDisplayName("DSP Copay Percentage");
                                pp.setElementName("dspPenPercDisplay");
                                pp.setBoldDisplay("yes");
                                pp.setValueFromSession("yes");
                                pp.setPageContext(pageContext);
                                pp.doEndTag();
                                //reason
                                LabelTextDisplay pr = new LabelTextDisplay();
                                pr.setDisplayName("DSP Copay Override Reason");
                                pr.setElementName("dspPenReasonDisplay");
                                pr.setBoldDisplay("yes");
                                pr.setValueFromSession("yes");
                                pr.setPageContext(pageContext);
                                pr.doEndTag();
                                out.println("</tr>");
                            }
                        }
                        out.println("<tr>");
                        //funder
                        LabelTextDisplay funder = new LabelTextDisplay();
                        funder.setDisplayName("Funder");
                        funder.setElementName("funder");
                        funder.setBoldDisplay("yes");
                        funder.setValueFromSession("yes");
                        funder.setPageContext(pageContext);
                        funder.doEndTag();
                        //amended date
                        LabelTextDisplay amendedDate = new LabelTextDisplay();
                        amendedDate.setDisplayName("Amended Date");
                        amendedDate.setElementName("amendedDate");
                        amendedDate.setBoldDisplay("yes");
                        amendedDate.setValueFromSession("yes");
                        amendedDate.setPageContext(pageContext);
                        amendedDate.doEndTag();
                        out.println("</tr>");
                        
                        if (client.equalsIgnoreCase("Sechaba")) {
                            //authorwise type
                            out.println("<tr>");
                            LabelTextDisplay authorwiseType = new LabelTextDisplay();
                            authorwiseType.setDisplayName("Authorwise Type");
                            authorwiseType.setElementName("authorWiseDisplay");
                            authorwiseType.setBoldDisplay("yes");
                            authorwiseType.setValueFromSession("yes");
                            authorwiseType.setPageContext(pageContext);
                            authorwiseType.doEndTag();
                            out.println("</tr>");
                        }
                    } else if (authType.trim().equals("5")){
                        //coPay
                        out.println("<tr>");
                        LabelTextDisplay coPay = new LabelTextDisplay();
                        coPay.setDisplayName("Co-Payment");
                        coPay.setElementName("spmOHCopayValue");
                        coPay.setBoldDisplay("yes");
                        coPay.setValueFromSession("yes");
                        coPay.setPageContext(pageContext);
                        coPay.doEndTag();
                        
                        if (client.equalsIgnoreCase("Sechaba")) {
                            //auth sub type
                            LabelTextDisplay authSubType = new LabelTextDisplay();
                            authSubType.setDisplayName("Condition");
                            authSubType.setElementName("authSubType_display");
                            authSubType.setBoldDisplay("yes");
                            authSubType.setValueFromSession("yes");
                            authSubType.setPageContext(pageContext);
                            authSubType.doEndTag();
                            out.println("</tr>");
                        }
                    } else if (authType.trim().equals("9")) {
                        //PMB
                        out.println("<tr>");
                        LabelTextDisplay pmb = new LabelTextDisplay();
                        pmb.setDisplayName("PMB Payment");
                        pmb.setElementName("pmb_display");
                        pmb.setBoldDisplay("yes");
                        pmb.setValueFromSession("yes");
                        pmb.setPageContext(pageContext);
                        pmb.doEndTag();
                        
                        //coPay
                        LabelTextDisplay coPay = new LabelTextDisplay();
                        coPay.setDisplayName("Co-Payment");
                        coPay.setElementName("coPay");
                        coPay.setBoldDisplay("yes");
                        coPay.setValueFromSession("yes");
                        coPay.setPageContext(pageContext);
                        coPay.doEndTag();
                        out.println("</tr>");
                    }

                    out.println("</tr></table></td></tr>");//remember when adding extra fields to move div tag

                    //new notes
                    out.println("<tr><td colspan=\"5\"><hr size=\"3px\" bgcolor=\"#0000CC\"/></td></tr>");
                    out.println("<tr><td colspan=\"5\"></td></tr>");
                    out.println("<tr><td colspan=\"5\"></td></tr>");
                    out.println("<tr><td colspan=\"5\" align=\"left\"><label class=\"labelTextDisplay\">Notes</label></td></tr>");
                    out.println("<tr>");
                    AuthNoteConfirmationTag notes = new AuthNoteConfirmationTag();
                    notes.setSessionAttribute("authNoteList");
                    //notes.setJavaScript("conTables");
                    notes.setPageContext(pageContext);
                    notes.doEndTag();
                    out.println("</tr>");

                    out.println("<tr><td colspan=\"5\"></td></tr>");
                    out.println("<tr><td colspan=\"5\"></td></tr>");
                    if (authType.trim().equalsIgnoreCase("6")) {
                        skipTariffCopayDisclaimer = true;
                        out.println("<tr><td colspan=\"5\" align=\"left\"><label class=\"labelTextDisplay\">Nappi Details</label></td></tr>");
                        out.println("<tr>");
                        //tariff
                        AuthNewNappiListDisplay napDis = new AuthNewNappiListDisplay();
                        napDis.setSessionAttribute("newNappiListArray");
                        //to do: set javeScript for "conTables"
                        napDis.setPageContext(pageContext);
                        napDis.doEndTag();
                        out.println("</tr>");
                    } else if (authType.trim().equals("5")) {
                        // tarrifs list
                        out.println("<tr><td colspan=\"5\" align=\"left\"><label class=\"labelTextDisplay\">Condition Specific Tarriff Details</label></td></tr>");
                        out.println("<tr>");
                        AuthBasketTariffDisplay tarList = new AuthBasketTariffDisplay();
                        tarList.setSessionAttribute("AuthBasketTariffs");
                        tarList.setPageContext(pageContext);
                        tarList.doEndTag();
                        out.println("</tr>");
                        //primary icd

                        // medicine list
                        out.println("<tr><td colspan=\"5\" align=\"left\"><label class=\"labelTextDisplay\">Condition Specific Medicine Details</label></td></tr>");
                        out.println("<tr>");
                        AuthNappiDisplayTag medList = new AuthNappiDisplayTag();
                        medList.setSessionAttribute("AuthNappiTariffs");
                        medList.setPageContext(pageContext);
                        medList.doEndTag();
                        out.println("</tr>");

                    } else {
                        out.println("<tr><td colspan=\"5\" align=\"left\"><label class=\"labelTextDisplay\">Tariff Details</label></td></tr>");
                        out.println("<tr>");
                        //tariff
                        AuthTariffListDisplayTag tarifftag = new AuthTariffListDisplayTag();
                        tarifftag.setCommandName("");
                        tarifftag.setSessionAttribute("tariffListArray");
                        //tarifftag.setJavaScript("conTables");
                        tarifftag.setPageContext(pageContext);
                        tarifftag.doEndTag();
                        out.println("</tr>");
                    }

                    if (authType.trim().equalsIgnoreCase("3") || authType.trim().equalsIgnoreCase("2") || authType.trim().equalsIgnoreCase("13")) {
                        out.println("<tr><td colspan=\"5\"></td></tr>");
                        out.println("<tr><td colspan=\"5\"></td></tr>");
                        out.println("<tr><td colspan=\"5\" align=\"left\"><label class=\"labelTextDisplay\">Dental Lab Tariff Details</label></td></tr>");
                        out.println("<tr>");
                        //lab Tariffs
                        AuthLabTariffListDisplayTag labTariffs = new AuthLabTariffListDisplayTag();
                        labTariffs.setCommandName("");
                        labTariffs.setSessionAttribute("labTariffs");
                        //labTariffs.setJavaScript("conTables");
                        labTariffs.setPageContext(pageContext);
                        labTariffs.doEndTag();
                        out.println("</tr>");

                    } else if (authType.trim().equalsIgnoreCase("4") || authType.trim().equals("11")) {
                        //cptList
                        out.println("<tr><td colspan=\"5\"></td></tr>");
                        out.println("<tr><td colspan=\"5\"></td></tr>");
                        out.println("<tr><td colspan=\"5\" align=\"left\"><label class=\"labelTextDisplay\">CPT Details</label></td></tr>");
                        out.println("<tr>");
                        AuthCPTListDisplay cptList = new AuthCPTListDisplay();
                        cptList.setCommandName("");
                        cptList.setSessionAttribute("cptList");
                        //cptList.setJavaScript("conTables");
                        cptList.setPageContext(pageContext);
                        cptList.doEndTag();
                        out.println("</tr>");

                        //level of care
                        out.println("<tr><td colspan=\"5\"></td></tr>");
                        out.println("<tr><td colspan=\"5\"></td></tr>");
                        out.println("<tr><td colspan=\"5\" align=\"left\"><label class=\"labelTextDisplay\">Hospital Level Of Care Details</label></td></tr>");
                        out.println("<tr>");
                        AuthLevelOfCareListDisplay locDisp = new AuthLevelOfCareListDisplay();
                        locDisp.setCommandName("");
                        //locDisp.setJavaScript("conTables");
                        locDisp.setSessionAttribute("locList");
                        locDisp.setPageContext(pageContext);
                        locDisp.doEndTag();
                        out.println("</tr>");

                    }

                    if (authType.trim().equalsIgnoreCase("4") || authType.trim().equalsIgnoreCase("9") || authType.trim().equalsIgnoreCase("10") || authType.trim().equalsIgnoreCase("11")) {
                        //rules message
                        out.println("<tr><td colspan=\"5\"></td></tr>");
                        out.println("<tr><td colspan=\"5\"></td></tr>");
                        out.println("<tr><td colspan=\"5\" align=\"left\"><label class=\"labelTextDisplay\">Validation Details</label></td></tr>");
                        out.println("<tr>");
                        AuthRuleMessageDisplay rules = new AuthRuleMessageDisplay();
                        rules.setSessionAttribute("ruleList");
                        //rules.setJavaScript("conTables");
                        rules.setPageContext(pageContext);
                        rules.doEndTag();
                        out.println("</tr>");
                    }

                    out.println("<tr><td colspan=\"5\"></td></tr>");
                    out.println("<tr><td colspan=\"5\"></td></tr>");
                    out.println("<tr><td colspan=\"5\"></td></tr>");
                    out.println("<tr><td><table id=\"authDisclaimerDetails\">");
                    out.println("<tr><td colspan=\"4\">");
                    out.println("<label class=\"subheader\">Disclaimer:</label>");
                    out.println("<p>" + authDisclaimer + "</p><br/>");
                } else {
                    System.out.println("getAuthType is null");
                }

                //add future date disclaimer
                if (!skipTariffCopayDisclaimer) {
                    String authNoStr = "" + session.getAttribute("authNo");
                    if (authNoStr != null && !authNoStr.equalsIgnoreCase("")) {
                        int fromDateYear = Integer.parseInt(("" + session.getAttribute("authFrom")).substring(0, 4));
                        int authNoYear = Integer.parseInt(authNoStr.substring(3, 7));

                        if (authNoYear < fromDateYear) {
                            out.println("<p>Please note that the tariffs and co-payments are based on " + authNoYear + " benefits. <br/> All " + fromDateYear + " authorisations will be subject to " + fromDateYear + " scheme rules and benefit allocation. <br/>This may affect the co-payment.</p>");
                        } else {
                            out.println("<p>Please note that the tariffs and co-payments are based on " + authNoYear + " benefits.</p>");
                        }
                    }
                }
                out.println("</td></tr>");
                out.println("</table></td></tr>");
            } else {
                System.out.println("getAuthDetails is empty");
            }

        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (JspException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (NumberFormatException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        return super.doEndTag();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author pavaniv
 */
public class ForwardToMemberAuditTrail extends NeoCommand {
     
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            context.getRequestDispatcher("/Claims/CallCenterAuditTrail.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(ForwardToMemberAuditTrail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ForwardToMemberAuditTrail.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public String getName() {
        return "ForwardToMemberAuditTrail";
    }   
}

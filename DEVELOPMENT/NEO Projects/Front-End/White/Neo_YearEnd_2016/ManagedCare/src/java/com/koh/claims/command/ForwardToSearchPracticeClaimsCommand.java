/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ForwardToSearchPracticeClaimsCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(ForwardToSearchPracticeClaimsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        logger.info("Inside ForwardToSearchPracticeClaimsCommand");

        try {

            //String nextJSP = "/Claims/SearchPracticeClaims.jsp";
            String nextJSP = "/Claims/PracticeClaimSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "ForwardToSearchPracticeClaimsCommand";
    }
}

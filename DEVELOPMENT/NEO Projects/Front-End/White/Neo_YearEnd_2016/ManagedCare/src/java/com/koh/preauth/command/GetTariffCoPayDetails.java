/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthTariffDetails;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.SecurityResponsibility;

/**
 *
 * @author johanl
 */
public class GetTariffCoPayDetails extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        PrintWriter out = null;

        List<AuthTariffDetails> tList = (List<AuthTariffDetails>) session.getAttribute("tariffListArray");
        String option = "" + session.getAttribute("schemeOption");
        int optionId = 0;

        if (option != null && !option.equalsIgnoreCase("null") && !option.equals("")) {
            optionId = Integer.parseInt(option);
        }

        if (optionId != 0) {
            
            double copay = 0.0d;
            String authType = "" + session.getAttribute("authType");

            String admissionD = "";
            if (authType.equals("9")) {
                admissionD = "" + session.getAttribute("authFromDate");
            } else {
                admissionD = "" + session.getAttribute("admissionDateTime");
            }

            Date admissionDate = null;
            XMLGregorianCalendar authStart = null;
            try {
                admissionDate = new SimpleDateFormat("yyyy/MM/dd").parse(admissionD);
                authStart = DateTimeUtils.convertDateToXMLGregorianCalendar(admissionDate);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }

            //copayment override validation
            NeoUser user = (NeoUser) session.getAttribute("persist_user");
            int userRespId = 0;
            for (SecurityResponsibility security : user.getSecurityResponsibility()) {
                userRespId = security.getResponsibilityId();
            }

            String allowRORemove = "";
            if (userRespId == 2 || userRespId == 3) {
                allowRORemove = "yes";
            } else {
                allowRORemove = "no";
            }

            try {
                out = response.getWriter();
                if (tList != null && tList.isEmpty() == false && authStart != null) {
                    copay = port.getTariffCoPaymentAmountForTariffs(tList, optionId, authStart);
                    System.out.println("GetTariffCoPayDetails specialized radiology co payment amount = " + copay);

                    out.println("Done|" + allowRORemove + "|" + copay);

                } else {
                    out.println("Error|" + allowRORemove);
                }
            } catch (IOException io) {
                io.printStackTrace();
            } finally {
                out.close();
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetTariffCoPayDetails";
    }
}

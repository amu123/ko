/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.contribution.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.ContributionAge;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;
import neo.manager.Security;

/**
 *
 * @author yuganp
 */
public class TransferBalFromResignedMemberCommand extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            String s = request.getParameter("command");
            System.out.println(this.getName() + " : " + s);
            if (s != null && !s.isEmpty()) {
                System.out.println("command : " + s);
                if (s.equalsIgnoreCase("FindMember")) {
                    findMember(request, response, context);
                } else if (s.equalsIgnoreCase("Save")) {
                    save(request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(TransferBalFromResignedMemberCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void findMember(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String memberType = request.getParameter("type");
        String number = request.getParameter(memberType);
        long timeStart = System.currentTimeMillis();
        KeyValueArray member = port.findMemberByCoverNumber(number);
        long timeEnd = System.currentTimeMillis();
        System.out.println("Get Member took " + (timeEnd - timeStart) + " ms");
        Map<String, String> fieldsMap = new HashMap<String, String>();
        Map<String, String> map = MapUtils.getMap(member);
        if (member == null || map == null || map.isEmpty()) {
            fieldsMap.put(memberType + "Lbl", number + " not found");
            fieldsMap.put(memberType, "");
            fieldsMap.put("memberStatus", "Error");
            if ("coverFrom".equalsIgnoreCase(memberType)) {
                fieldsMap.put("transferAmount","");
            }
        } else if ("Active".equalsIgnoreCase(map.get("coverStatus")) && "coverFrom".equalsIgnoreCase(memberType)) {
            fieldsMap.put(memberType + "Lbl", "<b>Member:</b> " + map.get("entityName") + " is still Active");
            fieldsMap.put(memberType, "");
            fieldsMap.put("memberStatus", "Error");
        } else {
            timeStart = System.currentTimeMillis();
            List<ContributionAge> memberAgeAnalysisList = port.getAgeAnalysis(number, null);
            BigDecimal bal = new BigDecimal("0.00");
            if (memberAgeAnalysisList != null) {
                for (ContributionAge ca : memberAgeAnalysisList) {
                    bal = bal.add(ca.getAmountCurrentBalance().add(ca.getAmountFuture()));
                }
            }
            timeEnd = System.currentTimeMillis();
            System.out.println("Get Ageing " + (timeEnd - timeStart) + " ms");
            String balance = "";
            if (memberAgeAnalysisList == null || ("coverFrom".equalsIgnoreCase(memberType) && bal.compareTo(BigDecimal.ZERO) == 0)  ) {
                fieldsMap.put(memberType + "Lbl", "<b>Member:</b> " + map.get("entityName") + " has a zero balance");
                fieldsMap.put(memberType, "");
                fieldsMap.put("memberStatus", "Error");
            } else {    
                balance = " <b>Balance:</b> " + getStringValue(bal);
                if ("coverFrom".equalsIgnoreCase(memberType)) {
                    fieldsMap.put("transferAmount", getStringValue(bal));
                }
                fieldsMap.put(memberType + "Lbl", "<b>Member:</b> " + map.get("entityName") + " <b>End Date:</b> " + map.get("coverEndDate") + balance);
                fieldsMap.put("memberStatus", "");
                fieldsMap.put(memberType, number);
            }
        }
        String updateFields = TabUtils.convertMapToJSON(fieldsMap);
        String result = TabUtils.buildJsonResult("OK", null, updateFields, null);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println(result);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (out != null) out.close();
        }
    }
    
    private void save(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        PrintWriter out = null;
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String additionalData;
        try {
            out = response.getWriter();
            Security security = TabUtils.getSecurity(request);
            String coverFrom = request.getParameter("coverFrom");
            String coverTo = request.getParameter("coverTo");
            boolean result = port.transferBalanceBetweenMembers(coverFrom, coverTo, security);
            if (result) {
                additionalData = "\"message\":\"Transfer has been processed.\"";
                additionalData = additionalData + ", \"forward\":\"/ManagedCare/Security/Welcome.jsp\"";
            } else {
                additionalData = "\"message\":\"There has been an error processing the transfer.\"";
            }
            out.println(TabUtils.buildJsonResult("OK", null, null, additionalData));
        } catch (Exception e) {
            e.printStackTrace();
            additionalData = "\"message\":\"There has been an error processing the transfer.\"";
            out.println(TabUtils.buildJsonResult("OK", null, null, additionalData));
        } finally {
            if (out != null) out.close();
        }
    }
    
    private String getStringValue(BigDecimal bd) {
        try {
            return bd.setScale(2).toPlainString();
        } catch (Exception e) {
            e.printStackTrace();
            return "0.00";
        }
    }
    
    
    @Override
    public String getName() {
        return "TransferBalFromResignedMemberCommand";
    }
    
}

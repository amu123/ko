/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.fe.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.Exception_Exception;
import neo.manager.NeoUser;
import neo.manager.SecurityResponsibility;
import neo.manager.ProductRestriction;

/**
 *
 * @author gerritj
 */
public class LoginCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        String userName = request.getParameter("user");
        String password = request.getParameter("pass");
        String sysErrorMsg = "";
        boolean isSysError = false;
        boolean passUsed = false;
        boolean passChanged = false;
        boolean successfulLogin = false;
        int passAccessLeft = 0;
        NeoUser user = null;
        try {
            user = service.getNeoManagerBeanPort().authenticateUser(userName, password);
            successfulLogin = user.isSuccess();
            passAccessLeft = user.getPassAccessLeft();
            if (!successfulLogin) {
                request.setAttribute("error", user.getErrorMsg());
                String nextJSP = "/Login.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                try {
                    dispatcher.forward(request, response);
                } catch (ServletException ex) {
                    System.out.println(ex.getMessage());
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            } else if (passAccessLeft == 0 || passAccessLeft < 0) {
                request.setAttribute("error", "Your account has been locked. Please contact your System Administrator");
                String nextJSP = "/Login.jsp";
                RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                try {
                    dispatcher.forward(request, response);
                } catch (ServletException ex) {
                    System.out.println(ex.getMessage());
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            } else {
                // Get active client
                List<neo.manager.LookupValue> lookUpsClient = service.getNeoManagerBeanPort().findCodeTableForLookupType(341);
                if (lookUpsClient != null) {
                    neo.manager.LookupValue val = lookUpsClient.get(0);
                    if (val != null) {
                        System.out.println("Client : " + val.getValue());
                        session.setAttribute("Client", val.getValue());
                    }
                }

                Calendar cal = Calendar.getInstance();
                int currentMonth = cal.get(Calendar.MONTH) + 1;
                int lastUpdateMonth = 0;
                ///////////////COMPARE DATES////////////////////
                if (successfulLogin) {
                    XMLGregorianCalendar lastUpdateDate = null;
                    lastUpdateDate = user.getPasswordLastUpdateDate();
                    lastUpdateMonth = lastUpdateDate.getMonth();
                }

                if (lastUpdateMonth != currentMonth && request.getParameter("changeRequired").equalsIgnoreCase("")) {
                    /////////ASK USER TO CHANGE PASSWORD HERE//////////////

                    request.setAttribute("changeRequired", "1");
                    request.setAttribute("user", userName);
                    request.setAttribute("pass", password);
                    request.setAttribute("error", "Your password has expired, please enter a new password");
                    String nextJSP = "/Login.jsp";
                    RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                    try {
                        dispatcher.forward(request, response);
                    } catch (ServletException ex) {
                        System.out.println(ex.getMessage());
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                } else {
                    request.setAttribute("changeRequired", "");
                }

                if (!request.getParameter("newPassword").equalsIgnoreCase("")) {

                    String name = user.getUsername();
                    String pass = request.getParameter("newPassword");

                    passUsed = service.getNeoManagerBeanPort().validatePasswordHistory(pass, name);
                    if (passUsed == true) {
                        request.setAttribute("changeRequired", "1");
                        request.setAttribute("user", userName);
                        request.setAttribute("pass", password);
                        request.setAttribute("error", "Password previously used, please enter a different password");
                        String nextJSP = "/Login.jsp";
                        RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
                        try {
                            dispatcher.forward(request, response);
                        } catch (ServletException ex) {
                            System.out.println(ex.getMessage());
                        } catch (IOException ex) {
                            System.out.println(ex.getMessage());
                        }
                    } else {
                        request.setAttribute("changeRequired", "");

                        passChanged = service.getNeoManagerBeanPort().changePassword(user, password, pass);
                    }
                }

                session.setAttribute("persist_user", user);
                session.setAttribute("persist_user_sup", false);
                session.setAttribute("persist_user_productViewRes", false);
                session.setAttribute("persist_user_teamLead", false);
                session.setAttribute("persist_user_ccClearing", false);
                session.setAttribute("persist_user_viewMemApps", false);
                session.setAttribute("persist_user_viewStaff", false);
                session.setAttribute("persist_user_pdcUser", false);
                session.setAttribute("persist_user_pdcAdmin", false);

                //set current user
                session.setAttribute("persist_currentUser", user.getName() + " " + user.getSurname());

                List<SecurityResponsibility> securityList = user.getSecurityResponsibility();
                Map<String, Integer> userRestrictionMap = new HashMap<String, Integer>();
                List<ProductRestriction> userProductRes = user.getUserProductRestriction();
                //note - if no records found, allow user to view all scheme members
                userRestrictionMap.put("allowSpec", 1);
                userRestrictionMap.put("allowReso", 1);
                if (userProductRes != null) {
                    for (ProductRestriction res : userProductRes) {
                        if (res.getProductID() == 1) {
                            System.out.println("setting allow reso off...");
                            userRestrictionMap.put("allowReso", 1);
                            userRestrictionMap.put("allowSpec", 0);
                            break;
                        }
                        if (res.getProductID() == 2) {
                            System.out.println("setting allow spm off...");
                            userRestrictionMap.put("allowSpec", 1);
                            userRestrictionMap.put("allowReso", 0);
                            break;
                        }
                    }
                }
                System.out.println("product restriction map size:" + userRestrictionMap.size());
                session.setAttribute("persist_productViewRestriction", userRestrictionMap);

                for (SecurityResponsibility sec : securityList) {
                    System.out.println("sec.getResponsibilityId() " + sec.getDescription());
                    if (sec.getResponsibilityId() == 2) {
                        session.setAttribute("persist_user_sup", true);
                        session.setAttribute("persist_user_viewStaff", true);
                        session.setAttribute("persist_user_pdcAdmin", true);
                        break;
                    }
                    if (sec.getResponsibilityId() == 3) {
                        //session.setAttribute("persist_user_teamLead", false);
                        session.setAttribute("persist_user_teamLead", true);
                        session.setAttribute("persist_user_viewStaff", true);
                        session.setAttribute("persist_user_viewMemApps", true);
                        break;
                    }
                    if (sec.getResponsibilityId() == 4) {
                        session.setAttribute("persist_user_viewMemApps", true);
                        break;
                    }
                    if (sec.getResponsibilityId() == 7) {
                        session.setAttribute("persist_user_viewMemApps", true);
                        break;
                    }
                    if (sec.getResponsibilityId() == 12) {
                        session.setAttribute("persist_user_viewStaff", true);
                        session.setAttribute("persist_user_viewMemApps", true);
                        break;
                    }
                    if (sec.getResponsibilityId() == 14) {
                        session.setAttribute("persist_user_ccClearing", true);
                        break;
                    }
                    if (sec.getResponsibilityId() == 18) {
                        session.setAttribute("persist_user_viewMemApps", true);
                        break;
                    }
                    if (sec.getResponsibilityId() == 19) {
                        session.setAttribute("persist_user_viewStaff", true);
                        break;
                    }
                    if (sec.getResponsibilityId() == 20) {
                        session.setAttribute("persist_user_pdcUser", true);
                        break;
                    }
                    if (sec.getResponsibilityId() == 21) {
                        session.setAttribute("persist_user_pdcAdmin", true);
                        break;
                    }
                }

                PrintWriter out;
                try {
                    String client = String.valueOf(context.getAttribute("Client"));
                    String environment = String.valueOf(context.getAttribute("Environment"));
                    out = response.getWriter();

                    out.println("<html>");
                    out.println("<head>");
                    out.println("<script type=\"text/javascript\">");
                    out.println("function doMenuClick(p) {");
                    out.println("parent.header.document.forms[0].application.value = p;");
                    out.println("parent.header.document.forms[0].submit();");

                    if (client != null && client.equalsIgnoreCase("Sechaba")) {
                        out.println("parent.workflow.document.forms[0].application.value = p;");
                        out.println("parent.workflow.document.forms[0].submit();");
                        out.println("parent.workflowFrameset.cols = \"34.2%,65.8%\";");
                    }
                    out.println(" }");
                    out.println(" </script>");
                    out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                    out.println("</head>");
                    out.println("<body onload=\"doMenuClick(\'internet\')\">");
                    out.println("<center>");
                    out.println("<table cellspacing=\"4\">");
                    out.println("<tr>");

                    if (client != null && client.equalsIgnoreCase("Agility")) {
                        out.println("<td><label class=\"header\">Welcome</label></td>");
                        out.println("<td><img src=\"resources/welcome.jpg\" alt=\"Welcome\"/></td>");
                    } else if (client != null && client.equalsIgnoreCase("Sechaba") && environment.equalsIgnoreCase("UAT")) {
                        //out.println("<td><label class=\"header\">Under Construction</label></td>");
                        out.println("<td><img src=\"resources/uat_notif.jpg\" alt=\"Welcome\"/></td>");
                    } else if (client != null && client.equalsIgnoreCase("Sechaba") && environment.equalsIgnoreCase("Production")) {
                        out.println("<td><label class=\"header\">Production</label></td>");
                        out.println("<td><img src=\"resources/production.jpg\" alt=\"Welcome\"/></td>");
                    }
                    out.println("</tr>");
                    out.println("</table>");
                    out.println("<p> </p>");
                    out.println("</center>");
                    out.println("</body>");
                    out.println("</html>");

                    out.close();
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (Exception_Exception ex) {
            Logger.getLogger(LoginCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "LoginCommand";
    }

    public static NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser) request.getSession().getAttribute("persist_user");
    }
}

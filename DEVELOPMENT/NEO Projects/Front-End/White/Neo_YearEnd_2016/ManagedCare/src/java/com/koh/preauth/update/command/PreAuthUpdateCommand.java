/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.update.command;

import com.agile.command.GetCoverDetailsByNumber;
import com.agile.command.GetICD10DetailsByCode;
import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.preauth.command.GetPMBByICDCommand;
import com.koh.preauth.dto.AuthConfirmationDetails;
import com.koh.utils.DateTimeUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthCPTDetails;
import neo.manager.AuthNoteDetail;
import neo.manager.AuthOrthoPlanDetails;
import neo.manager.AuthSpecificBasket;
import neo.manager.AuthSpecificBasketTariffs;
import neo.manager.AuthTariffDetails;
import neo.manager.Benefit;
import neo.manager.CoverDetails;
import neo.manager.Diagnosis;
import neo.manager.EAuthCoverDetails;
import neo.manager.LabTariffDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Option;
import neo.manager.PracticeCaptureDetails;
import neo.manager.PracticeNetworkDetails;
import neo.manager.PreAuthConfirmationDetails;
import neo.manager.PreAuthDetails;
import neo.manager.Procedure;
import neo.manager.ProviderSearchDetails;

/**
 *
 * @author johanl
 */
public class PreAuthUpdateCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        AuthConfirmationDetails authConfirm = null; //584461 618910 947219
        authConfirm = (AuthConfirmationDetails) session.getAttribute("PreAuthConfirmation");
        List<EAuthCoverDetails> resignedCovDepList = new ArrayList<EAuthCoverDetails>();
        //get resigned member details if exist
        if (session.getAttribute("memberClaimSearchResult") != null) {
            resignedCovDepList = (List<EAuthCoverDetails>) session.getAttribute("memberClaimSearchResult");
        }
                
        this.clearAllFromSession(request);
        //session.setAttribute("persist_authUpInd", "yes");

        List<PreAuthDetails> paList = authConfirm.getAuthDetails();
        if (paList != null && paList.size() > 0) {
            PreAuthDetails pa = paList.get(0);
            PreAuthConfirmationDetails preCon = authConfirm.getAuthConfirmation();
            String authType = pa.getAuthType();

            //hospital check for old auth
            String authNumber = pa.getAuthNumber();
            boolean newAuth = false;
            session.setAttribute("newAuthIndicator", false);
            if (authNumber != null && !authNumber.trim().equalsIgnoreCase("")) {
                String ind = authNumber.substring(authNumber.length() - 1, authNumber.length());
                if (ind.trim().equalsIgnoreCase("A")) {
                    session.setAttribute("newAuthIndicator", true);
                    newAuth = true;
                }
            }

            //set last updated user
            if (pa.getUserId() != 0) {
                NeoUser user = port.getUserSafeDetailsById(pa.getUserId());
                System.out.println("auth last updated by : " + pa.getUserId() + " ," + user.getName() + " " + user.getSurname());
                if (user.getSurname() != null && !user.getSurname().equalsIgnoreCase("")) {
                    session.setAttribute("lastUpUser", user.getName() + " " + user.getSurname());
                } else {
                    session.setAttribute("lastUpUser", user.getName());
                }
            }

            //set update fields
            session.setAttribute("updateAuthNumber", authConfirm.getAuthNumber());
            session.setAttribute("updateAuthID", authConfirm.getAuthId());

            /*session.setAttribute("scheme", preCon.getSchemeId());
             session.setAttribute("schemeOption", preCon.getSchemeOptionId());
             session.setAttribute("schemeName", preCon.getScheme());
             session.setAttribute("schemeOptionName", preCon.getSchemeOption());*/



            //set benefit allocated
            if (pa.getBenefitCode() != null && !pa.getBenefitCode().equalsIgnoreCase("")) {
                LookupValue lv = new LookupValue();
                String benCode = pa.getBenefitCode();
                Benefit ben = port.findBenefitByCode(benCode);
                String benDesc = ben.getDescription();

                lv.setId(benCode);
                lv.setValue(benDesc);

                session.setAttribute("benAllocated", lv);

            }

            //set auth period
            String dateSaved = "";
            if (resignedCovDepList == null || resignedCovDepList.isEmpty()) {
                dateSaved = new SimpleDateFormat("yyyy/MM/dd").format(pa.getAuthStartDate().toGregorianCalendar().getTime());
            } else {
                EAuthCoverDetails ea = resignedCovDepList.get(0);
                dateSaved = new SimpleDateFormat("yyyy/MM/dd").format(ea.getCoverEndDate().toGregorianCalendar().getTime());
            }

            String yearSaved = dateSaved.substring(0, 4);
            String monthSaved = dateSaved.substring(5, 7);
            System.out.println("year saved = " + yearSaved);
            System.out.println("month saved = " + monthSaved);
            session.setAttribute("authPeriod", yearSaved);
            session.setAttribute("authMonthPeriod", monthSaved);

            //validate dependant
            Date memberDate = null;
            XMLGregorianCalendar xMemDate = null;
            try {
                memberDate = new SimpleDateFormat("yyyy/MM/dd").parse(yearSaved + "/" + monthSaved + "/01");
                xMemDate = DateTimeUtils.convertDateToXMLGregorianCalendar(memberDate);
            } catch (ParseException ex) {
                Logger.getLogger(PreAuthUpdateCommand.class.getName()).log(Level.SEVERE, null, ex);
            }
            //CoverDetails cd = port.getDependentForCoverByDate(pa.getCoverNumber(), xMemDate, pa.getDependantCode());
            //session.setAttribute("covDepListDetails", cd);

            List<CoverDetails> coverList = new GetCoverDetailsByNumber().setCoverForUpdate(pa.getCoverNumber(), pa.getDependantCode());
            CoverDetails cd = null;
            if (coverList != null && !coverList.isEmpty()) {
                session.setAttribute("covMemListDetails", coverList);
                session.setAttribute("depListValues", pa.getDependantCode());
                for (CoverDetails cover : coverList) {
                    if (cover.getDependentNumber() == pa.getDependantCode()) {
                        cd = cover;
                        break;
                    }
                }
            }

            int optId = cd.getOptionId();
            Option opt = port.findOptionWithID(optId);
            String optionName = opt.getOptionName();
            //set product
            session.setAttribute("scheme", preCon.getSchemeId());
            session.setAttribute("schemeName", preCon.getScheme());
            session.setAttribute("schemeOption", optId);
            session.setAttribute("schemeOptionName", optionName);
            session.setAttribute("schemeOptionDetails", opt);


            String aDate = "";
            if (newAuth) {
                aDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(pa.getAuthorisationDate().toGregorianCalendar().getTime());
            } else {
                aDate = new SimpleDateFormat("yyyy/MM/dd").format(pa.getAuthorisationDate().toGregorianCalendar().getTime());
            }

            String aFrom = "";
            String aTo = "";
            XMLGregorianCalendar authStart = pa.getAuthStartDate();
            XMLGregorianCalendar authEnd = pa.getAuthEndDate();
            if (!authType.trim().equals("4") && !authType.trim().equals("5")) {
                if (pa.getAuthTariffs().isEmpty() != true) {
                    if (pa.getAuthTariffs().get(0).getTariffType().equalsIgnoreCase("3")) {
                        session.setAttribute("newNappiListArray", pa.getAuthTariffs());
                    } else {
                        session.setAttribute("tariffListArray", pa.getAuthTariffs());
                    }
                } else {
                    session.setAttribute("newNappiListArray", null);
                    session.setAttribute("tariffListArray", null);
                }

            }
            
            //dsp 
            String isDSPstr = "";
            if (session.getAttribute("isDSPPenalty") != null) {
                isDSPstr = "" + session.getAttribute("isDSPPenalty");
            }
            
            //DSP
            if (isDSPstr != null && !isDSPstr.equalsIgnoreCase("")) {
                if (isDSPstr.equalsIgnoreCase("n")) {
                    session.setAttribute("isDSPPenalty", "no");
                } else if (isDSPstr.equalsIgnoreCase("y")) {
                    session.setAttribute("isDSPPenalty", "yes");
                }
            }

            if (authType.trim().equals("4") || authType.trim().equals("11")) {
                if (authStart != null) {
                    aFrom = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(pa.getAuthStartDate().toGregorianCalendar().getTime());
                }
                if (authEnd != null) {
                    aTo = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(pa.getAuthEndDate().toGregorianCalendar().getTime());
                }
                //SET TARIFFS
                session.setAttribute("tariffListArray", pa.getAuthTariffs());
                //SET CPT's
                List<AuthCPTDetails> cptList = pa.getAuthCPTDetails();
                if (cptList != null) {
                    for (AuthCPTDetails cpt : cptList) {
                        Procedure proc = port.getProcedureForCode(cpt.getCptCode());
                        if (proc != null) {
                            cpt.setCptCodeDesc(proc.getDescription());
                        }
                    }
                    session.setAttribute("authCPTListDetails", cptList);
                } else {
                    session.setAttribute("authCPTListDetails", pa.getAuthCPTDetails());
                }
                //SET PMB's
                session.setAttribute("savedAuthPMBs", pa.getAuthPMBDetails());

            } else {
                if (authStart != null) {
                    aFrom = new SimpleDateFormat("yyyy/MM/dd").format(pa.getAuthStartDate().toGregorianCalendar().getTime());
                }
                if (authEnd != null) {
                    aTo = new SimpleDateFormat("yyyy/MM/dd").format(pa.getAuthEndDate().toGregorianCalendar().getTime());
                }
            }

            session.setAttribute("authType", pa.getAuthType());
            String aTypeStr = port.getValueFromCodeTableForTableId(89, pa.getAuthType());
            session.setAttribute("authType_text", aTypeStr);

            session.setAttribute("authDate", aDate);
            session.setAttribute("authDate_text", aDate);

            session.setAttribute("authFromDate", aFrom);
            session.setAttribute("authToDate", aTo);
            session.setAttribute("authStatus", pa.getAuthStatus());

            if (!pa.getAuthType().equalsIgnoreCase("4") && !pa.getAuthType().equalsIgnoreCase("11")) {
                if (pa.getAuthStatus().equalsIgnoreCase("2")) {
                    session.setAttribute("authRejReason", pa.getHospitalStatus());
                }
            }
            //session.setAttribute("notes", pa.getNotes()); old notes
            List<AuthNoteDetail> notes = pa.getAuthNotes();
//        String allNotes = "";
//        if (notes != null) {
//            for (AuthNoteDetail aNote : notes) {
//                allNotes = allNotes + aNote.getNote() + '\n';
//            }
//        }
            session.setAttribute("authNoteList", notes);
//        session.setAttribute("notes", allNotes);

            session.setAttribute("memberNum_label", pa.getCoverNumber());
            session.setAttribute("memberNum_text", pa.getCoverNumber());
            session.setAttribute("depListValues", pa.getDependantCode());
            //String memDetail = preCon.getCoverName() + " " + preCon.getCoverSurname();
            //session.setAttribute("depListValues_text", memDetail);

            session.setAttribute("treatingProvider_text", pa.getTreatingProviderNo());
            session.setAttribute("referringProvider_text", pa.getReferringProviderNo());

            session.setAttribute("primaryICD_text", pa.getPrimaryICD());
            session.setAttribute("secondaryICD_text", pa.getSecondaryICD());
            session.setAttribute("coMorbidityICD_text", pa.getCoMorbidityICD());
            session.setAttribute("admissionICD_text", pa.getAdmissionICD());
            //SET ICD DESCRIPTIONS
            if (pa.getPrimaryICD() != null && !pa.getPrimaryICD().equalsIgnoreCase("")) {
                String desc = getAuthICDDesc(port, null, pa.getPrimaryICD());
                session.setAttribute("primaryICDDescription", desc);
            }
            if (pa.getSecondaryICD() != null && !pa.getSecondaryICD().equalsIgnoreCase("")) {
                String[] icds = pa.getSecondaryICD().split(",");
                String desc = getAuthICDDesc(port, icds, null);
                session.setAttribute("secondaryICDDescription", desc);
            }
            if (pa.getCoMorbidityICD() != null && !pa.getCoMorbidityICD().equalsIgnoreCase("")) {
                String[] icds = pa.getCoMorbidityICD().split(",");
                String desc = getAuthICDDesc(port, icds, null);
                session.setAttribute("coMorbidityICDDescription", desc);
            }

            session.setAttribute("requestorName", pa.getRequestorName());
            session.setAttribute("requestorRelationship", pa.getRequestorRelationship());
            session.setAttribute("requestorReason", pa.getRequestorReason());
            session.setAttribute("requestorContact", pa.getRequestorContact());

            //load pmb details
            GetPMBByICDCommand getPMB = new GetPMBByICDCommand();
            getPMB.execute(request, response, context);

            //set copayment
            if (authType.trim().equalsIgnoreCase("4") || authType.trim().equalsIgnoreCase("11")) {
                //set copayment amount
                double copay = pa.getCoPaymentAmount();
                if (authType.trim().equalsIgnoreCase("4")) {
                    if (copay != 99999.0d && copay != 0.0d) {
                        session.setAttribute("coPay_error", null);

                    } else if (copay == 99999.0d) {
                        session.setAttribute("coPay_error",
                                "*Note: Procedure Excluded Unless PMB");
                    }
                }
                session.setAttribute("coPay", copay);

                if (copay == 102999.0d) {
                    copay = 99999.0d;
                }

                //set copay message
                session.setAttribute("coPay_error", null);
                if (copay != 99999.0d && copay != 0.0d) {
                    session.setAttribute("coPay_error", null);

                } else if (copay == 99999.0d) {
                    session.setAttribute("coPay_error",
                            "*Note: Procedure Excluded Unless PMB");
                }


                double dspCopay = 3675.0d;

                String option = String.valueOf(optId);
                boolean cptEmergency = false;
                if (pa.getAuthCPTDetails() != null && !pa.getAuthCPTDetails().isEmpty()) {
                    for (AuthCPTDetails cpts : pa.getAuthCPTDetails()) {
                        if (cpts.getProcedureLink() != null) {
                            if (cpts.getProcedureLink().equalsIgnoreCase("2")) {
                                cptEmergency = true;
                                break;
                            }
                        }
                    }
                }
                
                if ((option.equals("5") || option.equals("7")) && !cptEmergency) {
                    //check provider 
                    boolean isDSP = false;
                    String facilityProv = pa.getFacilityProviderNo();
                    boolean noProvSet = false;

                    System.out.println("facilityProv = " + facilityProv);
                    System.out.println("noProvSet = " + noProvSet);
                    
                    PracticeNetworkDetails pnd = service.getNeoManagerBeanPort().getPracticeNetworkByNumberForToday(facilityProv);
                    boolean provincialHosp = false;
                    if(pnd != null){
                        String facilityProvNetwork = pnd.getNetworkDescription();
                        session.setAttribute("facilityProvNetwork",facilityProvNetwork);
                        if(facilityProvNetwork.equalsIgnoreCase("Provincial Hospitals L1") 
                                || facilityProvNetwork.equalsIgnoreCase("Provincial Hospitals L2") 
                                || facilityProvNetwork.equalsIgnoreCase("Provincial Hospitals L3")){
                            provincialHosp = true;
                        }
                    }
                    if (!noProvSet && !provincialHosp) {
                        isDSP = port.isDSPNetwork(facilityProv, authStart, preCon.getSchemeId(), preCon.getSchemeOptionId());
                        if (copay != 0.0d && copay != 99999.0d && !isDSP) {
                            session.setAttribute("coPay_error",
                                    "*Note: Normal Copay of R" + DateTimeUtils.decimalFormat.format(copay) + " "
                                    + "applies with an addtional R3675 for Non-DSP");
                            copay += dspCopay;

                        } else if (copay == 0.0d && !isDSP) {
                            session.setAttribute("coPay_error",
                                    "*Note: An addtional R3675 for Non-DSP Applies on Facility " + facilityProv);
                            copay += dspCopay;

                        } else if (copay == 99999.0d && !isDSP) {
                            session.setAttribute("coPay_error",
                                    "*Note: Procedure Excluded Unless PMB R" + DateTimeUtils.decimalFormat.format(copay) + " "
                                    + "applies with an addtional R3675 for Non-DSP");
                            copay += dspCopay;
                        }
                        if (!isDSP) {
                            session.setAttribute("dspCopayApplies", "yes");
                        }
                    } else {
                        session.setAttribute("dspCopayApplies", "no");
                    }
                } else {
                    if (copay == 99999.0d) {
                        session.setAttribute("coPay_error",
                                "*Note: Procedure Excluded Unless PMB R" + DateTimeUtils.decimalFormat.format(copay) + " ");
                    }
                    session.setAttribute("dspCopayApplies", "no");
                }
            }
            
            if (authType.trim().equals("1")) {
                session.setAttribute("facilityProv_text", pa.getFacilityProviderNo());
                session.setAttribute("secondaryICD_text", pa.getSecondaryICD());
                session.setAttribute("coMorbidityICD_text", pa.getCoMorbidityICD());

                session.setAttribute("numDays", pa.getNumberOfDays());
                session.setAttribute("prevLens", pa.getPreviousLensRx());
                session.setAttribute("curLens", pa.getCurrentLensRx());

            } else if (authType.trim().equals("2") || authType.trim().equals("13")) {
                session.setAttribute("DentalSubType", pa.getAuthSubType());

                //set provider disc
                ProviderSearchDetails pd = service.getNeoManagerBeanPort().findPracticeAjaxDetailsByNumber(pa.getTreatingProviderNo());
                ProviderSearchDetails pd2 = service.getNeoManagerBeanPort().findPracticeAjaxDetailsByNumber(pa.getLabProviderNo());

                if (pd.getProviderNo() != null && !pd.getProviderNo().trim().equals("")) {
                    session.setAttribute("treatingProviderDiscTypeId", pd.getDisciplineType().getId());
                }

                if (pd2.getProviderNo() != null && !pd2.getProviderNo().trim().equals("")) {
                    session.setAttribute("labProviderDiscTypeId", pd2.getDisciplineType().getId());
                }

                List<LabTariffDetails> savedLabList = pa.getAuthLabTariffs();
                List<LabTariffDetails> labList = new ArrayList<LabTariffDetails>();
                List<LabTariffDetails> noLabList = new ArrayList<LabTariffDetails>();
                for (LabTariffDetails labs : savedLabList) {
                    if (labs.getLabCodeInd().trim().equals("1")) {
                        labList.add(labs);
                    } else if (labs.getLabCodeInd().trim().equals("0")) {
                        noLabList.add(labs);
                    }
                }
                session.setAttribute("labProvider_text", pa.getLabProviderNo());
                session.setAttribute("SavedDentalLabCodeList", labList);
                session.setAttribute("SavedDentalNoLabCodeList", noLabList);
                session.setAttribute("amountClaimed", pa.getAmountClaimed());

            } else if (authType.trim().equals("3")) {
                AuthOrthoPlanDetails ortho = pa.getOrthoPlanDisplay();

                session.setAttribute("planDuration", ortho.getDuration());
                session.setAttribute("amountClaimed", ortho.getTotalAmount());

                session.setAttribute("depositAmount", ortho.getDeposit());
                session.setAttribute("firstAmount", ortho.getFirstInstallment());
                session.setAttribute("remainAmount", ortho.getRemainingInstallment());

            } else if (authType.trim().equals("4") || authType.trim().equals("11")) {

                //set provider disc
                ProviderSearchDetails pd = service.getNeoManagerBeanPort().findPracticeAjaxDetailsByNumber(pa.getTreatingProviderNo());
                ProviderSearchDetails pd2 = service.getNeoManagerBeanPort().findPracticeAjaxDetailsByNumber(pa.getLabProviderNo());
                ProviderSearchDetails pd3 = service.getNeoManagerBeanPort().findPracticeAjaxDetailsByNumber(pa.getFacilityProviderNo());

                if (pd.getProviderNo() != null && !pd.getProviderNo().trim().equals("")) {
                    session.setAttribute("treatingProviderDiscTypeId", pd.getDisciplineType().getId());
                }

                if (pd2.getProviderNo() != null && !pd2.getProviderNo().trim().equals("")) {
                    session.setAttribute("labProviderDiscTypeId", pd2.getDisciplineType().getId());
                }

                if (pd3.getProviderNo() != null && !pd3.getProviderNo().trim().equals("")) {
                    session.setAttribute("facilityProvDiscTypeId", pd3.getDisciplineType().getId());
                }

                session.setAttribute("awType", pa.getAuthorwiseType());
                session.setAttribute("awTypeNew", pa.getAuthorwiseType());
                session.setAttribute("reimbursementModel", pa.getReinbursementModel());

                session.setAttribute("admissionDateTime", aFrom);
                session.setAttribute("dischargeDateTime", aTo);
                session.setAttribute("los", pa.getNumberOfDays());
                session.setAttribute("AuthLocList", pa.getHospitalLOCDetails());

                session.setAttribute("facilityProv_text", pa.getFacilityProviderNo());
                session.setAttribute("anaesthetistProv_text", pa.getAnaesthetistProviderNo());
                session.setAttribute("secondaryICD_text", pa.getSecondaryICD());
                session.setAttribute("coMorbidityICD_text", pa.getCoMorbidityICD());

                session.setAttribute("pmb", pa.getPmb());
                session.setAttribute("coPay", pa.getCoPaymentAmount());
                session.setAttribute("funder", pa.getFunderDetails());

                session.setAttribute("amountClaimed", pa.getAmountClaimed());
                session.setAttribute("hospStatus", pa.getHospitalStatus());
                session.setAttribute("hospInterim", pa.getAmountClaimed());

                //penalty 
                if (pa.getPenaltyPercentage() != null) {

                    //session.setAttribute("authPenFound", "yes");
                    session.setAttribute("isPenalty", "yes");

                    String penPerc = pa.getPenaltyPercentage();
                    String penReason = pa.getPenaltyOverReason();

                    session.setAttribute("penaltyPerc", penPerc);
                    session.setAttribute("penaltyReason", penReason);

                }
                //dsp  copay
                if (pa.getDspCopayPercentage() != null) {

                    session.setAttribute("isDSPPenalty", "yes");

                    String dspPerc = pa.getDspCopayPercentage();
                    String dspPenReason = pa.getDspCopayOverReason();

                    session.setAttribute("dspPenaltyPerc", dspPerc);
                    session.setAttribute("dspPenaltyReason", dspPenReason);
                }

            } else if (authType.trim().equals("5")) {
                session.setAttribute("authSub", pa.getAuthSubType());
                

                AuthSpecificBasket asb = pa.getAuthBasketSave();
                List<AuthTariffDetails> tariffs = new ArrayList<AuthTariffDetails>();
                List<AuthTariffDetails> tariffSpec = new ArrayList<AuthTariffDetails>();
                List<AuthTariffDetails> nappies = new ArrayList<AuthTariffDetails>();
                
                if(asb != null){
                    session.setAttribute("planYear", asb.getBasketYear());

                    List<AuthSpecificBasketTariffs> stList = asb.getBasketTariffs();
                    for (AuthSpecificBasketTariffs st : stList) {
                        AuthTariffDetails at = new AuthTariffDetails();

                        at.setProviderType(st.getProviderCategory());
                        at.setTariffCode(st.getCode());
                        at.setTariffType(st.getCodeType());
                        at.setTariffDesc(st.getCodeDesc());
                        at.setAmount(st.getCodeAmount());
                        at.setFrequency(st.getFrequency());
                        at.setTariffOption(st.getCodeOption());
                        at.setFreqAvailable(st.getFreqAvailable());

                        String codeType = st.getCodeType();
                        if (codeType.equals("2")) {
                            if (st.getSpecificInd() == 1) {
                                tariffSpec.add(at);
                            } else {
                                tariffs.add(at);
                            }
                        } else {
                            nappies.add(at);
                        }
                    }
                }
                //set copayment amount
                session.setAttribute("spmOHCopayValue", pa.getCoPaymentAmount());
                session.setAttribute("AuthBasketTariffs", tariffs);
                if(!tariffs.isEmpty()){
                    session.setAttribute("AuthBasketTariffsSize", tariffs.size());
                }else{
                    session.setAttribute("AuthBasketTariffsSize", "");
                }
                
                session.setAttribute("AuthNappiTariffs", nappies);
                session.setAttribute("AuthSpecificBasketTariffs", tariffSpec);

            } else if (authType.trim().equals("7")) {
                session.setAttribute("benSubType", pa.getAuthSubType());
                session.setAttribute("overrideInd", pa.getOverrideInd());

            } else if (authType.trim().equals("9")) {
                session.setAttribute("coPay", pa.getCoPaymentAmount());
                session.setAttribute("pmb", pa.getPmb());

            } else if (authType.trim().equals("10")) {
                ProviderSearchDetails pd = service.getNeoManagerBeanPort().findPracticeAjaxDetailsByNumber(pa.getTreatingProviderNo());
                if (pd.getProviderNo() != null && !pd.getProviderNo().trim().equals("")) {
                    session.setAttribute("treatingProviderDiscTypeId", pd.getDisciplineType().getId());
                }

            }

        }
        try {
            String nextJSP = "/PreAuth/GenericAuth_Update.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String getAuthICDDesc(NeoManagerBean port, String[] icds, String icd) {
        String icdDesc = "";
        if (icds == null) {
            //return primary
            Diagnosis d = port.getDiagnosisForCode(icd);
            icdDesc += d.getDescription();
        } else {
            //get multiple
            for (String code : icds) {
                Diagnosis d = port.getDiagnosisForCode(code.trim());
                icdDesc += d.getDescription() + ", ";
            }
            if (!icdDesc.equalsIgnoreCase("")) {
                icdDesc = icdDesc.substring(0, icdDesc.lastIndexOf(",") - 1);
            }
        }
        return icdDesc;
    }

    @Override
    public String getName() {
        return "PreAuthUpdateCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class ConsultantMemberSearchCommand extends NeoCommand {


    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            search(request, response, context);
        } catch (Exception ex) {
            Logger.getLogger(ConsultantMemberSearchCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void search(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        String consultantCode = request.getParameter("consultantCode");
        String consultantName = request.getParameter("consultantName");
        System.out.println("consultantCode " + consultantCode);
        System.out.println("consultantName " + consultantName);
        List<KeyValueArray> kva = port.getBrokerConsultantList(consultantCode, consultantName);

        Object col = MapUtils.getMap(kva);
        System.out.println("Size " + kva.size());
        request.setAttribute("ConsultantSearchResults", col);
        context.getRequestDispatcher("/Member/MemberConsultantSearchResults.jsp").forward(request, response);
    }

    @Override
    public String getName() {
        return "ConsultantMemberSearchCommand";
    }
}
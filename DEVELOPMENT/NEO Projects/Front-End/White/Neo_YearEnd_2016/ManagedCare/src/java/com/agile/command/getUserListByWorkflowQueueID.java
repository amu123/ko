/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.LookupUtils;
import com.koh.utils.XmlUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author gerritr
 */
public class getUserListByWorkflowQueueID extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        System.out.println("in getUserListByWorkflowQueueID");
        PrintWriter out = null;
        String xmlStr = "";
        
        if (request.getParameter("queueID") != null && !request.getParameter("queueID").isEmpty()) {
            Integer queueID = Integer.parseInt(request.getParameter("queueID"));
            List<LookupValue> lvList = port.getWorkflowUserOrQueueLookup(null, queueID, true);
            response.setHeader("Cache-Control", "no-cache");
            response.setContentType("text/xml");
            response.setStatus(200);

            @SuppressWarnings("static-access")
            Document doc = new XmlUtils().getDocument();

            Element root = doc.createElement("UserList");
            doc.appendChild(root);


            for (LookupValue lv : lvList) {
                Element option = doc.createElement("Users");

                String userIdName = lv.getId() + "|" + lv.getValue();
                option.setTextContent(userIdName);

                root.appendChild(option);

            }


            try {
                out = response.getWriter();

                try {
                    xmlStr = XmlUtils.getXMLString(doc);
                } catch (IOException ex) {
                    ex.printStackTrace();
                    xmlStr = "";

                }
                //System.out.println("product option xml = "+xmlStr);
                out.println(xmlStr);


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "getUserListByWorkflowQueueID";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.employer.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.EmployerGroup;
import neo.manager.MemberEmployer;
import neo.manager.NeoManagerBean;
import net.sf.json.JSONArray;
import org.apache.log4j.Logger;

/**
 *
 * @author yuganp
 */
public class EmployerListCommand extends NeoCommand{
    
    private static final Logger loggerEmpList = Logger.getLogger(EmployerListCommand.class);
    
    private PrintWriter out = null;
    
    public EmployerListCommand() {
        super();
    
    
    }

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        loggerEmpList.info("EmployerListCommand reached");
        
        HttpSession session = request.getSession();
        
        NeoManagerBean port = service.getNeoManagerBeanPort();
        
        try {
            
            out = response.getWriter();
              
            //MEMBER ENTITY ID
            int entityId = (Integer) session.getAttribute("memberCoverEntityId");
            loggerEmpList.debug("entityId : " + entityId);
            
            List<EmployerGroup> employerList = port.getEmployerGroupList();
            loggerEmpList.debug("Total employees returned : " + employerList.size());
         
            //LIST OF MEMBER BROKER LINKS
            List<MemberEmployer> memberEmployers = port.findMemberEmployerByMemberEntityId(entityId);
            loggerEmpList.debug("Total memberEmployer returned for memberEntityId " + entityId + "  :  " + memberEmployers.size());
         
            if (memberEmployers.isEmpty()) {
                loggerEmpList.error("Cannot find employer for member");
                return null;
            } 
            
            EmployerGroup employerForMember = null;
            
            //LOOP THROUGH ALL EMPLOYER
            for (EmployerGroup employer : employerList) {
                
                //LOOP THROUGH MEMBER EMPLOYER 
                for (MemberEmployer membEmpl : memberEmployers) {
                    
                    loggerEmpList.debug("Comparing : " + membEmpl.getEmployerEntityId() + " to " + employer.getEntityId());
                    if (membEmpl.getEmployerEntityId()==employer.getEntityId()) {
                        employerForMember = employer;
                        break;
                    } 
                }
                
                //BREAK WHEN FOUND
                if (employerForMember!=null) break;
            }
            
            if (employerForMember==null) loggerEmpList.error("Cannot Link employer to member");
            
            
            
            //LIST TO SEND BACK TO FRONTEND
            List<EmployerLookup> employerLookup = new ArrayList<EmployerLookup>();
           
            for (EmployerGroup empl : employerList) {

                if (empl.equals(employerForMember)) {
                    employerLookup.add(new EmployerLookup(empl.getEntityId(), empl.getEmployerName(),true));
                } else {
                    employerLookup.add(new EmployerLookup(empl.getEntityId(), empl.getEmployerName()));
                }

            }
            
            JSONArray array = new JSONArray();
            array.addAll(employerLookup);
            
            loggerEmpList.debug("JSON ARRAY : " + array.toString());
            out.println(array.toString());
            
        } catch (Exception ex) {
            loggerEmpList.error(ex);
        } finally {
            out.close();
        }
                
        return null;
    }

    @Override
    public String getName() {
        return "EmployerListCommand";
    }

}

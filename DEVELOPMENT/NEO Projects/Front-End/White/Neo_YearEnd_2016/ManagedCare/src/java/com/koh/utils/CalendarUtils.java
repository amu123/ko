/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author nick
 */
public class CalendarUtils {
    
    public List<String> getCurrentPreviousAndNextYear(){
        
        List<String> years = new ArrayList<String>();
        
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        
        years.add(""+(year-1));
        years.add(""+year);
        years.add(""+(year+1));
        
        return years;
    }
    
    public List<String> getFirstSixMonthsOfYear(){
        
        List<String> months = new ArrayList<String>();
        
        Calendar c = Calendar.getInstance();
        
        for(int i = 0; i < 6; i++){
            months.add(c.getDisplayName(Calendar.MONTH+i, Calendar.LONG, Locale.ENGLISH));
        }
        
        return months;
    }
    
    public List<String> getLastSixMonthsOfYear(){
        
        List<String> months = new ArrayList<String>();
        
        Calendar c = Calendar.getInstance();
        
        for(int i = 6; i < 12; i++){
            months.add(c.getDisplayName(Calendar.MONTH+i, Calendar.LONG, Locale.ENGLISH));
        }
        
        return months;
    }
    
    public List<String> getAllMonthsOfYear(){
        
        List<String> months = new ArrayList<String>();
        
        Calendar c = Calendar.getInstance();
        
        for(int i = 0; i < 12; i++){
            months.add(c.getDisplayName((Calendar.MONTH*0)+i, Calendar.LONG, Locale.ENGLISH));
        }
        
        return months;
    }
    
}

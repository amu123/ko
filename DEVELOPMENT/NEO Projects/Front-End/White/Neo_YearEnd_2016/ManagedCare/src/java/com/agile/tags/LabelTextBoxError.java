/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class LabelTextBoxError extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String elementName = null;
    private String displayName = null;
    private String mandatory = "no";
    private String numericValidation = "no";
    private String valueFromRequest = "";
    private String valueFromSession = "No";
    private String javaScript = "";
    private String enabled = "yes";
    private String readonly = "no";

    public void setReadonly(String readonly) {
        this.readonly = readonly;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setValueFromSession(String valueFromSession) {
        this.valueFromSession = valueFromSession;
    }

    public void setValueFromRequest(String valueFromRequest) {
        this.valueFromRequest = valueFromRequest;
    }

    public void setNumericValidation(String numericValidation) {
        this.numericValidation = numericValidation;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        try {
            String validations = "";
            String sessionVal = "";
            String sessionValerror = "";
            if (valueFromSession.equalsIgnoreCase("Yes")) {
                if (session.getAttribute(elementName) != null) {
                    sessionVal = "" + session.getAttribute(elementName);
                }
                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                }
                if (enabled.equalsIgnoreCase("no")) {
                    out.println("<td align=\"left\" width=\"160px\"><label id=\"" + elementName + "_label\">" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"" + sessionVal + "\" size=\"30\" " + javaScript + " disabled/></td>");
                } else if (readonly.equalsIgnoreCase("yes")) {

                    out.println("<td align=\"left\" width=\"160px\"><label id=\"" + elementName + "_label\">" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"" + sessionVal + "\" size=\"30\" " + javaScript + " readonly/></td>");
                } else {
                    out.println("<td align=\"left\" width=\"160px\"><label id=\"" + elementName + "_label\">" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"" + sessionVal + "\" size=\"30\" " + javaScript + "/></td>");
                }

            } else if (!valueFromRequest.equalsIgnoreCase("")) {
                out.println("<td align=\"left\" width=\"160px\"><label id=\"" + elementName + "_label\">" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"" + req.getAttribute(valueFromRequest) + "\" size=\"30\" " + javaScript + "/></td>");
            } else {
                out.println("<td align=\"left\" width=\"160px\"><label id=\"" + elementName + "_label\">" + displayName + ":</label></td><td align=\"left\" width=\"200px\"><input type=\"text\" id=\"" + elementName + "\" name=\"" + elementName + "\" value=\"\" size=\"30\" " + javaScript + "/></td>");
            }
            if (mandatory.equalsIgnoreCase("yes")) {
                out.print("<td><label class=\"red\">*</label></td>");
                validations += "ret = check_mandatory(thisForm, '" + elementName + "', '" + elementName + "_error');if(ret != true){toRet = false;} ";
            } else {
                out.print("<td></td>");
            }

            if (numericValidation.equalsIgnoreCase("yes")) {
                validations += "ret = isNumeric(thisForm, '" + elementName + "', '" + elementName + "_error');if(ret != true){toRet = false;} ";
            }
            if (valueFromSession.equalsIgnoreCase("Yes") && sessionValerror != null && !sessionValerror.equalsIgnoreCase("null")) {
                out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
            } else {
                out.println("<td></td><td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
            }

            String att = (String) pageContext.getAttribute("validate");
            if (att != null) {
                pageContext.setAttribute("validate", att + "" + validations);
            } else {
                pageContext.setAttribute("validate", "" + validations);
            }

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelTextBoxError tag", ex);
        }
        return super.doEndTag();
    }
}

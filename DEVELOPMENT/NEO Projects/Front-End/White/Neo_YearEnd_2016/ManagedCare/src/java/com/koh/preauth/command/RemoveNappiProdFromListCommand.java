/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthNappiProduct;

/**
 *
 * @author martins
 */
public class RemoveNappiProdFromListCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        
        String onScreen = "" + session.getAttribute("onScreen");
        String nextJSP = "/PreAuth/AuthNappiProductDetails.jsp";
        
        List<AuthNappiProduct> list = (List<AuthNappiProduct>) session.getAttribute("productNappiList");
        int removeNum = Integer.parseInt(request.getParameter("remove"));
        
        list.remove(removeNum -1);
        
        session.setAttribute("productNappiList", list);
        
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "RemoveNappiProdFromListCommand";
    }
}

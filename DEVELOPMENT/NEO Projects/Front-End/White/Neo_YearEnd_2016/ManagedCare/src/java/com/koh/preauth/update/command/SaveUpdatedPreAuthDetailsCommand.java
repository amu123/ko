/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.update.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.preauth.command.GetAuthConfirmationDetails;
import com.koh.preauth.utils.KPIntegration;
import java.util.logging.Level;
import java.util.logging.Logger;
import neo.manager.LabTariffDetails;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthCPTDetails;
import neo.manager.AuthHospitalLOC;
import neo.manager.AuthNappiProduct;
import neo.manager.AuthNoteDetail;
import neo.manager.AuthPMBDetails;
import neo.manager.AuthSpecificBasket;
import neo.manager.AuthSpecificBasketTariffs;
import neo.manager.AuthTariffDetails;
import neo.manager.CoverDetails;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.OrthodonticPlanDetails;
import neo.manager.PreAuthDetails;
import neo.manager.PreAuthSearchCriteria;

//import neo.manager.AuthHistoryDetails;
/**
 *
 * @author gerritj
 */
public class SaveUpdatedPreAuthDetailsCommand extends NeoCommand {

    private String commandName;
    private String javascript;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        PreAuthDetails pa = new PreAuthDetails();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        pa.setUserId(user.getUserId());
        //Generic Fields
        String schemeName = "" + session.getAttribute("schemeName");
        String schemeOptionName = "" + session.getAttribute("schemeOptionName");

        String authType = "" + session.getAttribute("authType");
        System.out.println("Auth Type = " + authType);
        String client = context.getAttribute("Client").toString();

        XMLGregorianCalendar authDate = null;
        XMLGregorianCalendar authFromDate = null;
        XMLGregorianCalendar authToDate = null;
        try {
            authDate = DateTimeUtils.convertDateToXMLGregorianCalendar(sdf.parse("" + session.getAttribute("authDate")));
            if (!authType.equalsIgnoreCase("4") && !authType.equalsIgnoreCase("11")) {
                authFromDate = DateTimeUtils.convertDateToXMLGregorianCalendar(sdf.parse("" + session.getAttribute("authFromDate")));
                authToDate = DateTimeUtils.convertDateToXMLGregorianCalendar(sdf.parse("" + session.getAttribute("authToDate")));
            }

        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        String memberNo = "" + session.getAttribute("memberNum_text");
        int depCode = Integer.parseInt("" + session.getAttribute("depListValues"));
        String treatProv = "" + session.getAttribute("treatingProvider_text");
        String referProv = "" + session.getAttribute("referringProvider_text");

        String pICD = "" + session.getAttribute("primaryICD_text");
        String aICD = "" + session.getAttribute("admissionICD_text");

        String reqName = "" + session.getAttribute("requestorName");
        String reqRelationship = "" + session.getAttribute("requestorRelationship");
        String reqReason = "" + session.getAttribute("requestorReason");
        String reqContact = "" + session.getAttribute("requestorContact");

        //String notes = "" + session.getAttribute("notes"); old notes
        //new notes
        List<AuthNoteDetail> noteList = (List<AuthNoteDetail>) session.getAttribute("authNoteList");
        if (noteList == null) {
            noteList = new ArrayList<AuthNoteDetail>();
        }

        String authStatus = "" + session.getAttribute("authStatus");
        if (authStatus.equalsIgnoreCase("null") || authStatus.trim().equalsIgnoreCase("")
                || authStatus.trim().equalsIgnoreCase("99")) {
            authStatus = "3";
        }

        if (authType.trim().equalsIgnoreCase("4") || authType.trim().equalsIgnoreCase("7")) {
            List<AuthNappiProduct> productList = (List<AuthNappiProduct>) session.getAttribute("productNappiList");
            if (productList != null) {
                pa.getProductList().addAll(productList);
            }

        }

        //check for auth type not Hospice and not Hospital AND auth status 2
        if (!authType.equalsIgnoreCase("4") && !authType.equalsIgnoreCase("11")) {
            if (authStatus.equalsIgnoreCase("2")) {
                String rejectReason = "" + request.getParameter("authRejReason");
                pa.setHospitalStatus(rejectReason);
            }
        }

        //benefit allocated
        if (session.getAttribute("benAllocated") != null) {
            LookupValue benAllocated = (LookupValue) session.getAttribute("benAllocated");
            String benCode = benAllocated.getId();
            pa.setBenefitCode(benCode);
        }

        //get update fields
        String updateAuthNumber = "" + session.getAttribute("updateAuthNumber");
        int updateAuthId = new Integer("" + session.getAttribute("updateAuthID"));

        //set generic fields
        pa.setAuthType(authType);
        pa.setAuthorisationDate(authDate);
        pa.setCoverNumber(memberNo);
        pa.setDependantCode(depCode);
        pa.setTreatingProviderNo(treatProv);
        pa.setReferringProviderNo(referProv);
        pa.setAdmissionICD(aICD);
        pa.setPrimaryICD(pICD);
        pa.setRequestorName(reqName);
        pa.setRequestorRelationship(reqRelationship);
        pa.setRequestorReason(reqReason);
        pa.setRequestorContact(reqContact);
        pa.setAuthStartDate(authFromDate);
        pa.setAuthEndDate(authToDate);
        //pa.setNotes(notes);
        pa.getAuthNotes().addAll(noteList);
        pa.setAuthStatus(authStatus);

        List<AuthTariffDetails> tariffList = null;
        if (!authType.trim().equalsIgnoreCase("4") && !authType.trim().equalsIgnoreCase("5") && !authType.trim().equalsIgnoreCase("11")) {
            if (authType.trim().equalsIgnoreCase("6")) {
                tariffList = (List<AuthTariffDetails>) session.getAttribute("newNappiListArray");
            } else {
                tariffList = (List<AuthTariffDetails>) session.getAttribute("tariffListArray");
            }
            pa.getAuthTariffs().addAll(tariffList);
        }

        //set neo awType for non hospital auths
        String awType = "";
        if (!authType.trim().equals("4") && !authType.trim().equals("9") && !authType.trim().equals("11")) {
            pa.setAuthorwiseType("NEO");
        } else if (authType.trim().equals("9")) {
            pa.setAuthorwiseType("BBX");
        } else if (authType.trim().equals("11")) {
            pa.setAuthorwiseType("BBC");
            awType = "BBC";
        } else if (authType.trim().equals("4")) {
            //check for old auth
            boolean newAuthInd = (Boolean) session.getAttribute("newAuthIndicator");
            if (newAuthInd) {
                awType = "" + session.getAttribute("awTypeNew");
            } else {
                awType = "" + session.getAttribute("awType");
            }

            pa.setAuthorwiseType(awType);

        }

        if (!schemeName.equalsIgnoreCase("Resolution Health")) {
            //penalty 
            if (session.getAttribute("penaltyPerc") != null) {

                String pp = "" + session.getAttribute("penaltyPerc");
                String pr = "" + session.getAttribute("penaltyReason");

                if (!pp.equalsIgnoreCase("99") && !pr.equalsIgnoreCase("99")) {

                    pa.setPenaltyPercentage(pp);
                    pa.setPenaltyOverReason(pr);

                }

            }

            //dsp copay
            if (session.getAttribute("dspPenaltyPerc") != null) {

                String dspp = "" + session.getAttribute("dspPenaltyPerc");
                String dspr = "" + session.getAttribute("dspPenaltyReason");

                if (!dspp.equalsIgnoreCase("99") && !dspr.equalsIgnoreCase("99")) {

                    pa.setDspCopayPercentage(dspp);
                    pa.setDspCopayOverReason(dspr);

                }

            }
        }

        //Set Auth Specific Fields
        List<LabTariffDetails> labList = null;
        List<AuthCPTDetails> cptList = null;
        List<AuthHospitalLOC> locList = null;
        List<LabTariffDetails> noLabList = null;

        //set copayment
        if (authType.trim().equalsIgnoreCase("4") || authType.trim().equalsIgnoreCase("11") || authType.trim().equalsIgnoreCase("9")) {
            //set copayment amount
            double coPayAmount = 0.0d;
            String sesCpVal = "" + session.getAttribute("coPay");
            if (sesCpVal != null && !sesCpVal.trim().equals("")) {
                try {
                    coPayAmount = new Double(sesCpVal);
                } catch (Exception ex) {
                    System.out.println("copay amount exception : " + ex.getMessage());
                    coPayAmount = 0.0d;
                }
            }
            pa.setCoPaymentAmount(coPayAmount);
            if (coPayAmount > 0.0d) {
                pa.setCoPayment("1");
            } else {
                pa.setCoPayment("0");
            }
        }

        if (authType.trim().equalsIgnoreCase("5")) {
            //set copayment amount
            double coPayAmount = 0.0d;
            String sesCpVal = "" + session.getAttribute("spmOHCopayValue");
            if (sesCpVal != null && !sesCpVal.trim().equals("")) {
                try {
                    coPayAmount = new Double(sesCpVal);
                } catch (Exception ex) {
                    System.out.println("spmOHCopayValue amount exception : " + ex.getMessage());
                    coPayAmount = 0.0d;
                }
            }
            pa.setCoPaymentAmount(coPayAmount);
            if (coPayAmount > 0.0d) {
                pa.setCoPayment("1");
            } else {
                pa.setCoPayment("0");
            }
        }

        if (authType.trim().equalsIgnoreCase("1")) {
            //Optical Details
            double numberOfDays = 0.0d;
            String numDays = "" + session.getAttribute("numDays");
            if (numDays != null && !numDays.trim().equalsIgnoreCase("")) {
                numberOfDays = new Double(numDays);
            }

            pa.setFacilityProviderNo("" + session.getAttribute("facilityProv_text"));
            pa.setSecondaryICD("" + session.getAttribute("secondaryICD_text"));
            pa.setCoMorbidityICD("" + session.getAttribute("coMorbidityICD_text"));
            pa.setNumberOfDays(numberOfDays);
            pa.setPreviousLensRx("" + session.getAttribute("prevLens"));
            pa.setCurrentLensRx("" + session.getAttribute("curLens"));

        } else if (authType.trim().equalsIgnoreCase("2") || authType.trim().equalsIgnoreCase("13")) {
            //Dental Details
            pa.setAuthSubType("" + session.getAttribute("DentalSubType"));
            pa.setLabProviderNo("" + session.getAttribute("labProvider_text"));
            labList = (List<LabTariffDetails>) session.getAttribute("SavedDentalLabCodeList");
            noLabList = (List<LabTariffDetails>) session.getAttribute("SavedDentalNoLabCodeList");
            if (labList == null) {
                labList = new ArrayList<LabTariffDetails>();
            }
            if (noLabList != null) {
                labList.addAll(noLabList);
            }
            double ac = 0.0d;
            String amount = "" + session.getAttribute("amountClaimed");
            if (amount != null && !amount.trim().equalsIgnoreCase("")
                    && !amount.trim().equalsIgnoreCase("null")) {
                ac = new Double(ac);
            }
            pa.setAmountClaimed(ac);
            pa.getAuthLabTariffs().addAll(labList);

        } else if (authType.trim().equalsIgnoreCase("3")) {
            //Save orthodontic plan details
            OrthodonticPlanDetails oplan = new OrthodonticPlanDetails();
            oplan.setAuthDate(authDate);
            oplan.setCoverNumber(memberNo);
            oplan.setDependentNumber(depCode);
            oplan.setProviderNumber(treatProv);
            //get tariff code
            String authTariff = tariffList.get(0).getTariffCode();
            oplan.setTariffCode(authTariff);
            oplan.setDuration(new Integer("" + session.getAttribute("planDuration")).intValue());
            oplan.setTotalAmount(new Double("" + session.getAttribute("amountClaimed")).doubleValue());
            oplan.setDeposit(new Double("" + session.getAttribute("depositAmount")).doubleValue());
            oplan.setFirstInstallment(new Double("" + session.getAttribute("firstAmount")).doubleValue());
            oplan.setMonthlyInstallment(new Double("" + session.getAttribute("remainAmount")).doubleValue());
            oplan.setPlanStartDate(authFromDate);
            oplan.setPlanEstimatedEndDate(authToDate);

            pa.setOrthoPlanSave(oplan);

        } else if (authType.trim().equalsIgnoreCase("4") || authType.trim().equals("11")) {
            //Hospital Details
            locList = (List<AuthHospitalLOC>) session.getAttribute("AuthLocList");

            SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            try {
                Date dateAdd = sdfTime.parse("" + session.getAttribute("admissionDateTime"));
                Date dateDis = sdfTime.parse("" + session.getAttribute("dischargeDateTime"));
                authFromDate = DateTimeUtils.convertDateToXMLGregorianCalendar(dateAdd);
                authToDate = DateTimeUtils.convertDateToXMLGregorianCalendar(dateDis);

            } catch (ParseException ex) {
                Logger.getLogger(SaveUpdatedPreAuthDetailsCommand.class.getName()).log(Level.SEVERE, null, ex);
            }
            pa.setAuthStartDate(authFromDate);
            pa.setAuthEndDate(authToDate);

            //Provider
            pa.setFacilityProviderNo("" + session.getAttribute("facilityProv_text"));
            pa.setAnaesthetistProviderNo("" + session.getAttribute("anaesthetistProv_text"));
            pa.setReinbursementModel("" + session.getAttribute("reimbursementModel"));

            pa.setSecondaryICD("" + session.getAttribute("secondaryICD_text"));
            pa.setCoMorbidityICD("" + session.getAttribute("coMorbidityICD_text"));

            try {
                List<AuthPMBDetails> savedPMBList = (List<AuthPMBDetails>) session.getAttribute("savedAuthPMBs");

                if (savedPMBList != null && !savedPMBList.isEmpty()) {
                    pa.getAuthPMBDetails().addAll(savedPMBList);
                    pa.setPmb("1");
                } else {
                    pa.setPmb("0");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            //set copayment amount
            double coPayAmount = 0.0d;
            String sesCpVal = "" + session.getAttribute("coPay");
            if (sesCpVal != null && !sesCpVal.trim().equals("")) {
                try {
                    coPayAmount = new Double(sesCpVal);
                } catch (Exception ex) {
                    System.out.println("copay amount exception : " + ex.getMessage());
                    coPayAmount = 0.0d;
                }
            }
            pa.setCoPaymentAmount(coPayAmount);
            if (coPayAmount > 0.0d) {
                pa.setCoPayment("1");
            } else {
                pa.setCoPayment("0");
            }
            pa.setNumberOfDays(new Double("" + session.getAttribute("los")));
            pa.setFunderDetails("" + session.getAttribute("funder"));

            double hospInterim = 0.0d;
            double estimatedCost = 0.0d;
            double amountClaimed = 0.0d;

            String hospAmount = "" + session.getAttribute("hospInterim");
            String cAmount = "" + session.getAttribute("amountClaimed");

            if (hospAmount != null && !hospAmount.equalsIgnoreCase("null") && !hospAmount.equalsIgnoreCase("")) {
                hospInterim = new Double(hospAmount);
            }
            if (cAmount != null && !cAmount.equalsIgnoreCase("null") && !cAmount.equalsIgnoreCase("")) {
                estimatedCost = new Double(cAmount);
            }

            if (hospInterim != 0.0d) {
                System.out.println("save hosp interim = " + hospInterim);
                System.out.println("save estimated = " + estimatedCost);
                amountClaimed = hospInterim;

            } else if (hospInterim > estimatedCost) {
                amountClaimed = hospInterim;

            } else {
                amountClaimed = estimatedCost;
            }
            pa.setAmountClaimed(amountClaimed);

            //set collection
            pa.getHospitalLOCDetails().addAll(locList);
            //SET TARIFFS
            tariffList = (List<AuthTariffDetails>) session.getAttribute("tariffListArray");
            pa.getAuthTariffs().addAll(tariffList);
            //SET CPT's
            cptList = (List<AuthCPTDetails>) session.getAttribute("authCPTListDetails");
            pa.getAuthCPTDetails().addAll(cptList);

            //TODO
            //set hospital update values
            pa.setHospitalStatus("" + session.getAttribute("hospStatus"));

        } else if (authType.trim().equalsIgnoreCase("5")) {
            
            pa.setAuthSubType("" + session.getAttribute("authSub"));
            
            //Condition Specific Details
            AuthSpecificBasket specificBasket = new AuthSpecificBasket();
            Collection<AuthSpecificBasketTariffs> nappiBTList = new ArrayList<AuthSpecificBasketTariffs>();
            Collection<AuthSpecificBasketTariffs> nhrplBTList = new ArrayList<AuthSpecificBasketTariffs>();

            specificBasket.setAuthDate(authDate);
            specificBasket.setBasketStartDate(authFromDate);
            specificBasket.setBasketEndDate(authToDate);
            specificBasket.setCoverNumber(memberNo);
            specificBasket.setDependantNumber(depCode);
            specificBasket.setBasketYear("" + session.getAttribute("planYear"));
            specificBasket.setModifiedBasket("" + session.getAttribute("ModifiedBasketID"));

            List<AuthTariffDetails> tariffs = (List<AuthTariffDetails>) session.getAttribute("AuthBasketTariffs");
            List<AuthTariffDetails> nappies = (List<AuthTariffDetails>) session.getAttribute("AuthNappiTariffs");
            System.out.println("The size of the nappie from SaveUpdatedPreAuthDetailsCommand " + nappies.size());
            if (tariffs != null && tariffs.size() > 0) {
                for (AuthTariffDetails at : tariffs) {
                    AuthSpecificBasketTariffs nhrpl = new AuthSpecificBasketTariffs();

                    nhrpl.setCode(at.getTariffCode());
                    nhrpl.setCodeType("2");
                    nhrpl.setCodeAmount(at.getAmount());
                    nhrpl.setDosage(at.getDosage());
                    nhrpl.setFrequency(at.getFrequency());
                    nhrpl.setProviderCategory(at.getProviderType());
                    nhrpl.setCoPay(at.getCoPayment());
                    System.out.println("The quantity value tarrif" + at.getQuantity());
                    //nhrpl.setQuantity(new Integer(at.getQuantity()));

                    nhrplBTList.add(nhrpl);
                }
            }

            if (nappies != null && nappies.size() > 0) {
                for (AuthTariffDetails at : nappies) {
                    AuthSpecificBasketTariffs nappi = new AuthSpecificBasketTariffs();

                    nappi.setCode(at.getTariffCode());
                    nappi.setCodeType("3");
                    nappi.setCodeAmount(at.getAmount());
                    nappi.setDosage(at.getDosage());
                    nappi.setFrequency(at.getFrequency());
                    nappi.setProviderCategory(at.getProviderType());
                    nappi.setCoPay(at.getCoPayment());
                    System.out.println("The quantity value nappies" + at.getQuantity());
                    //nhrpl.setQuantity(new Integer(at.getQuantity()));

                    nappiBTList.add(nappi);
                }
            }
            if (specificBasket.getBasketTariffs() != null) {
                specificBasket.getBasketTariffs().addAll(nhrplBTList);
                specificBasket.getBasketTariffs().addAll(nappiBTList);
            }
            pa.setAuthBasketSave(specificBasket);

        } else if (authType.trim().equals("7")) {

            String overrideInd = "" + session.getAttribute("overrideInd");
            if (overrideInd == null || overrideInd.equalsIgnoreCase("99")
                    || overrideInd.equalsIgnoreCase("null")) {
                pa.setOverrideInd("0");
            } else {
                pa.setOverrideInd(overrideInd);
            }

            String benefitSubType = "" + session.getAttribute("benSubType");
            pa.setAuthSubType(benefitSubType);

        } else if (authType.trim().equals("9")) {
            List<AuthPMBDetails> savedPMBList = (List<AuthPMBDetails>) session.getAttribute("savedAuthPMBs");
            String pmb = "0";
            if (savedPMBList != null && !savedPMBList.isEmpty()) {
                pa.getAuthPMBDetails().addAll(savedPMBList);
                pmb = "1";
            }
            pa.setPmb(pmb);

        }

        //upfront rejection of resigned dependant
        System.out.println("update member no = " + memberNo);
        System.out.println("update dependant = " + depCode);
        System.out.println("update resigned auth pa.getAuthType() = " + pa.getAuthType());

        CoverDetails cd = new CoverDetails();
        cd = port.getDependentForCoverByDate(memberNo, authFromDate, depCode);
        boolean reject = false;
        if (cd.getEntityId() == 0) {
            System.out.println("rejected entityId = " + cd.getEntityId());
            reject = true;
        } else {
            if (cd.getStatus().trim().equalsIgnoreCase("") || cd.getStatus().trim().equalsIgnoreCase("resign")) {
                reject = true;
            }
        }
        //if (reject) {
        //    pa.setAuthStatus("2");
        //    System.out.println("member "+memberNo+" dependant "+depCode+" auth rejected because member resigned");
        //}

        if (client.equalsIgnoreCase("Sechaba")) {
            pa.setCoPayment(null);
            pa.setCoPaymentAmount(0);
            pa.setPenaltyOverReason(null);
            pa.setPenaltyPercentage(null);
        }

        String authReturn = service.getNeoManagerBeanPort().updatePreAuthDetails(user, updateAuthId, updateAuthNumber, pa);
        PrintWriter out = null;
        boolean saveFailed = false;
        try {
            out = response.getWriter();
            if (reject) {
                //rejection screen
                out.println("<html>");
                out.println("<head>");
                out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                out.println("</head>");
                out.println("<body>");
                out.println("<center>");
                out.println("<table cellspacing=\"4\">");
                out.println("<tr>");
                out.println("<td><label class=\"header\">Authorisation saving failed.</label></td><br/>");
                out.println("<td><label class=\"label\">Member: " + memberNo + "</label></td>");
                out.println("<td><label class=\"label\">Dependant: " + depCode + "</label></td>");
                out.println("<td><label class=\"label\">is not active for the auth period provided. </label></td>");
                out.println("</tr>");
                out.println("</table>");
                out.println("<p> </p>");
                out.println("</center>");
                out.println("</body>");
                out.println("</html>");

                response.setHeader("Refresh", "5; URL=/ManagedCare/PreAuth/GenericAuth_Update.jsp");

            } else {
                if (authReturn != null && !authReturn.trim().equalsIgnoreCase("")) {

                    String[] authReturnValues = authReturn.split("\\|");
                    String authNumber = authReturnValues[0];
                    int authCode = Integer.parseInt(authReturnValues[1]);

                    if (authNumber != null && !authNumber.trim().equalsIgnoreCase("")) {

                        session.setAttribute("authCode", authCode);
                        session.setAttribute("authNumber", authNumber);

                        out.println("<html>");
                        out.println("<head>");
                        out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                        out.println("</head>");
                        out.println("<body>");
                        out.println("<center>");
                        out.println("<table cellspacing=\"4\">");
                        out.println("<tr>");
                        if (authNumber != null && !authNumber.equalsIgnoreCase("")) {
                            out.println("<td><label class=\"header\">Authorisation " + authNumber + " saved. under auth code " + authCode + "</label></td>");

                            //reprocess auth on update
                            if (authType.equals("4")) {
                                boolean reprocessAuth = KPIntegration.reprocessAuth(authNumber, authCode);
                                if (reprocessAuth) {
                                    port.deleteAuthFirings(authCode);
                                    port.removeOverrideReasonFromAuth(authCode);
                                    System.out.println("reprocessing auth - " + authNumber);
                                    port.processAuthorisation(authCode);
                                } else {
                                    System.out.println("update was not send for reprocessing");
                                }
                            }

                        } else {
                            out.println("<td><label class=\"header\">Authorisation saving failed.</label></td>");

                        }
                        out.println("</tr>");
                        out.println("</table>");
                        out.println("<p> </p>");
                        out.println("</center>");
                        out.println("</body>");
                        out.println("</html>");

                        GetAuthConfirmationDetails getConfirmed = new GetAuthConfirmationDetails();
                        getConfirmed.execute(request, response, context);

                    } else {
                        System.out.println("authReturn null");
                        saveFailed = true;
                    }
                } else {
                    System.out.println("authReturn null");
                    saveFailed = true;
                }

                if (saveFailed == true) {
                    out.println("<html>");
                    out.println("<head>");
                    out.println("<link rel=\"stylesheet\" href=\"resources/styles.css\"/>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<center>");
                    out.println("<table cellspacing=\"4\">");
                    out.println("<tr>");
                    out.println("<td><label class=\"header\">Authorisation saving failed.</label></td>");
                    out.println("</tr>");
                    out.println("</table>");
                    out.println("<p> </p>");
                    out.println("</center>");
                    out.println("</body>");
                    out.println("</html>");

                    response.setHeader("Refresh", "1; URL=/ManagedCare/PreAuth/GenericAuth_Update.jsp");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "SaveUpdatedPreAuthDetailsCommand";
    }

    private String getValue(Object obj) {
        String value = "";
        if (obj != null) {
            value = value + obj;
        }

        return value;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

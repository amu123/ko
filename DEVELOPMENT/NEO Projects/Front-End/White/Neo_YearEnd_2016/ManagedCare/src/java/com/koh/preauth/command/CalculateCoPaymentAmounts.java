/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.utils.FormatUtils;
import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.lang.Exception;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author johanl
 */
public class CalculateCoPaymentAmounts extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        //LOC details 
        //admission dates
        Date aDate = null;
        XMLGregorianCalendar xAdmission = null;
        XMLGregorianCalendar previousEnd = null;
        String admission = "" + session.getAttribute("admissionDateTime");
        try {
            aDate = FormatUtils.dateTimeFormat.parse(admission);
            xAdmission = DateTimeUtils.convertDateToXMLGregorianCalendar(aDate);

        } catch (ParseException ex) {
            Logger.getLogger(CalculateCoPaymentAmounts.class.getName()).log(Level.SEVERE, null, ex);
        }

        //skip LOC mapping on update
        String nextJSP = "" + session.getAttribute("onScreen");
        System.out.println("skip LOC onScreen = " + nextJSP);
        if (!nextJSP.equalsIgnoreCase("/PreAuth/HospitalAuthDetails_Update.jsp") 
                && !nextJSP.equalsIgnoreCase("/PreAuth/HospiceDetails_Update.jsp")) {
            //after cptList return call LOC mapping
            List<AuthHospitalLOC> locList = new ArrayList<AuthHospitalLOC>();
            List<AuthLocLosDetails> locLosList = new ArrayList<AuthLocLosDetails>();
            List<AuthCPTDetails> cptList = (List<AuthCPTDetails>) session.getAttribute("authCPTListDetails");
            String facility = "" + session.getAttribute("facilityProvider_text");
            String facilityDisc = "" + session.getAttribute("facilityProvDiscTypeId");
            String prodId = "" + session.getAttribute("scheme");
            String optId = "" + session.getAttribute("schemeOption");
            String pICD = "" + session.getAttribute("primaryICD_text");
            int provDesc = Integer.parseInt(facilityDisc);
            int pID = Integer.parseInt(prodId);
            int oID = Integer.parseInt(optId);
            
            int wardLookup = getWARDLookupID(provDesc);

            //set copayment
            double copay = 0.0d;
            copay = setCopayment(session);
            System.out.println("co payment before return amount = " + copay);
            session.setAttribute("coPay", copay);

            boolean skipLocLosCalc = false;
            skipLocLosCalc = validateLocLocProvDesc(provDesc);
            

            if (skipLocLosCalc) {
                if (cptList != null && !cptList.isEmpty()) {
                    locLosList = port.calculateLOSByCPT(cptList, facilityDisc);
                    locLosList = DateTimeUtils.orderLocLosByType(locLosList);

                    if (locLosList != null && !locLosList.isEmpty()) {

                        System.out.println("locLosList size = " + locLosList.size());

                        double totEstimatedWard = 0.0d;
                        int x = 0;
                        double totalLos = 0.0d;
                        for (AuthLocLosDetails ll : locLosList) {
                            if (previousEnd != null) {
                                xAdmission = previousEnd;
                                aDate = previousEnd.toGregorianCalendar().getTime();
                            }

                            String code = port.getValueFromCodeTableForTableId(wardLookup, ll.getLocType());

                            int los = ll.getLos();
                            totalLos = totalLos + (double) los;
                            double estimatedWardAmount = 0.0d;
                            //load ward tariff if exist
                            if (code != null && !code.equals("")) {
                                TariffCode t = null;
                                t = AddAuthLOSToSessionCommand.getWardTariff(xAdmission, pID, oID, code, facilityDisc, facility, pICD);
                                String bdAmount = FormatUtils.decimalFormat.format(t.getPrice());
                                estimatedWardAmount = Double.valueOf(bdAmount);

                                //multiply ward amount with los
                                double newTAmount = estimatedWardAmount * los;
                                newTAmount = Double.valueOf(FormatUtils.decimalFormat.format(newTAmount));
                                estimatedWardAmount = newTAmount;
                                totEstimatedWard += estimatedWardAmount;

                            }

                            //get end date form los 
                            Date endDate = DateTimeUtils.calculateLOCEndDateFromLOS(aDate, los);
                            previousEnd = DateTimeUtils.convertDateToXMLGregorianCalendar(endDate);


                            //set level of care
                            AuthHospitalLOC loc = new AuthHospitalLOC();
                            loc.setUserId(user.getUserId());

                            loc.setLevelOfCare(ll.getLocType());
                            loc.setLengthOfStay(los);
                            loc.setApprovedLos(los);
                            loc.setDateFrom(xAdmission);
                            loc.setDateTo(previousEnd);
                            loc.setEstimatedCost(estimatedWardAmount);
                            locList.add(loc);

                            x++;
                        }
                        String tarAmount = "" + session.getAttribute("totTariffCost");
                        double totTariff = 0.0d;
                        double totHosp = 0.0d;

                        if (tarAmount != null && !tarAmount.equalsIgnoreCase("")
                                && !tarAmount.equalsIgnoreCase("null")) {
                            totTariff = Double.valueOf(tarAmount);
                        }
                        totHosp = totEstimatedWard;

                        double hospInterim = totTariff + totHosp;
                        String hospIntStr = FormatUtils.decimalFormat.format(hospInterim);

                        session.setAttribute("hospInterim", hospIntStr);
                        session.setAttribute("AuthLocList", locList);
                        session.setAttribute("dischargeDateTime", FormatUtils.dateTimeFormat.format(previousEnd.toGregorianCalendar().getTime()));
                        session.setAttribute("los", totalLos);
                    } else {
                        System.out.println("loc list is null");
                    }
                }
            }
        }

        //always check 72 hours
        //validate penalty
        Calendar admissionCal = Calendar.getInstance();
        Calendar authDateCal = Calendar.getInstance();

        Date authorisationDate = null;
        String authDate = "" + session.getAttribute("authDate");
        try {
            if (authDate.length() == 10) {
                authorisationDate = FormatUtils.dateFormat.parse(authDate);
            } else {
                if (authDate.contains("T")) {
                    authDate = authDate.replace("T", " ");
                }
                if (authDate.contains(".")) {
                    int i = authDate.indexOf(".");
                    authDate = authDate.substring(0, i);
                }
                if (authDate.contains("-")) {
                    authDate = authDate.replaceAll("-", "/");
                }
                System.out.println("AUTH DATE FINAL = " + authDate);
                authorisationDate = FormatUtils.dateTimeFormat.parse(authDate);
            }
        } catch (ParseException ex) {
            Logger.getLogger(CalculateCoPaymentAmounts.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Date admissionDate72 = null;
        try {
            admissionDate72 = FormatUtils.dateTimeNoSecFormat.parse(admission);
        } catch (ParseException ex) {
            Logger.getLogger(SetHospitalLevelOfCare.class.getName()).log(Level.SEVERE, null, ex);
        }

        admissionCal.setTime(admissionDate72);
        authDateCal.setTime(authorisationDate);
        authDateCal.add(Calendar.HOUR_OF_DAY, -72);

        session.setAttribute("isPenalty", null);
        if (authDateCal.equals(admissionCal) || authDateCal.after(admissionCal)) {
            session.setAttribute("isPenalty", "yes");
        } else {
            String dspCoPay = "" + session.getAttribute("isDSPPenalty");
            if (dspCoPay.equalsIgnoreCase("no")) {
                session.setAttribute("isPenalty", null);
            }
            session.setAttribute("authPenFound", null);
        }

        try {
            System.out.println("return hosp nextJSP = " + nextJSP);
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public String getName() {
        return "CalculateCoPaymentAmounts";
    }

    public boolean validateLocLocProvDesc(int provDesc) {
        if (provDesc != 57 && provDesc != 58) {
            return false;
        } else {
            return true;
        }
    }

    public static double setCopayment(HttpSession session) {

        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        double copay = 0.0d;
        List<AuthCPTDetails> cptList = (List<AuthCPTDetails>) session.getAttribute("authCPTListDetails");
        
        String admissionD = (String) session.getAttribute("admissionDateTime");
//        Date admissionDate = null;
        XMLGregorianCalendar authStart = null;
        try {
            if (admissionD != null && !admissionD.equalsIgnoreCase("")) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                Pattern pattern = Pattern.compile("\\s");
                Matcher matcher = pattern.matcher(admissionD);
                boolean found = matcher.find();

                Date tFrom = null;
                XMLGregorianCalendar xTreatFrom = null;

                if (!found) {
                    tFrom = dateFormat.parse(admissionD);
                } else {
                    tFrom = dateTimeFormat.parse(admissionD);
                }
                authStart = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
            }
//            admissionDate = new SimpleDateFormat("yyyy/MM/dd").parse(admissionD);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        //get auth member option id
        int optionId = Integer.parseInt("" + session.getAttribute("schemeOption"));
        System.out.println("CalculateCoPaymentAmounts member option id for copay mapping = " + optionId);

        copay = port.getCoPaymentAmountForCPT(cptList, optionId, authStart);

        System.out.println("co payment amount = " + copay);
        session.setAttribute("coPay_error", null);
        if (copay != 99999.0d && copay != 0.0d) {
            session.setAttribute("coPay_error", null);

        } else if (copay == 99999.0d) {
            session.setAttribute("coPay_error",
                    "*Note: Procedure Excluded Unless PMB");
        }


        double dspCopay = 3675.0d;
        String product = "" + session.getAttribute("scheme");
        String option = "" + session.getAttribute("schemeOption");
        int prod = Integer.parseInt(product);
        int opt = Integer.parseInt(option);
        boolean cptEmergency = false;
        if (session.getAttribute("cptEmergencyCase") != null) {
            cptEmergency = (Boolean) session.getAttribute("cptEmergencyCase");
        }

        if ((option.equals("5") || option.equals("7")) && !cptEmergency) {
            //check provider 
            String facilityProv = "" + session.getAttribute("facilityProv_text");;
            boolean noProvSet = false;
            boolean isDSP = false;

            if (facilityProv == null || facilityProv.equalsIgnoreCase("null")
                    || facilityProv.equalsIgnoreCase("")) {
                noProvSet = true;
            }
            System.out.println("facilityProv = " + facilityProv);
            System.out.println("noProvSet = " + noProvSet);
            
            String facilityProvNetwork = "" + session.getAttribute("facilityProvNetwork");
            boolean provincialHosp = false;
            if(facilityProvNetwork.equalsIgnoreCase("Provincial Hospitals L1") 
                    || facilityProvNetwork.equalsIgnoreCase("Provincial Hospitals L2") 
                    || facilityProvNetwork.equalsIgnoreCase("Provincial Hospitals L3")){
                provincialHosp = true;
            }
            
            if (!noProvSet && !provincialHosp) {

                isDSP = port.isDSPNetwork(facilityProv, authStart, prod, opt);
                if (copay != 0.0d && copay != 99999.0d && !isDSP) {
                    session.setAttribute("coPay_error",
                            "*Note: Normal Copay of R" + DateTimeUtils.decimalFormat.format(copay) + " "
                            + "applies with an addtional R3675 for Non-DSP");
                    copay += dspCopay;

                } else if (copay == 0.0d && !isDSP) {
                    session.setAttribute("coPay_error",
                            "*Note: An addtional R3675 for Non-DSP Applies on Facility " + facilityProv);
                    copay += dspCopay;

                } else if (copay == 99999.0d && !isDSP) {
                    session.setAttribute("coPay_error",
                            "*Note: Procedure Excluded Unless PMB R" + DateTimeUtils.decimalFormat.format(copay) + " "
                            + "applies with an addtional R3675 for Non-DSP");
                    copay += dspCopay;
                }
                if (!isDSP) {
                    session.setAttribute("dspCopayApplies", "yes");
                }
            } else {
                session.setAttribute("dspCopayApplies", "no");
            }
        } else {
            if (copay == 99999.0d) {
                session.setAttribute("coPay_error",
                        "*Note: Procedure Excluded Unless PMB R" + DateTimeUtils.decimalFormat.format(copay) + " ");
            }
            session.setAttribute("dspCopayApplies", "no");

        }

        return copay;
    }
    
    public int getWARDLookupID(int providerType) {
        int wardId = 280;
        if (providerType == 79) {
            wardId = 281;

        } else if (providerType == 55) {
            wardId = 265;

        } else if (providerType == 59) {
            wardId = 266;

        } else if (providerType == 49) {
            wardId = 267;

        } else if (providerType == 76) {
            wardId = 268;

        } else if (providerType == 47) {
            wardId = 269;

        } else if (providerType == 56) {
            wardId = 270;

        } else if (providerType == 77) {
            wardId = 271;

        }
        return wardId;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.calltrack.command;

import com.koh.command.NeoCommand;
import com.koh.utils.NeoCoverDetailsDependantFilter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverSearchCriteria;
import neo.manager.EAuthCoverDetails;

/**
 *
 * @author josephm
 */
public class MemberDetailsCoverSearchCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
       // System.out.println("Inside MemberDetailsCoverSearchCommand");
        HttpSession session = request.getSession();
        String memberNumber = request.getParameter("memberNumber_text");
        String productId = request.getParameter("productId");
        this.saveScreenToSession(request);
        
        
        String exactFlag = request.getParameter("exactCoverNum");
        String exactFlag2 = null;
        
        if(exactFlag == null  || exactFlag.equals("")){
            exactFlag2 = (String) session.getAttribute("exactCoverNum");  // From LogNewCall.jsp
        }
        //System.out.println(">>>> Exact Cover Number : " + exactFlag);

        //Collection<CoverDetails> details = new ArrayList<CoverDetails>();
        List<EAuthCoverDetails> details = new ArrayList<EAuthCoverDetails>();
        try{
            CoverSearchCriteria search = new CoverSearchCriteria();
            if((exactFlag != null && exactFlag.equals("1")) || 
                    (exactFlag2 != null && exactFlag2.equals("1"))){
                search.setCoverNumberExact(memberNumber);
            } else {
                search.setCoverNumber(memberNumber);
            }
            details = service.getNeoManagerBeanPort().eAuthCoverSearch(search);

            if(details != null && details.size() > 0) {
                //filter dependants of members to only display one dependant record for each cover dependant
                NeoCoverDetailsDependantFilter filter = new NeoCoverDetailsDependantFilter();
                details = filter.EAuthCoverDetailsDependantFilter(details);
                
                request.setAttribute("memberCoverDetails", details);
                session.setAttribute("memberNumber_text", memberNumber);
                session.setAttribute("product", productId);
            
            }
            else {
                //out.print("Error|No such member|" + getName());
                //session.setAttribute("memberNumber_text", "");
                session.removeAttribute("memberNumber_text");
                session.removeAttribute("product");
            }
            session.removeAttribute("searchCalled");

        }catch(Exception ex) {
            ex.printStackTrace();
        }
      
        try {

            String nextJSP = "/Calltrack/LogNewCall.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
            //session.removeAttribute("memberNumber_text");

        }catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "MemberDetailsCoverSearchCommand";
    }
}

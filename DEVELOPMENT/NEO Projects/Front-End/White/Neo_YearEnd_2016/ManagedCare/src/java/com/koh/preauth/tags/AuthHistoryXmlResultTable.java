/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.preauth.tags;

import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
//import neo.manager.AuthHistoryDetails;

/**
 *
 * @author johanl
 */
public class AuthHistoryXmlResultTable extends TagSupport {
    private String commandName;
    private String javascript;

    /**
     * Called by the container to invoke this tag.
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */

    @Override
    public int doEndTag() throws JspException {
        System.out.println(" in tag result table");
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        System.out.println("XML Result Table entered");
        try {
            out.println("<table width=\"500\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");

//            AuthHistoryDetails history = new AuthHistoryDetails();
//            history = (AuthHistoryDetails) session.getAttribute("fullAuthHistory");
//            if (history != null) {
//                System.out.println("history xml result size = " + history.getXmlOutput());
//                //for (AuthHistoryDetails history : history) {
//
//                    out.println("<tr><th colspan=\"4\">Modified By: " + history.getCreatedBy() + "</th></tr>");
//                    out.println("<tr><th colspan=\"4\">Modified Date: " + history.getCreationDate() + "</th></tr>");
//                    out.println("<tr>" +
//                                    "<form>" +
//                                    "<td> " +
//                                        "<label class=\"label\">" + history.getHistoryId() + "</label>" +
//                                        "<input type=\"hidden\" name=\"historyId\" value=\"" + history.getHistoryId() + "\"/>" +
//                                    "</td> " +
//
//                                    "<td>" +
//                                        "<label class=\"label\">" + history.getAuthCode() + "</label>" +
//                                    "</td> " +
//
//                                    "<td>" +
//                                        "<label class=\"label\">" + history.getAuthType() + "</label>" +
//                                    "</td> " +
//                               "</tr>" +
//                               "<tr>" +
//                                    "<th></th>" +
//                                    "<th>Previous Value</th>" +
//                                    "<th>Changes Made</th>" +
//                               "</tr>" +
//                               history.getXmlOutput() +
//                               "<tr>" +
//                                    "<td colspan=\"4\" align=\"right\">" +
//                                        "<button name=\"opperation\" type=\"submit\" " + javascript + " value=\"" + commandName + "\">Return</button>" +
//                                    "</td>" +
//                                "</form></tr>");
//
//                //}
//                //out.println("<tr><td><button name=\"operation\" type=\"submit\" " + javascript + " value=\"" + commandName + "\">Select All</button></td></tr>");
//            }
            out.println("</table>");
           // out.println("<button name=\"operation\" type=\"submit\" " + javascript + " value=\"" + commandName + "\">Return</button>");

        } catch (java.io.IOException ex) {
            throw new JspException("Error in AuthSearchResultTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

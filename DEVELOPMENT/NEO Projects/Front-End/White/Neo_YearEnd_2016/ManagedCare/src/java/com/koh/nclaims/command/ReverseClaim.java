/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
import neo.manager.ClaimReversals;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.Security;

/**
 *
 * @author johanl
 */
public class ReverseClaim extends NeoCommand {

    private static final Logger logger = Logger.getLogger(ReverseClaim.class.getName());
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        PrintWriter out = null;

        Date today = new Date(System.currentTimeMillis());

        String reversalCode = request.getParameter("reversalReason");
        String reversalCodeText = port.getValueFromCodeTableForId("Claim Reversal Codes", reversalCode);
        
        int claimId = Integer.parseInt("" + session.getAttribute("claimId"));

        System.out.println("reversalCode = " + reversalCode);
        System.out.println("reversalCodeText = " + reversalCodeText);
        System.out.println("claimId = " + claimId);

        Security _secure = new Security();
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        _secure.setCreatedBy(user.getUserId());
        _secure.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        _secure.setLastUpdatedBy(user.getUserId());
        _secure.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        _secure.setSecurityGroupId(user.getSecurityGroupId());

        ClaimReversals cr = new ClaimReversals();
        cr.setReversalCode(reversalCode);
        cr.setReversalDescription(reversalCodeText);
        String returnMsg = "";

        Collection<Integer> claimLineIds = port.reverseClaim(claimId, cr, _secure);
        try{
            //port.processClaim(claimId, _secure);
            if (claimLineIds != null && claimLineIds.size() > 0) {
                returnMsg = "Done|Claim " + claimId + " reversed";
            } else {
                returnMsg = "Error|Failed to reverse claimId - "+ claimId;
            }
        } catch (Exception e) {
            returnMsg = "Error|Failed to reverse claimId - " + claimId;
            System.out.println("Process error : " + e.getMessage());
        }

        try {
            System.out.println("[fr]returnMsg = " + returnMsg);
            out = response.getWriter();
            out.println(returnMsg);
//            String nextJSP = "/NClaims/ClaimHistory.jsp";
//            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
//            dispatcher.forward(request, response);

        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "ReverseClaim";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.claims.command;

import com.koh.command.NeoCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.CoverPDCDetails;
import neo.manager.PersonDetails;

/**
 * TODO
 * @author josephm
 */
public class ViewMemberDetails extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        //System.out.println("entered ViewPolicyHolderDetailsCommand");
        //Integer entityId = (Integer) session.getAttribute("entityId");
        String coverNumber = "" + session.getAttribute("policyNumber");
        Integer depNumber = (Integer) session.getAttribute("dependantNumber");
        CoverPDCDetails list = service.getNeoManagerBeanPort().getCoverProductDetailsByCoverNumberForPDC(coverNumber, depNumber);
        PersonDetails person = service.getNeoManagerBeanPort().getPersonDetailsByEntityId(list.getEntityId());



        session.setAttribute("title", person.getTitle());
        session.setAttribute("initials", person.getInitials());
        session.setAttribute("idNumber", person.getIDNumber());
        session.setAttribute("matitalStatus", person.getMaritalStatus());
        session.setAttribute("homeLanguage", person.getHomeLanguage());
        session.setAttribute("employerName", person.getEmployer());
        session.setAttribute("incomeCategoty", person.getIncomeCategory());
        session.setAttribute("incomeCategoty", person.getJobTitle());

        //Remove for Address infor from session
        session.removeAttribute("PhysicalAddress");
        session.removeAttribute("PhysicalAddress1");
        session.removeAttribute("patientPhysicalAddress2");
        session.removeAttribute("PhysicalCity");
        session.removeAttribute("PostalAddress");
        session.removeAttribute("PostalAddress1");
        session.removeAttribute("PostalAddress2");
        session.removeAttribute("PhysicalCity");
        session.removeAttribute("email");
        session.removeAttribute("faxNumber");
        session.removeAttribute("cellNumber");
        session.removeAttribute("workNumber");
        session.removeAttribute("homeNumber");

        session.removeAttribute("dates");
        session.removeAttribute("dropDownDates");
        session.removeAttribute("sessionHash");
        session.removeAttribute("catched");

        try {
            String nextJSP = "/PDC/PolicyHolderDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewMemberDetails";
    }
}

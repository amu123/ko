/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.calltrack.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ProviderDetails;

/**
 *
 * @author josephm
 */
public class FindPracticeByCodeCommand extends NeoCommand {

     @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        String provider = request.getParameter("providerNumber");
        ProviderDetails details = service.getNeoManagerBeanPort().fetchPracticeNameForAjax(provider);

        try {
             PrintWriter out = response.getWriter();

                if(details.getPracticeName() != null){
                     String providerDesc = "";
                     providerDesc = details.getDisciplineType().getId() +" - "+ details.getDisciplineType().getValue();

                     out.print(getName() + "|ProviderName="+details.getPracticeName()+ "|ProviderDiscipline="+providerDesc+"$");

                 }
                 else {

                     out.print("Error|No such provider|" + getName());
                     //request.setAttribute("providerNumber", "");
                 }

             if(details.getDisciplineType() != null){
                    session.setAttribute("disciplineCode", details.getDisciplineType().getId());
             }
        }
        catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }
    
    @Override
    public String getName() {
        return "FindPracticeByCodeCommand";
    }

}

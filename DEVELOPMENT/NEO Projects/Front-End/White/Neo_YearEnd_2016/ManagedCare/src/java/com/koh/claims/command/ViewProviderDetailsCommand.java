/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.BankingDetails;
import neo.manager.EntityPaymentRunDetails;
import neo.manager.NeoManagerBean;
import neo.manager.PaymentRunSearchCriteria;
import neo.manager.ProviderDetails;
import neo.manager.ProviderNetworkDetails;
import org.apache.log4j.Logger;

/**
 *
 * @author josephm
 */
public class ViewProviderDetailsCommand extends NeoCommand {

    private Logger log = Logger.getLogger(ViewProviderDetailsCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        NeoManagerBean port = service.getNeoManagerBeanPort();
        PaymentRunSearchCriteria payRun = new PaymentRunSearchCriteria();
        Date today = new Date();
        XMLGregorianCalendar cal = DateTimeUtils.convertDateToXMLGregorianCalendar(today);

        String providerNumber = "";
        if (session.getAttribute("practiceNumber") != null) {
            providerNumber = (String) session.getAttribute("practiceNumber");
        } else {
            providerNumber = request.getParameter("provNum_text");
        }

        payRun.setEntityNo(providerNumber);

        ProviderDetails provDetails = port.getPracticeDetailsForEntityByNumber(providerNumber);
        Collection<BankingDetails> bankDetails = port.getAllBankingDetails(provDetails.getEntityId());

        log.info("The provider entity id is " + provDetails.getEntityId());
        //log.info("The banking details list is " + bankDetails.size());
        if (provDetails.getNetworkHistory() != null) {
            //log.info("The network history list " + provDetails.getNetworkHistory().size());
        }

        if (provDetails != null) {
//            log.info("The provide with a network " + provDetails.getNetworkName());
//            
            //log.info("Contract start date : " + provDetails.getContractStartDate());
            //log.info("Contract end date :  " + provDetails.getContractEndDate());
            //log.info("The dispensing status " + provDetails.getDispensingProvider().getValue());
//            log.info("The dispensing id :" + provDetails.getDispensingProvider().getId());

            session.setAttribute("provEntityID", provDetails.getEntityId());
            session.setAttribute("practiceNumber", provDetails.getPracticeNumber());
            session.setAttribute("memClaimResultProvNum", provDetails.getPracticeNumber());
            session.setAttribute("practiceName", provDetails.getPracticeName());
            session.setAttribute("practiceSurname", provDetails.getProviderName());
            session.setAttribute("disciplineType", provDetails.getDisciplineType().getValue());
            session.setAttribute("networkIndicator", provDetails.getNetworkName());
            session.setAttribute("restrictions", provDetails.getCashPractice().getValue());
            if (provDetails.getContractStartDate() != null) {
                session.setAttribute("contractStart", dateFormat.format(provDetails.getContractStartDate().toGregorianCalendar().getTime()));
            }

            if (provDetails.getContractEndDate() != null) {
                if (dateFormat.format(provDetails.getContractEndDate().toGregorianCalendar().getTime()).equalsIgnoreCase("2999/12/31")) {

                    session.setAttribute("contractEnd", "");
                } else {

                    session.setAttribute("contractEnd", dateFormat.format(provDetails.getContractEndDate().toGregorianCalendar().getTime()));
                }
            }
            //log.info("The network indicator : " + prov.getNetworkDescription());
            //log.info("The network start date is : " + prov.getEffectiveStartDate());
            //log.info("The network end date is : " + prov.getTerminationDate());

            if (provDetails.getDispensingProvider() != null && provDetails.getDispensingProvider().getValue().equalsIgnoreCase("Yes")) {

                session.setAttribute("dispensingStatus", "Dispensing");
            } else if (provDetails.getDispensingProvider() != null && provDetails.getDispensingProvider().getValue().equalsIgnoreCase("No")) {

                session.setAttribute("dispensingStatus", "Non-Dispensing");
            }


            if (provDetails.getProviderStatus() != null && provDetails.getProviderStatus().getValue().equalsIgnoreCase("No")) {

                session.setAttribute("status", "Active");
            } else if (provDetails.getProviderStatus() != null && provDetails.getProviderStatus().getValue().equalsIgnoreCase("Yes")) {

                session.setAttribute("status", "Closed");
            }

            if (provDetails.getBhfStartDate() != null) {
                session.setAttribute("startDate", dateFormat.format(provDetails.getBhfStartDate().toGregorianCalendar().getTime()));
            }
            if (provDetails.getBhfEndDate() != null) {
                session.setAttribute("endDate", dateFormat.format(provDetails.getBhfEndDate().toGregorianCalendar().getTime()));

                //log.info("The effective end date is " + provDetails.getBhfEndDate());
            }
            //log.info("The effective end date is " + provDetails.getBhfEndDate());
        }

        for (BankingDetails bank : bankDetails) {

            session.setAttribute("pracBankName", bank.getBankName());
            session.setAttribute("pracBranchCode", bank.getBranchCode());
            session.setAttribute("pracBranchName", bank.getBranchName());
            session.setAttribute("pracAccountNumber", bank.getAccountNo());
            session.setAttribute("pracAccountName", bank.getAccountName());
            session.setAttribute("pracAccountType", bank.getAccountType());
            if (bank.getEffectiveStartDate() != null) {
                session.setAttribute("pracEffectBankDate", dateFormat.format(bank.getEffectiveStartDate().toGregorianCalendar().getTime()));
            }
        }

        //log.info("The provider number is " + providerNumber);
        //log.info("Inside ViewProviderDetailsCommand ");
        //log.info("The provider payment run list " + paymentRunDetailsList.size());

        
        session.setAttribute("practiceNumber", providerNumber);

        boolean isClearingUser = (Boolean) session.getAttribute("persist_user_ccClearing");
        session.setAttribute("persist_isClearingClaims", false);
        if (isClearingUser) {
            if (!providerNumber.trim().equalsIgnoreCase("9999997")) {
                session.setAttribute("persist_isClearingClaims", true);
            }
            payRun.setClearingClaimPayment(true);
        }else{
            payRun.setClearingClaimPayment(false);
        }
        session.setAttribute("payRun", payRun);
        session.setAttribute("ProductIDTypeSelected", "null"); //Populate the DropdownBox
        session.setAttribute("ProviderOrMember","Provider");
        
        Collection<ProviderNetworkDetails> tariffNetworkHistory = port.getAllPracticeNetworkHistory(provDetails.getEntityId());
        
        int recordCount = 0;  //See if there's any expired records
        for(ProviderNetworkDetails details: tariffNetworkHistory){   
            GregorianCalendar calToday = new GregorianCalendar();
            Calendar calen = Calendar.getInstance();

            calen.setTime(calToday.getTime());
            int currentYear = calen.get(Calendar.YEAR);
            calen.setTime(details.getEffectiveStartDate().toGregorianCalendar().getTime());
            int startYear = calen.get(Calendar.YEAR);
            calen.setTime(details.getTerminationDate().toGregorianCalendar().getTime());
            int endYear = calen.get(Calendar.YEAR);
            
            if (startYear > currentYear || endYear < currentYear) {
                recordCount++;
            }
        }
        
        session.setAttribute("Show_InactiveNetworkIndicatorGrid", "true");
        if(recordCount == 0){
            session.setAttribute("Show_InactiveNetworkIndicatorGrid", "false");
        }
        session.setAttribute("networkHistory", tariffNetworkHistory);
        session.setAttribute("Show_NetworkIndicatorHistoryGridTag", "false");
        session.setAttribute("ShowTable", "false");
        try {
            //String nextJSP = "/Claims/ProviderDetailsTabbedPage.jsp";
            String nextJSP = "/Claims/PracticeDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {

        return "ViewProviderDetailsCommand";
    }
}

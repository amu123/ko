/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.tags;

import com.koh.command.NeoCommand;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CcClaimLineSearchResult;
import neo.manager.CcClaimLine;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Johan-NB
 */
public class MemberClaimLineDetailsTable extends TagSupport {

    private static final Logger logger = Logger.getLogger(MemberClaimLineDetailsTable.class.getName());

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

        //System.out.println("entered MemberClaimLineDetailTabel");

        //Claim selectedClaim = (Claim) session.getAttribute("selected");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        String serviceDateFrom = "";
        String paymentDate = "";

        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");

            List<CcClaimLine> claimLineList = (List<CcClaimLine>) session.getAttribute("memCCClaimLineDetails");

            if (claimLineList != null && claimLineList.isEmpty() == false) {
                boolean addForwardNav = false;
                boolean addBackwardNav = false;

                CcClaimLineSearchResult claimLineResults = (CcClaimLineSearchResult) session.getAttribute("memCCClaimLineSearchResult");
                int claimLineCount = claimLineResults.getClaimLineCount();
                int claimId = claimLineResults.getClaimId();
                int resultMin = claimLineResults.getResultMin();
                int resultMax = claimLineResults.getResultMax();

                logger.info("MemberClaimLineDetailsTable claimLineCount = "+ claimLineCount);
                logger.info("MemberClaimLineDetailsTable claimId = "+ claimId);
                logger.info("MemberClaimLineDetailsTable resultMin = "+ resultMin);
                logger.info("MemberClaimLineDetailsTable resultMax = "+ resultMax);

                if (claimLineCount == 100 && resultMin == 0) {
                    addForwardNav = true;

                } else if (claimLineCount == 100 && resultMin > 0) {
                    addForwardNav = true;
                    addBackwardNav = true;

                } else if (claimLineCount < 100 && resultMin > 0) {
                    addBackwardNav = true;
                }

                out.println("<table width=\"100%\" class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
                out.print("<tr>");
                out.print("<th align=\"left\">ClaimLine Number</th>");
                out.print("<th align=\"left\">Pay Status</th>");
                out.print("<th align=\"left\">Treatment From</th>");
                out.print("<th align=\"left\">Payment Date</th>");                
                out.print("<th align=\"left\">Amount Claimed</th>");
                out.print("<th align=\"left\">Amount Paid</th>");
                out.print("<th align=\"left\">Tariff Code</th>");
                out.print("<th align=\"left\"></th>");
                out.print("</tr>");

                for (CcClaimLine claimLine : claimLineList) {

                    out.print("<tr valign=\"top\">");

                    out.print("<td><label class=\"label\">" + claimLine.getClaimLineId() + "</label></td>");

                    //paystatus validation
                    Integer payStatusId = claimLine.getClaimLineStatusId();
                    if (payStatusId != 0) {
                        String payStatus = port.getValueForId(83, payStatusId.toString());
                        out.print("<td><label class=\"label\">" + payStatus + "</label></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if(claimLine.getTreatmentFromDate() != null){
                        out.print("<td><label class=\"label\">" + formatter.format(claimLine.getTreatmentFromDate().toGregorianCalendar().getTime()) + "</label></td>");
                    }else{
                        out.print("<td></td>");
                    }
                                                         

                    if (claimLine.getChequeRunDate() != null) {
                        paymentDate = formatter.format(claimLine.getChequeRunDate().toGregorianCalendar().getTime());
                        out.print("<td><label class=\"label\">" + paymentDate + "</label></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    
                    out.print("<td><label class=\"label\">" + new DecimalFormat("#####0.00").format(claimLine.getClaimedAmount()) + "</label></td>");
                    out.print("<td><label class=\"label\">" + new DecimalFormat("#####0.00").format(claimLine.getPaidAmount()) + "</label></td>");
                    out.println("<td><label class=\"label\">"+claimLine.getTariffCodeNr()+"</label></td>");
                    out.println("<td><button name=\"opperation\" type=\"button\" onClick=\"submitWithAction('ForwardToMemberClaimLineClaimLine', '" + claimLine.getClaimLineId() + "', '"+ claimLine.getPaidAmount() +"', '"+ claimLine.getDependantCode() +"')\"; >Details</button></td>");
                    out.print("</tr>");


                }
                //set navigation buttons
                if (addBackwardNav == true || addForwardNav == true) {
                    out.println("<tr><th colspan=\"9\" align=\"right\" >");

                    if (addBackwardNav) {
                        out.println("<button name=\"opperation\" type=\"button\" onClick=\"submitPageAction('ViewMemberClaimLineDetails', 'backward')\"; value=\"\">Previous</button>");
                    }
                    if (addForwardNav) {
                        out.println("<button name=\"opperation\" type=\"button\" onClick=\"submitPageAction('ViewMemberClaimLineDetails', 'forward')\"; value=\"\">Next</button>");
                    }


                    out.println("</th></tr></table>");
                } else {
                    out.println("</table>");
                }


            } else {
                System.out.println("claim line error");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return super.doEndTag();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.dashboard.tags;

//import com.google.gson.Gson;
import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.ActiveCountChart;
import neo.manager.MemberCountChart;
import neo.manager.MemberTotalCount;
import neo.manager.SuspendCountChart;

/**
 *
 * @author pavaniv
 */
public class MemberTotalCountTable extends TagSupport {

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        ServletRequest request = pageContext.getRequest();
        ServletContext context = pageContext.getServletContext();
        ServletResponse response = pageContext.getResponse();
        Collection<MemberTotalCount> mainList = new ArrayList<MemberTotalCount>();
        List<MemberCountChart> memChartList = new ArrayList<MemberCountChart>();
        Collection<ActiveCountChart> totAct = new ArrayList<ActiveCountChart>();
        Collection<SuspendCountChart> totSus = new ArrayList<SuspendCountChart>();
        mainList = NeoCommand.service.getNeoManagerBeanPort().getMemberCount();
        memChartList = NeoCommand.service.getNeoManagerBeanPort().getMemberChartCount();
        totAct = NeoCommand.service.getNeoManagerBeanPort().getMemberActiveChartCount();
        totSus = NeoCommand.service.getNeoManagerBeanPort().getMemberSuspendChartCount();
        System.out.println("entered memeber count" + mainList.size());

        System.out.println("entered memChartList" + memChartList.size());
        System.out.println("entered memeber count" + totAct.size());
        System.out.println("entered memeber count" + totSus.size());

        request.setAttribute("mainList", mainList);

        String memberChartValue;
        String activeChartValue;
        String suspendChartValue;

        try {
            out.print("<thead><tr>");
            out.print("<th scope=\"col\">Option</th>");
            out.print("<th scope=\"col\">Total Member Count</th>");
            out.print("<th scope=\"col\">Total Principal</th>");
            out.print("<th scope=\"col\">Total Adult Dependant</th>");
            out.print("<th scope=\"col\">Total Child Dependant</th>");
            out.print("</tr></thead>");
            int count = 0;


            for (MemberTotalCount wl : mainList) {
                
                out.print("<tr>");
                out.print("<td><label class=\"label\">" + wl.getOptions() + "</td>");
                out.print("<td><label class=\"label\">" + wl.getTotalMemCount() + "</td>");
                out.print("<td><label class=\"label\">" + wl.getTotalPrincipalCount() + "</td>");
                out.print("<td><label class=\"label\">" + wl.getTotalAdultCount() + "</td>");
                out.print("<td><label class=\"label\">" + wl.getTotalChildCount() + "</td>");
                out.print("</tr>");
                count++;
            }
//            memberChartValue = new Gson().toJson(memChartList);
//            System.out.println("testttt" + memberChartValue);
//            request.setAttribute("jsonMemData", memberChartValue);
//            System.out.println("seccccc" + request.getAttribute("jsonData"));
//            activeChartValue = new Gson().toJson(totAct);
//            request.setAttribute("jsonActiveData", activeChartValue);
//            suspendChartValue = new Gson().toJson(totSus);
//            request.setAttribute("jsonSuspendData", suspendChartValue);

        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }

        return super.doEndTag();
    }
}

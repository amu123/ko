/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.claims.command;

import com.koh.command.NeoCommand;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author
 * gerritr
 */
public class ViewNetworkIndicatorHistoryCommand extends NeoCommand{
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        org.apache.log4j.Logger.getLogger(this.getClass()).info("in ManageSessionCommand");
        HttpSession session = request.getSession();

        String selectedProductName = request.getParameter("selectedProductName");
        String selectedNetworkDescription = request.getParameter("selectedNetworkDescription");
        String Show_NetworkIndicatorHistoryGridTag = request.getParameter("Show_NetworkIndicatorHistoryGridTag");

        session.setAttribute("selectedProductName", selectedProductName);
        session.setAttribute("selectedNetworkDescription", selectedNetworkDescription);
        session.setAttribute("Show_NetworkIndicatorHistoryGridTag", Show_NetworkIndicatorHistoryGridTag);
        
        return null;
    }
    @Override
    public String getName() {
        return "ViewNetworkIndicatorHistoryCommand";
    }
}

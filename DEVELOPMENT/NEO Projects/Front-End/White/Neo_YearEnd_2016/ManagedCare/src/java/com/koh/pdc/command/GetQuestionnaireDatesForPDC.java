/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.PdcAnswer;

/**
 *
 * @author princes
 */
public class GetQuestionnaireDatesForPDC extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        String element = request.getParameter("element");
        String coverNumber = (String) session.getAttribute("policyNumber");
        Integer depNo = (Integer) session.getAttribute("dependantNumber");
        System.out.println("element " + element + "coverNumber " + coverNumber + "depNo " + depNo);
        Format formatter = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss");
        try {
            PrintWriter out = response.getWriter();
            //Get questionnaire dates
            HashMap<Integer, String> datesList = new HashMap<Integer, String>();
            List<PdcAnswer> dateList = service.getNeoManagerBeanPort().getQuestionnairesFilledByPatientPerPDC(coverNumber, depNo, new Integer(element));
            session.setAttribute("dropDownDates", dateList);
            String dates = "";
            String qDate = "";
            int i = 0;
            if (dateList != null && dateList.size() > 0) {
                for (PdcAnswer list : dateList) {
                    Date create = list.getAnsweredDate().toGregorianCalendar().getTime();
                    qDate = formatter.format(create);
                    dates = dates + "," + qDate;
                    datesList.put(i, qDate);
                    System.out.println("dates " + qDate);
                    i++;
                }
                out.print(getName() + "|Dates=" + dates + "$");
                session.setAttribute("dates", datesList);
            } else {
                out.print("Error|No payment dates|" + getName());
                session.setAttribute("tapView", "double");
                session.setAttribute("qaction", "create");
                session.setAttribute("dates", null);
            }
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (NumberFormatException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetQuestionnaireDatesForPDC";
    }
}

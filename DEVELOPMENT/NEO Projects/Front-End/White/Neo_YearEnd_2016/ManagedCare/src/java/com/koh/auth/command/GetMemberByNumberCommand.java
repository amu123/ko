/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.auth.command;

import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ContactDetails;
import neo.manager.CoverDetails;
import neo.manager.EmployerGroup;
import neo.manager.Member;
import neo.manager.NeoManagerBean;
import org.apache.log4j.Logger;

/**
 *
 * @author whauger
 */
public class GetMemberByNumberCommand extends NeoCommand {

    private CoverDetails c;
    private Logger logger = Logger.getLogger(GetMemberByNumberCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        logger.info("******************************************************");
        HttpSession session = request.getSession();
        String number = request.getParameter("number");
        session.setAttribute("memberCoverNumber", number);
        session.setAttribute("CustomerCareSide", "true");
        boolean viewStaff = (Boolean) session.getAttribute("persist_user_viewStaff");
        String email = "";

        logger.info("Number : " + number);

        if (number != null && !number.isEmpty()) {
            number = number.trim();
        }

        NeoManagerBean port = service.getNeoManagerBeanPort();
        try {
            PrintWriter out = response.getWriter();
            c = port.getEAuthResignedMembers(number);
            if (c != null) {
                session.setAttribute("memberCoverEntityId", c.getEntityId());
                session.setAttribute("memberEntityCommon", c.getEntityCommonId());
                request.setAttribute("entityID", c.getEntityId());
                request.setAttribute("memberEntityId", c.getEntityId());
            }
            Map<String, Integer> userProductRest = (Map<String, Integer>) session.getAttribute("persist_productViewRestriction");
            if (c != null) {
                for (Map.Entry<String, Integer> entry : userProductRest.entrySet()) {
                    if ("Resolution Health".equals(c.getProductName())) {
                        if (entry.getValue() == 0 && entry.getKey().equals("allowReso")) {
                            request.setAttribute("coverSearchResultsMessage", "Result list excludes Resolution Health members due to user restrictions");
                            out.print("Access|User access denied. Please contact your supervisor");
                            return null;
                        }
                    }
                    if ("Spectramed".equals(c.getProductName())) {
                        if (entry.getValue() == 0 && entry.getKey().equals("allowSpec")) {
                            request.setAttribute("coverSearchResultsMessage", "Result list excludes Spectramed members due to user restrictions");
                            out.print("Access|User access denied. Please contact your supervisor");
                            return null;
                        }
                    }
                }
            }
            logger.info("viewStaff " + viewStaff);
            if (!viewStaff) {
                if (c != null && c.getEntityId() > 0) {
                    Member mem = port.fetchMemberAddInfo(c.getEntityId());
                    c.setMemberCategory(mem.getMemberCategoryInd());
                    logger.info("EntityId " + c.getEntityId() + " MemberCategoryInd " + mem.getMemberCategoryInd());
                    if (c.getMemberCategory() != 2) {
                        //Get email contact details
                        ContactDetails cd = port.getContactDetails(c.getEntityId(), 1);
                        if (cd != null && cd.getPrimaryIndicationId() == 1) {
                            if (cd.getMethodDetails() == null) {
                                email = "";
                            } else {
                                email = cd.getMethodDetails() + "";
                            }
                        }
                        if (email == "") {
                            cd = port.getContactDetails(c.getEntityId(), 3);
                            if (cd != null && cd.getPrimaryIndicationId() == 3) {
                                if (cd.getMethodDetails() == null) {
                                    email = "";
                                } else {
                                    email = cd.getMethodDetails() + "";
                                }
                            }
                        }
                        if (email == "") {
                            cd = port.getContactDetails(c.getEntityId(), 7);
                            if (cd != null && cd.getPrimaryIndicationId() == 7) {
                                if (cd.getMethodDetails() == null) {
                                    email = "";
                                } else {
                                    email = cd.getMethodDetails() + "";
                                }
                            }
                        }
                        logger.info("Response : " + getName() + "|Number=" + number + "|Email=" + email + "$");
                        out.print(getName() + "|Number=" + number + "|Email=" + email + "|productId=" + c.getProductId() + "|entityId=" + c.getEntityId() + "$");
                        out.print("#");
                        out.print(getDirectories(number, request));
                        out.print("*");
                        logger.info("Directories: " + getDirectories(number, request));

                        request.setAttribute("productId", c.getProductId());
                    } else {
                        out.print("Access|User access denied. Please contact your supervisor|" + getName());
                    }
                } else {
                    out.print("Error|No such member|" + getName());
                }
            } else if (c != null && c.getEntityId() > 0) {
                logger.info("EntityId " + c.getEntityId() + " No MemberCategoryInd loaded");
                //Get email contact details
                ContactDetails cd = port.getContactDetails(c.getEntityId(), 1);
                if (cd != null && cd.getPrimaryIndicationId() == 1) {
                    if (cd.getMethodDetails() == null) {
                        email = "";
                    } else {
                        email = cd.getMethodDetails() + "";
                    }
                }
                if (email == "") {
                    cd = port.getContactDetails(c.getEntityId(), 3);
                    if (cd != null && cd.getPrimaryIndicationId() == 3) {
                        if (cd.getMethodDetails() == null) {
                            email = "";
                        } else {
                            email = cd.getMethodDetails() + "";
                        }
                    }
                }
                if (email == "") {
                    cd = port.getContactDetails(c.getEntityId(), 7);
                    if (cd != null && cd.getPrimaryIndicationId() == 7) {
                        if (cd.getMethodDetails() == null) {
                            email = "";
                        } else {
                            email = cd.getMethodDetails() + "";
                        }
                    }
                }
                logger.info("Response : " + getName() + "|Number=" + number + "|Email=" + email + "$");
                out.print(getName() + "|Number=" + number + "|Email=" + email + "|productId=" + c.getProductId() + "|entityId=" + c.getEntityId() + "$");
                out.print("#");
                out.print(getDirectories(number, request));
                out.print("*");
                logger.info("Directories: " + getDirectories(number, request));

                request.setAttribute("productId", c.getProductId());
                request.setAttribute("entityID", c.getEntityId());
                request.setAttribute("memberEntityId", c.getEntityId());
                System.out.println("entityID: " + c.getEntityId());

            } else {
                out.print("Error|No such member|" + getName());
            }

        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        }

        //******************REQUEST ITERATOR
        java.util.Enumeration attributeNames = request.getParameterNames();
        while (attributeNames.hasMoreElements()) {
            String name = String.valueOf(attributeNames.nextElement());
            String value = String.valueOf(request.getParameter(name));
            String viewClass = String.valueOf(request.getParameter(name).getClass());
            request.setAttribute(name, value);
        }
        //******************REQUEST ITERATOR
        return null;
    }

    @Override
    public String getName() {
        return "GetMemberByNumberCommand";
    }

    private String getDirectories(String memberNo, HttpServletRequest request) {
        StringBuffer sb = new StringBuffer();
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();

        try {
            SimpleDateFormat sTOd = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat dTOs = new SimpleDateFormat("yyyy/MM/dd");

            String path = new PropertiesReader().getProperty("statements");

            session.setAttribute("StatementProductID", String.valueOf(c.getProductId()));

//            EmployerGroup empGroup = port.getEmployerByEntityId(session.getAttribute("employerEntityId"));
            if (c.getProductId() == 1) {
                path += "ResolutionHealth\\";
            } else if (c.getProductId() == 2) {
                path += "Spectramed\\";
            } else if (c.getProductId() == 3) {
                path += "Sizwe\\";
            }

            //logger.info("Path: " + path);
            File dir = new File(path);
            boolean isAdd = false;

            for (File moreDir : dir.listFiles()) {

                if (moreDir.isDirectory()) {
                    File file = new File(moreDir.getAbsolutePath() + "/member/Statement_" + memberNo + ".pdf");

                    if (file.isFile()) {

                        if (isAdd) {
                            sb.append("!");
                        } else {
                            isAdd = true;
                        }

                        Date d = sTOd.parse(moreDir.getName());
                        String dateStr = dTOs.format(d);
                        sb.append(dateStr);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception getting the folders: " + e.getMessage());
        }
        return sb.toString();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.KeyValue;
import neo.manager.NeoManagerBean;

/**
 *
 * @author shimanem
 */
public class ViewDocumentGenerationCommand extends NeoCommand {

    private static final Logger LOG = Logger.getLogger(ViewDocumentGenerationCommand.class.getName());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        PrintWriter writer = null;
        try {
            HttpSession session = request.getSession();
            writer = response.getWriter();

            NeoManagerBean port = service.getNeoManagerBeanPort();

            List<KeyValue> docGenCDLMap = port.getPDCChronicConditions();
            List<KeyValue> docGenOtherCDLMap = port.getPDCOtherChronicConditions();

            session.setAttribute("documentGenerationMap", docGenCDLMap);
            session.setAttribute("documentOtherCDLMap", docGenOtherCDLMap);

            System.out.println("SERVER >>> Entered: Doucument Generation TAB");
            String view = "/PDC/PDCDocumentGeneration.jsp";
            RequestDispatcher dispatch = context.getRequestDispatcher(view);
            dispatch.forward(request, response);

        } catch (IOException ex) {
            Logger.getLogger(ViewDocumentGenerationCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(ViewDocumentGenerationCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewDocumentGenerationCommand";
    }
}

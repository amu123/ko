/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import neo.manager.LabTariffDetails;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.AuthTariffDetails;
import neo.manager.DentalTCode;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;
import neo.manager.TCode;

/**
 *
 * @author johanl
 */
public class GetAuthLabCodeByTariffCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        PrintWriter out = null;
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        System.out.println("inside GetAuthLabCodeByTariffCommand");
        String errorReturn = "";
        //get values from request
        String provTypeId = "" + session.getAttribute("treatingProviderDiscTypeId");
        String labTypeId = "" + session.getAttribute("labProviderDiscTypeId");
        String disciplineType = "";

        if (!labTypeId.equalsIgnoreCase("null") && labTypeId != null && !labTypeId.equalsIgnoreCase("")) {
            disciplineType = labTypeId;
        } else {
            disciplineType = provTypeId;
        }


        //auth from
        String servDate = "" + session.getAttribute("authFromDate");
        Date date = null;
        XMLGregorianCalendar serviceDate = null;
        try {
            date = DateTimeUtils.dateFormat.parse(servDate);
            serviceDate = DateTimeUtils.convertDateToXMLGregorianCalendar(date);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        String tariffSesVal = request.getParameter("sesVal");
        //get dentalTCode for clinical code
        ArrayList<AuthTariffDetails> ctList = (ArrayList<AuthTariffDetails>) session.getAttribute(tariffSesVal);

        try {
            out = response.getWriter();
            if (ctList != null && !ctList.isEmpty()) {
                System.out.println("ct list size in GetAuthLabCodeByTariffCommand = " + ctList.size());
                //List<DentalTCode> dentalTCodes = null;
                Collection<LabTariffDetails> labList = new ArrayList<LabTariffDetails>();
                Collection<LabTariffDetails> noLabList = new ArrayList<LabTariffDetails>();
                Collection<LabTariffDetails> tariffLabList = new ArrayList<LabTariffDetails>();
                session.setAttribute("DentalLabCodeList", labList);
                session.setAttribute("DentalNoLabCodeList", noLabList);

                //*********************CLINICAL TARIFF ****************
                for (AuthTariffDetails at : ctList) {

                    String clinicalTariff = at.getTariffCode();
                    
                    //int ctCode = Integer.parseInt(clinicalTariff);
                    tariffLabList = port.getDentalLabsByTariff(clinicalTariff, disciplineType, serviceDate);                  
                    LabTariffDetails noLabT = new LabTariffDetails();
                    noLabT.setClinicalTariff(clinicalTariff);
                    noLabT.setProviderType(disciplineType);
                    noLabList.add(noLabT);
                    System.out.println("no lab found = " + clinicalTariff);
                    session.setAttribute("DentalNoLabCodeList", noLabList);

                    if (tariffLabList != null && !tariffLabList.isEmpty()) {
                        //*********************TDENTAL CODES****************         
                        for (LabTariffDetails labs : tariffLabList) {

                            LabTariffDetails labT = new LabTariffDetails();
                            labT.setUserId(user.getUserId());
                            //set clinical tariff data
                            labT.setClinicalTariff(clinicalTariff);
                            labT.setProviderType(disciplineType);
                            //set lab code data
                            //labT.setTCode(tc.getTcode());
                            labT.setLabCode(labs.getLabCode());
                            labT.setLabCodeDisc(labs.getLabCodeDisc());
                            labT.setLabCodeQuantity(labs.getLabCodeQuantity());
                            labT.setLabCodeAmount(labs.getLabCodeAmount());

                            session.setAttribute(clinicalTariff + labs.getLabCode() + "OriginalTA", labs.getLabCodeAmount());
                            session.setAttribute(clinicalTariff + labs.getLabCode() + "OriginalTQ", labs.getLabCodeQuantity());                           
                            labList.add(labT);
                        }

                    }

                }//end clinical tariff loop 

                session.setAttribute("DentalLabCodeList", labList);            

                //Remove saved tariff labs if tariff is deleted
                ArrayList<LabTariffDetails> savedNoLabs = (ArrayList<LabTariffDetails>) session.getAttribute("SavedDentalNoLabCodeList");
                ArrayList<LabTariffDetails> savedLabs = (ArrayList<LabTariffDetails>) session.getAttribute("SavedDentalLabCodeList");

                ArrayList<LabTariffDetails> savedNewNoLabs = new ArrayList<LabTariffDetails>();
                ArrayList<LabTariffDetails> savedNewLabs = new ArrayList<LabTariffDetails>();

                boolean labsSet = false;
                boolean noLabsSet = false;

                if (savedLabs != null && !savedLabs.isEmpty()) {
                    labsSet = true;
                }
                if (savedNoLabs != null && !savedNoLabs.isEmpty()) {
                    noLabsSet = true;
                }

                if (noLabsSet || labsSet) {

                    for (AuthTariffDetails tariff : ctList) {

                        if (noLabsSet) {
                            for (LabTariffDetails noLab : savedNoLabs) {
                                if (tariff.getTariffCode().equals(noLab.getClinicalTariff())) {
                                    savedNewNoLabs.add(noLab);
                                }
                            }
                        }

                        if (labsSet) {
                            for (LabTariffDetails lab : savedLabs) {
                                if (tariff.getTariffCode().equals(lab.getClinicalTariff())) {
                                    savedNewLabs.add(lab);
                                }
                            }
                        }

                    }
                    session.setAttribute("SavedDentalNoLabCodeList", savedNewNoLabs);
                    session.setAttribute("SavedDentalLabCodeList", savedNewLabs);
                    
                }


                out.println("Done|");
            } else {
                errorReturn = "ERROR|No Clinical Tariff";
            }

            if (!errorReturn.trim().equalsIgnoreCase("")) {
                out.println("Error|");
            }

        } catch (IOException io) {
            System.out.println("GetAuthLabCodeByTariffCommand error : " + io.getMessage());
            out.println("Error|");
        } finally {
            out.close();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetAuthLabCodeByTariffCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.tags;

import com.koh.auth.dto.TariffSearchResult;
import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class TariffSearchResultTable extends TagSupport {
    private String javascript;
    private String commandName;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");
            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Code</th><th>Description</th><th>Discipline</th><th>Select</th></tr>");

            ArrayList<TariffSearchResult> tariffs = (ArrayList<TariffSearchResult>) req.getAttribute("searchTariffResult");
            if (tariffs != null) {
                for (int i = 0; i < tariffs.size(); i++) {
                    TariffSearchResult tariff = tariffs.get(i);
                    out.println("<tr><form><td><label class=\"label\">" + tariff.getCode() + "</label><input type=\"hidden\" name=\"code\" value=\"" + tariff.getCode() + "\"/></td>" +
                            "<td><label class=\"label\">" + tariff.getDescription() + "</label><input type=\"hidden\" name=\"description\" value=\"" + tariff.getDescription() + "\"/></td>" +
                            "<td><label class=\"label\">" + tariff.getDiscipline() + "</label><input type=\"hidden\" name=\"discipline\" value=\"" + tariff.getDiscipline() + "\"/></td>" +
                            "<td><button name=\"opperation\" name=\"opperation\" type=\"submit\" " + javascript + " value=\"" + commandName + "\">Select</button></td></form></tr>");

                }
            }
            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in TariffSearchResultTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

}

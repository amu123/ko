/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.tags;

import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import java.io.File;
import java.io.FilenameFilter;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.LookupValue;
import neo.manager.NeoManagerBean;

/**
 *
 * @author Christo
 */
public class DocumentIndexDropdownType extends TagSupport {

    private String elementName = null;
    private String displayName = null;

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            String docType = (String)req.getAttribute("docType") != null?(String)req.getAttribute("docType"):"";
            if (elementName.equals("profiletype")) {
                String sorceDrive = new PropertiesReader().getProperty("DocumentIndexScanDrive");
                String scanFolder = new PropertiesReader().getProperty("DocumentIndexScanFolder");
                String selectedProfileType = ((String) req.getParameter("profiletype"))!=null?((String) req.getParameter("profiletype")):"";
                File node = new File(sorceDrive+scanFolder);
                if(node.isDirectory()){
                    String[] folderList = node.list(new FilenameFilter() {
                        @Override
                        public boolean accept(File dir, String name) {
                            return new File(dir, name).isDirectory();
                        }
                        });
                    if(folderList!=null && folderList.length>0){
//                        System.out.println("File List result size : = " + folderList.length);
                        out.println("<td ><label>" + displayName + ":</label>");
                        out.println("<select  size=\"1\" name=\"" + elementName + "\" id=\"profiletype\">");
                        out.println("<option value=\"select\" >Select Profile</option>");
                        for (String folderName : folderList) {
                            if(selectedProfileType.trim().equals(folderName.trim())){
                                out.println("<option value=\"" + folderName + "\" selected=\"selected\" >" + folderName + "</option>");
                            }else{
                                out.println("<option value=\"" + folderName + "\" >" + folderName + "</option>");
                            }
                        }
                        out.println("</select>");
                        out.println("<button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexFileListCommand'; refreshFileListByProfile();\">Refresh</button>");
                        out.println("</td>");
                    }else{
                        out.println("<td ><label>" + displayName + ":</label>");
                        out.println("<select  size=\"1\" name=\"" + elementName + "\" id=\"profiletype\">");
                        out.println("<option value=\"select\" >Select Profile</option>");
                        out.println("</select>");
                        out.println("<button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexFileListCommand'; refreshFileListByProfile();\">Refresh</button>");
                        out.println("</td>");
                    }
                }else{
                        out.println("<td ><label>" + displayName + ":</label>");
                        out.println("<select  size=\"1\" name=\"" + elementName + "\" id=\"profiletype\">");
                        out.println("<option value=\"select\" >Select Profile</option>");
                        out.println("</select>");
                        out.println("<button type=\"submit\" name=\"opperation\" onClick=\"document.getElementById('opperation').value ='DocumentIndexFileListCommand'; refreshFileListByProfile();\">Refresh</button>");
                        out.println("</td>");
                }
            } else {
                NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();

                List<LookupValue> values = port.getCodeTable(212);

                if (values != null) {
//                    System.out.println("File List result size = " + values.size());
                    out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td><td align=\"left\" width=\"200px\">");
                    out.println("<select style=\"width:203px\" size=\"1\" name=\"" + elementName + "\" id=\""+ displayName +"\" >");
                    if(docType.contains("Claims general")){
                        out.println("<option value=\"10\" selected=\"selected\" >Claims general</option>");
                    }else{
                       for (LookupValue lookupValue : values) {
//                        System.out.println("lookupValue : "+ lookupValue+" docType : " + docType);
                        if(docType.equals(lookupValue.getValue()) || docType.equals(lookupValue.getId())){
                            out.println("<option value=\"" + lookupValue.getId() + "\" selected=\"selected\" >" + lookupValue.getValue() + "</option>");
                        }else{
                            out.println("<option value=\"" + lookupValue.getId() + "\" >" + lookupValue.getValue() + "</option>");
                        }
                    } 
                    }
                    
                    out.println("</select></td>");
                }
            }

        } catch (java.io.IOException ex) {
            throw new JspException("Error in DocumentIndexDropdownType tag", ex);
        }
        return super.doEndTag();
    }
}

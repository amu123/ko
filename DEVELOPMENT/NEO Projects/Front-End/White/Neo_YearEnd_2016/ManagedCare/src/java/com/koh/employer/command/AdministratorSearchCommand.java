/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.employer.command;


import com.koh.command.NeoCommand;
import com.koh.member.application.command.MemberApplicationSpecificQuestionsCommand;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.Broker;
import neo.manager.NeoManagerBean;
import neo.manager.NeoUser;

/**
 *
 * @author sphephelot
 */
public class AdministratorSearchCommand extends NeoCommand{
 
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            NeoManagerBean port = service.getNeoManagerBeanPort();
            search(port, request, response, context);
        } catch (Exception ex) {
            Logger.getLogger(MemberApplicationSpecificQuestionsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void search(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        NeoUser ctr = new NeoUser();
        
        String admName = request.getParameter("admName");
        String admSurname = request.getParameter("admSurname");
        String admUsername = request.getParameter("admUsername");
        String admEmail = request.getParameter("admEmail");
        
        ctr.setName(admName);
        ctr.setSurname(admSurname);
        ctr.setUsername(admUsername);
        ctr.setEmailAddress(admEmail);

        List<neo.manager.NeoUser> user = port.fetchNeoAdministator(ctr);
        request.setAttribute("AdministratorSearchResults", user);
        context.getRequestDispatcher("/Employer/AdministratorResult.jsp").forward(request, response);
    }
    
    @Override
    public String getName() {
        return "AdministratorSearchCommand";
    }
    
}

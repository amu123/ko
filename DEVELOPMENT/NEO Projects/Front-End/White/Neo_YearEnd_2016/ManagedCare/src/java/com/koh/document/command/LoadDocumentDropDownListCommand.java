/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;


import com.koh.command.NeoCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.LookupValue;

/**
 *
 * @author sphephelot
 */
public class LoadDocumentDropDownListCommand extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        String value = request.getParameter("lookUpID");
        int lookupId = Integer.parseInt(value);
        String lookupStr = "";
        
                if (lookupId != 0) {
                List<neo.manager.LookupValue> lookUps = NeoCommand.service.getNeoManagerBeanPort().getCodeTable(lookupId);

                for (LookupValue lv : lookUps) {
                    lookupStr += lv.getId() + ":" + lv.getValue() + "&";
                }
                if (!lookupStr.equalsIgnoreCase("")) {
                    lookupStr = lookupStr.substring(0, lookupStr.length() - 1);
                } 
            } 
        try {
            PrintWriter out = response.getWriter();
            out.println(lookupStr);
        } catch (IOException ex) {
            Logger.getLogger(LoadDocumentDropDownListCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "LoadDocumentDropDownListCommand";
    }
    
}

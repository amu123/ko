/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.document.command;

import agility.za.indexdocumenttype.CopyDocType;
import agility.za.indexdocumenttype.IndexDocumentRequest;
import agility.za.indexdocumenttype.IndexDocumentResponse;
import agility.za.indexdocumenttype.MergeDocType;
import com.koh.command.NeoCommand;
import com.koh.serv.PropertiesReader;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.NeoUser;
import org.apache.log4j.Logger;

/**
 *
 * @author Christo
 */
public class DocumentIndexCopyCommand extends NeoCommand {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            logger.info("------------DocumentIndexCopyCommand---------------------");
            String selectedFile = request.getParameter("selectedFile");
            String profileType = (String)request.getSession().getAttribute("profile")!=null?(String)request.getSession().getAttribute("profile"):"";
            String workDrive = new PropertiesReader().getProperty("DocumentIndexWorkDrive");


            String folderList = (String) request.getParameter("folderList");
            String listOrWork = (String) request.getParameter("listOrWork");
            request.setAttribute("folderList", folderList);
            request.setAttribute("listOrWork", listOrWork);
            if (selectedFile == null || selectedFile.equals("")) {
                errorMessage(request, response, "No file selected",profileType);
                return "DocumentIndexDoSplitCommand";
            }
            IndexDocumentRequest req = new IndexDocumentRequest();
            req.setIndexType("Copy");
            req.setSrcDrive(workDrive);
            NeoUser loggedInUser = (NeoUser) request.getSession().getAttribute("persist_user");
            req.setAgent(loggedInUser);
            req.setEntityNumber("n/a");
            CopyDocType copyDocType = new CopyDocType();
            copyDocType.setFolder(folderList+"/"+profileType);
            copyDocType.setFilename(selectedFile);
            copyDocType.setAmount(1);
            req.setCopy(copyDocType);
            System.out.println("Before Webservice");
            IndexDocumentResponse res = NeoCommand.service.getNeoManagerBeanPort().processIndexRequest(req);            
            request.setAttribute("folderList", new PropertiesReader().getProperty("DocumentIndexWorkFolder"));
            request.setAttribute("listOrWork", "Work");
            if(!res.isIsSuccess()){
                errorMessage(request, response, res.getMessage(), profileType);
                return "DocumentIndexCopyCommand";                              
            }            
            RequestDispatcher dispatcher =null;
            if(profileType.toLowerCase().contains("claim")){
                dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentClaims.jsp");
            }else if(profileType.toLowerCase().contains("member")){
                dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentMember.jsp");
            }else{
                dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentGeneric.jsp");
            }
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "DocumentIndexCopyCommand";
    }

    @Override
    public String getName() {
        return "DocumentIndexCopyCommand";

    }

    private void errorMessage(HttpServletRequest request, HttpServletResponse response, String msg, String profileType) throws Exception {
        request.setAttribute("errorMsg", msg);
        RequestDispatcher dispatcher = null;
        if(profileType.toLowerCase().contains("claim")){
                dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentClaims.jsp");
            }else if(profileType.toLowerCase().contains("member")){
                dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentMember.jsp");
            }else{
                dispatcher = request.getRequestDispatcher("/Indexing/IndexDocumentGeneric.jsp");
            }
        dispatcher.forward(request, response);
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.Command;
import com.koh.command.NeoCommand;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.CallTrackSearchCriteria;
import neo.manager.CallWorkbenchDetails;
import neo.manager.MainList;
import neo.manager.NeoManagerBean;
import neo.manager.PdcSearchCriteria;

/**
 *
 * @author princes
 */
public class NextEventHistoryListCommand extends Command {

    private Object _datatypeFactory;
    private int minResult;
    private int maxResult = 100;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        // System.out.println("Inside the CallWorkbenchCommand");
        CallTrackSearchCriteria search = new CallTrackSearchCriteria();
        HttpSession session = request.getSession();
        SimpleDateFormat dateTime = new SimpleDateFormat("yyyy/MM/dd");
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        Collection<MainList> mainList = new ArrayList<MainList>();

        String pageDirection = request.getParameter("pageDirection");
        String dateFrom = request.getParameter("dateFrom");
        String dateTo = request.getParameter("dateTo");
        PdcSearchCriteria page = (PdcSearchCriteria) session.getAttribute("searchHistory");

        if (page.getMinResult() == 0) {
            minResult = page.getMinResult();
            maxResult = page.getMaxResult();
        }

        if (page.getMaxResult() == 0) {
            page.setMaxResult(maxResult);
        }

        try {
            if (dateFrom != null && !dateFrom.equalsIgnoreCase("")) {
                Date callDateFrom = dateTime.parse(dateFrom);

                page.setDateFrom(convertDateXML(callDateFrom));
                session.setAttribute("dateFrom", page.getDateFrom());
            } else if (session.getAttribute("dateFrom") != null) {

                XMLGregorianCalendar xmlDateFrom = (XMLGregorianCalendar) session.getAttribute("dateFrom");

                page.setDateFrom(xmlDateFrom);
            }
            if (dateTo != null && !dateTo.equalsIgnoreCase("")) {

                Date callDateTo = dateTime.parse(dateTo);
                page.setDateTo(convertDateXML(callDateTo));
                session.setAttribute("dateTo", page.getDateTo());
            } else if (session.getAttribute("dateTo") != null) {

                XMLGregorianCalendar xmlDateTo = (XMLGregorianCalendar) session.getAttribute("dateTo");

                page.setDateTo(xmlDateTo);
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }

        if (pageDirection != null && pageDirection.equalsIgnoreCase("forward")) {

            minResult = minResult + 100;
            maxResult = maxResult + 100;
            page.setMinResult(minResult);
            page.setMaxResult(maxResult);
        }

        if (pageDirection != null && pageDirection.equalsIgnoreCase("backward")) {

            minResult = minResult - 100;
            maxResult = maxResult - 100;
            page.setMaxResult(maxResult);
            page.setMinResult(minResult);

        }

        mainList = NeoCommand.service.getNeoManagerBeanPort().getPolicyHolderHistoryEvents(page);
        session.setAttribute("mainList", mainList);

        try {
            String nextJSP = "/PDC/EventsHistory.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public XMLGregorianCalendar convertDateXML(Date date) {

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getDatatypeFactory().newXMLGregorianCalendar(calendar);

    }

    private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }

        return (DatatypeFactory) _datatypeFactory;
    }

    @Override
    public String getName() {
        return "NextEventHistoryListCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AddressDetails;
import neo.manager.CoverDetails;
import neo.manager.NeoManagerBean;

/**
 *
 * @author princes
 */
public class ViewAddressCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        NeoManagerBean port = service.getNeoManagerBeanPort();

        Integer entity = (Integer) session.getAttribute("entityId");
        String coverNumber = (String) session.getAttribute("policyNumber");
        int entityId = entity;
        boolean isPrinciple = false;
        CoverDetails coverDetailsObj = port.getPrincipalMemberDetailsToday(coverNumber);

        if (coverDetailsObj != null) {
            if (coverDetailsObj.getEntityId() == entity && coverDetailsObj.getDependentTypeId() == 17) {
                isPrinciple = true;
            }
        }
        session.setAttribute("isPrinciple", isPrinciple);
        ArrayList<AddressDetails> addsList = (ArrayList<AddressDetails>) port.getAllAddressDetails(entityId);

        for (AddressDetails ad : addsList) {
            if (ad.getAddressType().equals("Physical")) {
                session.setAttribute("PhysicalAddress", ad.getAddressLine1());
                session.setAttribute("PhysicalAddress1", ad.getAddressLine2());
                session.setAttribute("patientPhysicalAddress2", ad.getAddressLine3());
                session.setAttribute("PhysicalCity", ad.getTown());
                session.setAttribute("PhysicalCode", ad.getPostalCode());
            } else if (ad.getAddressType().equals("Postal")) {
                session.setAttribute("PostalAddress", ad.getAddressLine1());
                session.setAttribute("PostalAddress1", ad.getAddressLine2());
                session.setAttribute("PostalAddress2", ad.getAddressLine3());
                session.setAttribute("PostalCity", ad.getTown());
                session.setAttribute("PostalCode", ad.getPostalCode());

            }
        }

        try {
            String nextJSP = "/PDC/PolicyHolderAddressDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "ViewAddressCommand";
    }
}

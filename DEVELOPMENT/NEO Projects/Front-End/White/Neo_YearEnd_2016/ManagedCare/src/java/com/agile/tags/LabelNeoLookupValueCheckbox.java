/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.agile.tags;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.LookupValue;

/**
 *
 * @author josephm
 */
public class LabelNeoLookupValueCheckbox extends TagSupport {
    private String elementName;
    private String displayName;
    private String lookupId;
    private String javascript;
     
    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            HttpSession session = pageContext.getSession();
            out.println("<td colspan=\"2\"><table width=\"300\" align=\"left\">");
            out.println("<tr><td align=\"left\"><label class=\"header\">"+ displayName +"</label></br></br></td></tr>");
            out.println("<tr align=\"left\" valign=\"top\"><td>");
            
            String product = (String) session.getAttribute("product");
            
            ArrayList<LookupValue> lookUps =(ArrayList<LookupValue>) NeoCommand.service.getNeoManagerBeanPort().getCodeTable(new Integer(lookupId));
            for (int i = 0; i < lookUps.size(); i++) {
                LookupValue lookupValue = lookUps.get(i);
                String selectedBox = "" + session.getAttribute(elementName + "_" + i);
                if (selectedBox.isEmpty() || selectedBox.equalsIgnoreCase("null")) {
                selectedBox = "" + pageContext.getRequest().getAttribute(elementName + "_" + i);
                }
                if(product != null && product.equals("2")){
                    if(!lookupValue.getId().toString().equals("QC7")){
                        if (lookupValue.getId().toString().equalsIgnoreCase(selectedBox)) {
                            out.print("<div><input type=\"checkbox\" name=\"" + elementName + "_" + i + "\" id=\"" + elementName + "_" + i + "\" value=\"" + lookupValue.getId() + "\" checked><label>" + lookupValue.getValue() + "</label></input></div>");
                            //out.println("<div><input type=\"checkbox\" value=\"" + lookupValue.getId() + "\" checked=\"checked\" /><label>" + lookupValue.getValue() + "</label></div>");
                        } else {
                            out.print("<div><input type=\"checkbox\" name=\"" + elementName + "_" + i + "\" id=\"" + elementName + "_" + i + "\" value=\"" + lookupValue.getId() + "\" ><label>" + lookupValue.getValue() + "</label></div>");
                            //out.println("<div><input type=\"checkbox\" value=\"" + lookupValue.getId() + "\" /><label>" + lookupValue.getValue() + "</label></div>");
                        }
                    }
                }
                else{
                    if (lookupValue.getId().toString().equalsIgnoreCase(selectedBox)) {
                        out.print("<div><input type=\"checkbox\" name=\"" + elementName + "_" + i + "\" id=\"" + elementName + "_" + i + "\" value=\"" + lookupValue.getId() + "\" checked><label>" + lookupValue.getValue() + "</label></input></div>");
                        //out.println("<div><input type=\"checkbox\" value=\"" + lookupValue.getId() + "\" checked=\"checked\" /><label>" + lookupValue.getValue() + "</label></div>");
                    } else {
                        out.print("<div><input type=\"checkbox\" name=\"" + elementName + "_" + i + "\" id=\"" + elementName + "_" + i + "\" value=\"" + lookupValue.getId() + "\" ><label>" + lookupValue.getValue() + "</label></div>");
                        //out.println("<div><input type=\"checkbox\" value=\"" + lookupValue.getId() + "\" /><label>" + lookupValue.getValue() + "</label></div>");
                    }
                }
            }
            out.println("</td><tr></table></td>");
        } catch (IOException ex) {
            Logger.getLogger(LabelNeoLookupValueCheckbox.class.getName()).log(Level.SEVERE, null, ex);
        }
        return super.doEndTag();
    }


    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application;

import com.koh.employer.command.TabUtils;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class MemberAppChronicDetailsMapping {

    private static final String[][] FIELD_MAPPINGS = {
        {"memberAppNumber","Application Number","no", "int", "MemberApp","ApplicationNumber"},
        {"memberAppChronicId","Hospital Id","no", "int", "MemberApp","chronicDetailId"},
        {"Chronic_detail_date","Date","yes", "date", "MemberApp","startDate"},
        {"Chronic_detail_dependent","Dependant","yes", "int", "MemberApp","dependentNumber"},
        {"Chronic_detail_medication","Details","yes", "str", "MemberApp","medication"},
        {"Chronic_detail_condition","Reason","yes", "str", "MemberApp","condition"}
    };
    
    private static final Map<String,String[]> FIELD_MAP;

    static {
        HashMap hm = new HashMap<String,String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }

    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
        return errors;
    }

    public static MemAppChronicDetail getMemAppChronicDetail(HttpServletRequest request,NeoUser neoUser) {
        MemAppChronicDetail ma = (MemAppChronicDetail)TabUtils.getDTOFromRequest(request, MemAppChronicDetail.class, FIELD_MAPPINGS, "MemberApp");
        ma.setSecurityGroupId(neoUser.getSecurityGroupId());
        ma.setLastUpdatedBy(neoUser.getUserId());
        ma.setCreatedBy(neoUser.getUserId());
        return ma;
    }

    public static void setMemAppChronicDetail(HttpServletRequest request, MemAppChronicDetail ma) {
        TabUtils.setRequestFromDTO(request, ma, FIELD_MAPPINGS, "MemberApp");
    }
    
}

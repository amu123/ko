/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.calltrack.command;

import com.koh.auth.dto.MemberSearchResult;
import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.CoverSearchCriteria;
import neo.manager.EAuthCoverDetails;

/**
 *
 * @author josephm
 */
public class MemberSearchAjaxCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
       String coverNumber = request.getParameter("memberNumber_text");
       CoverSearchCriteria search = new CoverSearchCriteria();
       
        if (coverNumber != null && !coverNumber.equals("")){
            search.setCoverNumber(coverNumber);
        }
       // System.out.println("The value of the memberNo is " + coverNumber);
        ArrayList<MemberSearchResult> members = new ArrayList<MemberSearchResult>();
       
       List<EAuthCoverDetails> coverList = new ArrayList<EAuthCoverDetails>();

       try {
           PrintWriter out = response.getWriter();
           coverList = service.getNeoManagerBeanPort().eAuthCoverSearch(search);

               for(EAuthCoverDetails cd : coverList) {

              MemberSearchResult member = new MemberSearchResult();
              member.setMemberNumber(cd.getCoverNumber());
              member.setDependant("0" + cd.getDependentNumber());
              member.setName(cd.getCoverName());
              member.setSurname(cd.getCoverSurname());
              member.setScheme(cd.getProduct());
              member.setPlan(cd.getOption());
              member.setBenefitFromDate(cd.getCoverStartDate().toGregorianCalendar().getTime());
              member.setBenefitToDate(cd.getCoverEndDate().toGregorianCalendar().getTime());
              //member.setCoverStatus(cd.getCoverStatus());
              //member.setDependentCode(cd.getDependentNumber());
              //member.setDependentType(cd.getDependantType());
              members.add(member);

              if(coverList != null) {

                  out.println(getName() + "|memberNumber="  + member.getMemberNumber());
              }else {
                       out.println("Error|No such member" + getName());
                    }
               }
       }
       catch(Exception ex) {

           ex.printStackTrace();
       }

       return null;
    }

    @Override
    public String getName() {
        return "MemberSearchAjaxCommand";
    }
}

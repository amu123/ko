/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import com.koh.command.NeoCommand;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.LookupValue;

/**
 *
 * @author princes
 */
public class DropDownPerPatientLabel extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String displayName;
    private String elementName;
    private String sessionValue;
    private String javaScript;
    private String lookupId;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @return
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        try {
            out.println("<td><label class=\"label\">" + displayName + ":</label></td>");
            out.println("<td><select name=\"" + elementName + "\" id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");
            //String selectedBox = "" + session.getAttribute(elementName);
            HashMap<Integer, Integer> authType = (HashMap<Integer, Integer>) session.getAttribute(sessionValue);
            //Neo Lookups
            List<LookupValue> lookUps = NeoCommand.service.getNeoManagerBeanPort().getCodeTable(new Integer(lookupId));
            out.println("<option value=\"\" >Select Type</option>");
            if (authType != null && authType.size() > 0) {
                for (LookupValue lookupValue : lookUps) {
                    int lid = new Integer(lookupValue.getId());
                    if (lid != 13) {
                        if (authType.containsValue(lid)) {
                            out.println("<option value=\"" + lookupValue.getId() + "\" >" + lookupValue.getValue() + "</option>");
                        }
                    }
                }
            }
            /*
             for (int i = 0; i < lookUps.size(); i++) {

             System.out.println("--------------------------------------- " + authType.get(i));
             if (authType.containsValue(i)) {
             System.out.println("Yes, Name" + lookUps.get(i).getValue() + " ID " + lookUps.get(i).getId());
             LookupValue lookupValue = lookUps.get(i-1);
             out.println("<option value=\"" + lookupValue.getId() + "\" >" + lookupValue.getValue() + "</option>");
             }else{
             System.out.println("No " + authType.containsValue(i-1));
             }
             }*/
            out.println("</select></td>");

        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelNeoLookupValueDropDown tag", ex);
        }
        return super.doEndTag();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setSessionValue(String sessionValue) {
        this.sessionValue = sessionValue;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }
}

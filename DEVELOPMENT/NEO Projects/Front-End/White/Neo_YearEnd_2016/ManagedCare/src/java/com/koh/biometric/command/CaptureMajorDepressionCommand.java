/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.lang.Exception;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author josephm
 */
public class CaptureMajorDepressionCommand extends NeoCommand {

    private String nextJSP;
    private Object _datatypeFactory;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
        boolean checkError = false;
        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        //clearAllFromSession(request);

        MajorDepressionBiometrics depression = new MajorDepressionBiometrics();
        depression.setBiometricTypeId("9");

        NeoManagerBean port = service.getNeoManagerBeanPort();

        //String hamdScore = request.getParameter("hamdScore");//HAM-D Score
        String hamdScore = (String) session.getAttribute("hamdScore_text");
        if (hamdScore != null && !hamdScore.equalsIgnoreCase("")) {
            if (hamdScore.matches("([0-9]+(\\.[0-9]+)?)+")) {
                double hamdScoreValue = Double.parseDouble(hamdScore);
                if (hamdScoreValue > 24 || hamdScoreValue < 0) {
                    session.setAttribute("hamdScore_error", "HAM-D score must be between 0 and 24");
                    checkError = true;
                } else {
                    depression.setHamdScore(hamdScoreValue);
                    session.removeAttribute("hamdScore_text");
                }
            } else {

                session.setAttribute("hamdScore_error", "The HAM-D score should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("hamdScore_error", "The HAM-D score should be filled in");
         checkError = true;
         }*/

        //String detail = request.getParameter("detail");
        String detail = (String) session.getAttribute("depressionDetail_text");
        if (detail != null && !detail.equalsIgnoreCase("")) {

            depression.setDetail(detail);
            session.removeAttribute("depressionDetail_text");
        } /*else {

         session.setAttribute("detail_error", "The detail should be filled in");
         checkError = true;
         }*/


        //String exerciseInWeek = request.getParameter("exerciseQuestion");
        String exerciseInWeek = (String) session.getAttribute("exerciseQuestion_text");
        if (exerciseInWeek != null && !exerciseInWeek.equalsIgnoreCase("")) {
            if (exerciseInWeek.matches("[0-9]+")) {
                int exerciseInWeekValue = Integer.parseInt(exerciseInWeek);
                depression.setExercisePerWeek(exerciseInWeekValue);
            } else {

                session.setAttribute("exerciseQuestion_error", "The exercise field should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("exerciseQuestion_error", "The exercise field should be filled in");
         checkError = true;
         }*/

        //String currentSmoker = request.getParameter("currentSmoker");
        String currentSmoker = (String) session.getAttribute("currentSmoker_text");
        if (currentSmoker != null && !currentSmoker.equalsIgnoreCase("99")) {
            depression.setCurrentSmoker(currentSmoker);
            LookupValue smoker = new LookupValue();
            smoker.setId(currentSmoker);
            smoker.setValue(port.getValueFromCodeTableForId("YesNo Type", currentSmoker));
            depression.setCurrentSmokerLookup(smoker);
        } /*else {
         session.setAttribute("currentSmoker_error", "The current smoker field should be filled in");
         checkError = true;
         }*/

        //String numberOfCigaretes = request.getParameter("smokingQuestion");
        String numberOfCigaretes = (String) session.getAttribute("smokingQuestion_text");
        if (numberOfCigaretes != null && !numberOfCigaretes.equalsIgnoreCase("")) {
            if (numberOfCigaretes.matches("[0-9]+")) {
                int numberOfCigaretesValue = Integer.parseInt(numberOfCigaretes);
                depression.setCigarettesPerDay(numberOfCigaretesValue);
            } else {

                session.setAttribute("smokingQuestion_error", "The cigarettes field should be a numeric value");
                checkError = true;
            }

        } /*else {

         session.setAttribute("smokingQuestion_error", "The cigarettes field should be filled in");
         checkError = true;
         }*/

        //String exSmoker = request.getParameter("exSmoker");
        String exSmoker = (String) session.getAttribute("exSmoker_text");
        if (exSmoker != null && !exSmoker.equalsIgnoreCase("99")) {
            depression.setExSmoker(exSmoker);
        } /*else {

         session.setAttribute("exSmoker_error", "The ex smoker field should be filled in");
         checkError = true;
         }*/

        //String yearsSinceSmoking = request.getParameter("stopped");
        String yearsSinceSmoking = (String) session.getAttribute("stopped_text");
        if (yearsSinceSmoking != null && !yearsSinceSmoking.equalsIgnoreCase("")) {
            if (yearsSinceSmoking.matches("[0-9]+")) {
                int yearsSinceSmokingValue = Integer.parseInt(yearsSinceSmoking);
                depression.setYearsSinceStopped(yearsSinceSmokingValue);
                session.setAttribute("stopped_error", "");
            } else {
                session.setAttribute("stopped_error", "The years stopped field should be a numeric value");
                checkError = true;
            }
        }

        //String alcoholUnits = request.getParameter("alcoholConsumption");
        String alcoholUnits = (String) session.getAttribute("alcoholConsumption_text");
        if (alcoholUnits
                != null && !alcoholUnits.equalsIgnoreCase(
                "")) {
            if (alcoholUnits.matches("[0-9]+")) {
                int alcoholUnitsValue = Integer.parseInt(alcoholUnits);
                depression.setAlcoholUnitsPerWeek(alcoholUnitsValue);
            } else {

                session.setAttribute("alcoholConsumption_error", "The alcohol units field should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("alcoholConsumption_error", "The alcohol units field should be filled in");
         checkError = true;
         }*/

        //String weight = request.getParameter("weight");
        //String height = request.getParameter("length");
        String weight = (String) session.getAttribute("weight_text");
        String height = (String) session.getAttribute("length_text");
        if (weight != null && !weight.equalsIgnoreCase("")) {
            if (weight.matches("([0-9]+(\\.[0-9]+)?)+")) {
                if (height != null && !height.equalsIgnoreCase("")) {
                    if (height.matches("([0-9]+(\\.[0-9]+)?)+")) {
                        double heightValue = Double.parseDouble(height);
                        double weightValue = Double.parseDouble(weight);
                        depression.setHeight(heightValue);
                        depression.setWeight(weightValue);
                    } else {
                        session.setAttribute("length_error", "The length should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("length_error", "Require length to calculate BMI");
                    checkError = true;
                }
            } else {

                session.setAttribute("weight_error", "The weight should be a numeric value");
                checkError = true;
            }
        }/*else {

         session.setAttribute("weight_error", "The weight should be filled in");
         checkError = true;
         }*/

        if (height != null && !height.equalsIgnoreCase("")) {
            if (height.matches("([0-9]+(\\.[0-9]+)?)+")) {
                if (weight != null && !weight.equalsIgnoreCase("")) {
                    if (weight.matches("([0-9]+(\\.[0-9]+)?)+")) {
                        double heightValue = Double.parseDouble(height);
                        double weightValue = Double.parseDouble(weight);
                        depression.setHeight(heightValue);
                        depression.setWeight(weightValue);
                    } else {
                        session.setAttribute("weight_error", "The weight should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("weight_error", "Require weight to calculate BMI");
                    checkError = true;
                }
            } else {

                session.setAttribute("length_error", "The length should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("length_error", "The length should be filled in");
         checkError = true;
         }*/

        //String bmi = request.getParameter("bmi");
        String bmi = (String) session.getAttribute("bmi_text");
        if (bmi
                != null && !bmi.equalsIgnoreCase(
                "")) {
            if (bmi.matches("([0-9]+(\\.[0-9]+)?)+")) {
                double bmiValue = Double.parseDouble(bmi);
                depression.setBmi(bmiValue);
            } else {

                session.setAttribute("bmi_error", "The BMI should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bmi_error", "The BMI should be filled in");
         checkError = true;
         }*/

        //String bloodPresureSystolic = request.getParameter("bpSystolic");
        //String bloodPressureDiastolic = request.getParameter("bpDiastolic");
        String bloodPresureSystolic = (String) session.getAttribute("bpSystolic_text");
        String bloodPressureDiastolic = (String) session.getAttribute("bpDiastolic_text");
        if (bloodPresureSystolic != null && !bloodPresureSystolic.equalsIgnoreCase("")) {
            if (bloodPresureSystolic.matches("[0-9]+")) {
                if (bloodPressureDiastolic != null && !bloodPressureDiastolic.equalsIgnoreCase("")) {
                    if (bloodPressureDiastolic.matches("[0-9]+")) {
                        int bloodvalueSys = Integer.parseInt(bloodPresureSystolic);
                        int bloodValueDia = Integer.parseInt(bloodPressureDiastolic);
                        depression.setBloodPressureSystolic(bloodvalueSys);
                        depression.setBloodPressureDiastolic(bloodValueDia);
                    } else {

                        session.setAttribute("bpDiastolic_error", "The blood pressure should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("bpDiastolic_error", "The blood pressure should be filled in");
                    checkError = true;
                }
            } else {

                session.setAttribute("bpSystolic_error", "The blood pressure should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bpSystolic_error", "The blood pressure should be filled in");
         checkError = true;
         }*/

        if (bloodPressureDiastolic != null && !bloodPressureDiastolic.equalsIgnoreCase("")) {
            if (bloodPressureDiastolic.matches("[0-9]+")) {
                if (bloodPresureSystolic != null && !bloodPresureSystolic.equalsIgnoreCase("")) {
                    if (bloodPresureSystolic.matches("[0-9]+")) {
                        int bloodvalueSys = Integer.parseInt(bloodPresureSystolic);
                        int bloodValueDia = Integer.parseInt(bloodPressureDiastolic);
                        depression.setBloodPressureSystolic(bloodvalueSys);
                        depression.setBloodPressureDiastolic(bloodValueDia);
                    } else {

                        session.setAttribute("bpSystolic_error", "The blood pressure should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("bpSystolic_error", "The blood pressure should be filled in");
                    checkError = true;
                }
            } else {

                session.setAttribute("bpDiastolic_error", "The blood pressure should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bpDiastolic_error", "The blood pressure should be filled in");
         checkError = true;
         }*/

//        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String UpdateBiometrics = (String) session.getAttribute("UpdateBiometrics");
//        if (UpdateBiometrics != null && UpdateBiometrics.equals("true")) {
//            dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd");
//        }
        try {
            //String measured = request.getParameter("dateMeasured");
            String measured = (String) session.getAttribute("dateMeasured_text");
            if (measured != null && !measured.equalsIgnoreCase("")) {
//                Date dateMeasured = dateTimeFormat.parse(request.getParameter("dateMeasured"));
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                Pattern pattern = Pattern.compile("\\s");
                Matcher matcher = pattern.matcher(measured);
                boolean found = matcher.find();

                Date tFrom = null;
                XMLGregorianCalendar xTreatFrom = null;

                if (!found) {
                    tFrom = dateFormat.parse(measured);
                } else {
                    tFrom = dateTimeFormat.parse(measured);
                }

                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                depression.setDateMeasured(xTreatFrom);
            } else {
                session.setAttribute("dateMeasured_error", "The date measured should be filled in");
                checkError = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        try {
            //String receivedDate = request.getParameter("dateReceived");
            String receivedDate = (String) session.getAttribute("dateReceived_text");
            if (receivedDate != null && !receivedDate.equalsIgnoreCase("")) {
//                Date dateReceived = dateFormat.parse(receivedDate);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                Pattern pattern = Pattern.compile("\\s");
                Matcher matcher = pattern.matcher(receivedDate);
                boolean found = matcher.find();

                Date tFrom = null;
                XMLGregorianCalendar xTreatFrom = null;

                if (!found) {
                    tFrom = dateFormat.parse(receivedDate);
                } else {
                    tFrom = dateTimeFormat.parse(receivedDate);
                }

                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                depression.setDateReceived(xTreatFrom);
            } else {

                session.setAttribute("dateReceived_error", "The date received should be filled in");
                checkError = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //String treatingProvider = request.getParameter("providerNumber_text");
        String treatingProvider = (String) session.getAttribute("providerNumber_text");
        if (treatingProvider
                != null && !treatingProvider.equalsIgnoreCase(
                "")) {

            depression.setTreatingProvider(treatingProvider);
        } /*else {

         session.setAttribute("providerNumber_error", "The treating provider should be filled in");
         checkError = true;
         }*/

        //String source = request.getParameter("source");
        String source = (String) session.getAttribute("source_text");
        if (source
                != null && !source.equalsIgnoreCase(
                "99")) {

            depression.setSource(source);
        } /*else {

         session.setAttribute("source_error", "The source should be filled in");
         checkError = true;
         }*/

        //String icd10 = request.getParameter("icd10");
        //String icd10 = (String) session.getAttribute("icd10_text");
        String icd10 = (String) session.getAttribute("depressionLookupValue");
        if (icd10 != null && !icd10.equalsIgnoreCase("99")) {
            depression.setIcd10(icd10);
            LookupValue icd = new LookupValue();
            icd.setId(icd10);
            icd.setValue(port.getValueFromCodeTableForId("Depression Biometric ICD", icd10));
            depression.setIcd10Lookup(icd);
        } else {

            session.setAttribute("icd10_error", "The ICD10 should be filled in");
            checkError = true;
        }

        //String memberNumber = request.getParameter("memberNumber_text");
        String memberNumber = (String) session.getAttribute("memberNumber_text");
        if (memberNumber != null && !memberNumber.equalsIgnoreCase("")) {
            depression.setMemberNumber(memberNumber);
        } else {

            session.setAttribute("memberNumber_error", "The member number should be filled in");
            checkError = true;
        }

        //String dependantCode = request.getParameter("depListValues");
        String dependantCode = (String) session.getAttribute("depListValues_text");
        if (dependantCode != null && !dependantCode.equalsIgnoreCase("99") && !dependantCode.equalsIgnoreCase("")
                && !dependantCode.equalsIgnoreCase("null")) {

            int code = Integer.parseInt(dependantCode);
            depression.setDependantCode(code);
        } else {

            session.setAttribute("depListValues_error", "The cover dependants should be filled in");
            checkError = true;
        }


        try {
            if (checkError) {

                saveScreenToSession(request);
                nextJSP = "/biometrics/CaptureBiometrics.jsp";         //CaptureMajorDepression.jsp";
            } else {
                //Check if the user is UPDATING or CREATING
                if (UpdateBiometrics != null && UpdateBiometrics.equals("true")) {
                    String oldBioID = (String) session.getAttribute("BiometricID");
                    System.out.println("OLDBIOID = " + oldBioID);

                    //clearAllFromSession(request);
                    nextJSP = "/biometrics/CaptureBiometrics.jsp";         //CaptureMajorDepression.jsp";
                    int userId = user.getUserId();
                    BiometricsId bId = port.saveMajorDepressionBiometrics(depression, userId, 1, oldBioID);
                    request.setAttribute("revNo", bId.getReferenceNo());
                    int depressionId = bId.getId();
                    if (depressionId > 0) {
                        depression.setBiometricsId(depressionId);
//                      port.createTrigger(depression);
                        session.setAttribute("updated", "Your biometrics data has been captured");
                    } else {

                        session.setAttribute("failed", "Sorry! Major depression Biometrics save has failed, Please try again!");
                        nextJSP = "/biometrics/CaptureBiometrics.jsp";         //CaptureMajorDepression.jsp";
                    }
                } else {
                    //clearAllFromSession(request);
                    nextJSP = "/biometrics/CaptureBiometrics.jsp";         //CaptureMajorDepression.jsp";
                    int userId = user.getUserId();
                    BiometricsId bId = port.saveMajorDepressionBiometrics(depression, userId, 1, null);
                    request.setAttribute("revNo", bId.getReferenceNo());
                    int depressionId = bId.getId();
                    if (depressionId > 0) {
                        depression.setBiometricsId(depressionId);
//                      port.createTrigger(depression);
                        session.setAttribute("updated", "Your biometrics data has been captured");
                    } else {

                        session.setAttribute("failed", "Sorry! Major depression Biometrics save has failed, Please try again!");
                        nextJSP = "/biometrics/CaptureBiometrics.jsp";         //CaptureMajorDepression.jsp";
                    }
                }
            }
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

//    public XMLGregorianCalendar convertDateXML(Date date) {
//
//        GregorianCalendar calendar = new GregorianCalendar();
//        calendar.setTime(date);
//        return getDatatypeFactory().newXMLGregorianCalendar(calendar);
//
//    }
//
//    private DatatypeFactory getDatatypeFactory() {
//        if (_datatypeFactory == null) {
//
//            try {
//                _datatypeFactory = DatatypeFactory.newInstance();
//            } catch (DatatypeConfigurationException ex) {
//                ex.printStackTrace();
//            }
//        }
//
//        return (DatatypeFactory) _datatypeFactory;
//    }

    @Override
    public String getName() {
        return "CaptureMajorDepressionCommand";
    }
}

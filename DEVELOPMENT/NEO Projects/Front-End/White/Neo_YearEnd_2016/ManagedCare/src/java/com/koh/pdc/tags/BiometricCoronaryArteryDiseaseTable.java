/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CoronaryBiometrics;

/**
 *
 * @author princes
 */
public class BiometricCoronaryArteryDiseaseTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String javascript;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        List<CoronaryBiometrics> getCoronary = (List<CoronaryBiometrics>) session.getAttribute("getCoronary");
        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        String dateMeasured;
        try {

            out.print("<tr>");
            out.print("<th scope=\"col\">Date Measured</th>");
            out.print("<th scope=\"col\">Height</th>");
            out.print("<th scope=\"col\">Weight</th>");
            out.print("<th scope=\"col\">BMI</th>");
            out.print("<th scope=\"col\">Blood Pressure Systolic</th>");
            out.print("<th scope=\"col\">Blood Pressure Diastolic</th>");
            out.print("<th scope=\"col\">HBA1C(%)</th>");
            out.print("<th scope=\"col\">Total Cholesterol</th>");
            out.print("<th scope=\"col\">LDLC mmol/l</th>");
            out.print("<th scope=\"col\">HDLC mmol/l</th>");
            out.print("<th scope=\"col\">Tryglyceride mmol/l</th>");
            out.print("<th scope=\"col\">TC: HDL ratio</th>");
            out.print("<th scope=\"col\">Select</th>");

//            out.print("<th scope=\"col\">Date Measured</th>");
//            out.print("<th scope=\"col\">ICD10</th>");
//            out.print("<th scope=\"col\">Excercise</th>");            
//            out.print("<th scope=\"col\">Ex Smoker Stopped</th>");
//            out.print("<th scope=\"col\">Alchohol Units</th>");
//            out.print("<th scope=\"col\">BP SBP/DBP mmhg</th>");
//            out.print("<th scope=\"col\">HbAIC%</th>");
//            out.print("<th scope=\"col\">Score Risks</th>");
//            out.print("<th scope=\"col\">Total Cholestrol</th>");
//            out.print("<th scope=\"col\">LDLC mmol/l</th>");
//            out.print("<th scope=\"col\">HDLC mmol/l</th>");
//            out.print("<th scope=\"col\">Tryglyser ide mmol/l</th>");
//            out.print("<th scope=\"col\">BMI</th>");
//            out.print("<th scope=\"col\">TC:HDL Ratio</th>");
            out.print("</tr>");

            if (getCoronary != null && getCoronary.size() > 0) {
                for (CoronaryBiometrics c : getCoronary) {

                    out.print("<tr>");
                    if (c.getDateMeasured() != null) {
                        Date mDate = c.getDateMeasured().toGregorianCalendar().getTime();
                        dateMeasured = formatter.format(mDate);
                        out.print("<td><center><label class=\"label\">" + dateMeasured + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getHeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + c.getHeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getWeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + c.getWeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getBmi() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getBmi() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getBloodPressureSystolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getBloodPressureSystolic() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getBloodPressureDiastolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getBloodPressureDiastolic() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getHbAIC() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getHbAIC() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getTotalCholesterol() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + c.getTotalCholesterol() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getLdlcMol() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getLdlcMol() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getHdlcMol() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getHdlcMol() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getTryglyserideMol() != 0) {
                        out.print("<td><center><label class=\"label\">" + c.getTryglyserideMol() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (c.getTcHdlRatio() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + c.getTcHdlRatio() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("<td><center><button name=\"opperation\" type=\"button\" " + javascript + " value=\"" + c.getBiometricsId() + "\">Select</button></center></td>");

//                    if (c.getDateMeasured() != null) {
//                        Date mDate = c.getDateMeasured().toGregorianCalendar().getTime();
//                        dateMeasured = formatter.format(mDate);
//                        out.print("<td>" + dateMeasured + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getIcd10Lookup() != null) {
//                        out.print("<td>" + c.getIcd10Lookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getExercisePerWeek() != 0) {
//                        out.print("<td>" + c.getExercisePerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }                    
//
//                    if (c.getYearsSinceStopped() > 0) {
//                        out.print("<td>" + c.getYearsSinceStopped() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getAlcoholUnitsPerWeek() != 0) {
//                        out.print("<td>" + c.getAlcoholUnitsPerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getBloodPressureSystolic() != 0) {
//                        out.print("<td>" + c.getBloodPressureSystolic() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//
//                    if (c.getHbAIC() != 0) {
//                        out.print("<td>" + c.getHbAIC() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getScoreRisks() != 0.0) {
//                        out.print("<td>" + c.getScoreRisks() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getTotalCholesterol() != 0) {
//                        out.print("<td>" + c.getTotalCholesterol() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getLdlcMol() != 0) {
//                        out.print("<td>" + c.getLdlcMol() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getHdlcMol() != 0) {
//                        out.print("<td>" + c.getHdlcMol() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getTryglyserideMol() != 0) {
//                        out.print("<td>" + c.getTryglyserideMol() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getBmi() != 0) {
//                        out.print("<td>" + c.getBmi() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (c.getTcHdlRatio() != 0) {
//                        out.print("<td>" + c.getTcHdlRatio() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
                    out.print("</tr>");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.auth.dto;

/**
 *
 * @author gerritj
 */
public class TariffSearchResult {

    private String code;
    private String description;
    private String discipline;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String dicipline) {
        this.discipline = dicipline;
    }

    
    
}

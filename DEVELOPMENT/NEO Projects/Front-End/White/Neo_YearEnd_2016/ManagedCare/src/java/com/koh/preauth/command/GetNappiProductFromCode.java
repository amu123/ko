/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import java.util.Collection;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthNappiProduct;
import neo.manager.NeoManagerBean;
import neo.manager.AuthNappiProduct;

/**
 *
 * @author martins
 */
public class GetNappiProductFromCode extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        this.saveScreenToSession(request);
        HttpSession session = request.getSession();

        String onScreen = "" + session.getAttribute("onScreen");
        String nextJSP = "/PreAuth/AuthNappiProductDetails.jsp";


        NeoManagerBean port = service.getNeoManagerBeanPort();
        String prodID = session.getAttribute("productCode_text").toString();
        String prodName = "";
        String code = "%" + prodID + "%";
        String name = "%" + prodName + "%";

        if (code.length() == 2) {
            session.setAttribute("productCode_error", "Product Code Can Not Be Empty");
        } else {
            List<AuthNappiProduct> list = (List<AuthNappiProduct>) port.getNappiList(code.toString(), name.toString());
            if (list.size() <= 0) {
                session.setAttribute("productCode_error", "No Such Product");
            } else {
                session.setAttribute("productCode_error", "");
                session.setAttribute("prodDesc", list.get(0).getProductDesc());
                session.setAttribute("quantity", list.get(0).getQuantity());
                session.setAttribute("price", list.get(0).getRand());
                session.setAttribute("fixPrice", list.get(0).getRand());
                session.setAttribute("icd_text", list.get(0).getICD10());
                session.setAttribute("status", list.get(0).getStatus());
                session.setAttribute("rejectionReason", list.get(0).getReasonCode());
                session.setAttribute("dateFrom", list.get(0).getDateFrom());
                session.setAttribute("dateTo", list.get(0).getDateTo());
                session.setAttribute("fixPrice", list.get(0).getFixedPrice());
                session.setAttribute("unitPrice", list.get(0).getUnitPrice());
            }
        }


        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "GetNappiProductFromCode";
    }
}

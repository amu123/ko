/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.command.Command;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.AuthTariffDetails;

/**
 *
 * @author johanl
 */
public class RemoveAuthTariffCommand extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;
        ArrayList<AuthTariffDetails> atList = (ArrayList<AuthTariffDetails>) session.getAttribute("tariffListArray");
        if (atList != null && atList.size() != 0) {
            try {
                out = response.getWriter();
                String tCode = request.getParameter("tTableCode");
                String tQuan = request.getParameter("tTableQuan");
                for (AuthTariffDetails at : atList) {

                    if (tCode.equals(at.getTariffCode())) {
                        if (tQuan.equalsIgnoreCase(at.getQuantity())) {

                            session.setAttribute("tariffDesc", at.getTariffDesc());
                            session.setAttribute("tariffCode", at.getTariffCode());
                            session.setAttribute("tariffQuantity", at.getQuantity());
                            session.setAttribute("tariffAmount", at.getAmount());
                            out.print("OK|" + tCode);
                            atList.remove(at);
                            break;
                        }
                    }
                }
                session.setAttribute("tariffListArray", atList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            out.println("Error|");
        }
        try {
            String nextJSP = "/Auth/AuthSpecificTariff.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "RemoveAuthTariffCommand";
    }
}

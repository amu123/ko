/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.auth.command;

import com.koh.auth.dto.PracticeSearchResult;
import com.koh.command.NeoCommand;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.ProviderDetails;
import neo.manager.ProviderSearchCriteria;

/**
 *
 * @author whauger
 */
public class SearchPracticeCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        String practiceDiscipline = request.getParameter("pracType");
        String practiceNumber = request.getParameter("pracNo");
        String practiceName = request.getParameter("pracName");
        String authType = "" + session.getAttribute("authType");
        String provDesc = "" + session.getAttribute("facilityProvDiscTypeId");
        System.out.println("### practiceDiscipline " + practiceDiscipline);
        System.out.println("### authType " + authType);
        
        if (authType.equalsIgnoreCase("11")) {
            if (practiceDiscipline.equals("047") || practiceDiscipline.equals("049")
                    || practiceDiscipline.equals("059") || practiceDiscipline.equals("079")) {
                search(practiceDiscipline,practiceNumber,practiceName,request);
                System.out.println("### hospic search right");
            }else{
                request.setAttribute("searchPracticeResultMessage", "Only 047, 049, 059, 079 facilities are alowed.");
                System.out.println("hospic search wrong");
            }
        } else {
            search(practiceDiscipline,practiceNumber,practiceName,request);
        }
        try {
            String nextJSP = "/Auth/PracticeSearch.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SearchPracticeCommand";
    }
    
    public void search(String practiceDiscipline, String practiceNumber, String practiceName, HttpServletRequest request){
        ProviderSearchCriteria search = new ProviderSearchCriteria();
            if (practiceDiscipline != null && !practiceDiscipline.equals("")) {
                search.setDiciplineType(practiceDiscipline);
            }
            if (practiceNumber != null && !practiceNumber.equals("")) {
                search.setPracticeNumber(practiceNumber);
            }
            if (practiceName != null && !practiceName.equals("")) {
                search.setPracticeName(practiceName);
            }
            search.setEntityTypeID(3);

            ArrayList<PracticeSearchResult> providers = new ArrayList<PracticeSearchResult>();
            List<ProviderDetails> pdList = new ArrayList<ProviderDetails>();
            try {
                pdList = service.getNeoManagerBeanPort().findAllProvidersByCriteria(search);

                for (ProviderDetails pd : pdList) {
                    PracticeSearchResult practice = new PracticeSearchResult();
                    practice.setPracticeName(pd.getPracticeName());
                    practice.setPracticeNumber(pd.getPracticeNumber());
                    practice.setPracticeType(pd.getDisciplineType().getValue());
                    practice.setPracticeNetwork(pd.getNetworkName());
                    providers.add(practice);
                }

                if (pdList.size() == 0) {
                    request.setAttribute("searchPracticeResultMessage", "No results found.");
                } else if (pdList.size() >= 100) {
                    request.setAttribute("searchPracticeResultMessage", "Only first 100 results shown. Please refine search.");
                } else {
                    request.setAttribute("searchProviderResultMessage", null);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            /*
             ProviderSearchResult p = new ProviderSearchResult();
             p.setProviderName("Lancet");
             p.setProviderNumber("111111");
             p.setProviderType("054");

             ProviderSearchResult p2 = new ProviderSearchResult();
             p2.setProviderName("Lancet");
             p2.setProviderNumber("111111");
             p2.setProviderType("054");

             providers.add(p);
             providers.add(p2);
             */
            request.setAttribute("searchPracticeResult", providers);
    }
}

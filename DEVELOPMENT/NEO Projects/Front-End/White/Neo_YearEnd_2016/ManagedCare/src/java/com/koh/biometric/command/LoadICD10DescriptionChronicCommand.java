/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.Diagnosis;
import neo.manager.NeoManagerBean;

/**
 *
 * @author josephm
 */
public class LoadICD10DescriptionChronicCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        String icd10 = request.getParameter("icd10");

        NeoManagerBean port = service.getNeoManagerBeanPort();

        String icd10Code = port.getValueFromCodeTableForTableId(113, icd10);

        Diagnosis diagnosis = port.getDiagnosisForCode(icd10Code);

        try {

            PrintWriter out = response.getWriter();
            session.setAttribute("icdDescription", diagnosis.getDescription());

            if (icd10 != null && !icd10.equalsIgnoreCase("")) {

                out.print(getName() + "|Description=" + diagnosis.getDescription() + "$");
            } else {

                out.print("|Error|No such Such Diagnosis " + getName());
            }

        }catch(Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "LoadICD10DescriptionChronicCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.PreAuthDetails;
import neo.manager.PreAuthSearchCriteria;

/**
 *
 * @author princes
 */
public class PreviewAuthCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        String coverNumber = (String) session.getAttribute("policyNumber");
        String dateTo = request.getParameter("dateTo");
        String dateFrom = request.getParameter("dateFrom");

        String depenNo = (String) session.getAttribute("dependantNumber").toString();
        request.setAttribute("pdcAuthResultMsg", "");

        if (dateFrom == null || dateFrom.equalsIgnoreCase("")
                || dateFrom.equalsIgnoreCase("null")) {
            //set fixed 6month history search
            Date today = new Date(System.currentTimeMillis());
            Calendar cal = Calendar.getInstance();
            cal.setTime(today);
            cal.add(Calendar.MONTH, -6);
            Date startDate = cal.getTime();

            dateFrom = DateTimeUtils.dateFormat.format(startDate).substring(0, 8) + "01";
            dateTo = DateTimeUtils.dateFormat.format(today);

            request.setAttribute("pdcAuthResultMsg", "Auth detail retrieved  for the last 6 months history");
        }

        session.setAttribute("dateTo", dateTo);
        session.setAttribute("dateFrom", dateFrom);

        PreAuthSearchCriteria search = new PreAuthSearchCriteria();

        if ((dateFrom != null && !dateFrom.equalsIgnoreCase("")) && (dateTo != null && !dateTo.equalsIgnoreCase(""))) {

            Date tFrom = null;
            Date tTo = null;
            XMLGregorianCalendar xTreatFrom = null;
            XMLGregorianCalendar xTreatTo = null;

            try {
                tFrom = DateTimeUtils.dateFormat.parse(dateFrom);
                tTo = DateTimeUtils.dateFormat.parse(dateTo);

                xTreatFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(tFrom);
                xTreatTo = DateTimeUtils.convertDateToXMLGregorianCalendar(tTo);

                search.setAuthDate(xTreatFrom);
                search.setAuthDateTo(xTreatTo);
                search.setPdcAuth(true);

            } catch (java.text.ParseException ex) {
                Logger.getLogger(PreviewAuthCommand.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        search.setCoverNumber(coverNumber);
        search.setDependantCode(new Integer(depenNo));
        Collection<PreAuthDetails> details = service.getNeoManagerBeanPort().findAllPreAuthDetailsByCriteria(search);
        HashMap<Integer, Integer> authType = new HashMap<Integer, Integer>();
        if (details != null && !details.isEmpty()) {
            for (PreAuthDetails detail : details) {

                authType.put(new Integer(detail.getAuthType()), new Integer(detail.getAuthType()));

                session.setAttribute("authList", authType);
                session.setAttribute("authDetails", details);
            }
        } else {
            request.setAttribute("pdcAuthResultMsg", "No results found, Please update search criteria");
        }
        try {
            String nextJSP = "/PDC/AuthorizationDetails.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getName() {
        return "PreviewAuthCommand";
    }
}

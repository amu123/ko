/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.employer.command;

import com.koh.command.NeoCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yuganp
 */
public class SaveEmployerPaymentCommand extends NeoCommand {
    public static final String FIELD_PREFIX = "Employer_";
    public static final String[] REQUIRED_FIELDS = {};
    public static final String[] REQUIRED_FIELDS_DESCRIPTION = {};

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String result = validateAndSave(request);
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SaveEmployerPaymentCommand";
    }

    private String validateAndSave(HttpServletRequest request) {
        Map<String, String> errors = TabUtils.checkMandatory(request, FIELD_PREFIX, REQUIRED_FIELDS, REQUIRED_FIELDS_DESCRIPTION);
        validateOtherFields(request, errors);
        String validationErrors = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (!save(request)) {
                status = "ERROR";
            }
        }
        return TabUtils.buildJsonResult(status, validationErrors, updateFields, additionalData);
    }

    private void validateOtherFields(HttpServletRequest request, Map<String, String> errors) {

    }

    private boolean save(HttpServletRequest request) {
        boolean result = false;

        return result;
    }

}

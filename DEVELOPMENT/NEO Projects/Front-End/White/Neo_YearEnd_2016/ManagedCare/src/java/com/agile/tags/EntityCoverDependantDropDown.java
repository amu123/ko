/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.tags;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.CoverDetails;

/**
 *
 * @author johanl
 */
public class EntityCoverDependantDropDown extends TagSupport {

    private String elementName;
    private String displayName;
    private String javaScript;
    private String mandatory = "no";
    private String valueFromSession = "no";
    private String errorValueFromSession = "no";
    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        try {
            out.println("<td align=\"left\" width=\"160px\"><label>" + displayName + ":</label></td>");
            out.println("<td align=\"left\" width=\"200px\"><select style=\"width:215px\" name=\"" + elementName + "\" id=\"" + elementName + "\"");
            if (javaScript != null) {
                out.print(javaScript);
            }
            out.println(">");

            List<CoverDetails> cdList = (List<CoverDetails>) session.getAttribute("eAuthCoverList");
            out.println("<option value=\"99\"></option>");
            if(cdList != null){
                //sort list
                    Collections.sort(cdList, new Comparator<CoverDetails>() {
                        public int compare(CoverDetails o1, CoverDetails o2) {
                            return new Integer(o1.getDependentNumber()).compareTo(o2.getDependentNumber());
                        }
                    });

                System.out.println("cdList size = "+cdList.size());
                if(valueFromSession.trim().equalsIgnoreCase("yes")){
                    String sessionVal = "" + session.getAttribute(elementName);
                    for (CoverDetails cd : cdList) {
                        String depNo = String.valueOf(cd.getDependentNumber());
                        String depType = cd.getDependentType().substring(0, 1);
                        String dob = new SimpleDateFormat("yyyy/MM/dd").format(cd.getDateOfBirth().toGregorianCalendar().getTime());
                        String depInfo = depType + "-" + cd.getName() + "-" + dob;

                        if(sessionVal.equals(depNo)){
                            out.println("<option value="+depNo+" selected >"+ depInfo +"</option>");
                        }else{
                            out.println("<option value="+depNo+">"+ depInfo +"</option>");
                        }
                    }
                }
            }

            out.println("</select></td>");
            if (mandatory.equalsIgnoreCase("yes")) {
                out.println("<td><label class=\"red\">*</label></td>");
            } else {
                out.println("<td></td>");
            }
            //search column
            out.println("<td width=\"30px\"></td>");
            

            String sessionValerror = "";
            if (errorValueFromSession.equalsIgnoreCase("yes")) {

                if (session.getAttribute(elementName + "_error") != null) {
                    sessionValerror = "" + session.getAttribute(elementName + "_error");
                }

                if (sessionValerror != null && !sessionValerror.equalsIgnoreCase("null")) {
                    out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\">" + sessionValerror + "</label></td>");
                } else {
                    out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
                }
            }
            else{
                out.println("<td width=\"200px\" align=\"left\"><label id=\"" + elementName + "_error\" class=\"error\"></label></td>");
            }
            
        } catch (java.io.IOException ex) {
            throw new JspException("Error in LabelNeoLookupValueDropDown tag", ex);
        }

        return super.doEndTag();
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setJavaScript(String javaScript) {
        this.javaScript = javaScript;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public void setValueFromSession(String valueFromSession) {
        this.valueFromSession = valueFromSession;
    }
    
    public void setErrorValueFromSession(String errorValueFromSession) {
        this.errorValueFromSession = errorValueFromSession;
    }

}

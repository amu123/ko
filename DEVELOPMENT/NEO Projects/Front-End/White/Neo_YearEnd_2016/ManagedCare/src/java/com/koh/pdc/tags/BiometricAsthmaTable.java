/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.AsthmaBiometrics;

/**
 *
 * @author princes
 */
public class BiometricAsthmaTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String javascript;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        List<AsthmaBiometrics> getAsthma = (List<AsthmaBiometrics>) session.getAttribute("getAsthma");
        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        String dateMeasured;

        try {

            out.print("<tr>");
            out.print("<th scope=\"col\">Date Measured</th>");
            out.print("<th scope=\"col\">Height</th>");
            out.print("<th scope=\"col\">Weight</th>");
            out.print("<th scope=\"col\">PEF%(% of expected)</th>");
            out.print("<th scope=\"col\">Asthma Day</th>");
            out.print("<th scope=\"col\">Asthma Night</th>");
            out.print("<th scope=\"col\">SABA Use</th>");
            out.print("<th scope=\"col\">Select</th>");

//            out.print("<th scope=\"col\">ICD10</th>");
//            out.print("<th scope=\"col\">Excercise</th>");
//            out.print("<th scope=\"col\">Ex Smoker Years Stopped</th>");
//            out.print("<th scope=\"col\">Alchohol Units</th>");
//            out.print("<th scope=\"col\">Bpef</th>");
//            out.print("<th scope=\"col\">Asthma Day</th>");
//            out.print("<th scope=\"col\">Asthma Night</th>");
//            out.print("<th scope=\"col\">SABA Use</th>");
//            out.print("<th scope=\"col\">Source</th>");
//            out.print("<th scope=\"col\">Notes</th>");
            out.print("</tr>");
            if (getAsthma != null && getAsthma.size() > 0) {
                for (AsthmaBiometrics a : getAsthma) {

                    out.print("<tr>");
                    if (a.getDateMeasured() != null) {
                        Date mDate = a.getDateMeasured().toGregorianCalendar().getTime();
                        dateMeasured = formatter.format(mDate);
                        out.print("<td><center><label class=\"label\">" + dateMeasured + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (a.getHeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + a.getHeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (a.getWeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + a.getWeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (a.getBpef() != 0) {
                        out.print("<td><center><label class=\"label\">" + a.getBpef() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (a.getAsthmaDaySymptomsLookup() != null) {
                        out.print("<td><center><label class=\"label\">" + a.getAsthmaDaySymptomsLookup().getValue() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (a.getAsthmaNightSymptomsLookup() != null) {
                        out.print("<td><center><label class=\"label\">" + a.getAsthmaNightSymptomsLookup().getValue() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (a.getSabaUseLookup() != null) {
                        out.print("<td><center><label class=\"label\">" + a.getSabaUseLookup().getValue() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("<td><center><button name=\"opperation\" type=\"button\" " + javascript + " value=\"" + a.getBiometricsId() + "\">Select</button></center></td>");
//                    
//                    if (a.getIcd10Lookup() != null) {
//                        out.print("<td>" + a.getIcd10Lookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (a.getExercisePerWeek() != 0) {
//                        out.print("<td>" + a.getExercisePerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (a.getYearsSinceStopped() > 0) {
//                        out.print("<td>" + a.getYearsSinceStopped() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (a.getAlcoholUnitsPerWeek() != 0) {
//                        out.print("<td>" + a.getAlcoholUnitsPerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (a.getBpef() != 0) {
//                        out.print("<td>" + a.getBpef() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (a.getAsthmaDaySymptomsLookup() != null) {
//                        out.print("<td>" + a.getAsthmaDaySymptomsLookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//                    if (a.getAsthmaNightSymptomsLookup() != null) {
//                        out.print("<td>" + a.getAsthmaNightSymptomsLookup().getValue()  + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (a.getSabaUseLookup() != null) {
//                        out.print("<td>" + a.getSabaUseLookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (a.getSourceLookup() != null) {
//                        out.print("<td>" + a.getSourceLookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (a.getDetail() != null) {
//                        out.print("<td>" + a.getDetail() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
                    out.print("</tr>");

                }
            }

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

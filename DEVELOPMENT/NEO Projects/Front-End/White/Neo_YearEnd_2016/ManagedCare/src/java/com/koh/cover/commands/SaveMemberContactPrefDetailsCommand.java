/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.cover.commands;

import com.koh.command.NeoCommand;
import com.koh.cover.MemberMainMapping;
import com.koh.employer.command.TabUtils;
import com.koh.fe.command.LoginCommand;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Exception;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.*;

/**
 *
 * @author johanl
 */
public class SaveMemberContactPrefDetailsCommand extends NeoCommand {

    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        String result = validateAndSave(request);
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }              
        
        return null;
    }

    @Override
    public String getName() {
        return "SaveMemberContactPrefDetailsCommand";
    }
    
    private String validateAndSave(HttpServletRequest request) {
        Map<String, String> errors = MemberMainMapping.validateContact(request);
        String validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (save(request)) {
                additionalData = "\"message\":\"Member Contact Preference Details Updated\"";
            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error updating the member contact preference details.\"";
            }
        }
        return TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
    }

    private boolean save(HttpServletRequest request) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        NeoUser neoUser = LoginCommand.getNeoUser(request);
        
        Security sec = new Security();
        sec.setCreatedBy(neoUser.getUserId());
        sec.setLastUpdatedBy(neoUser.getUserId());
        sec.setSecurityGroupId(2);
        
        int entityId = MemberMainMapping.getEntityId(request);
        
        try {
            List<ContactPreference> adList = MemberMainMapping.getContactPrefDetails(request, neoUser, entityId);
            port.saveContactPreferenceDetails(adList, sec, entityId);
            
            return true;
        } catch (Exception e) {
            return false;
        }
    }    
    
}

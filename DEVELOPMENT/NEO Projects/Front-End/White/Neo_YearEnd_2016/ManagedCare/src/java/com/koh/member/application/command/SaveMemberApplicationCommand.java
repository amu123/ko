/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.member.application.command;

import com.koh.command.NeoCommand;
import com.koh.employer.command.TabUtils;
import com.koh.member.application.MemberAppMapping;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Exception;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;

/**
 *
 * @author yuganp
 */
public class SaveMemberApplicationCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        String s = request.getParameter("buttonPressed");
        System.out.println("buttonPressed " + s);

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("SearchBrokerButton".trim())) {
                try {
                    searchBroker(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("SearchEmployerButton".trim())) {
                try {
                    searchGroup(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("SearchConsultantButton".trim())) {
                try {
                    searchConsultant(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("doctorSearch".trim())) {
                try {
                    searchDoctor(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("dentistSearch".trim())) {
                try {
                    searchDentist(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }

        if (s != null && !s.isEmpty()) {
            if (s.equalsIgnoreCase("optometristSearch".trim())) {
                try {
                    searchOptometrist(port, request, response, context);
                } catch (ServletException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaveMemberApplicationCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        }

        String result = validateAndSave(port, request);
        
        try {
            PrintWriter out = response.getWriter();
            out.println(result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SaveMemberApplicationCommand";
    }

    private void searchBroker(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
//            request.setAttribute("MemAppSearchResults", col);
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }

        request.setAttribute("diagCodeId", "MemberApp_brokerCode");
        request.setAttribute("diagNameId", "MemberApp_brokerName");
        request.setAttribute("brokerFirm", "MemberApp_brokerBrokeragaName");
        request.setAttribute("tel", "MemberApp_brokerContactNumber");
        request.setAttribute("fax", "MemberApp_brokerFaxNumber");
        request.setAttribute("email", "MemberApp_brokerEmail");
        request.setAttribute("VIPBroker", "MemberApp_brokerVIP");
        context.getRequestDispatcher("/MemberApplication/MemberBrokerSearch.jsp").forward(request, response);

    }

    private void searchGroup(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
//            request.setAttribute("MemAppSearchResults", col);
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }
        request.setAttribute("diagCodeId", "MemberApp_groupCode");
        request.setAttribute("diagNameId", "MemberApp_groupName");
        String scheme = request.getParameter("MemberApp_productId");

        if (scheme == null || scheme == "" || scheme == "99") {
            request.setAttribute("noProductId", "disabled");
        } else {
            request.setAttribute("ProductId", scheme);
        }

        //request.setAttribute("groupType", "MemberApp_memberCategoty");
        context.getRequestDispatcher("/MemberApplication/MemberAppEmployerSearch.jsp").forward(request, response);

    }

    private void searchDoctor(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }

        request.setAttribute("practiceName", "MemberApp_doctorName");
        request.setAttribute("practicePracticeNumber", "MemberApp_doctorPracticeNumber");
        request.setAttribute("practiceNetwork", "MemberApp_doctorNetwork");
        request.setAttribute("DSPType", "doctor");
        
        context.getRequestDispatcher("/MemberApplication/MemberAppSearchPractice.jsp").forward(request, response);
    }

    private void searchDentist(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }

        request.setAttribute("practiceName", "MemberApp_dentistName");
        request.setAttribute("practicePracticeNumber", "MemberApp_dentistPracticeNumber");
        request.setAttribute("practiceNetwork", "MemberApp_dentistNetwork");
        request.setAttribute("DSPType", "dentist");
        
        context.getRequestDispatcher("/MemberApplication/MemberAppSearchPractice.jsp").forward(request, response);
    }

    private void searchOptometrist(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }

        request.setAttribute("practiceName", "MemberApp_optometristName");
        request.setAttribute("practicePracticeNumber", "MemberApp_optometristPracticeNumber");
        request.setAttribute("practiceNetwork", "MemberApp_optometristNetwork");
        request.setAttribute("DSPType", "optometrist");
        
        context.getRequestDispatcher("/MemberApplication/MemberAppSearchPractice.jsp").forward(request, response);
    }

    private void searchConsultant(NeoManagerBean port, HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
//            request.setAttribute("MemAppSearchResults", col);
        Map map = request.getParameterMap();
        for (Object s : map.keySet()) {
            request.setAttribute(s.toString(), request.getParameter(s.toString()));
            System.out.println(s.toString() + " = " + request.getParameter(s.toString()));
        }

        request.setAttribute("diagCodeId", "MemberApp_brokerConsultantCode");
        request.setAttribute("diagNameId", "MemberApp_brokerName");
        request.setAttribute("tel", "MemberApp_brokerContactNumber");
        request.setAttribute("fax", "MemberApp_brokerFaxNumber");
        request.setAttribute("email", "MemberApp_brokerEmail");
        request.setAttribute("VIPBroker", "MemberApp_brokerVIP");
        context.getRequestDispatcher("/MemberApplication/MemberConsultantSearch.jsp").forward(request, response);

    }

    private String validateAndSave(NeoManagerBean port, HttpServletRequest request) {
        Map<String, String> errors;
        String validationErros;
        if ("1".equals(request.getParameter("MemberApp_application_type"))) {
            errors = new HashMap<String, String>();
            if (request.getParameter("MemberApp_CoverStartDate") == null || request.getParameter("MemberApp_CoverStartDate").equalsIgnoreCase("") || request.getParameter("MemberApp_CoverStartDate").equalsIgnoreCase("null")) {
                errors.put("MemberApp_CoverStartDate_error", "Inception date is required");
            }
        } else {
            errors = MemberAppMapping.validate(request);
        }
        validationErros = TabUtils.convertMapToJSON(errors);

        String checkDublicate = (String) request.getSession().getAttribute("throughAppSearch");

        String memberApp_identificationNumber = (String) request.getParameter("MemberApp_identificationNumber");

        String appStatus = null;

        boolean appValidation = false;

        boolean validUser = false;
        NeoUser user = (NeoUser) request.getSession().getAttribute("persist_user");
        for (SecurityResponsibility res : user.getSecurityResponsibility()) {
            if (res.getResponsibilityId() == 19) {
                validUser = true;
            }
        }

        //validate group code against chosen scheme
        String product = request.getParameter("MemberApp_productId");
        String option = request.getParameter("MemberApp_optionId");
        String name = request.getParameter("MemberApp_groupName");
        String number = request.getParameter("MemberApp_groupCode");
        
        if (product != null && name != null && number != null) {
            if (!product.equalsIgnoreCase("") && !name.equalsIgnoreCase("") && !number.equalsIgnoreCase("")) {
                List<KeyValueArray> groups = port.getEmployerList(name, number, Integer.parseInt(product));
                if (groups.isEmpty()) {
                    if (product.equals("1")) {
                        errors.put("MemberApp_groupCode_error", "Invalid group for Resolution Health scheme");
                    } else if (product.equals("2")) {
                        errors.put("MemberApp_groupCode_error", "Invalid group for Spectramed scheme");
                    }
                }
            }
        }        

        if (option != null && option.equals("99")) {
            errors.put("MemberApp_optionId_error", "Please Select an Option");
        }

        if (checkDublicate == null && memberApp_identificationNumber != null) {
            // Check existing application
            List<KeyValueArray> kva = port.getMemberApplicationList(null, memberApp_identificationNumber, null, null, null);
            if (kva.size() > 0) {

                List<Map<String, String>> list = MapUtils.getMap(kva);
                for (Map<String, String> map : list) {
                    appStatus = map.get("applicationStatus");

                }
                if (appStatus.equals("8")) {
                    if (request.getSession().getAttribute("NTU") == null) {
                        errors.put("MemberApp_identificationNumber_error", "Member has status of Not taken up (NTU). Click save again to continue");
                        request.getSession().setAttribute("NTU", memberApp_identificationNumber);
                    } else if (!request.getSession().getAttribute("NTU").toString().equals(memberApp_identificationNumber)) {
                        errors.put("MemberApp_identificationNumber_error", "Member has status of Not taken up (NTU). Click save again to continue");
                        request.getSession().setAttribute("NTU", memberApp_identificationNumber);
                    }
                } else {
                    appValidation = true;
                    errors.put("MemberApp_identificationNumber_error", "There is an application for this member already");
                }
            }

            // Check existing active cover
            /*
             * CoverSearchCriteria cs = new CoverSearchCriteria();
             * cs.setCoverIDNumber(memberApp_identificationNumber);
             * List<CoverDetails> covList = new ArrayList<CoverDetails>();
             * covList = port.getCoverPrincipleList(cs);
             *
             *
             * if (covList.size() > 0) { for (CoverDetails cov : covList) { if
             * (cov.getStatus().equals("Active")) {
             * errors.put("MemberApp_identificationNumber_error", "Already an
             * active member"); } else if (cov.getStatus().equals("Resign")) {
             * errors.put("MemberApp_identificationNumber_error", "ID number
             * already exist status resigned"); } else if
             * (cov.getStatus().equals("Suspend")) {
             * errors.put("MemberApp_identificationNumber_error", "ID number
             * already exist status suspended"); } } }
             *
             */
            if (!appValidation) {
                List<CoverDetails> existingRec = port.getDependantsForPrincipleMemberByDateAndIdNumber(memberApp_identificationNumber);
                if (existingRec.size() < 0) {
                    existingRec.get(0).getEffectiveEndDate();
                    request.getParameter("MemberApp_CoverStartDate");
                    for (CoverDetails dep : existingRec) {
                        if (dep.getStatus().equals("Active")) {
                            if (!validUser) {
//                                System.out.println("SOMETHNG"+dep.getStatus());
                                errors.put("MemberApp_identificationNumber_error", "Member is already active in another cover");
                            } else if (request.getSession().getAttribute("Active") == null) {
                                errors.put("MemberApp_identificationNumber_error", "Member is already active in another cover. Click save again to override");
                                request.getSession().setAttribute("Active", memberApp_identificationNumber);
                            } else if (!request.getSession().getAttribute("Active").toString().equals(memberApp_identificationNumber)) {
                                errors.put("MemberApp_identificationNumber_error", "Member is already active in another cover. Click save again to override");
                                request.getSession().setAttribute("Active", memberApp_identificationNumber);
                            }
                        } else if (dep.getStatus().equals("Suspend")) {
                            errors.put("MemberApp_identificationNumber_error", "Member is already suspended in another cover");
                        } else if (dep.getStatus().equals("Resign")) {
                            if (request.getSession().getAttribute("Resign") == null) {
                                errors.put("MemberApp_identificationNumber_error", "Member is already resigned in another cover. Click save again to continue");
                                request.getSession().setAttribute("Resign", memberApp_identificationNumber);
                            } else if (!request.getSession().getAttribute("Resign").toString().equals(memberApp_identificationNumber)) {
                                errors.put("MemberApp_identificationNumber_error", "Member is already resigned in another cover. Click save again to continue");
                                request.getSession().setAttribute("Resign", memberApp_identificationNumber);
                            }
                        }
                    }
                }
            }

        }

        validationErros = TabUtils.convertMapToJSON(errors);
        String status = TabUtils.getStatus(errors);
        String additionalData = null;
        String updateFields = null;
        if ("OK".equalsIgnoreCase(status)) {
            if (save(port, request)) {
                MemberApplication ma = (MemberApplication) request.getAttribute("MemberApplication");
                Map<String, String> fieldsMap = new HashMap<String, String>();
                fieldsMap.put("member_header_memberName", ma.getFirstName() + " " + ma.getLastName());
                fieldsMap.put("memberAppType", Integer.toString(ma.getApplicationType()));
                fieldsMap.put("memberAppNumber", Integer.toString(ma.getApplicationNumber()));
                fieldsMap.put("memberAppStatus", Integer.toString(ma.getApplicationStatus()));
                fieldsMap.put("memberAppCoverNumber",ma.getCoverNumber());                

                updateFields = TabUtils.convertMapToJSON(fieldsMap);
                request.getSession().setAttribute("Resign", null);
                request.getSession().setAttribute("NTU", null);
                request.getSession().setAttribute("Active", null);
                additionalData = "\"message\":\"Member Application Saved\"";

            } else {
                status = "ERROR";
                additionalData = "\"message\":\"There was an error saving the member application.\"";
            }
        } else {
            additionalData = "\"message\":\"There were validation errors.\"";
        }
        return TabUtils.buildJsonResult(status, validationErros, updateFields, additionalData);
    }

    private boolean save(NeoManagerBean port, HttpServletRequest request) {
        NeoUser neoUser = getNeoUser(request);
        HttpSession session = request.getSession();
        /**
         * Need to remove *
         */
        if (neoUser == null) {
            neoUser = new NeoUser();
            neoUser.setSecurityGroupId(1);
            neoUser.setUserId(1);
        }
        List<MemberDependantApp> depeList = new ArrayList<MemberDependantApp>();
        try {
            MemberApplication ma = null;
            if (session.getAttribute("Client").equals("Sechaba")) {
                ma = MemberAppMapping.getMemberApplicationSechaba(request, neoUser);
                String networkIPA = request.getParameter("MemberApp_network_IPA");
                ma.setNetworkIpa(networkIPA);
                ma.setDoctorPracticeNumber(ma.getDoctorPracticeNumber());
                ma.setDentistPracticeNumber(ma.getDentistPracticeNumber());
                ma.setOptometristPracticeNumber(ma.getOptometristPracticeNumber());
                ma.setNetworkIpa(ma.getNetworkIpa());
            } else {
                ma = MemberAppMapping.getMemberApplication(request, neoUser);
            }

            Integer sameBank = getIntParam(request, "MemberApp_contribSameBanking");

            if (sameBank == 1) {
                ma.setPaymentsBankId(ma.getContribBankId());
                ma.setPaymentsBankBranchId(ma.getContribBankBranchId());
                ma.setPaymentsAccountType(ma.getContribAccountType());
                ma.setPaymentsAccountHolder(ma.getContribAccountHolder());
                ma.setPaymentsAccountNumber(ma.getContribAccountNumber());
            }
            if (!request.getParameter("MemberApp_application_type").equals("1")) {
                if (request.getParameter("memAppBroker").equals("1")) {
                    ma.setBrokerConsultantId(0);
                } else {
                    ma.setBrokerId(0);
                }
            }
            
//            if (ma.getApplicationStatus() == 0) {
//                ma.setApplicationStatus(1);
//            }

            if (session.getAttribute("Client").equals("Sechaba")) {
                ma.setApplicationType(1);
                ma = port.saveMemberApplicationSechaba(ma);
                System.out.println("Application Status " + ma.getApplicationStatus());
            } else {
                ma = port.saveMemberApplication(ma);
            }

            request.setAttribute("MemberApplication", ma);
            request.setAttribute("memberNumber", ma.getCoverNumber());
            request.setAttribute("memberAppNumber", ma.getApplicationNumber());
            request.setAttribute("memberIdNum", ma.getApplicationId());
            request.setAttribute("memberAppType", ma.getApplicationType());
            
            System.out.println("memberNumber " + request.getAttribute("memberNumber"));
            System.out.println("memberAppNumber " + request.getAttribute("memberAppNumber"));
            System.out.println("memberAppType " + request.getAttribute("memberAppType"));
            System.out.println("memberIdNum " + request.getAttribute("memberIdNum"));

            return true;
        } catch (Exception e) {
            System.err.append(e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    private NeoUser getNeoUser(HttpServletRequest request) {
        return (NeoUser) request.getSession().getAttribute("persist_user");
    }

    private int getIntParam(HttpServletRequest request, String paramName) {
        try {
            String maxDepStr = request.getParameter(paramName);
            return Integer.parseInt(maxDepStr);
        } catch (java.lang.Exception e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }
}

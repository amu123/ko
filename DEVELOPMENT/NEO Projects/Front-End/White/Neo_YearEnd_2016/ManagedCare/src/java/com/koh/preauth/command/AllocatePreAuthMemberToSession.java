/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.preauth.command;

import com.koh.auth.command.*;
import com.koh.command.Command;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.LookupValue;

/**
 *
 * @author gerritj
 */
public class AllocatePreAuthMemberToSession extends Command {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();

        String forField = "" + session.getAttribute("searchCalled");
        String onScreen = "" + session.getAttribute("onScreen");

        session.setAttribute(forField + "_text", request.getParameter("memNo"));
        session.setAttribute(forField, request.getParameter("name") + " " + request.getParameter("surname") + " " + request.getParameter("dependant"));
        //set scheme option and entity id
        //get request parameter
        String prodStr = request.getParameter("scheme");
        String optStr = request.getParameter("schemeOption");
        //split lookup strings from request
        String[] prodLookupStr = prodStr.split("-");
        String[] optLookupStr = optStr.split("-");
        //set ids and values
        String prodId = prodLookupStr[0];
        String prodValue = prodLookupStr[1];
        String optId = optLookupStr[0];
        String optValue = optLookupStr[1];
        //add ids and values to new lookups
        LookupValue productLookup = new LookupValue();
        productLookup.setId(prodId);
        productLookup.setValue(prodValue);
        LookupValue optionLookup = new LookupValue();
        optionLookup.setId(optId);
        optionLookup.setValue(optValue);
        //set lookups to session
        session.setAttribute("scheme", productLookup);
        session.setAttribute("schemeOption", optionLookup);

        String eId = request.getParameter("eId");
        String status = request.getParameter("status");
        System.out.println("membership status = " + status);
        if (!status.equalsIgnoreCase("active") || !status.equalsIgnoreCase("pended")) {
            session.setAttribute("memberStatusFlag", "false");
            session.setAttribute("memberEntityId", eId);
            session.setAttribute("memberPlan", optValue);
        }

        try {
            String nextJSP = onScreen;
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);

            dispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "AllocatePreAuthMemberToSession";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.pdc.tags;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import neo.manager.HypertensionBiometrics;

/**
 *
 * @author princes
 */
public class BiometricHypertensionTable extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String javascript;

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        HttpSession session = pageContext.getSession();
        List<HypertensionBiometrics> getHypertension = (List<HypertensionBiometrics>) session.getAttribute("getHypertension");
        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        String dateMeasured;

        try {
            out.print("<tr>");
            out.print("<th scope=\"col\">Date Measured</th>");
            out.print("<th scope=\"col\">Height</th>");
            out.print("<th scope=\"col\">Weight</th>");
            out.print("<th scope=\"col\">BMI</th>");
            out.print("<th scope=\"col\">Blood Pressure Systolic</th>");
            out.print("<th scope=\"col\">Blood Pressure Diastolic</th>");
            out.print("<th scope=\"col\">Select</th>");

//            out.print("<th scope=\"col\">ICD10</th>");
//            out.print("<th scope=\"col\">Excercise</th>");            
//            out.print("<th scope=\"col\">Ex Smoker Years Stopped</th>");
//            out.print("<th scope=\"col\">Alchohol Units</th>");
//            out.print("<th scope=\"col\">BP SBP</th>");
//            out.print("<th scope=\"col\">BMI</th>");
//            out.print("<th scope=\"col\">Source</th>");
//            out.print("<th scope=\"col\">Notes</th>");
            out.print("</tr>");
            if (getHypertension != null && getHypertension.size() > 0) {
                for (HypertensionBiometrics h : getHypertension) {

                    out.print("<tr>");
                    if (h.getDateMeasured() != null) {
                        Date mDate = h.getDateMeasured().toGregorianCalendar().getTime();
                        dateMeasured = formatter.format(mDate);
                        out.print("<td><center><label class=\"label\">" + dateMeasured + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (h.getHeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + h.getHeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (h.getWeight() != 0.0) {
                        out.print("<td><center><label class=\"label\">" + h.getWeight() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (h.getBmi() != 0) {
                        out.print("<td><center><label class=\"label\">" + h.getBmi() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (h.getBloodPressureSystolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + h.getBloodPressureSystolic() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    if (h.getBloodPressureDiastolic() != 0) {
                        out.print("<td><center><label class=\"label\">" + h.getBloodPressureDiastolic() + "</label></center></td>");
                    } else {
                        out.print("<td></td>");
                    }
                    out.print("<td><center><button name=\"opperation\" type=\"button\" " + javascript + " value=\"" + h.getBiometricsId() + "\">Select</button></center></td>");

//                    if (h.getIcd10Lookup() != null) {
//                        out.print("<td>" + h.getIcd10Lookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (h.getExercisePerWeek() != 0) {
//                        out.print("<td>" + h.getExercisePerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }                    
//
//                    if (h.getYearsSinceStopped() > 0) {
//                        out.print("<td>" + h.getYearsSinceStopped() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (h.getAlcoholUnitsPerWeek() != 0) {
//                        out.print("<td>" + h.getAlcoholUnitsPerWeek() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (h.getBloodPressureSystolic() != 0) {
//                        out.print("<td>" + h.getBloodPressureSystolic() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (h.getBmi() != 0) {
//                        out.print("<td>" + h.getBmi() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (h.getSourceLookup() != null) {
//                        out.print("<td>" + h.getSourceLookup().getValue() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
//
//                    if (h.getDetail() != null) {
//                        out.print("<td>" + h.getDetail() + "</td>");
//                    } else {
//                        out.print("<td></td>");
//                    }
                    out.print("</tr>");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return super.doEndTag();
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

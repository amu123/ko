/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agile.command;

import com.koh.command.NeoCommand;
import static com.koh.command.NeoCommand.service;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.NeoManagerBean;
import neo.manager.PracticeNetworkDetails;
import neo.manager.ProviderSearchDetails;

/**
 *
 * @author johanl
 */
public class FindPracticeDetailsByCode extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        PrintWriter out = null;
        NeoManagerBean port = service.getNeoManagerBeanPort();

        String providerNumber = request.getParameter("provNum");
        String authType = "" + session.getAttribute("authType");
        //The following statement is used for [ViewPracticeAddressDetailsCommand.java]:
        //To identify if its a Practice or Provider
        if (!providerNumber.isEmpty() && providerNumber.contains("|")) {
            String[] arrProviderNumber = providerNumber.split("\\|");
            providerNumber = arrProviderNumber[0];
            session.setAttribute("PracticeType", arrProviderNumber[1]);
        } else {
            session.setAttribute("PracticeType", "false");
        }

        String element = request.getParameter("element");
        String result = "";
        try {
            out = response.getWriter();
            if (providerNumber != null && !providerNumber.trim().equalsIgnoreCase("null")) {
                ProviderSearchDetails pd = service.getNeoManagerBeanPort().findPracticeAjaxDetailsByNumber(providerNumber);
                if (pd.getProviderNo() != null && !pd.getProviderNo().trim().equals("")) {
                    String provDescId = pd.getDisciplineType().getId();
                    String provDesc = pd.getDisciplineType().getValue();
                    String providerDesc = provDescId + " - " + provDesc;
                    String providerName = pd.getProviderName();
                    
                    //get network
                    if (element.equalsIgnoreCase("facilityProv") && authType.equalsIgnoreCase("11") && (!provDescId.equals("047") && !provDescId.equals("049")
                            && !provDescId.equals("059") && !provDescId.equals("079"))) {
                        out.println("Error|No such provider|" + getName());
                    } else {
                        PracticeNetworkDetails pnd = service.getNeoManagerBeanPort().getPracticeNetworkByNumberForToday(pd.getProviderNo());
                        String networkName = "None";
                        if (pnd != null) {
                            networkName = pnd.getNetworkDescription();
                        }

                    //String[] provNameSur = providerName.split("\\-");
                        //String provName = provNameSur[0];
                        //String provSurname = provNameSur[1];
                        session.setAttribute(element + "Name", providerName);
                        session.setAttribute(element + "Surname", providerName);
                        //session.setAttribute(element + "NameSur", provName + " " + provSurname);
                        session.setAttribute(element + "DiscType", provDesc);
                        session.setAttribute(element + "DiscTypeId", provDescId);
                        session.setAttribute(element + "Network", networkName);

                        if (element.equalsIgnoreCase("facilityProv")) {
                            int prodID = Integer.parseInt("" + session.getAttribute("scheme"));
                            int optID = Integer.parseInt("" + session.getAttribute("schemeOption"));

                            String admissionD = "" + session.getAttribute("admissionDateTime");
                            Date admissionDate = null;
                            XMLGregorianCalendar authStart = null;
                            try {
                                admissionDate = new SimpleDateFormat("yyyy/MM/dd").parse(admissionD);
                                authStart = DateTimeUtils.convertDateToXMLGregorianCalendar(admissionDate);
                            } catch (ParseException ex) {
                                ex.printStackTrace();
                            }

                            boolean isDSP = port.isDSPNetwork(providerNumber, authStart, prodID, optID);

                            if (isDSP) {
                                session.setAttribute("isDSPPenalty", "no");
                                result = "Done";
                            } else {
                                result = "Penalty";
                                session.setAttribute("isDSPPenalty", "yes");
                            }
                        } else {
                            result = "Done";
                        }
                        System.out.println("### result: " + result);
                        String responseText = result + "|ProviderName=" + providerName + "|ProviderDiscipline=" + providerDesc + "|ProviderNetwork=" + networkName + "$";
                        out.println(responseText);
                    }
                } else {
                    out.println("Error|No such provider|" + getName());
                }

            } else {
                out.println("Error|No such provider|" + getName());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "FindPracticeDetailsByCode";
    }
}

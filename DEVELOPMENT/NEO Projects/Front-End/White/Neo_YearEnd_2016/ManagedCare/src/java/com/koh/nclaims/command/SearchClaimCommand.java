/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.nclaims.command;

import com.koh.command.NeoCommand;
import com.koh.utils.DateTimeUtils;
import java.io.PrintWriter;
import java.lang.Exception;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author jpp
 */
public class SearchClaimCommand extends NeoCommand {

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        HttpSession session = request.getSession();
        NeoManagerBean port = NeoCommand.service.getNeoManagerBeanPort();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        String claimSearchResultMsg = "";
        String memberClaimSearchBeanList = "";

        PrintWriter out = null;
//        List<ClaimsBatch> claimList = null;
        //  List<ClaimsBatch> claimList = new ArrayList<ClaimsBatch>();
        ClaimSearchCriteria search = new ClaimSearchCriteria();
//        Collection<ClaimsBatch> batchList =  port.findClaimBySearchCriteria(search);

        NeoUser user = (NeoUser) session.getAttribute("persist_user");

        String memNum = request.getParameter("memNum");
        session.setAttribute("memNum", memNum);
        String depNum = request.getParameter("depNum");
        session.setAttribute("depNum", depNum);
        String providerNumber = request.getParameter("providerNumber");
        session.setAttribute("providerNumber", providerNumber);
        String discId = request.getParameter("discId");
        session.setAttribute("discId", discId);
        String practiceNumber = request.getParameter("practiceNumber");
        session.setAttribute("practiceNumber", practiceNumber);
        String disciplineType = request.getParameter("disciplineType");
        String processFrom = request.getParameter("processFrom");
        session.setAttribute("processFrom", processFrom);
        String processTo = request.getParameter("processTo");
        session.setAttribute("processTo", processTo);
        String serviceFrom = request.getParameter("serviceFrom");
        session.setAttribute("serviceFrom", serviceFrom);
        String serviceTo = request.getParameter("serviceTo");
        session.setAttribute("serviceTo", serviceTo);
        String accNumber = request.getParameter("accNumber");
        session.setAttribute("accNumber", accNumber);
        String scanNum = request.getParameter("scanNum");
        session.setAttribute("scanNum", scanNum);
        String batchNumber = request.getParameter("batchNumber");
        session.setAttribute("batchNumber", batchNumber);
        String claimId = request.getParameter("claimId");
        session.setAttribute("claimId", claimId);
        String authNumber = request.getParameter("authNumber");
        session.setAttribute("authNumber", authNumber);
        String paymentDate = request.getParameter("paymentDate");
        session.setAttribute("paymentDate", paymentDate);
        String ediClaim = request.getParameter("ediClaim");
        session.setAttribute("ediClaim", ediClaim);
        String tariffCode = request.getParameter("tariffCode");
        session.setAttribute("tariffCode", tariffCode);
        String claimStatus = request.getParameter("claimStatus");
        session.setAttribute("claimStatus", claimStatus);
        String toothNum = request.getParameter("toothNum");
        session.setAttribute("toothNum", toothNum);

        if (memNum != null && !memNum.equalsIgnoreCase("")) {
            search.setCoverNumber(memNum);
            System.out.println("memNum = " + memNum);
        }

        if (depNum != null && !depNum.equalsIgnoreCase("")) {
            int dl = Integer.parseInt(depNum);
            search.setDependentCode(dl);
            System.out.println("depNum= " + depNum);
        }

        if (providerNumber != null && !providerNumber.equalsIgnoreCase("")) {
            search.setProviderNumber(providerNumber);
            System.out.println("providerNumber = " + providerNumber);
        }

        
        if (discId != null && !discId.equalsIgnoreCase("")) {
            search.setDisciplineId(discId);
            System.out.println("discId = " + discId);
        }

        
        if (practiceNumber != null && !practiceNumber.equalsIgnoreCase("")) {
            search.setPracticeNumber(practiceNumber);
            System.out.println("practiceNumber = " + practiceNumber);
        }

//        System.out.println("disciplineType = " + disciplineType);
//        if (disciplineType != null && !disciplineType.equalsIgnoreCase("")) {
//            search.setDisciplineId(disciplineType);
//        }
        
        if (processFrom != null && !processFrom.equalsIgnoreCase("")) {
            try {
                Date date = new SimpleDateFormat("yyyy/MM/dd").parse(processFrom);
                XMLGregorianCalendar xFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(date);
                System.out.println("processfrom = " + processFrom);
                System.out.println("xFrom = " + xFrom);
                search.setProcessDateFrom(xFrom);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        
        if (processTo != null && !processTo.equalsIgnoreCase("")) {
            try {
                Date date = new SimpleDateFormat("yyyy/MM/dd").parse(processTo);
                XMLGregorianCalendar xTo = DateTimeUtils.convertDateToXMLGregorianCalendar(date);
                System.out.println("ProcessTo = " + processTo);
                System.out.println("xTo = " + xTo);
                search.setProcessDateTo(xTo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        
        if (serviceFrom != null && !serviceFrom.equalsIgnoreCase("")) {
            try {
                Date date = new SimpleDateFormat("yyyy/MM/dd").parse(serviceFrom);
                XMLGregorianCalendar xFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(date);
                System.out.println("serviceFrom = " + serviceFrom);
                System.out.println("xFrom = " + xFrom);
                search.setServiceDateFrom(xFrom);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        
        if (serviceTo != null && !serviceTo.equalsIgnoreCase("")) {
            try {
                Date date = new SimpleDateFormat("yyyy/MM/dd").parse(serviceTo);
                XMLGregorianCalendar xFrom = DateTimeUtils.convertDateToXMLGregorianCalendar(date);
                System.out.println("serviceTo = " + serviceTo);
                System.out.println("xFrom = " + xFrom);
                search.setServiceDateTo(xFrom);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        
        if (accNumber != null && !accNumber.equalsIgnoreCase("")) {
            search.setAccountNumber(accNumber);
            System.out.println("accNumber = " + accNumber);
        }

        
        if (scanNum != null && !scanNum.equalsIgnoreCase("")) {
            int sn = Integer.parseInt(scanNum);
            search.setScanId(sn);
            System.out.println("scanNum = " + scanNum);
        }

        
        if (batchNumber != null && !batchNumber.equalsIgnoreCase("")) {
            int bi = Integer.parseInt(batchNumber);
            search.setBatchId(bi);
            System.out.println("batchNumber = " + batchNumber);
        }

        
        if (claimId != null && !claimId.equalsIgnoreCase("")) {
            int ci = Integer.parseInt(claimId);
            search.setClaimId(ci);
            System.out.println("claimId = " + claimId);
        }

        
        if (authNumber != null && !authNumber.equalsIgnoreCase("")) {
            search.setAuthNumber(authNumber);
            System.out.println("authNumber = " + authNumber);
        }

        
        if (paymentDate != null && !paymentDate.equalsIgnoreCase("")) {
            try {
                Date date = new SimpleDateFormat("yyyy/MM/dd").parse(paymentDate);
                XMLGregorianCalendar xDate = DateTimeUtils.convertDateToXMLGregorianCalendar(date);
                System.out.println("paymentDate = " + paymentDate);
                System.out.println("xFrom = " + xDate);
                search.setPaymentDate(xDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        
        if (ediClaim != null && !ediClaim.equalsIgnoreCase("")) {
            int ec = Integer.parseInt(ediClaim);
            search.setEdiId(ec);
            System.out.println("ediClaim = " + ediClaim);
        }

        
        if (tariffCode != null && !tariffCode.equalsIgnoreCase("")) {
            search.setTariffCodeNo(tariffCode);
            System.out.println("tariffCode = " + tariffCode);
        }

        
        if (claimStatus != null && !claimStatus.equalsIgnoreCase("99")) {
            LookupValue lv = new LookupValue();
            lv.setValue(claimStatus);
            lv.setId(claimStatus);
            search.setClaimStatus(lv);
            System.out.println("claimStatus = " + claimStatus);
        }

        
        if (toothNum != null && !toothNum.equalsIgnoreCase("")) {
            int tn = Integer.parseInt(toothNum);
            search.setToothnumber(tn);
            System.out.println("toothNum = " + toothNum);
        }

        neo.manager.Security _secure = new neo.manager.Security();
        _secure.setCreatedBy(user.getUserId());
        _secure.setLastUpdatedBy(user.getUserId());
        _secure.setSecurityGroupId(user.getSecurityGroupId());
        System.out.println("search = " + search.toString() + " - " + search != null);
        if (search != null) {
            System.out.println("Not empty");
            ClaimSearchResult returnList = port.fetchBatchClaimAndClaimLineIdsBySearchCriteria(search, _secure);
            //Collection<ClaimsBatch> returnList = port.findClaimBySearchCriteria(search);

            if (returnList == null) {
                System.out.println("Please Redefine Search");
                request.setAttribute("ViewClaimSearchResultsMessage", "Please Redefine Search");
            } else {
                if (returnList.getBatchs() != null) {
                    if (returnList.getBatchs().size() != 0) {
                        System.out.println("success");
                        session.setAttribute("viewClaimSearchResults", returnList.getBatchs());
                    } else {
                        String msg = returnList.getSearchResultMessage();
                        System.out.println("message bach size = 0 " + msg);
                        request.setAttribute("ViewClaimSearchResultsMessage", msg);
                    }
                } else {
                    String msg = returnList.getSearchResultMessage();
                    System.out.println("message batch = null " + msg);
                    request.setAttribute("ViewClaimSearchResultsMessage", msg);
                }
            } 
        } else {
            System.out.println("Search is EMPTY");
        }

//            session.setAttribute("searchClaimResult", claimsBatch);
        try {
            String nextJSP = "/NClaims/ViewClaims.jsp";
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getName() {
        return "SearchClaimsCommand";
    }
}

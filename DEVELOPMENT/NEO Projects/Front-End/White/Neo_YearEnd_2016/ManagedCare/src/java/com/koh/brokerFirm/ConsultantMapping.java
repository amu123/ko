/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.brokerFirm;

import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import neo.manager.*;

/**
 *
 * @author princes
 */
public class ConsultantMapping {

    private static final String[][] FIELD_MAPPINGS = {
        {"ConsApp_brokerCode", "Consultant Code", "yes", "str", "ConsApp", "consultantCode","0","15"},
        {"consultantEntityId", "", "no", "int", "ConsApp", "entityId"},
        {"ConsApp_brokerInitials", "Initials", "no", "str", "ConsApp", "initials","0","10"},
        {"ConsApp_brokerName", "Name", "yes", "str", "ConsApp", "firstNames","0","50"},
        {"ConsApp_brokerSurname", "Surname", "yes", "str", "ConsApp", "surname","0","50"},
        {"ConsApp_brokerPrefferedName", "Preferred Name", "no", "str", "ConsApp", "nickName","0","50"},
        {"ConsApp_brokerIdNumber", "ID Number", "no", "str", "ConsApp", "idNumber","8","13"},
        {"ConsApp_brokerDateOfBirth", "Date Of Birth", "no", "date", "ConsApp", "dateOfBirth"},
        {"ConsApp_brokerStatus", "Status", "yes", "int", "ConsApp", "consultantStatus"},
        {"ConsApp_brokerRegion", "Region", "no", "int", "ConsApp", "region"}
    };
    private static final String[][] ACCREDITATION_MAPPINGS = {
        {"ConsApp_brokerAccreditationType", "Accreditation Type", "yes", "int", "Accreditation", "accreditationType"},
        {"ConsApp_brokerAccreditationNumber", "Accreditation Number", "yes", "str", "Accreditation", "accreditationNumber"},
        {"ConsApp_ConsApplicationDate", "Application Date", "yes", "date", "Accreditation", "applicationDate"},
        {"ConsApp_brokerAuthorisationDate", "Authorisation Date", "yes", "date", "Accreditation", "authorisationDate"},
        {"ConsApp_brokerStartDate", "Start Date", "yes", "date", "Accreditation", "effectiveStartDate"},
        {"ConsApp_brokerEndDate", "End Date", "yes", "date", "Accreditation", "effectiveEndDate"}
    };
    private static final String[][] CONTACT_MAPPINGS = {
        {"ConsApp_ContactDetails_email", "Email Address", "no", "str", "ContactDetails", ""},
        {"ConsApp_ContactDetails_altEmail", "Alternative Email Address", "no", "str", "ContactDetails", ""},
        {"ConsApp_PostalAddress_addressLine1", "Line 1", "no", "str", "AddressDetails", ""},
        {"ConsApp_PostalAddress_addressLine2", "Line 2", "no", "str", "AddressDetails", ""},
        {"ConsApp_PostalAddress_addressLine3", "Line 3", "no", "str", "AddressDetails", ""},
        {"ConsApp_PostalAddress_postalCode", "Postal Code", "no", "str", "AddressDetails", ""},
        {"ConsApp_ResAddress_addressLine1", "Line 1", "no", "str", "AddressDetails", ""},
        {"ConsApp_ResAddress_addressLine2", "Line 2", "no", "str", "AddressDetails", ""},
        {"ConsApp_ResAddress_addressLine3", "Line 3", "no", "str", "AddressDetails", ""},
        {"ConsApp_ResAddress_postalCode", "Physical Code", "no", "str", "AddressDetails", ""}
    };
    
  private static final String[][] HISTORY_MAPPINGS = {
        
        {"ConsApp_brokerHistoryCode", "End Date", "no", "str", "Accreditation", ""}, 
        {"ConsApp_brokerHistoryName", "End Date", "no", "str", "Accreditation", ""}, 
        {"ConsApp_historyStartDate", "End Date", "no", "date", "Accreditation", "effectiveStartDate"}, 
        {"Cons_historyEndDate", "End Date", "no", "date", "Accreditation", "effectiveEndDate"}
    };
  private static final String[][] CONS_HISTORY_MAPPING = {
      {"BrokerApp_ConsultantHistoryCode","End Date","no","Accreditation"},
      {"BrokerApp_ConsultantHistoryName","End Date","no","Accreditation"},
      {"BrokerApp_ConsultantHistoryStartDate","End Date","no","Accreditation", "effectiveStartDate"},
//      {"BrokerApp_ConsultantHistoryEndDate","End Date","no","Accreditation", "effectiveEndDate"}
  };
  
  private static final String[][] NOTES_MAPPINGS = {
        {"consultantEntityId","","no", "int", "ConsApp","entityId"},
        {"notesListSelect","Note Type","no", "int", "ConsApp","noteType"},
        {"BrokerConsultantAppNotesAdd_details","Details","no", "str", "ConsApp","noteDetails"}
    };
    private static final Map<String, String[]> FIELD_MAP;

    static {
        HashMap hm = new HashMap<String, String[]>();
        for (String[] s : FIELD_MAPPINGS) {
            hm.put(s[0], s);
        }
        FIELD_MAP = hm;
    }

    public static Map<String, String[]> getMapping() {
        return FIELD_MAP;
    }

    public static Map<String, String> validate(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, FIELD_MAPPINGS);
        return errors;
    }
    
    public static Map<String, String> validateNotes(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, NOTES_MAPPINGS);
        return errors;
    }

    public static Map<String, String> validateContact(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, CONTACT_MAPPINGS);
        return errors;
    }

    public static Map<String, String> validateAccreditation(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, ACCREDITATION_MAPPINGS);
        return errors;
    }
    
    public static Map<String, String> validateHistory(HttpServletRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors, HISTORY_MAPPINGS);
        return errors;
    }

    public static Map<String,String> validateBrokerConsultantHistory(HttpServletRequest request){
        Map<String,String> errors = new HashMap<String, String>();
        TabUtils.validate(request, errors,CONS_HISTORY_MAPPING);
        return errors;
    }
    
    public static BrokerConsultant ConsultantMapping(HttpServletRequest request, NeoUser neoUser) {
        BrokerConsultant br = (BrokerConsultant) TabUtils.getDTOFromRequest(request, BrokerConsultant.class, FIELD_MAPPINGS, "ConsApp");
        br.setSecurityGroupId(neoUser.getSecurityGroupId());
        br.setLastUpdatedBy(neoUser.getUserId());
        return br;
    }

    public static void setConsultantApplication(HttpServletRequest request, BrokerConsultant brokerApplication) {
        TabUtils.setRequestFromDTO(request, brokerApplication, FIELD_MAPPINGS, "ConsApp");
    }

    public static List<AddressDetails> getAddressDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<AddressDetails> cdl = new ArrayList<AddressDetails>();
        cdl.add(getAddressDetail(request, "ConsApp_PostalAddress_", neoUser, entityId, 2));
        cdl.add(getAddressDetail(request, "ConsApp_ResAddress_", neoUser, entityId, 1));
        return cdl;
    }

    private static AddressDetails getAddressDetail(HttpServletRequest request, String param, NeoUser neoUser, int entityId, int addrType) {
        AddressDetails ad = new AddressDetails();
        ad.setAddressTypeId(addrType);
        ad.setPrimaryInd("Y");
        ad.setAddressUse(1);
        ad.setAddressLine1(request.getParameter(param + "addressLine1"));
        ad.setAddressLine2(request.getParameter(param + "addressLine2"));
        ad.setAddressLine3(request.getParameter(param + "addressLine3"));
        ad.setPostalCode(request.getParameter(param + "postalCode"));
        //ad.setEntityId(entityId);
        //ad.setSecurityGroupId(neoUser.getSecurityGroupId());
        //ad.setLastUpdatedBy(neoUser.getUserId());
        return ad;
    }

    public static void setBroker(HttpServletRequest request, Broker broker) {
        TabUtils.setRequestFromDTO(request, broker, FIELD_MAPPINGS, "ConsApp");
    }

    public static void setAddressDetails(HttpServletRequest request, Collection<AddressDetails> addressDetails) {
        if (addressDetails == null) {
            return;
        }
        for (AddressDetails ad : addressDetails) {
            if (ad == null) {
                continue;
            }
            String fieldStart = ad.getAddressTypeId() == 1 ? "ConsApp_ResAddress_" : "ConsApp_PostalAddress_";
            request.setAttribute(fieldStart + "addressLine1", ad.getAddressLine1());
            request.setAttribute(fieldStart + "addressLine2", ad.getAddressLine2());
            request.setAttribute(fieldStart + "addressLine3", ad.getAddressLine3());
            request.setAttribute(fieldStart + "postalCode", ad.getPostalCode());
        }
    }

    public static List<ContactDetails> getContactDetails(HttpServletRequest request, NeoUser neoUser, int entityId) {
        List<ContactDetails> cdl = new ArrayList<ContactDetails>();
        cdl.add(getContactDetail(request, "ConsApp_ContactDetails_telNo", neoUser, entityId, 4));
        cdl.add(getContactDetail(request, "ConsApp_ContactDetails_faxNo", neoUser, entityId, 2));
        cdl.add(getContactDetail(request, "ConsApp_ContactDetails_email", neoUser, entityId, 1));
        cdl.add(getContactDetail(request, "ConsApp_ContactDetails_altEmail", neoUser, entityId, 8));
        return cdl;
    }

    private static ContactDetails getContactDetail(HttpServletRequest request, String param, NeoUser neoUser, int entityId, int commType) {
        ContactDetails cd = new ContactDetails();
        cd.setCommunicationMethodId(commType);
        cd.setMethodDetails(request.getParameter(param));
        cd.setPrimaryIndicationId(1);
        //cd.setEntityId(entityId);
        //cd.setSecurityGroupId(neoUser.getSecurityGroupId());
        //cd.setLastUpdatedBy(neoUser.getUserId());
        return cd;
    }

    public static void setContactDetails(HttpServletRequest request, Collection<ContactDetails> contactDetails) {
        request.setAttribute("ConsApp_ContactDetails_region", request.getAttribute("consultantRegion"));
        if (contactDetails == null) {
            return;
        }
        for (ContactDetails cd : contactDetails) {
            if (cd == null) {
                continue;
            }
            String fieldStart = "";
            fieldStart = cd.getCommunicationMethodId() == 4 ? "ConsApp_ContactDetails_telNo" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 2 ? "ConsApp_ContactDetails_faxNo" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 1 ? "ConsApp_ContactDetails_email" : fieldStart;
            fieldStart = cd.getCommunicationMethodId() == 8 ? "ConsApp_ContactDetails_altEmail" : fieldStart;
            request.setAttribute(fieldStart, cd.getMethodDetails() == null ? "" : cd.getMethodDetails());
        }
    }

    public static int getEntityId(HttpServletRequest request) {
        return new Integer(request.getParameter("consultantEntityId"));
    }

    public static BrokerAccred getAccreditationDetails(HttpServletRequest request, int entityId, NeoUser neoUser) {

        BrokerAccred ba = new BrokerAccred();
        Date today = new Date();
        ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setEntityId(entityId);
        ba.setCreatedBy(neoUser.getUserId());
        ba.setLastUpdatedBy(neoUser.getUserId());
        ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setSecurityGroupId(2);

        ba.setAccreditationType(Integer.parseInt(request.getParameter("ConsApp_brokerAccreditationType")));
        ba.setAccreditationNumber(request.getParameter("ConsApp_brokerAccreditationNumber"));


        if (request.getParameter("ConsApp_ConsApplicationDate") != null) {
            Date appDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("ConsApp_ConsApplicationDate"));
            ba.setApplicationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(appDate));
        }

        if (request.getParameter("ConsApp_brokerAuthorisationDate") != null) {
            Date authDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("ConsApp_brokerAuthorisationDate"));
            ba.setAuthorisationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(authDate));
        }

        if (request.getParameter("ConsApp_brokerStartDate") != null) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("ConsApp_brokerStartDate"));
            ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        }


        if (request.getParameter("ConsApp_brokerEndDate") != null) {
            Date endDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("ConsApp_brokerEndDate"));
            ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        }
        return ba;
    }
    
        public static BrokerBrokerConsult getConsultantHistoryDetails(HttpServletRequest request, int entityId, NeoUser neoUser) {
        System.out.println("Searched Broker " + request.getParameter("brokerEntityId"));    
        BrokerBrokerConsult ba = new BrokerBrokerConsult();
        Date today = new Date();
        ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setBrokerConsultEntityId(entityId);
        ba.setBrokerEntityId(new Integer(request.getParameter("brokerEntityId")));
        ba.setCreatedBy(neoUser.getUserId());
        ba.setLastUpdatedBy(neoUser.getUserId());
        ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ba.setSecurityGroupId(2);

        if (request.getParameter("ConsApp_historyStartDate") != null) {
            Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("ConsApp_historyStartDate"));
            ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
        }


        if (request.getParameter("Cons_historyEndDate") != null) {
            Date endDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("Cons_historyEndDate"));
            ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        }
        return ba;
    }
        
        public static BrokerBrokerConsult getBrokerConsultantHistoryDetails(HttpServletRequest request , int entityId , NeoUser neouser){
            System.out.println("Searched Consultant "+ request.getParameter("consultantEntityId"));
            BrokerBrokerConsult ba = new BrokerBrokerConsult();
            Date today = new Date();
            ba.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
            ba.setBrokerEntityId(entityId);
            ba.setBrokerConsultEntityId(Integer.parseInt(request.getParameter("consultantEntityId")));
            ba.setCreatedBy(neouser.getUserId());
            ba.setLastUpdatedBy(neouser.getUserId());
            ba.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
            ba.setSecurityGroupId(2);
            
            if(request.getParameter("BrokerApp_ConsultantHistoryStartDate")!=null && !request.getParameter("BrokerApp_ConsultantHistoryStartDate").isEmpty()){
//                System.out.println("Start Date : " + DateTimeUtils.convertFromYYYYMMDD(request.getParameter("BrokerApp_ConsultantHistoryStartDate")));
                Date startDate = DateTimeUtils.convertFromYYYYMMDD(request.getParameter("BrokerApp_ConsultantHistoryStartDate"));
                ba.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(startDate));
            }
            
            Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
            ba.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
            return ba;
        }
        
   public static EntityNotes getBrokerConsultantAppNotes(HttpServletRequest request,NeoUser neoUser) {
        EntityNotes ma = (EntityNotes)TabUtils.getDTOFromRequest(request, EntityNotes.class, NOTES_MAPPINGS, "ConsApp");
        Date today = new Date();        
        ma.setNoteDetails(request.getParameter("BrokerConsultantAppNotesAdd_details"));
        ma.setEntityId(new Integer(request.getParameter("consultantEntityId")));
        ma.setNoteType(new Integer(request.getParameter("notesListSelect")));
        ma.setSecurityGroupId(neoUser.getSecurityGroupId());
        ma.setLastUpdatedBy(neoUser.getUserId());
        ma.setCreatedBy(neoUser.getUserId());
        ma.setNoteDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ma.setCreationDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        ma.setEffectiveStartDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));
        Date endDate = DateTimeUtils.convertFromYYYYMMDD("2999/12/31");
        ma.setEffectiveEndDate(DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        ma.setLastUpdateDate(DateTimeUtils.convertDateToXMLGregorianCalendar(today));               
        return ma;
    }
}

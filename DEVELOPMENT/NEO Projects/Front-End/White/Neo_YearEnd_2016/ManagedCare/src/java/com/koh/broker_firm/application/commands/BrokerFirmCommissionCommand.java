/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker_firm.application.commands;

import com.koh.command.NeoCommand;
import com.koh.contribution.command.ContributionBankStatementCommand;
import com.koh.employer.command.TabUtils;
import com.koh.utils.DateTimeUtils;
import com.koh.utils.MapUtils;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import neo.manager.KeyValueArray;
import neo.manager.NeoManagerBean;

/**
 *
 * @author yuganp
 */
public class BrokerFirmCommissionCommand extends NeoCommand{

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        try {
            String s = request.getParameter("command");
            if (s != null && !s.isEmpty()) {
                System.out.println("command : " + s);
                if (s.equalsIgnoreCase("viewBrokerSummary")) {
                    viewBrokerSummary(request, response, context);
                } else if (s.equalsIgnoreCase("viewBrokerDetails")) {
                    viewBrokerDetails(request, response, context);
                }else if (s.equals("CommSearch")){
                    searchCommssion(request, response, context);
                }
            }
        } catch (java.lang.Exception ex) {
            Logger.getLogger(ContributionBankStatementCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void viewBrokerSummary(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        int entityId = TabUtils.getIntParam(request, "commBrokerFirmEntityId");
        int runId = TabUtils.getIntParam(request, "commCheqeRunId");
        List<KeyValueArray> kva = port.fetchBrokerCommissionSummary(entityId, runId);
        Object col = MapUtils.getMap(kva);                        
        request.setAttribute("CommissionSummary", col);
        context.getRequestDispatcher("/Broker/BrokerCommPaySummary.jsp").forward(request, response);
    }
    
    private void viewBrokerDetails(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        int entityId = TabUtils.getIntParam(request, "commBrokerEntityId");
        int runId = TabUtils.getIntParam(request, "commCheqeRunId");
        List<KeyValueArray> kva = port.fetchBrokerCommissionDetails(entityId, runId);
        Object col = MapUtils.getMap(kva);                        
        request.setAttribute("CommissionDetails", col);
        context.getRequestDispatcher("/Broker/BrokerCommPayDetails.jsp").forward(request, response);
    }
    
    private void searchCommssion(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws ServletException, IOException {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        int entityId = TabUtils.getIntParam(request, "commBrokerFirmEntityId");
        String comm_start_date = request.getParameter("comm_start_date");
        String comm_end_date = request.getParameter("comm_end_date");
        //System.err.println("Start : " + comm_start_date + " End " + comm_end_date + " entityId " + entityId);
        Date startDate = DateTimeUtils.convertFromYYYYMMDD(comm_start_date);
        Date endDate = DateTimeUtils.convertFromYYYYMMDD(comm_end_date);
        //#*_
        List<KeyValueArray> kva = port.fetchCommissionPaymentsByDate(entityId, DateTimeUtils.convertDateToXMLGregorianCalendar(startDate), DateTimeUtils.convertDateToXMLGregorianCalendar(endDate));
        System.out.println("Size " + kva.size());
        List<Map<String, String>> map = MapUtils.getMap(kva);
        request.setAttribute("comm_start_date", DateTimeUtils.convertToYYYYMMDD(startDate));
        request.setAttribute("comm_end_date", DateTimeUtils.convertToYYYYMMDD(endDate));
        request.setAttribute("brokerFirmEntityId", entityId);
        request.setAttribute("CommissionPayments", map);
        context.getRequestDispatcher("/Broker/CommissionTransactions.jsp").forward(request, response);
    }

    @Override
    public String getName() {
        return "BrokerFirmCommissionCommand";
    }
    
}

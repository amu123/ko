/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.broker_firm.application.commands;

import com.koh.brokerFirm.BrokerMapping;
import com.koh.command.NeoCommand;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import neo.manager.*;

/**
 *
 * @author princes
 */
public class BrokerAppTabContentsCommand extends NeoCommand{
    
    private static final String JSP_FOLDER = "/Broker/";

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        String tab = request.getParameter("tab");
        int entityId = 0;
        if (request.getParameter("brokerEntityId") != null && !request.getParameter("brokerEntityId").isEmpty()) {
            entityId = new Integer(request.getParameter("brokerEntityId"));
            request.setAttribute("brokerEntityId", entityId);
        }
        /*System.out.println("tab entityId " + entityId);
        System.out.println("tab brokerTitle " + request.getParameter("brokerTitle"));
        System.out.println("tab brokerContactPerson " + request.getParameter("brokerContactPerson"));
        System.out.println("tab brokerContactSurname " + request.getParameter("brokerContactSurname"));
        System.out.println("tab brokerContactRegion " + request.getParameter("brokerRegion"));*/
        String page = "";
        if ("BrokerAppDetails".equalsIgnoreCase(tab)) {
            page = getBrokerDetails(request, entityId);
        } else if ("Accreditation".equalsIgnoreCase(tab)) {
            page = getBrokerAccreditation(request, entityId);
        } else if ("BankingDetails".equalsIgnoreCase(tab)) {
            page = getBrokerBankingDetails(request, entityId);
        } else if ("ContactDetails".equalsIgnoreCase(tab)) {
            page = getBrokerContactDetails(request, entityId);
        } else if ("BrokerHistory".equalsIgnoreCase(tab)) {
            page = getBrokerHistory(request, entityId);
        } else if ("CommissionTransactions".equalsIgnoreCase(tab)) {
            page = getCommissionTransactions(request);
        } else if ("AuditTrail".equalsIgnoreCase(tab)) {
            page = getBrokerAuditTrail(request);
        } else if ("Notes".equalsIgnoreCase(tab)) {
            page = getBrokerNotes(request);
        } else if ("CoverDetails".equalsIgnoreCase(tab)) {
            page = getCoverDetails(request, entityId);
        } else if ("Documents".equalsIgnoreCase(tab)) {
            page = getDocuments(request, entityId);
        }else if("BrokerConsultant".equalsIgnoreCase(tab)){
            page = getBrokerConsultantdetail(request, entityId);
        }

            RequestDispatcher dispatcher = context.getRequestDispatcher(page);
        try {
            dispatcher.forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(BrokerAppTabContentsCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BrokerAppTabContentsCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String getName() {
        return "BrokerAppTabContentsCommand";
    }

    private String getBrokerDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        if (entityId != 0) {
            Broker bf = port.fetchBrokerByEntityId(entityId);
            BrokerMapping.setBrokerApplication(request, bf);
            if (bf.getLanguageId() == 0 || bf.getLanguageId() == 99) {
                request.setAttribute("BrokerApp_correspondanceLanguage", "2");
            }
            request.setAttribute("brokerEntityId", bf.getEntityId());
            request.setAttribute("brokerTitle", bf.getContactTitle());
            request.setAttribute("brokerContactPerson", bf.getContactFirstName());
            request.setAttribute("brokerRegion", bf.getRegion());
        } else {
            request.setAttribute("BrokerApp_correspondanceLanguage", "2");
            
        }
        return JSP_FOLDER + "BrokerDetails.jsp";
    }

    public String getBrokerAccreditation(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        if (entityId != 0) {
            List<BrokerAccred> ba = port.fetchBrokerAccredByEntityId(entityId);
            request.setAttribute("accreList", ba);
        }
        return JSP_FOLDER + "BrokerAccreditation.jsp";
    }  

    private String getBrokerBankingDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        request.setAttribute("broker_map", BrokerMapping.getMapping());
        if (entityId != 0) {
            Broker eg = port.fetchBrokerByEntityId(entityId);
            Collection<ContactDetails> cd = port.getContactDetailsByEntityId(entityId);
            Collection<AddressDetails> ad = port.getAddressDetailsByEntityId(entityId);
            BrokerMapping.setBroker(request, eg);
            BrokerMapping.setAddressDetails(request, ad);
            BrokerMapping.setContactDetails(request, cd);
            //request.setAttribute("employerEntityId", employerId);
            //request.setAttribute("EmployerName", eg.getEmployerName());
            //request.setAttribute("EmployerNumber", eg.getEmployerNumber());
        };
        return JSP_FOLDER + "BrokerBankingDetails.jsp";
    }

    private String getCommissionTransactions(HttpServletRequest request) {
//        List<KeyValueArray> kva = port.fetchQuestions(191);
//        Object obj = MapUtils.getMap(kva);
 //       Object obj = port.fetchAppQuestions(2);
//        request.setAttribute("MemAppGeneralQuestions", obj);
        return JSP_FOLDER + "BrokerCommissions.jsp";
    }
    
    public String getBrokerHistory(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        if (entityId != 0) {
            List<BrokerBrokerFirm> ba = port.fetchBrokerToFirmByEntityId(entityId);
            request.setAttribute("accreList", ba);
        }
        return JSP_FOLDER + "BrokerHistory.jsp";
    }
        
    private String getBrokerAuditTrail(HttpServletRequest request) {
//        List<KeyValueArray> kva = port.fetchQuestions(191);
//        Object obj = MapUtils.getMap(kva);
 //       Object obj = port.fetchAppQuestions(2);
//        request.setAttribute("MemAppGeneralQuestions", obj);
        return JSP_FOLDER + "BrokerAuditTrail.jsp";
    }
        
    private String getBrokerNotes(HttpServletRequest request) {
//        List<KeyValueArray> kva = port.fetchQuestions(191);
//        Object obj = MapUtils.getMap(kva);
        //       Object obj = port.fetchAppQuestions(2);
//        request.setAttribute("MemAppGeneralQuestions", obj);
        return JSP_FOLDER + "BrokerNotes.jsp";
    }
    
    public String getBrokerConsultantdetail(HttpServletRequest request, int entityId){
        NeoManagerBean port = service.getNeoManagerBeanPort();
        if (entityId != 0) {
            System.out.println("Entity id for broker : " + entityId);
            List<BrokerBrokerConsult> brokerConsultantList = port.fetchBrokerConsultantByBrokerEntityID(entityId);
            request.setAttribute("brokerConsultantList",brokerConsultantList);
        }
        return JSP_FOLDER + "BrokerConsultantHistory.jsp";
    }
    
    private String getCoverDetails(HttpServletRequest request, int entityId) {
        HttpSession session = request.getSession();
        NeoManagerBean port = service.getNeoManagerBeanPort();
        if (entityId != 0) {
            System.out.println("Entity id for broker : " + entityId);
            List<MemberBroker> ba = port.findMemberBrokerByBrokerEntityId(entityId);
            request.setAttribute("entityId", entityId);
            session.setAttribute("entityId", entityId);
            request.setAttribute("accreList", ba);
        }
        return JSP_FOLDER + "BrokerCoverDetails.jsp";
    }

    private String getBrokerContactDetails(HttpServletRequest request, int entityId) {
        NeoManagerBean port = service.getNeoManagerBeanPort();
        request.setAttribute("broker_map", BrokerMapping.getMapping());
        String brokerTitle = request.getParameter("brokerTitle");
        String brokerContactPerson = request.getParameter("brokerContactPerson");
        String brokerContactSurname = request.getParameter("brokerContactSurname");
        String brokerRegion = request.getParameter("brokerRegion");
        request.setAttribute("brokerTitle", brokerTitle);
        request.setAttribute("brokerContactPerson", brokerContactPerson);
        request.setAttribute("brokerContactSurname", brokerContactSurname);
        request.setAttribute("brokerRegion", brokerRegion);
        if (entityId != 0) {

            Broker eg = port.fetchBrokerByEntityId(entityId);
            Collection<ContactDetails> cd = port.getContactDetailsByEntityId(entityId);
            Collection<AddressDetails> ad = port.getAddressDetailsByEntityId(entityId);
            BrokerMapping.setBroker(request, eg);
            BrokerMapping.setAddressDetails(request, ad);
            BrokerMapping.setContactDetails(request, cd);
            //request.setAttribute("employerEntityId", employerId);
            //request.setAttribute("EmployerName", eg.getEmployerName());
            //request.setAttribute("EmployerNumber", eg.getEmployerNumber());
        }
        return JSP_FOLDER + "BrokerContactDetails.jsp";
    }
    
    private String getDocuments(HttpServletRequest request, int entityId) {
//        List<KeyValueArray> kva = port.fetchQuestions(191);
//        Object obj = MapUtils.getMap(kva);
        //       Object obj = port.fetchAppQuestions(2);
//        request.setAttribute("MemAppGeneralQuestions", obj);
        request.setAttribute("entityId", entityId);
        request.getSession().setAttribute("memberCoverEntityId", entityId);
        return JSP_FOLDER + "BrokerDocuments.jsp";
    }


}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.koh.biometric.command;

import com.koh.command.NeoCommand;
import java.lang.Exception;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import neo.manager.*;

/**
 *
 * @author josephm
 */
public class CaptureDiabetesBiometericsCommand extends NeoCommand {
    private Object _datatypeFactory;
    private String nextJSP;

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        HttpSession session = request.getSession();
        boolean checkError = false;
        NeoUser user = (NeoUser) session.getAttribute("persist_user");
        
        //clearAllFromSession(request);
        
        DiabetesBiometrics diabetes = new DiabetesBiometrics();
        
        diabetes.setBiometricTypeId("5");
        
        NeoManagerBean port = service.getNeoManagerBeanPort();

        //String exerciseInWeek = request.getParameter("exerciseQuestion");
        String exerciseInWeek = (String) session.getAttribute("exerciseQuestion_text");    
        if (exerciseInWeek != null && !exerciseInWeek.equalsIgnoreCase("")) {
            if(exerciseInWeek.matches("[0-9]+")){
            int exerciseInWeekValue = Integer.parseInt(exerciseInWeek);
            diabetes.setExercisePerWeek(exerciseInWeekValue);
            }else {
            
                session.setAttribute("exerciseQuestion_error", "The exercise field should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("exerciseQuestion_error", "The exercise field should be filled in");
            checkError = true;
        }*/
        
        //String currentSmoker = request.getParameter("currentSmoker");
        String currentSmoker = (String) session.getAttribute("currentSmoker_text");
        if (currentSmoker != null && !currentSmoker.equalsIgnoreCase("99")) {
                diabetes.setCurrentSmoker(currentSmoker);
                LookupValue smoker = new LookupValue();
                smoker.setId(currentSmoker);
                smoker.setValue(port.getValueFromCodeTableForId("YesNo Type", currentSmoker));
                diabetes.setCurrentSmokerLookup(smoker);
        } /*else {

            session.setAttribute("currentSmoker_error", "The current smoker field should be filled in");
            checkError = true;
        }*/
        
        //String numberOfCigaretes = request.getParameter("smokingQuestion");
        String numberOfCigaretes = (String) session.getAttribute("smokingQuestion_text");
        if (numberOfCigaretes != null && !numberOfCigaretes.equalsIgnoreCase("")) {
            if(numberOfCigaretes.matches("[0-9]+")){
            int numberOfCigaretesValue = Integer.parseInt(numberOfCigaretes);
            diabetes.setCigarettesPerDay(numberOfCigaretesValue);
            }else {
            
                session.setAttribute("smokingQuestion_error", "The cigarettes field should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("smokingQuestion_error", "The cigarettes field should be filled in");
            checkError = true;
        }*/
        
        //String exSmoker = request.getParameter("exSmoker");
        String exSmoker = (String) session.getAttribute("exSmoker_text");
        if (exSmoker != null && !exSmoker.equalsIgnoreCase("99")) {
            diabetes.setExSmoker(exSmoker);
        } /*else {

            session.setAttribute("exSmoker_error", "The ex smoker field should be filled in");
            checkError = true;
        }*/
        
        //String yearsSinceSmoking = request.getParameter("stopped");
        String yearsSinceSmoking = (String) session.getAttribute("stopped_text");
        if (yearsSinceSmoking != null && !yearsSinceSmoking.equalsIgnoreCase("")) {
            if (yearsSinceSmoking.matches("[0-9]+")) {
                int yearsSinceSmokingValue = Integer.parseInt(yearsSinceSmoking);
                diabetes.setYearsSinceStopped(yearsSinceSmokingValue);
                 session.setAttribute("stopped_error", "");
            } else {
                session.setAttribute("stopped_error", "The years stopped field should be a numeric value");
                checkError = true;
            }
        }
        
        //String alcoholUnits = request.getParameter("alcoholConsumption");
        String alcoholUnits = (String) session.getAttribute("alcoholConsumption_text");
        if (alcoholUnits != null && !alcoholUnits.equalsIgnoreCase("")) {
            if(alcoholUnits.matches("[0-9]+")){
            int alcoholUnitsValue = Integer.parseInt(alcoholUnits);
            diabetes.setAlcoholUnitsPerWeek(alcoholUnitsValue);
            }else {
            
                session.setAttribute("alcoholConsumption_error", "The alcohol units field should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("alcoholConsumption_error", "The alcohol units field should be filled in");
            checkError = true;
        }*/
        
        //String weight = request.getParameter("weight");
        //String height = request.getParameter("length");
        String weight = (String) session.getAttribute("weight_text");
        String height = (String) session.getAttribute("length_text");
        if (weight != null && !weight.equalsIgnoreCase("")) {
            if (weight.matches("([0-9]+(\\.[0-9]+)?)+")) {
                if (height != null && !height.equalsIgnoreCase("")) {
                    if (height.matches("([0-9]+(\\.[0-9]+)?)+")) {
                        double heightValue = Double.parseDouble(height);
                        double weightValue = Double.parseDouble(weight);
                        diabetes.setHeight(heightValue);
                        diabetes.setWeight(weightValue);
                    } else {
                        session.setAttribute("length_error", "The length should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("length_error", "Require length to calculate BMI");
                    checkError = true;
                }
            } else {

                session.setAttribute("weight_error", "The weight should be a numeric value");
                checkError = true;
            }
        }/*else {

         session.setAttribute("weight_error", "The weight should be filled in");
         checkError = true;
         }*/
        
        if (height != null && !height.equalsIgnoreCase("")) {
            if (height.matches("([0-9]+(\\.[0-9]+)?)+")) {
                if (weight != null && !weight.equalsIgnoreCase("")) {
                    if (weight.matches("([0-9]+(\\.[0-9]+)?)+")) {
                        double heightValue = Double.parseDouble(height);
                        double weightValue = Double.parseDouble(weight);
                        diabetes.setHeight(heightValue);
                        diabetes.setWeight(weightValue);
                    } else {
                        session.setAttribute("weight_error", "The weight should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("weight_error", "Require weight to calculate BMI");
                    checkError = true;
                }
            } else {

                session.setAttribute("length_error", "The length should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("length_error", "The length should be filled in");
         checkError = true;
         }*/
        
        //String bmi = request.getParameter("bmi");
        String bmi = (String) session.getAttribute("bmi_text");
        if (bmi != null && !bmi.equalsIgnoreCase("")) {
            if (bmi.matches("([0-9]+(\\.[0-9]+)?)+")) {
                double bmiValue = Double.parseDouble(bmi);
                diabetes.setBmi(bmiValue);
                diabetes.setBmiDiabe(bmiValue);
            } else {

                session.setAttribute("bmi_error", "The BMI should be a numeric value");
                checkError = true;
            }
        } /*else {

             session.setAttribute("bmi_error", "The BMI should be filled in");
             checkError = true;
        }*/
        
        //String bloodPresureSystolic = request.getParameter("bpSystolic");
        //String bloodPressureDiastolic = request.getParameter("bpDiastolic");
        String bloodPresureSystolic = (String) session.getAttribute("bpSystolic_text");
        String bloodPressureDiastolic = (String) session.getAttribute("bpDiastolic_text");
        if (bloodPresureSystolic != null && !bloodPresureSystolic.equalsIgnoreCase("")) {
            if (bloodPresureSystolic.matches("[0-9]+")) {
                if (bloodPressureDiastolic != null && !bloodPressureDiastolic.equalsIgnoreCase("")) {
                    if (bloodPressureDiastolic.matches("[0-9]+")) {
                        int bloodvalueSys = Integer.parseInt(bloodPresureSystolic);
                        int bloodValueDia = Integer.parseInt(bloodPressureDiastolic);
                        diabetes.setBloodPressureSystolic(bloodvalueSys);
                        diabetes.setBloodPressureDiastolic(bloodValueDia);
                    } else {

                        session.setAttribute("bpDiastolic_error", "The blood pressure should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("bpDiastolic_error", "The blood pressure should be filled in");
                    checkError = true;
                }
            } else {

                session.setAttribute("bpSystolic_error", "The blood pressure should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bpSystolic_error", "The blood pressure should be filled in");
         checkError = true;
         }*/
        
        if (bloodPressureDiastolic != null && !bloodPressureDiastolic.equalsIgnoreCase("")) {
            if (bloodPressureDiastolic.matches("[0-9]+")) {
                if (bloodPresureSystolic != null && !bloodPresureSystolic.equalsIgnoreCase("")) {
                    if (bloodPresureSystolic.matches("[0-9]+")) {
                        int bloodvalueSys = Integer.parseInt(bloodPresureSystolic);
                        int bloodValueDia = Integer.parseInt(bloodPressureDiastolic);
                        diabetes.setBloodPressureSystolic(bloodvalueSys);
                        diabetes.setBloodPressureDiastolic(bloodValueDia);
                    } else {

                        session.setAttribute("bpSystolic_error", "The blood pressure should be a numeric value");
                        checkError = true;
                    }
                } else {
                    session.setAttribute("bpSystolic_error", "The blood pressure should be filled in");
                    checkError = true;
                }
            } else {

                session.setAttribute("bpDiastolic_error", "The blood pressure should be a numeric value");
                checkError = true;
            }
        } /*else {

         session.setAttribute("bpDiastolic_error", "The blood pressure should be filled in");
         checkError = true;
         }*/

        //String memberNumber = request.getParameter("memberNumber_text");
        String memberNumber = (String) session.getAttribute("memberNumber_text");
        if (memberNumber != null && !memberNumber.equalsIgnoreCase("")) {

            diabetes.setMemberNumber(memberNumber);
        }else {

            session.setAttribute("memberNumber_error", "The member number should be filled in");
            checkError = true;
        }
        
        //String dependantCode = request.getParameter("depListValues");
        String dependantCode = (String) session.getAttribute("depListValues_text");
        if (dependantCode != null && !dependantCode.equalsIgnoreCase("99") && !dependantCode.equalsIgnoreCase("")
                && !dependantCode.equalsIgnoreCase("null")) {

            int code = Integer.parseInt(dependantCode);
            diabetes.setDependantCode(code);
        } else {

            session.setAttribute("depListValues_error", "The cover dependants should be filled in");
            checkError = true;
        }

        //String hbAIC = request.getParameter("hbAIC");
        String hbAIC = (String) session.getAttribute("diabetesHbAIC_text");
        if(hbAIC != null && !hbAIC.equalsIgnoreCase("")) {
            if(hbAIC.matches("([0-9]+(\\.[0-9]+)?)+")){
            double hbAICValue = Double.parseDouble(hbAIC);
            diabetes.setHbAIC(hbAICValue);
            session.removeAttribute("diabetesHbAIC_text");
            }else {
            
                 session.setAttribute("diabetesHbAIC_error", "The HB AIC should be a numeric value");
                 checkError = true;
            }
         } /*else {

            session.setAttribute("hbAIC_error", "The HB AIC should be filled in");
            checkError = true;
         }*/
        
        //String randonGlucose = request.getParameter("randomGlucose");
        String randonGlucose = (String) session.getAttribute("diabetesRandomGlucose_text");
        if (randonGlucose != null && !randonGlucose.equalsIgnoreCase("")) {
            if(randonGlucose.matches("([0-9]+(\\.[0-9]+)?)+")){
            double randonGlucoseValue = Double.parseDouble(randonGlucose);
            diabetes.setRandomGlucose(randonGlucoseValue);
            session.removeAttribute("diabetesRandomGlucose_text");
            }else {
            
            session.setAttribute("diabetesRandomGlucose_error", "The random glucose should be a numeric value");
            checkError = true; 
            }
        } /*else {

            session.setAttribute("randomGlucose_error", "The random glucose should be filled in");
            checkError = true;
        }*/
        
        //String fastingGlucose = request.getParameter("fastingGlucose");
        String fastingGlucose = (String) session.getAttribute("diabetesFastingGlucose_text");
        if (fastingGlucose != null && !fastingGlucose.equalsIgnoreCase("")) {
            if(fastingGlucose.matches("([0-9]+(\\.[0-9]+)?)+")){
            double fastingGlucoseValue = Double.parseDouble(fastingGlucose);
            diabetes.setFastingGlucose(fastingGlucoseValue);
            session.removeAttribute("diabetesFastingGlucose_text");
            }else {
            
                session.setAttribute("diabetesFastingGlucose_error", "The fasting glucose should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("fastingGlucose_error", "The fasting glucose should be filled in");
            checkError = true;
        }*/
        
        //String totalCholesterol = request.getParameter("totalCholesterol");
        String totalCholesterol = (String) session.getAttribute("diabetesTotalCholesterol_text");
        if (totalCholesterol != null && !totalCholesterol.equalsIgnoreCase("")) {
            if(totalCholesterol.matches("([0-9]+(\\.[0-9]+)?)+")){
            double totalCholesterolValue = Double.parseDouble(totalCholesterol);
            diabetes.setTotalCholesterol(totalCholesterolValue);
            session.removeAttribute("diabetesTotalCholesterol_text");
            }else {
            
                session.setAttribute("diabetesTotalCholesterol_error", "The cholesterol should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("totalCholesterol_error", "The cholesterol should be filled in");
            checkError = true;
        }*/
        
        //String ldlcMol = request.getParameter("ldlc");
        String ldlcMol = (String) session.getAttribute("diabetesldlc_text");
        if (ldlcMol != null && !ldlcMol.equalsIgnoreCase("")) {
            if(ldlcMol.matches("([0-9]+(\\.[0-9]+)?)+")){
            double ldlcMolValue = Double.parseDouble(ldlcMol);
            diabetes.setLdlcMol(ldlcMolValue);
            session.removeAttribute("diabetesldlc_text");
            }else {
            
                session.setAttribute("diabetesldlc_error", "The LDLC should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("ldlc_error", "The LDLC should be filled in");
            checkError = true;
        }*/
        
        //String hdlcMol = request.getParameter("hdlc");
        String hdlcMol = (String) session.getAttribute("diabeteshdlc_text");
        if (hdlcMol != null && !hdlcMol.equalsIgnoreCase("")) {
            if(hdlcMol.matches("([0-9]+(\\.[0-9]+)?)+")){
            double hdlcMolValue = Double.parseDouble(hdlcMol);
            diabetes.setHdlcMol(hdlcMolValue);
            session.removeAttribute("diabeteshdlc_text");
            }else {
            
                 session.setAttribute("diabeteshdlc_error", "The HDLC should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("hdlc_error", "The HDLC should be filled in");
            checkError = true;
        }*/
        
        //String tryglyseride = request.getParameter("tryglyseride");
        String tryglyseride = (String) session.getAttribute("diabetesTryglyseride_text");
        if (tryglyseride != null && !tryglyseride.equalsIgnoreCase("")) {
            if(tryglyseride.matches("([0-9]+(\\.[0-9]+)?)+")){
            double tryglyserideValue = Double.parseDouble(tryglyseride);
            diabetes.setTryglyseride(tryglyserideValue);
            session.removeAttribute("diabetesTryglyseride_text");
            }else {
            
                session.setAttribute("diabetesTryglyseride_error", "The tryglyceride should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("tryglyseride_error", "The tryglyceride should be filled in");
            checkError = true;
        }*/
        
        //String proteinuria = request.getParameter("proteinuria");//proteinuria
        String proteinuria = (String) session.getAttribute("diabetesProteinuria_text");
        if (proteinuria != null && !proteinuria.equalsIgnoreCase("")) {
            if(proteinuria.matches("([0-9]+(\\.[0-9]+)?)+")){
            double proteinuriaValue = Double.parseDouble(proteinuria);
            if(proteinuriaValue > 1000){
                session.setAttribute("diabetesProteinuria_error", "The proteinuria value may not exceed 4 digits");
                checkError = true;
            }else{
                diabetes.setProteinurianGL(proteinuriaValue);
                session.removeAttribute("diabetesProteinuria_text");
            }
            }else {
            
                session.setAttribute("diabetesProteinuria_error", "The proteinuria should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("proteinuria_error", "The proteinuria should be filled in");
            checkError = true;
        }*/
        
        //String tchdcration = request.getParameter("tchdlRation");
        String tchdcration = (String) session.getAttribute("diabetesTchdlRation_text");
        if (tchdcration != null && !tchdcration.equalsIgnoreCase("")) {
            if(tchdcration.matches("([0-9]+(\\.[0-9]+)?)+")){
            double tchdcrationValue = Double.parseDouble(tchdcration);
            diabetes.setTchdlRatio(tchdcrationValue);
            session.removeAttribute("diabetesTchdlRation_text");
            }else {
            
                session.setAttribute("diabetesTchdlRation_error", "The HDL ratio should be a numeric value");
                checkError = true;
            }
        } /*else {

            session.setAttribute("tchdlRation_error", "The HDL ratio should be filled in");
            checkError = true;
        }*/
        
        //String detail = request.getParameter("detail");
        String detail = (String) session.getAttribute("diabetesDetail_text");
        if (detail != null && !detail.equalsIgnoreCase("")) {

            diabetes.setDetail(detail);
            session.removeAttribute("diabetesDetail_text");
        } /*else {

            session.setAttribute("detailr_error", "The detail should be filled in");
            checkError = true;
        }*/

        //String source = request.getParameter("source");
        String source = (String) session.getAttribute("source_text");
        if(source != null && !source.equalsIgnoreCase("99")) {

            diabetes.setSource(source);
        } /*else {

            session.setAttribute("source_error", "The source should be filled in");
            checkError = true;
        }*/

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String UpdateBiometrics = (String) session.getAttribute("UpdateBiometrics");
        if (UpdateBiometrics != null && UpdateBiometrics.equals("true")) {
            dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        }
       try {
            //String measuredDate = request.getParameter("dateMeasured");
            String measuredDate =  (String) session.getAttribute("dateMeasured_text");
            if(measuredDate != null && !measuredDate.equalsIgnoreCase("")){
            Date dateMeasured = dateFormat.parse(measuredDate);
            diabetes.setDateMeasured(convertDateXML(dateMeasured));
            }else {

                session.setAttribute("dateMeasured_error", "The date measured should be filled in");
                checkError = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");

            //String receivedDate = request.getParameter("dateReceived");
            String receivedDate = (String) session.getAttribute("dateReceived_text");
            if(receivedDate != null && !receivedDate.equalsIgnoreCase("")) {
                Date dateReceived = sdf2.parse(receivedDate);
                  diabetes.setDateReceived(convertDateXML(dateReceived));
            }else {

                session.setAttribute("dateReceived_error", "The date received should be filled in");
                checkError = true;
            }
        } catch (Exception ex) {

            ex.printStackTrace();
        }

        //String treatingProvider = request.getParameter("providerNumber_text");
        String treatingProvider = (String) session.getAttribute("providerNumber_text");
        if (treatingProvider != null && !treatingProvider.equalsIgnoreCase("")) {

            diabetes.setTreatingProvider(treatingProvider);
        } /*else {

            session.setAttribute("providerNumber_error", "The treating provider should be filled in");
            checkError = true;
        }*/
        
        //String icd10 = request.getParameter("icd10");
        String icd10code = (String) session.getAttribute("icd10_text");
        String icd10 = (String) session.getAttribute("diabetesLookupValue");
        if (icd10 != null && !icd10.equalsIgnoreCase("99")) {

            diabetes.setIcd10(icd10);
            diabetes.setIcd10Code(icd10code.toUpperCase());
            
            LookupValue icd = new LookupValue();
            icd.setId(icd10);
            icd.setValue(port.getValueFromCodeTableForId("Diabetes Biometric ICD", icd10));
            diabetes.setIcd10Lookup(icd);
        }else {

            session.setAttribute("icd10_error", "The ICD10 should be filled in");
            checkError = true;
        }

        try {

            if(checkError) {

                saveScreenToSession(request);
                nextJSP = "/biometrics/CaptureBiometrics.jsp";     //CaptureDiabetes.jsp";

            }else {
                //Check if the user is UPDATING or CREATING
                if (UpdateBiometrics != null && UpdateBiometrics.equals("true")) {
                    String oldBioID = (String) session.getAttribute("BiometricID");
                    System.out.println("OLDBIOID = " + oldBioID);
                    
                    int userId = user.getUserId();
                    //clearAllFromSession(request);
                    BiometricsId bId = port.saveDiabetesBiometrics(diabetes, userId, 1, oldBioID);
                    request.setAttribute("revNo",bId.getReferenceNo());
                    int diabetesId = bId.getId();
                    nextJSP = "/biometrics/CaptureBiometrics.jsp";     //CaptureDiabetes.jsp";
                    if(diabetesId > 0) {
                        diabetes.setBiometricsId(diabetesId);
    //                    port.createTrigger(diabetes);
                        session.setAttribute("updated", "Your biometrics data has been captured");
                    }else{
                        session.setAttribute("failed", "Diabetes! Asthma Biometrics save has failed, Please try again!");
                        nextJSP = "/biometrics/CaptureBiometrics.jsp";     //CaptureDiabetes.jsp";
                    }
                } else {
                    int userId = user.getUserId();
                    //clearAllFromSession(request);
                    BiometricsId bId = port.saveDiabetesBiometrics(diabetes, userId, 1, null);
                    request.setAttribute("revNo",bId.getReferenceNo());
                    int diabetesId = bId.getId();
                    nextJSP = "/biometrics/CaptureBiometrics.jsp";     //CaptureDiabetes.jsp";
                    if(diabetesId > 0) {
                        diabetes.setBiometricsId(diabetesId);
    //                    port.createTrigger(diabetes);
                        session.setAttribute("updated", "Your biometrics data has been captured");
                    }else{
                        session.setAttribute("failed", "Diabetes! Asthma Biometrics save has failed, Please try again!");
                        nextJSP = "/biometrics/CaptureBiometrics.jsp";     //CaptureDiabetes.jsp";
                    }
                }
            }

            
            RequestDispatcher dispatcher = context.getRequestDispatcher(nextJSP);
            dispatcher.forward(request, response);
            
        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

    public XMLGregorianCalendar convertDateXML(Date date) {

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getDatatypeFactory().newXMLGregorianCalendar(calendar);

    }

    private DatatypeFactory getDatatypeFactory() {
        if (_datatypeFactory == null) {

            try {
                _datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException ex) {
                ex.printStackTrace();
            }
        }

        return (DatatypeFactory) _datatypeFactory;
    }

    @Override
    public String getName() {
        return "CaptureDiabetesBiometericsCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.auth.tags;

import com.koh.auth.dto.ICD10SearchResult;
import java.util.ArrayList;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author gerritj
 */
public class ICD10SearchResultTable extends TagSupport {

    private String commandName;
    private String javascript;

    /**
     * Called by the container to invoke this tag. 
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        ServletRequest req = pageContext.getRequest();
        try {
            out.println("<label class=\"header\">Search Results</label></br></br>");

            String message = (String) req.getAttribute("searchICD10ResultMessage");

            if (message != null && !message.equalsIgnoreCase(""))
            {
                out.println("<label class=\"red\">" + message + "</label></br></br>");
            }

            out.println("<table class=\"list\" style=\"border-style:none; border-collapse:collapse; border-width:1px;\">");
            out.println("<tr><th>Code</th><th>Description</th>" +
                    "<th>Select</th></tr>");

            ArrayList<ICD10SearchResult> icd10s = (ArrayList<ICD10SearchResult>) req.getAttribute("searchICD10Result");
            if (icd10s != null) {
                for (int i = 0; i < icd10s.size(); i++) {
                    ICD10SearchResult iCD10SearchResult = icd10s.get(i);
                    out.println("<tr><form method=\"post\"><td><label class=\"label\">" + iCD10SearchResult.getCode() + "</label><input type=\"hidden\" name=\"code\" value=\"" + iCD10SearchResult.getCode() + "\"/></td>" +
                            "<td><label class=\"label\">" + iCD10SearchResult.getDescription() + "</label><input type=\"hidden\" name=\"description\" value=\"" + iCD10SearchResult.getDescription() + "\"/></td>" +
                            "<td><button name=\"opperation\" name=\"opperation\" type=\"submit\" " + javascript + " value=\"" + commandName + "\">Select</button></td></form></tr>");

                }
            }
            out.println("</table>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in ICD10SearchResultTable tag", ex);
        }
        return super.doEndTag();
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" language="JavaScript">
            
                 //new member search
            function getCoverByNumber(str, element) {
                var ap = $("#authPeriod").val();
                if (ap != "99") {
                    submitWithAction('SearchAuthAmukelanim', 'refNo', '/PreAuth/AmukelaniSearch.jsp');
                }

            }

            function submitWithAction(action) {
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById("onScreen").value = onScreen;
                document.getElementById("searchCalled").value = forField;
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }

            function hideRows(id) {
                document.getElementById(id).style.display = 'none';
                document.getElementById(id + 'plus').style.display = '';
                document.getElementById(id + 'minus').style.display = 'none';
            }
            function showRows(id) {
                document.getElementById(id).style.display = '';
                document.getElementById(id + 'minus').style.display = '';
                document.getElementById(id + 'plus').style.display = 'none';

            }
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    
                    <label class="header">Amu Auth Search</label>
                    <br/><br/>
                    <table>
                        <agiletags:ControllerForm name="searchAuthForm">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            
                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Reference Number" elementName="refNo" searchFunction="yes" searchOperation="ForwardToAdvanceSearchAmu" onScreen="/PreAuth/AmukelanimSearch.jsp" mandatory="no" javaScript="onblur=\"getCoverByNumber(this.value, 'refNo');\""/></tr>

                            <tr><agiletags:ButtonOpperation type="button" align="left" span="1" commandName="SearchAuthAmukelanim" displayname="Search" javaScript="onClick=\"submitWithAction('SearchAuthAmukelanim');\"" /></tr>

                        </agiletags:ControllerForm>
                    </table>
                    <br/>
                    <HR color="#666666" WIDTH="50%" align="left">
                    <br/>
                    <agiletags:AmukelaniTag />

                    
                    <br><br>
                    <!-- content ends here -->
                </td></tr></table>
    </body>
</html>


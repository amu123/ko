<%-- 
    Document   : PDCDiseaseList
    Created on : May 27, 2016, 10:47:19 AM
    Author     : nick
--%>

<%@tag description="Display All Available Chronic Diseases" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ attribute name="javascript" rtexprvalue="true" required="false" %>

<table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
    <tr>
        <th>Condition Name</th>
        <th></th>
    </tr>
    <c:forEach var="entry" items="${sessionScope.cdlCarepathListPDC}"> 
        <tr>
            <td><label style="width: 300px; text-align: center">${entry.carePathName}</label></td>
            <td><input type="checkbox" name="cpCheck" id="cpCheck" value="${entry.carePathID}"></select></td>
        </tr>
    </c:forEach>
</table>
<br/>
<button 
    style="width: 300px; text-align: center" 
    type="button" 
    name="CreateDependantCarePathCommand" 
    onclick="submitCarePathsWithAction('CreateDependantCarePathCommand');">
    Generate Care Path(s)
</button>


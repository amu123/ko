<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <label class="header">Group Search</label>
     <agiletags:ControllerForm name="MemberEmployerSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="MemberEmployerSearchCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="EmployerSearch" />
        <input type="hidden" name="target_div" value="${target_div}" />
        <input type="hidden" name="main_div" value="${main_div}" />
        <input type="hidden" name="diagCode" id="icd10DiagCode" value="" />
        <input type="hidden" name="diagName" id="icd10DiagName" value="" />
        <input type="hidden" name="diagCodeId" id="icd10DiagCodeId" value="${diagCodeId}" />
        <input type="hidden" name="diagNameId" id="icd10DiagNameId" value="${diagNameId}" />
        <input type="hidden" name="productId" id="productId" value="${ProductId}" />
        <input type="hidden" name="groupType" id="groupType" value="${groupType}" />
        <table>
            <tr><agiletags:LabelTextBoxError displayName="Group Number" elementName="groupCode"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Group Name" elementName="groupName"/></tr>
            <tr>
                <td><input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
                <td align="right"><input type="button" value="Search" onclick="submitFormWithAjaxPost(this.form, 'GroupSearchResults');" ${noProductId}></td>
            </tr>
        </table><br>
        <c:if test="${noProductId == 'disabled'}">
            <table>
                <tr>
                    <td><label class="error">Select scheme under plan and product selection for group search</label></td>
                </tr>
            </table>
        </c:if>
      
      <div style ="display: none"  id="GroupSearchResults"></div>
      </agiletags:ControllerForm>
<%-- 
    Document   : MemberClaimsDetails
    Created on : 2011/08/19, 05:33:21
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script>
            
            $(function() {
                //For Practice validation
                var searchVal = document.getElementById('searchCalled').value;
                if (searchVal == 'practiceNumber'){
                    validatePractice(document.getElementById('practiceNumber_text').value, 'practiceNumber');
                }
                document.getElementById('searchCalled').value = '';

            });
            
            function GetXmlHttpObject(){
                if (window.ActiveXObject){
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest){
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }
            
            //practice search
            function validatePractice(str, element){
                xhr = GetXmlHttpObject();
                if (xhr==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var error = "#"+element+"_error";
                if (str.length > 0) {
                    var url="/ManagedCare/AgileController";
                    url=url+"?opperation=FindPracticeDetailsByCode&provNum="+str+"&element="+element;
                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                            $(document).find(error).text("");
                            if(result == "Error"){
                                $(document).find(error).text("No such provider");
                            }else if(result == "Done"){
                                $(document).find(error).text("");
                            }
                        }
                    };
                    xhr.open("POST",url,true);
                    xhr.send(null);
                } else {
                    $(document).find(error).text("");
                }
            }
            
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;         
                document.forms[0].submit();
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;            
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">

                    <agiletags:ClaimsTabbedButton buttonCommand2="ViewMemberPreAuthorisationCommand" buttonDisplay2="Authorisation"
                                                  buttonCommand3="ViewMemberAddressDetailsCommand" buttonDisplay3="Address Details"
                                                  buttonCommand4="ForwardToMemberBenefitsCommand" buttonDisplay4="Benefit Details"
                                                  buttonCommand5="ForwardToMemberClaimsCommand" buttonDisplay5="Claims"
                                                  buttonCommand6="ViewMemberPaymentDetailsCommand" buttonDisplay6="Payment Details"
                                                  buttonCommand7="ViewMemberBankingDetailsCommand" buttonDisplay7="Banking Details"
                                                  buttonCommand8="ForwardToMemberStatementsCommand" buttonDisplay8="Statements"
                                                  buttonCommand1="ForwardToMemberDetailsCommand" buttonDisplay1="Policy Holder Details"  disabledDisplay="Claims" numberOfButtons="10"/>

                    <br/><br/>
                    <table>
                        <agiletags:ControllerForm name="practiceDetails">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="listIndex" id="listIndex" value=""/>

                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Provider Number" elementName="practiceNumber" searchFunction="yes" searchOperation="ForwardToSearchPracticeCommand" onScreen="/Claims/MemberClaimsDetails.jsp" mandatory="no" javaScript="onChange=\"validatePractice(this.value, 'practiceNumber');\""/></tr>
                            <tr><agiletags:LabelTextBoxDate displayname="Treatment Date From" elementName="treatmentFrom"/></tr>
                            <tr><agiletags:LabelTextBoxDate displayname="Treatment Date To" elementName="treatmentTo"/></tr>

                            <tr><agiletags:ButtonOpperation align="left" span="1" type="button" commandName="SearchMemberClaimsCommand" displayname="Search Claims" javaScript="onClick=\"submitWithAction('SearchMemberClaimsCommand');\""/></tr>


                        </agiletags:ControllerForm>
                    </table>
                    
                </td></tr></table>
    </body>
</html>

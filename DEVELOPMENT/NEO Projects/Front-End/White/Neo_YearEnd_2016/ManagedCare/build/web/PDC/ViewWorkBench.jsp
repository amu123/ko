<%-- 
    Document   : ViewWorkBench.jsp
    Created on : 2010/04/15, 10:29:38
    Author     : princes
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.scrollTo-1.4.2-min.js"></script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
        <title>PDC WorkBench</title>      
        <script type="text/javascript" language="JavaScript">

            $(function () {
                var pdcAdmin = document.getElementById('pdcAdminYesNo').value;

                if (pdcAdmin !== null && (pdcAdmin === true || pdcAdmin === "true")) {
                    $("#taskListRow").show();
                    $("#taskUserRow").show();
                    $("#searchTaskRow").show();
                } else {
                    $("#taskListRow").hide();
                    $("#taskUserRow").hide();
                    $("#searchTaskRow").hide();
                }
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.forms[0].submit();
            }

            function submitDetailsWithAction(action, workListID) {
                document.getElementById('opperation').value = action;
                document.getElementById('workListID').value = workListID;
                document.forms[0].submit();
            }

            function submitPageAction(action, pageDirection) {
                document.getElementById('opperation').value = action;
                document.getElementById('pageDirection').value = pageDirection;
                document.forms[0].submit();
            }

            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }
            function enableTaskSearch() {
                var status = 0;
                status = document.getElementById("tasksPerUser").value;
                if (status === 0) {
                    document.getElementById("btnSearch").disabled = true;
                } else {
                    document.getElementById("btnSearch").disabled = false;
                }
            }

        </script>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="10px"></td><td align="left">
                    <!-- content goes here -->       
                    <c:choose>
                        <c:when test="${applicationScope.Client == 'Sechaba'}">
                            <label class="header">PCC - View Work Bench</label>
                        </c:when>    
                        <c:otherwise>
                            <label class="header">PDC - View Work Bench</label>
                        </c:otherwise>
                    </c:choose>
                    <br/><br/>

                    <agiletags:ControllerForm name="viewWorkBench">
                        <input type="hidden" name="workListID" id="workListID" value="" />
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <input type="hidden" name="listIndex" id="listIndex" value="" />
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <input type="hidden" name="pageDirection" id="pageDirection" value="" />
                        <input type="hidden" name="refreshScreen" id="refreshScreen" value="" />   
                        <input type="hidden" name="pdcUserYesNo" id="pdcUserYesNo" value="${sessionScope.persist_user_pdcUser}" />
                        <input type="hidden" name="pdcAdminYesNo" id="pdcAdminYesNo" value="${sessionScope.persist_user_pdcAdmin}" />
                        <table>
                            <tr><agiletags:LabelTextBoxError displayName="Policy Number" elementName="policyNumber" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxDateTime elementName="dateFrom" displayName="Date From" mandatory="yes"/></tr>
                            <tr><agiletags:LabelTextBoxDateTime elementName="dateTo" displayName="Date To" mandatory="yes"/></tr>
                            <tr><agiletags:LabelNeoLookupValueDropDown displayName="Type" elementName="pdcType" lookupId="103"/></tr>
                            <tr><agiletags:LabelNeoLookupValueDropDown displayName="Risk Rating" elementName="riskRating" lookupId="175"/></tr>
                            <c:if test="${sessionScope.persist_user_pdcAdmin || sessionScope.persist_user_pdcAdmin == 'true'}">
                                <tr><agiletags:LabelNoeUserByResposibilityDropDown elementName="pdcUser" displayName="Assign To" resposibilityId="20"/></tr>
                            </c:if>
                            <tr><agiletags:LabelNeoLookupValueDropDown displayName="Event Status" elementName="eventStatus" lookupId="95"/></tr>              
                            <tr></tr>
                            <tr><agiletags:ButtonOpperation align="left" displayname="Search" commandName="ViewOpenBenchmarkCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('ViewOpenBenchmarkCommand')\";"/></tr>
                            <tr><td><hr/></td></tr>
                            <!--<tr id="taskListRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Tasks" elementName="tasks" lookupId="95" firstIndexSelected="yes" /></tr>
                            <tr id="taskUserRow"><agiletags:LabelNoeUserByResposibilityDropDown displayName="Tasks per user" elementName="tasksPerUser" resposibilityId="20" javaScript="onChange=enableTaskSearch();"/></tr>
                            <tr></tr>
                            <tr id="searchTaskRow"><td><input id="btnSearch" type="button" disabled="false" value="Search Task" onclick="submitWithAction('ViewPDCTasksByUserCommand');"/></td></tr>-->
                        </table>
                        <br/>
                        <c:choose>
                            <c:when test="${!empty sessionScope.eventTaskTable}">
                                <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                    <agiletags:PDCViewTasksByUserTag select="yes" javascript="onClick=\"submitWithAction('AssignTaskForTaskTableCommand', this.value)\";"/>
                                </table>  
                            </c:when>
                            <c:otherwise>
                                <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                    <agiletags:ViewWorkBenchTable/>
                                </table>  
                            </c:otherwise>
                        </c:choose>
                    </agiletags:ControllerForm>                   
                    <br/><br/><br/><br/>
                </td></tr></table>
    </body>
</html>

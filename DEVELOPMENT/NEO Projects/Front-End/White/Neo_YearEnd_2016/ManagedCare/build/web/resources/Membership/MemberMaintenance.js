/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//RETURN SUBMIT TO RETURN MEMBER PAGES
function submitGenericReload(action, onScreen) {
    document.getElementById("onScreen").value = onScreen;
    document.getElementById("opperation").value = action;
    document.forms[0].submit();
}

function GetXmlHttpObject() {
    if (window.ActiveXObject) {
        // code for IE6, IE5
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        return new XMLHttpRequest();
    }
    return null;
}


//DOM PARSER FOR XML OF DROPDOWNS RETURNED BY AJAX            
function GetDOMParser(xmlStr) {
    var xmlDoc;
    if (window.DOMParser) {
        parser = new DOMParser();
        xmlDoc = parser.parseFromString(xmlStr, "text/xml");
    } else {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = "false";
        xmlDoc.loadXML(xmlStr);
    }
    return xmlDoc;
}

//FUNCTION TO LOAD BRANCHES WHEN BANK NAME CHANGES
function toggleBranch(bankId, element2Toggle) {
    if (bankId != "99") {
        addBankBranch(bankId, element2Toggle);
    }
}

//AJAX FUNCTION TO GET BRANCHES FOR BANK ID
function addBankBranch(bankId, element2Toggle) {

    var element2ToggleJQ = "#" + element2Toggle;

    var url = "/ManagedCare/AgileController";
    var data = {
        'opperation': 'GetBankBranchByBankIdCommand',
        'bankId': bankId,
        'id': new Date().getTime()
    };


    $.get(url, data, function(resultTxt) {
        if (resultTxt != null) {
            $(element2ToggleJQ).empty();
            $(element2ToggleJQ).append("<option value=\"99\"></option>");
            var xmlDoc = GetDOMParser(resultTxt);
            //print value from servlet
            $(xmlDoc).find("BankBranches").each(function() {
                //set product details
                $(this).find("Branch").each(function() {
                    var prodOpt = $(this).text();
                    var opt = prodOpt.split("|");
                    //Add option to dependant dropdown
                    var optVal = document.createElement('OPTION');
                    optVal.value = opt[0];
                    optVal.text = opt[1];
                    document.getElementById(element2Toggle).options.add(optVal);

                });
            });
        }
        //SET RETURN TYPE AS XML    
    }, "text/xml");
}

function addProductOption(prodId, element2Toggle) {
    var elem = "#" + element2Toggle;
    var url = "/ManagedCare/AgileController";

    var data = {
        'opperation': 'GetProductOptionCommand',
        'prodId': prodId,
        'id': new Date().getTime()
    }

    //GET RESPONSE FROM BACKEND
    $.get(url, data, function(result) {
        $(elem).empty();
        $(elem).append("<option value=\"99\"></option>");
        var xmlDoc = GetDOMParser(result);
        //print value from servlet
        $(xmlDoc).find("ProductOptions").each(function() {
            //set product details
            $(this).find("Option").each(function() {
                var prodOpt = $(this).text();
                var opt = prodOpt.split("|");
                //Add option to dependant dropdown
                var optVal = document.createElement('OPTION');
                optVal.value = opt[0];
                optVal.text = opt[1];
                document.getElementById(element2Toggle).options.add(optVal);

            });
        });

    }, 'XML');
}
    


    function populateProductOption(prodId) {


    var url = "/ManagedCare/AgileController";

    var data = {
        'opperation': 'GetProductOptionCommand',
        'prodId': prodId,
        'id': new Date().getTime()
    }

    //GET RESPONSE FROM BACKEND
    $.get(url, data, function(result) {

        $("#optionList").empty();
        $("#optionList").append("<option value=\"99\"></option>");

        //PARSE RESULT TO XML MODEL
        var xmlDoc = GetDOMParser(result);
        //print value from servlet
        var productOption = $(xmlDoc).find("ProductOptions");

        var currentOption = $("#CoverDetails_option").val();

        var optionsList = '';

        for (x = 0; x < productOption.length; x++) {

            var options = $(productOption[x]).find("Option")

            for (i = 0; i < options.length; i++) {

                var prodOpt = $(options[i]).text();

                var opt = prodOpt.split("|");

                //Add option to dependant dropdown
                var optVal = document.createElement('OPTION');
                optVal.value = opt[0];
                optVal.text = opt[1];

                if (currentOption == optVal.value) {
                    optionsList += '<option selected value="' + optVal.value + '">' + optVal.text + '</option>';
                } else {
                    optionsList += '<option value="' + optVal.value + '">' + optVal.text + '</option>';
                }

            }
        }

        //APPEND TO SELECT BOX
        $("#optionList").append(optionsList);

        $("#optionList").change(function(e) {
            if ($(this).val() != $("#CoverDetails_option").val()) {
                $("#CoverDetails_optionIdStartDate").show();
                $("#CoverDetails_optionIdStartDate").css('visibility', 'visible');
            } else {
                $("#CoverDetails_optionIdStartDate").css("visibility", "hidden");
            }

        });

        //SET RETURN TYPE AS XML    
    }, 'XML');

}

function populateEmployerList() {

    var url = "/ManagedCare/AgileController";

    var data = {
        'opperation': 'EmployerListCommand',
        'id': new Date().getTime()
    }

    //GET RESPONSE FROM BACKEND
    $.getJSON(url, data, function(result) {

        var employerList = "<option value='99'></option>";

        $.each(result, function(key, value) {

            if (value.selected) {
                employerList += '<option selected value="' + value.employerEntityId + '">' + value.name + '</option>';

                //SET TEXT HIDDEN TEXT FIELD TO ENTITY ID FOR ON CHANGE EVENT LOGIC
                $("#CoverDetails_employerEntityId").val(value.employerEntityId);
            } else {
                employerList += '<option value="' + value.employerEntityId + '">' + value.name + '</option>';
            }
        });


        $("#employerList").empty();
        $("#employerList").append(employerList);

        //CHANGE EVENT FOR SELECT BOX
        $("#employerList").change(function(e) {

            if ($("#CoverDetails_employerEntityId").val() != $("#employerList").val()) {
                $("#CoverDetails_employerStartDate").show();
                $("#CoverDetails_employerStartDate").css('visibility', 'visible');
            } else {
                $("#CoverDetails_employerStartDate").css("visibility", "hidden");
            }

        });

        $("#MemberCover").unmask();

    });

}
function submitActionAndDocType(thisForm, action, docType) {
    thisForm.opperation.value = action;
    thisForm.docType.value = docType;
    thisForm.entityId.value = document.getElementById("memberEntityId").value;
//    thisForm.submit(); 
// submitFormWithAjaxPost(thisForm, 'MemberCoverDetails');
}
function submitActionAndFile(thisForm, action, fileLocation) {
    thisForm.opperation.value = action;
    thisForm.fileLocation.value = fileLocation;

}
function submitActionAndFileView(thisForm, action, fileLocation) {
    thisForm.opperation.value = action;
    thisForm.fileLocation.value = fileLocation;
    thisForm.submit();
}

function submitActionAndViewFile(thisForm, action, fileLocation) {
    thisForm.opperation.value = action;
    thisForm.fileLocation.value = fileLocation;
    thisForm.submit();
}
function submitActionAndFileReIndex(thisForm, action, entityType, fileLocation, docType, entityNumber, refNumber, indexID) {
    //alert("submitActionAndFileReIndex Called");
    thisForm.opperation.value = action;
    thisForm.fileLocation.value = fileLocation;
    thisForm.entityType.value = entityType;
    thisForm.docType.value = docType;
    thisForm.entityNumber.value = entityNumber;
    thisForm.refNumber.value = refNumber;
    thisForm.indexID.value = indexID;
}
function saveIncome(btn) {
    btn.disabled = true;
    document.getElementById('memberIncomeButtonPressed').value = 'SaveMemberIncome';
    formVal = document.getElementById("MemberIncomeForm");
    var params = encodeFormForSubmit(formVal);
    $(formVal).mask("Saving...");
    $.post(formVal.action, params, function(result) {
        updateFormFromData(result);
        $(formVal).unmask();
        if (document.getElementById('incomeStatus').value == 'ok') {
            swapDivVisbible('overlay', 'main_div');
        } else {
            btn.disabled = false;
        }
    });

}

function saveSubsidy(btn) {
    btn.disabled = true;
    document.getElementById('memberIncomeButtonPressed').value = 'SaveMemberSubsidy';
    formVal = document.getElementById("MemberIncomeForm");
    var params = encodeFormForSubmit(formVal);
    $(formVal).mask("Saving...");
    $.post(formVal.action, params, function(result) {
        updateFormFromData(result);
        $(formVal).unmask();
        if (document.getElementById('subsidyStatus').value == 'ok') {
            swapDivVisbible('overlay', 'main_div');
        } else {
            btn.disabled = false;
        }
    });

}

function saveDebitDate(btn) {
    btn.disabled = true;
    document.getElementById('memberIncomeButtonPressed').value = 'SaveMemberDebitDate';
    formVal = document.getElementById("MemberIncomeForm");
    var params = encodeFormForSubmit(formVal);
    $(formVal).mask("Saving...");
    $.post(formVal.action, params, function(result) {
        updateFormFromData(result);
        $(formVal).unmask();
        if (document.getElementById('incomeStatus').value == 'ok') {
            swapDivVisbible('overlay', 'main_div');
        } else {
            btn.disabled = false;
        }
    });

}

function savePaymentMethod(btn) {
    btn.disabled = true;
    document.getElementById('memberIncomeButtonPressed').value = 'SaveMemberPaymentMethod';
    formVal = document.getElementById("MemberIncomeForm");
    var params = encodeFormForSubmit(formVal);
    $(formVal).mask("Saving...");
    $.post(formVal.action, params, function(result) {
        updateFormFromData(result);
        $(formVal).unmask();
        if (document.getElementById('incomeStatus').value == 'ok') {
            swapDivVisbible('overlay', 'main_div');
        } else {
            btn.disabled = false;
        }
    });

}

function saveDepType(btn) {
    btn.disabled = true;
    document.getElementById('memberDepTypeButtonPressed').value = 'SaveDepType';
    formVal = document.getElementById("MemberDependantTypeForm");
    var params = encodeFormForSubmit(formVal);
    $(formVal).mask("Saving...");
    $.post(formVal.action, params, function(result) {
        updateFormFromData(result);
        $(formVal).unmask();
        if (document.getElementById('dependantStatus').value == 'ok') {
            swapDivVisbible('overlay', 'main_div');
        } else {
            btn.disabled = false;
        }
    });

}

function SaveFinType(btn) {
    btn.disabled = true;
    document.getElementById('memberDepTypeButtonPressed').value = 'SaveFinType';
    formVal = document.getElementById("MemberDependantTypeForm");
    var params = encodeFormForSubmit(formVal);
    $(formVal).mask("Saving...");
    $.post(formVal.action, params, function(result) {
        updateFormFromData(result);
        $(formVal).unmask();
        if (document.getElementById('dependantStatus').value == 'ok') {
            swapDivVisbible('overlay', 'main_div');
        } else {
            btn.disabled = false;
        }
    });

}

function SaveRelType(btn) {
    btn.disabled = true;
    document.getElementById('memberDepTypeButtonPressed').value = 'SaveRelType';
    formVal = document.getElementById("MemberDependantTypeForm");
    var params = encodeFormForSubmit(formVal);
    $(formVal).mask("Saving...");
    $.post(formVal.action, params, function(result) {
        updateFormFromData(result);
        $(formVal).unmask();
        if (document.getElementById('dependantStatus').value == 'ok') {
            swapDivVisbible('overlay', 'main_div');
        } else {
            btn.disabled = false;
        }
    });

}

function saveStudent(btn) {
    btn.disabled = true;
    document.getElementById('memberDepTypeButtonPressed').value = 'SaveStudent';
    formVal = document.getElementById("MemberDependantTypeForm");
    var params = encodeFormForSubmit(formVal);
    $(formVal).mask("Saving...");
    $.post(formVal.action, params, function(result) {
        updateFormFromData(result);
        $(formVal).unmask();
        if (document.getElementById('dependantStatus').value == 'ok') {
            swapDivVisbible('overlay', 'main_div');
        } else {
            btn.disabled = false;
        }
    });

}

function saveHandicap(btn) {
    btn.disabled = true;
    document.getElementById('memberDepTypeButtonPressed').value = 'SaveHandicap';
    formVal = document.getElementById("MemberDependantTypeForm");
    var params = encodeFormForSubmit(formVal);
    $(formVal).mask("Saving...");
    $.post(formVal.action, params, function(result) {
        updateFormFromData(result);
        $(formVal).unmask();
        if (document.getElementById('dependantStatus').value == 'ok') {
            swapDivVisbible('overlay', 'main_div');
        } else {
            btn.disabled = false;
        }
    });

}

function calculateAge(dobString) {
    var dob = new Date(dobString);
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    var birthdayThisYear = new Date(currentYear, dob.getMonth(), dob.getDate());
    var age = currentYear - dob.getFullYear();

    if (birthdayThisYear > currentDate) {
        age--;
    }

    return age;
}

function populateBrokerList() {

    var url = "/ManagedCare/AgileController";

    var data = {
        'opperation': 'BrokerListCommand',
        'id': new Date().getTime()
    }


    //GET RESPONSE FROM BACKEND
    $.getJSON(url, data, function(response) {

        var brokerList = "<option value='99'></option>";

        if (response != null) {
            $.each(response, function(key, value) {
                if (value.selected) {
                    brokerList += '<option selected value="' + value.brokerEntityId + '">' + value.name + '</option>';
                    $("#CoverDetails_brokerEntityId").val(value.brokerEntityId);
                } else {
                    brokerList += '<option value="' + value.brokerEntityId + '">' + value.name + '</option>';
                }
            });
        }


        $("#brokerList").empty();
        $("#brokerList").append(brokerList);

        //CHANGE EVENT FOR SELECT BOX
        $("#brokerList").change(function(e) {

            if ($("#CoverDetails_brokerEntityId").val() != $("#brokerList").val()) {
                $("#CoverDetails_brokerStartDate").show();
                $("#CoverDetails_brokerStartDate").css('visibility', 'visible');
            } else {
                $("#CoverDetails_brokerStartDate").css("visibility", "hidden");
            }
        });

    });

}

function handleRadioButtons() {

    $("#radioResignations,#radioReinstatements").change(function(e) {

        var radioB = $(e.target);

        var dependantListRow = $("#dependantListRow");

        switch (radioB.val()) {

            case 'Resignations' :
                dependantListRow.css("visibility", "visible")
                break;

            case 'Reinstatements' :
                dependantListRow.css("visibility", "visible")
                break;

            default :
                alert("nope");
        }

        return false;
    });

}

function checkIfCoverIsSuspended() {

    var url = "/ManagedCare/AgileController";

    var data = {
        'opperation': 'CheckCoverSuspensionCommand',
        'id': new Date().getTime()
    }

    //GET RESPONSE FROM BACKEND
    $.getJSON(url, data, function(result) {

        var brokerList = "<option value='99'></option>";

        $.each(result, function(key, value) {

            if (value.selected) {
                brokerList += '<option selected value="' + value.brokerEntityId + '">' + value.name + '</option>';
                $("#CoverDetails_brokerEntityId").val(value.brokerEntityId);
            } else {
                brokerList += '<option value="' + value.brokerEntityId + '">' + value.name + '</option>';
            }
        });

        //CoverDetails_employerEntityId

        $("#brokerList").empty();
        $("#brokerList").append(brokerList);

        //CHANGE EVENT FOR SELECT BOX
        $("#brokerList").change(function(e) {

            if ($("#CoverDetails_brokerEntityId").val() != $("#brokerList").val()) {
                $("#CoverDetails_brokerStartDate").show();
                $("#CoverDetails_brokerStartDate").css('visibility', 'visible');
            } else {
                $("#CoverDetails_brokerStartDate").css("visibility", "hidden");
            }
        });
    });

}

function resignMember(btn, main_div, target_div) {
    btn.disabled = true;
    document.getElementById('disorderValuesButtonPressed').value = 'CheckClaimsAndAuthsAfterDate';
    formVal = document.getElementById("ResignMemberForm");
    var params = encodeFormForSubmit(formVal);
    $(formVal).mask("Checking Claims and Pre-Auths");
    $.post(formVal.action, params, function(result) {
        var cont = true;
        if ($.trim(result).length != 0) {
            var r = window.confirm(result);
            if (r == false) {
                cont = false;
            }
        }
        $(formVal).unmask();
        if (cont) {
            document.getElementById('disorderValuesButtonPressed').value = 'CheckMemTypeAndGroupType';
            $(formVal).mask("Checking Member Type and Group Type");
            params = encodeFormForSubmit(formVal);
            $.post(formVal.action, params, function(result) {
                var cont = true;
                if ($.trim(result).length != 0) {
                    var r = window.confirm(result);
                    if (r == false) {
                        cont = false;
                    }
                }
                $(formVal).unmask();
                if (cont) {
                    document.getElementById('disorderValuesButtonPressed').value = 'SaveResignMemberButton';
                    submitFormWithAjaxPost(formVal, 'MemberCover', btn, main_div, target_div);
                }


                else {
                    btn.disabled = false;

                }
            });
        }
    });
}

function SuspendMember(btn, main_div, target_div) {
    document.getElementById('disorderValuesButtonPressed').value = 'CheckMemTypeAndGroupType';
    formVal = document.getElementById("SuspendMemberForm");
    var params = encodeFormForSubmit(formVal);
    $(formVal).mask("Checking Member Type and Group Type");
    $.post(formVal.action, params, function(result) {
        var cont = true;
        if ($.trim(result).length != 0) {
            var r = window.confirm(result);
            if (r == false) {
                cont = false;
            }
        }
        $(formVal).unmask();
        if (cont) {
            document.getElementById('disorderValuesButtonPressed').value = 'SaveMemberSuspensionButton';
            submitFormWithAjaxPost(formVal, 'MemberCover', btn, main_div, target_div);
        } else {
            btn.disabled = false;
        }
    });
}


function MemGroupConcession(btn, main_div, target_div) {
    document.getElementById('disorderValuesButtonPressed').value = 'CheckGroupConcession';
    formVal = document.getElementById("GroupAndMemberForm");
    var params = encodeFormForSubmit(formVal);
    $(formVal).mask("Checking Member Link To Group Concession");
    $.post(formVal.action, params, function(result) {
        var cont = true;
        if ($.trim(result).length != 0) {
            var r = window.confirm(result);
            if (r == false) {
                cont = false;
            }
        }
        $(formVal).unmask();
        if (cont) {
            document.getElementById('disorderValuesButtonPressed').value = 'SaveGroupMemberButton';
            submitFormWithAjaxPost(formVal, 'MemberCover', btn, main_div, target_div);
        } else {
            btn.disabled = false;
        }
    });
}

function MemBrokerConcession(btn, main_div, target_div) {
    document.getElementById('disorderValuesButtonPressed').value = 'CheckBrokerConcession';
    formVal = document.getElementById("BrokerAndMemberForm");
    var params = encodeFormForSubmit(formVal);
    $(formVal).mask("Checking Member Link To Broker Concession");
        $.post(formVal.action, params, function(result) {
        var cont = true;
        if ($.trim(result).length != 0) {
            var r = window.confirm(result);
            if (r == false) {
                cont = false;
            }
        }
            $(formVal).unmask();
                if (cont) {
                    document.getElementById('disorderValuesButtonPressed').value = 'SaveBrokerMemberButton';
                    submitFormWithAjaxPost(formVal, 'MemberCover', btn, main_div, target_div);
                } else {
                    btn.disabled = false;
                }
        });
    }

function linkUnionToMember(btn, main_div, target_div){
    formVal = document.getElementById("memberToUnionLinkForm");
    var params = encodeFormForSubmit(formVal);
    $.post(formVal.action, params, function(result) {
        var cont = true;
        if ($.trim(result).length != 0) {
           
        }

        document.getElementById('unionMembershipLinkButtonPressed').value = 'SaveLinkUnionToMember';
        submitFormWithAjaxPost(formVal, 'MemberCover', btn, main_div, target_div);
    });
}


function memberUpdatebtn(btn, main_div, target_div) {

    //$(form).mask("Processing...");
    formVal = document.getElementById("MemberUnderwritingForm");
    var params = encodeFormForSubmit(formVal);
    $.post(formVal.action, params, function(result) {
        var cont = true;
        if ($.trim(result).length != 0) {
            var r = window.confirm(result);
            if (r == false) {
                cont = false;
            }
        }


        document.getElementById('buttonPressedUnderwriting').value = 'SaveConcession';
        // submitFormWithAjaxPost(this.form, 'MemberUnderwriting');
        submitFormWithAjaxPostAdd(formVal, 'MemberUnderwriting', btn, main_div, target_div);


    });
}

function validateAddBtn(btn, main_div, target_div)
{

    formVal = document.getElementById("MemberUnderwritingForm");
    var params = encodeFormForSubmit(formVal);
    $.post(formVal.action, params, function(result) {
        var cont = true;
        if ($.trim(result).length != 0) {
            var r = window.confirm(result);
            if (r == false) {
                cont = false;
            }
        }


        var valid = true;
        $("#ConcessionStart_error").text("");

        $("#ConcessionEnd_error").text("");
        var selectedType = document.getElementById("ConcesType");
        var selectedText = selectedType.options[selectedType.selectedIndex].text;

        if (selectedText == 'No') {


            if (document.getElementById("ConcessionStart").value !== "")
            {


                valid = validateDate(document.getElementById("ConcessionStart").value);






                if (valid) {
                    if (document.getElementById("ConcessionStart").value == "") {



                        valid = false;
                    }
                } else {

                    $("#ConcessionStart_error").text("Incorrect Format");
                    valid = false;
                }

            }
            if (document.getElementById("ConcessionEnd").value !== "")
            {


                valid = validateDate(document.getElementById("ConcessionEnd").value);


                if (valid) {
                    if (document.getElementById("ConcessionEnd").value == "") {



                        valid = false;
                    }
                } else {

                    $("#ConcessionEnd_error").text("Incorrect Format");
                    valid = false;
                }

            }

        }
        else {

            if (document.getElementById("ConcessionStart").value !== "")
            {


                valid = validateDate(document.getElementById("ConcessionStart").value);




            }

            if (valid) {
                if (document.getElementById("ConcessionStart").value == "") {
                    $("#ConcessionStart_error").text("Please enter the Start Date.");


                    valid = false;
                }
            } else {

                $("#ConcessionStart_error").text("Incorrect Format");
                valid = false;
            }


            if (document.getElementById("ConcessionEnd").value !== "")
            {


                valid = validateDate(document.getElementById("ConcessionEnd").value);


                if (valid) {
                    if (document.getElementById("ConcessionEnd").value == "") {



                        valid = false;
                    }
                } else {

                    $("#ConcessionEnd_error").text("Incorrect Format");
                    valid = false;
                }

            }





        }


        var start = document.getElementById("ConcessionStart").value;
        var end = document.getElementById("ConcessionEnd").value;
        var year1 = start.substr(0, 4);
        var month1 = start.substr(5, 2);
        var day1 = start.substr(8, 2);
        month1 = parseInt(month1);
        if (month1 < 10) {
            month1 = "0" + month1;
        }

        day1 = parseInt(day1);
        if (day1 < 10) {
            day1 = "0" + day1;
        }

        var year2 = end.substr(0, 4);
        var month2 = end.substr(5, 2);
        var day2 = end.substr(8, 2);

        month2 = parseInt(month2);
        if (month2 < 10) {
            month2 = "0" + month2;
        }

        day2 = parseInt(day2);
        if (day2 < 10) {
            day2 = "0" + day2;
        }
        var startDate = new Date(year1, month1 - 1, day1);
        var endDate = new Date(year2, month2 - 1, day2);

        if (endDate <= startDate)
        {
            //alert("End date greater than Start date");
            $("#ConcessionEnd_error").text("End Date Must Be Greater Than Start Date");
            valid = false;
            //event.preventDefault();
            //document.IdentificationForm.itedate.focus();

        }



        if (valid) {

            document.getElementById('buttonPressedUnderwriting').value = 'SaveConcession';
            submitFormWithAjaxPostAdd(formVal, 'MemberUnderwriting', btn, main_div, target_div);
            alert("Memeber Concession Flag Added");

        }



    });
}



function validateDate(dateString) {

    //  check for valid numeric strings
    var strValidChars = "0123456789/";
    var strChar;
    var strChar2;
    var blnResult = true;

    if (dateString.length < 10 || dateString.length > 10)
        blnResult = false;

    //  test strString consists of valid characters listed above
    for (i = 0; i < dateString.length; i++)
    {
        strChar = dateString.charAt(i);
        if (strValidChars.indexOf(strChar) == -1)
        {
            blnResult = false;
        }
    }

    // test if slash is at correct position (yyyy/mm/dd)
    strChar = dateString.charAt(4);
    strChar2 = dateString.charAt(7);
    if (strChar != '/' || strChar2 != '/')
        blnResult = false;

    return blnResult;
}


function submitWithAction(action) {
    document.getElementById('opperation').value = action;
    document.forms[0].submit();
}

function validateUpdateBtn(btn, main_div, target_div)
{

    formVal = document.getElementById("MemberUnderwritingForm");
    var params = encodeFormForSubmit(formVal);
    $.post(formVal.action, params, function(result) {
        var cont = true;
        if ($.trim(result).length != 0) {
            var r = window.confirm(result);
            if (r == false) {
                cont = false;
            }
        }



        var valid = true;
        $("#ConcessionStart_error").text("");

        $("#ConcessionEnd_error").text("");
        var selectedType = document.getElementById("ConcesType");
        var selectedText = selectedType.options[selectedType.selectedIndex].text;

        if (selectedText == 'No') {
            if (document.getElementById("ConcessionStart").value !== "")
            {


                valid = validateDate(document.getElementById("ConcessionStart").value);






                if (valid) {
                    if (document.getElementById("ConcessionStart").value == "") {



                        valid = false;
                    }
                } else {

                    $("#ConcessionStart_error").text("Incorrect Format");
                    valid = false;
                }

            }
            if (document.getElementById("ConcessionEnd").value !== "")
            {


                valid = validateDate(document.getElementById("ConcessionEnd").value);


                if (valid) {
                    if (document.getElementById("ConcessionEnd").value == "") {



                        valid = false;
                    }
                } else {

                    $("#ConcessionEnd_error").text("Incorrect Format");
                    valid = false;
                }

            }
        }
        else {
            if (document.getElementById("ConcessionStart").value !== "")
            {


                valid = validateDate(document.getElementById("ConcessionStart").value);




            }

            if (valid) {
                if (document.getElementById("ConcessionStart").value == "") {
                    $("#ConcessionStart_error").text("Please enter the Start Date.");


                    valid = false;
                }
            } else {

                $("#ConcessionStart_error").text("Incorrect Format");
                valid = false;
            }


            if (document.getElementById("ConcessionEnd").value !== "")
            {


                valid = validateDate(document.getElementById("ConcessionEnd").value);


                if (valid) {
                    if (document.getElementById("ConcessionEnd").value == "") {



                        valid = false;
                    }
                } else {

                    $("#ConcessionEnd_error").text("Incorrect Format");
                    valid = false;
                }

            }
        }
        var start = document.getElementById("ConcessionStart").value;
        var end = document.getElementById("ConcessionEnd").value;
        var year1 = start.substr(0, 4);
        var month1 = start.substr(5, 2);
        var day1 = start.substr(8, 2);
        month1 = parseInt(month1);
        if (month1 < 10) {
            month1 = "0" + month1;
        }

        day1 = parseInt(day1);
        if (day1 < 10) {
            day1 = "0" + day1;
        }

        var year2 = end.substr(0, 4);
        var month2 = end.substr(5, 2);
        var day2 = end.substr(8, 2);

        month2 = parseInt(month2);
        if (month2 < 10) {
            month2 = "0" + month2;
        }

        day2 = parseInt(day2);
        if (day2 < 10) {
            day2 = "0" + day2;
        }
        var startDate = new Date(year1, month1 - 1, day1);
        var endDate = new Date(year2, month2 - 1, day2);

        if (endDate <= startDate)
        {

            $("#ConcessionEnd_error").text("End Date Must Be Greater Than Start Date");
            valid = false;

        }


        if (valid) {

            document.getElementById('buttonPressedUnderwriting').value = 'SaveConcession';
            submitFormWithAjaxPostAdd(formVal, 'MemberUnderwriting', btn, main_div, target_div);
            alert("Memeber Concession Flag Updated");
        }

    });
}


function memberAddBtn(btn, main_div, target_div) {
    formVal = document.getElementById("MemberUnderwritingForm");
    var params = encodeFormForSubmit(formVal);

    $.post(formVal.action, params, function (result) {
        var cont = true;
        if ($.trim(result).length != 0) {
            var r = window.confirm(result);
            if (r == false) {
                cont = false;
            }
        }
        document.getElementById('buttonPressedUnderwriting').value = 'SaveConcession';
        submitFormWithAjaxPostAdd(formVal, 'MemberUnderwriting', btn, main_div, target_div);
    });
}

function toggleMemberStatus() {
    if (document.getElementById('MemberApp_optionId').value !== "" && document.getElementById('MemberApp_lastName').value !== "") {
        document.getElementById('memAppStatus').removeAttribute("enable");
    }
}

       
function exportRun(btn) {
    console.log("SSSSS");
    var entityId = $('#memberCoverEntityId').val();
//    alert("This Is" + entityId);
    var url = "/ManagedCare/AgileController?opperation=LoadBrokerCoverDetailsCommand&entityId="+entityId;
    $.get(url, function (response) {
        //var result = resultTxt.trim();
        
        var a = window.document.createElement('a');

        a.href = window.URL.createObjectURL(new Blob([response], {type: 'text/csv'}));
        a.download = "ExportBrokerCoverDetails.csv";

        // Append anchor to body.
        document.body.appendChild(a);
        a.click();

        // Remove anchor from body
        document.body.removeChild(a);

    });
}

function fetchPracticeNotes() {
    var url = "/ManagedCare/AgileController?opperation=PracticeManagementNotesCommand&command=FetchPracticeNotes";
    $.get(url, function (data) {
        updateFormFromData(data, "PracticeNotes_Summary");
    });
}

function viewPracticeNote(noteId) {
    var url = "/ManagedCare/AgileController?opperation=PracticeManagementNotesCommand&command=ViewNote&noteId=" + noteId;
    $.get(url, function (response) {
//            $('#Practice_Notes_View_Div').empty().html(response);
//            $('#Practice_Notes_View_Div').show();
//            $('#Practice_notes_details').hide();
//            $('#Practice_Notes_Add_Div').hide();
        $('#noteDetails').text(response);
        swapDivVisbible('Practice_notes_details', 'Practice_Notes_View_Div');
    });
}

function returnToViewNotes() {
    swapDivVisbible('Practice_Notes_Add_Div', 'Practice_notes_details');
    fetchPracticeNotes();
}

function savePracticeNote(form, targetId, btn) {
 if (btn != undefined) {
        btn.disabled = true;
    }
    if (btn != undefined) {
        btn.disabled = true;
    }
    var params = encodeFormForSubmit(form);
    //alert("params = " + params);
    var noteDetails = CKEDITOR.instances.notes.getData();
    //var noteDetails = CKEDITOR.instances.notes.editable().getText();
    CKEDITOR.instances.notes.setData('');
    noteDetails = getPlainText(noteDetails);
    params = params += "&noteDetails=" + noteDetails;
    var formId = $(form).attr('id');
    if(noteDetails === ""){
        alert("please enter a note");
        if (btn != undefined) {
            btn.disabled = false;
        }
        return;
    }
    //$(form).mask("Processing...");
    $.post(form.action, params, function(result) {
        displayNeoSuccessNotification('Practice Note Saved');
        $('#notes').val('');
        //alert("result nappi = " + result + "\ntargetId = " + targetId);
        updateFormFromData(result, targetId);

        var url = "/ManagedCare/AgileController?opperation=PracticeManagementNotesCommand&command=FetchPracticeNotes";
        $.post(url, function (data) {
            updateFormFromData(data, 'PracticeNotes_Summary');
        });

        if (btn != undefined) {
            btn.disabled = false;
        }
        if ($('#' + formId).unmask != null)
            $(form).unmask();
    });
}
function addPracticeNote(){
    //alert("addClaimNote Called");
    var url = "/ManagedCare/AgileController?opperation=PracticeManagementNotesCommand&command=AddNote";
    $.get(url, function(response){
            $('#Practice_Notes_Add_Div').empty().html(response);
            $('#Practice_Notes_Add_Div').show();
            $('#Practice_notes_details').hide();
    });
    
    var url = "/ManagedCare/AgileController?opperation=PracticeManagementNotesCommand&command=FetchPracticeNotes";
    $.get(url,function(data){
        updateFormFromData(data,"PracticeNotes_Summary");
    });
 
}

function exportExcel(from){
    var url = "/ManagedCare/AgileController?opperation=EmployerExportCommand";
    var name = "download.csv";
    var entityId = $('#employerEntityId').val();
    var groupId = $('#entityId').val();
    console.log(groupId);

    
    if(from === "Age"){
        url = "/ManagedCare/AgileController?opperation=EmployerExportCommand&command=ExportAge&entityId=" + entityId;
        name = "ageAnalysis.csv";
    } else if(from === "Member"){
        url = "/ManagedCare/AgileController?opperation=EmployerExportCommand&command=ExportMemeber&entityId=" + entityId;
        name = "MembersInGroup.csv";
    }else if(from === "Employer"){
         console.log("EmployerContributionAllocation ");
        url = "/ManagedCare/AgileController?opperation=EmployerExportCommand&command=ExportGroupContributionAllocation&entityId=" + groupId;
        name = "EmployerContributionAllocation.csv";
    }
    $.get(url, function (response) {
        //var result = resultTxt.trim();
        
        var a = window.document.createElement('a');

        a.href = window.URL.createObjectURL(new Blob([response], {type: 'text/csv'}));
        a.download = name;

        // Append anchor to body.
        document.body.appendChild(a);
        a.click();

        // Remove anchor from body
        document.body.removeChild(a);

    });
}

function submitWithActionAndDocType(thisForm, action, docType){
    thisForm.opperation.value = action;
    thisForm.docuType.value = docType;
    $('#docuType').val(docType);
    thisForm.submit();
}

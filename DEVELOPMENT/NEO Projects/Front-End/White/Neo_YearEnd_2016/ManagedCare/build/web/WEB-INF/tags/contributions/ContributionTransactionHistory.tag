<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@attribute name="items" required="true" type="java.util.Collection" %>
<%@attribute name="showHeader" required="false" type="java.lang.Boolean" %>
<div>
    <c:if test="${showHeader != false}">
        <label class="subheader">Contribution Transaction History</label>
    </c:if>
    <c:choose>
        <c:when test="${empty items}">
            <span><label>No Contribution details available</label></span>
        </c:when>
        <c:otherwise>
            <c:set var="showMSA" value="false"></c:set>
            <c:set var="showLJP" value="false"></c:set>
            <c:set var="showMember" value="false"></c:set>
            <c:set var="showEmployer" value="false"></c:set>
            <c:set var="showOption" value="false"/>
            <c:set var="showAD" value="false"/>
            <c:set var="showCD" value="false"/>
            <c:set var="currentOption" value=""/>
            <c:forEach var="contribAmts" items="${items}">
                <c:if test="${empty currentOption}"><c:set var="currentOption" value="${contribAmts.optionName}"/></c:if>
                <c:set var="showMSA" value="${showMSA || (contribAmts.msaAmount != null && contribAmts.msaAmount != 0)}"></c:set>
                <c:set var="showLJP" value="${showLJP || (contribAmts.ljpAmount != null && contribAmts.ljpAmount != 0)}"></c:set>
                <c:set var="showMember" value="${showMember || (contribAmts.memberAmount != null && contribAmts.memberAmount != 0)}"></c:set>
                <c:set var="showEmployer" value="${showEmployer || (contribAmts.employerAmount != null && contribAmts.employerAmount != 0)}"></c:set>
                <c:set var="showOption" value="${showOption || (contribAmts.optionName != currentOption)}"></c:set>
                <c:set var="showAD" value="${showAD || (contribAmts.adultDependants != null && contribAmts.adultDependants > 0)}"></c:set>
                <c:set var="showCD" value="${showCD || (contribAmts.childDependants != null && contribAmts.childDependants > 0)}"></c:set>
                
            </c:forEach>
            <table class="list" style="border-collapse:collapse; border-width:1px;" width="100%">
                <thead>
                    <tr>
                        <th colspan="2" rowspan="2" style="border-right-color: #FFFFFF; ">User</th>
                        <th style="border-right-color: #FFFFFF; ">Trans</th>
                        <th style="border-right-color: #FFFFFF; ">Action</th>
                        <th rowspan="2" style="border-right-color: #FFFFFF; ">Type</th>
                        <th rowspan="2" style="border-right-color: #FFFFFF; ">Description</th>
                        ${showOption ? "<th rowspan='2' style='border-right-color: #FFFFFF;'>Option</th>": ""}
                        <c:if test="${showAD}"><th rowspan="2" style="border-right-color: #FFFFFF; ">AD</th></c:if>
                        <c:if test="${showCD}"><th rowspan="2" style='border-right-color: #FFFFFF;'>CD</th></c:if>
                        ${showMSA || showLJP ? "<th style='border-right-color: #FFFFFF;'>Risk</th>": ""}
                        ${showMSA ? "<th style='border-right-color: #FFFFFF;'>MSA</th>": ""}
                        ${showLJP ? "<th style='border-right-color: #FFFFFF;'>LJP</th>": ""}
                        ${showMember ? "<th colspan='2' style='border-right-color: #FFFFFF; border-bottom-color:#FFFFFF;'>Member</th>": ""}
                        ${showEmployer ? "<th colspan='2' style='border-right-color: #FFFFFF; border-bottom-color:#FFFFFF;'>Group</th>": ""}
                    </tr>
                    <tr>
                        <th style='border-right-color: #FFFFFF;'>Date</th>
                        <th style='border-right-color: #FFFFFF;'>Date</th>
                        ${showMSA || showLJP ? "<th style='border-right-color: #FFFFFF;'>Amount</th>": ""}
                        ${showMSA ? "<th style='border-right-color: #FFFFFF;'>Amount</th>": ""}
                        ${showLJP ? "<th style='border-right-color: #FFFFFF;'>Amount</th>": ""}
                        ${showMember ? "<th style='border-right-color: #FFFFFF;'>Amount</th>": ""}
                        ${showMember ? "<th style='border-right-color: #FFFFFF;'>Bal</th>": ""}
                        ${showEmployer ? "<th style='border-right-color: #FFFFFF;'>Amount</th>": ""}
                        ${showEmployer ? "<th style='border-right-color: #FFFFFF;'>Bal</th>": ""}
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="${8 + (showMSA || showLJP ? 1 : 0)  + (showMSA ? 1 : 0)  + (showLJP ? 1 : 0) + (showAD ? 1 : 0) + (showCD ? 1 : 0) + (showOption ? 1 : 0)  + (showMember ? 2 : 0) + (showEmployer ? 2 : 0)} ">&nbsp</td>
                    </tr>
                </tfoot>
                <tbody>
                    <c:set var="dateTest" value=""></c:set>
                    <c:forEach var="entry" items="${Contributions}">
                        <c:set var="contribDate" value="${agiletags:formatXMLGregorianDate(entry.contributionDate)}"></c:set>
                        <c:if test="${dateTest != contribDate}">
                        </tbody>
                        <c:set var="subsAmt" value="0"></c:set>
                        <c:set var="payAmt" value="0"></c:set>
                        <c:set var="jnlAmt" value="0"></c:set>
                        <c:forEach var="contribAmts" items="${items}">
                            <c:if test="${contribAmts.contributionDate == entry.contributionDate}">
                                <c:if test="${contribAmts.transactionTypeId < 4}">
                                    <c:set var="subsAmt" value="${subsAmt + contribAmts.amount}"></c:set>
                                </c:if>
                                <c:if test="${contribAmts.transactionTypeId == 4 || contribAmts.transactionTypeId == 5 }">
                                    <c:set var="payAmt" value="${payAmt + contribAmts.amount}"></c:set>
                                </c:if>
                                <c:if test="${contribAmts.transactionTypeId > 5}">
                                    <c:set var="jnlAmt" value="${jnlAmt + contribAmts.amount}"></c:set>
                                </c:if>
                            </c:if> 
                        </c:forEach>
                        <tbody>
                            <tr style="background-color:#f0f8ff;">
                                <td><input type="Button" value="+" onclick="toggle_table_group(this, 'contrib_${contribDate}');"></td>
                                <td><label><b>${contribDate}</b></label></td>
                                <td colspan="${6 + (showMSA || showLJP ? 1 : 0)  + (showMSA ? 1 : 0)  + (showLJP ? 1 : 0) + (showAD ? 1 : 0) + (showCD ? 1 : 0) + (showOption ? 1 : 0)  + (showMember ? 2 : 0) + (showEmployer ? 2 : 0)} ">
                                    <label><b>Subs:</b>&nbsp;&nbsp;  ${agiletags:formatBigDecimal(subsAmt)}</label>
                                    <label><b>Payments:</b>&nbsp;&nbsp;  ${agiletags:formatBigDecimal(payAmt)}</label>
                                    <c:if test="${jnlAmt != 0}">
                                        <label><b>Journals:</b>&nbsp;&nbsp;  ${agiletags:formatBigDecimal(jnlAmt)}</label>
                                    </c:if>
                                    <label><b>Balance:</b>&nbsp;&nbsp;  ${agiletags:formatBigDecimal(subsAmt + payAmt + jnlAmt)}</label>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="contrib_${contribDate}"  style="display:none;">
                        </c:if>

                        <tr>
                            <td colspan="2"><label>${entry.userName}</label></td>
                            <td><label>${agiletags:formatXMLGregorianDate(entry.transactionDate)}</label></td>
                            <td><label>${agiletags:formatXMLGregorianDate(entry.actionDate)}</label></td>
                            <td><label>${entry.transactionTypeDesription}</label></td>
                            <td><label>${entry.description}</label></td>
                            <c:if test="${showOption}"><td><label>${entry.optionName}</label></td></c:if>
                            <c:if test="${showAD}"><td><label>${entry.transactionTypeId < 4 ? entry.adultDependants : ""}</label></td></c:if>
                            <c:if test="${showCD}"><td><label>${entry.transactionTypeId < 4 ? entry.childDependants : ""}</label></td></c:if>
                            <c:if test="${showMSA || showLJP}">
                                <td align="right"><label>${entry.riskAmount == null ? "" : agiletags:formatBigDecimal(entry.riskAmount)}</label></td>
                            </c:if>
                            <c:if test="${showMSA}">
                                <td align="right"><label>${entry.msaAmount == null ? "" : agiletags:formatBigDecimal(entry.msaAmount)}</label></td>
                            </c:if>
                            <c:if test="${showLJP}">
                                <td align="right"><label>${entry.ljpAmount == null ? "" : agiletags:formatBigDecimal(entry.ljpAmount)}</label></td>
                            </c:if>
                            <c:if test="${showMember}">
                                <td align="right"><label>${entry.memberAmount == null ? "" : agiletags:formatBigDecimal(entry.memberAmount)}</label></td>
                                <td align="right"><label>${agiletags:formatBigDecimal(entry.memberRunningTotal)}</label></td>
                            </c:if>
                            <c:if test="${showEmployer}">
                                <td align="right"><label>${entry.employerAmount == null ? "" : agiletags:formatBigDecimal(entry.employerAmount)}</label></td>
                                <td align="right"><label>${agiletags:formatBigDecimal(entry.employerRunningTotal)}</label></td>
                            </c:if>
                        </tr>
                        <c:set var="dateTest" value="${contribDate}"></c:set>
                    </c:forEach>
                </tbody>

            </table>
        </c:otherwise>
    </c:choose>
</div>
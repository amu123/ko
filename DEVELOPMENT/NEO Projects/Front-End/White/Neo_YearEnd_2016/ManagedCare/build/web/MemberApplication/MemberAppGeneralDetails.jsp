
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

  <agiletags:ControllerForm name="MemberAppGeneralDetailForm">
    <input type="hidden" name="opperation" id="opperation" value="MemberApplicationGeneralQuestionsCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberApplicationGeneralDetails" />
    <input type="hidden" name="memberAppNumber" id="memberAppGeneralNumber" value="${memberAppNumber}" />
    <input type="hidden" name="memberAppGenId" id="memberAppGeneralQId" value="${memberAppGenId}" />
    <input type="hidden" name="target_div" value="${target_div}" />
    <input type="hidden" name="main_div" value="${main_div}" />
    <input type="hidden" name="buttonPressed" id="MemberAppGeneralQuestionsDetails_button_pressed" value="" />

    <label class="header">Add General Question Detail</label>
    <hr>
    <br>
     <table>
                <tr><agiletags:LabelNeoMapListDropDownErrorReq displayName="Question"  elementName="General_detail_question" mapList="GeneralQuestions" mandatory="yes" errorValueFromSession="no" firstIndexSelected="yes" />        </tr>
                <tr><agiletags:LabelNeoMapListDropDownErrorReq displayName="Dependant"  elementName="General_detail_dependent" mapList="DependentList" mandatory="yes" errorValueFromSession="no" firstIndexSelected="yes" />        </tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="Details" elementName="General_detail_details" valueFromRequest="General_detail_details" mandatory="yes"/>        </tr>
      </table>
      
    <hr>
    <table id="MemberAppGenQDetailsSaveTable">
        <tr>
           <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
           <td><input type="button" value="Save" onclick="document.getElementById('MemberAppGeneralQuestionsDetails_button_pressed').value='SaveButton';submitFormWithAjaxPost(this.form, 'GeneralQuestions', null,'${main_div}', '${target_div}');"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>

<%-- 
    Document   : LinkMemberToUnion
    Created on : Aug 11, 2016, 11:35:25 AM
    Author     : shimanem
--%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<agiletags:ControllerForm name="memberToUnionLinkForm">
    <input type="hidden" name="opperation" id="opperation" value="LinkMembershipToUnionCommand"/>
    <input type="hidden" name="buttonPressed" id="unionMembershipLinkButtonPressed" value="" />
    <input type="hidden" name="onScreen" id="onScreen" value="" />
    <input type="hidden" name="target_div" value="${target_div}" />
    <input type="hidden" name="main_div" value="${main_div}" />
    <input type="hidden" name="memberEntityId" id="memberCoverDetailsEntityId" value="${sessionScope.memberCoverEntityId}">
    <input type="hidden" name="memberCoverDepType" id="memberCoverDepType" value="${sessionScope.memberCoverDepType}">
    <input type="hidden" name="memberCoverNumber" id="memberCoverNumber" value="${sessionScope.memberCoverNumber}">
    <br/>
    <br/>
    <label class="subheader">Link Member to Union</label>
    <hr>
    <table>
        <tr><agiletags:LabelTextSearchWithNoText  displayName="Union Name" elementName="employerGroup_unionName" mandatory="yes" onClick="document.getElementById('unionMembershipLinkButtonPressed').value = 'advancedUnionSearch'; getPageContents(document.forms['memberToUnionLinkForm'],null,'overlay','overlay2');"/></tr>
        <tr><agiletags:LabelTextBoxError readonly="yes"  mandatory="yes" displayName="Union ID" elementName="employerGroup_unionID" valueFromSession="yes"/></tr>
        <tr><agiletags:LabelTextBoxError readonly="yes" mandatory="yes" displayName="Union Number" elementName="employerGroup_unionNumber" valueFromSession="yes"/></tr>
        <tr><agiletags:LabelTextBoxErrorReq mandatory="yes" displayName="Representative Name" elementName="member_repName" valueFromSession="yes"/></tr>
        <tr><agiletags:LabelTextBoxErrorReq mandatory="yes" displayName="Representative Surname" elementName="member_repSurname" valueFromSession="yes"/></tr>
        <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Region"  elementName="member_unionRegion" lookupId="251" mandatory="yes" javaScript="" errorValueFromSession="yes" /></tr> 
        <tr><agiletags:LabelTextBoxDate displayname="Inception Date" elementName="member_unionInceptionDate" mandatory="yes" valueFromSession="yes"/></tr>
        <tr>
            <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}', '${main_div}');"></td>
            <td><input type="button" value="Save" onclick="linkUnionToMember(this, '${main_div}', '${target_div}');"> </td>
        </tr>
    </table>
<!--    <div style ="display: none"  id="EmployerUnionResults"></div>-->
</agiletags:ControllerForm>
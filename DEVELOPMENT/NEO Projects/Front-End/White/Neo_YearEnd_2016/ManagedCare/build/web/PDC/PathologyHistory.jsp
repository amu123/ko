<%-- 
    Document   : PathologyHistory
    Created on : 2010/04/16, 03:59:46
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Policy Holder - Pathology</label>
                    <br/><br/>
                    <table>
                        <agiletags:ControllerForm name="pathologyHistory">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                <agiletags:PathologyHistoryTable/>
                            </table>
                            <br/>
                            <table>
                                <tr><agiletags:ButtonOpperation align="left" displayname="Close" commandName="ViewPolicyHolderDetailsCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('ViewPolicyHolderDetailsCommand')\";"/></tr>
                            </table>
                        </agiletags:ControllerForm>
                    </table>
                    <br/>
                </td></tr></table>
    </body>
</html>

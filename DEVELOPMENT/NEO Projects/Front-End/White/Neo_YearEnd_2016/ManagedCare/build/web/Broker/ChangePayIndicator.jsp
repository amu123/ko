<%-- 
    Document   : ChangePayIndicator
    Created on : 29 Apr 2015, 11:38:57 AM
    Author     : chrisdp
--%>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
<link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
<script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>

<html>
    <head>
        <script>
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            
            function validateDate(dateString) {
                
                //  check for valid numeric strings
                var strValidChars = "0123456789/";
                var strChar;
                var strChar2;
                var blnResult = true;

                if (dateString.length < 10 || dateString.length > 10) blnResult = false;

                //  test strString consists of valid characters listed above
                for (i = 0; i < dateString.length; i++)
                {
                    strChar = dateString.charAt(i);
                    if (strValidChars.indexOf(strChar) === -1)
                    {
                        blnResult = false;
                    }
                }

                // test if slash is at correct position (yyyy/mm/dd)
                strChar = dateString.charAt(4);
                strChar2 = dateString.charAt(7);
                if (strChar !== '/' || strChar2 !== '/') blnResult = false;

                return blnResult;
            }
            
            function submitSave() {
                var currentValue = '${sessionScope.currentCommissionInd}';
                var startDate = document.getElementById("BrokerFirm_StartDate").value;
                document.getElementById("commInd").value = document.getElementById("FirmApp_CommisionInd").value;            
                var newValue = document.getElementById("commInd").value;      
                var validDate = validateDate(startDate);

                if((currentValue === newValue || newValue === 99) || (startDate === null || startDate === "")){
                    alert("Invalid input entered.");                 
                } else {    
                    if(validDate){
                        submitWithAction('SavePayIndicatorCommand');
                    } else {
                       document.getElementById("BrokerFirm_StartDate_error").value = "Invalid date.";
                       alert("Invalid input entered.");
                    }
                }
            }

        </script>
    </head>
    <body>
        <agiletags:ControllerForm name="BrokerFirmCommissionIndicatorForm">
            <input type="hidden" name="opperation" id="opperation" value="" />
            <input type="hidden" name="onScreen" id="onScreen" value="" />
            <input type="hidden" name="brokerFirmEntityId" id="brokerFirmEntityId" value="${requestScope.brokerFirmEntityId}" />
            <input type="hidden" name="commInd" id="commInd" value="${requestScope.commissionIndicator}" />

            <h1 class="subheader">Payment Indicator</h1>
            <HR WIDTH="80%" align="left">
            <table>
                <tr>
                    <td><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Commission" elementName="FirmApp_CommisionInd" lookupId="67" mandatory="yes" javaScript="" /></td>
                </tr>    
                <tr>
                    <td><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="BrokerFirm_StartDate"  valueFromSession="no" mandatory="yes" javascript="" /></td>
                </tr>     
            </table>

            <table id="SaveTable">
                <tr>
                    <td><input type="button" value="Cancel" onclick="submitWithAction('CancelPayIndicatorCommand');"></td>
                    <td><input type="button" value="Save" id="save" onclick="submitSave();"></td>         
                </tr>
            </table>

        </agiletags:ControllerForm>

    </body>
</html>


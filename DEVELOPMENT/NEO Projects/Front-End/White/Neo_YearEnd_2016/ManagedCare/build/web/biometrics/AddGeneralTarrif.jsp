<%--
    Document   : AddGeneralTarrif
    Created on : 2010/05/14, 12:03:39
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Add General Tarrif</label>
                    <table>
                        <agiletags:ControllerForm name="AddGeneralTarrif">
                            <input type="hidden" name="opperation" id="opperation" value=""/>

                            <tr><agiletags:LabelTextSearchText displayName="Tarrif Code" elementName="tarrifCode" valueFromSession="yes" searchFunction="" onScreen=""/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Tarrif Description" elementName="tarrifDescription" valueFromSession="yes" enabled="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Test Result" elementName="testResult" valueFromSession="yes"/></tr>

                            <tr>
                                <agiletags:ButtonOpperation commandName="SaveCardiacBiometricsCommand" align="left" displayname="Save" span="1" type="button"  javaScript="onClick=\"submitWithAction('SaveCallTrackDetailsCommand')\";"/>
                                <agiletags:ButtonOpperation commandName="ResetCardiacBiometricsCommand" align="left" displayname="Add" span="1" type="button"  javaScript="onClick=\"submitWithAction('ResetCardiacBiometricsCommand')\";"/>
                                <agiletags:ButtonOpperation commandName="ResetTarrifComamnd" align="left" displayname="Delete" span="1" type="button"  javaScript="onClick=\"submitWithAction('ResetTarrifComamnd')\";"/>
                            </tr>


                        </agiletags:ControllerForm>
                    </table>


                    <!-- content ends here -->
        </td></tr></table>
    </body>
</html>


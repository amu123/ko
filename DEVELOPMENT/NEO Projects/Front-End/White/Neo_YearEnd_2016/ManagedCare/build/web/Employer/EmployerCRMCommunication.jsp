<%-- 
    Document   : EmployerCRMCommunication
    Created on : Aug 27, 2016, 2:30:07 AM
    Author     : janf
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
    <br>
    <c:if test="${applicationScope.Client == 'Sechaba'}">
        <nt:NeoPanel title="CRM Communication" command="WorkflowCommand" action="memberCRMByEntity" actionId="${entityId}" reload="true" />
    </c:if>

  
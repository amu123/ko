<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <label class="header">Broker Search</label>
     <agiletags:ControllerForm name="MemberBrokerSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="MemberBrokerSearchCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="DisorderSearch" />
        <input type="hidden" name="target_div" value="${target_div}" />
        <input type="hidden" name="main_div" value="${main_div}" />
        <input type="hidden" name="diagCode" id="icd10DiagCode" value="" />
        <input type="hidden" name="diagName" id="icd10DiagName" value="" />
        <input type="hidden" name="diagCodeId" id="icd10DiagCodeId" value="${diagCodeId}" />
        <input type="hidden" name="diagNameId" id="icd10DiagNameId" value="${diagNameId}" />
        <input type="hidden" name="brokerFirm" id="brokerFirm" value="${brokerFirm}" />
        <input type="hidden" name="tel" id="tel" value="${tel}" />
        <input type="hidden" name="fax" id="fax" value="${fax}" />
        <input type="hidden" name="email" id="email" value="${email}" />
        <input type="hidden" name="VIPBroker" id="VIPBroker" value="${VIPBroker}" />
        <table>
            <tr><agiletags:LabelTextBoxError displayName="Broker Code" elementName="brokerCode"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Broker Name" elementName="brokerName"/></tr>
            <tr>
                <td><input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
                <td align="right"><input type="button" value="Search" onclick="submitFormWithAjaxPost(this.form, 'BrokerSearchResults');"></td>
            </tr>
        </table>
      
      <div style ="display: none"  id="BrokerSearchResults"></div>
      </agiletags:ControllerForm>
<%-- 
    Document   : Header
    Created on : 2009/09/29, 11:21:49
    Author     : gerritj
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="resources/fisheye/fisheye-menu.css" />
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script src="resources/fisheye/fisheye.js" type='text/javascript'></script>
        <script type="text/javascript">
            function doMenuClick(p) {
                alert(p);
                parent.leftMenu.document.forms[0].application.value = p;
                parent.leftMenu.document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <table width="100%" cellpadding="0" cellspacing="0"><tr>
                <td width="100%">
                    <table width="100%">
                        <tr height="90">
                            <td>
                                <c:if test="${applicationScope.Client == 'Sechaba'}">
                                    <img src="resources/Sechaba logo.png"  alt="Agility_Logo" style="margin-bottom:4px;"/>
                                </c:if>
                                <c:if test="${applicationScope.Client == 'Agility'}">
                                    <img src="resources/Agility GHS logo.png"  alt="Agility_Logo" style="margin-bottom:4px;"/>
                                </c:if>
                            </td>
                            <td width="100%" style="text-align:center">
                                <table style="width:100%">
                                    <tr><td>
                                            <div style="float:right">
                                                <center>
                                                    <ul id="fisheye_menu">
                                                    </ul>
                                                </center>
                                            </div>
                                        </td></tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <c:if test="${applicationScope.Client == 'Sechaba'}">
                <tr bgcolor="#6cc24a"><!--#a50d12-->
                    <td width="100%" height="8">&nbsp;</td>
                </tr>
            </c:if>
            <c:if test="${applicationScope.Client == 'Agility'}">
                <tr bgcolor="#660000"><!--#a50d12-->
                    <td width="100%" height="8">&nbsp;</td>
                </tr>
            </c:if>
        </table>
        <form action="/ManagedCare/AgileController" name="header">
            <input type="hidden" name="opperation" value="TopMenuCommand" size="30" />
            <input type="hidden" name="application" value="0" size="30" />
        </form>
    </body>
</html>

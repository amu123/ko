<%--
    Document   : CapturePathology
    Created on : 2010/05/13, 01:14:20
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>

        <script language="JavaScript">

            $(document).ready(function() {

                $("#dialog-saved").hide();
                $("#dialog-notsaved").hide();

                displayDialog();
                displayError();
            });
    
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }


            function getICD10ForCode(str, element){
                xhr=GetXmlHttpObject();
                if (xhr==null)
                {
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=GetICD10DetailsByCode&code="+str+"&element="+element;
                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        // alert(result);
                        if(result == "Error"){
                            var error = "#"+element+"_error";
                            $(document).find(error).text("No such diagnosis");
                        }
                    }
                };
                xhr.open("POST",url,true);
                xhr.send(null);
            }

            function getTariffForCode(str, element){
                //alert('Are we here!');
                // var str = document.getElementById('tariffCode').value;
                //alert(str);
                xmlhttp=GetXmlHttpObject();
                //alert(xmlhttp);
                //xmlhttp.overrideMimeType('text/xml');
                if(xmlhttp==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=LoadBiometricsTariffCodeCommand&tariffCode="+str;

                xmlhttp.onreadystatechange=function(){stateChanged(element)};
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function GetDOMParser(xmlStr){
                var xmlDoc;
                if(window.DOMParser){
                    parser=new DOMParser();
                    xmlDoc= parser.parseFromString(xmlStr,"text/xml");
                }else{
                    xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async="false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }

            function getCoverByNumber(str, element){

                xhr = GetXmlHttpObject();//xhr = XMLHttpRequest object
                if(xhr==null){
                    alert("ERROR: Browser Incompatability");
                    return;
                }
                //url command GetCoverDetailsByNumber location - com.agile.command
                var url = "/ManagedCare/AgileController";
                url+="?opperation=GetCoverDetailsByNumber&number="+ str + "&element="+element + "&exactCoverNum=1";
                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        $("#depListValues").empty();//clear EntityCoverDependantDropDown tag with elementName of depListValues
                        $("#depListValues").append("<option value=\"99\"></option>");//add empty option to force selection
                        var xmlDoc = GetDOMParser(xhr.responseText);//loads xml document of servlets response to the page
                        //print value from servlet
                        $(xmlDoc).find("EAuthCoverDetails").each(function (){//for each <EAuthCoverDetails> in the xml document
                            //set product details
                            var prodName = $(this).find("ProductName").text();//textContent of <ProductName> in the <EAuthCoverDetails>  tag
                            var optName = $(this).find("OptionName").text();//textContent of <OptionName> in the <EAuthCoverDetails>  tag

                            //extract dependent info from tag
                            $(this).find("DependantInfo").each(function(){//for each <DependantInfo> in the <EAuthCoverDetails>  tag
                                var depInfo = $(this).text();
                                var nameSurnameType = depInfo.split("|");
                                //Add option to dependant dropdown
                                var optVal = document.createElement('OPTION');
                                optVal.value = nameSurnameType[0];
                                optVal.text = nameSurnameType[1];
                                document.getElementById("depListValues").options.add(optVal);

                            });
                            //DISPLAY MEMBER DETAIL LIST

                            $(document).find("#schemeName").text(prodName);//set product name to LabelTextDisplay tag with the element name of schemeName
                            $(document).find("#schemeOptionName").text(optName);//set option name to LabelTextDisplay tag with the element name of schemeOptionName
                            $(document).find(element+"_error").text($(this).text());
                        });
                    }
                }
                xhr.open("POST",url,true);
                //xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                xhr.send(null);
            }

            function stateChanged(element){
                if (xmlhttp.readyState==4)
                {
                    // alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    if(result == "LoadBiometricsTariffCodeCommand"){
                        var description = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=')+1, xmlhttp.responseText.lastIndexOf('$'));
                        //var testResults = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=')+1, xmlhttp.responseText.lastIndexOf('$'));
                        document.getElementById('tariffDescription').value = description;
                        //document.getElementById('testResults').value = testResults;
                        document.getElementById('searchCalled').value = "";
                        document.getElementById(element + '_error').innerHTML = '';
                        s//ubmitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/biometrics/CapturePathology.jsp");
                    }else if(result == "GetMemberByNumberCommand"){
                                                                        
                        var resultArr = xmlhttp.responseText.split("|");
                        var numberArr = resultArr[1].split("=");
                                                                
                        var number = numberArr[1];
                        document.getElementById(element + '_error').innerHTML = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('searchCalled').value = "";
                        //submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                    }else if(result == "FindProviderByCodeCommand"){
                        document.getElementById(element + '_error').innerHTML = '';
                    }else if(result == "Error"){
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|')+1, xmlhttp.responseText.length);
                        if(forCommand == "FindProviderByCodeCommand"){
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById('provName').value = '';
                            document.getElementById('discipline').value = '';
                            document.getElementById('searchCalled').value = "";
                        } else if(forCommand == "GetMemberByNumberCommand"){
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element + '_text').value = '';
                            document.getElementById('searchCalled').value = "";
                            submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/biometrics/CapturePathology.jsp");
                        } else if(forCommand == "LoadBiometricsTariffCodeCommand") {
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element + '_text').value = '';
                            document.getElementById('searchCalled').value = "";
                        }
                    }
                }
            }

            function CheckSearchCommands(){
                //reload ajax commands for screen population
                var searchVal = document.getElementById('searchCalled').value;
                if (searchVal == 'memberNumber'){
                    //alert(document.getElementById('memberNumber_text').value);
                    getCoverByNumber(document.getElementById('memberNumber_text').value, 'memberNumber');

                }else if (searchVal == 'treatingProvider'){
                    //alert(document.getElementById('providerNumber_text').value);
                    validateProvider(document.getElementById('treatingProvider_text').value, 'treatingProvider');

                }else if(searchVal == 'tariffCode') {
                    //alert(document.getElementById('tarrifCode_text').value);
                    getTariffForCode(document.getElementById('tariffCode_text').value, 'tariffCode');
                }
                document.getElementById('searchCalled').value = '';
            }

            function validateMember(str, element) {
                xmlhttp=GetXmlHttpObject();
                if(xmlhttp==null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=MemberDetailsCoverSearchCommand&memberNumber="+str+ "&exactCoverNum=1";
                xmlhttp.onreadystatechange=function(){stateChanged(element)};
                xmlhttp.open("POST",url,true);
                xmlhttp.send(null);
            }

            function getICDDescription(str, element) {
                xmlhttp=GetXmlHttpObject();
                if(xmlhttp==null) {
                    alert('Your browser does not support XMLHTTP!')
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=LoadICD10BiometricsCommand&icd10="+str;
                xmlhttp.onreadystatechange=function(){stateChanged(element)};
                xmlhttp.open("POST",url,true);
                xmlhttp.send(null);
            }


            function getICD10ForCode(str, element){
                xhr=GetXmlHttpObject();
                if (xhr==null)
                {
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=GetICD10DetailsByCode&code="+str+"&element="+element;
                xhr.onreadystatechange=function(){
                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        // alert(result);
                        if(result == "Error"){
                            var error = "#"+element+"_error";
                            $(document).find(error).text("No such diagnosis");
                        }
                    }
                };
                xhr.open("POST",url,true);
                xhr.send(null);
            }

            function displayDialog() {
            <%  if ((session.getAttribute("updated")) != null && (!session.getAttribute("updated").equals("null"))) {
            %>

                            $("#dialog-saved").dialog({ modal: true, width:550,
                                buttons: {
                                    'Done': function() {
                                        $(this).dialog('close');
                                    }
                                }
                            });
            <%
                            session.setAttribute("updat        ed", null);
                                }%>

                                    }

                                    function displayError() {
            <%  if ((session.getAttribute("failed")) != null && (!session.getAttribute("failed").equals("null"))) {
            %>

                                $("#dialog-notsaved").dialog({ modal: true, width:550,
                                    buttons: {
                                        'Done': function() {
                                            $(this).dialog('close');
                                        }
                                    }
                                });
            <%
                                session.setAttribute("        failed", null);
                                    }%>

                                        }

                                        function validateProvider(str, element){
                                            xmlhttp=GetXmlHttpObject();
                                            if (xmlhttp==null)
                                            {
                                                alert ("Your browser does not support XMLHTTP!");
                                                return;
                                            }
                                            var url="/ManagedCare/AgileController";
                                            url=url+"?opperation=FindProviderByCodeCommand&providerNumber="+str;
                                            xmlhttp.onreadystatechange=function(){stateChanged(element)};
                                            xmlhttp.open("POST",url,true);
                                            xmlhttp.send(null);
                                        }

        </script>
    </head>
    <body onload="CheckSearchCommands()">
        <div id="dialog-saved" title="Biometrics Saved"><p>Pathology has been saved</p></div>
        <div id="dialog-notsaved" title="Pathology Not Saved"><label class="error">Pathology save has failed!</label></div>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Pathology</label>
                    <table>
                        <agiletags:ControllerForm name="CapturePathology">
                            <input type="hidden" name="opperation" id="opperation" value=""/>
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="providerDetails" id="providerDetails" value="email" />

                            <tr><agiletags:LabelTextSearchText displayName="Member Number" elementName="memberNumber" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand"  valueFromRequest="yes" valueFromSession="yes" onScreen="/biometrics/CapturePathology.jsp" mandatory="yes" javaScript="onChange=\"getCoverByNumber(this.value, 'memberNumber');\""/></tr>
                            <tr><agiletags:EntityCoverDependantDropDown displayName="Cover Dependants" elementName="depListValues" javaScript="" mandatory="Yes" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Scheme" elementName="schemeName" valueFromSession="yes" javaScript="" /></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Scheme Option" elementName="schemeOptionName" valueFromSession="yes" javaScript="" /></tr>

                            <tr><agiletags:LabelTextBoxDateTime displayName="Service Date" elementName="serviceDate" valueFromSession="yes" mandatory="yes"/></tr>
                            <tr><agiletags:LabelTextBoxDate displayname="Date Received" elementName="dateReceived" valueFromSession="yes" mandatory="yes"/></tr>
                            <tr><agiletags:LabelTextSearchText displayName="Treating Provider" elementName="providerNumber" valueFromSession="yes" searchFunction="yes" searchOperation="ForwardToSearchProviderCommand" onScreen="/biometrics/CapturePathology.jsp" mandatory="yes" javaScript="onChange=\"validateProvider(this.value, 'providerNumber');\""/></tr>
                            <!--<tr><agiletags:LabelTextBoxError displayName="Source" elementName="source" valueFromSession="yes" readonly="yes" mandatory="yes"/></tr>-->
                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Source" elementName="source" lookupId="104" mandatory="yes" errorValueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextSearchText displayName="Tarrif Code" elementName="tariffCode" searchFunction="yes" searchOperation="ForwardToTarrifSearchCommand"  valueFromRequest="yes" valueFromSession="yes"  onScreen="/biometrics/CapturePathology.jsp" mandatory="yes" javaScript="onChange=\"getTariffForCode(this.value, 'tariffCode');\""/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Tarrif Description" elementName="tariffDescription" valueFromSession="yes" mandatory="yes" readonly="yes" valueFromRequest="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Test Result" elementName="testResults" valueFromSession="yes" mandatory="yes" valueFromRequest="yes"/></tr>
                            <tr><agiletags:LabelTextSearchText displayName="ICD10" elementName="code" searchFunction="yes" searchOperation="ForwardToSearchICDCommand" valueFromSession="yes" onScreen="/biometrics/CapturePathology.jsp" javaScript="onChange=\"getICD10ForCode(this.value, 'icd10');\"" mandatory="yes"/></tr>
                            <tr><agiletags:LabelTextAreaError displayName="Detail" elementName="detail" valueFromSession="yes" mandatory="yes"/></tr>

                            <tr>
                                <agiletags:ButtonOpperation commandName="ResetPathologyCommand" align="left" displayname="Reset" span="1" type="button"  javaScript="onClick=\"submitWithAction('ResetPathologyCommand')\";"/>
                                <agiletags:ButtonOpperation commandName="CapturePathologyCommand" align="left" displayname="Save" span="1" type="button"  javaScript="onClick=\"submitWithAction('CapturePathologyCommand')\";"/>
                            </tr>

                        </agiletags:ControllerForm>
                    </table>
                    <!-- content ends here -->
                </td></tr></table>
    </body>
</html>


<%-- 
    Document   : PDCTabs
    Created on : 2013/10/08, 02:08:52
    Author     : johanl
--%>

<%@tag description="pdc menu item tabs tag" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="tabSelectedIndex"%>

<%-- any content can be specified here e.g.: --%>
<div id="header" class="header_area">
    <ol id="tabs">
        <li><a id="link_PH" onClick="submitWithAction('ViewPolicyHolderDetailsCommand');" href="#PolicyHolder" >Patient Details</a></li>
        <li><a id="link_Auth" onClick="submitWithAction('ViewAuthorizationCommand');" href="#Auths">Authorisations</a></li>
        <li><a id="link_AD" onClick="submitWithAction('ViewAddressCommand');" href="#AddressDetail">Address Details</a></li>
        <li><a id="link_Bio" onClick="submitWithAction('ViewBiometricsCommand');" href="#Biometrics">Biometrics</a></li>
        <li><a id="link_EV" onClick="submitWithAction('ViewEventsCommand');" href="#Events">View History</a></li>
        <li><a id="link_CD" onClick="submitWithAction('ViewClaimsCommand');" href="#Claims">Claims</a></li>
        <li><a id="link_RR" onClick="submitWithAction('ViewRatePatientCommand');" href="#RiskRate">Risk Rate Patient</a></li>
        <li><a id="link_MN" onClick="submitWithAction('ViewNoteDetailsCommand');" href="#PDCNotes">Notes</a></li>
        <li><a id="link_CDL" onClick="submitWithAction('ViewCDLStatusCommand');" href="#PDCCDLStatus">Flag Patient</a></li>
        <li><a id="link_WB" onClick="submitWithAction('ViewWorkbenchFromTabIntex');" href="#WorkBench">WorkBench Details</a></li>
        <li><a id="link_RA" onClick="submitWithAction('ViewReAssignPHMCommand');" href="#Assign">Re-Assign</a></li>
        <li><a id="link_ME" onClick="submitWithAction('ManageMyPatientCommand');" href="#ManageEvent">Manage My Patient</a></li>
        <li><a id="link_CO" onClick="submitWithAction('ViewManageEventCommand');" href="#Communication">Communication</a></li>
        <li><a id="link_DCT" onClick="submitWithAction('ViewDocumentsCommand');" href="#Documents">Documents</a></li>
        <li><a id="link_DGN" onClick="submitWithAction('ViewDocumentGenerationCommand');" href="#DocumentGeneration">Document Generation</a></li>
    </ol>
</div>
<br/>
<script>setSelectedCCTab('${tabSelectedIndex}');</script>
<br/>
<%-- 
    Document   : unionDetails
    Created on : Jul 5, 2016, 11:21:29 AM
    Author     : shimanem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" language="JavaScript">

            $(document).ready(function () {
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });

            function submitWithAction(action) {
                console.log("Validate Status : " + validate());
                if (validate() === true) {
                    document.getElementById("opperation").value = action;
                    document.forms[0].submit();
                }
            }

            function validate() {
                //$("#unionName_error").text("");
                $("#repName_error").text("");
                $("#repSurname_error").text("");
                $("#unionRepCapacity_error").text("");
                $("#unionRegion_error").text("");
                $("#unionInceptionDate_error").text("");
                $("#unionTerminationDate_error").text("");
                $("#contactNumber_error").text("");
                $("#emailAddress_error").text("");
                var valid = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if ($("#repName").val() === "") {
                    $("#repName_error").text("Please fill in the Representative Name");
                    return false;
                }
                if ($("#repSurname").val() === "") {
                    $("#repSurname_error").text("Please fill in the Representative Surname");
                    return false;
                }
                if ($("#unionRepCapacity").val() === "") {
                    $("#unionRepCapacity_error").text("Please choose the Union Representative Capacity");
                    return false;
                }
                if ($("#unionRegion").val() === "") {
                    $("#unionRegion_error").text("Please select the Region");
                    return false;
                }
                if ($("#unionInceptionDate").val() === "") {
                    $("#unionInceptionDate_error").text("Please choose the Inception Date");
                    return false;
                }
                if ($("#unionTerminationDate").val() === "") {
                    $("#unionTerminationDate_error").text("Please choose the Termination Date");
                    return false;
                }
                if ($("#contactNumber").val() === "") {
                    $("#contactNumber_error").text("Please fill the Telephone Number");
                    return false;
                }
                if ($("#emailAddress").val() === "" || !valid.test($("#emailAddress").val())) {
                    $("#emailAddress_error").text("Please enter a valid email address");
                    return false;
                }

                return true;
            }
        </script>
    </head>
    <body>

        <div class="main_div">
            <table width=100% height=100%>
                <tr valign="top">
                    <td width="5px"></td>
                    <td align="left">

                        <agiletags:UnionTabs tabSelectedIndex="link_union_UD" />
                        <agiletags:ControllerForm name="unionDetailsForm">

                            <input type="hidden" name="opperation" id="opperation" value=""/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="unionID" id="union_note_id" value="${sessionScope.unionID}" />
                            <input type="hidden" name="unionNumber" id="unionNumber" value="" />
                            <input type="hidden" name="unionName" id="unionName" value="" />

                            <fieldset id="pageBorder">
                                <div class="header">
                                    <table width="100%" bgcolor="#dddddd"  style=" border: 1px solid #c9c3ba;">
                                        <tr>
                                            <td class="label" width="200"><span><label><b>Union Name :</b></label> <label id="unionName" > ${sessionScope.unionName}</label></span></td>
                                            <td class="label" width="200"><b>Union Number :</b> ${sessionScope.unionNumber}</td>
                                            <td class="label" width="200"><b>Alternative Union Number :</b> ${sessionScope.unionNumber}</td>
                                        </tr>
                                    </table>
                                </div><br/>
                                <!-- content goes here -->
                                <br/><label class="header">Union Details</label><br/>  
                                <HR color="#666666" WIDTH="100%" align="left">
                                <table>
                                    <tr><agiletags:LabelTextBoxError readonly="Yes" displayName="Union Name" elementName="unionName" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="Yes" displayName="Union Number" elementName="unionNumber" valueFromSession="yes" /></tr>
                                    <tr><agiletags:LabelTextBoxError readonly="Yes" displayName="Alternative Union Number" elementName="altUnionNumber" valueFromSession="yes" /></tr>
                                    <tr><agiletags:LabelTextBoxErrorReq mandatory="yes" displayName="Representative Name" elementName="repName" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxErrorReq mandatory="yes" displayName="Representative Surname" elementName="repSurname" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Capacity of Representative"  elementName="unionRepCapacity" lookupId="347" mandatory="yes" javaScript="" errorValueFromSession="yes" /></tr>
                                    <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Region"  elementName="unionRegion" lookupId="251" mandatory="yes" javaScript="" errorValueFromSession="yes" /></tr>
                                    <tr><agiletags:LabelTextBoxDate mandatory="yes" displayname="Inception Date" elementName="unionInceptionDate" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxDate mandatory="yes" displayname="Termination Date" elementName="unionTerminationDate" valueFromSession="yes"/></tr>                               
                                </table>

                                <br/><br/>
                                <label class="header">Contact Details</label><br/>
                                <HR color="#666666" WIDTH="100%" align="left">
                                <table>
                                    <tr><agiletags:LabelTextBoxErrorReq mandatory="yes" displayName="Telephone Number" elementName="contactNumber" valueFromSession="yes" numericValidation="yes"/></tr>                         
                                    <tr><agiletags:LabelTextBoxError displayName="Fax Number" elementName="faxNumber" valueFromSession="yes" numericValidation="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxErrorReq mandatory="yes" displayName="Email Address" elementName="emailAddress" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Alternative Email Address" elementName="altEmailAddress" valueFromSession="yes"/></tr>                              
                                </table>

                                <br/><br/>
                                <label class="header">Postal Address</label><br/>   
                                <HR color="#666666" WIDTH="100%" align="left">
                                <table>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 1" elementName="postLine1" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 2" elementName="postLine2" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 3" elementName="postLine3" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Location" elementName="postLocation" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Code" elementName="postCode" valueFromSession="yes" numericValidation="yes"/></tr>
                                </table>

                                <br/><br/>
                                <label class="header">Physical Address</label><br/>
                                <HR color="#666666" WIDTH="100%" align="left">
                                <table>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 1" elementName="physicalLine1" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 2" elementName="physicalLine2" valueFromSession="yes"/></tr> 
                                    <tr><agiletags:LabelTextBoxError displayName="Line 3" elementName="physicalLine3" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Location" elementName="physicalLocation" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Code" elementName="physicalCode" valueFromSession="yes" numericValidation="yes"/></tr>
                                </table>

                                <br/><HR color="#666666" WIDTH="100%" align="left"><br/>
                                <table>
                                    <tr>
                                        <agiletags:ButtonOpperationLabelError displayName="Reset" elementName="unionContactResetBtn" type="button" commandName="" javaScript=""/>
                                        <agiletags:ButtonOpperationLabelError displayName="Save" elementName="unionContactSaveBtn" type="button" commandName="" javaScript="onClick=\"submitWithAction('UnionUpdateCommand')\";"/>
                                    </tr>
                                </table>
                            </fieldset>
                        </agiletags:ControllerForm>  
                    </td>             
                </tr>
            </table>
        </div>
    </body>
</html>


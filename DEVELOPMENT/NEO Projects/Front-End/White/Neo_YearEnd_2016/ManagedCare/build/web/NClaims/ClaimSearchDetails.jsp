<%-- 
    Document   : ClaimSearchDetails
    Created on : 2016/03/04, 07:54:38
    Author     : jpp
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>

<nt:NeoPanel title="Search Results ${batchId}"  collapsed="false" reload="false">
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="100%">
        <thead>
            <tr>
                <th>Batch Number</th>
                <th>Claim ID </th>
                <th>Practice Number</th>
                <th>Received Date</th>
                <th>Service Date</th>
                <th>Payment Date</th>
                <th>Claimed Amount</th>
                <th>Paid Amount</th>
                <th>Network Indicator</th>
                <th>Status</th>
                <th>Action</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="item" items="${searchClaimResult}">
                <c:set var="principalID" value="principalEID${item.claimId}"></c:set>
                <c:set var="categoryFullName" value="categoryName${item.claimId}"></c:set>
                <c:set var="linesCaptured" value="lineCaptured${item.claimId}"></c:set>
                <c:set var="linesCaptureds" value="${requestScope[linesCaptured]}"></c:set>
                <c:set var="condition" value="1"></c:set>
                <tr>
                    <td><label>${item.batchNumber}</label></td>
                    <td><label>${item.claimID}</label></td>
                    <td><label>${item.practiceNumber}</label></td>
                    <td><label>${item.receivedDate}</label></td>
                    <td><label>${item.serviceDate}</label></td>
                    <td><label>${item.paymentDate}</label></td>
                    <td><label>${item.claimedAmount}</label></td>
                    <td><label>${item.paidAmount}</label></td>
                    <td><label>${item.networkIndicator}</label></td>
                    <td><label>${item.status}</label></td>
                    <td><input type="button" value="Search" onclick="captureClaimLine('${item.batchNumber}','${item.claimID}','${item.practiceNumber}', '${item.receivedDate}')',${item.memNum}')';"></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</nt:NeoPanel>    
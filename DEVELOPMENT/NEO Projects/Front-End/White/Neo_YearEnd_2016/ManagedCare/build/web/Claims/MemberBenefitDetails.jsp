<%-- 
    Document   : MemberBenefitDetails
    Created on : 2011/08/18, 06:53:16
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="bt" tagdir="/WEB-INF/tags/benefits" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.scrollTo-1.4.2-min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.fixedtableheader.min.js"></script>
        <link rel="stylesheet" href="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>

        <script>
            
            $(function() {
                var dep = "<%= session.getAttribute("depListValues")%>";
                var str = $("#policyHolderNumber").val();
                setCoverDepByNumber(str, "policyHolderNumber",dep);
                
                newResizeContent();
                //attach on resize event
                $(window).resize(function() {
                    newResizeContent();
                });

            });

            function newResizeContent() {
                resizeContent();
                $('#fixedtableheader0').css('margin-top', $('.header_area').height());
            }
            
            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function getPrincipalMemberForNumber(str, element){
                xmlhttp=GetXmlHttpObject();
                if (xmlhttp==null)
                {
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=GetMemberByNumberCommand&number="+ str + "&element=" + element;
                xmlhttp.onreadystatechange=function(){stateChanged(element)};
                xmlhttp.open("POST",url,true);
                xmlhttp.send(null);
            }


            function stateChanged(element){
                if (xmlhttp.readyState==4)
                {
                    // alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    // alert(result);
                    if(result == "FindPracticeByCodeCommand"){
                        var name = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=')+1, xmlhttp.responseText.lastIndexOf('|'));
                        var discipline = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=')+1, xmlhttp.responseText.lastIndexOf('$'));
                        document.getElementById('discipline').value = discipline;
                        document.getElementById('provName').value = name;
                        document.getElementById(element + '_error').innerHTML = '';
                        document.getElementById('searchCalled').value = "";
                    }else if(result == "GetMemberByNumberCommand"){
                                                        
                        var resultArr = xmlhttp.responseText.split("|");
                        var numberArr = resultArr[1].split("=");
                                                                
                        var number = numberArr[1];
                        document.getElementById(element + '_error').innerHTML = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('searchCalled').value = "";
                        submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                    }else if(result == "Error"){
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|')+1, xmlhttp.responseText.length);
                        if(forCommand == "FindPracticeByCodeCommand"){
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById('provName').value = '';
                            document.getElementById('discipline').value = '';
                            document.getElementById('searchCalled').value = "";
                        } else if(forCommand == "GetMemberByNumberCommand"){
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element + '_text').value = '';
                            document.getElementById('searchCalled').value = "";
                            submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                        }
                    }
                }
            }

            function getCoverByNumber(str, element){
                if(str != null && str != ""){
                    xhr = GetXmlHttpObject();
                    if(xhr==null){
                        alert("ERROR: Browser Incompatability");
                        return;
                    }
                    //check date 
                    var date = $("#depBenDate").val();
                    var url = "/ManagedCare/AgileController";
                    url+="?opperation=GetCoverDetailsByNumberWithDate&number="+ str + "&element="+element+"&date="+date;
                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            $("#depListValues").empty();
                            $("#depListValues").append("<option value=\"99\"></option>");
                            var xmlDoc = GetDOMParser(xhr.responseText);
                            //print value from servlet
                            $(xmlDoc).find("EAuthCoverDetails").each(function (){
                                //extract dependent info from tag
                                $(this).find("DependantInfo").each(function(){
                                    var depInfo = $(this).text();
                                    var nameSurnameType = depInfo.split("|");
                                    //Add option to dependant dropdown
                                    var optVal = document.createElement('OPTION');
                                    optVal.value = nameSurnameType[0];
                                    optVal.text = nameSurnameType[1];
                                    document.getElementById("depListValues").options.add(optVal);
                                });
                            });
                        }
                    }
                    xhr.open("POST",url,true);
                    xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                    xhr.send(null);
                    
                }
            }
            
            function setCoverDepByNumber(str, element, dep){
                if(str != null && str != ""){
                    xhr = GetXmlHttpObject();
                    if(xhr==null){
                        alert("ERROR: Browser Incompatability");
                        return;
                    }
                    //check date 
                    var date = $("#depBenDate").val();
                    var url = "/ManagedCare/AgileController";
                    url+="?opperation=GetCoverDetailsByNumberWithDate&number="+ str + "&element="+element+"&date="+date;
                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            $("#depListValues").empty();
                            $("#depListValues").append("<option value=\"99\"></option>");
                            var xmlDoc = GetDOMParser(xhr.responseText);
                            //print value from servlet
                            var index = 1;
                            $(xmlDoc).find("EAuthCoverDetails").each(function (){
                                //extract dependent info from tag
                                $(this).find("DependantInfo").each(function(){
                                    var depInfo = $(this).text();
                                    var nameSurnameType = depInfo.split("|");
                                    //Add option to dependant dropdown
                                    var optVal = document.createElement('OPTION');
                                    optVal.value = nameSurnameType[0];
                                    optVal.text = nameSurnameType[1];
                                    
                                    document.getElementById("depListValues").options.add(optVal);
                                    if(dep ==  optVal.value){
                                        document.getElementById("depListValues").selectedIndex = index;
                                    }
                                    index++;
                                });
                            });
                        }
                    }
                    xhr.open("POST",url,true);
                    xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                    xhr.send(null);
                    
                }
            }
            
            function validateDate(dateString) {
                //  check for valid numeric strings
                var strValidChars = "0123456789/";
                var strChar;
                var strChar2;
                var blnResult = "true|";

                if (dateString.length < 10 || dateString.length > 10) return "false|Incorrect Date Length";

                //  test strString consists of valid characters listed above
                for (i = 0; i < dateString.length; i++)
                {
                    strChar = dateString.charAt(i);
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        return "false|Incorrect Date Format (YYYY/MM/DD)";
                    }
                }

                // test if slash is at correct position (yyyy/mm/dd)
                strChar = dateString.charAt(4);
                strChar2 = dateString.charAt(7);
                if (strChar != '/' || strChar2 != '/') return "false|Incorrect Date Format (YYYY/MM/DD)";
                return blnResult;
            }

            function GetDOMParser(xmlStr){
                var xmlDoc;
                if(window.DOMParser){
                    parser=new DOMParser();
                    xmlDoc= parser.parseFromString(xmlStr,"text/xml");
                }else{
                    xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async="false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }
            
            function validateBenDate(date){
                var covNum = $("#policyHolderNumber").val();
                var date = $("#depBenDate").val();
                var dateErrorStr = validateDate(date);
                var errors = dateErrorStr.split("|");
                var dateError = errors[0];
                var errorMsg = errors[1];
                
                if(dateError == "false"){
                    $("#depBenDate_error").text(errorMsg+" "+date);
                }else{                
                    $("#depBenDate_error").text("");
                    getCoverByNumber(covNum, "policyHolderNumber");
                }
                
            }
            
            function validateDepSelection(command){
                
                var depCode = $("#depListValues").val();
                var depBenDate = $("#depBenDate").val();
                $("#depListValues_error").text("");
                $("#depBenDate_error").text("");
                
                
                if(depBenDate == null || depBenDate == ""){
                    $("#depBenDate_error").text("Please Select a Date");
                    
                } else if(depCode == "99"){
                    $("#depListValues_error").text("Please Select a Dependant");
                                       
                }else{
                    submitWithAction(command);
                }                
                
            }
            
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }            
            
            function submitWithAction1(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.forms[0].submit();
            }
            
            function submitBDDAction(action, benId, onScreen, depCode, isThres) {
               
                document.getElementById('opperation').value = action;
                document.getElementById('benId').value = benId;
                document.getElementById('onBenefitScreen').value = onScreen;
                document.getElementById('depCode').value = depCode;
                document.getElementById('isThres').value = isThres;
                
                document.forms[0].submit();

            }
            
            
            function openContributionGrid(){
                document.getElementById("contributionTable").style.display = '';
                $.scrollTo($("#contributionTable"),600);
            }
            
            function closeContributionGrid(){
                document.getElementById("contributionTable").style.display = 'none';
                $.scrollTo($("#saver"),600);
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">

                    <agiletags:CallCenterTabs cctSelectedIndex="link_Ben" />
                    <fieldset id="pageBorder">
                        <div class="content_area" >
                            <label class="header">Benefit Details</label>
                            <br/><br/>
                            <table>
                                <agiletags:ControllerForm name="practiceDetails">
                                    <input type="hidden" name="opperation" id="opperation" value="" />
                                    <agiletags:HiddenField elementName="searchCalled" />
                                    <input type="hidden" name="onScreen" id="onScreen" value="" />
                                    <input type="hidden" name="listIndex" id="listIndex" value=""/>
                                    <input type="hidden" name="onBenefitScreen" id="onBenefitScreen" value=""/>
                                    <input type="hidden" name="benId" id="benId" value=""/>
                                    <input type="hidden" name="depCode" id="depCode" value=""/>
                                    <input type="hidden" name="isThres" id="isThres" value=""/>
                                    <input type="hidden" name="exactCoverNum" id="exactCoverNum" value="1" />
                                    
                                    <tr><agiletags:LabelTextBoxError displayName="Policy Number" elementName="policyHolderNumber"  valueFromSession="Yes" readonly="yes" javaScript="onblur=\"getCoverByNumber(this.value, 'policyHolderNumber');\""/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Benefit Date" elementName="depBenDate" javaScript="onChange=\"validateBenDate(this.value);\"" mandatory="yes" valueFromSession="yes"  /></tr>
                                    <tr><agiletags:EntityCoverDependantDropDown displayName="Cover Dependants" elementName="depListValues" javaScript="" mandatory="yes" valueFromSession="yes" /></tr>

                                    <tr><td colspan="5"></td></tr>
                                    <tr><td colspan="5"></td></tr>
                                    <tr><agiletags:ButtonOpperation align="left" displayname="View Benefits" commandName="ViewMemberBenefitsCommand" span="3" type="button" javaScript="onClick=\"validateDepSelection('ViewMemberBenefitsCommand')\";"/></tr>

                                </agiletags:ControllerForm>
                            </table>
                            <bt:ViewBenefits items="${memberBenefitsList2}" selectCommand="ViewMemberClaimsBenefitsCommand"/>
                        </div>
                    </fieldset>
                </td></tr></table>
                        <script>                
                            $('#benefitsTable').fixedtableheader({headerrowsize: 2});
                        </script>
    </body>
</html>

<%-- 
    Document   : QLinkErrors
    Created on : 2013/05/10, 02:42:31
    Author     : yuganp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script>
            function selectDetailLine(detailId, coverNumber, detailLineStatus) {
                document.getElementById('coverNumber').value=coverNumber;
                document.getElementById('detailsId').value=detailId;
                document.getElementById('detailLineStatus').value=detailLineStatus;
                document.forms['QLinkErrorsDetailsForm'].submit();
            }
        </script>
    </head>
    <body>
        <label class="header">QLink Errors</label>
        <hr>
        <br>
     <agiletags:ControllerForm name="QLinkErrorsPayrollForm">
        <input type="hidden" name="opperation" id="opperation" value="QLinkErrorsCommand" />
        <input type="hidden" name="command" id="QLinkErrorsPayrollCommand" value="selectPayroll" />
        <table id="ErrorsTable">
            <tr>
                <td width="160"><label class="label">Payroll</label></td>
                <td>
                    <select name="payroll" onchange="submitFormWithAjaxPost(this.form, 'QLinkBatchDiv', this);">
                        <option value="">Please select a Payroll</option>
                        <option value="0001">Persal</option>
                        <option value="0002">DOD</option>
                        <option value="0041">SASSA</option>
                    </select>
                </td>
            </tr>
        </table>
     </agiletags:ControllerForm>

        <div id="QLinkBatchDiv"></div>
        
    </body>
</html>

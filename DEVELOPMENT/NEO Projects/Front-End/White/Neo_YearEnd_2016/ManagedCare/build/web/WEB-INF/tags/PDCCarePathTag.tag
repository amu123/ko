<%-- 
    Document   : PDCCarePathTag
    Created on : May 24, 2016, 8:25:56 AM
    Author     : nick
--%>

<%@tag description="Display Clinical Care Path for Dependant CDL" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ attribute name="select" rtexprvalue="true" required="true" %>
<%@ attribute name="javascript" rtexprvalue="true" required="true" %>

<table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
    <c:if test="${!empty sessionScope.eventTaskTable}"> 
        <tr>
            <th scope="col">Worklist No</th>
            <th scope="col">Assigned to</th>
            <th scope="col">Event Task ID</th>
            <th scope="col">Event ID</th>
            <th scope="col">Event Status</th>
            <th scope="col">Task Type</th>
            <th scope="col">Due Date</th>
            <th scope="col">Completed Date</th>
            <th scope="col">Action</th>
        </tr>
        <c:set var="indexer" value="0" />
            <c:forEach var="entry" items="${sessionScope.eventTaskTable}"> 
                <tr>
                    <td><label class="label">${indexer + 1}</label></td>
                    <c:set var="indexer" value="${indexer + 1}" />
                    <td><label class="label">${sessionScope.taskAssignedTo}</label></td>
                    <td><label class="label">${entry.eventTaskId}</label></td>
                    <td><label class="label">${entry.eventId}</label></td>
                    <td><label class="label">${entry.eventStatus.value}</label></td>
                    <td><label class="label">${entry.taskType.value}</label></td>
                    <td><label class="label">${agiletags:formatXMLGregorianDate(entry.taskDueDate)}</label></td>
                    <td><label class="label">${agiletags:formatXMLGregorianDate(entry.taskCompletedDate)}</label></td>  
                    <c:if test="${select == 'yes'}">
                        <c:choose>
                            <c:when test="${entry.eventStatus.id == '2'}">
                                <td>&nbsp;</td>
                            </c:when>
                            <c:when test="${not empty entry.taskCompletedDate}">
                                <td>&nbsp;</td>
                            </c:when>
                            <c:otherwise>
                                <td><button name="opperation" type="submit" ${javascript} value="${entry.eventId}">Select</button></td>
                            </c:otherwise>
                        </c:choose>                  
                    </c:if>
                </tr>
            </c:forEach>
    </c:if>
</table>

<table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
            <tr>
                <th scope="col" colspan="3" align="left"><button><<</button>2015</th><th scope="col" colspan="2" align="left">2016</th><th scope="col" colspan="3" align="right">2017<button>>></button></th>
            </tr>
            <tr>
                <th scope="col" rowspan="2">Asthma</th>
                <th scope="col" colspan="4">January</th>
            </tr>
            <tr>
                <th scope="col">W1</th>
                <th scope="col">W2</th>
                <th scope="col">W3</th>
                <th scope="col">W4</th>
            </tr>
            <tr><th scope="col">Task 1</th><td></td><td></td><td></td><td></td></tr>
            <tr><th scope="col">Task 2</th><td></td><td></td><td></td><td></td></tr>
            <tr><th scope="col">Task 3</th><td></td><td></td><td></td><td></td></tr>
            <tr><th scope="col">Task 4</th><td></td><td></td><td></td><td></td></tr>
            <tr><th scope="col">Task 5</th><td></td><td></td><td></td><td></td></tr>
            <tr><th scope="col">Task 6</th><td></td><td></td><td></td><td></td></tr>
        </table>


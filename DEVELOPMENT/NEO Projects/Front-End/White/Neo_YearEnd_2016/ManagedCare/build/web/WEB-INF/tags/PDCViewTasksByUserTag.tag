<%-- 
    Document   : PDCViewTasksByUserTag
    Created on : 10 Feb 2015, 2:23:16 PM
    Author     : chrisdp
--%>

<%@tag description="Display tasks by user assigned to that task" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ attribute name="select" rtexprvalue="true" required="true" %>
<%@ attribute name="javascript" rtexprvalue="true" required="true" %>

<table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
    <c:if test="${!empty sessionScope.eventTaskTable}"> 
        <tr>
            <th scope="col">Worklist No</th>
            <th scope="col">Assigned to</th>
            <th scope="col">Event Task ID</th>
            <th scope="col">Event ID</th>
            <th scope="col">Event Status</th>
            <th scope="col">Task Type</th>
            <th scope="col">Due Date</th>
            <th scope="col">Completed Date</th>
            <th scope="col">Action</th>
        </tr>
        <c:set var="indexer" value="0" />
            <c:forEach var="entry" items="${sessionScope.eventTaskTable}"> 
                <tr>
                    <td><label class="label">${indexer + 1}</label></td>
                    <c:set var="indexer" value="${indexer + 1}" />
                    <td><label class="label">${sessionScope.taskAssignedTo}</label></td>
                    <td><label class="label">${entry.eventTaskId}</label></td>
                    <td><label class="label">${entry.eventId}</label></td>
                    <td><label class="label">${entry.eventStatus.value}</label></td>
                    <td><label class="label">${entry.taskType.value}</label></td>
                    <td><label class="label">${agiletags:formatXMLGregorianDate(entry.taskDueDate)}</label></td>
                    <td><label class="label">${agiletags:formatXMLGregorianDate(entry.taskCompletedDate)}</label></td>  
                    <c:if test="${select == 'yes'}">
                        <c:choose>
                            <c:when test="${entry.eventStatus.id == '2'}">
                                <td>&nbsp;</td>
                            </c:when>
                            <c:when test="${not empty entry.taskCompletedDate}">
                                <td>&nbsp;</td>
                            </c:when>
                            <c:otherwise>
                                <td><button name="opperation" type="submit" ${javascript} value="${entry.eventId}">Select</button></td>
                            </c:otherwise>
                        </c:choose>                  
                    </c:if>
                </tr>
            </c:forEach>
    </c:if>
</table>


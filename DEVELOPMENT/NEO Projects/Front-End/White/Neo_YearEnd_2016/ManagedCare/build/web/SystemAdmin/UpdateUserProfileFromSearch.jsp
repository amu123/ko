<%-- 
    Document   : UpdateUserProfileFromSearch
    Created on : 2011/12/09, 01:26:11
    Author     : josephm
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="neo.manager.NeoUser" %>
<%@ page import="neo.manager.SecurityResponsibility" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.tools.min.js"></script>       
        <script language="JavaScript">

            $(document).ready(function() {
                $("#passwordRow").hide();
                $("#passwordRetypeRow").hide();
                $("#accountStatusRow").hide();
                checkUserResponsibility();
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('userId').value = index;
                document.forms[0].submit();
            }

            function checkUserResponsibility() {
            <%
                NeoUser user = (NeoUser) session.getAttribute("persist_user");
                System.out.println("From the JSP " + user.getName());
                for (SecurityResponsibility sec : user.getSecurityResponsibility()) {
                    if (sec.getResponsibilityId() == 1 || sec.getResponsibilityId() == 13) {
            %>
                //alert("Super User");
                $("#passwordRow").show();
                $("#passwordRetypeRow").show();
                $("#accountStatusRow").show();
            <%                                    }
                    System.out.println("From the JSP " + sec.getDescription());
                }
            %>
            }
            
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">

                    <agiletags:ControllerForm name="updateUserProfile">
                        <table>
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="userId" id="userId" value="" />

                            <tr><td><label class="header">Update Details</label></td></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Username" elementName="username" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Name" elementName="name" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="surname" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Email" elementName="email"  valueFromSession="yes"/></tr>
                            <tr id="passwordRow"><agiletags:LabelPasswordError displayName="Password" elementName="password"  valueFromSession="yes"/></tr>
                            <tr id="passwordRetypeRow"><agiletags:LabelPasswordError displayName="Retype Password" elementName="retypePassword"  valueFromSession="yes"/></tr>
                            <tr id="accountStatusRow"><agiletags:LabelTextBoxError displayName="Status" elementName="accountStatus"  valueFromSession="yes" readonly="yes"/></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td><label class="header">Responsibility Description</label></td></tr>
                            <agiletags:ResponsibilityRadioButtonLabel elementName="resp" numberPerRow="3"/>
                            <tr><td>&nbsp;</td></tr>
                            <agiletags:HiddenField elementName="workitem_id"/>
                            <agiletags:HiddenField elementName="user_id"/>
                            <c:if test="${applicationScope.Client != 'Sechaba'}">
                                <tr><td><label class="header">User Product Restriction</label></td></tr>
                                <agiletags:LabelNeoRadioButtonGroupProductRestriction elementName="productRest" />
                            </c:if>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <agiletags:ButtonOpperation type="button" align="left" commandName="UpdateUserProfileCommand" displayname="Update" span="1" javaScript="onClick=\"submitWithAction('UpdateUserProfileCommand')\";"/>
                                <agiletags:ButtonOpperation type="button" align="left" commandName="LockUserCommand" displayname="Lock" span="1" javaScript="onClick=\"submitWithAction('LockUserCommand')\";"/>
                                <agiletags:ButtonOpperation type="submit" align="left" commandName="UnlockUserCommand" displayname="Unlock" span="1" javaScript="onClick=\"submitWithAction('UnlockUserCommand')\";"/>
                                <agiletags:ButtonOpperation type="button" align="left" commandName="CancelUserProfileCommand" displayname="Cancel" span="1" javaScript="onClick=\"submitWithAction('CancelUserProfileCommand')\";"/>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <agiletags:NeoUserProfileSearchResultTable commandName="" javaScript=""/>
                        </table>
                    </agiletags:ControllerForm>

                </td></tr></table>
    </body>
</html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="MemberAppAuditForm">
    <input type="hidden" name="opperation" id="MemberAppAudit_opperation" value="MemberAppAuditCommand" />
    <input type="hidden" name="onScreen" id="MemberAppAudit_onScreen" value="MemberAppAudit" />
    <input type="hidden" name="applicationNumber" value="${requestScope.memberAppNumber}" />
    <label class="header">Member Application Audit Trail</label>
    <hr>

      <table id="MemberAppAuditTable">
          <tr>
              <td width="160" class="label">Details</td>
              <td>
                  <select style="width:215px" name="auditList" id="MemberAppList" onChange="submitFormWithAjaxPost(this.form, 'MemberAppAuditDetails');">
                      <option value="99" selected>&nbsp;</option>
<!--                      <option value="APPLICANTAPP_DETAILS_MEMAPP">Applicant Details</option>-->
                      <option value="PERSON_DETAILS_MEMAPP">Member details</option>
                      <option value="ADDRESS_DETAILS_MEMAPP">Address Details</option>
                      <option value="BANK_DETAILS_MEMAPP">Bank Details</option>
                      <option value="CONTACT_DETAILS_MEMAPP">Contact Details</option>
                      <option value="BROKER_DETAILS_MEMAPP">Member Broker</option>
                      <option value="GROUP_DETAILS_MEMAPP">Member Group</option>
                      <option value="OPTION_DETAILS_MEMAPP">Member Option</option>
                      <option value="UNDERWRITING_DETAILS_MEMAPP">Member Underwriting</option>
                      <option value="DEPENDANT_DETAILS_MEMAPP">Dependant Details</option>
                      
                  </select>
              </td>
                  
          </tr>
      </table>          
 </agiletags:ControllerForm>
 <div style ="display: none"  id="MemberAppAuditDetails"></div>
   
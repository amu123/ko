<%-- 
    Document   : MemberViewGridTag
    Created on : 11 Feb 2015, 3:39:09 PM
    Author     : gerritr
--%>

<%@tag description="For Member Detail - BiometricsView.jsp" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>   

<%@attribute name="commandName" required="true" type="java.lang.String" %>
<%@attribute name="javaScript" required="true" type="java.lang.String" %>
<%@attribute name="sessionAttribute" required="true" type="java.lang.String" %>
<%@attribute name="onScreen" required="true" type="java.lang.String" %>

<c:if test="${!empty sessionScope.MemberCoverDetails}">
    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th align="left">Cover Number</th>
            <th align="left">Code</th>
            <th align="left">Title, Name, Surname</th>
            <th align="left">Date Of Birth</th>
            <th align="left">Gender</th>
            <th align="left">Status</th>
            <th align="left">Option</th>
            <th align="left">Start Date</th>
            <th align="left">Resignation Date</th>
            <th align="left">Join Date</th>
        </tr>

        <c:forEach var="cd" items="${sessionScope.MemberCoverDetails}">
            <tr class="label">
                <td>${cd.coverNumber}</td>
                <td>${cd.dependentNumber}</td>
                <td>${cd.title}, ${cd.name}, ${cd.surname}</td>
                <td>${fn:replace(fn:substring(cd.dateOfBirth,0, 10),'-','/')}</td>
                <td>${cd.gender}</td>
                <td>${cd.status}</td>
                <td>${cd.optionName}</td>
                <td>${fn:replace(fn:replace(fn:substring(cd.coverStartDate,0, 10),'-','/'),'2999/12/31','')}</td>
                <td>${fn:replace(fn:replace(fn:substring(cd.coverEndDate,0, 10),'-','/'),'2999/12/31','')}</td>
                <c:if test="${cd.schemeJoinDate ne null}">
                    <td>${fn:replace(fn:replace(fn:substring(cd.schemeJoinDate,0, 10),'-','/'),'2999/12/31','')}</td>
                </c:if>
                <c:if test="${cd.schemeJoinDate == null}">
                    <td>${fn:replace(fn:replace(fn:substring(cd.coverStartDate,0, 10),'-','/'),'2999/12/31','')}</td>
                </c:if>
            </tr>
        </c:forEach>
    </table>
</c:if>
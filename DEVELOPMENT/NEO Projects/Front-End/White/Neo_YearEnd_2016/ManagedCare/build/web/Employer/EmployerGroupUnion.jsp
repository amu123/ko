<%-- 
    Document   : EmployerGroupUnion
    Created on : Jul 9, 2016, 11:42:32 AM
    Author     : shimanem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" language="JavaScript">

            $(document).ready(function () {
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <div id="main_div">

            <agiletags:ControllerForm name="employerUnionGroupForm">

                <input type="hidden" name="opperation" id="opperation" value="FowardToAdvancedUnionSearch"/>
                <input type="hidden" name="buttonPressed" id="employerGroupButtonPressed" value="saveEmployerUnionGroup" />
                <input type="hidden" name="tab" value="EmployerGroupUnion" />
                <input type="hidden" name="employerEntityId" id="employerEntityId" value="${employerEntityId}" />
                <input type="hidden" name="employerName" id="employerName" value="${EmployerName}" />
                <input type="hidden" name="employerNumber" id="employerNumber" value="${EmployerNumber}" />
                <input type="hidden" name="onScreen" id="onScreen" value="" />

                <!-- content goes here -->
                <br/><label class="header">Union Details</label><br/>  
                <HR color="#666666" WIDTH="100%" align="left">
                <table>
                    <%--
                    <c:if test="${not empty sessionScope.employerGroup_unionName && (sessionScope.employerGroup_unionName != null || sessionScope.employerGroup_unionName != 'null')}">
                        <tr><agiletags:LabelTextBoxError readonly="yes"  mandatory="yes" displayName="Union Name" elementName="employerGroup_unionName" valueFromSession="yes"/></tr>
                    </c:if>
                    <c:if test="${empty sessionScope.employerGroup_unionName && (sessionScope.employerGroup_unionName  == null || sessionScope.employerGroup_unionName == 'null')}" >
                        <tr><agiletags:LabelTextSearchWithNoText  displayName="Union Name" elementName="employerGroup_unionName" mandatory="yes" onClick="document.getElementById('employerGroupButtonPressed').value = 'advancedUnionSearch'; getPageContents(document.forms['employerUnionGroupForm'],null,'main_div','overlay');"/></tr>
                    </c:if>
                    --%>
                    <tr><agiletags:LabelTextSearchWithNoText  displayName="Union Name" mandatory="yes" elementName="employerGroup_unionName" onClick="document.getElementById('employerGroupButtonPressed').value = 'advancedUnionSearch'; getPageContents(document.forms['employerUnionGroupForm'],null,'main_div','overlay');"/></tr>
                    <tr><agiletags:LabelTextBoxError readonly="yes"  mandatory="yes" displayName="Union ID" elementName="employerGroup_unionID" valueFromSession="yes"/></tr>
                    <tr><agiletags:LabelTextBoxError readonly="yes" mandatory="yes" displayName="Union Number" elementName="employerGroup_unionNumber" valueFromSession="yes"/></tr>
                    <tr><agiletags:LabelTextBoxErrorReq mandatory="yes" displayName="Representative Name" elementName="repName" valueFromSession="yes"/></tr>
                    <tr><agiletags:LabelTextBoxErrorReq mandatory="yes" displayName="Representative Surname" elementName="repSurname" valueFromSession="yes"/></tr>
                    <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Capacity of Representative"  elementName="unionRepCapacity" lookupId="347" mandatory="yes" javaScript="" errorValueFromSession="yes"/></tr>
                    <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Region"  elementName="unionRegion" lookupId="251" mandatory="yes" javaScript="" errorValueFromSession="yes" /></tr> 
                    <tr><agiletags:LabelTextBoxDate displayname="Inception Date" elementName="unionInceptionDate" mandatory="yes" valueFromSession="yes"/></tr>
                    <tr><agiletags:LabelTextBoxDate displayname="Termination Date" elementName="unionTerminationDate" mandatory="yes" valueFromSession="yes"/></tr>                               
                </table>
                <br/><br/>
                <label class="header">Contact Details</label><br/>
                <HR color="#666666" WIDTH="100%" align="left">
                <table>
                    <tr><agiletags:LabelTextBoxErrorReq displayName="Telephone Number" elementName="contactNumber" valueFromSession="yes"/></tr>                         
                    <tr><agiletags:LabelTextBoxError displayName="Fax Number" elementName="faxNumber" valueFromSession="yes"/></tr>
                    <tr><agiletags:LabelTextBoxErrorReq displayName="Email Address" elementName="emailAddress" valueFromSession="yes"/></tr>
                    <tr><agiletags:LabelTextBoxError displayName="Alternative Email Address" elementName="altEmailAddress" valueFromSession="yes"/></tr>                              
                </table>
                <br/><br/>
                <table>
                    <tr>                 
                        <td><input type="reset" value="Reset"></td>
                        <td><input type="button" value="Save" onclick="document.getElementById('employerGroupButtonPressed').value = 'saveEmployerUnionGroup';
                                submitFormWithAjaxPost(this.form, 'EmployerGroupUnion', this);"></td>
                    </tr>
                </table>
            </agiletags:ControllerForm>
        </div>
    </body>
</html>
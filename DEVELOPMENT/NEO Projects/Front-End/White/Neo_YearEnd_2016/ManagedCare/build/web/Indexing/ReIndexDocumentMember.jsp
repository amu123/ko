<%-- 
    Document   : IndexDocument
    Created on : 02 Jul 2012, 3:34:17 PM
    Author     : Christo
--%>
<%@page import="com.koh.serv.PropertiesReader"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<div id="tabTableLayout">
    <table width=100% height=100%><tr valign="left"><td width="50px"></td><td align="left">
                <label class="header">Document Re-Indexing For <%=request.getAttribute("fileName")%> </label>
                <br/><br/><br/>
               <agiletags:ControllerForm name="documentReIndexing"> 
                <table>   
                        <tr><td>                                    
                                <table>
                                    <input type="hidden" name="indexFileLocation" id="indexFileLocation" value="${requestScope.indexFileLocation}" />
                                    <input type="hidden" name="indexEntityType" id="indexEntityType" value="${requestScope.indexEntityType}" />
                                    <input type="hidden" name="indexDocType" id="indexDocType" value="${requestScope.docType}" />
                                    <input type="hidden" name="indexEntityNumber" id="indexEntityNumber" value="${requestScope.indexEntityNumber}">
                                    <input type="hidden" name="indexRefNumber" id="indexRefNumber" value="${requestScope.indexRefNumber}">
                                    <input type="hidden" name="indexID" id="indexID" value="${requestScope.indexID}">
                                    <tr><td><label id="errortext" class="error"/></td></tr>
                                    <tr><agiletags:DocumentIndexLableText displayName="Member Number" elementName="memberNumber"/></tr>
                                    <tr><agiletags:DocumentIndexDropdownType displayName="Document Type" elementName="docType"/></tr>
                                    <tr><td><button type="button" name="opperation" id="reindexButton" onclick="reIndexFileGeneric();">Re-Index</button><button type="button" name="opperation" id="reIndexCancelButton" onclick="swapDivVisbible('overlay','main_div');" >Cancel</button></td></tr>
                                </table>
                            </td></tr>
                </table>
               </agiletags:ControllerForm>
            </td></tr></table>
</div>     


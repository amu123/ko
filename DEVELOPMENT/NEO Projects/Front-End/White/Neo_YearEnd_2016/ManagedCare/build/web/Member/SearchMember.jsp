<%-- 
    Document   : SearchMember
    Created on : 2012/05/14, 10:38:02
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" language="JavaScript">
                
            function validateMemberSearch(action, onScreen){
                $("#btn_MemSearch_error").text("");
                $("#memNo_error").text("");
                
                var count = 0;
                if ($("#memNo").val() == "" && $("#idNo").val() == "") {
                    count = ($("#initials").val() == "" ? 0 : 1) + ($("#surname").val() == "" ? 0 : 1) + ($("#dob").val() == "" ? 0 : 1);
                    if (count < 2) {
                        $("#btn_MemSearch_error").text("Please provide search criteria");
                        return false;
                    }
                }
                return submitCoverSearch(action, onScreen);
            }
                
            function submitCoverSearch(action, onScreen){
                document.getElementById("onScreen").value = onScreen;
                document.getElementById("opperation").value = action;
//                document.forms[0].submit();
                return true;
            }
                
            function reloadCoverDeps(coverNum){
                document.getElementById("coverNum").value = coverNum;
                document.getElementById("onScreen").value = "/Member/DependantSelection.jsp";
                document.getElementById("opperation").value = "ViewDependantsForCover";
                document.forms[0].submit();
            }
              
                
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Search Member</label>
                    <br/><br/>
                    <table>
                        <agiletags:ControllerForm name="saveAuth" validate="yes">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="coverNum" id="coverNum" value="" />

                            <tr><agiletags:LabelTextBoxError displayName="Member Number" elementName="memNo"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="ID Number" elementName="idNo"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Initials" elementName="initials"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="surname"/></tr>
                            <tr><agiletags:LabelTextBoxDate displayname="Date of Birth" elementName="dob"/></tr>
                            <!-- BUTTON OF TYPE SUBMIT TO ACCOMMODATE THE 'ENTER' KEY -->
                            <tr><agiletags:ButtonOpperationLabelError displayName="Search" elementName="btn_MemSearch" type="submit" commandName="" javaScript="onClick=\"return validateMemberSearch('MemberSearchByCriteria','/Member/SearchMember.jsp')\";"/></tr>
                            
                        </agiletags:ControllerForm>                            
                    </table>
                    <br/>
                    <HR color="#666666" WIDTH="80%" align="left">
                    <br/>
                    <label class="header">Member Search Result</label><br><br>
                    <c:if test="${not empty coverSearchResultsMessage}">
                        <label class="red">${coverSearchResultsMessage}</label>
                    </c:if>
                    <c:if test="${ not empty MemberCoverDetails}">
                        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                            <tr>
                                <th align="left">Cover Number</th>
                                <th align="left">Name</th>
                                <th align="left">Option</th>
                                <th align="left">Status</th>
                                <th align="left">Join Date</th>
                                <th align="left">Option Date</th>
                                <th align="left">Resigned Date</th>
                                <th>&nbsp;</th>
                            </tr>
                            <c:forEach items="${MemberCoverDetails}" var="item">
                                <c:choose>
                                    <c:when test="${(applicationScope.Client == 'Sechaba') && (item.status == 'Suspend')}" >
                                        <tr style="background-color: #ff4d4d">
                                    </c:when>
                                    <c:otherwise>
                                        <tr>
                                    </c:otherwise>
                                </c:choose>
                                    <td><label class="label">${item.coverNumber}</label></td>
                                    <td><label class="label">${item.name} ${item.surname}</label></td>
                                    <td><label class="label">${item.optionName}</label></td>
                                    <td><label class="label">${item.status}</label></td>
                                    <td><label class="label">${agiletags:formatXMLGregorianDate(item.schemeJoinDate)}</label></td>
                                    <td><label class="label">${agiletags:formatXMLGregorianDate(item.coverStartDate)}</label></td>
                                    <td><label class="label">${agiletags:formatXMLGregorianDate(item.coverEndDate) == "2999/12/31" ? "" : agiletags:formatXMLGregorianDate(item.coverEndDate)}</label></td>
                                    <td><button name="opperation" type="button" onClick="reloadCoverDeps('${item.coverNumber}');">Details</button></td>
                                </tr>
                            </c:forEach>
                        </table>
                    </c:if>
                </td>
            </tr>
        </table>
    </td></tr></table>
</body>
</html>


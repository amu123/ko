<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions' %>
    <label class="header">Write-Off Contributions</label>
    <hr>
    <br>
    <table>
        <tr>
            <td width="160"><label>Member Number:</label></td>
            <td><label> ${coverNo}</label></td>
        </tr>
        <tr>
            <td><label>Member Name:</label></td>
            <td><label> ${memberName} ${memberSurname}</label></td>
        </tr>
    </table>
    <br>
    <c:choose>
        <c:when test="${empty writeOffs}">
            <span><label>No Contribution Write-Off details available</label></span>
            <br>
            <input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}','${main_div}');">
        </c:when>
        <c:otherwise>
            <br>
            <table>
                <tr><agiletags:LabelTextBoxDate displayname="Refund Date" elementName="refundDate" mandatory="yes"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="Claims Recovered Amount" elementName="refundClaims" mandatory="yes"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="Savings Paid" elementName="refundSavings" mandatory="yes"/></tr>
                <tr>
                    <td><input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
                    <td><input type="button" value="Refund" onclick="saveWriteOff(this);"> </td>
                </tr>
            </table>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Contribution Period</th>
                        <th>Payment Narrative</th>
                        <th>Debit Order Date</th>
                        <th>Amount</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${writeOffs}">
                        <tr>
                            <td><label>${entry['contribDate']}</label></td>
                            <td><label>${entry['narrative']}</label></td>
                            <td><label>${entry['statementDate']}</label></td>
                            <td align="right"><label>${agiletags:formatStringAmount(entry['amount'])}</label></td>
                            <td><button type="button" id="btnTransfer_${entry['contribTranId']}" onclick="transferDOAlloc(${entry['contribTranId']});">Transfer</button></td>
                        <input type="hidden" id="contribTranId_${entry['contribTranId']}" value="${entry['contribTranId']}">
                        <input type="hidden" id="entityId_${entry['contribTranId']}" value="${entry['entityId']}">
                        <input type="hidden" id="optionId_${entry['contribTranId']}" value="${entry['optionId']}">
                        <input type="hidden" id="entityTypeId_${entry['contribTranId']}" value="${entry['entityTypeId']}">
                        <input type="hidden" id="statementDate_${entry['contribTranId']}" value="${entry['statementDate']}">
                        <input type="hidden" id="contribDate_${entry['contribTranId']}" value="${entry['contribDate']}">
                        <input type="hidden" id="paymentNarrative_${entry['contribTranId']}" value="${entry['narrative']}">
                        <input type="hidden" id="amount_${entry['contribTranId']}" value="${entry['amount']}">
                        </tr>
                    </c:forEach>
                </table>

        </c:otherwise>
    </c:choose>
        
        
      

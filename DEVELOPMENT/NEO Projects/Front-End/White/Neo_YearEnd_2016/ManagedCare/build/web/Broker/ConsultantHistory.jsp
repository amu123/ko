
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

  <agiletags:ControllerForm name="ConsultantHistoryForm">
      <div id="consultantHistoryGrid">
    <input type="hidden" name="opperation" id="opperation" value="AddConsultantHistoryCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberDependantApp" />
    <input type="hidden" name="memberAppNumber" id="memberAppDepNumber" value="${memberAppNumber}" />
    <input type="hidden" name="buttonPressed" id="consultantHistoryButtonPressed" value="" />
    <input type="hidden" name="depNo" id="memberAppDepNo" value="" />
    <input type="hidden" name="consultantEntityId" id="consultantHistoryEntityId" value="${requestScope.consultantEntityId}" />
    <input type="hidden" name="consultantRegion" id="consultantHisRegion" value="${requestScope.consultantRegion}" />
    <input type="hidden" name="brokerBrokerConsultantID" id="brokerBrokerConsultantID" value="">
    <label class="header">Consultant History</label>
    <br>
<input type="button" name="AddHistoryButton" value="Add History" onclick="document.getElementById('consultantHistoryButtonPressed').value = this.value; getPageContents(this.form,null,'main_div','overlay');"/>
<br>
                <table id="depListTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%">
                    <tr>
                        <th>Broker Code</th>
                        <th>Broker Name</th>
                        <th>Broker Start Date</th>
                        <th>Broker End Date</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${accreList}">

                        <tr>
                            <td>${entry.brokerCode}</td>
                            <td>${entry.brokerName}</td>
                            <td><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveStartDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.day}"/></td>
                            <td><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveEndDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.day}"/></td>                            
                            <td style="width: 50px">
                                <c:if test="${agiletags:formatXMLGregorianDate(entry.effectiveEndDate) == '2999/12/31'}">
                                    <input type="button" value="End" onclick="document.getElementById('brokerBrokerConsultantID').value ='${entry.brokerBrokerConsultId}';document.getElementById('consultantHistoryButtonPressed').value = this.value;getPageContents(this.form,null,'main_div','overlay');"/>
                                </c:if>
                            </td>
                        </tr>
                        
  
                    </c:forEach>
                </table>
      </div>
  </agiletags:ControllerForm>

<%-- 
    Document   : WorkflowMyWorkbench
    Created on : 2016/07/13, 11:21:49
    Author     : gerritr
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html style="height:100%; border-right: medium solid #6cc24a;">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/Workflow/WorkflowTabUtil.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Workflow/Workflow.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script>

            //FUNCTION TO ENABLE/DISABLE THE FORWARD & REASSIGN BTN
            function buttonEnable(userID, elementName) {
                if (userID !== "99" && userID !== "") {
                    document.getElementById(elementName).disabled = false;
                }
                else {
                    document.getElementById(elementName).disabled = true;
                }
            }


            //FUNCTION TO LOAD USERS WHEN WORKFLOW QUEUE IS SELECTED
            function userFilterByQueueID(queueID, element2Toggle) {
                if (queueID !== "99") {
                    addUserList(queueID, element2Toggle);
                }
            }

            //AJAX FUNCTION TO GET THE FILTERED USERS
            //NOTE: dont call this function directly
            function addUserList(queueID, element2Toggle) {

                var element2ToggleJQ = "#" + element2Toggle;

                var url = "/ManagedCare/AgileController";
                var data = {
                    'opperation': 'getUserListByWorkflowQueueID',
                    'queueID': queueID
                };

                $.get(url, data, function (resultTxt) {
                    if (resultTxt !== null) {
                        $(element2ToggleJQ).empty();
                        $(element2ToggleJQ).append("<option value=\"99\"></option>");
                        var xmlDoc = GetDOMParser(resultTxt);
                        //print value from servlet
                        $(xmlDoc).find("UserList").each(function () {
                            //set product details
                            $(this).find("Users").each(function () {
                                var prodOpt = $(this).text();
                                var opt = prodOpt.split("|");
                                //Add option to dependant dropdown
                                var optVal = document.createElement('OPTION');
                                optVal.value = opt[0];
                                optVal.text = opt[1];
                                document.getElementById(element2Toggle).options.add(optVal);

                            });
                        });
                    }
                    //SET RETURN TYPE AS XML    
                }
                , "text/xml");
            }
            //DOM PARSER FOR XML OF DROPDOWNS RETURNED BY AJAX            
            function GetDOMParser(xmlStr) {
                var xmlDoc;
                if (window.DOMParser) {
                    parser = new DOMParser();
                    xmlDoc = parser.parseFromString(xmlStr, "text/xml");
                } else {
                    xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async = "false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }

            function clearAllFromSession() {
                var url = "/ManagedCare/AgileController";
                var data = {'opperation': 'LeftMenuClearAllFromSession', 'id': new Date().getTime()};
                $.post(url, data);
            }

            function submitWithAction(action) {
                alert('submitWithAction(action)');
                checkMandatoryFields();
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }


            function submitWithAction(action, forField, onScreen) {
                console.log("onScreen: " + onScreen);
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function getPrincipalMemberForNumber(str, element) {
                console.log("in getPrincipalMemberForNumber");
//                disableSwitch();
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=GetMemberByNumberCommand&number=" + str + "&element=" + element;
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element);
                };
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function validateMember(str, element) {
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=MemberDetailsCoverSearchCommand&memberNumber=" + str + "&exactCoverNum=1";
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element)
                };
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function validateProvider(str, element) {
                console.log("in validateProvider");
//                disableSwitch();
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=FindPracticeByCodeCommand&providerNumber=" + str;
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element)
                };
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }
            
            function validateGroup(str, element){
                console.log("in validateGroup");
                 xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=WorkflowFindGroupsByNumberCommand&groupNum=" + str;
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element)
                };
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }
            
            function validateEnitiy(str, element){
                console.log("in validateEnitiy");
                var entityType = document.getElementById("entityType").value;
                console.log("entityType " + entityType);
                if(entityType == 1){ //member
                    getPrincipalMemberForNumber(str, element);
                }else if(entityType == 2){//provider
                    validateProvider(str, element);
                }else if(entityType == 3){//broker
                    
                }else if(entityType == 4){//third party
                    
                }else if(entityType == 5){//groups
                    validateGroup(str, element)
                }
            }

            function stateChanged(element) {
                if (xmlhttp.readyState == 4)
                {
                    // alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    console.log("result " + result);
                    if (result == "FindPracticeByCodeCommand") {
                        var name = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=') + 1, xmlhttp.responseText.lastIndexOf('|'));
                        var discipline = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=') + 1, xmlhttp.responseText.lastIndexOf('$'));
//                        document.getElementById('discipline').value = discipline;
//                        document.getElementById('provName').value = name;
                        document.getElementById(element + '_error').innerHTML = '';
                        // document.getElementById('searchCalled').value = "";
                    } else if (result == "GetMemberByNumberCommand") {

                        var resultArr = xmlhttp.responseText.split("|");
                        var numberArr = resultArr[1].split("=");
                        var emailArr = resultArr[2].split("=");
                        var productArr = resultArr[3].split("=");
                        var entityArr = resultArr[4].split("=");

                        var number = numberArr[1];
                        var productId = productArr[1];
                        var email = emailArr[1];
                        var entityID = entityArr[1];

                        document.getElementById(element + '_error').innerHTML = '';
//                        document.getElementById(element + '_text').value = number;
                        document.getElementById('productId').value = productId;
                        document.getElementById('entityID_text').value = entityID;

                        if (email != null && email != "") {
                            document.getElementById('callerContact').value = email;
                        } else if (number != null && number != "") {
                            document.getElementById('callerContact').value = number;
                        }

                        //document.getElementById('searchCalled').value = "";
            <%
                session.setAttribute("exactCoverNum", "1");
            %>
                        submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                    } else if(result == "WorkflowFindGroupsByNumberCommand"){
                        var name = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=') + 1, xmlhttp.responseText.lastIndexOf('|'));
                        document.getElementById(element + '_error').innerHTML = '';
                    } else if (result == "Error") {
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|') + 1, xmlhttp.responseText.length);
                        if (forCommand == "FindPracticeByCodeCommand") {
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById('provName').value = '';
                            document.getElementById('discipline').value = '';
                            // document.getElementById('searchCalled').value = "";
                        } else if (forCommand == "GetMemberByNumberCommand") {
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element + '_text').value = '';
                            //document.getElementById('searchCalled').value = "";
                            //submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                        }else if (forCommand == "WorkflowFindGroupsByNumberCommand") {
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element + '_text').value = '';
                            //document.getElementById('searchCalled').value = "";
                            //submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                        }
                    }
                }
            }

//            function disableSwitch() {
//                if (document.getElementById("memberNum").value > 0) {
//                    document.getElementById("provNum").disabled = true;
//                    document.getElementById("provNum").value = "";
//                    document.getElementById("provNum_error").value = "";
////                    document.getElementById('btnChange').disabled = false;
//                }else{
//                    document.getElementById("provNum").disabled = false;
//                }
//                if (document.getElementById("provNum").value > 0) {
//                    document.getElementById("memberNum").disabled = true;
//                    document.getElementById("memberNum").value = "";
//                    document.getElementById("memberNum_error").value = "";
////                    document.getElementById('btnChange').disabled = false;
//                }else{
//                    document.getElementById("memberNum").disabled = false;
//                }
////                if(document.getElementById("workflowSubType").value == "" && document.getElementById("workflowSubType").value == "0" && document.getElementById("workflowSubType").value == "99" && document.getElementById("workflowSubType").value == null){
////                    document.getElementById("btnChange").disabled = true;
////                }
//            }
        </script>
    </head>
    <body>
        <div align="Center">
            <agiletags:WorkflowTabs wftSelectedIndex="link_WfMyWorkbench"/>
            <label class="header">My Workbench</label>
            <br/><br/>
            <fieldset id="pageBorder" style="padding-left: 0; padding-right: 0; padding-top:0; padding-bottom: 0;  margin-left: 0; overflow: auto; height: calc(100vh - 110px);">
                <div id="myWorkbench_Conent" style="height:100%; position: relative" >
                    <div id="neoOptionPanelSelect_Content">
                        <br/>
                        <c:forEach var="nPanel" items="${wfPanelList}" varStatus="loop">
                            <c:if test="${nPanel.statusID == 1}">
                                <nt:NeoPanel title="${nPanel.refNum} - ${nPanel.sourceName}<br/>${nPanel.subject}<br/>Category: ${category}" command="WorkflowPopulateOptionPanelCommand" action="Workflow/WorkflowOptPnlContent.jsp" actionId="${nPanel.itemID}" collapsed="true" reload="true" optionList="${arrOptionList}" panelColor="#7F7F7F"/>
                            </c:if>
                            <c:if test="${nPanel.statusID == 2}">
                                <nt:NeoPanel title="${nPanel.refNum} - ${nPanel.sourceName}<br/>${nPanel.subject}<br/>Category: ${category}" command="WorkflowPopulateOptionPanelCommand" action="Workflow/WorkflowOptPnlContent.jsp" actionId="${nPanel.itemID}" collapsed="true" reload="true" optionList="${arrOptionList}" panelColor="#A349A4"/>
                            </c:if>
                            <c:if test="${nPanel.statusID == 3}">
                                <nt:NeoPanel title="${nPanel.refNum} - ${nPanel.sourceName}<br/>${nPanel.subject}<br/>Category: ${category}" command="WorkflowPopulateOptionPanelCommand" action="Workflow/WorkflowOptPnlContent.jsp" actionId="${nPanel.itemID}" collapsed="true" reload="true" optionList="${arrOptionList}" panelColor="#E17F27"/>
                            </c:if>
                            <c:if test="${nPanel.statusID == 4}">
                                <nt:NeoPanel title="${nPanel.refNum} - ${nPanel.sourceName}<br/>${nPanel.subject}<br/>Category: ${category}" command="WorkflowPopulateOptionPanelCommand" action="Workflow/WorkflowOptPnlContent.jsp" actionId="${nPanel.itemID}" collapsed="true" reload="true" optionList="${arrOptionList}" panelColor="#FFF200"/>
                            </c:if>
                            <c:if test="${nPanel.statusID == 5}">
                                <nt:NeoPanel title="${nPanel.refNum} - ${nPanel.sourceName}<br/>${nPanel.subject}<br/>Category: ${category}" command="WorkflowPopulateOptionPanelCommand" action="Workflow/WorkflowOptPnlContent.jsp" actionId="${nPanel.itemID}" collapsed="true" reload="true" optionList="${arrOptionList}" panelColor="#3F48CC"/>
                            </c:if>
                            <c:if test="${nPanel.statusID == 6}">
                                <nt:NeoPanel title="${nPanel.refNum} - ${nPanel.sourceName}<br/>${nPanel.subject}<br/>Category: ${category}" command="WorkflowPopulateOptionPanelCommand" action="Workflow/WorkflowOptPnlContent.jsp" actionId="${nPanel.itemID}" collapsed="true" reload="true" optionList="${arrOptionList}" panelColor="#FF1111"/>
                            </c:if>
                        </c:forEach>
                        <br/>
                    </div>
                </div>
            </fieldset>
        </div>
        <agiletags:ControllerForm name="myWorkbenchContent" action="/ManagedCare/AgileController">
            <input type="hidden" name="opperation" id="opperation" value="WorkflowContentCommand" size="30" />
            <input type="hidden" name="onScreen" id="onScreen" value="Workflow/WorkflowMyWorkbench.jsp" size="30" />
        </agiletags:ControllerForm>
    </body>
</html>

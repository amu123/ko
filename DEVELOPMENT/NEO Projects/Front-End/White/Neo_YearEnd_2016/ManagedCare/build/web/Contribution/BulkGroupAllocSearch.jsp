<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script >
            function setEmpVals(entityId, gName, gNumber, amount) {
                document.getElementById('RecieptsSearchCommand').value = 'SelectEmployer';
                document.getElementById('entityId').value = entityId;
                document.getElementById('amount').value = amount;                
                document.getElementById('groupName').value = gName;                
                document.getElementById('groupNumber').value = gNumber;                
            }
            
            function validateAndSearchEmployer(frm, btn) {
                document.getElementById('RecieptsSearchCommand').value = 'Search';
                var isValid = false;
                
                if ($('#employerNumber').val().length > 0 || $('#employerName').val().length > 0 ) {
                    isValid = true;
                } 
                if (isValid) {
                    $('#btn_EmpSearch_error').text("");
                    submitFormWithAjaxPost(frm, 'UnallocatedtListDiv', btn);
                } else {
                    $('#btn_EmpSearch_error').text("Please enter some search criteria");
                }
            }
            
        </script>
    </head>
    <body>
     <agiletags:ControllerForm name="ReciptsSearchtForm">
        <input type="hidden" name="opperation" id="opperation" value="ContributionBulkAllocSearchCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="ReceiptsSearch" />
        <input type="hidden" name="command" id="RecieptsSearchCommand" value="" />
        <input type="hidden" name="entityId" id="entityId" value="" />
        <input type="hidden" name="amount" id="amount" value="" />
        <input type="hidden" name="groupName" id="groupName" value="" />
        <input type="hidden" name="groupNumber" id="groupNumber" value="" />
        <label class="header">Bulk Allocation Group Search</label>
        <hr>
        <br>
        <table id="EmployerTable">
            <tr><agiletags:LabelTextBoxError displayName="Group Number" elementName="employerNumber"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Group Name" elementName="employerName"/></tr>
            <tr><td colspan="2" align="right"><input type="button" value="Search" onclick="validateAndSearchEmployer(this.form, this);"></td>
                <td></td>
                <td></td>
                <td><label id="btn_EmpSearch_error" class="error"></label></td>
            </tr>
        </table>

    <div id="UnallocatedtListDiv"></div>
     </agiletags:ControllerForm>
    </body>
</html>

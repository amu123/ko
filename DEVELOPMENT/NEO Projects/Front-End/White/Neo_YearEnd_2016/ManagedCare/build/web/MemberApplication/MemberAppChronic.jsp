
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<agiletags:ControllerForm name="MemberAppChronicForm">
    <input type="hidden" name="opperation" id="opperation" value="MemberApplicationChronicCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberApplicationHospital" />
    <input type="hidden" name="memberAppNumber" id="MemberAppChronic_application_number" value="${memberAppNumber}" />
    <input type="hidden" name="memberAppChronicId" id="memberAppChronicId" value="" />
    <input type="hidden" name="buttonPressed" id="MemberAppChronic_button_pressed" value="" />
    <input type="hidden" name="memberAppType" id="memberAppChronicType" value="${requestScope.memberAppType}" />

    <label class="header">Chronic Medication</label>
    <hr>
    <br>
    <c:if test="${persist_user_viewMemApps == false}">
        <input type="button" name="AddHospitalButton" value="Add Details" onclick="document.getElementById('MemberAppChronic_button_pressed').value = this.value; getPageContents(this.form,null,'main_div','overlay');"/>
        <br>    
    </c:if>

    <table width="90%" id="MemberAppChronicTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th>Dependant</th>
            <th>Prescribed Medication</th>
            <th>Medical Condition</th>
            <th>Date</th>
            <th></th>
            <th></th>
        </tr>

        <c:forEach var="entry" items="${MemberAppChronicDetails}">
            <tr>
                <td><label>${DepMap[entry.dependentNumber]}</label></td>
                <td><label>${entry.medication}</label></td>
                <td><label>${entry.condition}</label></td>
                <td><label>${entry.startDateAsString}</label></td>
                <td><input type="button" value="Edit" name="EditButton" onclick="document.getElementById('memberAppChronicId').value=${entry.chronicDetailId};document.getElementById('MemberAppChronic_button_pressed').value='EditButton';getPageContents(this.form,null,'main_div','overlay');"></td>
                <td><input type="button" value="Remove" name="RemoveButton" onclick="document.getElementById('memberAppChronicId').value=${entry.chronicDetailId};document.getElementById('MemberAppChronic_button_pressed').value='RemoveButton';submitFormWithAjaxPost(this.form, 'ChronicMedication', this);"></td>
            </tr>
        </c:forEach>
    </table>
</agiletags:ControllerForm>
</body>
</html>
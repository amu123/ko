<%-- 
    Document   : Login
    Created on : 2009/09/28, 11:31:08
    Author     : gerritj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agile Login</title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>

        <script type='text/javascript' src="./resources/jQuery/jquery-1.4.2.js"></script>

        <script type='text/javascript' >
            $(function () {
                var cr = $("#changeRequired").val();
                if (cr === null || cr === "null" || cr === "") {
                    $("#trNewPassword").hide();
                    $("#trConfirmPassword").hide();
                    $("#trSubmitPasswordChange").hide();
                    $("#trSubmitLogin").show();
                    $("#userName").val($("#user").val());
                    $("#password").val($("#pass").val());
                } else {
                    $("#trNewPassword").show();
                    $("#trConfirmPassword").show();
                    $("#trSubmitPasswordChange").show();
                    $("#trSubmitLogin").hide();
                    $("#userName").val($("#user").val());
                    $("#password").val($("#pass").val());
                    $("#newPass").focus();
                }

                $("#loginForm").keypress(function (e) {
                    var code = e.which;
                    if (code === 13) {
                        if (cr === null || cr === "null" || cr === "") {
                            submitWithAction('LoginCommand');
                        } else {
                            validatePasswordChange('LoginCommand');
                        }

                        return true;
                    }
                });

            });

            function validatePasswordChange(command) {
                var oldP = $("#password").val();
                var newP = $("#newPass").val();
                var confirmP = $("#confirmPassword").val();
                var oldError = "#oldPassword_error";

                $("#confirmPassword_error").text("");
                $("#newPass_error").text("");

                if (oldP.length > 0)
                {
                    $(oldError).text("");
                    //check if retype same
                    var error = false;
                    if (newP === confirmP) {
                    } else {
                        $("#confirmPassword_error").text("Password Mismatch");
                        error = true;
                    }

                    if (newP.length < 8) {
                        $("#newPass_error").text("Password has to be more than 8 characters");
                        error = true;
                    }
                    if (confirmP.length < 8) {
                        $("#confirmPassword_error").text("Password has to be more than 8 characters");
                        error = true;
                    }
                    if (error === false) {
                        submitWithAction(command);
                    }

                } else {
                    $("#password_error").text("The old password is mandatory");
                }
            }
            ;

            function submitWithAction(action) {
                var user = $("#userName").val();
                var pass = $("#password").val();
                var newPass = $("#newPass").val();
                $("#newPassword").val(newPass);
                $("#user").val(user);
                $("#pass").val(pass);
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            ;
        </script> 
    </head>
    <body>
        <table width=100% height=100%>
            <tr valign="center">
                <td style="float:left;margin-left:31%">

                    <label class="header" style="margin-left:45%">Login</label>
                    <br/><br/>
                    <table cellpadding="4" cellspacing="1">
                        <form id="loginForm" name="loginForm" action="${pageContext.request.contextPath}/AgileController" method="POST">
                            <input type="hidden" name="opperation" id="opperation" value=""/>
                            <input type="hidden" name="changeRequired" id="changeRequired" value="${requestScope.changeRequired}"/>
                            <input type="hidden" name="user" id="user" value="${requestScope.user}"/>
                            <input type="hidden" name="pass" id="pass" value="${requestScope.pass}"/>
                            <input type="hidden" name="newPassword" id="newPassword" value="${requestScope.newPassword}"/>

                            <!--<input type="hidden" name="opperation" value="LoginCommand" size="30" />-->

                            <tr><td align="right"><label>Username: </label></td><td style="padding-right: 0px; margin-right:1px"><input type="text" name="userName" value="" size="25" id="userName" /></td><td><label class="error" id="userName_error" style=" position: fixed"></label></td></tr>
                            <tr><td align="right"><label>Password: </label></td><td style="padding-right: 0px; margin-right:1px"><input type="password" name="password" value="" size="26" id="password" /></td><td><label class="error" id="password_error" style=" position: fixed"></label></td></tr>
                            <tr id="trNewPassword" style="display: none"><td align="right"><label>New Password: </label></td><td style="padding-right: 0px; margin-right:1px"><input type="password" name="newPass" value="" size="26" id="newPass" /></td><td><label class="error" id="newPass_error" style=" position: fixed"></label></td></tr>
                            <tr id="trConfirmPassword" style="display: none"><td align="right"><label>Confirm Password: </label></td><td style="padding-right: 0px; margin-right:1px"><input type="password" name="confirmPassword" value="" size="26" id="confirmPassword" /></td><td><label class="error" id="confirmPassword_error" style=" position: fixed"></label></td></tr>
                            <tr id="trSubmitLogin"><td colspan="2" align="right"><agiletags:ErrorTag/>&nbsp;&nbsp;&nbsp;<button name="opperation" type="button" value="LoginCommand" onClick="submitWithAction('LoginCommand');">Submit</button></td></tr>
                            <tr id="trSubmitPasswordChange" style="display: none"><td colspan="2" align="right"><agiletags:ErrorTag/>&nbsp;&nbsp;&nbsp;<button name="validatePass" type="button" value="validatePass" onClick="validatePasswordChange('LoginCommand');">Submit</button></td></tr>                                
                        </form>
                        <tr><td align="right" colspan="1"><a href="./SystemAdmin/AddUser.jsp"><p>Register</p></a></td><td align="right"><a href="./Security/ForgotPassword.jsp"><p>Forgot your password?</p></a></td></tr>
                    </table>
                </td></tr></table>
    </body>
</html>

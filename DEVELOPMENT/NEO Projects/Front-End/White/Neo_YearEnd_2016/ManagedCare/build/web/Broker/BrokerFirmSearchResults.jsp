<%-- 
    Document   : BrokerFirmSearchResults
    Created on : 2012/06/20, 01:22:10
    Author     : princes
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Broker Firm Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty BrokerFirmResults}">
            <span>No Broker Firms found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Firm Code</th>
                        <th>Firm Name</th>
                        <th>Alternative Code</th>
                        <th>Vat Registration No</th>
                        <th>Contract Start Date</th>
                        <th>Contract end Date</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${BrokerFirmResults}">
                        <tr>
                            <td><label class="label">${entry['firmCode']}</label></td>
                            <td><label class="label">${entry['firmName']}</label></td>
                            <td><label class="label">${entry['altCode']}</label></td>
                            <td><label class="label">${entry['vatRegNo']}</label></td>
                            <td><label class="label">${entry['startDade']}</label></td>
                            <td><label class="label">${entry['endDate']}</label></td>
                            <td><button type="submit" value="${entry['firmCode']}" onclick="setMemberAppValues(this.value,'${agiletags:escapeStr(entry['firmName'])}','${entry['firmEntityId']}','${agiletags:escapeStr(entry['firstName'])}','${entry['title']}','${agiletags:escapeStr(entry['lastName'])}','${entry['region']}');">View</button></td>
                        </tr>
                    </c:forEach>

                </table>
            </c:otherwise>
        </c:choose>

<%-- 
    Document   : NetworkIndicatorHistoryGridTag
    Created on : 15 Sep 2015, 3:39:09 PM
    Author     : gerritr
--%>

<%@tag description="For Member Detail - BiometricsView.jsp" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>   

<c:if test="${!empty sessionScope.Show_NetworkIndicatorHistoryGridTag && sessionScope.Show_NetworkIndicatorHistoryGridTag == 'true'}">
    <table width="50%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th align="center" colspan="3">History of Network</th>
        </tr>
        <tr>  
            <th align="center" colspan="3">Scheme: <b><c:out value="${sessionScope.selectedProductName} (${sessionScope.selectedNetworkDescription})"/></b></th>
        </tr>
        <tr>
            <td height="10" colspan="3"></td>
        </tr>
        <tr>
            <th align="left">Effective Start Date</th>
            <th align="left">Termination Date</th>
            <th align="left">Process Date</th>
        </tr>

        <c:forEach var="nh" items="${sessionScope.networkHistory}">
            <c:if test="${(nh.productName eq sessionScope.selectedProductName) && (nh.networkDescription eq sessionScope.selectedNetworkDescription)}">
                <tr class="label" height="25">
                    <td><label class="label">${fn:replace(fn:substring(nh.effectiveStartDate, 0, 10), '-', '/')}</label></td>
                    <c:if test="${fn:replace(fn:substring(nh.terminationDate, 0, 10), '-', '/') eq '2999/12/31'}">
                        <td><label class="label">&nbsp;</label></td>
                    </c:if>
                    <c:if test="${!(fn:replace(fn:substring(nh.terminationDate, 0, 10), '-', '/') eq '2999/12/31')}">
                        <td><label class="label">${fn:replace(fn:substring(nh.terminationDate, 0, 10), '-', '/')}</label></td>
                    </c:if>
                    <td><label class="label">${fn:replace(fn:substring(nh.processDate, 0, 10), '-', '/')}</label></td>
                </tr>
            </c:if>
        </c:forEach>
    </table>
</c:if>
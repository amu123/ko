<%-- 
    Document   : CallWorkbenchHistory
    Created on : 2010/03/22, 06:59:57
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="neo.manager.CallTrackDetails"%>
<%@page import="neo.manager.CallWorkbenchDetails"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Collection"%>
<%@page import="java.io.PrintWriter"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script language="JavaScript">

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <%
            //session = request.getSession();
            String callType = "";
            String userName = "";
            String date = "";
            CallTrackDetails ct = (CallTrackDetails) session.getAttribute("WorkbenchCallTrackDetail");
            callType = ct.getCallType() + " History Details";
            userName = ct.getResoUserName();
            date = ct.getCallStartDate().toString();
        %>


        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header"><%=callType%></label>
                    </br></br>
                    <%
                        if (ct.getCallType().equals("Provider")) {

                    %>
            <tr>
                <td>
                    <label class="label"> Provider Number: </label>
                </td>
                <td>
                    <% ct.getProviderNumber(); %>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label"> Provider Name & Surname: </label>
                </td>
                <td>
                    <% ct.getProviderNameSurname(); %>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label"> Discipline: </label>
                </td>
                <td>
                    <% ct.getProviderDiscipline(); %>
                </td>
                <td>
                    <label class="label"> CoverNumber: </label>
                </td>
                <td>
                    <% ct.getCoverNumber(); %>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label"> Caller Name: </label>
                </td>
                <td>
                    <% ct.getCallerName(); %>
                </td>
                <td>
                    <label class="label"> Contact Details: </label>
                </td>
                <td>
                    <% ct.getContactDetails(); %>
                </td>
            </tr>

            <%

            } else if (ct.getCallType().equals("Member")) {

            %>
            <tr>
                <td>
                    <label class="label"> Provider Number: </label>
                </td>
                <td>
                    <% ct.getProviderNumber(); %>
                </td>
                <td>
                    <label class="label"> CoverNumber: </label>
                </td>
                <td>
                    <% ct.getCoverNumber(); %>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label"> Caller Name: </label>
                </td>
                <td>
                    <% ct.getCallerName(); %>
                </td>
                <td>
                    <label class="label"> Contact Details: </label>
                </td>
                <td>
                    <% ct.getContactDetails(); %>
                </td>
            </tr>

            <%
                }
                Collection<CallWorkbenchDetails> cwList = (Collection<CallWorkbenchDetails>) session.getAttribute("UserCallWorkbench");

            %>
            <table width="600" class="list" style="border-style:none" border-width:1px>
                <tr><th>Nr</th><th>Call Number</th><th>Call Status</th><th>Queried</th><th>Previously serviced by</th><th>Call Date</th></tr>

                <%                        out.println("");
                    for (CallWorkbenchDetails cw : cwList) {
                %>
                <tr>
                    <td><label class="label"><%=cw.getCallTrackId()%></label><input type="hidden" name="trackId" value="<%=cw.getCallTrackId()%>"/></td>
                    <td><label class="label"><%=cw.getCallRefNo()%></label></td>
                    <td><label class="label"><%=cw.getCallStatus()%></label></td>
                    <td><label class="label"><%=cw.getCallQueries()%></label></td>
                    <td><label class="label"><%=cw.getResoUserName()%></label></td>
                    <td><label class="label"><%=cw.getCallLastAttendedDate()%></label></td>
                    <td><agiletags:ButtonOpperation type="submit" align="left" span="3" commandName="UpdateCallTrackingCommand" displayname="Update"/></td>
                </tr>
                <%
                    }
                %>
            </table>
            <tr>
                <td><agiletags:ButtonOpperation type="submit" align="left" span="3" commandName="CreateNewCallCommand" displayname="New Call"/></td>
        <td><agiletags:ButtonOpperation type="submit" align="left" span="3" commandName="ReloadCallCommand" displayname="Cancel"/></td>
</tr>
<tr align="right"><td><label class="label">User</label></td><td><%=userName%></td></tr>
<tr align="right"><td><label class="label">Call Date</label></td><td><%=date%></td></tr>
<!-- content ends here -->
</td></tr></table>
</body>
</html>

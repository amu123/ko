<%-- 
    Document   : SelectNewChronicCondition
    Created on : May 26, 2016, 2:14:51 PM
    Author     : nick
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.scrollTo-1.4.2-min.js"></script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
        <title>Select New Chronic Condition</title>      
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>
            function submitCarePathsWithAction(cmd) {
                var cdlList = [];

                var cpList = document.getElementsByName("cpCheck");

                for (var i = 0; i < cpList.length; i++) {
                    if (cpList[i].checked) {
                        cdlList.push(cpList[i].value);
                    }
                }

                document.getElementById('opperation').value = cmd;
                document.getElementById('cpList').value = cdlList;
                document.forms[0].submit();
            }
            ;
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="10px"></td><td align="left">
                    <!-- content goes here -->     
            <c:choose>
                <c:when test="${applicationScope.Client == 'Sechaba'}">
                    <label class="header">PCC - Select New Chronic Condition</label>
                </c:when>    
                <c:otherwise>
                    <label class="header">PDC - Select New Chronic Condition</label>
                </c:otherwise>
            </c:choose>
            <br/><br/>

            <agiletags:ControllerForm name="selectCondition">
                <input type="hidden" name="opperation" id="opperation" value="" />
                <input type="hidden" name="listIndex" id="listIndex" value="" />
                <input type="hidden" name="opperation" id="opperation" value="" />
                <input type="hidden" name="pageDirection" id="pageDirection" value="" />
                <input type="hidden" name="refreshScreen" id="refreshScreen" value="" />   
                <input type="hidden" name="pdcUserYesNo" id="pdcUserYesNo" value="${sessionScope.persist_user_pdcUser}" />
                <input type="hidden" name="pdcAdminYesNo" id="pdcAdminYesNo" value="${sessionScope.persist_user_pdcAdmin}" />
                <input type="hidden" name="cpList" id="cpList" value="" />
                <c:choose>
                    <c:when test="${!empty sessionScope.cdlCarepathListPDC}">
                        <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                            <agiletags:PDCDiseaseList/>
                        </table>  
                    </c:when>
                    <c:otherwise>
                        <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                            <tr><td><label style="color: red">No active Care Paths found...</label></td></tr>
                        </table>  
                    </c:otherwise>
                </c:choose>
            </agiletags:ControllerForm>                   
            <br/>
        </td></tr></table>
</body>
</html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <label class="header">Group Search</label>
     <agiletags:ControllerForm name="GroupSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="ContributionBankStatementCommand" />
        <input type="hidden" name="command" value="EmployerSearch" />
        <input type="hidden" name="bsStatementId" value="${bsStatementId}" />
        <input type="hidden" name="bsLineId" value="${bsLineId}" />
        <input type="hidden" name="target_div" value="${target_div}" />
        <input type="hidden" name="main_div" value="${main_div}" />
        <input type="hidden" name="entityForm" value="${entityForm}" />
        <table>
            <tr><agiletags:LabelProductDropDown displayName="Scheme" elementName="scheme" mandatory="yes"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Group Number" elementName="groupNumber"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Group Name" elementName="groupName"/></tr>
            <tr>
                <td><input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
                <td align="right"><input type="button" value="Search" onclick="validateAndSearchGroup(this.form, 'EmployerSearchResults');"></td>
            </tr>
        </table>
      
      <div style ="display: none"  id="EmployerSearchResults"></div>
      </agiletags:ControllerForm>

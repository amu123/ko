<%-- 
    Document   : BankingDetails
    Created on : 2012/06/18, 11:22:40
    Author     : princes
--%>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="MemberApplicationForm">
    <input type="hidden" name="opperation" id="MemberApp_opperation" value="SaveBrokerFirmBankingDetails" />
    <input type="hidden" name="onScreen" id="MemberApp_onScreen" value="Member Application" />
    <input type="hidden" name="brokerFirmEntityId" id="brokerFirmBankingEntityId" value="${requestScope.brokerFirmEntityId}" />
    <input type="hidden" name="brokerFirmTile" id="brokerFirmDetailsTile" value="${requestScope.brokerFirmTile}" />
    <input type="hidden" name="brokerFirmContactPerson" id="brokerFirmContactBankingPerson" value="${requestScope.brokerFirmContactPerson}" />
    <input type="hidden" name="brokerFirmContactSurname" id="brokerFirmContactBankingSurname" value="${requestScope.brokerFirmContactSurname}" />
    <input type="hidden" name="firmRegion" id="firmRegionBanking" value="${requestScope.firmRegion}" />


    <agiletags:HiddenField elementName="MemberApp_app_number"/>

    <label class="subheader">Banking Details</label>
    <hr>
      <table id="BrokerFirmBankingTable">        
        <tr id="FirmBankApp_bankRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Name of Bank"  elementName="FirmBankApp_bank" lookupId="bank" mandatory="yes" errorValueFromSession="yes" javaScript="onChange=\"toggleBranch(this.value, 'FirmBankApp_branch')\""/>        </tr>
        <tr id="FirmBankApp_branchRow"><agiletags:LabelNeoBranchDropDownErrorReq displayName="Branch"  elementName="FirmBankApp_branch"  mandatory="yes" errorValueFromSession="yes" parentElement="FirmBankApp_bank"/>         </tr>        
        <tr id="FirmBankApp_accountTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Account Type"  elementName="FirmBankApp_accountType" lookupId="37" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="FirmBankApp_accountHolderRow"><agiletags:LabelTextBoxErrorReq displayName="Account Holder" elementName="FirmBankApp_accountHolder" valueFromRequest="FirmBankApp_accountHolder" mandatory="yes"/>        </tr>
        <tr id="FirmBankApp_accountNoRow"><agiletags:LabelTextBoxErrorReq displayName="Account no." elementName="FirmBankApp_accountNo" valueFromRequest="FirmBankApp_accountNo" mandatory="yes"/>        </tr>        
      </table>

    
    <hr>
    <table id="FirmAppSaveTable">
        <tr>
           <td><input type="reset" value="Reset"></td>
           <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'SaveBrokerFirmBankingDetails')"></td>
        </tr>
    </table>

  </agiletags:ControllerForm>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="MemberApplicationForm">
    <input type="hidden" name="opperation" id="MemberApp_opperation" value="SaveMemberApplicationCommand" />
    <input type="hidden" name="onScreen" id="MemberApp_onScreen" value="Member Application" />
    <input type="hidden" name="brokerEntityId" id="brokerContactEntityId" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="brokerTitle" id="brokerTitle" value="${requestScope.brokerTitle}" />
    <input type="hidden" name="brokerContactPerson" id="brokerCPerson" value="${requestScope.brokerContactPerson}" />
    <input type="hidden" name="brokerContactSurname" id="brokerCSurname" value="${requestScope.brokerContactSurname}" />
    <input type="hidden" name="brokerRegion" id="brokerComRegion" value="${requestScope.brokerRegion}" />

    <agiletags:HiddenField elementName="MemberApp_app_number"/>

    <label class="subheader">Broker Commission Transaction</label>
    <hr>
      <table id="BrokerFirmBankingTable">
        
      </table>

    
    <hr>

  </agiletags:ControllerForm>

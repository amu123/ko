<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="CommisionTrasactionsDiv">
    <br/>
    <label class="subheader">Commission Transactions</label>
    <br/>
    <br/>
    <agiletags:ControllerForm name="CommSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="BrokerFirmCommissionCommand" />
        <input type="hidden" name="command" id="CommSearchCommand" value="CommSearch" />
        <input type="hidden" name="commBrokerFirmEntityId" value="${brokerFirmEntityId}" />
        <input type="hidden" name="buttonPressedExport" id="buttonPressedExport" value="Search" />

        <table>
            <td><label>Start Date</label></td>
            <td><input name="comm_start_date" id="comm_start_date" size="30" value="${comm_start_date}"></td>
            <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('comm_start_date', this);"/></td>
            <td>&nbsp;</td>
            <td><label>End Date</label></td>
            <td><input name="comm_end_date" id="comm_end_date" size="30" value="${comm_end_date}"></td>
            <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('comm_end_date', this);"/></td>
            <td>&nbsp;</td>        
            <td><button type="button" onclick="submitFormWithAjaxPost(this.form, 'CommisionTrasactionsDiv', this);">Search</button></td>
        </table>
    </agiletags:ControllerForm>

    <agiletags:ControllerForm name="BrokerFirmCommissionForm">
        <input type="hidden" name="opperation" value="BrokerFirmCommissionCommand" />
        <input type="hidden" name="onScreen" value="Broker Firm Commission" />
        <input type="hidden" name="commBrokerFirmEntityId" value="${brokerFirmEntityId}" />
        <input type="hidden" name="commBrokerEntityId" id="commBrokerEntityId" value="" />
        <input type="hidden" name="commCheqeRunId" id="commCheqeRunId" value="" />
        <input type="hidden" name="command" id="BrokerFirmCommissionCommand" value="" />

        <hr>
        <c:choose>
            <c:when test="${empty CommissionPayments}">
                <span>No Commission Payments found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Payment Date</th>
                        <th>Scheme</th>
                        <th>Status</th>
                        <th>Narrative</th>
                        <th>Amount</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${CommissionPayments}">

                        <tr>
                            <td><label>${entry.runDate}</label></td>
                            <td><label>${entry.scheme}</label></td>
                            <td><label>${entry.status}</label></td>
                            <td><label>${entry.narrative}</label></td>
                            <td align="right"><label>${agiletags:formatStringAmount(entry.amount)}</label></td>
                            <td><button type="button" onclick="document.getElementById('BrokerFirmCommissionCommand').value = 'viewBrokerSummary';
                    document.getElementById('commCheqeRunId').value = '${entry.detailId}';
                    submitFormWithAjaxPost(this.form, 'BrokerFirmPaymentDetails', this);">View Broker Details</button></td>
                        </tr>
                    </c:forEach>

                </table>
                <div id="BrokerFirmPaymentDetails"></div>
            </c:otherwise>
        </c:choose>

    </agiletags:ControllerForm>

</div>
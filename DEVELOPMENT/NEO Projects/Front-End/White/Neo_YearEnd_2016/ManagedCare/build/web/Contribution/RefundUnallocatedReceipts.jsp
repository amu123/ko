<%-- 
    Document   : RefundUnallocatedReceipts
    Created on : 2013/10/21, 01:53:26
    Author     : yuganp
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<jsp:useBean id="now" class="java.util.Date"/>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script>
            
            function selectReceipt(receipt) {
                var sr = receipt.split("_");
                if (sr[1] == undefined) {
                    $('#unallocatedAmount').text("");
                } else {
                    $('#unallocatedAmount').text(parseFloat(sr[1]).toFixed(2));
                }
            }
            
            function saveDetails(form, btn) {
                btn.disabled = true;
                var valid = validateNeoDateField("refundDate", true, '2013/01/01', "${agiletags:formatDate(now)}");
                valid = valid & validateNeoRequiredField("refundReason");
                valid = valid & validateNeoRequiredField("ContribReceiptsId");
                if (valid) {
                    submitFormWithAjaxPost(form, "refundReceiptsDiv");
                } else {
                    displayNeoErrorNotification("There are validation errors");
                    btn.disabled = false;
                }
            }
            
        </script>
    </head>
    <body>
        <div class="neonotification"></div>
        
     <agiletags:ControllerForm name="RefundUnallocatedReceiptsForm">
        <input type="hidden" name="opperation" id="opperation" value="RefundUnallocatedReceiptsCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="MemberPaymentAllocation" />
        <input type="hidden" name="command" id="BankStatementCommand" value="Save" />
        <input type="hidden" name="entityId" id="entityId" value="${entityId}" />
        <input type="hidden" name="amountUnallocated" id="amountUnallocated" value="" />
    <label class="header">Refund Unallocated Receipt</label>
    <hr>
    <div id="refundReceiptsDiv">
    <table>
        <c:if test="${empty coverNo}">
            <tr>
                <td width="160"><label>Group Number:</label></td>
                <td align="right"><label>${groupNumber}</label></td>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td><label>Group Name:</label></td>
                <td align="right"><label>${groupName}</label></td>
                <td colspan="3">&nbsp;</td>
            </tr>
        </c:if>
        <c:if test="${not empty coverNo}">
            <tr>
                <td width="160"><label>Member Number:</label></td>
                <td><label> ${coverNo}</label></td>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td><label>Member Name:</label></td>
                <td><label> ${memberName} ${memberSurname}</label></td>
                <td colspan="3">&nbsp;</td>
            </tr>
        </c:if>            
        <tr>
            <td><label>Receipt</label></td>
            <td>
                <c:if test="${empty receipts}"><label>Not Receipts Available</label></c:if>
                <c:if test="${not empty receipts}">
                    <select style="width:215px" name="ContribReceiptsId" id ="ContribReceiptsId" onchange="selectReceipt(this.value);">
                        <option value="">Select a receipt</option>
                        <c:forEach var="rec" items="${receipts}">
                            <optgroup label="${agiletags:formatXMLGregorianDate(rec.statementDate)} ${rec.receiptType == 1 ? 'Payment' : 'Unallocated Amount'}">
                            <option value="${rec.contribReceiptsId}_  ${rec.amount}">${rec.transactionDescription} R ${agiletags:formatBigDecimal(rec.amount)}</option>
                            </optgroup>
                        </c:forEach>
                    </select>
                </c:if>
            </td>
            <td></td>
            <td></td>
            <td><label id="ContribReceiptsId_error" class="error"></label></td>
        </tr>
        <tr>
            <td><label>Refund Amount:</label></td>
            <td align="right"><label id="unallocatedAmount"></label></td>
        </tr>
        <tr><agiletags:LabelTextBoxDate displayname="Refund Date" elementName="refundDate" mandatory="yes"/></tr>
        <tr><agiletags:LabelTextBoxError displayName="Refund Reason" elementName="refundReason" mandatory="yes"/></tr>
    </table>
        <br>
                <c:if test="${not empty receipts}">
        <input type="reset">
        <input type="button" value="Save" onclick="saveDetails(this.form, this)">
                </c:if>
    </div>
     </agiletags:ControllerForm>
    </body>
</html>

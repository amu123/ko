<%--
    Document   : Welcome
    Created on : 2010/01/14, 12:16:19
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
   <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
    </head>

    <body>
        <center>
            <table cellspacing="4">
                <tr>
                <c:if test="${applicationScope.Client == 'Sechaba' && applicationScope.Environment == 'UAT'}">
                    <!--<td><label class="header">Under Construction</label></td>-->
                    <td><img src="/ManagedCare/resources/uat_notif.jpg" alt="Welcome"></td>
                    </c:if>  
                    <c:if test="${applicationScope.Client == 'Sechaba' && applicationScope.Environment == 'Production'}">
                    <td><label class="header">Production</label></td>
                    <td><img src="/ManagedCare/resources/production.jpg" alt="Welcome"></td>
                    </c:if>
                <c:if test="${applicationScope.Client == 'Agility'}">
                    <td><label class="header">Welcome</label></td>
                    <td><img src="/ManagedCare/resources/welcome.jpg" alt="Welcome"></td>
                    </c:if>
                </tr>
            </table>
            <p></p>
        </center>
     </body>
</html>


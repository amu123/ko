<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

  <agiletags:ControllerForm name="GroupAndMemberForm">
    <input type="hidden" name="opperation" id="opperation" value="SaveMemberCoverDetailsCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberDependantAppAdd" />
     <input type="hidden" name="buttonPressed" id="disorderValuesButtonPressed" value="" />
    <input type="hidden" name="memberEntityId" id="memberLinkBrokerEntityId" value="${memberCoverEntityId}" />
    <input type="hidden" name="employerEntityId" id="employerEntityId" value="${requestScope.employerEntityId}" />
    <br/>
    <br/>
    <label class="header">Link Member To Group</label>
    <br>
     <table>                        
        <tr><agiletags:LabelTextSearchTextDiv displayName="Group Code" elementName="Disorder_diagCode" mandatory="yes" onClick="document.getElementById('disorderValuesButtonPressed').value = 'SearchGroupButton'; getPageContents(document.forms['GroupAndMemberForm'],null,'overlay','overlay2');"/>        </tr>
        <tr><agiletags:LabelTextDisplay displayName="Group Name" elementName="Disorder_diagName" valueFromSession="no" javaScript="" /></tr>
        <tr id="FirmApp_brokerStartDateRow"><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="GroupApp_StartDate" valueFromSession="yes" mandatory="yes"/>        </tr> 
      </table>
      
    <hr>
    <table id="MemberAppDepSaveTable">
        <tr>
           <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
           <td><input type="button" value="Save" onclick="MemGroupConcession( this,'${main_div}', '${target_div}');"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>





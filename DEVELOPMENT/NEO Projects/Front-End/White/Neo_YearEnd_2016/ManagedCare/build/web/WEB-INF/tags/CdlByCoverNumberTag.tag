<%-- 
    Document   : CdlByCoverNumberTag
    Created on : 12 Feb 2015, 5:38:05 PM
    Author     : dewaldvdb
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ attribute name="javascript" rtexprvalue="true" required="true" %>
<%@attribute name="id" rtexprvalue="true" required="true"%>
<%@ attribute name="displayName" rtexprvalue="true" required="true" %>
<%-- The list of normal or fragment attributes can be specified here: --%>
<c:set var="cdlVal" value="${sessionScope.cdl}"/>
<td class="label" align="left" width="160px">${displayName}:</td>
<td align="left" width="200px">
   <select style="width:215px" name="icdSelect" ${javascript}>
       <option value="0"></option>
       <c:forEach items="${sessionScope.IcdList}" var="entry">
           <c:choose>
               <c:when test="${cdlVal == entry.id}">
                   <option value="${entry.id}" selected>${entry.value}</option>
               </c:when>
               <c:otherwise>
                   <option value="${entry.id}">${entry.value}</option>
               </c:otherwise>
           </c:choose>
       </c:forEach>
   </select>
</td>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="MemberAppNotesForm">
    <input type="hidden" name="opperation" id="EmployeeAppNotes_opperation" value="EmployeeAppNotesCommands" />
    <input type="hidden" name="onScreen" id="EmployeeAppNotes_onScreen" value="EmployeeAppNotes" />
    <input type="hidden" name="employerEntityId" id="employerNotesEntityId" value="${requestScope.employerEntityId}" />
    <input type="hidden" name="noteId" id="EmployeeAppNotes_note_id" value="" />
    <input type="hidden" name="buttonPressed" id="EmployeeAppNotes_button_pressed" value="" />
    <input type="hidden" name="menuResp" id="menuResp" value="${param.menuResp}" />
    <label class="header">Group Application Notes</label>
    <hr>

      <table id="EmployeeAppNotesTable">
          <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Details"  elementName="notesListSelect" lookupId="214" mandatory="no" errorValueFromSession="no" javaScript="onchange=\"document.getElementById('EmployeeAppNotes_button_pressed').value='SelectButton';submitFormWithAjaxPost(this.form, 'EmployeeAppNotesDetails');\"" />        </tr>
      </table>          
 <div style ="display: none"  id="EmployeeAppNotesDetails"></div>
 </agiletags:ControllerForm>
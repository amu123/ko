


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <script type="text/javascript">
 
 
 
 var codesite_token = "125d90c7016dbfd9c828c03b95ed61d0";
 
 
 var logged_in_user_email = "whauger@gmail.com";
 
 </script>
 
 
 <title>date-af-ZA.js - 
 datejs -
 
 Project Hosting on Google Code</title>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
 
 <link type="text/css" rel="stylesheet" href="http://www.gstatic.com/codesite/ph/1676283234471223295/css/ph_core.css">
 
 <link type="text/css" rel="stylesheet" href="http://www.gstatic.com/codesite/ph/1676283234471223295/css/ph_detail.css" >
 
 
 <link type="text/css" rel="stylesheet" href="http://www.gstatic.com/codesite/ph/1676283234471223295/css/d_sb_20080522.css" >
 
 
 
<!--[if IE]>
 <link type="text/css" rel="stylesheet" href="http://www.gstatic.com/codesite/ph/1676283234471223295/css/d_ie.css" >
<![endif]-->
 <style type="text/css">
 .menuIcon.off { background: no-repeat url(http://www.gstatic.com/codesite/ph/images/dropdown_sprite.gif) 0 -42px }
 .menuIcon.on { background: no-repeat url(http://www.gstatic.com/codesite/ph/images/dropdown_sprite.gif) 0 -28px }
 .menuIcon.down { background: no-repeat url(http://www.gstatic.com/codesite/ph/images/dropdown_sprite.gif) 0 0; }
 </style>
</head>
<body class="t4">
 <div id="gaia">
 
 <span>
 
 <b>whauger@gmail.com</b>
 
 
 | <a href="/u/whauger/" id="projects-dropdown" onclick="return false;">My favorites</a><img width="14" height="14" class="menuIcon off" id="menuIcon-projects-dropdown" src="http://www.gstatic.com/codesite/ph/images/cleardot.gif"/>
 
 | <a href="/u/whauger/" onclick="_CS_click('/gb/ph/profile');" title="Profile, Updates, and Settings">Profile</a>
 | <a href="https://www.google.com/accounts/Logout?continue=http%3A%2F%2Fcode.google.com%2Fp%2Fdatejs%2Fsource%2Fbrowse%2Ftrunk%2Fbuild%2Fdate-af-ZA.js" onclick="_CS_click('/gb/ph/signout');">Sign out</a>
 
 </span>

 </div>
 <div class="gbh" style="left: 0pt;"></div>
 <div class="gbh" style="right: 0pt;"></div>
 
 
 <div style="height: 1px"></div>
<!--[if IE 6]>
<div style="text-align:center;">
Support browsers that contribute to open source, try <a href="http://www.firefox.com">Firefox</a> or <a href="http://www.google.com/chrome">Google Chrome</a>.
</div>
<![endif]-->
 <table style="padding:0px; margin: 20px 0px 0px 0px; width:100%" cellpadding="0" cellspacing="0">
 <tr style="height: 58px;">
 
 <td style="width: 55px; text-align:center;">
 <a href="/p/datejs/">
 
 
 
 <img src="/p/datejs/logo?logo_id=1238229949" alt="Logo">
 
 
 </a>
 </td>
 
 <td style="padding-left: 0.8em">
 
 <div id="pname" style="margin: 0px 0px -3px 0px">
 <a href="/p/datejs/" style="text-decoration:none; color:#000">datejs</a>
 </div>
 
 <div id="psum">
 <i><a id="project_summary_link" href="/p/datejs/" style="text-decoration:none; color:#000">A JavaScript Date Library</a></i>
 </div>
 
 </td>
 <td style="white-space:nowrap; text-align:right">
 
 <form action="/hosting/search">
 <input size="30" name="q" value="">
 <input type="submit" name="projectsearch" value="Search projects" >
 </form>
 
 </tr>
 </table>



 
<table id="mt" cellspacing="0" cellpadding="0" width="100%" border="0">
 <tr>
 <th onclick="if (!cancelBubble) _go('/p/datejs/');">
 <div class="tab inactive">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <a onclick="cancelBubble=true;" href="/p/datejs/">Project&nbsp;Home</a>
 </div>
 </div>
 </th><td>&nbsp;&nbsp;</td>
 
 
 
 
 <th onclick="if (!cancelBubble) _go('/p/datejs/downloads/list');">
 <div class="tab inactive">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <a onclick="cancelBubble=true;" href="/p/datejs/downloads/list">Downloads</a>
 </div>
 </div>
 </th><td>&nbsp;&nbsp;</td>
 
 
 
 
 
 <th onclick="if (!cancelBubble) _go('/p/datejs/w/list');">
 <div class="tab inactive">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <a onclick="cancelBubble=true;" href="/p/datejs/w/list">Wiki</a>
 </div>
 </div>
 </th><td>&nbsp;&nbsp;</td>
 
 
 
 
 
 <th onclick="if (!cancelBubble) _go('/p/datejs/issues/list');">
 <div class="tab inactive">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <a onclick="cancelBubble=true;" href="/p/datejs/issues/list">Issues</a>
 </div>
 </div>
 </th><td>&nbsp;&nbsp;</td>
 
 
 
 
 
 <th onclick="if (!cancelBubble) _go('/p/datejs/source/checkout');">
 <div class="tab active">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <a onclick="cancelBubble=true;" href="/p/datejs/source/checkout">Source</a>
 </div>
 </div>
 </th><td>&nbsp;&nbsp;</td>
 
 
 <td width="100%">&nbsp;</td>
 </tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0" class="st">
 <tr>
 
 
 
 
 
 
 <td>
 <div class="st2">
 <div class="isf">
 
 
 
 <span class="inst1"><a href="/p/datejs/source/checkout">Checkout</a></span> |
 <span class="inst2"><a href="/p/datejs/source/browse/">Browse</a></span> |
 <span class="inst3"><a href="/p/datejs/source/list">Changes</a></span> |
 
 <form action="http://www.google.com/codesearch" method="get" style="display:inline"
 onsubmit="document.getElementById('codesearchq').value = document.getElementById('origq').value + ' package:http://datejs\\.googlecode\\.com'">
 <input type="hidden" name="q" id="codesearchq" value="">
 <input maxlength="2048" size="35" id="origq" name="origq" value="" title="Google Code Search" style="font-size:92%">&nbsp;<input type="submit" value="Search Trunk" name="btnG" style="font-size:92%">
 
 
 
 </form>
 </div>
</div>

 </td>
 
 
 
 
 
 <td height="4" align="right" valign="top" class="bevel-right">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 </td>
 </tr>
</table>
<script type="text/javascript">
 var cancelBubble = false;
 function _go(url) { document.location = url; }
</script>


<div id="maincol"
 
>

 
<!-- IE -->



<div class="expand">


<style type="text/css">
 #file_flipper { display: inline; float: right; white-space: nowrap; }
 #file_flipper.hidden { display: none; }
 #file_flipper .pagelink { color: #0000CC; text-decoration: underline; }
 #file_flipper #visiblefiles { padding-left: 0.5em; padding-right: 0.5em; }
</style>
<div id="nav_and_rev" class="heading">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner" id="bub">
 <div class="bub-top">
 <div class="pagination" style="margin-left: 2em">
 <table cellpadding="0" cellspacing="0" class="flipper">
 <tbody>
 <tr>
 
 <td>
 <ul class="leftside">
 
 <li><a href="/p/datejs/source/browse/trunk/build/date-af-ZA.js?r=190" title="Previous">&lsaquo;r190</a></li>
 
 </ul>
 </td>
 
 <td><b>r195</b></td>
 
 </tr>
 </tbody>
 </table>
 </div>
 
 <div class="" style="vertical-align: top">
 <div class="src_crumbs src_nav">
 <strong class="src_nav">Source path:&nbsp;</strong>
 <span id="crumb_root">
 
 <a href="/p/datejs/source/browse/">svn</a>/&nbsp;</span>
 <span id="crumb_links" class="ifClosed"><a href="/p/datejs/source/browse/trunk/">trunk</a><span class="sp">/&nbsp;</span><a href="/p/datejs/source/browse/trunk/build/">build</a><span class="sp">/&nbsp;</span>date-af-ZA.js</span>
 
 
 </div>
 
 </div>
 <div style="clear:both"></div>
 </div>
 </div>
</div>

<style type="text/css">
 
  tr.inline_comment {
 background: #fff;
 vertical-align: top;
 }
 div.draft, div.published {
 padding: .3em;
 border: 1px solid #999; 
 margin-bottom: .1em;
 font-family: arial, sans-serif;
 max-width: 60em;
 }
 div.draft {
 background: #ffa;
 } 
 div.published {
 background: #e5ecf9;
 }
 div.published .body, div.draft .body {
 padding: .5em .1em .1em .1em;
 max-width: 60em;
 white-space: pre-wrap;
 white-space: -moz-pre-wrap;
 white-space: -pre-wrap;
 white-space: -o-pre-wrap;
 word-wrap: break-word;
 }
 div.draft .actions {
 margin-left: 1em;
 font-size: 90%;
 }
 div.draft form {
 padding: .5em .5em .5em 0;
 }
 div.draft textarea, div.published textarea {
 width: 95%;
 height: 10em;
 font-family: arial, sans-serif;
 margin-bottom: .5em;
 }


 
 .nocursor, .nocursor td, .cursor_hidden, .cursor_hidden td {
 background-color: white;
 height: 2px;
 }
 .cursor, .cursor td {
 background-color: darkblue;
 height: 2px;
 display: '';
 }

</style>
<div class="fc">
 


<table class="opened" id="review_comment_area"><tr>
<td id="nums">
<pre><table width="100%"><tr class="nocursor"><td></td></tr></table></pre>

<pre><table width="100%"><tr id="gr_svn191_1"><td id="1"><a href="#1">1</a></td></tr
><tr id="gr_svn191_2"><td id="2"><a href="#2">2</a></td></tr
><tr id="gr_svn191_3"><td id="3"><a href="#3">3</a></td></tr
><tr id="gr_svn191_4"><td id="4"><a href="#4">4</a></td></tr
><tr id="gr_svn191_5"><td id="5"><a href="#5">5</a></td></tr
><tr id="gr_svn191_6"><td id="6"><a href="#6">6</a></td></tr
><tr id="gr_svn191_7"><td id="7"><a href="#7">7</a></td></tr
><tr id="gr_svn191_8"><td id="8"><a href="#8">8</a></td></tr
><tr id="gr_svn191_9"><td id="9"><a href="#9">9</a></td></tr
><tr id="gr_svn191_10"><td id="10"><a href="#10">10</a></td></tr
><tr id="gr_svn191_11"><td id="11"><a href="#11">11</a></td></tr
><tr id="gr_svn191_12"><td id="12"><a href="#12">12</a></td></tr
><tr id="gr_svn191_13"><td id="13"><a href="#13">13</a></td></tr
><tr id="gr_svn191_14"><td id="14"><a href="#14">14</a></td></tr
><tr id="gr_svn191_15"><td id="15"><a href="#15">15</a></td></tr
><tr id="gr_svn191_16"><td id="16"><a href="#16">16</a></td></tr
><tr id="gr_svn191_17"><td id="17"><a href="#17">17</a></td></tr
><tr id="gr_svn191_18"><td id="18"><a href="#18">18</a></td></tr
><tr id="gr_svn191_19"><td id="19"><a href="#19">19</a></td></tr
><tr id="gr_svn191_20"><td id="20"><a href="#20">20</a></td></tr
><tr id="gr_svn191_21"><td id="21"><a href="#21">21</a></td></tr
><tr id="gr_svn191_22"><td id="22"><a href="#22">22</a></td></tr
><tr id="gr_svn191_23"><td id="23"><a href="#23">23</a></td></tr
><tr id="gr_svn191_24"><td id="24"><a href="#24">24</a></td></tr
><tr id="gr_svn191_25"><td id="25"><a href="#25">25</a></td></tr
><tr id="gr_svn191_26"><td id="26"><a href="#26">26</a></td></tr
><tr id="gr_svn191_27"><td id="27"><a href="#27">27</a></td></tr
><tr id="gr_svn191_28"><td id="28"><a href="#28">28</a></td></tr
><tr id="gr_svn191_29"><td id="29"><a href="#29">29</a></td></tr
><tr id="gr_svn191_30"><td id="30"><a href="#30">30</a></td></tr
><tr id="gr_svn191_31"><td id="31"><a href="#31">31</a></td></tr
><tr id="gr_svn191_32"><td id="32"><a href="#32">32</a></td></tr
><tr id="gr_svn191_33"><td id="33"><a href="#33">33</a></td></tr
><tr id="gr_svn191_34"><td id="34"><a href="#34">34</a></td></tr
><tr id="gr_svn191_35"><td id="35"><a href="#35">35</a></td></tr
><tr id="gr_svn191_36"><td id="36"><a href="#36">36</a></td></tr
><tr id="gr_svn191_37"><td id="37"><a href="#37">37</a></td></tr
><tr id="gr_svn191_38"><td id="38"><a href="#38">38</a></td></tr
><tr id="gr_svn191_39"><td id="39"><a href="#39">39</a></td></tr
><tr id="gr_svn191_40"><td id="40"><a href="#40">40</a></td></tr
><tr id="gr_svn191_41"><td id="41"><a href="#41">41</a></td></tr
><tr id="gr_svn191_42"><td id="42"><a href="#42">42</a></td></tr
><tr id="gr_svn191_43"><td id="43"><a href="#43">43</a></td></tr
><tr id="gr_svn191_44"><td id="44"><a href="#44">44</a></td></tr
><tr id="gr_svn191_45"><td id="45"><a href="#45">45</a></td></tr
><tr id="gr_svn191_46"><td id="46"><a href="#46">46</a></td></tr
><tr id="gr_svn191_47"><td id="47"><a href="#47">47</a></td></tr
><tr id="gr_svn191_48"><td id="48"><a href="#48">48</a></td></tr
><tr id="gr_svn191_49"><td id="49"><a href="#49">49</a></td></tr
><tr id="gr_svn191_50"><td id="50"><a href="#50">50</a></td></tr
><tr id="gr_svn191_51"><td id="51"><a href="#51">51</a></td></tr
><tr id="gr_svn191_52"><td id="52"><a href="#52">52</a></td></tr
><tr id="gr_svn191_53"><td id="53"><a href="#53">53</a></td></tr
><tr id="gr_svn191_54"><td id="54"><a href="#54">54</a></td></tr
><tr id="gr_svn191_55"><td id="55"><a href="#55">55</a></td></tr
><tr id="gr_svn191_56"><td id="56"><a href="#56">56</a></td></tr
><tr id="gr_svn191_57"><td id="57"><a href="#57">57</a></td></tr
><tr id="gr_svn191_58"><td id="58"><a href="#58">58</a></td></tr
><tr id="gr_svn191_59"><td id="59"><a href="#59">59</a></td></tr
><tr id="gr_svn191_60"><td id="60"><a href="#60">60</a></td></tr
><tr id="gr_svn191_61"><td id="61"><a href="#61">61</a></td></tr
><tr id="gr_svn191_62"><td id="62"><a href="#62">62</a></td></tr
><tr id="gr_svn191_63"><td id="63"><a href="#63">63</a></td></tr
><tr id="gr_svn191_64"><td id="64"><a href="#64">64</a></td></tr
><tr id="gr_svn191_65"><td id="65"><a href="#65">65</a></td></tr
><tr id="gr_svn191_66"><td id="66"><a href="#66">66</a></td></tr
><tr id="gr_svn191_67"><td id="67"><a href="#67">67</a></td></tr
><tr id="gr_svn191_68"><td id="68"><a href="#68">68</a></td></tr
><tr id="gr_svn191_69"><td id="69"><a href="#69">69</a></td></tr
><tr id="gr_svn191_70"><td id="70"><a href="#70">70</a></td></tr
><tr id="gr_svn191_71"><td id="71"><a href="#71">71</a></td></tr
><tr id="gr_svn191_72"><td id="72"><a href="#72">72</a></td></tr
><tr id="gr_svn191_73"><td id="73"><a href="#73">73</a></td></tr
><tr id="gr_svn191_74"><td id="74"><a href="#74">74</a></td></tr
><tr id="gr_svn191_75"><td id="75"><a href="#75">75</a></td></tr
><tr id="gr_svn191_76"><td id="76"><a href="#76">76</a></td></tr
><tr id="gr_svn191_77"><td id="77"><a href="#77">77</a></td></tr
><tr id="gr_svn191_78"><td id="78"><a href="#78">78</a></td></tr
><tr id="gr_svn191_79"><td id="79"><a href="#79">79</a></td></tr
><tr id="gr_svn191_80"><td id="80"><a href="#80">80</a></td></tr
><tr id="gr_svn191_81"><td id="81"><a href="#81">81</a></td></tr
><tr id="gr_svn191_82"><td id="82"><a href="#82">82</a></td></tr
><tr id="gr_svn191_83"><td id="83"><a href="#83">83</a></td></tr
><tr id="gr_svn191_84"><td id="84"><a href="#84">84</a></td></tr
><tr id="gr_svn191_85"><td id="85"><a href="#85">85</a></td></tr
><tr id="gr_svn191_86"><td id="86"><a href="#86">86</a></td></tr
><tr id="gr_svn191_87"><td id="87"><a href="#87">87</a></td></tr
><tr id="gr_svn191_88"><td id="88"><a href="#88">88</a></td></tr
><tr id="gr_svn191_89"><td id="89"><a href="#89">89</a></td></tr
><tr id="gr_svn191_90"><td id="90"><a href="#90">90</a></td></tr
><tr id="gr_svn191_91"><td id="91"><a href="#91">91</a></td></tr
><tr id="gr_svn191_92"><td id="92"><a href="#92">92</a></td></tr
><tr id="gr_svn191_93"><td id="93"><a href="#93">93</a></td></tr
><tr id="gr_svn191_94"><td id="94"><a href="#94">94</a></td></tr
><tr id="gr_svn191_95"><td id="95"><a href="#95">95</a></td></tr
><tr id="gr_svn191_96"><td id="96"><a href="#96">96</a></td></tr
><tr id="gr_svn191_97"><td id="97"><a href="#97">97</a></td></tr
><tr id="gr_svn191_98"><td id="98"><a href="#98">98</a></td></tr
><tr id="gr_svn191_99"><td id="99"><a href="#99">99</a></td></tr
><tr id="gr_svn191_100"><td id="100"><a href="#100">100</a></td></tr
><tr id="gr_svn191_101"><td id="101"><a href="#101">101</a></td></tr
><tr id="gr_svn191_102"><td id="102"><a href="#102">102</a></td></tr
><tr id="gr_svn191_103"><td id="103"><a href="#103">103</a></td></tr
><tr id="gr_svn191_104"><td id="104"><a href="#104">104</a></td></tr
><tr id="gr_svn191_105"><td id="105"><a href="#105">105</a></td></tr
><tr id="gr_svn191_106"><td id="106"><a href="#106">106</a></td></tr
><tr id="gr_svn191_107"><td id="107"><a href="#107">107</a></td></tr
><tr id="gr_svn191_108"><td id="108"><a href="#108">108</a></td></tr
><tr id="gr_svn191_109"><td id="109"><a href="#109">109</a></td></tr
><tr id="gr_svn191_110"><td id="110"><a href="#110">110</a></td></tr
><tr id="gr_svn191_111"><td id="111"><a href="#111">111</a></td></tr
><tr id="gr_svn191_112"><td id="112"><a href="#112">112</a></td></tr
><tr id="gr_svn191_113"><td id="113"><a href="#113">113</a></td></tr
><tr id="gr_svn191_114"><td id="114"><a href="#114">114</a></td></tr
><tr id="gr_svn191_115"><td id="115"><a href="#115">115</a></td></tr
><tr id="gr_svn191_116"><td id="116"><a href="#116">116</a></td></tr
><tr id="gr_svn191_117"><td id="117"><a href="#117">117</a></td></tr
><tr id="gr_svn191_118"><td id="118"><a href="#118">118</a></td></tr
><tr id="gr_svn191_119"><td id="119"><a href="#119">119</a></td></tr
><tr id="gr_svn191_120"><td id="120"><a href="#120">120</a></td></tr
><tr id="gr_svn191_121"><td id="121"><a href="#121">121</a></td></tr
><tr id="gr_svn191_122"><td id="122"><a href="#122">122</a></td></tr
><tr id="gr_svn191_123"><td id="123"><a href="#123">123</a></td></tr
><tr id="gr_svn191_124"><td id="124"><a href="#124">124</a></td></tr
><tr id="gr_svn191_125"><td id="125"><a href="#125">125</a></td></tr
><tr id="gr_svn191_126"><td id="126"><a href="#126">126</a></td></tr
><tr id="gr_svn191_127"><td id="127"><a href="#127">127</a></td></tr
><tr id="gr_svn191_128"><td id="128"><a href="#128">128</a></td></tr
><tr id="gr_svn191_129"><td id="129"><a href="#129">129</a></td></tr
><tr id="gr_svn191_130"><td id="130"><a href="#130">130</a></td></tr
><tr id="gr_svn191_131"><td id="131"><a href="#131">131</a></td></tr
><tr id="gr_svn191_132"><td id="132"><a href="#132">132</a></td></tr
><tr id="gr_svn191_133"><td id="133"><a href="#133">133</a></td></tr
><tr id="gr_svn191_134"><td id="134"><a href="#134">134</a></td></tr
><tr id="gr_svn191_135"><td id="135"><a href="#135">135</a></td></tr
><tr id="gr_svn191_136"><td id="136"><a href="#136">136</a></td></tr
><tr id="gr_svn191_137"><td id="137"><a href="#137">137</a></td></tr
><tr id="gr_svn191_138"><td id="138"><a href="#138">138</a></td></tr
><tr id="gr_svn191_139"><td id="139"><a href="#139">139</a></td></tr
><tr id="gr_svn191_140"><td id="140"><a href="#140">140</a></td></tr
><tr id="gr_svn191_141"><td id="141"><a href="#141">141</a></td></tr
><tr id="gr_svn191_142"><td id="142"><a href="#142">142</a></td></tr
><tr id="gr_svn191_143"><td id="143"><a href="#143">143</a></td></tr
><tr id="gr_svn191_144"><td id="144"><a href="#144">144</a></td></tr
><tr id="gr_svn191_145"><td id="145"><a href="#145">145</a></td></tr
></table></pre>

<pre><table width="100%"><tr class="nocursor"><td></td></tr></table></pre>
</td>
<td id="lines">
<pre class="prettyprint"><table width="100%"><tr class="cursor_stop cursor_hidden"><td></td></tr></table></pre>

<pre class="prettyprint lang-js"><table><tr
id=sl_svn191_1><td class="source">/**<br></td></tr
><tr
id=sl_svn191_2><td class="source"> * @version: 1.0 Alpha-1<br></td></tr
><tr
id=sl_svn191_3><td class="source"> * @author: Coolite Inc. http://www.coolite.com/<br></td></tr
><tr
id=sl_svn191_4><td class="source"> * @date: 2008-05-13<br></td></tr
><tr
id=sl_svn191_5><td class="source"> * @copyright: Copyright (c) 2006-2008, Coolite Inc. (http://www.coolite.com/). All rights reserved.<br></td></tr
><tr
id=sl_svn191_6><td class="source"> * @license: Licensed under The MIT License. See license.txt and http://www.datejs.com/license/. <br></td></tr
><tr
id=sl_svn191_7><td class="source"> * @website: http://www.datejs.com/<br></td></tr
><tr
id=sl_svn191_8><td class="source"> */<br></td></tr
><tr
id=sl_svn191_9><td class="source">Date.CultureInfo={name:&quot;af-ZA&quot;,englishName:&quot;Afrikaans (South Africa)&quot;,nativeName:&quot;Afrikaans (Suid Afrika)&quot;,dayNames:[&quot;Sondag&quot;,&quot;Maandag&quot;,&quot;Dinsdag&quot;,&quot;Woensdag&quot;,&quot;Donderdag&quot;,&quot;Vrydag&quot;,&quot;Saterdag&quot;],abbreviatedDayNames:[&quot;Son&quot;,&quot;Maan&quot;,&quot;Dins&quot;,&quot;Woen&quot;,&quot;Dond&quot;,&quot;Vry&quot;,&quot;Sat&quot;],shortestDayNames:[&quot;So&quot;,&quot;Ma&quot;,&quot;Di&quot;,&quot;Wo&quot;,&quot;Do&quot;,&quot;Vr&quot;,&quot;Sa&quot;],firstLetterDayNames:[&quot;S&quot;,&quot;M&quot;,&quot;D&quot;,&quot;W&quot;,&quot;D&quot;,&quot;V&quot;,&quot;S&quot;],monthNames:[&quot;Januarie&quot;,&quot;Februarie&quot;,&quot;Maart&quot;,&quot;April&quot;,&quot;Mei&quot;,&quot;Junie&quot;,&quot;Julie&quot;,&quot;Augustus&quot;,&quot;September&quot;,&quot;Oktober&quot;,&quot;November&quot;,&quot;Desember&quot;],abbreviatedMonthNames:[&quot;Jan&quot;,&quot;Feb&quot;,&quot;Mar&quot;,&quot;Apr&quot;,&quot;Mei&quot;,&quot;Jun&quot;,&quot;Jul&quot;,&quot;Aug&quot;,&quot;Sep&quot;,&quot;Okt&quot;,&quot;Nov&quot;,&quot;Des&quot;],amDesignator:&quot;&quot;,pmDesignator:&quot;nm&quot;,firstDayOfWeek:0,twoDigitYearMax:2029,dateElementOrder:&quot;ymd&quot;,formatPatterns:{shortDate:&quot;yyyy/MM/dd&quot;,longDate:&quot;dd MMMM yyyy&quot;,shortTime:&quot;hh:mm tt&quot;,longTime:&quot;hh:mm:ss tt&quot;,fullDateTime:&quot;dd MMMM yyyy hh:mm:ss tt&quot;,sortableDateTime:&quot;yyyy-MM-ddTHH:mm:ss&quot;,universalSortableDateTime:&quot;yyyy-MM-dd HH:mm:ssZ&quot;,rfc1123:&quot;ddd, dd MMM yyyy HH:mm:ss GMT&quot;,monthDay:&quot;dd MMMM&quot;,yearMonth:&quot;MMMM yyyy&quot;},regexPatterns:{jan:/^jan(uarie)?/i,feb:/^feb(ruarie)?/i,mar:/^maart/i,apr:/^apr(il)?/i,may:/^mei/i,jun:/^jun(ie)?/i,jul:/^jul(ie)?/i,aug:/^aug(ustus)?/i,sep:/^sep(t(ember)?)?/i,oct:/^okt(ober)?/i,nov:/^nov(ember)?/i,dec:/^des(ember)?/i,sun:/^so(n(dag)?)?/i,mon:/^ma(an(dag)?)?/i,tue:/^di(ns(dag)?)?/i,wed:/^wo(en(sdag)?)?/i,thu:/^do(nd(erdag)?)?/i,fri:/^vr(y(dag)?)?/i,sat:/^sa(t(erdag)?)?/i,future:/^next/i,past:/^last|past|prev(ious)?/i,add:/^(\+|aft(er)?|from|hence)/i,subtract:/^(\-|bef(ore)?|ago)/i,yesterday:/^yes(terday)?/i,today:/^t(od(ay)?)?/i,tomorrow:/^tom(orrow)?/i,now:/^n(ow)?/i,millisecond:/^ms|milli(second)?s?/i,second:/^sec(ond)?s?/i,minute:/^mn|min(ute)?s?/i,hour:/^h(our)?s?/i,week:/^w(eek)?s?/i,month:/^m(onth)?s?/i,day:/^d(ay)?s?/i,year:/^y(ear)?s?/i,shortMeridian:/^(a|p)/i,longMeridian:/^(a\.?m?\.?|p\.?m?\.?)/i,timezone:/^((e(s|d)t|c(s|d)t|m(s|d)t|p(s|d)t)|((gmt)?\s*(\+|\-)\s*\d\d\d\d?)|gmt|utc)/i,ordinalSuffix:/^\s*(st|nd|rd|th)/i,timeContext:/^\s*(\:|a(?!u|p)|p)/i},timezones:[{name:&quot;UTC&quot;,offset:&quot;-000&quot;},{name:&quot;GMT&quot;,offset:&quot;-000&quot;},{name:&quot;EST&quot;,offset:&quot;-0500&quot;},{name:&quot;EDT&quot;,offset:&quot;-0400&quot;},{name:&quot;CST&quot;,offset:&quot;-0600&quot;},{name:&quot;CDT&quot;,offset:&quot;-0500&quot;},{name:&quot;MST&quot;,offset:&quot;-0700&quot;},{name:&quot;MDT&quot;,offset:&quot;-0600&quot;},{name:&quot;PST&quot;,offset:&quot;-0800&quot;},{name:&quot;PDT&quot;,offset:&quot;-0700&quot;}]};<br></td></tr
><tr
id=sl_svn191_10><td class="source">(function(){var $D=Date,$P=$D.prototype,$C=$D.CultureInfo,p=function(s,l){if(!l){l=2;}<br></td></tr
><tr
id=sl_svn191_11><td class="source">return(&quot;000&quot;+s).slice(l*-1);};$P.clearTime=function(){this.setHours(0);this.setMinutes(0);this.setSeconds(0);this.setMilliseconds(0);return this;};$P.setTimeToNow=function(){var n=new Date();this.setHours(n.getHours());this.setMinutes(n.getMinutes());this.setSeconds(n.getSeconds());this.setMilliseconds(n.getMilliseconds());return this;};$D.today=function(){return new Date().clearTime();};$D.compare=function(date1,date2){if(isNaN(date1)||isNaN(date2)){throw new Error(date1+&quot; - &quot;+date2);}else if(date1 instanceof Date&amp;&amp;date2 instanceof Date){return(date1&lt;date2)?-1:(date1&gt;date2)?1:0;}else{throw new TypeError(date1+&quot; - &quot;+date2);}};$D.equals=function(date1,date2){return(date1.compareTo(date2)===0);};$D.getDayNumberFromName=function(name){var n=$C.dayNames,m=$C.abbreviatedDayNames,o=$C.shortestDayNames,s=name.toLowerCase();for(var i=0;i&lt;n.length;i++){if(n[i].toLowerCase()==s||m[i].toLowerCase()==s||o[i].toLowerCase()==s){return i;}}<br></td></tr
><tr
id=sl_svn191_12><td class="source">return-1;};$D.getMonthNumberFromName=function(name){var n=$C.monthNames,m=$C.abbreviatedMonthNames,s=name.toLowerCase();for(var i=0;i&lt;n.length;i++){if(n[i].toLowerCase()==s||m[i].toLowerCase()==s){return i;}}<br></td></tr
><tr
id=sl_svn191_13><td class="source">return-1;};$D.isLeapYear=function(year){return((year%4===0&amp;&amp;year%100!==0)||year%400===0);};$D.getDaysInMonth=function(year,month){return[31,($D.isLeapYear(year)?29:28),31,30,31,30,31,31,30,31,30,31][month];};$D.getTimezoneAbbreviation=function(offset){var z=$C.timezones,p;for(var i=0;i&lt;z.length;i++){if(z[i].offset===offset){return z[i].name;}}<br></td></tr
><tr
id=sl_svn191_14><td class="source">return null;};$D.getTimezoneOffset=function(name){var z=$C.timezones,p;for(var i=0;i&lt;z.length;i++){if(z[i].name===name.toUpperCase()){return z[i].offset;}}<br></td></tr
><tr
id=sl_svn191_15><td class="source">return null;};$P.clone=function(){return new Date(this.getTime());};$P.compareTo=function(date){return Date.compare(this,date);};$P.equals=function(date){return Date.equals(this,date||new Date());};$P.between=function(start,end){return this.getTime()&gt;=start.getTime()&amp;&amp;this.getTime()&lt;=end.getTime();};$P.isAfter=function(date){return this.compareTo(date||new Date())===1;};$P.isBefore=function(date){return(this.compareTo(date||new Date())===-1);};$P.isToday=function(){return this.isSameDay(new Date());};$P.isSameDay=function(date){return this.clone().clearTime().equals(date.clone().clearTime());};$P.addMilliseconds=function(value){this.setMilliseconds(this.getMilliseconds()+value);return this;};$P.addSeconds=function(value){return this.addMilliseconds(value*1000);};$P.addMinutes=function(value){return this.addMilliseconds(value*60000);};$P.addHours=function(value){return this.addMilliseconds(value*3600000);};$P.addDays=function(value){this.setDate(this.getDate()+value);return this;};$P.addWeeks=function(value){return this.addDays(value*7);};$P.addMonths=function(value){var n=this.getDate();this.setDate(1);this.setMonth(this.getMonth()+value);this.setDate(Math.min(n,$D.getDaysInMonth(this.getFullYear(),this.getMonth())));return this;};$P.addYears=function(value){return this.addMonths(value*12);};$P.add=function(config){if(typeof config==&quot;number&quot;){this._orient=config;return this;}<br></td></tr
><tr
id=sl_svn191_16><td class="source">var x=config;if(x.milliseconds){this.addMilliseconds(x.milliseconds);}<br></td></tr
><tr
id=sl_svn191_17><td class="source">if(x.seconds){this.addSeconds(x.seconds);}<br></td></tr
><tr
id=sl_svn191_18><td class="source">if(x.minutes){this.addMinutes(x.minutes);}<br></td></tr
><tr
id=sl_svn191_19><td class="source">if(x.hours){this.addHours(x.hours);}<br></td></tr
><tr
id=sl_svn191_20><td class="source">if(x.weeks){this.addWeeks(x.weeks);}<br></td></tr
><tr
id=sl_svn191_21><td class="source">if(x.months){this.addMonths(x.months);}<br></td></tr
><tr
id=sl_svn191_22><td class="source">if(x.years){this.addYears(x.years);}<br></td></tr
><tr
id=sl_svn191_23><td class="source">if(x.days){this.addDays(x.days);}<br></td></tr
><tr
id=sl_svn191_24><td class="source">return this;};var $y,$m,$d;$P.getWeek=function(){var a,b,c,d,e,f,g,n,s,w;$y=(!$y)?this.getFullYear():$y;$m=(!$m)?this.getMonth()+1:$m;$d=(!$d)?this.getDate():$d;if($m&lt;=2){a=$y-1;b=(a/4|0)-(a/100|0)+(a/400|0);c=((a-1)/4|0)-((a-1)/100|0)+((a-1)/400|0);s=b-c;e=0;f=$d-1+(31*($m-1));}else{a=$y;b=(a/4|0)-(a/100|0)+(a/400|0);c=((a-1)/4|0)-((a-1)/100|0)+((a-1)/400|0);s=b-c;e=s+1;f=$d+((153*($m-3)+2)/5)+58+s;}<br></td></tr
><tr
id=sl_svn191_25><td class="source">g=(a+b)%7;d=(f+g-e)%7;n=(f+3-d)|0;if(n&lt;0){w=53-((g-s)/5|0);}else if(n&gt;364+s){w=1;}else{w=(n/7|0)+1;}<br></td></tr
><tr
id=sl_svn191_26><td class="source">$y=$m=$d=null;return w;};$P.getISOWeek=function(){$y=this.getUTCFullYear();$m=this.getUTCMonth()+1;$d=this.getUTCDate();return p(this.getWeek());};$P.setWeek=function(n){return this.moveToDayOfWeek(1).addWeeks(n-this.getWeek());};$D._validate=function(n,min,max,name){if(typeof n==&quot;undefined&quot;){return false;}else if(typeof n!=&quot;number&quot;){throw new TypeError(n+&quot; is not a Number.&quot;);}else if(n&lt;min||n&gt;max){throw new RangeError(n+&quot; is not a valid value for &quot;+name+&quot;.&quot;);}<br></td></tr
><tr
id=sl_svn191_27><td class="source">return true;};$D.validateMillisecond=function(value){return $D._validate(value,0,999,&quot;millisecond&quot;);};$D.validateSecond=function(value){return $D._validate(value,0,59,&quot;second&quot;);};$D.validateMinute=function(value){return $D._validate(value,0,59,&quot;minute&quot;);};$D.validateHour=function(value){return $D._validate(value,0,23,&quot;hour&quot;);};$D.validateDay=function(value,year,month){return $D._validate(value,1,$D.getDaysInMonth(year,month),&quot;day&quot;);};$D.validateMonth=function(value){return $D._validate(value,0,11,&quot;month&quot;);};$D.validateYear=function(value){return $D._validate(value,0,9999,&quot;year&quot;);};$P.set=function(config){if($D.validateMillisecond(config.millisecond)){this.addMilliseconds(config.millisecond-this.getMilliseconds());}<br></td></tr
><tr
id=sl_svn191_28><td class="source">if($D.validateSecond(config.second)){this.addSeconds(config.second-this.getSeconds());}<br></td></tr
><tr
id=sl_svn191_29><td class="source">if($D.validateMinute(config.minute)){this.addMinutes(config.minute-this.getMinutes());}<br></td></tr
><tr
id=sl_svn191_30><td class="source">if($D.validateHour(config.hour)){this.addHours(config.hour-this.getHours());}<br></td></tr
><tr
id=sl_svn191_31><td class="source">if($D.validateMonth(config.month)){this.addMonths(config.month-this.getMonth());}<br></td></tr
><tr
id=sl_svn191_32><td class="source">if($D.validateYear(config.year)){this.addYears(config.year-this.getFullYear());}<br></td></tr
><tr
id=sl_svn191_33><td class="source">if($D.validateDay(config.day,this.getFullYear(),this.getMonth())){this.addDays(config.day-this.getDate());}<br></td></tr
><tr
id=sl_svn191_34><td class="source">if(config.timezone){this.setTimezone(config.timezone);}<br></td></tr
><tr
id=sl_svn191_35><td class="source">if(config.timezoneOffset){this.setTimezoneOffset(config.timezoneOffset);}<br></td></tr
><tr
id=sl_svn191_36><td class="source">if(config.week&amp;&amp;$D._validate(config.week,0,53,&quot;week&quot;)){this.setWeek(config.week);}<br></td></tr
><tr
id=sl_svn191_37><td class="source">return this;};$P.moveToFirstDayOfMonth=function(){return this.set({day:1});};$P.moveToLastDayOfMonth=function(){return this.set({day:$D.getDaysInMonth(this.getFullYear(),this.getMonth())});};$P.moveToNthOccurrence=function(dayOfWeek,occurrence){var shift=0;if(occurrence&gt;0){shift=occurrence-1;}<br></td></tr
><tr
id=sl_svn191_38><td class="source">else if(occurrence===-1){this.moveToLastDayOfMonth();if(this.getDay()!==dayOfWeek){this.moveToDayOfWeek(dayOfWeek,-1);}<br></td></tr
><tr
id=sl_svn191_39><td class="source">return this;}<br></td></tr
><tr
id=sl_svn191_40><td class="source">return this.moveToFirstDayOfMonth().addDays(-1).moveToDayOfWeek(dayOfWeek,+1).addWeeks(shift);};$P.moveToDayOfWeek=function(dayOfWeek,orient){var diff=(dayOfWeek-this.getDay()+7*(orient||+1))%7;return this.addDays((diff===0)?diff+=7*(orient||+1):diff);};$P.moveToMonth=function(month,orient){var diff=(month-this.getMonth()+12*(orient||+1))%12;return this.addMonths((diff===0)?diff+=12*(orient||+1):diff);};$P.getOrdinalNumber=function(){return Math.ceil((this.clone().clearTime()-new Date(this.getFullYear(),0,1))/86400000)+1;};$P.getTimezone=function(){return $D.getTimezoneAbbreviation(this.getUTCOffset());};$P.setTimezoneOffset=function(offset){var here=this.getTimezoneOffset(),there=Number(offset)*-6/10;return this.addMinutes(there-here);};$P.setTimezone=function(offset){return this.setTimezoneOffset($D.getTimezoneOffset(offset));};$P.hasDaylightSavingTime=function(){return(Date.today().set({month:0,day:1}).getTimezoneOffset()!==Date.today().set({month:6,day:1}).getTimezoneOffset());};$P.isDaylightSavingTime=function(){return(this.hasDaylightSavingTime()&amp;&amp;new Date().getTimezoneOffset()===Date.today().set({month:6,day:1}).getTimezoneOffset());};$P.getUTCOffset=function(){var n=this.getTimezoneOffset()*-10/6,r;if(n&lt;0){r=(n-10000).toString();return r.charAt(0)+r.substr(2);}else{r=(n+10000).toString();return&quot;+&quot;+r.substr(1);}};$P.getElapsed=function(date){return(date||new Date())-this;};if(!$P.toISOString){$P.toISOString=function(){function f(n){return n&lt;10?&#39;0&#39;+n:n;}<br></td></tr
><tr
id=sl_svn191_41><td class="source">return&#39;&quot;&#39;+this.getUTCFullYear()+&#39;-&#39;+<br></td></tr
><tr
id=sl_svn191_42><td class="source">f(this.getUTCMonth()+1)+&#39;-&#39;+<br></td></tr
><tr
id=sl_svn191_43><td class="source">f(this.getUTCDate())+&#39;T&#39;+<br></td></tr
><tr
id=sl_svn191_44><td class="source">f(this.getUTCHours())+&#39;:&#39;+<br></td></tr
><tr
id=sl_svn191_45><td class="source">f(this.getUTCMinutes())+&#39;:&#39;+<br></td></tr
><tr
id=sl_svn191_46><td class="source">f(this.getUTCSeconds())+&#39;Z&quot;&#39;;};}<br></td></tr
><tr
id=sl_svn191_47><td class="source">$P._toString=$P.toString;$P.toString=function(format){var x=this;if(format&amp;&amp;format.length==1){var c=$C.formatPatterns;x.t=x.toString;switch(format){case&quot;d&quot;:return x.t(c.shortDate);case&quot;D&quot;:return x.t(c.longDate);case&quot;F&quot;:return x.t(c.fullDateTime);case&quot;m&quot;:return x.t(c.monthDay);case&quot;r&quot;:return x.t(c.rfc1123);case&quot;s&quot;:return x.t(c.sortableDateTime);case&quot;t&quot;:return x.t(c.shortTime);case&quot;T&quot;:return x.t(c.longTime);case&quot;u&quot;:return x.t(c.universalSortableDateTime);case&quot;y&quot;:return x.t(c.yearMonth);}}<br></td></tr
><tr
id=sl_svn191_48><td class="source">var ord=function(n){switch(n*1){case 1:case 21:case 31:return&quot;st&quot;;case 2:case 22:return&quot;nd&quot;;case 3:case 23:return&quot;rd&quot;;default:return&quot;th&quot;;}};return format?format.replace(/(\\)?(dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|S)/g,function(m){if(m.charAt(0)===&quot;\\&quot;){return m.replace(&quot;\\&quot;,&quot;&quot;);}<br></td></tr
><tr
id=sl_svn191_49><td class="source">x.h=x.getHours;switch(m){case&quot;hh&quot;:return p(x.h()&lt;13?(x.h()===0?12:x.h()):(x.h()-12));case&quot;h&quot;:return x.h()&lt;13?(x.h()===0?12:x.h()):(x.h()-12);case&quot;HH&quot;:return p(x.h());case&quot;H&quot;:return x.h();case&quot;mm&quot;:return p(x.getMinutes());case&quot;m&quot;:return x.getMinutes();case&quot;ss&quot;:return p(x.getSeconds());case&quot;s&quot;:return x.getSeconds();case&quot;yyyy&quot;:return p(x.getFullYear(),4);case&quot;yy&quot;:return p(x.getFullYear());case&quot;dddd&quot;:return $C.dayNames[x.getDay()];case&quot;ddd&quot;:return $C.abbreviatedDayNames[x.getDay()];case&quot;dd&quot;:return p(x.getDate());case&quot;d&quot;:return x.getDate();case&quot;MMMM&quot;:return $C.monthNames[x.getMonth()];case&quot;MMM&quot;:return $C.abbreviatedMonthNames[x.getMonth()];case&quot;MM&quot;:return p((x.getMonth()+1));case&quot;M&quot;:return x.getMonth()+1;case&quot;t&quot;:return x.h()&lt;12?$C.amDesignator.substring(0,1):$C.pmDesignator.substring(0,1);case&quot;tt&quot;:return x.h()&lt;12?$C.amDesignator:$C.pmDesignator;case&quot;S&quot;:return ord(x.getDate());default:return m;}}):this._toString();};}());<br></td></tr
><tr
id=sl_svn191_50><td class="source">(function(){var $D=Date,$P=$D.prototype,$C=$D.CultureInfo,$N=Number.prototype;$P._orient=+1;$P._nth=null;$P._is=false;$P._same=false;$P._isSecond=false;$N._dateElement=&quot;day&quot;;$P.next=function(){this._orient=+1;return this;};$D.next=function(){return $D.today().next();};$P.last=$P.prev=$P.previous=function(){this._orient=-1;return this;};$D.last=$D.prev=$D.previous=function(){return $D.today().last();};$P.is=function(){this._is=true;return this;};$P.same=function(){this._same=true;this._isSecond=false;return this;};$P.today=function(){return this.same().day();};$P.weekday=function(){if(this._is){this._is=false;return(!this.is().sat()&amp;&amp;!this.is().sun());}<br></td></tr
><tr
id=sl_svn191_51><td class="source">return false;};$P.at=function(time){return(typeof time===&quot;string&quot;)?$D.parse(this.toString(&quot;d&quot;)+&quot; &quot;+time):this.set(time);};$N.fromNow=$N.after=function(date){var c={};c[this._dateElement]=this;return((!date)?new Date():date.clone()).add(c);};$N.ago=$N.before=function(date){var c={};c[this._dateElement]=this*-1;return((!date)?new Date():date.clone()).add(c);};var dx=(&quot;sunday monday tuesday wednesday thursday friday saturday&quot;).split(/\s/),mx=(&quot;january february march april may june july august september october november december&quot;).split(/\s/),px=(&quot;Millisecond Second Minute Hour Day Week Month Year&quot;).split(/\s/),pxf=(&quot;Milliseconds Seconds Minutes Hours Date Week Month FullYear&quot;).split(/\s/),nth=(&quot;final first second third fourth fifth&quot;).split(/\s/),de;$P.toObject=function(){var o={};for(var i=0;i&lt;px.length;i++){o[px[i].toLowerCase()]=this[&quot;get&quot;+pxf[i]]();}<br></td></tr
><tr
id=sl_svn191_52><td class="source">return o;};$D.fromObject=function(config){config.week=null;return Date.today().set(config);};var df=function(n){return function(){if(this._is){this._is=false;return this.getDay()==n;}<br></td></tr
><tr
id=sl_svn191_53><td class="source">if(this._nth!==null){if(this._isSecond){this.addSeconds(this._orient*-1);}<br></td></tr
><tr
id=sl_svn191_54><td class="source">this._isSecond=false;var ntemp=this._nth;this._nth=null;var temp=this.clone().moveToLastDayOfMonth();this.moveToNthOccurrence(n,ntemp);if(this&gt;temp){throw new RangeError($D.getDayName(n)+&quot; does not occur &quot;+ntemp+&quot; times in the month of &quot;+$D.getMonthName(temp.getMonth())+&quot; &quot;+temp.getFullYear()+&quot;.&quot;);}<br></td></tr
><tr
id=sl_svn191_55><td class="source">return this;}<br></td></tr
><tr
id=sl_svn191_56><td class="source">return this.moveToDayOfWeek(n,this._orient);};};var sdf=function(n){return function(){var t=$D.today(),shift=n-t.getDay();if(n===0&amp;&amp;$C.firstDayOfWeek===1&amp;&amp;t.getDay()!==0){shift=shift+7;}<br></td></tr
><tr
id=sl_svn191_57><td class="source">return t.addDays(shift);};};for(var i=0;i&lt;dx.length;i++){$D[dx[i].toUpperCase()]=$D[dx[i].toUpperCase().substring(0,3)]=i;$D[dx[i]]=$D[dx[i].substring(0,3)]=sdf(i);$P[dx[i]]=$P[dx[i].substring(0,3)]=df(i);}<br></td></tr
><tr
id=sl_svn191_58><td class="source">var mf=function(n){return function(){if(this._is){this._is=false;return this.getMonth()===n;}<br></td></tr
><tr
id=sl_svn191_59><td class="source">return this.moveToMonth(n,this._orient);};};var smf=function(n){return function(){return $D.today().set({month:n,day:1});};};for(var j=0;j&lt;mx.length;j++){$D[mx[j].toUpperCase()]=$D[mx[j].toUpperCase().substring(0,3)]=j;$D[mx[j]]=$D[mx[j].substring(0,3)]=smf(j);$P[mx[j]]=$P[mx[j].substring(0,3)]=mf(j);}<br></td></tr
><tr
id=sl_svn191_60><td class="source">var ef=function(j){return function(){if(this._isSecond){this._isSecond=false;return this;}<br></td></tr
><tr
id=sl_svn191_61><td class="source">if(this._same){this._same=this._is=false;var o1=this.toObject(),o2=(arguments[0]||new Date()).toObject(),v=&quot;&quot;,k=j.toLowerCase();for(var m=(px.length-1);m&gt;-1;m--){v=px[m].toLowerCase();if(o1[v]!=o2[v]){return false;}<br></td></tr
><tr
id=sl_svn191_62><td class="source">if(k==v){break;}}<br></td></tr
><tr
id=sl_svn191_63><td class="source">return true;}<br></td></tr
><tr
id=sl_svn191_64><td class="source">if(j.substring(j.length-1)!=&quot;s&quot;){j+=&quot;s&quot;;}<br></td></tr
><tr
id=sl_svn191_65><td class="source">return this[&quot;add&quot;+j](this._orient);};};var nf=function(n){return function(){this._dateElement=n;return this;};};for(var k=0;k&lt;px.length;k++){de=px[k].toLowerCase();$P[de]=$P[de+&quot;s&quot;]=ef(px[k]);$N[de]=$N[de+&quot;s&quot;]=nf(de);}<br></td></tr
><tr
id=sl_svn191_66><td class="source">$P._ss=ef(&quot;Second&quot;);var nthfn=function(n){return function(dayOfWeek){if(this._same){return this._ss(arguments[0]);}<br></td></tr
><tr
id=sl_svn191_67><td class="source">if(dayOfWeek||dayOfWeek===0){return this.moveToNthOccurrence(dayOfWeek,n);}<br></td></tr
><tr
id=sl_svn191_68><td class="source">this._nth=n;if(n===2&amp;&amp;(dayOfWeek===undefined||dayOfWeek===null)){this._isSecond=true;return this.addSeconds(this._orient);}<br></td></tr
><tr
id=sl_svn191_69><td class="source">return this;};};for(var l=0;l&lt;nth.length;l++){$P[nth[l]]=(l===0)?nthfn(-1):nthfn(l);}}());<br></td></tr
><tr
id=sl_svn191_70><td class="source">(function(){Date.Parsing={Exception:function(s){this.message=&quot;Parse error at &#39;&quot;+s.substring(0,10)+&quot; ...&#39;&quot;;}};var $P=Date.Parsing;var _=$P.Operators={rtoken:function(r){return function(s){var mx=s.match(r);if(mx){return([mx[0],s.substring(mx[0].length)]);}else{throw new $P.Exception(s);}};},token:function(s){return function(s){return _.rtoken(new RegExp(&quot;^\s*&quot;+s+&quot;\s*&quot;))(s);};},stoken:function(s){return _.rtoken(new RegExp(&quot;^&quot;+s));},until:function(p){return function(s){var qx=[],rx=null;while(s.length){try{rx=p.call(this,s);}catch(e){qx.push(rx[0]);s=rx[1];continue;}<br></td></tr
><tr
id=sl_svn191_71><td class="source">break;}<br></td></tr
><tr
id=sl_svn191_72><td class="source">return[qx,s];};},many:function(p){return function(s){var rx=[],r=null;while(s.length){try{r=p.call(this,s);}catch(e){return[rx,s];}<br></td></tr
><tr
id=sl_svn191_73><td class="source">rx.push(r[0]);s=r[1];}<br></td></tr
><tr
id=sl_svn191_74><td class="source">return[rx,s];};},optional:function(p){return function(s){var r=null;try{r=p.call(this,s);}catch(e){return[null,s];}<br></td></tr
><tr
id=sl_svn191_75><td class="source">return[r[0],r[1]];};},not:function(p){return function(s){try{p.call(this,s);}catch(e){return[null,s];}<br></td></tr
><tr
id=sl_svn191_76><td class="source">throw new $P.Exception(s);};},ignore:function(p){return p?function(s){var r=null;r=p.call(this,s);return[null,r[1]];}:null;},product:function(){var px=arguments[0],qx=Array.prototype.slice.call(arguments,1),rx=[];for(var i=0;i&lt;px.length;i++){rx.push(_.each(px[i],qx));}<br></td></tr
><tr
id=sl_svn191_77><td class="source">return rx;},cache:function(rule){var cache={},r=null;return function(s){try{r=cache[s]=(cache[s]||rule.call(this,s));}catch(e){r=cache[s]=e;}<br></td></tr
><tr
id=sl_svn191_78><td class="source">if(r instanceof $P.Exception){throw r;}else{return r;}};},any:function(){var px=arguments;return function(s){var r=null;for(var i=0;i&lt;px.length;i++){if(px[i]==null){continue;}<br></td></tr
><tr
id=sl_svn191_79><td class="source">try{r=(px[i].call(this,s));}catch(e){r=null;}<br></td></tr
><tr
id=sl_svn191_80><td class="source">if(r){return r;}}<br></td></tr
><tr
id=sl_svn191_81><td class="source">throw new $P.Exception(s);};},each:function(){var px=arguments;return function(s){var rx=[],r=null;for(var i=0;i&lt;px.length;i++){if(px[i]==null){continue;}<br></td></tr
><tr
id=sl_svn191_82><td class="source">try{r=(px[i].call(this,s));}catch(e){throw new $P.Exception(s);}<br></td></tr
><tr
id=sl_svn191_83><td class="source">rx.push(r[0]);s=r[1];}<br></td></tr
><tr
id=sl_svn191_84><td class="source">return[rx,s];};},all:function(){var px=arguments,_=_;return _.each(_.optional(px));},sequence:function(px,d,c){d=d||_.rtoken(/^\s*/);c=c||null;if(px.length==1){return px[0];}<br></td></tr
><tr
id=sl_svn191_85><td class="source">return function(s){var r=null,q=null;var rx=[];for(var i=0;i&lt;px.length;i++){try{r=px[i].call(this,s);}catch(e){break;}<br></td></tr
><tr
id=sl_svn191_86><td class="source">rx.push(r[0]);try{q=d.call(this,r[1]);}catch(ex){q=null;break;}<br></td></tr
><tr
id=sl_svn191_87><td class="source">s=q[1];}<br></td></tr
><tr
id=sl_svn191_88><td class="source">if(!r){throw new $P.Exception(s);}<br></td></tr
><tr
id=sl_svn191_89><td class="source">if(q){throw new $P.Exception(q[1]);}<br></td></tr
><tr
id=sl_svn191_90><td class="source">if(c){try{r=c.call(this,r[1]);}catch(ey){throw new $P.Exception(r[1]);}}<br></td></tr
><tr
id=sl_svn191_91><td class="source">return[rx,(r?r[1]:s)];};},between:function(d1,p,d2){d2=d2||d1;var _fn=_.each(_.ignore(d1),p,_.ignore(d2));return function(s){var rx=_fn.call(this,s);return[[rx[0][0],r[0][2]],rx[1]];};},list:function(p,d,c){d=d||_.rtoken(/^\s*/);c=c||null;return(p instanceof Array?_.each(_.product(p.slice(0,-1),_.ignore(d)),p.slice(-1),_.ignore(c)):_.each(_.many(_.each(p,_.ignore(d))),px,_.ignore(c)));},set:function(px,d,c){d=d||_.rtoken(/^\s*/);c=c||null;return function(s){var r=null,p=null,q=null,rx=null,best=[[],s],last=false;for(var i=0;i&lt;px.length;i++){q=null;p=null;r=null;last=(px.length==1);try{r=px[i].call(this,s);}catch(e){continue;}<br></td></tr
><tr
id=sl_svn191_92><td class="source">rx=[[r[0]],r[1]];if(r[1].length&gt;0&amp;&amp;!last){try{q=d.call(this,r[1]);}catch(ex){last=true;}}else{last=true;}<br></td></tr
><tr
id=sl_svn191_93><td class="source">if(!last&amp;&amp;q[1].length===0){last=true;}<br></td></tr
><tr
id=sl_svn191_94><td class="source">if(!last){var qx=[];for(var j=0;j&lt;px.length;j++){if(i!=j){qx.push(px[j]);}}<br></td></tr
><tr
id=sl_svn191_95><td class="source">p=_.set(qx,d).call(this,q[1]);if(p[0].length&gt;0){rx[0]=rx[0].concat(p[0]);rx[1]=p[1];}}<br></td></tr
><tr
id=sl_svn191_96><td class="source">if(rx[1].length&lt;best[1].length){best=rx;}<br></td></tr
><tr
id=sl_svn191_97><td class="source">if(best[1].length===0){break;}}<br></td></tr
><tr
id=sl_svn191_98><td class="source">if(best[0].length===0){return best;}<br></td></tr
><tr
id=sl_svn191_99><td class="source">if(c){try{q=c.call(this,best[1]);}catch(ey){throw new $P.Exception(best[1]);}<br></td></tr
><tr
id=sl_svn191_100><td class="source">best[1]=q[1];}<br></td></tr
><tr
id=sl_svn191_101><td class="source">return best;};},forward:function(gr,fname){return function(s){return gr[fname].call(this,s);};},replace:function(rule,repl){return function(s){var r=rule.call(this,s);return[repl,r[1]];};},process:function(rule,fn){return function(s){var r=rule.call(this,s);return[fn.call(this,r[0]),r[1]];};},min:function(min,rule){return function(s){var rx=rule.call(this,s);if(rx[0].length&lt;min){throw new $P.Exception(s);}<br></td></tr
><tr
id=sl_svn191_102><td class="source">return rx;};}};var _generator=function(op){return function(){var args=null,rx=[];if(arguments.length&gt;1){args=Array.prototype.slice.call(arguments);}else if(arguments[0]instanceof Array){args=arguments[0];}<br></td></tr
><tr
id=sl_svn191_103><td class="source">if(args){for(var i=0,px=args.shift();i&lt;px.length;i++){args.unshift(px[i]);rx.push(op.apply(null,args));args.shift();return rx;}}else{return op.apply(null,arguments);}};};var gx=&quot;optional not ignore cache&quot;.split(/\s/);for(var i=0;i&lt;gx.length;i++){_[gx[i]]=_generator(_[gx[i]]);}<br></td></tr
><tr
id=sl_svn191_104><td class="source">var _vector=function(op){return function(){if(arguments[0]instanceof Array){return op.apply(null,arguments[0]);}else{return op.apply(null,arguments);}};};var vx=&quot;each any all&quot;.split(/\s/);for(var j=0;j&lt;vx.length;j++){_[vx[j]]=_vector(_[vx[j]]);}}());(function(){var $D=Date,$P=$D.prototype,$C=$D.CultureInfo;var flattenAndCompact=function(ax){var rx=[];for(var i=0;i&lt;ax.length;i++){if(ax[i]instanceof Array){rx=rx.concat(flattenAndCompact(ax[i]));}else{if(ax[i]){rx.push(ax[i]);}}}<br></td></tr
><tr
id=sl_svn191_105><td class="source">return rx;};$D.Grammar={};$D.Translator={hour:function(s){return function(){this.hour=Number(s);};},minute:function(s){return function(){this.minute=Number(s);};},second:function(s){return function(){this.second=Number(s);};},meridian:function(s){return function(){this.meridian=s.slice(0,1).toLowerCase();};},timezone:function(s){return function(){var n=s.replace(/[^\d\+\-]/g,&quot;&quot;);if(n.length){this.timezoneOffset=Number(n);}else{this.timezone=s.toLowerCase();}};},day:function(x){var s=x[0];return function(){this.day=Number(s.match(/\d+/)[0]);};},month:function(s){return function(){this.month=(s.length==3)?&quot;jan feb mar apr may jun jul aug sep oct nov dec&quot;.indexOf(s)/4:Number(s)-1;};},year:function(s){return function(){var n=Number(s);this.year=((s.length&gt;2)?n:(n+(((n+2000)&lt;$C.twoDigitYearMax)?2000:1900)));};},rday:function(s){return function(){switch(s){case&quot;yesterday&quot;:this.days=-1;break;case&quot;tomorrow&quot;:this.days=1;break;case&quot;today&quot;:this.days=0;break;case&quot;now&quot;:this.days=0;this.now=true;break;}};},finishExact:function(x){x=(x instanceof Array)?x:[x];for(var i=0;i&lt;x.length;i++){if(x[i]){x[i].call(this);}}<br></td></tr
><tr
id=sl_svn191_106><td class="source">var now=new Date();if((this.hour||this.minute)&amp;&amp;(!this.month&amp;&amp;!this.year&amp;&amp;!this.day)){this.day=now.getDate();}<br></td></tr
><tr
id=sl_svn191_107><td class="source">if(!this.year){this.year=now.getFullYear();}<br></td></tr
><tr
id=sl_svn191_108><td class="source">if(!this.month&amp;&amp;this.month!==0){this.month=now.getMonth();}<br></td></tr
><tr
id=sl_svn191_109><td class="source">if(!this.day){this.day=1;}<br></td></tr
><tr
id=sl_svn191_110><td class="source">if(!this.hour){this.hour=0;}<br></td></tr
><tr
id=sl_svn191_111><td class="source">if(!this.minute){this.minute=0;}<br></td></tr
><tr
id=sl_svn191_112><td class="source">if(!this.second){this.second=0;}<br></td></tr
><tr
id=sl_svn191_113><td class="source">if(this.meridian&amp;&amp;this.hour){if(this.meridian==&quot;p&quot;&amp;&amp;this.hour&lt;12){this.hour=this.hour+12;}else if(this.meridian==&quot;a&quot;&amp;&amp;this.hour==12){this.hour=0;}}<br></td></tr
><tr
id=sl_svn191_114><td class="source">if(this.day&gt;$D.getDaysInMonth(this.year,this.month)){throw new RangeError(this.day+&quot; is not a valid value for days.&quot;);}<br></td></tr
><tr
id=sl_svn191_115><td class="source">var r=new Date(this.year,this.month,this.day,this.hour,this.minute,this.second);if(this.timezone){r.set({timezone:this.timezone});}else if(this.timezoneOffset){r.set({timezoneOffset:this.timezoneOffset});}<br></td></tr
><tr
id=sl_svn191_116><td class="source">return r;},finish:function(x){x=(x instanceof Array)?flattenAndCompact(x):[x];if(x.length===0){return null;}<br></td></tr
><tr
id=sl_svn191_117><td class="source">for(var i=0;i&lt;x.length;i++){if(typeof x[i]==&quot;function&quot;){x[i].call(this);}}<br></td></tr
><tr
id=sl_svn191_118><td class="source">var today=$D.today();if(this.now&amp;&amp;!this.unit&amp;&amp;!this.operator){return new Date();}else if(this.now){today=new Date();}<br></td></tr
><tr
id=sl_svn191_119><td class="source">var expression=!!(this.days&amp;&amp;this.days!==null||this.orient||this.operator);var gap,mod,orient;orient=((this.orient==&quot;past&quot;||this.operator==&quot;subtract&quot;)?-1:1);if(!this.now&amp;&amp;&quot;hour minute second&quot;.indexOf(this.unit)!=-1){today.setTimeToNow();}<br></td></tr
><tr
id=sl_svn191_120><td class="source">if(this.month||this.month===0){if(&quot;year day hour minute second&quot;.indexOf(this.unit)!=-1){this.value=this.month+1;this.month=null;expression=true;}}<br></td></tr
><tr
id=sl_svn191_121><td class="source">if(!expression&amp;&amp;this.weekday&amp;&amp;!this.day&amp;&amp;!this.days){var temp=Date[this.weekday]();this.day=temp.getDate();if(!this.month){this.month=temp.getMonth();}<br></td></tr
><tr
id=sl_svn191_122><td class="source">this.year=temp.getFullYear();}<br></td></tr
><tr
id=sl_svn191_123><td class="source">if(expression&amp;&amp;this.weekday&amp;&amp;this.unit!=&quot;month&quot;){this.unit=&quot;day&quot;;gap=($D.getDayNumberFromName(this.weekday)-today.getDay());mod=7;this.days=gap?((gap+(orient*mod))%mod):(orient*mod);}<br></td></tr
><tr
id=sl_svn191_124><td class="source">if(this.month&amp;&amp;this.unit==&quot;day&quot;&amp;&amp;this.operator){this.value=(this.month+1);this.month=null;}<br></td></tr
><tr
id=sl_svn191_125><td class="source">if(this.value!=null&amp;&amp;this.month!=null&amp;&amp;this.year!=null){this.day=this.value*1;}<br></td></tr
><tr
id=sl_svn191_126><td class="source">if(this.month&amp;&amp;!this.day&amp;&amp;this.value){today.set({day:this.value*1});if(!expression){this.day=this.value*1;}}<br></td></tr
><tr
id=sl_svn191_127><td class="source">if(!this.month&amp;&amp;this.value&amp;&amp;this.unit==&quot;month&quot;&amp;&amp;!this.now){this.month=this.value;expression=true;}<br></td></tr
><tr
id=sl_svn191_128><td class="source">if(expression&amp;&amp;(this.month||this.month===0)&amp;&amp;this.unit!=&quot;year&quot;){this.unit=&quot;month&quot;;gap=(this.month-today.getMonth());mod=12;this.months=gap?((gap+(orient*mod))%mod):(orient*mod);this.month=null;}<br></td></tr
><tr
id=sl_svn191_129><td class="source">if(!this.unit){this.unit=&quot;day&quot;;}<br></td></tr
><tr
id=sl_svn191_130><td class="source">if(!this.value&amp;&amp;this.operator&amp;&amp;this.operator!==null&amp;&amp;this[this.unit+&quot;s&quot;]&amp;&amp;this[this.unit+&quot;s&quot;]!==null){this[this.unit+&quot;s&quot;]=this[this.unit+&quot;s&quot;]+((this.operator==&quot;add&quot;)?1:-1)+(this.value||0)*orient;}else if(this[this.unit+&quot;s&quot;]==null||this.operator!=null){if(!this.value){this.value=1;}<br></td></tr
><tr
id=sl_svn191_131><td class="source">this[this.unit+&quot;s&quot;]=this.value*orient;}<br></td></tr
><tr
id=sl_svn191_132><td class="source">if(this.meridian&amp;&amp;this.hour){if(this.meridian==&quot;p&quot;&amp;&amp;this.hour&lt;12){this.hour=this.hour+12;}else if(this.meridian==&quot;a&quot;&amp;&amp;this.hour==12){this.hour=0;}}<br></td></tr
><tr
id=sl_svn191_133><td class="source">if(this.weekday&amp;&amp;!this.day&amp;&amp;!this.days){var temp=Date[this.weekday]();this.day=temp.getDate();if(temp.getMonth()!==today.getMonth()){this.month=temp.getMonth();}}<br></td></tr
><tr
id=sl_svn191_134><td class="source">if((this.month||this.month===0)&amp;&amp;!this.day){this.day=1;}<br></td></tr
><tr
id=sl_svn191_135><td class="source">if(!this.orient&amp;&amp;!this.operator&amp;&amp;this.unit==&quot;week&quot;&amp;&amp;this.value&amp;&amp;!this.day&amp;&amp;!this.month){return Date.today().setWeek(this.value);}<br></td></tr
><tr
id=sl_svn191_136><td class="source">if(expression&amp;&amp;this.timezone&amp;&amp;this.day&amp;&amp;this.days){this.day=this.days;}<br></td></tr
><tr
id=sl_svn191_137><td class="source">return(expression)?today.add(this):today.set(this);}};var _=$D.Parsing.Operators,g=$D.Grammar,t=$D.Translator,_fn;g.datePartDelimiter=_.rtoken(/^([\s\-\.\,\/\x27]+)/);g.timePartDelimiter=_.stoken(&quot;:&quot;);g.whiteSpace=_.rtoken(/^\s*/);g.generalDelimiter=_.rtoken(/^(([\s\,]|at|@|on)+)/);var _C={};g.ctoken=function(keys){var fn=_C[keys];if(!fn){var c=$C.regexPatterns;var kx=keys.split(/\s+/),px=[];for(var i=0;i&lt;kx.length;i++){px.push(_.replace(_.rtoken(c[kx[i]]),kx[i]));}<br></td></tr
><tr
id=sl_svn191_138><td class="source">fn=_C[keys]=_.any.apply(null,px);}<br></td></tr
><tr
id=sl_svn191_139><td class="source">return fn;};g.ctoken2=function(key){return _.rtoken($C.regexPatterns[key]);};g.h=_.cache(_.process(_.rtoken(/^(0[0-9]|1[0-2]|[1-9])/),t.hour));g.hh=_.cache(_.process(_.rtoken(/^(0[0-9]|1[0-2])/),t.hour));g.H=_.cache(_.process(_.rtoken(/^([0-1][0-9]|2[0-3]|[0-9])/),t.hour));g.HH=_.cache(_.process(_.rtoken(/^([0-1][0-9]|2[0-3])/),t.hour));g.m=_.cache(_.process(_.rtoken(/^([0-5][0-9]|[0-9])/),t.minute));g.mm=_.cache(_.process(_.rtoken(/^[0-5][0-9]/),t.minute));g.s=_.cache(_.process(_.rtoken(/^([0-5][0-9]|[0-9])/),t.second));g.ss=_.cache(_.process(_.rtoken(/^[0-5][0-9]/),t.second));g.hms=_.cache(_.sequence([g.H,g.m,g.s],g.timePartDelimiter));g.t=_.cache(_.process(g.ctoken2(&quot;shortMeridian&quot;),t.meridian));g.tt=_.cache(_.process(g.ctoken2(&quot;longMeridian&quot;),t.meridian));g.z=_.cache(_.process(_.rtoken(/^((\+|\-)\s*\d\d\d\d)|((\+|\-)\d\d\:?\d\d)/),t.timezone));g.zz=_.cache(_.process(_.rtoken(/^((\+|\-)\s*\d\d\d\d)|((\+|\-)\d\d\:?\d\d)/),t.timezone));g.zzz=_.cache(_.process(g.ctoken2(&quot;timezone&quot;),t.timezone));g.timeSuffix=_.each(_.ignore(g.whiteSpace),_.set([g.tt,g.zzz]));g.time=_.each(_.optional(_.ignore(_.stoken(&quot;T&quot;))),g.hms,g.timeSuffix);g.d=_.cache(_.process(_.each(_.rtoken(/^([0-2]\d|3[0-1]|\d)/),_.optional(g.ctoken2(&quot;ordinalSuffix&quot;))),t.day));g.dd=_.cache(_.process(_.each(_.rtoken(/^([0-2]\d|3[0-1])/),_.optional(g.ctoken2(&quot;ordinalSuffix&quot;))),t.day));g.ddd=g.dddd=_.cache(_.process(g.ctoken(&quot;sun mon tue wed thu fri sat&quot;),function(s){return function(){this.weekday=s;};}));g.M=_.cache(_.process(_.rtoken(/^(1[0-2]|0\d|\d)/),t.month));g.MM=_.cache(_.process(_.rtoken(/^(1[0-2]|0\d)/),t.month));g.MMM=g.MMMM=_.cache(_.process(g.ctoken(&quot;jan feb mar apr may jun jul aug sep oct nov dec&quot;),t.month));g.y=_.cache(_.process(_.rtoken(/^(\d\d?)/),t.year));g.yy=_.cache(_.process(_.rtoken(/^(\d\d)/),t.year));g.yyy=_.cache(_.process(_.rtoken(/^(\d\d?\d?\d?)/),t.year));g.yyyy=_.cache(_.process(_.rtoken(/^(\d\d\d\d)/),t.year));_fn=function(){return _.each(_.any.apply(null,arguments),_.not(g.ctoken2(&quot;timeContext&quot;)));};g.day=_fn(g.d,g.dd);g.month=_fn(g.M,g.MMM);g.year=_fn(g.yyyy,g.yy);g.orientation=_.process(g.ctoken(&quot;past future&quot;),function(s){return function(){this.orient=s;};});g.operator=_.process(g.ctoken(&quot;add subtract&quot;),function(s){return function(){this.operator=s;};});g.rday=_.process(g.ctoken(&quot;yesterday tomorrow today now&quot;),t.rday);g.unit=_.process(g.ctoken(&quot;second minute hour day week month year&quot;),function(s){return function(){this.unit=s;};});g.value=_.process(_.rtoken(/^\d\d?(st|nd|rd|th)?/),function(s){return function(){this.value=s.replace(/\D/g,&quot;&quot;);};});g.expression=_.set([g.rday,g.operator,g.value,g.unit,g.orientation,g.ddd,g.MMM]);_fn=function(){return _.set(arguments,g.datePartDelimiter);};g.mdy=_fn(g.ddd,g.month,g.day,g.year);g.ymd=_fn(g.ddd,g.year,g.month,g.day);g.dmy=_fn(g.ddd,g.day,g.month,g.year);g.date=function(s){return((g[$C.dateElementOrder]||g.mdy).call(this,s));};g.format=_.process(_.many(_.any(_.process(_.rtoken(/^(dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|zz?z?)/),function(fmt){if(g[fmt]){return g[fmt];}else{throw $D.Parsing.Exception(fmt);}}),_.process(_.rtoken(/^[^dMyhHmstz]+/),function(s){return _.ignore(_.stoken(s));}))),function(rules){return _.process(_.each.apply(null,rules),t.finishExact);});var _F={};var _get=function(f){return _F[f]=(_F[f]||g.format(f)[0]);};g.formats=function(fx){if(fx instanceof Array){var rx=[];for(var i=0;i&lt;fx.length;i++){rx.push(_get(fx[i]));}<br></td></tr
><tr
id=sl_svn191_140><td class="source">return _.any.apply(null,rx);}else{return _get(fx);}};g._formats=g.formats([&quot;\&quot;yyyy-MM-ddTHH:mm:ssZ\&quot;&quot;,&quot;yyyy-MM-ddTHH:mm:ssZ&quot;,&quot;yyyy-MM-ddTHH:mm:ssz&quot;,&quot;yyyy-MM-ddTHH:mm:ss&quot;,&quot;yyyy-MM-ddTHH:mmZ&quot;,&quot;yyyy-MM-ddTHH:mmz&quot;,&quot;yyyy-MM-ddTHH:mm&quot;,&quot;ddd, MMM dd, yyyy H:mm:ss tt&quot;,&quot;ddd MMM d yyyy HH:mm:ss zzz&quot;,&quot;MMddyyyy&quot;,&quot;ddMMyyyy&quot;,&quot;Mddyyyy&quot;,&quot;ddMyyyy&quot;,&quot;Mdyyyy&quot;,&quot;dMyyyy&quot;,&quot;yyyy&quot;,&quot;Mdyy&quot;,&quot;dMyy&quot;,&quot;d&quot;]);g._start=_.process(_.set([g.date,g.time,g.expression],g.generalDelimiter,g.whiteSpace),t.finish);g.start=function(s){try{var r=g._formats.call({},s);if(r[1].length===0){return r;}}catch(e){}<br></td></tr
><tr
id=sl_svn191_141><td class="source">return g._start.call({},s);};$D._parse=$D.parse;$D.parse=function(s){var r=null;if(!s){return null;}<br></td></tr
><tr
id=sl_svn191_142><td class="source">if(s instanceof Date){return s;}<br></td></tr
><tr
id=sl_svn191_143><td class="source">try{r=$D.Grammar.start.call({},s.replace(/^\s*(\S*(\s+\S+)*)\s*$/,&quot;$1&quot;));}catch(e){return null;}<br></td></tr
><tr
id=sl_svn191_144><td class="source">return((r[1].length===0)?r[0]:null);};$D.getParseFunction=function(fx){var fn=$D.Grammar.formats(fx);return function(s){var r=null;try{r=fn.call({},s);}catch(e){return null;}<br></td></tr
><tr
id=sl_svn191_145><td class="source">return((r[1].length===0)?r[0]:null);};};$D.parseExact=function(s,fx){return $D.getParseFunction(fx)(s);};}());<br></td></tr
></table></pre>

<pre class="prettyprint"><table width="100%"><tr class="cursor_stop cursor_hidden"><td></td></tr></table></pre>
</td>
</tr></table>



 <div id="log">
 <div style="text-align:right">
 <a class="ifCollapse" href="#" onclick="_toggleMeta('', 'p', 'datejs', this)">Show details</a>
 <a class="ifExpand" href="#" onclick="_toggleMeta('', 'p', 'datejs', this)">Hide details</a>
 </div>
 <div class="ifExpand">
 
 <div class="pmeta_bubble_bg" style="border:1px solid white">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <div id="changelog">
 <p>Change log</p>
 <div>
 <a href="/p/datejs/source/detail?spec=svn195&r=191">r191</a>
 by ge...@coolite.com
 on May 13, 2008
 &nbsp; <a href="/p/datejs/source/diff?spec=svn195&r=191&amp;format=side&amp;path=/trunk/build/date-af-ZA.js&amp;old_path=/trunk/build/date-af-ZA.js&amp;old=190">Diff</a>
 </div>
 <pre>--------------------

2008-05-12 [geoffrey.mcgill]
<a href="/p/datejs/source/detail?r=191">Revision #191</a>

1.  Added .same() function to sugarpak.js.
The new .same() function will compare two
date objects to
        determine if they occur on/in
exactly the same instance of the given
date part.

...</pre>
 </div>
 
 
 
 
 
 
 <script type="text/javascript">
 var detail_url = '/p/datejs/source/detail?r=191&spec=svn195';
 var publish_url = '/p/datejs/source/detail?r=191&spec=svn195#publish';
 // describe the paths of this revision in javascript.
 var changed_paths = [];
 var changed_urls = [];
 
 changed_paths.push('/trunk/CHANGELOG.txt');
 changed_urls.push('/p/datejs/source/browse/trunk/CHANGELOG.txt?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/core.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/core.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-af-ZA.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-af-ZA.js?r=191&spec=svn195');
 
 var selected_path = '/trunk/build/date-af-ZA.js';
 
 
 changed_paths.push('/trunk/build/date-ar-AE.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-AE.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-BH.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-BH.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-DZ.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-DZ.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-KW.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-KW.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-LY.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-LY.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-OM.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-OM.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-SA.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-SA.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-TN.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-TN.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-ar-YE.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-ar-YE.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-az-Cyrl-AZ.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-az-Cyrl-AZ.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-be-BY.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-be-BY.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-bg-BG.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-bg-BG.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-bs-Latn-BA.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-bs-Latn-BA.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-cs-CZ.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-cs-CZ.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-da-DK.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-da-DK.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-de-CH.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-de-CH.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-de-LI.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-de-LI.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-dv-MV.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-dv-MV.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-029.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-029.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-BZ.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-BZ.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-CA.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-CA.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-JM.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-JM.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-NZ.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-NZ.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-PH.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-PH.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-TT.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-TT.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-US.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-US.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-ZA.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-ZA.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-en-ZW.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-en-ZW.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-AR.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-AR.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-CL.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-CL.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-CO.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-CO.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-CR.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-CR.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-DO.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-DO.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-EC.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-EC.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-ES.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-ES.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-GT.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-GT.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-HN.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-HN.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-MX.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-MX.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-PE.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-PE.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-PY.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-PY.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-SV.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-SV.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-es-UY.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-es-UY.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-et-EE.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-et-EE.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-eu-ES.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-eu-ES.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-fi-FI.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-fi-FI.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-fr-CA.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-fr-CA.js?r=191&spec=svn195');
 
 
 changed_paths.push('/trunk/build/date-fr-FR.js');
 changed_urls.push('/p/datejs/source/browse/trunk/build/date-fr-FR.js?r=191&spec=svn195');
 
 
 function getCurrentPageIndex() {
 for (var i = 0; i < changed_paths.length; i++) {
 if (selected_path == changed_paths[i]) {
 return i;
 }
 }
 }
 function getNextPage() {
 var i = getCurrentPageIndex();
 if (i < changed_paths.length - 1) {
 return changed_urls[i + 1];
 }
 return null;
 }
 function getPreviousPage() {
 var i = getCurrentPageIndex();
 if (i > 0) {
 return changed_urls[i - 1];
 }
 return null;
 }
 function gotoNextPage() {
 var page = getNextPage();
 if (!page) {
 page = detail_url;
 }
 window.location = page;
 }
 function gotoPreviousPage() {
 var page = getPreviousPage();
 if (!page) {
 page = detail_url;
 }
 window.location = page;
 }
 function gotoDetailPage() {
 window.location = detail_url;
 }
 function gotoPublishPage() {
 window.location = publish_url;
 }
</script>
 
 <style type="text/css">
 #review_nav {
 border-top: 3px solid white;
 padding-top: 6px;
 margin-top: 1em;
 }
 #review_nav td {
 vertical-align: middle;
 }
 #review_nav select {
 margin: .5em 0;
 }
 </style>
 <div id="review_nav">
 <table><tr><td>Go to:&nbsp;</td><td>
 <select name="files_in_rev" onchange="window.location=this.value">
 
 <option value="/p/datejs/source/browse/trunk/CHANGELOG.txt?r=191&amp;spec=svn195"
 
 >/trunk/CHANGELOG.txt</option>
 
 <option value="/p/datejs/source/browse/trunk/build/core.js?r=191&amp;spec=svn195"
 
 >/trunk/build/core.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-af-ZA.js?r=191&amp;spec=svn195"
 selected="selected"
 >/trunk/build/date-af-ZA.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-AE.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-AE.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-BH.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-BH.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-DZ.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-DZ.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-KW.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-KW.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-LY.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-LY.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-OM.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-OM.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-SA.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-SA.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-TN.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-TN.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-ar-YE.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-ar-YE.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-az-Cyrl-AZ.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-az-Cyrl-AZ.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-be-BY.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-be-BY.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-bg-BG.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-bg-BG.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-bs-Latn-BA.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-bs-Latn-BA.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-cs-CZ.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-cs-CZ.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-da-DK.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-da-DK.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-de-CH.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-de-CH.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-de-LI.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-de-LI.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-dv-MV.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-dv-MV.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-029.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-029.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-BZ.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-BZ.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-CA.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-CA.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-JM.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-JM.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-NZ.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-NZ.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-PH.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-PH.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-TT.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-TT.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-US.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-US.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-ZA.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-ZA.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-en-ZW.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-en-ZW.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-AR.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-AR.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-CL.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-CL.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-CO.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-CO.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-CR.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-CR.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-DO.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-DO.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-EC.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-EC.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-ES.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-ES.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-GT.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-GT.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-HN.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-HN.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-MX.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-MX.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-PE.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-PE.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-PY.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-PY.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-SV.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-SV.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-es-UY.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-es-UY.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-et-EE.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-et-EE.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-eu-ES.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-eu-ES.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-fi-FI.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-fi-FI.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-fr-CA.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-fr-CA.js</option>
 
 <option value="/p/datejs/source/browse/trunk/build/date-fr-FR.js?r=191&amp;spec=svn195"
 
 >/trunk/build/date-fr-FR.js</option>
 
 </select>
 </td></tr></table>
 
 
 




 
 </div>
 
 
 </div>
 <div class="round1"></div>
 <div class="round2"></div>
 <div class="round4"></div>
 </div>
 <div class="pmeta_bubble_bg" style="border:1px solid white">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <div id="older_bubble">
 <p>Older revisions</p>
 
 
 <div class="closed" style="margin-bottom:3px;" >
 <img class="ifClosed" onclick="_toggleHidden(this)" src="http://www.gstatic.com/codesite/ph/images/plus.gif" >
 <img class="ifOpened" onclick="_toggleHidden(this)" src="http://www.gstatic.com/codesite/ph/images/minus.gif" >
 <a href="/p/datejs/source/detail?spec=svn195&r=190">r190</a>
 by ge...@coolite.com
 on May 12, 2008
 &nbsp; <a href="/p/datejs/source/diff?spec=svn195&r=190&amp;format=side&amp;path=/trunk/build/date-af-ZA.js&amp;old_path=/trunk/build/date-af-ZA.js&amp;old=189">Diff</a>
 <br>
 <pre class="ifOpened">--------------------
2008-05-12 [geoffrey.mcgill]
<a href="/p/datejs/source/detail?r=190">Revision #190</a>

1.  Added .today() equality check
...</pre>
 </div>
 
 <div class="closed" style="margin-bottom:3px;" >
 <img class="ifClosed" onclick="_toggleHidden(this)" src="http://www.gstatic.com/codesite/ph/images/plus.gif" >
 <img class="ifOpened" onclick="_toggleHidden(this)" src="http://www.gstatic.com/codesite/ph/images/minus.gif" >
 <a href="/p/datejs/source/detail?spec=svn195&r=189">r189</a>
 by ge...@coolite.com
 on May 07, 2008
 &nbsp; <a href="/p/datejs/source/diff?spec=svn195&r=189&amp;format=side&amp;path=/trunk/build/date-af-ZA.js&amp;old_path=/trunk/build/date-af-ZA.js&amp;old=187">Diff</a>
 <br>
 <pre class="ifOpened">--------------------
2008-05-07 [geoffrey.mcgill]
<a href="/p/datejs/source/detail?r=189">Revision #189</a>

1.  Fixed bug in TimePeriod. See http:
...</pre>
 </div>
 
 <div class="closed" style="margin-bottom:3px;" >
 <img class="ifClosed" onclick="_toggleHidden(this)" src="http://www.gstatic.com/codesite/ph/images/plus.gif" >
 <img class="ifOpened" onclick="_toggleHidden(this)" src="http://www.gstatic.com/codesite/ph/images/minus.gif" >
 <a href="/p/datejs/source/detail?spec=svn195&r=187">r187</a>
 by ge...@coolite.com
 on May 05, 2008
 &nbsp; <a href="/p/datejs/source/diff?spec=svn195&r=187&amp;format=side&amp;path=/trunk/build/date-af-ZA.js&amp;old_path=/trunk/build/date-af-ZA.js&amp;old=0">Diff</a>
 <br>
 <pre class="ifOpened">[No log message]</pre>
 </div>
 
 
 <a href="/p/datejs/source/list?path=/trunk/build/date-af-ZA.js&start=191">All revisions of this file</a>
 </div>
 </div>
 <div class="round1"></div>
 <div class="round2"></div>
 <div class="round4"></div>
 </div>
 <div class="pmeta_bubble_bg" style="border:1px solid white">
 <div class="round4"></div>
 <div class="round2"></div>
 <div class="round1"></div>
 <div class="box-inner">
 <div id="fileinfo_bubble">
 <p>File info</p>
 
 <div>Size: 30642 bytes,
 145 lines</div>
 
 <div><a href="http://datejs.googlecode.com/svn/trunk/build/date-af-ZA.js">View raw file</a></div>
 </div>
 
 </div>
 <div class="round1"></div>
 <div class="round2"></div>
 <div class="round4"></div>
 </div>
 </div>
 </div>


</div>
</div>

 <script src="http://www.gstatic.com/codesite/ph/1676283234471223295/js/prettify/prettify.js"></script>

<script type="text/javascript">prettyPrint();</script>

<script src="http://www.gstatic.com/codesite/ph/1676283234471223295/js/source_file_scripts.js"></script>

 <script type="text/javascript" src="http://kibbles.googlecode.com/files/kibbles-1.3.1.comp.js"></script>
 <script type="text/javascript">
 var lastStop = null;
 var initilized = false;
 
 function updateCursor(next, prev) {
 if (prev && prev.element) {
 prev.element.className = 'cursor_stop cursor_hidden';
 }
 if (next && next.element) {
 next.element.className = 'cursor_stop cursor';
 lastStop = next.index;
 }
 }
 
 function pubRevealed(data) {
 updateCursorForCell(data.cellId, 'cursor_stop cursor_hidden');
 if (initilized) {
 reloadCursors();
 }
 }
 
 function draftRevealed(data) {
 updateCursorForCell(data.cellId, 'cursor_stop cursor_hidden');
 if (initilized) {
 reloadCursors();
 }
 }
 
 function draftDestroyed(data) {
 updateCursorForCell(data.cellId, 'nocursor');
 if (initilized) {
 reloadCursors();
 }
 }
 function reloadCursors() {
 kibbles.skipper.reset();
 loadCursors();
 if (lastStop != null) {
 kibbles.skipper.setCurrentStop(lastStop);
 }
 }
 // possibly the simplest way to insert any newly added comments
 // is to update the class of the corresponding cursor row,
 // then refresh the entire list of rows.
 function updateCursorForCell(cellId, className) {
 var cell = document.getElementById(cellId);
 // we have to go two rows back to find the cursor location
 var row = getPreviousElement(cell.parentNode);
 row.className = className;
 }
 // returns the previous element, ignores text nodes.
 function getPreviousElement(e) {
 var element = e.previousSibling;
 if (element.nodeType == 3) {
 element = element.previousSibling;
 }
 if (element && element.tagName) {
 return element;
 }
 }
 function loadCursors() {
 // register our elements with skipper
 var elements = CR_getElements('*', 'cursor_stop');
 var len = elements.length;
 for (var i = 0; i < len; i++) {
 var element = elements[i]; 
 element.className = 'cursor_stop cursor_hidden';
 kibbles.skipper.append(element);
 }
 }
 function toggleComments() {
 CR_toggleCommentDisplay();
 reloadCursors();
 }
 function keysOnLoadHandler() {
 // setup skipper
 kibbles.skipper.addStopListener(
 kibbles.skipper.LISTENER_TYPE.PRE, updateCursor);
 // Set the 'offset' option to return the middle of the client area
 // an option can be a static value, or a callback
 kibbles.skipper.setOption('padding_top', 50);
 // Set the 'offset' option to return the middle of the client area
 // an option can be a static value, or a callback
 kibbles.skipper.setOption('padding_bottom', 100);
 // Register our keys
 kibbles.skipper.addFwdKey("n");
 kibbles.skipper.addRevKey("p");
 kibbles.keys.addKeyPressListener(
 'u', function() { window.location = detail_url; });
 kibbles.keys.addKeyPressListener(
 'r', function() { window.location = detail_url + '#publish'; });
 
 kibbles.keys.addKeyPressListener('j', gotoNextPage);
 kibbles.keys.addKeyPressListener('k', gotoPreviousPage);
 
 
 }
 window.onload = function() {keysOnLoadHandler();};
 </script>


<!-- code review support -->
<script src="http://www.gstatic.com/codesite/ph/1676283234471223295/js/code_review_scripts.js"></script>
<script type="text/javascript">
 
 // the comment form template
 var form = '<div class="draft"><div class="header"><span class="title">Draft comment:</span></div>' +
 '<div class="body"><form onsubmit="return false;"><textarea id="$ID">$BODY</textarea><br>$ACTIONS</form></div>' +
 '</div>';
 // the comment "plate" template used for both draft and published comment "plates".
 var draft_comment = '<div class="draft" ondblclick="$ONDBLCLICK">' +
 '<div class="header"><span class="title">Draft comment:</span><span class="actions">$ACTIONS</span></div>' +
 '<pre id="$ID" class="body">$BODY</pre>' +
 '</div>';
 var published_comment = '<div class="published">' +
 '<div class="header"><span class="title"><a href="$PROFILE_URL">$AUTHOR:</a></span><div>' +
 '<pre id="$ID" class="body">$BODY</pre>' +
 '</div>';

 function showPublishInstructions() {
 var element = document.getElementById('review_instr');
 if (element) {
 element.className = 'opened';
 }
 }
 function revsOnLoadHandler() {
 // register our source container with the commenting code
 var paths = {'svn191': '/trunk/build/date-af-ZA.js'}
 CR_setup('', 'p', 'datejs', '', 'svn195', paths,
 '125d90c7016dbfd9c828c03b95ed61d0', CR_BrowseIntegrationFactory);
 // register our hidden ui elements with the code commenting code ui builder.
 CR_registerLayoutElement('form', form);
 CR_registerLayoutElement('draft_comment', draft_comment);
 CR_registerLayoutElement('published_comment', published_comment);
 
 CR_registerActivityListener(CR_ACTIVITY_TYPE.REVEAL_DRAFT_PLATE, showPublishInstructions);
 
 CR_registerActivityListener(CR_ACTIVITY_TYPE.REVEAL_PUB_PLATE, pubRevealed);
 CR_registerActivityListener(CR_ACTIVITY_TYPE.REVEAL_DRAFT_PLATE, draftRevealed);
 CR_registerActivityListener(CR_ACTIVITY_TYPE.DISCARD_DRAFT_COMMENT, draftDestroyed);
 
 
 
 
 
 
 
 
 
 var initilized = true;
 reloadCursors();
 }
 window.onload = function() {keysOnLoadHandler(); revsOnLoadHandler();};
</script>

<script type="text/javascript" src="http://www.gstatic.com/codesite/ph/1676283234471223295/js/dit_scripts_20081013.js"></script>

 
 <script type="text/javascript" src="http://www.gstatic.com/codesite/ph/1676283234471223295/js/core_scripts_20081103.js"></script>
 <script type="text/javascript" src="/js/codesite_product_dictionary_ph.pack.04102009.js"></script>
 
 
 
 
 
 </div>
<div id="footer" dir="ltr">
 
 <div class="text">
 
 &copy;2009 Google -
 <a href="/">Code Home</a> -
 <a href="/projecthosting/terms.html">Terms of Service</a> -
 <a href="http://www.google.com/privacy.html">Privacy Policy</a> -
 <a href="/more/">Site Directory</a> -
 <a href="/p/support/">Project Hosting Help</a>
 
 </div>
</div>
<script type="text/javascript">
/**
 * Reports analytics.
 * It checks for the analytics functionality (window._gat) every 100ms
 * until the analytics script is fully loaded in order to invoke siteTracker.
 */
function _CS_reportAnalytics() {
 window.setTimeout(function() {
 if (window._gat) {
 try {
 siteTracker = _gat._getTracker(CS_ANALYTICS_ACCOUNT);
 siteTracker._trackPageview();
 } catch (e) {}
 var projectTracker = _gat._getTracker("UA-2699273-4");
projectTracker._initData();
projectTracker._trackPageview();
 } else {
 _CS_reportAnalytics();
 }
 }, 100);
}
</script>

 
 
 <div class="hostedBy" style="margin-top: -20px;">
 <span style="vertical-align: top;">Hosted by</span>
 <a href="/hosting/">
 <img src="http://www.gstatic.com/codesite/ph/images/google_code_tiny.png" width="107" height="24" alt="Google Code">
 </a>
 </div>
 
 
 
 


 
 </body>
</html>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
    </head>
    <body>

     <agiletags:ControllerForm name="CommissionGenerateForm">
        <input type="hidden" name="opperation" id="opperation" value="CommissionCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="Commission" />
        <input type="hidden" name="command" id="commissionCommand" value="genCommissionReport" />
    <label class="header">Generate Commissions Run Report</label>
    <hr>
    <br>
        <table>
<!--        <tr><td colspan="2" align="right"><input type="button" value="Generate" onclick="this.disabled = true; this.form.submit();"></td></tr> -->
        </table>
      </agiletags:ControllerForm>
    </body>
</html>

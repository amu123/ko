<%@tag description=""  dynamic-attributes="dynattrs" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@attribute name="title" required="true" type="java.lang.String" %>
<%@attribute name="command" required="false" type="java.lang.String" %>
<%@attribute name="action" required="false" type="java.lang.String" %>
<%@attribute name="actionId" required="false" type="java.lang.String" %>
<%@attribute name="reload" required="false" type="java.lang.Boolean"%>
<%@attribute name="collapsed" required="false" type="java.lang.Boolean"%>
<%@attribute name="optionList" required="false" type="java.util.ArrayList"%>
<%@attribute name="panelColor" required="false" type="java.lang.String"%>

<c:set value="" var="toggleCmd"/>
<c:if test="${not empty command}">
    <c:set var="toggleCmd" value=",'${command}'"/>
    <c:if test="${not empty action}">
        <c:set var="toggleCmd" value="${toggleCmd},'${action}'"/>
        <c:if test="${not empty actionId}">
            <c:set var="toggleCmd" value="${toggleCmd},'${actionId}'"/>
            <c:if test="${not empty reload}">
                <c:set var="toggleCmd" value="${toggleCmd},${reload}"/>
            </c:if>
        </c:if>
    </c:if>
</c:if>

<c:if test="${not empty optionList && not empty panelColor}">
    <div class="${collapsed == null || collapsed == true ? 'neooptionpanelcollapsed' : 'neooptionpanel'}" style="background: ${panelColor};">
        <div class="optPnlLst_dropdown" style="position: absolute; left: calc(100vw - 65px); margin-top:5px; padding:5vh 10px 5vh 28px;">
            <div class="optPnlLst-content" style="right: 1px; top:20px">
                <c:forEach items="${optionList}" var="entry">
                    <c:if test="${!(not empty nPanel.statusID && nPanel.statusID == 5 && nPanel.statusID == 6 && entry.id eq 'Change Status')}">
                        <a href="#" onclick="NeoOptionPanelSelect('${command}', '${entry.value}', ${actionId}, '33%, 67%')">${entry.id}</a>
                    </c:if>
                </c:forEach>
            </div>
        </div>
        <h2 style="text-align: left; padding-right:30px;" onclick="toggleNeoOptionPanel($(this).parent()${toggleCmd})">${title}</h2>
        <div class="neopanelcontent"><jsp:doBody/></div>
    </div>
</c:if>

<c:if test="${empty optionList || empty panelColor}">
    <div class="${collapsed == null || collapsed == true ? 'neopanelcollapsed' : 'neopanel'}">
        <h2 onclick="toggleNeoPanel($(this).parent()${toggleCmd})">${title}</h2>
        <div class="neopanelcontent"><jsp:doBody/></div>
    </div>
</c:if>


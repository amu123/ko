<%-- 
    Document   : BiometricTypeDropdownTag
    Created on : 16 Mar 2015, 6:59:35 PM
    Author     : gerritr
--%>

<%@tag description="Displays the Lookup_Values in a DropdownList" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ attribute name="javascript" rtexprvalue="true" required="true" %>
<%-- The list of normal or fragment attributes can be specified here: --%>
<td class="label" align="left" width="160px">Biometrics Type Selection:</td>
<td align="left" width="200px">
    <select style="width:215px" id="bioTypeSelect" name="bioTypeSelect" ${javascript}>
        <c:if test="${!empty sessionScope.BioTypeSelected}">
            <c:if test="${sessionScope.BioTypeSelected != null && BioTypeSelected eq 'null'}">
                <option value="0">&nbsp;</option>
                <option value="100">Chronic Disease List</option>
                <option value="291">Other Chronic Diseases</option>
            </c:if>
            <c:if test="${sessionScope.BioTypeSelected != null && BioTypeSelected eq 'ICD10'}">
                <option value="100">Chronic Disease List</option>
                <option value="291">Other Chronic Diseases</option>
            </c:if> 
            <c:if test="${sessionScope.BioTypeSelected != null && BioTypeSelected eq 'Other'}">
                <option value="291">Other Chronic Diseases</option>
                <option value="100">Chronic Disease List</option>
            </c:if>
        </c:if>
        <c:if test="${empty sessionScope.BioTypeSelected}">
            <option value="0">No Biometric Types available</option>
        </c:if>
    </select>   
</td>
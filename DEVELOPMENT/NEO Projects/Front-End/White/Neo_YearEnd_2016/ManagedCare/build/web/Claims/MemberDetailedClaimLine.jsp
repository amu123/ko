<%-- 
    Document   : MemberDetailedClaimLine
    Created on : Sep 23, 2011, 10:47:06 AM
    Author     : Johan-NB
--%>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.getElementById('onScreen').value = "MemberDetailedClaimLine.jsp";
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%>
            <tr valign="top">
                <td width="5px">

                </td>
                <td align="left">
                    <!-- content goes here -->
                    <table>
                        <tr valign="top"></tr>
                        <agiletags:ControllerForm name="claimsHistory">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                        </agiletags:ControllerForm>
                    </table>
                    <agiletags:ClaimLineDetailTag />
                    <br/>
                    <table>
                        <c:if test="${!empty sessionScope.returnFlag && (sessionScope.returnFlag eq true || sessionScope.returnFlag eq 'true')}">
                            <button name="opperation" type="button" onclick="submitWithAction('ViewSubWorkBenchCommand')" value="ViewSubWorkBenchCommand">Return</button>
                        </c:if>
                        <c:if test="${empty sessionScope.returnFlag || sessionScope.returnFlag eq false || sessionScope.returnFlag eq '' || sessionScope.returnFlag eq null}">
                            <tr><agiletags:ButtonOpperation align="right" span="5" type="button" commandName="ReturnMemberClaimDetails" displayname="Return" javaScript="onClick=\"submitWithAction('ReturnMemberClaimDetails');\""/></tr>
                        </c:if>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions' %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <!--        <script type="text/javascript">
                    $(document).ready(function(){
                        resizeContent();
                        //attach on resize event
                        $(window).resize(function() {
                            resizeContent();
                        });       
                    });
                    
                    
        
                 
                </script>-->
    </head>
    <body >
        <c:if test="${isFutureInception == true}">
            <label class="error">No summary details could be retrieved, member will only be active on ${coverInceptionDate}</label>
        </c:if>
        <c:if test="${empty memberSummaryDetails && isFutureInception == false}">
            <label class="error">No summary details could be retrieved, Member resigned more than two years ago</label>
        </c:if>
            <br/>
            <br/>
        <div id="tabTableLayout">
            <agiletags:ControllerForm name="MemberSummaryForm">

                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th align="centre">Month</th>
                        <th align="centre">Status</th>
                        <th align="centre">Reason</th>
                        <th align="centre">Group</th>
                        <th align="centre">Option</th>
                        <th align="centre">Dep</th>
                        <th align="centre">Broker Code</th>
                        <th align="centre">Core</th>
                        <th align="centre">Savings</th>
                        <th align="centre">LJP</th>
                        <th align="centre">Total</th>
                        <th align="centre">Payment Method</th>
                    </tr>

                    <c:forEach items="${memberSummaryDetails}" var="item">
                        <tr>
                            <td><label class="label">${item.summaryDate}</label></td>
                            <td><label class="label">${item.status}</label></td>
                            <td><label class="label">${item.statusReason}</label></td>
                            <td><label class="label">${item.group}</label></td>
                            <td><label class="label">${item.option}</label></td>
                            <td><label class="label">${item.dependentNumber}</label></td>
                            <td><label class="label">${item.brokerCode}</label></td>
                            <td><label class="label">${item.core}</label></td>
                            <td><label class="label">${item.savings}</label></td>
                            <td><label class="label">${item.ljp}</label></td>
                            <td><label class="label">${item.total}</label></td>
                            <td><label class="label">${item.paymentMethod}</label></td>
                        </tr>
                    </c:forEach>
                </table>
            </agiletags:ControllerForm>
    </body>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>

<script type='text/javascript' src="${pageContext.request.contextPath}/resources/Claims/Claims.js"></script>

<nt:NeoPanel title="Capture Claim Header"  collapsed="false" reload="false" contentClass="neopanelcontentwhite">
    <c:set var="a" value="adsss"></c:set>
    <agiletags:ControllerForm name="ClaimHeaderForm" > <!--enctype="multipart/form-data"-->
        <input type="hidden" name="opperation" id="ClaimHeader_opperation" value="ClaimHeaderCommand" />
        <input type="hidden" name="onScreen" id="ClaimHeader_onScreen" value="ClaimHeader" />
        <input type="hidden" name="ClaimHeader_batchId" id="ClaimHeader_batchId" value="" />
        <input type="hidden" name="ClaimHeader_claimId" id="ClaimHeader_claimId" value="" />
        <input type="hidden" name="command" id="ClaimHeader_command" value="saveClaimHeader" />

        <table id="ClaimHeaderTable">
            <tr><nt:NeoTextField displayName="Cover Number" elementName="ClaimHeader_CoverNumber" mandatory="true" type="search" searchFunc="swapDivVisbible('Main_Claim_Div','Search_Member_Div');" onblur="findClaimCoverNumber($(this).val());" /></tr>
            <tr><nt:NeoTextField displayName="Practice Number" elementName="ClaimHeader_PracticeNumber"  mandatory="true" type="search" searchFunc="swapDivVisbible('Main_Claim_Div', 'Search_Practice_Div')"  onblur="findClaimPracticeNumber($(this).val());" /></tr>
            <tr><nt:NeoTextField displayName="Provider Number" elementName="ClaimHeader_ProviderNumber"  mandatory="true" type="search" searchFunc="swapDivVisbible( 'Main_Claim_Div', 'Search_Provider_Div')"  onblur="findClaimProviderNumber($(this).val());" /></tr>
            <tr><nt:NeoTextField displayName="Category" elementName="ClaimHeader_Category" mandatory="false" disabled="disabled" /></tr>
            <tr><nt:NeoTextField displayName="Account Number" elementName="ClaimHeader_AccountNumber" mandatory="true"/></tr>
            <tr><nt:NeoTextField displayName="Referring Provider Number" elementName="ClaimHeader_ReferringProviderNumber"  type="search" searchFunc="swapDivVisbible( 'Main_Claim_Div', 'Search_Ref_Provider_Div')" onblur="findClaimRefProviderNumber($(this).val());" /></tr>
            <tr><nt:NeoTextField displayName="Referral Number" elementName="ClaimHeader_ReferalNumber" /></tr>
            <tr><nt:NeoTextField displayName="Date Received" elementName="ClaimHeader_DateReceived" mandatory="true" type="date"/></tr>
            <tr><nt:NeoTextField displayName="Claim Total" elementName="ClaimHeader_ClaimTotal" mandatory="true"/></tr>
            <!--<tr>\<nt:NeoTextField displayName="Currency" elementName="ClaimHeader_Currency"/></tr>-->
            <tr><nt:NeoTextField displayName="Scan Index Number" elementName="ClaimHeader_ScanIndexNumber"  /></tr>
            <tr><nt:NeoTextField displayName="Receipt By" elementName="ClaimHeader_ReceiptBy" lookupId="170"  /></tr>
            <tr><nt:NeoTextField displayName="Motivation Received" elementName="ClaimHeader_Motivation" type="checkbox"  /></tr>
                <table>
                    <tr>
                        <td><label>Paper Claim Document</label></td>
                        <td><input type="file" name="ClaimHeader_ClaimDocument" id="ClaimHeader_ClaimDocument" size="75" form="uploadForm"/></td>
                        <td><input type="submit" id="submitFile" value="Upload Document" form="uploadForm"/></td>
                    </tr>
                </table>
            <tr><td><input type="button" value="Save" onclick="saveClaimHeader(this.form, null, this);"></td></tr>
        </table>
    </agiletags:ControllerForm>
        <!--<agiletags:FileUploader action="SubmitClaimDocumentCommand" displayName="Paper Claim Document"/>-->
        <iframe name="uploadFrame" id="uploadFrame" style="display: none"></iframe>    
        <form id="uploadForm" enctype="multipart/form-data" method="POST" target="uploadFrame" action="/ManagedCare/AgileController?opperation=SubmitClaimDocumentCommand&command=upload">
        </form>
</nt:NeoPanel>

<div id="ClaimHeader_Summary">
        <jsp:include page="CaptureClaims_ClaimSummary.jsp" />
</div>        
<script>
    $(function() {
        var newDate = new Date();
        var month = newDate.getMonth() + 1;
        var day = newDate.getDate();
        if(month < 10) {
            month = "0" + month;
        }
        if (day  < 10) {
            day = "0" + day;
        }
        $('#ClaimHeader_DateReceived').val(newDate.getFullYear() + "/" + month + "/" + day);
    });
    
    //var newDate = new Date();
    //$('#ClaimHeader_DateReceived').val(newDate.getFullYear() + "/" + (newDate.getMonth() + 1) + "/" + (newDate.getDate()));
    
</script>
<%-- 
    Document   : MemAppDocumentGeneration
    Created on : 16 Aug 2013, 2:26:32 PM
    Author     : sphephelot
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.koh.serv.PropertiesReader"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%
    System.out.println("-------------MemberDocGen JSP------------------");



    String errorMsg = (String) request.getAttribute("errorMsg");
%>
<script type="text/javascript">
    function submitActionAndDocType(thisForm, action, docType){
        thisForm.opperation.value = action;
        thisForm.docType.value = docType;
        thisForm.memberAppCoverNumber.value = document.getElementById("memberAppCoverNumber").value;
        //    thisForm.submit(); 
        // submitFormWithAjaxPost(thisForm, 'MemberCoverDetails');
    }
</script>
<div>
    <form name="documentGeneration" id="documentGeneration" action="/ManagedCare/AgileController" method="POST">

        <table width=100% height=100%><tr valign="left"><td width="50px"></td><td align="left">
                    <label class="header">Document Generation</label>
                    <br/><br/><br/>
                    <table>
                        <% if (errorMsg != null) {%>
                        <tr><td><label id="error" class="error"><%=errorMsg%></label></td></tr>
                        <% }%>

                        <input type="hidden" name="entityId" id="entityId" value="" />
                        <input type="hidden" name="memberEntityId" id="memberEntityId" value="${memberAppCoverNumber}" /> 
                        <input type="hidden" name="memberAppCoverNumber" id="memberEntityId" value="${memberAppCoverNumber}" />
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <input type="hidden" name="onScreen" id="onScreen" value="" />
                        <input type="hidden" name="docType" id="docType" value="" />
                        <tr><td>
                                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                    <tr><th>Document Type</th><th>Document Name</th><th></th></tr>                                    
                                    <tr><td><label class="label">Terms Of Acceptance</label></td>
                                        <td><label id="DocGen_termsOfAcceptance" class="label"></label></td>
                                        <td><input id="termsOfAcceptanceButton" type="button" value="Generate" onClick="submitActionAndDocType(this.form, 'MemberAppDocumentGenerationCommand','termsOfAcceptance'); submitFormWithAjaxPost(this.form, 'DocumentGeneration');" /></td></tr>
                                </table>
                            </td></tr>
                    </table>
                </td></tr></table>
    </form>
</div>


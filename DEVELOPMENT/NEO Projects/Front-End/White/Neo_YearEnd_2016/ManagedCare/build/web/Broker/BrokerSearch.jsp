<%-- 
    Document   : BrokerSearch
    Created on : 2012/06/21, 03:10:54
    Author     : princes
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>

        <script>
            function viewMemberApp(form, id, target) {
//                document.getElementById("employerId").value = id;
                submitFormWithAjaxPost(form, target);
            }

            function setMemberAppValues(id, name, entityId, title, person , lastName, region, vipBrokerInd) {
                document.getElementById("brokerCodeRes").value = id;
                document.getElementById("brokerNameRes").value = name;
                document.getElementById("brokerEntityId").value = entityId;
                document.getElementById("brokerTitle").value = title;
                document.getElementById("brokerContactPerson").value = person;
                document.getElementById("brokerContactSurname").value = lastName;
                document.getElementById("brokerRegion").value = region;
                document.getElementById("vipBrokerInd").value = vipBrokerInd;
                
            }
        </script>
    </head>
    <body>

    <label class="header">Broker Search</label>
    <hr>
    <br>
     <agiletags:ControllerForm name="BrokerSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="BrokerSearchCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="BrokerApplicationSearch" />
        <table>
            <tr><agiletags:LabelTextBoxError displayName="Broker Code" elementName="brokerCode"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Broker Surname" elementName="brokerName"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="ID Number" elementName="IdNumber"/></tr>
            <tr><td colspan="2" align="right"><input type="button" value="Search" onclick="viewMemberApp(this.form, 0, 'BrokerResults')"></td></tr>
        </table>
      </agiletags:ControllerForm>

     <agiletags:ControllerForm name="BrokerSearchResultsForm">
        <input type="hidden" name="opperation" id="search_opperation" value="BrokerViewCommand" />
        <input type="hidden" name="onScreen" id="search_onScreen" value="BrokerResults" />
        <input type="hidden" name="brokerCode" id="brokerCodeRes" value="">
        <input type="hidden" name="brokerName" id="brokerNameRes" value="">
        <input type="hidden" name="brokerEntityId" id="brokerEntityId" value="" />
        <input type="hidden" name="brokerTitle" id="brokerTitle" value="" />
        <input type="hidden" name="brokerContactPerson" id="brokerContactPerson" value="" />
        <input type="hidden" name="brokerContactSurname" id="brokerContactSurname" value="" />
        <input type="hidden" name="brokerRegion" id="brokerRegion" value="" />
        <input type="hidden" name="vipBrokerInd" id="vipBrokerInd" value="" />
      <div style ="display: none"  id="BrokerResults"></div>
      </agiletags:ControllerForm>

    </body>
</html>


<%-- 
    Document   : AssignPHM
    Created on : 2010/04/14, 01:47:48
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Assign PHM</title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            $(document).ready(function () {
                $("#historyTable").hide();
            });

            function showTable() {
                $("#historyTable").show();
            }

        </script>          
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <agiletags:PDCTabs tabSelectedIndex="link_RA" />
                    <fieldset id="pageBorder">
                        <div class="">
                            <br/><br/>
                            <label class="header">Assign PHM</label>
                            <br/><br/>

                            <agiletags:ControllerForm name="assignPHM">
                                <table>
                                    <input type="hidden" name="opperation" id="opperation" value=""/>
                                    <tr><agiletags:LabelNoeUserByResposibilityDropDown elementName="assginPMH" displayName="Assign To" resposibilityId="20"/></tr>                            
                                    <tr><agiletags:LabelNeoLookupValueDropDown displayName="Reason for Assignment" elementName="reasonForAssignment" lookupId="99"/></tr>
                                    <tr><agiletags:LabelCheckboxSingle displayName="Notify Patient" elementName="notifyPatient" value="yes"/></tr>
                                    <tr><agiletags:LabelTextAreaError displayName="Details to new case manager" elementName="caseDetails"/></tr>
                                </table>
                                <table>
                                    <tr>
                                        <agiletags:ButtonOpperation align="left" displayname="Assign" commandName="AssignPHMCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('AssignPHMCommand')\";"/>
                                        <td><button align="left" displayname="History" span="4" type="button" onclick="showTable();" >History</button></td>
                                    </tr>
                                </table>
                                <br/>
                                <agiletags:AssignHistory/>
                            </agiletags:ControllerForm>
                        </div></fieldset>
                </td></tr></table>
    </body>
</html>


<%-- 
    Document   : BiometricsTabs
    Created on : 02 Feb 2015, 2:27:27 PM
    Author     : martins
--%>

<%@tag description="biometrics submenu item tabs tag" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="tabSelectedBIndex"%>

<%-- any content can be specified here e.g.: --%>
<div id="header" style="position: relative; width: 100%"> <%--class="header_area"--%>
    <ol id="tabs">
        <%=session.getAttribute("Atabs")%>
      <%--       
        <li><a id="link_AST" onClick="load_condition(this.id);" href="#Asthma" >Asthma</a></li>
        <li><a id="link_HTENS" onClick="load_Hypertension();" href="#Hypertension">Hypertension</a></li>
        <li><a id="link_CRD" onClick="load_ChronicRenal();" href="#ChronicalRenalDisease">Chronical Renal Disease</a></li>
        <li><a id="link_CF" onClick="load_CardiacFailure();" href="#CardiacFailure">Cardiac Failure / Cardio Myopathy</a></li>
        <li><a id="link_DIA" onClick="load_Diabetes();" href="#Diabetes">Diabetes</a></li>
        <li><a id="link_CAD" onClick="load_CoronaryDisease();" href="#CoronaryArteryDisease">Coronary Artery Disease</a></li>
        <li><a id="link_CB" onClick="load_COPD();" href="#COPD">COPD and Bronchiectasis</a></li>
        <li><a id="link_HLIP" onClick="load_Hyperlipidaemia();" href="#Hyperlipidaemia">Hyperlipidaemia</a></li>
        <li><a id="link_MD" onClick="load_MajorDepression();" href="#Major Depression">Major Depression</a></li>
        <li><a id="link_CDLOTH" onClick="submitWithAction('ViewCDLStatusCommand');" href="#PDCCDLStatus">Other</a></li> --%> 
    </ol>
</div>
<%--      <script>setSelectedBTab('${tabSelectedBIndex}');</script>     --%>
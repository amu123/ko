<%--
    Document   : ForgotPassword
    Created on : 2010/01/14, 09:59:47
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
    </head>
    <body>
        <agiletags:ControllerForm name="forgotPassword">
        <table width=100% height=100%><tr valign="center">
            <td align="center">
            <table cellpadding="4" cellspacing="1" align="center">
                <label class="header">Forgot Password</label>
                <br/><br/>

                <tr><td align="right"><label>User name: </label></td><td><input type="text" name="userName" value="" size="25" /></td></tr>

                <tr><agiletags:ButtonOpperation type="submit" align="left" commandName="SendForgotPasswordCommand" displayname="Submit" span="3"/></tr>
            </table>
            </td></tr></table>
            </agiletags:ControllerForm>
    </body>
</html>


<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="BroherAppNotesForm">
    <input type="hidden" name="opperation" id="BrokerAppNotes_opperation" value="BrokerNotesCommand" />
    <input type="hidden" name="onScreen" id="BrokerAppNotes_onScreen" value="BrokerAppNotes" />
    <input type="hidden" name="memberAppNumber" value="${memberAppNumber}" />
    <input type="hidden" name="noteId" id="BrokerAppNotes_note_id" value="" />
    <input type="hidden" name="buttonPressed" id="BrokerAppNotes_button_pressed" value="" />
     <input type="hidden" name="brokerEntityId" id="brokerNotesEntityId" value="${brokerEntityId}" />
    <label class="header">Broker Notes</label>
    <hr>

      <table id="BrokerAppNotesTable">
          <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Details"  elementName="notesListSelect" lookupId="216" mandatory="no" errorValueFromSession="no" javaScript="onchange=\"document.getElementById('BrokerAppNotes_button_pressed').value='SelectButton';submitFormWithAjaxPost(this.form, 'BrokerAppNotesDetails');\"" />        </tr>
      </table>          
 <div style ="display: none"  id="BrokerAppNotesDetails"></div>
 </agiletags:ControllerForm>
   

<%-- 
    Document   : IndividualStatements
    Created on : 2010/03/15, 10:30:53
    Author     : whauger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script language="JavaScript">

            function validateDate(dateString) {
                //  check for valid numeric strings
                var strValidChars = "0123456789/";
                var strChar;
                var strChar2;
                var blnResult = true;

                if (dateString.length < 10 || dateString.length > 10) return false;

                //  test strString consists of valid characters listed above
                for (i = 0; i < dateString.length; i++)
                {
                    strChar = dateString.charAt(i);
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        return false;
                    }
                }

                // test if slash is at correct position (yyyy/mm/dd)
                strChar = dateString.charAt(4);
                strChar2 = dateString.charAt(7);
                if (strChar != '/' || strChar2 != '/') return false;
                return blnResult;
            }

            function getPaymentRunDates(element){
                xmlhttp=GetXmlHttpObject();
                if (xmlhttp==null)
                {
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                var fromDate = document.getElementById('statementFromDate').value;
                var toDate = document.getElementById('statementToDate').value;
                if (validateDate(fromDate) && validateDate(toDate)) {
                    url=url+"?opperation=GetPaymentRunDatesCommand&fromDate="+ fromDate + "&toDate=" + toDate + "&element=" + element;
                    xmlhttp.onreadystatechange=function(){stateChanged(element)};
                    xmlhttp.open("POST",url,true);
                    xmlhttp.send(null);
                } else {
                    document.getElementById(element).options.length = 0;
                }
            }

            function getPrincipalMemberForNumber(str, element){
                xmlhttp=GetXmlHttpObject();
                if (xmlhttp==null)
                {
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                if (str.length > 0) {
                    var url="/ManagedCare/AgileController";
                    url=url+"?opperation=GetMemberByNumberCommand&number="+ str + "&element=" + element;
                    xmlhttp.onreadystatechange=function(){stateChanged(element)};
                    xmlhttp.open("POST",url,true);
                    xmlhttp.send(null);
                }
            }

            function getProviderForNumber(str, element){
                xmlhttp=GetXmlHttpObject();
                if (xmlhttp==null)
                {
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=GetProviderByNumberCommand&number="+ str + "&element=" + element;
                xmlhttp.onreadystatechange=function(){stateChanged(element)};
                xmlhttp.open("POST",url,true);
                xmlhttp.send(null);
            }

            function getPracticeForNumber(str, element){
                xmlhttp=GetXmlHttpObject();
                if (xmlhttp==null)
                {
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var url="/ManagedCare/AgileController";
                url=url+"?opperation=GetPracticeByNumberCommand&number="+ str + "&element=" + element;
                xmlhttp.onreadystatechange=function(){stateChanged(element)};
                xmlhttp.open("POST",url,true);
                xmlhttp.send(null);
            }

            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }
            function stateChanged(element){
                if (xmlhttp.readyState==4)
                {
                    //alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    //alert(result);
                    if(result == "GetPaymentRunDatesCommand"){
                        var dateString = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=')+1, xmlhttp.responseText.lastIndexOf('$'));
                        var dates = new Array();
                        dates = dateString.split(',');
                        document.getElementById(element).options.length = 0;
                        for(i=0;i<dates.length;i++) {
                            var optVal = document.createElement('OPTION');
                            optVal.value = dates[i];
                            optVal.text = dates[i];
                            document.getElementById(element).options.add(optVal);
                        }
                        document.getElementById('emailAddress').focus();
                    }else if(result == "GetProviderByNumberCommand"){
                        var number = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=')+1, xmlhttp.responseText.lastIndexOf('|'));
                        var email = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=')+1, xmlhttp.responseText.lastIndexOf('$'));
                        document.getElementById(element + '_error').innerText = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('emailAddress').value = email;
                        document.getElementById('searchCalled').value = '';
                    }else if(result == "GetPracticeByNumberCommand"){
                        var number = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=')+1, xmlhttp.responseText.lastIndexOf('|'));
                        var email = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=')+1, xmlhttp.responseText.lastIndexOf('$'));
                        document.getElementById(element + '_error').innerText = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('emailAddress').value = email;
                        document.getElementById('searchCalled').value = '';
                    }else if(result == "GetMemberByNumberCommand"){
                                                                                
                        var resultArr = xmlhttp.responseText.split("|");
                        var numberArr = resultArr[1].split("=");
                        var emailArr = resultArr[2].split("=");
                        var productArr = resultArr[3].split("=");
                        
                        var number = numberArr[1];
                        var email = emailArr[1];
                        var product = productArr[1];
                        document.getElementById(element + '_error').innerText = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('emailAddress').value = email;
                        document.getElementById('searchCalled').value = '';
                        document.getElementById('productId').value = product;
                    }else if(result == "Error"){
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|')+1, xmlhttp.responseText.length);
                        if(forCommand == "GetPaymentRunDatesCommand"){
                            document.getElementById(element).options.length = 0;
                        } else if(forCommand == "GetProviderByNumberCommand"){
                            document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element + '_text').value = '';
                            document.getElementById('emailAddress').value = '';
                            document.getElementById('searchCalled').value = '';
                        } else if(forCommand == "GetPracticeByNumberCommand"){
                            document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element + '_text').value = '';
                            document.getElementById('emailAddress').value = '';
                            document.getElementById('searchCalled').value = '';
                        } else if(forCommand == "GetMemberByNumberCommand"){
                            document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element + '_text').value = '';
                            document.getElementById('emailAddress').value = '';
                            document.getElementById('searchCalled').value = '';
                        }
                    }
                }
            }

            function toggle(src, destId, newId) {
                var value = document.getElementById(src).value;
                //alert(value);
                if(value == '2'){
                    document.getElementById(newId).style.display = 'none';
                    document.getElementById(destId).style.display = '';
                    document.getElementById('emailAddress').value = '';
                }else{
                    document.getElementById(destId).style.display = 'none';
                    document.getElementById(newId).style.display = '';
                    document.getElementById('emailAddress').value = '';
                }

                //alert(document.getElementById('searchCalled').value);
                if (document.getElementById('searchCalled').value == 'memberNo')  {
                    //alert(document.getElementById('memberNumber_text').value);
                    getPrincipalMemberForNumber(document.getElementById('memberNo_text').value, 'memberNo');
                }
                if (document.getElementById('searchCalled').value == 'providerNo')  {
                    //alert(document.getElementById('providerNumber_text').value);
                    getProviderForNumber(document.getElementById('providerNo_text').value, 'providerNo');
                }
            }

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            
            function submitWithAction(action, actionParam) {
                document.getElementById('opperation').value = action;
                document.getElementById('opperationParam').value = actionParam;
                document.forms[0].submit();
            }

            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, actionParam, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.getElementById('opperationParam').value = actionParam;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body onLoad="toggle('statementType', 'providerType', 'memberType');">
        <table width=100% height=100%><tr valign="left"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Statement Request</label>
                    <br></br><br></br>
                    <table>
                        <agiletags:ControllerForm name="sendStatement" validate="yes">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="providerDetails" id="providerDetails" value="email" />
                            <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                            <input type="hidden" name="productId" id="productId" value="" />
                            <input type="hidden" name="exactCoverNum" id="exactCoverNum" value="1" />
                            <tr><agiletags:LabelNeoLookupValueDropDown displayName="Type" elementName="statementType" lookupId="108" javaScript="onchange=\"toggle('statementType', 'providerType', 'memberType');\""/></tr>
                            <tr id="memberType" style="">
                                <!--agiletags:LabelTextBoxError displayName="Member number" elementName="memberNo" javaScript="onChange=\"getPrincipalMemberForNumber(this.value, 'memberNo');\""/>-->
                                <agiletags:LabelTextSearchText displayName="Member number" elementName="memberNo" valueFromSession="Yes" onScreen="/Statement/IndividualStatements.jsp" searchFunction="Yes" searchOperation="ForwardToSearchMemberCommand" javaScript="onChange=\"getPrincipalMemberForNumber(this.value, 'memberNo');\""/>
                            </tr>
                            <tr id="providerType" style="display:none">
                                <!--agiletags:LabelTextBoxError displayName="Practice number" elementName="providerNo" javaScript="onChange=\"getPracticeForNumber(this.value, 'providerNo');\""/>-->
                                <agiletags:LabelTextSearchText displayName="Practice number" elementName="providerNo" valueFromSession="Yes" onScreen="/Statement/IndividualStatements.jsp" searchFunction="Yes" searchOperation="ForwardToSearchProviderCommand" javaScript="onChange=\"getPracticeForNumber(this.value, 'providerNo');\""/>
                            </tr>
                            <tr><agiletags:LabelTextBoxDate displayname="Date from" elementName="statementFromDate" javascript="onChange=\"getPaymentRunDates('paymentRunDate');\""/></tr>
                            <tr><agiletags:LabelTextBoxDate displayname="Date to" elementName="statementToDate" javascript="onChange=\"getPaymentRunDates('paymentRunDate');\""/></tr>
                            <tr><agiletags:LabelDropDownButton displayName="Payment run date" elementName="paymentRunDate" javaScript="onClick=\"getPaymentRunDates('paymentRunDate');\""/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="E-Mail address" elementName="emailAddress"/></tr>
                            <tr>
                                <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Cancel" javaScript="onClick=\"submitWithAction('ReloadIndividualStatementCommand')\";"/>
                                <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Preview" javaScript="onClick=\"submitWithAction('SubmitIndividualStatementCommand', 'Preview')\";"/>
                                <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Submit" javaScript="onClick=\"submitWithAction('SubmitIndividualStatementCommand', 'Send')\";"/>
                            </tr>
                        </agiletags:ControllerForm>
                    </table>

                    <!-- content ends here -->
        </td></tr></table>
    </body>
</html>

<%-- 
    Document   : BiometricDetails
    Created on : 2010/04/15, 03:50:16
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Biometrics</title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" language="JavaScript">
            var cdlHolder = '1';

            $(document).ready(function () {
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });

                var resultTxt = $("#pdcBiometricResultMsg").val();
                if (resultTxt != null && resultTxt != "" && resultTxt != "null") {
                    $("#pdcBiometricResultMsgTxt").text(resultTxt);
                } else {
                    $("#pdcBiometricResultMsgTxt").text("");
                }

                if (document.getElementById('searchCalled').value === "showTable") {
                    var index = document.getElementById('historyTableFlag').value;
                    document.getElementById(index).style.display = 'block';
                    document.getElementById('summaryTable').style.display = 'block';
                } else {
                    document.getElementById('summaryTable').style.display = 'none';
                    document.getElementById('searchCalled').value = '';
                }

            });

            function viewBiometricsWithAction(biometricId, action, bioName) {
                document.getElementById('opperation').value = action;
                document.getElementById('biometricID').value = biometricId;
                document.getElementById('bioName').value = bioName;
                document.forms[0].submit();
            }

            function showOrHide(index) {
                document.getElementById(1).style.display = 'none';
                document.getElementById(2).style.display = 'none';
                document.getElementById(3).style.display = 'none';
                document.getElementById(4).style.display = 'none';
                document.getElementById(5).style.display = 'none';
                document.getElementById(6).style.display = 'none';
                document.getElementById(7).style.display = 'none';
                document.getElementById(8).style.display = 'none';
                document.getElementById(9).style.display = 'none';
                document.getElementById(10).style.display = 'none';
                document.getElementById(11).style.display = 'none';
                document.getElementById('summaryTable').style.display = 'none';
                document.getElementById('cdlDropdown').value = index;

                if (${sessionScope.ID_text == 100}) {
                    document.getElementById(index).style.display = 'block';
                } else {
                    index = "11";
                    document.getElementById(index).style.display = 'block';
                }
            }

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function ClearValidation() {
                $("#dateFrom_error").text("");
                $("#dateTo_error").text("");
            }

            function ClearValidationContents() {
                document.getElementById('dateFrom').value = "";
                document.getElementById('dateTo').value = "";
            }

            function submitWithActionForm(action) {
                var dateFrom = $("#dateFrom").val("");
                var dateTo = $("#dateTo").val("");

                var valid = true;
                ClearValidation();

                /* if (dateFrom != null && dateFrom != "" && dateFrom != "null") {
                 valid = validateDate(dateFrom);
                 if (!valid) {
                 $("#dateFrom_error").text("Incorrect Date Format");
                 }
                 if (dateTo == null || dateTo == "" || dateTo == "null") {
                 valid = false;
                 $("#dateTo_error").text("Please enter to date");
                 }
                 }
                 if (dateTo != null && dateTo != "" && dateTo != "null") {
                 valid = validateDate(dateTo);
                 if (!valid) {
                 $("#dateTo_error").text("Incorrect Date Format");
                 }
                 if (dateFrom == null || dateFrom == "" || dateFrom == "null") {
                 valid = false;
                 $("#dateFrom_error").text("Please enter to date");
                 } 
                 } */

                if (valid) {

                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }
            }

            function validateDate(dateString) {

                //  check for valid numeric strings
                var strValidChars = "0123456789/";
                var strChar;
                var strChar2;

                if (dateString.length < 10 || dateString.length > 10)
                    return false;

                //  test strString consists of valid characters listed above
                for (i = 0; i < dateString.length; i++)
                {
                    strChar = dateString.charAt(i);
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        return false;
                    }
                }

                // test if slash is at correct position (yyyy/mm/dd)
                strChar = dateString.charAt(4);
                strChar2 = dateString.charAt(7);
                if (strChar != '/' || strChar2 != '/')
                    return false;

                return true;
            }

            function getDepHistory(ID, action) {
                document.getElementById('onScreen').value = "/PDC/BiometricDetails.jsp";
                document.getElementById('ID_text').value = ID;
                var dateFrom = $("#dateFrom").val("");
                var dateTo = $("#dateTo").val("");

                var valid = true;
                ClearValidation();

                /* if (dateFrom !== null && dateFrom !== "" && dateFrom !== "null") {
                 valid = validateDate(dateFrom);
                 if (!valid) {
                 $("#dateFrom_error").text("Incorrect Date Format");
                 }
                 if (dateTo === null || dateTo === "" || dateTo === "null") {
                 valid = false;
                 $("#dateTo_error").text("Please enter to date");
                 }
                 }
                 if (dateTo !== null && dateTo !== "" && dateTo !== "null") {
                 valid = validateDate(dateTo);
                 if (!valid) {
                 $("#dateTo_error").text("Incorrect Date Format");
                 }
                 if (dateFrom === null || dateFrom === "" || dateFrom === "null") {
                 valid = false;
                 $("#dateFrom_error").text("Please enter to date");
                 }
                 } */

                if (valid) {
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }
            }

            function setBioDropdownVisible(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <agiletags:PDCTabs tabSelectedIndex="link_Bio" />
                    <fieldset id="pageBorder">
                        <div class="content_area" >
                            <label class="header">Biometric Details</label>
                            <br/>
                            <br/>
                            <agiletags:ControllerForm name="biometricDetails">
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <input type="hidden" name="bioName" id="bioName" value="" />
                                <input type="hidden" name="biometricID" id="biometricID" value="" />
                                <input type="hidden" name="onScreen" id="onScreen" value="" />
                                <agiletags:HiddenField elementName="cdlDropdown" />
                                <agiletags:HiddenField elementName="searchCalled" />
                                <agiletags:HiddenField elementName="historyTableFlag" />
                                <agiletags:HiddenField elementName="ID_text" />
                                <input type="hidden" name="pdcBiometricResultMsg" id="pdcBiometricResultMsg" value="${requestScope.pdcBiometricResultMsg}"/>

                                <table>
                                    <tr><agiletags:LabelTextBoxDate displayname="Date From" elementName="dateFrom" valueFromSession="yes" mandatory="no"/></tr>
                                    <tr><agiletags:LabelTextBoxDate displayname="Date To" elementName="dateTo" valueFromSession="yes" mandatory="no"/></tr>
                                    <tr><agiletags:EntityCoverDependantDropDown displayName="Cover Dependants" elementName="depListValues" javaScript="onChange=\"setBioDropdownVisible('RefreshPdcBiometricsCommand');\"" mandatory="no" valueFromSession="yes"/></tr>
                                    <tr><agiletags:BiometricTypeDropdownTag javascript="onChange=\"getDepHistory(this.value, 'PreviewBiometricsCommand');\""/></tr>
                                    <tr><agiletags:CdlByCoverNumberTag id="cdl" displayName="CDL" javascript="onchange=\"showOrHide(this.value);\""/></tr>
                                </table>
                                <table>
                                    <tr><label id="pdcBiometricResultMsgTxt" class="red"></label></tr>
                                </table>
                                <br/>
                                <table>
                                    <tr>
                                        <td><agiletags:ButtonOpperation align="right" span="1" type="button" commandName="NavigateBiometricsCommand" displayname="Capture New" javaScript="onClick=\"submitWithAction('NavigateBiometricsCommand');\""/></td>
                                    </tr>
                                </table>

                                <div id="1" style="display:none">
                                    <br/>
                                    <label class="header">Policy Holder - Biometric Asthma</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:BiometricAsthmaTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'Asthma');\""/>
                                    </table>
                                </div>

                                <div id="2" style="display:none">
                                    <br/>
                                    <label class="header">Policy Holder - Biometric Hypertension</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:BiometricHypertensionTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'Hypertension');\""/>
                                    </table>
                                </div>

                                <div id="3" style="display:none"> 
                                    <br/>
                                    <label class="header">Policy Holder - Chronic renal Disease</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:BiometricChronicRenalDiseaseTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'ChronicRenalDisease');\""/>
                                    </table>
                                </div>

                                <div id="4" style="display:none">
                                    <br/>
                                    <label class="header">Policy Holder - Cardiac Failure/Cardio Myopathy</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:BiometricCardiacFailureAndCardiacMyothy javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'CardiacFailure');\""/>
                                    </table>
                                </div>

                                <div id="5" style="display:none"> 
                                    <br/>
                                    <label class="header">Policy Holder - Biometric Diabetes</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:BiometricDiabetesTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'Diabetes');\""/>
                                    </table>
                                </div>

                                <div id="6" style="display:none"> 
                                    <br/>
                                    <label class="header">Policy Holder - Coronary Artery Disease</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:BiometricCoronaryArteryDiseaseTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'Coronary');\""/>
                                    </table>
                                </div>

                                <div id="7" style="display:none">
                                    <br/>
                                    <label class="header">Policy Holder - COPD and Bronchiectasis</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:BiometricCOPDandBronchiectasisTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'Bronchiectasis');\""/>
                                    </table>
                                </div>

                                <div id="8" style="display:none">
                                    <br/>
                                    <label class="header">Policy Holder - Hyperlipidaemia</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:BiometricHyperlipidaemiaTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'Hyperlipidaemia');\""/>
                                    </table>
                                </div>

                                <div id="9" style="display:none"> 
                                    <br/>
                                    <label class="header">Policy Holder - Major Depression</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:BiometricMajorDepressionTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'MajorDepression');\""/>
                                    </table>
                                </div>
                                <div id="10" style="display:none"> 
                                    <br/>
                                    <label class="header">Policy Holder - Biometric General</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:BiometricsGeneralTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'General');\""/>
                                    </table>
                                </div>

                                <div id="11" style="display:none">
                                    <br/>
                                    <label class="header">Policy Holder - Biometric Other</label>
                                    <br/><br/>
                                    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                        <agiletags:BiometricsOtherTable javascript="onClick=\"viewBiometricsWithAction(this.value, 'ViewBiometricCommand', 'Other');\""/>
                                    </table>
                                </div>    
                                <div id="summaryTable" style="display:none">
                                    <br><br>
                                    <agiletags:BiometricTableTag/>
                                </div>
                            </agiletags:ControllerForm>
                        </div>
                    </fieldset>
                </td>
            </tr>
        </table>
    </body>
</html>
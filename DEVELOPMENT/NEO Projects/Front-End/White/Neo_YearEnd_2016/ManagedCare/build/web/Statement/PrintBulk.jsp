<%-- 
    Document   : MemberDocument
    Created on : 02 Jul 2012, 3:34:17 PM
    Author     : Christo
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%
    String errorMsg = (String)request.getAttribute("errorMsg");
    
    String displayOption = (String)request.getAttribute("displayOption");
    String displayDay = (String)request.getAttribute("displayDay");
    String displayGroup = (String)request.getAttribute("displayGroup");
    String displayType = (String)request.getAttribute("displayType");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>

        <script language="JavaScript">
            function submitF() {
                document.getElementById('opperation').value = 'PrintBulkCommand';
                document.forms[0].submit();
            }            
            function setParams(a,b,c){
                document.getElementById('opperation').value = a;
                document.getElementById('selectedFile').value = b;
                document.getElementById('selectedId').value = c;
//                document.forms[0].submit();
            }
        </script>
    </head>
   
<body onLoad="submitF();">
        <table width=100% height=100%><tr valign="left"><td width="50px"></td><td align="left">
                    <label class="header">Print Pool</label>
                    <br/><br/><br/>
                    <table>
                        <form name="printBulk" > 
                        <input type="hidden" name="displayOption" id="displayOption" value="<%=displayOption%>" />
                        <input type="hidden" name="displayDay" id="displayDay" value="<%=displayDay%>" />
                        <input type="hidden" name="displayGroup" id="displayGroup" value="<%=displayGroup%>" />
                        <input type="hidden" name="displayType" id="displayType" value="<%=displayType%>" />
                        <input type="hidden" name="opperation" id="opperation" value="PrintBulkCommand" />

                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <tr><td>
                                    <table>
                                        <tr>
                                            <td><label id="print" >Printing...</label></td>
                                        </tr>
                                    </table>
                                </td></tr>
                            </form> 
                    </table>
          </td></tr></table>
    </body>
</html>


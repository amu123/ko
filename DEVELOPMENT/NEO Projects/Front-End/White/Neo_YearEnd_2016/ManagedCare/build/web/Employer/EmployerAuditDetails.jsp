<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <br>
    <label class="subheader">Employer Audit Trail Details</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty EmployerAuditDetails}">
                <span class="lable">No Audit details available</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <c:forEach var="headerMap" items="${EmployerAuditDetails[0]}">
                            <th>${headerMap.key}</th>
                        </c:forEach>           
                    </tr>
                    <c:forEach var="entry" items="${EmployerAuditDetails}">
                        <tr>
                            <c:forEach var="item" items="${entry}">
                                <td><label class="label">${item.value}</label></td>
                            </c:forEach>
                        </tr>
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>
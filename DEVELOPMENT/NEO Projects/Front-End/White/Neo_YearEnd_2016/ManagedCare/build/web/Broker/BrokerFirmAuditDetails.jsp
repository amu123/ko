<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <br>
    <label class="subheader">Broker Firm Audit Trail Details</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty FirmAuditDetails}">
            <span>No Audit details available</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <c:forEach var="headerMap" items="${FirmAuditDetails[0]}">
                            <th>${headerMap.key}</th>
                        </c:forEach>           
                    </tr>
                    <c:forEach var="entry" items="${FirmAuditDetails}">
                        <tr>
                            <c:forEach var="item" items="${entry}">
                                <td><label class="label">${item.value}</label></td>
                            </c:forEach>
                        </tr>
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>

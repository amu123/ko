<%-- 
    Document   : MemberDocument
    Created on : 02 Jul 2012, 3:34:17 PM
    Author     : Christo
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%
    String errorMsg = (String)request.getAttribute("errorMsg");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script language="JavaScript">
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }            
            function setParams(){
             
            
            }
        </script>
    </head>
    <body>

        <table width=100%><tr valign="left"><td width="50px"></td><td align="left">
                    <label class="header">Document List</label>
                    <br/><br/><br/>
                    <table>
                        <% if(errorMsg != null){ %>
                        <tr><td><label id="error" class="error"><%=errorMsg%></label></td></tr>
                        <% } %>

                        <agiletags:ControllerForm name="documentIndexing"> 
                        <input type="hidden" name="folderList" id="folderList" value="" />
                        <input type="hidden" name="listOrWork" id="listOrWork" value="" />
                            <input type="hidden" name="opperation" id="opperation" value="MemberDocumentSearchCommand" />
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                            <tr><td>
                                    <table>
                                        <tr>
                                        <td align="left" width="160px"><label>Member Number:</label></td><td align="left" width="200px"><input type="text" id="memberNumber" name="memberNumber" value="" size="30" /></td>
                                        </tr>
                                          <tr><agiletags:ButtonOpperation type="submit" align="left" span="1" commandName="MemberDocumentSearchCommand" displayname="Search"/></tr>
                                    </table>
                                </td></tr>
                            </agiletags:ControllerForm> 
                    </table>
                    <agiletags:MemberDocumentList command="MemberDocumentSearchCommand" />
                </td></tr></table>
    </body>
</html>


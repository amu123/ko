<%-- 
    Document   : ClaimsNotes
    Created on : 26 May 2016, 11:25:48 AM
    Author     : janf
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Claims/Claims.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/ckeditor/ckeditor.js"></script>
        <script>

        </script>
        <title>Claim Notes</title>
    </head>
    <body>
        <label><h1>Claim Notes</h1></label>
        <div class="neonotification"></div>
        <div id="Main_Notes_Div">
            <div id="DetailedClaimNotes_Div">
                <jsp:include page="ClaimsNotesDetails.jsp"></jsp:include>
                </div>
                <div id="Claim_Notes_Add_Div" style="display:none;">

                </div>
                <div id="Claim_Notes_View_Div" style="display:none;">

                </div>
                <div id="Search_Notes_Div" style="display:none;">
                <nt:NeoPanel title="Search Notes"  collapsed="false" reload="false" contentClass="neopanelcontentwhite">
                    <agiletags:ControllerForm name="ClaimNotesForm">
                        <input type="hidden" name="opperation" id="ClaimNotes_opperation" value="ClaimNotesCommand" />
                        <input type="hidden" name="command" id="ClaimNotes_SearchNotesCommand" value="SearchNotes" />
                        <table>
                            <tr><nt:NeoTextField displayName="Bach Number" elementName="ClaimNotes_SearchBachNumber"/></tr>
                            <tr><nt:NeoTextField displayName="Claim Number" elementName="ClaimNotes_SearchClaimNumber"/></tr>
                            <tr><nt:NeoTextField displayName="Practice Number" elementName="ClaimNotes_SearchPracticeNumber"/></tr>
                            <tr><nt:NeoTextField displayName="Cover Number" elementName="ClaimNotes_SearchCoverNumber"/></tr>
                            <tr><td><input type="button" value="Cancel" onclick="swapDivVisbible('Search_Notes_Div', 'DetailedClaimNotes_Div')"></td>
                                <td><input type="button" value="Search" onclick="submitFormWithAjaxPostAdd(this.form, 'SearchNotesResults_Div', this)"></td></tr>
                        </table>
                    </agiletags:ControllerForm>
                </nt:NeoPanel>
                <div id="SearchNotesResults_Div"></div>
            </div>
        </div>
    </body>
</html>


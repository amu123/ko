<%-- 
    Document   : ClaimsHistory
    Created on : 2010/04/19, 04:12:34
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript">

            $(function () {
                //For Practice validation
                var searchVal = document.getElementById('searchCalled').value;
                if (searchVal == 'practiceNumber') {
                    validatePractice(document.getElementById('practiceNumber_text').value, 'practiceNumber');
                }
                document.getElementById('searchCalled').value = '';

                //set cover dependants
                var covNum = "<%= session.getAttribute("policyHolderNumber")%>";
                if (covNum != null && covNum != "") {
                    getCoverByNumber(covNum, 'depListValues');
                }
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });

            function GetXmlHttpObject() {
                if (window.ActiveXObject) {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function GetDOMParser(xmlStr) {
                var xmlDoc;
                if (window.DOMParser) {
                    parser = new DOMParser();
                    xmlDoc = parser.parseFromString(xmlStr, "text/xml");
                } else {
                    xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async = "false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }

            function ClearValidationContents() {
                document.getElementById('dateFrom').value = "";
                document.getElementById('dateTo').value = "";
            }

            //cover search
            function getCoverByNumber(str, element) {
                if (str != null && str != "") {
                    xhr = GetXmlHttpObject();
                    if (xhr == null) {
                        alert("ERROR: Browser Incompatability");
                        return;
                    }
                    var url = "/ManagedCare/AgileController";
                    url += "?opperation=GetCoverDetailsByNumber&number=" + str + "&element=" + element + "&exactCoverNum=1";
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.statusText == "OK") {
                            $("#depListValues").empty();
                            $("#depListValues").append("<option value=\"99\"></option>");
                            var xmlDoc = GetDOMParser(xhr.responseText);
                            //print value from servlet
                            $(xmlDoc).find("EAuthCoverDetails").each(function () {
                                //set product details
                                var prodName = $(this).find("ProductName").text();
                                var optName = $(this).find("OptionName").text();

                                //extract dependent info from tag
                                $(this).find("DependantInfo").each(function () {
                                    var depInfo = $(this).text();
                                    var nameSurnameType = depInfo.split("|");
                                    //Add option to dependant dropdown
                                    var optVal = document.createElement('OPTION');
                                    optVal.value = nameSurnameType[0];
                                    optVal.text = nameSurnameType[1];
                                    document.getElementById("depListValues").options.add(optVal);
                                });
                                //DISPLAY MEMBER DETAIL LIST
                                //$(document).find("#schemeName").text(prodName);
                                //$(document).find("#schemeOptionName").text(optName);
                                //error check
                                $("#" + element + "_error").text("");
                                var errorText = $(this).text();
                                var errors = errorText.split("|");
                                if (errors[0] == "ERROR") {
                                    $("#" + element + "_error").text(errors[1]);
                                }

                            });
                        }
                    }
                    xhr.open("POST", url, true);
                    xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                    xhr.send(null);
                }
            }

            //practice search
            function validatePractice(str, element) {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var error = "#" + element + "_error";
                if (str.length > 0) {
                    var url = "/ManagedCare/AgileController";
                    url = url + "?opperation=FindPracticeDetailsByCode&provNum=" + str + "&element=" + element;
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.statusText == "OK") {
                            var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                            $(document).find(error).text("");
                            if (result == "Error") {
                                $(document).find(error).text("No such provider");
                            } else if (result == "Done") {
                                $(document).find(error).text("");
                            }
                        }
                    };
                    xhr.open("POST", url, true);
                    xhr.send(null);
                } else {
                    $(document).find(error).text("");
                }
            }

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, claimId, providerNo) {
                document.getElementById('opperation').value = action;
                document.getElementById('memClaimId').value = claimId;
                document.getElementById('memClaimProvNum').value = providerNo;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <!-- content goes here -->
                    <agiletags:PDCTabs tabSelectedIndex="link_CD" />
                    <fieldset id="pageBorder">
                        <div class="">
                            <br/>
                            <br/>
                            <label class="header">Claim Search</label>
                            <br/><br/>
                            <table>
                                <agiletags:ControllerForm name="memberDetails">
                                    <input type="hidden" name="opperation" id="opperation" value="" />
                                    <input type="hidden" name="mcPageDirection" id="mcPageDirection" value="" />

                                    <agiletags:HiddenField elementName="searchCalled" />
                                    <input type="hidden" name="onScreen" id="onScreen" value="" />
                                    <input type="hidden" name="memClaimId" id="memClaimId" value=""/>
                                    <input type="hidden" name="memClaimProvNum" id="memClaimProvNum" value=""/>

                                    <tr><agiletags:LabelTextBoxDate displayname="Treatment Date From" elementName="treatmentFrom"/></tr>
                                    <tr><agiletags:LabelTextBoxDate displayname="Treatment Date To" elementName="treatmentTo"/></tr>
                                    <table>
                                        <tr>
                                            <td><agiletags:ButtonOpperation align="left" span="1" type="button" commandName="ViewPolicyHolderDetailsCommand" displayname="Back" javaScript="onClick=\"submitWithAction('ViewPolicyHolderDetailsCommand');\""/></td>
                                            <td><agiletags:ButtonOpperation align="left" span="1" type="button" commandName="GetMemberClaimDetailsPDCCommand" displayname="Search Claims" javaScript="onClick=\"submitWithAction('GetMemberClaimDetailsPDCCommand');\""/></td>
                                        </tr> 
                                    </table>

                                </agiletags:ControllerForm>
                            </table>
                            <br/>
                            <HR color="#666666" WIDTH="50%" align="left">
                            <br/>
                            <!--<agiletags:ViewMemberClaimSearchResultPDC sessionAttribute="memCCClaimSearchResult" />-->
                            <agiletags:PDCClaimHeaderTag/>

                        </div></fieldset>
                </td></tr></table>
    </body>
</html>


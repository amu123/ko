<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>


  <agiletags:ControllerForm name="BrokerLinkToConsultantForm">
    <input type="hidden" name="opperation" id="opperation" value="AddConsultantHistoryCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="ConsultantDependantAppAdd" />
    <input type="hidden" name="buttonPressed" id="consultantValuesButtonPressed" value="" />
    <input type="hidden" name="consultantEntityId" id="brokerConsultantEntityId" value="${requestScope.consultantEntityId}" />
    <input type="hidden" name="endbrokerBrokerConsultantId" id="endbrokerBrokerConsultantId" value="${requestScope.endbrokerBrokerConsultantId}"/>
    <br/> 
    <br/>
    <label class="header">End Broker Link to Broker Consultant</label>
    <br><br>
     <table>                        
         <tr id="ConsApp_brokerEndReason"><agiletags:LabelNeoLookupValueDropDownErrorReq elementName="reasonForEndingBroker" displayName="Reason" lookupId="262" javaScript="" mandatory="yes"/></tr>
         <tr></tr>
         <tr id="ConsApp_brokerEndDateRow"><agiletags:LabelTextBoxDateReq  displayname="End Date" elementName="Cons_historyEndDate" valueFromSession="yes" mandatory="yes"/></tr> 
         <tr></tr>
      </table>
    <hr>
    <table id="EndConsultantTable">
        <tr>
           <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
           <td><input type="button" value="Save" onclick="document.getElementById('consultantValuesButtonPressed').value='EndBroker';submitFormWithAjaxPost(this.form, 'consultantHistoryGrid', null,'${main_div}', '${target_div}');"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>
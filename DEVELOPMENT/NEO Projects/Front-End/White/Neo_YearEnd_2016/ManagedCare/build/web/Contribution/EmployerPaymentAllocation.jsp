<%-- 
    Document   : EmployerPaymentAllocation
    Created on : 2012/07/05, 05:07:38
    Author     : yuganp
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Membership/MemberMaintenance.js"></script>
        <style>
            img {display: block;   margin-left: auto;   margin-right: auto;}
            input{text-align:right;}
            input:focus {outline:none; border-color:#9ecaed; box-shadow:0 0 10px #9ecaed;}
        </style>
        <script>
            function calculateBalance() {
                total = parseFloat($('#unallocatedAmount').text());
                allocated = 0;
                $(".contribAlloc").each( function(){
                    tmp=parseFloat($(this).val());
                    if (isNaN(tmp)){tmp=0;};
                    allocated= allocated + tmp;     
                });
                $('#allocatedAmount').text(allocated.toFixed(2));
                $('#remainingAmount').text((total - allocated).toFixed(2));
            }
            
            function changeValue(item) {
                tmp=parseFloat(item.value);
                if (isNaN(tmp)){item.value = ''} else {item.value = tmp.toFixed(2)};
                calculateBalance();
            }
            
            function selectReceipt(receipt) {
                var sr = receipt.split("_");
                $('#unallocatedAmount').text(parseFloat(sr[1]).toFixed(2));
                calculateBalance();
            }
            function allocateSingle(lineId) {
                contribId = "contrib_" + lineId;
                outId = "outAmount_" + lineId;
                $('#' + contribId).val($('#'+ outId).text());
                calculateBalance();
            }
            
            function unallocateSingle(lineId) {
                contribId = "contrib_" + lineId;
                outId = "outAmount_" + lineId;
                $('#' + contribId).val('');
                calculateBalance();
            }
            
            function autoAllocate() {
                allocated = 0;
                remain = parseFloat($('#remainingAmount').text());
                $(".contribAlloc").each( function(){
                    if ($(this).val() == '') {
                        lineId = $(this).attr('id').substring(8);
                        outVal = parseFloat($('#outAmount_'+ lineId).text());
                        if (remain >= outVal) {
                             $('#contrib_' + lineId).val(outVal.toFixed(2));
                             remain = remain - outVal;
                        }
                    }
                });
                calculateBalance();
            }
            
            function saveDetails(form, btn) {
                remain = parseFloat($('#remainingAmount').text());
                if (remain < 0) {
                    alert ("Allocated amount is greater than the unallocated amount");
                } else {
                    $('#amountUnallocated').val($('#remainingAmount').text());
                    btn.disabled = true;
                    var params = encodeFormForSubmit(form);
                    $(form).mask("Saving...");

                    $.post(form.action, params,function(result){
                        updateFormFromData(result);
                        if (btn != undefined) {
                            btn.disabled = false;
                        }
                        $(form).unmask();
                        window.location='/ManagedCare/Security/Welcome.jsp'; 
                    });
                }
            }
            
            function encodeNonBlankFormForSubmit(form) {
                var encStr = "formName=" + escape(form.name) + "&";
                for (var i=0; i < form.elements.length; i++) {
                    var element = form.elements[i];
                    var elementType = element.type.toUpperCase();
                    var elementVal = "";
                    if (!element.name) continue;
                    if (elementType == "CHECKBOX") {
                        elementVal = element.checked ? element.checked : "false";
                    } else {
                        elementVal = element.value;
                    }
                    if (elementVal) {
                        encStr += elementVal ? element.name + "=" + elementVal : "";
                        if (i < form.elements.length - 1) encStr += "&";
                    }
                }
                return encStr;
            }            
            
        </script>
    </head>
    <body>
        
     <agiletags:ControllerForm name="PaymentAllocationForm">
        <input type="hidden" name="opperation" id="opperation" value="PaymentAllocationCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="MemberPaymentAllocation" />
        <input type="hidden" name="command" id="BankStatementCommand" value="SaveEmployer" />
        <input type="hidden" name="entityId" id="entityId" value="${requestScope.entityId}" />
        <input type="hidden" name="amountUnallocated" id="amountUnallocated" value="" />

    <label class="header">Group Contribution Allocation</label>
    <hr>
    <table>
        <tr>
            <td width="160"><label>Group Number:</label></td>
            <td align="right"><label> ${groupNumber}</label></td>
        </tr>
        <tr>
            <td><label>Group Name:</label></td>
            <td align="right"><label> ${groupName}}</label></td>
        </tr>
        <tr>
            <td><label>Amount To be Allocated:</label></td>
            <td align="right"><label id="unallocatedAmount"><b>${agiletags:formatStringAmount(amount)}</b></label></td>
        </tr>
        <tr>
            <td><label>Allocated Amount:</label></td>
            <td align="right"><label id="allocatedAmount"><b>0.00</b></label></td>
        </tr>
        <tr>
            <td><label>Remaining Amount:</label></td>
            <td align="right"><label id="remainingAmount"><b>${agiletags:formatStringAmount(amount)}</b></label></td>
        </tr>
        <tr>
            <td><label>Receipt</label></td>
            <td>
                <c:if test="${empty receipts}"><label>Not Receipts Available</label></c:if>
                <c:if test="${not empty receipts}">
                    <c:set var="totalReceipts" value="0"></c:set>
                    <c:forEach var="rec" items="${receipts}">
                        <c:set var="totalReceipts" value="${totalReceipts + rec.amount}"></c:set>
                    </c:forEach>
                    <select name="ContribReceiptsId" onchange="selectReceipt(this.value);">
                        <optgroup label="All">
                            <option value="0_${totalReceipts}" selected>All Receipts R ${agiletags:formatBigDecimal(totalReceipts)}</option>
                        </optgroup>
                        <c:forEach var="rec" items="${receipts}">
                            <optgroup label="${agiletags:formatXMLGregorianDate(rec.statementDate)} ${rec.receiptType == 1 ? 'Payment' : 'Unallocated Amount'}">
                            <option value="${rec.contribReceiptsId}_${rec.amount}">${rec.transactionDescription} R ${agiletags:formatBigDecimal(rec.amount)}</option>
                            </optgroup>
                        </c:forEach>
                    </select>
                </c:if>
            </td>
        </tr>
    </table>
        <br>
        <div id="PageContents" >
            <c:if test="${empty employerContribs}">
                <Label>No Contributions to Allocate</Label>
            </c:if>
            <c:if test="${!empty employerContribs}">
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Contribution Period</th>
                        <th>Member Name</th>
                        <th>Member Number</th>
                        <th>Employee Number</th>
                        <th>Option</th>
                        <th>Contribution</th>
                        <th>Allocated</th>
                        <th>Outstanding</th>
                        <th>Current Allocation</th>
                        <th colspan="2"><button type="button" onclick="autoAllocate()">Auto Allocate</button></th>
                    </tr>
                    <c:forEach var="entry" items="${employerContribs}">
<tr>
                            <c:set var="lineId" value="${fn:replace(entry['contribDate'],'/','')}_${entry['coverNumber']}_${entry['optionId']}"></c:set>
<td><label>${entry['contribDate']}</label></td>
<td><label>${entry['memberName']}</label></td>
<td><label>${entry['coverNumber']}</label></td>
<td><label>${entry['employeeNumber']}</label></td>
<td><label>${entry['optionName']}</label></td>
<td align="right"><label>${agiletags:formatStringAmount(entry['contribAmount'])}</label></td>
<td align="right"><label>${agiletags:formatStringAmount(entry['paidAmount'])}</label></td>
<td align="right"><label id="outAmount_${lineId}" >${agiletags:formatStringAmount(entry['outstandingAmount'])}</label></td>
<td><input size="10" type="text" name="contrib_${lineId}" id="contrib_${lineId}" class="contribAlloc" onchange="changeValue(this)"></td>
<td><img src="/ManagedCare/resources/checkmark.gif" onclick="allocateSingle('${lineId}')"></td>
<td><img src="/ManagedCare/resources/x-red.gif" onclick="unallocateSingle('${lineId}')"></td>
 </tr>
                    </c:forEach>
                </table>
            </c:if>
        </div>
        <br>
        <input type="reset">
        <input type="button" value="Save" onclick="saveDetails(this.form, this)">
         <agiletags:ButtonOpperationLabelError displayName="Export" elementName="export" type="button" javaScript="onClick=\"exportExcel('Employer');\""/> 
     </agiletags:ControllerForm>
    
    </body>
</html>

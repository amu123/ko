<%-- 
    Document   : AlternativeMemberNumbers
    Created on : Aug 5, 2016, 12:56:08 AM
    Author     : janf
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="items" required="false" type="java.util.Collection" %>

<%-- any content can be specified here e.g.: --%>
<table class="list" style="border-collapse:collapse; border-width:1px;" width="50%">
    <thead>
        <tr>
            <th rowspan="2" style="border-right-color: #FFFFFF; ">Alternative Number</th>
            <th style="border-right-color: #FFFFFF; ">Start Date</th>
            <th style="border-right-color: #FFFFFF; ">End Date</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="item" items="${items}">
            <tr>
                <td><label>${item.infoValue}</label></td>
                <td><label>${agiletags:formatXMLGregorianDate(item.startDate)}</label></td>
                <td><label>${agiletags:formatXMLGregorianDate(item.endDate)}</label></td>
            </tr>
        </c:forEach>
    </tbody>
</table>
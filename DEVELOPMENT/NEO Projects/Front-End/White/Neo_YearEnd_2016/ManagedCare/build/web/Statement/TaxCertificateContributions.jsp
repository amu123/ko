<%-- 
    Document   : TaxCertificateContributions
    Created on : 12 May 2014, 3:21:49 PM
    Author     : almaries
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib  prefix="nt" tagdir="/WEB-INF/tags/contributions" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>

        <script language="JavaScript">
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            
            function submitWithAction1(action, actionParam, screen) {
                document.getElementById('opperation').value = action;
                document.getElementById('opperationParam').value = actionParam;
                document.getElementById('onScreen').value = screen;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="left"><td width="50px"></td><td align="left">
                    <label class="header">Tax Certificate</label>
                    <br/><br/><br/>
                    <agiletags:ControllerForm name="TaxCertificateContributions">
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                        <input type="hidden" name="onScreen" id="onScreen" value="" />
                        <input type="hidden" name="contrib_start_date" id="contrib_start_date" value="${sessionScope.startDate}" />
                        <input type="hidden" name="contrib_end_date" id="contrib_end_date" size="10" value="${sessionScope.endDate}" />
                        <input type="hidden" name="exp_start_date" id="exp_start_date" value="${sessionScope.startDate}" />
                        <input type="hidden" name="exp_end_date" id="exp_end_date" value="${sessionScope.endDate}" />
                        <input type="hidden" name="buttonPressedExport" id="buttonPressedExport" value="Export" />
                        <div id="ContribTranResultsx">
                            <nt:ContributionTransactionHistory items="${Contributions}"/>
                        </div>
                        <div>
                            <input type="button" value="Export To Excel" onclick="submitWithAction('LoadTaxCertContribTranCommand')">
                        </div>
                        <br>
                        <HR WIDTH="100%" align="left">
                        <div align="left">
                            <input type="button" value="Return" onclick="submitWithAction1('ViewTaxCertificateDetailsCommand', 'Return','TaxCertificateContributions.jsp')"></td>
                        </div>
                    </agiletags:ControllerForm>
                </td></tr>
        </table>
    </body>
</html>


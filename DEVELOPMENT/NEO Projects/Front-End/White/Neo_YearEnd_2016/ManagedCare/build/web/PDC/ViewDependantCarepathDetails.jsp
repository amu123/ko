<%-- 
    Document   : ViewDependantCarepathDetails
    Created on : May 27, 2016, 2:18:54 PM
    Author     : nick
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.scrollTo-1.4.2-min.js"></script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
        <title>Manage Patient Care Path</title>      
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/modal.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/popup.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>

            $(function () {
                var scrollTo = document.getElementById('scrollTrigger').value;
                $("a.trigger").css({'z-index': '13'});
                $("a.btn btnBelow").css({'z-index': '13'});
                if (scrollTo === "true") {
                    $.scrollTo($("#calendarAnchor"), 1);
                } else {
                    $.scrollTo($("#topAnchor"), 1);
                }

                $("div.pop-up").css({'display': 'block', 'opacity': '0'});

                $("a.trigger").hover(
                        function () {
                            var hoveringTask = document.getElementById('hoveringTaskID').value;
                            $("a.trigger").css({'z-index': '0'});
                            $("a.btn btnBelow").css({'z-index': '0'});
                            $("a#Task" + hoveringTask).css({'z-index': '13'});
                            $(this).prev().stop().animate({
                                opacity: 1
                            }, 500);
                        },
                        function () {
                            $(this).prev().stop().animate({
                                opacity: 0
                            }, 200);
                            $("a.trigger").css({'z-index': '13'});
                            $("a.btn btnBelow").css({'z-index': '13'});
                        }
                );

            });

            function submitCarePathsWithAction(cmd) {
                var cdlList = [];

                var cpList = document.getElementsByName("cpCheck");

                for (var i = 0; i < cpList.length; i++) {
                    if (cpList[i].checked) {
                        cdlList.push(cpList[i].value);
                    }
                }

                document.getElementById('opperation').value = cmd;
                document.getElementById('cpList').value = cdlList;
                document.forms[0].submit();
            }
            

            function submitWithAction(cmd) {
                document.getElementById('opperation').value = cmd;
                document.forms[0].submit();
            }
            
            
            function setSelectedTaskIdOnForm(id) {
                document.getElementById('selectedTaskId').value = id;
            }
            

            function setSelectedConditionOnForm(id, val) {
                document.getElementById('selectedConditionId').value = id;
                document.getElementById('selectedConditionName').value = val;
                document.getElementById('conditionLabel').innerHTML = 'Task Condition: ' + val;
            }
            

            function submitYearChangeWithAction(cmd, incOrDec) {
                document.getElementById('opperation').value = cmd;
                document.getElementById('incOrDecYear').value = incOrDec;
                document.forms[0].submit();
            }
            

            function setHoveringTaskID(id) {
                document.getElementById("hoveringTaskID").value = id;
            }
            

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="10px"></td><td align="left">
                    <!-- content goes here -->
                    <a id="topAnchor" name="topAnchor"></a>
                    <label class="header">Manage Patient Care Path</label>
                    <br/><br/>

                    <agiletags:ControllerForm name="selectCondition">
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <input type="hidden" name="listIndex" id="listIndex" value="" />
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <input type="hidden" name="pageDirection" id="pageDirection" value="" />
                        <input type="hidden" name="refreshScreen" id="refreshScreen" value="" />   
                        <input type="hidden" name="pdcUserYesNo" id="pdcUserYesNo" value="${sessionScope.persist_user_pdcUser}" />
                        <input type="hidden" name="pdcAdminYesNo" id="pdcAdminYesNo" value="${sessionScope.persist_user_pdcAdmin}" />
                        <input type="hidden" name="cpList" id="cpList" value="" />
                        <input type="hidden" name="selectedTaskId" id="selectedTaskId" value="" />
                        <input type="hidden" name="selectedConditionId" id="selectedConditionId" value="" />
                        <input type="hidden" name="selectedConditionName" id="selectedConditionName" value="" />
                        <input type="hidden" name="incOrDecYear" id="incOrDecYear" value="" />
                        <input type="hidden" name="scrollTrigger" id="scrollTrigger" value="${sessionScope.pdcCarePathScrollTrue}" />
                        <input type="hidden" name="hoveringTaskID" id="hoveringTaskID" value="" />

                        <c:choose>
                            <c:when test="${!empty sessionScope.depCarePathTaskUnassignedMap}">
                                <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                    <agiletags:PDCCarePathTaskList/>
                                </table>  
                            </c:when>
                            <c:otherwise>
                                <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                    <tr><td><label style="color: red">No Care Paths found for patient...</label></td></tr>
                                </table>  
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${!empty sessionScope.depCarePathTaskAssignedMap}">
                                <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                    <a id="calendarAnchor" name="calendarAnchor"></a>
                                    <agiletags:PDCCarepathSchedule/>
                                </table>  
                            </c:when>
                            <c:otherwise>
                                <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                    <tr><td><label style="color: red">No assigned Tasks found for patient, please assign above...</label></td></tr>
                                </table>  
                            </c:otherwise>
                        </c:choose>

                        <!-- Modal Assign-->
                        <a href="#" class="modal" id="modal-assign" aria-hidden="true">
                        </a>
                        <div class="modal-dialog">
                            <div class="modal-header">
                                <h2>Assign Task:</h2>
                                <a href="#" class="btn-close" aria-hidden="true">×</a>
                            </div>
                            <div class="modal-body">
                                <table border="0">
                                    <tr>
                                        <td><label class="label" width="100px">Task Year:</label></td>
                                        <td>
                                            <select style="width: 215px" name="pdcTaskYear" id="pdcTaskYear">
                                                <option value="99"></option>
                                                <c:forEach var="year" items="${sessionScope.pdcUsableYears}">
                                                    <option value="${year}">${year}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>
                                    <br/>
                                    <tr>
                                        <td><label class="label" width="100px">Task Month:</label></td>
                                        <td>
                                            <select style="width: 215px" name="pdcTaskMonth" id="pdcTaskMonth">
                                                <option value="99"></option>
                                                <c:forEach var="month" items="${sessionScope.pdcAllMonths}">
                                                    <option value="${month.id}">${month.value}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>
                                    <br/>
                                    <tr>
                                        <td><label class="label" width="100px">Task Week:</label></td>
                                        <td>
                                            <select style="width: 215px" name="pdcTaskWeek" id="pdcTaskMonth">
                                                <option value="99"></option>
                                                <c:forEach var="week" items="${sessionScope.pdcUsableWeeks}">
                                                    <option value="${week}">${week}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>
                                    <br/>
                                    <tr>
                                        <td><label class="label" width="100px">Task Type:</label></td>
                                        <td>
                                            <select style="width: 215px" name="pdcTaskType" id="pdcTaskType">
                                                <option value="99"></option>
                                                <c:forEach var="type" items="${sessionScope.pdcTaskTypeLookup}">
                                                    <option value="${type.id}">${type.value}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <c:choose>
                                    <c:when test="${applicationScope.Client == 'Sechaba'}">
                                        <a href="#" class="btnGreen" 
                                            onclick="submitWithAction('AssignCarePathTaskCommand');">
                                            Submit</a>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="#" class="btn" 
                                            onclick="submitWithAction('AssignCarePathTaskCommand');">
                                            Submit</a>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <!-- /Modal Assign-->

                        <!-- Modal Remove-->
                        <a href="#" class="modal" id="modal-remove" aria-hidden="true">
                        </a>
                        <div class="modal-dialog">
                            <div class="modal-header">
                                <h2>Remove Task:</h2>
                                <a href="#" class="btn-close" aria-hidden="true">×</a>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure you want to remove this task?</p>
                            </div>
                            <div class="modal-footer">
                                <c:choose>
                                    <c:when test="${applicationScope.Client == 'Sechaba'}">
                                        <a href="#" class="btnGreen" 
                                            onclick="submitWithAction('RemoveCarePathTaskCommand');">
                                            Yes</a>
                                        <a href="#" class="btnGreen">No</a>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="#" class="btn" 
                                            onclick="submitWithAction('RemoveCarePathTaskCommand');">
                                            Yes</a>
                                        <a href="#" class="btn">No</a>
                                    </c:otherwise>
                                </c:choose>
                                
                            </div>
                        </div>
                        <!-- /Modal Remove-->

                        <!-- Modal Create-->
                        <a href="#" class="modal" id="modal-create" aria-hidden="true">
                        </a>
                        <div class="modal-dialog">
                            <div class="modal-header">
                                <h2>Create Task:</h2>
                                <a href="#" class="btn-close" aria-hidden="true">×</a>
                            </div>
                            <div class="modal-body">
                                <table border="0">
                                    <tr>
                                        <td><label class="label" id="conditionLabel" width="100px"></label></td>
                                    </tr>
                                    <tr><td><br/></td></tr>
                                    <tr>
                                        <td><label class="label" width="100px">Task:</label></td>
                                        <td><textarea rows="10" cols="50" name="taskDescription" id="taskDescription"></textarea></td>
                                    </tr>
                                    <br/>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <c:choose>
                                    <c:when test="${applicationScope.Client == 'Sechaba'}">
                                        <a href="#" class="btnGreen" 
                                            onclick="submitWithAction('CreateCarePathTaskCommand');">
                                            Submit</a>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="#" class="btn" 
                                            onclick="submitWithAction('CreateCarePathTaskCommand');">
                                            Submit</a>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <!-- /Modal Create-->
                        
                        <!-- Modal Decide-->
                        <a href="#" class="modal" id="modal-decide" aria-hidden="true">
                        </a>
                        <div class="modal-dialog">
                            <div class="modal-header">
                                <h2>Task Decision:</h2>
                                <a href="#" class="btn-close" aria-hidden="true">×</a>
                            </div>
                            <div class="modal-body">
                                <p>Would you like to edit or close this task?</p>
                            </div>
                            <div class="modal-footer">
                                <c:choose>
                                    <c:when test="${applicationScope.Client == 'Sechaba'}">
                                        <a href="#modal-edit" class="btnGreen">Edit</a>
                                        <a href="#modal-close" class="btnGreen">Close</a>
                                        <a href="#" class="btnGreen">Cancel</a>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="#modal-edit" class="btn">Edit</a>
                                        <a href="#modal-close" class="btn">Close</a>
                                        <a href="#" class="btn">Cancel</a>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <!-- /Modal Decide-->
                        
                        <!-- Modal Edit-->
                        <a href="#" class="modal" id="modal-edit" aria-hidden="true">
                        </a>
                        <div class="modal-dialog">
                            <div class="modal-header">
                                <h2>Edit Task:</h2>
                                <a href="#" class="btn-close" aria-hidden="true">×</a>
                            </div>
                            <div class="modal-body">
                                <table border="0">
                                    <tr>
                                        <td><label class="label" width="100px">Task Year:</label></td>
                                        <td>
                                            <select style="width: 215px" name="pdcTaskYearEdit" id="pdcTaskYearEdit">
                                                <option value="99"></option>
                                                <c:forEach var="year" items="${sessionScope.pdcUsableYears}">
                                                    <option value="${year}">${year}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>
                                    <br/>
                                    <tr>
                                        <td><label class="label" width="100px">Task Month:</label></td>
                                        <td>
                                            <select style="width: 215px" name="pdcTaskMonthEdit" id="pdcTaskMonthEdit">
                                                <option value="99"></option>
                                                <c:forEach var="month" items="${sessionScope.pdcAllMonths}">
                                                    <option value="${month.id}">${month.value}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>
                                    <br/>
                                    <tr>
                                        <td><label class="label" width="100px">Task Week:</label></td>
                                        <td>
                                            <select style="width: 215px" name="pdcTaskWeekEdit" id="pdcTaskMonthEdit">
                                                <option value="99"></option>
                                                <c:forEach var="week" items="${sessionScope.pdcUsableWeeks}">
                                                    <option value="${week}">${week}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>
                                    <br/>
                                    <tr>
                                        <td><label class="label" width="100px">Task Type:</label></td>
                                        <td>
                                            <select style="width: 215px" name="pdcTaskTypeEdit" id="pdcTaskTypeEdit">
                                                <option value="99"></option>
                                                <c:forEach var="type" items="${sessionScope.pdcTaskTypeLookup}">
                                                    <option value="${type.id}">${type.value}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <c:choose>
                                    <c:when test="${applicationScope.Client == 'Sechaba'}">
                                        <a href="#" class="btnGreen" 
                                            onclick="submitWithAction('EditCarePathTaskCommand');">
                                            Save</a>
                                        <a href="#" class="btnGreen">Cancel</a>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="#" class="btn" 
                                            onclick="submitWithAction('EditCarePathTaskCommand');">
                                            Save</a>
                                        <a href="#" class="btn">Cancel</a>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <!-- /Modal Edit-->
                        
                        <!-- Modal Close-->
                        <a href="#" class="modal" id="modal-close" aria-hidden="true">
                        </a>
                        <div class="modal-dialog">
                            <div class="modal-header">
                                <h2>Close Task:</h2>
                                <a href="#" class="btn-close" aria-hidden="true">×</a>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure you want to close this task?</p>
                            </div>
                            <div class="modal-footer">
                                <c:choose>
                                    <c:when test="${applicationScope.Client == 'Sechaba'}">
                                        <a href="#" class="btnGreen" 
                                            onclick="submitWithAction('CloseCarePathTaskCommand');">
                                            Yes</a>
                                        <a href="#" class="btnGreen">No</a>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="#" class="btn" 
                                            onclick="submitWithAction('CloseCarePathTaskCommand');">
                                            Yes</a>
                                        <a href="#" class="btn">No</a>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <!-- /Modal Close-->
                        <br/><br/>
                        <c:choose>
                            <c:when test="${applicationScope.Client == 'Sechaba'}">
                                <a href="#" class="btnGreen btnBelow" 
                                    onclick="submitWithAction('ViewChronicConditionSelectionCommand');">
                                    Add New Condition</a>
                            </c:when>
                            <c:otherwise>
                                <a href="#" class="btn btnBelow" 
                                    onclick="submitWithAction('ViewChronicConditionSelectionCommand');">
                                    Add New Condition</a>
                            </c:otherwise>
                        </c:choose>
                        
                        <c:choose>
                            <c:when test="${applicationScope.Client == 'Sechaba'}">
                                <a href="#" class="btnGreen btnBelow" 
                                    onclick="submitWithAction('ViewWorkBenchCommand');">
                                    Back To Workbench</a>
                            </c:when>
                            <c:otherwise>
                                <a href="#" class="btn btnBelow" 
                                    onclick="submitWithAction('ViewWorkBenchCommand');">
                                    Back To Workbench</a>
                            </c:otherwise>
                        </c:choose>
                    </agiletags:ControllerForm>                   
                    <br/>
                </td></tr></table>
    </body>
</html>
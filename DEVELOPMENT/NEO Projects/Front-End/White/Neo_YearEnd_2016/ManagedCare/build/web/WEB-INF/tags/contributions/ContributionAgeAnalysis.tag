<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@attribute name="items" required="true" type="java.util.Collection" %>
<%@attribute name="showMember" required="false" type="java.lang.Boolean" %>
<%@attribute name="showHeader" required="false" type="java.lang.Boolean" %>
<%@attribute name="productID" required="false" type="java.lang.Integer" %>

 
<c:if test="${!empty items}">
    <c:if test="${showHeader != false}">
        <label class="subheader">Age Analysis</label>
    </c:if>
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <c:set var="mAge" value="${fn:length(items) gt 1 ? true : false}"></c:set>
            <c:if test="${mAge}">
                <th>&nbsp;</th>
                </c:if>
            <th>Name</th>
            <c:if test="${showMember == true}"><th>Cover Number</th></c:if>
                <c:choose>
                    <c:when test="${(productID eq 2)}">
                        <th>180 Days Plus</th>
                        <th>180 Days</th>
                        <th>150 Days</th>
                        <th>120 Days</th>
                    </c:when>
                    <c:otherwise>
                        <th>90 Days Plus</th>
                    </c:otherwise>                      
                </c:choose>      
                        
            <th>90 Days</th>
            <th>60 Days</th>
            <th>30 Days</th>
            <th>Current</th>
            <th>Current Balance</th>
            <th>Future</th>
        </tr>
        <tbody id="contrib_age_details" style="${mAge ? 'display:none' : 'display:table-row-group'}}">
            <c:set var="age180plus" value="0" ></c:set>
            <c:set var="age180" value="0" ></c:set>
            <c:set var="age150" value="0" ></c:set>
            <c:set var="age120" value="0" ></c:set>
            <c:set var="age90plus" value="0" ></c:set>
            <c:set var="age90" value="0" ></c:set>
            <c:set var="age60" value="0" ></c:set>
            <c:set var="age30" value="0" ></c:set>
            <c:set var="agecurrent" value="0" ></c:set>
            <c:set var="agebalance" value="0" ></c:set>
            <c:set var="agefuture" value="0" ></c:set>
            <c:forEach items="${items}" var="maa">
                <c:if test="${not mAge || (maa.amountCurrent != 0 || maa.amount90DaysPlus != 0 || maa.amount90Days != 0 || maa.amount60Days != 0 || maa.amount30Days != 0 || maa.amountFuture != 0)}">
                    <tr>
                        <c:if test="${mAge}">
                            <td>&nbsp;</td>
                        </c:if>
                        <td><label>${showMember == true ? maa.memberName : maa.entityName}</label></td>
                        <c:if test="${showMember == true}"><td><label>${maa.coverNumber}</label></td></c:if>

                        <c:choose>
                            <c:when test="${(productID eq 2)}">
                                <td align="right"><label>${agiletags:formatBigDecimal(maa.amount180DaysPlus)}</label></td>
                                <td align="right"><label>${agiletags:formatBigDecimal(maa.amount180Days)}</label></td>
                                <td align="right"><label>${agiletags:formatBigDecimal(maa.amount150Days)}</label></td>
                                <td align="right"><label>${agiletags:formatBigDecimal(maa.amount120Days)}</label></td>
                            </c:when>
                            <c:otherwise>
                                <td align="right"><label>${agiletags:formatBigDecimal(maa.amount90DaysPlus)}</label></td>
                            </c:otherwise>                      
                        </c:choose>   
                        
                        <td align="right"><label>${agiletags:formatBigDecimal(maa.amount90Days)}</label></td>
                        <td align="right"><label>${agiletags:formatBigDecimal(maa.amount60Days)}</label></td>
                        <td align="right"><label>${agiletags:formatBigDecimal(maa.amount30Days)}</label></td>
                        <td align="right"><label>${agiletags:formatBigDecimal(maa.amountCurrent)}</label></td>
                        <td align="right"><label>${agiletags:formatBigDecimal(maa.amountCurrentBalance)}</label></td>
                        <td align="right"><label>${agiletags:formatBigDecimal(maa.amountFuture)}</label></td>
                        
                        <c:set var="age180plus" value="${age180plus + maa.amount180DaysPlus}" ></c:set>
                        <c:set var="age180" value="${age180 + maa.amount180Days}" ></c:set>
                        <c:set var="age150" value="${age150 + maa.amount150Days}" ></c:set>
                        <c:set var="age120" value="${age120 + maa.amount120Days}" ></c:set>
                        <c:set var="age90plus" value="${age90plus + maa.amount90DaysPlus}" />
                        <c:set var="age90" value="${age90 + maa.amount90Days}" ></c:set>
                        <c:set var="age60" value="${age60 + maa.amount60Days}" ></c:set>
                        <c:set var="age30" value="${age30 + maa.amount30Days}" ></c:set>
                        <c:set var="agecurrent" value="${agecurrent + maa.amountCurrent}" ></c:set>
                        <c:set var="agebalance" value="${agebalance + maa.amountCurrentBalance}" ></c:set>
                        <c:set var="agefuture" value="${agefuture + maa.amountFuture}" ></c:set>
                        </tr>
                </c:if>
            </c:forEach>
        </tbody>
        <c:if test="${mAge}">
            <tr style="background-color:#f0f8ff;">
                <td><input type="Button" value="+" onclick="toggle_table_group(this, 'contrib_age_details');"></td>
                <td><label><b>Total</b></label></td>
                <c:if test="${showMember == true}"><td><label>&nbsp;</label></td></c:if>
                  
                    <c:choose>
                        <c:when test="${(productID eq 2)}">
                            <td align="right"><label><b>${agiletags:formatBigDecimal(age180plus)}</b></label></td>
                            <td align="right"><label><b>${agiletags:formatBigDecimal(age180)}</b></label></td>
                            <td align="right"><label><b>${agiletags:formatBigDecimal(age150)}</b></label></td>
                            <td align="right"><label><b>${agiletags:formatBigDecimal(age120)}</b></label></td> 
                        </c:when>
                        <c:otherwise>
                            <td align="right"><label><b>${agiletags:formatBigDecimal(age90plus)}</b></label></td> 
                        </c:otherwise>                      
                    </c:choose>

                <td align="right"><label><b>${agiletags:formatBigDecimal(age90)}</b></label></td>
                <td align="right"><label><b>${agiletags:formatBigDecimal(age60)}</b></label></td>
                <td align="right"><label><b>${agiletags:formatBigDecimal(age30)}</b></label></td>
                <td align="right"><label><b>${agiletags:formatBigDecimal(agecurrent)}</b></label></td>
                <td align="right"><label><b>${agiletags:formatBigDecimal(agebalance)}</b></label></td>
                <td align="right"><label><b>${agiletags:formatBigDecimal(agefuture)}</b></label></td>
            </tr>
        </c:if>

    </table>
    <br>
</c:if>
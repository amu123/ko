<%@tag description=""  dynamic-attributes="dynattrs" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@attribute name="data" required="true" type="java.util.Collection" %>
<%@attribute name="prefix" required="true" type="java.lang.String"%>

<c:set var="neoSummaryTableFields" scope="request" />
<c:set var="neoSummaryTableLabels" scope="request" />
<c:set var="neoSummaryTableType"   scope="request" />

<jsp:doBody />

<c:set var="_fields" value="${fn:split(neoSummaryTableFields, '||')}" />
<c:set var="_labels" value="${fn:split(neoSummaryTableLabels, '||')}" />
<c:set var="_types" value="${fn:split(neoSummaryTableType, '||')}" />
${neoSummaryTableFields}<br>
${neoSummaryTableLabels}<br>
${neoSummaryTableType}<br>
<table 
    <c:forEach items="${dynattrs}" var="a"> 
        ${a.key}="${a.value}" 
    </c:forEach>
    class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
    <thead>
        <c:forEach items="${_labels}" var="_label">
            <th>${_label}</th>
        </c:forEach>
    </thead>
    <tbody>
        <c:forEach items="${data}" var="item" varStatus="loopindex">
            <tr>
                <c:forTokens items="${neoSummaryTableFields}" delims="||" var="_field" varStatus="fieldindex">
                    <c:if test="${_field eq 'none'}">
                        <td>${loopindex.index}</td>
                    </c:if>
                    <c:if test="${not (empty _field || _field eq 'none')}">
                        <c:choose>
                            <c:when test="${fn:toLowerCase(_types[fieldindex.index]) eq 'xmldate'}"><td><label>${agiletags:formatXMLGregorianDate(item[_field])}</label></td></c:when>
                            <c:when test="${fn:toLowerCase(_types[fieldindex.index]) eq 'date'}"><td><label>${agiletags:formatDate(item[_field])}</label></td></c:when>
                            <c:when test="${fn:toLowerCase(_types[fieldindex.index]) eq 'bdamount'}"><td align="right"><label>${agiletags:formatBigDecimal(item[_field])}</label></td></c:when>
                            <c:when test="${fn:toLowerCase(_types[fieldindex.index]) eq 'stramount'}"><td align="right"><label>${agiletags:formatStringAmount(item[_field])}</label></td></c:when>
                            <c:when test="${fn:toLowerCase(_types[fieldindex.index]) eq 'string'}"><td><label>${item[_field]}</label></td></c:when>
                        </c:choose>
                    </c:if>
                </c:forTokens>
            </tr>
        </c:forEach>
    </tbody>
</table>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Administrator Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty AdministratorSearchResults}">
                <span class="label">No Administrator found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${AdministratorSearchResults}">
                        <tr>
                            <td><label>${entry.name}</label></td>
                            <td><label>${entry.surname}</label></td>
                            <td><label>${entry.username}</label></td>
                            <td><label>${entry.emailAddress}</label></td>
                            <td><input type="button" value="Select" onclick="setElemValues({'${diagCodeId}':'${agiletags:escapeStr(entry.name)} ${agiletags:escapeStr(entry.surname)}'});
                                swapDivVisbible('${target_div}','${main_div}');"/></td>
                        </tr>
                    </c:forEach>

                </table>
            </c:otherwise>
        </c:choose>

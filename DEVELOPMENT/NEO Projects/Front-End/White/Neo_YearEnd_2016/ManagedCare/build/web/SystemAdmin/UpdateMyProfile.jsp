<%-- 
    Document   : UpdateMyProfile
    Created on : 2011/11/15, 02:11:45
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script>
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
            
                    <agiletags:UserDetailsTable header="User Details"/>
                    <HR color="#666666" WIDTH="50%" align="left">
                    <agiletags:ControllerForm name="updateMyDetails">
                        <table>
                            
                            <input type="hidden" name="opperation" id="opperation" value="" />
                                
                            <tr><td><label class="header">Update Details</label></td></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Name" elementName="name" valueFromRequest="username" mandatory="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="surname" valueFromRequest="usersurname" mandatory="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Email" elementName="email" valueFromRequest="useremail" mandatory="yes"/></tr>
                             
                            <table>
                            <tr>
                                <agiletags:ButtonOpperation type="submit" align="left" commandName="UpdateMyProfileCommand" displayname="Update" span="3" javaScript="onClick=\"submitWithAction('UpdateMyProfileCommand')\";"/>
                                <agiletags:ButtonOpperation type="Submit" align="left" commandName="CancelUserProfileCommand" displayname="Cancel" span="1" javaScript="onClick=\"submitWithAction('CancelUserProfileCommand')\";"/>
                            </tr>
                            </table>
                                
                        </table>
                            
                    </agiletags:ControllerForm>
                        
                </td></tr></table>
    </body>
</html>

<%-- 
    Document   : PracticeSearch
    Created on : 2010/07/26, 03:04:39
    Author     : whauger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

 <%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
        <!-- content goes here -->
            <label class="header">Practice Search</label>
                    </br></br>
                    <table>
                        <agiletags:ControllerForm name="searchPractice">
                        <tr><agiletags:LabelTextBoxError displayName="Practice Number" elementName="pracNo"/></tr>
                        <tr><agiletags:LabelTextBoxError displayName="Practice Name" elementName="pracName"/></tr>
                        <tr><agiletags:LabelTextBoxError displayName="Practice Type" elementName="pracType"/></tr>
                        <tr><agiletags:ButtonOpperation type="submit" align="left" span="3" commandName="SearchPracticeCommand" displayname="Search"/></tr>
                        </agiletags:ControllerForm>
                    </table>
                    </br>
                    <HR color="#666666" WIDTH="50%" align="left">
                    </br>
                    <agiletags:PracticeSearchResultTable commandName="AllocatePracticeToSessionCommand" javaScript=""/>

        <!-- content ends here -->
        </td></tr></table>
    </body>
</html>

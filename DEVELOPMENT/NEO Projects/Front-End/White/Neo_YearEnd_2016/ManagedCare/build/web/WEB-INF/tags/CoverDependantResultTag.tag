<%-- 
    Document   : CoverDependantResultTag
    Created on : 03 Feb 2015, 10:00:21 AM
    Author     : gerritr
--%>

<%@tag description="CoverDependentResultTag" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>   

<%@attribute name="commandName" required="true" type="java.lang.String" %>
<%@attribute name="javaScript" required="true" type="java.lang.String" %>
<%@attribute name="sessionAttribute" required="true" type="java.lang.String" %>
<%@attribute name="onScreen" required="true" type="java.lang.String" %>
<%@attribute name="buttonDisplay" required="false" type="java.lang.String" %>
<%@attribute name="alwaysShowButton" required="false" type="java.lang.String" %>
<%@attribute name="onPage" required="true" type="java.lang.String" %>

<c:if test="${empty buttonDisplay}">
    <c:set var="buttonDisplay" value="Details"/>
</c:if>
<c:if test="${empty alwaysShowButton}">
    <c:set var="alwaysShowButton" value="no"/>
</c:if>

<c:if test="${!empty sessionScope.MemberCoverDetails}">
    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th align="left">Option</th>
            <th align="left">Code</th>
            <th align="left">First Name</th>
            <th align="left">Surname</th>
            <th align="left">Dependant Type</th>
            <th align="left">Date Of Birth</th>
            <th align="left">Gender</th>
            <th align="left">Id Number</th>
            <th align="left">Status</th>
            <th align="left">Join Date</th>
            <th align="left">Received Date</th>
            <th align="left">Option Date</th>
            <th align="left">Resigned Date</th>
            <th align="left">Select</th>
        </tr>
        <c:set var="indexer" value="0" />
        <c:forEach var="cd" items="${sessionScope.MemberCoverDetails}" varStatus="i">
            <tr class="label">
                <td>${cd.optionName}</td>
                <td>${cd.dependentNumber}</td>
                <td>${cd.name}</td>
                <td>${cd.surname}</td>
                <td>${cd.dependentType}</td>
                <td>${sessionScope.covDateOfBirth[i.index]}</td>
                <td>${cd.gender}</td>
                <td>${cd.idNumber}</td>
                <td>${cd.status}</td>
                <c:if test="${cd.schemeJoinDate ne null}">
                    <td>${sessionScope.covJoinDate[i.index]}</td>
                </c:if>
                <c:if test="${cd.schemeJoinDate == null}">
                    <td>${sessionScope.covCoverStartDate[i.index]}</td>
                </c:if>
                <td>${fn:replace(sessionScope.covApplicationRecievedDate[i.index],'2999/12/31','')}</td>
                <td>${sessionScope.covCoverStartDate[i.index]}</td>
                <td>${fn:replace(sessionScope.covCoverEndDate[i.index],'2999/12/31','')}</td>
                
               <c:if test="${cd.onPdc == 'yes'}">
                    
                    <c:if test="${onPage=='MemberS'}">
                        <td><button  style="width:100px;  " name="opperation" type="button" ${javaScript} value="${cd.dependentNumber}">${buttonDisplay}</button></td>
                    </c:if>
                 
                        <c:if test="${onPage=='SearchB'}">
                            
                         <td><button  style="width:100px;  " name="opperation" type="button" ${javaScript} value="${cd.dependentNumber}">${buttonDisplay}</button></td>
                        </c:if>
                        
                </c:if>
                    
             
                   
                <c:if test="${(cd.onPdc == 'no')&& (onPage=='MemberS')}">
                    
                     <c:if test="${cd.status == 'Resign'}">
                         <td><button style="width:100px; " name="risRatekButton" onclick="navigateToRiskrate();" value=${cd.dependentNumber} disabled="True" >Risk Rate</button>
                       </c:if>
                       
                    <c:if test="${cd.status != 'Resign'}">
                        <td><button style="width:100px; " name="risRatekButton" onclick="navigateToRiskrate('${cd.dependentNumber}', '${cd.coverNumber}', '${cd.name}', '${cd.surname}');" value=${cd.dependentNumber} >Risk Rate</button>
                     </c:if>
                
                      </c:if>
                  
                    <c:if test="${(onPage=='SearchB') && (cd.onPdc == 'no')}">  
                       <td><button  style="width:100px;  " name="opperation" type="button" ${javaScript} value="${cd.dependentNumber}">${buttonDisplay}</button></td>
                               
                     </c:if>   
                    
                <c:set var="indexer" value="${cd.dependentNumber}" />
            </tr>
        </c:forEach>
    </table>
</c:if>
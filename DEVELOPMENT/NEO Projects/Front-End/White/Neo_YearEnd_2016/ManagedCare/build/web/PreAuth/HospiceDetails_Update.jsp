<%-- 
    Document   : HospiceDetails_Update
    Created on : 2011/11/30, 05:12:04
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="neo.manager.NeoUser" %>
<%@ page import="neo.manager.SecurityResponsibility" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type="text/javascript" language="JavaScript">
            //set current user
            <%
                NeoUser us2 = (NeoUser) session.getAttribute("persist_user");
                session.setAttribute("currentUser", us2.getName() + " " + us2.getSurname());
            %>

            $(function () {
                CheckSearchCommands();
            });

            function setCoPayAccordingToPMB() {
                //set copay zero if pmb
                var pmbFound = $("#pmbFound").val();
                var isPMB = $("#isPMB").val();
                if (pmbFound === "yes" && isPMB === "yes") {
                    $("#coPay").val("0.0");
                    $("#coPay_error").val("");
                }
            }

            function GetXmlHttpObject() {
                if (window.ActiveXObject) {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function GetDOMParser(xmlStr) {
                var xmlDoc;
                if (window.DOMParser) {
                    parser = new DOMParser();
                    xmlDoc = parser.parseFromString(xmlStr, "text/xml");
                } else {
                    xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async = "false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }

            function setICDToUpper(icd) {
                var icd = $.trim(icd);
                var tc = icd.charAt(0).toUpperCase();
                var sub = tc + icd.substring(1, icd.length);
                return sub;
            }

            function getICD10ForCode(str, element) {
                xhr = GetXmlHttpObject();
                if (xhr == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=GetICD10DetailsByCode&code=" + str + "&element=" + element;
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        var error = "#" + element + "_error";
                        var jqElement = "#" + element + "_text";
                        var descr = "#" + element + "Description";
                        $(error).text("");
                        if (result == "Error") {
                            $(error).text("No such diagnosis");
                        } else {
                            var up = setICDToUpper(str);
                            $(jqElement).val(up);
                            var description = xhr.responseText.substring(xhr.responseText.lastIndexOf('=') + 1, xhr.responseText.lastIndexOf('$'));
                            $(descr).text(description);
                            $("#icd3DescRow").show();
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function setMultiICDToUpper(icds) {
                var returnStr = "";
                var newIcd;
                var icdList = icds.split(",");
                if (icdList.length == 1) {
                    return returnStr = setICDToUpper(icds);
                } else {
                    for (i = 0; i < icdList.length; i++) {
                        var icd = icdList[i];
                        newIcd = setICDToUpper(icd);
                        returnStr = returnStr + newIcd + ",";
                    }
                    return returnStr.substring(0, returnStr.length - 1);
                }

            }

            function getMultiICD10ForCode(str, element) {
                xhr = GetXmlHttpObject();
                if (xhr == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=GetMultipleICD10DetailsByCode&code=" + str + "&element=" + element;
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var rText = xhr.responseText.split("|");
                        var result = rText[0];
                        var error = "#" + element + "_error";
                        var jqElement = "#" + element + "_text";
                        var descr = "#" + element + "Description";
                        $(error).text("");
                        if (result == "Error") {
                            $(error).text(rText[1]);
                        } else if (result == "Done") {
                            var up = setMultiICDToUpper(str);
                            $(jqElement).val(up);
                            var description = rText[1];
                            //set descriptions in order of codes
                            $(descr).text(description);
                            $("#icd2DescRow").show();
                            reloadPMBDetails();
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function CheckSearchCommands() {

                $("#penReasonRow").hide();
                $("#penRow").hide();
                $("#dspcReasonRow").hide();
                $("#dspcRow").hide();

                //reload ajax commands for screen population
                var searchVal = document.getElementById('searchCalled').value;
                if (searchVal == 'facilityProv') {
                    validatePractice(document.getElementById('facilityProv_text').value, 'facilityProv');
                }
                if (searchVal == 'anaesthetistProv') {
                    validatePractice(document.getElementById('anaesthetistProv_text').value, 'anaesthetistProv');

                } else if (searchVal == 'secondaryICD') {
                    getMultiICD10ForCode(document.getElementById("secondaryICD_text").value, 'secondaryICD');

                } else if (searchVal == 'coMorbidityICD') {
                    getMultiICD10ForCode(document.getElementById("coMorbidityICD_text").value, 'coMorbidityICD');
                } else if (searchVal === 'authLOCButton') {
                    setSeventyTwoHourPenalty();

                } else if (searchVal === 'cptButton') {
                    setSeventyTwoHourPenalty();

                } else if (searchVal === 'authTariffButton') {
                    setSeventyTwoHourPenalty();
                }

                //dsp copay
                if (${applicationScope.Client != 'Sechaba'}) {
                    var authDSPCopay = '<%= session.getAttribute("isDSPPenalty")%>';
                    if (authDSPCopay !== null && authDSPCopay !== "null"
                            && authDSPCopay === "yes") {
                        setAuthDSPCopay();
                    }
                }

                document.getElementById('searchCalled').value = '';
                //toggleHospTariff(document.getElementById("awType").value);
                togglePMBOnLoad();
                //checkForOldAuth();
                //VALIDATE FACILITY PROVIDER
                var str = $("#facilityProv_text").val();
                validatePractice(str, 'facilityProv');

            }

            function setAuthDSPCopay() {
                var scheme = '<%= session.getAttribute("schemeName")%>';
                if (scheme !== "Resolution Health" && ${applicationScope.Client != 'Sechaba'}) {
                    $("#dspcReasonRow").show();
                    $("#dspcRow").show();
                }
            }

            function setSeventyTwoHourPenalty() {
                var applyPen = $("#applyPen").val();
                if (applyPen === "yes") {
                    $("#penaltyPerc").val("1");
                    $("#penaltyReason").val("1");

                    $("#penReasonRow").show();
                    $("#penRow").show();
                    $("#AuthNWarning").show();
                } else {
                    $("#penaltyPerc").val("99");
                    $("#penaltyReason").val("99");

                    $("#penReasonRow").hide();
                    $("#penRow").hide();
                    $("#AuthNWarning").hide();
                    togglePenalty();
                }
            }

            function togglePenalty() {

                var applyPen = $("#applyPen").val();
                var applyDSPPen = '<%= session.getAttribute("isDSPPenalty")%>';

                var noPenalty = false;
                var noDSPCoPay = false;
                //72 hour penalty
                if (applyPen !== null && applyPen !== "" && applyPen === "yes") {
                    $("#penaltyPerc").val("1");
                    $("#penaltyReason").val("1");
                    $("#penReasonRow").show();
                    $("#penRow").show();
                    $("#AuthNWarning").show();

                } else {
                    $("#penaltyPerc").val("99");
                    $("#penaltyReason").val("99");
                    $("#penReasonRow").hide();
                    $("#penRow").hide();
                    noPenalty = true;
                }

                //DSP Copayment
                if (applyDSPPen === "yes") {
                    $("#dspPenaltyPerc").val("3");
                    $("#dspPenaltyReason").val("1");
                    $("#dspcReasonRow").show();
                    $("#dspcRow").show();
                    $("#AuthNWarning").show();

                } else {
                    $("#dspPenaltyPerc").val("99");
                    $("#dspPenaltyReason").val("99");
                    $("#dspcReasonRow").hide();
                    $("#dspcRow").hide();
                    noDSPCoPay = true
                }

                if (noDSPCoPay && noPenalty) {
                    $("#AuthNWarning").hide();
                }

            }

            //practice search
            function validatePractice(str, element) {
                //alert("validateProvider str = "+str);
                var error = "#" + element + "_error";
                if (str.length > 0) {
                    var url = "/ManagedCare/AgileController";
                    var data = {'opperation': 'FindPracticeDetailsByCode', 'provNum': str, 'element': element, 'id': new Date().getTime()};
                    //alert("url = "+url);
                    $.get(url, data, function (resultTxt) {
                        if (resultTxt != null) {
                            //alert("resultTxt = "+resultTxt);
                            var resTxtArr = resultTxt.split("|");
                            var result = resTxtArr[0];
                            if (result == "Error") {
                                $(document).find(error).text("No such provider");
                                $("#dspcReasonRow").hide();
                                $("#dspcRow").hide();
                                $("#facilityProvName").text("");
                            } else if (result == "Done") {
                                $(document).find(error).text("");
                                //set provider name
                                var descArr = resTxtArr[1].split("=");
                                var pracName = descArr[1];
                                $("#dspcReasonRow").hide();
                                $("#dspcRow").hide();
                                $("#dspPenaltyPerc").val("99");
                                $("#dspPenaltyReason").val("99");

                                if (element == "anaesthetistProv") {
                                    $("#anaesthetistProvName").text(pracName);
                                } else {
                                    $("#facilityProvName").text(pracName);
                                }

                                //set network
                                var descArr = resTxtArr[3].split("=");
                                var net = descArr[1];
                                var length = net.length;
                                var end = length - 3;
                                var network = net.substr(0, end);
                                $("#facilityProvNetwork").text(network);
                            } else if (result === "Penalty") {
                                $(document).find(error).text("");
                                //set provider name
                                var descArr = resTxtArr[1].split("=");
                                var pracName = descArr[1];
                                $("#facilityProvName").text(pracName);
                                $("#facilityProvNetwork").text("*Provider not part of DSP network, penalty applies");

                                $("#dspPenaltyPerc").val("3");
                                $("#dspPenaltyReason").val("1");

                                $("#dspcReasonRow").show();
                                $("#dspcRow").show();
                                $("#AuthNWarning").show();

                            }
                        }
                    });
                }
            }

            function reloadPMBDetails() {
                //get form values
                var secIcd = $("#secondaryICD_text").val();
                var coMorIcd = $("#coMorbidityICD_text").val();

                var url = "${pageContext.request.contextPath}/AgileController";
                var data = {
                    'opperation': 'GetPMBByICDCommand',
                    'secondaryICD_text': secIcd,
                    'coMorbidityICD_text': coMorIcd,
                    'id': new Date().getTime()
                };

                $.get(url, data, function (resultTxt) {
                    if (resultTxt !== null) {
                        var resTxtArr = resultTxt.split("|");
                        var result = resTxtArr[0];
                        if (result === "Done") {
                            submitICDForcedRefreshWithScreen("/PreAuth/HospiceDetails_Update.jsp");
                        }
                    }
                });
                data = null;

            }

            function togglePMB() {
                var pmb = $("#pmb").val();
                if (pmb == "99") {

                } else if (pmb == "1") {
                    $("#coPay").val("0.0");
                    $("#coPay_error").val("");
                    var applyDSP = $("#applyDSPCoPay").val();
                    if (applyDSP != null && applyDSP != "" && applyDSP != "null" && applyDSP == "yes") {
                        $("#coPay").val("3300.00");
                        $("#coPay_error").val("*Note: An addtional R3300 for Non-DSP Applies");
                    }

                } else {
                    xhr = GetXmlHttpObject();
                    if (xhr == null) {
                        alert("Your browser does not support XMLHTTP!");
                        return;
                    }

                    var url = "/ManagedCare/AgileController?opperation=GetCoPaymentFromCPTs";
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.statusText == "OK") {
                            $("#cptButton_error").text("");
                            var rText = xhr.responseText.split("|");
                            var result = rText[0];
                            //readonly remove validation
                            var readOnlyRemove = rText[1];
                            if (readOnlyRemove == "yes") {
                                $("#coPay").attr('readonly', false);
                            } else {
                                $("#coPay").attr('readonly', true);
                            }

                            if (result == "Error") {
                                $("#cptButton_error").text("No CPT(s) Found");
                                $("#coPay").val("0.0");

                            } else if (result == "Done") {
                                $("#coPay").val(rText[2]);
                            }
                        }
                    };
                    xhr.open("POST", url, true);
                    xhr.send(null);
                }
            }

            function togglePMBOnLoad() {
                var pmb = $("#pmb").val();
                if (pmb == "99") {

                } else if (pmb == "1") {
                    $("#coPay").val("0.0");
                    $("#coPay_error").val("");

                    var applyDSP = $("#applyDSPCoPay").val();
                    if (applyDSP != null && applyDSP != "" && applyDSP != "null" && applyDSP == "yes") {
                        $("#coPay").val("3300.00");
                        $("#coPay_error").val("*Note: An addtional R3300 for Non-DSP Applies");
                    }

                } else {
                    xhr = GetXmlHttpObject();
                    if (xhr == null) {
                        alert("Your browser does not support XMLHTTP!");
                        return;
                    }

                    var url = "/ManagedCare/AgileController?opperation=GetCoPaymentFromCPTs";
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.statusText == "OK") {
                            $("#cptButton_error").text("");
                            var rText = xhr.responseText.split("|");
                            var result = rText[0];
                            //readonly remove validation
                            var readOnlyRemove = rText[1];
                            if (readOnlyRemove == "yes") {
                                $("#coPay").attr('readonly', false);
                            } else {
                                $("#coPay").attr('readonly', true);
                            }

                            if (result == "Error") {
                                $("#cptButton_error").text("No CPT(s) Found");
                                $("#coPay").val("0.0");

                            } else if (result == "Done") {
                                var copay = '<%= session.getAttribute("coPay")%>';
                                $("#coPay").val(copay);
                            }
                            setCoPayAccordingToPMB();
                        }
                    };
                    xhr.open("POST", url, true);
                    xhr.send(null);
                }
            }


            function getUserResp() {
                var valid = false;
            <%
                NeoUser user = (NeoUser) session.getAttribute("persist_user");
                int userRespId = 0;
                for (SecurityResponsibility security : user.getSecurityResponsibility()) {
                    userRespId = security.getResponsibilityId();
                }
                if (userRespId == 2 || userRespId == 3) {
            %>
                valid = true;
            <%}%>

                return valid;
            }

            function setPenaltyReasonMandatory(str) {
                if (str !== "1") {
                    $("#madatoryReason").val("yes");
                }
            }

            function setDSPCopayReasonMandatory(str) {
                if (str !== "2" && str !== "1") {
                    $("#madatoryReason2").val("yes");
                }
            }

            function setPenaltyReason(str) {
                var applyPen = $("#applyPen").val();
                if (applyPen !== null) {
                    if (applyPen === "yes") {
                        if (str !== "1") {
                            $("#madatoryReason").val("no");
                        }
                    }
                }
            }

            function setDSPCopayReason(str) {
                var applyDSPPen = '<%= session.getAttribute("isDSPPenalty")%>';
                if (applyDSPPen !== null) {
                    if (applyDSPPen === "yes") {
                        if (str !== "1") {
                            $("#madatoryReason2").val("no");
                        }
                    }
                }
            }

            function validateHospice(command) {

                var fromDate = $("#admissionDateTime").val();

                //validate penalty change
                var penRChanged = $("#madatoryReason").val();
                if (penRChanged !== null) {
                    if (penRChanged === "yes") {
                        $("#penaltyReason_error").text("Please provide reason for penalty override");
                        proceed = false;
                    }
                }
                //validate dsp copay change
                var dspRChanged = $("#madatoryReason2").val();
                if (dspRChanged !== null) {
                    if (dspRChanged === "yes") {
                        $("#dspPenaltyReason_error").text("Please provide reason for dsp penalty override");
                        proceed = false;
                    }
                }

                xhr = GetXmlHttpObject();
                if (xhr == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=ValidateHospiceCare&fromDate=" + fromDate;
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        if (result == "ERROR") {
                            var errorResponse = xhr.responseText.substring(xhr.responseText.indexOf('|') + 1, xhr.responseText.length)
                            var mandatoryList = errorResponse.split("|");
                            for (i = 0; i < mandatoryList.length; i++) {
                                var elementError = mandatoryList[i].split(":");
                                var element = elementError[0];
                                var errorMsg = elementError[1];

                                $(document).find("#" + element + "_error").text(errorMsg);
                            }

                        } else if (result == "Done") {
                            validateHospitalDetails(command);

                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function validateHospitalDetails(command) {
                clearHospMandatory();
                xhr = GetXmlHttpObject();
                if (xhr == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=ValidateHospitalDetails";
                //get form values
                var awType = $("#awTypeNew").val();
                var facility = $("#facilityProv_text").val();
                var rModel = $("#reimbursementModel").val();
                var pmb = $("#pmb").val();
                var funder = $("#funder").val();
                var isPMB = $("#isPMB").val();
                var foundPMB = $("#pmbFound").val();

                //validate penalty change
                var penRChanged = $("#madatoryReason").val();
                if (penRChanged !== null) {
                    if (penRChanged === "yes") {
                        $("#penaltyReason_error").text("Please provide reason for penalty override");
                        proceed = false;
                    }
                }
                //validate dsp copay change
                var dspRChanged = $("#madatoryReason2").val();
                if (dspRChanged !== null) {
                    if (dspRChanged === "yes") {
                        $("#dspPenaltyReason_error").text("Please provide reason for dsp penalty override");
                        proceed = false;
                    }
                }

                //dates
                var admitDate = $("#admissionDateTime").val();
                var disDate = $("#dischargeDateTime").val();

                //check icds
                var secIcd = $("#secondaryICD_text").val();
                var coMorIcd = $("#coMorbidityICD_text").val();
                var appendedVals = new String;
                if (secIcd != null && secIcd != "") {
                    appendedVals = appendedVals + "&secIcd=" + secIcd;
                }
                if (coMorIcd != null && coMorIcd != "") {
                    appendedVals = appendedVals + "&comIcd=" + coMorIcd;
                }
                var copayAmount = $("#coPay").val();

                url = url + "&fromScreen=capture&awTypeNew=" + awType + "&facility=" + facility + "&rModel=" + rModel;
                url = url + "&admissionDateTime=" + admitDate + "&dischargeDateTime=" + disDate;
                url = url + "&pmb=" + pmb + "&funder=" + funder + "&copay=" + copayAmount + "&isPMB=" + isPMB + "&foundPMB=" + foundPMB + appendedVals;

                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));

                        if (result == "Error") {
                            var errorResponse = xhr.responseText.substring(xhr.responseText.indexOf('|') + 1, xhr.responseText.length)
                            var mandatoryList = errorResponse.split("|");
                            for (i = 0; i < mandatoryList.length; i++) {
                                var elementError = mandatoryList[i].split(":");
                                var element = elementError[0];
                                var errorMsg = elementError[1];

                                $(document).find("#" + element + "_error").text(errorMsg);
                            }

                        } else if (result == "Done") {
                            document.getElementById('saveButton').disabled = true;
                            //$("#saveButton").hide();
                            submitWithAction(command);
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function validateCopay(str) {
                var strChar;
                var valid = true;
                var strValidChars = "0123456789.";

                for (i = 0; i < str.length; i++) {
                    strChar = str.charAt(i);
                    if (strValidChars.indexOf(strChar) == -1) {
                        valid = false;
                    }
                }
                if (valid == false) {
                    $("#coPay_error").text("Invalid Amount");
                } else {
                    $("#coPay_error").text("");
                }
            }

            function setHospStatus(status) {
                if (status != null && status != "99") {
                    xhr = GetXmlHttpObject();
                    if (xhr == null) {
                        alert("ERROR: Browser Incompatability");
                        return;
                    }
                    var url = "/ManagedCare/AgileController";
                    url += "?opperation=GetHospitalStatusCommand&status=" + status;
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.statusText == "OK") {
                            $("#hospStatus").empty();
                            $("#hospStatus").append("<option value=\"99\"></option>");
                            var xmlDoc = GetDOMParser(xhr.responseText);
                            //print value from servlet
                            $(xmlDoc).find("HospitalStatus").each(function () {
                                //extract status info from tag
                                $(this).find("StatusInfo").each(function () {
                                    var statusInfo = $(this).text();
                                    var statusValues = statusInfo.split("|");
                                    //Add option to dependant dropdown
                                    var optVal = document.createElement('OPTION');
                                    optVal.value = statusValues[0];
                                    optVal.text = statusValues[1];
                                    document.getElementById("hospStatus").options.add(optVal);
                                });
                                //error check
                                var errorText = $(this).text();
                                var errors = errorText.split("|");
                                if (errors[0] == "ERROR") {
                                    $("#hospStatus_error").text(errors[1]);
                                }

                            });
                        }
                    }
                    xhr.open("POST", url, true);
                    //xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                    xhr.send(null);
                }
            }

            //provider search
            function validateProvider(command, button, page) {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }

                var str = "<%= session.getAttribute("treatingProvider_text")%>";
                var element = "authTariffButton";

                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=FindProviderDetailsByCode&provNum=" + str + "&element=treatingProvider";
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        var error = "#" + element + "_error";
                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                        $(document).find(error).text("");
                        if (result == "Error") {
                            $(document).find(error).text("Error when loading treating provider");
                        } else if (result == "Done") {
                            $(document).find(error).text("");
                            submitWithAction(command, button, page);
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function clearHospMandatory() {
                $("#awType_error").text("");
                $("#facilityProv_error").text("");
                $("#reimbursementModel_error").text("");
                $("#pmb_error").text("");
                $("#funder_error").text("");
                $("#cptButton_error").text("");
                $("#authTariffButton_error").text("");
                $("#authLOCButton_error").text("");
            }

            function clearElement(element) {
                part = element + '_text';
                document.getElementById(element).value = '';
                document.getElementById(part).value = '';
            }

            function validateECDoub(str, element) {
                var check = validateDouble(str);
                var errorField = "#" + element + "_error";

                if (check === false) {
                    $(errorField).text("Invalid Amount : " + str);
                } else {
                    $(errorField).text("");
                }
            }

            function submitICDForcedRefreshWithScreen(screen, scrollField) {
                $("#opperation").val("RefreshCommand");
                $("#searchCalled").val(scrollField);
                $("#refreshScreen").val(screen);
                document.forms[0].submit();
            }

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Hospice Detail</label>
                    <br/><br/>
                    <agiletags:ControllerForm name="HospitalDetails" validate="yes">
                        <table>
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="applyDSPCoPay" id="applyDSPCoPay" value="${sessionScope.dspCopayApplies}" />
                            <input type="hidden" name="isPMB" id="isPMB" value="${empty sessionScope.savedAuthPMBs ? "no" : "yes"}" />
                            <input type="hidden" name="pmbFound" id="pmbFound" value="${empty sessionScope.authPMBDetails ? "no" : "yes"}" />
                            <input type="hidden" name="refreshScreen" id="refreshScreen" value="" />
                            <input type="hidden" name="primaryICD" id="primaryICD" value="${sessionScope.primaryICD_text}"/>

                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Reimbursement Model" elementName="reimbursementModel" lookupId="122" mandatory="yes"  errorValueFromSession="yes"/></tr>

                            <tr><agiletags:LabelTextBoxError displayName="Admission Date" elementName="admissionDateTime" valueFromSession="yes" readonly="yes" /></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Discharge Date" elementName="dischargeDateTime" valueFromSession="yes" readonly="yes" /></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Length Of Stay" elementName="los" valueFromSession="yes" readonly="yes" /><tr>
                            <tr id="MandatoryFacility"><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Facility Provider" elementName="facilityProv" searchFunction="yes" searchOperation="ForwardToSearchPracticeCommand" onScreen="/PreAuth/HospiceDetails_Update.jsp" mandatory="yes" javaScript="onChange=\"validatePractice(this.value, 'facilityProvider');\""/></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Provider Name" elementName="facilityProvName" valueFromSession="yes" javaScript=""/></tr>
                            <tr><td colspan="5"></td></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Network" elementName="facilityNetwork" valueFromSession="yes" javaScript=""/></tr>                            
                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Anaesthetist Provider" elementName="anaesthetistProv" searchFunction="yes" searchOperation="ForwardToSearchPracticeCommand" onScreen="/PreAuth/HospiceDetails_Update.jsp" mandatory="no" javaScript="onChange=\"validatePractice(this.value, 'anaesthetistProvider');\""/></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Provider Name" elementName="anaesthetistProvName" valueFromSession="yes" javaScript=""/></tr>

                            <!-- Tariff Details -->
                            <tr id="TariffHeader"><td colspan="5" align="left"><label class="subheader">Tariff Details Allocated</label></td></tr>
                            <tr id="AuthTariff"><agiletags:AuthTariffListDisplayTag commandName="" sessionAttribute="tariffListArray" /></tr>
                            <tr id="AuthTButton"><agiletags:ButtonOpperationLabelError elementName="authTariffButton" type="button" commandName="ForwardToAuthTariffCommand" valueFromSession="no" displayName="Add New Tariff" mandatory="yes" javaScript="onClick=\"validateProvider('ForwardToAuthTariffCommand','authTariffButton','/PreAuth/HospiceDetails_Update.jsp')\";"/></tr>
                            <tr><td colspan="5"></td></tr>

                            <tr><td colspan="5"></td></tr>
                            <tr id="CPTHeader"><td colspan="5" align="left"><label class="subheader">CPT Code(s) Allocated</label></td></tr>
                            <tr id="CPTTable"><agiletags:AuthCPTListDisplay commandName="" sessionAttribute="authCPTListDetails" javaScript="" /></tr>
                            <tr id="CPTButton"><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Managed CPT Code(s)" elementName="cptButton" javaScript="onClick=\"submitWithAction('ForwardToAuthSpecificCPT','cptButton','/PreAuth/HospiceDetails_Update.jsp');\"" mandatory="yes" /></tr>

                            <tr><td colspan="5"></td></tr>
                            <tr><td colspan="5" align="left"><label class="subheader">Hospital Level Of Care Details</label></td></tr>
                            <tr><agiletags:AuthLevelOfCareListDisplay sessionAttribute="AuthLocList" /></tr>
                            <tr><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Manage Level of Care" elementName="authLOCButton" javaScript="onClick=\"submitWithAction('ForwardToAuthLOC','basketButton','/PreAuth/HospiceDetails_Update.jsp');\"" mandatory="no" /></tr>
                            <tr><td colspan="5"></td></tr>                            

                            <tr><agiletags:LabelTextBoxError displayName="Hospital Interim Amount" elementName="hospInterim" valueFromSession="yes" mandatory="yes" javaScript="onblur=\"validateECDoub(this.value, 'savingAchieved');\""/></tr>
                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Secondary ICD10" elementName="secondaryICD" searchFunction="yes" searchOperation="ForwardToSearchPreAuthMultiICD10" onScreen="/PreAuth/HospiceDetails_Update.jsp" mandatory="no" javaScript="onChange=\"getMultiICD10ForCode(this.value, 'secondaryICD');\"" /></tr>
                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Co-Morbidity ICD10" elementName="coMorbidityICD" searchFunction="yes" searchOperation="ForwardToSearchPreAuthMultiICD10" onScreen="/PreAuth/HospiceDetails_Update.jsp" mandatory="no" javaScript="onChange=\"getMultiICD10ForCode(this.value, 'coMorbidityICD');\"" /></tr>
                        </table>
                        <br/>
                        <table>
                            <c:choose>
                                <c:when test="${empty sessionScope.authPMBDetails}"></c:when>
                                <c:when test="${sessionScope.authPMBDetails == null}"></c:when>
                                <c:otherwise>
                                    <tr id="PMBHeader"><td colspan="5" align="left"><label class="subheader">Auth PMB Details</label></td></tr>
                                    <tr id="PMBTable"><agiletags:AuthPMBListDisplay commandName="" javaScript="" /></tr>
                                    <tr id="PMBButton"><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Managed PMB(s)" elementName="pmbButton" javaScript="onClick=\"submitWithAction('ForwardToAuthSpecificPMB','pmbButton','/PreAuth/HospiceDetails_Update.jsp');\"" mandatory="yes" /></tr>
                                </c:otherwise>
                            </c:choose>
                        </table>
                        <br/>
                        <table>
                        <!--<tr><agiletags:LabelNeoLookupValueDropDownError displayName="PMB" elementName="pmb" lookupId="67" mandatory="yes"  errorValueFromSession="yes" javaScript="onblur=\"togglePMB();\""/></tr>-->
                            <c:if test="${applicationScope.Client != 'Sechaba'}">
                                <tr><agiletags:LabelTextBoxError displayName="Co-Payment" elementName="coPay" valueFromSession="yes" readonly="yes" javaScript="onchange=\"validateCopay(this.value);\"" /></tr>
                                <!-- PENALTY -->
                                <tr id="penRow"><agiletags:LabelNeoLookupValueDropDownError displayName="Penalty %" elementName="penaltyPerc" errorValueFromSession="yes" mandatory="yes" lookupId="288" javaScript="onChange=\"setPenaltyReasonMandatory(this.value);\"" /></tr>
                                <tr id="penReasonRow"><agiletags:LabelNeoLookupValueDropDownError displayName="Penalty Override Reason" elementName="penaltyReason" errorValueFromSession="yes" mandatory="yes" lookupId="287" javaScript="onChange=\"setPenaltyReason(this.value);\"" /></tr>
                                <!-- DSP Copay -->
                                <tr id="dspcRow"><agiletags:LabelNeoLookupValueDropDownError displayName="DSP Copay %" elementName="dspPenaltyPerc" errorValueFromSession="yes" mandatory="yes" lookupId="286" javaScript="onChange=\"setDSPCopayReasonMandatory(this.value);\"" /></tr>
                                <tr id="dspcReasonRow"><agiletags:LabelNeoLookupValueDropDownError displayName="DSP Override Reason" elementName="dspPenaltyReason" errorValueFromSession="yes" mandatory="yes" lookupId="285" javaScript="onChange=\"setDSPCopayReason(this.value);\"" /></tr>
                            </c:if>
                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Funder Details" elementName="funder" lookupId="123" mandatory="yes"  errorValueFromSession="yes"/></tr>

                            <!--<tr><agiletags:LabelTextAreaError valueFromSession="Yes" displayName="Notes" elementName="notes" /></tr>-->
                            <tr><td colspan="5"></td></tr>
                            <tr><td colspan="5" align="left"><label class="subheader">Auth Note Details</label></td></tr>
                            <tr><agiletags:AuthNoteListDisplay sessionAttribute="authNoteList" commandName="" javaScript="" /></tr>
                            <tr><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Manage Notes" elementName="authNoteButton" javaScript="onClick=\"submitWithAction('ForwardToAuthSpecificNote','authNoteButton','/PreAuth/HospiceDetails_Update.jsp');\"" mandatory="no" /></tr>

                            <tr><agiletags:AuthStatusByResponsibility /></tr>
                            <tr><agiletags:AuthHospitalStatusByResponsibility /></tr>
                            <tr><td colspan="5"></td></tr>
                            <tr><td colspan="5"></td></tr>
                            <tr>
                                <td colspan="2" align="left">
                                    <button name="opperation" type="button" onClick="submitWithAction('ReturnToGenericAuth', 'return', '/PreAuth/GenericAuth_Update.jsp');" value="" >Back</button>
                                    <button name="opperation" type="button" onClick="submitWithAction('ReloadGenericPreAuthCommand', 'reload', '/PreAuth/HospiceDetails_Update.jsp');" value="" >Reset</button>
                                    <button id="saveButton" name="saveButton" type="button" onClick="validateHospice('SaveUpdatedPreAuthDetailsCommand');" value="" >Save</button>
                                </td>
                            </tr>
                        </table>
                    </agiletags:ControllerForm>
                    <br/>
                    <table align="right">
                        <tr><agiletags:LabelTextDisplay displayName="Last Updated By" elementName="lastUpUser" boldDisplay="yes" javaScript="" valueFromSession="yes" /></tr>
                        <tr><agiletags:LabelTextDisplay boldDisplay="yes" displayName="Current User" elementName="currentUser" javaScript="" valueFromSession="yes" /></tr>
                    </table>
                </td></tr></table>
    </body>
</html>
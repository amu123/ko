<%-- 
    Document   : Employer
    Created on : 2012/03/16, 11:06:14
    Author     : yuganp
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Membership/MemberMaintenance.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/ckeditor/ckeditor.js"></script>
        <style>
            #header { position: fixed; top: 0; left: 8px; right: 8px; z-index: 999;background: #fff;}
            #footer { position: fixed; bottom: 0; left: 0; height: 100px; }
            #content {position: relative; margin-top: 100px;}
        </style>
        <script>
            $(document).ready(function () {
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });

            function changeStatus() {
                document.getElementById('employerStatus').value = document.getElementById('empStatus').value;
                if (document.getElementById('employerStatus').value == '9') {
                    getPageContents(document.forms[0], null, 'main_div', 'overlay');
                } else {
                    submitFormWithAjaxPost(document.forms[0]);
                }
            }

            // HIDE MAX AMOUNT
            function hideMaxAmount(value) {
                if (value.toString() === '1') {
                    //$("#EmployerBilling_maxAmountRow").hide();
                    var row = document.getElementById("EmployerBilling_maxAmountRow");
                    row.style.display = 'none';
                } else if (value.toString() === '2') {
                    //$("#EmployerBilling_maxAmountRow").show();
                    var row = document.getElementById("EmployerBilling_maxAmountRow");
                    row.style.display = '';
                }
            }

            function resizeContent() {
                $('#content').css('margin-top', $('#header').height());
            }

            function returnToParent() {
                document.forms['BranchControllerForm'].submit();
            }
        </script>
    </head>
    <body onload="initAgileTabs();">
        <agiletags:ControllerForm name="TabControllerForm">
            <input type="hidden" name="opperation" id="opperation" value="EmployerTabContentsCommand" />
            <input type="hidden" name="onScreen" id="onScreen" value="Employer" />
            <input type="hidden" name="employerEntityId" id="employerEntityId" value="${requestScope.employerEntityId}" />
            <input type="hidden" name="employerParentEntityId" id="employerParentEntityId" value="${requestScope.parentEntityId}" />
            <input type="hidden" name="employerStatus" id="employerStatus" value="${requestScope.empStatus}" />
            <input type="hidden" name="menuResp" id="menuResp" value="${param.menuResp}" />
        </agiletags:ControllerForm>
        <agiletags:ControllerForm name="BranchControllerForm">
            <input type="hidden" name="opperation" id="opperation" value="EmployerViewCommand" />
            <input type="hidden" name="employerId" id="ParentemployerEntityId" value="${requestScope.parentEntityId}" />
        </agiletags:ControllerForm>
        <div id="main_div">

            <div id="header">
                <ol id="tabs" class="${empty requestScope.employerEntityId?"hide":""}">
                    <li><a href="#EmployerDetails" class="label">${empty requestScope.parentEntityId ? 'Group Details' : 'Branch Details'}</a></li>
                    <li><a href="#EmployerEligibility" class="">Eligibility</a></li>
                    <li><a href="#EmployerBilling" class="">Billing</a></li>
                    <li><a href="#EmployerCommunication" class="">Communication</a></li>
                    <li><a href="#EmployerIntermediary" class="">Intermediary</a></li>

                    ${empty requestScope.parentEntityId ? '<li><a href="#EmployerBranches" class="">Branches</a></li>' : ''}
                    <li><a href="#EmployerUnderwriting" class="">Underwriting</a></li>
                    <li><a href="#Members" class="">Members</a></li>
                    <li><a href="#Documents" class="">Documents</a></li>
                    <li><a href="#EmployerAudit" class="">Audit Trail</a></li>
                    <li><a href="#Notes"  >Notes</a></li>
                    <li><a href="#EmployerStatements">Statements</a></li>
                    <li><a href="#EmployerTransactions">Transactions</a></li>
                        <c:if test="${applicationScope.Client == 'Sechaba'}">
                        <li><a href="#EmployerGroupUnion">Union</a></li>
                        <li><a href="#EmployerCRMCommunication">CRM Communication</a></li>
                        </c:if>
                </ol>
                <c:choose>
                    <c:when test="${param.menuResp == 'customerCare'}">
                        <fieldset disabled>
                        </c:when>
                        <c:otherwise>
                            <fieldset>
                            </c:otherwise>
                        </c:choose>
                        <table width="100%" bgcolor="#dddddd" style="border: 1px solid #c9c3ba;">
                            <tr>

                                <td width="375" class="label"><span style="font-size: 1.0em;">${empty requestScope.parentEntityId ? 'Group Name: ' : 'Branch Name : '}<label id="employer_header_employerName" >${requestScope.employerName}</label></span></td>
                                <td class="label"><span style="font-size: 1.0em;">${empty requestScope.parentEntityId ? 'Group Number: ' : 'Branch Number : '}<label id="employer_header_employerNumber">${requestScope.employerNumber}</label></span></td>
                                <td>
                                    <c:if test="${!empty requestScope.parentEntityId }">
                                        <button type="button" onclick="returnToParent(${requestScope.parentEntityId});">Return to Main Group</button>
                                    </c:if>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="label">Status: 
                                    <select  id="empStatus" onchange="document.getElementById('changeStatusBtn').style.visibility = 'visible';" >
                                        <c:if test="${requestScope.empStatus != 9}">
                                            <option value="1" ${empty requestScope.empStatus || requestScope.empStatus == 1 ? "selected" : ""}>Capturing in progress</option>
                                            <option value="2" ${requestScope.empStatus == 2 ? "selected" : ""}>Waiting for documentation</option>
                                            <option value="3" ${requestScope.empStatus == 3 ? "selected" : ""}>Validated and ready for quality assessment</option>
                                            <option value="4" ${requestScope.empStatus == 4 ? "selected" : ""}>Cleared for underwriting</option>
                                            <option value="5" ${requestScope.empStatus == 5 ? "selected" : ""}>Quality assessment failed</option>
                                            <option value="6" ${requestScope.empStatus == 6 ? "selected" : ""}>Underwriting in progress</option>
                                            <option value="7" ${requestScope.empStatus == 7 ? "selected" : ""}>Active</option>
                                            <option value="8" ${requestScope.empStatus == 8 ? "selected" : ""}>Not taken up (NTU)</option>
                                        </c:if>
                                        <option value="9" ${requestScope.empStatus == 9 ? "selected" : ""}>Terminate</option>
                                        <c:if test="${requestScope.empStatus == 9}">
                                            <option value="10">Reinstate</option>
                                        </c:if>
                                    </select>
                                    <button style="visibility: hidden;" id="changeStatusBtn" value="Update Status" onclick="this.style.visibility = 'hidden';
                                            changeStatus();">Change Status</button></td>
                            </tr>
                        </table>
                    </fieldset>
            </div>
            <c:choose>
                <c:when test="${param.menuResp == 'customerCare'}">
                    <fieldset disabled>
                    </c:when>
                    <c:otherwise>
                        <fieldset>
                        </c:otherwise>
                    </c:choose>
                    <div  id="content">
                        <div class="tabContent" id="EmployerDetails">Loading...</div>
                        <div class="tabContent hide" id="EmployerEligibility">Loading...</div>
                        <div class="tabContent hide" id="EmployerBilling">Loading...</div>
                        <div class="tabContent hide" id="EmployerCommunication">Loading...</div>
                        <div class="tabContent hide" id="EmployerIntermediary">Loading...</div>
                        <div class="tabContent hide" id="EmployerBranches">Loading...</div>
                        <div class="tabContent hide" id="EmployerUnderwriting">Loading...</div>
                </fieldset>
                <div class="tabContent hide" id="EmployerAudit">Loading...</div>
                <div class="tabContent hide" id="Notes">Loading...</div>
                <div class="tabContent hide" id="Members">Loading...</div>
                <div class="tabContent hide" id="Documents">Loading...</div>
                <div class="tabContent hide" id="EmployerCRMCommunication">Loading...</div>
                <div class="tabContent hide" id="EmployerTransactions">Loading...</div>
                <c:choose>
                    <c:when test="${param.menuResp == 'customerCare'}">
                        <fieldset disabled>
                        </c:when>
                        <c:otherwise>
                            <fieldset>
                            </c:otherwise>
                        </c:choose>
                        <div class="tabContent hide" id="EmployerStatements">Loading...</div>
                        
                        <div class="tabContent hide" id="EmployerGroupUnion">Loading...</div>
                    </fieldset>
                    </div>
                    <!--</fieldset>-->
                    </div>              
                    <div id="overlay" style="display:none;"></div>
                    <div id="overlay2" style="display:none;"></div>

                    </body>
                    </html>

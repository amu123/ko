<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags/contributions" %>
<c:if test="${not empty debitOrder}">
    <nt:ContributionDebitOrderDetails items="${debitOrder}"  showHeader="false"/>
</c:if>
<c:if test="${empty debitOrder}">
    <label>No Debit Order details available</label>
</c:if>

<%-- 
    Document   : OldQuestionnaire
    Created on : 2010/06/06, 12:48:15
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
                alert(action);
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('questionIndex').value = index;
                document.forms[0].submit();
            }

            function hideDIV(label) {
                if (document.getElementById(label).style.display == "none") {
                    document.getElementById(label).style.display = '';
                } else {
                    document.getElementById(label).style.display = 'none';
                }

            }

        </script>
        <style type="text/css">
            .btn {
                color:#666666;
                font: bold 84% 'trebuchet ms',helvetica,sans-serif;
            }
        </style>

    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->

                    <agiletags:ControllerForm name="caseManangement">
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <input type="hidden" name="questionIndex" id="questionIndex" value="" />

                        <agiletags:QuestionnaireHistotyTab buttonCommand1="ShowCurrentQuestionnaire" buttonDisplay1="Questionnaire"
                                                           buttonCommand2="ShowOldQuestionnaire" buttonDisplay2="Capture New Questionnaire" disabledDisplay="" />
                        <label class="header">Event Manangement - Contact Patient</label>
                        <br/><br/>
                        <div id="page">
                            <table>
                                <tr><agiletags:LabelTextBoxError displayName="Reason" elementName="caseReason"/></tr>
                                <tr><agiletags:OldQuestionsCheckboxandLabelText elementName="question" displayName="Questions"/><tr>
                                <table>
                                    <tr>
                                        <agiletags:ButtonOpperation align="left" displayname="Back" commandName="ViewPolicyHolderDetailsCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('ViewPolicyHolderDetailsCommand')\";"/>
                                    </tr>
                                </table>
                            </table>
                        </div>

                    </agiletags:ControllerForm>
        </table>
        <br/>
    </body>
</html>


<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

  <agiletags:ControllerForm name="BrokerAccreditationForm">
    <input type="hidden" name="opperation" id="opperation" value="AddBrokerAccreditationCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberDependantAppAdd" />
    <input type="hidden" name="memberAppNumber" id="memberAppNumber" value="${memberAppNumber}" />
    <input type="hidden" name="memberAppDepNumber" id="memberAppDepNumber" value="${memberAppDepNumber}" />
    <input type="hidden" name="target_div" value="${target_div}" />
    <input type="hidden" name="main_div" value="${main_div}" />
    <input type="hidden" name="buttonPressed" id="memberAppDepAddButtonPressed" value="" />
    <input type="hidden" name="brokerEntityId" id="brokerFirmAddAccreEntityId" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="brokerTile" id="brokerFirmAddAccreTile" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="brokerContactPerson" id="brokerFirmContactAccrePerson" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="brokerRegion" id="brokerARegion" value="${requestScope.brokerRegion}" />
    <input type="hidden" name="updateOrSave" id="updateOrSaveBackend" value="" />

    <label class="header">Add Accreditation</label>
    <br>
     <table>                
        <tr id="FrimApp_brokerAccreditationTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Accreditation Type"  elementName="BrokerApp_brokerAccreditationType" lookupId="210" mandatory="yes" errorValueFromSession="yes"/></tr>
        <tr id="FirmrApp_brokerAccreditationNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Accreditation Number" elementName="BrokerApp_brokerAccreditationNumber" valueFromRequest="BrokerApp_brokerAccreditationNumber" mandatory="yes"/>        </tr>
        <tr id="FirmApp_brokerApplicationDateRow"><agiletags:LabelTextBoxDateReq  displayname="Application Date" elementName="BrokerApp_brokerApplicationDate" valueFromSession="no" mandatory="no"/>        </tr> 
        <tr id="FirmApp_brokerAuthorisationDateRow"><agiletags:LabelTextBoxDateReq  displayname="Authorisation Date" elementName="BrokerApp_brokerAuthorisationDate" valueFromSession="no" mandatory="no"/>        </tr>        
        <tr id="FirmApp_brokerStartDateRow"><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="BrokerApp_brokerStartDate" valueFromSession="yes" mandatory="yes"/>        </tr> 
        <tr id="FirmApp_brokerEndDateRow"><agiletags:LabelTextBoxDateReq  displayname="End Date" elementName="BrokerApp_brokerEndDate" valueFromSession="yes" mandatory="yes"/>        </tr> 
      </table>
      
    <hr>
    <table id="MemberAppDepSaveTable">
        <tr>
           <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
           <td><input type="button" value="Save" onclick="document.getElementById('memberAppDepAddButtonPressed').value='SaveButton'; document.getElementById('updateOrSaveBackend').value = 'save'; submitFormWithAjaxPost(this.form, 'Accreditation', null,'${main_div}', '${target_div}');"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>

<%-- 
    Document   : MemberNoteDetails
    Created on : 2013/01/21, 09:49:15
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/ckeditor/ckeditor.js"></script>
        <script>
            
            $(function(){
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });   
                
                $("#notesListDiv").show();
                $("#noteCompleteView").hide();
                
                var notesComplete = $("#notesDet").val();
                if(notesComplete != null && notesComplete != "" 
                    && notesComplete != "null"){
                    
                    $("#notesListDiv").hide();
                    $("#noteCompleteView").show();
                }
                
            });
            
            function closeDetailView(){
                $("#notesListDiv").show();
                $("#noteCompleteView").hide();
            }
            
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            
            function submitWithActionAndButton(action, button) {
                document.getElementById('opperation').value = action;
                document.getElementById('buttonPressed').value = button;
                document.forms[0].submit();
            }
            
            function submitActionButtonAndNote(action, button, noteID) {
                document.getElementById('opperation').value = action;
                document.getElementById('buttonPressed').value = button;
                document.getElementById('noteId').value = noteID;
                document.forms[0].submit();
            }
            
            
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">

                    <agiletags:CallCenterTabs cctSelectedIndex="link_ND" />

                    <fieldset id="pageBorder">

                        <div class="content_area" >

                            <label class="header">Member Note Details</label>
                            <br/><br/>
                            <agiletags:ControllerForm name="policyHolderNote">
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <input type="hidden" name="noteId" id="noteId" value="" />
                                <input type="hidden" name="buttonPressed" id="buttonPressed" value="" />
                                <input type="hidden" name="memberEntityId" id="memberEntityId" value="${sessionScope.coverEntityId}" />
                                <input type="hidden" name="notesDet" id="notesDet" value="${requestScope.noteDetails}" />
                                <label class="header">Member Notes</label>
                                <hr>

                                <table id="MemberAppNotesTable">
                                    <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Details"  elementName="notesListSelect" lookupId="200" mandatory="no" errorValueFromSession="no"  />        </tr>
                                </table>
                                <label class="subheader">Member Notes Details</label>
                                <table>
                                    <td><label>Start Date</label></td>
                                    <td><input name="note_start_date" id="note_start_date" size="30" value="${note_start_date}"></td>
                                    <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('note_start_date', this);"/></td>
                                    <td>&nbsp;</td>
                                    <td><label>End Date</label></td>
                                    <td><input name="note_end_date" id="note_end_date" size="30" value="${note_end_date}"></td>
                                    <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('note_end_date', this);"/></td>
                                    <td>&nbsp;</td>
                                    <td><input type="button" name="SearchNotesButton2" value="Search" onclick="submitWithActionAndButton('MemberNotesCommand','SearchNotesButton2');"/></td>
                                </table>
                                <div style ="display: none"  id="MemberAppNotesDetails"></div>

                            </agiletags:ControllerForm>
                            <br>

                            <hr>
                            <br>
                            <div id="notesListDiv">
                                <c:choose>
                                    <c:when test="${empty MemberNoteDetails}">
                                        <span class="label">No Notes available</span>
                                    </c:when>
                                    <c:otherwise>
                                        <table width="90%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                                            <tr>
                                                <th>Date</th>
                                                <c:if test="${notesListSelect == '7'}">
                                                    <th>Type</th>
                                                </c:if>
                                                <th>User</th>
                                                <th>Note</th>
                                                <th></th>
                                            </tr>
                                            <c:forEach var="entry" items="${MemberNoteDetails}">
                                                <tr>
                                                    <td><label>${entry['noteDate']}</label></td>
                                                    <c:if test="${notesListSelect == '7'}">
                                                        <td><label>${entry['noteGroup']}</label></td>
                                                    </c:if>
                                                    <td><label>${entry['noteUser']}</label></td>
                                                    <td><label>${entry['noteDetails']}</label></td>
                                                    <td><input type="button" value="View" name="ViewButton2" onclick="submitActionButtonAndNote('MemberNotesCommand','ViewButton2','${entry['noteId']}')"></td>
                                                </tr>
                                            </c:forEach>
                                        </table>
                                    </c:otherwise>
                                </c:choose>
                            </div>    
                            <div id="noteCompleteView">
                                <br>
                                <table>
                                    <tr>
                                        <td align="left" width="160px"><label>Note:</label></td>
                                        <td align="left">
                                            <c:if test="${empty noteId}">
                                                <textarea name="MemberAppNotesAdd_details" rows="20" cols="50">noteCompleteView ${noteDetails}</textarea>
                                            </c:if>
                                            <c:if test="${!empty noteId}">
                                                <label>${noteDetails}</label>
                                            </c:if>

                                        </td>
                                    </tr>
                                    <tr><td><input type="button" value="Return" onclick="closeDetailView();"></td></tr>
                            </div>
                        </div>
                    </fieldset>
                </td></tr></table>
    </body>
</html>

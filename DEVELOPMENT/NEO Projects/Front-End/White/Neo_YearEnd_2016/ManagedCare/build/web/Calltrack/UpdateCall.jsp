<%--
    Document   : UpdateCall
    Created on : 2010/04/07, 12:18:26
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ page import="java.util.Date" %>
<%@ page import="com.koh.calltrack.command.GenerateCallCoverCommand" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="neo.manager.NeoUser" %>
<%@ page import="neo.manager.SecurityResponsibility" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script language="JavaScript">

            function loadNewCallPage() {
                //toggle first
                toggleCallType();
                checkNotes();
                checkCallStatus();
                diablecheckBoxes();
                hideSubCategory();

                var max_length = $("#notesLength").text().length;
                //alert(max_length);
                whenKeyDown(max_length);
                // toggleReferred();

            <%
                Date today = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
                String date = sdf.format(today);
                String trackDate = sdf2.format(today);
                //  String ctNumber = "CT" + trackDate + "N#";
                NeoUser user = (NeoUser) session.getAttribute("persist_user");
                session.setAttribute("uName", user.getName());
                session.setAttribute("disDate", date);
                // session.setAttribute("callTrackNumber", ctNumber);
            %>

            <%
                for (SecurityResponsibility security : user.getSecurityResponsibility()) {
                    if (security.getResponsibilityId() == 7 || security.getResponsibilityId() == 9) {%>
                $("#updateClosed").hide();
            <%} else {%>
                $("#updateClosed").show();

            <%}
                }%>

            }

            function toggleCallType() {
                var value = document.getElementById('callType').value;
                if (value === '2') {
                    var state = document.getElementById('providerType').style.display;
                    document.getElementById('providerType').style.display = '';
                    //document.getElementById('popCover').style.display = '';
                    document.getElementById('popCoverSearch').style.display = 'none';
                    document.getElementById('memberGrid').style.display = 'none';
                } else {
                    document.getElementById('providerType').style.display = 'none';
                    //document.getElementById('popCover').style.display = 'none';
                    document.getElementById('popCoverSearch').style.display = '';
                    document.getElementById('memberGrid').style.display = '';
                }
            }

            //function toggleReferred() {
            // var value = document.getElementById('callStatus').value;
            // if(value == '2'){
            // var state = document.getElementById('referedUsers').style.display;
            //document.getElementById('referedUsers').style.display = '';
            // }else{
            //  document.getElementById('referedUsers').style.display = 'none';
            // }
            // }

            function checkNotes() {
                var noteLen = $("#notesLength").val().length;
                //alert(noteLen);
            }

            function checkCallStatus() {
                var value = document.getElementById('callStatus').value;
                if (value === '3') {
                    $("#callStatus").attr("disabled", true);
                    $("#updateClosed").hide();
                }
            }

            function whenKeyDown(max_length) {
                $("#notesLength").unbind().keyup(function () {
                    if (document.activeElement.id === "notesLength") {
                        var text = $(this).val();

                        var numOfChars = text.length;

                        if (numOfChars <= max_length) {
                            // $("#counter").html("").html(text.length);
                        } else {
                            $(this).val(text.substring(0, max_length));
                        }
                    }
                });
            }

            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }
            function stateChanged(element) {
                if (xmlhttp.readyState === 4)
                {
                    //alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    //alert(result);
                    if (result === "MemberSearchAjaxCommand") {
                        var dateString = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=') + 1, xmlhttp.responseText.lastIndexOf('$'));
                        var dates = new Array();
                        dates = dateString.split(',');
                        document.getElementById(element).options.length = 0;
                        for (i = 0; i < dates.length; i++) {
                            var optVal = document.createElement('OPTION');
                            optVal.value = dates[i];
                            optVal.text = dates[i];
                            document.getElementById(element).options.add(optVal);
                        }
                        document.getElementById('memberNumber_text').focus();
                    } else if (result === "MemberSearchAjaxCommand") {
                        //var number = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=')+1, xmlhttp.responseText.lastIndexOf('|'));
                        // var email = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=')+1, xmlhttp.responseText.lastIndexOf('$'));
                        document.getElementById(element + '_error').innerText = '';
                        document.getElementById(element).value = number;
                        document.getElementById('memberNumber_text').value = email;
                    } else if (result === "MemberSearchAjaxCommand") {
                        //var number = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=')+1, xmlhttp.responseText.lastIndexOf('|'));
                        // var email = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=')+1, xmlhttp.responseText.lastIndexOf('$'));
                        document.getElementById(element + '_error').innerText = '';
                        document.getElementById(element).value = number;
                        document.getElementById('memberNumber_text').value = email;
                    } else if (result === "Error") {
                        //var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|')+1, xmlhttp.responseText.length);
                        if (forCommand === "MemberSearchAjaxCommand") {
                            document.getElementById(element).options.length = 0;
                        } else if (forCommand === "MemberSearchAjaxCommand") {
                            document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element).value = '';
                            document.getElementById('memberNumber_text').value = '';
                        } else if (forCommand === "MemberSearchAjaxCommand") {
                            document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element).value = '';
                            document.getElementById('memberNumber_text').value = '';
                        }
                    }
                }
            }

            function diablecheckBoxes() {
                //Query method
                $("#queryCategories").attr("disabled", "disabled");
                $("#contactType").attr("disabled", "disabled");
                $("#doneButon").attr("disabled", "disabled");
                //Claims
                $("#claimsFC").attr("disabled", "disabled");
                $("#claimsSC").attr("disabled", "disabled");
                $("#claimsTC").attr("disabled", "disabled");
                //Stattements
                $("#statementsFC").attr("disabled", "disabled");
                $("#statementsTC").attr("disabled", "disabled");
                //Benefits
                var product = document.getElementById('product').value;
                if (product === '1') {
                    $("#benefitsFCReso").show();
                    $("#benefitsFCSpec").hide();
                } else {
                    $("#benefitsFCReso").hide();
                    $("#benefitsFCSpec").show();
                }
                $("#benefitsFCReso").attr("disabled", "disabled");
                $("#benefitsFCSpec").attr("disabled", "disabled");
                $("#benefitsSC").attr("disabled", "disabled");
                $("#benefitsTC").attr("disabled", "disabled");
                //Premiums
                $("#prenmiumsFC").attr("disabled", "disabled");
                $("#premiumsTC").attr("disabled", "disabled");
                //Membership
                $("#membershipFC").attr("disabled", "disabled");
                $("#memberShipTC").attr("disabled", "disabled");
                //Service Provider
                $("#serviceProviderFC").attr("disabled", "disabled");
                $("#serviceProviderTC").attr("disabled", "disabled");
                //Chronic
                $("#chronicFC").attr("disabled", "disabled");
                $("#chronicSC").attr("disabled", "disabled");
                //Oncology
                $("#oncologyFC").attr("disabled", "disabled");
                $("#oncologySC").attr("disabled", "disabled");
                //Wellcare
                $("#wellcareFC").attr("disabled", "disabled");
                $("#wellcareSC").attr("disabled", "disabled");
                //Foundation
                $("#foundationFC").attr("disabled", "disabled");
                $("#foundationSC").attr("disabled", "disabled");

            }
            function hideSubCategory() {
                $("#disciplineCategory").hide();
                $("#benefitSub").show();
                $("#claimSub").show();
                $("#membershipSub").show();
                $("#UnderwritingSub").show();
                $("#queryCategories").show();

                // new categories
                $("#claimsFC").hide();
                $("#claimsSC").hide();
                $("#claimsTC").hide();
                $("#statementsFC").hide();
                $("#statementsTC").hide();
                $("#benefitsFCReso").hide();
                $("#benefitsFCSpec").hide();
                $("#benefitsSC").hide();
                $("#benefitsTC").hide();
                $("#prenmiumsFC").hide();
                $("#premiumsTC").hide();
                $("#membershipFC").hide();
                $("#memberShipTC").hide();
                $("#serviceProviderFC").hide();
                $("#serviceProviderTC").hide();
                $("#chronicFC").hide();
                $("#chronicSC").hide();
                $("#oncologyFC").hide();
                $("#oncologySC").hide();
                $("#wellcareFC").hide();
                $("#wellcareSC").hide();
                $("#foundationFC").hide();
                $("#foundationSC").hide();

            }


            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.getElementById('memNo').value = $('memberNumber_text').val();
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function hideRows(id) {
                document.getElementById(id).style.display = 'none';
                document.getElementById(id + 'plus').style.display = '';
                document.getElementById(id + 'minus').style.display = 'none';
            }
            function showRows(id) {
                document.getElementById(id).style.display = '';
                document.getElementById(id + 'minus').style.display = '';
                document.getElementById(id + 'plus').style.display = 'none';

            }

        </script>
    </head>

    <body onload="loadNewCallPage();">
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Update Call</label>
                    <br></br>
                    <table>
                        <agiletags:ControllerForm name="saveCallInfo" validate="yes">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="searchCalled" id="searchCalled" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="product" id="product" value="${productId}" />

                            <table>
                                <tr><agiletags:LabelNeoLookupValueDropDown displayName="Call Type" elementName="callType" lookupId="108" javaScript="onchange=\"toggleCallType();\""/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Call Number" elementName="callTrackNumber" valueFromSession="yes" enabled="no"/></tr>
                                <tr><agiletags:LabelNeoLookupValueDropDown displayName="Call Status" elementName="callStatus" lookupId="105" javaScript="" /></tr>

                                <!-- --> <tr id="popCoverSearch" style="display:none" align="left">

                                    <agiletags:LabelTextBoxError displayName="Member Number" elementName="memberNumber_text" valueFromSession="yes" enabled="no"/>
                                </tr>
                                <!-- -->
                                <tr id="popCover" style="display:none" align="left">
                                    <agiletags:LabelTextBoxError displayName="Member Number" elementName="memNo" valueFromSession="yes" enabled="no"/>
                                </tr>

                                <tr><agiletags:LabelTextBoxError displayName="Provider Number" elementName="providerNumber_text" valueFromSession="yes" enabled="no"/></tr>

                                <tr id="providerType" style="display:none">
                                    <agiletags:LabelTextBoxError displayName="Provider Name" elementName="provName" valueFromSession="yes" enabled="no"/>
                                    <agiletags:LabelTextBoxError displayName="Discipline" elementName="discipline" valueFromSession="yes" enabled="no"/>
                                </tr>

                                <tr><agiletags:LabelTextBoxError displayName="Caller Name" elementName="callerName"  valueFromSession="yes" /></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Contact Details" elementName="callerContact" valueFromSession="yes" /></tr>
                                <tr id="contactType"><agiletags:QueriesCheckBoxTag displayName="Query Method" elementName="queryMethod" lookupId="150" javascript="" /></tr>

                                <tr><td colspan="6"><div id="memberGrid" style="display:none">
                                            <agiletags:MemberCoverSearchTag commandName="" javaScript="" />
                                        </div></td></tr>

                                <div>
                                    <tr id="queryCategories"><agiletags:QueriesCheckBoxTag displayName="Query Categories" elementName="callQ" lookupId="151" javascript="" /></tr>
                                </div>
                                <!--                <div>
                                                    <tr id="doneButon"><td colspan="5" align="left">
                                                            <button name="opperation" type="button" value="Done">Done</button>
                                                    </td></tr>
                                                </div>-->

                                <!-- This is the part of the new lookups -->
                                <!-- Claims -->
                                <div>
                                    <tr id="claimsFC"><agiletags:QueriesCheckBoxTag displayName="Claims First Category" elementName="claimsFC" lookupId="152" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="claimsSC"><agiletags:QueriesCheckBoxTag displayName="Claims Second Category" elementName="claimsSC" lookupId="153" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="claimsTC"><agiletags:QueriesCheckBoxTag displayName="Claims Third Category" elementName="claimsTC" lookupId="154" javascript=""/></tr>
                                </div>

                                <!-- Statement -->
                                <div>
                                    <tr id="statementsFC"><agiletags:QueriesCheckBoxTag displayName="Statements First Category" elementName="statementsFC" lookupId="155" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="statementsTC"><agiletags:QueriesCheckBoxTag displayName="Statements Third Category" elementName="statementsTC" lookupId="156" javascript=""/></tr>
                                </div>

                                <!-- Benefits -->
                                <div>
                                    <tr id="benefitsFCReso"><agiletags:QueriesCheckBoxTag displayName="Benefits First Category" elementName="benefitsFCReso" lookupId="157" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="benefitsFCSpec"><agiletags:QueriesCheckBoxTag displayName="Benefits First Category" elementName="benefitsFCSpec" lookupId="284" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="benefitsSC"><agiletags:QueriesCheckBoxTag displayName="Benefits Second Category" elementName="benefitsSC" lookupId="158" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="benefitsTC"><agiletags:QueriesCheckBoxTag displayName="Benefits Third Category" elementName="benefitsTC" lookupId="159" javascript=""/></tr>
                                </div>

                                <!-- Premiums -->
                                <div>
                                    <tr id="prenmiumsFC"><agiletags:QueriesCheckBoxTag displayName="Premiums Second Category" elementName="prenmiumsFC" lookupId="160" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="premiumsTC"><agiletags:QueriesCheckBoxTag displayName="Premiums Third Category" elementName="premiumsTC" lookupId="161" javascript=""/></tr>
                                </div>

                                <!-- Membership -->
                                <div>
                                    <tr id="membershipFC"><agiletags:QueriesCheckBoxTag displayName="Membership First Category" elementName="membershipFC" lookupId="162" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="memberShipTC"><agiletags:QueriesCheckBoxTag displayName="Membership Third Category" elementName="memberShipTC" lookupId="163" javascript=""/></tr>
                                </div>

                                <!-- Service Provider -->
                                <div>
                                    <tr id="serviceProviderFC"><agiletags:QueriesCheckBoxTag displayName="Service Provider First Category" elementName="serviceProviderFC" lookupId="164" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="serviceProviderTC"><agiletags:QueriesCheckBoxTag displayName="Service Provider Third Category" elementName="serviceProviderTC" lookupId="165" javascript=""/></tr>
                                </div>
                                
                                <!-- Chronic -->
                                <div>
                                    <tr id="chronicFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Chronic First Category" elementName="chronicFC" lookupId="357" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="chronicSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Chronic Second Category" elementName="chronicSC" lookupId="358" javascript=""/></tr>
                                </div>
                                
                                <!-- Oncology -->
                                <div>
                                    <tr id="oncologyFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Oncology First Category" elementName="oncologyFC" lookupId="359" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="oncologySC"><agiletags:LabelNeoLookupValueCheckbox displayName="Oncology Second Category" elementName="oncologySC" lookupId="360" javascript=""/></tr>
                                </div>
                                
                                <!-- Wellcare -->
                                <div>
                                    <tr id="wellcareFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Wellcare First Category" elementName="wellcareFC" lookupId="361" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="wellcareSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Wellcare Second Category" elementName="wellcareSC" lookupId="362" javascript=""/></tr>
                                </div>

                                <!-- Foundation -->
                                <div>
                                    <tr id="foundationFC"><agiletags:QueriesCheckBoxTag displayName="Foundation First Category" elementName="foundationFC" lookupId="166" javascript=""/></tr>
                                </div>
                                <div>
                                    <tr id="foundationSC"><agiletags:QueriesCheckBoxTag displayName="Foundation Second Category" elementName="foundationSC" lookupId="167" javascript=""/></tr>
                                </div>
                                <!-- End of the new lookups -->
                            </table>
                            <table>

                                <tr id="notesLength"><agiletags:LabelTextAreaErrorBig displayName="Notes" elementName="notes" valueFromSession="Yes"  /></tr>

                                <tr id="updateClosed">
                                    <agiletags:ButtonOpperation commandName="UpdateLoadedCommand" align="left" displayname="Update" span="1" type="button"  javaScript="onClick=\"submitWithAction('UpdateLoadedCommand')\";"/>
                                    <agiletags:ButtonOpperation commandName="ReloadCallLogCommand" align="left" displayname="Cancel" span="1" type="button"  javaScript="onClick=\"submitWithAction('ReloadCallLogCommand')\";"/>
                                </tr>
                            </table>
                            <table align="right">
                                <tr>
                                    <td colspan="2">&nbsp;</td><agiletags:LabelTextBoxError displayName="User Name" elementName="uName" valueFromSession="Yes" enabled="no" />
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td><agiletags:LabelTextBoxError displayName="Date" elementName="disDate" valueFromSession="Yes" enabled="no"/>
                                </tr>
                            </table>

                        </agiletags:ControllerForm>
                    </table>

                    <!-- content ends here -->
                </td></tr></table>
    </body>
</html>


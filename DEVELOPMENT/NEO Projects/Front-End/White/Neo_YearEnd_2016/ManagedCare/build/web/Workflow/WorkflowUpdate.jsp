<%-- 
    Document   : WorkflowUpdate
    Created on : Aug 25, 2016, 12:34:03 PM
    Author     : martins
--%>

<%@page import="neo.manager.NeoUser"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<agiletags:ControllerForm name="WorkflowChangeStatus" action="/ManagedCare/AgileController">
    <!--<input type="hidden" name="opperation" value="WorkflowContentCommand" size="30" />-->
    <input type="hidden" name="onScreen" value="/Workflow/WorkflowUpdate.jsp" size="30" />
    <input type="hidden" name="actionDesc" value="" size="30" />
    <input type="hidden" name="workflowStatus" value="" size="30" />
    <input type="hidden" name="opperation" id="opperation" value="" />
    <input type="hidden" name="productId" id="productId" value="" />
    <input type="hidden" name="product" id="product" value="${product}" />
    <input type="hidden" name="entityID_text" id="entityID_text" value="" />

    <agiletags:HiddenField elementName="searchCalled"/>
    <!-- <input type="hidden" name="searchCalled" id="searchCalled" value="" /> -->

    <!--<input type="hidden" name="onScreen" id="onScreen" value="" />-->
    <c:if test="${not empty wfPanel.itemID}">
        <input type="hidden" name="itemId" value="${wfPanel.itemID}" size="30" />
    </c:if>

    <div align="Center">
        <table>
            <tr>
                <td colspan="6" style="text-align: center">
                    <label class="subheader">Update</label>
                </td>
            </tr>
            <tr style="border-bottom: #000000 thin">
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr style="border-bottom: #000000 thin"/>
            <c:if test="${not empty wfPanel.refNum}">
                <tr style="text-align: left">
                    <td align="left" width="160px">
                        <label>Reference Number:</label>
                    </td>
                    <td align="left" width="200px">
                        <label>${wfPanel.refNum}</label>
                    </td>
                </tr>
            </c:if>
                
            <c:if test="${catG== 'Membership'}">
                <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Sub Type " elementName="workflowSubType" lookupId="366" javaScript="onChange=\"if(this.value !== '${wfPanel.subTypeValue}'){buttonEnable(this.value, 'btnChange')}else{document.getElementById('btnChange').disabled = true;}\""/></tr>
            </c:if>
            <c:if test="${catG == 'Call Centre Admin' || catG == 'Queries'}">
                <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Sub Type " elementName="workflowSubType" lookupId="367" javaScript="onChange=\"if(this.value !== '${wfPanel.subTypeValue}'){buttonEnable(this.value, 'btnChange')}else{document.getElementById('btnChange').disabled = true;}\""/></tr>
            </c:if>  
            
            <tr style="border-bottom: #000000 thin"/>
            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Entity Type " elementName="entityType" lookupId="108"/></tr>
            <tr id="entityNumber" align="left">
                <agiletags:LabelTextBoxError valueFromSession="yes" valueFromRequest="yes" displayName="Entity Number" elementName="entityNum" javaScript="onChange=\"validateEnitiy(this.value, 'entityNum');\""/>
            </tr>
<!--            <tr id="popCoverSearch" align="left">
                <%--<agiletags:LabelTextSearchText valueFromSession="yes" displayName="Member Number" elementName="memberNumber" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/Workflow/WorkflowUpdate.jsp" mandatory="yes" javaScript="onChange=\"getPrincipalMemberForNumber(this.value, 'memberNumber');\""/>--%>
                <agiletags:LabelTextBoxError valueFromSession="yes" displayName="Member Number" elementName="memberNum" javaScript="onChange=\"getPrincipalMemberForNumber(this.value, 'memberNum');\""/>
            </tr>
            <tr id="popProviderSearch" align="left">
                <%--<agiletags:LabelTextSearchText valueFromSession="yes" displayName="Provider Number" elementName="providerNumber" searchFunction="yes" searchOperation="ForwardToSearchPracticeCommand" onScreen="/Workflow/WorkflowUpdate.jsp" mandatory="yes" javaScript="onChange=\"validateProvider(this.value, 'providerNumber');\""/>--%>
                <agiletags:LabelTextBoxError valueFromSession="yes" displayName="Provider Number" elementName="provNum" javaScript="onChange=\"validateProvider(this.value, 'provNum');\""/>
            </tr>
            <tr id="providerType">
                <agiletags:LabelTextBoxError displayName="Provider Name" elementName="provName"  valueFromSession="yes"/>
            </tr>
            <tr id="providerType">
                <agiletags:LabelTextBoxError displayName="Discipline" elementName="discipline" valueFromSession="yes" readonly="yes"/>
            </tr>-->
            <tr>
                <td align="left" width="100%" colspan="3">
                    <label id="wfNoteDate">Note:</label>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="3">
                    <textarea style="font-family:arial;" id="wfNote" name="wfNote" cols="56" wrap="true" rows="10"></textarea>
                </td>
            </tr>
            <tr style="border-bottom: #000000 thin"/>
        </table>

        <table>
            <tr>
                <td>
                    <input type="button" id="btnChange" value="Update" disabled="true" onClick="document.getElementsByName('actionDesc')[0].value = 'WorkflowUpdate';
                        if(document.getElementById('workflowSubType').value != null){    
                            document.getElementsByName('workflowSubType')[0].value = document.getElementById('workflowSubType').value;
                        }
                        submitWithActionOnScreen('WorkflowContentCommand', 'Workflow/WorkflowMyWorkbench.jsp', '34.2%, 65.8%');"> 
                </td>
            </tr>
        </table>
        <br>
        <br/>
    </div>
</agiletags:ControllerForm>


<%-- 
    Document   : PreAuthConfirmation
    Created on : 2010/06/22, 04:53:12
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ page import="neo.manager.NeoUser" %>
<%@ page import="neo.manager.SecurityResponsibility" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="../resources/styles.css"/>
        <script type="text/javascript" src="../resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="../resources/jQuery/jquery-1.4.2.js"></script>

        <%

            String authStatus = "" + session.getAttribute("authStatus");
            System.out.println("auth type on confirmation = " + authStatus);
            if (authStatus.trim().equalsIgnoreCase("reject")) {

        %>
        <style type="text/css">
            body {
                background-image: url(../resources/rejected_auth.jpg);
                background-position: top center;
                background-repeat: no-repeat;
            }

            @media print {
                body {
                    background-image: url(../resources/rejected_auth.jpg);
                    background-position: top center;
                    background-repeat: no-repeat;
                }
                #AuthButtonRow, #footer, #nav, #sidenav, .print, .search, .breadcrumb, .noprint {display: none;}
            }

        </style>
        <% } else {%>
        <style type="text/css" media="print">
            #AuthButtonRow, #footer, #nav, #sidenav, .print, .search, .breadcrumb, .noprint {display: none;}

        </style>

        <% }%>

        <script type="text/javascript" language="JavaScript">

            $(document).ready(function () {
            <%
                NeoUser user = (NeoUser) session.getAttribute("persist_user");
                for (SecurityResponsibility security : user.getSecurityResponsibility()) {
                    if (security.getResponsibilityId() == 9 || security.getResponsibilityId() == 11 || security.getResponsibilityId() == 4 || security.getResponsibilityId() == 12) {%>
                $("#createAuth").hide();
                $("#updateAuth").hide();
            <%} else {%>
                $("#createAuth").show();
                $("#updateAuth").show();

            <%}
                }%>
                //super user validation
                var at = "<%= session.getAttribute("authType")%>";
                var isSup = "<%= session.getAttribute("persist_user_sup")%>";
                var isTL = "<%= session.getAttribute("persist_user_teamLead")%>";
                if (at == "7" || at == "12") {
                    if (isSup == "true" || isTL == "true") {
                        $("#updateAuth").show();
                    } else {
                        $("#updateAuth").hide();
                    }
                } else if (at == "6") {
                    $("#updateAuth").hide();
                }

            });

            function GetXmlHttpObject() {
                if (window.ActiveXObject) {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function GenerateConfirmation() {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("ERROR: Browser Incompatability");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=ViewPreAuthConfirmation";
                xhr.onreadystatechange = function () {};
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            <%--            function setConfiramtionBackground(){
                            var authStatus = '<%= session.getAttribute("authStatus")%>';
                            if(authStatus == 'Reject'){
                                $('#confimationBody').css({

                                    "background": "url(../resources/rejected_auth.jpg) middle center ",
                                    "background-repeat": "no-repeat"
                                });
                            }
                        }--%>

            function ForwardRequest(command) {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("ERROR: Browser Incompatability");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=" + command;
                xhr.onreadystatechange = function () {};
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function hideRows(id) {
                $("#" + id + "plus").show();
                $("#" + id + "minus").hide();
                var hiddenRows = $("tr[id^=" + id + "hidden]");
                for (i = 0; i < hiddenRows.size(); i++) {
                    $(hiddenRows[i]).hide();
                }

            }
            function showRows(id) {
                $("#" + id + "plus").hide();
                $("#" + id + "minus").show();
                var hiddenRows = $("tr[id^=" + id + "hidden]");
                for (i = 0; i < hiddenRows.size(); i++) {
                    $(hiddenRows[i]).show();
                }
            }

            function printConfirmation() {
                parent.frames["worker"].print();
                //TODO - add backend method and commands
                //       for pdf generation
                //memberNum_text depListValues
            <%--                                var test = $("#confimationBody").html();
                                            var memNo = $("#memberNum_text").text();
                                            var depNo = $("#depListValues").text();
                                            alert(test);

                                xhr = GetXmlHttpObject();
                                if(xhr == null){
                                    alert ("Your browser does not support XMLHTTP!");
                                    return;
                                }
                                var url="/ManagedCare/AgileController?opperation=GeneratePDFForPrint&memNo="+memNo+"&depNo="+depNo+"&html="+test;
                                xhr.onreadystatechange=function(){
                                    if(xhr.readyState == 4 && xhr.statusText == "OK"){
                                        var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                                        if(result == "Done"){
                                            
                                        }else if(result == "Error"){
                                            alert(result);
                                        }
                                    }
                                };
                                xhr.open("POST", url, true);
                                xhr.send(null);--%>
            }

            function submitWithAction(action) {
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById("onScreen").value = onScreen;
                document.getElementById("searchCalled").value = forField;
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body id="confimationBody">
        <div>
            <c:if test="${applicationScope.Client == 'Sechaba'}">
            <img src="../resources/Sechaba logo.png"  alt="Agility_Logo" align="middle"/>
            </c:if>
            <c:if test="${applicationScope.Client == 'Agility'}">
            <img src="../resources/Agility GHS logo.png"  alt="Agility_Logo" align="middle"/>
            </c:if>
            &nbsp;&nbsp;
            <label class="confiramtionHeader">Authorisation</label>
        </div>
        <br/>
        <div>
            <label class="confirmationSubHeaders">Practice Details</label>
        </div>
        <br/>
        <table align="center" width="100%">
            <tr>
                <agiletags:LabelTextDisplay displayName="Provider Number" elementName="treatingProvider_text" boldDisplay="yes" valueFromSession="yes" />
                <td>&nbsp;&nbsp;</td>
            </tr>
            <tr>
                <agiletags:LabelTextDisplay displayName="Provider Name" elementName="treatingProviderName" boldDisplay="yes" valueFromSession="yes" />
                <td>&nbsp;&nbsp;</td>
            </tr>            
        </table>
        <br/>
        <div>
            <label class="confirmationSubHeaders">Member Details</label>
        </div>
        <br/>
        <table align="center" width="100%">
            <tr>
                <agiletags:LabelTextDisplay displayName="Member Number" elementName="memberNum_text" boldDisplay="yes" valueFromSession="yes" />
                <agiletags:LabelTextDisplay displayName="Dependant Number" elementName="depListValues" boldDisplay="yes" valueFromSession="yes" />
            </tr>
            <tr><td colspan="5"></td></tr> 
            <tr><td colspan="5"></td></tr> 
            <tr><td colspan="5"></td></tr> 
            <tr><td colspan="5"></td></tr> 
            <tr>
                <td colspan="5"><label class="noteLabel">Please note that the member's cover period is no longer active - refer to Resigned Date.</label></td>
            </tr> 
            <tr>
                <td colspan="5"><label class="noteLabel">The event is no longer authorised.</label></td>
            </tr> 
            <tr>
                <td colspan="5">
                    <agiletags:MemberDependantHistoryTable commandName="" javaScript="" />
                </td>
            </tr>

        </table>
        <br/>
        <div>
            <label class="confirmationSubHeaders">Authorisation Details</label>
        </div>
        <br/>
        <table>
            <agiletags:AuthConfirmationByType />
        </table>
        <br/>
        <hr size="3px" bgcolor="#0000CC"/>

        <agiletags:ControllerForm name="saveAuth" validate="yes" >
            <table>
                <input type="hidden" name="opperation" id="opperation" value="" />
                <agiletags:HiddenField elementName="searchCalled"/>
                <input type="hidden" name="onScreen" id="onScreen" value="" />
                <tr id="AuthButtonRow">
                    <td align="right" colspan="6">
                        <button id="createAuth" name="opperation" type="button" onClick="submitWithAction('ResetAuthDetails');" value="" >Create New Auth</button>
                        <button id="updateAuth" name="opperation" type="button" onClick="submitWithAction('PreAuthUpdateCommand');" value="" > Update Auth</button>
                        <button name="opperation" type="button" onClick="submitWithAction('ReturnToAuthSearchCommand');" value="" > Search Auth</button>
                        <button name="opperation" type="button" onClick="printConfirmation();" value="" > Print Confirmation</button>
                        <c:if test="${!empty sessionScope.returnFlag && (sessionScope.returnFlag eq true || sessionScope.returnFlag eq 'true')}">
                            <button name="opperation" type="button" onclick="submitWithAction('ViewSubWorkBenchCommand')" value="ViewSubWorkBenchCommand">Return</button>
                        </c:if>
                    </td>
                </tr>
            </table>
        </agiletags:ControllerForm>
        <br/><br/><br/><br/><br/><br/>
    </body>
</html>

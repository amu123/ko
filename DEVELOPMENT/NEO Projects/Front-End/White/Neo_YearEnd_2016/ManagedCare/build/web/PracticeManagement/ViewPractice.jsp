<%-- 
    Document   : viewPractice
    Created on : 04 Mar 2014, 9:50:57 AM
    Author     : almaries
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script language="JavaScript">
            
            var errors = true;
           
            $(function() {
                var searchVal = document.getElementById('searchCalled').value;
                if (searchVal == 'provNum'){
                    validatePractice(document.getElementById('provNum_text').value, 'provNum');
                } 
        
            });
           
            function GetXmlHttpObject(){
                if (window.ActiveXObject){
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest){
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }
            
            //provider search
            function validatePractice(str, element){
                document.getElementById('provButton').disabled = true;
                $("#"+element+"_error").text("");
                var error = "#"+element+"_error";
                if (str.length > 0) {
                    var url="/ManagedCare/AgileController";
                    var data = {'opperation':'FindPracticeDetails','provNum':str,'element':element,'id':new Date().getTime()};

                    //alert("url = "+url);
                    $.get(url, data, function(resultTxt){
                        if (resultTxt!=null) {
                            //alert("resultTxt = "+resultTxt);
                            var resTxtArr = resultTxt.split("|");
                            var result = resTxtArr[0];
                            if(result == "Error"){
                                $(document).find(error).text(resTxtArr[1]);
                                errors = true;
                                    
                            }else if(result == "Done"){
                                document.getElementById('provButton').disabled = false;                                
                                errors = false;
                                
                            }
                        }
                    });

                }else {
                    if(element == "treatingProvider"){
                        $(document).find(error).text("Please Enter a Practice Number");
                    }                        
                }
            }            

            function valPrac(action){
                
                var pracNo = $("#provNum_text").val();
                //validate
                validatePractice(pracNo, 'provNum');
                if(errors == false){
                    var onScreen = document.getElementById('onScreen').value = "/PracticeManagement/PracticeDetails.jsp";
                    var field = document.getElementById('searchCalled').value = "provNum";
                    submitWithAction(action, field, onScreen);
                }                
                
            }

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <label class="header">View Practice</label>
                    <br>
                    <table>
                        <agiletags:ControllerForm name="ViewProviderClaimsForm" action="/ManagedCare/AgileController">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="tab" id="tab" value="" />

                            <tr><agiletags:LabelTextSearchText displayName="Practice Number" elementName="provNum" searchFunction="yes" searchOperation="ForwardToSearchPracticeCommand" onScreen="/PracticeManagement/ViewPractice.jsp" valueFromSession="yes" mandatory="yes" javaScript="onChange=\"validatePractice(this.value, 'provNum');\""/></tr>
                            <tr>
                                <td colspan="2" align="right">
                                    <button id="provButton" name="provButton" type="button" onClick="valPrac('LoadPracticeMenuTabs');" value="" >View</button>
                                </td>
                            </tr>
                        </agiletags:ControllerForm>
                    </table>


                </td></tr></table>
    </body>
</html>


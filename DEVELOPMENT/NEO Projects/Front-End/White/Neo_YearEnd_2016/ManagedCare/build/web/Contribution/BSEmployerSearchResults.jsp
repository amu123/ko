<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Group Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty EmployerSearchResults}">
            <span>No Groups found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Group Number</th>
                        <th>Group Name</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${EmployerSearchResults}">
                        <tr>
                            <td><label>${entry.employerNumber}</label></td>
                            <td><label>${entry.employerName}</label></td>
                            <td>
                                <c:if test="${entityForm == 'BankStatementForm'}">
                                    <input type="button" value="Select" onclick="updateLineFromSearch(${entry.entityId}, '${bsStatementId}', '${bsLineId}','E', '${entry.employerNumber} - ${agiletags:escapeStr(entry.employerName)}');swapDivVisbible('${target_div}','${main_div}');"/>
                                </c:if>
                                <c:if test="${entityForm == 'SplitGroupForm'}">
                                    <input type="button" value="Select" onclick="addSplitGroup(${entry.entityId}, '${entry.employerNumber}', '${agiletags:escapeStr(entry.employerName)}');swapDivVisbible('${target_div}','${main_div}');"/>
                                </c:if>
                            </td> 
                        </tr>
                    </c:forEach>

                </table>
            </c:otherwise>
        </c:choose>

   
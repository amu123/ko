<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<agiletags:ControllerForm name="ConditionSpecificForm">
    <input type="hidden" name="opperation" id="opperation" value="MemberUnderwritingCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="ConditionSpecific" />
    <input type="hidden" name="buttonPressed" id="conditionSpecificButtonPressed" value="" />
    <input type="hidden" name="memberEntityId" id="conditionSpecificEntityId" value="${memberCoverEntityId}" />
    <!--<input type="hidden" name="disorderId_text" id="conditionSpecificDisorderId" value="" /> -->

    <br/>
    <br/>
    <label class="header">Add Condition Specific</label>
    <hr>
    <br/>
    <br/>
    <table>                                
        <tr id="conditionSpecificDependentReasonRow">
            <td align="left" width="160px"><label>Dependant</label></td><td align="left" width="200px">
                <select style="width:215px" name="conDeptList" id="conDeptList">
                    <c:forEach var="entry" items="${MemberCoverDependantDetails}">
                        <c:if test="${entry.status == 'Active'}">
                            <option value="${entry.dependentNumber}" selected>${entry.dependentNumber} ${entry.name} ${agiletags:formatXMLGregorianDate(entry.dateOfBirth)}</option>
                        </c:if>

                    </c:forEach>                      
                </select>
            </td>
        </tr>        
        <tr><agiletags:LabelTextSearchTextDiv displayName="ICD" elementName="Disorder_diagCode" mandatory="yes" onClick="document.getElementById('conditionSpecificButtonPressed').value = 'SearchICD'; getPageContents(document.forms['ConditionSpecificForm'],null,'overlay','overlay2');"/>        </tr>
      <tr><agiletags:LabelTextDisplay displayName="Description" elementName="Disorder_diagName" valueFromSession="no" javaScript="" /></tr>
        <!--<tr><agiletags:LabelTextDisplay displayName="Id" elementName="disorderId_text" valueFromSession="no" javaScript="" /></tr> -->
        <tr id="conditionSpecificId" style="display: none" ><agiletags:LabelTextBoxError displayName="Id" elementName="disorderId_text" valueFromSession="no"/></tr>
        <tr id="ConditionSpecificStartDateRow"><td align="left" width="160px"><label>Period</label></td><td align="left" width="200px">
                <select style="width:215px" name="conditionSpecificDeptList" id="conditionSpecificDeptList">
                    <option value="1" selected>1 Month</option>
                    <option value="2" selected>2 Months</option>
                    <option value="3" selected>3 Months</option>
                    <option value="4" selected>4 Months</option>
                    <option value="5" selected>5 Months</option>
                    <option value="6" selected>6 Months</option>
                    <option value="7" selected>7 Months</option>
                    <option value="8" selected>8 Months</option>
                    <option value="9" selected>9 Months</option>
                    <option value="10" selected>10 Months</option>
                    <option value="11" selected>11 Months</option>
                    <option value="12" selected>12 Months</option>                                                
                </select>
            </td></tr>  
        <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="startDate" valueFromSession="yes" mandatory="no"/></tr>
        <!--<tr><agiletags:LabelTextBoxDateReq  displayname="End Date" elementName="endDate" valueFromSession="yes" mandatory="no"/></tr> -->
    </table>

    <br/>
    <table id="ConditionSpecificTable">
        <tr>
            <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
            <td><input type="button" value="Save" onclick="document.getElementById('conditionSpecificButtonPressed').value='SaveConSpecific';submitFormWithAjaxPost(this.form, 'MemberUnderwriting', null,'${main_div}', '${target_div}');"></td>
        </tr>
    </table>
</agiletags:ControllerForm>



  <agiletags:ControllerForm name="EmpProductOptionForm">
    <input type="hidden" name="opperation" id="opperation" value="" />
    <input type="hidden" name="onScreen" id="onScreen" value="" />

    <label class="header">EmpProductOption</label>

    <fieldset>
      <table id="EmpProductOptionTable">
        <tr id="empProductOptionIdRow"><agiletags:LabelNeoLookupValueDropDownError displayName="EmpProductOption"  elementName="empProductOptionId" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="productOptionIdRow"><agiletags:LabelNeoLookupValueDropDownError displayName="ProductOption"  elementName="productOptionId" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="productIdRow"><agiletags:LabelNeoLookupValueDropDownError displayName="Product"  elementName="productId" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="optionIdRow"><agiletags:LabelNeoLookupValueDropDownError displayName="Option"  elementName="optionId" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="effectiveStartDateRow"><agiletags:LabelTextBoxDateTime  displayName="EffectiveStartDate" elementName="EffectiveStartDate" valueFromSession="no" mandatory="no"/>        </tr>
        <tr id="effectiveEndDateRow"><agiletags:LabelTextBoxDateTime  displayName="EffectiveEndDate" elementName="EffectiveEndDate" valueFromSession="no" mandatory="no"/>        </tr>
      </table>
    </fieldset>
  </agiletags:ControllerForm>

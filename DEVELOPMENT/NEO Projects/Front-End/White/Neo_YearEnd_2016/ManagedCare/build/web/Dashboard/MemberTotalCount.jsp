<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ page import="com.koh.dashboard.tags.MemberTotalCountTable" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Total Member Count</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/AgileTabs.js"></script>
    <!--[if lte IE 8]><script src="/ManagedCare/resources/charts/excanvas.min.js"></script><![endif]-->
        <script type="text/javascript" src="/ManagedCare/resources/charts/jquery.flot.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/charts/jquery.flot.pie.min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/charts/jquery.flot.tooltip.min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/charts/agilecharts.js"></script>
    </head>    
    <body>
        
        <nt:NeoPanel title="Total Member Count"  collapsed="false" reload="false">
             <table id="memCountTbl" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" >
                <agiletags:MemberTotalCountTable/>
             </table>
             <br><br>
             <div id="container"  style="width: 550px; height: 300px;"></div>
        </nt:NeoPanel>
             <br>
        <nt:NeoPanel title="Member Status Count"  collapsed="false" reload="false">
            <table id="memStatusTbl" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" >
                <agiletags:MemberActSusTable/>
            </table>
            <br><br>
            <center><h3>Active Members</h3></center>
            <div id="containerAct" style="width: 550px; height: 300px; "></div>
            <br><br>
            <center><h3>Suspended Members</h3></center>
            <div id="containerSus"  style="width: 550px; height: 300px; "></div>
        </nt:NeoPanel>

        
    <script>
        AgileTable2Pie('memCountTbl', 'container',0,1, true);
        AgileTable2Pie('memStatusTbl', 'containerAct',0,2, true);
        AgileTable2Pie('memStatusTbl', 'containerSus',0,3, true);
    </script>

    </body>
</html>


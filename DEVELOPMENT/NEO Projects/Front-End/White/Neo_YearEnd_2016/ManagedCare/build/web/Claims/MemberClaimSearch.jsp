<%-- 
    Document   : MemberClaimSearch
    Created on : Sep 19, 2011, 9:39:05 PM
    Author     : Johan-NB
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script> 
        <script type="text/javascript">
            
            $(function() {
                //For Practice validation
                var searchVal = document.getElementById('searchCalled').value;
                if (searchVal == 'practiceNumber'){
                    validatePractice(document.getElementById('practiceNumber_text').value, 'practiceNumber');
                }
                document.getElementById('searchCalled').value = '';
                
                //set cover dependants
                var covNum = "<%= session.getAttribute("policyHolderNumber")%>";
                if(covNum != null && covNum != ""){
                    getCoverByNumber(covNum, 'depListValues');
                }
                
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });            

            });  
            
            function validation(){
                var errorCount = 0;
                
                //Tariff Code
                if(isNaN($('#tariffCode').val())){
                    $("#tariffCode_error").text("Incorrect Number");
                    errorCount++;
                } else {
                    $("#tariffCode_error").text("");
                }
                
                //TOOTH NUMBER
                if(isNaN($('#toothNum').val())){
                    $("#toothNum_error").text("Incorrect Number");
                    errorCount++;
                } else {
                    $("#toothNum_error").text("");
                }
                
                if(errorCount > 0){
                    //document.getElementById('btnDisplay').style.visibility = 'hidden';
                    return false;
                }
                else {
                    return true;
                }
            }
            
            function GetXmlHttpObject(){
                if (window.ActiveXObject){
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest){
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }
            
            function GetDOMParser(xmlStr){
                var xmlDoc;
                if(window.DOMParser){
                    parser=new DOMParser();
                    xmlDoc= parser.parseFromString(xmlStr,"text/xml");
                }else{
                    xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async="false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }
            
            //cover search
            function getCoverByNumber(str, element){
                if(str != null && str != ""){
                    xhr = GetXmlHttpObject();
                    if(xhr==null){
                        alert("ERROR: Browser Incompatability");
                        return;
                    }
                    var url = "/ManagedCare/AgileController";
                    url+="?opperation=GetCoverDetailsByNumber&number="+ str + "&element="+element + "&exactCoverNum=1";
                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            $("#depListValues").empty();
                            $("#depListValues").append("<option value=\"99\"></option>");
                            var xmlDoc = GetDOMParser(xhr.responseText);
                            //print value from servlet
                            $(xmlDoc).find("EAuthCoverDetails").each(function (){
                                //set product details
                                var prodName = $(this).find("ProductName").text();
                                var optName = $(this).find("OptionName").text();

                                //extract dependent info from tag
                                $(this).find("DependantInfo").each(function(){
                                    var depInfo = $(this).text();
                                    var nameSurnameType = depInfo.split("|");
                                    //Add option to dependant dropdown
                                    var optVal = document.createElement('OPTION');
                                    optVal.value = nameSurnameType[0];
                                    optVal.text = nameSurnameType[1];
                                    document.getElementById("depListValues").options.add(optVal);
                                });
                                //DISPLAY MEMBER DETAIL LIST
                                //$(document).find("#schemeName").text(prodName);
                                //$(document).find("#schemeOptionName").text(optName);
                                //error check
                                $("#"+element+"_error").text("");
                                var errorText = $(this).text();
                                var errors = errorText.split("|");
                                if(errors[0] == "ERROR"){
                                    $("#"+element+"_error").text(errors[1]);
                                }

                            });
                        }
                    }
                    xhr.open("POST",url,true);
                    xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                    xhr.send(null);
                }
            }

            //practice search
            function validatePractice(str, element){
                xhr = GetXmlHttpObject();
                if (xhr==null){
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                var error = "#"+element+"_error";
                if (str.length > 0) {
                    var url="/ManagedCare/AgileController";
                    url=url+"?opperation=FindPracticeDetailsByCode&provNum="+str+"&element="+element;
                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                            $(document).find(error).text("");
                            if(result == "Error"){
                                $(document).find(error).text("No such provider");
                            }else if(result == "Done"){
                                $(document).find(error).text("");
                            }
                        }
                    };
                    xhr.open("POST",url,true);
                    xhr.send(null);
                } else {
                    $(document).find(error).text("");
                }
            }

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction1(action, claimId, providerNo) {
                document.getElementById('opperation').value = action;
                document.getElementById('memClaimId').value = claimId;
                document.getElementById('memClaimProvNum').value = providerNo;
                document.forms[0].submit();
            }
            
            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitPageAction(action, pageDirection) {
                document.getElementById('opperation').value = action;
                document.getElementById('mcPageDirection').value = pageDirection;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">

                    <agiletags:CallCenterTabs cctSelectedIndex="link_CD" />
                    <fieldset id="pageBorder">
                        <div class="content_area" >

                            <label class="header">Claim Search</label>

                            <br/><br/>
                            <table>
                                <agiletags:ControllerForm name="memberDetails">
                                    <input type="hidden" name="opperation" id="opperation" value="" />
                                    <input type="hidden" name="mcPageDirection" id="mcPageDirection" value="" />
                                    <agiletags:HiddenField elementName="searchCalled" />
                                    <input type="hidden" name="onScreen" id="onScreen" value="" />
                                    <input type="hidden" name="memClaimId" id="memClaimId" value=""/>
                                    <input type="hidden" name="memClaimProvNum" id="memClaimProvNum" value=""/>

                                    <tr><agiletags:EntityCoverDependantDropDown displayName="Member Dependants" elementName="depListValues" javaScript="" mandatory="no" valueFromSession="no" /></tr>
                                    <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Practice Number" elementName="practiceNumber" searchFunction="yes" searchOperation="ForwardToSearchPracticeCommand" onScreen="/Claims/MemberClaimSearch.jsp" mandatory="no" javaScript="onChange=\"validatePractice(this.value, 'practiceNumber');\""/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Claim Id" elementName="mcsClaimId" javaScript="onblur=\"validation();\""/></tr>
                                    <tr><agiletags:LabelTextBoxDate displayname="Treatment Date From" elementName="treatmentFrom"/></tr>
                                    <tr><agiletags:LabelTextBoxDate displayname="Treatment Date To" elementName="treatmentTo"/></tr>
                                    <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Discipline Type" elementName="disciplineType" lookupId="59"/></tr>
                                    <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Claim Status" elementName="claimStatus" lookupId="83"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Tariff Code" elementName="tariffCode" javaScript="onblur=\"validation();\""/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Tooth Number" elementName="toothNum" javaScript="onblur=\"validation();\""/></tr>
                                    <c:if test="${userRestriction != true}">
                                        <tr id="btnDisplay" style="visibility: visible"><agiletags:ButtonOpperation align="left" span="1" type="button" commandName="GetMemberClaimDetailsCommand" displayname="Search Claims" javaScript="onClick=\"if(validation()){submitWithAction('GetMemberClaimDetailsCommand');}\""/></tr>
                                    </c:if>
                                </agiletags:ControllerForm>
                            </table>
                            <br/>
                            <HR color="#666666" WIDTH="50%" align="left">
                            <br/>
                            <!--<agiletags:ViewMemberClaimSearchResult sessionAttribute="memCCClaimSearchResult" />-->
                              <agiletags:ClaimHeaderTag />
                        </div>
                      <c:if test="${userRestriction == true}"><span class="neonotificationnormal neonotificationwarning">Content contained is confidential. Please refer to Team leader for assistance</span></c:if>
                    </fieldset>
                </td></tr></table>
    </body>
</html>

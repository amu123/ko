<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="at" tagdir="/WEB-INF/tags" %>
    <br>
    <at:CommunicationsLog items="${communicationsLog}"/>
    <c:if test="${applicationScope.Client == 'Sechaba'}">
        <at:NeoPanel title="CRM Communication" command="WorkflowCommand" action="memberCRMByEntity" actionId="${entityId}" reload="true" />
    </c:if>

  
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="BrokerFirmAuditForm">
    <input type="hidden" name="opperation" id="BrokerAudit_opperation" value="FirmAuditCommand" />
    <input type="hidden" name="onScreen" id="BrokerAudit_onScreen" value="BrokerAudit" />
    <input type="hidden" name="brokerFirmEntityId" id="brokerFirmAuditEntityId" value="${requestScope.brokerFirmEntityId}"
    <input type="hidden" name="brokerFirmTile" id="brokerFirmAuditTile" value="${requestScope.brokerFirmTile}" />
    <input type="hidden" name="firmRegion" id="firmRegionAut" value="${requestScope.firmRegion}" />
    <agiletags:HiddenField elementName="EmployerAudit_employerEntityId"/>
    <label class="header">Broker Firm Audit Trail</label>
    <hr>

      <table id="EmployerAuditTable">
          <tr>
              <td width="160" style="font-size: 1.0em;"><label class="label">Details : </label></td>
              <td>
                  <select style="width:215px" name="auditList" id="auditList" onChange="submitFormWithAjaxPost(this.form, 'FirmAuditDetails');">
                      <option value="99" selected>&nbsp;</option>
                      <option value="BROKER_FIRM_ACCREDITATION_DETAILS">Accreditation Details</option>
                      <option value="ADDRESS_DETAILS">Address Details</option>
                      <option value="BANK_DETAILS">Bank Details</option>
                      <option value="COMMISSION_IND">Commission Indicator</option>
                      <option value="CONTACT_DETAILS">Contact Details</option>
                      <option value="BROKER_FIRM">Broker Firm Details</option>    
                  </select>
              </td>
                  
          </tr>
      </table>          
 </agiletags:ControllerForm>
 <div style ="display: none"  id="FirmAuditDetails"></div>
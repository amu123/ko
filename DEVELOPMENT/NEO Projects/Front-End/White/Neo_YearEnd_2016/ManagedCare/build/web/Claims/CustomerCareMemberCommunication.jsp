<%-- 
    Document   : MemberDocument
    Created on : 28 Nov 2014
    Author     : marcelp
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<%
    String errorMsg = (String) request.getAttribute("errorMsg");
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Workflow/Workflow.js"></script>
        <script type='text/javascript'>
            $(function(){
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                }); 
            });
            
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <div>
            <% if (errorMsg != null) {%>
            <label id="error" class="error"><%=errorMsg%></label><br>
            <% }%>
            <agiletags:CallCenterTabs cctSelectedIndex="link_Comm" />
            <agiletags:ControllerForm name="MemberCommunicationLog">
                <input type="hidden" name="opperation" id="opperation" value="MemberCommunicationLogCommand" />
                <input type="hidden" name="onScreen" id="onScreen" value="" />
            
                <nt:NeoPanel title="Other Communications"  collapsed="false" reload="false">
                    <nt:CommunicationsLog items="${communicationsLog}" showHeader="false"/>
                </nt:NeoPanel>
                <c:if test="${applicationScope.Client == 'Sechaba'}">
                    <nt:NeoPanel title="CRM Communication" command="WorkflowCommand" action="memberCRMByCov" actionId="${coverNum}" reload="true" />
                </c:if>
                    
            </agiletags:ControllerForm> 
        </div>
    </body>
</html>


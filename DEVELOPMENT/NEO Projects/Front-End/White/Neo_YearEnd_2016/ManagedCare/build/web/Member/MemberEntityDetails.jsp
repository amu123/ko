<%-- 
    Document   : MemberEntityDetails
    Created on : 2012/05/14, 10:19:34
    Author     : johanl
--%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="tabTableLayout">
    <agiletags:ControllerForm name="MemberEntityDetailsForm">
        <input type="hidden" name="opperation" id="MemberPersonal_opperation" value="SaveMemberPersonalDetailsCommand" />
        <input type="hidden" name="onScreen" id="MemberPersonal_onScreen" value="MemberEntityDetails" />
        <input type="hidden" name="refreshScreen" id="refreshScreen" value="" />
        <input type="hidden" name="coverNum" id="coverNum" value="" />
        <input type="hidden" name="depTypeId" id="depTypeIdE" value="${depTypeId}" />
        <input type="hidden" name="entityId" id="entityIdE" value="${entityId}" />
        <input type="hidden" name="depNumber" id="depNumberE" value="${depNumber}" />
        <c:if test="${applicationScope.Client == 'Sechaba'}">
            <label class="subheader">Alternative Membership Numbers</label>
            <agiletags:AlternativeMemberNumbers items="${alt_MemNum_Details}"/>
        </c:if>
        <HR WIDTH="80%" align="left">

        <label class="subheader">Member Personal Details </label>
        <HR WIDTH="80%" align="left">
        <table id="PersonalDetailsTable">
            <c:if test="${!empty optionId && optionId eq '100'}">
                <tr id="PersonDetails_network_IPARow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Network IPA" lookupId="351" elementName="PersonDetails_network_IPA" mandatory="no" errorValueFromSession="yes" firstIndexSelected="no"/></tr>
            </c:if>
            <tr id="PersonDetails_titleRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Title"  elementName="PersonDetails_title" lookupId="24" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <tr id="PersonDetails_initialsRow"><agiletags:LabelTextBoxErrorReq displayName="Initials" elementName="PersonDetails_initials" valueFromRequest="PersonDetails_initials" mandatory="no"/>        </tr>
            <tr id="PersonDetails_firstNameRow"><agiletags:LabelTextBoxErrorReq displayName="First Name" elementName="PersonDetails_firstName" valueFromRequest="PersonDetails_firstName" mandatory="no"/>        </tr>
            <tr id="PersonDetails_secondNameRow"><agiletags:LabelTextBoxErrorReq displayName="Preferred Name" elementName="PersonDetails_secondName" valueFromRequest="PersonDetails_secondName" mandatory="no"/>        </tr>
            <tr id="PersonDetails_lastNameRow"><agiletags:LabelTextBoxErrorReq displayName="Last Name" elementName="PersonDetails_lastName" valueFromRequest="PersonDetails_lastName" mandatory="no"/>        </tr>
            <tr id="PersonDetails_genderRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Gender"  elementName="PersonDetails_gender" lookupId="23" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <tr id="PersonDetails_maritalStatusRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Marital Status"  elementName="PersonDetails_maritalStatus" lookupId="88" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <tr id="PersonDetails_dobRow"><agiletags:LabelTextBoxDateReq  displayname="Date Of Birth" elementName="PersonDetails_dob" valueFromSession="yes" mandatory="no"/>        </tr>
            <tr id="PersonDetails_idTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Identification Type"  elementName="PersonDetails_idType" lookupId="16" mandatory="yes" errorValueFromSession="yes"/>        </tr>
            <tr id="PersonDetails_idNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Identification Number" elementName="PersonDetails_idNumber" valueFromRequest="PersonDetails_idNumber" mandatory="no"/>        </tr>
            <tr id="PersonDetails_homeLanguageRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Language"  elementName="PersonDetails_homeLanguage" lookupId="31" mandatory="no" errorValueFromSession="yes"/>        </tr>
        </table>

        <table id="MemberPersonalSaveTable">
            <tr>
                <td><input type="reset" value="Reset"></td>
                <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'MemberEntityDetails')"></td>
            </tr>
        </table>
    </agiletags:ControllerForm>
    <br>
    <agiletags:ControllerForm name="MemberAdditionalForm">
        <input type="hidden" name="opperation" id="MemberPersonal_opperation" value="SaveMemberAdditionalInfoCommand" />
        <input type="hidden" name="onScreen" id="MemberPersonal_onScreen" value="MemberEntityDetails" />
        <input type="hidden" name="MAI_ID" value="${MAI_ID}" />
        <input type="hidden" name="MAI_DODate" value="${MAI_DODate}" />
        <input type="hidden" name="buttonPressed" id="memberValuesButtonPressed" value="" />
        <input type="hidden" name="depTypeId" id="depTypeIdDetails" value="${depTypeId}" />

        <c:if test="${memberCoverDepType == '17'}">
            <label class="subheader">Additional Details</label>
            <HR WIDTH="80%" align="left">
            <table id="AdditionalDetailsTable">
                <tr><agiletags:LabelTextBoxErrorReq displayName="Employee/Persal Number" elementName="MAI_EmpNo" valueFromRequest="MAI_EmpNo" mandatory="no"/>        </tr>
             <!--   <tr><agiletags:LabelTextBoxErrorReq displayName="Income Amount" elementName="MAI_Income" valueFromRequest="MAI_Income" mandatory="no"/>        </tr> -->                                
                <tr><agiletags:LabelTextBoxErrorReq displayName="Tax Number" elementName="MAI_TaxNo" valueFromRequest="MAI_TaxNo" mandatory="no"/>        </tr>
                <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Risk Category"  elementName="MAI_RiskCat" lookupId="189" mandatory="no" errorValueFromSession="yes"/>        </tr> 
            <!--    <tr><agiletags:LabelTextBoxErrorReq displayName="Member Contrib Portion" elementName="MAI_Subsidy" valueFromRequest="MAI_Subsidy" mandatory="no"/>        </tr> -->
                <tr><agiletags:LabelTextSearchWithNoText displayName="Member Contrib Portion" elementName="MAI_Subsidy" mandatory="no" onClick="document.getElementById('memberValuesButtonPressed').value = 'changeSubsidyButton'; getPageContents(document.forms['MemberAdditionalForm'],null,'main_div','overlay');"/>        </tr>
                <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Member Category"  elementName="MAI_Category" lookupId="223" mandatory="yes" errorValueFromSession="yes"/>
                    <!--    <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Payment Method"  elementName="MAI_PayMethod" lookupId="188" mandatory="no" errorValueFromSession="yes"/>   -->
                <tr><agiletags:LabelTextSearchWithNoText displayName="Payment Method" elementName="MAI_PayMethod" mandatory="no" onClick="document.getElementById('memberValuesButtonPressed').value = 'changePaymentButton'; getPageContents(document.forms['MemberAdditionalForm'],null,'main_div','overlay');"/>        </tr>
                <tr><div id="debit_date">
                    <c:if test="${MAI_PayMethod != 'Electronic Transfer'}" >
                        <agiletags:LabelTextSearchWithNoText displayName="Debit Date" elementName="MAI_DebitDate" mandatory="no" onClick="document.getElementById('memberValuesButtonPressed').value = 'changeDebitDateButton'; getPageContents(document.forms['MemberAdditionalForm'],null,'main_div','overlay');"/>    
                    </c:if>                    
                </div></tr> 
                <c:if test="${sessionScope.hasDebt == '1'}" >
                    <tr>
                        <td class="label" align="left" width="160px">Debt Collection:</td>
                        <td align="left" width="200px">
                            <select style="width:215px" id="MAI_DebtCollectionSelect" name="MAI_DebtCollectionSelect" onchange="showHandedOver('debtCollectionSelect');">
                                <option value="0">No</option>
                                <option selected value="1">Yes</option>
                            </select>
                        </td>
                    </tr>
                </c:if>
                <c:if test="${sessionScope.hasDebt != '1'}" >
                    <tr>
                        <td class="label" align="left" width="160px">Debt Collection:</td>
                        <td align="left" width="200px">
                            <select style="width:215px" id="MAI_DebtCollectionSelect" name="MAI_DebtCollectionSelect" onchange="showHandedOver('debtCollectionSelect');">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </td>
                    </tr>
                </c:if>

                <c:if test="${sessionScope.hasDebt == '1'}" >
                    <tr style="display: table-row" id="handedOverSelectTR">
                        <td class="label" align="left" width="160px">Handed Over:</td>
                        <td align="left" width="200px">
                            <select style="width:215px" id="MAI_HandedOverSelect" name="MAI_HandedOverSelect" onchange="showHandedOver('handedOverSelect');">
                                <c:if test="${sessionScope.debtHandedOver == '1'}" >
                                    <option value="0">No</option>
                                    <option selected value="1">Yes</option>
                                </c:if>
                                <c:if test="${sessionScope.debtHandedOver != '1'}" >
                                    <option selected value="0">No</option>
                                    <option value="1">Yes</option>
                                </c:if>
                            </select>
                        </td>
                    </tr>
                </c:if>
                <c:if test="${sessionScope.hasDebt != '1'}" >
                    <tr style="display: none" id="handedOverSelectTR">
                        <td class="label" align="left" width="160px">Handed Over:</td>
                        <td align="left" width="200px">
                            <select style="width:215px" id="MAI_HandedOverSelect" name="MAI_HandedOverSelect" onchange="showHandedOver('handedOverSelect');">
                                <c:if test="${sessionScope.debtHandedOver == '1'}" >
                                    <option value="0">No</option>
                                    <option selected value="1">Yes</option>
                                </c:if>
                                <c:if test="${sessionScope.debtHandedOver != '1'}" >
                                    <option selected value="0">No</option>
                                    <option value="1">Yes</option>
                                </c:if>
                            </select>
                        </td>
                    </tr>
                </c:if>

                <c:if test="${sessionScope.debtHandedOver == '1'}" >
                    <tr style="display: table-row" id="handedOverDateSelectTR">
                        <agiletags:LabelTextBoxDateReq  displayname="Date Handed Over:" elementName="MAI_HandedOverDateSelect" valueFromSession="yes" mandatory="yes"/>
                    </tr>
                </c:if>
                <c:if test="${sessionScope.debtHandedOver != '1'}" >
                    <tr style="display: none" id="handedOverDateSelectTR">
                        <agiletags:LabelTextBoxDateReq  displayname="Date Handed Over:" elementName="MAI_HandedOverDateSelect" valueFromSession="yes" mandatory="yes"/>
                    </tr>
                </c:if>
                    <tr><agiletags:LabelTextSearchWithNoText displayName="Income Amount" elementName="MAI_Income" mandatory="no" onClick="document.getElementById('memberValuesButtonPressed').value = 'changeIncomeButton'; getPageContents(document.forms['MemberAdditionalForm'],null,'main_div','overlay');"/></tr>
                    <tr><td width="160px"></td><td width="200px" align=\"left\"><label class="error">* Value may affect contribution amount</label></td></tr>
            </table>
            <table id="AdditionalDetailsSaveTable">
                <tr>
                    <td><input type="reset" value="Reset"></td>                    
                    <td><input type="button" value="Save" onclick="if (showHandedOver('ValidationSave')) {document.getElementById('memberValuesButtonPressed').value = ''; submitFormWithAjaxPost(this.form, 'MemberEntityDetails')}"></td>
                </tr>
            </table>
        </c:if>

        <c:if test="${memberCoverDepType != '17'}">
            <label class="subheader">Additional Details</label>
            <HR WIDTH="80%" align="left">
            <table id="AdditionalDepDetailsTable">
                <tr><agiletags:LabelTextSearchWithNoText displayName="Dependant Type" elementName="MAI_DepType" mandatory="no" onClick="document.getElementById('memberValuesButtonPressed').value = 'changeDepType'; getPageContents(document.forms['MemberAdditionalForm'],null,'main_div','overlay');"/>        </tr>               
                <tr><agiletags:LabelTextSearchWithNoText displayName="Financially Dependant" elementName="MAI_FinanciallyDependant" mandatory="no" onClick="document.getElementById('memberValuesButtonPressed').value = 'changeFinType'; getPageContents(document.forms['MemberAdditionalForm'],null,'main_div','overlay');"/>        </tr>               
                <tr><agiletags:LabelTextSearchWithNoText displayName="Student" elementName="MAI_Student" mandatory="no" onClick="document.getElementById('memberValuesButtonPressed').value = 'changeStudent'; getPageContents(document.forms['MemberAdditionalForm'],null,'main_div','overlay');"/>        </tr>               
                <tr><agiletags:LabelTextSearchWithNoText displayName="Handicap" elementName="MAI_Handicap" mandatory="no" onClick="document.getElementById('memberValuesButtonPressed').value = 'changeHandicap'; getPageContents(document.forms['MemberAdditionalForm'],null,'main_div','overlay');"/>        </tr>
                <tr><agiletags:LabelTextSearchWithNoText displayName="Relationship Type" elementName="MAI_RelType" mandatory="no" onClick="document.getElementById('memberValuesButtonPressed').value = 'changeRelType'; getPageContents(document.forms['MemberAdditionalForm'],null,'main_div','overlay');"/>        </tr>               
            </table>
        </c:if>
    </agiletags:ControllerForm>
            
    <c:if test="${!empty memberCoverDepType && !empty optionId && optionId eq '100'}">
        <agiletags:ControllerForm name="MemberNominatedProviderForm">
            <input type="hidden" name="opperation" id="MemberPersonal_opperation" value="SaveNominatedProviderInfoCommand" />
            <input type="hidden" name="onScreen" id="MemberPersonal_onScreen" value="MemberEntityDetails" />
            <input type="hidden" name="buttonPressed" id="memberButtonPressed" value="" />
            <br>
            <label class="subheader">Nominated Practitioner Details</label>
            <hr>
            <label class="subheader">Doctor Details:</label>
            <br>
            <table>
                <tr><agiletags:LabelTextSearchWithNoText displayName="Doctor Name" elementName="MAI_doctorPracticeName" mandatory="no" onClick="document.getElementById('memberButtonPressed').value = 'doctorSearch'; getPageContents(document.forms['MemberNominatedProviderForm'],null,'main_div','overlay');"/></tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="BHF Practice Number"  elementName="MAI_doctorPracticeNumber" mandatory="no"  valueFromRequest="MAI_doctorPracticeNumber"/></tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="Network"  elementName="MAI_doctorPracticeNetwork" mandatory="no" enabled="false" valueFromRequest="MAI_doctorPracticeNetwork"/></tr>
                <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MAI_doctorPracticeStartDate" valueFromSession="yes" mandatory="no"/></tr>
            </table>
            <hr>
            <label class="subheader">Dentist Details:</label>
            <br>
            <table>
                <tr><agiletags:LabelTextSearchWithNoText displayName="Dentist Name" elementName="MAI_dentistPracticeName" mandatory="no" onClick="document.getElementById('memberButtonPressed').value = 'dentistSearch'; getPageContents(document.forms['MemberNominatedProviderForm'],null,'main_div','overlay');"/></tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="BHF Practice Number"  elementName="MAI_dentistPracticeNumber" mandatory="no"  valueFromRequest="MAI_dentistPracticeNumber"/></tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="Network"  elementName="MAI_dentistPracticeNetwork" mandatory="no" enabled="false" valueFromRequest="MAI_dentistPracticeNetwork"/></tr>
                <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MAI_dentistPracticeStartDate" valueFromSession="yes" mandatory="no"/></tr>
            </table>
            <hr>
            <label class="subheader">Optometrist Details:</label>
            <br>
            <table>
                <tr><agiletags:LabelTextSearchWithNoText displayName="Optometrist Name" elementName="MAI_optometristPracticeName" mandatory="no" onClick="document.getElementById('memberButtonPressed').value = 'optometristSearch'; getPageContents(document.forms['MemberNominatedProviderForm'],null,'main_div','overlay');"/></tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="BHF Practice Number"  elementName="MAI_optometristPracticeNumber" mandatory="no"  valueFromRequest="MAI_optometristPracticeNumber"/></tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="Network"  elementName="MAI_optometristPracticeNetwork" mandatory="no" enabled="false" valueFromRequest="MAI_optometristPracticeNetwork"/></tr>
                <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="MAI_optometristPracticeStartDate" valueFromSession="yes" mandatory="no"/></tr>
            </table>

            <table id="NominatedProviderSaveTable">
                <tr>
                    <td><input type="reset" value="Reset"></td>                    
                    <td><input type="button" value="Save" onclick="{document.getElementById('memberButtonPressed').value = ''; submitFormWithAjaxPost(this.form, 'MemberEntityDetails')}"></td>
                </tr>
            </table>
        </agiletags:ControllerForm>
    </c:if>
</div>        

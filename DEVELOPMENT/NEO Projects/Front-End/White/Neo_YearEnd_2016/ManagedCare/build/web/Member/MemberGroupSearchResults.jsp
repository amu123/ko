<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Group Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty GroupSearchResults}">
            <span>No group found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Group Code</th>
                        <th>Group Name</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${GroupSearchResults}">
                        <tr>
                            <td><label>${entry.employerNumber}</label></td>
                            <td><label>${entry.employerName}</label></td>
                            <td><input type="button" value="Select" onclick="setElemValues({'${diagCodeId}':'${entry.employerNumber}','${diagNameId}':'${agiletags:escapeStr(entry.employerName)}','employerEntityId':'${entry.entityId}'});swapDivVisbible('${target_div}','${main_div}');"/></td> 
                        </tr>
                    </c:forEach>

                </table>
            </c:otherwise>
        </c:choose>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags/contributions" %>
<c:if test="${not empty Receipts}">
    <nt:ContributionUnallocatedReceipts items="${Receipts}" showHeader="false"/>
</c:if>
<c:if test="${empty Receipts}">
    <label>No Unallocated Receipts available</label>
</c:if>
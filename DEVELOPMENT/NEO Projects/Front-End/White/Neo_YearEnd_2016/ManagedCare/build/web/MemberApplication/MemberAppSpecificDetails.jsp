<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <agiletags:ControllerForm name="DisorderValuesForm">
            <input type="hidden" name="opperation" id="opperation" value="MemberApplicationSpecificQuestionsCommand" />
            <input type="hidden" name="onScreen" id="onScreen" value="DisorderValues" />
            <input type="hidden" name="memberAppNumber" value="${memberAppNumber}" />
            <input type="hidden" name="buttonPressed" id="disorderValuesButtonPressed" value="" />
            <input type="hidden" name="memberAppSpecId" id="memberAppGeneralQId" value="${memberAppSpecId}" />

            <label class="header">${empty memberAppSpecId ? "Add" : "Edit"} Specific Health Questions Details</label>
            <br>
            <table>
                <tr><agiletags:LabelNeoMapListDropDownErrorReq displayName="Question"  elementName="Disorder_question" mapList="SpecificQuestions" mandatory="yes" errorValueFromSession="no" firstIndexSelected="yes" />        </tr>
                <tr><agiletags:LabelNeoMapListDropDownErrorReq displayName="Dependant"  elementName="Disorder_dependent" mapList="DependentList" mandatory="yes" errorValueFromSession="no" firstIndexSelected="yes" />        </tr>
                <tr><agiletags:LabelTextBoxDateReq  displayname="Date" elementName="Disorder_date" valueFromSession="yes" mandatory="yes"/>        </tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="Treatment" elementName="Disorder_treatment" valueFromRequest="Disorder_treatment" mandatory="yes"/>        </tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="Consulting Doctor" elementName="Disorder_doctor" valueFromRequest="Disorder_doctor" mandatory="yes"/>        </tr>
                <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Current Condition"  elementName="Disorder_condition" lookupId="199" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="yes" />        </tr>
                <tr><agiletags:LabelTextSearchTextDiv displayName="ICD 10 Code" elementName="Disorder_diagCode" mandatory="yes" onClick="document.getElementById('disorderValuesButtonPressed').value = 'SearchICDButton'; getPageContents(document.forms['DisorderValuesForm'],null,'overlay','overlay2');"/>        </tr>
                <tr><agiletags:LabelTextDisplay displayName="ICD 10 Description" elementName="Disorder_diagName" valueFromSession="no" javaScript="" /></tr>
            <tr>
                <td><input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
                <td align="right"><input type="button" value="Save" onclick="document.getElementById('disorderValuesButtonPressed').value='SaveButton';submitFormWithAjaxPost(this.form, 'SpecificQuestions', null,'${main_div}', '${target_div}');"></td>
            </tr>
            </table>
        </agiletags:ControllerForm>

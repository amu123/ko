<%-- 
    Document   : PDCClaimHeaderTag
    Created on : 25 Sep 2013, 3:57:17 PM
    Author     : sphephelot
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>

<%-- any content can be specified here e.g.: --%>
<script type="text/javascript">
    function toggle_contrib_table_group(btn, id) {
        var tblElem = document.getElementById(id).style.display;
        if (tblElem === "none") {
            document.getElementById(id).style.display = 'table-row-group';
            btn.value = '-';
        } else {
            document.getElementById(id).style.display = 'none';
            btn.value = '+';
        }
    }
</script>
<style type="text/css">
    div.bodyid  
    {   /* bkgrnd color is set in Site.css, overflow makes it scrollable */
        height:500px; width:680px; position: static;  overflow-y:auto; float:left;                             
    }
    div.tableHeadid
    {   /* this div used as a fixed column header above the porfolio table, so we set bkgrnd color here */
        background-color:#7ac0da; height:45px; width:680px; position: static;  float:left;                             
    }    
</style>
<c:if test="${empty sessionScope.meberClaimSearchBeanList && sessionScope.meberClaimSearchBeanList eq ''}">
    <span><label class="red">No results found</label></span>
    </c:if>

<c:if test="${!empty sessionScope.meberClaimSearchBeanList && sessionScope.meberClaimSearchBeanList ne ''}">
    <label class="red">Claim history results</label><br/><br/>    
            <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                <div id="tableHeadid">
                    <tr>
                        <th>&nbsp;&nbsp;</th>   
                        <th align="left">Practice Number</th>
                        <th align="left">Practice Name</th>
                        <th align="left">Service Provider</th>
                        <th align="left">Service Provider Name</th>
                        <th align="left">Discipline Type</th>    
                        <th align="left">Treatment From</th>
                        <th align="left">Treatment To</th>
                        <th align="left">Network</th>
                        <th align="left">Action</th>
                    </tr>
                </div>
                <c:forEach var="batch" items="${sessionScope.meberClaimSearchBeanList}">
                    <div id="bodyid">
                        <tr style="border:3px">
                            <td><input type="Button" value="+" onclick="toggle_contrib_table_group(this, 'sub_${batch.claimID}');"></td>
                            <td><label class="label">${batch.practiceNumber}</label></td>
                            <td><label class="label">${batch.practiceName}</label></td>
                            <td><label class="label">${batch.serviceProvider}</label></td>
                            <td><label class="label">${batch.serviceProviderName}</label></td>
                            <td><label class="label">${batch.disciplineType}</label></td>            
                            <td><label class="label">${batch.treatmentFrom}</label></td>
                            <td><label class="label">${batch.treatmentTo}</label></td>
                            <td><label class="label">${batch.network}</label></td>            
                            <td><button name="opperation" type="button" onClick="submitWithAction('ViewMemberClaimLineDetailsPDC', '${batch.claimID}', '${batch.serviceProviderNo}');" value="">Details</button></td>
                        </tr>
                    </div>

                    <tbody id="sub_${batch.claimID}" style="display:none;">
                        <tr>
                            <td rowspan="2" style="border-right-color: white;">&nbsp;&nbsp;</td>      
                            <td align="left" style="border-right-color: white;border-bottom-color:white;text-decoration:underline;"><label class="noteLabel"><b>Claim Type</b></label></td>
                            <td align="left" style="border-right-color: white;border-bottom-color:white;text-decoration:underline;"><label class="noteLabel"><b>Claim Number</b></label></td>
                            <td align="left" style="border-right-color: white;border-bottom-color:white;text-decoration:underline;"><label class="noteLabel"><b>Account Number</b></label></td>
                            <td align="left" style="border-right-color: white;border-bottom-color:white;text-decoration:underline;"><label class="noteLabel"><b>Total Claimed</b></label></td>
                            <td align="left" style="border-right-color: white;border-bottom-color:white;text-decoration:underline;"><label class="noteLabel"><b>Total Paid</b></label></td>   
                            <td align="left" style="border-right-color: white;border-bottom-color:white;text-decoration:underline;"><label class="noteLabel"><b>Recipient</b></label></td>
                            <td align="left" style="border-right-color: white;border-bottom-color:white;text-decoration:underline;" colspan="2"><label class="noteLabel"><b>Co-Payment</b></label></td>
                            <td rowspan="2" >&nbsp;&nbsp;</td>
                        </tr>
                        <tr>        
                            <td style="border-right-color: white"><label class="label">${batch.claimType}</label></td>
                            <td style="border-right-color: white"><label class="label">${batch.claimNumber}</label></td>
                            <td style="border-right-color: white"><label class="label">${batch.accountNumber}</label></td>         
                            <td style="border-right-color: white"><label class="label">${batch.totalClaimed}</label></td>
                            <td style="border-right-color: white"><label class="label">${batch.totalPaid}</label></td>         
                            <td style="border-right-color: white"><label class="label">${batch.recipient}</label></td>
                            <td style="border-right-color: white;" colspan="2"><label class="label">${batch.co_payment}</label></td>  
                        </tr>
                    </tbody>

                </c:forEach>    
            </table>
        </c:if>    

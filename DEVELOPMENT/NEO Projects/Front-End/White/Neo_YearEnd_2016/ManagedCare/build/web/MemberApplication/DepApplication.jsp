<%--
    Document   : Member Search
    Created on : 2012/03/16, 11:06:14
    Author     : yuganp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>

        <script>
            function setMemberAppValues(id, name, status) {
                document.getElementById("memberNumberRes").value = id;
                document.getElementById("memberNameRes").value = name;
                document.getElementById("memberStatusRes").value = status;
            }
        </script>
    </head>
    <body>

    <label class="header">Add New Dependant Member Search</label>
    <hr>
    <br>
     <agiletags:ControllerForm name="NewDepAppSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="NewDepAppSearchCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="DepApplicationSearch" />
        <input type="hidden" name="command" value="Search" />
        <table>
            <tr><agiletags:LabelTextBoxError displayName="Member Number" elementName="memNo"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="ID Number" elementName="idNo"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Initials" elementName="initials"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="surname"/></tr>
            <tr><agiletags:LabelTextBoxDate displayname="Date of Birth" elementName="dob"/></tr>
            <tr><td colspan="2" align="right"><input type="button" value="Search" onclick="submitFormWithAjaxPost(this.form, 'MemberApplicationSearchResults', this);"></td></tr>
        </table>
      </agiletags:ControllerForm>

     <agiletags:ControllerForm name="MemberApplicationSearchResultsForm">
        <input type="hidden" name="opperation" id="search_opperation" value="NewDepAppSearchCommand" />
        <input type="hidden" name="command" value="Select" />
        <input type="hidden" name="onScreen" id="search_onScreen" value="NewDepAppSearchResults" />
        <input type="hidden" name="memberNumberRes" id="memberNumberRes" value="">
      <div style ="display: none"  id="MemberApplicationSearchResults"></div>
      </agiletags:ControllerForm>

    </body>
</html>

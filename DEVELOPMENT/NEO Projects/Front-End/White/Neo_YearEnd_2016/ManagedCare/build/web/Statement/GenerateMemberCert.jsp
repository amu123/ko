<%-- 
    Document   : GenerateMemberCert
    Created on : 2013/01/23, 04:20:19
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" language="JavaScript">
            
            $(function(){
                
                $("#memberNum_text").blur(function(){
                    getCoverByNumber($(this).val(), 'memberNum');
                });
        
                
                var searchVal = $("#searchCalled").val(); // document.getElementById('searchCalled').value;
                if (searchVal == 'memberNum'){
                    getCoverByNumber($("#memberNum_text").val(), 'memberNum');
                } 
                
                //OVERRIDE DEFAULT SUBMIT BEHAVIOUR
                $("#ViewClaimsForm").submit(function(){
                    validateMember('ViewCoverClaimsCommand');
                    return true;
                });
                
                var error = $("#btnError").val();
                if(error != null && error != "" && error != "null"){
                    if(error == true || error == "true"){
                        document.getElementById('memberButton').disabled = true;
                    }else if (error == false || error == "false"){
                        document.getElementById('memberButton').disabled = false;
                    }
                }else{
                    document.getElementById('memberButton').disabled = true;
                }
                
            });
           
            //member search
            function getCoverByNumber(str, element){
                
                document.getElementById('memberButton').disabled = true;
                $("#memberNum_error").text("");
                
                if(str != null && str != ""){
                    
                    var url =  "/ManagedCare/AgileController";
                    var data = {
                        "opperation":"GetMemberByNumberCommand",
                        "number":str
                    }
                    
                    $.get(url, data, function(response){
                        if (response != null){
                            var resultTxt = response.substr(0, response.length -1);
                            var resultArr = resultTxt.split("|");
                            var result = resultArr[0];
                            
                            if(result == "Error"){
                                $("#"+element+"_error").text("No Cover Found");
                                $("#btnError").val(true);
                        
                            }else{
                                
                                var entityIdArr = resultArr[3].split("=");
                                $("#entityId").val(entityIdArr[1]);
                                
                                $("#btnError").val(false);
                                $("#memberButton").removeAttr('disabled');
                                $("#"+element+"_error").text("");
                                
                            }            
                        }
                    },'TEXT');
                    
                }else{
                    $("#"+element+"_error").text("Please enter a Cover Number");
                }
            }

            
            function validateMember(action, process, location){
                var error = $("#btnError").val();
                if(error == false || error == "false"){
                    submitWithActionGeneration(action, process, location);
                }                
            }

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.getElementById('onScreen').value = "/Statement/GenerateMemberCert.jsp";
                document.forms[0].submit();
                $("#memberButton").attr("disabled", "disabled");
            }

            
            function submitWithActionGeneration(action, process, location){
                
                var entityId = $("#entityId").val();
                
                document.getElementById('opperation').value = action;
                document.getElementById('process').value = process;
                document.getElementById('entityId').value = entityId;
                document.getElementById('fileLocation').value = location;
                
                document.forms[0].submit();
                
            }
            
            
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">

                    <div align="left"><label class="header">Member Certificate</label></div>
                    <br/>
                    <agiletags:ControllerForm name="GenMemCertForm" validate="yes">
                        <table>
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="fileLocation" id="fileLocation" value="" />
                            <input type="hidden" name="process" id="process" value="" />
                            <input type="hidden" name="entityId" id="entityId" value="${entityId}" />
                            <input type="hidden" name="folderList" id="folderList" value="${folderList}" />
                            <input type="hidden" name="fileLocation" id="fileLocation" value="${fileLocation}" />
                            <input type="hidden" name="btnError" id="btnError" value="${btnError}" />

                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Member Number" elementName="memberNum" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/Statement/GenerateMemberCert.jsp" mandatory="yes" /></tr>

                            <tr>
                                <td colspan="2" align="left">
                                    <button id="memberButton" name="memberButton" type="submit" value="" onclick="validateMember('MemberCertificateGenerationCC','search',null);" disabled >Search</button>
                                </td>
                            </tr>                            
                        </table>
                        <br/>
                        <HR color="#666666" WIDTH="50%" align="left"><br/>
                        <div align="left"><label class="header">Search Result</label></div>
                        <table>
                            <tr><agiletags:MemberCertTag /></tr>
                        </table>
                    </agiletags:ControllerForm>

                </td></tr></table>
    </body>
</html>

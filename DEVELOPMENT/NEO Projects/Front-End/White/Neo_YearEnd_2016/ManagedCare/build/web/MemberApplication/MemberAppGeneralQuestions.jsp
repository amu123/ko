
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<agiletags:ControllerForm name="MemberAppSpecificQuestionsForm">
    <input type="hidden" name="opperation" id="opperation" value="MemberApplicationGeneralQuestionsCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="" />
    <input type="hidden" name="memberAppNumber" id="MemberAppGeneralQuestions_application_number" value="${memberAppNumber}" />
    <input type="hidden" name="memberAppGenId" id="memberAppGeneralQId" value="" />
    <input type="hidden" name="buttonPressed" id="MemberAppGeneralQuestions_button_pressed" value="" />
    <input type="hidden" name="memberAppType" id="memberAppGeneralType" value="${requestScope.memberAppType}" />

    <label class="header">General Health Questions</label>
    <hr>
    <br>
    <table id="MemberAppGeneralQuestionsTable">
        <c:forEach var="entry" items="${MemAppGeneralQuestions}">
            <tr id="Member_General_${entry.questionNumber}_Row">
                <td width ="20"><label>${entry.questionValue}.</label></td>
                <td width="60%"><label>${entry.description}</label></td>
                <td>
                    <select name="Member_General_${entry.questionNumber}_${entry.questionValue}" id="Member_General_${entry.questionNumber}">
                        <option value="0" ${empty entry.answerValue || entry.answerValue == "0" ? "selected" : ""}>No</option>
                        <option value="1" ${!empty entry.answerValue && entry.answerValue == "1" ? "selected" : ""}>Yes</option>
                    </select>
                </td>
            </tr>
        </c:forEach>
    </table>
    <hr>
    <c:if test="${persist_user_viewMemApps == false}">
        <table id="MemberAppSpecificSaveTable">
            <tr>
                <td><input type="reset" value="Reset"></td>
                <td><input name="SaveButton" type="button" value="Save" onclick="document.getElementById('MemberAppGeneralQuestions_button_pressed').value = this.value;submitFormWithAjaxPost(this.form, null, this)"></td>
            </tr>
        </table>
    </c:if>
    <br>
    <br>
    <label class="subheader">Details</label>
    <hr>
    <br>
    <c:if test="${persist_user_viewMemApps == false}">
        <input type="button" name="AddSpecificDetailsButton" value="Add Details" onclick="document.getElementById('MemberAppGeneralQuestions_button_pressed').value = this.value; getPageContents(this.form,null,'main_div','overlay');"/>
        <br>
    </c:if>

    <table width="90%" id="MemberAppGeneralQuestionsDetailsTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th>Question</th>
            <th>Dependant</th>
            <th>Details</th>
            <th></th>
            <th></th>
        </tr>
        <c:forEach var="entry" items="${MemberAppGenDetails}">
            <tr>
                <td><label>${MemberAppGenQMap[entry.questionNumber]}</label></td>
                <td><label>${MemberAppGenDepMap[entry.dependentNumber]}</label></td>
                <td><label>${entry.details}</label></td>
                <td><input type="button" value="Edit" name="EditButton" onclick="document.getElementById('memberAppGeneralQId').value=${entry.generalDetailId};document.getElementById('MemberAppGeneralQuestions_button_pressed').value='EditButton';getPageContents(this.form,null,'main_div','overlay');"></td>
                <td><input type="button" value="Remove" name="RemoveButton" onclick="document.getElementById('memberAppGeneralQId').value=${entry.generalDetailId};document.getElementById('MemberAppGeneralQuestions_button_pressed').value='RemoveButton';submitFormWithAjaxPost(this.form, 'GeneralQuestions');"></td>
            </tr>
        </c:forEach>

    </table>
</agiletags:ControllerForm>
</body>
</html>
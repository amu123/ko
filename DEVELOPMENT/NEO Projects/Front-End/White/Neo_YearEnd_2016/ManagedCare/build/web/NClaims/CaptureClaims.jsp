<%-- 
    Document   : CaptureClaims
    Created on : 2014/05/08, 01:09:41
    Author     : yuganp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Claims/Claims.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <title>Capture Claims</title>
    </head>
    <body>
        <c:if test="${applicationScope.Client == 'Sechaba'}">
        <div style="border-style: solid; border-color: #6cc24a">
            </c:if>
    <c:if test="${applicationScope.Client == 'Agility'}">
        <div style="border-style: solid; border-color: #660000">
    </c:if>
        <label><h1>Capture Claims</h1></label>
        <div class="neonotification"></div>
        <div id="Main_Claim_Div">
            <div id="Capture_Claims_Div">
                <jsp:include page="CaptureClaims_ClaimHeader.jsp"></jsp:include>
            </div>
            <div id="Capture_Claim_Lines_Div" style="display:none;">

            </div>
            <div id="Capture_Claim_Ndc_Div" style="display:none;">
                <jsp:include page="CaptureClaims_CaptureNdc.jsp"></jsp:include>
            </div>
            <div id="Capture_Claim_Modifier_Div" style="display:none;">
                <jsp:include page="CaptureClaims_CaptureModifier.jsp"></jsp:include>
            </div>
        </div>
        <div id="Search_Practice_Div" style="display:none;">
            <nt:NeoPanel title="Search Practice"  collapsed="false" reload="false" contentClass="neopanelcontentwhite">
                <agiletags:ControllerForm name="ClaimHeaderForm">
                    <input type="hidden" name="opperation" id="ClaimHeader_opperation" value="ClaimHeaderCommand" />
                    <input type="hidden" name="command" id="ClaimHeader_SearchPracticeCommand" value="SearchPractice" />
                    <table>
                        <tr><nt:NeoTextField displayName="Practice Number" elementName="ClaimHeader_SearchPracticeNumber"/></tr>
                        <tr><nt:NeoTextField displayName="Practice Name" elementName="ClaimHeader_SearchPracticeName"/></tr>
                        <tr><nt:NeoTextField displayName="Practice Type" elementName="ClaimHeader_SearchPracticeType"/></tr>
                        <tr><td><input type="button" value="Cancel" onclick="swapDivVisbible('Search_Practice_Div', 'Main_Claim_Div')"></td>
                        <td><input type="button" value="Search" onclick="submitFormWithAjaxPost(this.form, 'SearchPracticeResults_Div', this)"></td></tr>
                    </table>
                </agiletags:ControllerForm>
            </nt:NeoPanel>
            <div id="SearchPracticeResults_Div"></div>
        </div>
        <div id="Search_Provider_Div" style="display:none;">
            <nt:NeoPanel title="Search Provider"  collapsed="false" reload="false" contentClass="neopanelcontentwhite">
                <agiletags:ControllerForm name="ClaimHeaderForm">
                    <input type="hidden" name="opperation" id="ClaimHeader_opperation" value="ClaimHeaderCommand" />
                    <input type="hidden" name="command" id="ClaimHeader_SearchProviderCommand" value="SearchProvider" />
                    <table>
                        <tr><nt:NeoTextField displayName="Provider Initials" elementName="ClaimHeader_SearchProviderInitials"/></tr>
                        <tr><nt:NeoTextField displayName="Provider Name" elementName="ClaimHeader_SearchProviderName"/></tr>
                        <tr><nt:NeoTextField displayName="Provider Surname" elementName="ClaimHeader_SearchProviderSurname"/></tr>
                        <tr><td><input type="button" value="Cancel" onclick="swapDivVisbible('Search_Provider_Div', 'Main_Claim_Div')"></td>
                        <td><input type="button" value="Search" onclick="submitFormWithAjaxPost(this.form, 'SearchProviderResults_Div', this)"></td></tr>
                    </table>
                </agiletags:ControllerForm>
            </nt:NeoPanel>
            <div id="SearchProviderResults_Div"></div>
        </div>
        <div id="Search_Ref_Provider_Div" style="display:none;">
            <nt:NeoPanel title="Search Referring Provider"  collapsed="false" reload="false" contentClass="neopanelcontentwhite">
                <agiletags:ControllerForm name="ClaimHeaderForm">
                    <input type="hidden" name="opperation" id="ClaimHeader_opperation" value="ClaimHeaderCommand" />
                    <input type="hidden" name="command" id="ClaimHeader_SearchProviderCommand" value="SearchRefProvider" />
                    <table>
                        <tr><nt:NeoTextField displayName="Provider Initials" elementName="ClaimHeader_SearchRefProviderInitials"/></tr>
                        <tr><nt:NeoTextField displayName="Provider Name" elementName="ClaimHeader_SearchRefProviderName"/></tr>
                        <tr><nt:NeoTextField displayName="Provider Surname" elementName="ClaimHeader_SearchRefProviderSurname"/></tr>
                        <tr><td><input type="button" value="Cancel" onclick="swapDivVisbible('Search_Ref_Provider_Div', 'Main_Claim_Div')"></td>
                        <td><input type="button" value="Search" onclick="submitFormWithAjaxPost(this.form, 'SearchRefProviderResults_Div', this)"></td></tr>
                    </table>
                </agiletags:ControllerForm>
            </nt:NeoPanel>
            <div id="SearchRefProviderResults_Div"></div>
        </div>
        <div id="Search_Member_Div" style="display:none;" onload="setProductOptionValues()">
            <nt:NeoPanel title="Search Member"  collapsed="false" reload="false" contentClass="neopanelcontentwhite">
                <agiletags:ControllerForm name="ClaimHeaderForm">
                    <input type="hidden" name="opperation" id="ClaimHeader_opperation" value="ClaimHeaderCommand" />
                    <input type="hidden" name="command" id="ClaimHeader_MemberCommand" value="SearchMember" />
                    <table>
                        <tr><nt:NeoTextField displayName="Member Number:" elementName="ClaimHeader_SearchMemberNumber"/></tr>
                        <tr><nt:NeoTextField displayName="Identification Number:" elementName="ClaimHeader_SearchMemberIDNum"/></tr>
                        <tr><nt:NeoTextField displayName="Name:" elementName="ClaimHeader_SearchMemberName"/></tr>
                        <tr><nt:NeoTextField displayName="Surname:" elementName="ClaimHeader_SearchMemberSurname"/></tr>
                        <tr><nt:NeoTextField displayName="Date Of Birth:" elementName="ClaimHeader_SearchDOB" type="date"/></tr>
                        <!--<tr><nt:NeoTextField displayName="Scheme:" elementName="ClaimHeader_SearchMemberScheme"/></tr>
                        <tr><nt:NeoTextField displayName="Scheme Options:" elementName="ClaimHeader_SearchMemberSchemeOptions"/></tr>
                        <tr><agiletags:LabelProductDropDown displayName="Scheme:" elementName="ClaimHeader_SearchMemberScheme" javaScript="onblur=\"toggleProduct(this.value);\""/></tr>
                        <tr><agiletags:LabelDropDown displayName="Scheme Option:" elementName="ClaimHeader_SearchMemberSchemeOptions" /></tr>-->
                        <tr><td><input type="button" value="Cancel" onclick="swapDivVisbible('Search_Member_Div', 'Main_Claim_Div')"></td>
                        <td><input type="button" value="Search" onclick="submitFormWithAjaxPost(this.form, 'SearchMemberResults_Div', this)"></td></tr>
                    </table>
                </agiletags:ControllerForm>
            </nt:NeoPanel>
            <div id="SearchMemberResults_Div"></div>
        </div>
            </div>
    </body>
</html>

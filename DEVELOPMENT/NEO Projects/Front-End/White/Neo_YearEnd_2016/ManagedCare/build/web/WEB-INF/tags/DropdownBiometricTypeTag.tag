<%-- 
   Document   : DropdownBiometricTypeTag
   Created on : 18 Feb 2015, 2:38:05 PM
   Author     : gerritr
--%>

<%@tag description="Displays the Lookup_Values in a DropdownList" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ attribute name="javascript" rtexprvalue="true" required="true" %>
<%@ attribute name="displayName" rtexprvalue="true" required="true" %>
<%-- The list of normal or fragment attributes can be specified here: --%>
<td class="label" align="left" width="160px">${displayName}:</td>
<td align="left" width="200px">
<!--    <select style="width:215px" id="icdSelect" name="icdSelect" ${javascript}>
    <c:if test="${!empty sessionScope.IcdList}">
        <c:if test="${!empty sessionScope.ddSelection}">
            <option value="0">${sessionScope.ddSelection}</option>
        </c:if>
        <c:if test="${empty sessionScope.ddSelection}">
            <option value="0"></option>
        </c:if>
        <c:forEach items="${sessionScope.IcdList}" var="entry" varStatus="i">
            <option value="${entry.id}">${entry.value}</option>
        </c:forEach>
    </c:if>
    <c:if test="${empty sessionScope.IcdList}">
        <option value="0">No items to display</option>
    </c:if>
</select>-->

    <select style="width:215px" id="icdSelect" name="icdSelect" ${javascript}>
        <c:if test="${!empty sessionScope.IcdList}">
            <c:forEach items="${sessionScope.IcdList}" var="entry">
                <option value="${entry.id}">${entry.value}</option>
            </c:forEach>
        </c:if>
        <c:if test="${empty sessionScope.IcdList}">
            <option value="0">No items to display</option>
        </c:if>
    </select>   
</td>
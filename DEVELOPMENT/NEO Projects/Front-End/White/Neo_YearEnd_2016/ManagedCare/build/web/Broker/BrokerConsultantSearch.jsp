<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>

        <script>
            function viewMemberApp(form, id, target) {
//                document.getElementById("employerId").value = id;
                submitFormWithAjaxPost(form, target);
            }

            function setMemberAppValues(id, name, entityId, region) {
                document.getElementById("consultantCodeRes").value = id;
                document.getElementById("consultantNameRes").value = name;
                document.getElementById("consultantEntityId").value = entityId;
                document.getElementById("consultantRegion").value = region;
            }
        </script>
    </head>
    <body>

    <label class="header">Consultant Search</label>
    <hr>
    <br>
     <agiletags:ControllerForm name="ConsultantSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="ConsultantSearchCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="ConsultantApplicationSearch" />
        <table>
            <tr><agiletags:LabelTextBoxError displayName="Consultant Code" elementName="consultantCode"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Consultant Name" elementName="consultantName"/></tr>
            <tr><td colspan="2" align="right"><input type="button" value="Search" onclick="viewMemberApp(this.form, 0, 'ConsultantResults')"></td></tr>
        </table>
      </agiletags:ControllerForm>

     <agiletags:ControllerForm name="ConsultantSearchResultsForm">
        <input type="hidden" name="opperation" id="search_opperation" value="ConsultantViewCommand" />
        <input type="hidden" name="onScreen" id="search_onScreen" value="ConsultantResults" />
        <input type="hidden" name="consultantCode" id="consultantCodeRes" value="">
        <input type="hidden" name="consultantName" id="consultantNameRes" value="">
        <input type="hidden" name="consultantEntityId" id="consultantEntityId" value="" />
        <input type="hidden" name="consultantRegion" id="consultantRegion" value="" />
      <div style ="display: none"  id="ConsultantResults"></div>
      </agiletags:ControllerForm>

    </body>
</html>



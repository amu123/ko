<%--
    Document   : SearchRequest.jsp
    Created on : 2010/04/14, 04:18:25
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>

            function loadSearch() {
                toggleSearchType();
                toggleMemberSearch();
            }
            function toggleSearchType() {
                var value = document.getElementById('callType').value;
                if (value == '2') {
                    var state = document.getElementById('providerNumber').style.display;
                    document.getElementById('providerNumber').style.display = '';
                    document.getElementById("CallNumber").style.display = '';
                    document.getElementById("coverNumber").style.display = 'none';
                } else {
                    document.getElementById('providerNumber').style.display = 'none';
                    document.getElementById("CallNumber").style.display = '';
                    document.getElementById("coverNumber").style.display = '';
                }
            }
            function toggleMemberSearch() {
                var value1 = document.getElementById('callType').value;
                if (value1 == '1') {
                    var status = document.getElementById('coverNumber').style.display;
                    document.getElementById('providerNumber').style.display = 'none';
                    document.getElementById('CallNumber').style.display = '';
                    document.getElementById('coverNumber').style.display = '';
                } else {
                    document.getElementById('providerNumber').style.display = '';
                    document.getElementById('CallNumber').style.display = '';
                    document.getElementById('coverNumber').style.display = 'none';
                }
            }

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->

                    <label class="header">Search Request</label>
                    <agiletags:ControllerForm name="SearchRequest">
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <input type="hidden" name="onScreen" id="onScreen" value="" />
                <tr><td><label>Call Type</label></td><td><select id="callType" name="callType" onchange="toggleSearchType();">
                            <option value="1" selected="selected">Member</option>
                            <option value="2">Provider</option>
                        </select>
                    </td></tr>


                <tr id="coverNumber" style="display:none">
                    <agiletags:LabelTextBoxError displayName="Cover Number" elementName="coverNumber" />
                </tr>

                <agiletags:LabelTextBoxError displayName="Call Number" elementName="callNumber" valueFromSession="Yes" />


                <tr id="providerNumber" style="display:none">
                    <agiletags:LabelTextBoxError displayName="Provider Number" elementName="providerNumber" />
                </tr>

                <tr>
                    <agiletags:ButtonOpperation commandName="ProviderCallTrackHistoryCommand" align="left" displayname="Search" span="1" type="button" javaScript="onClick=\"submitWithAction('ProviderCallTrackHistoryCommand')\";"/>
                </tr>
            </agiletags:ControllerForm>

            <!-- content ends here -->
        </td></tr></table>
</body>
</html>


<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });       
            });
            function hideRows(id) {
                document.getElementById(id).style.display='none';
                document.getElementById(id+ 'plus').style.display='';
                document.getElementById(id+ 'minus').style.display='none';
            }
            function showRows(id) {
                document.getElementById(id).style.display='';
                document.getElementById(id+ 'minus').style.display='';
                document.getElementById(id+ 'plus').style.display='none';

            }
 
         
        </script>
    </head>
  <agiletags:ControllerForm name="EmployerUnderwritingForm">
    <input type="hidden" name="opperation" id="EmployerUnderwriting_opperation" value="SaveEmployerUnderwritingCommand" />
    <input type="hidden" name="onScreen" id="EmployerUnderwriting_onScreen" value="" />
    <input type="hidden" name="EmployerGroup_entityId" id="EmployerGroup_Underwriting_entityId" value="${EmployerGroup_entityId}" />
    <label class="header">Underwriting Details</label>
    <hr>

      <table id="EmployerUnderwritingTable">
        <tr id="EmployerUnderwriting_waitingPeriodRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Apply Waiting Periods"  elementName="EmployerUnderwriting_waitingPeriod" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerUnderwriting_pmbRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Allow PMB"  elementName="EmployerUnderwriting_pmb" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerUnderwriting_pmaRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Allow PMA"  elementName="EmployerUnderwriting_pma" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerUnderwriting_Concession"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Concession"  elementName="ConcesType" lookupId="263" mandatory="no" errorValueFromSession="yes" firstIndexSelected="yes" javaScript="onChange=\"refreshEmployerText();\""/></tr>  
     
        <tr id="EmployerUnderwriting_concessiontartDateRow"><agiletags:LabelTextBoxDateReq  displayname="Concession Start" elementName="EmployerUnderwriting_startDate" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="EmployerUnderwriting_concessionEndDateRow"><agiletags:LabelTextBoxDateReq  displayname="Concession End" elementName="EmployerUnderwriting_endDate" valueFromSession="yes" mandatory="no"/>        </tr>
      
        <tr id="EmployerUnderwriting_individualRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Individual Underwriting"  elementName="EmployerUnderwriting_individual" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerUnderwriting_branchRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Branch Underwriting"  elementName="EmployerUnderwriting_branch" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Category A 3 Month"  elementName="EmployerUnderwriting_catA3MonthInd" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Category A 12 Month"  elementName="EmployerUnderwriting_catA12MonthInd" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Category B 3 Month"  elementName="EmployerUnderwriting_catB3MonthInd" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Category B 12 Month"  elementName="EmployerUnderwriting_catB12MonthInd" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Category C 3 Month"  elementName="EmployerUnderwriting_catC3MonthInd" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Category C 12 Month"  elementName="EmployerUnderwriting_catC12MonthInd" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
      </table>

    <hr>
    <table id="EmployerGroupSaveTable">
        <tr>
           <td><input type="reset" value="Reset"></td>
           <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'EmployerDetails');"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>

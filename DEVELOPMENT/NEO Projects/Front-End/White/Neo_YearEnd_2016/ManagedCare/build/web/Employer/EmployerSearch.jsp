<%--
    Document   : Employer Search
    Created on : 2012/03/16, 11:06:14
    Author     : yuganp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>

        <script>
            function setEmpValues(id, name, number) {
                document.getElementById("employerIdRes").value = id;
                document.getElementById("employerNameRes").value = name;
                document.getElementById("employerNumberRes").value = number;
            }
            
            function ClearValidation(){
                $("#scheme_error").text("");
            }
            
            function validateScheme(form,command,obj){
                ClearValidation();
                var scheme = $("#scheme").val();

                if(scheme == null || scheme == "" || scheme == "99"){
                    $("#scheme_error").text("Scheme is mandatory");
                }
                else{
                    submitFormWithAjaxPost(form,command,obj);
                }         
            }
        </script>
    </head>
    <body>

    <label class="header">Group Search</label>
    <hr>
    <br>
     <agiletags:ControllerForm name="EmployerSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="EmployerSearchCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="EmployerSearch" />
        <input type="hidden" name="menuResp" id="menuResp" value="${param.menuResp}" />
        <table>
            <tr><agiletags:LabelProductDropDown displayName="Scheme" elementName="scheme" mandatory="yes"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Group Number" elementName="employerNumber"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Group Name" elementName="employerName"/></tr>
            <tr><td colspan="2" align="right"><input type="button" value="Search" onclick="validateScheme(this.form, 'EmployerSearchResults', this)"></td></tr>
        </table>
      </agiletags:ControllerForm>

     <agiletags:ControllerForm name="EmployerSearchResultsForm">
        <input type="hidden" name="opperation" id="search_opperation" value="EmployerViewCommand" />
        <input type="hidden" name="onScreen" id="search_onScreen" value="EmployerSearchResults" />
        <input type="hidden" name="employerId" id="employerIdRes" value="">
        <input type="hidden" name="employerName" id="employerNameRes" value="">
        <input type="hidden" name="employerNumber" id="employerNumberRes" value="">
        <input type="hidden" name="menuResp" id="menuResp" value="${param.menuResp}" />
      <div style ="display: none"  id="EmployerSearchResults"></div>
      </agiletags:ControllerForm>

    </body>
</html>

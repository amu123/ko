<%-- 
    Document   : PracticeClaimsHistoryDetails
    Created on : 2011/08/25, 11:22:49
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>
            
            
            function submitReturnWithAction(action) {
                document.getElementById('opperation').value = action;
                document.getElementById('onScreen').value = "PracticeClaimsHistoryDetails.jsp";                
                document.forms[0].submit();
            }

            function submitWithAction(action, claimLineId, paidAmount) {
                document.getElementById('opperation').value = action;
                document.getElementById('pracDetailedClaimLineId').value = claimLineId;
                document.getElementById('pracDetailedClaimPaidAmount').value = paidAmount;
                document.forms[0].submit();
            }
            
            function submitPageAction(action, pageDirection) {
                document.getElementById('opperation').value = action;
                document.getElementById('pcPageDirection').value = pageDirection;
                document.getElementById('onClaimLineScreen').value = 'PracticeClaimsHistoryDetails.jsp';
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <label class="header">Claim Line Detail</label>
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="claimsHistoryPrac">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="pracDetailedClaimLineId" id="pracDetailedClaimLineId" value="" />
                            <input type="hidden" name="pracDetailedClaimPaidAmount" id="pracDetailedClaimPaidAmount" value="" />
                            <input type="hidden" name="pcPageDirection" id="pcPageDirection" value="" />
                            <input type="hidden" name="onClaimLineScreen" id="onClaimLineScreen" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />

                        </agiletags:ControllerForm>

                    </table>
                    <br/>
                    <HR color="#666666" WIDTH="50%" align="left">
                    <br/>
                    <agiletags:PracticeClaimLineDetailsTable />
                    <br/>
                    <table>
                        <tr><agiletags:ButtonOpperation align="right" span="5" type="button" commandName="ReturnPracticeClaimDetails" displayname="Return" javaScript="onClick=\"submitReturnWithAction('ReturnPracticeClaimDetails');\""/></tr>
                    </table>
                </td></tr></table>
    </body>
</html>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <label class="header">Member Search</label>
     <agiletags:ControllerForm name="MemberSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="ContributionBankStatementCommand" />
        <input type="hidden" name="command" value="MemberSearch" />
        <input type="hidden" name="bsStatementId" value="${bsStatementId}" />
        <input type="hidden" name="bsLineId" value="${bsLineId}" />
        <input type="hidden" name="target_div" value="${target_div}" />
        <input type="hidden" name="main_div" value="${main_div}" />
        <table>
            <tr><agiletags:LabelTextBoxError displayName="Member Number" elementName="memNo"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="ID Number" elementName="idNo"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Initials" elementName="initials"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="surname"/></tr>
            <tr><agiletags:LabelTextBoxDate displayname="Date of Birth" elementName="dob"/></tr>
            <tr>
                <td><input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
                <td align="right"><input type="button" value="Search" onclick="submitFormWithAjaxPost(this.form, 'MemberSearchResults');"></td>
            </tr>
        </table>
      
      <div style ="display: none"  id="MemberSearchResults"></div>
      </agiletags:ControllerForm>

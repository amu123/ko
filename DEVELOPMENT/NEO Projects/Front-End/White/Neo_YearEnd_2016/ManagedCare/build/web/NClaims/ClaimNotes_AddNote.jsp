<%-- 
    Document   : ClaimNotes_AddNote
    Created on : 27 May 2016, 9:52:08 AM
    Author     : janf
--%>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <!--<link rel="stylesheet" href="/ManagedCare/resources/bootstrap/bootstrap.min.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/bootstrap/bootstrap-theme.min.css"/>
        <!--<script type="text/javascript" src="/ManagedCare/resources/jquery-2.2.0.min.js"></script>-->
        <script type="text/javascript" src="/ManagedCare/resources/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" language="JavaScript">

            function CheckSearchCommands() {
                //reload ajax commands for screen population
                var searchVal = document.getElementById('searchCalled').value;

            }

            function GetXmlHttpObject() {
                if (window.ActiveXObject) {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function clearElement(element) {
                part = element + '_text';
                document.getElementById(element).value = '';
                document.getElementById(part).value = '';
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById('opperation').value = action;
                document.getElementById('searchCalled').value = forField;
                document.forms[0].submit();
            }


        </script>
    </head>
    <body>
        <agiletags:ControllerForm name="saveClaimNotesForm">
            <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                        <label class="header">Add Note</label>
                        <br/>
                        <table>
                            <!--<agiletags:ControllerForm name="saveClaimNotesForm">-->
                                <input type="hidden" name="opperation" id="ClaimNotes_opperation" value="ClaimNotesCommand" />
                                <input type="hidden" name="onScreen" id="ClaimHeader_onScreen" value="addNote" />
                                <input type="hidden" name="command" id="ClaimHeader_command" value="SaveNote" />
                                <input type="hidden" name="claimId" id="claimId" value="${claimId}"/>
                                <agiletags:HiddenField elementName="searchCalled" />
                                <!--<tr><agiletags:LabelTextAreaErrorBig valueFromSession="Yes" displayName="Notes" elementName="notes" /></tr> -->
                                <!--<tr><agiletags:ButtonOpperationLabelError elementName="saveNoteButton" commandName="" displayName="Save" type="button" javaScript="onClick=\"saveClaimNote(this.form, null, this);\"" /></tr>-->
                                <!--<tr><input type="button" value="Save" onclick="saveClaimNote(this.form, null, this);"></tr>-->
                                <!--</agiletags:ControllerForm>-->
                                
                                <div >
                                    <textarea class="ckeditor" id="notes" name="notes"></textarea>
                                    <script type="text/javascript">
                                        jQuery(document).ready(function() {
                                        CKEDITOR.replace('notes', 
                                        {
                                            toolbar : 'Basic'
                                        });
                                        
                                    });
                                    </script>
                                </div>

                                <br/>

                                <tr>
                                    <agiletags:ButtonOpperation align="left" commandName="" displayname="Save" span="2" type="button" javaScript="onclick=\"saveClaimNote(this.form, null, this);\""/>
                                    <agiletags:ButtonOpperation align="right" commandName="" displayname="Back" span="2" type="button" javaScript="onClick=\"returnToViewNotes();\"" />
                                </tr>
                        </table>
                        <div id="ClaimsNotes_Summary">

                        </div>  
                    </td></tr></table>
                </agiletags:ControllerForm>
    </body>
</html>

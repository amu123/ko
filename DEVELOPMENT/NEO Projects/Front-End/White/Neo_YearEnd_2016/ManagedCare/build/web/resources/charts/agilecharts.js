function AgileTable2Pie(tbl, container, labelCol, valueCol, hasSummary) {
	var arr = getTableRowsAsArray(tbl);
	var data = [];
	for (i = 0; i < arr.length + (hasSummary ? -1 : 0); i++) {
		data[i] = {
			label: arr[i][labelCol],
			data: arr[i][valueCol]
		};
	}
	var placeholder = $("#" + container);
	$.plot(placeholder, data, {
		series: {
			pie: { 
				show: true
			}
		},
		grid: {
			hoverable: true
		},
		tooltip: true,
		tooltipOpts: {
			content: "%s [%p.0%]",
			defaultTheme: false
		}
	});
}

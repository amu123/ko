  <agiletags:ControllerForm name="EmployerBillingForm">
    <input type="hidden" name="opperation" id="opperation" value="" />
    <input type="hidden" name="onScreen" id="onScreen" value="" />

    <label class="header">EmployerBilling</label>

    <fieldset>
      <table id="EmployerBillingTable">
        <tr id="billingIdRow"><agiletags:LabelNeoLookupValueDropDownError displayName="Billing"  elementName="billingId" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="paymentMethodRow"><agiletags:LabelTextBoxError displayName="PaymentMethod" elementName="paymentMethod" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="paymentDateRow"><agiletags:LabelTextBoxDateTime  displayName="PaymentDate" elementName="PaymentDate" valueFromSession="no" mandatory="no"/>        </tr>
        <tr id="paymentFileFormatRow"><agiletags:LabelTextBoxError displayName="PaymentFileFormat" elementName="paymentFileFormat" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="paymentSortIndRow"><agiletags:LabelNeoLookupValueDropDownError displayName="PaymentSort"  elementName="paymentSortInd" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="debitOrderCalendarIdRow"><agiletags:LabelNeoLookupValueDropDownError displayName="DebitOrderCalendar"  elementName="debitOrderCalendarId" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="coaIdRow"><agiletags:LabelNeoLookupValueDropDownError displayName="Coa"  elementName="coaId" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="paymentAdministratorRow"><agiletags:LabelTextBoxError displayName="PaymentAdministrator" elementName="paymentAdministrator" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="employerEntityIdRow"><agiletags:LabelNeoLookupValueDropDownError displayName="EmployerEntity"  elementName="employerEntityId" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="pensionerMembersIndRow"><agiletags:LabelNeoLookupValueDropDownError displayName="PensionerMembers"  elementName="pensionerMembersInd" lookupId="67" mandatory="no" errorValueFromSession="yes"/>        </tr>
      </table>
    </fieldset>
  </agiletags:ControllerForm>

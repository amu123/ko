<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

  <agiletags:ControllerForm name="SuspendMemberForm">
    <input type="hidden" name="opperation" id="opperation" value="SaveMemberCoverDetailsCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="SuspendMember" />
    <input type="hidden" name="buttonPressed" id="disorderValuesButtonPressed" value="" />
    <input type="hidden" name="memberEntityId" id="suspendEntityId" value="${memberCoverEntityId}" />
    <input type="hidden" name="brokerEntityId" id="brokerEntityId" value="${requestScope.brokerEntityId}" />
    <br/>
    <br/>
    <label class="header">Suspend Member</label>
    <br/>
    <br/>
     <table>                                
        <tr id="suspensionReasonRow"><agiletags:LabelNeoLookupValueDropDownWideErrorReq displayName="Suspension Reason"  elementName="suspensionReason" lookupId="219" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="suspensionStartDateRow"><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="suspensionStartDate" valueFromSession="yes" mandatory="yes"/>        </tr> 
      </table>
      
    <hr>
    <table id="MemberSuspendTable">
        <tr>
           <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
           <td><input type="button" value="Save" onclick="SuspendMember( this,'${main_div}','${target_div}');"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>



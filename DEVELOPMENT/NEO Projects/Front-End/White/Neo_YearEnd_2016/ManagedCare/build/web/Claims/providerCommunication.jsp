<%-- 
    Document   : providerCommunication
    Created on : 08 Jun 2015, 11:29:24 AM
    Author     : dewaldo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Provider Communication</title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript'>
            $(function() {
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });
            });

                        
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <div>
            <div class="content_area"></div>
            <agiletags:CallCenterProviderTabs isClearingUser="${sessionScope.persist_user_ccClearing}" cctSelectedIndex="link_prov_Comm"/>
            <agiletags:ControllerForm name="ProviderCommunicationLog">
                <input type="hidden" name="opperation" id="opperation" value="" />
                <input type="hidden" name="onScreen" id="ProviderCommunication_onScreen" value="ProviderCommunication" />
                <agiletags:HiddenField elementName="searchCalled" />
                <input type="hidden" name="listIndex" id="listIndex" value=""/>
                
                <nt:NeoPanel title="Other Communications"  collapsed="false" reload="false">
                    <nt:CommunicationsLog items="${communicationsLog}" showHeader="false"/>
                </nt:NeoPanel>
                <c:if test="${applicationScope.Client == 'Sechaba'}">
                    <nt:NeoPanel title="CRM Communication" command="WorkflowCommand" action="memberCRMByEntity" actionId="${entityId}" reload="true" />
                </c:if>
            </agiletags:ControllerForm> 
        </div>
    </body>
</html>

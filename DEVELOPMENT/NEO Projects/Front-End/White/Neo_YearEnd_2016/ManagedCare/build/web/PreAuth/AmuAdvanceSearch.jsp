<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript">

            function hideRows(id) {
                document.getElementById(id).style.display = 'none';
                document.getElementById(id + 'plus').style.display = '';
                document.getElementById(id + 'minus').style.display = 'none';
            }
            function showRows(id) {
                document.getElementById(id).style.display = '';
                document.getElementById(id + 'minus').style.display = '';
                document.getElementById(id + 'plus').style.display = 'none';
            }
        </script>
    </head>
    <body >
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Advanced Search Amu</label>
                    <br/><br/>
                    <table>
                        <agiletags:ControllerForm name="searchMember">
                            <tr><agiletags:LabelTextBoxError displayName="Entiy Name" elementName="entityName" mandatory="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Subject" elementName="subject" mandatory="yes"/></tr>
                            <tr><agiletags:ButtonOpperation type="submit" align="left" span="1" commandName="AdvanceSearchAuthAmu" displayname="Search" javaScript=""/></tr>
                        </agiletags:ControllerForm>
                    </table>
                    <br/>
                    <HR color="#666666" WIDTH="50%" align="left">
                    <!--br/>
                    <--agiletags:AmukelaniTag />
                    <br/-->
                    <br/>
                    <b><agiletags:ErrorTag /></b>
                    <!-- content ends here -->
                </td></tr></table>

    </body>
</html>

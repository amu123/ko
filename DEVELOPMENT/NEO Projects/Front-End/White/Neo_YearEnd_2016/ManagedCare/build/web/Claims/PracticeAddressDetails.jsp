<%-- 
    Document   : PracticeAddressDetails
    Created on : 2011/08/23, 07:00:42
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript">
            
            $(function() {
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });        
                                
            });
            
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>

    </head>
    <body>

        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">

                    <agiletags:CallCenterProviderTabs isClearingUser="${sessionScope.persist_user_ccClearing}" cctSelectedIndex="link_prov_AD" />

                    <fieldset id="pageBorder">
                        <div class="content_area" >                    
                            <label class="header">Address Details</label>
                            <br/><br/>
                            <agiletags:ControllerForm name="policyHolder">
                                <input type="hidden" name="opperation" id="opperation" value="" />
                            </agiletags:ControllerForm>
                            <label class="subheader">Physical Address</label>
                            <br/>
                            <table>
                                <tr><agiletags:LabelTextBoxError displayName="Line 1" elementName="practicePhysicalAddressLine1"  valueFromSession="Yes" readonly="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Line 2" elementName="practicePhysicalAddressLine2"  valueFromSession="Yes" readonly="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Line 3" elementName="practicePhysicalAddressLine3"  valueFromSession="Yes" readonly="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Physical City" elementName="practicePhysicalCityAddrees"  valueFromSession="Yes" readonly="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Physical Code" elementName="practicePhysicalCodeAddress"  valueFromSession="Yes" readonly="yes"/></tr>
                            </table>
                            <br/>
                            <br/>   
                            <label class="subheader">Postal Address</label>
                            <br/>
                            <table>
                                <tr><agiletags:LabelTextBoxError displayName="Line 1" elementName="practicePostalAddressLine1"  valueFromSession="Yes" readonly="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Line 2" elementName="practicePostalAddressLine2"  valueFromSession="Yes" readonly="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Line 3" elementName="practicePostalAddressLine3"  valueFromSession="Yes" readonly="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Postal City" elementName="practicePostalCityAddress"  valueFromSession="Yes" readonly="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Postal Code" elementName="practicePostalCodeAddress"  valueFromSession="Yes" readonly="yes"/></tr>
                            </table>
                            <br/>
                            <br/>
                            <label class="subheader">Contact Details</label>
                            <br/>
                            <table>
                                <tr><agiletags:LabelTextBoxError displayName="Home Telephone Number" elementName="practiceHomeTelNumber"  valueFromSession="Yes" readonly="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Fax Number" elementName="practiceFaxNo"  valueFromSession="Yes" readonly="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Work Number" elementName="practiceWorkNo"  valueFromSession="Yes" readonly="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Preferred Method" elementName="practicePreferredMethod"  valueFromSession="Yes" readonly="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Contact Person Tel" elementName="practiceContactPersonTelNo"  valueFromSession="Yes" readonly="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Cell Phone Number" elementName="practiceCellNo"  valueFromSession="Yes" readonly="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="Email" elementName="practiceEmailAddress"  valueFromSession="Yes" readonly="yes"/></tr>
                            </table>

                        </div></fieldset>

                </td></tr></table>
    </body>
</html>

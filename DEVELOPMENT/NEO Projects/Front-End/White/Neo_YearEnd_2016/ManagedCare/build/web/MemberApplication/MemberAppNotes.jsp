<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="MemberAppNotesForm">
    <input type="hidden" name="opperation" id="MemberAppNotes_opperation" value="MemberAppNotesCommand" />
    <input type="hidden" name="onScreen" id="MemberAppNotes_onScreen" value="MemberAppNotes" />
    <input type="hidden" name="memberAppNumber" value="${memberAppNumber}" />
    <input type="hidden" name="noteId" id="MemberAppNotes_note_id" value="" />
    <input type="hidden" name="buttonPressed" id="MemberAppNotes_button_pressed" value="" />
    <input type="hidden" name="menuResp" id="menuResp" value="${param.menuResp}" />
    <label class="header">Member Application Notes</label>
    <hr>

      <table id="MemberAppNotesTable">
          <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Details"  elementName="notesListSelect" lookupId="200" mandatory="no" errorValueFromSession="no" javaScript="onchange=\"document.getElementById('MemberAppNotes_button_pressed').value='SelectButton';submitFormWithAjaxPost(this.form, 'MemberAppNotesDetails');\"" />        </tr>
      </table>          
 <div style ="display: none"  id="MemberAppNotesDetails"></div>
 </agiletags:ControllerForm>
   
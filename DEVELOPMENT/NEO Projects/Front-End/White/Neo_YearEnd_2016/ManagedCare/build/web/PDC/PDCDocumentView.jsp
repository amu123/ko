<%-- 
    Document   : PDCDocumentView
    Created on : 19 Feb 2015, 11:12:55 AM
    Author     : shimanem
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script>

            $(document).ready(function () {
                resizeContent();

                var docType = '<%=session.getAttribute("docTypeSelected")%>';
                //var email = <%=session.getAttribute("email")%>;
                if (docType != null)
                {
                    if (docType == 'BenefitLetter') {
                        document.getElementById('optDocType').selectedIndex = 1;
                    }
                }
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });
            /*
             function getIndexWithAction(index, action)
             {
             document.getElementById('selectIndex').value = document.getElementById('optSelectID').value;
             document.getElementById('opperation').value = action;
             document.forms[0].submit();
             }
             */
            function submitWithAction(action)
            {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            function submitActionAndView(action, fileLocation, status) {
                document.getElementById('opperation').value = action;
                document.getElementById('fileLocation').value = fileLocation;
                document.getElementById('status').value = status;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <agiletags:PDCTabs tabSelectedIndex="link_DCT" />
        <fieldset id="pageBorder">
            <div class="" >
                <!-- content goes here -->
                <br/>
                <br/>
                <label class="header">Member Document</label>
                <hr>

                <agiletags:ControllerForm name="documentSearch">

                    <input type="hidden" name="opperation" id="opperation" />
                    <input type="hidden" name="fileLocation" id="fileLocation" value="" />
                    <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                    <input type="hidden" name="selectIndex" id="selectIndex" value="" />
                    <input type="hidden" name="status" id="status" value="" />
                    <%--
                    <table>
                        <tr>
                            <agiletags:PDCDocDropDownSelectTag id="docDescription" displayName="Benefit Letter Types:" javascript="onChange=\"getIndexWithAction(this.value, 'PDCDocumentSearchCommand');\""/>
                        </tr>
                        <tr>
                            <agiletags:PDCCascadedDropdownDocumentList id="docName" displayName="Document Name" javascript=""/>
                        </tr>
                    </table><br/>
                    --%>
                    <table>
                        <tr>
                            <c:choose>
                                <c:when test="${applicationScope.Client == 'Sechaba'}">
                                    <td class="label" align="left" width="160px">PCC Document Type:</td>
                                </c:when>    
                                <c:otherwise>
                                    <td class="label" align="left" width="160px">PDC Document Type:</td>
                                </c:otherwise>
                            </c:choose>
                            <td align="left" width="200px">
                                <select style="width:200px" id="optDocType" name="optDocType">
                                    <option value="0"></option>
                                    <option value="BenefitLetter">Benefit Letter(s)</option>                                
                                </select> 
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td><label>Email:</label></td>
                            <td><input name="memberEmail" id="pdcUserEmail" size="30" value="${sessionScope.email}"></td>
                            <td>&nbsp;</td><td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td><label>Start Date:</label></td>
                            <td><input name="minDate" id="minDate" size="30" value="${minDate}"></td>
                            <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('minDate', this);"/></td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td><label>End Date:</label></td>
                            <td><input name="maxDate" id="maxDate" size="30" value="${maxDate}"></td>
                            <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('maxDate', this);"/></td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td><input type="button" name="SearchButton" id="searchButton" value="Search" onClick="submitWithAction('PDCDocumentSearchViewCommand')"/></td>
                        </tr>
                    </table>
                    <br/><hr color="#666666" WIDTH="50%" align="left"><br/>
                    <div align="left"><label class="header">Search Result</label></div>
                    <table>
                        <tr><agiletags:PDCDocumentSearchResultTag /></tr>
                    </table>
                </agiletags:ControllerForm>
            </div>
        </fieldset>
    </body>
</html>

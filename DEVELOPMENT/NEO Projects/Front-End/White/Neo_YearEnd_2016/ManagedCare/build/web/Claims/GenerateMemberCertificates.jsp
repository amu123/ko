<%-- 
    Document   : GenerateMemberCertificates
    Created on : 04 May 2015, 4:39:50 PM
    Author     : gerritr
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        
        
        <link rel="stylesheet" href="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/jquery.loadmask.css"/>
        <script type='text/javascript' src="/ManagedCare/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="/ManagedCare/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="/ManagedCare/resources/Membership/MemberMaintenance.js"></script>
        
        <script language="JavaScript">

            $(function() {
                resizeContent();
                $("#content_area").load("/ManagedCare/Member/MemberDocumentGeneration.jsp");
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });
            });

            function submitWithAction(action, fileLoc) {
                document.forms[0].elements["opperation"].value = action;
                document.getElementById('fileLocation').value = fileLoc;
                document.forms[0].submit();
            }
            function submitActionAndFile(thisForm, action, fileLocation) {
                thisForm.opperation.value = action;
                thisForm.fileLocation.value = fileLocation;
                thisForm.submit();

            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td></td><td align="left">
                    <agiletags:CallCenterTabs cctSelectedIndex="link_DGN" />
                    <fieldset id="pageBorder">  
                        <agiletags:ControllerForm name="documentSorting">
                            <input type="hidden" name="opperation" id="opperation" value="CallCenterDocumentListCommand" />
                            <input type="hidden" name="fileLocation" id="fileLocation" value="" />
                            <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                            <input type="hidden" name="memberEntityId" id="memberEntityId" value="${sessionScope.memberCoverEntityId}" />
                        </agiletags:ControllerForm>
                        <div class="content_area" id ="content_area" >
                            <label class="header">Document Generation</label>
                        </div>
<!--                        <div id="content">
                            <div class="tabContent" id="MemberSummary">Loading...</div>
                            <div class="tabContent hide" id="MemberEntityDetails">Loading...</div>
                            <div class="tabContent hide" id="MemberAddressDetails">Loading...</div>
                            <div class="tabContent hide" id="MemberBankingDetails">Loading...</div>
                            <div class="tabContent hide" id="MemberContactDetails">Loading...</div>
                            <div class="tabContent hide" id="MemberCover">Loading...</div>
                            <div class="tabContent hide" id="MemberCoverAudit">Loading...</div>
                            <div class="tabContent hide" id="MemberContributions">Loading...</div>
                            <div class="tabContent hide" id="Notes">Loading...</div>
                            <div class="tabContent hide" id="MemberUnderwriting">Loading...</div>
                            <div class="tabContent hide" id="MemberDocuments">Loading...</div>
                            <div class="tabContent hide" id="DocumentGeneration">Loading...</div>
                            <div class="tabContent hide" id="MemberCommunication">Loading...</div>
                        </div>-->
                    </fieldset>
                </td></tr></table>
    </body>
</html>
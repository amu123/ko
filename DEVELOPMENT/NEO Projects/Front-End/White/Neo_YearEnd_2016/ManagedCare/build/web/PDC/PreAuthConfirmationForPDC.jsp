<%-- 
    Document   : PreAuthConfirmationForPDC
    Created on : 2010/06/30, 04:07:19
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script language="JavaScript">

            function GetXmlHttpObject() {
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject) {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function GenerateConfirmation() {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("ERROR: Browser Incompatability");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=ViewPreAuthConfirmation";
                xhr.onreadystatechange = function () {};
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function ForwardRequest(command) {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("ERROR: Browser Incompatability");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=" + command;
                xhr.onreadystatechange = function () {};
                xhr.open("POST", url, true);
                xhr.send(null);
            }

            function hideRows(id) {
                $("#" + id + "plus").show();
                $("#" + id + "minus").hide();
                var hiddenRows = $("tr[id^=" + id + "hidden]");
                for (i = 0; i < hiddenRows.size(); i++) {
                    $(hiddenRows[i]).hide();
                }

            }
            function showRows(id) {
                $("#" + id + "plus").hide();
                $("#" + id + "minus").show();
                var hiddenRows = $("tr[id^=" + id + "hidden]");
                for (i = 0; i < hiddenRows.size(); i++) {
                    $(hiddenRows[i]).show();
                }
            }

            function submitWithAction(action) {
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById("onScreen").value = onScreen;
                document.getElementById("searchCalled").value = forField;
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>

        <label class="header">Authorisation Result</label>
        <br></br>
        <table>
            <agiletags:AuthConfirmationByType />

        </table>
        <br></br>
        <hr size="3px" bgcolor="#0000CC"/>
        <table>
            <agiletags:ControllerForm name="saveAuth" validate="yes" >
                <input type="hidden" name="opperation" id="opperation" value="" />
                <agiletags:HiddenField elementName="searchCalled"/>
                <input type="hidden" name="onScreen" id="onScreen" value="" />
                <tr>
                    <td align="right" colspan="4">
                        <agiletags:ButtonOpperation align="left" displayname="Back" commandName="BackToAuthorizationCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('BackToAuthorizationCommand')\";"/>
                    </td>
                </tr>
            </agiletags:ControllerForm>
        </table>
    </body>
</html>


<%-- 
    Document   : ActivateUser
    Created on : 2009/11/23, 03:00:55
    Author     : gerritj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
        <!-- content goes here -->
        <label class="header">Activate User</label>
        </br></br>
        <agiletags:ErrorTag/>
        <agiletags:ControllerForm name="activateUser">
        <table>
            <tr><agiletags:LabelLabelHiddenField displayName="Name" elementName="name"/></tr>
            <tr><agiletags:LabelLabelHiddenField displayName="Surname" elementName="surname"/></tr>
            <tr><agiletags:LabelLabelHiddenField displayName="Email" elementName="email"/></tr>
            <tr><agiletags:LabelLabelHiddenField displayName="Username" elementName="username"/></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td><label class="header">Responsibilities</label></td></tr>
            <agiletags:ResponsibilityRadioActivationLabel elementName="resp" numberPerRow="3"/>
            <tr><td>&nbsp;</td></tr>
            <agiletags:HiddenField elementName="workitem_id"/>
            <agiletags:HiddenField elementName="user_id"/>
            <tr><agiletags:ButtonOpperation type="submit" align="left" commandName="ActivateUserCommand" displayname="Activate" span="0"/>
            <agiletags:ButtonOpperation type="submit" align="left" commandName="RejectUserCommand" displayname="Reject" span="0"/></tr>
        </table>
        </agiletags:ControllerForm>
        <!-- content ends here -->
        </td></tr></table>
    </body>
</html>

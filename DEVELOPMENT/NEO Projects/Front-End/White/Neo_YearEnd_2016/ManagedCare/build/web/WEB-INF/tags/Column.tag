<%@tag description="" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@attribute name="label" required="true" type="java.lang.String" %>
<%@attribute name="field" required="true" type="java.lang.String" %>
<%@attribute name="type" required="false" type="java.lang.String" %>

<c:if test="${empty type}">
    <c:set var="type" value="string" />
</c:if>
<c:if test="${empty field}">
    <c:set var="field" value="none" />
</c:if>
<c:if test="${not empty neoSummaryTableLabels}">
    <c:set var="neoSummaryTableLabels" value="${neoSummaryTableLabels}||" scope="request" />
    <c:set var="neoSummaryTableFields" value="${neoSummaryTableFields}||" scope="request" />
    <c:set var="neoSummaryTableType" value="${neoSummaryTableType}||" scope="request" />
</c:if>
    <c:set var="neoSummaryTableLabels" value="${neoSummaryTableLabels}${label}" scope="request" />
    <c:set var="neoSummaryTableFields" value="${neoSummaryTableFields}${field}" scope="request" />
    <c:set var="neoSummaryTableType" value="${neoSummaryTableType}${type}" scope="request" />

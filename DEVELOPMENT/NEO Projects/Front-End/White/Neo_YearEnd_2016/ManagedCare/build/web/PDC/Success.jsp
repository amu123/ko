<%-- 
    Document   : Success
    Created on : 2010/07/07, 11:14:18
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/Accumed/resources/styles.css"/>
        <script type="text/javascript" src="/Accumed/resources/datePicker/DatePicker.js"></script>
        <script>
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>

        <agiletags:ControllerForm name="ConfirmClaims">
            <input type="hidden" name="opperation" id="opperation" value="" />

            <agiletags:SuccesssConfirmation/>

            <table>
                <tr>
                    <td width="750"></td>
                    <td align="right">
                        <agiletags:ButtonOpperation commandName="BackTaskViewCommand" align="left" displayname="Back" span="1" type="button"  javaScript="onClick=\"submitWithAction('BackTaskViewCommand')\";"/>
                    </td>
                </tr>
            </table>
        </agiletags:ControllerForm>

    </body>
</html>

<%-- 
    Document   : PaymentRun_DetailsSummary
    Created on : 13 Jun 2016, 3:57:44 PM
    Author     : janf
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>

<label class="header">Payment Run Summary</label>
<br/><br/>
<table id="main" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
    <thead>
        <tr>
            <th>Name Surname</th>
            <th>Entity ID</th>
            <th>Entity Type</th>
            <th>Amount</th>
            <th>Date</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="item" items="${PaymentRunSummary}">
        <tr>
            <td><label>${item.entityName} ${item.entitySurname}</label></td>
            <td><label>${item.entityId}</label></td>
            <td><label>${item.entityType}</label></td>
            <td><label>${item.amount}</label></td>
            <td><label>${item.paymentRunDate}</label></td>
        </tr>
        </c:forEach>
    </tbody>
</table>
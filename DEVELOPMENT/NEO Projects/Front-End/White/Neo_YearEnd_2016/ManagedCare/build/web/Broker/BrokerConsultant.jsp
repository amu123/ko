<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Membership/MemberMaintenance.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/ckeditor/ckeditor.js"></script>
    </head>
    <body onload="initAgileTabs();">
     <agiletags:ControllerForm name="TabControllerForm">
        <input type="hidden" name="opperation" id="opperation" value="ConsultantAppTabContentsCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="ConsultantApplication" />
        <input type="hidden" name="consultantAppCode" id="consultantAppCode" value="${requestScope.consultantAppCode}" />
        <input type="hidden" name="consultantAppCodeRes" id="consultantAppAACode" value="${requestScope.consultantAppCode}" />
        <input type="hidden" name="consultantEntityId" id="consultantEntityId" value="${requestScope.consultantEntityId}" />
        <input type="hidden" name="consultantRegion" id="consultantAppRegion" value="${requestScope.consultantRegion}" />
        
      </agiletags:ControllerForm>
      <div id="main_div">
      <div class="header_area">
        <ol id="tabs" class="${empty requestScope.consultantEntityId?"hide":""}">
          <li><a href="#ConsultantAppDetails" class="">Consultant Details</a></li>
          <li><a href="#Accreditation"  >Accreditation</a></li>         
          <li><a href="#ContactDetails"  >Contact Details</a></li>
          <li><a href="#ConsultantHistoty"  >History Details</a></li>
          <li><a href="#Notes"  >Notes</a></li>
          <li><a href="#Documents"  >Documents</a></li>
          <li><a href="#CoverDetails"  >Cover Details</a></li>
          <li><a href="#AuditTrail"  >Audit Trail</a></li>
        </ol>
        <table width="100%" bgcolor="#dddddd" style="border: 1px solid #c9c3ba;">
            <tr>
                <td width="375"><label class="label">Consultant Name: </label><label id="consultant_header_memberName" class="label">${requestScope.consultantAppName}</label></td>
                <td><label class="label">Consultant Number: </label><label id="consultant_header_applicationNumber" class="label">${requestScope.consultantAppCode}</label></td>
            <td><label ></label></td>
            </tr>
        </table>
      </div>
      <div class="content_area">
        <div class="tabContent"      id="ConsultantAppDetails">Loading...</div>
        <div class="tabContent hide" id="Accreditation">Loading...</div>
        <div class="tabContent hide" id="ContactDetails">Loading...</div>
        <div class="tabContent hide" id="ConsultantHistoty">Loading...</div>
        <div class="tabContent hide" id="Notes">Loading...</div>
        <div class="tabContent hide" id="AuditTrail">Loading...</div>
        <div class="tabContent hide" id="Documents">Loading...</div>
        <div class="tabContent hide" id="CoverDetails">Loading...</div>
      </div>
        </div>
                    <div id="overlay" style="display:none;"></div>
                    <div id="overlay2" style="display:none;"></div>
        
                        
    </body>
</html>





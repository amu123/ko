<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<agiletags:ControllerForm name="MemberAppNotesForm">
    <input type="hidden" name="opperation" id="BrokerAppNotes_opperation" value="MemberNotesCommand" />
    <input type="hidden" name="onScreen" id="MemberAppNotes_onScreen" value="MemberAppNotes" />
    <input type="hidden" name="noteId" id="MemberAppNotes_note_id" value="" />
    <input type="hidden" name="noteType" id="MemberAppNotes_note_type" value="" />
    <input type="hidden" name="buttonPressed" id="MemberAppNotes_button_pressed" value="" />
    <input type="hidden" name="memberEntityId" id="memberNotesEntityId" value="${sessionScope.memberCoverEntityId}" />
    <label class="header">Member Notes</label>
    <hr>

    <table id="MemberAppNotesTable">
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Details"  elementName="notesListSelect" lookupId="257" mandatory="no" errorValueFromSession="no"  />        </tr>
    </table>
    <label class="subheader">Member Notes Details</label>
    <table>
        <td><label>Start Date</label></td>
        <td><input name="note_start_date" id="note_start_date" size="30" value="${note_start_date}"></td>
        <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('note_start_date', this);"/></td>
        <td>&nbsp;</td>
        <td><label>End Date</label></td>
        <td><input name="note_end_date" id="note_end_date" size="30" value="${note_end_date}"></td>
        <td align="left" width="15"><img src="/ManagedCare/resources/Calendar.gif" width="28" height="28" alt="Calendar" onclick="displayDatePicker('note_end_date', this);"/></td>
        <td>&nbsp;</td>
        <td><input type="button" name="SearchNotesButton" value="Search" onclick="document.getElementById('MemberAppNotes_button_pressed').value = 'SearchNotesButton'; submitFormWithAjaxPost(this.form, 'MemberAppNotesDetails');"/></td>
    </table>
    <div style ="display: none"  id="MemberAppNotesDetails"></div>
</agiletags:ControllerForm>

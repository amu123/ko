<%-- 
    Document   : ManageEvent
    Created on : Mar 13, 2012, 2:20:38 PM
    Author     : Princes
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script>
            $(document).ready(function () {
                //Check what the current session input is and show/hide appropriate fields
                var indexA = document.getElementById('contactPatient').value;
                hideContact(indexA);
                var indexB = document.getElementById('reasonForAssignment').value;
                hideContactDetails(indexB);
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('questionIndex').value = index;
                document.forms[0].submit();
            }

            function submitWithActionForm(action) {
                ClearValidationContents();
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function ClearValidationContents() {
                document.getElementById('subject_error').value = "";
                document.getElementById('userEmail_error').value = "";
            }

            function hideContact(value) {
                if (value == 1) {
                    document.getElementById("contact").style.display = 'block';
                    document.getElementById("phone").style.display = 'block';
                    document.getElementById("cell").style.display = 'block';
                } else {
                    document.getElementById("contact").style.display = 'none';
                    document.getElementById("phone").style.display = 'none';
                    document.getElementById("cell").style.display = 'none';
                    document.getElementById("msg").style.display = 'none';
                    document.getElementById("email").style.display = 'none';
                }
            }

            function hideContactDetails(value) {

                if (value == 1) {
                    document.getElementById("phone").style.display = 'block';
                    document.getElementById("cell").style.display = 'block';
                    document.getElementById("msg").style.display = 'none';
                    document.getElementById("email").style.display = 'none';
                    document.getElementById("PhoneReason").style.display = 'block';
                    document.getElementById("emailTextArea").style.display = 'none';
                } else if (value == 2) {
                    document.getElementById("phone").style.display = 'none';
                    document.getElementById("cell").style.display = 'block';
                    document.getElementById("msg").style.display = 'block';
                    document.getElementById("email").style.display = 'none';
                    document.getElementById("PhoneReason").style.display = 'none';
                    document.getElementById("emailTextArea").style.display = 'none';

                } else if (value == 3) {
                    document.getElementById("phone").style.display = 'none';
                    document.getElementById("cell").style.display = 'none';
                    document.getElementById("msg").style.display = 'none';
                    document.getElementById("email").style.display = 'block';
                    document.getElementById("PhoneReason").style.display = 'none';
                    document.getElementById("emailTextArea").style.display = 'block';
                }
            }

            function loadHistory() {

                var url = "${pageContext.request.contextPath}/AgileController";
                var data = {
                    //replace this with your command name
                    'opperation': 'GetContactHistoryCommand',
                    'id': new Date().getTime()
                };

                $.get(url, data, function (resultTxt) {
                    if (resultTxt !== null) {
                        var resTxtArr = resultTxt.split("|");
                        var result = resTxtArr[0];
                        if (result === "Done") {
                            submitScreenRefresh("/PDC/ManageEvent.jsp");
                            hideContact(0);
                            hideContactDetails(0);
                        } else {
                            //replace this with your text box name but leave the _error there
                            $("#History_error").text("Your call to the command has failed");
                        }
                    } else {
                        //replace this with your text box name but leave the _error there
                        $("#History_error").text("Your call to the command has failed!");
                    }
                });
                data = null;
            }

            function submitScreenRefresh(screen) {
                $("#opperation").val("RefreshCommand");
                $("#refreshScreen").val(screen);
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <agiletags:PDCTabs tabSelectedIndex="link_CO"/>
                    <fieldset id="pageBorder">
                        <div class="" >
                            <agiletags:ControllerForm name="caseManangement">
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <input type="hidden" name="questionIndex" id="questionIndex" value="" />
                                <input type="hidden" name="refreshScreen" id="refreshScreen" value="" />
                                <input type="hidden" name="onScreen" id="onScreen" value="" />
                                <input type="hidden" name="searchCall" id="searchCall" value="" />

                                <br/><br/>
                                <label class="header">Event Manangement - Contact Patient</label>
                                <br/><br/>

                                <table width="224px">
                                    <tr><agiletags:LabelNeoLookupValueDropDown displayName="Do you want to contact the patient?" elementName="contactPatient" lookupId="67" javaScript="onChange=\"hideContact(this.value)\";"/></tr>
                                </table>
                                <table>
                                    <tbody id="contact" style="display:none">
                                        <tr><agiletags:LabelNeoLookupValueDropDown displayName="Contact Methods" elementName="reasonForAssignment" lookupId="121" javaScript="onChange=\"hideContactDetails(this.value)\";"/></tr>
                                    </tbody>
                                    <tbody id="PhoneReason" style="display:none">
                                        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Reason for phone call" elementName="reasonForPhoneCall" lookupId="289" firstIndexSelected="yes"/></tr>
                                    </tbody>
                                    <tbody id="phone" style="display:none">
                                        <tr><agiletags:LabelTextBoxError displayName="Phone Number" elementName="homeNumber"  valueFromSession="Yes" mandatory="yes"/></tr>
                                    </tbody>
                                    <tbody id="cell" style="display:none">
                                        <tr><agiletags:LabelTextBoxError displayName="Cell Phone Number" elementName="cellNumber"  valueFromSession="Yes" mandatory="yes"/></tr>
                                    </tbody>
                                    <tbody id="email" style="display:none">
                                        <tr><agiletags:LabelTextBoxError displayName="Subject" elementName="subject" valueFromSession="yes" mandatory="yes"/></tr>
                                        <tr><agiletags:LabelTextBoxError displayName="Email" elementName="userEmail"  valueFromSession="Yes" mandatory="yes"/></tr>
                                    </tbody>
                                    <tbody id="msg" style="display:none">
                                        <tr><agiletags:LabelTextAreaError displayName="SMS Message" elementName="smsMessage" mandatory="yes" valueFromSession="yes"/></tr>
                                    </tbody>
                                    <tbody id="emailTextArea" style="display:none">
                                        <tr><agiletags:LabelTextAreaErrorBig displayName="Email Message" elementName="emailMessage" mandatory="yes" valueFromSession="yes"/></tr>
                                    </tbody>
                                </table>
                                <table>
                                    <tr>
                                        <td><agiletags:ButtonOpperation align="left" displayname="Save" commandName="SaveCaseManagementCommand" span="3" type="button" javaScript="onClick=\"submitWithActionForm('SaveCaseManagementCommand')\";"/></td>
                                        <td><agiletags:ButtonOpperation align="left" displayname="History" commandName="GetContactHistoryCommand" span="3" type="button" javaScript="onClick=\"loadHistory()\";"/></td>
                                    </tr>
                                </table>
                                <br/><br/>
                                <agiletags:CommunicationHistory/>
                            </agiletags:ControllerForm>
                        </div></fieldset>
        </table>
    </body>
</html>

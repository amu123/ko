<%-- 
    Document   : PracticeClaim
    Created on : Jan 31, 2013, 1:08:03 PM
    Author     : sphephelot
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>


<%-- any content can be specified here e.g.: --%>
<script type="text/javascript">
            function toggle_contrib_table_group(btn, id) {
                var tblElem = document.getElementById(id).style.display;
                if (tblElem == "none") {
                    document.getElementById(id).style.display = 'table-row-group';
                    btn.value = '-';
                } else {
                    document.getElementById(id).style.display = 'none';
                    btn.value = '+';
                }
            }
</script>
<style type="text/css">
    div.body  
    {   /* bkgrnd color is set in Site.css, overflow makes it scrollable */
        height:500px; width:680px; position: static;  overflow-y:auto; float:left;                             
    }
    div.tableHead
    {   /* this div used as a fixed column header above the porfolio table, so we set bkgrnd color here */
        background-color:#7ac0da; height:45px; width:680px; position: static;  float:left;                             
    }    
</style>
<c:set var="list" value="${sessionScope.meberClaimSearchBeanList}"/>
<c:choose>
<c:when test="${empty list}">
    <span><label class="red"s><c:out value="${sessionScope.practiceClaimSearchResultsMessage}"/></label></span>
</c:when>
<c:otherwise>
<label class="red"><c:out value="${sessionScope.practiceClaimSearchResultsMessage}"/><label><br/><br/>    

    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
   <div id="tableHead">
    <tr>
    <th>&nbsp;&nbsp;</th> 
    <th align="left">Member Number</th>
    <th align="left">Member Details</th>
    <th align="left">Service Provider</th>
    <th align="left">Service Provider Name</th>
    <th align="left">Discipline Type</th>    
    <th align="left">Treatment From</th>
    <th align="left">Treatment To</th>
    <th align="left">Total Claimed</th>
    <th align="left">Total Paid</th>
    <th align="left">Recipient</th>
    <th align="left">Action</th>
    </tr>
    </div>
<c:forEach var="batch" items="${sessionScope.meberClaimSearchBeanList}">
    <div id="body">   
    <tr style="border:3px">
            <td><input type="Button" value="+" onclick="toggle_contrib_table_group(this, 'sub_${batch.claimID}');"></td>
            <td><label class="label">${batch.memberNumber}</label></td>
            <td><label class="label">${batch.memberName}</label></td>
            <td><label class="label">${batch.serviceProvider}</label></td>
            <td><label class="label">${batch.serviceProviderName}</label></td>
            <td><label class="label">${batch.disciplineType}</label></td>            
            <td><label class="label">${batch.treatmentFrom}</label></td>
            <td><label class="label">${batch.treatmentTo}</label></td>
            <td><label class="label">${batch.totalClaimed}</label></td>
            <td><label class="label">${batch.totalPaid}</label></td>
            <td><label class="label">${batch.recipient}</label></td>                  
            <td><button name="opperation" type="button" onClick="submitWithAction1('ViewPracticeClaimLineDetails', '${batch.claimID}','${batch.memberNumber}', '${batch.serviceProvider}');" value="">Details</button></td>
        </tr>
       </div>  
    <!--the sub table goes here-->    
    <tbody id="sub_${batch.claimID}" style="display:none;">
        <tr>
            <td rowspan="2" style="border-right-color: white;">&nbsp;&nbsp;</td>      
            <td align="left" style="border-right-color: white;border-bottom-color:white;text-decoration:underline;"><label class="noteLabel"><b>Claim Type</b></label></td>
            <td align="left" style="border-right-color: white;border-bottom-color:white;text-decoration:underline;"><label class="noteLabel"><b>Account Number</b></label></td>
            <td align="left" style="border-right-color: white;border-bottom-color:white;text-decoration:underline;"><label class="noteLabel"><b>Co-Payment</b></label></td>
            <td align="left" style="border-bottom-color:white;text-decoration:underline;" colspan="8" ></td>
        </tr>
        <tr>        
            <td style="border-right-color: white"><label class="label">${batch.claimType}</label></td>
            <td style="border-right-color: white"><label class="label">${batch.accountNumber}</label></td>        
            <td style="border-right-color: white;"><label class="label">${batch.co_payment}</label></td>  
            <td colspan="8"></td>
        </tbody>
        
        
        </c:forEach>    
    </table>
    </c:otherwise>
</c:choose>


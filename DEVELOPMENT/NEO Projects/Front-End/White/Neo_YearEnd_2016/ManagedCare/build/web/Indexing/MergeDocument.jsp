<%-- 
    Document   : IndexDocument
    Created on : 02 Jul 2012, 3:34:17 PM
    Author     : Christo
--%>
<%@page import="com.koh.serv.PropertiesReader"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<% String folderList = (String) request.getAttribute("folderList");
System.out.println("-------------JSP------------------");
    System.out.println("folderList in page = " + folderList);
    if (folderList == null) {
        folderList = new PropertiesReader().getProperty("DocumentIndexScanFolder");
    }
    String listOrWork = (String)request.getAttribute("listOrWork");
    if(listOrWork == null){
        listOrWork = "List";
    }
String errorMsg = (String)request.getAttribute("errorMsg");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script language="JavaScript">
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }            
            function setParams(){
             
                document.getElementById('folderList').value =  '<%=folderList%>';
                document.getElementById('listOrWork').value = '<%=listOrWork%>';
            }
        </script>
    </head>
    <body>

        <table width=100% height=100%><tr valign="left"><td width="50px"></td><td align="left">
                    <label class="header">Document Merge <%=request.getSession().getAttribute("profile")%></label>
                    <br/><br/><br/>
                    <table>
                        <% if(errorMsg != null){ %>
                        <tr><td><label id="error" class="error"><%=errorMsg%></label></td></tr>
                        <% } %>
                        <agiletags:ControllerForm name="documentIndexing">
                        <input type="hidden" name="folderList" id="folderList" value="" />
                        <input type="hidden" name="listOrWork" id="listOrWork" value="" />
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />


                            <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                            <tr><td>
                                    <table>
                                        <tr><agiletags:DocumentIndexingFileList  command="memberNumber" multiple="true" /></tr>
                                    </table> </td><td>
                                    <table>
                                          <tr><td> <button type="submit" name="opperation" onClick="document.getElementById('opperation').value ='DocumentIndexDoMergeCommand'; setParams();">Merge</button> </td></tr>
                                    </table>
                                </td></tr>
                            </agiletags:ControllerForm>
                    </table>
                </td></tr></table>

    </body>
</html>


<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<br>
        <label class="subheader">Broker Commission Payment Summary</label>
        <c:choose>
            <c:when test="${empty CommissionSummary}">
                <span><label>No Payment details available</label></span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Payment Date</th>
                        <th>Broker Code</th>
                        <th>Broker Name</th>
                        <th>Members</th>
                        <th>Commission</th>
                        <th>Vat</th>
                        <th>Paid</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${CommissionSummary}">
                        <tr>
                            <td><label>${entry.chequeRunDate}</label></td>
                            <td><label>${entry.brokerCode}</label></td>
                            <td><label>${entry.brokerName}</label></td>
                            <td><label>${entry.numMembers}</label></td>
                            <td align="right"><label>${agiletags:formatStringAmount(entry.commAmount)}</label></td>
                            <td align="right"><label>${agiletags:formatStringAmount(entry.commVat)}</label></td>
                            <td align="right"><label>${agiletags:formatStringAmount(entry.commPaid)}</label></td>
                            <td><button type="button" onclick="document.getElementById('BrokerFirmCommissionCommand').value = 'viewBrokerDetails';document.getElementById('commBrokerEntityId').value = '${entry.brokerEntityId}';submitFormWithAjaxPost(this.form, 'BrokerPaymentDetails', this);">View Member Details</button></td>
                        </tr>
                    </c:forEach>
                </table>
                <div id="BrokerPaymentDetails"></div>
            </c:otherwise>
        </c:choose>

<%-- 
    Document   : AuthHistoryNotes
    Created on : Oct 4, 2010, 11:22:00 AM
    Author     : Johan-NB
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script language="JavaScript">

            function CheckSearchCommands(){
                //reload ajax commands for screen population
                var searchVal = document.getElementById('searchCalled').value;
                if (searchVal == 'noteMemberNum'){
                    getCoverByNumber(document.getElementById('noteMemberNum_text').value, 'noteMemberNum');
                }
                document.getElementById('searchCalled').value = '';
            }

            function GetXmlHttpObject(){
                if (window.ActiveXObject){
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest){
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function GetDOMParser(xmlStr){
                var xmlDoc;
                if(window.DOMParser){
                    parser=new DOMParser();
                    xmlDoc= parser.parseFromString(xmlStr,"text/xml");
                }else{
                    xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async="false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }
            //member search
            function getCoverByNumber(str, element){
                if(str != null && str != ""){
                    xhr = GetXmlHttpObject();
                    if(xhr==null){
                        alert("ERROR: Browser Incompatability");
                        return;
                    }
                    var url = "/ManagedCare/AgileController";
                    url+="?opperation=GetCoverDetailsByNumber&number="+ str + "&element="+element;
                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            $("#noteDepListValues").empty();
                            $("#noteDepListValues").append("<option value=\"99\"></option>");
                            var xmlDoc = GetDOMParser(xhr.responseText);
                            //print value from servlet
                            $(xmlDoc).find("EAuthCoverDetails").each(function (){
                                //extract dependent info from tag
                                $(this).find("DependantInfo").each(function(){
                                    var depInfo = $(this).text();
                                    var nameSurnameType = depInfo.split("|");
                                    //Add option to dependant dropdown
                                    var optVal = document.createElement('OPTION');
                                    optVal.value = nameSurnameType[0];
                                    optVal.text = nameSurnameType[1];
                                    document.getElementById("noteDepListValues").options.add(optVal);
                                });
                                //error check
                                $("#"+element+"_error").text("");
                                var errorText = $(this).text();
                                var errors = errorText.split("|");
                                if(errors[0] == "ERROR"){
                                    $("#"+element+"_error").text(errors[1]);
                                }

                            });
                        }
                    }
                    xhr.open("POST",url,true);
                    //xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                    xhr.send(null);
                }
            }

            function clearElement(element) {
                part = element + "_text";
                document.getElementById(element).value = "";
                document.getElementById(part).value = "";
            }
            function submitWithAction(action) {
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body onload="CheckSearchCommands();">
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">

                    <div align="left"><label class="header">Auth History Notes</label></div>
                    <br/>

                    <agiletags:ControllerForm name="searchHistoryNotes" validate="" >
                        <table>
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />

                            <tr><agiletags:LabelTextBoxError valueFromSession="yes" displayName="Member Number" elementName="noteMemberNum" javaScript=""/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Cover Dependants" elementName="noteDepListValues" javaScript="" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxDate displayname="Creation Date" elementName="crDate" valueFromSession="yes" /></tr>

                            <tr><agiletags:ButtonOpperation align="right" commandName="SearchAuthHistoryNotes" displayname="Search" span="" type="button" javaScript="onClick=\"submitWithAction('SearchAuthHistoryNotes');\"" /></tr>

                        </table>
                    </agiletags:ControllerForm>

                    <agiletags:HistoryNotesSearchResult />

                    <!-- content ends here -->
                </td></tr></table>
    </body>
</html>


<%-- 
    Document   : PreAuthICD10Search
    Created on : 2010/06/07, 11:34:00
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
    <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js">


        function submitWithAction(action) {
            document.getElementById('opperation').value = action;
            document.forms[0].submit();
        }
        function submitWithAction(action, forField, onScreen) {
            document.getElementById('onScreen').value = onScreen;
            document.getElementById('searchCalled').value = forField;
            document.getElementById('opperation').value = action;
            document.forms[0].submit();
        }

    </script>
</head>
<body>
<table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
    <!-- content goes here -->
    <label class="header">Pre Auth ICD10 Search</label>
    <br/>
    <table>
        <agiletags:ControllerForm name="searchICD">
            <tr><agiletags:LabelTextBoxError displayName="Code" elementName="code"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Description" elementName="description"/></tr>
            <tr><agiletags:ButtonOpperation type="submit" align="left" span="3" commandName="SearchPreAuthICD10" displayname="Search"/></tr>
        </agiletags:ControllerForm>
    </table>
    <br/>
    <HR color="#666666" WIDTH="50%" align="left">
    <%
        String multiInd = " " + session.getAttribute("multiICDind");
        if (multiInd != null && !multiInd.trim().equalsIgnoreCase("")) {
            if(multiInd.trim().equalsIgnoreCase("yes")){
    %>
                <agiletags:ICD10SearchResultTable commandName="AllocatePreAuthMultiICD10ToSession" />
    <%      
            } else if(multiInd.trim().equalsIgnoreCase("no")){
    %>
                <agiletags:ICD10SearchResultTable commandName="AllocatePreAuthICD10ToSession" />
    <%      }
        } else {
    %>
            <agiletags:ICD10SearchResultTable commandName="AllocatePreAuthICD10ToSession" />
    <%  }
    %>
    <!-- content ends here -->
</td></tr></table>
</body>
</html>

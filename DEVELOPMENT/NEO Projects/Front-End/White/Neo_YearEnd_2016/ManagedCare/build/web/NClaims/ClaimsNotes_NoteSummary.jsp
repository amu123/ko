<%-- 
    Document   : ClaimsNotes_NoteSummary
    Created on : 26 May 2016, 2:06:01 PM
    Author     : janf
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>

<nt:NeoPanel title="Notes for Claim ${claimId}"  collapsed="false" reload="false">
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>User</th>
                <th>Note</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="item" items="${ClaimNotes}">
                <c:set var="condition" value="1"></c:set>
                <tr>
                    <td><label>${item.creationDate}</label></td>
                    <td><label>${item.userName}</label></td>
                    <td><label>${item.shortNote}</label></td>
                    <td><input type="button" value="View" onclick="viewClaimNote(${item.noteId})"></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</nt:NeoPanel>    
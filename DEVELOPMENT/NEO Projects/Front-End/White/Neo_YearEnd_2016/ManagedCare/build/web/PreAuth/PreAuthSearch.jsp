<%-- 
    Document   : PreAuthSearch
    Created on : 2010/01/11, 03:41:45
    Author     : whauger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" language="JavaScript">

            $(document).ready(function () {
                //reload ajax commands for screen population
                var searchVal = document.getElementById('searchCalled').value;
                if (searchVal == 'memberNum') {
                    getCoverByNumber(document.getElementById('memberNum_text').value, 'memberNum');
                }
                document.getElementById('searchCalled').value = '';

                var optVal = document.createElement('OPTION');
                optVal.value = "99";
                optVal.text = "";
                $("#schemeOption option").add(optVal);
                toggleProduct($("#scheme").val());
                var resultMsg = <%= session.getAttribute("searchReturnMsg")%>;
                if (resultMsg != null && resultMsg != "") {
                    $("#searchReturnMsg").text(resultMsg);
                } else {
                    $("#searchReturnMsg").text("");
                }

            });


            function GetXmlHttpObject() {
                if (window.ActiveXObject) {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function GetDOMParser(xmlStr) {
                var xmlDoc;
                if (window.DOMParser) {
                    parser = new DOMParser();
                    xmlDoc = parser.parseFromString(xmlStr, "text/xml");
                } else {
                    xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async = "false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }

            function toggleProduct(prodId) {
                if (prodId != "99") {
                    addProductOption(prodId);
                }
            }

            function addProductOption(prodId) {
                xhr = GetXmlHttpObject();
                if (xhr == null) {
                    alert("ERROR: Browser Incompatability");
                    return;
                }
                var url = "/ManagedCare/AgileController?opperation=GetProductOptionCommand&prodId=" + prodId;
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.statusText == "OK") {
                        $("#schemeOption").empty();
                        $("#schemeOption").append("<option value=\"99\"></option>");
                        var xmlDoc = GetDOMParser(xhr.responseText);
                        //print value from servlet
                        $(xmlDoc).find("ProductOptions").each(function () {
                            //set product details
                            $(this).find("Option").each(function () {
                                var prodOpt = $(this).text();
                                var opt = prodOpt.split("|");
                                //Add option to dependant dropdown
                                var optVal = document.createElement('OPTION');
                                optVal.value = opt[0];
                                optVal.text = opt[1];
                                document.getElementById("schemeOption").options.add(optVal);

                            });
                        });
                    }
                }
                xhr.open("POST", url, true);
                xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                xhr.send(null);

            }

            //member search
            function getCoverByNumber(str, element) {
                if (str != null && str != "") {
                    xhr = GetXmlHttpObject();
                    if (xhr == null) {
                        alert("ERROR: Browser Incompatability");
                        return;
                    }
                    var url = "/ManagedCare/AgileController";
                    url += "?opperation=GetCoverDetailsByNumber&number=" + str + "&element=" + element;
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.statusText == "OK") {
                            $("#depListValues").empty();
                            $("#depListValues").append("<option value=\"99\"></option>");
                            var xmlDoc = GetDOMParser(xhr.responseText);
                            //print value from servlet
                            $(xmlDoc).find("EAuthCoverDetails").each(function () {
                                //set product details
                                var prodName = $(this).find("ProductName").text();
                                var optName = $(this).find("OptionName").text();

                                //extract dependent info from tag
                                $(this).find("DependantInfo").each(function () {
                                    var depInfo = $(this).text();
                                    var nameSurnameType = depInfo.split("|");
                                    //Add option to dependant dropdown
                                    var optVal = document.createElement('OPTION');
                                    optVal.value = nameSurnameType[0];
                                    optVal.text = nameSurnameType[1];
                                    document.getElementById("depListValues").options.add(optVal);
                                });
                                //DISPLAY MEMBER DETAIL LIST
                                $(document).find("#schemeName").text(prodName);
                                $(document).find("#schemeOptionName").text(optName);
                                //error check
                                $("#" + element + "_error").text("");
                                var errorText = $(this).text();
                                var errors = errorText.split("|");
                                if (errors[0] == "ERROR") {
                                    $("#" + element + "_error").text(errors[1]);
                                }

                            });
                        }
                    }
                    xhr.open("POST", url, true);
                    xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                    xhr.send(null);
                }
            }

            function submitWithAction(action) {
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }
            function submitWithAction(action, forField, onScreen) {
                document.getElementById("onScreen").value = onScreen;
                document.getElementById("searchCalled").value = forField;
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }

            function hideRows(id) {
                document.getElementById(id).style.display = 'none';
                document.getElementById(id + 'plus').style.display = '';
                document.getElementById(id + 'minus').style.display = 'none';
            }
            function showRows(id) {
                document.getElementById(id).style.display = '';
                document.getElementById(id + 'minus').style.display = '';
                document.getElementById(id + 'plus').style.display = 'none';

            }
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <c:if test="${!empty sessionScope.pdcFlag && (sessionScope.pdcFlag eq true || sessionScope.pdcFlag eq 'true')}">
                        <agiletags:PDCTabs tabSelectedIndex="link_Auth" />
                    </c:if>
                    <label class="header">Authorisation Search</label>
                    <br/><br/>
                    <table>
                        <agiletags:ControllerForm name="searchAuthForm">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="exactCoverNum" id="exactCoverNum" value="1" />

                            <tr><agiletags:LabelTextBoxError displayName="Authorisation Number" elementName="authNo"/></tr>
                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Auth Type" lookupId="89" elementName="authType"/></tr>
                            <tr><agiletags:LabelTextBoxDate displayname="Authorisation Date" elementName="authDate"/></tr>

                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Member Number" elementName="memberNum" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/PreAuth/PreAuthSearch.jsp" mandatory="no" javaScript="onblur=\"getCoverByNumber(this.value, 'memberNum');\""/></tr>
                            <tr><agiletags:EntityCoverDependantDropDown displayName="Cover Dependants" elementName="depListValues" javaScript="" mandatory="no" valueFromSession="yes" /></tr>

                            <tr><agiletags:LabelTextBoxError displayName="First Name" elementName="name" /></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Last Name" elementName="surname" /></tr>

                            <tr><agiletags:LabelProductDropDown displayName="Scheme" elementName="scheme" javaScript="onblur=\"toggleProduct(this.value);\"" /></tr>
                            <tr><agiletags:LabelDropDown displayName="Scheme Option" elementName="schemeOption" /></tr>

                            <tr><agiletags:ButtonOpperation type="button" align="left" span="1" commandName="SearchPreAuthCommand" displayname="Search" javaScript="onClick=\"submitWithAction('SearchPreAuthCommand');\"" /></tr>

                        </agiletags:ControllerForm>
                    </table>
                    <br/>
                    <HR color="#666666" WIDTH="50%" align="left">
                    <br/>
                    <agiletags:PreAuthSearchResultTable commandName="ForwardToPreAuthConfirmation" javascript="onClick=\"submitWithAction('ForwardToPreAuthConfirmation');\""/>

                    
                    <br><br>
                    <!--<c:if test="${!empty sessionScope.returnFlag && (sessionScope.returnFlag eq true || sessionScope.returnFlag eq 'true')}">
                        <button name="opperation" type="button" onclick="submitWithAction('ViewSubWorkBenchCommand')" value="ViewSubWorkBenchCommand">Return</button>
                    </c:if>-->
                    <!-- content ends here -->
                </td></tr></table>
    </body>
</html>

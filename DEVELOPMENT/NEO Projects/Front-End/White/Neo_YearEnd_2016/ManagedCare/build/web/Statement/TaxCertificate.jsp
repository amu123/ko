<%-- 
    Document   : TaxCertificate
    Created on : 2011/06/24, 10:39:51
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>

        <script language="JavaScript">

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function validateMemberNo() {
                var memNo = document.getElementById('memberNo_text').value;
                if (memNo == null || memNo == "") {
                    $("#memberNo_error").text("Member number is required");
                    return false;
                }
                return true;
            }

            function submitWithAction1(action, actionParam) {
                var test = validateMemberNo();
                if (test) {
                    document.getElementById('opperation').value = action;
                    document.getElementById('opperationParam').value = actionParam;
                    document.forms[0].submit();
                }
            }

            function submitWithAction(action, forField, onScreen) {
                document.getElementById('opperation').value = action;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('onScreen').value = onScreen;
                document.forms[0].submit();
            }

            function getPrincipalMemberForNumber(str, element) {
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                if (str.length > 0) {
                    var url = "/ManagedCare/AgileController";
                    url = url + "?opperation=GetMemberByNumberCommand&number=" + str + "&element=" + element;
                    xmlhttp.onreadystatechange = function () {
                        stateChanged(element)
                    };
                    xmlhttp.open("POST", url, true);
                    xmlhttp.send(null);
                }
            }


            function GetXmlHttpObject() {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function stateChanged(element) {
                if (xmlhttp.readyState == 4)
                {
                    //alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    //alert(result);
                    if (result == "GetMemberByNumberCommand") {

                        var resultArr = xmlhttp.responseText.split("|");
                        var numberArr = resultArr[1].split("=");
                        var emailArr = resultArr[2].split("=");

                        var number = numberArr[1];
                        var email = emailArr[1];
                        document.getElementById(element + '_error').innerText = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('emailAddress').value = email;
                        document.getElementById('searchCalled').value = '';
                    } else if (result == "Error") {
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|') + 1, xmlhttp.responseText.length);
                        if (forCommand == "GetMemberByNumberCommand") {
                            document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            //document.getElementById(element + '_text').value = number;
                            // document.getElementById('emailAddress').value = email;
                            //document.getElementById('searchCalled').value = '';
                        }
                    } else if (result == "Access") {
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|') + 1, xmlhttp.responseText.length);
                        if (forCommand == "GetMemberByNumberCommand") {
                            document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            //document.getElementById(element + '_text').value = number;
                            // document.getElementById('emailAddress').value = email;
                            //document.getElementById('searchCalled').value = '';
                        }
                    }
                }
            }

            function onChange(val) {
                if (val < 2014) {
                    document.getElementById('detailsBtn').disabled = true;
                } else {
                    document.getElementById('detailsBtn').disabled = false;
                }
            }

            $(function () {
                var memberNo = document.getElementById('memberNo_text').value;
                if (memberNo != null || memberNo != '') {
                    getPrincipalMemberForNumber(memberNo, 'memberNo');
                }
            });

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="left"><td width="50px"></td><td align="left">
                    <label class="header">Tax Certificate</label>
                    <br/><br/><br/>
                    <table>
                        <agiletags:ControllerForm name="sendTaxCertificate">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                            <input type="hidden" name="exactCoverNum" id="exactCoverNum" value="1" />
                            <table>
                                <tr>
                                    <agiletags:LabelProductDropDown displayName="Scheme" elementName="schemeOption" valueFromSession="Yes" javaScript="onChange=\"toggleStmtType(1);\""/>
                                </tr>
                                <tr>
                                    <agiletags:YearRangeSelector displayName="Year Range" elementName="yearRange" javaScript="onChange=\"onChange(this.value);\"" mandatory="yes" valueFromSession="yes" />
                                </tr>
                                <tr>
                                    <agiletags:LabelTextSearchText displayName="Member number" elementName="memberNo" mandatory="yes" valueFromSession="Yes" searchFunction="Yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/Statement/TaxCertificate.jsp" javaScript="onblur=\"getPrincipalMemberForNumber(this.value, 'memberNo');\""/>
                                </tr>
                                <tr>
                                    <agiletags:LabelTextBoxError displayName="E-Mail address" elementName="emailAddress"/>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Cancel" javaScript="onClick=\"submitWithAction('ReloadTaxCertificateCommand')\";"/>
                                    <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Preview" javaScript="onClick=\"submitWithAction1('SubmitTaxCertificateCommand', 'Preview')\";"/>
                                    <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Submit" javaScript="onClick=\"submitWithAction1('SubmitTaxCertificateCommand', 'Send')\";"/>
                                    <%-- <agiletags:ButtonOpperation type="button" align="left" span="1" commandName="" displayname="Details" javaScript="onClick=\"submitWithAction1('ViewTaxCertificateDetailsCommand', 'Details')\";"/> --%>
                                    <td colspan="1"><input type="button" id="detailsBtn" onclick="submitWithAction1('ViewTaxCertificateDetailsCommand', 'Details');" value="Details" /></td>
                                </tr>
                            </table>
                        </agiletags:ControllerForm>
                    </table>
                </td></tr></table>
    </body>
</html>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

  <agiletags:ControllerForm name="BrokerAndFirmForm">
    <input type="hidden" name="opperation" id="opperation" value="AddBrokerHistoryCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberDependantAppAdd" />
     <input type="hidden" name="buttonPressed" id="disorderValuesButtonPressed" value="" />
    <input type="hidden" name="brokerEntityId" id="brokerFirmAddAccreEntityId" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="brokerFirmEntityId" id="brokerFirmEntityId" value="${requestScope.brokerFirmEntityId}" />
    <input type="hidden" name="brokerTile" id="brokerFirmAddAccreTile" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="brokerContactPerson" id="brokerFirmContactAccrePerson" value="${requestScope.brokerEntityId}" />
    <br/>
    <br/>
    <label class="header">Link Broker To Firm</label>
    <br>
     <table>                        
        <tr><agiletags:LabelTextSearchTextDiv displayName="Firm Code" elementName="Disorder_diagCode" mandatory="yes" onClick="document.getElementById('disorderValuesButtonPressed').value = 'SearchFirmButton'; getPageContents(document.forms['BrokerAndFirmForm'],null,'overlay','overlay2');"/>        </tr>
        <tr><agiletags:LabelTextDisplay displayName="Firm Name" elementName="Disorder_diagName" valueFromSession="no" javaScript="" /></tr>
        <tr id="FirmApp_brokerStartDateRow"><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="BrokerApp_historyStartDate" valueFromSession="yes" mandatory="yes"/>        </tr> 
        <tr id="FirmApp_brokerEndDateRow"><agiletags:LabelTextBoxDateReq  displayname="End Date" elementName="Broker_historyEndDate" valueFromSession="yes" mandatory="yes"/>        </tr> 
      </table>
      
    <hr>
    <table id="MemberAppDepSaveTable">
        <tr>
           <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
           <td><input type="button" value="Save" onclick="document.getElementById('disorderValuesButtonPressed').value='SaveButton';submitFormWithAjaxPost(this.form, 'BrokerHistory', null,'${main_div}', '${target_div}');"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>

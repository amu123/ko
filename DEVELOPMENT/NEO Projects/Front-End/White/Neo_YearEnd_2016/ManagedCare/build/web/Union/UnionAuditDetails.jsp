<%-- 
    Document   : UnionAuditDetails
    Created on : Aug 30, 2016, 9:49:02 AM
    Author     : shimanem
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<br>
<label class="subheader">Employer Audit Trail Details</label>
<hr>
<br>
<c:choose>
    <c:when test="${empty UnionAuditDetails}">
        <span class="lable">No Audit details found</span>
    </c:when>
    <c:otherwise>
        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
            <tr>
                <th align="left">User ID</th>
                <th align="left">Date</th>
                <th align="left">Type</th>
                <th align="left">Changes</th>
            </tr>
            <c:forEach items="${UnionAuditDetails}" var="union">
                <tr>
                    <td><label class="label">${union.userId}</label></td>
                    <td><label class="label">${union.lastUpdateDate}</label></td>
                    <td><label class="label">${union.auditTypeId}</label></td>
                    <td><label class="label">${union.description}</label></td>
                </tr>
            </c:forEach>
        </table>
    </c:otherwise>
</c:choose>
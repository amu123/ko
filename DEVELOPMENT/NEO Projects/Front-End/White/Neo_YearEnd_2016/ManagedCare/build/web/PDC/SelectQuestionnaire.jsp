<%-- 
    Document   : SelectQuestionnaire
    Created on : 2010/05/26, 04:02:09
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PDC WorkBench</title>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.tools.min.js"></script>
        <script>
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.forms[0].submit();
            }

            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function getFilledPDCDates(element) {
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                var reason = document.getElementById('reason').value;
                url = url + "?opperation=GetQuestionnaireDatesForPDC&element=" + reason;
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element)
                };
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);

            }

            function stateChanged(element) {
                if (xmlhttp.readyState == 4)
                {
                    //alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    //alert(result);
                    if (result == "GetQuestionnaireDatesForPDC") {
                        var dateString = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=') + 1, xmlhttp.responseText.lastIndexOf('$'));
                        var dates = new Array();
                        dates = dateString.split(',');
                        document.getElementById(element).options.length = 0;
                        for (i = 0; i < dates.length; i++) {
                            var optVal = document.createElement('OPTION');
                            optVal.value = dates[i];
                            optVal.text = dates[i];
                            document.getElementById(element).options.add(optVal);
                        }
                    } else if (result == "Error") {
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|') + 1, xmlhttp.responseText.length);
                        if (forCommand == "GetQuestionnaireDatesForPDC") {
                            document.getElementById(element).options.length = 0;
                        }
                    }
                }
            }

            function hideViewDates() {
                var value = document.getElementById('reason').value;
                alert(value);
                if (value == 9) {
                    document.getElementById('dates').style.display = 'none';
                } else {
                    document.getElementById('dates').style.display = 'block';
                }
            }
            $(document).ready(function () {
                $("#pdcReason").change(function () {

                    //get value of selected item of combobox
                    var icdType = $("#reason :selected").val();


                    if (icdType == 9) {
                        $("#dates").hide();
                    } else {
                        $("#dates").show();
                    }
                });
            });

        </script>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="10px"></td><td align="left">
                    <!-- content goes here -->
                    <br/><br/>

                    <agiletags:ControllerForm name="viewWorkBench">
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <input type="hidden" name="listIndex" id="listIndex" value="" />
                        <label class="header">Select Questionnaire</label>
                        <br/>
                        <br/>
                        <table>
                            <tr id="pdcReason"><agiletags:LabelNeoLookupValueDropDown displayName="Reason" elementName="reason" lookupId="129"/></tr>
                            <tr id="dates"><agiletags:LabelDropDownButton displayName="Questionnaire Date" elementName="pdcDates" javaScript="onClick=\"getFilledPDCDates('pdcDates');\""/></tr>
                        </table>
                        <br/>
                        <table>
                            <tr>
                                <agiletags:ButtonOpperation align="left" displayname="Back" commandName="ViewPolicyHolderDetailsCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('ViewPolicyHolderDetailsCommand')\";"/>
                                <agiletags:ButtonOpperation align="left" displayname="View/Capture" commandName="ContactPatientCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('ContactPatientCommand')\";"/>
                            </tr>
                        </table>
                    </agiletags:ControllerForm>

                    <br/>
                </td></tr></table>
    </body>
</html>

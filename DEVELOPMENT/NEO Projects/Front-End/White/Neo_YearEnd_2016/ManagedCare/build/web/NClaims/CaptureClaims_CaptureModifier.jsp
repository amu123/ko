<%-- 
    Document   : CaptureClaims_CaptureModifier
    Created on : 11 Apr 2016, 11:01:46 AM
    Author     : janf
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>

<nt:NeoPanel title="Capture Modifier For Claim Line "  collapsed="false" reload="false" contentClass="neopanelcontentwhite">
    <c:set var="a" value="adsss"></c:set>
    <agiletags:ControllerForm name="ClaimLine_Ndc_Form">
        <input type="hidden" name="opperation" id="ClaimLine_opperation" value="ClaimLineModifierCommand" />
        <input type="hidden" name="onScreen" id="ClaimHeader_onScreen" value="Modifier" />
        <input type="hidden" name="ClaimLine_claimId" id="ClaimLine_claimId" value="${claimId}" />
        <input type="hidden" name="ClaimLine_Practice_Type" id="ClaimLine_Practice_Type" value="" />
        <input type="hidden" name="command" id="ClaimHeader_command" value="SaveModifierLine" />
        <input type="hidden" name="ClaimLine_ClaimLineId" id="ClaimLine_ClaimLineId" value=""/>
        <input type="hidden" name="modList" id="modList" value="${empty modList ? null : modList}}"/>

        <table id="ClaimLine_Modifier_Capture">
            <tbody>
                <tr><nt:NeoTextField elementName="ClaimLine_ModifierCode" displayName="Modifier" onblur=""/></tr>
                <tr><nt:NeoTextField elementName="ClaimLine_ModifierUnits" displayName="Units" value="1" onblur=""/></tr>
                <tr><nt:NeoTextField elementName="ClaimLine_ModifierTime" displayName="Time" value="0" onblur=""/></tr>
            </tbody>
            <tbody id="ClaimLine_ModifierTable_Buttons">
                <tr>
                    <td><input type="button" value="Close" onclick="swapDivVisbible('Capture_Claim_Modifier_Div', 'Capture_Claim_Lines_Div')"></td>
                    <td><input type="button" value="Save" onclick="saveModifier(this.form, null, this);"></td>
                </tr>
            </tbody>
        </table>
    </agiletags:ControllerForm>

</nt:NeoPanel>

<div id="ClaimLine_Modifier_Summary">
    <jsp:include page="CaptureClaims_ModifierSummary.jsp" />
</div> 

<%--
    Document   : ProviderSearch
    Created on : 2010/07/06, 11:27:18
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Provider Search</label>
                    </br></br>
                    <table>
                        <agiletags:ControllerForm name="searchProvider">
                            <tr><agiletags:LabelTextBoxError displayName="Provider Type" elementName="provType"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Provider Number" elementName="provNo"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Provider name" elementName="provName"/></tr>
                            <tr><agiletags:ButtonOpperation type="submit" align="left" span="3" commandName="SearchProviderCommand" displayname="Search"/></tr>
                        </agiletags:ControllerForm>
                    </table>
                    </br>
                    <HR color="#666666" WIDTH="50%" align="left">
                    </br>
                    <agiletags:ProviderSearchResultTable commandName="AllocateProviderToSessionCommand" javaScript=""/>


                    <!-- content ends here -->
        </td></tr></table>
    </body>
</html>


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function searchResults(btn) {
    submitWithActionOnScreen('SearchWorkflowCommand', '/Workflow/WorkflowSearch.jsp', '30%, 70%');
}

function submitWithActionOnScreen(action, onScreen, size) {
    if (size !== null || size !== "") {
        parent.workflowFrameset.cols = size;
    }
    document.getElementById('opperation').value = action;
    document.getElementById('onScreen').value = onScreen;
    document.forms[0].submit();
}

function submitWithAction(action) {
    document.getElementById('opperation').value = action;
    document.forms[0].submit();
}


//This is to download the requested item [EML]
function submitActionAndViewFile(thisForm, action, fileLocation) {
    thisForm.opperation.value = action;
    thisForm.fileLocation.value = fileLocation;
    thisForm.submit();
}

function viewSelectedWorkflowItem(action, itemId) {
    var url = "/ManagedCare/AgileController";
    var data = {
        'opperation': action,
        'onScreen': 'Workflow/WorkflowDetails.jsp',
        'id': itemId,
        'itemId': itemId
    };
    $.get(url, data, function (resultTxt) {
        if (resultTxt != null) {
            document.getElementById('workflow_SearchResult_Content').innerHTML = resultTxt;
        }
    });

    var data = null;
}

function submitForm(form, btn) {

    if (btn !== undefined) {
        btn.disabled = true;
    }

    //MASK SUBMITED FORM UNTIL POST IS COMPLETED
    $(form).mask("Processing...");
    
    document.forms[form.name].submit();
}
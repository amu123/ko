<%-- 
    Document   : PdcSubWorkBenchTable
    Created on : 24 april 2016, 15:00:00 PM
    Author     : dewaldo
--%>
<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>


<table width="80%">
    <td>
        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px; width:100%;">
            <c:choose>
                <c:when test="${sessionScope.subWorkBenchResult != null}" >
                    <tr>
                        <th>Work list No</th>
                            <c:choose>
                                <c:when test="${applicationScope.Client == 'Sechaba'}">
                                    <th>PCC Type</th>
                                </c:when>    
                                <c:otherwise>
                                    <th>PDC Type</th>
                                </c:otherwise>
                            </c:choose>
                        <th>Risk Rating</th>
                        <th>Description</th>
                        <th>Hyperlink</th>
                        <th>Date Created</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${sessionScope.subWorkBenchResult}"> 
                        <tr>
                            <td><label class="label">${entry.worklistNumber}</label></td>
                            <td><label class="label">${fn:trim(entry.eventType.value)}</label></td>
                            <td><label class="label">${entry.eventRiskRating}</label></td>
                            <td><label class="label">${fn:trim(entry.firingDesc)}</label></td>
                            <td>
                                <label class="label">
                                    <c:if test="${fn:trim(entry.eventType.id) eq 1 || fn:trim(entry.eventType.id) eq '1'}">
                                        <img 
                                            onClick="submitAuthWithAction('${entry.operation}', '${entry.authNumber}', ${entry.authId});"
                                            src="resources/EAuth_small.gif" 
                                            alt="PreAuth" 
                                            style="width: 40px; height: 40px;">
                                        <span style="color: rgb(102, 102, 102); width: 40px; display: block;">
                                            PreAuth
                                        </span>
                                    </c:if>
                                    <c:if test="${fn:trim(entry.eventType.id) eq 2 || fn:trim(entry.eventType.id) eq '2'}">
                                        <img 
                                            onclick="submitClaimLineWithAction('ForwardToMemberClaimLineClaimLine', ${fn:trim(entry.claimLineId)})" 
                                            src="resources/Claims_small.gif" 
                                            alt="Claims" 
                                            style="width: 40px; height: 40px;">
                                        <span style="color: rgb(102, 102, 102); width: 40px; display: block;">
                                            Claims
                                        </span>
                                    </c:if>
                                    <c:if test="${fn:trim(entry.eventType.id) eq 3 || fn:trim(entry.eventType.id) eq '3'}">
                                        <img 
                                            onClick="submitBiometricWithAction('CoverDependendGridSelectionCommand',${entry.dependentNumber}, ${entry.coverNumber});"
                                            src="resources/Biometrics_small.gif" 
                                            alt="Biometrics" 
                                            style="width: 40px; height: 40px;">
                                        <span style="color: rgb(102, 102, 102); width: 40px; display: block;">
                                            Biometrics
                                        </span>
                                    </c:if>
                                    <c:if test="${fn:trim(entry.eventType.id) eq 4 || fn:trim(entry.eventType.id) eq '4'}">

                                    </c:if>
                                </label>
                            </td>
                            <td><label class="label">${entry.creationDate}</label></td>
                            <td><label class="label">${fn:trim(entry.eventStatus.value)}</label></td>   
                            <td>
                                <button 
                                    type="button" 
                                    name="ViewWorkBenchCommand" 
                                    onclick="submitWithAction('ViewWorkBenchCommand', ${entry.dependentNumber}, ${entry.eventId}, ${entry.workListID}, '${entry.coverNumber}')">
                                    Manage
                                </button>
                            </td>
                        </tr>
                    </c:forEach>
                </c:when>                       
                <c:otherwise>
                    <span class="label" style="color: #FF0000; width: 100px;">No Events Found!</span>
                </c:otherwise>
            </c:choose>
        </table>
    </td>
</table>



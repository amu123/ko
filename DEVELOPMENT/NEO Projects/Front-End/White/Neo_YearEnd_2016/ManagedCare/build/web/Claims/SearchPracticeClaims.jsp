<%-- 
    Document   : SearchPracticeClaims
    Created on : 2011/08/17, 03:13:39
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
                         <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">

                    <agiletags:ClaimsTabbedButton buttonCommand2="ViewPracticeAuthorisationCommand" buttonDisplay2="Authorisation"
                                         buttonCommand3="ViewPracticeBankingDetailsCommand" buttonDisplay3="Banking Details"
                                         buttonCommand4="ViewProviderClaimsCommand" buttonDisplay4="Claims Details"
                                         buttonCommand5="ViewPracticePaymentDetailsCommand" buttonDisplay5="Payment Details"
                                         buttonCommand6="ViewPracticeAddressDetailsCommand" buttonDisplay6="Address Details"
                                         buttonCommand7="ForwardToProviderStatementCommand" buttonDisplay7="Statements"
                                         buttonCommand1="ForwardToPracticeDetailsCommand" buttonDisplay1="Practice Details"  disabledDisplay="Claims Details" numberOfButtons="6"/>

                    <br/><br/>
                    <table>
                        <agiletags:ControllerForm name="practiceDetails">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="listIndex" id="listIndex" value=""/>
                            <input type="hidden" name="exactCoverNum" id="exactCoverNum" value="1" />

                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Member Number" elementName="memberNum" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/PreAuth/PreAuthSearch.jsp" mandatory="no" javaScript="onblur=\"getCoverByNumber(this.value, 'memberNum');\""/></tr>
                            <tr><agiletags:LabelTextBoxDate displayname="Treatment Date From" elementName="treatmentFrom"/></tr>
                            <tr><agiletags:LabelTextBoxDate displayname="Treatment Date To" elementName="treatmentTo"/></tr>

                            <tr><agiletags:ButtonOpperation align="left" span="1" type="button" commandName="SearchPracticeClaimsCommand" displayname="Search Claims" javaScript="onClick=\"submitWithAction('SearchPracticeClaimsCommand');\""/></tr>

                        </agiletags:ControllerForm>

                    </table>
        </td></tr></table>
    </body>
</html>

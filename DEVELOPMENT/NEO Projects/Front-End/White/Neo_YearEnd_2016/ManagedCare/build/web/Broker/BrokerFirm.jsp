<%-- 
    Document   : BrokerFirm
    Created on : 2012/06/18, 09:39:11
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Membership/MemberMaintenance.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/ckeditor/ckeditor.js"></script>
        <style>
            #header { position: fixed; top: 0; left: 8px; right: 8px; z-index: 999;background: #fff;}
            #footer { position: fixed; bottom: 0; left: 0; height: 100px; }
            #content {position: relative; margin-top: 100px;}
        </style>
        <script>
            $(document).ready(function(){  
               resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                    });
            });
            
            function resizeContent() {
                $('#content').css('margin-top', $('#header').height());
            }
        </script>
    </head>
    <body onload="initAgileTabs();">
     <agiletags:ControllerForm name="TabControllerForm">
        <input type="hidden" name="opperation" id="opperation" value="BrokerFirmAppTabContentsCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="FirmBrokerApplication" />
        <input type="hidden" name="firmAppCodeRes" id="firmAppCode" value="${requestScope.firmAppCode}" />
        <input type="hidden" name="brokerFirmEntityId" id="brokerFirmEntityId" value="${requestScope.brokerFirmEntityId}" />
        <input type="hidden" name="brokerFirmTile" id="brokerFirmTile" value="${requestScope.brokerFirmTile}" />
        <input type="hidden" name="brokerFirmContactPerson" id="brokerFirmContactPerson" value="${requestScope.brokerFirmContactPerson}" />
        <input type="hidden" name="brokerFirmContactSurname" id="brokerFirmContactSurname" value="${requestScope.brokerFirmContactSurname}" />
        <input type="hidden" name="firmRegion" id="brokerFirmRegion" value="${requestScope.firmRegion}" />
     
      </agiletags:ControllerForm>
      <div id="main_div">
      <div id="header">
        <ol id="tabs" class="${empty requestScope.brokerFirmEntityId?"hide":""}">
          <li><a href="#FirmAppDetails" class="">Firm Details</a></li>
          <li><a href="#Accreditation"  >Accreditation</a></li>
          <li><a href="#BankingDetails"  >Bank Details</a></li>
          <li><a href="#CommissionTransactions"  >Commission Transactions</a></li>
          <li><a href="#ContactDetails"  >Contact Details</a></li>
          <li><a href="#Notes"  >Notes</a></li>
          <li><a href="#Documents"  >Documents</a></li>
          <li><a href="#Transactions"  >Audit Trail</a></li>
          <li><a href="#CommunicationsLog"  >Communications Log</a></li>
        </ol>
        <table width="100%" bgcolor="#dddddd" style="border: 1px solid #c9c3ba;">
            <tr>
                <td width="375"><label class="label">Broker Firm Name: </label><label id="firm_header_memberName" class="label">${requestScope.firmAppName}</label></td>
                <td><label class="label">Broker Firm Number: </label><label id="firm_header_applicationNumber" class="label">${requestScope.firmAppCode}</label></td>
            <td><label ></label></td>
            </tr>
        </table>
      </div>
      <div id="content">
        <div class="tabContent" id="FirmAppDetails">Loading...</div>
        <div class="tabContent hide" id="Accreditation">Loading...</div>
        <div class="tabContent hide" id="BankingDetails">Loading...</div>
        <div class="tabContent hide" id="CommissionTransactions">Loading...</div>
        <div class="tabContent hide" id="ContactDetails">Loading...</div>
        <div class="tabContent hide" id="Transactions">Loading...</div>
        <div class="tabContent hide" id="Notes">Loading...</div>
        <div class="tabContent hide" id="Documents">Loading...</div>
        <div class="tabContent hide" id="CommunicationsLog">Loading...</div>
      </div>
      </div>
      
      <div id="overlay" style="display:none;"></div>
      <div id="overlay2" style="display:none;"></div>
                        
    </body>
</html>




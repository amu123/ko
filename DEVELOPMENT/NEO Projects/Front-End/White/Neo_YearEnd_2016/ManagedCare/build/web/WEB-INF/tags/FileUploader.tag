<%-- 
    Document   : FileUploader
    Created on : 09 Jun 2015, 1:00:20 PM
    Author     : dewaldo
--%>

<%@tag description="Used for uploading files" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@ attribute name="action" required="true" %>
<%@ attribute name="displayName" required="true" %>

<form action="/ManagedCare/AgileController?opperation=${action}" enctype="multipart/form-data" method="POST">
    <table class="list" style="border-collapse:collapse; border-width:1px; width:500px;">
        <tr>
            <th>${displayName}</th>
        </tr>
    </table>
    <table class="list" style="border-collapse:collapse; border-width:1px; width:500px;">
        <tr>
        <input type="file" name="fileName" size="75"/>
        <br/>
        </tr>
        <tr>
        <br/><input style="position:absolute" type="submit" id="submitFile"/>
        </tr>
    </table>
</form>    
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<jsp:useBean id="now" class="java.util.Date"/>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script >
            function calculateBalance() {
                var total = parseFloat($('#refundAmount').val());
                if (isNaN(total)) {total = 0};
                var allocated = 0;
                $(".contribAlloc").each( function(){
                    var tmp=parseFloat($(this).val());
                    if (isNaN(tmp)){tmp=0;};
                    allocated= allocated + tmp;     
                });
                $('#allocatedAmountId').val(allocated.toFixed(2));
                $('#remainingAmountId').val((total - allocated).toFixed(2));
            }
            
            function changeValue(item) {
                var tmp=parseFloat(item.value);
                if (isNaN(tmp) || tmp < 0){item.value = ''} else {item.value = tmp.toFixed(2)};
                calculateBalance();
            }
            
            function setMemVals(entityId, coverNo, mName, mSurname, optionId) {
                document.getElementById('entityId').value = entityId;
                document.getElementById('RefundSearchCommand').value = 'SelectMemberContribRefund';
                document.getElementById('coverNo').value = coverNo;
                document.getElementById('memberName').value = mName;                
                document.getElementById('memberSurname').value = mSurname;
                document.getElementById('optionId').value = optionId;
                formVal = document.getElementById("RefundContribSearchForm");
                getPageContents(formVal, null, 'main_div', 'overlay');
            }
            
            function setEmpVals(entityId, eName) {
                document.getElementById('entityId').value = entityId;
                document.getElementById('RefundSearchCommand').value = 'SelectEmployerContribRefund';
                document.getElementById('employerName').value = eName;                
                formVal = document.getElementById("RefundContribSearchForm");
                getPageContents(formVal, null, 'main_div', 'overlay');
            }
            
            function saveRefund(btn) {
                btn.disabled = true;
                document.getElementById('RefundSearchCommand').value = 'SaveMemberContribRefund';
                var formVal = document.getElementById("RefundContribSearchForm");
                var params = encodeFormForSubmit(formVal);
                $(formVal).mask("Saving...");
                $.post(formVal.action, params,function(result){
                    updateFormFromData(result);
                    $(formVal).unmask();
                    if (document.getElementById('transferRefundStatus').value == 'ok') {
                        swapDivVisbible('overlay','main_div');
                    } else {
                        btn.disabled = false;
                    }
                });
                
            }
            
            function saveEmployerRefund(btn) {
                if (!validateEmployerRefund()) {
                    displayNeoErrorNotification("There are validation errors");
                    return;
                }
                btn.disabled = true;
                document.getElementById('RefundSearchCommand').value = 'SaveGroupContribRefund';
                var formVal = document.getElementById("RefundContribSearchForm");
                var params = encodeFormForSubmit(formVal);
                $(formVal).mask("Saving...");
                $.post(formVal.action, params,function(result){
                    updateFormFromData(result);
                    $(formVal).unmask();
                    if (document.getElementById('transferRefundStatus').value == 'ok') {
                        swapDivVisbible('overlay','main_div');
                        displayNeoSuccessNotification("Refund has been saved");
                    } else {
                        btn.disabled = false;
                        displayNeoErrorNotification("There was an error saving the refund");
                    }
                });
                
            }
            
            function validateEmployerRefund() {
                var valid = validateNeoStringField("refundReason", true,1,40);
                valid = valid & validateNeoDateField("refundDate", true,'2012/01/01', "${agiletags:formatDate(agiletags:dateAdd(now,'month',1))}");
                valid = valid & validateNeoAmountField("refundAmount", true,0.01);
                if (valid) {
                    var remAmt = parseFloat($('#remainingAmountId').val());
                    if (remAmt != '0.00') {
                        $('#refundAmount_error').text('Refund allocation does not match refund amount');
                        valid = false;
                    }
                }
                return valid;
            }
            
            function validateAndSearchMember(frm, btn) {
                document.getElementById('RefundSearchCommand').value = 'SearchMemberContribRefund';
                var isValid = false;
                
                if ($('#memNo').val().length > 0 || $('#idNo').val().length > 0 ) {
                    isValid = true;
                } else {
                    var valCount = $('#initials').val().length > 0 ? 1 : 0;
                    valCount = valCount + ( $('#surname').val().length > 0 ? 1 : 0);
                    valCount = valCount + ($('#dob').val().length > 0 ? 1 : 0);
                    if (valCount > 1 ) {
                        isValid = true;
                    }
                }
                
                if (isValid) {
                    $('#btn_MemSearch_error').text("");
                    submitFormWithAjaxPost(frm, 'SearchListDiv', btn);
                } else {
                    $('#btn_MemSearch_error').text("Please enter some search criteria");
                }
            }
            
            function ClearValidation(){
                $("#scheme_error").text("");
                $('#btn_EmpSearch_error').text("");
            }
            
            function validateAndSearchEmployer(frm, btn) {
                document.getElementById('RefundSearchCommand').value = 'SearchEmployerContribRefund';
                
                ClearValidation();
                var scheme = $("#scheme").val();
                var isValid = false;
                
                if(scheme == null || scheme == "" || scheme == "99"){
                    $("#scheme_error").text("Scheme is mandatory");
                }
                else{
                    if ($('#groupNumber').val().length > 0 || $('#groupName').val().length > 0 ) {
                        isValid = true;
                    }
                    else{
                        $('#btn_EmpSearch_error').text("Please enter additional search criteria");
                    }
                }
                
                if (isValid) {
                    $('#btn_EmpSearch_error').text("");
                    submitFormWithAjaxPost(frm, 'SearchListDiv', btn);
                }
            }
        </script>
    </head>
    <body>
        <div class="neonotification"></div>
     <agiletags:ControllerForm name="RefundContribSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="RefundContribCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="RefundSearch" />
        <input type="hidden" name="command" id="RefundSearchCommand" value="" />
        <input type="hidden" name="entityId" id="entityId" value="" />
        <input type="hidden" name="coverNo" id="coverNo" value="" />
        <input type="hidden" name="memberName" id="memberName" value="" />
        <input type="hidden" name="memberSurname" id="memberSurname" value="" />
        <input type="hidden" name="optionId" id="optionId" value="" />
        <input type="hidden" name="transferRefundStatus" id="transferRefundStatus" value="">
        <input type="hidden" name="employerName" id="employerName" value="">
        <div id="main_div">
            <label class="header">Refund Contributions Search</label>
            <hr>
            <br>
            <table>
                <tr>
                    <td width="160"><Label>Type</Label></td>
                    <td>
                        <select style="width:215px" name="ReceiptsType" id="ReceiptsType" onChange="setElemStyle('MemberTable',this.value == 'M'? 'table' : 'none');setElemStyle('EmployerTable',this.value == 'E'? 'table' : 'none');">
                            <option value="M" selected>Member</option>
                            <option value="E">Group</option>
                        </select>
                    </td>

                </tr>
            </table>
            <table id="MemberTable">
                <tr><agiletags:LabelTextBoxError displayName="Member Number" elementName="memNo"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="ID Number" elementName="idNo"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="Initials" elementName="initials"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="surname"/></tr>
                <tr><agiletags:LabelTextBoxDate displayname="Date of Birth" elementName="dob"/></tr>
                <tr><td colspan="2" align="right"><input type="button" value="Search" onclick="validateAndSearchMember(this.form, this);"></td>
                    <td></td>
                    <td></td>
                    <td><label id="btn_MemSearch_error" class="error"></label></td>
                </tr>
            </table>
            <table id="EmployerTable" style="display:none;">
                <tr><agiletags:LabelProductDropDown displayName="Scheme" elementName="scheme" mandatory="yes"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="Group Number" elementName="groupNumber"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="Group Name" elementName="groupName"/></tr>
                <tr><td colspan="2" align="right"><input type="button" value="Search" onclick="validateAndSearchEmployer(this.form, this);"></td>
                    <td></td>
                    <td></td>
                    <td><label id="btn_EmpSearch_error" class="error"></label></td>
                </tr>
            </table>
            <div id="SearchListDiv"></div>
        </div>
        <div id="overlay"></div>
     </agiletags:ControllerForm>
    </body>
</html>

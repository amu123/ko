<%-- 
    Document   : MemberCoverAudit
    Created on : 2012/05/28, 01:46:49
    Author     : johanl
--%>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<div id="tabTableLayout">
  <agiletags:ControllerForm name="MemberCoverAuditForm">
    <input type="hidden" name="opperation" id="MemberCoverAudit_opperation" value="MemberCoverAuditCommand" />
    <input type="hidden" name="onScreen" id="MemberCoverAudit_onScreen" value="MemberCoverAudit" />
    <input type="hidden" name="memberEntityId" id="memberEntityId" value="${sessionScope.memberCoverEntityId}" />
    <label class="header">Member Audit Trail</label>
    <hr>

      <table id="MemberCoverAuditTable">
          <tr>
              <td width="160" class="label">Details</td>
              <td>
                  <select style="width:215px" name="auditList" id="auditList" onChange="submitFormWithAjaxPost(this.form, 'MemberCoverAuditDetails');">
                      <option value="99" selected>&nbsp;</option>
                      <option value="PERSON_DETAILS">Member Details</option>
                      <option value="ADDRESS_DETAILS">Address Details</option>
                      <option value="BANK_DETAILS">Bank Details</option>
                      <option value="CONTACT_DETAILS">Contact Details</option>
                      <option value="CONTACT_PREF_DETAILS">Contact Preference Details</option>
                      <option value="COVER_DETAILS">Cover Details</option>
                      <option value="MEMBER_GROUP">Member Group</option>
                      <option value="MEMBER_BROKER">Member Broker</option>
                      <option value="MEMBER_UNDERWRITING">Member Underwriting</option>
                      <option value="DOCUMENT_REINDEX">Document ReIndex</option>
                      <option value="MEMBER_ALL">All</option>
                  </select>
              </td>
              
          </tr>
      </table>          
 </agiletags:ControllerForm>
</div>
 <div style ="display: none"  id="MemberCoverAuditDetails"></div>
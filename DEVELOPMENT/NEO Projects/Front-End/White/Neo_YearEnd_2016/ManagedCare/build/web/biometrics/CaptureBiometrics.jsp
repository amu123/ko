<%-- 
    Document   : CaptureBiometrics
    Created on : 02 Feb 2015, 12:12:06 PM
    Author     : martins
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>

<%
    String revNo = (String) request.getAttribute("revNo");

%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tooltest/style.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.tools.min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/tooltest/script.js"></script>
        <!--<script type="text/javascript" src="/ManagedCare/resources/Biometrics/BiometricsTabUtil.js"></script> -->

        <script language="JavaScript">
            var valueZero = '0';
            var valueEmpty = '';
            var currentSmoker1 = "";
            var exSmoker1 = "";
            var count = 0;
            var depNoLoad = "true"; 

            $(document).ready(function() {
                $("#div1").end();
                $("#dialog-saved").hide();
                $("#dialog-notsaved").hide();
                
            <c:if test="${!empty sessionScope.tabSelected}">
                load_selectedTabs("${sessionScope.tabSelected}");
            </c:if>  

            <c:if test="${!empty sessionScope.getTrue}">
                <c:remove var="getTrue" scope="session" />
                <%response.sendRedirect("/ManagedCare/biometrics/CaptureBiometrics.jsp?appName=Capture+Biometrics");%>
            </c:if>
                
            <c:if test="${!empty sessionScope.refreshed}">
                if(document.getElementById("icd10") !== null || document.getElementById("icd10") !== ""){
                    <c:remove var="refreshed" scope="session" />
                    $("#div1").hide();
                }
            </c:if>    

                    $(".tooltext").tooltip({
                        // place tooltip on the right edge
                        position: "center right",
                        // a little tweaking of the position
                        offset: [-2, 10],
                        // use the built-in fadeIn/fadeOut effect
                        effect: "fade",
                        // custom opacity setting
                        opacity: 0.7

                    });

            <%--      $("#icdcode").change(function() {

                    var icdcode = $("#icd10 :selected").val();

                    if (icdcode == 99) {
                        $("#showdescription").hide();
                    } else {
                        $("#showdescription").show();
                    }

                });
            --%>
                    displayDialog();
                    var smoker = '<%= session.getAttribute("currentSmoker_text")%>';
                    var exsmoker = '<%= session.getAttribute("exSmoker_text")%>';

                    if (exsmoker === "true") {
                        document.getElementById("exSmoker").checked = true;
                        document.getElementById("currentSmoker").checked = false;
                    } else if (smoker === "true") {
                        document.getElementById("currentSmoker").checked = true;
                        document.getElementById("exSmoker").checked = false;
                    } else {
                        document.getElementById("exSmoker").checked = false;
                        document.getElementById("currentSmoker").checked = false;
                    }

                    if (document.getElementById("currentSmoker").checked === true) {
                        toggleSmokingQuestions("true", "currentSmoker");
                    } else if (document.getElementById("exSmoker").checked === true) {
                        toggleExSmokingQuestion("true", "exSmoker");
                    } else {
                        document.getElementById("exSmoker").checked = false;
                        document.getElementById("currentSmoker").checked = false;
                    }
                    displayError();

                    //toggleSmokingQuestions();
                    //toggleExSmokingQuestion();

            <%--          <c:if test="${currentSmoker == false && exSmoker == false}">
                          toggleSmokingQuestions();
                      </c:if>
                    
                      <c:if test="${currentSmoker == true}">
                          toggleSmokingQuestions();
                      </c:if>  
                    
                      <c:if test="${exSmoker == true}">
                          toggleExSmokingQuestion();
                      </c:if>  
            --%>

//----------From BiometricsView.JSP

            <c:if test="${sessionScope.UpdateBiometrics != 'true'}">
                <c:if test="${empty sessionScope.Atabs}">
                    $("#div1").hide();
                </c:if>
            </c:if>
            <c:if test="${sessionScope.trueAntiReload == 'trueAntiReload'}">
                <c:remove var="trueAntiReload" scope="session" />
                    getCoverByNumber('${sessionScope.memberNumber_text}', 'memberNumber');
            </c:if>


                });

                function toggleSmokingQuestions(str, element) {

                    setSession(str, element);

                    $("#exsmoke").show();
                    $("#yearsstopped").show();
                    $("#ciganumber").show();


                    currentSmoker1 = document.getElementById("currentSmoker").checked;

                    if (currentSmoker1 === true) {
                        $("#exsmoke").hide();
                        $("#yearsstopped").hide();
                        $("#ciganumber").show();
                        setSession("false", "exSmoker");
                        setSession("0", "stopped");
                        $("#stopped").val(valueZero);

                    } else if (exSmoker1 === true) {
                        toggleExSmokingQuestion(str, element);
                    } else {
                        $("#smokingQuestion").val(valueZero);
                        $("#stopped").val(valueZero);
                        document.getElementById("currentSmoker").checked = false;
                        document.getElementById("exSmoker").checked = false;
                    }
                }

                function toggleExSmokingQuestion(str, element) {

                    setSession(str, element);

                    $("#yearsstopped").show();
                    $("#ciganumber").show();
                    $("#smoke").show();


                    exSmoker1 = document.getElementById("exSmoker").checked;

                    if (exSmoker1 === true) {
                        $("#yearsstopped").show();
                        $("#ciganumber").hide();
                        $("#smoke").hide();

                        setSession("false", "currentSmoker");
                        setSession("0", "smokingQuestion");
                        $("#smokingQuestion").val(valueZero);
                    } else if (currentSmoker1 === true) {
                        toggleSmokingQuestions(str, element);
                    } else {
                        $("#smokingQuestion").val(valueZero);
                        $("#stopped").val(valueZero);
                        document.getElementById("currentSmoker").checked = false;
                        document.getElementById("exSmoker").checked = false;
                    }
                }

            <%--  
                          function toggleSmokingQuestions() {
                                var currentSmoker = $("#currentSmoker :selected").val()
                                //var currentSmoker = document.getElementById("currentSmoker").checked;


                    var exSmoker = $("#exSmoker :selected").val();
                    //alert(x);
                    var valueZero = '0';
                    var valueEmpty = '';
                    if (currentSmoker == 0) {
                        $("#exsmoke").show();
                        $("#ciganumber").hide();
                        $("#yearsstopped").hide();

                        $("#smokingQuestion").val(valueZero);
                        $("#stopped").val(valueZero);

                    } else if (currentSmoker == 99) {
                        $("#exsmoke").show();
                        $("#ciganumber").hide();
                        $("#yearsstopped").show();
                        $("#smokingQuestion").val(valueZero);
                        $("#stopped").val(valueEmpty);


                    } else {
                        var number = $("#smokingQuestion").val();
                        if (number > 0) {
                            $("#smokingQuestion").val(number);
                        } else {
                            $("#smokingQuestion").val(valueEmpty);
                        }

                        $("#exSmoker").val(0);
                        $("#stopped").val(valueZero);

                        $("#ciganumber").show();
                        $("#exsmoke").hide();
                        $("#yearsstopped").hide();


                    }

                }

                function toggleExSmokingQuestion() {
                    var exSmoker = $("#exSmoker :selected").val();

                    //alert(exSmoker);
                    var valueZero = '0';
                    var valueEmpty = '';

                    if (exSmoker == 0 || exSmoker == 99) {
                        $("#stopped").val(valueZero);
                        $("#yearsstopped").hide();
                    } else {
                        $("#stopped").val(valueEmpty);
                        $("#yearsstopped").show();
                    }
                }
            --%>
                function validateMember(str, element) {
                    xmlhttp = GetXmlHttpObject();
                    if (xmlhttp == null)
                    {
                        alert("Your browser does not support XMLHTTP!");
                        return;
                    }
                    var url = "/ManagedCare/AgileController";
                    url = url + "?opperation=FindMemberByNumberCommand&memberNumber=" + str;
                    xmlhttp.onreadystatechange = function() {
                        stateChanged(element)
                    };
                    xmlhttp.open("POST", url, true);
                    xmlhttp.send(null);
                }

                function loadQuestions(str, element) {

                    setSession(str, element);

            <%--                    xmlhttp = GetXmlHttpObject();
                                if (xmlhttp == null) {
                                    alert('Your browser does not support XMLHTTP!')
                                    return;
                                }
                                var val = $("#memberNumber_text").val();
                                var url = "/ManagedCare/AgileController";
                                url = url + "?opperation=LoadQuestionsCommand&depListValues=" + str + "&memberNumber=" + val;
                                xmlhttp.onreadystatechange = function() {
                                    stateChanged(element)
                                };
                                xmlhttp.open("POST", url, true);
                                xmlhttp.send(null);
                                $("#bpSystolic").change();
                                $("#bpDiastolic").change();
                                $("#weight").blur();
                                $("#length").blur();
                                $("#bmi").change();
                                $("#memberNumber").change();
                                $("#depListValues").change();
                                $("#dateMeasured").blur();
                                $("#dateReceived").blur(); 
                  
                               $("#alcoholConsumption").change();
                               $("#currentSmoker").click();
                               $("#smokingQuestion").change();
                               $("#exSmoker").click();
                               $("#stopped").change();
                               $("#exerciseQuestion").change();
                               $("#providerNumber").change();
                               $("#source").change();
                               $("#icd10").change();
            --%>
                }

                function validateProvider(str, element) {

                    setSession(str, element);

                    xmlhttp = GetXmlHttpObject();
                    if (xmlhttp == null)
                    {
                        alert("Your browser does not support XMLHTTP!");
                        return;
                    }
                    var url = "/ManagedCare/AgileController";
                    url = url + "?opperation=FindProviderByCodeCommand&providerNumber=" + str;
                    xmlhttp.onreadystatechange = function() {
                        stateChanged(element)
                    };
                    xmlhttp.open("POST", url, true);
                    xmlhttp.send(null);
                }

                function GetXmlHttpObject()
                {
                    if (window.ActiveXObject)
                    {
                        // code for IE6, IE5
                        return new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    if (window.XMLHttpRequest)
                    {
                        // code for IE7+, Firefox, Chrome, Opera, Safari
                        return new XMLHttpRequest();
                    }

                    return null;
                }

                function GetDOMParser(xmlStr) {
                    var xmlDoc;
                    if (window.DOMParser) {
                        parser = new DOMParser();
                        xmlDoc = parser.parseFromString(xmlStr, "text/xml");
                    } else {
                        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                        xmlDoc.async = "false";
                        xmlDoc.loadXML(xmlStr);
                    }
                    return xmlDoc;
                }

                function getCoverByNumber(str, element) {
                    setSession(str, element);

                    if (str !== null && str !== "") {
                        xhr = GetXmlHttpObject();//xhr = XMLHttpRequest object
                        if (xhr == null) {
                            alert("ERROR: Browser Incompatability");
                            return;
                        }
                        //url command GetCoverDetailsByNumber location - com.agile.command
                        var url = "/ManagedCare/AgileController";
                        url += "?opperation=GetCoverDetailsByNumber&number=" + str + "&element=" + element + "&exactCoverNum=1";
                        xhr.onreadystatechange = function() {
                            if (xhr.readyState == 4 && xhr.statusText == "OK") {
                                if (${sessionScope.UpdateBiometrics == 'true'} && depNoLoad === "true") {
                                    //loadQuestions(str, element);
                                    $("#depListValues").empty();//clear EntityCoverDependantDropDown tag with elementName of depListValues
                                    $("#depListValues").append("<option value=\"99\"></option>");//add empty option to force selection
                                    var xmlDoc = GetDOMParser(xhr.responseText);//loads xml document of servlets response to the page
                                    //print value from servlet
                                    $(xmlDoc).find("EAuthCoverDetails").each(function() {//for each <EAuthCoverDetails> in the xml document
                                        //set product details
                                        var prodName = $(this).find("ProductName").text();//textContent of <ProductName> in the <EAuthCoverDetails>  tag
                                        var optName = $(this).find("OptionName").text();//textContent of <OptionName> in the <EAuthCoverDetails>  tag

                                        //extract dependent info from tag
                                        $(this).find("DependantInfo").each(function() {//for each <DependantInfo> in the <EAuthCoverDetails>  tag
                                            var depInfo = $(this).text();
                                            var nameSurnameType = depInfo.split("|");
                                            //Add option to dependant dropdown
                                            var optVal = document.createElement('OPTION');
                                            optVal.value = nameSurnameType[0];
                                            optVal.text = nameSurnameType[1];
                                            document.getElementById("depListValues").options.add(optVal);

                                        });
                                        //DISPLAY MEMBER DETAIL LIST

                                        $(document).find("#schemeName").text(prodName);//set product name to LabelTextDisplay tag with the element name of schemeName
                                        $(document).find("#schemeOptionName").text(optName);//set option name to LabelTextDisplay tag with the element name of schemeOptionName
                                        //error check
                                        $("#" + element + "_error").text("");
                                        var errorText = $(this).text();
                                        var errors = errorText.split("|");
                                        if (errors[0] == "ERROR") {
                                            $("#" + element + "_error").text(errors[1]);
                                        }

                                    });

                                    document.getElementById('depListValues').value = '${sessionScope.depListValues}';
                                    depNoLoad === "false";
                                    BiometricsToSession('${sessionScope.icd10_text}', 'BiometricsTabsCommand', 'icd10');
                                }
                                else
                                {
                                    //loadQuestions(str, element);
                                    $("#depListValues").empty();//clear EntityCoverDependantDropDown tag with elementName of depListValues
                                    $("#depListValues").append("<option value=\"99\"></option>");//add empty option to force selection
                                    var xmlDoc = GetDOMParser(xhr.responseText);//loads xml document of servlets response to the page
                                    //print value from servlet
                                    $(xmlDoc).find("EAuthCoverDetails").each(function() {//for each <EAuthCoverDetails> in the xml document
                                        //set product details
                                        var prodName = $(this).find("ProductName").text();//textContent of <ProductName> in the <EAuthCoverDetails>  tag
                                        var optName = $(this).find("OptionName").text();//textContent of <OptionName> in the <EAuthCoverDetails>  tag

                                        //extract dependent info from tag
                                        $(this).find("DependantInfo").each(function() {//for each <DependantInfo> in the <EAuthCoverDetails>  tag
                                            var depInfo = $(this).text();
                                            var nameSurnameType = depInfo.split("|");
                                            //Add option to dependant dropdown
                                            var optVal = document.createElement('OPTION');
                                            optVal.value = nameSurnameType[0];
                                            optVal.text = nameSurnameType[1];
                                            document.getElementById("depListValues").options.add(optVal);

                                        });
                                        //DISPLAY MEMBER DETAIL LIST

                                        $(document).find("#schemeName").text(prodName);//set product name to LabelTextDisplay tag with the element name of schemeName
                                        $(document).find("#schemeOptionName").text(optName);//set option name to LabelTextDisplay tag with the element name of schemeOptionName
                                        //error check
                                        $("#" + element + "_error").text("");
                                        var errorText = $(this).text();
                                        var errors = errorText.split("|");
                                        if (errors[0] == "ERROR") {
                                            $("#" + element + "_error").text(errors[1]);
                                        }

                                    });
                                }
                            }
                        }
                        xhr.open("POST", url, true);
                        //xhr.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
                        xhr.send(null);

                    }
                }

                function stateChanged(element) {

                    if (xmlhttp.readyState == 4)
                    {
                        var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                        if (result == "LoadQuestionsCommand") {

                            var weigth = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            var exercise = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            var alcoholUnits = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=') + 1, xmlhttp.responseText.lastIndexOf('$'));

                            var splitResult = weigth.split('|');
                            var blae = splitResult[1];
                            var b = blae.split('=');
                            var w = splitResult[0];
                            var e = b[1];
                            var l = splitResult[2].split('=');
                            var le = l[1];
                            var bmiV = splitResult[3].split('=');
                            var bmi = bmiV[1];
                            var bpsV = splitResult[4].split('=');
                            var bps = bpsV[1];
                            var bpdV = splitResult[5].split('=');
                            var bpd = bpdV[1];
                            document.getElementById('weight').value = w;
                            document.getElementById('exerciseQuestion').value = e;
                            document.getElementById('alcoholConsumption').value = alcoholUnits;
                            document.getElementById('length').value = le;
                            document.getElementById('bmi').value = bmi;
                            document.getElementById('bpSystolic').value = bps;
                            document.getElementById('bpDiastolic').value = bpd;
                        }
                        else if (result == "LoadICDLongDescriptionsCommand") {
                            var description = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=') + 1, xmlhttp.responseText.lastIndexOf('$'));
                            $(document).find('#icdDescription').text(description);
                            //sendMemberNumber(str, element);
                        } else if (result == "FindProviderByCodeCommand") {
                            document.getElementById(element + '_error').innerHTML = '';
                        } else if (result == "Error") {
                            var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|') + 1, xmlhttp.responseText.length);
                            if (forCommand == "FindProviderByCodeCommand") {
                                document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                                //document.getElementById('provName').value = '';
                                document.getElementById('discipline').value = '';
                                document.getElementById('searchCalled').value = "";
                            } else if (forCommand == "GetMemberByNumberCommand") {
                                document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                                document.getElementById(element + '_text').value = '';
                                document.getElementById('searchCalled').value = "";
                                submitWithAction("MemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/LogNewCall.jsp");
                            } else if (forCommand == "LoadQuestionsCommand") {
                                //document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                                document.getElementById('weight').value = '';
                                document.getElementById('exerciseQuestion').value = '';
                                document.getElementById('alcoholConsumption').value = '';
                                document.getElementById('bmi').value = '';
                                document.getElementById('bpSystolic').value = '';
                                document.getElementById('length').value = '';
                                document.getElementById('bpDiastolic').value = '';
                                document.getElementById('searchCalled').value = "";
                            } else if (forCommand == "FindProviderByCodeCommand") {
                                alert(forCommand);
                                document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            }
                        }
                    }
                }

                function validateMember(str, element) {
                    xmlhttp = GetXmlHttpObject();
                    if (xmlhttp == null) {
                        alert("Your browser does not support XMLHTTP!");
                        return;
                    }
                    var url = "/ManagedCare/AgileController";
                    url = url + "?opperation=MemberDetailsCoverSearchCommand&memberNumber=" + str + "&exactCoverNum=1";
                    xmlhttp.onreadystatechange = function() {
                        stateChanged(element)
                    };
                    xmlhttp.open("POST", url, true);
                    xmlhttp.send(null);
                }

                function submitWithAction(action) {
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }

                function submitWithAction(action, forField, onScreen) {
                    document.getElementById('onScreen').value = onScreen;
                    document.getElementById('searchCalled').value = forField;
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }

                function calculateBMI(str, element) {

                    setSession(str, element);

                    var height = document.getElementById('length').value;
                    var weight = document.getElementById('weight').value;
                    var h2 = height * height
                    var bmi = weight / h2;
                    var roundedBMI = Math.round(bmi);
                    if (height != 0) {
                        document.getElementById('bmi').value = roundedBMI;
                        setSession(roundedBMI, 'bmi');
                    } else {
                        document.getElementById('bmi').value = '';
                    }

                    return;

                }

                function loadICDDescription(str, element) {
                    xmlhttp = GetXmlHttpObject();
                    if (xmlhttp == null) {
                        alert('Your browser does not support XMLHTTP!')
                        return;
                    }
                    var url = "/ManagedCare/AgileController";
                    url = url + "?opperation=LoadICDLongDescriptionsCommand&icd10=" + str;
                    xmlhttp.onreadystatechange = function() {
                        stateChanged(element)
                    };
                    xmlhttp.open("POST", url, true);
                    xmlhttp.send(null);
                }


                function CheckSearchCommands() {
                    displayDialog();
                    //reload ajax commands for screen population
                    var searchVal = document.getElementById('searchCalled').value;
                    if (searchVal == 'memberNumber') {
                        //alert(document.getElementById('memberNumber_text').value);
                        getCoverByNumber(document.getElementById('memberNumber_text').value, 'memberNumber');
                        //toggleAnswerType();
                    } else if (searchVal == 'treatingProvider') {
                        //alert(document.getElementById('providerNumber_text').value);
                        validateProvider(document.getElementById('treatingProvider_text').value, 'treatingProvider');
                    }
                    document.getElementById('searchCalled').value = '';
                    // var ex = $("#exSmoker :selected").val();
                    // var curr = $("#currentSmoker :selected").val();
                    //$("#smoke").change();
                }

                function displayDialog() {
            <%  if ((session.getAttribute("updated")) != null && (!session.getAttribute("updated").equals("null"))) {
            %>

                    $("#dialog-saved").dialog({modal: true, width: 550,
                        buttons: {
                            'Done': function() {
                                $(this).dialog('close');
                            }
                        }
                    });
            <%
                    session.setAttribute("updated", null);
                }%>
                }

                function displayError() {
            <%  if ((session.getAttribute("failed")) != null && (!session.getAttribute("failed").equals("null"))) {
            %>

                    $("#dialog-notsaved").dialog({modal: true, width: 550,
                        buttons: {
                            'Done': function() {
                                $(this).dialog('close');
                            }
                        }
                    });
            <%
                    session.setAttribute("failed", null);
                }%>


                }

                function load_selectedTabs(id) {
                    if (id === "link_AST") {
                        $("#div1").load("CaptureAsthma.jsp");
                    }
                    if (id === "link_HTENS") {
                        $("#div1").load("CaptureHypertension.jsp");
                    }
                    if (id === "link_CRD") {
                        $("#div1").load("CaptureChronicRenal.jsp");
                    }
                    if (id === "link_CF") {
                        $("#div1").load("CaptureCardiacFailure.jsp");
                    }
                    if (id === "link_DIA") {
                        $("#div1").load("CaptureDiabetes.jsp");
                    }
                    if (id === "link_CAD") {
                        $("#div1").load("CaptureCoronaryDisease.jsp");
                    }
                    if (id === "link_CB") {
                        $("#div1").load("CaptureCOPD.jsp");
                    }
                    if (id === "link_HLIP") {
                        $("#div1").load("CaptureHyperlipidaemia.jsp");
                    }
                    if (id === "link_MD") {
                        $("#div1").load("CaptureMajorDepression.jsp");
                    }
                    if (id === "link_CDLOTH") {
                        ReloadOther();
                    }
                }

                //used to refresh the content of the div in Internet Explorer
                function ReloadOther() {
                    var test = 'CaptureOther.jsp';
                    if(${!empty sessionScope.refreshed}){
                        <c:remove var="refreshed" scope="session"/>
                        test = 'biometrics/CaptureOther.jsp';
                    }
                    var
                            $http,
                            $self = arguments.callee;

                    if (window.XMLHttpRequest) {
                        $http = new XMLHttpRequest();
                    } else if (window.ActiveXObject) {
                        try {
                            $http = new ActiveXObject('Msxml2.XMLHTTP');
                        } catch (e) {
                            $http = new ActiveXObject('Microsoft.XMLHTTP');
                        }
                    }

                    if ($http) {
                        $http.onreadystatechange = function()
                        {
                            if (/4|^complete$/.test($http.readyState)) {
                                document.getElementById('div1').innerHTML = $http.responseText;
                            }
                        };
                        $http.open('GET', test + '?' + new Date().getTime(), true);
                        $http.send(null);
                    }

                }


                function submitScreenRefresh(screen) {
                    $("#opperation").val("RefreshCommand");
                    $("#refreshScreen").val(screen);
                    document.forms[0].submit();

                }

                function setSession(value, elementName) {

                    var url = "/ManagedCare/AgileController";
                    var data = {
                        //replace this with your command name
                        'opperation': 'CaptureSessionCommand',
                        'value_text': value,
                        'elementName_text': elementName,
                        'id': new Date().getTime()
                    };

                    $.get(url, data, function() {
                    });

                    data = null;

                    if (elementName === "dateReceived") {
                        $("#dateMeasured").blur();
                    }

                }

                //function to reload all the text box etc. into the session to capture new CDL
                function reloadAndCapture() {
                    var smoker = document.getElementById("currentSmoker").checked;
                    var exsmoker = document.getElementById("exSmoker").checked;
                    var memnum = document.getElementById("memberNumber_text").value;
                    var deplistval = document.getElementById("depListValues").value;
                    var datemeasured = document.getElementById("dateMeasured").value;
                    var datereceived = document.getElementById("dateReceived").value;
                    var bpsystolic = document.getElementById("bpSystolic").value;
                    var bpdiastolic = document.getElementById("bpDiastolic").value;
                    var weight = document.getElementById("weight").value;
                    var length = document.getElementById("length").value;
                    var bmi = document.getElementById("bmi").value;
                    //var alcoholconsumption = document.getElementById("alcoholConsumption").value;
                    $("#alcoholConsumption").change();
                    var smokingquestion = document.getElementById("smokingQuestion").value;
                    var stopped = document.getElementById("stopped").value;
                    var icd10 = document.getElementById("icd10_text").value;
//                    setSession(smoker,"currentSmoker");
//                    setSession(exsmoker, "exSmoker");
                    setSession(memnum, "memberNumber");
                    setSession(deplistval, "depListValues");
                    setSession(datemeasured, "dateMeasured");
                    setSession(datereceived, "dateReceived");
                    setSession(bpsystolic, "bpSystolic");
                    setSession(bpdiastolic, "bpDiastolic");
                    setSession(weight, "weight");
                    setSession(length, "length");
                    setSession(bmi, "bmi");
                    //setSession(alcoholconsumption, "alcoholConsumption");
                    setSession(smokingquestion, "smokingQuestion");
                    setSession(stopped, "stopped");
                    setSession(icd10, "icd10");
                    $("#exerciseQuestion").change();
                    $("#providerNumber").change();
                    $("#source").change();

                    //reload every value into sessions
//                    $("#bpSystolic").change();
//                    $("#bpDiastolic").change();
//                    $("#weight").blur();
//                    $("#length").blur();
//                    $("#bmi").change();
//                    $("#memberNumber").change();
//                    $("#depListValues").change();
//                    $("#dateMeasured").blur();
//                    $("#dateReceived").blur(); 
//                    $("#alcoholConsumption").change();

                    //get the values of the check boxes
                    if (smoker === true) {
                        setSession(smoker, "currentSmoker");
                    } else {
                        setSession("false", "currentSmoker");
                    }

                    if (exsmoker === true) {
                        setSession(exsmoker, "exSmoker");
                    } else {
                        setSession("false", "exSmoker");
                    }

//                    $("#smokingQuestion").change();
//                    $("#stopped").change();
//                    $("#exerciseQuestion").change();
//                    $("#providerNumber").change();
//                    $("#source").change();
//                    $("#icd10").change();
                    //submit the page to capute the value
                    document.getElementById('onScreen').value = "/biometrics/CaptureBiometrics.jsp";
                    submitWithAction('CaptureBiometricsCommand');
            <%--<c:remove scope="session" var="icd10_text" />--%>
                }

                function BiometricsToSession(ICD10, command, element) {

                    setSession(ICD10, element);

                    if (ICD10 === "" && ${sessionScope.UpdateBiometrics != 'true'}) {
                        $("#pageBorder").hide();
                        $("#div1").hide();
                    }

                    var url = "/ManagedCare/AgileController";

                    var data = {
                        'Ticd10_text': ICD10,
                        'opperation': command,
                        'id': new Date().getTime()

                    };

                    $.get(url, data, function(resultTxt) {

                        if (resultTxt !== null) {
                            var resTxtArr = resultTxt.split("|");
                            var result = resTxtArr[0];
                            if (result === "Done") {
                                submitScreenRefresh("/biometrics/CaptureBiometrics.jsp");
                            } else {
                                $("#icd10_error").text("ICD10 error");
                                $("#div1").hide();
                            }
                        } else {
                            $("#icd10_error").text("ICD10 error");
                            $("#div1").hide();
                        }
                    });
                    data = null;
                }
                
                function submitReturnAction(action) {
                    document.getElementById("opperation").value = action;
                    document.forms[0].submit();
                }

        </script>

    </head>
    <body onload="CheckSearchCommands();">
        <div id="dialog-saved" title="Biometrics Saved"><p>Biometrics has been saved. Reference No:<%=revNo%></p></div>
        <div id="dialog-notsaved" title="Biometrics Not Saved"><label class="error">Biometrics save has failed!</label></div>
        <table><tr valign="center"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <c:if test="${!empty sessionScope.pdcFlag && (sessionScope.pdcFlag eq true || sessionScope.pdcFlag eq 'true')}">
                        <agiletags:PDCTabs tabSelectedIndex="link_Auth" />
                        <br/><br/><br/>
                    </c:if>
                    <label class="header">Biometrics Measurement</label>  
                    <table>
                        <tr>
                            <td>
                                <agiletags:ControllerForm name="CaptureBiometrics">
                                    <input type="hidden" name="opperation" id="opperation" value="" />
                                    <agiletags:HiddenField elementName="searchCalled"/>
                                    <input type="hidden" name="onScreen" id="onScreen" value="" />
                                    <input type="hidden" name="providerDetails" id="providerDetails" value="email" />
                                    <input type="hidden" name="Source" id="source" value=""/>
                                    <input type="hidden" name="refreshScreen" id="refreshScreen" value=""/>
                                    <input type="hidden" name="exactCoverNum" id="exactCoverNum" value="1" /> 
                                </td>
                            </tr>
                            <tr>
                                <td><agiletags:LabelTextSearchText displayName="Member Number" elementName="memberNumber" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand"  valueFromRequest="yes" valueFromSession="yes" onScreen="/biometrics/CaptureBiometrics.jsp" mandatory="yes" javaScript="onChange=\"getCoverByNumber(this.value, 'memberNumber');\""/></td>
                                <td><agiletags:EntityCoverDependantDropDown displayName="Cover Dependants" elementName="depListValues" javaScript="onChange=\"loadQuestions(this.value, 'depListValues');\"" mandatory="Yes" valueFromSession="yes" errorValueFromSession="yes"/></td>
                            </tr>

                            <tr>
                                <td><agiletags:LabelTextBoxDateTime displayName="Date Measured" elementName="dateMeasured" valueFromSession="yes" mandatory="yes" javaScript="onBlur=\"setSession(this.value, 'dateMeasured');\"" /></td>
                                <td><agiletags:LabelTextBoxDate displayname="Date Received" elementName="dateReceived" valueFromSession="yes" mandatory="yes" javascript="onBlur=\"setSession(this.value, 'dateReceived');\""  /></td>
                            </tr>

                            <tr><td><br/></td></tr>

                            <tr>
                                <td><agiletags:LabelTextBoxError displayName="Blood Pressure Systolic(mmHg)" elementName="bpSystolic" valueFromSession="yes" mandatory="no" javaScript="onChange=\"setSession(this.value, 'bpSystolic');\""/></td>
                                <td><agiletags:LabelTextBoxError displayName="Blood Pressure Diastolic(mmHg)" elementName="bpDiastolic" valueFromSession="yes" mandatory="no" javaScript="onChange=\"setSession(this.value, 'bpDiastolic');\"" /></td>
                            </tr>

                            <tr>
                                <td><agiletags:LabelTextBoxError displayName="Weight (KG) e.g. 50.5" elementName="weight" valueFromSession="yes" mandatory="no" javaScript="onBlur=\"calculateBMI(this.value, 'weight');\""/></td>
                                <td><agiletags:LabelTextBoxError displayName="Length (M) e.g. 1.65" elementName="length" valueFromSession="yes" mandatory="no" javaScript="onBlur=\"calculateBMI(this.value, 'length');\""/></td>
                            </tr>
                            <tr>
                                <td><agiletags:LabelTextBoxError displayName="BMI" elementName="bmi" valueFromSession="yes" mandatory="no" readonly="yes" javaScript="onChange=\"setSession(this.value, 'bmi');\"" /></td>
                                <td id="alcoholConsumption"><agiletags:LabelGenericNumberDropDownError displayName="Alcohol Units Per Week?" elementName="alcoholConsumption" errorValueFromSession="yes" mandatory="no" listSize="21" javaScript="onChange=\"setSession(this.value, 'alcoholConsumption');\""/></td>
                            </tr>

                            <%--<tr id="smoke"><td ><agiletags:LabelNeoLookupValueDropDownError displayName="Current Smoker" elementName="currentSmoker" lookupId="67" mandatory="no" javaScript="onChange=\"toggleSmokingQuestions();\"" errorValueFromSession="yes"/></td></tr> --%>  
                            <tr id="smoke"><td><agiletags:LabelCheckboxSingle displayName="Current Smoker" elementName="currentSmoker" javascript="onClick=\"toggleSmokingQuestions(this.checked, 'currentSmoker');\"" value="" /></td></tr>
                            <tr id="ciganumber"><td><agiletags:LabelTextBoxError displayName="Cigarettes Per Day" elementName="smokingQuestion" valueFromSession="yes" mandatory="no" javaScript="onChange=\"setSession(this.value, 'smokingQuestion');\""/></td></tr>



                            <%--<tr id="exsmoke"><td ><agiletags:LabelNeoLookupValueDropDownError displayName="Ex Smoker" elementName="exSmoker" lookupId="67" mandatory="no" javaScript="onChange=\"toggleExSmokingQuestion();\"" errorValueFromSession="yes"/></td></tr> --%>
                            <tr id="exsmoke"><td><agiletags:LabelCheckboxSingle displayName=" Ex Smoker" elementName="exSmoker" javascript="onClick=\"toggleExSmokingQuestion(this.checked, 'exSmoker');\"" value="" /></td></tr>
                            <tr id="yearsstopped"><td><agiletags:LabelTextBoxError displayName="Number of Years Since Stopped" elementName="stopped" valueFromSession="yes" mandatory="no" javaScript="onChange=\"setSession(this.value, 'stopped');\""/></td></tr>

                            <tr>
                                <td id="exerciseQuestion"><agiletags:LabelGenericNumberDropDownError displayName="Exercise Per Week" elementName="exerciseQuestion" errorValueFromSession="yes" mandatory="no" listSize="8" javaScript="onChange=\"setSession(this.value, 'exerciseQuestion');\""/></td>
                            </tr>     

                            <tr>
                                <td><agiletags:LabelTextSearchText displayName="Treating Provider" elementName="providerNumber" searchFunction="yes" searchOperation="ForwardToSearchProviderCommand"  valueFromSession="yes" onScreen="/biometrics/CaptureBiometrics.jsp" mandatory="yes" javaScript="onChange=\"validateProvider(this.value, 'providerNumber');\""/></td>
                                <td><agiletags:LabelNeoLookupValueDropDownError displayName="Source" elementName="source" lookupId="104" mandatory="no" errorValueFromSession="yes" javaScript="onChange=\"setSession(this.value, 'source');\""/></td>
                            </tr>

                            <tr>
                                <%--<td  id="icdcode"><agiletags:LabelNeoLookupValueDropDownError displayName="ICD10" elementName="icd10" lookupId="120" javaScript="onBlur=\"loadICDDescription(this.value, 'icd10');\"" mandatory="yes" errorValueFromSession="yes"/></td>--%>
                                <%--<tr><td><agiletags:LabelTextBoxError displayName="ICD10" elementName="icd10" javaScript="onBlur=\"BiometricsToSession(this.value, 'BiometricsTabsCommand', 'icd10');\"" mandatory="yes" valueFromSession="yes"/></td></tr>--%>
                            <tr><td><agiletags:LabelTextSearchText displayName="ICD10" elementName="icd10" javaScript="onBlur=\"BiometricsToSession(this.value, 'BiometricsTabsCommand', 'icd10');\"" mandatory="yes" valueFromSession="yes"  valueFromRequest="yes" onScreen="/biometrics/CaptureBiometrics.jsp" searchFunction="yes" searchOperation="ForwardToSearchICDCommand"/></td></tr>
                            <%-- <td id="showdescription"><agiletags:LabelTextDisplay displayName="ICD10 Description" elementName="icdDescription" valueFromSession="yes" javaScript=""/></td>  --%>

                        </agiletags:ControllerForm>
                    </table><br/><br/>

                    <!--div to load tabs and jsp-->
                    <div id="div1" class="">
                    </div>
                    <br/>  

                    <table>
                        <agiletags:ButtonOpperation commandName="ResetBiometricsCommand" align="" displayname="Reset" span="3" type="button"  javaScript="onClick=\"submitWithAction('ResetBiometricsCommand')\";"/>
                        <c:if test="${sessionScope.UpdateBiometrics != 'true'}">
                            <agiletags:ButtonOpperation commandName="CaptureBiometricsCommand" align="" displayname="Save" span="3" type="button"  javaScript="onClick=reloadAndCapture()"/>
                        </c:if>
                        <c:if test="${sessionScope.UpdateBiometrics == 'true' || sessionScope.trueAntiReload == 'trueAntiReload'}">
                            <agiletags:ButtonOpperation commandName="CaptureBiometricsCommand" align="" displayname="Update" span="3" type="button"  javaScript="onClick=reloadAndCapture()"/>
                        </c:if>
                    </table>

                </td></tr>
        </table>

    </body>
</html>

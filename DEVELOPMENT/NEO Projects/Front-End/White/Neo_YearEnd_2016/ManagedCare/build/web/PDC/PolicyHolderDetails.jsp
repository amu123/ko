<%-- 
    Document   : PolicyHolderDetails
    Created on : 2010/04/14, 03:56:36
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" language="JavaScript">

            $(document).ready(function () {
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <!-- content goes here -->
                    <agiletags:PDCTabs tabSelectedIndex="link_PH" />
                    <fieldset id="pageBorder">
                        <div class="content_area" >
                            <agiletags:ControllerForm name="policyHolder">
                                <label class="header">Patient Details</label>
                                <br/>
                                <br/>
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <input type="hidden" name="onScreen" id="onScreen" value="" />
                                <table>
                                    <tr><agiletags:LabelTextBoxError displayName="Policy Number" elementName="policyNumber" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Policy Status" elementName="policyStatus" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Dependant Number" elementName="dependantNumber" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Title" elementName="title" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Initials" elementName="initials" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Name" elementName="patientName" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="surname" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Gender" elementName="patientGender" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Date Of Birth" elementName="birthDate" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="ID Number" elementName="idNumber" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Marital Status" elementName="matitalStatus" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Home Language" elementName="homeLanguage" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Income Category" elementName="incomeCategoty" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Job Title" elementName="jobTitle" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Employer Name" elementName="employerName" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Product Name" elementName="productName" valueFromSession="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Option Name" elementName="optionName" valueFromSession="yes"/></tr>
                                </table>
                                <br/>
                                <!--<agiletags:ButtonOpperation align="left" displayname="Questionnaire" commandName="SelectQuestionnaireCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('SelectQuestionnaireCommand')\";"/>-->
                                <table>
                                    <agiletags:ButtonOpperation align="left" displayname="Back" commandName="BackToWorkbenchCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('BackToWorkbenchCommand')\";"/>                              
                                    <!--
                                    <agiletags:ButtonOpperation align="left" displayname="Schedule Task/Close Event" commandName="ScheduleTaskCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('ScheduleTaskCommand')\";"/>
                                    <agiletags:ButtonOpperation align="left" displayname="Manage Event" commandName="ViewManageEventCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('ViewManageEventCommand')\";"/>
                                    -->
                                </table>
                            </agiletags:ControllerForm>
                        </div></fieldset>
                </td></tr></table>
    </body>
</html>

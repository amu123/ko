<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

  <agiletags:ControllerForm name="ConsultantAndBrokerForm">
    <input type="hidden" name="opperation" id="opperation" value="AddConsultantHistoryCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="ConsultantDependantAppAdd" />
    <input type="hidden" name="buttonPressed" id="consultantValuesButtonPressed" value="" />
    <input type="hidden" name="brokerEntityId" id="brokerFirmAddHistoryEntityId" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="consultantEntityId" id="brokerConsultantEntityId" value="${requestScope.consultantEntityId}" />
    <br/>
    <br/>
    <label class="header">Link Consultant To Broker</label>
    <br>
     <table>                        
        <tr><agiletags:LabelTextSearchTextDiv displayName="Broker Code" elementName="Disorder_diagCode" mandatory="yes" onClick="document.getElementById('consultantValuesButtonPressed').value = 'SearchBrokerButton'; getPageContents(document.forms['ConsultantAndBrokerForm'],null,'overlay','overlay2');"/>        </tr>
        <tr><agiletags:LabelTextDisplay displayName="Broker Name" elementName="Disorder_diagName" valueFromSession="no" javaScript="" /></tr>
        <tr id="FirmApp_brokerStartDateRow"><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="ConsApp_historyStartDate" valueFromSession="yes" mandatory="yes"/>        </tr> 
        <tr id="FirmApp_brokerEndDateRow"><agiletags:LabelTextBoxDateReq  displayname="End Date" elementName="Cons_historyEndDate" valueFromSession="yes" mandatory="yes"/>        </tr> 
     </table>

    <hr>
    <table id="MemberAppDepSaveTable">
        <tr>
           <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
           <td><input type="button" value="Save" onclick="document.getElementById('consultantValuesButtonPressed').value='SaveButton';submitFormWithAjaxPost(this.form, 'ConsultantHistoty', null,'${main_div}', '${target_div}');"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>

<%-- 
    Document   : CallCenterTabs
    Created on : 2013/01/17, 03:36:30
    Author     : johanl
--%>

<%@tag description="call center menu item tabs tag" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="cctSelectedIndex"%>

<%-- any content can be specified here e.g.: --%>
<div id="header" class="header_area">
    <ol id="tabs">
        <li><a id="link_PH" onClick="submitWithAction('ForwardToMemberDetailsCommand');" href="#PolicyHolder" >Policy Holder</a></li>
        <li><a id="link_MI" onClick="submitWithAction('ViewMemberInfoCommand');" href="#MemberInformation" >Member Information</a></li>
        <li><a id="link_AD" onClick="submitWithAction('ViewMemberAddressDetailsCommand');" href="#AddressDetail">Address Details</a></li>
        <c:if test="${applicationScope.Client == 'Sechaba'}">
            <li><a id="link_DSP" onClick="submitWithAction('ForwardToMemberNominatedProviderCommand');" href="#MemberNominatedProviderDetails">DSP</a></li>
        </c:if>
        <li><a id="link_BD" onClick="submitWithAction('ViewMemberBankingDetailsCommand');"href="#BankingDetail">Banking Details</a></li>
        <li><a id="link_Ben" onClick="submitWithAction('ForwardToMemberBenefitsCommand');" href="#Benefits">Benefits</a></li>
        <li><a id="link_CD" onClick="submitWithAction('ForwardToMemberClaimsCommand');" href="#Claims">Claims</a></li>
        <li><a id="link_PR" onClick="submitWithAction('ViewMemberPaymentDetailsCommand');" href="#PaymentRuns">Payments</a></li>
        <li><a id="link_SD" onClick="submitWithAction('ForwardToMemberStatementsCommand');" href="#Statements">Statements</a></li>
        <li><a id="link_Auth" onClick="submitWithAction('ViewMemberPreAuthorisationCommand');" href="#Auths">Authorisations</a></li>
        <li><a id="link_UD" onClick="submitWithAction('ViewMemberUnderwritingCommand');" href="#Underwriting">Underwriting</a></li>
        <li><a id="link_Contri" onClick="submitWithAction('ViewMemberContributionsCommand');" href="#Contributions">Contributions</a></li>
        <li><a id="link_Docs" onClick="submitWithAction('CallDocumentViewCommand');" href="#Documents">Documents</a></li>
        <li><a id="link_DGN" onClick="submitWithAction('MemberMaintenanceTabContentCommand');" href="#DocumentGeneration">Document Generation</a></li>
        <li><a id="link_ND" onClick="submitWithAction('ForwardToMemberNotes');" href="#Notes">Notes</a></li>
        <li><a id="link_AT" onClick="submitWithAction('ForwardToMemberAuditTrail');" href="#MemberCoverAudit">Audit Trail</a></li>
        <li><a id="link_Comm" onClick="submitWithAction('ForwardToMemberCommunicationCommand');" href="#MemberCommunication">Communications</a></li>
    </ol>
</div>
<br/>
<script>setSelectedCCTab('${cctSelectedIndex}');</script>
<br/>


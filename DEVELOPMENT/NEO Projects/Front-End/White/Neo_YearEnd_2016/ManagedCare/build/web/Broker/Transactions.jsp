<%-- 
    Document   : Transactions
    Created on : 2012/06/18, 11:45:32
    Author     : princes
--%>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="MemberApplicationForm">
    <input type="hidden" name="opperation" id="MemberApp_opperation" value="SaveMemberApplicationCommand" />
    <input type="hidden" name="onScreen" id="MemberApp_onScreen" value="Member Application" />

    <agiletags:HiddenField elementName="MemberApp_app_number"/>

    <label class="subheader">Audit Trail</label>
    <hr>
      <table id="BrokerFirmContactTable">
          <tr>
              <td width="160">Details</td>
              <td>
                  <select style="width:215px" name="auditList" id="MemberAppList" onChange="submitFormWithAjaxPost(this.form, 'MemberAppAuditDetails');">
                      <option value="99" selected>&nbsp;</option>
                      <option value="APPLICANT_DETAILS">Applicant Details</option>
                      <option value="ADDRESS_DETAILS">Address Details</option>
                      <option value="BANK_DETAILS">Bank Details</option>
                      <option value="CONTACT_DETAILS">Contact Details</option>
                  </select>
              </td>
                  
          </tr>
      </table>

    
    <hr>
    <table id="MemberAppSaveTable">
        <tr>
           <td><input type="reset" value="Reset"></td>
           <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'MemberApplicationDetails')"></td>
        </tr>
    </table>

  </agiletags:ControllerForm>

<%-- 
    Document   : AuditTrail
    Created on : 11 Mar 2014, 9:43:14 AM
    Author     : almaries
--%>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<div id="tabTableLayout">
  <agiletags:ControllerForm name="MemberCoverAuditForm">
    <input type="hidden" name="opperation" id="PracticeAudit_opperation" value="PracticeAuditTrailCommand" />
    <input type="hidden" name="onScreen" id="PracticeAudit_onScreen" value="PracticeAuditTrail" />
    <input type="hidden" name="provNumEntityId" id="provNumEntityId" value="${sessionScope.provNumEntityId}" />
    
    <label class="header">Practice Audit Trail</label>
    <hr>

      <table id="MemberCoverAuditTable">
          <tr>
              <td width="160" class="label">Details</td>
              <td>
                  <select style="width:215px" name="auditList" id="auditList" onChange="submitFormWithAjaxPost(this.form, 'PracticeAuditDetails');">
                      <option value="99" selected>&nbsp;</option>
                      <option value="PRACTICE_DETAILS">Practice Details</option>
                      <option value="ADDRESS_DETAILS">Address Details</option>
                      <option value="BANK_DETAILS">Bank Details</option>
                      <option value="CONTACT_DETAILS">Contact Details</option>
                      <option value="MEMBER_ALL">All</option>
                  </select>
              </td>
              
          </tr>
      </table>          
 </agiletags:ControllerForm>
</div>
 <div style ="display: none"  id="PracticeAuditDetails"></div>

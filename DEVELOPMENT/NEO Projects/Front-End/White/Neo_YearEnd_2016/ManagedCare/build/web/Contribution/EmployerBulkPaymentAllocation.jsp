<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <style>
            img.center {display: block;   margin-left: auto;   margin-right: auto;}
            input{text-align:right;}
        </style>
        <script>
            function selectReceipt(receipt) {
                var sr = receipt.split("_");
                $('#unallocatedAmount').text(parseFloat(sr[1]).toFixed(2));
                $('#amountUnallocated').val(sr[1]);
            }
            
function exportExcel(){
    var url = "/ManagedCare/AgileController?opperation=BulkAllocFileUploadCommand&command=Export";
    var name = "BulkEmployerPaymentAllocation.csv";

    $.get(url, function (response) {
        //var result = resultTxt.trim();
        
        var a = window.document.createElement('a');

        a.href = window.URL.createObjectURL(new Blob([response], {type: 'text/csv'}));
        a.download = name;

        // Append anchor to body.
        document.body.appendChild(a);
        a.click();

        // Remove anchor from body
        document.body.removeChild(a);

    });
}
            
        </script>
    </head>
    <body>
        <form action="${pageContext.request.contextPath}/AgileController?opperation=BulkAllocFileUploadCommand" method="post"  enctype="multipart/form-data" onsubmit="document.getElementById('uploadBtn').disabled = true;">
        <input type="hidden" name="opperation" id="opperation" value="BulkAllocFileUploadCommand" />
        <input type="hidden" name="entityId" id="entityId" value="${entityId}" />
        <input type="hidden" name="amountUnallocated" id="amountUnallocated" value="${amount}" />
        <input type="hidden" name="groupName" id="groupName" value="${groupName}" />
        <input type="hidden" name="groupNumber" id="groupNumber" value="${groupNumber}" />
    <label class="header">Group Contribution Allocation</label>
    <hr>
    <table>
        <tr>
            <td width="160"><label>Group Number:</label></td>
            <td><label> ${groupNumber}</label></td>
        </tr>
        <tr>
            <td><label>Group Name:</label></td>
            <td><label> ${groupName}</label></td>
        </tr>
        <tr>
            <td><label>Amount To be Allocated:</label></td>
            <td><label id="unallocatedAmount"><b>${agiletags:formatStringAmount(amount)}</b></label></td>
        </tr>
        <tr> 
            <td><label>Receipt</label></td>
            <td>
                <c:if test="${empty receipts}"><label>Not Receipts Available</label></c:if>
                <c:if test="${not empty receipts}">
                    <c:set var="totalReceipts" value="0"></c:set>
                    <c:forEach var="rec" items="${receipts}">
                        <c:set var="totalReceipts" value="${totalReceipts + rec.amount}"></c:set>
                    </c:forEach>
                    <select name="ContribReceiptsId" onchange="selectReceipt(this.value);">
                        <optgroup label="All">
                            <option value="0_${totalReceipts}" selected>All Receipts R ${agiletags:formatBigDecimal(totalReceipts)}</option>
                        </optgroup>
                        <c:forEach var="rec" items="${receipts}">
                            <optgroup label="${agiletags:formatXMLGregorianDate(rec.statementDate)} ${rec.receiptType == 1 ? 'Payment' : 'Unallocated Amount'}">
                            <option value="${rec.contribReceiptsId}_${rec.amount}">${rec.transactionDescription} R ${agiletags:formatBigDecimal(rec.amount)}</option>
                            </optgroup>
                        </c:forEach>
                    </select>
                </c:if>
            </td>
        </tr>
            <c:if test="${empty resultList && not empty receipts}">
        <tr><agiletags:LabelTextBoxDateReq  displayname="Contribution Date" elementName="Contribution_Date" valueFromSession="yes" mandatory="yes"/>        </tr>
        <tr> 
            <td width="160"><label>CSV File</label></td>
            <td>
                <input type="file" name="file2" id="fileSelect2"/>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td align="right"><input id="uploadBtn" type="submit" value="Upload File"></td>
        </tr>
        
        </c:if>
    </table>
        <br>
        <br>
        
     </form>
        <div id="errorList">
            <c:if test="${! empty errMap}">
                <label class="subheader">${importResults}</label>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Line</th>
                        <th>Details</th>
                    </tr>
                <c:forEach var="entry" items="${errMap}">
                    <tr>
                        <td><label>${entry.key}</label></td>
                        <td><label>${entry.value}</label></td>
                    </tr>
                </c:forEach>
                </table>
            </c:if>
        </div>        
        <div id="resultList">
            <c:if test="${! empty resultList}">
                <label class="subheader">Import Results</label>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" id="resultsTable">
                    <tr>
                        <th>Line</th>
                        <th>Member Number</th>
                        <th>ID Number</th>
                        <th>Employee Number</th>
                        <th>Amount</th>
                        <th>Status</th>
                    </tr>
                <c:forEach var="entry" items="${resultList}">
                    <tr>
                        <td><label>${entry["line"]}</label></td>
                        <td><label>${entry["memNum"]}</label></td>
                        <td><label>${entry["idNum"]}</label></td>
                        <td><label>${entry["employeeNumber"]}</label></td>
                        <td><label>${entry["amount"]}</label></td>
                        <td><label>${entry["status"] == "5" ? "Allocated" : "Not Allocated"}</label></td>
                    </tr>
                </c:forEach>
                </table>
                <agiletags:ButtonOpperationLabelError displayName="Export" elementName="export" type="button" javaScript="onClick=\"exportExcel();\""/>
            </c:if>
        </div>        
    </body>
</html>

<%-- 
    Document   : BankStatementList
    Created on : 2012/07/05, 05:06:33
    Author     : yuganp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script>
            function ClearValidation(){
                $("#scheme_error").text("");
                $('#btn_EmpSearch_error').text("");
            }
            
            function validateAndSearchEmployer(frm, btn) {
                document.getElementById('EmployerSearchCommand').value = 'Search';
                
                ClearValidation();
                var scheme = $("#scheme").val();
                var isValid = false;
                
                if(scheme == null || scheme == "" || scheme == "99"){
                    $("#scheme_error").text("Scheme is mandatory");
                }
                else{
                    if ($('#employerNumber').val().length > 0 || $('#employerName').val().length > 0 ) {
                        isValid = true;
                    }else {
                        $('#btn_EmpSearch_error').text("Please enter additional search criteria");
                    }
                }
                
                if (isValid) {
                    submitFormWithAjaxPost(frm, 'SearchResultsDiv', btn);
                } 
            }
            
        </script>
    </head>
    <body>
     <agiletags:ControllerForm name="ReciptsSearchtForm">
        <input type="hidden" name="opperation" id="opperation" value="GroupBillingStatementCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="ReceiptsSearch" />
        <input type="hidden" name="command" id="EmployerSearchCommand" value="" />
        <input type="hidden" name="entityId" id="entityId" value="" />
        <label class="header">Group Billing Statement Search</label>
        <hr>
        <br>
        <table id="EmployerTable">
            <tr><agiletags:LabelProductDropDown displayName="Scheme" elementName="scheme" mandatory="yes"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Group Number" elementName="employerNumber"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Group Name" elementName="employerName"/></tr>
            <tr><agiletags:LabelTextBoxDateReq  displayname="Billing Period" elementName="billingPeriod" valueFromSession="yes" mandatory="yes"/>        </tr>
            <tr>
                <td><label>Format</label></td>
                <td>
                    <select name="fileFormat">
                        <option value="csv">CSV</option>
                        <option value="xls">Excel (XLS Format)</option>
                        <option value="xlsx">Excel (XLSX Format)</option>
                        <option value="pdf">PDF</option>
                        <option value="oldpdf">PDF - Old Format</option>
                    </select>
                </td>
            </tr>
            <tr><td colspan="2" align="right"><input type="button" value="Search" onclick="validateAndSearchEmployer(this.form, this);"></td>
                <td></td>
                <td></td>
                <td><label id="btn_EmpSearch_error" class="error"></label></td>
            </tr>
        </table>

    <div id="SearchResultsDiv"></div>
     </agiletags:ControllerForm>
    </body>
</html>

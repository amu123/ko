<%-- 
    Document   : MemberHIVStatus
    Created on : 2013/12/05, 09:05:09
    Author     : marcelp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" language="JavaScript">
            $(document).ready(function () {
                var hivStatusInd = $('#hivStatusInd').val();
                if (hivStatusInd == "1") {
                    document.getElementById('hivStatus').checked = true;
                }
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });


            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function toggle() {
                if (document.getElementById('hivStatus').checked) {
                    document.getElementById("hivStatusInd").value = 1;
                } else {
                    document.getElementById("hivStatusInd").value = 0;
                }
            }
        </script>
    </head>
    <body>
        <agiletags:PDCTabs tabSelectedIndex="link_CDL" />
        <fieldset id="pageBorder">
            <div class="content_area" >
                <c:choose>
                    <c:when test="${applicationScope.Client == 'Sechaba'}">
                        <label class="header">PCC HIV Status</label>
                    </c:when>    
                    <c:otherwise>
                        <label class="header">PDC HIV Status</label>
                    </c:otherwise>
                </c:choose>
                </br>
                <agiletags:ControllerForm name="saveHIVStatus">
                    <input type="hidden" name="opperation" id="opperation" value="" /> 
                    <input type="hidden" name="hivStatusInd" id="hivStatusInd" value="${memberHIVStatusInd}" /> 
                    <input type="hidden" name="entityId" id="entityId" value="${entityId}" />
                    <agiletags:LabelCheckboxSingle displayName="HIV Status" elementName="hivStatus" value="${memberHIVStatusInd}" javascript="onclick=\"toggle();\""/>
                    </br>
                    <table>
                        <tr>  
                            <td><agiletags:ButtonOpperationLabelError valueFromSession="yes" elementName="SaveMemberCDLStatusCommand"  displayName="Save" commandName="SaveMemberCDLStatusCommand" type="button" javaScript="onClick=\"submitWithAction('SaveMemberCDLStatusCommand')\";" /></td>
                        </tr>
                    </table>
                </agiletags:ControllerForm>
            </div>
        </fieldset>
    </body>

</html>

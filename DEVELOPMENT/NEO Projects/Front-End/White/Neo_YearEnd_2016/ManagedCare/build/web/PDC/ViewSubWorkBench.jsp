<%-- 
    Document   : viewSubWorkBench
    Created on : May 24, 2016, 2:44:09 PM
    Author     : dewaldo
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.scrollTo-1.4.2-min.js"></script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
        <title>PDC Sub-Work Bench</title>      
        <script type="text/javascript" language="JavaScript">
            function submitWithAction(action, depNo, eventID, workListID, covNum) {
                document.getElementById('opperation').value = action;
                document.getElementById('dependentNumber').value = depNo;
                document.getElementById('EVENT_ID').value = eventID;
                document.getElementById('workListID').value = workListID;
                document.getElementById('coverNumber').value = covNum;
                document.forms[0].submit();
            }

            function submitAuthWithAction(action, authNumber, authId) {
                document.getElementById('opperation').value = action;
                document.getElementById('authNumber').value = authNumber;
                document.getElementById('authId').value = authId;
                document.getElementById('returnFlag').value = true;
                document.forms[0].submit();
            }

            function submitClaimLineWithAction(action, claimLineID) {
                document.getElementById('opperation').value = action;
                document.getElementById('memberDetailedClaimLineId').value = claimLineID;
                document.getElementById('returnFlag').value = true;
                document.forms[0].submit();
            }

            function submitBiometricWithAction(command, ID, memberNo) {
                document.getElementById('returnFlag').value = true;
                var data = {
                    'DepCode_text': ID,
                    'memberNo_text': memberNo,
                    'opperation': command,
                    'id': new Date().getTime()
                };

                var url = "/ManagedCare/AgileController";
                $.get(url, data, function (resultTxt) {
                    if (resultTxt !== null) {
                        var resTxtArr = resultTxt.split("|");
                        var result = resTxtArr[0];
                        if (result === "Done") {
                            submitScreenRefresh("/biometrics/BiometricsView.jsp");
                        } else {
                        }
                    }
                });
            }

            function submitScreenRefresh(screen) {
                $("#opperation").val("RefreshCommand");
                $("#refreshScreen").val(screen);
                document.forms[0].submit();
            }

            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }
        </script>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="10px"></td><td align="left">

                    <c:choose>
                        <c:when test="${applicationScope.Client == 'Sechaba'}">
                            <label class="header">PCC - View Work Bench</label>
                        </c:when>    
                        <c:otherwise>
                            <label class="header">PDC - View Work Bench</label>
                        </c:otherwise>
                    </c:choose>
                    <br/><br/>

                    <agiletags:ControllerForm name="viewWorkBench">
                        <!--****** Hidden inputs ******-->
                        <input type="hidden" name="workListID" id="workListID" value="" />
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <input type="hidden" name="refreshScreen" id="refreshScreen" value="" />   
                        <input type="hidden" name="dependentNumber" id="dependentNumber" value="" />
                        <input type="hidden" name="EVENT_ID" id="EVENT_ID" value="" />
                        <input type="hidden" name="authNumber" id="authNumber" value=""/>
                        <input type="hidden" name="authId" id="authId" value=""/>
                        <input type="hidden" name="coverNumber" id="coverNumber" value=""/>
                        <input type="hidden" name="memberDetailedClaimLineId" id="memberDetailedClaimLineId" value=""/>
                        <input type="hidden" name="returnFlag" id="returnFlag" value=""/>
                        <!--**********************-->

                        <!--****** Sub work bench results (Events) *****-->
                        <agiletags:PdcSubWorkBenchTable/>
                        <!--**********************-->

                    </agiletags:ControllerForm>
                    <br/><br/><br/><br/><br/>
                </td></tr></table>

    </body>
</html>
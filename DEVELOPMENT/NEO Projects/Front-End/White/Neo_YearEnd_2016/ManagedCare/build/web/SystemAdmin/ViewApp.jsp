<%-- 
    Document   : ViewApp
    Created on : 2013/06/10, 06:26:57
    Author     : yuganp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script>
            function killApp(processId) {
                $.post('/ManagedCare/AgileController?opperation=RunAppCommand&command=terminateApp&processId=' + processId);
                location.reload();
            }
        </script>
    </head>
    <body>
        <h1>Running Applications</h1>
        <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
            <tr>
                <th>Application</th>
                <th>Action</th>
            </tr>
            <c:forEach items="${processes}" var="items" >
                <tr>
                    <td>${items.value.applicationName}</td>
                    <td><button type="button" onclick="killApp('${items.key}');">Kill</button></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>

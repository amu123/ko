<%-- 
    Document   : ClaimNote_SearchNotesSummary
    Created on : 31 May 2016, 4:26:16 PM
    Author     : janf
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="100%">
    <thead>
        <tr>
            <th>Batch Number</th>
            <th>Claim Number</th>
            <!--<th>Claim Line ID</th>-->
            <th>Practice Number</th>
            <th>Member Number</th>
            <th>Dependant</th>
            <th>Created By</th>
            <th>Created Date</th>
            <th>Notes</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="item" items="${SearchNoteResults}">
            <tr>
                <td><label>${item.batchId}</label></td>
                <td><label>${item.claimId}</label></td>
                <!--<td><label>${item.claimLineId}</label></td>-->
                <td><label>${item.practiceNo}</label></td>
                <td><label>${item.coverNo}</label></td>
                <td><label>${item.dependantCode}</label></td>
                <td><label>${item.userName}</label></td>
                <td><label>${item.creationDate}</label></td>
                <td><label>${item.shortNotes}</label></td>
                <td><input type="button" value="View Note" onclick="viewClaimNote(${item.noteId},true)"></td>
            </tr>
        </c:forEach>
    </tbody>
</table>
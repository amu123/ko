<%-- 
    Document   : WorkflowMailRetrieval
    Created on : Jul 24, 2016, 4:52:39 PM
    Author     : chrisdp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<!DOCTYPE html>
<html style="height:100%; border-right: medium solid #6cc24a;">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/Workflow/WorkflowTabUtil.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script>
            function refresh() {
                var formVal = document.getElementById('WorkflowMailForm');
                var params = encodeFormForSubmit(formVal);
                $(formVal).mask("Retrieving mails...");
                $.post(formVal.action, params,function(result){
                    //alert('Posting' + result);
                    updateFormFromData(result);
                    $(formVal).unmask();
                //    if (document.getElementById('transferRefundStatus').value == 'ok') {
                //        swapDivVisbible('overlay','main_div');
                //    } else {
                //        btn.disabled = false;
                //    }
                });  
            };
        </script>
    </head>
    <body>
        <div align="Center">
            <agiletags:WorkflowTabs wftSelectedIndex="link_WfMailRetrieval" />
                      
            <agiletags:ControllerForm name="WorkflowMailForm" action="/ManagedCare/AgileController">
                <input type="hidden" name="opperation" id="opperation" value="WorkflowContentCommand" />
                <input type="hidden" name="application" value="0" size="30" />
                <input type="hidden" name="onScreen" id="onScreen" value="WorkflowMailRetrieval.jsp" />
                <input type="hidden" name="pullMails" id="pullMails" value="Y" />
                <input type="hidden" name="pullMailsResponse" id="pullMailsResponse" value="" />
                                
                <label class="header">Retrieve Mails</label>
                <br/><br/>
                <table>
                    <tr><td><input type="button" value="Refresh" onclick="refresh()" /></td></tr>
                </table>
                
            </agiletags:ControllerForm>
        </div>
    </body>
</html>

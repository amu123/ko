<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<c:if test="${pageVal == 1}">
    <div id="PageContents_${pageVal}" style="display:${pageVal == 1 ? 'block' : 'none'};">
    </c:if>
    <table id="memberListTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%">
        <tr>
            <th>Member Number</th>
            <th>Surname</th>
            <th>Name</th>
            <th>Option</th>
            <th>Start Date</th>
            <th>End Date</th>
        </tr>
        <c:forEach var="entry" items="${accreList}">

            <tr class="label">
                <td>${entry.coverNumber}</td>
                <td>${entry.surname}</td>
                <td>${entry.name}</td>
                <td>${entry.option}</td>
                <td>${entry.startDate}</td>
                <td>${entry.endDate}</td>                            
            </tr>

        </c:forEach>
    </table>
    <br>
    <table width="90%">
        <tr>
            <td></td>
            <td width="70"><button type="button" class="nav-button" style='visibility:${pageVal == 1 ? "hidden" : ""}' onclick="paging_changePage(forms['MemberForm'],'PageContents', '${pageVal -1}','${pageVal}');"><img src="/ManagedCare/resources/left_grey.png"></button></td>
            <td width="40">Page ${pageVal}</td>
            <td width="70"><button class="nav-button"  type="button" style='visibility:${lastPage == "yes"  ? "hidden" : ""}'  onclick="paging_changePage(forms['MemberForm'],'PageContents', '${pageVal +1}','${pageVal}');"><img src="/ManagedCare/resources/right_grey.png"></button> </td>
            <td></td>
        </tr>
    </table>
</div>

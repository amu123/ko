<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="BrokerFirmAuditForm">
    <input type="hidden" name="opperation" id="consultantAudit_opperation" value="ViewConsultantAuditTrailCommand" />
    <input type="hidden" name="onScreen" id="ConsultantAudit_onScreen" value="ConsultantAudit" />
    <input type="hidden" name="consultantEntityId" id="consultantAuditEntityId" value="${requestScope.consultantEntityId}"
    <input type="hidden" name="brokerTile" id="brokerAuditTile" value="${requestScope.brokerTile}" />
    <label class="header">Consultant Audit Trail</label>
    <hr>

      <table id="EmployerAuditTable">
          <tr>
              <td width="160" style="font-size: 1.0em;">Details</td>
              <td>
                  <select style="width:215px" name="auditList" id="auditList" onChange="submitFormWithAjaxPost(this.form, 'FirmAuditDetails');">
                      <option value="99" selected>&nbsp;</option>
                      <option value="CONSULTANT_DETAILS">Consultant Details</option>
                      <option value="ADDRESS_DETAILS">Address Details</option>
                      <option value="CONTACT_DETAILS">Contact Details</option>
                      <option value="BROKER_CONSULTANT_ACCREDITATION_DETAILS">Accreditation Details</option>
                      <option value="BROKER_CONSULTANT_HISTORY">History Details</option>                      
                  </select>
              </td>
                  
          </tr>
      </table>          
 </agiletags:ControllerForm>
 <div style ="display: none"  id="FirmAuditDetails"></div>
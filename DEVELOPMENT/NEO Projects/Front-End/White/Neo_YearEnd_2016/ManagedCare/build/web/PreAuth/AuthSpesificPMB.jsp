<%-- 
    Document   : AuthSpesificPMB
    Created on : 2012/09/03, 07:52:38
    Author     : johanl
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>  
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthScreens/SubmitRequests.js"></script>
        <script type="text/javascript" language="JavaScript">
            
            function setCheck(check){
                document.getElementById("checked").value = check;
            }
            
            function setPMBCode(){
                $("#opperation").val("SetSelectedPMBCode");

                var indexSelections = new String;
                
                $('#pmbSelection input[id="boxes"]:checked').each(function(){
                    setCheck("true");
                    var parentRow = $(this).closest("tr").attr("id");
                    parentRow = parentRow.replace("pmbRow", "");
                    indexSelections = indexSelections + parentRow + "|";
                });
                
                if(indexSelections !== null && indexSelections !== ""){
                    indexSelections = indexSelections.substr(0, indexSelections.length-1);
                    //check count
                    document.getElementById('selectedValues').value = indexSelections;
                    document.forms[0].submit(); 
                }else{
                    document.forms[0].submit();
                }                
            }
            
            function submitWithAction(action) {
                $("#opperation").val(action);
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top" ><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <div align="left"><label class="header">Authorisation Specific PMB Details</label></div>
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="selectPMB" validate="yes">
                            <input type="hidden" name="opperation" id="opperation" value="" /> 
                            <input type="hidden" name="selectedValues" id="selectedValues" value="" /> 
                            <input type="hidden" name="checked" id="checked" value="" /> 
                        </agiletags:ControllerForm>
                        <br/>
                        <tr><agiletags:AuthPMBListTag commandName="" javaScript="" /></tr>
                        <tr><agiletags:ButtonOpperation align="right" commandName="" displayname="Override PMB" span="2" type="button" javaScript="onClick=\"submitWithAction('OverridePMB');\"" /></tr>
                        <tr><agiletags:ButtonOpperation align="right" commandName="" displayname="Continue" span="2" type="button" javaScript="onClick=\"setPMBCode();\"" /></tr>
                        <tr><td><label id="savePMB_error" class="error"></label></td></tr>
                    </table>
                </td></tr></table>
    </body>
</html>

<%-- 
    Document   : Workbench
    Created on : 2016/07/06, 11:21:49
    Author     : gerritr
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript">
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <form id="workflow" name="workflow" action="/ManagedCare/AgileController">
            <input type="hidden" name="opperation" value="WorkflowContentCommand" size="30" />
            <input type="hidden" name="application" value="0" size="30" />
            <input type="hidden" name="onScreen" value="Workflow/WorkflowMyWorkbench.jsp" size="30" />
        </form>

    </body>
</html>

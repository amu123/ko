<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<agiletags:ControllerForm name="EmployerGroupForm">
    <input type="hidden" name="opperation" id="EmployerGroup_opperation" value="SaveEmployerDetailsCommand" />
    <input type="hidden" name="onScreen" id="EmployerGroup_onScreen" value="EmployerDetails" />
    <input type="hidden" name="EmployerGroup_entityId" id="EmployerGroup_entityId" value="${EmployerGroup_entityId}" />
    <input type="hidden" name="EmployerGroup_parentEntityId" id="EmployerGroup_parentEntityId" value="${EmployerGroup_parentEntityId}" />
    <input type="hidden" name="buttonPressed" id="employerValuesButtonPressed" value="" />

    <label class="subheader">Group Details</label>
    <hr>
    <table id="EmployerGroupTable">
        <tr id="EmployerGroup_productRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Scheme"  elementName="EmployerGroup_product" lookupId="product" mandatory="yes" errorValueFromSession="no" firstIndexSelected="yes" />        </tr>
        <!--<tr id="EmployerGroup_administratorRow"><agiletags:LabelTextBoxErrorReq displayName="Administrator" elementName="EmployerGroup_administrator" valueFromRequest="EmployerGroup_administrator" mandatory="yes"/>        </tr>-->
        <tr><agiletags:LabelTextSearchWithNoText displayName="Administrator" elementName="EmployerGroup_administrator" mandatory="yes" onClick="document.getElementById('employerValuesButtonPressed').value = 'changeAdmin'; getPageContents(document.forms['EmployerGroupForm'],null,'main_div','overlay');"/>        </tr>
        <tr id="EmployerGroup_employerGroupTypeRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Group Type"  elementName="EmployerGroup_employerGroupType" lookupId="181" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerGroup_employerNumberRow"><agiletags:LabelTextBoxErrorReq displayName="Group Number" elementName="EmployerGroup_employerNumber" valueFromRequest="EmployerGroup_employerNumber" mandatory="yes"/>        </tr>
        <tr id="EmployerGroup_employerNameRow"><agiletags:LabelTextBoxErrorReq displayName="Group Name" elementName="EmployerGroup_employerName" valueFromRequest="EmployerGroup_employerName" mandatory="yes"/>        </tr>
        <tr id="EmployerGroup_tradingNameRow"><agiletags:LabelTextBoxErrorReq displayName="Trading Name" elementName="EmployerGroup_tradingName" valueFromRequest="EmployerGroup_employerName" mandatory="no"/>        </tr>
        <tr id="EmployerGroup_companyRegistrationNoRow"><agiletags:LabelTextBoxErrorReq displayName="Group Registration No" elementName="EmployerGroup_companyRegistrationNo" valueFromRequest="EmployerGroup_companyRegistrationNo" mandatory="no"/>        </tr>
        <tr id="EmployerGroup_vatRegistrationNoRow"><agiletags:LabelTextBoxErrorReq displayName="Vat No" elementName="EmployerGroup_vatRegistrationNo" valueFromRequest="EmployerGroup_vatRegistrationNo" mandatory="no"/>        </tr>
        <tr id="EmployerGroup_natureOfBusinessIdRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Nature Of Business"  elementName="EmployerGroup_natureOfBusinessId" lookupId="180" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerGroup_applicationReceiveDateRow"><agiletags:LabelTextBoxDateReq  displayname="Application Receive Date" elementName="EmployerGroup_ApplicationReceiveDate" valueFromSession="yes" mandatory="yes"/>        </tr>
        <tr id="EmployerGroup_certifiedDateRow"><agiletags:LabelTextBoxDateReq  displayname="Signed Date" elementName="EmployerGroup_certifiedDate" valueFromSession="yes" mandatory="yes"/>        </tr>
        <tr id="EmployerGroup_startDateRow"><agiletags:LabelTextBoxDateReq  displayname="Inception Date" elementName="EmployerGroup_startDate" valueFromSession="yes" mandatory="yes"/>        </tr>
    </table>

    <br>
    <label class="subheader">Contact Details</label>
    <hr>
    <table id="EmployerGroupContactTable">
        <tr id="EmployerGroup_ContactDetails_titleRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Title"  elementName="EmployerGroup_ContactDetails_title" lookupId="24" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerGroup_ContactDetails_contactPersonRow"><agiletags:LabelTextBoxErrorReq displayName="Contact Person" elementName="EmployerGroup_ContactDetails_contactPerson" valueFromRequest="EmployerGroup_ContactDetails_contactPerson" mandatory="no"/>        </tr>
        <tr id="EmployerGroup_ContactDetails_telNoRow"><agiletags:LabelTextBoxErrorReq displayName="Telephone Number" elementName="EmployerGroup_ContactDetails_telNo" valueFromRequest="EmployerGroup_ContactDetails_telNo" mandatory="EmployerGroup_ContactDetails_telNo"/>        </tr>
        <tr id="EmployerGroup_ContactDetails_faxNoRow"><agiletags:LabelTextBoxErrorReq displayName="Fax No" elementName="EmployerGroup_ContactDetails_faxNo" valueFromRequest="EmployerGroup_ContactDetails_faxNo" mandatory="no"/>        </tr>
        <tr id="EmployerGroup_ContactDetails_emailRow"><agiletags:LabelTextBoxErrorReq displayName="Email Address" elementName="EmployerGroup_ContactDetails_email" valueFromRequest="EmployerGroup_ContactDetails_email" mandatory="no"/>        </tr>
        <tr id="EmployerGroup_ContactDetails_altEmailRow"><agiletags:LabelTextBoxErrorReq displayName="Alternative Email Address" elementName="EmployerGroup_ContactDetails_altEmail" valueFromRequest="EmployerGroup_ContactDetails_altEmail" mandatory="no"/>        </tr>
    </table>

    <br>
    <label class="subheader">Postal Address</label>
    <hr>
    <table id="EmployerGroupPostalTable">
        <tr id="EmployerGroup_PostalAddress_addressLine1Row"><agiletags:LabelTextBoxErrorReq displayName="Line 1" elementName="EmployerGroup_PostalAddress_addressLine1" valueFromRequest="EmployerGroup_PostalAddress_addressLine1" mandatory="no"/>        </tr>
        <tr id="EmployerGroup_PostalAddress_addressLine2Row"><agiletags:LabelTextBoxErrorReq displayName="Line 2" elementName="EmployerGroup_PostalAddress_addressLine2" valueFromRequest="EmployerGroup_PostalAddress_addressLine2" mandatory="no"/>        </tr>
        <tr id="EmployerGroup_PostalAddress_addressLine3Row"><agiletags:LabelTextBoxErrorReq displayName="Line 3" elementName="EmployerGroup_PostalAddress_addressLine3" valueFromRequest="EmployerGroup_PostalAddress_addressLine3" mandatory="no"/>        </tr>
        <tr id="EmployerGroup_PostalAddress_postalCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Code" elementName="EmployerGroup_PostalAddress_postalCode" valueFromRequest="EmployerGroup_PostalAddress_postalCode" mandatory="no"/>        </tr>
    </table>

    <br>
    <label class="subheader">Physical Address</label>
    <hr>
    <table id="EmployerGroupPhysicalTable">
        <tr id="EmployerGroup_ResAddress_addressLine1Row"><agiletags:LabelTextBoxErrorReq displayName="Line 1" elementName="EmployerGroup_ResAddress_addressLine1" valueFromRequest="EmployerGroup_ResAddress_addressLine1" mandatory="no"/>        </tr>
        <tr id="EmployerGroup_ResAddress_addressLine2Row"><agiletags:LabelTextBoxErrorReq displayName="Line 2" elementName="EmployerGroup_ResAddress_addressLine2" valueFromRequest="EmployerGroup_ResAddress_addressLine2" mandatory="no"/>        </tr>
        <tr id="EmployerGroup_ResAddress_addressLine3Row"><agiletags:LabelTextBoxErrorReq displayName="Line 3" elementName="EmployerGroup_ResAddress_addressLine3" valueFromRequest="EmployerGroup_ResAddress_addressLine3" mandatory="no"/>        </tr>
        <tr id="EmployerGroup_ResAddress_postalCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Code" elementName="EmployerGroup_ResAddress_postalCode" valueFromRequest="EmployerGroup_ResAddress_postalCode" mandatory="no"/>        </tr>
    </table>

    <hr>
    <table id="EmployerGroupSaveTable">
        <tr>
            <td><input type="reset" value="Reset"></td>
            <td>
                <c:if test="${(EmployerGroup_entityId ne null) and (EmployerGroup_entityId ne '') }">
                        <input type="button" value="Save" onclick="document.getElementById('employerValuesButtonPressed').value = 'update_button';submitFormWithAjaxPost(this.form, 'EmployerDetails', this)">
                </c:if>
                <c:if test="${(EmployerGroup_entityId eq null) or (EmployerGroup_entityId eq '') }">
                        <input type="button" value="Save" onclick="document.getElementById('employerValuesButtonPressed').value = '';submitFormWithAjaxPost(this.form, 'EmployerDetails', this)">
                </c:if>
            </td>
        </tr>
    </table>

</agiletags:ControllerForm>
<form

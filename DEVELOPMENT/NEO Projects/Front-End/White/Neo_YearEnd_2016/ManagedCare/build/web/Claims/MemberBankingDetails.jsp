<%-- 
    Document   : MemberBankingDetails
    Created on : 2011/08/19, 02:05:39
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script>
            
            $(function(){
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });                            
            });
            
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">

                    <agiletags:CallCenterTabs cctSelectedIndex="link_BD" />
                    
                    <fieldset id="pageBorder">
                        <div class="content_area" >

                            <label class="header">Reimbursement Banking Details</label>
                            <br/><br/>

                            <agiletags:ControllerForm name="practiceDetails">
                                <table>
                                    <input type="hidden" name="opperation" id="opperation" value="" />
                                    <agiletags:HiddenField elementName="searchCalled" />
                                    <input type="hidden" name="onScreen" id="onScreen" value="" />
                                    <input type="hidden" name="listIndex" id="listIndex" value=""/>

                                    <tr><agiletags:LabelTextBoxError displayName="Policy Holder Number" elementName="policyHolderNumber"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="First Name" elementName="memberName"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="memberSurname"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Bank Name" elementName="bankName"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Branch Code" elementName="branchCode"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Branch Name" elementName="branchName"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Account Number" elementName="accountNumber"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Account Name" elementName="accountName"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Account Type" elementName="accountType"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Effective Date" elementName="effectiveBankDate" valueFromSession="Yes" readonly="yes"/></tr>
                                </table>
                                <br/>
                                <label class="header">Contribution Collection Details</label>
                                <table>
                                    <tr><agiletags:LabelTextBoxError displayName="Bank Name" elementName="bankNameCon"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Branch Code" elementName="branchCodeCon"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Branch Name" elementName="branchNameCon"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Account Number" elementName="accountNumberCon"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Account Name" elementName="accountNameCon"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Account Type" elementName="accountTypeCon"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Effective Date" elementName="effectiveBankDateCon" valueFromSession="Yes" readonly="yes"/></tr>
                                </table>

                            </agiletags:ControllerForm>
                        </div>
                    </fieldset>
                </td></tr></table>
    </body>
</html>

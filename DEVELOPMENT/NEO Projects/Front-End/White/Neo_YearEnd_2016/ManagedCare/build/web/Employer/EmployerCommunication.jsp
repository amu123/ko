<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="EmployerCommunicationForm">
    <input type="hidden" name="opperation" id="EmployerCommunication_opperation" value="SaveEmployerCommunicationCommand" />
    <input type="hidden" name="onScreen" id="EmployerCommunication_onScreen" value="" />
    <input type="hidden" name="EmployerGroup_entityId" id="EmployerGroup_Communication_entityId" value="${EmployerGroup_entityId}" />
    <label class="header">Communication</label>
    <hr>

      <table id="EmployerCommunicationTable">
        <tr id="EmployerCommunication_memberCardsRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Membership Cards"  elementName="EmployerCommunication_memberCards" lookupId="186" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerCommunication_communicateDirectlyRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Communicate Directly with Members"  elementName="EmployerCommunication_communicateDirectly" lookupId="67" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerCommunication_communicationMethodRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Communication Method"  elementName="EmployerCommunication_communicationMethod" lookupId="187" mandatory="yes" errorValueFromSession="yes"/>        </tr>
        <tr id="EmployerCommunication_communicationOtherRow"><agiletags:LabelTextBoxErrorReq displayName="Other Details" elementName="EmployerCommunication_communicationOther" valueFromRequest="EmployerCommunication_communicationOther" mandatory="no"/>        </tr>
        <tr id="EmployerCommunication_contactPersonRow"><agiletags:LabelTextBoxErrorReq displayName="Contact Person" elementName="EmployerCommunication_contactPerson" valueFromRequest="EmployerCommunication_contactPerson" mandatory="no"/>        </tr>
        <tr id="EmployerCommunication_contactTelRow"><agiletags:LabelTextBoxErrorReq displayName="Contact no." elementName="EmployerCommunication_contactNo" valueFromRequest="EmployerCommunication_contactNo" mandatory="no"/>        </tr>
        <tr id="EmployerCommunication_contactEmailRow"><agiletags:LabelTextBoxErrorReq displayName="Contact Email" elementName="EmployerCommunication_contactEmail" valueFromRequest="EmployerCommunication_contactEmail" mandatory="no"/>        </tr>
      </table>

    <hr>
    <table id="EmployerGroupSaveTable">
        <tr>
           <td><input type="reset" value="Reset"></td>
           <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'EmployerDetails', this)"></td>
        </tr>
    </table>
  </agiletags:ControllerForm>

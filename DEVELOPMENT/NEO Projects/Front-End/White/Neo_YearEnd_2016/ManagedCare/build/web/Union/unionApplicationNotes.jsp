<%-- 
    Document   : unionApplicationNotes
    Created on : Jun 9, 2016, 2:39:19 PM
    Author     : shimanem
--%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <script type='text/javascript' src="/ManagedCare/resources/AgileTabs.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" language="JavaScript">

            $(document).ready(function () {
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <agiletags:UnionTabs tabSelectedIndex="link_union_NT" />
        <div id="main_div">  
            <agiletags:ControllerForm name="unionNotesForm">

                <input type="hidden" name="opperation" id="opperation" value="UnionNotesCommand"/>
                <input type="hidden" name="onScreen" id="onScreen" value="unionApplicationNotes" />
                <input type="hidden" name="unionID" id="union_id" value="${sessionScope.unionID}" />
                <input type="hidden" name="noteId" id="union_note_id" value="" />
                <input type="hidden" name="repID" id="rep_id" value="${sessionScope.representativeNumber}" />
                <input type="hidden" name="noteGroupType" id="note_group_type" value="" />
                <input type="hidden" name="buttonPressed" id="unionNotes_button_pressed" value="" />

                <fieldset id="pageBorder">
                    <div class="header">
                        <table width="100%" bgcolor="#dddddd"  style=" border: 1px solid #c9c3ba;">
                            <tr>
                                <td class="label" width="200"><span><label><b>Union Name :</b></label> <label id="unionName" > ${sessionScope.unionName}</label></span></td>
                                <td class="label" width="200"><b>Union Number :</b> ${sessionScope.unionNumber}</td>
                                <td class="label" width="200"><b>Alternative Union Number :</b> ${sessionScope.unionNumber}</td>
                            </tr>
                        </table>
                    </div><br/>

                    <!-- content goes here -->
                    <label class="subheader">Union Application Notes</label>
                    <hr>
                    <br/>
                    <table>
                        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Details"  elementName="noteTypeSelectID" lookupId="354" mandatory="yes" firstIndexSelected="no" javaScript="onchange=\"document.getElementById('unionNotes_button_pressed').value='SelectButton';submitFormWithAjaxPost(this.form, 'UnionNotesDetails');\""/></tr>
                    </table>
                    <div style ="display: none"  id="UnionNotesDetails"></div>

                </fieldset>
            </div>           
        </agiletags:ControllerForm>
        <div id="overlay" style="display:none;"></div>

    </body>
</html>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <agiletags:ControllerForm name="GroupSplitForm">
        <input type="hidden" name="opperation" id="opperation" value="ContributionBankStatementCommand" />
        <input type="hidden" name="command" value="GroupSplit" />
        <input type="hidden" name="bsStatementId" value="${bsStatementId}" />
        <input type="hidden" name="bsLineId" value="${bsLineId}" />
        <input type="hidden" name="bsLineAmount" value="${bsLineAmount}" />
        <input type="hidden" name="target_div" value="${target_div}" />
        <input type="hidden" name="main_div" value="${main_div}" />
        <input type="hidden" name="addGroupNumber" value="" />
        <input type="hidden" name="addGroupName" value="" />
        <input type="hidden" name="addGroupId" value="" />

        <label class="header">Split Group Payment</label>
        <hr>
    <table>
        <tr>
            <td width="160"><label>Date:</label></td>
            <td><label>${statementDate}</label></td>
        </tr>
        <tr>
            <td><label>Description:</label></td>
            <td><label>${bsLineDescription}</label></td>
        </tr>
        <tr>
            <td><label>Amount To be Split:</label></td>
            <td align="right"><label id="unallocatedAmount"><b>${agiletags:formatStringAmount(bsLineAmount)}</b></label></td>
        </tr>
        <tr>
            <td><label>Split Amount:</label></td>
            <td align="right"><label id="allocatedAmount"><b>0.00</b></label></td>
        </tr>
        <tr>
            <td><label>Remaining Amount:</label></td>
            <td align="right"><label id="remainingAmount"><b>${agiletags:formatStringAmount(bsLineAmount)}</b></label></td>
        </tr>
    </table>
        <br>
        <button type="button" onclick="swapDivVisbible('${target_div}','${main_div}');">Cancel</button>
        <button type="button" onclick="searchSplitGroup(${bsStatementId},${bsLineId} );">Add Group</button>
        <div style="display: block" id="GroupSplitResults"> 
        </div>
        <input type="button" value="Done" onclick="updateLineFromSplit(this.form, this, ${bsStatementId}, ${bsLineId}, 'ES')">
    </agiletags:ControllerForm>

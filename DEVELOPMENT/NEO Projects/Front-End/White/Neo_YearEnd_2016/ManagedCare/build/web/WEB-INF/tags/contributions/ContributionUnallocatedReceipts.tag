<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@attribute name="items" required="true" type="java.util.Collection" %>
<%@attribute name="showHeader" required="false" type="java.lang.Boolean" %>

<c:if test="${!empty items}">
    <c:if test="${showHeader != false}">
        <label class="subheader">Unallocated Receipts</label>
    </c:if>
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th>Type</th>
            <th>Date</th>
            <th>Description</th>
            <th>Amount</th>
        </tr>
        <c:forEach var="rec" items="${items}">
            <tr>
                <td><label>${rec.receiptType == 1 ? 'Payment' : 'Unallocated Amount'}</label></td>
                <td><label>${agiletags:formatXMLGregorianDate(rec.statementDate)}</label></td>
                <td><label>${rec.transactionDescription}</label></td>
                <td align="right"><label>${agiletags:formatBigDecimal(rec.amount)}</label></td>
            </tr>
        </c:forEach>
    </table>
    <br>
</c:if>

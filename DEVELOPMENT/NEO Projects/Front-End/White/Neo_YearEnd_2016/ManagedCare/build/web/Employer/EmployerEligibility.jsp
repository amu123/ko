<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
  <agiletags:ControllerForm name="EmployerEligibilityForm">
    <input type="hidden" name="opperation" id="EmployerEligibility_opperation" value="SaveEmployerEligibilityCommand" />
    <input type="hidden" name="onScreen" id="EmployerEligibility_onScreen" value="" />
    <input type="hidden" name="EmployerGroup_entityId" id="EmployerGroup_Eligibility_entityId" value="${EmployerGroup_entityId}" />

    <label class="header">Employer Eligibility Details</label>
    <hr>

      <table id="EmployerEligibilityTable">
        <tr id="EmployerEligibility_productOptionsIdRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Scheme"  elementName="EmployerEligibility_productOptionsId" lookupId="product" mandatory="no" errorValueFromSession="yes" firstIndexSelected="yes"/>        </tr>
        <tr id="EmployerEligibility_availableToAllIndRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Available To All?"  elementName="EmployerEligibility_availableToAllInd" lookupId="67" mandatory="no"/>        </tr>
        <tr id="EmployerEligibility_totalStaffCountRow"><agiletags:LabelTextBoxErrorReq displayName="Total Staff Count" elementName="EmployerEligibility_totalStaffCount" valueFromRequest="EmployerEligibility_totalStaffCount" mandatory="no"/>        </tr>
        <tr id="EmployerEligibility_totalStaffEligibleRow"><agiletags:LabelTextBoxErrorReq displayName="Total Staff Eligible" elementName="EmployerEligibility_totalStaffEligible" valueFromRequest="EmployerEligibility_totalStaffEligible" mandatory="no"/>        </tr>
        <tr id="EmployerEligibility_totalStaffParticipatingRow"><agiletags:LabelTextBoxErrorReq displayName="Total Staff Participating" elementName="EmployerEligibility_totalStaffParticipating" valueFromRequest="EmployerEligibility_totalStaffParticipating" mandatory="no"/>        </tr>
        <tr id="EmployerEligibility_totalPensionersRow"><agiletags:LabelTextBoxErrorReq displayName="Total Pensioners" elementName="EmployerEligibility_totalPensioners" valueFromRequest="EmployerEligibility_totalPensioners" mandatory="no"/>        </tr>
        <tr id="EmployerEligibility_totalPensionersEligibleRow"><agiletags:LabelTextBoxErrorReq displayName="Total Pensioners Eligible" elementName="EmployerEligibility_totalPensionersEligible" valueFromRequest="EmployerEligibility_totalPensionersEligible" mandatory="no"/>        </tr>
        <tr id="EmployerEligibility_totalPensionersParticipatingRow"><agiletags:LabelTextBoxErrorReq displayName="Total Pensioners Participating" elementName="EmployerEligibility_totalPensionersParticipating" valueFromRequest="EmployerEligibility_totalPensionersParticipating" mandatory="no"/>        </tr>
        <tr id="EmployerEligibility_hrCorrospondenceIndRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="HR Corrospondence"  elementName="EmployerEligibility_hrCorrospondenceInd" lookupId="206" mandatory="no"/>        </tr>
        <tr id="EmployerEligibility_ageLimitRow"><agiletags:LabelTextBoxErrorReq displayName="Average Age Limit" elementName="EmployerEligibility_ageLimit" valueFromRequest="EmployerEligibility_ageLimit" mandatory="no"/>        </tr>
      </table>

    <br>
    <label class="subheader">Product Options</label>
    <hr>
      <table id="EmployerOptionTable">
          <c:forEach var="entry" items="${ProductOptions}">
            <tr id="EmployerEligibility_option_${entry.optionId}_Row"><agiletags:LabelTextBoxErrorReq displayName="${entry.optionName}" elementName="EmployerEligibility_option_${entry.optionName}_${entry.optionId}" valueFromRequest="EmployerEligibility_option_${entry.optionId}" mandatory="no"/>        </tr>
          </c:forEach>
      </table>

    <br>
    <label class="subheader">Existing Scheme Details</label>
    <hr>
      <table id="EmployerSchemeTable">
        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Name of Scheme"  elementName="EmployerEligibility_Scheme_id1" lookupId="193" mandatory="no" errorValueFromSession="yes"/>        </tr>  
        <tr id="EmployerEligibility_Scheme_schemeName1Row"><agiletags:LabelTextBoxErrorReq displayName="Other Scheme" elementName="EmployerEligibility_Scheme_schemeName1" valueFromRequest="EmployerEligibility_Scheme_schemeName1" mandatory="no"/>        </tr>
        <tr id="EmployerEligibility_Scheme_schemeStartDate1Row"><agiletags:LabelTextBoxDateReq  displayname="From" elementName="EmployerEligibility_Scheme_startDate1" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="EmployerEligibility_Scheme_schemeEndDate1Row"><agiletags:LabelTextBoxDateReq  displayname="To" elementName="EmployerEligibility_Scheme_endDate1" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr><td colspan="5"><hr></td></tr>

        <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Name of Scheme"  elementName="EmployerEligibility_Scheme_id2" lookupId="193" mandatory="no" errorValueFromSession="yes"/>        </tr>  
        <tr id="EmployerEligibility_Scheme_schemeName2Row"><agiletags:LabelTextBoxErrorReq displayName="Other Scheme" elementName="EmployerEligibility_Scheme_schemeName2" valueFromRequest="EmployerEligibility_Scheme_schemeName2" mandatory="no"/>        </tr>
        <tr id="EmployerEligibility_Scheme_schemeStartDate2Row"><agiletags:LabelTextBoxDateReq  displayname="From" elementName="EmployerEligibility_Scheme_startDate2" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr id="EmployerEligibility_Scheme_schemeEndDate2Row"><agiletags:LabelTextBoxDateReq  displayname="To" elementName="EmployerEligibility_Scheme_endDate2" valueFromSession="yes" mandatory="no"/>        </tr>
        <tr><td colspan="5"><hr></td></tr>

        <tr id="EmployerEligibility_Scheme_declinedRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Declined"  elementName="EmployerEligibility_Scheme_declined" lookupId="67" mandatory="yes" />        </tr>
        <tr id="EmployerEligibility_Scheme_declinedDetailsRow"><agiletags:LabelTextAreaError displayName="Declined Details"  elementName="EmployerEligibility_Scheme_declinedDetails" mandatory="no" />        </tr>

        <tr id="EmployerEligibility_Scheme_loadedRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Loaded"  elementName="EmployerEligibility_Scheme_loaded" lookupId="67" mandatory="yes" />        </tr>
        <tr id="EmployerEligibility_Scheme_loadedDetailsRow"><agiletags:LabelTextAreaError displayName="LoadedDetails"  elementName="EmployerEligibility_Scheme_loadedDetails" mandatory="no" />        </tr>

        <tr id="EmployerEligibility_Scheme_exclusionRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Exclusion"  elementName="EmployerEligibility_Scheme_exclusion" lookupId="67" mandatory="yes" />        </tr>
        <tr id="EmployerEligibility_Scheme_exclusionDetailsRow"><agiletags:LabelTextAreaError displayName="Exclusion Details"  elementName="EmployerEligibility_Scheme_exclusionDetails" mandatory="no" />        </tr>
      </table>

    <hr>
    <table id="EmployerEligibilitySaveTable">
        <tr>
           <td><input type="reset" value="Reset"></td>
           <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'EmployerEligibility', this)"></td>
        </tr>
    </table>
 </agiletags:ControllerForm>

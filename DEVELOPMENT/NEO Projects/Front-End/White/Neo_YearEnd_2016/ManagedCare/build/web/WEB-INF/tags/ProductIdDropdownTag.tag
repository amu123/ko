<%-- 
    Document   : ProductIdDropdownTag
    Created on : 26 Mar 2015, 8:26:28 AM
    Author     : gerritr
--%>

<%@tag description="" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ attribute name="javascript" rtexprvalue="true" required="true" %>
<%-- The list of normal or fragment attributes can be specified here: --%>
<td class="label" align="left" width="160px">Product Type Selection:</td>
<td align="left" width="200px">
    <select style="width:215px" id="pIDTypeSelect" name="pIDTypeSelect" ${javascript}>
        <c:if test="${applicationScope.Client == 'Sechaba'}">
            <c:if test="${sessionScope.ProductIDTypeSelected != null && sessionScope.ProductIDTypeSelected eq '3'}">
                <option value="3">Sizwe</option>
            </c:if>
            <c:if test="${sessionScope.ProductIDTypeSelected == null || sessionScope.ProductIDTypeSelected eq 'null'}">
                <option value="0">&nbsp;</option>
                <option value="3">Sizwe</option>
            </c:if>
        </c:if>

        <c:if test="${applicationScope.Client == 'Agility'}">
            <c:if test="${!empty sessionScope.ProductIDTypeSelected}">
                <c:if test="${sessionScope.ProductIDTypeSelected != null && sessionScope.ProductIDTypeSelected eq 'null'}">
                    <c:if test="${sessionScope.productDescription != null && sessionScope.productDescription eq 'Resolution Health'}">
                        <option value="0">&nbsp;</option>
                        <option value="1">Resolution Health</option>
                    </c:if>
                    <c:if test="${sessionScope.productDescription != null && sessionScope.productDescription eq 'Spectramed'}">
                        <option value="0">&nbsp;</option>
                        <option value="2">Spectramed</option>
                    </c:if>
                    <c:if test="${sessionScope.productDescription == null || sessionScope.productDescription eq 'null'}">
                        <option value="0">&nbsp;</option>
                        <option value="1">Resolution Health</option>
                        <option value="2">Spectramed</option>
                    </c:if>
                </c:if>
                <c:if test="${sessionScope.ProductIDTypeSelected != null && sessionScope.ProductIDTypeSelected eq '1'}">
                    <option value="1">Resolution Health</option>
                </c:if>
                <c:if test="${sessionScope.ProductIDTypeSelected != null && sessionScope.ProductIDTypeSelected eq '2'}">
                    <option value="2">Spectramed</option>
                </c:if>
            </c:if>
            <c:if test="${empty sessionScope.ProductIDTypeSelected}">
                <option value="0">No Product Types available</option>
            </c:if>
        </c:if>
    </select>
</td>
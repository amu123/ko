<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Group Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty GroupSearchResults}">
                <span class="label">No Group found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Group Number</th>
                        <th>Group Name</th>
                        <th>Termination Date</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${GroupSearchResults}">
                        <tr>
                            <td><label>${entry.employerNumber}</label></td>
                            <td><label>${entry.employerName}</label></td>
                            <td><label>${entry.endDate == null ? "&nbsp;" : entry.endDate}</label></td>
                            <td><c:choose>
                                    <c:when test="${entry.endDate == null}">
                                        <input type="button" value="Select" onclick="setElemValues({'${diagCodeId}':'${entry.employerNumber}','${diagNameId}':'${agiletags:escapeStr(entry.employerName)}','groupEntityId':'${entry.entityId}','${groupType}':'${entry.groupType}'});updateFormFromData(' ','${target_div}'); swapDivVisbible('${target_div}','${main_div}');"/>
                                    </c:when>
                                    <c:otherwise>
                                        <input type="button" value="Select" disabled="disabled"/>
                                    </c:otherwise>
                                </c:choose></td> 
                        </tr>
                    </c:forEach>

                </table>
            </c:otherwise>
        </c:choose>
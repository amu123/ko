<%-- 
    Document   : MemberDocument
    Created on : 02 Jul 2012, 3:34:17 PM
    Author     : Christo
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<%
    String errorMsg = (String)request.getAttribute("errorMsg");
%>
<div>
                        <% if(errorMsg != null){ %>
                        <label id="error" class="error"><%=errorMsg%></label><br>
                        <% } %>

                        <form name="communicationLog" id="communicationLog" action="/ManagedCare/AgileController" method="POST">
                            <input type="hidden" name="opperation" id="opperation" value="MemberCommunicationLogCommand" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <nt:NeoPanel title="Claims, Suspension, Un-Suspension and Debit Order Communications"  collapsed="false" reload="false">
                                <agiletags:MemberCommunicationList command="" />
                            </nt:NeoPanel>
                        </form> 
                        <nt:NeoPanel title="Other Communications"  collapsed="false" reload="false">
                            <nt:CommunicationsLog items="${communicationsLog}" showHeader="false"/>
                        </nt:NeoPanel>
                        <c:if test="${applicationScope.Client == 'Sechaba'}">
                            <nt:NeoPanel title="CRM Communication" command="WorkflowCommand" action="memberCRMByEntity" actionId="${entityId}" reload="true" />
                        </c:if>
</div>


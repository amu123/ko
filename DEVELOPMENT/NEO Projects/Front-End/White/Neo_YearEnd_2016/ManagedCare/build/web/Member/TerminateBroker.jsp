<%-- 
    Document   : TerminateBroker
    Created on : Jul 5, 2016, 10:30:27 AM
    Author     : charlh
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script language="JavaScript">
            function clickTermButton() {
                document.getElementById('brokerAndConsultantTermButtonPressed').value = 'terminateBroker';
                submitFormWithAjaxPost(document.forms['TerminateBrokerForm'], 'MemberCover', null,'${main_div}', '${target_div}');
            }
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
            <label class="header">Terminate Broker or Consultant</label>
            <br/><br/>
            <agiletags:ControllerForm name="TerminateBrokerForm">
                <input type="hidden" name="opperation" id="opperation" value="SaveMemberCoverDetailsCommand" />
                <input type="hidden" name="onScreen" id="onScreen" value="TerminateMemberOrConsultant"/>
                <input type="hidden" name="buttonPressed" id="brokerAndConsultantTermButtonPressed" value=""/>
                <input type="hidden" name="memberEntityId" id="memberLinkBrokerEntityId" value="${memberCoverEntityId}" />
                <input type="hidden" name="brokerEntityId" id="brokerEntityId" value="${requestScope.brokerEntityId}" />
                <input type="hidden" name="consultantEntityId" id="consultantEntityId" value="${requestScope.consultantEntityId}" />
                <input type="hidden" name="brokEntityId" id="brokEntityId" value="${requestScope.ConBroEntityId}" />
                <input type="hidden" name="consultEntityId" id="consultEntityId" value="${requestScope.ConBroEntityId}" />
                <input type="hidden" name="entID" id="entID" value="${sessionScope.ConBroEntityId}" />
                <input type="hidden" name="conEntID" id="conEntID" value="${sessionScope.ConBroEntityId}" />

                <table>
                    <tr id="BrokerApp_brokerTermEndDateRow"><agiletags:LabelTextBoxDateReq  displayname="End Date" elementName="BrokerApp_EndDate" mandatory="yes"/></tr>
                </table>
                <table>
                    <tr>
                        <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}', '${main_div}');"></td>
                        <td><input type="button" value="Save" onclick="clickTermButton()"></td>
                    </tr>
                </table>
            </agiletags:ControllerForm>
        </td></table>
    </body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Consultant Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty ConsultantResults}">
            <span>No Consultant found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Consultant Code</th>
                        <th>Broker Name</th>                        
                        <th>Initials</th>
                        <th>Surname</th>
                      
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${ConsultantResults}">
                        <tr>
                            <td><label class="label">${entry['consultantCode']}</label></td>
                            <td><label class="label">${entry['fstName']}</label></td>
                            <td><label class="label">${entry['initials']}</label></td>
                            <td><label class="label">${entry['surname']}</label></td>
                            <td><button type="submit" value="${entry['consultantCode']}" onclick="setMemberAppValues(this.value,'${agiletags:escapeStr(entry['fstName'])} ${agiletags:escapeStr(entry['surname'])}','${entry['entityId']}','${entry['region']}');">View</button></td>
                        </tr>
                    </c:forEach>

                </table>
            </c:otherwise>
        </c:choose>



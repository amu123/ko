<%-- 
    Document   : LabelNeoLookupDropDownReq
    Created on : 20 Jun 2016, 13:04:05 PM
    Author     : charlh

    External Parameters:
    SESSION:
        DeseasedReasonList = <LookupValue> OR
        ${elementName}List = <LookupValue> [RECOMMENDED]

--%>

<%@tag description="Displays array of data in a DropdownList" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ attribute name="elementName" rtexprvalue="true" required="true" %>
<%@ attribute name="displayName" rtexprvalue="true" required="true" %>
<%@ attribute name="javaScript" rtexprvalue="true" required="false" %>
<%@ attribute name="mandatory" rtexprvalue="true" required="false" %>
<%@ attribute name="unSelectedText" rtexprvalue="true" required="false" %>
<%@ attribute name="noResultText" rtexprvalue="true" required="false" %>
<%@ attribute name="errorValueFromSession" rtexprvalue="true" required="false" %>
<%@ attribute name="errorValue" required="false" %>

<c:set var="errorElemName" value="${elementName}_error" />
<c:set var="errorValue" value="${(empty errorValue) ? (empty requestScope[errorElemName] ? sessionScope[errorElemName] : requestScope[errorElemName]) : errorValue}" />
<c:set var="lookupListElemName" value="${elementName}List" />
<c:set var="DeseasedLookupList" value="${(empty DeseasedLookupList) ? (empty requestScope[lookupListElemName] ? sessionScope[lookupListElemName] : requestScope[lookupListElemName]) : DeseasedLookupList}" />
<c:set var="mandatory" value="${empty mandatory ? 'no' : (fn:toLowerCase(mandatory))}" />
<c:set var="errorValueFromSession" value="${empty errorValueFromSession ? 'no' : (fn:toLowerCase(errorValueFromSession))}" />

<td class="label" align="left" width="160px">${displayName}:</td>
    <td align="left" width="200px">
        <select style="width:215px" id="${elementName}" name="${elementName}" ${javaScript}>
            <c:if test="${sessionScope.DeseasedLookupList != null && !empty sessionScope.DeseasedLookupList}">
                <c:if test="${unSelectedText == null}">
                    <option value=""></option>
                </c:if>
                <c:if test="${unSelectedText != null}">
                    <option value="">${unSelectedText}</option>
                </c:if>
                <c:forEach items="${sessionScope.DeseasedLookupList}" var="entry">
                    <option value="${entry.id}">${entry.value}</option>
                </c:forEach>
            </c:if>
            <c:if test="${sessionScope.DeseasedLookupList == null || empty sessionScope.DeseasedLookupList}">
                <c:if test="${noResultText == null}">
                    <option value="">No items to display</option>
                </c:if>
                <c:if test="${noResultText != null}">
                    <option value="">${noResultText}</option>
                </c:if>
            </c:if>
        </select>   
    </td>
<td>
    <c:if test="${mandatory != null && mandatory == 'yes'}">
        <label class="red">*</label>
    </c:if>
</td>
<td width="200px" align="left">
    <c:if test="${errorValueFromSession != null && errorValueFromSession == 'yes'}">  
        <c:if test="${errorValue != null && !empty errorValue}">
            <label id="${elementName}_error" class="error">${errorValue}</label>
        </c:if>   
        <c:if test="${errorValue == null || empty errorValue}">
            <label id="${elementName}_error" class="error"></label>
        </c:if>
    </c:if>    
</td>
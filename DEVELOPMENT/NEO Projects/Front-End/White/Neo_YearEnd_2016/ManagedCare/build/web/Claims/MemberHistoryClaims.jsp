<%-- 
    Document   : MemberHistoryClaims
    Created on : 2011/08/16, 01:46:54
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script>
            
            function submitReturnWithAction(action) {
                document.getElementById('opperation').value = action;
                document.getElementById('onScreen').value = "MemberHistoryClaims.jsp";                
                document.forms[0].submit();
            }

            function submitWithAction(action, claimLineId, paidAmount, depCode) {
                document.getElementById('opperation').value = action;
                document.getElementById('memberDetailedClaimLineId').value = claimLineId;
                document.getElementById('memberDetailedClaimPaidAmount').value = paidAmount;
                document.getElementById('memberDetailedClaimDepCode').value = depCode;
                document.forms[0].submit();
            }
            
            function submitPageAction(action, pageDirection) {
                document.getElementById('opperation').value = action;
                document.getElementById('mcPageDirection').value = pageDirection;
                document.getElementById('onClaimLineScreen').value = 'MemberHistoryClaims.jsp';
                document.forms[0].submit();
            }            

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Claim Line Detail</label>
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="claimsHistoryMem">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <input type="hidden" name="memberDetailedClaimLineId" id="memberDetailedClaimLineId" value="" />
                            <input type="hidden" name="memberDetailedClaimPaidAmount" id="memberDetailedClaimPaidAmount" value="" />
                            <input type="hidden" name="memberDetailedClaimDepCode" id="memberDetailedClaimDepCode" value="" />
                            <input type="hidden" name="mcPageDirection" id="mcPageDirection" value="" />
                            <input type="hidden" name="onClaimLineScreen" id="onClaimLineScreen" value="" />
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <!--<table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                            </table>-->
                        </agiletags:ControllerForm>
                    </table>
                    <br/>
                    <HR color="#666666" WIDTH="50%" align="left">
                    <br/>
                    <agiletags:MemberClaimLineDetailsTable />
                    <br/>
                    <table>
                        <tr><agiletags:ButtonOpperation align="right" span="5" type="button" commandName="ReturnMemberClaimDetails" displayname="Return" javaScript="onClick=\"submitReturnWithAction('ReturnMemberClaimDetails');\""/></tr>
                    </table>
                </td></tr></table>
    </body>
</html>

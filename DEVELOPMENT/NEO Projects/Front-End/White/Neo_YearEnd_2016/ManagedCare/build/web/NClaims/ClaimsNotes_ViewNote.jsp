<%-- 
    Document   : ClaimsNotes_ViewNote
    Created on : 31 May 2016, 10:59:46 AM
    Author     : janf
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" language="JavaScript">

        </script>
    </head>
    <body>
        <agiletags:ControllerForm name="saveClaimNotesForm">
            <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                        <label class="header">View Note</label>
                        <br/>
                        <hr>
                        <table>
                            <input type="hidden" name="opperation" id="ClaimNotes_opperation" value="ClaimNotesCommand" />
                            <input type="hidden" name="onScreen" id="ClaimHeader_onScreen" value="addNote" />
                            <input type="hidden" name="command" id="ClaimHeader_command" value="SaveNote" />
                            <input type="hidden" name="claimId" id="claimId" value="${claimId}"/>

                            <tr>
                                <td align="left" width="160px"><label>Note:</label></td>
                                <td align="left">
                                    <label>${noteDetails}</label>
                                </td>
                            </tr>
                            <br/>
                        </table>
                        <hr>
                <tr><input type="button" value="Close" onclick="swapDivVisbible('Claim_Notes_View_Div', 'DetailedClaimNotes_Div');"></tr>

                </td></tr></table>
            </agiletags:ControllerForm>
    </body>
</html>

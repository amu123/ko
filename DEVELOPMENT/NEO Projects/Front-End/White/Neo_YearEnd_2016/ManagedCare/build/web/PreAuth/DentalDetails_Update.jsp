<%-- 
    Document   : DentalDetails_Update
    Created on : 2010/07/11, 06:36:50
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>.
<%@ page import="neo.manager.NeoUser" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/AuthDatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/PreAuthValidation/DateValidation.js"></script>
        <script type="text/javascript" language="JavaScript">
            //set current user
            <%
                NeoUser us2 = (NeoUser) session.getAttribute("persist_user");
                session.setAttribute("currentUser", us2.getName() + " " + us2.getSurname());
            %>

                $(function() {
                    var as = $("#authStatus").val();
                    checkAuthRejectStatus(as);                                                        
                    CheckSearchCommands();
                });
                
                function CheckSearchCommands(){
                    //reload ajax commands for screen population
                    var searchVal = document.getElementById('searchCalled').value;
                    if (searchVal == 'labProvider'){
                        validatePractice(document.getElementById('labProvider_text').value, 'labProvider');

                    }
                    document.getElementById('searchCalled').value = '';
                }

                function GetXmlHttpObject(){
                    if (window.ActiveXObject){
                        // code for IE6, IE5
                        return new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    if (window.XMLHttpRequest){
                        // code for IE7+, Firefox, Chrome, Opera, Safari
                        return new XMLHttpRequest();
                    }
                    return null;
                }

                //practice search
                function validatePractice(str, element){
                    //alert("validateProvider str = "+str);
                    var error = "#"+element+"_error";
                    if (str.length > 0) {
                        var url="/ManagedCare/AgileController";
                        var data = {'opperation':'FindPracticeDetailsByCode','provNum':str,'element':element,'id':new Date().getTime()};
                        //alert("url = "+url);
                        $.get(url, data, function(resultTxt){
                            if (resultTxt!=null) {
                                //alert("resultTxt = "+resultTxt);
                                var resTxtArr = resultTxt.split("|");
                                var result = resTxtArr[0];
                                if(result == "Error"){
                                    $(document).find(error).text("No such provider");
                                }else if(result == "Done"){
                                    //set provider name
                                    var descArr = resTxtArr[1].split("=");
                                    var pracName = descArr[1];
                                    $("#labProviderName").text(pracName);  
                                    $(document).find(error).text("");
                                }
                            }
                        });

                    }else {
                        //$(document).find(error).text("Please Enter a Provider Number");
                    }
                }

                function validateDental(command){
                    clearDentalMandatory();
                    xhr = GetXmlHttpObject();
                    if (xhr==null)
                    {
                        alert ("Your browser does not support XMLHTTP!");
                        return;
                    }
                    var url="/ManagedCare/AgileController?opperation=ValidateDentalDetails";
                    //get form values
                    var dst = $("#DentalSubType").val();
                    var afd = $("#authFromDate").val();
                    var atd = $("#authToDate").val();
                    var as = $("#authStatus").val();

                    url = url + "&dst="+dst+"&afd="+afd+"&atd="+atd+"&as="+as;

                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4 && xhr.statusText == "OK"){
                            var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));

                            if(result == "Error"){
                                var errorResponse = xhr.responseText.substring(xhr.responseText.indexOf('|')+1, xhr.responseText.length)
                                var mandatoryList = errorResponse.split("|");
                                for(i=0;i<mandatoryList.length;i++) {
                                    var elementError = mandatoryList[i].split(":");
                                    var element = elementError[0];
                                    var errorMsg = elementError[1];
                                    var error =
                                        $(document).find("#"+element+"_error").text(errorMsg);
                                }

                            }else if(result == "Done"){
                                document.getElementById('saveButton').disabled = true;
                                //$("#saveButton").hide();
                                submitWithAction(command);
                            }
                        }
                    };
                    xhr.open("POST",url,true);
                    xhr.send(null);
                }

                function clearDentalMandatory(){
                    $("#DentalSubType_error").text("");
                    $("#labButton_error").text("");
                    $("#authFromDate_error").text("");
                    $("#authToDate_error").text("");
                    $("#authStatus_error").text("");
                }


                function clearElement(element) {
                    part = element + '_text';
                    document.getElementById(element).value = '';
                    document.getElementById(part).value = '';
                }
                function submitWithAction(action) {
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }
                function submitWithAction(action, forField, onScreen) {
                    document.getElementById('onScreen').value = onScreen;
                    document.getElementById('searchCalled').value = forField;
                    document.getElementById('opperation').value = action;
                    document.forms[0].submit();
                }
                function hideRows(id) {
                    $("#"+id+"plus").show();
                    $("#"+id+"minus").hide();
                    var hiddenRows = $("tr[id^="+id+"hidden]");
                    for(i = 0; i < hiddenRows.size(); i++){
                        $(hiddenRows[i]).hide();
                    }

                }
                function showRows(id) {
                    $("#"+id+"plus").hide();
                    $("#"+id+"minus").show();
                    var hiddenRows = $("tr[id^="+id+"hidden]");
                    for(i = 0; i < hiddenRows.size(); i++){
                        $(hiddenRows[i]).show();
                    }
                }

                function validateADSession(date, element){
                    var at = '<%= session.getAttribute("authType")%>';
                    var ap = '<%= session.getAttribute("authPeriod")%>';
                    validatePreAuthDate(date, element, ap, at, "no");

                }
                
                function checkAuthRejectStatus(status){
                    if(status === "2"){
                        $("#authRejReasonRow").show();
                    }else{
                        $("#authRejReasonRow").hide();
                    }
                }                

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Dental Detail</label>
                    <br/>
                    <agiletags:ControllerForm name="dentalDetail">
                        <table border="0">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Dental Sub type" elementName="DentalSubType" lookupId="90" mandatory="yes" errorValueFromSession="yes" /></tr>

                            <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Lab Provider" elementName="labProvider" searchFunction="yes" searchOperation="ForwardToSearchPracticeCommand" onScreen="/PreAuth/DentalDetails_Update.jsp" mandatory="no" javaScript="onChange=\"validatePractice(this.value, 'labProvider');\""/></tr>
                            <tr><agiletags:LabelTextDisplay displayName="Provider Name" elementName="labProviderName" valueFromSession="yes" javaScript=""/></tr>                            
                            <tr><td colspan="5"></td></tr>
                            <tr><td colspan="5" align="left"><label class="subheader">Dental Code Details Allocated</label></td></tr>
                            <tr><agiletags:AuthLabTariffListDisplayTag sessionAttribute="SavedDentalLabCodeList" commandName="" javaScript="" /></tr>
                            <tr><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Manage Dental Code(s)" elementName="labButton" javaScript="onClick=\"submitWithAction('ForwardToLabSpecificTariff','labButton','/PreAuth/DentalDetails_Update.jsp');\"" mandatory="yes" /></tr>
                            <tr><td colspan="5"></td></tr>
                            <tr><agiletags:LabelTextBoxError valueFromSession="Yes" displayName="Amount Claimed" elementName="amountClaimed"/></tr>
                            <tr><agiletags:LabelTextBoxDate valueFromSession="Yes" displayname="Date Valid From" elementName="authFromDate" mandatory="yes" javascript="onblur=\"validateADSession(this.value, this.id);\"" /></tr>
                            <tr><agiletags:LabelTextBoxDate valueFromSession="Yes" displayname="Date Valid To" elementName="authToDate" mandatory="yes" javascript="onblur=\"validateADSession(this.value, this.id);\"" /></tr>

                            <tr><td colspan="5"></td></tr>
                            <tr><td colspan="5" align="left"><label class="subheader">Auth Note Details</label></td></tr>
                            <tr><agiletags:AuthNoteListDisplay sessionAttribute="authNoteList" commandName="" javaScript="" /></tr>
                            <tr><agiletags:ButtonOpperationLabelError type="button" commandName="" displayName="Manage Notes" elementName="authNoteButton" javaScript="onClick=\"submitWithAction('ForwardToAuthSpecificNote','authNoteButton','/PreAuth/DentalDetails_Update.jsp');\"" mandatory="no" /></tr>

                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Auth Status" elementName="authStatus" lookupId="111" mandatory="yes" javaScript="" errorValueFromSession="yes" javaScript="onchange=\"checkAuthRejectStatus(this.value);\""/></tr>
                            <tr id="authRejReasonRow"><agiletags:LabelNeoLookupValueDropDown elementName="authRejReason" displayName="Auth Reject Reason" lookupId="282" /></tr>                                                                                    
                            <tr><td colspan="5"></td></tr>
                            <tr><td colspan="5"></td></tr>
                            <tr>
                                <td colspan="5" align="left">
                                    <button name="opperation" type="button" onClick="submitWithAction('ReturnToGenericAuth','return','/PreAuth/GenericAuth_Update.jsp');" value="">Back</button>
                                    <button name="opperation" type="button" onClick="submitWithAction('ReloadGenericPreAuthCommand','reload','/PreAuth/DentalDetails_Update.jsp');" value="">Reset</button>
                                    <button id="saveButton" name="saveButton" type="button" onClick="validateDental('SaveUpdatedPreAuthDetailsCommand');" value="">Save</button>
                                </td>
                            </tr>

                        </table>
                    </agiletags:ControllerForm>
                    <br/>
                    <table align="right">
                        <tr><agiletags:LabelTextDisplay displayName="Last Updated By" elementName="lastUpUser" boldDisplay="yes" javaScript="" valueFromSession="yes" /></tr>
                        <tr><agiletags:LabelTextDisplay boldDisplay="yes" displayName="Current User" elementName="currentUser" javaScript="" valueFromSession="yes" /></tr>
                    </table>
                    <!-- content ends here -->
                </td></tr></table>
    </body>
</html>

<%-- 
    Document   : IndexDocument
    Created on : 02 Jul 2012, 3:34:17 PM
    Author     : Christo
--%>
<%@page import="com.koh.serv.PropertiesReader"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<% String folderList = (String) request.getAttribute("folderList");
System.out.println("-------------JSP------------------");
    System.out.println("folderList in page = " + folderList);
    if (folderList == null) {
        folderList = new PropertiesReader().getProperty("DocumentIndexScanFolder");
    }
    String listOrWork = (String)request.getAttribute("listOrWork");
    if(listOrWork == null){
        listOrWork = "List";
    }
String errorMsg = (String)request.getAttribute("errorMsg");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script language="JavaScript">
            var error = true;
            var idClicked = "";
             $(document).ready(function(){
                $("#memberNumber").blur(function(){
                    validateMemberNumber($(this).val(), 'memberNumber');
                });
                
                $("#profileIndexButton").click(function () {
                    idClicked="profileIndexButton";
                });
                
                 $("#documentIndexing").submit(function(){
                    if(idClicked=='profileIndexButton'){
                        document.getElementById('profileIndexButton').disabled = true;
                        idClicked="";
                   if(error == true){
                       validateMemberNumber(document.getElementById('memberNumber').value, 'memberNumber');
                       if(error == true){
                           document.getElementById('profileIndexButton').disabled = false;
                          return false;
                       }else{
                           return true;
                       }
                   }else{
                       return true;
                   }
                  }else{
                      return true;
                  }
                });
               });
               
              function validateMemberNumber(str, element){
                $("#errorText").text(" ");
                document.getElementById('profileIndexButton').disabled = true;
                if(str != null && str != ""){
                    var url =  "/ManagedCare/AgileController";
                    var data = {
                        "opperation":"GetMemberByNumberCommand",
                        "number":str,
                        "nocache": new Date().getTime()
                    }
                    $.get(url, data, function(response){
                        var result = response.substring(0, response.indexOf('|'));
                        if (result != null){
                        if(result == "Error"){
                            error = true;
                            document.getElementById('profileIndexButton').disabled = true;
                            $("#error").text(" ");
                            $("#errorText").text("Please enter valid member number");
                    }else{
                        error = false;
                        $("#error").text(" ");
                        document.getElementById('profileIndexButton').disabled = false;
                        $("#errorText").text(" ");
                        }               
                    }else{
                            error = false;
                            document.getElementById('profileIndexButton').disabled = false;
                            $("#error").text(" ");
                            $("#errorText").text(" ");
                        }
                },'TEXT');
                }else{
                    error = true;
                    document.getElementById('profileIndexButton').disabled = true;
                    $("#error").text(" ");
                    $("#errorText").text("Please enter Member Number");
                }
            }
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }            
            function setParams(){
                document.getElementById('folderList').value =  '<%=folderList%>';
                document.getElementById('listOrWork').value = '<%=listOrWork%>';
            }
        </script>
    </head>
    <body>

        <table width=100% height=100%><tr valign="left"><td width="50px"></td><td align="left">
                    <label class="header">Document Indexing For <%=request.getSession().getAttribute("profile")%></label>
                    <br/><br/><br/>
                    <table>
                        <% if(errorMsg != null){ %>
                        <tr><td><label id="error" class="error"><%=errorMsg%></label></td></tr>
                        <% } %>
                        <tr><td><label id="errorText" class="error"></label></td>
                        <agiletags:ControllerForm name="documentIndexing"> 
                        <input type="hidden" name="folderList" id="folderList" value="" />
                        <input type="hidden" name="listOrWork" id="listOrWork" value="" />
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />


                            <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                            <tr><td>
                                    <table>
                                        <tr><agiletags:DocumentIndexingFileList  command="memberNumber" multiple="false" /></tr>
                                    </table> </td><td>
                                    <table>
                                          <tr><agiletags:DocumentIndexLableText displayName="Member Number" elementName="memberNumber"/></tr>
                                          <tr><agiletags:DocumentIndexDropdownType displayName="Document Type" elementName="docType"/></tr>
                                    </table>
                                </td></tr>
                            </agiletags:ControllerForm>
                    </table>
                </td></tr></table>

    </body>
</html>


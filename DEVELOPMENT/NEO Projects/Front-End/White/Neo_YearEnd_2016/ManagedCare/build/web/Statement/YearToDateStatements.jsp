<%-- 
    Document   : YearToDateStatements
    Created on : Sep 13, 2011, 2:08:15 PM
    Author     : Johan-NB
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>.
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script language="JavaScript">
            var submitDone = false;

            $(function() {
                toggleStmtType();
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction1(action, actionParam) {
                if (submitDone == "false" || submitDone != actionParam) {
                    submitDone = actionParam;
                    if (actionParam == "Preview") {
                        document.getElementById('btn_stmtPreview').disabled = true;
                    } else if (actionParam == "Send") {
                        document.getElementById('btn_stmtSubmit').disabled = true;
                    }

                    document.getElementById('opperation').value = action;
                    document.getElementById('opperationParam').value = actionParam;
                    document.forms[0].submit();
                }
            }

            function submitWithAction(action, forField, onScreen) {
                document.getElementById('opperation').value = action;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('onScreen').value = onScreen;
                document.forms[0].submit();
            }
            
            function validateDate(element, dateString) {
                $("#stmtToDate_error").text("");
                $("#stmtFromDate_error").text("");
                
                //  check for valid numeric strings
                var strValidChars = "0123456789/";
                var strChar;
                var strChar2;
                var blnResult = true;

                if (dateString.length < 10 || dateString.length > 10) blnResult = false;

                //  test strString consists of valid characters listed above
                for (i = 0; i < dateString.length; i++)
                {
                    strChar = dateString.charAt(i);
                    if (strValidChars.indexOf(strChar) == -1)
                    {
                        blnResult = false;
                    }
                }

                // test if slash is at correct position (yyyy/mm/dd)
                strChar = dateString.charAt(4);
                strChar2 = dateString.charAt(7);
                if (strChar != '/' || strChar2 != '/') blnResult = false;

                if(element == "" || element == null){
                    return blnResult;
                    
                }else{
                    if(blnResult == false){
                        var error = "#"+element+"_error";
                        $(error).text("Error: Incorrect Statement Date");
                    }
                }
                submitDone = false;
                document.getElementById('btn_stmtPreview').disabled = false;
                document.getElementById('btn_stmtSubmit').disabled = false;
            }

            function datePickerClosed(targetDateField) {
                targetDateField.onchange();
            }

            function toggleStmtType(){
                var str = $("#stmtType").val();
                if(str == "99"){
                    $("#memRow").hide();
                    $("#provRow").hide();
                }else if (str == "1"){
                    $("#memRow").show();
                    $("#provRow").hide();
                }else if (str == "2"){
                    $("#memRow").hide();
                    $("#provRow").show();
                }
                submitDone = false;
                document.getElementById('btn_stmtPreview').disabled = false;
                document.getElementById('btn_stmtSubmit').disabled = false;
            }

            function getPrincipalMemberForNumber(str, element){
                xmlhttp=GetXmlHttpObject();
                if (xmlhttp==null)
                {
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                if (str.length > 0) {
                    var url="/ManagedCare/AgileController";
                    url=url+"?opperation=GetMemberByNumberCommand&number="+ str + "&element=" + element;
                    xmlhttp.onreadystatechange=function(){stateChanged(element)};
                    xmlhttp.open("POST",url,true);
                    xmlhttp.send(null);
                }
                submitDone = false;
                document.getElementById('btn_stmtPreview').disabled = false;
                document.getElementById('btn_stmtSubmit').disabled = false;
            }
            //provider search
            function validateProvider(str, element){
                xmlhttp=GetXmlHttpObject();
                if (xmlhttp==null)
                {
                    alert ("Your browser does not support XMLHTTP!");
                    return;
                }
                if (str.length > 0) {
                    var url="/ManagedCare/AgileController";
                    url=url+"?opperation=GetProviderByNumberCommand&number="+ str + "&element=" + element;
                    xmlhttp.onreadystatechange=function(){stateChanged(element)};
                    xmlhttp.open("POST",url,true);
                    xmlhttp.send(null);
                }
                submitDone = false;
                document.getElementById('btn_stmtPreview').disabled = false;
                document.getElementById('btn_stmtSubmit').disabled = false;
            }
            
            function validateStatementRequest(action, actionParam, validateEmail){
                var stmtType = $("#stmtType").val();
                var errorCount = 0;
                var errorMsg = "";
                clearErrorFields();

                if(stmtType == "99"){
                    errorCount++;
                    errorMsg = "#stmtType_error-Please Select a Statement Type|";

                }else{
                    //statement type number validation
                    if(stmtType == "1"){
                        var memNo = $("#memberNo_text").val();
                        if(memNo == null || memNo == ""){
                            errorCount++;
                            errorMsg = "#memberNo_error-Please Enter a Member Number|";
                        }

                    }else if (stmtType == "2"){
                        var provNo = $("#providerNo_text").val();
                        if(provNo == null || provNo == ""){
                            errorCount++;
                            errorMsg = "#providerNo_error-Please Enter a Provider Number|";
                        }
                    }
                    //satement date validation
                    var stmtFrom = $("#stmtFromDate").val();
                    var stmtTo = $("#stmtToDate").val();
                    var fromOK = validateDate(null, stmtFrom);
                    var toOK = validateDate(null, stmtTo);
                    
                    if(fromOK == false){
                        errorCount++;
                        errorMsg = "#stmtFromDate_error-Error: Incorrect Statement From Date|";
                    }
                    if(toOK == false){
                        errorCount++;
                        errorMsg = "#stmtToDate_error-Error: Incorrect Statement To Date|";
                    }
                    
                    if(fromOK == true && toOK == true){
                        //check if from before end
                        var from = new Date(stmtFrom).getTime();
                        var to = new Date(stmtTo).getTime();
                        
                        if(to < from){
                            errorCount++;
                            errorMsg = "#stmtToDate_error-Error: To Date cannot be before From Date|";
                        }
                        
                    }


                    //validate email
                    if(validateEmail == "yes"){
                        var email = $("#emailAddress").val();
                        if(email == null || email == ""){
                            errorCount++;
                            errorMsg = "#emailAddress_error-Please Enter a Email Address|";
                        }
                    }
                }
                //print errors
                if(errorCount > 0){
                    var errors = errorMsg.split("|");
                    for(i=0; i<errors.length; i++) {
                        var error = errors[i].split("-");
                        $(error[0]).text(error[1]);
                    }

                }else{
                    submitWithAction1(action, actionParam);
                }
            }            


            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }
            
            function clearErrorFields(){
                $("#emailAddress_error").text("");
                $("#stmtToDate_error").text("");
                $("#stmtFromDate_error").text("");
                $("#providerNo_error").text("");
                $("#memberNo_error").text("");
                $("#stmtType_error").text("");               
                
            }

            function stateChanged(element){
                if (xmlhttp.readyState==4)
                {
                    //alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    //alert(result);
                    if(result == "GetMemberByNumberCommand"){
                                                                                
                        var resultArr = xmlhttp.responseText.split("|");
                        var numberArr = resultArr[1].split("=");
                        var emailArr = resultArr[2].split("=");
                        var productArr = resultArr[3].split("=");
                        
                        var number = numberArr[1];
                        var email = emailArr[1];
                        var product = productArr[1];
                        document.getElementById(element + '_error').innerText = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('emailAddress').value = email;
                        document.getElementById('searchCalled').value = '';
                        document.getElementById('productId').value = product;

                    }else if(result == "GetProviderByNumberCommand"){
                        var number = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=')+1, xmlhttp.responseText.lastIndexOf('|'));
                        var email = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=')+1, xmlhttp.responseText.lastIndexOf('$'));
                        document.getElementById(element + '_error').innerText = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('emailAddress').value = email;
                        document.getElementById('searchCalled').value = '';

                    }else if(result == "Error"){
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|')+1, xmlhttp.responseText.length);
                        if(forCommand == "GetMemberByNumberCommand"){
                            document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                            //document.getElementById(element + '_text').value = number;
                            // document.getElementById('emailAddress').value = email;
                            //document.getElementById('searchCalled').value = '';
                        }else if(forCommand == "GetProviderByNumberCommand"){
                            document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                        }
                    }else if(result == "Access"){
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|')+1, xmlhttp.responseText.length);
                        if(forCommand == "GetMemberByNumberCommand"){
                            document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                            //document.getElementById(element + '_text').value = number;
                            // document.getElementById('emailAddress').value = email;
                            //document.getElementById('searchCalled').value = '';
                        }else if(forCommand == "GetProviderByNumberCommand"){
                            document.getElementById(element + '_error').innerText = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|')+1, xmlhttp.responseText.lastIndexOf('|'));
                        }
                    }
                }
            }
            
        </script>
    </head>

    <body>

        <table width=100% height=100%><tr valign="left"><td width="50px"></td><td align="left">
                    <label class="header">YTD Statements</label>
                    <br/><br/><br/>
                    <table>
                        <agiletags:ControllerForm name="getYTDStmt">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <agiletags:HiddenField elementName="searchCalled"/>
                            <input type="hidden" name="onScreen" id="onScreen" value="" />
                            <input type="hidden" name="opperationParam" id="opperationParam" value="" />
                            <input type="hidden" name="productId" id="productId" value="" />
                            <table>
                                <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Statement Type" elementName="stmtType" errorValueFromSession="yes" lookupId="169" javaScript="onChange=\"toggleStmtType();\"" mandatory="yes" /></tr>
                                <tr id="memRow"><agiletags:LabelTextSearchText displayName="Member Number" elementName="memberNo" valueFromSession="Yes" searchFunction="Yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/Statement/YearToDateStatements.jsp" javaScript="onChange=\"getPrincipalMemberForNumber(this.value, 'memberNo');\""/></tr>
                                <tr id="provRow"><agiletags:LabelTextSearchText displayName="Provider Number" elementName="providerNo" valueFromSession="Yes" searchFunction="Yes" searchOperation="ForwardToSearchProviderCommand" onScreen="/Statement/YearToDateStatements.jsp" javaScript="onChange=\"validateProvider(this.value, 'providerNo');\""/></tr>
                                <tr><agiletags:LabelTextBoxDate displayname="Payment Date from" elementName="stmtFromDate" javascript="onChange=\"validateDate('stmtFromDate',this.value);\"" mandatory="yes"/></tr>
                                <tr><agiletags:LabelTextBoxDate displayname="Payment Date to" elementName="stmtToDate" javascript="onChange=\"validateDate('stmtToDate',this.value);\"" mandatory="yes"/></tr>
                                <tr><agiletags:LabelTextBoxError displayName="E-Mail address" elementName="emailAddress"/></tr>
                            </table>
                            <br/>
                            <table>
                                <tr>
                                    <td colspan="2" align="left">
                                        <button id="btn_stmtCancel" name="btn_stmtCancel" type="button" onClick="submitWithAction('ReloadYTDStatementCommand');" value="" >Cancel</button>
                                        <button id="btn_stmtPreview" name="btn_stmtPreview" type="button" onClick="validateStatementRequest('SubmitYTDStatementCommand', 'Preview', 'no');" value="" >Preview</button>
                                        <button id="btn_stmtSubmit" name="btn_stmtSubmit" type="button" onClick="validateStatementRequest('SubmitYTDStatementCommand', 'Send', 'yes');" value="" >Send</button>
                                    </td>
                                </tr>
                            </table>                                
                        </agiletags:ControllerForm>

                    </table>

                </td></tr></table>

    </body>
</html>

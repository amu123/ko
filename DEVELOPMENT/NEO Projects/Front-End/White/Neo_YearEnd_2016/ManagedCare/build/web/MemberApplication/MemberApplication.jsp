<%-- 
    Document   : MemberApplication
    Created on : 2012/03/16, 11:06:14
    Author     : yuganp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>

        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Membership/MemberMaintenance.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/ckeditor/ckeditor.js"></script>
        <style>
            #header { position: fixed; top: 0; left: 8px; right: 8px; z-index: 999;background: #fff;}
            #footer { position: fixed; bottom: 0; left: 0; height: 100px; }
            #content {position: relative; margin-top: 100px;}
        </style>
        <script>
            function changeStatus() {
                document.getElementById("memberAppStatus").value = document.getElementById("memAppStatus").value;
                if (document.getElementById("memberAppStatus").value == '7' || (document.getElementById("memberAppStatus").value == '2' && ${applicationScope.Client == 'Sechaba'})) {
                    getPageContents(document.forms[0], null, 'main_div', 'overlay');
                } else {
                    submitFormWithAjaxPost(document.forms[0]);
                }
            }

            $(document).ready(function () {
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                    });
                    
                    var currentVal =  document.getElementById('memAppStatus').value;
                    
                    if(currentVal === '7') {
                        document.getElementById('memAppStatus').disabled = true;
                    } else if (document.getElementsByName('MemberApp_productId').value === null || document.getElementsByName('MemberApp_productId').value === undefined || document.getElementsByName('MemberApp_productId').value === 'undefined') {
                        document.getElementById('memAppStatus').disabled = true;
                    }
            });

            function resizeContent() {
                $('#content').css('margin-top', $('#header').height());
            }

            function addProductOption(prodId, element2Toggle, optVersion) {
                var elem = "#" + element2Toggle;
                var url = "/ManagedCare/AgileController";

                var data = {
                    'opperation': 'GetProductOptionCommand',
                    'prodId': prodId,
                    'id': new Date().getTime(),
                    'currentOptions': optVersion
                };

                //GET RESPONSE FROM BACKEND
                $.get(url, data, function (result) {
                    $(elem).empty();
                    $(elem).append("<option value=\"99\"></option>");
                    var xmlDoc = GetDOMParser(result);
                    //print value from servlet
                    $(xmlDoc).find("ProductOptions").each(function () {
                        //set product details
                        $(this).find("Option").each(function () {
                            var prodOpt = $(this).text();
                            var opt = prodOpt.split("|");
                            //Add option to dependant dropdown
                            var optVal = document.createElement('OPTION');
                            optVal.value = opt[0];
                            optVal.text = opt[1];
                            document.getElementById(element2Toggle).options.add(optVal);

                        });
                    });

                }, 'XML');
            }
            
            function dateCheck(){
                var birthInput = $("#MemberDependantApp_DateOfBirth").val();
                var today = new Date();
                var birthDate = new Date(birthInput);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()))
                {
                    age--;
                }
                $("#ageWarn").text("");
                var depTypeSelected = $("#MemberDependantApp_dependant_type option:selected").text();
                var disabledSelected = $("#MemberDependantApp_disabledInd option:selected").text();
                var studentSelected = $("#MemberDependantApp_studentInd option:selected").text();

                if (age < 18 && depTypeSelected == "Adult")
                {
                    $("#ageWarn").text("**** Warning : Child will be linked to Adult premium ****");
                }

                if (age >= 18 && depTypeSelected == "Child" && (disabledSelected == "No" || disabledSelected == "") && (studentSelected == "No" || studentSelected == ""))
                {
                    $("#ageWarn").text("**** Warning : Adult will be linked to Child premium ****");
                }

                if (age > 25 && depTypeSelected == "Child" && (disabledSelected == "No" || disabledSelected == "") && (studentSelected == "Yes"))
                {
                    $("#ageWarn").text("**** Warning : Student over 25 will be linked to Child premium ****");
                }
            }
            
            function changeMedicalPractice(optionID) {
                if (optionID == '100') {
                    document.getElementById('currentMedicalPractitioner').style.display = 'none';
                    document.getElementById('gomomoMedicalPractitioner').style.display = '';
                    document.getElementById('MemberApp_network_IPARow').style.display = '';
                } else {
                    document.getElementById('currentMedicalPractitioner').style.display = '';
                    document.getElementById('gomomoMedicalPractitioner').style.display = 'none';
                    document.getElementById('MemberApp_network_IPARow').style.display = 'none';
                }
            }
            
            window.setInterval(function (){
                var lastName = document.getElementById('MemberApp_lastName');
                if (lastName.value !== "") {
                    document.getElementById('memAppStatus').removeAttribute("disabled");
                }
                
                var optionID = document.getElementById('MemberApp_optionId');                
                if (optionID != 'undefined' && optionID != 'null' && optionID != undefined && optionID != '') {
                    changeMedicalPractice(optionID.value);
                }
            }, 3000);
            
        function toggleNetworkIPA(optionId) {
            var stopInterval = false;
            var interval = window.setInterval(function (){
                var depNetwIPA = document.getElementById('MemberDependantApp_network_IPARow');
                if (optionId.value == '100') {
                    if (depNetwIPA != 'null' && depNetwIPA != null) {
                        depNetwIPA.style.display = '';
                        stopInterval = true;
                    }
                }
                
                if (stopInterval) {
                    clearInterval(interval);
                }
            }, 3000);
        };
        </script>
    </head>
    <body onload="initAgileTabs();">
        <agiletags:ControllerForm name="TabControllerForm">
            <input type="hidden" name="opperation" id="opperation" value="MemberApplicationTabContentsCommand" />
            <input type="hidden" name="onScreen" id="onScreen" value="MemberApplication" />
            <input type="hidden" name="memberAppNumber" id="memberAppNumber" value="${requestScope.memberAppNumber}" />
            <input type="hidden" name="memberAppStatus" id="memberAppStatus" value="${requestScope.memberAppStatus}" />
            <input type="hidden" name="memberAppCoverNumber" id="memberAppCoverNumber" value="${requestScope.memberCoverNumber}" />
            <input type="hidden" name="memberAppType" id="memberAppType" value="${requestScope.memberAppType}" />
            <input type="hidden" name="target_div" value="${target_div}" />
            <input type="hidden" name="main_div" value="${main_div}" />
            <input type="hidden" name="menuResp" id="menuResp" value="${param.menuResp}" />
            
        </agiletags:ControllerForm>
        <div id="main_div">
      <div id="header">
        <ol id="tabs" >
          <li><a href="#MemberAppDetails" class="">Member Details</a></li>
          <li><a href="#Dependants"  >Dependants</a></li>
          <li><a href="#SpecificQuestions"  >Specific Questions</a></li>
          <li><a href="#GeneralQuestions"  >General Questions</a></li>
          <li><a href="#HospitalAdmission"  >Hospital Admissions</a></li>
          <li><a href="#ChronicMedication"  >Chronic Medication</a></li>
          <li><a href="#Underwriting"  >Underwriting</a></li>
          <li><a href="#Notes"  >Notes</a></li>
          <li><a href="#AuditTrail"  >Audit Trail</a></li>
          <li><a href="#Documents"  >Documents</a></li>
          <!--<li><a href="#DocumentsGeneration"  >Documents Generation</a></li>-->
        </ol>    
        <c:choose>
              <c:when test="${param.menuResp == 'customerCare'}">
                  <fieldset disabled>
              </c:when>
              <c:otherwise>
                  <fieldset>
              </c:otherwise>
          </c:choose>
        <table width="100%" bgcolor="#dddddd" style="border: 1px solid #c9c3ba;">            
            <tr>
            <td width="375" class="label"><span style="font-family: Arial, Helvetica, sans-serif;font-size: 1.0em;">Member Name: <label id="member_header_memberName" >${requestScope.memberName}</label></span></td>
            <td class="label"><span style="font-family: Arial, Helvetica, sans-serif;font-size: 1.0em;">Member Number: <label id="member_header_applicationNumber">${requestScope.memberAppCoverNumber}</label></span></td>
            <td><label ></label></td>
            </tr>
            <tr>
            <%--<c:if test="${sessionScope.MemberApp_productId < 3}">--%>
                <td colspan="3" class="label"><span style="font-family: Arial, Helvetica, sans-serif;font-size: 1.0em;">Application Status: </span>
                    <select id="memAppStatus" name="memAppStatus" onchange="document.getElementById('changeStatusBtn').style.visibility='visible';" >
                        <option value="1" ${empty requestScope.memberStatus || requestScope.memberStatus == 1 ? "selected" : ""}>Capturing in progress</option>
                        <option value="2" ${requestScope.memberStatus == 2 ? "selected" : ""}>Waiting for documentation</option>
                        <option value="3" ${requestScope.memberStatus == 3 ? "selected" : ""}>Validated and ready for quality assessment</option>
                        <option value="4" ${requestScope.memberStatus == 4 ? "selected" : ""}>Cleared for underwriting</option>
                        <option value="5" ${requestScope.memberStatus == 5 ? "selected" : ""}>Quality assessment failed</option>
                        <option value="6" ${requestScope.memberStatus == 6 ? "selected" : ""}>Underwriting in progress</option>
                        <option value="9" ${requestScope.memberStatus == 9 ? "selected" : ""}>Underwriting completed</option>
                        <option value="11" ${requestScope.memberStatus == 11 ? "selected" : ""}>Terms Of Acceptance</option>
                        <c:if test="${persist_user_viewMemApps == false}">
                        <option value="7" ${requestScope.memberStatus == 7 ? "selected" : ""}>Active</option>
                        </c:if>
                        <option value="8" ${requestScope.memberStatus == 8 ? "selected" : ""}>Not taken up (NTU)</option>
                    </select>
                    <button style="visibility: hidden;" id="changeStatusBtn" value="Update Status" onclick="this.style.visibility='hidden'; changeStatus();">Change Status</button></td>
                <%--</c:if>--%>
            </tr>
        </table> 
      </fieldset>
      </div>
        <c:choose>
            <c:when test="${param.menuResp == 'customerCare'}">
                <fieldset disabled>
            </c:when>
            <c:otherwise>
                <fieldset>
            </c:otherwise>
        </c:choose>
      <div id="content">
        <div class="tabContent" id="MemberAppDetails">Loading...</div>
        <div class="tabContent hide" id="Dependants">Loading...</div>
        <div class="tabContent hide" id="SpecificQuestions">Loading...</div>
        <div class="tabContent hide" id="GeneralQuestions">Loading...</div>
        <div class="tabContent hide" id="HospitalAdmission">Loading...</div>
        <div class="tabContent hide" id="ChronicMedication">Loading...</div>
        <div class="tabContent hide" id="Underwriting">Loading...</div>
        </fieldset>
        <div class="tabContent hide" id="Notes">Loading...</div>
        <div class="tabContent hide" id="AuditTrail">Loading...</div>
        <div class="tabContent hide" id="Documents">Loading...</div>
        <div class="tabContent hide" id="DocumentsGeneration">Loading...</div>      
      </div>
                <!--</fieldset>-->
        </div>
        <div id="overlay" style="display:none;"></div>
        <div id="overlay2" style="display:none;"></div>

    </body>
</html>

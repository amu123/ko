<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <br>
    <label class="subheader">Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty BankStatementSearchResults}">
            <span>No Unallocated Statements found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Date</th>
                        <th>Scheme</th>
                        <th>Bank</th>
                        <th>Account</th>
                        <th>Allocated Amount</th>
                        <th>Allocated Lines</th>
                        <th>${suspType} Amount</th>
                        <th>${suspType} Lines</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${BankStatementSearchResults}">
                        <tr>
                            <td><label>${entry['statementDate']}</label></td>
                            <td><label>${entry['productName']}</label></td>
                            <td><label>${entry['bankName']}</label></td>
                            <td><label>${entry['accountNumber']}</label></td>
                            <td><label>${entry['totalAllocated']}</label></td>
                            <td><label>${entry['countAllocated']}</label></td>
                            <td><label>${entry['totalUnallocated']}</label></td>
                            <td><label>${entry['countUnallocated']}</label></td>
                            <td><input type="submit" value="Select" onclick="document.getElementById('BankStatementCommand').value = 'Select';document.getElementById('BankStatementId').value = '${entry['statementId']}';"></td>
                        </tr>
                    </c:forEach>

                </table>
            </c:otherwise>
        </c:choose>

   
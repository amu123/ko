
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

  <agiletags:ControllerForm name="EmployerAppNotesAddForm">
    <input type="hidden" name="opperation" id="opperation" value="EmployeeAppNotesCommands" />
    <input type="hidden" name="onScreen" id="onScreen" value="EmployeeApplicationNotesAdd" />
    <input type="hidden" name="employerEntityId" id="employerAddEntityId" value="${employerEntityId}" />
    <input type="hidden" name="notesListSelect" id="employeeAppNoteslistSelect" value="${notesListSelect}" />

<input type="hidden" name="target_div" value="${target_div}" />
    <input type="hidden" name="main_div" value="${main_div}" />
    <input type="hidden" name="buttonPressed" id="EmployeeAppNotesAdd_button_pressed" value="SaveButton" />

    <label class="header">${empty noteId ? 'Add' : 'View'} Note</label>
    <hr>
    <br>
     <table>
         <tr>
             <td align="left" width="160px"><label>Note:</label></td>
             <td align="left">
                 <c:if test="${empty noteId}">
                 <textarea class="ckeditor" name="EmployeerAppNotesAdd_details" id="EmployeerAppNotesAdd_details" rows="20" cols="50">${noteDetails}</textarea>
                 <script type="text/javascript">
                                        jQuery(document).ready(function() {
                                        CKEDITOR.replace('EmployeerAppNotesAdd_details', 
                                        {
                                            toolbar : 'Basic'
                                        });
                                        
                                    });
                                    </script>
                 </c:if>
                 <c:if test="${!empty noteId}">
                     <label>${noteDetails}</label>
                 </c:if>
                 
             </td>
         </tr>
     </table>
      
    <hr>
    <table id="EmployerAppNoteDetailsSaveTable">
        <tr>
           <td><input type="button" value="${empty noteId ? 'Cancel' : 'Close'}" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
           <c:if test="${empty noteId}">
               <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'EmployeeAppNotesDetails', null,'${main_div}', '${target_div}');"></td>
           </c:if>
        </tr>
    </table>
  </agiletags:ControllerForm>


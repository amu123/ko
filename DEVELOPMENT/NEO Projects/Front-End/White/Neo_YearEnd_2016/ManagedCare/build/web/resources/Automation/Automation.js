/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function paymentRun(btn) {
    if (!validatePaymentRun()) {
        displayNeoErrorNotification('There are validation errors');
        return;
    }
    if (btn != undefined) {
        btn.disabled = true;
    }
    var processFrom = $('#processFrom').val();
    var processTo = $('#processTo').val();
    var productId = $('#productId').val();
    var url = "/ManagedCare/AgileController?opperation=PaymentRunCommand&command=paymentRun&processFrom=" + processFrom + "&processTo=" + processTo + "&productId=" + productId;
    $.get(url, function (resultTxt) {
        var resultArr = resultTxt.split("|");
        if (resultArr[0] === "DONE") {
            $('#paymentRunLines').val(resultArr[1]);
            $('#paymentRunValue').val(resultArr[2]);
        }
        
        url = "/ManagedCare/AgileController?opperation=PaymentRunCommand&command=populateRunSummary";
        $.post(url, function(data) {
            updateFormFromData(data, 'PaymentRun_Summary');
        });
        
        if (btn != undefined) {
            btn.disabled = false;
        }
    });

}

function validatePaymentRun() {
    var valid = true;
    var res = true;
    var productId = $('#productId').val();
    if (productId === null || productId === '99' || productId === '') {
        valid = false;
        $('#productId_error').text("Please choose a Product ID");
    }

    res = validateNeoDateField('processFrom', true, null);
    valid = valid && res;
    res = validateNeoDateField('processTo', true, null);
    valid = valid && res;

    return valid;
}

function approvePaymentRun(btn) {
    var url = "/ManagedCare/AgileController?opperation=PaymentRunCommand&command=approveRun";
    $.get(url, function (resultTxt) {
        var resultTxtArr = resultTxt.split("|");
        if (resultTxtArr[0] === "DONE") {
            if (btn != undefined) {
                btn.disabled = true;
            }
            $('#approveRun_error').text(resultTxtArr[1]);
        } else if(resultTxtArr[0] === "ERROR"){
            if (btn != undefined && resultTxtArr[1] !== "Total Amount is zero") {
                btn.disabled = false;
            }
            $('#approveRun_error').text(resultTxtArr[1]);
        }
    });
}

function exportRun(btn) {
    var url = "/ManagedCare/AgileController?opperation=PaymentRunCommand&command=exportRun";
    $.get(url, function (response) {
        //var result = resultTxt.trim();
        
        var a = window.document.createElement('a');

        a.href = window.URL.createObjectURL(new Blob([response], {type: 'text/csv'}));
        a.download = "exportPayRun.csv";

        // Append anchor to body.
        document.body.appendChild(a);
        a.click();

        // Remove anchor from body
        document.body.removeChild(a);

    });
}

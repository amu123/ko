<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    function isMemberResigned() {
        var selectedNewPrincipalEntity = $("#newPrincipalEntityId").val();
        var coverStatusVal = document.getElementById(selectedNewPrincipalEntity).value;
        $("#newPrincipalEntityId_error").text("");
        $("#saveButton").removeAttr('disabled');
        
        if(coverStatusVal == "Resign"){
            $("#newPrincipalEntityId_error").text("Dependant selected is resigned swap not allowed");
            $("#saveButton").attr("disabled", "disabled");
        }
    }
</script>

<agiletags:ControllerForm name="SwapMemberForm">
    <input type="hidden" name="opperation" id="opperation" value="SaveMemberCoverDetailsCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="DepSwap" />
    <input type="hidden" name="buttonPressed" id="depSwapButtonPressed" value="" />
    <input type="hidden" name="memberEntityId" id="swapMemberEntityId" value="${memberCoverEntityId}" />
    <input type="hidden" name="brokerEntityId" id="brokerEntityId" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="employerEntityId" id="employerEntityId" value="${requestScope.groupEntityId}" />
    <br/>
    <label class="header">Dependant Swap</label>
    <hr>
    <br/>
    <table>                                
        <tr>
            <td align="left" width="160px"><label>Current Principal Member</label></td><td align="left" width="200px">
                <label>
                    <c:forEach var="entry" items="${MemberCoverDependantDetails}">
                        <c:if test="${entry.dependentTypeId == 17}">
                            ${entry.dependentNumber} ${entry.name} ${agiletags:formatXMLGregorianDate(entry.dateOfBirth)}
                        </c:if>
                    </c:forEach>                      
                </label>
            </td>
        </tr>
        <tr>
            <td align="left" width="160px"><label>New Principal Member</label></td>
            <td align="left" width="200px">
                <select style="width:215px" name="newPrincipalEntityId" id="newPrincipalEntityId" onblur="isMemberResigned()">
                    <c:forEach var="entry" items="${MemberCoverDependantDetails}">
                        <c:if test="${entry.dependentTypeId > 17}">
                            <option value="${entry.entityId}" selected>${entry.dependentNumber} ${entry.name} ${agiletags:formatXMLGregorianDate(entry.dateOfBirth)}</option>
                        </c:if>
                    </c:forEach>
                </select>
                <c:forEach var="entry" items="${MemberCoverDependantDetails}">
                    <c:if test="${entry.dependentTypeId > 17}">
                        <input type="hidden" name="coverStatus" id="${entry.entityId}" value="${entry.status}" /> 
                    </c:if>
                </c:forEach>
            </td>
            <td><label class="red">*</label></td>
            <td>&nbsp;</td>
            <td width="200px"><label id="newPrincipalEntityId_error" class="error"></label></td>
        </tr>
        <tr><agiletags:LabelTextBoxDateReq  displayname="Start Date" elementName="depSwapStartDate" valueFromSession="yes" mandatory="yes"/>        </tr> 
    </table>
    <br>
    <label class="subheader">Group Details</label>
    <hr>
    <br>
    <table>                        
        <tr><agiletags:LabelTextSearchTextDiv displayName="Group Code" elementName="swapGroupCode" mandatory="yes" onClick="document.getElementById('depSwapButtonPressed').value = 'SearchSwapGroupButton'; getPageContents(document.forms['SwapMemberForm'],null,'overlay','overlay2');"/>        </tr>
        <tr><agiletags:LabelTextDisplayReq displayName="Group Name" elementName="swapGroupName"  /></tr>
    </table>
    <br>
    <label class="subheader">Broker Details</label>
    <hr>
    <br>
    <table>                        
        <tr><agiletags:LabelTextSearchTextDiv displayName="Broker Code" elementName="swapBrokerCode" mandatory="yes" onClick="document.getElementById('depSwapButtonPressed').value = 'SearchSwapBrokerButton'; getPageContents(document.forms['SwapMemberForm'],null,'overlay','overlay2');"/>        </tr>
        <tr><agiletags:LabelTextDisplayReq displayName="Broker Name" elementName="swapBrokerName" /></tr>
    </table>

    <br/>
    <table id="MemberSwapTable">
        <tr>
            <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}', '${main_div}');"></td>
            <td><input id="saveButton" name="saveButton" type="button" value="Save" onclick="document.getElementById('depSwapButtonPressed').value = 'SaveDepSwapButton';submitFormWithAjaxPost(this.form, 'MemberCover', null, '${main_div}', '${target_div}');"></td>
        </tr>
    </table>
</agiletags:ControllerForm>



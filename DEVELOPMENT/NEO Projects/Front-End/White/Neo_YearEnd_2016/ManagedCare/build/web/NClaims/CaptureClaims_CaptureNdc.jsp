<%-- 
    Document   : CaptureClaims_CaptureNdc
    Created on : 10 Mar 2016, 9:24:32 AM
    Author     : janf
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>

<nt:NeoPanel title="Capture NDC For Claim Line "  collapsed="false" reload="false" contentClass="neopanelcontentwhite">
    <c:set var="a" value="adsss"></c:set>
    <agiletags:ControllerForm name="ClaimLine_Ndc_Form">
        <input type="hidden" name="opperation" id="ClaimLine_opperation" value="ClaimLineNdcCommand" />
        <input type="hidden" name="onScreen" id="ClaimHeader_onScreen" value="Ndc" />
        <input type="hidden" name="ClaimLine_claimId" id="ClaimLine_claimId" value="${claimId}" />
        <input type="hidden" name="ClaimLine_Practice_Type" id="ClaimLine_Practice_Type" value="" />
        <input type="hidden" name="command" id="ClaimHeader_command" value="SaveNdc" />
        <input type="hidden" name="ClaimLine_ClaimLineId" id="ClaimLine_ClaimLineId" value=""/>
        <input type="hidden" name="claimLineNdcList" id="claimLineNdcList" value="${empty claimLineNdcList ? null : claimLineNdcList}}"/>

        <table id="ClaimLine_NCD_Capture">
            <tbody>
                <tr><nt:NeoTextField elementName="ClaimLine_NappiCode" displayName="NDC Code" onblur="findNappi($(this).val())"/></tr>
                <tr><nt:NeoTextField elementName="ClaimLine_NappiQuantity" displayName="NDC Quantity" value="1" onblur=""/></tr>
                <tr><nt:NeoTextField elementName="ClaimLine_NappiDosage" displayName="NDC Dosage" value="1" onblur=""/></tr>
                <tr><nt:NeoTextField elementName="ClaimLine_NappiAmount" displayName="NDC Amount" value="" onblur=""/></tr>
                <tr><nt:NeoTextField elementName="ClaimLine_NappiPrimary" displayName="Primary NDC" type="checkbox"/></tr>
            </tbody>
            <tbody id="ClaimLine_NdcTable_Buttons">
                <tr>
                    <td><input type="button" value="Close" onclick="returnFromNappi('true')"></td>
                    <td><input type="button" value="Save" onclick="saveNappi(this.form, null, this);"></td>
                </tr>
            </tbody>
        </table>
    </agiletags:ControllerForm>

</nt:NeoPanel>

<div id="ClaimLine_NDC_Summary">
    <jsp:include page="CaptureClaims_NdcSummary.jsp" />
</div> 


<%-- 
    Document   : WorkflowLogNewCall
    Created on : 2010/03/22, 02:26:26
    Author     : gerritr
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>

        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery.tools.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/Workflow/WorkflowTabUtil.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Workflow/Workflow.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script language="JavaScript">

            $(document).ready(function () {
                hideSubCategory();
                $("#missing-fields").hide();

                $("#doneButon").click(function () {

                    if (document.getElementsByName('callQ_0')[0].checked) {
                        //The logic for the new list
                        $("#claimsFC").show();
                        $("#claimsSC").show();
                        $("#claimsTC").show();
                    }
                    if (document.getElementsByName('callQ_1')[0].checked) {
                        //The logic for the new list
//                        $("#backOfficeFC").show(); Call Centre Admin
//                        $("#backOfficeSC").show();

                    }
                    if (document.getElementsByName('callQ_2')[0].checked) {
                        //The logic for the new list
                        $("#groupsFC").show();
//                        $("#groupSC").show();

                    }
                    if (document.getElementsByName('callQ_3')[0].checked) {
                        //The logic for the new list
                        $("#brokerFC").show();
//                        $("#brokerSC").show();

                    }
                    if (document.getElementsByName('callQ_4')[0].checked) {
                        //The logic for the new list
                        $("#wellnessFC").show();

                    }
                    if (document.getElementsByName('callQ_5')[0].checked) {
                        $("#brokerfirmFC").show();
                    }

                    if (document.getElementsByName('callQ_6')[0].checked) {
                        //The logic for the new list
                        $("#statementsFC").show();
                        $("#statementsTC").show();

                    }
                    if (document.getElementsByName('callQ_7')[0].checked) {
                        // The logic for the new list
                        var product = document.getElementById('product').value;
                        if (product === '1') {
                            $("#benefitsFCReso").show();
                        } else {
                            $("#benefitsFCSpec").show();
                        }
                        $("#benefitsSC").show();
                        $("#benefitsTC").show();
                    }
                    if (document.getElementsByName('callQ_8')[0].checked) {
                        // The logic for the new list
                        $("#prenmiumsFC").show();
                        $("#premiumsTC").show();

                    }
                    if (document.getElementsByName('callQ_9')[0].checked) {
                        $("#membershipFC").show();
                        $("#memberShipTC").show();

                    }
                    if (document.getElementsByName('callQ_10')[0].checked) {
                        $("#serviceProviderFC").show();
                        $("#serviceProviderTC").show();

                    }
                    if (document.getElementsByName('callQ_11')[0].checked) {
                        $("#chronicFC").show();
                        $("#chronicSC").show();
                    }
                    if (document.getElementsByName('callQ_12')[0].checked) {
                        $("#oncologyFC").show();
                        $("#oncologySC").show();
                    }
                    if (document.getElementsByName('callQ_13')[0].checked) {
                        $("#wellcareFC").show();
                        $("#wellcareSC").show();
                    }
                    $("#queryCategories").hide();
                    $("#doneButon").hide();
                });

                checkMandatoryFields();

            });


            function loadNewCallPage() {
                //toggle first
                toggleCallType();
                // toggleReferred();
                setCallDate();


                //alert(document.getElementById('searchCalled').value);
                if (document.getElementById('searchCalled').value == 'memberNumber') {
                    //alert(document.getElementById('memberNumber_text').value);
                    getPrincipalMemberForNumber(document.getElementById('memberNumber_text').value, 'memberNumber');
                }
                if (document.getElementById('searchCalled').value == 'providerNumber') {
                    //alert(document.getElementById('providerNumber_text').value);
                    validateProvider(document.getElementById('providerNumber_text').value, 'providerNumber');
                }
            }

            function populateRequests() {
                document.getElementById('callType').value = '${callType}';
                document.getElementById('callStatus').value = '${callStatus}';
                document.getElementById('notes').value = '${notes}';
                document.getElementById('uRName').value = '${uName}';
            }

            function toggleCallType() {
                var value = document.getElementById('callType').value;
                if (value == '2') {
                    var state = document.getElementById('providerType').style.display;
                    document.getElementById('providerType').style.display = '';
                    document.getElementById('popCover').style.display = 'none';
                    document.getElementById('popCoverSearch').style.display = 'none';
                    document.getElementById('memberGrid').style.display = 'none';
                    document.getElementById('popProvider').style.display = 'none';
                    document.getElementById('popProviderSearch').style.display = '';
                } else if (value == '5') {
                    var state = document.getElementById('providerType').style.display;
                    document.getElementById('providerType').style.display = '';
                    document.getElementById('popCover').style.display = '';
                    document.getElementById('popCoverSearch').style.display = 'none';
                    document.getElementById('memberGrid').style.display = 'none';
                    document.getElementById('popProvider').style.display = 'none';
                    document.getElementById('popProviderSearch').style.display = '';
                    document.getElementById('GroupValue').style.display = '';
                    document.getElementById('GroupName').style.display = '';
                } else {
                    document.getElementById('providerType').style.display = 'none';
                    document.getElementById('popCover').style.display = 'none';
                    document.getElementById('popCoverSearch').style.display = '';
                    document.getElementById('memberGrid').style.display = '';
                    document.getElementById('popProvider').style.display = 'none';
                    document.getElementById('popProviderSearch').style.display = 'none';
                }
            }

            function submitWithAction(action) {
                checkMandatoryFields();
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }


            function submitWithAction(action, forField, onScreen) {
                document.getElementById('onScreen').value = onScreen;
                document.getElementById('searchCalled').value = forField;
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }


            function hideRows(id) {
                document.getElementById(id).style.display = 'none';
                document.getElementById(id + 'plus').style.display = '';
                document.getElementById(id + 'minus').style.display = 'none';
            }


            function showRows(id) {
                document.getElementById(id).style.display = '';
                document.getElementById(id + 'minus').style.display = '';
                document.getElementById(id + 'plus').style.display = 'none';

            }



            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function getPrincipalMemberForNumber(str, element, form) {
                if (form !== null) {
                    $(form).mask("Loading...");
                }
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                document.getElementById('opperation').value = 'GetMemberByNumberCommand';
                encodeVal = encodeFormForSubmit(form);
                var url = "/ManagedCare/AgileController?";
                url += encodeVal + "&number=" + str + "&element=" + element;
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element);
                }
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function validateMember(str, element) {
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null) {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=WorkflowMemberDetailsCoverSearchCommand&memberNumber=" + str + "&exactCoverNum=1";
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element);
                };
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function encodeFormForSubmit(form) {
                var encStr = "formName=" + escape(form.name) + "&";
                for (var i = 0; i < form.elements.length; i++) {
                    var element = form.elements[i];
                    var elementType = element.type.toUpperCase();
                    var elementVal = "";
                    if (!element.name)
                        continue;
                    encStr += element.name + "=";
                    if (elementType == "CHECKBOX") {
                        elementVal = element.checked ? element.checked : "false";
                    } else {
                        elementVal = element.value;
                    }
                    encStr += escape(elementVal ? elementVal : "");
                    if (i < form.elements.length - 1)
                        encStr += "&";
                }
                return encStr;
            }

            function validateProvider(str, element) {
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=WorkflowFindPracticeByCodeCommand&providerNumber=" + str;
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element)
                };
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function validateGroup(str, element) {
                xmlhttp = GetXmlHttpObject();
                if (xmlhttp == null)
                {
                    alert("Your browser does not support XMLHTTP!");
                    return;
                }
                var url = "/ManagedCare/AgileController";
                url = url + "?opperation=WorkflowFindGroupsByNumberCommand&groupNum=" + str;
                xmlhttp.onreadystatechange = function () {
                    stateChanged(element)
                };
                xmlhttp.open("POST", url, true);
                xmlhttp.send(null);
            }

            function stateChanged(element) {
                if (xmlhttp.readyState == 4)
                {
                    // alert(xmlhttp.responseText);
                    var result = xmlhttp.responseText.substring(0, xmlhttp.responseText.indexOf('|'));
                    // alert(result);
                    if (result == "WorkflowFindPracticeByCodeCommand") {
                        var name = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=') + 1, xmlhttp.responseText.lastIndexOf('|'));
                        var discipline = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('=') + 1, xmlhttp.responseText.lastIndexOf('$'));
                        document.getElementById('discipline').value = discipline;
                        document.getElementById('provName').value = name;
                        document.getElementById(element + '_error').innerHTML = '';
                    } else if (result == "WorkflowFindGroupsByNumberCommand") {
                        var name = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('=') + 1, xmlhttp.responseText.lastIndexOf('$'));
                        document.getElementById('groupName_text').value = name;
                        document.getElementById(element + '_error').innerHTML = '';
                    } else if (result == "GetMemberByNumberCommand") {
                        var resultArr = xmlhttp.responseText.split("|");
                        var numberArr = resultArr[1].split("=");
                        var emailArr = resultArr[2].split("=");
                        var productArr = resultArr[3].split("=");
                        var entityArr = resultArr[4].split("=");

                        var number = numberArr[1];
                        var productId = productArr[1];
                        var email = emailArr[1];
                        var entityID = entityArr[1];

                        document.getElementById(element + '_error').innerHTML = '';
                        document.getElementById(element + '_text').value = number;
                        document.getElementById('productId').value = productId;
                        document.getElementById('entityID_text').value = entityID;
                        var sEntity_Id = "" + entityID.toString();
                        sEntity_Id = sEntity_Id.slice(0, -3);
                        document.getElementById('memberEntityId').value = sEntity_Id;
                        document.getElementById('memberCoverEntityId').value = sEntity_Id;

                        if (email != null && email != "") {
                            document.getElementById('callerContact').value = email;
                        } else if (number != null && number != "") {
                            document.getElementById('callerContact').value = number;
                        }

                        //document.getElementById('searchCalled').value = "";
            <%
                request.setAttribute("exactCoverNum", "1");
            %>
                        submitWithAction("WorkflowMemberDetailsCoverSearchCommand", element + '_text', "/Calltrack/WorkflowLogNewCall.jsp");
                    } else if (result == "Error") {
                        var forCommand = xmlhttp.responseText.substring(xmlhttp.responseText.lastIndexOf('|') + 1, xmlhttp.responseText.length);
                        if (forCommand == "WorkflowFindPracticeByCodeCommand") {
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById('provName').value = '';
                            document.getElementById('discipline').value = '';
                        } else if (forCommand == "WorkflowFindGroupsByNumberCommand") {
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById('groupNum_text').value = '';
                            document.getElementById('groupName_text').value = '';
                        } else if (forCommand == "GetMemberByNumberCommand") {
                            document.getElementById(element + '_error').innerHTML = xmlhttp.responseText.substring(xmlhttp.responseText.indexOf('|') + 1, xmlhttp.responseText.lastIndexOf('|'));
                            document.getElementById(element + '_text').value = '';
                        }
                    }
                }
            }


            function hideSubCategory() {
                $("#disciplineCategory").hide();
                $("#benefitSub").hide();
                $("#claimSub").hide();
                $("#membershipSub").hide();
                $("#UnderwritingSub").hide();
                $("#queryCategories").show();

                // new categories
                $("#claimsFC").hide();
                $("#claimsSC").hide();
                $("#claimsTC").hide();
                $("#statementsFC").hide();
                $("#statementsTC").hide();
                $("#wellnessFC").hide();
                $("#benefitsFCReso").hide();
                $("#benefitsFCSpec").hide();
                $("#benefitsSC").hide();
                $("#benefitsTC").hide();
                $("#prenmiumsFC").hide();
                $("#premiumsTC").hide();
                $("#membershipFC").hide();
                $("#memberShipTC").hide();
                $("#serviceProviderFC").hide();
                $("#serviceProviderTC").hide();
                $("#chronicFC").hide();
                $("#chronicSC").hide();
                $("#oncologyFC").hide();
                $("#oncologySC").hide();
                $("#wellcareFC").hide();
                $("#wellcareSC").hide();
                $("#backOfficeFC").hide();
                $("#backOfficeSC").hide();
                $("#groupsFC").hide();
                $("#groupsSC").hide();
                $("#brokerFC").hide();
                $("#brokerSC").hide();
                $("#foundationFC").hide();
                $("#foundationSC").hide();
                $("#brokerfirmFC").hide();
            }
            function checkMandatoryFields() {
            <%  if ((request.getAttribute("mandatoryfailed")) != null && (!request.getAttribute("mandatoryfailed").toString().equalsIgnoreCase("null")) && (!request.getAttribute("mandatoryfailed").toString().equalsIgnoreCase(""))) {

            %>

                $("#missing-fields").dialog({modal: true, width: 550,
                    buttons: {
                        'Done': function () {
                            $(this).dialog('close');
                        }
                    }
                });

            <%                    request.setAttribute("mandatoryfailed", null);

                }%>

            }

            function setCallDate() {

                var date = new Date();

                var year = date.getFullYear();
                var month = (date.getMonth() + 1);
                var day = date.getDate();

                if (month < 10) {
                    month = "0" + month;
                }

                if (day < 10) {
                    day = "0" + day;
                }

                var dateStr = "CT" + year + "" + month + "" + day + "N#";
                $("#callTrackNumber").val(dateStr);
                //document.getElementById('callTrackId').value = dateStr;
                $("#callTrackId").val(dateStr);
                //var authFromDate = $("#authFromDate").val(dateStr);

            }


            function ForwardToCC(number, type) {
                if (type != undefined && type === 'Member') {
                    window.open('/ManagedCare/AgileController?opperation=ViewCoverClaimsCommand&policyNumber_text=' + number + '&onScreen=MemberCCLogin', 'worker');
                } else if (type != undefined && type === 'Provider') {
                    window.open('/ManagedCare/AgileController?opperation=ViewProviderDetailsCommand&provNum_text=' + number + '&onScreen=MemberCCLogin', 'worker');
                }
            }


        </script>
    </head>

    <body onload="if (parent.document.getElementById('canRefresh').value === '1' && parent.document.getElementById('actionDesc').value === 'Log new call') {
                parent.document.getElementById('canRefresh').value = '0';
                submitWithAction('WorkflowContentCommand', null, 'Calltrack/WorkflowLogNewCall.jsp');
            }
            if (parent.document.getElementById('actionDesc').value === 'Log new call') {
                loadNewCallPage();
            }
            
            populateRequests();
            toggleCallType();
            if (parent.document.getElementById('actionDesc').value !== 'Log new call') {
                if (${not empty memberNum}) {
                    getPrincipalMemberForNumber('${memberNum}', 'memberNumber', null);
                } else if (${not empty memberNumber_text}) {
                    getPrincipalMemberForNumber('${memberNumber_text}', 'memberNumber', null);
                }
                if (${not empty provNum}) {
                    validateProvider('${provNum}', 'provNum');
                }else if (${not empty providerNumber_text}) {
                    validateProvider('${providerNumber_text}', 'providerNumber');
                }
            } else {
                document.getElementById('actionDesc').value = parent.document.getElementById('actionDesc').value;
                toggleCallType();
            }">
        <div align="Center">
            <div id="missing-fields" title="Missing Fields"><p>You must select at least one of the Query Categories</p></div>
            <table width=100% height=100%><tr valign="center"><td width="50px"></td><td align="left">
                        <!-- content goes here -->
                        <label class="header">Log New Call</label>
                        <br/>
                        <table>
                            <agiletags:ControllerForm name="saveCallInfo" validate="yes">
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <input type="hidden" name="productId" id="productId" value="" />
                                <input type="hidden" name="product" id="product" value="${product}" />
                                <input type="hidden" name="actionDesc" id="actionDesc" value="" />
                                <input type="hidden" name="entityID_text" id="entityID_text" value="" />
                                <input type="hidden" name="trackId" id="trackId" value="${trackId}" />
                                <input type="hidden" name="memberCoverEntityId" id="memberCoverEntityId" value="${memberCoverEntityId}" />
                                <input type="hidden" name="memberEntityId" id="memberEntityId" value="" />
                                <input type="hidden" name="onScreen" id="onScreen" value="Calltrack/WorkflowLogNewCall.jsp" />

                                <agiletags:HiddenField elementName="searchCalled"/>

                                <table>

                                    <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Call Type" elementName="callType" lookupId="108" javaScript="onchange=\"toggleCallType();\"" errorValueFromSession="yes" mandatory="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Call Number" elementName="callTrackNumber" valueFromRequest="callTrackNumber" readonly="yes"/></tr>

                                    <c:if test="${empty sessionScope.callQueries2}">
                                        <div id="callStatus" style="display: block">
                                        </c:if>
                                        <c:if test="${not empty sessionScope.callQueries2}">
                                            <div id="callStatus" style="display: none">
                                            </c:if>
                                            <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Call Status" elementName="callStatus" lookupId="105" javaScript="" errorValueFromSession="yes" mandatory="yes" /></tr>
                                        </div>

                                        <tr id="popCoverSearch" style="display:none" align="left">
                                            <td align="left" width="160px">
                                                <label>Member Number:</label>
                                            </td>
                                            <td align="left" width="200px">
                                                <input type="text" id="memberNumber_text" name="memberNumber_text" value="${memberNumber_text}" size="30" onchange="getPrincipalMemberForNumber(this.value, 'memberNumber', this.form);">
                                            </td>
                                            <td>
                                                <label class="red">*</label>
                                            </td>
                                            <td align="left">
                                                <img src="/ManagedCare/resources/ForwardToCC.gif" width="28" height="28" alt="Search" border="0" onclick="ForwardToCC(document.getElementById('memberNumber_text').value, 'Member');">
                                            </td>
                                            <td width="200px" align="left">
                                                <label id="memberNumber_error" class="error"></label>
                                            </td>
                                        </tr>
                                        <tr id="popProvider" style="display:none" align="left">
                                            <td align="left" width="160px">
                                                <label>Provider Number:</label>
                                            </td>
                                            <td align="left" width="200px">
                                                <input type="text" id="provNum" name="provNum" value="${provNum}" size="30" onchange="validateProvider(this.value, 'provNum');">
                                            </td>
                                            <td>
                                                <label class="red">*</label>
                                            </td>
                                            <td align="left">
                                                <img src="/ManagedCare/resources/ForwardToCC.gif" width="28" height="28" alt="Search" border="0" onclick="ForwardToCC(document.getElementById('provNum').value, 'Provider');">
                                            </td>
                                            <td width="200px" align="left">
                                                <label id="providerNumber_error" class="error"></label>
                                            </td>
                                        </tr>
                                        <tr id="popProviderSearch" style="display:none" align="left">
                                            <td align="left" width="160px">
                                                <label>Provider Number:</label>
                                            </td>
                                            <td align="left" width="200px">
                                                <input type="text" id="providerNumber_text" name="providerNumber_text" value="${providerNumber_text}" size="30" onchange="validateProvider(this.value, 'providerNumber');">
                                            </td>
                                            <td>
                                                <label class="red">*</label>
                                            </td>
                                            <td align="left">
                                                <img src="/ManagedCare/resources/ForwardToCC.gif" width="28" height="28" alt="Search" border="0" onclick="ForwardToCC(document.getElementById('providerNumber_text').value, 'Provider');">
                                            </td>
                                            <td width="200px" align="left">
                                                <label id="providerNumber_error" class="error"></label>
                                            </td>
                                        </tr>
                                        <tr id="popCover" style="display:none" align="left">
                                            <td align="left" width="160px">
                                                <label>Member Number:</label>
                                            </td>
                                            <td align="left" width="200px">
                                                <input type="text" id="memberNum" name="memberNum" value="${memberNum}" size="30" onchange="getPrincipalMemberForNumber(this.value, 'memberNumber', this.form);">
                                            </td>
                                            <td>
                                                <label class="red">*</label>
                                            </td>
                                            <td align="left">
                                                <img src="/ManagedCare/resources/ForwardToCC.gif" width="28" height="28" alt="Search" border="0" onclick="ForwardToCC(document.getElementById('memberNum').value, 'Member');">
                                            </td>
                                            <td width="200px" align="left">
                                                <label id="memberNumber_error" class="error"></label>
                                            </td>
                                        <tr id="providerType" style="display:none">
                                            <agiletags:LabelTextBoxError displayName="Provider Name" elementName="provName"  valueFromRequest="provName"/>
                                            <agiletags:LabelTextBoxError displayName="Discipline" elementName="discipline" valueFromRequest="discipline" readonly="yes"/>
                                        </tr>
                                        <tr id="GroupValue" style="display:none" align="left">
                                            <td align="left" width="160px">
                                                <label>Group Number:</label>
                                            </td>
                                            <td align="left" width="200px">
                                                <input type="text" id="groupNum_text" name="groupNum_text" value="${groupNum_text}" size="30" onchange="validateGroup(this.value, 'groupNum');">
                                            </td>
                                            <td>
                                                <label class="red">*</label>
                                            </td>
                                            <!--                                        <td align="left">
                                                                                        <img src="/ManagedCare/resources/ForwardToCC.gif" width="28" height="28" alt="Search" border="0" onclick="ForwardToCC(document.getElementById('providerNumber_text').value, 'Provider');">
                                                                                    </td>-->
                                            <td width="200px" align="left">
                                                <label id="groupNum_error" class="error"></label>
                                            </td>
                                        </tr>
                                        <tr id="GroupName" style="display:none" align="left">
                                            <td align="left" width="160px">
                                                <label>Group Name:</label>
                                            </td>
                                            <td align="left" width="200px">
                                                <input type="text" id="groupName_text" name="groupName_text" value="${groupName_text}" size="30" readonly>
                                            </td>
                                        </tr>
                                        <tr><agiletags:LabelTextBoxError displayName="Caller Name" elementName="callerName" mandatory="yes" valueFromRequest="callerName"/></tr>
                                        <tr><agiletags:LabelTextBoxError displayName="Contact Details" elementName="callerContact" mandatory="yes" valueFromRequest="callerContact"/></tr>

                                        <c:if test="${empty sessionScope.callQueries2}">
                                            <agiletags:LabelNeoLookupValueCheckbox displayName="Contact Type" elementName="queryMethod" lookupId="150" javascript="" />
                                        </c:if>
                                        <c:if test="${not empty sessionScope.callQueries2}">
                                            <tr id="contactType"><agiletags:QueriesCheckBoxTag displayName="Query Method" elementName="queryMethod" lookupId="150" javascript="" /></tr>
                                        </c:if>

                                        <tr><td colspan="6"><div id="memberGrid" style="display:none">
                                                    <agiletags:MemberCoverSearchTag commandName="" javaScript="" />
                                                </div></td></tr>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="queryCategories"><agiletags:LabelNeoLookupValueCheckbox displayName="Query Categories" elementName="callQ" lookupId="151" javascript="" /></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="queryCategories"><agiletags:QueriesCheckBoxTag displayName="Query Categories" elementName="callQ" lookupId="151" javascript="" /></tr>    
                                            </c:if>
                                        </div>
                                        <div>
                                            <tr id="doneButon"><td colspan="5" align="left">
                                                    <button name="opperation" type="button" value="Done">Done</button>
                                                </td></tr>
                                        </div>

                                        <!-- Claims -->
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr class="claimsLeve11" id="claimsFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Claims First Category" elementName="claimsFC" lookupId="152" javascript=""/></tr>
                                            </c:if>

                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr class="claimsLeve11" id="claimsFC"><agiletags:QueriesCheckBoxTag displayName="Claims First Category" elementName="claimsFC" lookupId="152" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr class="claimsLevel2" id="claimsSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Claims Second Category" elementName="claimsSC" lookupId="153" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr class="claimsLevel2" id="claimsSC"><agiletags:QueriesCheckBoxTag displayName="Claims Second Category" elementName="claimsSC" lookupId="153" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr class="claimsLeve3" id="claimsTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Claims Third Category" elementName="claimsTC" lookupId="154" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr class="claimsLeve3" id="claimsTC"><agiletags:QueriesCheckBoxTag displayName="Claims Third Category" elementName="claimsTC" lookupId="154" javascript=""/></tr>
                                            </c:if>
                                        </div>

                                        <!-- Back Office -->
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="backOfficeFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Back Office First Category" elementName="backOfficeFC" lookupId="155" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="backOfficeFC"><agiletags:QueriesCheckBoxTag displayName="Back Office First Category" elementName="backOfficeFC" lookupId="155" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="backOfficeSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Back Office Second Category" elementName="backOfficeSC" lookupId="156" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="backOfficeSC"><agiletags:QueriesCheckBoxTag displayName="Back Office Second Category" elementName="backOfficeSC" lookupId="156" javascript=""/></tr>
                                            </c:if>
                                        </div>

                                        <!-- Groups -->
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="groupsFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Groups First Category" elementName="groupsFC" lookupId="363" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="groupsFC"><agiletags:QueriesCheckBoxTag displayName="Groups First Category" elementName="groupsFC" lookupId="363" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="groupsSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Groups Second Category" elementName="groupsSC" lookupId="156" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="groupsSC"><agiletags:QueriesCheckBoxTag displayName="Groups Second Category" elementName="groupsSC" lookupId="156" javascript=""/></tr>
                                            </c:if>
                                        </div>

                                        <!-- Brokers -->
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="brokerFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Brokers First Category" elementName="brokerFC" lookupId="364" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="brokerFC"><agiletags:QueriesCheckBoxTag displayName="Brokers First Category" elementName="brokerFC" lookupId="364" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="brokerSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Brokers Second Category" elementName="brokerSC" lookupId="156" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="brokerSC"><agiletags:QueriesCheckBoxTag displayName="Brokers Second Category" elementName="brokerSC" lookupId="156" javascript=""/></tr>
                                            </c:if>
                                        </div>

                                        <!-- Broker Firm -->
                                        <div>
                                            <tr id="brokerfirmFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Broker Firm First Category" elementName="brokerfirmFC" lookupId="368" javascript=""/></tr>
                                        </div>

                                        <!-- Wellness -->
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="wellnessFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Wellness First Category" elementName="wellnessFC" lookupId="365" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="wellnessFC"><agiletags:QueriesCheckBoxTag displayName="Wellness First Category" elementName="wellnessFC" lookupId="365" javascript=""/></tr>
                                            </c:if>
                                        </div>

                                        <!-- Statement -->
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="statementsFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Statements First Category" elementName="statementsFC" lookupId="155" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="statementsFC"><agiletags:QueriesCheckBoxTag displayName="Statements First Category" elementName="statementsFC" lookupId="155" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="statementsTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Statements Second Category" elementName="statementsTC" lookupId="156" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="statementsTC"><agiletags:QueriesCheckBoxTag displayName="Statements Second Category" elementName="statementsTC" lookupId="156" javascript=""/></tr>
                                            </c:if>
                                        </div>

                                        <!-- Benefits -->
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="benefitsFCReso"><agiletags:LabelNeoLookupValueCheckbox displayName="Benefits First Category" elementName="benefitsFCReso" lookupId="157" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="benefitsFCReso"><agiletags:QueriesCheckBoxTag displayName="Benefits First Category" elementName="benefitsFCReso" lookupId="157" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="benefitsFCSpec"><agiletags:LabelNeoLookupValueCheckbox displayName="Benefits First Category" elementName="benefitsFCSpec" lookupId="284" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="benefitsFCSpec"><agiletags:QueriesCheckBoxTag displayName="Benefits First Category" elementName="benefitsFCSpec" lookupId="284" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="benefitsSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Benefits Second Category" elementName="benefitsSC" lookupId="158" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="benefitsSC"><agiletags:QueriesCheckBoxTag displayName="Benefits Second Category" elementName="benefitsSC" lookupId="158" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="benefitsTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Benefits Third Category" elementName="benefitsTC" lookupId="159" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="benefitsTC"><agiletags:QueriesCheckBoxTag displayName="Benefits Third Category" elementName="benefitsTC" lookupId="159" javascript=""/></tr>
                                            </c:if>
                                        </div>

                                        <!-- Premiums -->
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="prenmiumsFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Premiums First Category" elementName="prenmiumsFC" lookupId="160" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="prenmiumsFC"><agiletags:QueriesCheckBoxTag displayName="Premiums First Category" elementName="prenmiumsFC" lookupId="160" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="premiumsTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Premiums Second Category" elementName="premiumsTC" lookupId="161" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="premiumsTC"><agiletags:QueriesCheckBoxTag displayName="Premiums Second Category" elementName="premiumsTC" lookupId="161" javascript=""/></tr>
                                            </c:if>
                                        </div>

                                        <!-- Membership -->
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="membershipFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Membership First Category" elementName="membershipFC" lookupId="162" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="membershipFC"><agiletags:QueriesCheckBoxTag displayName="Membership First Category" elementName="membershipFC" lookupId="162" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="memberShipTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Membership Second Category" elementName="memberShipTC" lookupId="163" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="memberShipTC"><agiletags:QueriesCheckBoxTag displayName="Membership Second Category" elementName="memberShipTC" lookupId="163" javascript=""/></tr>
                                            </c:if>
                                        </div>

                                        <!-- Service Provider -->
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="serviceProviderFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Service Provider First Category" elementName="serviceProviderFC" lookupId="164" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="serviceProviderFC"><agiletags:QueriesCheckBoxTag displayName="Service Provider First Category" elementName="serviceProviderFC" lookupId="164" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="serviceProviderTC"><agiletags:LabelNeoLookupValueCheckbox displayName="Service Provider Second Category" elementName="serviceProviderTC" lookupId="165" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="serviceProviderTC"><agiletags:QueriesCheckBoxTag displayName="Service Provider Second Category" elementName="serviceProviderTC" lookupId="165" javascript=""/></tr>
                                            </c:if>
                                        </div>

                                        <!-- Chronic -->
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="chronicFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Chronic First Category" elementName="chronicFC" lookupId="357" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="chronicFC"><agiletags:QueriesCheckBoxTag displayName="Chronic First Category" elementName="chronicFC" lookupId="357" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="chronicSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Chronic Second Category" elementName="chronicSC" lookupId="358" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="chronicSC"><agiletags:QueriesCheckBoxTag displayName="Chronic Second Category" elementName="chronicSC" lookupId="358" javascript=""/></tr>
                                            </c:if>
                                        </div>

                                        <!-- Oncology -->
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="oncologyFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Oncology First Category" elementName="oncologyFC" lookupId="359" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="oncologyFC"><agiletags:QueriesCheckBoxTag displayName="Oncology First Category" elementName="oncologyFC" lookupId="359" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="oncologySC"><agiletags:LabelNeoLookupValueCheckbox displayName="Oncology Second Category" elementName="oncologySC" lookupId="360" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="oncologySC"><agiletags:QueriesCheckBoxTag displayName="Oncology Second Category" elementName="oncologySC" lookupId="360" javascript=""/></tr>
                                            </c:if>
                                        </div>

                                        <!-- Wellcare -->
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="wellcareFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Wellcare First Category" elementName="wellcareFC" lookupId="361" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="wellcareFC"><agiletags:QueriesCheckBoxTag displayName="Wellcare First Category" elementName="wellcareFC" lookupId="361" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="wellcareSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Wellcare Second Category" elementName="wellcareSC" lookupId="362" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="wellcareSC"><agiletags:QueriesCheckBoxTag displayName="Wellcare Second Category" elementName="wellcareSC" lookupId="362" javascript=""/></tr>
                                            </c:if>
                                        </div>

                                        <!-- Foundation -->
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="foundationFC"><agiletags:LabelNeoLookupValueCheckbox displayName="Foundation First Category" elementName="foundationFC" lookupId="166" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="foundationFC"><agiletags:QueriesCheckBoxTag displayName="Foundation First Category" elementName="foundationFC" lookupId="166" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${empty sessionScope.callQueries2}">
                                                <tr id="foundationSC"><agiletags:LabelNeoLookupValueCheckbox displayName="Foundation Second Category" elementName="foundationSC" lookupId="167" javascript=""/></tr>
                                            </c:if>
                                            <c:if test="${not empty sessionScope.callQueries2}">
                                                <tr id="foundationSC"><agiletags:QueriesCheckBoxTag displayName="Foundation Second Category" elementName="foundationSC" lookupId="167" javascript=""/></tr>
                                            </c:if>
                                        </div>
                                </table>
                                <table>
                                    <!-- End of the new lookups -->
                                    <tr><agiletags:LabelTextAreaErrorBig displayName="Notes" elementName="notes" valueFromSession="no" /></tr> 
                                    <tr><agiletags:LabelTextBoxError displayName="Requested By" readonly="yes" elementName="uRName" valueFromRequest="uRName"/></tr>

                                    <tr>
                                        <c:if test="${empty sessionScope.callQueries2}">
                                            <agiletags:ButtonOpperation commandName="WorkflowSaveCallTrackDetailsCommand" align="left" displayname="Submit" span="1" type="button"  javaScript="onClick=\"submitWithAction('WorkflowSaveCallTrackDetailsCommand'),checkMandatoryFields()\";"/>
                                        </c:if>
                                        <c:if test="${not empty sessionScope.callQueries2}">
                                            <agiletags:ButtonOpperation commandName="WorkflowUpdateLoadedCommand" align="left" displayname="Update" span="1" type="button"  javaScript="onClick=\"submitWithAction('WorkflowUpdateLoadedCommand'),checkMandatoryFields()\";"/>
                                        </c:if>
                                        <agiletags:ButtonOpperation commandName="WorkflowReloadCallLogCommand" align="left" displayname="Cancel" span="1" type="button"  javaScript="onClick=\"parent.document.getElementById('canRefresh').value == '1'; parent.document.getElementById('actionDesc').value == 'Log new call'; submitWithAction('WorkflowReloadCallLogCommand')\";"/>
                                    </tr>
                                </table>
                                <table  align="right">
                                    <tr>
                                        <agiletags:LabelTextBoxError displayName="User Name" elementName="uName" readonly="yes" valueFromRequest="uName"/>
                                    </tr>
                                    <tr>
                                        <agiletags:LabelTextBoxError displayName="Date" elementName="disDate" readonly="yes" valueFromRequest="disDate"/>
                                    </tr>
                                </table>
                            </agiletags:ControllerForm>
                        </table>

                        <!-- content ends here -->
                    </td></tr></table>
        </div>

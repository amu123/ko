<%-- 
    Document   : EditClaims_ClaimLineSummary
    Created on : 13 May 2016, 2:35:13 PM
    Author     : janf
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>

<nt:NeoPanel title="Claim Lines for Claim ${claimId}"  collapsed="false" reload="false">
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" colspan="5" width="100%">
        <thead>
            <tr>
                <th>Claim Line</th>
                <th>Dependant</th>
                <th>Service Date</th>
                <th>Diagnosis</th>
                <th>Tariff</th>
                <th>Amount</th>
                <th>Edit</th>
                <!--<th></th>-->
            </tr>
        </thead>
        <tbody>
            <c:forEach var="item" items="${ClaimLines}">
                <c:choose>
                    <c:when test="${item.claimLineId == editClaimLineId}" >
                        <tr style="background-color: #63e76c">
                    </c:when>
                    <c:otherwise>
                    <tr>
                    </c:otherwise>
                </c:choose>
                    <td><label>${item.claimLineId}</label></td>
                    <td><label>${item.insuredPersonDependentCode}</label></td>
                    <td><label>${agiletags:formatXMLGregorianDate(item.serviceDateFrom)}</label></td>
                    <td><label>
                            <c:forEach var="itm" items="${item.icd10}">${itm.icd10Code} </c:forEach>
                            </label>
                        </td>
                        <td><label>${item.tarrifCodeNo}</label></td>
                    <td><label>${item.claimedAmount}</label></td>
                    <c:choose>
                        <c:when test="${item.chequeRunDate == null}">
                            <td><label><input type="button" value="Select" onclick="editClaimLine(this.form,${item.claimLineId})"</label></td>
                                </c:when>
                                <c:otherwise>
                            <td><label></label></td>
                        </c:otherwise>
                    </c:choose>
                    <!--<td><label><input type="button" value="Select" onclick="editClaimLine(this.form,\${item.claimLineId})"</label></td>-->
                    <!--<td><label><input type="button" value="Delete" onclick=""</label></td>-->
                </tr>
            </c:forEach>
        </tbody>
    </table>
</nt:NeoPanel>    
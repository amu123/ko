<%-- 
    Document   : PdcUserDropDownTag
    Created on : 02 Feb 2015, 4:00:28 PM
    Author     : chrisdp
--%>

<%@tag description="PDC User details drop down" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 

<%-- The list of normal or fragment attributes can be specified here: --%>

            <td>Assign to:</td>
            <td>
                <select name="pdcNames">
                   <c:forEach items="${sessionScope.pdcUserDetails}" var="entry">
                        <option value="${entry.value}">${entry.meaning}</option>
                    </c:forEach>
                </select>
            </td>
            <%--<c:if test="${!empty sessionScope.pdcUserDetails}"> 
            </c:if> --%>


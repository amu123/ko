<%-- 
    Document   : ScheduleTask
    Created on : 2010/05/27, 09:58:55
    Author     : princes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PDC WorkBench</title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script>
            $(document).ready(function () {
                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.forms[0].submit();
            }

            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function hideContact(value) {
                if (value === 1) {
                    document.getElementById("contact").style.display = 'none';
                } else {
                    document.getElementById("contact").style.display = 'block';
                }
            }
        </script>

    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">
                    <agiletags:PDCTabs tabSelectedIndex="link_ME" />
                    <fieldset id="pageBorder">
                        <div class="content_area">
                            <agiletags:ControllerForm name="viewWorkBench">
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <input type="hidden" name="listIndex" id="listIndex" value="" />
                                <label class="header">Manage My Patient</label>
                                <br/>
                                <br/>
                                <table>
                                    <tr><agiletags:LabelTextBoxError displayName="Member Name" elementName="patientName" valueFromSession="yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Member Surname" elementName="surname" valueFromSession="yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Policy Number" elementName="policyNumber" valueFromSession="yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Option Name" elementName="optionName" valueFromSession="yes" readonly="yes"/></tr>
                                </table>
                                <table width="224px">
                                    <tr><agiletags:LabelNeoLookupValueDropDown displayName="Do you want to close the event?" elementName="closeEvent" lookupId="67" javaScript="onChange=\"hideContact(this.value)\";"/></tr>
                                </table>
                                <table>
                                    <tbody id="contact" style="display:block">
                                        <tr><agiletags:LabelNeoLookupValueDropDown displayName="Task" elementName="task" lookupId="98"/></tr>
                                        <tr><agiletags:LabelTextBoxDateTime elementName="scheduleTask" displayName="Task Schedule" mandatory="yes"/></tr>
                                    </tbody>
                                </table>
                                <table>
                                    <tr>
                                        <agiletags:ButtonOpperation align="left" displayname="Save" commandName="SaveScheduledTaskCommand" span="3" type="button" javaScript="onClick=\"submitWithAction('SaveScheduledTaskCommand')\";"/>
                                    </tr>
                                </table>
                                <br/>
                            </agiletags:ControllerForm>
                        </div></fieldset>
                </td></tr></table>
    </body>
</html>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    function showDeaseasedReason(val) {
        if (val == 8) {
            document.getElementById("deaseasedReasonRow").removeAttribute("hidden");
        } else {
            document.getElementById("deaseasedReasonRow").setAttribute('hidden', 'true');
            var select = document.getElementById('deseasedReason');
            $('#' + select.id).val('').change();
        }
    }
</script>

<agiletags:ControllerForm name="ResignMemberForm">
    <input type="hidden" name="opperation" id="opperation" value="SaveMemberCoverDetailsCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="ResignMember" />
    <input type="hidden" name="buttonPressed" id="disorderValuesButtonPressed" value="" />
    <input type="hidden" name="dropDownPressed" id="resignReasonDropDownPressed" value="" />
    <input type="hidden" name="memberEntityId" id="ResignEntityId" value="${memberCoverEntityId}" />
    <input type="hidden" name="brokerEntityId" id="brokerEntityId" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="memberEntityId" id="memberCoverDetailsEntityId" value="${sessionScope.memberCoverEntityId}">
    <label class="header">Resign Member</label>
    <br/>
    <br/>
    <hr>
    <table> 
        <tr id="resignReasonRow">
            <td align="left" width="160px"><label>Dependant</label></td><td align="left" width="200px">
                <select style="width:215px" name="deptList" id="resignDeptList">
                    <c:forEach var="entry" items="${MemberCoverDependantDetails}">
                        <%--<c:if test="${entry.status == 'Active' || entry.status == 'Suspend' && agiletags:formatXMLGregorianDate(entry.coverEndDate) == '2999/12/31'}"> --%>
                        <c:if test="${(entry.status == 'Active' && agiletags:formatXMLGregorianDate(entry.coverEndDate) == '2999/12/31') || (entry.status == 'Suspend' && agiletags:formatXMLGregorianDate(entry.coverEndDate) == '2999/12/31')}">
                            <option value="${entry.dependentNumber}" selected>${entry.dependentNumber} ${entry.name} ${agiletags:formatXMLGregorianDate(entry.dateOfBirth)}</option>
                        </c:if>
                    </c:forEach> 
                </select>
            </td>
        </tr>
        <!--<tr id="resignReasonRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Resign Reason"  elementName="resignReason" lookupId="204" mandatory="yes" errorValueFromSession="yes"/>        </tr>-->
        <tr id="resignReasonRow"><agiletags:LabelNeoLookupDropDownErrorReq displayName="Resign Reason" elementName="resignReason" mandatory="yes" errorValueFromSession="yes" javaScript="onchange=\"showDeaseasedReason(this.value)\";"/></tr>
        <tr id="deaseasedReasonRow" hidden="true"><agiletags:LabelNeoLookupDropDownReq displayName="Deseased Reason" elementName="deseasedReason" mandatory="no" errorValueFromSession="yes"/></tr>
        <tr id="resignStartDateRow"><agiletags:LabelTextBoxDateReq displayname="Start Date" elementName="resignStartDate" valueFromSession="yes" mandatory="yes"/></tr> 
    </table>
    <br/>
    <br/>
    <table id="MemberResignTable">
        <tr>
            <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
            <td><input type="button" value="Save" onclick="resignMember( this,'${main_div}', '${target_div}');"></td>
        </tr>
    </table>
</agiletags:ControllerForm>
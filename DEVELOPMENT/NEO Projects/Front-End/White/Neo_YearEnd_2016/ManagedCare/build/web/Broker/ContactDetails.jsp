<%-- 
    Document   : ContactDetails
    Created on : 2012/06/18, 11:37:49
    Author     : princes
--%>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
  <agiletags:ControllerForm name="MemberApplicationForm">
    <input type="hidden" name="opperation" id="MemberApp_opperation" value="SaveBrokerFirmContactDetailsCommand" />
    <input type="hidden" name="onScreen" id="MemberApp_onScreen" value="Member Application" />
    <input type="hidden" name="brokerFirmEntityId" id="brokerFirmContactEntityId" value="${requestScope.brokerFirmEntityId}" />
    <input type="hidden" name="brokerFirmTile" id="brokerFirmContactTile" value="${requestScope.brokerFirmTile}" />
    <input type="hidden" name="brokerFirmContactPerson" id="brokerFirmPerson" value="${requestScope.brokerFirmContactPerson}" />
    <input type="hidden" name="brokerFirmContactSurname" id="brokerFirmSurname" value="${requestScope.brokerFirmContactSurname}" />
    <input type="hidden" name="firmRegion" id="firmRegionContact" value="${requestScope.firmRegion}" />

    <agiletags:HiddenField elementName="MemberApp_app_number"/>

    <label class="subheader">Contact Details</label>
    <hr>
      <table id="FirmContactDetailsTable">
        <tr id="FirmApp_ContactDetails_titleRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Title"  elementName="FirmApp_ContactDetails_title" lookupId="24" mandatory="no" errorValueFromSession="yes"/>        </tr>
        <tr id="FirmApp_ContactDetails_contactPersonRow"><agiletags:LabelTextBoxErrorReq displayName="Contact Person" elementName="FirmApp_ContactDetails_contactPerson" valueFromRequest="FirmApp_ContactDetails_contactPerson" mandatory="no"/>        </tr>
        <tr id="FirmApp_ContactDetails_telNoRow"><agiletags:LabelTextBoxErrorReq displayName="Telephone Number" elementName="FirmApp_ContactDetails_telNo" valueFromRequest="FirmApp_ContactDetails_telNo" mandatory="FirmApp_ContactDetails_telNo"/>        </tr>
        <tr id="FirmApp_ContactDetails_faxNoRow"><agiletags:LabelTextBoxErrorReq displayName="Fax No" elementName="FirmApp_ContactDetails_faxNo" valueFromRequest="FirmApp_ContactDetails_faxNo" mandatory="no"/>        </tr>
        <tr id="FirmApp_ContactDetails_emailRow"><agiletags:LabelTextBoxErrorReq displayName="Email Address" elementName="FirmApp_ContactDetails_email" valueFromRequest="FirmApp_ContactDetails_email" mandatory="no"/>        </tr>
        <tr id="FirmApp_ContactDetails_altEmailRow"><agiletags:LabelTextBoxErrorReq displayName="Alternative Email Address" elementName="FirmApp_ContactDetails_altEmail" valueFromRequest="FirmApp_ContactDetails_altEmail" mandatory="no"/>        </tr>
        <tr id="FirmApp_ContactDetails_altEmailRow"><agiletags:LabelTextBoxErrorReq displayName="Cell" elementName="FirmApp_ContactDetails_cell" valueFromRequest="FirmApp_ContactDetails_cell" mandatory="no"/>        </tr>
        <tr id="BrokerApp_regionRow"><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Region"  elementName="FirmBankApp_ContantRegion" lookupId="251" mandatory="no" errorValueFromSession="yes" sort="yes"/>        </tr>
      </table>

    <br>
    <label class="subheader">Postal Address</label>
    <hr>
      <table id="FirmPostalAddressTable">
        <tr id="FirmApp_PostalAddress_addressLine1Row"><agiletags:LabelTextBoxErrorReq displayName="Line 1" elementName="FirmApp_PostalAddress_addressLine1" valueFromRequest="FirmApp_PostalAddress_addressLine1" mandatory="no"/>        </tr>
        <tr id="FirmApp_PostalAddress_addressLine2Row"><agiletags:LabelTextBoxErrorReq displayName="Line 2" elementName="FirmApp_PostalAddress_addressLine2" valueFromRequest="FirmApp_PostalAddress_addressLine2" mandatory="no"/>        </tr>
        <tr id="FirmApp_PostalAddress_addressLine3Row"><agiletags:LabelTextBoxErrorReq displayName="Line 3" elementName="FirmApp_PostalAddress_addressLine3" valueFromRequest="FirmApp_PostalAddress_addressLine3" mandatory="no"/>        </tr>
        <tr id="FirmApp_PostalAddress_postalCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Postal Code" elementName="FirmApp_PostalAddress_postalCode" valueFromRequest="FirmApp_PostalAddress_postalCode" mandatory="no"/>        </tr>
      </table>

    <br>
    <label class="subheader">Physical Address</label>
    <hr>
      <table id="FirmPhysicalAddressTable">
        <tr id="FirmApp_ResAddress_addressLine1Row"><agiletags:LabelTextBoxErrorReq displayName="Line 1" elementName="FirmApp_ResAddress_addressLine1" valueFromRequest="FirmApp_ResAddress_addressLine1" mandatory="no"/>        </tr>
        <tr id="FirmApp_ResAddress_addressLine2Row"><agiletags:LabelTextBoxErrorReq displayName="Line 2" elementName="FirmApp_ResAddress_addressLine2" valueFromRequest="FirmApp_ResAddress_addressLine2" mandatory="no"/>        </tr>
        <tr id="FirmApp_ResAddress_addressLine3Row"><agiletags:LabelTextBoxErrorReq displayName="Line 3" elementName="FirmApp_ResAddress_addressLine3" valueFromRequest="FirmApp_ResAddress_addressLine3" mandatory="no"/>        </tr>
        <tr id="FirmApp_ResAddress_physicalCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Physical Code" elementName="FirmApp_ResAddress_postalCode" valueFromRequest="FirmApp_ResAddress_postalCode" mandatory="no"/>        </tr>
      </table>

    
    <hr>
    <table id="FirmAppSaveTable">
        <tr>
           <td><input type="reset" value="Reset"></td>
           <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'SaveBrokerFirmContactDetailsCommand')"></td>
        </tr>
    </table>

  </agiletags:ControllerForm>



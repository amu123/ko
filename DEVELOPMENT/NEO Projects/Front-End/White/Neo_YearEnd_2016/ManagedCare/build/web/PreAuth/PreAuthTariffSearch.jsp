<%-- 
    Document   : PreAuthTariffSearch
    Created on : 2010/07/02, 04:16:03
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" language="JavaScript">
            
            function checkSelect(){
                var desc = $("#provDescipline").val();
                //alert("desc = "+desc);
                if(desc == null || desc == ""){
                    $("#provDescipline_error").text("Provider Discipline is Mandatory");
                }else{
                    var check1 = /^\d{3}$/;
                    var numCheck = check1.test(desc);
                    if(numCheck == false){
                        $("#provDescipline_error").text("Incorrect Provider Discipline "+desc);
                    }else{
                        submitWithAction("SearchPreAuthBasketAllTariffs");
                    }
                }
            }
            
            function submitWithAction(action) {
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }
            
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Search Specific Tariffs</label>
                    <br/>
                    <table>
                        <agiletags:ControllerForm name="searchBasketTariff">
                            <input type="hidden" name="opperation" id="opperation" value="" />
                            <tr><agiletags:LabelTextBoxError displayName="Provider Discipline" elementName="provDescipline" mandatory="yes" javaScript="" valueFromSession="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Code" elementName="code" /></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Description" elementName="description" /></tr>
                            <tr><agiletags:ButtonOpperation type="button" align="right" span="3" commandName="" displayname="Search" javaScript="onClick=\"checkSelect();\"" /></tr>
                        </agiletags:ControllerForm>
                    </table>
                    <br/>
                    <HR color="#666666" WIDTH="50%" align="left">
                    <agiletags:AuthBasketTariffSpecSearchResult commandName="AllocateBasketTariffToSession"  javaScript="" />
                    <!-- content ends here -->
                </td></tr></table>
    </body>
</html>

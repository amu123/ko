<%-- 
    Document   : MemberDetails
    Created on : 2012/05/14, 10:05:27
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tab.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery.loadmask.css"/>


        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery-1.4.2.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/jQuery/jquery.loadmask.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/AgileTabs.js"></script>
        <script type='text/javascript' src="${pageContext.request.contextPath}/resources/Membership/MemberMaintenance.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/ckeditor/ckeditor.js"></script>

        <style>
            #header { position: fixed; top: 0; left: 8px; right: 8px; z-index: 999;background: #fff;}
            #content {position: relative; margin-top: 100px;}
        </style>
        <script>
            $(document).ready(function() {
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });
            });

            function resizeContent() {
                $('#content').css('margin-top', $('#header').height());
            }
            
            function setSession(sessionName, value) {
                var url = "/ManagedCare/AgileController";
                var data = {
                    'opperation': 'ManageSessionCommand',
                    'value': value,
                    'sessionName': sessionName
                };

                $.get(url, data, function() {});

                data = null;
            }
            
            function showHandedOver(type) {
                if(type == 'debtCollectionSelect'){
                    var id = document.getElementById('MAI_DebtCollectionSelect').value;
                    setSession("hasDebt", id);
                    if(id == '1'){
                        document.getElementById('handedOverSelectTR').style.display = 'table-row';
                    } else{
                        document.getElementById('handedOverSelectTR').style.display = 'none';
                        document.getElementById('handedOverDateSelectTR').style.display = 'none';
                        $('#MAI_HandedOverSelect').val('No');
                        $('#MAI_HandedOverDateSelect').val('');
                        setSession("showHandedOverFlag", 'false');
                    }
                }
                if(type == 'handedOverSelect'){
                    var id = document.getElementById('MAI_HandedOverSelect').value;
                    setSession("debtHandedOver", id);
                    if(id == '1'){
                        document.getElementById('handedOverDateSelectTR').style.display = 'table-row';
                    } else{
                        document.getElementById('handedOverDateSelectTR').style.display = 'none';
                        $('#MAI_HandedOverDateSelect').val('');
                        setSession("showHandedOverFlag", 'false');
                    }
                }
                if(type == 'ValidationSave'){
                    var date = document.getElementById('MAI_HandedOverDateSelect').value;
                    setSession("debtHandedOverDate", date);
                    var handedOverId = document.getElementById('MAI_HandedOverSelect').value;
                    if(handedOverId == '1'){
                        rex = /^\d{4}\/\d{1,2}\/\d{1,2}$/;
                        if(date.indexOf('/') !== -1 && date.match(rex)) {
                            document.getElementById("MAI_HandedOverDateSelect_error").innerHTML = "";
                            
                            var from = $("#MAI_HandedOverDateSelect").val().split("/");
                            var dDate = new Date(from[0], from[1] - 1, from[2]);
                            
                            var today = new Date();
                            if(dDate.getTime() <= today.getTime()){
                                setSession("showHandedOverFlag", 'true');
                                document.getElementById('handedOverTD').style.visibility = 'visible';
                            } else{
                                document.getElementById('handedOverTD').style.visibility = 'hidden';
                                setSession("showHandedOverFlag", 'false');
                            }
                            return true;
                        }
                        document.getElementById("MAI_HandedOverDateSelect_error").innerHTML = "Incorrect Date Format";
                        document.getElementById('handedOverTD').style.visibility = 'hidden';
                        setSession("showHandedOverFlag", 'false');
                        return false;
                    }
                    document.getElementById("MAI_HandedOverDateSelect_error").innerHTML = "";
                    document.getElementById('handedOverTD').style.visibility = 'hidden';
                    setSession("showHandedOverFlag", 'false');
                    return true;
                }
            }
            function onScreenAction(command, index) {
                EmailBtnName = 'btnEmail' + index;
                document.getElementById(EmailBtnName).disabled = true;
                document.getElementById(EmailBtnName).value = "Sending...";
                var url = "/ManagedCare/AgileController";
                var data = {
                    'opperation': command,
                    'emailAddress': document.getElementById('emailAddress').value,
                    'btnIndex': index
                };
                $.get(url, data, function(resultTxt) {
                    if (resultTxt != null) {
                        document.getElementById(EmailBtnName).value = resultTxt;
                    }
                });

                var data = null;
            }
        </script>
    </head>
    <body onload="initAgileTabs();">

        <agiletags:ControllerForm name="TabControllerForm">
            <input type="hidden" name="opperation" id="opperation" value="MemberMaintenanceTabContentCommand" />
            <input type="hidden" name="onScreen" id="onScreen" value="" />
            <input type="hidden" name="memberEntityId" id="memberEntityId" value="${sessionScope.memberCoverEntityId}" />
            <input type="hidden" name="depNumber" id="depNumberDetails" value="" />
            <input type="hidden" name="memberCoverNoCheck" id="memberCoverNoCheck" value="${sessionScope.memberCoverNumber}" />

        </agiletags:ControllerForm>
        <div id="main_div">
            <div id="header">
                <ol id="tabs" >
                    <!--${sessionScope.memberCoverDepType == 17 ? '<li><a href="#MemberSummary">Summary</a></li>' : ''}-->
                    <li><a href="#MemberEntityDetails" class="label">Member Details</a></li>
                        ${sessionScope.memberCoverDepType == 17 ? '<li><a href="#MemberBankingDetails"  >Banking Details</a></li>' : ''}
                        ${sessionScope.memberCoverDepType == 17 ? '<li><a href="#MemberContactDetails"  >Contact Details</a></li>' : ''}
                        ${sessionScope.memberCoverDepType == 17 ? '<li><a href="#MemberAddressDetails"  >Address Details</a></li>' : ''}
                        ${sessionScope.memberCoverDepType == 17 ? '<li><a href="#MemberContributions"  >Contributions</a></li>' : ''}
                        ${sessionScope.memberCoverDepType == 17 ? '<li><a href="#MemberCover" >Cover Details</a></li>' : ''}                    
                        ${sessionScope.memberCoverDepType == 17 ? '<li><a href="#MemberUnderwriting" >Underwriting</a></li>' : ''}
                    <li><a href="#Notes"  >Notes</a></li>
                    <li><a href="#MemberCoverAudit"  >Audit Details</a></li>
                    <li><a href="#MemberDocuments"  >Documents</a></li>
                        ${sessionScope.memberCoverDepType == 17 ? '<li><a href="#DocumentGeneration" >Document Generation</a></li>' : ''}
                    <li><a href="#MemberCommunication"  >Member Communication</a></li>
                </ol>
                <table width="100%" bgcolor="#dddddd"  style=" border: 1px solid #c9c3ba;">
                    <tr>
                        <td width="375"><span ><label><b>Member Number:</b></label> <label id="member_header_memberName" >${sessionScope.memberCoverNumber}</label></span></td>
                        <td class="label"><b>Group Type:</b> ${sessionScope.memberEmployerBrokerInfo.groupType}</td>
                        <td class="label"><b>Administrator:</b> ${sessionScope.memberEmployerBrokerInfo.administrator}</td>
                        <c:if test="${sessionScope.memberEmployerBrokerInfo.VIPBroker == '1'}" >
                            <td><div class="vipBroker" style="width: 20%">(VIP)</div></td>
                        </c:if>
                        <td><label></label></td>
                    </tr>
                    <c:if test="${sessionScope.memberCoverDepType == 17}" >
                        <tr>
                            <td class="label"><b>Group:</b> ${sessionScope.memberEmployerBrokerInfo.employerNumber} - ${sessionScope.memberEmployerBrokerInfo.employerName}</td>
                            <td class="label"><b>Billing Method:</b> ${sessionScope.memberEmployerBrokerInfo.billingMethod}</td>
                            <td class="label"><b>Payment Method:</b> ${sessionScope.memberEmployerBrokerInfo.paymentMethod} </td>
                            <td class="label"><b>Debit Order Day:</b> ${sessionScope.memberEmployerBrokerInfo.debitOrderDay}</td>
                        </tr>
                    </c:if>
                    <tr>
                        <td class="label"><b>Scheme:</b> <span style="color:red"><b>${sessionScope.schemeName}</b></span></td>
                        <c:if test="${sessionScope.showHandedOverFlag == 'true'}" >
                            <td id="handedOverTD" style="visibility: visible"><div class="vipBroker" style="width: 130px">Member Handed Over</div></td>
                        </c:if>
                        <c:if test="${sessionScope.showHandedOverFlag != 'true'}" >
                            <td id="handedOverTD" style="visibility: hidden"><div class="vipBroker" style="width: 130px">Member Handed Over</div></td>
                        </c:if>
                    </tr>
                </table>
            </div>

            <div id="content">
                <!--<div class="tabContent" id="MemberSummary">Loading...</div>-->
                <div class="tabContent hide" id="MemberEntityDetails">Loading...</div>
                <div class="tabContent hide" id="MemberAddressDetails">Loading...</div>
                <div class="tabContent hide" id="MemberBankingDetails">Loading...</div>
                <div class="tabContent hide" id="MemberContactDetails">Loading...</div>
                <div class="tabContent hide" id="MemberCover">Loading...</div>
                <div class="tabContent hide" id="MemberCoverAudit">Loading...</div>
                <div class="tabContent hide" id="MemberContributions">Loading...</div>
                <div class="tabContent hide" id="Notes">Loading...</div>
                <div class="tabContent hide" id="MemberUnderwriting">Loading...</div>
                <div class="tabContent hide" id="MemberDocuments">Loading...</div>
                <div class="tabContent hide" id="DocumentGeneration">Loading...</div>
                <div class="tabContent hide" id="MemberCommunication">Loading...</div>
            </div>
            <br/>
            <div id="tabTableLayout" align="left">
                <input type="button" value="Return" onclick="submitGenericReload('ReloadPreauthGeneric', '/Member/DependantSelection.jsp');"></td>
            </div>
        </div>
        <div id="overlay" style="display:none;"></div>
        <div id="overlay2" style="display:none;"></div>
    </body>
</html>

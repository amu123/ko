
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<agiletags:ControllerForm name="BrokerConsultantHistoryForm">
    <div id="brokerConsultantListGrid">
    <input type="hidden" name="opperation" id="opperation" value="AddBrokerConsultantHistoryCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberDependantApp" />
    <input type="hidden" name="brokerEntityId" id="brokerConsultantHistoryEntityId" value="${requestScope.brokerEntityId}" />
    <input type="hidden" name="buttonPressed" id="brokerConsultantHistoryButtonPressed" value="" />
    <input type="hidden" name="brokerTile" id="brokerConsultantHistoryTile" value="${requestScope.brokerTile}" />
    <input type="hidden" name="brokerContactPerson" id="brokerConsultantContactHistoryPerson" value="${requestScope.brokerContactPerson}" />
    <input type="hidden" name="brokerRegion" id="brokerConsultantHRegion" value="${requestScope.brokerRegion}" />
    <input type="hidden" name="depNo" id="memberAppDepNo" value="" />
    <input type="hidden" name="memberAppNumber" id="memberAppDepNumber" value="${memberAppNumber}" />

    <label class="header">Broker Consultant History</label>
    <br>
    <input type="button" name="AddHistoryButton" value="Add History" onclick="document.getElementById('brokerConsultantHistoryButtonPressed').value = this.value; getPageContents(this.form,null,'main_div','overlay');"/>
    <br>
        <table id="brokerConsultantListTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%">
            <tr>
                <th>Broker Consultant Code</th>
                <th>Broker Consultant Name</th>
                <th>Start Date</th>
                <th>End Date</th>                         
            </tr>
            <c:forEach var="entry" items="${requestScope.brokerConsultantList}">
                <tr>
                    <td class="label">${entry.consultantCode}</td>
                    <td class="label">${entry.consultantName}</td>
                    <td class="label"><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveStartDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveStartDate.day}"/></td>
                    <td class="label"><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.effectiveEndDate.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.effectiveEndDate.day}"/></td>                            
                </tr>
            </c:forEach>
        </table>
    </div>
</agiletags:ControllerForm>

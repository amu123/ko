<%-- 
    Document   : PracticeCapture
    Created on : Aug 20, 2010, 6:47:14 AM
    Author     : Johan-NB
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script language="JavaScript">

            function CheckSearchCommands(){
                //reload ajax commands for screen population
                toggelPracTypes($("#pracType").val());
            }

            function GetXmlHttpObject(){
                if (window.ActiveXObject){
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (window.XMLHttpRequest){
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                return null;
            }

            function GetDOMParser(xmlStr){
                var xmlDoc;
                if(window.DOMParser){
                    parser=new DOMParser();
                    xmlDoc= parser.parseFromString(xmlStr,"text/xml");
                }else{
                    xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async="false";
                    xmlDoc.loadXML(xmlStr);
                }
                return xmlDoc;
            }

            function toggelPracTypes(){
                var str = $("#pracType").val();

                if(str == "1"){
                    //                    $("#ProvHeader").show();
                    //                    $("#provCounRow").show();
                    //                    $("#provTitleRow").show();
                    //                    $("#provIniRow").show();
                    //                    $("#provNameRow").show();
                    //                    $("#provSurRow").show();
                    //                    $("#provIdRow").show();
                    $("#ProviderInfo").show();

                }else{
                    //                    $("#ProvHeader").hide();
                    //                    $("#provCounRow").hide();
                    //                    $("#provTitleRow").hide();
                    //                    $("#provIniRow").hide();
                    //                    $("#provNameRow").hide();
                    //                    $("#provSurRow").hide();
                    //                    $("#provIdRow").hide();
                    $("#ProviderInfo").hide();
                }

            }

            function setZeroDisc(disc){
                var sub = disc;
                var firstChar = disc.charAt(0);
                if(firstChar != "0" && disc.length < 3){
                    sub = "0"+disc;
                }
                return sub;
            }

            function validateMandatory(command){
                ClearPracErrors();
                var valid = true;
                var pracType = $("#pracType").val();
                //practice values
                var disc = $("#disc").val();
                var pracNo = $("#pracNo").val();
                var provNo = $("#provNo").val();
                var pracName = $("#pracName").val();
                var gpInd = $("#gpInd").val();
                var nop = $("#nop").val();
                var effectiveDate = $("#effectiveDate").val();
                var cashPractice = $("#cashPractice").val();

                //provider values
                var councilNo = $("#councilNo").val();
                var title = $("#title").val();
                var initial = $("#initial").val();
                var firstName = $("#firstName").val();
                var surname = $("#surname").val();
                var idNo = $("#idNo").val();

                //valid = validatePracInfo(disc, pracNo, provNo, pracName, gpInd, nop, cashPractice);
                valid = validatePracInfo(disc, pracNo, provNo, pracName, gpInd, nop, effectiveDate, cashPractice)
                if(pracType == "1"){
                    valid = validateProvInfo(councilNo, title, initial, firstName, surname, idNo, effectiveDate);
                }

                if(valid == true){
                    submitWithAction(command);
                } else {
                    alert("Validation Errors");
                }

            }

            function validateDate(dateString, element) {
                //  check for valid numeric strings
                var strValidDateChars = "0123456789/";

                var strChar;
                var strChar2;
                var blnResult = true;

                //if (dateString.length < 10 || dateString.length > 10){
                //    blnResult = false;
                //}
                //*test strString consists of valid characters listed above
                //for (i = 0; i < dateString.length; i++){
                //    strChar = dateString.charAt(i);
                //    if (strValidDateChars.indexOf(strChar) == -1){
                //        blnResult = false;
                //    }
                //}

                //test if slash is at correct position (yyyy/mm/dd)
                strChar = dateString.charAt(4);
                strChar2 = dateString.charAt(7);
                if (strChar != '/' || strChar2 != '/'){
                    blnResult = false;
                }

                var error = "#"+element+"_error";
                if(blnResult == false){
                    $(error).text("Incorrect Date (DD/MM/YYYY)");
                }else{
                    $(error).text("");
                }
                return blnResult;
            }


            function validatePracInfo(disc, pracNo, provNo, pracName, gpInd, nop, effectiveDate, cashPractice){
                var valid = true;
                if(pracName == null || pracName == ""){
                    valid = false;
                    $("#pracName_error").text("Discipline is Mandatory");
                }

                if(effectiveDate == null || effectiveDate == ""){
                    valid = false;
                    $("#effectiveDate_error").text("effectiveDate is Mandatory");
                }

                if ($("#effectiveDate").val() != "") {
                    valid = validateDate($("#effectiveDate").val(), 'effectiveDate');
                }


                if(disc == null || disc == ""){
                    valid = false;
                    $("#disc_error").text("Discipline is Mandatory");
                }else{
                    if(disc.length > 3){
                        valid = false;
                    }else{
                        var check = validNo(disc);
                        if(check == false){
                            valid = false;
                            $("#disc_error").text("Discipline is invalid : "+disc);
                        }else{
                            var newDisc = setZeroDisc(disc);
                            $("#disc").val(newDisc);
                        }
                    }
                }

                if(pracNo == null || pracNo == ""){
                    valid = false;
                    $("#pracNo_error").text("Practice Number is Mandatory");
                }else{
                    var check = validNo(pracNo);
                    if(check == false){
                        valid = false;
                        $("#pracNo_error").text("Practice Number is invalid : "+pracNo);
                    }
                    if(pracNo.length != 7){
                        $("#pracNo_error").text("Practice Number length is invalid");
                    }
                }

                if(provNo == null || provNo == ""){
                    valid = false;
                    $("#provNo_error").text("Provider Number is Mandatory");
                }else{
                    var check = validNo(provNo);
                    if(check == false){
                        valid = false;
                        $("#provNo_error").text("Provider Number is invalid : "+provNo);
                    }
                    if(provNo.length != 7){
                        $("#provNo_error").text("Provider Number length is invalid");
                    }
                }

                if(provNo != pracNo){
                    valid = false;
                    $("#pracNo_error").text("Numbers must be the same");
                    $("#provNo_error").text("Numbers must be the same");
                }

                if(gpInd == null || gpInd == "99"){
                    valid = false;
                    $("#gpInd_error").text("Group Ind is Mandatory");
                }

                if(cashPractice == null || cashPractice == "99"){
                    valid = false;
                    $("#cashPractice_error").text("Cash Practice is Mandatory");
                }

                if(nop != null && nop != ""){
                    var check = validNo(nop);
                    if(check == false){
                        valid = false;
                        $("#nop_error").text("Invalid Number");
                    }
                }

                return valid;

            }

            function validateProvInfo(councilNo, title, initial, firstName, surname, idNo){
                var valid = true;

                if(councilNo == null || councilNo == ""){
                    valid = false;
                    $("#councilNo_error").text("Council Number is Mandatory");
                }else{
                    var check = validateCouncil(councilNo);
                    if(check == false){
                        valid = false;
                        $("#councilNo_error").text("Council Number is Invalid : "+councilNo);
                    }
                }
                
                if(idNo == null || idNo == ""){
                    valid = false;
                    $("#idNo_error").text("ID Number is Mandatory");
                }else{
                    var check = validIDNo(idNo);
                    if(check == false){
                        valid = false;
                        $("#idNo_error").text("ID Number is invalid : "+idNo);
                    }
                }

                if(title == null || title == "99"){
                    valid = false;
                    $("#title_error").text("Provider Title is Mandatory");
                }
                if(initial == null || initial == ""){
                    valid = false;
                    $("#initial_error").text("Provider Initail is Mandatory");
                }
                if(firstName == null || firstName == ""){
                    valid = false;
                    $("#firstName_error").text("Provider Name is Mandatory");
                }
                if(surname == null || surname == ""){
                    valid = false;
                    $("#surname_error").text("Provider Surname is Mandatory");
                }
                return valid;
            }

            function validateCouncil(str){
                var valid = true;

                if(str.length > 10 || str.length < 8){
                    valid = false;
                }

                if(str.length == 10){
                    var t1 = RegExp("[A-Z]").test(str.charAt(0));
                    var t2 = RegExp("[A-Z]").test(str.charAt(1));
                    var t3 = RegExp("[A-Z]").test(str.charAt(2));

                    if(t1 == false || t2 == false || t3 == false){
                        valid = false;
                    }else{
                        var numbers = str.substr(3, str.length);
                        valid = validCouncilNo(numbers);
                    }
                }
                if(str.length == 9){
                    var t1 = RegExp("[A-Z]").test(str.charAt(0));
                    var t2 = RegExp("[A-Z]").test(str.charAt(1));

                    if(t1 == false || t2 == false){
                        valid = false;
                    }else{
                        var numbers = str.substr(2, str.length);
                        valid = validCouncilNo(numbers);
                    }
                }
                if(str.length == 8){
                    var numbers = str.substr(0, str.length);
                    valid = validCouncilNo(numbers);
                }
                return valid;
            }

            function validIDNo(str){
                var strValidChars = "0123456789";
                var strChar;

                if (str.length < 13 || str.length > 13) return false;

                for (j = 0; j < str.length; j++){
                    strChar = str.charAt(j);
                    if (strValidChars.indexOf(strChar) == -1){
                        return false;
                    }
                }
                return true;
            }

            function validCouncilNo(str){
                var strValidChars = "0123456789";
                var strChar;

                for (j = 0; j < str.length; j++){
                    strChar = str.charAt(j);
                    if (strValidChars.indexOf(strChar) == -1){
                        return false;
                    }
                }
                return true;
            }

            function validNo(str){
                var strValidChars = "0123456789";
                var strChar;

                for (j = 0; j < str.length; j++){
                    strChar = str.charAt(j);
                    if (strValidChars.indexOf(strChar) == -1){
                        return false;
                    }
                }
                return true;
            }

            function ClearPracErrors(){
                $("#disc_error").text("");
                $("#pracName_error").text("");
                $("#pracNo_error").text("");
                $("#provNo_error").text("");
                $("#surname_error").text("");
                $("#firstName_error").text("");
                $("#initial_error").text("");
                $("#title_error").text("");
                $("#idNo_error").text("");
                $("#councilNo_error").text("");
                $("#effectiveDate_error").text("");
                $("#cashPractice_error").text("");
            }

            function clearElement(element) {
                part = element + "_text";
                document.getElementById(element).value = "";
                document.getElementById(part).value = "";
            }
            function submitWithAction(action) {
                document.getElementById("opperation").value = action;
                document.forms[0].submit();
            }


        </script>
    </head>
    <body onload="CheckSearchCommands();">
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <!-- content goes here -->
                    <label class="header">Capture Practice Details</label>
                    <br/>

                    <agiletags:ControllerForm name="savePractice" validate="yes" >
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <table>
                            <tr><label class="subheader">Practice Details</label></tr>
            </table>
            <hr size="3px" bgcolor="#0000CC"/>
            <table>
                <tr><td align="left" width="160px"><label>Practice Type:</label></td>
                    <td align="left" width="200px"><select name="pracType" id="pracType" style="width:215px" onchange="toggelPracTypes();">
                            <option value="1">Individual Practice</option>
                            <option value="2">Practice Only</option>
                        </select></td><td></td><td></td><td></td></tr>

                <tr><agiletags:LabelTextBoxError displayName="Discipline" elementName="disc" valueFromSession="yes" javaScript="" mandatory="yes" /></tr>
                <tr><agiletags:LabelTextBoxError displayName="Sub Discipline" elementName="subDisc" valueFromSession="yes" javaScript="" mandatory="no" /></tr>
                <tr><agiletags:LabelTextBoxError displayName="Practice Number" elementName="pracNo" valueFromSession="yes" javaScript="" mandatory="yes" /></tr>
                <tr><agiletags:LabelTextBoxError displayName="Provider Number" elementName="provNo" valueFromSession="yes" javaScript="" mandatory="yes" /></tr>
                <tr><agiletags:LabelTextBoxError displayName="Practice Name" elementName="pracName" valueFromSession="yes" javaScript="" mandatory="yes" /></tr>
                <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Group Practice Ind" elementName="gpInd" lookupId="67" mandatory="yes"  errorValueFromSession="yes"/></tr>
                <tr><agiletags:LabelTextBoxError displayName="Number Of Partners" elementName="nop" valueFromSession="yes" mandatory="yes"  /></tr>
                <tr><agiletags:LabelTextBoxDateTime displayName="Effective Date" elementName="effectiveDate" mandatory="yes" valueFromSession="yes"/></tr>                
                <tr ><agiletags:LabelNeoLookupValueDropDown displayName="Restrictions" elementName="cashPractice" javaScript="" lookupId="356" /></tr>
            </table>
            <br/>
            <table>
                <tr id="ProvHeader"><label class="subheader">Provider Details</label></tr>
        </table>
        <hr size="3px" bgcolor="#0000CC"/>
        <table id="ProviderInfo">
            <tr id="provCounRow"><agiletags:LabelTextBoxError displayName="Council No" elementName="councilNo" valueFromSession="yes" javaScript="" mandatory="yes" /></tr>
            <tr id="provTitleRow"><agiletags:LabelNeoLookupValueDropDown displayName="Title" elementName="title" javaScript="" lookupId="24" /></tr>
            <tr id="provIniRow"><agiletags:LabelTextBoxError displayName="Initials" elementName="initial" valueFromSession="yes" javaScript="" mandatory="yes" /></tr>
            <tr id="provNameRow"><agiletags:LabelTextBoxError displayName="First Name" elementName="firstName" valueFromSession="yes" javaScript="" mandatory="yes" /></tr>
            <tr id="provSurRow"><agiletags:LabelTextBoxError displayName="Surname" elementName="surname" valueFromSession="yes" javaScript="" mandatory="yes" /></tr>
            <tr id="provIdRow"><agiletags:LabelTextBoxError displayName="Id Number" elementName="idNo" valueFromSession="yes" javaScript="" mandatory="yes" /></tr>
        </table>
        <table>
            <tr>
                <td colspan="5" align="left">
                    <button name="opperation" type="button" onClick="submitWithAction('ClearPracticeScreen');" value="">Reset</button>
                    <button name="opperation" type="button" onClick="validateMandatory('SavePracticeDetails');" value="">Save</button>
                </td>
            </tr>
        </table>
    </agiletags:ControllerForm>

    <!-- content ends here -->
</td></tr></table>
</body>
</html>

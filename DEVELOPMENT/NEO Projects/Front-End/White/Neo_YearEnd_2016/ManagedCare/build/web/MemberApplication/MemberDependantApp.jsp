
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<agiletags:ControllerForm name="MemberDependantAppForm">
    <input type="hidden" name="opperation" id="opperation" value="MemberDependantAppCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="MemberDependantApp" />
    <input type="hidden" name="memberAppNumber" id="memberAppDepNumber" value="${memberAppNumber}" />
    <input type="hidden" name="memberAppCoverNumber" id="memberAppDepCoverNumber" value="${memberAppCoverNumber}" />
    <input type="hidden" name="buttonPressed" id="memberAppDepButtonPressed" value="" />
    <input type="hidden" name="depNo" id="memberAppDepNo" value="" />
    <input type="hidden" name="maxDep" value="${maxDep}" />
    <input type="hidden" name="memberAppType" id="memberAppType" value="${requestScope.memberAppType}" />
    <input type="hidden" name="memberAppDep_optionId" id="memberAppDep_optionId" value="${requestScope.optionId}" />
    
    <label class="header">Family Members</label>
    <br>
    <c:if test="${persist_user_viewMemApps == false}">
        <input type="button" id="AddDependantButton" name="AddDependantButton" value="Add Dependant" onclick="document.getElementById('memberAppDepButtonPressed').value = this.value; getPageContents(this.form,null,'main_div','overlay'); toggleNetworkIPA(document.getElementById('memberAppDep_optionId'));"/>
    </c:if>

    <br>
    <table id="depListTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%">
        <tr>
            <th>Code</th>
            <th>First Name</th>
            <th>Surname</th>
            <th>Date of Birth</th>
            <th>Gender</th>
            <th>Dependant Type</th>
            <th></th>
            <th></th>
        </tr>
        <c:forEach var="entry" items="${depList}">
            <c:if test="${entry.dependentNumber > 0}">
                <tr>
                    <td><label>${entry.dependentNumber}</label></td>
                    <td><label>${entry.firstName}</label></td>
                    <td><label>${entry.lastName}</label></td>
                    <td><label><fmt:formatNumber minIntegerDigits="2" groupingUsed="false" value="${entry.dateOfBirth.year}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.dateOfBirth.month}"/>/<fmt:formatNumber minIntegerDigits="2" value="${entry.dateOfBirth.day}"/></label></td>
                    <td><label>${entry.gender == 1 ? "Male" : entry.gender == 2 ? "Female" : ""}</label></td>
                    <td><label>${entry.dependantType == 18 ? "Adult" : "Child"}</label></td>
                    <td><input type="button" value="Edit" name="EditButton" onclick="document.getElementById('memberAppDepNo').value=${entry.dependentNumber};document.getElementById('memberAppDepButtonPressed').value='EditButton';getPageContents(this.form,null,'main_div','overlay');"></td>
                    <td><input type="button" value="Remove" name="RemoveButton" onclick="document.getElementById('memberAppDepNo').value=${entry.dependentNumber};document.getElementById('memberAppDepButtonPressed').value='RemoveButton';submitFormWithAjaxPost(this.form, 'Dependants');"></td>
                </tr>
            </c:if>
        </c:forEach>
    </table>
</agiletags:ControllerForm>
<%--
    Document   : ChangePassword
    Created on : 2010/08/02, 04:00:49
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>

        <script language="JavaScript">

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function validatePassword(str, element) {

                var error = "#"+element+"_error";
                if(str.length < 8) {
                    $(error).text("The password must be at least 8 characters!");
                } else {
                    $(error).text("");
                }
            }
            
            function validateChange(command) {

                var oldP = $("input#oldPassword").val();
                var newP = $("input#newPassword").val();
                var confirmP = $("input#confirmPassword").val();
                var olderror = "#oldPassword_error";
                var error = "#confirmPassword_error";
                
                $("#confirmPassword_error").text("");
                $("#newPassword_error").text("");

                if(oldP.length > 0)
                {
                    $(olderror).text("");
                    //check if retype same
                    var error = false;
                    if(newP === confirmP){} else {
                        $("#confirmPassword_error").text("Password Mismatch");
                        error = true;
                    }
                
                    if(newP.length < 8){
                        $("#newPassword_error").text("Password has to be more than 8 characters");
                        error = true;
                    }
                    if(confirmP.length < 8){
                        $("#confirmPassword_error").text("Password has to be more than 8 characters");
                        error = true;
                    }
                
                    if(!error){
                        submitWithAction(command);
                    } else {
                        $(error).text("The new passwords do not match!");
                    }
                } else {
                    $(olderror).text("The old password is mandatory");
                }
            }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="center"><td align="left">
                    <fieldset id="pageBorder">
                        <legend><div align="left"><label class="header">Change Password</label></div></legend>
                        <br/>

                        <table>
                            <agiletags:ControllerForm name="PasswordChange">
                                <input type="hidden" name="opperation" id="opperation" value=""/>
                                
                                <tr><agiletags:LabelPasswordError displayName="Old Password" elementName="oldPassword" mandatory="yes" valueFromSession="yes"/></tr>
                                <tr><agiletags:LabelPasswordError displayName="New Password" elementName="newPassword" mandatory="yes" valueFromSession="yes" javaScript="onChange=\"validatePassword(this.value, 'newPassword');\""/></tr>
                                <tr><agiletags:LabelPasswordError displayName="Confirm New Password" elementName="confirmPassword" mandatory="yes" valueFromSession="yes" javaScript="onChange=\"validatePassword(this.value, 'confirmPassword');\""/></tr>

                                <tr>
                                    <!--agiletags:ButtonOpperation commandName="" align="left" displayname="Reset" span="1" type="button" javaScript="onClick=\"validatePassword()\";"/>-->
                                    <agiletags:ButtonOpperation commandName="" align="left" displayname="Change" span="1" type="button" javaScript="onClick=\"validateChange('ChangePasswordCommand')\";"/>
                                </tr>

                            </agiletags:ControllerForm>
                        </table>

                        <!-- content ends here -->
                    </fieldset>
                </td></tr></table>
    </body>
</html>


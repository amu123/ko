<%-- 
    Document   : ProviderDetailsTabbedPage
    Created on : 2011/08/03, 01:52:02
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <link rel="stylesheet" href="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/jQueryUI/jquery-ui-1.8.2.custom.min.js"></script>

        <script>

            $(document).ready(function() {

                $("#providerTabs").tabs();
            });
            
        </script>
    </head>
    <body>
        <div class="providerPage">

            <div id="providerTabs">

                <ul>
                    <li><a href="#practiceDetailsTab">Practice Details</a></li>
                    <li><a href="#practicePaymentTab">Payment Details</a></li>
                    <li><a href="#practiceClaimTab">Claims Details</a></li>
                    <li><a href="#practiceBankingTab">Banking Details</a></li>
                    <li><a href="#practiceAuthorizationTab">Authourisation</a></li>
                </ul>

                <agiletags:ControllerForm name="providerTabHistory">
                <div id="practiceDetailsTab">
                    <table width=100% height=100%><tr valign="center"><td width="5px"></td><td align="left">

                    <tr><agiletags:LabelTextBoxError displayName="Practice Number" elementName="practiceNumber" valueFromSession="yes"/></tr>
                    <tr><agiletags:LabelTextBoxError displayName="Practice Name" elementName="practiceName"  valueFromSession="yes"/></tr>
                    <tr><agiletags:LabelTextBoxError displayName="Discipline Type" elementName="disciplineType" valueFromSession="yes"/></tr>
                    <tr><agiletags:LabelTextBoxError displayName="Network Indicator" elementName="networkIndicator" valueFromSession="yes"/></tr>
                    <tr><agiletags:LabelTextBoxError displayName="Physical Address" elementName="physicalAddress" valueFromSession="yes"/></tr>
                    <tr><agiletags:LabelTextBoxError displayName="Status" elementName="status" valueFromSession="yes"/></tr>
                    <tr><agiletags:LabelTextBoxError displayName="Effective Start Date" elementName="startDate" valueFromSession="yes"/></tr>
                    <tr><agiletags:LabelTextBoxError displayName="Effective End Date" elementName="endDate" valueFromSession="yes"/></tr>

                 </td></tr></table>
                </div>

                <div id="practicePaymentTab">
                    <table>
                        <tr><agiletags:ViewProviderPaymentRunDetailsTable commandName="" javaScript=""/></tr>
                    </table>
                </div>

                <div id="practiceClaimTab">
                    <p>In progress...</p>
                </div>

                <div id="practiceBankingTab">
                    <table>
                            <tr><agiletags:LabelTextBoxError displayName="Practice Number" elementName="practiceNumber"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="First Name" elementName="practiceName"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Surname" elementName="practiceSurname"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Bank Name" elementName="pracBankName"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Branch Code" elementName="pracBranchCode"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Branch Name" elementName="pracBranchName"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Account Number" elementName="pracAccountNumber"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Account Name" elementName="pracAccountName"  valueFromSession="Yes" readonly="yes"/></tr>
                            <tr><agiletags:LabelTextBoxError displayName="Account Type" elementName="pracAccountType"  valueFromSession="Yes" readonly="yes"/></tr>

                        </table>
                </div>

                <div id="practiceAuthorizationTab">
                    <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                                    <!-- content goes here -->
                                    <label class="header">Authorisation Search</label>
                                    <br/><br/>
                                    <table>
                                        <!-- The controller form searchAuthForm -->
                                        <input type="hidden" name="opperation" id="opperation" value="" />
                                        <agiletags:HiddenField elementName="searchCalled" />
                                        <input type="hidden" name="onScreen" id="onScreen" value="" />
                                        <input type="hidden" name="exactCoverNum" id="exactCoverNum" value="1" />

                                        <tr><agiletags:LabelTextBoxError displayName="Authorisation Number" elementName="authNo"/></tr>
                                        <tr><agiletags:LabelNeoLookupValueDropDownError displayName="Auth Type" lookupId="89" elementName="authType"/></tr>
                                        <tr><agiletags:LabelTextBoxDate displayname="Authorisation Date" elementName="authDate"/></tr>

                                        <tr><agiletags:LabelTextSearchText valueFromSession="yes" displayName="Member Number" elementName="memberNum" searchFunction="yes" searchOperation="ForwardToSearchMemberCommand" onScreen="/PreAuth/PreAuthSearch.jsp" mandatory="no" javaScript="onblur=\"getCoverByNumber(this.value, 'memberNum');\""/></tr>
                                        <tr><agiletags:EntityCoverDependantDropDown displayName="Cover Dependants" elementName="depListValues" javaScript="" mandatory="no" valueFromSession="yes" /></tr>

                                        <tr><agiletags:LabelTextBoxError displayName="First Name" elementName="name"  /></tr>
                                        <tr><agiletags:LabelTextBoxError displayName="Last Name" elementName="surname" /></tr>

                                        <tr><agiletags:LabelProductDropDown displayName="Scheme" elementName="scheme" javaScript="onblur=\"toggleProduct(this.value);\"" /></tr>
                                        <tr><agiletags:LabelDropDown displayName="Scheme Option" elementName="schemeOption" /></tr>

                                        <tr><agiletags:ButtonOpperation type="button" align="left" span="1" commandName="PreAuthDetailsCommand" displayname="Search" javaScript="onClick=\"submitWithAction('PreAuthDetailsCommand');\"" /></tr>

                                        <!-- end the cotroller form -->
                                    </table>
                                    <br/>
                                    <HR color="#666666" WIDTH="50%" align="left">
                                    <br/>
                                    <agiletags:PreAuthSearchResultTable commandName="ForwardToCallCenterPreAuthCommand" javascript="onClick=\"submitWithAction('ForwardToCallCenterPreAuthCommand');\""/>

                                    <!-- content ends here -->
                        </td></tr></table>
                </div>

            </agiletags:ControllerForm>
            </div>

        </div>
    </body>
</html>

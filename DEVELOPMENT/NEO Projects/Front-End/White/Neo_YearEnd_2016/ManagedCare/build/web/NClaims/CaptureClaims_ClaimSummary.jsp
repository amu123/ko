<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="nt" tagdir="/WEB-INF/tags" %>

<nt:NeoPanel title="Claims for Batch ${batchId}"  collapsed="false" reload="false">
    <!--<input type="button" value="Submit" onclick="processClaimBatch($('#ClaimHeader_batchId').val());">-->
    <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="100%">
        <thead>
            <tr>
                <th>Claim Number</th>
                <th>Cover Number</th>
                <th>Practice Number</th>
                <th>Provider Number</th>
                <th>Category</th>
                <th>Date Received</th>
                <th>Claim Total</th>
                <th>Receipt By</th>
                <th>Motivation Received</th>
                <th>Lines Captured</th>
                <th>Provider Type</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="item" items="${ClaimBatch.claims}">
                <c:set var="principalID" value="principalEID${item.claimId}"></c:set>
                <c:set var="categoryFullName" value="categoryName${item.claimId}"></c:set>
                <c:set var="linesCaptured" value="lineCaptured${item.claimId}"></c:set>
                <c:set var="linesCaptureds" value="${requestScope[linesCaptured]}"></c:set>
                <c:set var="condition" value="1"></c:set>
                <tr>
                    <td><label>${item.claimId}</label></td>
                    <td><label>${item.coverNumber}</label></td>
                    <td><label>${item.practiceNo}</label></td>
                    <td><label>${item.serviceProviderNo}</label></td>
                    <td><label>${requestScope[categoryFullName]}</label></td>
                    <td><label>${agiletags:formatXMLGregorianDate(item.receiveDate)}</label></td>
                    <td><label>${agiletags:formatDouble(item.claimSubmittedTotal)}</label></td>
                    <!--<td><label>${item.recipient}</label></td>-->
                    <c:choose>
                        <c:when test="${item.recipient == 3}">
                            <td><label>Practice</label></td>
                        </c:when>
                        <c:otherwise>
                            <td><label>Insured Person</label></td>
                        </c:otherwise>
                    </c:choose>
                    <td><label>${item.motivationReceived}</label></td>  
                    <c:choose>
                        <c:when test="${linesCaptureds == condition}">
                            <td><label>Yes</label></td>
                        </c:when>
                        <c:otherwise>
                            <td><label>No</label></td>
                        </c:otherwise>
                    </c:choose>
    <!--                <td><label>ClaimStatusId</label></td>  -->
                    <td><label>${item.serviceProviderDisciplineId}</label></td>
                    <td><input type="button" value="Capture" onclick="captureClaimLine('${item.claimId}','${item.claimCategoryTypeId}','${item.coverNumber}', '${item.serviceProviderDisciplineId}', 'false', '${item.practiceNo}', '${item.serviceProviderNo}');"></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <input type="button" value="Submit" onclick="processClaimBatch($('#ClaimHeader_batchId').val(),'false',this);">
</nt:NeoPanel>    
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <div id="DisorderHeader">
        <agiletags:ControllerForm name="DisorderValuesForm">
            <input type="hidden" name="opperation" id="opperation" value="DisorderSearchCommand" />
            <input type="hidden" name="onScreen" id="onScreen" value="DisorderValues" />
            <input type="hidden" name="memberAppNumber" value="${memberAppNumber}" />
            <input type="hidden" name="target_div" value="${target_div}" />
            <input type="hidden" name="main_div" value="${main_div}" />
            <input type="hidden" name="buttonPressed" id="disorderValuesButtonPressed" value="" />
                <label class="header">Current Specific Health Questions Details</label>
                <table id="DisorderValuesTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;" width="90%">
                    <tr>
                        <th>Question</th>
                        <th>Dependent</th>
                        <th>Date</th>
                        <th>ICD10</th>
                        <th>Treatment</th>
                        <th>Doctor</th>
                        <th>Current Condition</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${DisorderValues}">
                        <tr>
                            <td>${entry['question']}</td>
                            <td>${entry['dependent']}</td>
                            <td>${entry['date']}</td>
                            <td>${entry['code']}</td>
                            <td>${entry['treatment']}</td>
                            <td>${entry['doctor']}</td>
                            <td>${entry['condition']}</td>
                            <td><input type="button" value="Remove" name="RemoveButton" onclick="document.getElementById('disorderId').value=${entry['id']};document.getElementById('disorderValuesButtonPressed').value='RemoveButton';submitFormWithAjaxPost(this.form, 'DisorderHeader');"></td>
                        </tr>
                    </c:forEach>

                </table>

                <br>
        <input type="button" value="Cancel" name="CancelButton" onclick="swapDivVisbible('${target_div}','${main_div}');">
        <input type="button" value="Save" name="DoneButton" onclick="submitFormWithAjaxPost(this.form, 'DisorderSearchResults');">
<br><br>
            <label class="header">Add Specific Health Questions Details</label>
            <br>
            <table>
                <tr><agiletags:LabelNeoMapListDropDownErrorReq displayName="Question"  elementName="Disorder_question" mapList="SpecificQuestions" mandatory="yes" errorValueFromSession="no" firstIndexSelected="yes" />        </tr>
                <tr><agiletags:LabelNeoMapListDropDownErrorReq displayName="Dependant"  elementName="Disorder_dependent" mapList="DependentList" mandatory="yes" errorValueFromSession="no" firstIndexSelected="yes" />        </tr>
                <tr><agiletags:LabelTextBoxDateReq  displayname="Date" elementName="Disorder_date" valueFromSession="yes" mandatory="yes"/>        </tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="Treatment" elementName="Disorder_treatment" valueFromRequest="" mandatory="yes"/>        </tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="Consulting Doctor" elementName="Disorder_doctor" valueFromRequest="" mandatory="yes"/>        </tr>
                <tr><agiletags:LabelNeoLookupValueDropDownErrorReq displayName="Current Condition"  elementName="Disorder_condition" lookupId="199" mandatory="yes" errorValueFromSession="yes" firstIndexSelected="yes" />        </tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="ICD 10 Code" elementName="Disorder_diagCode" valueFromRequest="" mandatory="yes" readonly="yes"/>        </tr>
                <tr><agiletags:LabelTextBoxErrorReq displayName="ICD 10 Description" elementName="Disorder_diagName" valueFromRequest="" mandatory="yes" readonly="yes"/>        </tr>
            </table>
            <input type="button" value="Add" name="AddButton" onclick="addTableRow('DisorderValuesTable',this.form,
                {'Disorder_question':'formValue','Disorder_dependent':'formValue','Disorder_date':'formValue','Disorder_diagCode':'formValue','Disorder_treatment':'formValue','Disorder_doctor':'formValue','Disorder_condition':'formValue','Remove':'button'},
                ['Disorder_treatment','Disorder_doctor','Disorder_question','Disorder_dependent','Disorder_date','Disorder_condition','Disorder_diagCode'])">                
            <br>
        </agiletags:ControllerForm>
        <hr>
    </div>
    <label class="header">ICD10 Search</label>
     <agiletags:ControllerForm name="DisorderSearchForm">
        <input type="hidden" name="opperation" id="opperation" value="DisorderSearchCommand" />
        <input type="hidden" name="onScreen" id="onScreen" value="DisorderSearch" />
        <table>
            <tr><agiletags:LabelTextBoxError displayName="Code" elementName="DisorderCode"/></tr>
            <tr><agiletags:LabelTextBoxError displayName="Description" elementName="DisorderDescription"/></tr>
            <tr><td colspan="2" align="right"><input type="button" value="Search" onclick="submitFormWithAjaxPost(this.form, 'DisorderSearchResults');"></td></tr>
        </table>
      </agiletags:ControllerForm>

      <div style ="display: none"  id="DisorderSearchResults"></div>
    
<%-- 
    Document   : MemberAddressDetails
    Created on : 2011/07/14, 02:56:54
    Author     : josephm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script>
            
            $(function(){
                resizeContent();
                //attach on resize event
                $(window).resize(function() {
                    resizeContent();
                });            
                if (${MailResultMessage != null}){
                    alert("${MailResultMessage}");
                    <% session.setAttribute( "MailResultMessage", null); %>
                }
            });
            
            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }
            
            
        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="5px"></td><td align="left">

                    <agiletags:CallCenterTabs cctSelectedIndex="link_AD" />
                    <fieldset id="pageBorder">
                        <div class="content_area" >
                            <label class="header">Principal Member Address Details</label>
                            <br/><br/>
                            <agiletags:ControllerForm name="policyHolder">
                                <input type="hidden" name="opperation" id="opperation" value="" />

                                <label class="subheader">Physical Address</label>
                                <br/>
                                <table>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 1" elementName="memberPhysicalAddressLine1"  valueFromSession="Yes" /></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 2" elementName="memberPhysicalAddressLine2"  valueFromSession="Yes" /></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 3" elementName="memberPhysicalAddressLine3"  valueFromSession="Yes" /></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Physical City" elementName="memberPhysicalCityAddrees"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Physical Code" elementName="memberPhysicalCodeAddress"  valueFromSession="Yes" /></tr>
                                </table>
                                <br/>
                                <br/>
                                <label class="subheader">Postal Address</label>
                                <br/>
                                <table>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 1" elementName="memberPostalAddressLine1"  valueFromSession="Yes" /></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 2" elementName="memberPostalAddressLine2"  valueFromSession="Yes" /></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Line 3" elementName="memberPostalAddressLine3"  valueFromSession="Yes" /></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Postal City" elementName="memberPostalCityAddress"  valueFromSession="Yes" readonly="yes"/></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Postal Code" elementName="memberPostalCodeAddress"  valueFromSession="Yes" /></tr>
                                </table>
                                <br/>
                                <br/>
                                <label class="subheader">Contact Details</label>
                                <br/>
                                <table>
                                    <tr><agiletags:LabelTextBoxError displayName="Home Telephone Number" elementName="memberHomeTelNumber"  valueFromSession="Yes" /></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Fax Number" elementName="memberFaxNo"  valueFromSession="Yes" /></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Work Number" elementName="memberWorkNo"  valueFromSession="Yes" /></tr>
                                   <!-- <tr><agiletags:LabelTextBoxError displayName="Contact Person Tel" elementName="memberContactPersonTelNo"  valueFromSession="Yes" /></tr> -->
                                    <tr><agiletags:LabelTextBoxError displayName="Cell Phone Number" elementName="memberCellNo"  valueFromSession="Yes" /></tr>
                                    <tr><agiletags:LabelTextBoxError displayName="Email" elementName="memberEmailAddress"  valueFromSession="Yes" /></tr>
                                    <!--<tr><agiletags:LabelTextBoxError displayName="Preferred Method" elementName="communicationMethod"  valueFromSession="Yes" /></tr>-->
                                </table>
                                <br/>
                                <br/>
                                <label class="subheader">Contact Preference Details</label>
                                <table id="ContactDetailsTable">
                                    <tr id="MemberDetails_claimProcRow"><agiletags:LabelNeoConPrefDropdownReq commType="1" displayName="Claim Processing" elementName="ConPrefDetails_claimProc" javaScript="" /></tr>
                                    <tr id="MemberDetails_contriStmtRow"><agiletags:LabelNeoConPrefDropdownReq commType="2" displayName="Contributions Statement" elementName="ConPrefDetails_contriStmt" javaScript="" /></tr>
                                    <tr id="MemberDetails_claimStmtRow"><agiletags:LabelNeoConPrefDropdownReq commType="3" displayName="Claim Statement" elementName="ConPrefDetails_claimStmt" javaScript="" /></tr>
                                    <tr id="MemberDetails_claimPayRow"><agiletags:LabelNeoConPrefDropdownReq commType="4" displayName="Claim Payment" elementName="ConPrefDetails_claimPay" javaScript="" /></tr>
                                    <tr id="MemberDetails_taxCertRow"><agiletags:LabelNeoConPrefDropdownReq commType="5" displayName="Tax Certificate" elementName="ConPrefDetails_taxCert" javaScript="" /></tr>
                                    <tr id="MemberDetails_preAuthRow"><agiletags:LabelNeoConPrefDropdownReq commType="6" displayName="Pre-Auths" elementName="ConPrefDetails_preauth" javaScript="" /></tr>
                                </table>
                                <br/>
                                <table>
                                    <c:if test="${saveOption == 'yes'}">
                                        <tr><td><agiletags:ButtonOpperation type="button" align="left" span="1" commandName="SaveCallCenterContactDetails" displayname="Save" javaScript="onClick=\"submitWithAction('SaveCallCenterContactDetails');\"" /></td></tr>
                                    </c:if>
                                </table>
                            </agiletags:ControllerForm>  
                        </div>
                    </fieldset>
                </td>
            </tr>
        </table>
    </body>
</html>

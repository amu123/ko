<%-- 
    Document   : BiometricsInfoTag
    Created on : 17 Feb 2015, 2:50:52 PM
    Author     : gerritr
--%>

<%@tag description="This table is displayed when the Biometrics Dropdown is selected" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>   

<%@attribute name="commandName" required="true" type="java.lang.String" %>
<%@attribute name="javaScript" required="true" type="java.lang.String" %>
<%@attribute name="sessionAttribute" required="true" type="java.lang.String" %>
<%@attribute name="onScreen" required="true" type="java.lang.String" %>

<c:if test="${!empty sessionScope.ViewBiometricsInfo}">
    <label class="subheader">Biometric Detail [${sessionScope.ddSelection}]</label>
    <br/>
    <br/>
    <HR color="#666666" WIDTH="100%" align="left">
    <table width="100%" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
        <tr>
            <th align="left">Captured Date</th>
            <th align="left">Captured By</th>
            <th align="left">Diagnosis Code</th>
            <th align="left">Diagnosis Description</th>
            <th align="left">History</th>
        </tr>

        <c:set var="indexer" value="0" />
        <c:forEach var="bioView" items="${sessionScope.ViewBiometricsInfo}" varStatus="i">
            <tr class="label">
                <%--<td>${agiletags:formatXMLGregorianDate(bioView.capturedDate)}</td>--%>
                <td>${fn:replace(fn:replace(fn:substring(bioView.capturedDate,0, 10),'-','/'),'2999/12/31','')}</td>
                <td>${bioView.capturedBy}</td>
                <td>${bioView.diagnosisCode}</td>
                <td>${bioView.diagnosisDescription}</td>
                <td style="alignment-adjust: central"><button name="opperation" type="button" ${javaScript} value="${indexer}">View</button></td>
            </tr>
            <c:set var="indexer" value="${indexer + 1}" />
        </c:forEach>
    </table>
</c:if>
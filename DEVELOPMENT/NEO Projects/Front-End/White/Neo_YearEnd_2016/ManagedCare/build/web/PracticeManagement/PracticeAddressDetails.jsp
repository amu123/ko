<%-- 
    Document   : PracticeAddressDetails
    Created on : 04 Mar 2014, 11:17:51 AM
    Author     : almaries
--%>

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<div id="tabTableLayout">
    <agiletags:ControllerForm name="MemberAddressDetailsForm">
        <input type="hidden" name="opperation" id="PracticeAddress_opperation" value="SavePracticeAddressDetailsCommand" />
        <input type="hidden" name="onScreen" id="PracticeAddress_onScreen" value="PracticeAddressDetails" />

        <label class="subheader">Practice Address Details</label>
        <HR WIDTH="80%" align="left">
        <br/>
        <label class="subheader">Physical Address</label>
        <table id="PhysicalAddressTable">
            <tr id="PracticeDetails_physicalAddressLine1Row"><agiletags:LabelTextBoxErrorReq displayName="Line1" elementName="AddressDetails_res_addressLine1" valueFromRequest="AddressDetails_res_addressLine1" mandatory="no"/>        </tr>
            <tr id="PracticeDetails_physicalAddressLine2Row"><agiletags:LabelTextBoxErrorReq displayName="Line2" elementName="AddressDetails_res_addressLine2" valueFromRequest="AddressDetails_res_addressLine2" mandatory="no"/>        </tr>
            <tr id="PracticeDetails_physicalAddressLine3Row"><agiletags:LabelTextBoxErrorReq displayName="Line3" elementName="AddressDetails_res_addressLine3" valueFromRequest="AddressDetails_res_addressLine3" mandatory="no"/>        </tr>
            <tr id="PracticeDetails_physicalCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Physical Code" elementName="AddressDetails_res_postalCode" valueFromRequest="AddressDetails_res_postalCode" mandatory="no"/>        </tr>
        </table>
        <br/>    
        <label class="subheader">Postal Address</label>
        <table id="PostalAddressTable">
            <tr id="PracticeDetails_postalAddressLine1Row"><agiletags:LabelTextBoxErrorReq displayName="Line1" elementName="AddressDetails_pos_addressLine1" valueFromRequest="AddressDetails_pos_addressLine1" mandatory="no"/>        </tr>
            <tr id="PracticeDetails_postalAddressLine2Row"><agiletags:LabelTextBoxErrorReq displayName="Line2" elementName="AddressDetails_pos_addressLine2" valueFromRequest="AddressDetails_pos_addressLine2" mandatory="no"/>        </tr>
            <tr id="PracticeDetails_postalAddressLine3Row"><agiletags:LabelTextBoxErrorReq displayName="Line3" elementName="AddressDetails_pos_addressLine3" valueFromRequest="AddressDetails_pos_addressLine3" mandatory="no"/>        </tr>
            <tr id="PracticeDetails_postalCodeRow"><agiletags:LabelTextBoxErrorReq displayName="Postal Code" elementName="AddressDetails_pos_postalCode" valueFromRequest="AddressDetails_pos_postalCode" mandatory="no"/>        </tr>
        </table>
        <br/>
        <table id="MemberPersonalSaveTable">
            <tr>
                <td><input type="reset" value="Reset"></td>
                <td><input type="button" value="Save" onclick="submitFormWithAjaxPost(this.form, 'PracticeAddressDetails')"></td>
            </tr>
        </table>

    </agiletags:ControllerForm>
</div>


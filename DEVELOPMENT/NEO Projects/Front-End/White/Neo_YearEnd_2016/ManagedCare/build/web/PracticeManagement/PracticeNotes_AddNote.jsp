<%-- 
    Document   : ClaimNotes_AddNote
    Created on : 27 May 2016, 9:52:08 AM
    Author     : janf
--%>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <!--<link rel="stylesheet" href="/ManagedCare/resources/bootstrap/bootstrap.min.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/bootstrap/bootstrap-theme.min.css"/>
        <!--<script type="text/javascript" src="/ManagedCare/resources/jquery-2.2.0.min.js"></script>-->
        <script type="text/javascript" src="/ManagedCare/resources/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
    </head>
    <body>
        <agiletags:ControllerForm name="savePracticeNotesForm">
            <input type="hidden" name="opperation" id="PracticeNotes_opperation" value="PracticeManagementNotesCommand" />
            <input type="hidden" name="onScreen" id="onScreen" value="addNote" />
            <input type="hidden" name="command" id="command" value="SaveNote" />
            <input type="hidden" name="entityId" id="entityId" value="${entityId}"/>
            <agiletags:HiddenField elementName="searchCalled" />
            <label class="header">Add Note</label>
            <hr>
            <br>
            <table width=100% height=100%>
                <tr>
                    <td align="left" width="160px"><label>Note:</label></td>
                    <td align="left">



                        <textarea class="ckeditor" id="notes" name="notes" rows="20" cols="50"></textarea>
                        <script type="text/javascript">
                            jQuery(document).ready(function () {
                                CKEDITOR.replace('notes',
                                        {
                                            toolbar: 'Basic'
                                        });

                            });
                        </script>
                    </td>
                </tr>
            </table>
            <hr> 
            <table>
                <tr>
                    <td><agiletags:ButtonOpperation align="left" commandName="" displayname="Save" span="2" type="button" javaScript="onclick=\"savePracticeNote(this.form, null, this);returnToViewNotes();\""/></td>
                    <td><agiletags:ButtonOpperation align="right" commandName="" displayname="Back" span="2" type="button" javaScript="onClick=\"returnToViewNotes();\"" /></td>
                </tr>
            </table>
            <div id="PracticeNotes_Summary">
            </div>  
        </agiletags:ControllerForm>
    </body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
    <br>
    <label class="subheader">Broker for Member Search Results</label>
    <hr>
    <br>
        <c:choose>
            <c:when test="${empty BrokerSearchResults}">
            <span>No broker found</span>
            </c:when>
            <c:otherwise>
                <table class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                    <tr>
                        <th>Broker Code</th>
                        <th>Broker Name</th>
                        <th></th>
                    </tr>
                    <c:forEach var="entry" items="${BrokerSearchResults}">
                        <tr>
                            <td><label>${entry.brokerCode}</label></td>
                            <td><label>${entry.fstName}</label></td>
                            <td><input type="button" value="Select" onclick="setElemValues({'${diagCodeId}':'${entry.brokerCode}','${diagNameId}':'${agiletags:escapeStr(entry.fstName)}','brokerEntityId':'${entry.brokerEntityId}'});swapDivVisbible('${target_div}','${main_div}');"/></td> 
                        </tr>
                    </c:forEach>

                </table>
            </c:otherwise>
        </c:choose>

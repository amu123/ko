<%-- 
    Document   : PDCCascadedDropdownDocumentList
    Created on : 25 Mar 2015, 10:27:54 AM
    Author     : shimanem
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@ taglib prefix="cct" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="javascript" rtexprvalue="true" required="true" %>
<%@attribute name="id" rtexprvalue="true" required="true"%>
<%@ attribute name="displayName" rtexprvalue="true" required="true" %>
<td class="label" align="left" width="160px">${displayName}:</td>
<td align="left" width="200px">
    <select style="width:200px" id="optSelectName" name="optSelectName">
        <option value="0"></option>      
            <c:if test="${sessionScope.optIndex == '290'}">
                <c:forEach var="items" items="${sessionScope.documentGenerationMap}">
                    <option value="${items.key}" id="optCDL">${items.value}</option>
                </c:forEach>
            </c:if>
            <c:if test="${sessionScope.optIndex == '291'}">
                <c:forEach var="items" items="${sessionScope.documentOtherCDLMap}">
                    <option value="${items.key}" id="optCDL">${items.value}</option>
                </c:forEach>
            </c:if>     
    </select> 
</td>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<agiletags:ControllerForm name="ResignMemberForm">
    <input type="hidden" name="opperation" id="opperation" value="SaveMemberCoverDetailsCommand" />
    <input type="hidden" name="onScreen" id="onScreen" value="ResignMember" />
    <input type="hidden" name="buttonPressed" id="disorderValuesButtonPressed" value="" />
    <input type="hidden" name="memberEntityId" id="ResignEntityId" value="${memberCoverEntityId}" />
    <input type="hidden" name="brokerEntityId" id="brokerEntityId" value="${requestScope.brokerEntityId}" />
    <br/>
    <br/>
    <label class="header">Change Inception Date</label>
    <br/>
    <br/>
    <hr>
    <br/>
    <br/>
    <table>                                
        <tr id="resignReasonRow">
            <td align="left" width="160px"><label>Dependant</label></td><td align="left" width="200px">
                <select style="width:215px" name="deptList" id="resignDeptList">
                    <c:forEach var="entry" items="${MemberCoverDependantDetails}">
                        <option value="${entry.dependentNumber}" selected>${entry.dependentNumber} ${entry.name} Inception Date : ${agiletags:formatXMLGregorianDate(entry.schemeJoinDate)}</option>
                    </c:forEach>                      
                </select>
            </td>
        </tr>        
        <tr id="inceptionStartDateRow"><agiletags:LabelTextBoxDateReq  displayname="Inception Date" elementName="inceptionStartDate" valueFromSession="yes" mandatory="yes"/>        </tr> 
        
    </table>
    <br/>
    <br/>
    <table id="MemberResignTable">
        <tr>
            <td><input type="button" value="Cancel" onclick="swapDivVisbible('${target_div}','${main_div}');"></td>
            <td><input type="button" value="Save" onclick="document.getElementById('disorderValuesButtonPressed').value='SaveInceptionButton';submitFormWithAjaxPost(this.form, 'MemberCover', null,'${main_div}', '${target_div}');"></td>
        </tr>
    </table>
</agiletags:ControllerForm>



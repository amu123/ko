<%-- 
    Document   : AuthSpecificBasketTariff
    Created on : 2010/06/30, 09:31:33
    Author     : johanl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
        <title></title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" language="JavaScript">

            function toggelAddBasket() {
            <%--                var searchOpperation = '<%= session.getAttribute("opperation")%>';
                            if(searchOpperation == 'ForwardToAuthTariffSearchBasketCommand'){
                                $("#normal").show();
                                $("#specific").hide();

                }else if(searchOpperation == 'ForwardToAuthBasketTariffSearch'){
                    $("#specific").show();
                    $("#normal").hide();

                }--%>
                    }

                    function GetXmlHttpObject() {
                        if (window.ActiveXObject) {
                            // code for IE6, IE5
                            return new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        if (window.XMLHttpRequest) {
                            // code for IE7+, Firefox, Chrome, Opera, Safari
                            return new XMLHttpRequest();
                        }

                        return null;
                    }

                    function getTariffForCode() {
                        var str = document.getElementById("tariffCode").value;
                        xmlhttp = GetXmlHttpObject();
                        if (xmlhttp == null) {
                            alert("Your browser does not support XMLHTTP!");
                            return;
                        }
                        var url = "/ManagedCare/AgileController";
                        url = url + "?opperation=GetAuthTariffByCodeCommand&tariffCode=" + str;

                        xmlhttp.onreadystatechange = function() {
                            stateChanged("")
                        };
                        xmlhttp.open("POST", url, true);
                        xmlhttp.send(null);
                    }

                    function CalculateTariffQuantity(str) {
                        xhr = GetXmlHttpObject();
                        if (xhr == null) {
                            alert("Your browser does not support XMLHTTP!");
                            return;
                        }
                        var url = "/ManagedCare/AgileController";
                        url = url + "?opperation=CalculateTariffQuantityCommand&quantity=" + str + "&tariffType=clinic";
                        xhr.onreadystatechange = function() {
                            if (xhr.readyState == 4 && xhr.statusText == "OK") {
                                var resText = xhr.responseText;
                                var strArr = new Array();
                                strArr = resText.split('|');

                                var tQuan = new String(strArr[1]);
                                var tAmount = new String(strArr[100]);

                                document.getElementById("tariffQuantity").value = tQuan;
                                document.getElementById("tariffAmount").value = tAmount;
                            }
                        };
                        xhr.open("POST", url, true);
                        xhr.send(null);
                    }
                    function ModifyBTFromList(index) {
                        var x = document.forms[index];
                        var btHidden = new String();
                        for (var i = 0; i < x.length; i++)
                        {
                            if (x.elements[i].id == "btHidden")
                                btHidden = x.elements[i].value;

                        }

                        xhr = GetXmlHttpObject();
                        if (xhr == null) {
                            alert("Your browser does not support XMLHTTP!");
                            return;
                        }
                        var url = "/ManagedCare/AgileController";
                        url = url + "?opperation=ModifyAuthBasketTariffFromList&btHidden=" + btHidden;
                        xhr.onreadystatechange = function() {
                            if (xhr.readyState == 4 && xhr.statusText == "OK") {
                                var rText = xhr.responseText.split("|");
                                var result = rText[0];
                                if (result == "Done") {
                                    // disc code desc
                                    $("#pcType").val(rText[1]);
                                    $("#disc").text(rText[1]);

                                    $("#basketCode").val(rText[2]);
                                    $("#code").text(rText[2]);

                                    $("#basketDesc").val(rText[3]);
                                    $("#desc").text(rText[3]);
                                    $("#tariffFreq").val(rText[4]);


                                    var selected = $("tr[id=btRow" + btHidden + index + "]");
                                    for (var j = 0; j < selected.size(); j++) {
                                        $(selected[j]).hide();
                                    }

                                } else if (result == "Error") {
                                    alert(result);
                                }
                            }
                        };
                        xhr.open("POST", url, true);
                        xhr.send(null);
                    }
                    function RemoveBTFromList(index) {
                        var x = document.forms[index];
                        var btHidden = new String();
                        for (var i = 0; i < x.length; i++)
                        {
                            if (x.elements[i].id == "btHidden")
                                btHidden = x.elements[i].value;

                        }

                        xhr = GetXmlHttpObject();
                        if (xhr == null) {
                            alert("Your browser does not support XMLHTTP!");
                            return;
                        }

                        var url = "/ManagedCare/AgileController";
                        url = url + "?opperation=RemoveAuthBasketTariffFromList&btHidden=" + btHidden;
                        xhr.onreadystatechange = function() {
                            if (xhr.readyState == 4 && xhr.statusText == "OK") {
                                var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                                if (result == "Done") {
                                    var selected = $("tr[id=btRow" + btHidden + index + "]");
                                    for (var j = 0; j < selected.size(); j++) {
                                        $(selected[j]).hide();
                                    }
                                } else if (result == "Error") {
                                    alert(result);
                                }
                            }
                        };
                        xhr.open("POST", url, true);
                        xhr.send(null);

                    }

                    function validateFreq(str) {
                        if (str != null && str != "") {
                            var valCheck = freqRegex(str);
                            if (valCheck == true) {
                                $("#btGrid_error").text("");
                            } else if (valCheck == false) {
                                $("#btGrid_error").text("Invalid Frequency");
                            }
                        } else {
                            $("#btGrid_error").text("Empty Frequency Not Allowed");
                        }
                    }

                    function freqRegex(str) {
                        var check1 = /^[0-9]+$/;
                        var numCheck = check1.test(str);
                        return numCheck;
                    }

                    function ClearTariffFieldsFromSession() {
                        xhr = GetXmlHttpObject();
                        if (xhr == null) {
                            alert("Your browser does not support XMLHTTP!");
                            return;
                        }
                        var url = "/ManagedCare/AgileController";
                        url = url + "?opperation=ClearAuthTariffCommand";
                        xhr.onreadystatechange = function() {
                            stateChanged("clearTariffFields")
                        };
                        xhr.open("POST", url, true);
                        xhr.send(null);
                    }

                    function addBTtoSession(command) {
                        xhr = GetXmlHttpObject();
                        if (xhr == null) {
                            alert("Your browser does not support XMLHTTP!");
                            return;
                        }
                        var tariffFreq = $("#basketFreq").val();
                        if (tariffFreq == "" || tariffFreq == null) {
                            $("#btGrid_error").text("Empty Frequency Not Allowed");

                        } else {
                            var url = "/ManagedCare/AgileController";
                            url = url + "?opperation=" + command + "&tariffFreq=" + tariffFreq;
                            xhr.onreadystatechange = function() {
                                if (xhr.readyState == 4 && xhr.statusText == "OK") {
                                    var result = xhr.responseText.substring(0, xhr.responseText.indexOf('|'));
                                    if (result == "Done") {
                                        $("#btGrid_error").text("");

                                    } else {
                                        $("#btGrid_error").text("Error");
                                    }
                                }
                            };
                            xhr.open("POST", url, true);
                            xhr.send(null);
                        }

                    }

                    function clearElement(element) {
                        part = element + '_text';
                        document.getElementById(element).value = '';
                        document.getElementById(part).value = '';
                    }
                    function submitWithAction(action) {
                        document.getElementById('opperation').value = action;
                        document.forms[0].submit();
                    }
                    function submitWithAction(action, forField, onScreen) {
                        document.getElementById('onScreen').value = onScreen;
                        document.getElementById('searchCalled').value = forField;
                        document.getElementById('opperation').value = action;
                        document.forms[0].submit();
                    }

                    function submitSpecWithAction(action, forField, onScreen) {
                        document.getElementById('asbtOnScreen').value = onScreen;
                        document.getElementById('searchCalled').value = forField;
                        document.getElementById('opperation').value = action;
                        document.forms[0].submit();
                    }

        </script>
    </head>
    <body>
        <table width=100% height=100%><tr valign="top"><td width="50px"></td><td align="left">
                    <label class="header">Basket Specific Tariff Details</label>
                    <br/>
                    <br/>
                    <agiletags:ControllerForm name="saveBasketTariff">
                        <input type="hidden" name="opperation" id="opperation" value="" />
                        <agiletags:HiddenField elementName="searchCalled"/>
                        <input type="hidden" name="onScreen" id="onScreen" value="" />
                        <table>
                            <tr>
                                <agiletags:ButtonOpperation align="left" commandName="" displayname="Search Scheme Specific Tariff Basket" span="1" type="button" javaScript="onClick=\"submitWithAction('ForwardToAuthTariffSearchBasketCommand');\""/>
                                <agiletags:ButtonOpperation align="left" commandName="" displayname="Search Tariff List" span="1" type="button" javaScript="onClick=\"submitWithAction('ForwardToAuthBasketTariffSearch');\"" />
                            </tr>
                        </table>  
                        <br/>
                        <table>
                            <tr><agiletags:BasketTariffSearchTag commandName="" javaScript="onClick=\"submitWithAction('AddAuthBasketTariffToSession');\"" /></tr>
                            <!--<tr id="specific"><agiletags:BasketSpecTariffSearchTag commandName="" javaScript="onClick=\"submitWithAction('AddAuthBasketTariffToSession');\"" /></tr>-->
                        </table>  
                        <br/>
                        <table>
                            <tr><agiletags:AuthBasketTariffTableTag sessionAttribute="AuthBasketTariffs" /></tr>
                            <tr><agiletags:ButtonOpperation align="right" commandName="" displayname="Continue" span="1" type="button" javaScript="onClick=\"submitWithAction('ReturnAuthBasketTariffCommand');\"" /></tr>
                        </table>
                    </agiletags:ControllerForm>
                </td></tr></table>
    </body>
</html>

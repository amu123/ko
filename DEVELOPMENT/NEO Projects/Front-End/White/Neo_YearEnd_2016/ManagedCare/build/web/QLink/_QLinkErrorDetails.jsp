<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>

        <c:choose>
            <c:when test="${empty BatchResults}">
            <span>No results found</span>
            </c:when>
            <c:otherwise>
                <br>
                <h2>Batch Details</h2>
                <br>
                <input type="checkbox" checked="true" value="Data Load" onclick="$('.line_status_1').css('display',this.checked?'table-row':'none' )">Show Not Viewed Items
                <input type="checkbox" checked="true" value="Viewed"  onchange="$('.line_status_2').css('display',this.checked?'table-row':'none' )">Show Viewed Items
                <agiletags:ControllerForm name="QLinkErrorsDetailsForm">
                    <input type="hidden" name="opperation" id="opperationDetails" value="QLinkErrorsCommand" />
                    <input type="hidden" name="command" id="QLinkErrorsDetailsCommand" value="selectDetails" />
                    <input type="hidden" name="detailsId" id="detailsId" value="">
                    <input type="hidden" name="coverNumber" id="coverNumber" value="">
                    <input type="hidden" name="detailLineStatus" id="detailLineStatus" value="">
                    <table id="DetailsTable" class="list" style="border-style:none; border-collapse:collapse; border-width:1px;">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Cover Number</th>
                                <th>Employee Number</th>
                                <th>Name</th>
                                <th>ID Number</th>
                                <th>Error Code</th>
<!--                                 <th>Error Description</th> -->
                                <th>Status</th>
                                <th>User</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:set var="errorCodeTest" value=""></c:set>
                            <c:forEach  var="entry2" items="${BatchResults}">
                                <c:set var="errorCode" value="${entry2['ERROR_CODE']}"></c:set>
                                <c:if test="${errorCodeTest != errorCode}">
                                    </tbody>
                                    <tbody>
                                    <tr>
                                        <td><input type="Button" value="+" onclick="toggle_table_group(this, 'errorCode_${entry2['ERROR_CODE']}');"></td>
                                        <td colspan="7">
                                            <label><b>Error Code: ${entry2['ERROR_CODE']}</b> - ${entry2['ERROR_DESCRIPTION']}</label>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tbody id="errorCode_${entry2['ERROR_CODE']}"  style="display:none;">
                                </c:if>
                                        <tr class="line_status_${entry2['STATUS']}">
                                    <td>&nbsp;</td>
                                    <td><label><a href="#${entry2['COVER_NUMBER']}"  onclick="selectDetailLine('${entry2['QLINK_DETAIL_ID']}','${entry2['COVER_NUMBER']}', '${entry2['STATUS']}');">${entry2['COVER_NUMBER']}</a></label></td>
                                    <td><label>${entry2['EMPLOYEE_NUMBER']}</label></td>
                                    <td><label>${entry2['INITIALS']} ${entry2['SURNAME']}</label></td>
                                    <td><label>${entry2['ID_NUMBER']}</label></td>
                                    <td><label>${entry2['ERROR_CODE']}</label></td>
<!--                                    <td>${entry2['ERROR_DESCRIPTION']}</td> -->
                                    <td><label>${entry2['STATUS_DESCRIPTION']}</label></td>
                                    <td><label>${entry2['USER_NAME']}</label></td>
                                </tr>
                                <c:set var="errorCodeTest" value="${entry2['ERROR_CODE']}"></c:set>
                            </c:forEach>
                        </tbody>
                    </table>
                </agiletags:ControllerForm>
            </c:otherwise>
        </c:choose>
                        
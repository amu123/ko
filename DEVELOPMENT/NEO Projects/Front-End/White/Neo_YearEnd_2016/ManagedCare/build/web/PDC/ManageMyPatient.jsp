<%-- 
    Document   : ManageMyPatient
    Created on : 2016/05/26, 09:58:55
    Author     : dewaldo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="/WEB-INF/tlds/AgileTags" prefix="agiletags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PDC WorkBench</title>
        <link rel="stylesheet" href="/ManagedCare/resources/styles.css"/>
        <link rel="stylesheet" href="/ManagedCare/resources/tab.css"/>
        <script type="text/javascript" src="/ManagedCare/resources/CallCenter/CallCenterTabUtil.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/jQuery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/datePicker/DatePicker.js"></script>
        <script type="text/javascript" src="/ManagedCare/resources/dateTimePicker/datetimepicker_css.js"></script>
        <script>

            $(document).ready(function () {
                var ViewDepCarePathBtn = document.getElementById("ViewDepCarePathBtn");
                var CreateConditionSpecific = document.getElementById("CreateConditionSpecific");

                if (ViewDepCarePathBtn !== null) {
                    ViewDepCarePathBtn.style.visibility = 'hidden';
                }

                if (CreateConditionSpecific !== null) {
                    CreateConditionSpecific.style.visibility = 'hidden';
                }


                resizeContent();
                //attach on resize event
                $(window).resize(function () {
                    resizeContent();
                });
            });

            function submitWithAction(action) {
                document.getElementById('opperation').value = action;
                document.forms[0].submit();
            }

            function submitWithAction(action, index) {
                document.getElementById('opperation').value = action;
                document.getElementById('listIndex').value = index;
                document.forms[0].submit();
            }

            function GetXmlHttpObject()
            {
                if (window.XMLHttpRequest)
                {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    return new XMLHttpRequest();
                }
                if (window.ActiveXObject)
                {
                    // code for IE6, IE5
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                return null;
            }

            function hideSpecified(value, element) {
                if (value === '1') {
                    element.style.visibility = 'hidden';
                } else if (value === '0') {
                    element.style.visibility = 'visible';
                } else {
                    element.style.visibility = 'hidden';
                }
            }

            function hideElement(value) {
                var ViewDepCarePathBtn = document.getElementById("ViewDepCarePathBtn");
                var CreateConditionSpecific = document.getElementById("CreateConditionSpecific");

                if (ViewDepCarePathBtn !== null) {
                    hideSpecified(value, ViewDepCarePathBtn);
                }

                if (CreateConditionSpecific !== null) {
                    hideSpecified(value, CreateConditionSpecific);
                }
            }
        </script>
    </head>
    <body>
        <table width=100% height=100%>
            <tr valign="top">
                <td width="5px">

                </td>
                <td align="left">
                    <agiletags:PDCTabs tabSelectedIndex="link_ME" />
                    <fieldset id="pageBorder">
                        <div class="content_area">
                            <agiletags:ControllerForm name="viewWorkBench">
                                <input type="hidden" name="opperation" id="opperation" value="" />
                                <input type="hidden" name="listIndex" id="listIndex" value="" />
                                <label class="header">Manage My Patient</label>
                                <br/>
                                <br/>
                                <table>
                                    <tr>
                                        <agiletags:LabelTextBoxError 
                                            displayName="Member Name" 
                                            elementName="patientName" 
                                            valueFromSession="yes" 
                                            readonly="yes"/>
                                    </tr>
                                    <tr>
                                        <agiletags:LabelTextBoxError 
                                            displayName="Member Surname" 
                                            elementName="surname" 
                                            valueFromSession="yes" 
                                            readonly="yes"/>
                                    </tr>
                                    <tr>
                                        <agiletags:LabelTextBoxError 
                                            displayName="Policy Number" 
                                            elementName="policyNumber" 
                                            valueFromSession="yes" 
                                            readonly="yes"/>
                                    </tr>
                                    <tr>
                                        <agiletags:LabelTextBoxError 
                                            displayName="Option Name" 
                                            elementName="optionName" 
                                            valueFromSession="yes" 
                                            readonly="yes"/>
                                    </tr>
                                </table>
                                <table width="224px">
                                    <tr>
                                        <agiletags:LabelNeoLookupValueDropDown 
                                            displayName="Do you want to close the event?" 
                                            elementName="closeEvent" 
                                            lookupId="67" 
                                            javaScript="onChange=\"hideElement(this.value)\";"/>
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                    <button 
                                        type="button" 
                                        name="SaveManageMyPatientCommand" 
                                        onclick="submitWithAction('SaveManageMyPatientCommand');">
                                        Save
                                    </button>
                                    </tr>
                                </table>
                                <br><br>
                                <table>
                                    <tr>
                                        <td>
                                            <button 
                                                style="width: 300px; text-align: center" 
                                                type="button" 
                                                name="commandName"
                                                id="CreateConditionSpecific"    
                                                onclick="submitWithAction('ForwardToConditionSpecificAuth');">
                                                Create New Authorisation
                                            </button>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                                <br>
                                <table>
                                    <tr>
                                        <td id="ViewDepCarePathBtn"> 
                                            <c:choose>
                                                <c:when test="${sessionScope.depHasCarePath == 'true'}">
                                                    <button 
                                                        style="width: 300px; text-align: center;" 
                                                        type="button" 
                                                        name="ViewDepCarePathBtn1" 
                                                        id="ViewDepCarePathBtn1"    
                                                        onclick="submitWithAction('ViewDepCarePathCommand');">
                                                        View Care Path
                                                    </button>
                                                </c:when>
                                                <c:otherwise>
                                                    <button 
                                                        style="width: 300px; text-align: center;" 
                                                        type="button" 
                                                        name="ViewDepCarePathBtn2" 
                                                        id="ViewDepCarePathBtn2"
                                                        onclick="submitWithAction('ViewChronicConditionSelectionCommand');">
                                                        Create Care Path
                                                    </button>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                                <br/>
                            </agiletags:ControllerForm>
                        </div>
                    </fieldset>
                </td>
            </tr>
        </table>
    </body>
</html>

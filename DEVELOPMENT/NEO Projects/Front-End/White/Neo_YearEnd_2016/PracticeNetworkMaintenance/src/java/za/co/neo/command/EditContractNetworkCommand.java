/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.neo.command;

import com.koh.network_management.command.NeoUser;
import flexjson.JSONSerializer;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import za.co.agile.command.FECommand;
import za.co.resolution.NeoContractNetwork;
import za.co.resolution.NeoContractPracticeNetwork;


/**
 *
 * @author Administrator
 */
public class EditContractNetworkCommand extends FECommand {
    
    private static Logger loggerNL = Logger.getLogger(EditContractNetworkCommand.class);
    
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        loggerNL.info("**************** DEBUG EditContractNetworkCommand *******************");
        
        NeoUser user = (NeoUser) request.getSession().getAttribute("networkMaintanceNeoUser");
        
        String practiceNetworkId = request.getParameter("practiceNetworkId");
        
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        
        loggerNL.info("startDate : "   + startDate);
        loggerNL.info("endDate : "   + endDate);
        loggerNL.info("practiceNetworkId : "   + practiceNetworkId);
        
        editNetworkPracticeDates(response, startDate,
                endDate, Long.parseLong(practiceNetworkId),user);
        
        
        loggerNL.info("******************************************");
        
        return null;
    }

    private void editNetworkPracticeDates(HttpServletResponse response,String startDate
            ,String endDate,Long practiceNetworkId,NeoUser user) {
        
          
        NeoContractPracticeNetwork contractPracticeNetwork = 
                neoContractPracticeNetworkJpaController.findNeoContractPracticeNetwork(practiceNetworkId);
        
         
        RespondObject respondObject = new RespondObject();
        
        Date newStartDate = null;
        Date newEndDate = null;
        
        try {
            newStartDate = sdfDate.parse(startDate);
            newEndDate = sdfDate.parse(endDate);
            
        } catch (ParseException ex) {
            loggerNL.error(ex);
        }
        
        
        contractPracticeNetwork.setEffectiveStartDate(newStartDate);
        contractPracticeNetwork.setStartDate(newStartDate);

        contractPracticeNetwork.setEffectiveEndDate(newEndDate);
        contractPracticeNetwork.setEndDate(newEndDate);
        
        
        contractPracticeNetwork.setLastUpdatedBy(new BigInteger("" + user.getId()));
        contractPracticeNetwork.setLastUpdateDate(new Date());

        contractPracticeNetwork.setSecurityGroupId(new BigInteger("" + user.getSecurityGroupId()));
        
        try {
            loggerNL.info("Updating old record....");
            neoContractPracticeNetworkJpaController.edit(contractPracticeNetwork);
            loggerNL.info("Old record updated");
             respondObject.done = 1;
        } catch (Exception ex) {
            loggerNL.error(ex);
        }
       
        PrintWriter out = null;

        try {
            out = response.getWriter();

            out.println(new JSONSerializer()
                 .deepSerialize(respondObject));

        } catch (Exception ex) {
            loggerNL.error(ex);
        } finally {
            out.close();
        } 

        
        
    }
    
    
    private void processAddPracticeToNetwork(HttpServletResponse response,String startDate,String endDate,Integer networkId,Long entityCommonId) {
        
        RespondObject respondObject = new RespondObject();
        
        Date newStartDate = null;
        Date newEndDate = null;

        try {
            newStartDate = sdfDate.parse(startDate);
            newEndDate = sdfDate.parse(endDate);

        } catch (ParseException ex) {
            loggerNL.error(ex);
            respondObject.done = 0;
        }
        
        //LAST CONTRACT NETWORK FOR PROVIDER
        NeoContractPracticeNetwork newContractPracticeNetwork = new NeoContractPracticeNetwork();
        
        newContractPracticeNetwork.setCreatedBy(new BigInteger("1"));
        newContractPracticeNetwork.setCreationDate(new Date());

        newContractPracticeNetwork.setEffectiveStartDate(newStartDate);
        newContractPracticeNetwork.setStartDate(newStartDate);

        newContractPracticeNetwork.setEffectiveEndDate(newEndDate);
        newContractPracticeNetwork.setEndDate(newEndDate);

        newContractPracticeNetwork.setSecurityGroupId(new BigInteger("2"));
        
        newContractPracticeNetwork.setLastUpdatedBy(new BigInteger("2"));
        newContractPracticeNetwork.setLastUpdateDate(new Date());
        
        loggerNL.info("Finding NeoContractNetwork for networkId : " + networkId);
        //FIND CONTRACT NETWORK FOR CHOSEN NETWORK BY NETWORK ID
        NeoContractNetwork contractNetwork = neoContractNetworkJpaController.findNeoContractNetworkByNetworkId(networkId);
        
        Long entityId;
        try {
            //FIND NEO_ENTITY_COMMON RECORD TO RETREIVE ENTITY_ID
            entityId = neoEntityCommonJpaController.retreiveEntityIdByCommonId(entityCommonId, sdfDate.parse("2999/12/31"));
            loggerNL.info("entityId for practice  : " + entityId);
            
            //SET ENTITY ID FOR NEW CONTRACT PRACTICE NETWORK
            newContractPracticeNetwork.setEntityId(entityId);
            
        } catch (ParseException ex) {
            loggerNL.error(ex);
        }
        
        if (contractNetwork!=null) {
            
            loggerNL.info("NetworkName : " + contractNetwork.getNetworkDescription());
            newContractPracticeNetwork.setNetworkId(contractNetwork);
            
            
            
            try {
                loggerNL.info("Starting insert of new record...");
                neoContractPracticeNetworkJpaController.create(newContractPracticeNetwork);
                loggerNL.info("record inserted!");
                respondObject.done = 1;

            } catch (Exception ex) {
                loggerNL.error(ex);
                respondObject.done = 0;
            }

        } else {
            respondObject.done = 0;
        }
        
        PrintWriter out = null;

        try {
            out = response.getWriter();

            out.println(new JSONSerializer()
                 .deepSerialize(respondObject));

        } catch (Exception ex) {
            loggerNL.error(ex);
        } finally {
            out.close();
        } 

        
    
    }
    
    @Override
    public String getName() {
        return "EditContractNetworkCommand";
    }
    
    private class RespondObject {
    
        int done;
    
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.neo.command;

import flexjson.JSONSerializer;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import za.co.agile.command.FECommand;
import za.co.resolution.NeoContractPracticeNetwork;


/**
 *
 * @author Administrator
 */
public class ContractNetworkCommand extends FECommand {
    
    private static Logger loggerNL = Logger.getLogger(ContractNetworkCommand.class);
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        loggerNL.info("**************** DEBUG ContractNetworkCommand *******************");
        String entityCommonId = request.getParameter("entityCommonId");
        
        loggerNL.info("Received entity Common Id : "   + entityCommonId);
        
        List<NeoContractPracticeNetwork> networkHistory = 
                neoContractPracticeNetworkJpaController.findNeoContractPracticeNetworkByEntityCommonId(Long.parseLong(entityCommonId));
        
        loggerNL.info("wickit... total networkHistory found : " + networkHistory.size()); 
       
        
        String contractNetworksJson = new JSONSerializer()
                 
                 .exclude("networkId.neoContractPracticeNetworkSet")
                 
                 .deepSerialize(networkHistory);
         
         /*       .exclude("patient.encounters")
                .exclude("patient.patientAddress")
                .exclude("patient.patientContact")
                .exclude("patient.calendarEvents")
           */     
        
        loggerNL.info(contractNetworksJson);
        
        PrintWriter out = null;

        try {
            out = response.getWriter();
            out.println(contractNetworksJson);
        } catch (Exception ex) {
            loggerNL.error(ex);
        } finally {
            out.close();
        }

        loggerNL.info("******************************************");
            
        
        return null;
    }

    @Override
    public String getName() {
        return "ContractNetworkCommand";
    }
}

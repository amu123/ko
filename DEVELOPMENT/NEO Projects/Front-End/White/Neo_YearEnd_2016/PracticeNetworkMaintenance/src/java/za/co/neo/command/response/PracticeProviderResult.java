/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.neo.command.response;

/**
 *
 * @author Administrator
 */
public class PracticeProviderResult {
    
    String practiceNumber;
    String practiceName;
    private Long entityCommonId;
    private Long entityId;
    
    
    public PracticeProviderResult() {

    }

    
    
    public String getPracticeName() {
        return practiceName;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public void setPracticeName(String practiceName) {
        this.practiceName = practiceName;
    }

    public Long getEntityCommonId() {
        return entityCommonId;
    }

    
    public String getPracticeNumber() {
        return practiceNumber;
    }

    public void setPracticeNumber(String practiceNumber) {
        this.practiceNumber = practiceNumber;
    }

    public void setEntityCommonId(Long entityCommonId) {
        this.entityCommonId = entityCommonId;
    }

   
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.neo.command;

import com.koh.network_management.command.NeoUser;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import za.co.agile.command.FECommand;


/**
 *
 * @author Administrator
 */
public class StartupCommand extends FECommand {
    
    private static Logger loggerNL = Logger.getLogger(StartupCommand.class);
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        loggerNL.info("**************** DEBUG StartupCommand *******************");
        
        loggerNL.info("request context : " + request.getContextPath());
        
        NeoUser user = (NeoUser) context.getAttribute("networkMaintanceNeoUser");
        
        PrintWriter out = null;
        
        try {
            out = response.getWriter();
        } catch (IOException ex) {
            loggerNL.error(ex);
        }
        
        if (user==null) {
            out.println("failed");
            loggerNL.error("User not found !");
            
        } else {
            loggerNL.info("User found !");
            
            //ADD TO SESSIONS
            request.getSession().setAttribute("networkMaintanceNeoUser", user);
            
            //NULL THE TEMP NEO USER IN CONTEXT.
            context.setAttribute("networkMaintanceNeoUser", null);
            out.println("success");
        }
        loggerNL.info("******************************************");
            
        
        return null;
    }

    @Override
    public String getName() {
        return "StartupCommand";
    }
}

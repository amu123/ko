/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.neo.command;

import flexjson.JSONSerializer;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import za.co.agile.command.FECommand;

/**
 *
 * @author Administrator
 */
public class NeoDisciplineTypesCommand extends FECommand {
    
    private static Logger loggerDT = Logger.getLogger(NeoDisciplineTypesCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        loggerDT.info("**************** DEBUG NeoDisciplineTypesCommand *******************");

        List<Object[]> tarrifDetailsList = new ArrayList<Object[]>();

        tarrifDetailsList = neoNetworkTariffDetailsController.findAllDistinct();

        loggerDT.info("Total returned  : " + tarrifDetailsList.size());
           
         String contractNetworksJson = new JSONSerializer()
                 .deepSerialize(tarrifDetailsList);
        
        loggerDT.info("jsonObjects : " + contractNetworksJson.toString());

        loggerDT.info("******************************************");
        
        PrintWriter out = null;

        try {
            out = response.getWriter();
            out.println(contractNetworksJson);
        } catch (Exception ex) {
            loggerDT.error(ex);
        } finally {
            out.close();
        }
        return null;
    }

    @Override
    public String getName() {
        return "NeoDisciplineTypesCommand";
    }
}

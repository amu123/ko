/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.neo.command;

import flexjson.JSONSerializer;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import za.co.agile.command.FECommand;

/**
 *
 * @author Administrator
 */
public class NetworkListLookup extends FECommand {

    private static Logger loggerNL = Logger.getLogger(NetworkListLookup.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        loggerNL.info("**************** DEBUG NetworkListLookup *******************");

        //NETWORK NAME
        String networkName = (String) request.getParameter("term");
        String yearRange = (String) request.getParameter("yearRange");
        
        //USE INTEGER LIST TO FIND VALID NETWORKS FOR CHOSEN OPTION
        List<Object[]> neoContractNetworks =
                neoContractNetworkJpaController.findNeoContractNetworkByYearAndName(yearRange,networkName);

        String contractNetworksJson = new JSONSerializer()
                 .deepSerialize(neoContractNetworks);
        
        loggerNL.info("Total networks returned : " + neoContractNetworks.size());

        loggerNL.info(contractNetworksJson);
        
        PrintWriter out = null;

        try {
            out = response.getWriter();
            out.println(contractNetworksJson);
        } catch (Exception ex) {
            loggerNL.error(ex);
        } finally {
            out.close();
        }

        loggerNL.info("******************************************");

        return null;
    }

    @Override
    public String getName() {
        return "NetworkListLookup";
    }
}

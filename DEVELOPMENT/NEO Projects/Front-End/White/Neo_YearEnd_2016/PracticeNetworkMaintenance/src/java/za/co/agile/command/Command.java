/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package za.co.agile.command;

import java.util.Enumeration;
import java.text.SimpleDateFormat;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author LeslieJj
 * Base command that other commands extends from
 */
public abstract class Command {

    public SimpleDateFormat sdfTime = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

    public SimpleDateFormat sdfTimeWithoutSeconds = new SimpleDateFormat("dd/MM/yyyy hh:mm");

    public SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
    
    public static SimpleDateFormat sdfDateXML = new SimpleDateFormat("yyyymmdd");

    /**
     *
     * @param request
     * @param response
     * @return
     * Method used for command execution
     */
    public abstract Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception;

    /**
     * Method for getting command name
     * @return
     */
    public abstract String getName();

    /**
     * Method to add screen parameters to session
     * @return
     */
    protected void saveScreenToSession(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Enumeration<String> e = request.getParameterNames();

        while (e.hasMoreElements()) {
            String param = e.nextElement();

            if (param.length() > 4) {
                String name = param.substring(param.length() - 4, param.length());
                if (!name.equalsIgnoreCase("list")) {
                    
                    session.setAttribute(param, request.getParameter(param));
                }
            } else {

                session.setAttribute(param, request.getParameter(param));
            }
        }
    }

    /**
     * Method to remove screen parameters from session
     * @return
     */
    protected void clearScreenFromSession(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Enumeration<String> e = request.getParameterNames();
        while (e.hasMoreElements()) {
            String param = e.nextElement();
            session.setAttribute(param, null);
        }
    }

     /**
     * Method to remove all parameters from session
     * @return
     */
    protected void clearAllFromSession(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Enumeration<String> e = session.getAttributeNames();
        while (e.hasMoreElements()) {
            String attribute = e.nextElement();
            //logger.info(attribute);
            if (attribute.length() > 7) {
                String name = attribute.substring(0, 7);
                    if (!name.equalsIgnoreCase("persist")) {
                        session.removeAttribute(attribute);
                    }
            } else {
                session.removeAttribute(attribute);
            }
        }
    }

    protected void clearErrorsFromSessionForScreen(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Enumeration<String> e = request.getParameterNames();
        while (e.hasMoreElements()) {
            String param = e.nextElement();
            session.setAttribute(param +"_error", null);
        }
    }
}

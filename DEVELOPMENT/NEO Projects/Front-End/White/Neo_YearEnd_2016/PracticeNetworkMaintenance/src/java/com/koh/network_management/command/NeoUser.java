/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.koh.network_management.command;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class NeoUser implements Serializable {
    
    private int id;
    private int securityGroupId; 
    private String username;
    private String password;

    public NeoUser() {
    }
    
    public NeoUser(int id,int securityGroupId, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.securityGroupId = securityGroupId;
        
    }

    public int getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(int securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
}

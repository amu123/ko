/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.neo.command;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONSerializer;
import org.apache.log4j.Logger;
import za.co.agile.command.FECommand;

/**
 *
 * @author Administrator
 */
public class NeoOptionLookupCommand extends FECommand {
    
    private static Logger loggerLC = Logger.getLogger(NeoOptionLookupCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        try {

            loggerLC.debug("**************** DEBUG NeoOptionLookupCommand *******************");
            String yearRange = request.getParameter("yearRange");

            loggerLC.debug("yearRange : " + yearRange);

            List<Object[]> optionList =
                    neoProductOptionVerJpaController.findNeoProductOptionVersionFromToDatePerYear(yearRange);

            loggerLC.debug("Total returned : " + optionList.size());
            
            List jsonObjects = new ArrayList();
            
            for (Object[] object : optionList) {
                
                loggerLC.debug("object[0].toString() : " + object[0].toString());
                loggerLC.debug("object[1].toString() : " + object[1].toString());
                loggerLC.debug("object[2].toString() : " + object[2].toString());
                
                //OptionResult result = new OptionResult(object[0].toString(), object[1].toString());
                jsonObjects.add(JSONSerializer.toJSON(object));
            }
            
            
            PrintWriter out = null;
            loggerLC.debug("JSON : " + jsonObjects.toString());
            
            loggerLC.debug("******************************************");

            try {
                out = response.getWriter();
                out.println(jsonObjects);
            } catch (Exception ex) {
                loggerLC.error(ex);
            } finally {
                out.close();
            }

        } catch (Exception e) {
            loggerLC.error(e);
        }

        return null;
    }

    @Override
    public String getName() {
        return "NeoOptionLookupCommand";
    }
}

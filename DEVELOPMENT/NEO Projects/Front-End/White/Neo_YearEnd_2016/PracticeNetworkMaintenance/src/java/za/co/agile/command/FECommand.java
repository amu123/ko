/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.agile.command;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.apache.log4j.Logger;
import za.co.resolution.NeoContractNetworkJpaController;
import za.co.resolution.NeoContractPracticeNetworkJpaController;
import za.co.resolution.NeoEntityCommonJpaController;
import za.co.resolution.NeoLookupTypesJpaController;
import za.co.resolution.NeoNegotiatedTariffDetailsJpaController;
import za.co.resolution.NeoNegotiatedTariffListJpaController;
import za.co.resolution.NeoNegotiatedTariffOptionJpaController;
import za.co.resolution.NeoNetworkTariffDetailsJpaController;
import za.co.resolution.NeoNetworkTariffListJpaController;

import za.co.resolution.NeoNetworkTariffNetworkJpaController;
import za.co.resolution.NeoNetworkTariffOptionJpaController;
import za.co.resolution.NeoPracticeDetailsJpaController;
import za.co.resolution.NeoProductOptionVersionJpaController;
import za.co.resolution.NeoSysDisciplinePracticeJpaController;
import za.co.resolution.NeoTreatmentCodesJpaController;
import za.co.resolution.treatment.rule.NeoTreatmentRulesJpaController;

/**
 *
 * @author LeslieJj
 * Base command for all security and fe related webservice processing
 */
public abstract class FECommand extends Command {
    
    public static final String PROVIDER_NETWORK_S = "1";
    public static final String DISCIPLINE_NETWORK_S = "2";
    public static final String NON_NETWORK_S = "3";
    public static final String SPECIALIST_NETWORK_S = "4";

    public static final int PROVIDER_NETWORK = 1;
    public static final int DISCIPLINE_NETWORK = 2;
    public static final int NON_NETWORK = 3;
    public static final int SPECIALIST_NETWORK = 4;

    public static Logger logger = Logger.getLogger(FECommand.class);
    public static NeoLookupTypesJpaController lookupTypesController;
    
    public static NeoNetworkTariffDetailsJpaController neoNetworkTariffDetailsController;
    public static NeoNetworkTariffListJpaController networkTariffListJpaController;
    public static NeoNetworkTariffNetworkJpaController networkTariffNetworkJpaController;
    public static NeoNetworkTariffOptionJpaController neoNetworkTariffOptionJpaController;
    public static NeoNetworkTariffListJpaController neoNetworkTariffListJpaController;
    
    public static NeoNegotiatedTariffOptionJpaController neoNegotiatedTariffOptionJpaController;
    public static NeoNegotiatedTariffListJpaController neoNegotiatedTariffListJpaController;
    public static NeoNegotiatedTariffDetailsJpaController neoNegotiatedTariffDetailsJpaController;

    public static NeoTreatmentCodesJpaController neoTreatmentCodesJpaController;
    public static NeoSysDisciplinePracticeJpaController neoSysDisciplinePracticeJpaController;
    
    public static NeoContractNetworkJpaController neoContractNetworkJpaController;
    
    public static NeoContractPracticeNetworkJpaController neoContractPracticeNetworkJpaController;
    
    public static NeoProductOptionVersionJpaController neoProductOptionVerJpaController;
    
    public static NeoTreatmentRulesJpaController neoTreatmentRulesJpaController;
    
    public static NeoPracticeDetailsJpaController neoPracticeDetailsJpaController;
    
    public static NeoEntityCommonJpaController neoEntityCommonJpaController;
    

    static {
        getWebServicePort();
    }

    /**
     * INITIALIZE STATIC CONTROLLER
     */
    private static void getWebServicePort() {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ResoMemberStatementModelPU");
        lookupTypesController = new NeoLookupTypesJpaController(emf);
        neoNetworkTariffDetailsController = new NeoNetworkTariffDetailsJpaController(emf);
        networkTariffListJpaController = new NeoNetworkTariffListJpaController(emf);
        neoContractNetworkJpaController= new NeoContractNetworkJpaController(null,emf);
        networkTariffNetworkJpaController = new NeoNetworkTariffNetworkJpaController(emf);
        neoProductOptionVerJpaController = new NeoProductOptionVersionJpaController(emf);
        neoNetworkTariffOptionJpaController = new NeoNetworkTariffOptionJpaController(emf);
        neoNegotiatedTariffOptionJpaController = new NeoNegotiatedTariffOptionJpaController(emf);
        neoNegotiatedTariffListJpaController = new NeoNegotiatedTariffListJpaController(emf);
        neoNegotiatedTariffDetailsJpaController = new NeoNegotiatedTariffDetailsJpaController(emf);
        neoTreatmentCodesJpaController = new NeoTreatmentCodesJpaController(emf);
        neoSysDisciplinePracticeJpaController = new NeoSysDisciplinePracticeJpaController(null, emf);
        neoTreatmentRulesJpaController = new NeoTreatmentRulesJpaController(null, emf);
        neoNetworkTariffListJpaController = new NeoNetworkTariffListJpaController(emf);
        neoPracticeDetailsJpaController = new NeoPracticeDetailsJpaController(null,emf);
        neoContractPracticeNetworkJpaController= new NeoContractPracticeNetworkJpaController(null, emf);
        neoEntityCommonJpaController = new NeoEntityCommonJpaController(emf);
        
    }
}

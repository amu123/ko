/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.neo.command;

import flexjson.JSONSerializer;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import za.co.agile.command.FECommand;


/**
 *
 * @author Administrator
 */
public class PracticeDetailCommand extends FECommand {

    private static Logger loggerTC = Logger.getLogger(PracticeDetailCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        try {

            loggerTC.info("**************** DEBUG PracticeDetailCommand *******************");

            List<Object[]> practiceDetails = new ArrayList<Object[]>();
            
            String practiceDetailsJSON = "";
            
            //INDICATOR WHETHER PRACTICE BELONGS TO NETWORK OR NOT
            String networkSwitch = (String) request.getParameter("networkSwitch");
            
            if (networkSwitch.equalsIgnoreCase("network")) {

                practiceDetails = processNetworkPracticeDetailsRequest(request);

                practiceDetailsJSON = new JSONSerializer().deepSerialize(practiceDetails);

                loggerTC.info(practiceDetailsJSON.toString());


            } else {

                practiceDetails = processNonNetworkPracticeDetailsRequest(request);

                loggerTC.info("Total Practices returned : " + practiceDetails.size());

                practiceDetailsJSON = new JSONSerializer().deepSerialize(practiceDetails);

                loggerTC.info(practiceDetailsJSON.toString());

            }
            
            PrintWriter out = null;

            try {
                out = response.getWriter();
                out.println(practiceDetailsJSON);
            } catch (Exception ex) {
                loggerTC.error(ex);
            } finally {
                out.close();
            }

            loggerTC.info("******************************************");


        } catch (Exception e) {
            loggerTC.error(e);
        }

        return null;
    }

    private List<Object[]> processNetworkPracticeDetailsRequest(HttpServletRequest request) {

        //VALUE OF THE CODE OR DESCRIPTION 
        String practiceNameOrNumber = (String) request.getParameter("term");

        //INDICATOR WHETHER VALUE ABOVE IS FOR A PRACTICE NAME OR NUMBER
        String nameOrNumber = (String) request.getParameter("nameOrNumber");

        //INDICATOR WHETHER PRACTICE BELONGS TO NETWORK OR NOT
        String networkSwitch = (String) request.getParameter("networkSwitch");

        String networkId = (String) request.getParameter("networkId");

        loggerTC.info("practiceNameOrNumber : " + practiceNameOrNumber);
        loggerTC.info("nameOrNumber : " + nameOrNumber);
        loggerTC.info("networkSwitch : " + networkSwitch);
        loggerTC.info("networkId : " + networkId);

        List<Object[]> tmpList = new ArrayList<Object[]>();

        loggerTC.info("Finding practice by network...");

        //SEARCH BY CODE OR DESCRIPTION
        if (nameOrNumber.equalsIgnoreCase("number")) {
            tmpList = neoPracticeDetailsJpaController.findNeoPracticeDetailsByProviderNoAndNetworkId(practiceNameOrNumber, Integer.parseInt(networkId));
        } else {
            tmpList = neoPracticeDetailsJpaController.findNeoPracticeDetailsByPracticeNameAndNetworkId(practiceNameOrNumber, Integer.parseInt(networkId));
        }
        loggerTC.info("Total practice details  found : " + tmpList.size());

        return tmpList;
    }

    private List<Object[]> processNonNetworkPracticeDetailsRequest(HttpServletRequest request) {

        //VALUE OF THE CODE OR DESCRIPTION 
        String practiceNameOrNumber = (String) request.getParameter("term");

        //INDICATOR WHETHER VALUE ABOVE IS FOR A PRACTICE NAME OR NUMBER
        String nameOrNumber = (String) request.getParameter("nameOrNumber");

        //INDICATOR WHETHER PRACTICE BELONGS TO NETWORK OR NOT
        String networkSwitch = (String) request.getParameter("networkSwitch");

        String networkId = (String) request.getParameter("networkId");

        loggerTC.info("practiceNameOrNumber : " + practiceNameOrNumber);
        loggerTC.info("nameOrNumber : " + nameOrNumber);
        loggerTC.info("networkSwitch : " + networkSwitch);
        loggerTC.info("networkId : " + networkId);

        List<Object[]> tmpList = new ArrayList<Object[]>();

        loggerTC.info("Finding practice by nonNetwork...");

        //SEARCH BY CODE OR DESCRIPTION
        if (nameOrNumber.equalsIgnoreCase("number")) {
            tmpList = neoPracticeDetailsJpaController.findNeoPracticeDetailsByProviderNo(practiceNameOrNumber);
        } else {
            tmpList = neoPracticeDetailsJpaController.findNeoPracticeDetailsByPracticeName(practiceNameOrNumber);
        }

        loggerTC.info("Total practice details  found : " + tmpList.size());

        return tmpList;
    }

    @Override
    public String getName() {
        return "PracticeDetailCommand";
    }
}

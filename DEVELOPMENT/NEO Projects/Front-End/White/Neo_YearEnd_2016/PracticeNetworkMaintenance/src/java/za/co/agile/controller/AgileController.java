/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.agile.controller;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import za.co.agile.command.Command;
import za.co.neo.command.ContractNetworkCommand;
import za.co.neo.command.EditContractNetworkCommand;
import za.co.neo.command.MoveContractNetworkCommand;
import za.co.neo.command.NeoDisciplineTypesCommand;
import za.co.neo.command.NeoLookupCommand;
import za.co.neo.command.NeoOptionLookupCommand;
import za.co.neo.command.NetworkListLookup;
import za.co.neo.command.PracticeCommand;
import za.co.neo.command.PracticeDetailCommand;
import za.co.neo.command.PracticeNetworkForward;
import za.co.neo.command.RemoveContractNetworkCommand;
import za.co.neo.command.StartupCommand;
import za.co.neo.command.TariffCommand;


/**
 *
 * @author Administrator
 */
public class AgileController extends HttpServlet {
    
    HashMap<String, Command> commands = new HashMap<String, Command>();
    
    Logger logger = Logger.getLogger(AgileController.class);
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.debug("processRequest called");
        
        String requestId = request.getParameter("id"); 
        
        logger.debug("requestId : " + requestId);
         
        String command = null;

        try {
        
            Enumeration e = request.getParameterNames();
            
            while (e.hasMoreElements()) {

                String parameterName = e.nextElement().toString();
                
                logger.debug("Parameter Name : " + parameterName);
                logger.debug("Parameter Value : " + request.getParameter(parameterName));

                if (parameterName.equalsIgnoreCase("opperation")) {
                    
                    command = request.getParameter(parameterName);

                    if (command == null || command.equalsIgnoreCase("submit") || command.equalsIgnoreCase("null")) {
                        command = "LoginCommand";
                    }

                }
            }

            //executes command link to opperation attribute in the form
            Command c = commands.get(command);

            if (c != null) {
                c.execute(request, response, getServletContext());
           
            } else {
                logger.error("Cannot find command : " + command);
            }
        } catch (Exception e) {
            logger.error(e);
            logger.error(e.getStackTrace());
            
        } 
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    @Override
    public void init() throws ServletException {
        
        NeoOptionLookupCommand nolc = new NeoOptionLookupCommand();
        commands.put(nolc.getName(), nolc);
        
        NeoLookupCommand nlc = new NeoLookupCommand();
        commands.put(nlc.getName(), nlc);
        
        TariffCommand tariffC = new TariffCommand();
        commands.put(tariffC.getName(), tariffC);
      
        NetworkListLookup nll = new NetworkListLookup();
        commands.put(nll.getName(), nll);
        
        NeoDisciplineTypesCommand ndtc = new NeoDisciplineTypesCommand();
        commands.put(ndtc.getName(), ndtc);
        
        PracticeCommand pc = new PracticeCommand();
        commands.put(pc.getName(), pc);
        
        ContractNetworkCommand nc = new ContractNetworkCommand();
        commands.put(nc.getName(), nc);
        
        PracticeDetailCommand pdc = new PracticeDetailCommand();
        commands.put(pdc.getName(), pdc);
        
        RemoveContractNetworkCommand rcnc = new RemoveContractNetworkCommand();
        commands.put(rcnc.getName(), rcnc);
        
        MoveContractNetworkCommand mcnc = new MoveContractNetworkCommand();
        commands.put(mcnc.getName(), mcnc);
        
        EditContractNetworkCommand ecnc = new EditContractNetworkCommand();
        commands.put(ecnc.getName(), ecnc);

        PracticeNetworkForward pnf = new PracticeNetworkForward();
        commands.put(pnf.getName(), pnf);
        
        StartupCommand startupC = new StartupCommand();
        commands.put(startupC.getName(), startupC);
        

    }
}

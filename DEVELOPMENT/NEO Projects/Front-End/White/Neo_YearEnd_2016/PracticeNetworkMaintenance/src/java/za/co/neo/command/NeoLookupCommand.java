/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.neo.command;

import flexjson.JSONSerializer;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import net.sf.json.JSONSerializer;
//import net.sf.json.JsonConfig;
import net.sf.json.util.PropertyFilter;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.log4j.Logger;
import za.co.agile.command.FECommand;
import za.co.resolution.NeoLookupValues;

/**
 *
 * @author Administrator
 */
public class NeoLookupCommand extends FECommand {
    
    private static Logger loggerLC = Logger.getLogger(NeoLookupCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
        
        loggerLC.debug("**************** DEBUG NeoLookupCommand *******************");
        
        String lookupTypeName = (String) request.getParameter("lookupTypeName");
        
        loggerLC.debug("lookupTypeName : " + lookupTypeName);
        
        List<NeoLookupValues> lookupValues = new ArrayList<NeoLookupValues>();
        
        lookupValues.addAll(lookupTypesController.findNeoLookupTypesEntities(lookupTypeName).getNeoLookupValuesSet());
        
        Collections.sort(lookupValues, new BeanComparator("lookupMeaning"));
        
        String practiceDetailsJSON = "";
        
        /*JsonConfig config = new JsonConfig();

        config.setJsonPropertyFilter(new PropertyFilter() {

            @Override
            public boolean apply(Object source, String name, Object value) {
                //EXCLUDE LIST
                if ("lookupTypeId".equals(name)
                        || "effectiveStartDate".equals(name)
                        || "effectiveEndDate".equals(name)
                        || "creationDate".equals(name)
                        || "lastUpdatedBy".equals(name)
                        || "securityGroupId".equals(name)
                        || "lastUpdateDate".equals(name)) {
                    return true;
                }
                return false;
            }
        });

        List jsonObjects = new ArrayList();
        for (NeoLookupValues object : lookupValues) {
            jsonObjects.add(JSONSerializer.toJSON(object, config));
        }*/
        
        practiceDetailsJSON = new JSONSerializer().deepSerialize(lookupValues);
        
        loggerLC.debug("jsonObjects : " + practiceDetailsJSON.toString());

        loggerLC.debug("******************************************");
        
        PrintWriter out = null;

        try {
            out = response.getWriter();
            out.println(practiceDetailsJSON);
        } catch (Exception ex) {
            loggerLC.error(ex);
        } finally {
            out.close();
        }

        return null;
    }

    @Override
    public String getName() {
        return "NeoLookupCommand";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.neo.command;

import com.koh.network_management.command.NeoUser;
import java.io.ObjectInputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import za.co.agile.command.FECommand;


/**
 *
 * @author Administrator
 */
public class PracticeNetworkForward extends FECommand {
    
    private static Logger loggerNL = Logger.getLogger(PracticeNetworkForward.class);
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {
         
        loggerNL.info("******************PracticeNetworkForward********************");
        
        //ON THE OTHER SIDE
        ObjectInputStream is;
        
        try {
            
            loggerNL.info("Creating ObjectInputStream from request....");
            
            is = new ObjectInputStream(request.getInputStream());
            
            loggerNL.info("InputStream Ready. reading...");
            
            Object obj = is.readObject();
            
            loggerNL.info("Done");

            if (obj instanceof NeoUser) {
                
                NeoUser user = (NeoUser) obj;
                
                loggerNL.info("User Found !!");
                loggerNL.info("username : " + user.getUsername());
                
            } else {
                loggerNL.error("ERROR READING NEOUSER OBJECT !!!");
            }
                
        
        } catch (Exception ex) {
            loggerNL.error(ex);
        }


        loggerNL.info("********* PracticeNetworkForward END ********************");
            
        
        return null;
    }

    @Override
    public String getName() {
        return "PracticeNetworkForward";
    }
}

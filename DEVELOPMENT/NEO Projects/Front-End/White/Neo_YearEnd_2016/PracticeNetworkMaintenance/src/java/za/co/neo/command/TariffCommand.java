/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.neo.command;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONSerializer;
import org.apache.log4j.Logger;
import za.co.agile.command.FECommand;
import za.co.neo.command.response.TariffResult;
import za.co.resolution.NeoNegotiatedTariffDetails;
import za.co.resolution.NeoNetworkTariffDetails;

/**
 *
 * @author Administrator
 */
public class TariffCommand extends FECommand {

    private static Logger loggerTC = Logger.getLogger(TariffCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        try {

            loggerTC.debug("**************** DEBUG TariffCommand *******************");
            
            //NETWORK OR NON-NETWORK
            int networkType = Integer.parseInt(request.getParameter("networkType"));
            
            loggerTC.debug("networkType : " + networkType);
            
            List jsonObjects = new ArrayList();

            //1 = Provider NETWORK; 2 = Discipline Network 3 = NON-NETWORK
            switch (networkType) {

                case PROVIDER_NETWORK:

                    List<NeoNetworkTariffDetails> tariffs = processNetworkTypeProviderRequest(request);

                    for (NeoNetworkTariffDetails networkTarrifDetail : tariffs) {

                        TariffResult tr = new TariffResult();

                        tr.setNetworkListName(networkTarrifDetail.getNetworkListId().getNetworkListName());
                        tr.setTreatmentPrice(networkTarrifDetail.getTreatmentPrice().toString());
                        tr.setTreatmentUnits(networkTarrifDetail.getTreatmentUnits().toString());
                        tr.setTreatmentCode(networkTarrifDetail.getTreatmentCode());
                        tr.setTreatmentDescription(networkTarrifDetail.getTreatmentDescription());
                        tr.setDisciplineType(networkTarrifDetail.getDisciplineType());
                        
                        jsonObjects.add(JSONSerializer.toJSON(tr));
                    }

                    break;

                case DISCIPLINE_NETWORK:

                    List<NeoNegotiatedTariffDetails> negTariffs = processNetworkTypeDisciplineRequest(request);

                    for (NeoNegotiatedTariffDetails object : negTariffs) {

                        TariffResult tr = new TariffResult();

                        tr.setNetworkListName(object.getNegotiatedListId().getNegotiatedListName());
                        tr.setTreatmentPrice(object.getTreatmentPrice().toString());
                        tr.setTreatmentUnits(object.getTreatmentUnits().toString());
                        tr.setTreatmentCode(object.getTreatmentCode());
                        tr.setTreatmentDescription(object.getTreatmentDescription());
                        tr.setDisciplineType(object.getDisciplineType());

                        //GET RULES MESSAGE BY TREATMENT CODE SET IN PIPED DELIMTED 
                        tr.setRulesMessage(neoTreatmentRulesJpaController
                                .findNeoTreatmentRuleByTreatmentCode(
                                    processPipedOptionIds(request.getParameter("option"))[0], object.getTreatmentCode()));
                        
                        jsonObjects.add(JSONSerializer.toJSON(tr));
                    }
                    
                    break;
                
                //NON NETWORK REQUEST FOR 3
                case NON_NETWORK:
                    List<Object[]> tariffList = processNonNetworkTypeRequest(request);
                    
                    for (Object[] object : tariffList) {

                        TariffResult tr = new TariffResult();

                        tr.setNetworkListName("");
                        tr.setTreatmentPrice(object[0].toString());
                        tr.setTreatmentUnits(object[1].toString());
                        tr.setTreatmentCode(object[2].toString());
                        tr.setTreatmentDescription(object[3].toString());
                        
                        logger.debug("TreatmentPrice : " + object[0].toString());
                        logger.debug("TreatmentUnits : " + object[1].toString());
                        logger.debug("TreatmentCode : " + object[2].toString());
                        logger.debug("TreatmentDescription : " + object[3].toString());
                        logger.debug("Practice Type : " + object[4].toString());
                        logger.debug("Discipline Type : " + object[5].toString());
                        
                        tr.setDisciplineType(object[5].toString());

                        jsonObjects.add(JSONSerializer.toJSON(tr));
                    }
                    break;
                
                //SPECIALIST
                case 4:
                    
                    List<Object[]> specialistTariffList = processNonNetworkTypeRequest(request);
                    
                    for (Object[] object : specialistTariffList) {

                        TariffResult tr = new TariffResult();

                        tr.setNetworkListName("");
                        tr.setTreatmentPrice(object[0].toString());
                        tr.setTreatmentUnits(object[1].toString());
                        tr.setTreatmentCode(object[2].toString());
                        tr.setTreatmentDescription(object[3].toString());
                        
                        logger.debug("TreatmentPrice : " + object[0].toString());
                        logger.debug("TreatmentUnits : " + object[1].toString());
                        logger.debug("TreatmentCode : " + object[2].toString());
                        logger.debug("TreatmentDescription : " + object[3].toString());
                      

                        jsonObjects.add(JSONSerializer.toJSON(tr));
                    }
                    break;
               
                default:
                    loggerTC.error("Network Type parameter required !!!");
                    return null;

            }


            loggerTC.debug(jsonObjects.toString());

            loggerTC.debug("******************************************");
            
            PrintWriter out = null;

            try {
                out = response.getWriter();
                out.println(jsonObjects);
            } catch (Exception ex) {
                loggerTC.error(ex);
            } finally {
                out.close();
            }

        } catch (Exception e) {
            loggerTC.error(e);
        }

        return null;
    }

    private List<NeoNetworkTariffDetails> processNetworkTypeProviderRequest(HttpServletRequest request) {

        //VALUE OF THE CODE OR DESCRIPTION 
        String codeOrDesc = (String) request.getParameter("term");

        //INDICATOR WHETHER VALUE ABOVE IS FOR A CODE OR DESCRIPTION
        String codeOrDescription = (String) request.getParameter("codeOrDescription");

        //RETREIVE NETWORK TARIFFS FOR SELECTED NETWORK
        String selectedNetworkId = request.getParameter("networkId");
        
        //YEAR IS USED TO NARROW DOWN TARIFFS PER YEAR
        String selectedYear = request.getParameter("yearRange");

        //DISCIPLINE TYPE. THIS COMES IN AS AN ARRAY OF STRINGS 
        String disciplineType = request.getParameter("disciplineType");

        List<String> disTypes = new ArrayList<String>();
       
        //NICE STATEMENT. REMOVES " FROM STRING. PARSE []STRING TO List<String> :)
        disTypes.addAll(Arrays.asList(disciplineType.substring(1, disciplineType.length() - 1).replace("\"", "").split(",")));

        loggerTC.debug("disciplineType : " + disciplineType);
        loggerTC.debug("strippedDisciplines : " + disciplineType.substring(1, disciplineType.length() - 1).replace("\"", "").split(","));
        loggerTC.debug("codeOrDesc : " + codeOrDesc);
        loggerTC.debug("codeOrDescription : " + codeOrDescription);
        loggerTC.debug("yearRange: " + selectedYear);
        loggerTC.debug("selectedNetworkId : " + selectedNetworkId);
        loggerTC.debug("selectedYear : " + selectedYear);

        String pipedOptionIds = request.getParameter("option");
        
        String networkOptionVersionId = processPipedOptionIds(pipedOptionIds)[1];
        String optionId =  processPipedOptionIds(pipedOptionIds)[0];
        
        loggerTC.debug("optionId : " + optionId);
        loggerTC.debug("networkOptionVersionId : " + networkOptionVersionId);
        
        List<NeoNetworkTariffDetails> tmpList = new ArrayList<NeoNetworkTariffDetails>();
        
        //SEARCH BY CODE OR DESCRIPTION
        if (codeOrDescription.equalsIgnoreCase("code")) {
            tmpList = neoNetworkTariffDetailsController
                    .findByDisciplineTypeAndTariffAutoCodeCompleteNetwork(
                        Integer.parseInt(selectedNetworkId), disTypes, codeOrDesc, Integer.parseInt(networkOptionVersionId),selectedYear);
        } else {
            tmpList = neoNetworkTariffDetailsController
                    .findByDisciplineTypeAndTariffAutoDescCompleteNetwork(
                        Integer.parseInt(selectedNetworkId),disTypes, codeOrDesc, Integer.parseInt(networkOptionVersionId),selectedYear);
        }
        loggerTC.debug("Total provider tariffs returned  found : " + tmpList.size());
        return tmpList;
    }

    private List<NeoNegotiatedTariffDetails> processNetworkTypeDisciplineRequest(HttpServletRequest request) {

        //COLLECTION TO HOLD RESPONSE
        List<NeoNegotiatedTariffDetails> tmpList = new ArrayList<NeoNegotiatedTariffDetails>();

        String disciplineType = request.getParameter("disciplineType");
        String selectedNetworkId = request.getParameter("networkId");
        String selectedYear = request.getParameter("yearRange");

        String strippedDisciplines = disciplineType.substring(1, disciplineType.length() - 1);

        List<String> disTypes = new ArrayList<String>();

        //NICE STATEMENT. REMOVES " FROM STRING. PARSE STRING[] TO List<String> 
        disTypes.addAll(Arrays.asList(strippedDisciplines.replace("\"", "").split(",")));

        //THE VALUE OF THE CODE OR DESCRIPTION
        String codeOrDesc = (String) request.getParameter("term");

        //SWITCH TO SAY IF ITS A CODE OR DESCRIPTION
        String codeOrDescription = (String) request.getParameter("codeOrDescription");

        String yearRange = (String) request.getParameter("yearRange");

        String networkName = (String) request.getParameter("NetworkName");

        String option = (String) request.getParameter("option");

        loggerTC.debug("disciplineType : " + disciplineType);
        loggerTC.debug("strippedDisciplines : " + strippedDisciplines);
        loggerTC.debug("codeOrDesc : " + codeOrDesc);
        loggerTC.debug("codeOrDescription : " + codeOrDescription);
        loggerTC.debug("yearRange: " + yearRange);
        loggerTC.debug("networkName: " + networkName);
        loggerTC.debug("option: " + option);

        loggerTC.debug("selectedNetworkId : " + selectedNetworkId);
        loggerTC.debug("selectedYear : " + selectedYear);

        //SEARCH BY CODE OR DESCRIPTION
        if (codeOrDescription.equalsIgnoreCase("code")) {
            tmpList = neoNegotiatedTariffDetailsJpaController
                    .findByDisciplineTypeAndTariffAutoCodeCompleteNetwork(
                        Long.parseLong(selectedNetworkId), disTypes, codeOrDesc,yearRange);
        } else {
            tmpList = neoNegotiatedTariffDetailsJpaController
                    .findByDisciplineTypeAndTariffAutoDescCompleteNetwork(
                        Long.parseLong(selectedNetworkId), disTypes, codeOrDesc,yearRange);
        }
        loggerTC.debug("Total discipline tariffs returned  found : " + tmpList.size());
        return tmpList;
    }

    private List<Object[]> processNonNetworkTypeRequest(HttpServletRequest request) {

        List<Object[]> tariffs = new ArrayList<Object[]>();
        
        //NON-NETWORK OR SPECIALIST
        int networkType = Integer.parseInt(request.getParameter("networkType"));
        
        switch (networkType) { 
            
            //NON NETWORK PER DISCIPLINE TYPE 
            case 3: 
                
                String disciplineType = request.getParameter("disciplineType");

                String strippedDisciplines = disciplineType.substring(1, disciplineType.length() - 1);

                List<String> disTypes = new ArrayList<String>();

                //NICE STATEMENT. REMOVES " FROM STRING. PARSE STRING[] TO List<String> 
                disTypes.addAll(Arrays.asList(strippedDisciplines.replace("\"", "").split(",")));

                String codeOrDescValue = (String) request.getParameter("term");

                String codeOrDescriptionswitch = (String) request.getParameter("codeOrDescription");

                String yearRange = (String) request.getParameter("yearRange");

                loggerTC.debug("disciplineType : " + disciplineType);
                loggerTC.debug("strippedDisciplines : " + strippedDisciplines);
                loggerTC.debug("codeOrDescValue : " + codeOrDescValue);
                loggerTC.debug("codeOrDescriptionswitch : " + codeOrDescriptionswitch);
                loggerTC.debug("yearRange: " + yearRange);

                //SEARCH BY CODE OR DESCRIPTION
                if (codeOrDescriptionswitch.equalsIgnoreCase("code")) {
                    tariffs = neoTreatmentCodesJpaController.findByDisciplineTypeAndTariffCodeAutoCompleteNonNetwork(disTypes, codeOrDescValue, yearRange);
                } else {
                    tariffs = neoTreatmentCodesJpaController.findByDisciplineTypeAndTariffDescAutoCompleteNonNetwork(disTypes, codeOrDescValue, yearRange);
                }
                loggerTC.debug("Total nonnetwork tariffs returned  found : " + tariffs.size());
                
                break;
            
            //SPECIALIST WITH OPTION 
            case 4:    
               
                String specialistCodeOrDescValue = (String) request.getParameter("term");

                String specialistCodeOrDescriptionswitch = (String) request.getParameter("codeOrDescription");

                String specialistYearRange = (String) request.getParameter("yearRange");

                loggerTC.debug("codeOrDescValue : " + specialistCodeOrDescValue);
                loggerTC.debug("codeOrDescriptionswitch : " + specialistCodeOrDescriptionswitch);
                loggerTC.debug("yearRange: " + specialistYearRange);

                //SEARCH BY CODE OR DESCRIPTION
                if (specialistCodeOrDescriptionswitch.equalsIgnoreCase("code")) {
                    tariffs = neoTreatmentCodesJpaController.findBySpecialistAndTariffCodeAutoCompleteNonNetwork(specialistCodeOrDescValue, specialistYearRange);
                } else {
                    tariffs = neoTreatmentCodesJpaController.findBySpecialistAndTariffCodeAutoCompleteNonNetwork(specialistCodeOrDescValue, specialistYearRange);
                }
                loggerTC.debug("Total specialist tariffs returned  found : " + tariffs.size());
                
                break;
                
            default :
                logger.error("IMPOSSIBLE !!!!!!!!!!!!!!!!!!!!!11");
        }

        return tariffs;
    }
    
    private String[] processPipedOptionIds(String pipedOptionIds) {
    
        String[] returnedOptionIdArray = pipedOptionIds. split("\\|");
   
        loggerTC.debug("optionId : " + returnedOptionIdArray[0]);
        loggerTC.debug("networkOptionVersionId : " + returnedOptionIdArray[1]);

        return returnedOptionIdArray;
    }
    
    
    @Override
    public String getName() {
        return "TariffCommand";
    }

}

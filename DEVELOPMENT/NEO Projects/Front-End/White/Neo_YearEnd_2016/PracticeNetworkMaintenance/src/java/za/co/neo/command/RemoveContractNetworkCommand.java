/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.neo.command;

import com.koh.network_management.command.NeoUser;
import flexjson.JSONSerializer;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import za.co.agile.command.FECommand;
import za.co.resolution.NeoContractPracticeNetwork;


/**
 *
 * @author Administrator
 */
public class RemoveContractNetworkCommand extends FECommand {
    
    private static Logger loggerNL = Logger.getLogger(RemoveContractNetworkCommand.class);
    
    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        loggerNL.info("**************** DEBUG RemoveContractNetworkCommand *******************");
        
        NeoUser user = (NeoUser) request.getSession().getAttribute("networkMaintanceNeoUser");
        
        String entityCommonId = request.getParameter("entityCommonId");
        
        String terminationDate = request.getParameter("terminationDate");
        
        loggerNL.info("Remove entity Common Id : "   + entityCommonId);
        loggerNL.info("terminationDate : "   + terminationDate);
        
        List<NeoContractPracticeNetwork> networkHistory = 
                neoContractPracticeNetworkJpaController.findNeoContractPracticeNetworkByEntityCommonId(Long.parseLong(entityCommonId));
        
        //LAST CONTRACT NETWORK FOR PROVIDER
        NeoContractPracticeNetwork lastRecord = networkHistory.get(0);

        Date newEndDate = null;
        
        try {
            newEndDate = sdfDate.parse(terminationDate);
        } catch (ParseException ex) {
             loggerNL.error(ex);
        }
        
        //END PREVIOUS RECORD
        lastRecord.setEndDate(newEndDate);
        lastRecord.setLastUpdatedBy(new BigInteger("" + user.getId()));
        lastRecord.setLastUpdateDate(new Date());
        
        PrintWriter out = null;
        RespondObject respondObject = new RespondObject();

        try {
            out = response.getWriter();
            
            try {
                neoContractPracticeNetworkJpaController.edit(lastRecord);
                loggerNL.info("Practice removed from Network");
                respondObject.done = 1;
            } catch (Exception ex) {
                respondObject.done = 0;
                loggerNL.error(ex);
            }
            
            String respondObjectJson = new JSONSerializer()
                 .deepSerialize(respondObject);
            
            out.println(respondObjectJson)  ;
            
        } catch (Exception ex) {
            loggerNL.error(ex);
        } finally {
            out.close();
        } 

        loggerNL.info("******************************************");
            
        
        return null;
    }

    @Override
    public String getName() {
        return "RemoveContractNetworkCommand";
    }
    
    private class RespondObject {
    
        int done;
    
    }
}

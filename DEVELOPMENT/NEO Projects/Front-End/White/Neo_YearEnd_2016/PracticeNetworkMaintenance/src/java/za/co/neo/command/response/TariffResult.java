/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.neo.command.response;

/**
 *
 * @author Administrator
 */
public class TariffResult {
    
    String networkListName;
    String treatmentPrice;
    String treatmentUnits;
    String treatmentDescription;
    String treatmentCode;
    String disciplineType;
    String rulesMessage;
    
    

    public TariffResult() {
    }

    public String getRulesMessage() {
        return rulesMessage;
    }

    public void setRulesMessage(String rulesMessage) {
        this.rulesMessage = rulesMessage;
    }

    public String getDisciplineType() {
        return disciplineType;
    }

    public void setDisciplineType(String disciplineType) {
        this.disciplineType = disciplineType;
    }

    
    public String getTreatmentCode() {
        return treatmentCode;
    }

    public void setTreatmentCode(String treatmentCode) {
        this.treatmentCode = treatmentCode;
    }

    public String getTreatmentDescription() {
        return treatmentDescription;
    }

    public void setTreatmentDescription(String treatmentDescription) {
        this.treatmentDescription = treatmentDescription;
    }

    
    
    public String getNetworkListName() {
        return networkListName;
    }

    public void setNetworkListName(String nerworkListName) {
        this.networkListName = nerworkListName;
    }

    public String getTreatmentPrice() {
        return treatmentPrice;
    }

    public void setTreatmentPrice(String treatmentPrice) {
        this.treatmentPrice = treatmentPrice;
    }

    public String getTreatmentUnits() {
        return treatmentUnits;
    }

    public void setTreatmentUnits(String treatmentUnits) {
        this.treatmentUnits = treatmentUnits;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.agile.servlet;

import com.koh.network_management.command.NeoUser;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class AuthenticateServlet extends HttpServlet {
    
    private Logger logger = Logger.getLogger(AuthenticateServlet.class);

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("******************AuthenticateServlet********************");
        
        //ON THE OTHER SIDE
        ObjectInputStream is;
        
        ObjectOutputStream os;
        
        String result;
        
        try {
            
            is = new ObjectInputStream(request.getInputStream());
            os = new ObjectOutputStream(response.getOutputStream());
            
            logger.info("Available bytes in stream..." + is.available());
            
            logger.info("InputStream Ready. reading...");
            
            Object obj = is.readObject();
            
            logger.info("Done");

            if (obj instanceof NeoUser) {
                NeoUser user = (NeoUser) obj;
                logger.info("User Found !!");
                logger.info("username : " + user.getUsername());
                getServletContext().setAttribute("networkMaintanceNeoUser", user);
                
                if (user!=null) {
                    result = "success";
                } else {
                    result ="failed";
                }
                
                os.writeObject(result);
                
                
            } else {
                result = "failed";
                
                os.writeObject(result);
                logger.error("ERROR READING NEOUSER OBJECT !!!");
            }
                
        
        } catch (Exception ex) {
            logger.error(ex);
        }


        logger.info("********* AuthenticateServlet END ********************");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.neo.command;

import flexjson.JSONSerializer;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import za.co.agile.command.FECommand;
import za.co.neo.command.response.PracticeProviderResult;
import za.co.resolution.NeoPracticeDetails;

/**
 *
 * @author Administrator
 */
public class PracticeCommand extends FECommand {

    private static Logger loggerTC = Logger.getLogger(PracticeCommand.class);

    @Override
    public Object execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

        try {

            loggerTC.info("**************** DEBUG PracticeCommand *******************");
            
            List<Object[]> practiceDetails = processProviderPracticeRequest(request);
            
            PrintWriter out = null;

            try {
                out = response.getWriter();
                out.println(new JSONSerializer().deepSerialize(practiceDetails));
            } catch (Exception ex) {
                loggerTC.error(ex);
            } finally {
                out.close();
            }

        } catch (Exception e) {
            loggerTC.error(e);
        }
        
         loggerTC.info("******************************************");
           

        return null;
    }

    private List<Object[]> processProviderPracticeRequest(HttpServletRequest request) {

        //VALUE OF THE CODE OR DESCRIPTION 
        String provNumPracticeName = (String) request.getParameter("term");

        //INDICATOR WHETHER VALUE ABOVE IS FOR A PROVIDER NUM OR PRACTICE CODE
        String providerOrPractice = (String) request.getParameter("providerOrPractice");

        String selectedYear = request.getParameter("yearRange");
        
        String networkId = request.getParameter("networkId");
        
        loggerTC.info("provNumPracticeName : " + provNumPracticeName);
        loggerTC.info("providerOrPractice : " + providerOrPractice);
        loggerTC.info("yearRange: " + selectedYear);
        loggerTC.info("yearRange: " + networkId);

        List<Object[]> tmpList = new ArrayList<Object[]>();
        
        //SEARCH BY CODE OR DESCRIPTION
        if (providerOrPractice.equalsIgnoreCase("Provider")) {
            tmpList = neoPracticeDetailsJpaController
                    .findNeoPracticeDetailsByProviderNoAndNetworkId(provNumPracticeName,Integer.parseInt(networkId));
        } else {
            tmpList = neoPracticeDetailsJpaController
                    .findNeoPracticeDetailsByPracticeNameAndNetworkId(provNumPracticeName,Integer.parseInt(networkId));
        }
        loggerTC.info("Total provider tariffs returned  found : " + tmpList.size());
        return tmpList;
    }

    @Override
    public String getName() {
        return "PracticeCommand";
    }

}

var formatDate = new Date().format("yyyy/mm/dd");
var formatDay = new Date().format("dd");

var formatDayWord = new Date().format("dddd");
var urlString = "/PracticeNetworkMaintenance/AgileController";

$(document).ready(function() {
    
    
    

    //DECLARE JQUERY SELECTS AS VARIABLES
    //FOR THE CURRENT WINDOW. THIS GIVES US QUICK ACCESS TO ALL ELEMENTS
    //BY USING $gridsScreen. CONTROLE + SPACEBAR
    $networkMaintenanceScreen = {
        
        NetworkSwitch : $("#NetworkSwitch"),
        NetworkSwitchCheckedVal : $("input[name=NetworkSwitch]:checked"),
        
        PracticeNumber : $("#PracticeNumber"),
        PracticeNumber_error : $("#PracticeNumber_error"),
        
        PracticeName : $("#PracticeName"),
        PracticeName_error : $("#PracticeName_error"),
       
        PracticeNetworkIndicator : $("#PracticeNetworkIndicator"),
        PracticeNetworkIndicator_error : $("#PracticeNetworkIndicator_error"),
        
        NetworkIndicator : $("#NetworkIndicator"),
        NetworkIndicator_error : $("#NetworkIndicator_error"),
        
        //DIALOGS
        MoveNetworkDialogForm : $("#MoveNetworkDialog-form"),
        RemoveFromNetworkDialogForm : $("#RemoveFromNetworkDialog-form"),
        EditNetworkDialogForm : $("#EditNetworkDialog-form"),
        
        //DIALOG FIELDS
        TerminationDate : $("#TerminationDate"),
        
        ContractStartDate : $("#ContractStartDate"),
        ContractEndDate : $("#ContractEndDate"),
        
        NetworkStartDate : $("#NetworkStartDate"),
        NetworkEndDate : $("#NetworkEndDate"),
        
        
        
        //BUTTONS
        RemoveFromNetwork : $("#RemoveFromNetwork"),
        TransferPractice : $("#TransferPractice"),
        
        NetworkHistoryTable : $("#NetworkHistoryTable")
    };
    
    
    //SET NETWORK AS CHECKED OPTION
    $("input[name=NetworkSwitch][value=network]").attr('checked', 'checked');
    
    init();

});


function init() {
    initStartup();
    
    
    
}

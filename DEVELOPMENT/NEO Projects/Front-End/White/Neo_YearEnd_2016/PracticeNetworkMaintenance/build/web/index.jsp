<%-- 
    Document   : index
    Created on : 2012/03/22, 04:21:02
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <!-- STYLES SHEETS -->
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.17.custom.css"/>
        <link rel="stylesheet" href="css/jquery.loadmask.css"/>
        <link rel="stylesheet" href="css/jquery.multiselect.filter.css"/>

        <!-- JAVASCRIPT-->
        <script type="text/javascript" src="scripts/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="scripts/jquery-ui-1.8.17.custom.min.js"></script>
        <script type="text/javascript" src="scripts/jquery.multiselect.min.js"></script>
        <script type="text/javascript" src="scripts/jquery.multiselect.filter.js"></script>
        <script type="text/javascript" src="scripts/jquery.loadmask.js"></script>
        <script type="text/javascript" src="scripts/DateFormat.js"></script>

        <script type="text/javascript" src="scripts/json2.js"></script>

        <!-- PRACTICE NETWORK MAINTENANCE JAVASCRIPT LIB -->
        <script type="text/javascript" src="scripts/PracticeNetworkMaintenance.js"></script>
        <script type="text/javascript" src="scripts/JQuery.js"></script>

    </head>
    <body>

        <div id="dialog-form" title="Capture New Practice">
            <form>
                <fieldset>
                    <label for="PracticeNumber">Practice Number</label>
                    <input type="text" name="PracticeNumber" id="PracticeNumber"  />
                    <label id="PracticeNumber_error" class="error"></label>
                    <br/>

                    <label for="PracticeName">Practice Name</label>
                    <input type="text" name="PracticeName" id="PracticeName"  />
                    <label id="PracticeName_error" class="error"></label>
                    <br/>

                    <label for="DisciplineType">Discipline Type</label>
                    <select name="DisciplineType" id="DisciplineType">
                    </select>
                    <label id="DisciplineType_error" class="error"></label>
                    <br/>

                    <label for="NetworkIndicator">Network Indicator</label>
                    <input type="text" name="NetworkIndicator" id="NetworkIndicator" />
                    <label id="NetworkIndicator_error" class="error"></label>
                    <br/>

                    <label for="ContractStartDate">Contract Start Date</label>
                    <input type="text" name="ContractStartDate" id="ContractStartDate" class="datepicker" />
                    <label id="ContractStartDate_error" class="error"></label>
                    <br/>

                    <label for="ContractEndDate">Contract End Date</label>
                    <input type="text" name="ContractEndDate" id="ContractEndDate"/>
                    <label id="ContractEndDate_error" class="error"></label>
                    <br/>
                    
                    <label for="DispensingStatus">Dispensing Status</label>
                    <select name="DispensingStatus" id="DispensingStatus" >
                        <option value="1">Dispensing</option>
                        <option value="2">Non-Dispensing</option>
                    </select>
                    <label id="DispensingStatus_error" class="error"></label>
                    <br/>

                    <label for="Status">Status</label>
                    <input type="text" name="Status" id="Status"  />
                    <label id="Status_error" class="error"></label>
                    <br/>

                    <label for="BHFStartDate">BHF Start Date</label>
                    <input type="text" name="BHFStartDate" id="BHFStartDate"  />
                    <label id="BHFStartDate_error" class="error"></label>
                    <br/>

                    <label for="BHFEndDate">BHF End Date</label>
                    <input type="text" name="BHFEndDate" id="BHFEndDate"  />
                    <label id="BHFEndDate_error" class="error"></label>
                    <br/>

                </fieldset>
            </form>
        </div>


        <fieldset style="width: 150px;display: inline;vertical-align: top">

            <legend>EMC</legend>

            <ul id="EMC" class="connectedSortable">
                <li id="1.1" class="ui-state-default">Practice Name</li>
                <li id="1.2" class="ui-state-default">Practice Name</li>
                <li id="1.3" class="ui-state-default">Provider Name</li>
                <li id="1.4" class="ui-state-default">Practice Name</li>
                <li id="1.5" class="ui-state-default">Provider Name</li>
            </ul>
            <button class="create-user"> + </button>

        </fieldset>



        <fieldset style="width: 150px;display: inline;vertical-align: top">

            <legend>Clinix Hospitals</legend>

            <ul id="ClinixHospitals" class="connectedSortable">
                <li id="5200431" class="ui-state-default">Clinix Private Hospital-Soweto</li>
                <li id="2.2" class="ui-state-default">Clinix Private Hospital -Vosloorus</li>
                <li id="2.3" class="ui-state-default">Clinix private Hospital-Lesedi</li>
                <li id="2.4" class="ui-state-default">Clinix Selby Park Hospital</li>
                <li id="2.5" class="ui-state-default">Clinix Private Hospital -Sebokeng</li>
            </ul>
            <button class="create-user">+</button>

        </fieldset>

        <fieldset style="width: 150px;display: inline;vertical-align: top">

            <legend>Pathcare Pathology</legend>

            <ul id="PathcarePathology" class="connectedSortable">
                <li id="3.1"  class="ui-state-default">Practice Name</li>
                <li id="3.2" class="ui-state-default">Practice Name</li>
                <li id="3.3" class="ui-state-default">Provider Name</li>
                <li id="3.4" class="ui-state-default">Practice Name</li>
                <li id="3.5" class="ui-state-default">Provider Name</li>
            </ul>
            <button class="create-user">+</button>

        </fieldset>
    </span>


</body>
</html>

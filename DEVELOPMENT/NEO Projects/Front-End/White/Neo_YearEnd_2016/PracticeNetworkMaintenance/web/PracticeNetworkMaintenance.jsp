<%-- 
    Document   : index
    Created on : 2012/03/22, 04:21:02
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Practice Network Maintenance</title>

        <!-- STYLES SHEETS -->
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.17.custom.css"/>
        <link rel="stylesheet" href="css/jquery.loadmask.css"/>
        <link rel="stylesheet" href="css/jquery.multiselect.filter.css"/>

        <!-- JAVASCRIPT-->
        <script type="text/javascript" src="scripts/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="scripts/jquery-ui-1.8.17.custom.min.js"></script>
        <script type="text/javascript" src="scripts/jquery.validate.min.js"></script>
        <script type="text/javascript" src="scripts/jquery.multiselect.min.js"></script>
        <script type="text/javascript" src="scripts/jquery.multiselect.filter.js"></script>
        <script type="text/javascript" src="scripts/jquery.loadmask.js"></script>
        <script type="text/javascript" src="scripts/DateFormat.js"></script>
        <script type="text/javascript" src="scripts/jquery.ui.datepicker.validation.min.js"></script>

        <script type="text/javascript" src="scripts/json2.js"></script>

        <!-- PRACTICE NETWORK MAINTENANCE JAVASCRIPT LIB -->
        <script type="text/javascript" src="scripts/PracticeNetworkMaintenance.js"></script>
        <script type="text/javascript" src="scripts/JQuery.js"></script>

    </head>
    <body>
        <div id="contentDiv" style="display: none">
            
            <div id="EditNetworkDialog-form" title="Edit Network" chosenId="">
                <form id="editNetworkForm" name="editNetworkForm" >
                    <fieldset>
                        <label for="NetworkStartDate">Network Start Date</label>
                        <input type="text" name="NetworkStartDate" id="NetworkStartDate" name="NetworkStartDate" class="dpDate" />
                        <br/>

                        <label for="NetworkEndDate">Network End Date</label>
                        <input type="text" name="NetworkEndDate" id="NetworkEndDate" name="NetworkEndDate" class="dpDate" value="2999/12/31" />
                        <br/>
                    </fieldset>
                </form>
            </div>

            <div id="MoveNetworkDialog-form" title="Move To Network">
                <form id="moveNetworkForm" name="moveNetworkForm">
                    <fieldset>
                        <label for="ContractStartDate">Contract Start Date</label>
                        <input type="text" name="ContractStartDate" id="ContractStartDate" name="ContractStartDate" class="dpDate" />
                        <br/>

                        <label for="ContractEndDate">Contract End Date</label>
                        <input type="text" name="ContractEndDate" id="ContractEndDate" name="ContractEndDate" class="dpDate" value="2999/12/31" />
                        <br/>

                    </fieldset>
                </form>
            </div>

            <div id="RemoveFromNetworkDialog-form"  title="Remove From Network">
                <form id="terminationForm" name="terminationForm">
                    <fieldset>
                        <label for="TerminationDate">Termination Date</label>
                        <input type="text" name="TerminationDate" id="TerminationDate" name="TerminationDate" class="dpDate" />
                        <label id="TerminationDate_error" class="error"></label>
                        <br/>
                    </fieldset>
                </form>
            </div>

            <div id="mainDiv">
                <span id="PracticeDetailsSpan" style="float:left">

                    <fieldset>

                        <legend>Network Practice Details</legend>

                        <input type="radio" name="NetworkSwitch" value="network" checked="checked" />Network
                        <input type="radio" name="NetworkSwitch" value="nonNetwork" />Non-Network
                        <br/><br/>

                        <label for="PracticeNetworkIndicator">Network Indicator</label>
                        <input type="text" name="PracticeNetworkIndicator" id="PracticeNetworkIndicator" />
                        <label id="PracticeNetworkIndicator_error" class="error"></label>
                        <br/>

                        <label for="PracticeNumber">Practice Number</label>
                        <input type="text" name="PracticeNumber" id="PracticeNumber"  />
                        <label id="PracticeNumber_error" class="error"></label>
                        <br/>

                        <label for="PracticeName">Practice Name</label>
                        <input type="text" name="PracticeName" id="PracticeName"  />
                        <label id="PracticeName_error" class="error"></label>
                        <br/><br/>

                        <button id="RemoveFromNetwork" >Remove From Network</button>

                        <div id="GridDiv" style="visibility: visible">
                            <fieldset>
                                <legend>Network History</legend>
                                <table id="NetworkHistoryTable" class="list">
                                    <thead id="HistoryDataHeader">
                                        <tr>
                                            <th>Network Indicator</th>
                                            <th>Effective Start Date</th>
                                            <th>Termination Date</th>
                                            <th>Process Date</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </fieldset>
                        </div>

                    </fieldset>
                </span>

                <span id="TransferButtonSpan">
                    <button id="TransferPractice"> >> </button>
                </span>

                <span id="NetworkDetailsSpan" style="float:left">
                    <fieldset>
                        <legend>Network Details</legend>
                        <label for="NetworkIndicator">Network Indicator</label>
                        <input type="text" name="NetworkIndicator" id="NetworkIndicator" />
                        <label id="NetworkIndicator_error" class="error"></label>
                        <br/>
                    </fieldset>
                </span>
            </div>
    </div>

    </body>
</html>

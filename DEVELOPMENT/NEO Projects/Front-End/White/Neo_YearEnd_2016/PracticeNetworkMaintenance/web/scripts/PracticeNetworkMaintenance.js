function initAutoCompleteNetwork() {
    
    $networkMaintenanceScreen.PracticeNetworkIndicator.autocomplete({
                    
        minLength: 3,
       
        //define callback to format results
        source: function(request, response){
            AutoCompleteNetworkInputSource(request, response,"PracticeNetworkIndicator");
        },
        
        //define select handler
        select: function(e, ui) {
            if (!ui.item.value) {
                AutoCompleteNetworkSelect(ui,1);
            }
            return false;
        }
    
        //OVERRIDE DISPLAY METHOD
    }).data( "autocomplete" )._renderItem = function( ul, item ) {
        AutoCompleteNetworkDisplay(item, ul);
    };
    
    
    $networkMaintenanceScreen.NetworkIndicator.autocomplete({
                    
        minLength: 3,
       
        //define callback to format results
        source: function(request, response){
            AutoCompleteNetworkInputSource(request, response,"NetworkIndicator");
        },
        
        //define select handler
        select: function(e, ui) {
            
            if (ui.item.value!="") {
                AutoCompleteNetworkSelect(ui,2);
            }
            
            return false;
        }
    
        //OVERRIDE DISPLAY METHOD
    }).data( "autocomplete" )._renderItem = function( ul, item ) {
        AutoCompleteNetworkDisplay(item, ul);
    };
    
}

function AutoCompleteNetworkInputSource(request,response,type) {
  
    
    //UNIQUE ID
    request.id = new Date().getTime();
    request.opperation = "NetworkListLookup";
    request.yearRange = '2013';
    
    //pass request to server
    $.getJSON(urlString, request, function(serverResponse) {
        
        //CHECK IF SERVER RESPONDED
        if (serverResponse!=null && serverResponse.length!=0) {
            
            //create array for response objects
            var suggestions = [];
            
            // iterate over response from server
            for(var x=0;x < serverResponse.length;x++) {
                suggestions.push({ 
                    networkName : serverResponse[x][0],
                    networkNumber : serverResponse[x][1]
                });
            }
            //SET ARRAY TO RESPONSE FOR AUTOCOMPLETE
            response(suggestions);
            
            switch (type) {
                
                case "PracticeNetworkIndicator" :
                    $networkMaintenanceScreen.PracticeNetworkIndicator.removeClass("ui-autocomplete-loading");
                    break;
                
                case "NetworkIndicator" :
                    $networkMaintenanceScreen.NetworkIndicator.removeClass("ui-autocomplete-loading");
                    break;
                
                default :
                    alert("ERROR ON TYPE SWITCH !");
            }
         
        //IF NOTHING RETURNED
        } else {
            
            switch (type) {
                
                case "PracticeNetworkIndicator" :
                    $networkMaintenanceScreen.PracticeNetworkIndicator_error.html("Network Not Found");
                    $networkMaintenanceScreen.PracticeNetworkIndicator.removeClass("ui-autocomplete-loading");
                    
                    //DISABLE TRANSFER BUTTON IF NOTHING WAS FOUND
                    $networkMaintenanceScreen.TransferPractice.button({disabled: true});
                    break;
                
                case "NetworkIndicator" :
                    $networkMaintenanceScreen.NetworkIndicator_error.html("Network Not Found");
                    $networkMaintenanceScreen.NetworkIndicator.removeClass("ui-autocomplete-loading");
                    
                    break;
                
                default :
                    alert("ERROR ON TYPE SWITCH !");
            }
            
        }
    });
}

function AutoCompleteNetworkSelect(ui,type) {
    
    switch(type) {
        
        //PRACTICE NETWORK
        case 1:
            
            $networkMaintenanceScreen.PracticeNetworkIndicator_error.html("");
            
            //SET CHOSEN PRACTICE NETWORK
            $networkMaintenanceScreen.PracticeNetworkIndicator.val($.trim(ui.item.networkName));
            
            //ADD CHOSEN NETWORK ID TO METATAG CALLED NETWORKID TO RETREIVE LATER
            $networkMaintenanceScreen.PracticeNetworkIndicator.attr("networkId",ui.item.networkNumber);
        
            //ENABLE PRACTICE NUMBER AND NAME INPUTS
            $networkMaintenanceScreen.PracticeNumber.removeAttr('disabled');
            $networkMaintenanceScreen.PracticeNumber.removeClass('readonly');
           
            $networkMaintenanceScreen.PracticeName.removeAttr('disabled');
            $networkMaintenanceScreen.PracticeName.removeClass('readonly');
            
            //SET FOCUS ON PRACTICE NUMBER FORM ELEMENT
            $networkMaintenanceScreen.PracticeNumber.focus();
            break;
        
        //TRANSFERING NETWORK
        case 2:
            
            //SET CHOSEN NETWORK
            $networkMaintenanceScreen.NetworkIndicator.val($.trim(ui.item.networkName));
            
            //ADD CHOSEN NETWORK ID TO METATAG CALLED NETWORKID TO RETREIVE LATER
            $networkMaintenanceScreen.NetworkIndicator.attr("networkId",ui.item.networkNumber);

            //CLEAR ERROR
            $networkMaintenanceScreen.NetworkIndicator_error.html("");
            
            //IF WORKING WITH NETWORK PRACTICES
            if ($("input[name=NetworkSwitch]:checked").val()=="network") {
                 
                 //IF PRACTICE NETWORK HAS BEEN SELECTED 
                if ($networkMaintenanceScreen.PracticeNetworkIndicator.attr("networkId")==null) {

                    //CLEAR SELECTION
                    $networkMaintenanceScreen.NetworkIndicator.val("");

                    $networkMaintenanceScreen.PracticeName_error.html("Please Select Practice First");
                    $networkMaintenanceScreen.PracticeNumber_error.html("Please Select Practice First");

                } else {
                    //alert("else...");
                      $networkMaintenanceScreen.TransferPractice.button({disabled: false})
                }
             
            //PRACTICE NOT ON NETWORK
            } else {
                
                //alert("$networkMaintenanceScreen.PracticeName.val() : " + $networkMaintenanceScreen.PracticeName.val());
                //alert("$networkMaintenanceScreen.PracticeNumber.val() : " + $networkMaintenanceScreen.PracticeNumber.val());
                
                if (!$networkMaintenanceScreen.PracticeName.val() || !$networkMaintenanceScreen.PracticeNumber.val()) {
                    
                    $networkMaintenanceScreen.PracticeName_error.html("Please Select Practice First");
                    $networkMaintenanceScreen.PracticeNumber_error.html("Please Select Practice First");
                
                } else {
                    $networkMaintenanceScreen.PracticeName_error.html("");
                    $networkMaintenanceScreen.PracticeNumber_error.html("");

                    $networkMaintenanceScreen.TransferPractice.button({disabled: false})

                }
             } 
            
            
            break;
        
        default :
            alert("error");
            
    }
    
}

function AutoCompleteNetworkDisplay(item,ul) {
    
    return $( "<li></li>" )
    .data( "item.autocomplete", item )
    .append( "<a>" + item.networkName + "</a>" )
    .appendTo( ul );
}

function initAutoCompletePractice() {
    
    $networkMaintenanceScreen.PracticeNumber.autocomplete({
                    
        minLength: 4,
        
        //define callback to format results
        source: function(request, response){
            AutoCompletePracticeInputSource(request, response,'number');
        },
        
        //define select handler
        select: function(e, ui) {
            
            AutoCompletePracticeSelect(ui,1);
            
            if ($('input[name=NetworkSwitch]:checked').val()=='network') {
                $("#mainDiv").mask("Loading history....");
                retreiveNetworkHistory();
            }
            
            return false;
        }
    
        //OVERRIDE DISPLAY METHOD
    }).data( "autocomplete" )._renderItem = function( ul, item ) {
        AutoCompletePracticeDisplay(item, ul,1);
    };
    
    $networkMaintenanceScreen.PracticeName.autocomplete({
                    
        minLength: 5,
       
        //define callback to format results
        source: function(request, response){
            AutoCompletePracticeInputSource(request, response,'name');
        },
        
        //define select handler
        select: function(e, ui) {
            AutoCompletePracticeSelect(ui,2);
            if ($('input[name=NetworkSwitch]:checked').val()=='network') {
                $("#mainDiv").mask("Loading history....");
                retreiveNetworkHistory();
            }
            
            return false;
        }
    
        //OVERRIDE DISPLAY METHOD
    }).data( "autocomplete" )._renderItem = function( ul, item ) {
        AutoCompletePracticeDisplay(item, ul,2);
    };
    
}

function AutoCompletePracticeInputSource(request,response,nameOrNum) {
  
    //UNIQUE ID
    request.id = new Date().getTime();
    request.opperation = "PracticeDetailCommand";
    request.nameOrNumber = nameOrNum;
    request.networkId = $networkMaintenanceScreen.PracticeNetworkIndicator.attr("networkId");
    
    //FIND CHECKED RADIO BOX
    request.networkSwitch = $('input[name=NetworkSwitch]:checked').val();
    
    //pass request to server
    $.getJSON(urlString, request, function(serverResponse) {
        
        if (serverResponse!=null  && serverResponse.length!=0) {

            response(serverResponse);

        //NOTHING RETURNED
        }else {
            
            $networkMaintenanceScreen.TransferPractice.button({disabled: true})
            
            switch (nameOrNum) {
                case 'name' :
                    $networkMaintenanceScreen.PracticeName_error.html("Practice Not Found");
                    $networkMaintenanceScreen.PracticeName.removeClass("ui-autocomplete-loading");
                    break;
                case 'number' :
                    $networkMaintenanceScreen.PracticeNumber_error.html("Practice Not Found");
                    $networkMaintenanceScreen.PracticeNumber.removeClass("ui-autocomplete-loading");
                    break;
            }
        }
    });
}

function AutoCompletePracticeSelect(ui,type) {

    //CREATE METATAGS FOR ENTITY IDS 
    $networkMaintenanceScreen.PracticeNumber.attr("entityCommonId",ui.item[0]);
    $networkMaintenanceScreen.PracticeNumber.attr("entityId",ui.item[3]);

    //SET FORM VALUES
    $networkMaintenanceScreen.PracticeName.val($.trim(ui.item[1]));
    $networkMaintenanceScreen.PracticeNumber.val($.trim(ui.item[2]));
    
    //CLEAR ERROR MESSAGES
    $networkMaintenanceScreen.PracticeNumber_error.html("");
    $networkMaintenanceScreen.PracticeName_error.html("");
    
    //REMOVE LOADING IMAGES
    $networkMaintenanceScreen.PracticeNumber.removeClass("ui-autocomplete-loading");
    $networkMaintenanceScreen.PracticeName.removeClass("ui-autocomplete-loading");
    
    //GET VALUE OF RADIO BOXES
    var networkSwitchVal = $('input[name=NetworkSwitch]:checked').val();
            
    switch(type) {
        //NUMBER
        case 1:
            if (networkSwitchVal=='network') {
                $networkMaintenanceScreen.RemoveFromNetwork.button({disabled: false});
            } else {
                $networkMaintenanceScreen.RemoveFromNetwork.button({disabled: true});
            }
            
            break;
        
        //NAME
        case 2:
            
            if (networkSwitchVal=='network') {
                $networkMaintenanceScreen.RemoveFromNetwork.button({disabled: false});
            }else {
                $networkMaintenanceScreen.RemoveFromNetwork.button({disabled: true});
            }
            
            break;
        
        default :
            alert("error");
    }
    
}

function AutoCompletePracticeDisplay(item,ul,type) {
    
    if (type==1) {
        return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<a>" + item[2] + " : " + item[1] + "</a>" )
        .appendTo( ul );
    } else {
        return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<a>" + item[1] + " : " + item[2] + "</a>" )
        .appendTo( ul );
    }
    
}

function retreiveNetworkHistory() {
   
    //FIND CHECKED RADIO BOX
    //request.networkSwitch = $('input[name=NetworkSwitch]:checked').val();
    
    var requestData = {
        'id':new Date().getTime(),
        'entityCommonId':$networkMaintenanceScreen.PracticeNumber.attr("entityCommonId"),
        'terminationDate':$networkMaintenanceScreen.TerminationDate.val(),
        "opperation" : "ContractNetworkCommand"
    }
    
    handleServerToJsonCall(requestData, "RetreiveNetworkHistory");
    
    $(".removeFromNetwork").button({disabled: false})
    
}

//DATE DIALOGS
function initDialogs() {
    
    //MOVE PRACTICE TO NETWORK DIALOG
    $networkMaintenanceScreen.EditNetworkDialogForm.dialog({ 
                        
            autoOpen: false,
            height: 250,
            width: 350,
            modal: true,
            
            buttons: {
                    "Edit Network": function() {
                        
                        $( this ).dialog( "close" );

                        $("#mainDiv").mask("Editing Network....");
                        
                        var requestData = {
                            'id':new Date().getTime(),
                            'practiceNetworkId': $networkMaintenanceScreen.EditNetworkDialogForm.attr('chosenId'),
                            'startDate':$networkMaintenanceScreen.NetworkStartDate.val(),
                            'endDate':$networkMaintenanceScreen.NetworkEndDate.val(),
                            "opperation" : "EditContractNetworkCommand"
                        }

                        handleServerToJsonCall(requestData, "EditNetwork");
                       
                        $networkMaintenanceScreen.NetworkStartDate.val("");
                        $networkMaintenanceScreen.NetworkEndDate.val("2999/12/31");

                    },

                    Cancel: function() {
                            $networkMaintenanceScreen.EditNetworkDialogForm.dialog( "close" );
                    }
            },
            close: function() {
                //allFields.val("").removeClass( "ui-state-error" );
            }
    });
    
    $(":button:contains('Edit Network')").attr("disabled","disabled").addClass("ui-state-disabled"); 
    
    $networkMaintenanceScreen.NetworkStartDate.change(function(){
        if ($("#editNetworkForm").valid()) {
            $(":button:contains('Edit Network')").removeAttr("disabled").removeClass("ui-state-disabled"); 
        } else {
            $(":button:contains('Edit Network')").attr("disabled","disabled").addClass("ui-state-disabled"); 
        }
    });
    
    
    $networkMaintenanceScreen.NetworkEndDate.change(function(){
        if ( $("#editNetworkForm").valid()) {
            $(":button:contains('Edit Network')").removeAttr("disabled").removeClass("ui-state-disabled"); 
        } else {
            $(":button:contains('Edit Network')").attr("disabled","disabled").addClass("ui-state-disabled"); 
        }
    });
    
    
    //MOVE PRACTICE TO NETWORK DIALOG
    $networkMaintenanceScreen.MoveNetworkDialogForm.dialog({ 
                        
            autoOpen: false,
            height: 250,
            width: 350,
            modal: true,
            buttons: {
                    "Transfer Practice": function() {
                        $( this ).dialog( "close" );

                        $("#mainDiv").mask("Transfering Practice....");
                        
                        var requestData = {
                            'id':new Date().getTime(),
                            'entityCommonId':$networkMaintenanceScreen.PracticeNumber.attr("entityCommonId"),
                            'entityId':$networkMaintenanceScreen.PracticeNumber.attr("entityId"),
                            'startDate':$networkMaintenanceScreen.ContractStartDate.val(),
                            'endDate':$networkMaintenanceScreen.ContractEndDate.val(),
                            'networkId':$networkMaintenanceScreen.NetworkIndicator.attr("networkId"),
                            'networkSwitch': $('input[name=NetworkSwitch]:checked').val(),
                            "opperation" : "MoveContractNetworkCommand"
                        }

                        handleServerToJsonCall(requestData, "TransferPractice");
                        
                        $networkMaintenanceScreen.ContractStartDate.val("");
                        $networkMaintenanceScreen.ContractEndDate.val("2999/12/31");

                        //DISABLE TRANSFER BUTTON SINCE PRACTICE CANNOT BE TRANSFERED 
                        //TO SAME NETWORK AGAIN
                        $networkMaintenanceScreen.TransferPractice.button({disabled: true})

                    },

                    Cancel: function() {
                            $networkMaintenanceScreen.MoveNetworkDialogForm.dialog( "close" );
                    }
            },
            close: function() {
                //allFields.val("").removeClass( "ui-state-error" );
            }
    });
    
    $(":button:contains('Transfer Practice')").attr("disabled","disabled").addClass("ui-state-disabled"); 
    
    $networkMaintenanceScreen.ContractStartDate.change(function(){
        if ($("#moveNetworkForm").valid()) {
            $(":button:contains('Transfer Practice')").removeAttr("disabled").removeClass("ui-state-disabled"); 
        } else {
            $(":button:contains('Transfer Practice')").attr("disabled","disabled").addClass("ui-state-disabled"); 
        }
    });
    
    $networkMaintenanceScreen.ContractEndDate.change(function(){
        if ( $("#moveNetworkForm").valid()) {
            $(":button:contains('Transfer Practice')").removeAttr("disabled").removeClass("ui-state-disabled"); 
        } else {
            $(":button:contains('Transfer Practice')").attr("disabled","disabled").addClass("ui-state-disabled"); 
        }
    });
    
    
    //REMOVE FROM PRACTICE DIALOG
    $networkMaintenanceScreen.RemoveFromNetworkDialogForm.dialog({ 
                        
            autoOpen: false,
            height: 200,
            width: 350,
            modal: true,
            buttons: {
                    "Remove From Network": function() {

                        var requestData = {
                            'id':new Date().getTime(),
                            'entityCommonId':$networkMaintenanceScreen.PracticeNumber.attr("entityCommonId"),
                            'terminationDate':$networkMaintenanceScreen.TerminationDate.val(),
                            "opperation" : "RemoveContractNetworkCommand"};

                        handleServerToJsonCall(requestData, "RemoveFromNetwork");
                        
                        $networkMaintenanceScreen.TerminationDate.val("");

                    },
                    Cancel: function() {
                            $networkMaintenanceScreen.RemoveFromNetworkDialogForm.dialog( "close" );
                    }
            },
            close: function() {
                //allFields.val("").removeClass( "ui-state-error" );
            }
    });
    
    $(":button:contains('Remove From Network')").attr("disabled","disabled").addClass("ui-state-disabled"); 
    
    $networkMaintenanceScreen.TerminationDate.change(function(){
        if ( $("#terminationForm").valid()) {
            $(":button:contains('Remove From Network')").removeAttr("disabled").removeClass("ui-state-disabled"); 
        } else {
            $(":button:contains('Remove From Network')").attr("disabled","disabled").addClass("ui-state-disabled"); 
        }
    });
    
    
}

function initDateFields() {
 
      //MOVE NETWORK DATES
      $networkMaintenanceScreen.ContractStartDate.datepicker({
                                    dateFormat: 'yy/mm/dd'
                                    ,showButtonPanel: true,
                                    
                                    //FILTER OUT DAYS THAT ARE NOT THE LAST OF THE MONTH
                                    beforeShowDay: function (date) {
                                        //getDate() returns the day (0-31)
                                        if (date.getDate() == 1) {
                                            return [true, ''];
                                         }
                                         return [false, ''];
                                    }
                                });

       $networkMaintenanceScreen.ContractEndDate.datepicker({
                                    dateFormat: 'yy/mm/dd'
                                    ,showButtonPanel: true,
                                    
                                    //FILTER OUT DAYS THAT ARE NOT THE LAST OF THE MONTH
                                    beforeShowDay: function (date) {
                                        //getDate() returns the day (0-31)
                                        if (date.getDate() == LastDayOfMonth(date.getFullYear(),date.getMonth()) ) {
                                            return [true, ''];
                                         }
                                         return [false, ''];
                                    }
                                });
       
       //INIT VALIDATOR  
       $("#moveNetworkForm").validate({
            rules: {
                ContractStartDate: {
                    required: true
                
                },ContractEndDate: {
                    required: true
                }

            },
            messages: {
                ContractStartDate: 'Please enter a valid date (yyyy/mm/dd)',
                ContractEndDate: 'Please enter a valid date (yyyy/mm/dd)'
            }
        });
        
      
        //REMOVE NETWORK DATES
        $networkMaintenanceScreen.TerminationDate.datepicker({
                                    dateFormat: 'yy/mm/dd'
                                    ,showButtonPanel: true,
                                    //FILTER OUT DAYS THAT ARE NOT THE LAST OF THE MONTH
                                    beforeShowDay: function (date) {
                                        //getDate() returns the day (0-31)
                                         if (date.getDate() == LastDayOfMonth(date.getFullYear(),date.getMonth())) {
                                            return [true, ''];
                                         }
                                         return [false, ''];
                                    }
                                });
                                
        $("#terminationForm").validate({
            rules: {
                TerminationDate: {
                    required: true
                }

            },
            messages: {
                TerminationDate: 'Please enter a valid date (yyyy/mm/dd)'
            }
        });
        
       
      //EDIT NETWORK DATES  
      $networkMaintenanceScreen.NetworkStartDate.datepicker({
                                    dateFormat: 'yy/mm/dd'
                                    ,showButtonPanel: true,
                                    
                                    //FILTER OUT DAYS THAT ARE NOT THE LAST OF THE MONTH
                                    beforeShowDay: function (date) {
                                        //getDate() returns the day (0-31)
                                        if (date.getDate() == 1) {
                                            return [true, ''];
                                         }
                                         return [false, ''];
                                    }
                                });
                     
        
       $networkMaintenanceScreen.NetworkEndDate.datepicker({
                                    dateFormat: 'yy/mm/dd'
                                    ,showButtonPanel: true,
                                    
                                    //FILTER OUT DAYS THAT ARE NOT THE LAST OF THE MONTH
                                    beforeShowDay: function (date) {
                                        //getDate() returns the day (0-31)
                                        if (date.getDate() == LastDayOfMonth(date.getFullYear(),date.getMonth()) ) {
                                            return [true, ''];
                                         }
                                         return [false, ''];
                                    }
                                });
                                
       $("#editNetworkForm").validate({
            rules: {
                NetworkStartDate: {
                    required: true
                },
                NetworkEndDate: {
                    required: true
                }
            },
            messages: {
                NetworkStartDate: 'Please enter a valid date (yyyy/mm/dd)',
                NetworkEndDate: 'Please enter a valid date (yyyy/mm/dd)'
            }
        });
        
}

//GET LAST DAY OF THE MONTH
function LastDayOfMonth(Year, Month) {
    return(new Date((new Date(Year, Month+1,1)) -1)).getDate();
}

function initButtonClicks() {
    
    //CLICK EVENT FOR TRANSFER BUTTON
    $networkMaintenanceScreen.TransferPractice.button({disabled: true}).click(function() {
        $networkMaintenanceScreen.MoveNetworkDialogForm.dialog( "open" );
    });
    
    $networkMaintenanceScreen.RemoveFromNetwork.button({disabled: true}).click(function(){
       $networkMaintenanceScreen.RemoveFromNetworkDialogForm.dialog( "open" );
    })
    
}

function initRadioEvents() {
    
    $('input[name=networkSwitch]').change(function(){
        
        
        //RESET INPUT ELEMENTS
        $networkMaintenanceScreen.PracticeNetworkIndicator.val("");
        $networkMaintenanceScreen.PracticeName.val("");
        $networkMaintenanceScreen.PracticeNumber.val("");
        
        //DISABLE REMOVE ELEMENT
        $networkMaintenanceScreen.RemoveFromNetwork.button({disabled: true});
        
        //CLEAR NETWORK HISTORY TABLE
        $networkMaintenanceScreen.NetworkHistoryTable.find("tbody").html("");
         
         switch ($(this).val()) {
             case 'network' :
                 $networkMaintenanceScreen.PracticeNetworkIndicator.removeAttr('disabled');
                 $networkMaintenanceScreen.PracticeNetworkIndicator.removeClass('readonly');
                 $networkMaintenanceScreen.PracticeNetworkIndicator.focus();
                 break;
             
             case 'nonNetwork' :
                 
                 //DISABLE NETWORK INPUT 
                 $networkMaintenanceScreen.PracticeNetworkIndicator.attr('disabled', 'disabled');
                 //GRAY OUT BOX
                 $networkMaintenanceScreen.PracticeNetworkIndicator.addClass('readonly');

                 //ENABLE PRACTICE NAME 
                 $networkMaintenanceScreen.PracticeName.removeAttr('disabled');
                 $networkMaintenanceScreen.PracticeName.removeClass('readonly');
                 
                 //ENABLE PRACTICE NUMBER
                 $networkMaintenanceScreen.PracticeNumber.removeAttr('disabled');
                 $networkMaintenanceScreen.PracticeNumber.removeClass('readonly');
                 
                 //SET FOCUS ON PRACTICE NUMBER
                 $networkMaintenanceScreen.PracticeNumber.focus();
                 
                 break;
         }
       
    });
}

function handleServerToJsonCall(requestData,SubSystem) {
    
    //SEND REQUEST TO SERVER IN ASYNC
    $.getJSON(urlString,requestData, function(serverResponse) {
        
        //alert("SubSystem : " + SubSystem);

        if (serverResponse!=null  && serverResponse.length!=0) {

            switch (SubSystem) {
                //REMOVE FROM NETWORK
                case "RemoveFromNetwork" :
                    RemoveFromNetworkJson();
                    break;
                //REMOVE FROM NETWORK
                case "EditNetwork" :
                    EditNetworkPracticeJson();
                    break;
                //REMOVE FROM NETWORK
                case "TransferPractice" :
                    TransferPracticeJson();
                    break;
                //REMOVE FROM NETWORK
                case "RetreiveNetworkHistory" :
                    RetreiveNetworkHisoryJson(serverResponse);             
                    break;
                
                default :
                    alert("SUBSYSTEM NOT FOUND !");

            }//SWITCH
            
            $("#mainDiv").unmask();

        //NOTHING RETURNED
        } else {
            alert("ERROR COMMUNICATING WITH BACKEND !");
            $("#mainDiv").unmask();
        }
    });
}

function RemoveFromNetworkJson() {
    //CLOSE DIALOG
    $networkMaintenanceScreen.RemoveFromNetworkDialogForm.dialog( "close" );

    //RETREIVE NETWORK HISTORY
    retreiveNetworkHistory();
    
}

function TransferPracticeJson() {
    //CLOSE DIALOG
    $networkMaintenanceScreen.MoveNetworkDialogForm.dialog( "close" );

   
    //SET NETWORK AS SELECTED
    $("input[name=NetworkSwitch][value=network]").attr('checked', 'checked');
    
    $networkMaintenanceScreen.PracticeNetworkIndicator.val($networkMaintenanceScreen.NetworkIndicator.val())
    
    $networkMaintenanceScreen.PracticeNetworkIndicator.removeAttr('disabled');
    $networkMaintenanceScreen.PracticeNetworkIndicator.removeClass('readonly');
    
    retreiveNetworkHistory();
    
}   

function EditNetworkPracticeJson() {
    $networkMaintenanceScreen.EditNetworkDialogForm.dialog( "close" );

    retreiveNetworkHistory();
    
}

function RetreiveNetworkHisoryJson(serverResponse) {
    var htmlString = ''

    for (x=0;x<serverResponse.length;x++) {
        htmlString += "<tr>";
        htmlString += "<td>" + serverResponse[x].networkId.networkDescription + "</td>";
        htmlString +=  "<td>" + new Date(serverResponse[x].startDate).format('yyyy/mm/dd') + "</td>";

        if (new Date(serverResponse[x].endDate).format('yyyy/mm/dd')=='2999/12/31') {
            htmlString +=  "<td></td>";
        } else {
            htmlString +=  "<td>" + new Date(serverResponse[x].endDate).format('yyyy/mm/dd') + "</td>";
        }

        htmlString +=  "<td>" + new Date(serverResponse[x].creationDate).format('yyyy/mm/dd') + "</td>";

        //EDIT BUTTON
        htmlString +=  "<td><button id='" + serverResponse[x].practiceNetworkId + "' startDate='" 
                + new Date(serverResponse[x].startDate).format('yyyy/mm/dd') + "'>Edit</button></td>";

        htmlString +=  "</tr>";

    }

    //ADD HTML TO TABLE
    $networkMaintenanceScreen.NetworkHistoryTable.find("tbody").html(htmlString);

    //CLICK EVENT FOR EDIT BUTTON IN ROW
    $networkMaintenanceScreen.NetworkHistoryTable.find("button").button().click(function(){

        $networkMaintenanceScreen.NetworkStartDate.val($(this).attr("startDate"));

        //OPEN EDIT DIALOG
        $networkMaintenanceScreen.EditNetworkDialogForm.dialog( "open" );

        //ADD METATAG TO EDIT DIAGLOG DIV CALLED chosenId WITH THE VALUE OF THE ID TAG SET 
        //ABOVE IN THE FOR LOOP
        $networkMaintenanceScreen.EditNetworkDialogForm.attr('chosenId',$(this).attr("id"));
        //alert("setted id to : " + $networkMaintenanceScreen.EditNetworkDialogForm.attr('chosenId'));
    });        
    
}

function initStartup() {
    var requestData = {
        'id':new Date().getTime(),
        "opperation" : "StartupCommand"
    }
    
    $.post("/PracticeNetworkMaintenance/AgileController",requestData,function(result){
        
        if (result.substr(0, 7)!='success') {
            alert("Unauthorized Access !!!!");
            window.location.replace("/PracticeNetworkMaintenance/UnauthorizedAccess.jsp");
        
        } else {
            initDialogs();
            initDateFields();
            initButtonClicks();
            initRadioEvents();

            initAutoCompleteNetwork();
            initAutoCompletePractice();

            $networkMaintenanceScreen.PracticeNumber.attr('disabled', 'disabled');
            $networkMaintenanceScreen.PracticeNumber.addClass('readonly');

            $networkMaintenanceScreen.PracticeName.attr('disabled', 'disabled');
            $networkMaintenanceScreen.PracticeName.addClass('readonly');

            //SELECT CONTENT ON ALL TEXT INPUTS ON FOCUS
            $("input[type=text]").focus(function(){
                // Select field contents
                this.select();
            });

            //SHOW MAIN DIV
            $("#contentDiv").removeAttr("style")

            $networkMaintenanceScreen.PracticeNetworkIndicator.focus();
            
        }

        
    });
    
    
}
<%@ page language="java" import="java.util.*" %>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        
    </head>
<body>
    <table class="list">
        <thead>
        <tr>
            <th>Name</th><th>Value</th><th>Type</th>
        </tr>    
        </thead>
        <tbody>
        
        
<%
        for (Enumeration e = getServletContext().getAttributeNames();e.hasMoreElements();) {
            String attribName = (String) e.nextElement();
            Object attribValue = session.getAttribute(attribName);
            String objectType = attribValue.getClass().getName();
   
%>
        <tr>
            <td><%= attribName %></td><td><%= attribValue %></td><td><%= objectType %></td>
        </tr>
<%
        }
%>
    </tbody>
    </table>
       

</body>
</html>
